#if !UNITY_WEBGL
using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TBEasyWebCam;
using System.Linq;

public class QRDecodeTest : MonoBehaviour
{
    
    public QRCodeDecodeController e_qrController;
	public GameObject QRCodeCanvas;

	public void qrScanFinished(string dataText)
	{
        Debug.Log(dataText);
		if (dataText.Contains("https://snewpar.herokuapp.com/room/"))
		{
			string access = dataText.Split('/').Last();
			string id;
			string pin;
			if (access.Contains('&'))
			{
				id = access.Split('&').First();
				pin = access.Split('&').Last();
				PlayerPrefs.SetString("pincode", pin);
				PlayerPrefs.SetInt("fromDeepLink", 1);
				PlayerPrefs.SetInt("autoConnect", 1);
			}
			else
			{
				id = access;
			}
			print(id);
			string json = "{'token':'" + Common.getUserInfo() + "','id':'" + id + "'}";

			json = json.Replace("'", "\"");

			this.fetchText(Common.Baseurl + "api/brainstorming/addAccess", json, (res) =>
			{
				print(res);
			});
			Stop();
			QRCodeCanvas.SetActive(false);
		}
		

	}
	public void Play()
	{
		if (this.e_qrController != null)
		{
			this.e_qrController.StartWork();
		}
	}

	public void Stop()
	{
		if (this.e_qrController != null)
		{
			this.e_qrController.StopWork();
		}

		
	}




}
#endif