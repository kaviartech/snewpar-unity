﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using Unity.VideoHelper;
using UnityEngine;

public class initVideo : MonoBehaviour
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    
    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/



    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    IEnumerator Start()
    {
        yield return new WaitForSeconds(2);
        GetComponent<VideoController>().PrepareForUrl("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
    }

    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/


    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

}
