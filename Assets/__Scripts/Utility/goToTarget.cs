﻿using System;
using UnityEngine;

public class goToTarget : MonoBehaviour
{

    public Vector3 targetPos;
    internal Quaternion targetRot;
    internal Vector3 targetScale;

    void Update()
    {
        if ((transform.localPosition - targetPos).magnitude >= .01f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, 7 * Time.deltaTime);
        }

        if (Math.Abs(Quaternion.Angle(transform.localRotation, targetRot)) >= .1f)
        {
            transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRot, 7 * Time.deltaTime);
        }

        if ((transform.localScale - targetScale).magnitude >= .01f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, targetScale, 7 * Time.deltaTime);
        }
    }
}
