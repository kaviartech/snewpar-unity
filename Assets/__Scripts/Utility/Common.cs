﻿using FreeDraw;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using Unity.VectorGraphics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Common : Singleton<Common>
{

    private static string baseurl = "https://snewpar.herokuapp.com/";
    //private static string baseurl = "http://localhost:5000/";
    private static string baseurlHosted =  "https://snewpar.herokuapp.com/";
  



    

    public static string Baseurl 
    { 
        get 
        {
            print(NetworkingClient.isHosted ? baseurlHosted : baseurl);
            return NetworkingClient.isHosted ? baseurlHosted : baseurl; 
        } 
        set => baseurl = value; 
    }

    public override void Awake()
    {
        base.Awake();
        clean();
        IsFirstTime = PlayerPrefs.GetInt("isFirstTime", 1) == 1;

    }

    private void clean()
    {
        isTaskbarActive = false;
        isControlsActive = false;
    }

    private static bool isFirstTime;

    public static Sprite SpriteFromTexture2D(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }

    public static bool isTaskbarActive = false;

    public Animator taskbar;
    public GameObject votingPanel;
    public GameObject controlPanel;
    public GameObject fullScreenPanel;
    public GameObject settingMenuPanel;
    public GameObject helpPinchPanel;

    public static void toggleControlPanel(bool state)
    {
        //if (SessionManager.instance.brainstorming.isOwner || SessionManager.instance.brainstorming.mode == 1)
        //{
        if (ObjectLoader.isLoaded ||  TapToPlace.isPlaced)
        {
            instance.fullScreenPanel.SetActive(!state);
            instance.settingMenuPanel.SetActive(!state);
            if(PlayerPrefs.GetInt("isFirstTimePinchHelp", 0) == 0)
            {
                instance.helpPinchPanel.SetActive(true);
                PlayerPrefs.SetInt("isFirstTimePinchHelp", 1);
            }

        }
        
        instance.votingPanel.SetActive(!state);
        instance.controlPanel.SetActive(state);
        //}
    }
    public static void toggleTaskbar(bool state)
    {
        //if (SessionManager.instance.brainstorming.isOwner || SessionManager.instance.brainstorming.mode == 1)
        //{
            isTaskbarActive = state;
            instance.taskbar.Play(state ? "show" : "hide");
        //}
    }

    public void load(int index)
    {
        StartCoroutine(LoadYourAsyncScene(index));
    }

    public GameObject loading;
    public static bool isControlsActive = false;

    public static bool IsFirstTime
    {
        get => isFirstTime;
        set
        {
            isFirstTime = value;
            PlayerPrefs.SetInt("isFirstTime", value ? 1 : 0);
        }
    }



    public static bool isMainHelp = false;

    public GameObject startLoader(string name)
    {
        GameObject temp = Instantiate(Resources.Load("loading", typeof(GameObject))) as GameObject;

        temp.name = name;

        return temp;

    }

    internal static void showEndLoader()
    {
        instance.loading.SetActive(true);
    }

    IEnumerator LoadYourAsyncScene(int index)
    {
        loading.SetActive(true);
        Debug.Log("ass");

        yield return new WaitForSeconds(.5f);

        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index);

        //Slider sd = loading.GetComponentInChildren<Slider>(true);
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            //sd.value = asyncLoad.progress;

            yield return null;
        }
    }

    public static bool IsPointerOverUIObject(Vector2 position)
    {
        var isUi = false;
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = position;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if(EventSystem.current.currentSelectedGameObject)
            if (EventSystem.current.currentSelectedGameObject.layer == 5) {
                Debug.Log("ui");
                    isUi = true;
            }
        return isUi && results.Count > 0;
    }

    public Sprite getSvgImage(string data)
    {
        string svg = data;
        int start = data.IndexOf("<circle", 0);
        int end = data.IndexOf("</circle>", 0) + 9;
        //print(start);
        //print(end);

        string removedCircle = data.Remove(start, end - start);

        //print(circle);

        svg = removedCircle;

        var tessOptions = new VectorUtils.TessellationOptions()
        {
            StepDistance = 100.0f,
            MaxCordDeviation = 0.5f,
            MaxTanAngleDeviation = 0.1f,
            SamplingStepSize = 0.01f
        };

        // Dynamically import the SVG data, and tessellate the resulting vector scene.
        var sceneInfo = SVGParser.ImportSVG(new StringReader(svg));
        var geoms = VectorUtils.TessellateScene(sceneInfo.Scene, tessOptions);

        // Build a sprite with the tessellated geometry.
        var sprite = VectorUtils.BuildSprite(geoms, 10.0f, VectorUtils.Alignment.Center, Vector2.zero, 128, true);

        return sprite;
    }

    [System.Serializable]
    public struct UserInfo
    {
        public string id;
        public int iat, exp;

    }

    public static string getUserInfo()
    {
        string token = PlayerPrefs.GetString("token", "");

        if (!getUserInfo(out string id,out string path))
        {
            instance.load(0);
        }

        return token;


    }

    public static bool getUserInfo(out string id,out string path)
    {
        string token = PlayerPrefs.GetString("token", "");
        id = "";
        path = PlayerPrefs.GetString("path", "");
        if (token == "") return false;

        var parts = token.Split('.');
        if (parts.Length > 2)
        {
            var decode = parts[1];
            var padLength = 4 - decode.Length % 4;
            if (padLength < 4)
            {
                decode += new string('=', padLength);
            }
            var bytes = System.Convert.FromBase64String(decode);
            var userInfo = System.Text.ASCIIEncoding.ASCII.GetString(bytes);

            UserInfo i = JsonUtility.FromJson<UserInfo>(userInfo);

            id = i.id;
            int now = (int)(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
            //print(-now + i.exp);
            return i.exp > now;

        }

        return false;
    }

    public void instanceAlert(string v)
    {
        alert(v);
    }

    public void tryReconnect()
    {
        print("trying to reconnect");

        load(3);

    }

    public static void error(string v)
    {
        GameObject temp = Instantiate(Resources.Load("error", typeof(GameObject))) as GameObject;
        temp.GetComponentInChildren<TextMeshProUGUI>().text = v;
    }

    public static void alert(string v)
    {
        GameObject temp = Instantiate(Resources.Load("alert", typeof(GameObject))) as GameObject;
        temp.GetComponentInChildren<TextMeshProUGUI>().text = v;
    }

    public static Dictionary<string, string> history;
    public static void alert(string v,string id)
    {
        if(!history.TryGetValue(id,out string value))
        {
            GameObject temp = Instantiate(Resources.Load("alert", typeof(GameObject))) as GameObject;
            temp.GetComponentInChildren<TextMeshProUGUI>().text = v;
            history.Add(id, v);
        }
    }


    public static void wait(string v,string invokeable)
    {
         GameObject temp = Instantiate(Resources.Load("wait", typeof(GameObject))) as GameObject;

         temp.GetComponentInChildren<TextMeshProUGUI>().text = v;

        temp.GetComponentInChildren<Button>().onClick.AddListener(delegate
        {
            instance.CancelInvoke("tryReconnect");
        });

         instance.Invoke("tryReconnect", 10);
    }


    //internal static void alert(string v, Action p)
    //{
    //    GameObject temp = Instantiate(Resources.Load("alert", typeof(GameObject))) as GameObject;

    //    temp.GetComponentInChildren<TextMeshProUGUI>().text = v;

    //    p();
    //}

    public static void  OpenUrl(string url)
    {
        Application.OpenURL(url);
    }

    internal static void alert(string v, Action p)
    {
        GameObject temp = Instantiate(Resources.Load("alert", typeof(GameObject))) as GameObject;

        temp.GetComponentInChildren<TextMeshProUGUI>().text = v;
        Button[] bts = temp.GetComponentsInChildren<Button>();

        //bts[0].onClick.AddListener(delegate { p(); });
        bts[1].onClick.AddListener(delegate { p(); });
    }

    internal static void alertYN(string v, Action yesCallback, Action noCallback)
    {
        GameObject temp = Instantiate(Resources.Load("alert-YN", typeof(GameObject))) as GameObject;

        temp.GetComponentInChildren<TextMeshProUGUI>().text = v;
        Button[] bts = temp.GetComponentsInChildren<Button>();

        //bts[0].onClick.AddListener(delegate { p(); });
        bts[1].onClick.AddListener(delegate { yesCallback(); });
        bts[2].onClick.AddListener(delegate { noCallback(); });
        bts[0].onClick.AddListener(delegate { noCallback(); });
    }

    public bool isTeamPanelActive;
    public GameObject teamPanel, open, close;
    public void toggleTeamPanel()
    {
        open.SetActive(!open.activeSelf);
        close.SetActive(!close.activeSelf);
        deactivateAll();
        Gestures.instance.GesturesReady = false; //isActive;
        teamPanel.GetComponent<Animator>().Play(isTeamPanelActive ? "hide" : "show");
        Debug.Log(isTeamPanelActive);
        isTeamPanelActive = close.activeSelf;
    }

    public void deactivateTeamPanel()
    {
        Gestures.instance.GesturesReady = false; //isActive;
        teamPanel.GetComponent<Animator>().Play("hide");
     
        if (isTeamPanelActive)
        {
            open.SetActive(true);
            close.SetActive(false);
        }
        
        isTeamPanelActive = false;
    }

    public GameObject controlsDrawing,sizeDrawing, colorDrawing;

    public void deactivateAllInstance()
    {
        if (sizeDrawing.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("hide"))
        {
            deactivateAll(true);
            return;
        }
            
        if (MyDrawable.drawable)
        {
            if (!MyDrawable.drawable.transform.parent.gameObject.activeInHierarchy)
            {
                deactivateAll();
            }
            else
            {
                deactivateAll(true);
            }
        }
        else
        {
            Debug.Log("here");
            deactivateAll(true);
        }

    }

    internal static void deactivateAll(bool first = false)
    {
        //if (!dontReset)
        //{
        //    instance.controlsDrawing.SetActive(false);
        //    instance.sizeDrawing.GetComponent<Animator>().Play("hide");
        //    instance.colorDrawing.GetComponent<Animator>().Play("hide");

        //    drawManager.instance.ResetCanvas();
        //}

        HistoryManager.instance.deactivate();
        //MediaManager.instance.deactivate();
        //TextManager.instance.deactivate();
        PostManager.instance.deactivate();
        //FormesManager.instance.deactivate();
        if (first)
        {
            instance.fullScreenPanel.SetActive(false);
            instance.settingMenuPanel.SetActive(false);

        }
        instance.deactivateTeamPanel();
        helpSystem.moveDevice(true, false);
        helpSystem.tapToPlace(true, false);
    }
    public Animator topBarAnimator;
    public Animator timerAnimator;
    public void FullScreenEnter()
    {
        topBarAnimator.Play("hide");
        timerAnimator.Play("hide");
        settingAnimator.Play("fadeout");
        deactivateAll();
        toggleTaskbar(false);

    }
    public void FullScreenExit()
    {
        topBarAnimator.Play("show");
        timerAnimator.Play("show");
        settingAnimator.Play("fadein");

        deactivateAll();
        toggleTaskbar(true);
    }

    public bool isShown = false;
    public Animator settingAnimator;
    public Animator settingMenuAnimator;
    public void toggleSettingMenu()
    {
        isShown = !isShown;
        settingMenuAnimator.Play(isShown ? "show" : "hide");
    }

    public void set1To1Scale()
    {
        GameObject.Find("Meeting(Clone)").transform.localScale = Vector3.one;
    }
}
