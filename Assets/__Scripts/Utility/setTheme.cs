﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class setTheme : Singleton<setTheme>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public Sprite moon, sahara;

    public Transform loadingPanel,end;

    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    public override void Awake()
    {
        base.Awake();

        if (PinInput.brainstorming != null)
        {
            if (PinInput.brainstorming.owner != "")
            {

                transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = PinInput.brainstorming.theme == 0 ? moon : sahara;
                transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = PinInput.brainstorming.theme == 0 ? moon : sahara;
                if (loadingPanel)
                {
                    loadingPanel.GetChild(0).GetComponent<Image>().sprite = PinInput.brainstorming.theme == 0 ? moon : sahara;
                }
                if (end)
                {
                    end.GetChild(0).GetComponent<Image>().sprite = PinInput.brainstorming.theme == 0 ? moon : sahara;

                }
            }
        }

    }


    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void change()
    {
        //transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = PinInput.brainstorming.theme == 0 ? moon : sahara;
        //transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = PinInput.brainstorming.theme == 0 ? moon : sahara;
    }

}
