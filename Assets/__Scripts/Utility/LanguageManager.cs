﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class LanguageManager : Singleton<LanguageManager>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public TextMeshProUGUI languageText;
    //public UnityEvent<bool> change = new UnityEvent<bool>();

    public delegate void Change(bool x);
    public static event Change changed;

    public static bool isFR {
        get
        {
            return instance.IsFR;
        }
        set
        {
            instance.IsFR = value;
        }
    }

    public bool IsFR
    {
        get
        {
            return PlayerPrefs.GetInt("isFR", 1) == 1;
        }
        set
        {
            PlayerPrefs.SetInt("isFR", value ? 1 : 0);
            if(languageText != null)
            {
                languageText.text = value ? "FR" : "EN";
            }

            //change.Invoke(value);
            if (changed != null)
                changed(value);
        }

    }

    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        IsFR = IsFR;
    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/
    
    public void toggleLanguage()
    {
        IsFR = !IsFR;
    }

}
