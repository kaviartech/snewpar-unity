﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class Fetch : Singleton<Fetch>
{
    //public static void Text(string url, string json, Action<string> callback)
    //{
    //    instance.StartCoroutine(downloadText(url, json, callback));
    //}


    public static IEnumerator downloadText(string url, string json, Action<string> callback)
    {


        using (UnityWebRequest request = UnityWebRequest.Put(url, json))
        {
            request.method = UnityWebRequest.kHttpVerbPOST;

            request.SetRequestHeader("Content-Type", "Application/json");
            request.SetRequestHeader("Accept", "Application/json");
            request.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            request.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            request.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return request.SendWebRequest();

            if (!request.isNetworkError && !request.isHttpError)
            {

                callback(request.downloadHandler.text);

            }
            else
            {
                Debug.Log(request.error);
            }

        }
    }

    public static IEnumerator downloadGetText(string url, Action<string> callback)
    {

        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            request.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            request.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            request.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return request.SendWebRequest();

            if (!request.isNetworkError && !request.isHttpError)
            {

                callback(request.downloadHandler.text);

            }
            else
            {
                Debug.Log(request.error);
            }

        }

    }

    public static IEnumerator downloadImage(string name, string url, Action<Texture2D> callback, Action<string> error = null)
    {
        using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(url))
        {
            request.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            request.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            request.SetRequestHeader("Access-Control-Allow-Origin", "*");

            yield return request.SendWebRequest();

            if (!request.isNetworkError && !request.isHttpError)
            {

                callback(DownloadHandlerTexture.GetContent(request));

            }
            else
            {
                Debug.LogError(name + ":::" + url + ":::" + request.error);
                if (error != null)
                {
                    error(request.error);
                }
            }

        }
    }

    [System.Serializable]
    public struct pathResponse
    {
        public bool success;
        public string path;
    }

    public static IEnumerator uploadImage(string url,string path,string token, Action<string> callback, Action<string> error = null)
    {
        byte[] imageData = File.ReadAllBytes(path);

        var data = new List<IMultipartFormSection> {
             new MultipartFormDataSection("token",token),
             new MultipartFormFileSection("image", imageData, "test.png", "image/png")
         };

        //init handshake
        var request = UnityWebRequest.Post(url, data);
        request.SetRequestHeader("Access-Control-Allow-Credentials", "true");
        request.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
        request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        request.SetRequestHeader("Access-Control-Allow-Origin", "*");
        yield return request.SendWebRequest();


        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            error?.Invoke(request.error);
        }
        else
        {
            Debug.Log("Upload complete!");

            string newPath = JsonUtility.FromJson<pathResponse>(request.downloadHandler.text).path;

            callback(newPath);

        }

        //using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(url))
        //{

        //    yield return request.SendWebRequest();

        //    if (!request.isNetworkError && !request.isHttpError)
        //    {

        //        //callback(DownloadHandlerTexture.GetContent(request));

        //    }
        //    else
        //    {
        //        Debug.Log(request.error);
        //        if (error != null)
        //        {
        //            error(request.error);
        //        }
        //    }

        //}
    }

    //public static void Text(string url, string json, Action<string> callback, Action<string> error)
    //{
    //    instance.StartCoroutine(downloadText(url, json, callback, error));
    //}


    public static IEnumerator downloadText(string url, string json, Action<string> callback, Action<string> errorCallback)
    {


        using (UnityWebRequest request = UnityWebRequest.Put(url, json))
        {
            request.method = UnityWebRequest.kHttpVerbPOST;

            request.SetRequestHeader("Content-Type", "Application/json");
            request.SetRequestHeader("Accept", "Application/json");
            request.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            request.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            request.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return request.SendWebRequest();

            if (!request.isNetworkError && !request.isHttpError)
            {

                callback(request.downloadHandler.text);

            }
            else
            {
                Debug.Log(request.error);
                errorCallback(request.error);
            }

        }
    }
}
