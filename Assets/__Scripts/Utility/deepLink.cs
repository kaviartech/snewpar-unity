﻿#if !UNITY_WEBGL
using ImaginationOverflow.UniversalDeepLinking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UnityEngine;
using Lean.Gui;
using UnityEngine.SceneManagement;
    
public class deepLink : MonoBehaviour
{
    public static string pin;

    void Start()
    {
        DeepLinkManager.Instance.LinkActivated += Instance_LinkActivated;
    }

    private void Instance_LinkActivated(LinkActivation s)
    {
        print(s.Uri);
        string access = s.Uri.Split('/').Last();
        string id;
        string pin;
        string hd;
        string isDemo  = "0";
        if (s.Uri.Contains("/room"))
        {
            foreach (string ss in HttpUtility.ParseQueryString(s.Uri).AllKeys)
                Debug.Log(ss);

            if (HttpUtility.ParseQueryString(s.Uri).Get("HD") != null)
            {
                hd = HttpUtility.ParseQueryString(s.Uri).Get("HD");
                Debug.Log(hd);
                if (SceneManager.GetActiveScene().name == "Auth")
                    PlayerPrefs.SetInt("isHD", 1);
                else
                    GraphicManager.isHD = true;
            }
            if (HttpUtility.ParseQueryString(s.Uri).Get("is_demo") != null)
            {
                isDemo = HttpUtility.ParseQueryString(s.Uri).Get("is_demo");
            }

            string[] urlParams = access.Split('&');
            id = urlParams[0];
            pin = urlParams[1];
            Debug.Log(id);
            Debug.Log(pin);

            PlayerPrefs.SetString("pincode", pin);
            PlayerPrefs.SetInt("autoConnect", 1);
            PlayerPrefs.SetInt("fromDeepLink", 1);



            print(id);
            string json = "{'token':'" + Common.getUserInfo() + "','id':'" + id + "'}";

            json = json.Replace("'", "\"");

            this.fetchText(Common.Baseurl + "api/brainstorming/addAccess", json, (res) =>
            {
                print(res);
            });
            if (isDemo == "1")
            {
                InvitePostRequest invitePostRequest = new()
                {
                    username = PlayerPrefs.GetString("username", ""),
                    pin = pin,
                    id = id,
                    mail = "team@kaviar.app",
                    token = PlayerPrefs.GetString("token", ""),
                    isdemo = true
                };

                string jsonUti = JsonUtility.ToJson(invitePostRequest);
                print(jsonUti);
                this.fetchText(Common.Baseurl + "api/brainstorming/inviteOne", jsonUti, response =>
                {
                    print(response);
                });
            }
        }
        else if (s.Uri.Contains("/verify"))
        {
            FindObjectOfType<LeanPulse>().Pulse();
        }
        else
        {

        }
            //gi.deepLinkPortal(id);
        }

    void OnDestroy()
    {
        DeepLinkManager.Instance.LinkActivated -= Instance_LinkActivated;
    }
}
#endif