using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GraphicManager : Singleton<GraphicManager>
{
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public TextMeshProUGUI graphicText;
    //public UnityEvent<bool> change = new UnityEvent<bool>();

    public delegate void Change(bool x);
    public static event Change changed;

    public static bool isHD
    {
        get
        {
            return instance.IsHD;
        }
        set
        {
            instance.IsHD = value;
        }
    }

    public bool IsHD
    {
        get
        {
            return PlayerPrefs.GetInt("isHD", 1) == 1;
        }
        set
        {
            PlayerPrefs.SetInt("isHD", value ? 1 : 0);
            QualitySettings.SetQualityLevel(value ? 2 : 0, false);
            Debug.Log(QualitySettings.names[value ? 2 : 0]);
            if (graphicText != null)
            {
                graphicText.text = value ? "HD" : "SD";
            }

            //change.Invoke(value);
            if (changed != null)
                changed(value);
        }

    }

    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        IsHD = IsHD;
    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void toggleLanguage()
    {
        IsHD = !IsHD;
    }
}