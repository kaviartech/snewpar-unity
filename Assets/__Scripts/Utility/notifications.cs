using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.SceneManagement;

#if !UNITY_WEBGL

public class notifications : MonoBehaviour
{
    public  static string registrationToken;

    public void Start()
    {
        
        Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        Debug.Log("Received Registration Token: " + token.Token);
        registrationToken = token.Token;
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        //Debug.Log("Received a new message from: " + e.Message.Notification.ClickAction);
        //Debug.Log("Received key event: " + e.Message.Data["id"]);
        //print(e.Message.Link.AbsoluteUri);

        //string id = e.Message.Data["id"];
        //PlayerPrefs.SetString("pincode", e.Message.Data["pin"]);
        //PlayerPrefs.SetInt("autoConnect", 1);
        //PlayerPrefs.SetInt("fromDeepLink", 1);
      
        //print(id);
        //string json = "{'token':'" + Common.getUserInfo() + "','id':'" + id + "'}";

        //json = json.Replace("'", "\"");

        //this.fetchText(Common.Baseurl + "api/brainstorming/addAccess", json, (res) =>
        //{
        //    print(res);
        //});

    }

}
#endif
