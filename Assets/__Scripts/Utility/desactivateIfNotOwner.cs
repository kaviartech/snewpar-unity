﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class desactivateIfNotOwner : MonoBehaviour
{
    public bool desactivateIfFreeMode = false;
    public bool desactivateIfNotowner = false;
    void Start()
    {
        if (desactivateIfNotowner && !PinInput.brainstorming.isOwner)
        {
            gameObject.SetActive(false);
        }

        if (desactivateIfFreeMode && PinInput.brainstorming.mode == 1)
        {
            gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        if (PinInput.brainstorming.isOwner && VotingBehaviour.ideaGiven)
            gameObject.transform.localScale = new Vector3(1, 1, 1);
        else if (PinInput.brainstorming.isOwner && !VotingBehaviour.ideaGiven)
            gameObject.transform.localScale = new Vector3(0, 0, 0);
    }
}
