﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class colorImage : MonoBehaviour
{
    public Color c;
    public void color()
    {
        GetComponent<Image>().color = c;
        GetComponentsInChildren<TextMeshProUGUI>()[0].color = Color.white;
        GetComponentsInChildren<TextMeshProUGUI>()[1].color = Color.white;
    }
}
