﻿using System.Collections;
using UnityEngine;

public class alertBehaviour : MonoBehaviour
{
    public void closeAlert()
    {
        StartCoroutine(close());
    }

    IEnumerator close()
    {
        GetComponent<Animator>().Play("hide");
        yield return new WaitForSeconds(.5f);
        Destroy(gameObject);
    }
}
