﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class IdeaUiBehaviour : MonoBehaviour
{

    public string owner;

    public double time;

    public TextMeshProUGUI sad;
    public TextMeshProUGUI happy;
    public TextMeshProUGUI angry;
    public TextMeshProUGUI commentsNumber;

    public GameObject commentsPanel;

    public GameObject vidPref;

    private Submission s;

    public string content;
    public string id;
    public string contentType;

    private List<Comment> comments;

    public List<Comment> Comments
    {
        get
        {
            List<Comment> temp = new List<Comment>();

            foreach (var item in S.ideas)
            {
                if (item.id == id)
                {
                    foreach (var comm in item.comments)
                    {
                        temp.Add(comm);
                    }
                }
            }

            Comments = temp;

            return comments;
        }
        set
        {
            if (value.Count() > 0)
            {
                commentsNumber.text = value.Count().ToString();
                commentsNumber.transform.parent.parent.gameObject.SetActive(true);
                commentsNumber.transform.parent.gameObject.SetActive(true);
            }
            else
            {
                commentsNumber.text = value.Count().ToString();
                commentsNumber.transform.parent.parent.gameObject.SetActive(false);
                commentsNumber.transform.parent.gameObject.SetActive(false);
            }

            comments = value;
        }
    }

    public Submission S { get { return VotingBehaviour.instance.submissions[owner]; } set => s = value; }

    public Idea Idea
    {

        get { return S.idea(idea.id); }

        set
        {

            S.setIdea(value.id, value);
            idea = value;
        }
    }

    private Idea idea;

    public void set(MonoBehaviour mb, Idea idea, string username)
    {
        TextMeshProUGUI[] texts = GetComponentsInChildren<TextMeshProUGUI>(true);
        owner = username;
        Idea = idea;

        id = idea.id;

        content = idea.netObj.content;



        texts[0].text = username;
        //texts[1].text = idea;

        if (content == "0")
        {
            transform.GetChild(2).gameObject.SetActive(true);
            transform.GetChild(2).GetComponent<RawImage>().texture = MediaManager.instance.texxtures[0];
            transform.GetChild(2).GetComponent<RawImage>().fitIn(280, 280);
        }
        else if (content == "1")
        {
            transform.GetChild(2).gameObject.SetActive(true);
            transform.GetChild(2).GetComponent<RawImage>().texture = MediaManager.instance.texxtures[1];
            transform.GetChild(2).GetComponent<RawImage>().fitIn(280, 280);
        }
        else if (content == "2")
        {
            transform.GetChild(2).gameObject.SetActive(true);
            transform.GetChild(2).GetComponent<RawImage>().texture = MediaManager.instance.texxtures[2];
            transform.GetChild(2).GetComponent<RawImage>().fitIn(280, 280);
        }
        else if (idea.netObj.type.ToUpper() == "IMAGE" || idea.netObj.type.ToUpper() == "FORME" || idea.netObj.type.ToUpper() == "DRAW")
        {
            mb.fetchImage(content, (response) =>
            {
                transform.GetChild(2).gameObject.SetActive(true);
                transform.GetChild(2).GetComponent<RawImage>().texture = response;
                transform.GetChild(2).GetComponent<RawImage>().fitIn(280, 280);
            });
        }
        else if (idea.netObj.type.ToUpper() == "VIDEO")
        {
            GameObject temp = Instantiate(vidPref);

            RenderTexture rt = new RenderTexture(100, 100, 16, RenderTextureFormat.ARGB32);
            rt.Create();


            VideoPlayer videoPlayer = temp.GetComponentInChildren<VideoPlayer>();


            videoPlayer.targetTexture = rt;

            //print(1);

            videoPlayer.enabled = true;

            try
            {
                videoPlayer.url = content;
                //print(2);

                videoPlayer.playOnAwake = true;
                //print(3);

                videoPlayer.Play();
                //print(4);

                mb.StartCoroutine(stopInAWHile(videoPlayer, rt, 280));
            }
            catch (System.Exception)
            {
                print("videos from s3 cant stream in windows");
            }
        }
        else
        {
            texts[1].gameObject.SetActive(true);
            texts[1].text = content;
        }

        //print(username + " is owner of " + idea + " for : " + p.ToString());

        //print(p * 100 + "% like ratio");
    }

    IEnumerator stopInAWHile(VideoPlayer videoPlayer, Texture rt, int width = 430)
    {
        yield return new WaitUntil(() => videoPlayer.isPlaying);


        yield return new WaitForSeconds(.1f);

        try
        {
            videoPlayer.Pause();

            //print($"playing this path {path}");

            transform.GetChild(width == 430 ? 3 : 2).gameObject.SetActive(true);
            transform.GetChild(width == 430 ? 3 : 2).GetComponent<RawImage>().texture = rt;
            transform.GetChild(width == 430 ? 3 : 2).GetComponent<RawImage>().fitIn(width, width);

        }
        catch (Exception)
        {
            print("videos from s3 cant stream in windows");
        }

        yield return null;

    }

    internal int likes()
    {
        return S.likesCount(id);
    }

    internal void setVote(MonoBehaviour mb, Idea idea, string username)
    {
        set(mb, idea, username);

        TextMeshProUGUI[] texts = GetComponentsInChildren<TextMeshProUGUI>(true);

        //texts[1].text = idea;
        float percent = ((float)idea.likes.Count) / (idea.likes.Count + idea.dislikes.Count);

        float x = ((percent > 0 ? percent : 0) * 100);

        if (float.IsNaN(x))
        {
            x = 0;
        }

        print("______________________________________________________________________");
        print(x);
        print(x.ToString("F2"));
        print("______________________________________________________________________");

        texts[2].text = x.ToString("F2") + "% aiment cette idée";

        transform.GetChild(transform.childCount - 1).gameObject.SetActive(true);

        //print(v * 100 + "% aiment cette idée");

        //print(username + " is owner of " + idea + " for : " + v.ToString());

    }

    public void launchVote()
    {
        transform.parent.parent.parent.parent.gameObject.SetActive(false);
        GameObject.Find(id).GetComponentInChildren<itemEvents>().react();
    }


    public void comment(string comment)
    {
        VotingBehaviour.instance.comment(S, comment, Idea);
        Comments = Comments;
    }

    public void showComments()
    {
        transform.parent.parent.parent.parent.gameObject.SetActive(false);
        Instantiate(commentsPanel).GetComponent<commentsPanelManager>().set(Comments);
    }

    public Sprite[] types;
    public Image type;

    public GameObject setIdea(MonoBehaviour mb, Idea idea, string username)
    {
        owner = username;
        id = idea.id;
        Idea = idea;
        time = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;


        name = "idea_" + idea.id;

        TextMeshProUGUI[] texts = GetComponentsInChildren<TextMeshProUGUI>(true);

        content = idea.netObj.content;
        contentType = idea.netObj.type;

        texts[0].text = username;

        if (content == "0")
        {
            transform.GetChild(3).gameObject.SetActive(true);
            transform.GetChild(3).GetComponent<RawImage>().texture = MediaManager.instance.texxtures[0];
            transform.GetChild(3).GetComponent<RawImage>().fitIn(430, 430);
            type.sprite = types[5];
        }
        else if (content == "1")
        {
            transform.GetChild(3).gameObject.SetActive(true);
            transform.GetChild(3).GetComponent<RawImage>().texture = MediaManager.instance.texxtures[1];
            transform.GetChild(3).GetComponent<RawImage>().fitIn(430, 430);
            type.sprite = types[5];
        }
        else if (content == "2")
        {
            transform.GetChild(3).gameObject.SetActive(true);
            transform.GetChild(3).GetComponent<RawImage>().texture = MediaManager.instance.texxtures[2];
            transform.GetChild(3).GetComponent<RawImage>().fitIn(430, 430);
            type.sprite = types[5];
        }
        else if (idea.netObj.type.ToUpper() == "IMAGE" || idea.netObj.type.ToUpper() == "FORME" || idea.netObj.type.ToUpper() == "DRAW")
        {
            mb.fetchImage(content, (response) =>
            {
                transform.GetChild(3).gameObject.SetActive(true);
                transform.GetChild(3).GetComponent<RawImage>().texture = response;
                transform.GetChild(3).GetComponent<RawImage>().fitIn(430, 430);
            });

            type.sprite = contentType.ToUpper().Contains("DRAW") ? types[2] : contentType.ToUpper().Contains("FORME") ? types[1] : types[5];
        }
        else if (idea.netObj.type.ToUpper() == "VIDEO")
        {
            GameObject temp = Instantiate(vidPref);

            RenderTexture rt = new RenderTexture(100, 100, 16, RenderTextureFormat.ARGB32);
            rt.Create();


            VideoPlayer videoPlayer = temp.GetComponentInChildren<VideoPlayer>();

            type.sprite = types[6];

            videoPlayer.targetTexture = rt;

            //print(1);

            videoPlayer.enabled = true;

            try
            {
                videoPlayer.url = content;
                //print(2);

                videoPlayer.playOnAwake = true;
                //print(3);

                videoPlayer.Play();
                //print(4);

                mb.StartCoroutine(stopInAWHile(videoPlayer, rt));
            }
            catch (Exception)
            {
                print("videos from s3 cant stream in windows");
            }
        }
        else
        {
            texts[1].gameObject.SetActive(true);
            texts[1].text = content;

            //print("__________________________________________________________________" + typee);

            type.sprite = contentType.ToUpper().Contains("TEXT") ? types[4] : contentType.ToUpper().Contains("POST") ? types[3] : types[0];

        }


        return gameObject;
    }

    internal void refreshVote(Idea ideaSub)
    {

        TextMeshProUGUI[] texts = GetComponentsInChildren<TextMeshProUGUI>(true);

        content = ideaSub.netObj.content;

        //texts[1].text = idea;
        if (ideaSub.netObj.type == "TEXT" || ideaSub.netObj.type == "POST")
        {
            texts[1].gameObject.SetActive(true);
            texts[1].text = content;
        }


        //texts[1].text = idea;
        //FIX COUNT

        float percent = ((float)ideaSub.likes.Count) / (ideaSub.likes.Count + ideaSub.dislikes.Count);

        float x = ((percent > 0 ? percent : 0) * 100);

        if (float.IsNaN(x))
        {
            x = 0;
        }

        print("______________________________________________________________________");
        print(x);
        print(x.ToString("F2"));
        print("______________________________________________________________________");

        texts[2].text = x.ToString("F2") + "% aiment cette idée";

        //texts[2].text = (((float)idea.likes.Count) / (idea.likes.Count + idea.dislikes.Count) * 100).ToString("F2") + "% aiment cette idée";
    }

    internal void refreshIdea(Idea ideaSub)
    {
        TextMeshProUGUI[] texts = GetComponentsInChildren<TextMeshProUGUI>(true);
        
        content = ideaSub.netObj.content;

        //texts[1].text = idea;
        if (ideaSub.netObj.type == "TEXT" || ideaSub.netObj.type == "POST")
        {
            texts[1].gameObject.SetActive(true);
            texts[1].text = content;
        }
        
    }
}
