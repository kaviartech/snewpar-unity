﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class voteUiBehaviour : MonoBehaviour
{
    public GameObject statSlider;

    public void set(string question, string comment, StatsAnswer[] answers)
    {
        TextMeshProUGUI[] texts = GetComponentsInChildren<TextMeshProUGUI>();

        texts[0].text = question;
        texts[1].text = comment;

        int sum = NetworkingClient.instance.users.Count; //Sum(answers);

        int i = 1;

        foreach (var item in answers)
        {

            GameObject s = Instantiate(statSlider, transform);

            TextMeshProUGUI[] Stexts = s.GetComponentsInChildren<TextMeshProUGUI>();

            Stexts[0].text = i.ToString() + ")";

            Stexts[1].text = ((float)item.number / (float)sum * 100).ToString("00");

            s.GetComponentInChildren<Slider>().value = (float)item.number / (float)sum;

            i++;
        }

    }

    private int Sum(StatsAnswer[] answers)
    {
        int sum = 0;
        foreach (var item in answers)
        {
            sum += item.number;
        }

        return sum;
    }
}
