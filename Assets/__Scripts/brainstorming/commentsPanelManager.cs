﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

internal class commentsPanelManager : MonoBehaviour
{
    public GameObject commentUI;

    public Dictionary<string, Color> colors = new Dictionary<string, Color>();



    public void set(List<Comment> comments)
    {
        foreach (var item in comments)
        {
            Instantiate(commentUI, transform.GetChild(1).GetChild(0).GetChild(0)).GetComponent<CommentUI>().set(item.user, item.comment , getColor(item.user) );
        }
    }


    Color getColor(string user)
    {
        if (colors.TryGetValue(user, out Color c))
        {
            return c;
        }
        else
        {
            Color x = Color.HSVToRGB(Random.Range(0, 361)/360f, .53f, .79f);

            colors.Add(user, x);

            return x;
        }
    }

    public void close()
    {
        Destroy(gameObject);
    }
}