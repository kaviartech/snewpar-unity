﻿/*

*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Video;

[System.Serializable]
public class Category
{
    public string _id, name;
}
[System.Serializable]
public class ResponseCategory
{
    public bool success;
    public List<Category> categories = new List<Category>();
}
[System.Serializable]
public class stringArray
{
    public string[] categoryIDs;
    public string token;

    public stringArray(string token, string[] categories)
    {
        this.categoryIDs = categories;
        this.token = token;
    }
}

public class reactionBehaviour : Singleton<reactionBehaviour>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    //public string id;
    public Idea idea;

    public Dictionary<String, int> reactions = new Dictionary<string, int>();

    public TextMeshProUGUI happy, angry, sad, categoryText;

    public TMP_InputField inputComment;

    public GameObject category;
    public GameObject categoryPanel;


    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/

    private int sadI;
    private int happyI;
    private int angryI;
    private string currentCategory;

    public string CurrentCategory
    {
        get => currentCategory;
        set
        {
            if (value != "")
            {
                categoryText.text = value;
            }
            else
            {
                //categoryText.text = "non attribué";
            }

            currentCategory = value;
        }
    }
    public int HappyI
    {
        get => happyI;
        set
        {
            happy.text = value.ToString();
            happyI = value;
        }
    }
    public int AngryI
    {
        get => angryI;
        set
        {
            angry.text = value.ToString();
            angryI = value;
        }
    }
    public int SadI
    {
        get => sadI;
        set
        {
            sad.text = value.ToString();
            sadI = value;
        }
    }

    public string contentType { get; internal set; }

    public string currentContent;

    public Sprite[] types;
    public Image type;
    public GameObject vidPref;

    public Transform preview;

    public string content
    {
        get
        {
            return currentContent;
        } 
        set
        {
            currentContent = value;

            string idea = currentContent;
            string typee = contentType;

            preview.GetChild(0).gameObject.SetActive(false);
            preview.GetChild(1).gameObject.SetActive(false);

            MonoBehaviour mb = this;

            TextMeshProUGUI[] texts = preview.GetComponentsInChildren<TextMeshProUGUI>(true);


            if ((idea.StartsWith("http://") || idea.StartsWith("https://")) && (idea.EndsWith("jpg") || idea.EndsWith("png") || (idea.Contains("img_") && idea.Contains("amazonaws.com"))))
            {
                mb.fetchImage(idea, (response) =>
                {
                    preview.GetChild(1).gameObject.SetActive(true);
                    preview.GetChild(1).GetComponent<RawImage>().texture = response;
                    preview.GetChild(1).GetComponent<RawImage>().fitIn(370, 370);
                });

                type.sprite = typee.ToUpper().Contains("DRAW") ? types[2] : typee.ToUpper().Contains("FORME") ? types[1] : types[5];
            }
            else if ((idea.StartsWith("http://") || idea.StartsWith("https://")) && (idea.EndsWith("mp4") || idea.EndsWith("mov") || (idea.Contains("vid_") && idea.Contains("amazonaws.com"))))
            {
                GameObject temp = Instantiate(vidPref);

                RenderTexture rt = new RenderTexture(100, 100, 16, RenderTextureFormat.ARGB32);
                rt.Create();


                VideoPlayer videoPlayer = temp.GetComponentInChildren<VideoPlayer>();

                type.sprite = types[6];

                videoPlayer.targetTexture = rt;

                //print(1);

                videoPlayer.enabled = true;

                try
                {
                    videoPlayer.url = idea;
                    //print(2);

                    videoPlayer.playOnAwake = true;
                    //print(3);

                    videoPlayer.Play();
                    //print(4);

                    mb.StartCoroutine(stopInAWHile(videoPlayer, rt, 370));
                }
                catch (System.Exception)
                {
                    print("videos from s3 cant stream in windows");
                }
            }
            //else if (idea == "0")
            //{
            //    preview.GetChild(1).gameObject.SetActive(true);
            //    preview.GetChild(1).GetComponent<RawImage>().texture = MediaManager.instance.texxtures[0];
            //    preview.GetChild(1).GetComponent<RawImage>().fitIn(370, 370);
            //    type.sprite = types[5];
            //}
            //else if (idea == "1")
            //{
            //    preview.GetChild(1).gameObject.SetActive(true);
            //    preview.GetChild(1).GetComponent<RawImage>().texture = MediaManager.instance.texxtures[1];
            //    preview.GetChild(1).GetComponent<RawImage>().fitIn(370, 370);
            //    type.sprite = types[5];
            //}
            //else if (idea == "2")
            //{
            //    preview.GetChild(1).gameObject.SetActive(true);
            //    preview.GetChild(1).GetComponent<RawImage>().texture = MediaManager.instance.texxtures[2];
            //    preview.GetChild(1).GetComponent<RawImage>().fitIn(370, 370);
            //    type.sprite = types[5];
            //}
            else
            {
                texts[0].gameObject.SetActive(true);
                texts[0].text = idea;

                //print("__________________________________________________________________" + typee);

                type.sprite = typee.ToUpper().Contains("TEXT") ? types[4] : typee.ToUpper().Contains("POST") ? types[3] : types[0];

            }

        }
    }

    IEnumerator stopInAWHile(VideoPlayer videoPlayer, Texture rt, int width = 430)
    {
        yield return new WaitUntil(() => videoPlayer.isPlaying);


        yield return new WaitForSeconds(.1f);

        try
        {
            videoPlayer.Pause();

            //print($"playing this path {path}");

            preview.GetChild(1).gameObject.SetActive(true);
            preview.GetChild(1).GetComponent<RawImage>().texture = rt;
            preview.GetChild(1).GetComponent<RawImage>().fitIn(width, width);

        }
        catch (System.Exception)
        {
            print("videos from s3 cant stream in windows");
        }

        yield return null;

    }

    //public List<Category> categories = new List<Category>();

    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    private void Start()
    {

        if (PinInput.brainstorming != null)
        {
            updateCategories(PinInput.brainstorming.isOwner);

        }

    }



    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/


    public void react(int reaction)
    {
        string username = PlayerPrefs.GetString("username", "none");

        if (reactions.TryGetValue(idea.id, out int temp))
        {
            reactions[idea.id] = reaction;
            string json = $"^'idea':{JsonUtility.ToJson(idea)},'username' : '{username}' ,'reaction':{reaction},'oldReaction' : {temp} @".ToJSON();
            print(json);
            NetworkingClient.s.emit("updateReactItem", json);
        }
        else
        {
            reactions.Add(idea.id, reaction);
            string json = $"^'idea':{JsonUtility.ToJson(idea)},'username' : '{username}' ,'reaction':{reaction}@".ToJSON();
            print(json);
            NetworkingClient.s.emit("reactItem", json);

        }

        //string ideaId = "idea_" + id;

        //IdeaUiBehaviour idUB = FindInActiveObjectByName(ideaId).GetComponent<IdeaUiBehaviour>();

        //if (reaction == 2)
        //{
        //    idUB.like();
        //}
        //else
        //{
        //    idUB.dislike();
        //}

        Common.alert(LanguageManager.isFR ? "Vous avez donné votre avis" : "You gave your opinion");
        Invoke("updateReacts", 2);

        //inputComment.text = "";

        //print("________________________________ reacting");
    }

    void updateReacts()
    {
        List<int> Reacts = FindInActiveObjectByName(idea.id).GetComponentInChildren<itemEvents>().Reacts;

        var happies = Reacts.Where(num => num == 2);
        var angries = Reacts.Where(num => num == 1);
        var sads = Reacts.Where(num => num == 0);

        HappyI = happies.Count();
        AngryI = angries.Count();
        SadI = sads.Count();

        SessionManager.instance.RefreshIdeas();
    }

    public void comment()
    {
        string comm = inputComment.text;

        if (comm == "")
        {
            Common.alert(LanguageManager.isFR ? "Vous ne pouvez pas emvoyer un commentaire vide." : "You cannot send an empty comment.");
            return;
        }
        string ideaId = "idea_" + idea.id;
        IdeaUiBehaviour idUB = FindInActiveObjectByName(ideaId).GetComponent<IdeaUiBehaviour>();

        idUB.comment(comm);

        inputComment.text = "";

        Common.alert(LanguageManager.isFR ? "Votre commentaire a été envoyé avec succès" : "Your comment has been sent successfully");
    }

    public void react(Idea idea)
    {
        this.idea = idea;
        GetComponent<Animator>().Play("show");
    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void assignCategory(Category categ)
    {
        string json = $"^'idea':{JsonUtility.ToJson(idea)},'idCategory':'{categ._id}','nameCategory':'{categ.name}'@".ToJSON();

        NetworkingClient.s.emit("assignCategory", json);

        //GetComponent<Animator>().Play("hide");

        Common.alert(LanguageManager.isFR ? "la catégorie a été attribuée avec succès" : "The category has been assigned successfully");
    }

    public void categoryInfo()
    {
        Common.alert(LanguageManager.isFR ? "les catégories en bleu sont spécifiques à cette session" : "the categories in blue are specific to this session");
    }


    void updateCategories(bool isOwner)
    {
        foreach (Transform child in categoryPanel.transform)
        {
            Destroy(child.gameObject);
        }

        string token = Common.getUserInfo();

        string json = $"^'token':'{token}'@".ToJSON();

        //controlled by animator and not an owner
        if (PinInput.brainstorming.mode == 2 && !isOwner)
        {
            return;
        }

        if (PinInput.brainstorming != null)
        {

            stringArray sa = new stringArray(token, PinInput.brainstorming.categories);

            string jsonBrainstorming = JsonUtility.ToJson(sa);

            this.fetchText(Common.Baseurl + "api/category/array/get", jsonBrainstorming, response =>
            {
                //print("category response :" + response);

                ResponseCategory rc = JsonUtility.FromJson<ResponseCategory>(response);

                if (response != "")
                {
                    if (rc.success)
                    {

                        List<Category> categories = rc.categories;
                        foreach (var item in categories)
                        {
                            GameObject temp = Instantiate(category, categoryPanel.transform);

                            ColorUtility.TryParseHtmlString("#516AEF", out Color tempColor);
                            temp.GetComponent<Image>().color = tempColor;  //498B8E

                            temp.transform.GetChild(0).gameObject.SetActive(false);
                            temp.transform.GetChild(1).gameObject.SetActive(true);

                            temp.GetComponentInChildren<TextMeshProUGUI>().text = item.name;
                            temp.GetComponentInChildren<Button>().onClick.AddListener(() =>
                            {
                                inputComment.text = "";
                                assignCategory(item);
                            });

                        }
                    }
                    else
                    {
                        Common.alert("");
                    }
                }

            });


        }

        if (isOwner)
        {
            this.fetchText(Common.Baseurl + "api/category/get", json, response =>
            {
                //print("category response :" + response);

                ResponseCategory rc = JsonUtility.FromJson<ResponseCategory>(response);

                if (response != "")
                {
                    if (rc.success)
                    {

                        foreach (var item in rc.categories)
                        {
                            GameObject temp = Instantiate(category, categoryPanel.transform);

                            temp.GetComponentInChildren<TextMeshProUGUI>().text = item.name;
                            temp.GetComponentInChildren<Button>().onClick.AddListener(() =>
                            {
                                assignCategory(item);
                            });

                        }
                    }
                    else
                    {
                        Common.alert("");
                    }
                }

            });
        }
    }


    GameObject FindInActiveObjectByName(string name)
    {
        Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].hideFlags == HideFlags.None)
            {
                if (objs[i].name == name)
                {
                    return objs[i].gameObject;
                }
            }
        }
        return null;
    }
}
