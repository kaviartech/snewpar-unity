﻿/*

*/
using System;
using TMPro;
using UnityEngine.UI;

public class ideapoolBehaviour : Singleton<ideapoolBehaviour>
{
    private int ideas = 0;

    public int Ideas { get => ideas; 
        set 
        { 
            if(value == 0)
            {
                transform.GetChild(1).gameObject.SetActive(false);
                var image = transform.GetComponent<Image>();
                var tempColor = image.color;
                tempColor.a = 0.5f;
                image.color = tempColor;
            }
            else
            {
                transform.GetChild(1).gameObject.SetActive(true);
                var image = transform.GetComponent<Image>();
                var tempColor = image.color;
                tempColor.a = 1f;
                image.color = tempColor;
            }
            GetComponentInChildren<TextMeshProUGUI>(true).text = value.ToString();
            ideas = value;
        } 
    }

    internal void recalculateIdeas()
    {
        //throw new NotImplementedException();
        var count = 0;
        print("users count is = " + VotingBehaviour.instance.submissions.Count);
        
        foreach (Submission sub in VotingBehaviour.instance.submissions.Values)
        {
            print("ideas count is = " + sub.ideas.Count);
            count += sub.ideas.Count;
        }

        print("total count is = " + count);

        Ideas = count;

    }
}
