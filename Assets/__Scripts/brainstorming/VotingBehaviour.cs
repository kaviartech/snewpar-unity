﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[System.Serializable]
public class Idea
{
    public string id;
    public string username;
    public NetworkObject netObj;
    public List<string> likes = new List<string>();
    public List<string> dislikes = new List<string>();
    public List<Comment> comments = new List<Comment>();
    public bool isFirstTime = false;

    public Idea(NetworkObject item, string username)
    {
        id = item.id;
        this.username = username;
        netObj = item;
    }

    internal void destroy()
    {
        Submission parentSub = VotingBehaviour.instance.submissions[username];

        int index = -1;
        for (int i = 0; i < parentSub.ideas.Count; i++)
        {
            if(parentSub.ideas[i].id == id)
            {
                index = i;
                break;
            }
        }

        if(index > -1)
        {
            parentSub.ideas.RemoveAt(index);
        }

    }
}

[System.Serializable]
public class Submission
{
    public string username;
    public float timeSpoken;
    public List<Idea> ideas = new List<Idea>();

    public Submission(string username)
    {
        this.username = username;
    }

    public Idea Add(NetworkObject item, string username)
    {
        Idea idea = new Idea(item, username);
        ideas.Add(idea);
        return idea;
    }

    public void like(string user, string id)
    {
        foreach (var it in ideas)
        {
            if (it.id == id)
            {
                it.likes.Add(user);
                return;
            }
        }
    }

    public void dislike(string user, string id)
    {
        foreach (var it in ideas)
        {
            if (it.id == id)
            {
                it.dislikes.Add(user);
                return;
            }
        }
    }

    public void comment(string user, string comment, string id)
    {
        foreach (var it in ideas)
        {
            if (it.id == id)
            {
                it.comments.Add(new Comment(user, comment));
                return;
            }
        }
    }

    public int likesCount(string id)
    {
        foreach (var it in ideas)
        {
            if (it.id == id)
            {
                return it.likes.Count();
            }
        }
        return 0;
    }

    internal void setIdea(string id, Idea value)
    {
        foreach (var it in ideas)
        {
            if (it.id == id)
            {
                it.likes = value.likes;
                it.dislikes = value.dislikes;
                it.netObj = value.netObj;
                it.comments = value.comments;
            }
        }
    }

    internal Idea idea(string id)
    {
        foreach (var it in ideas)
        {
            if (it.id == id)
            {
                return it;
            }
        }
        return null;
    }
}

public class VotingBehaviour : Singleton<VotingBehaviour>
{
    NetworkingClient nc;

    public static bool voteEnded = false;
    public static bool ideaGiven = false;
    public GameObject ideaPanel, ideaPrefab, vidPref;

    public Dictionary<string, Submission> submissions = new Dictionary<string, Submission>();

    private string inputIdea;
    public TMP_InputField input;
    public string InputIdea { get => inputIdea; set => inputIdea = value; }
    public string CommentInput { get => commentInput; set => commentInput = value; }

    public GameObject votingTaskbar;

    void SocketSetup()
    {
        NetworkingClient.s.on("launchIdeaPool", E =>
        {
            print("lauchIdeaPool");
            if (!SessionManager.instance.brainstorming.isOwner)
            {
                transform.GetChild(1).GetComponent<Animator>().Play("show");
            }

            NetworkPoll.instance.voting.transform.parent.GetChild(1).GetComponent<Animator>().Play("select");
            NetworkPoll.instance.voting.transform.parent.GetChild(0).GetComponent<Animator>().Play("deselect");

        });

        NetworkingClient.s.on("giveIdea", E =>
        {
            print(E);
            var data = new JSONObject(E.ToJSON());
            print("giveIdea");

            Idea idea = JsonUtility.FromJson<Idea>(data.ToString());

            //print(E.data.ToString());

            addIdea(idea);

            if (!SessionManager.instance.brainstorming.isPoll)
            {
                ideapoolBehaviour.instance.recalculateIdeas();
            }

        });

        NetworkingClient.s.on("launchVote", E =>
        {
            print("launchVote");
            transform.GetChild(1).GetComponent<Animator>().Play("hide");

            NetworkPoll.instance.voting.GetComponent<Animator>().Play("select");
            NetworkPoll.instance.voting.transform.parent.GetChild(1).GetComponent<Animator>().Play("deselect");

            print("started vote");

            vote();
        });

        NetworkingClient.s.on("like", E =>
        {
            print("like");
            print(E);
            var data = new JSONObject(E.ToJSON());
            string owner = data["owner"].ToString().RemoveQuotes();
            string user = data["user"].ToString().RemoveQuotes();
            string id = data["id"].ToString().RemoveQuotes();

            submissions[owner].like(user, id);

            SessionManager.instance.RefreshIdeas();

        });

        NetworkingClient.s.on("dislike", E =>
        {
            print("dislike");
            print(E);
            var data = new JSONObject(E.ToJSON());
            string owner = data["owner"].ToString().RemoveQuotes();
            string user = data["user"].ToString().RemoveQuotes();
            string id = data["id"].ToString().RemoveQuotes();

            submissions[owner].dislike(user, id);

            SessionManager.instance.RefreshIdeas();

        });

        NetworkingClient.s.on("comment", E =>
        {
            //print("comment" + E.data.ToString());
            print(E);
            var data = new JSONObject(E.ToJSON());
            string owner = data["owner"].ToString().RemoveQuotes();
            string user = data["user"].ToString().RemoveQuotes();
            string id = data["id"].ToString().RemoveQuotes();
            string comment = data["comment"].ToString().RemoveQuotes();
            //print(user + " commenting on " + idea + " with " + comment);

            submissions[owner].comment(user, comment, id);

            foreach (var item in GetComponentsInChildren<IdeaUiBehaviour>())
            {
                item.Comments = item.Comments;
            }

            SessionManager.instance.RefreshIdeas();

        });

        NetworkingClient.s.on("endSession", E =>
        {
            if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
                if (!SessionManager.instance.brainstorming.isOwner)
                {
                    StartCoroutine(showEnd());
                }
        });

        NetworkingClient.s.on("doneVoting", E =>
        {
            voters++;
            if (voters == NetworkingClient.serverObjects.Count)
            {
                SessionManager.instance.RefreshIdeas();
            }
        });
    }
    private void Start()
    {

        SocketSetup();
    }

    public int voters = 0;

    public void giveIdea()
    {
        if (TapToPlace.instance.spawnedObject)
        {
            if (input.text == "")
            {
                Common.alert(LanguageManager.isFR ? "Vous ne pouvez pas soumettre une idée vide." : "You cannot submit an empty idea.");
                return;
            }else if (voteEnded)
            {
                Common.alert(LanguageManager.isFR ? "Vous ne pouvez pas soumettre une idée le vote est terminé." : "You cannot submit an idea the vote has endend.");
                return;
            }

            string username = PlayerPrefs.GetString("username", "none");

            if (!submissions.TryGetValue(username, out Submission s))
            {
                submissions.Add(username, new Submission(username));

            }

            PlacementSystem.instance.placeIdea(inputIdea);

            //submissions[PlayerPrefs.GetString("username", "none")].Add(inputIdea);
            submissions[username].timeSpoken = AccountManager.instance.timeSpoken;

            //Submission s = new Submission(PlayerPrefs.GetString("username", "none"));

            //s.ideas.Add(new Idea(inputIdea));

            transform.GetChild(1).GetComponentInChildren<TMP_InputField>().text = "";
            inputIdea = "";
            ideaGiven = true;

            //NetworkingClient.s.emit("giveIdea", new JSONObject(JsonUtility.ToJson(submissions[PlayerPrefs.GetString("username", "none")])));
        }else
        {
            Common.alert(LanguageManager.isFR ? "Posez l'objet avant de pouvoir donner une idée." : "Place the object before submiting and idea.");
        }
    }


    public void giveIdea(NetworkObject item)
    {

        string username = PlayerPrefs.GetString("username", "none");

        if (!submissions.TryGetValue(username, out Submission s))
        {
            submissions.Add(username, new Submission(username));

        }
        Idea idea = submissions[username].Add(item, username);
        submissions[username].timeSpoken = AccountManager.instance.timeSpoken;

        //Submission s = new Submission(PlayerPrefs.GetString("username", "none"));

        //s.ideas.Add(new Idea(inputIdea));

        //ideapoolBehaviour.instance.Ideas++;

        //print(JsonUtility.ToJson(submissions[PlayerPrefs.GetString("username", "none")]));

        NetworkingClient.s.emit("giveIdea", JsonUtility.ToJson(idea));

        SessionManager.instance.RefreshIdeas();
    }

    private void addIdea(Idea idea)
    {

        ideaPanel.GetComponentsInChildren<TextMeshProUGUI>().Last().text = "";

        //Idea idea = s.ideas.Last();

        print($"idea received from {idea.username} : {idea.id}");

        string username = PlayerPrefs.GetString("username", "none");

        if (idea.isFirstTime || idea.username != username)
        {
            if (!submissions.TryGetValue(idea.username, out Submission su))
            {
                submissions.Add(idea.username, new Submission(idea.username));
            }

            submissions[idea.username].Add(idea.netObj, idea.username);
        }


        SessionManager.instance.RefreshIdeas();

        Instantiate(ideaPrefab, ideaPanel.transform.GetChild(1).GetChild(1).GetChild(0))
            .GetComponent<IdeaUiBehaviour>()
            .setIdea(this, idea, idea.username);

    }

    public void launchIdeaPool()
    {
        if (!SessionManager.instance.brainstorming.isOwner)
        {
            return;
        }

        NetworkPoll.instance.voting.transform.parent.GetChild(1).GetComponent<Animator>().Play("select");
        NetworkPoll.instance.voting.transform.parent.GetChild(0).GetComponent<Animator>().Play("deselect");

        NetworkingClient.s.emit("launchIdeaPool", "");

        ideaPanel.SetActive(true);


        ideaPanel.GetComponentsInChildren<TextMeshProUGUI>().Last().text = "";

    }

    public void launchVote()
    {
        NetworkingClient.s.emit("launchVote", "");

        NetworkPoll.instance.voting.GetComponent<Animator>().Play("select");
        NetworkPoll.instance.voting.transform.parent.GetChild(1).GetComponent<Animator>().Play("deselect");

        ideaPanel.SetActive(false);

        vote();
    }

    int currentSubmission = 0;
    int currentIdea = 0;
    public GameObject ideaVote;
    public GameObject end;
    public GameObject ownerEnd;

    public void vote()
    {
        if (Common.isTaskbarActive)
        {
            //activate taskbar
            PostManager.instance.GetComponentInParent<Animator>().Play("hide");
            Common.isTaskbarActive = false;
        }
        if (submissions.Count > 0)
        {
            votingTaskbar.SetActive(true);
            Debug.Log("submission count "+submissions.Count);
            showIdea();
        }
        else
        {
            Common.alert(LanguageManager.isFR ? "il n'y a pas d'idées sur lesquelles voter" : "there are no ideas to vote on");
            PostManager.instance.GetComponentInParent<Animator>().Play("hide");
            Common.isTaskbarActive = true;
        }
    }

    GameObject lastIdea;

    public void showIdea()
    {
        Debug.Log("submissions "+ submissions);
        Debug.Log("current idea " + currentIdea);
        Debug.Log("current submission " + currentSubmission);
        Idea temp = submissions.ToList()[currentSubmission].Value.ideas[currentIdea];

        GameObject i = Instantiate(ideaVote);

        lastIdea = i;

        i.GetComponentsInChildren<TextMeshProUGUI>(true)[0].text = submissions.ToList()[currentSubmission].Key;

        RawImage ri = i.GetComponentsInChildren<RawImage>(true)[0];

        if (temp.netObj.content == "0")
        {
            ri.gameObject.SetActive(true);
            //ri.texture = MediaManager.instance.texxtures[0];
            ri.fitIn(739, 739);
        }
        else if (temp.netObj.content == "1")
        {
            ri.gameObject.SetActive(true);
            //ri.texture = MediaManager.instance.texxtures[1];
            ri.fitIn(739, 739);
        }
        else if (temp.netObj.content == "2")
        {

            ri.gameObject.SetActive(true);
            //ri.texture = MediaManager.instance.texxtures[2];
            ri.fitIn(739, 739);

        }
        else if (temp.netObj.type.ToUpper() == "IMAGE" || temp.netObj.type.ToUpper() == "FORME" || temp.netObj.type.ToUpper() == "DRAW")
        {
            this.fetchImage(temp.netObj.content, (response) =>
            {
                ri.gameObject.SetActive(true);
                ri.texture = response;
                ri.fitIn(739, 739);
            });
        }
        else if (temp.netObj.type.ToUpper() == "VIDEO")
        {
            GameObject tempVid = Instantiate(vidPref);

            RenderTexture rt = new RenderTexture(100, 100, 16, RenderTextureFormat.ARGB32);
            rt.Create();


            VideoPlayer videoPlayer = tempVid.GetComponentInChildren<VideoPlayer>();


            videoPlayer.targetTexture = rt;

            //print(1);

            videoPlayer.enabled = true;

            try
            {
                videoPlayer.url = temp.netObj.content;
                //print(2);

                videoPlayer.playOnAwake = true;
                //print(3);

                videoPlayer.Play();
                //print(4);

                StartCoroutine(stopInAWHile(videoPlayer, rt));
            }
            catch (System.Exception)
            {
                print("videos from s3 cant stream in windows");
            }
        }
        else
        {
            i.GetComponentsInChildren<TextMeshProUGUI>(true)[1].gameObject.SetActive(true);
            i.GetComponentsInChildren<TextMeshProUGUI>(true)[1].text = temp.netObj.content;
        }




    }


    IEnumerator stopInAWHile(VideoPlayer videoPlayer, Texture rt, int width = 430)
    {
        yield return new WaitUntil(() => videoPlayer.isPlaying);


        yield return new WaitForSeconds(.1f);

        try
        {
            videoPlayer.Pause();

            //print($"playing this path {path}");

            transform.GetChild(width == 430 ? 3 : 2).gameObject.SetActive(true);
            transform.GetChild(width == 430 ? 3 : 2).GetComponent<RawImage>().texture = rt;
            transform.GetChild(width == 430 ? 3 : 2).GetComponent<RawImage>().fitIn(width, width);

        }
        catch (Exception)
        {
            print("videos from s3 cant stream in windows");
        }

        yield return null;

    }

    void next()
    {
        lastIdea.GetComponent<alertBehaviour>().closeAlert();
        currentIdea++;

        if (currentIdea == submissions.ToList()[currentSubmission].Value.ideas.Count)
        {
            currentSubmission++;

            if (currentSubmission == submissions.Count)
            {
                Common.alert(LanguageManager.isFR ? "le vote est terminé, vous pouvez trouver des résultats sur le côté, jusqu'à ce que l'animateur termine cette réunion." : "the vote is over, you can find results on the side, until the host finishes this meeting.");

                votingTaskbar.GetComponentInParent<Animator>().Play("hide");

                //MediaManager.instance.GetComponentInParent<Animator>().Play("show");
                //Common.isTaskbarActive = true;
                Common.isTaskbarActive = false;

                SessionManager.instance.RefreshIdeas();

                NetworkingClient.s.emit("doneVoting",  "");
                voteEnded = true;
            }
            else
            {
                currentIdea = 0;
                showIdea();
            }

        }
        else
        {
            showIdea();
        }
    }

    public void showEndOwner()
    {
        ownerEnd.SetActive(true);
        votingTaskbar.SetActive(false);

        ownerEnd.GetComponentsInChildren<Button>()[0].onClick.AddListener(delegate
        {
            if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
                NetworkingClient.s.emit("endSession", "");

            StartCoroutine(showEnd(true));
            Common.showEndLoader();

            //NetworkingClient.instance.Close();
            //Common.instance.load(2);
        });

        ownerEnd.GetComponentsInChildren<Button>()[1].onClick.AddListener(delegate
        {
            ownerEnd.SetActive(false);
            //NetworkingClient.instance.Close();
            //Common.instance.load(2);
        });

        //NetworkingClient.instance.Emit("endSession");
    }

    IEnumerator showEnd(bool isForced = false)
    {
        end.SetActive(true);
        votingTaskbar.SetActive(false);

        yield return new WaitForSeconds(10);

        if (isForced)
        {
            if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
                NetworkingClient.s.close();
            Common.instance.load(2);
        }

        end.GetComponentInChildren<Button>().onClick.AddListener(delegate
        {
            if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
                NetworkingClient.s.close();
            Common.instance.load(2);
        });
    }

    public GameObject commentPanel;

    public void showCommentPanel()
    {
        commentPanel.GetComponent<Animator>().Play("show");
    }

    private string commentInput;

    public void comment()
    {
        string username = PlayerPrefs.GetString("username", "none");
        Idea temp = submissions.ToList()[currentSubmission].Value.ideas[currentIdea];

        submissions.ToList()[currentSubmission].Value.comment(username, CommentInput, temp.id);

        commentPanel.GetComponent<Animator>().Play("hide");

        //string json = $"^'owner':'{ submissions.ToList()[currentSubmission].Value.username }','user':'{ username }','id':'{ temp }','comment':'{ CommentInput }','time':{ AccountManager.instance.timeSpoken }@".ToJSON();
        string json = $"^'idea':{JsonUtility.ToJson(temp) },'username':'{ username }','comment':'{ CommentInput }'@".ToJSON();
        //json = json.Replace("'", "\"");

        NetworkingClient.s.emit("comment", json);

        SessionManager.instance.RefreshIdeas();

    }

    public void like()
    {
        string username = PlayerPrefs.GetString("username", "none");
        Idea temp = submissions.ToList()[currentSubmission].Value.ideas[currentIdea];

        submissions.ToList()[currentSubmission].Value.like(username, temp.id);

        //string json = "{'owner':'" + submissions.ToList()[currentSubmission].Value.username + "','user':'" + username + "','id':'" + temp + "','time':" + AccountManager.instance.timeSpoken + "}";

        //json = json.Replace("'", "\"");

        string json = $"^'idea':{JsonUtility.ToJson(temp)},'username' : '{username}' @".ToJSON();

        NetworkingClient.s.emit("like", json);

        SessionManager.instance.RefreshIdeas();

        next();
    }

    public void dislike()
    {
        string username = PlayerPrefs.GetString("username", "none");
        Idea temp = submissions.ToList()[currentSubmission].Value.ideas[currentIdea];

        submissions.ToList()[currentSubmission].Value.dislike(username, temp.id);

        //string json = "{'owner':'" + submissions.ToList()[currentSubmission].Value.username + "','user':'" + username + "','id':'" + temp + "','time':" + AccountManager.instance.timeSpoken + "}";

        //json = json.Replace("'", "\"");

        string json = $"^'idea':{JsonUtility.ToJson(temp)},'username' : '{username}' @".ToJSON();

        NetworkingClient.s.emit("dislike", json);

        SessionManager.instance.RefreshIdeas();

        next();
    }

    //public void like(Submission s, string id)
    //{
    //    string username = PlayerPrefs.GetString("username", "none");
    //    string temp = id;

    //    s.like(username, temp);
    //    //submissions[s.username].like(username, temp);

    //    string json = "{'owner':'" + s.username + "','user':'" + username + "','id':'" + temp + "','time':" + AccountManager.instance.timeSpoken + "}";

    //    json = json.Replace("'", "\"");

    //    SessionManager.instance.RefreshIdeas();

    //    NetworkingClient.s.emit("like", new JSONObject(json));

    //}

    //public void dislike(Submission s, string id)
    //{
    //    string username = PlayerPrefs.GetString("username", "none");
    //    string temp = id;

    //    s.dislike(username, temp);

    //    string json = "{'owner':'" + s.username + "','user':'" + username + "','id':'" + temp + "','time':" + AccountManager.instance.timeSpoken + "}";

    //    json = json.Replace("'", "\"");

    //    SessionManager.instance.RefreshIdeas();

    //    NetworkingClient.s.emit("dislike", new JSONObject(json));
    //}

    public void comment(Submission s, string comment, Idea idea)
    {
        string username = PlayerPrefs.GetString("username", "none");
        //string temp = id;

        s.comment(username, comment, idea.id);

        string json = $"^'idea':{JsonUtility.ToJson(idea) },'username':'{ username }','comment':'{ comment }'@".ToJSON();

        //json = json.Replace("'", "\"");

        SessionManager.instance.RefreshIdeas();

        NetworkingClient.s.emit("comment", json);

    }

}