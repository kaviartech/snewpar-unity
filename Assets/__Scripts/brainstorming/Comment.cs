﻿[System.Serializable]
public class Comment
{
    public string user;
    public string comment;

    public Comment(string user, string comment)
    {
        this.user = user;
        this.comment = comment;
    }
}