﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Networking;

[System.Serializable]
public class brainstormingResponse
{
    public bool success, isOwner, isParticipant, dontExist,isPoll,isEnded;
    public string message;
    public string title, objectives, owner, date ,path ,startHour, endHour,_id , table,pin;
    public int mode,theme,time;
    public string[] participants;
    public string[] medias;
    public string[] categories;
}

public class PinInput : MonoBehaviour
{
    public string pincode;
    public GameObject submitGameObject;

    private string brainstormingPath = "api/brainstorming/get/pin";

    public static brainstormingResponse brainstorming;

    public static string message = "";


    public string Pincode { get => pincode; 
        set 
        {
            pincode = value;
            if (pincode.Length == 6)
                submitGameObject.SetActive(true);
            else
                submitGameObject.SetActive(false);

            PlayerPrefs.SetString("pincode", value);
        } }

    private void Awake()
    {
        Pincode = PlayerPrefs.GetString("pincode", "");
        GetComponent<TMP_InputField>().text = Pincode;
    }

    private void Start()
    {
        StartCoroutine(requestCam());

        if (message.Contains("notHere"))
        {
            Common.wait(LanguageManager.isFR ? "veuillez attendre que l'animateur démarre la session. nous essaierons de vous reconnecter dans 10 secondes" : "Please wait for the host to start the session. We will try to reconnect you in 10 seconds", "tryReconnect");
            message = "";
        }
        else if (message.Contains("disconnected"))
        {
            Common.alert(LanguageManager.isFR ? "l'animateur a fermé la réunion, vérifiez l'application web pour le résultat" : "the host closed the meeting, check the web application for the result");
            message = "";
        }
        else if (message.Contains("tooLate"))
        {
            Common.alert( LanguageManager.isFR ? "vous êtes entré dans la réunion trop tard, le sondage/vote a déjà commencé" : "you entered the meeting too late, the poll / vote has already started");
            message = "";
        }
    }

    IEnumerator requestCam()
    {

#if UNITY_IOS
        //bool accepted = false;
        //while (!accepted)
        //{
        //    yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        //    if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        //    {
        //        Debug.Log("webcam found");
        //        accepted = true;
        //    }
        //    else
        //    {
        //        Debug.Log("webcam not found");
        //        yield return null;
        //    }
        //}

#elif UNITY_ANDROID
        bool accepted = false;
        while (!accepted)
        {
            if (Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                // The user authorized use of the microphone.
                accepted = true;
            }
            else
            {
                // We do not have permission to use the microphone.
                // Ask for permission or proceed without the functionality enabled.
                Permission.RequestUserPermission(Permission.Camera);
            }
            yield return null;
        }
#endif
        yield return null;
    }

    public void submit()
    {
        StartCoroutine(checkPermissions());
    }

    [System.Serializable]
    struct checkPermissionsRequestData
    {
        public string pin;
        public string token;

        public checkPermissionsRequestData(string pin, string token)
        {
            this.pin = pin;
            this.token = token;
        }
    }

    IEnumerator checkPermissions()
    {
        string token = Common.getUserInfo();

        if (token == "")
        {
            yield break;
            //SessionManager.isEvent = false;
        }

        
        string jsonStr = JsonUtility.ToJson(new checkPermissionsRequestData(Pincode, token));

        print(jsonStr);

        print(Common.Baseurl + brainstormingPath);

        GameObject loading = Common.instance.startLoader("pin.checkPermission");

        this.fetchText(Common.Baseurl + brainstormingPath, jsonStr, 
            response => 
            {
                Debug.Log(response);
                brainstormingResponse res = JsonUtility.FromJson<brainstormingResponse>(response);

                print(res.table);
                print(res.owner);

                if (res.success)
                {
                    if (res.owner != "617ea675d03201002335b31f")
                    {
                        if (res.isEnded)
                        {
                            var context = res.isPoll ? LanguageManager.isFR ? "sondage" : "poll" : LanguageManager.isFR ? "réunion" : "meeting";

                            Destroy(loading);
                            Common.alert(LanguageManager.isFR ?
                                $"la { context } à laquelle vous essayez d'accéder est terminé, vérifiez le résultat sur le portail Web ou essayez une nouvelle { context }" :
                                $"the { context } you are trying to access has ended , check the result in the web portal or try a new { context }");
                            return;
                        }
                    }
                    brainstorming = res;
                    NetworkingClient.pincode = Pincode;
                    PlayerPrefs.SetInt("fromDeepLink", 0);
                    Destroy(loading);

                    //setTheme.instance.change();
                    //SessionManager.isEvent = false;
                    Debug.Log("loadd");
                    SessionManager.isEnded = false;
                    Common.instance.load(3);

                    
                    //print(res.token);

                    //Common.instance.load(1);
                }
                else
                {
                    //if (Pincode == "0000")
                    //{
                    //    SessionManager.isEvent = true;
                    //    SessionManager.isBilboard = false;
                    //    SessionManager.isBall = false;
                    //    //setTheme.instance.change();
                    //    brainstorming = new brainstormingResponse();
                    //    brainstorming.isOwner = false;
                    //    NetworkingClient.pincode = Pincode;
                    //    PlayerPrefs.SetInt("fromDeepLink", 0);
                    //    Common.instance.load(3);
                    //}else if (Pincode == "1001")
                    //{
                    //    SessionManager.isEvent = true;
                    //    SessionManager.isBilboard = true;
                    //    SessionManager.isBall = false;
                    //    //setTheme.instance.change();
                    //    brainstorming = new brainstormingResponse();
                    //    brainstorming.isOwner = false;
                    //    NetworkingClient.pincode = Pincode;
                    //    PlayerPrefs.SetInt("fromDeepLink", 0);
                    //    Common.instance.load(3);
                    //}
                    //else if (Pincode == "1111")
                    //{
                    //    SessionManager.isEvent = true;
                    //    SessionManager.isBilboard = false;
                    //    SessionManager.isBall = true;
                    //    //setTheme.instance.change();
                    //    brainstorming = new brainstormingResponse();
                    //    brainstorming.isOwner = false;
                    //    NetworkingClient.pincode = Pincode;
                    //    PlayerPrefs.SetInt("fromDeepLink", 0);
                    //    Common.instance.load(3);
                    //}
                    //else
                    if (res.dontExist)
                    {
                        Destroy(loading);
                        Common.alert(LanguageManager.isFR ? "Aucun accès avec ce PIN." : "No access with this PIN.");
                    }
                    else
                    {
                        Destroy(loading);
                        if (res.owner != "617ea675d03201002335b31f" && PlayerPrefs.GetInt("fromDeepLink", 0) == 0)
                            Common.alert(LanguageManager.isFR ? "Vous n'avez pas accès à cette salle, assurez-vous que l'animateur vous a ajouté à la rénuion." : "You don't have access to this room, make sure the moderator has added you to the meeting.");
                        Debug.Log(PlayerPrefs.GetInt("fromDeepLink"));
                        if(PlayerPrefs.GetInt("fromDeepLink", 0) == 1)
                            PlayerPrefs.SetInt("autoConnect", 1);
                    }
                }
            }, 
            error => 
            {
                print(error);
                Destroy(loading);
                Common.alert(LanguageManager.isFR ? "assurez-vous que vous êtes connecté à Internet ... ou nos serveurs sont peut-être en maintenance ... essayez plus tard" : "make sure you are connected to the internet ... or our servers may be under maintenance ... try later");
            });

    }

    private void Update()
    {

        if (PlayerPrefs.GetInt("autoConnect") == 1)
        {
            Pincode = PlayerPrefs.GetString("pincode", "");
            StartCoroutine(checkPermissions());
            PlayerPrefs.SetInt("autoConnect", 0);
        }
    }
}
