using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class DemoJson {
    public List<Demo> demo = new List<Demo>();
}

[Serializable]
public class Demo
{
    public int id;
    public string cat;
    public string link;
    public string img;
    public string title;

   
    
}
