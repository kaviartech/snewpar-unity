using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System.Web;

public class DemoImporter : MonoBehaviour
{
    public  GameObject webViewerContainer;
    public  TextMeshProUGUI buttonTxt;
    GameObject webViewer;
     bool isWV;
    public static string baseUrl = "https://snewpar.io/awe.php?api_ws_format=json";

    public static Dictionary<int, Texture2D> blurredThumbnailes = new Dictionary<int, Texture2D>();
    public static Dictionary<int, Texture2D> thumbnailes = new Dictionary<int, Texture2D>();
    public GameObject demoPanel;
    public Transform originalParent;

    [Tooltip("how many projects yet to download")]
    public int toLoad;

    [Tooltip("are we downloading now or not")]
    public bool isWaiting = false;


    public static  DemoJson pj,totalDj;
    // Start is called before the first frame update
    public IEnumerator Start()
    {
        isWV = false;
        totalDj = new DemoJson();

            yield return StartCoroutine(getPortalsJson());


        //totalPj = JsonUtility.FromJson<portalsJson>(json);

        pj = totalDj;

            /*foreach (Transform child in sceneHolder.instance.transform)
            {
                child.parent = transform;
            }*/

            transform.parent = originalParent;

            /*foreach (Transform child in sceneHolder.instance.transform)
            {
                DestroyImmediate(child.gameObject);
            }*/

            //print("starting");

            if (transform.childCount == 0)
            {
                StartCoroutine(spawnGallery());
            }


            yield return null;
        
    }



    public static IEnumerator getPortalsJson()
    {
        using (UnityWebRequest www = UnityWebRequest.Get(baseUrl))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(www.error);
            }
            else
            {
                //print(www.downloadHandler.text);
                string t = www.downloadHandler.text;
                Debug.Log(t);
               
                totalDj = JsonUtility.FromJson<DemoJson>(t);
                Debug.Log(totalDj.demo[0].cat);
            }
        }

       


    }
    public IEnumerator spawnGallery()
    {
        int countryCount = 0;
        int count = 0;
        
        foreach (Demo demo in totalDj.demo)
        {


            yield return StartCoroutine(download(demo.img, demo.id));

            yield return StartCoroutine(download(demo.img, demo.id, true));

            Texture2D t, b;
            thumbnailes.TryGetValue(demo.id, out t);
            blurredThumbnailes.TryGetValue(demo.id, out b);

            if (t && b)
                addButtonNC(demo.title, t, b, demo.id, count, demo.link);

            count++;
            //toLoad--;

            //yield return new WaitForSeconds(.1f);

            //loading.SetActive(toLoad >= 0);

            //yield return new WaitUntil(() => toLoad >= 0);



            countryCount++;
        }

    }

    /// <summary>
    /// manages the downloading of a thumbnail of a project and checks if it is already downloaded and loads it locally
    /// </summary>
    public static IEnumerator download(string url, int id, bool isBlurred = false)
    {
        //print(Application.persistentDataPath + "/uploads/thumbnail/" + id + ".jpg");

        if ((!thumbnailes.ContainsKey(id) && !isBlurred) || (!blurredThumbnailes.ContainsKey(id) && isBlurred))
        {

            using (var uwr = new UnityWebRequest(url, UnityWebRequest.kHttpVerbGET))
            {
                print("this is the url  :  " + url);
                uwr.downloadHandler = new DownloadHandlerTexture();
                uwr.SetRequestHeader("Access-Control-Allow-Credentials", "true");
                uwr.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
                uwr.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
                uwr.SetRequestHeader("Access-Control-Allow-Origin", "*");
                yield return uwr.SendWebRequest();
#if !UNITY_WEBGL
                while (!uwr.isDone)
                {
                    yield return uwr;
                }
#endif
                if (uwr.result == UnityWebRequest.Result.ProtocolError || uwr.result == UnityWebRequest.Result.ConnectionError)
                {
                    print(uwr.error);
                }

                Texture2D temp = DownloadHandlerTexture.GetContent(uwr);


                if (isBlurred)
                {

                    blurredThumbnailes.Add(id, temp);

                    byte[] bytes = temp.EncodeToJPG();


                }
                else
                {
                    thumbnailes.Add(id, temp);

                    byte[] bytes = temp.EncodeToJPG();

                }
            }
        }

    }



   
    /// <summary>
    /// manages the adding of a project's button to the gallery
    /// </summary>
    public void addButtonNC(string name, Texture2D texture, Texture2D blurred, int id, int order, string link)
    {
        //print("adding " + name);

        int itemsPerLine = 2;
        int dist = 510;

        if (order % itemsPerLine == 0)
        {

            GameObject tt = Instantiate(demoPanel, transform);
            tt.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -(order / itemsPerLine) * dist - 100);
            //print(-(order / itemsPerLine) * 209);
            GetComponent<RectTransform>().sizeDelta = new Vector2(792, ((order / itemsPerLine + 1) * dist) + 900);

        }

        GameObject temp = transform.GetChild(order / itemsPerLine).GetChild(order % itemsPerLine).gameObject;

        temp.name = id.ToString();

        temp.GetComponentInChildren<TextMeshProUGUI>().text = name;
        //temp.GetComponentsInChildren<TextMeshProUGUI>()[1].text = countryName;
        temp.GetComponentInChildren<RawImage>().texture = blurred;
        //temp.GetComponentsInChildren<RawImage>()[1].texture = texture;

        temp.GetComponentInChildren<RawImage>().GetComponent<RectTransform>().sizeDelta = fitOut360(texture);
        //temp.GetComponentsInChildren<RawImage>()[1].GetComponent<RectTransform>().sizeDelta = fitIn360(texture);

        temp.GetComponentInChildren<Button>().onClick.AddListener(delegate { getPin(link); });

        temp.SetActive(true);
    }




    /// <summary>
    /// fits the thumbnail in the button rectangle
    /// </summary>
    Vector2 fitIn360(Texture2D t)
    {
        Vector2 temp = new Vector2();

        if (t.width > t.height)
        {
            temp.x = 380;
            temp.y = (float)t.height / ((float)t.width / 380);
            //temp.y = 160;
        }
        else
        {

            temp.y = 480;
            temp.x = (float)t.width / ((float)t.height / 480);

            //temp.y = (float)t.height / ((float)t.width / 160);
            //temp.x = 160;
        }

        temp.y = (int)temp.y;

        temp.x = (int)temp.x;

        return temp;
    }

    /// <summary>
    /// covers the blurredThumbnail in the button rectangle
    /// </summary>
    Vector2 fitOut360(Texture2D t)
    {
        Vector2 temp = new Vector2();

        if (t.width < t.height)
        {
            temp.x = 360;
            temp.y = (float)t.height / ((float)t.width / 360);
            //temp.y = 160;
        }
        else
        {

            temp.y = 460;
            temp.x = (float)t.width / ((float)t.height / 460);

            //temp.y = (float)t.height / ((float)t.width / 160);
            //temp.x = 160;
        }

        temp.y = (int)temp.y;

        temp.x = (int)temp.x;

        return temp;
    }

    public void getPin(string s)
    {

        print(s);
        
        string access = s.Split('/').Last();
        string id;
        string pin;
        string hd;
        string isDemo = "0";
        foreach (string ss in HttpUtility.ParseQueryString(s).AllKeys)
            Debug.Log(ss);

        if (HttpUtility.ParseQueryString(s).Get("HD") != null)
        {
            hd = HttpUtility.ParseQueryString(s).Get("HD");
            Debug.Log(hd);
            GraphicManager.isHD = true;
        }
        if (HttpUtility.ParseQueryString(s).Get("is_demo") != null)
        {
            isDemo = HttpUtility.ParseQueryString(s).Get("is_demo");
        }

        string[] urlParams = access.Split('&');
        id = urlParams[0];
        pin = urlParams[1];
        Debug.Log(id);
        Debug.Log(pin);

        PlayerPrefs.SetString("pincode", pin);
        PlayerPrefs.SetInt("autoConnect", 1);
        PlayerPrefs.SetInt("fromDeepLink", 1);


    
        print(id);
        string json = "{'token':'" + Common.getUserInfo() + "','id':'" + id + "'}";

        json = json.Replace("'", "\"");

        this.fetchText(Common.Baseurl + "api/brainstorming/addAccess", json, (res) =>
        {
            print(res);
        });

        if (isDemo == "1")
        {
            InvitePostRequest invitePostRequest = new InvitePostRequest()
            {
                username = PlayerPrefs.GetString("username", ""),
                pin = PinInput.brainstorming.pin,
                id = PinInput.brainstorming._id,
                mail = "team@kaviar.app",
                token = PlayerPrefs.GetString("token", ""),
                isdemo = true
            };

            string jsonUti = JsonUtility.ToJson(invitePostRequest);
            print(jsonUti);
            this.fetchText(Common.Baseurl + "api/brainstorming/inviteOne", jsonUti, response =>
            {
                print(response);
            });
        }
        setUrl();
        //gi.deepLinkPortal(id);
    }

    public void setUrl()
    {
        isWV = !isWV;

        webViewerContainer.SetActive(isWV);
        if (isWV)
            buttonTxt.text = LanguageManager.isFR ? "Retour" : "Back";
        else
            buttonTxt.text = "Demo";
        //if (webViewer.transform.childCount < 0)
        //    webViewer.transform.GetChild(0).gameObject.SetActive(true);
    }

    /// <summary>
    /// updates everytime gallery gets scrolled and checks if we need to load more
    /// </summary>
    public void Ff()
    {
        if (GetComponent<RectTransform>().sizeDelta.y - GetComponent<RectTransform>().anchoredPosition.y < 2000)
        {
            if (!isWaiting)
            {
                toLoad = 4;
                isWaiting = true;
            }
        }
        else
        {
            isWaiting = false;
        }
    }

}
