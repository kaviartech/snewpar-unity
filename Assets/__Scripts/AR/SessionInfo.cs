﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SessionInfo : Singleton<SessionInfo>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public TextMeshProUGUI titleG, objectifG, timeG;
    public RawImage imageG;

    public GameObject sondage;
    
    
    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void Init(string title, string objectif, int time, Texture image, bool isSondage = false)
    {
        titleG.text = title;
        objectifG.text = objectif;
        //timeG.text = (LanguageManager.isFR ? "Temps : " : "Time : ") + time;

        sondage.SetActive(isSondage);

        if (image != null)
        {
            imageG.texture = image;
            imageG.GetComponent<AspectRatioFitter>().aspectRatio = (float)image.width / image.height;
        }
        else
            imageG.gameObject.SetActive(false);

        GetComponent<Animator>().Play("show");

    }

}
