﻿using BasicTools.ButtonInspector;
using JetBrains.Annotations;
using System;
using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;

public class Gestures : Singleton<Gestures>
{
    //My Variables
    private float degreeHor = 0;
    private float degreeVer = 0;
    private bool gesturesReady = false;
    public bool GesturesReady { get => gesturesReady; set => setGesturesReady(value); }

    private string gest;
    public string gestureType
    {
        get
        {
            return gest;
        }
        set
        {
            gest = value;

            controls.transform.GetChild(0).GetChild(6).gameObject.SetActive((value == "POST" || value == "TEXT"));
        }
    }

    public void setGesturesReady(bool value)
    {
        //print("trying to set gestures " + value.ToString());

        if (value)
        {
            //print( "show");
            Common.toggleControlPanel(true);
            Common.isControlsActive = value;

            if (PlacementSystem.instance.spawnedObject)
            {
                if (PlacementSystem.instance.spawnedObject.name.Contains("Meeting"))
                {
                    Common.instance.taskbar.Play("show");

                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanDragTranslate>().enabled = true;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanPinchScale>().enabled = true;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanTwistRotateAxis>().enabled = true;
                    PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                }
                else if(PlacementSystem.instance.spawnedObject.name.Contains("bilboard"))
                {
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanDragTranslate>().enabled = true;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanPinchScale>().enabled = true;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanTwistRotateAxis>().enabled = true;
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                }
                else if (PlacementSystem.instance.spawnedObject.name.Contains("ball"))
                {
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanDragTranslate>().enabled = true;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanPinchScale>().enabled = true;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanTwistRotateAxis>().enabled = true;
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                }
                else if (PlacementSystem.instance.spawnedObject.name.Contains("gate 3"))
                {
                    Common.instance.taskbar.Play("show");

                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
                    //PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanDragTranslate>().enabled = true;
                    //PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanPinchScale>().enabled = true;
                    //PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanTwistRotateAxis>().enabled = true;
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                }
                else
                {
                    Common.instance.taskbar.Play("show");
                    PlacementSystem.instance.spawnedObject.GetComponentInChildren<itemEvents>().Select();
                    PlacementSystem.instance.spawnedObject.GetComponentInChildren<LeanDragTranslate>().enabled = true;
                    PlacementSystem.instance.spawnedObject.GetComponentInChildren<LeanPinchScale>().enabled = true;
                    PlacementSystem.instance.spawnedObject.GetComponentInChildren<LeanTwistRotateAxis>().enabled = true;
                }
            }

        }
        else
        {
            //print("hide");
            //controls.Play("hide");
            Common.isControlsActive = value;
            if (PlacementSystem.instance.spawnedObject)
            {
                if (PlacementSystem.instance.spawnedObject.name.Contains("Meeting"))
                {
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
                    NetworkPoll.instance.voting.transform.parent.GetComponentsInChildren<Animator>()[0].Play("deselect");
                    if (!VotingBehaviour.voteEnded || PinInput.brainstorming.owner == "617ea675d03201002335b31f")
                        Common.toggleTaskbar(true);
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanDragTranslate>().enabled = false;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanPinchScale>().enabled = false;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanTwistRotateAxis>().enabled = false;
                    PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
                }
                else if (PlacementSystem.instance.spawnedObject.name.Contains("bilboard"))
                {
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
                    Common.toggleTaskbar(true);
                    NetworkPoll.instance.voting.transform.parent.GetComponentsInChildren<Animator>()[0].Play("deselect");
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanDragTranslate>().enabled = false;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanPinchScale>().enabled = false;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanTwistRotateAxis>().enabled = false;
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                }
                 else if (PlacementSystem.instance.spawnedObject.name.Contains("ball"))
                {
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
                    Common.toggleTaskbar(true);
                    NetworkPoll.instance.voting.transform.parent.GetComponentsInChildren<Animator>()[0].Play("deselect");
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanDragTranslate>().enabled = false;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanPinchScale>().enabled = false;
                    PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanTwistRotateAxis>().enabled = false;
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                }
                else if (PlacementSystem.instance.spawnedObject.name.Contains("gate 3"))
                {
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
                    Common.toggleTaskbar(true);
                    NetworkPoll.instance.voting.transform.parent.GetComponentsInChildren<Animator>()[0].Play("deselect");
                    //PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanDragTranslate>().enabled = false;
                    //PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanPinchScale>().enabled = false;
                    //PlacementSystem.instance.spawnedObject.transform.GetComponent<LeanTwistRotateAxis>().enabled = false;
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                }
                else
                {
                    PlacementSystem.instance.spawnedObject.GetComponentInChildren<itemEvents>().Deselect();
                    PlacementSystem.instance.spawnedObject.GetComponentInChildren<LeanDragTranslate>().enabled = false;
                    PlacementSystem.instance.spawnedObject.GetComponentInChildren<LeanPinchScale>().enabled = false;
                    PlacementSystem.instance.spawnedObject.GetComponentInChildren<LeanTwistRotateAxis>().enabled = false;
                }
            }
        }

        if (!value)
        {
            mt = moveType.None;

            //for (int i = 0; i < 5; i++)
            //{
            //    controls.transform.GetChild(0).GetChild(i).GetChild(0).gameObject.SetActive(false);
            //}
            Common.toggleControlPanel(false);

            refScale = Vector3.zero;
        }

        if (Common.IsFirstTime && !Common.isMainHelp)
        {
            //helpSystem.instance.CurrentType = 1;
            //Common.IsFirstTime = false;
            //helpSystem.instance.curretnNumber = -1;
            //helpSystem.instance.transform.GetChild(4).gameObject.SetActive(true);
            //helpSystem.instance.next();
        }

        gesturesReady = value;
    }

#if UNITY_EDITOR

    [Button("test", "activateGestures", true)]
    public bool button_1;

    public void activateGestures()
    {
        GesturesReady = true;
    }

#endif

    public Animator controls;

    [System.Serializable]
    public enum moveType { Plane, Up, Horizontal, Vertical, Scale, None };

    public moveType mt = moveType.None;

    Vector3 oldMousePos;

    public void change(int m)
    {
        moveType temp = mt;

        //if (refScale == Vector3.zero)
        //{
        //    refScale = PlacementSystem.instance.spawnedObject.transform.localScale;
        //}

        //mt = mt == (moveType)m ? moveType.None : (moveType)m;

        //if (mt == moveType.Scale)
        //{
        //    controls.Play("showSlider");
        //    refScale = Vector3.zero;
        //}
        //else if (temp == moveType.Scale)
        //{
        //    controls.Play("hideSlider");
        //    //gesturesReady = false;
        //    return;
        //}

        //if (mt == moveType.None)
        //{
        //    for (int i = 0; i < 5; i++)
        //    {
        //        controls.transform.GetChild(0).GetChild(i).GetChild(0).gameObject.SetActive(false);
        //    }
        //    //GesturesReady = false;
        //    refScale = Vector3.zero;
        //    return;
        //}
        //else
        //{
        //    for (int i = 0; i < 5; i++)
        //    {
        //        controls.transform.GetChild(0).GetChild(i).GetChild(0).gameObject.SetActive(false);
        //    }
        //    controls.transform.GetChild(0).GetChild((int)mt).GetChild(0).gameObject.SetActive(true);
        //}
        if (m == 3)
        {
            degreeVer += PlacementSystem.instance.spawnedObject.transform.rotation.x + 45;
            PlacementSystem.instance.spawnedObject.transform.rotation = Quaternion.AngleAxis(degreeVer, Vector3.right);
        }
        if(m == 2)
        {
            degreeHor += PlacementSystem.instance.spawnedObject.transform.rotation.z + 45;
            PlacementSystem.instance.spawnedObject.transform.rotation = Quaternion.AngleAxis(degreeHor, Vector3.forward);
        }

    }


    public void activateTextBox()
    {
        if (gest == "TEXT")
        {
            TextManager.instance.isEditing = true;
            TextManager.instance.toggleActive(true);
        }
        else if (gest == "POST")
        {
            PostManager.instance.isEditing = true;
            PostManager.instance.toggleActive(true);
        }


        GesturesReady = false;
    }

    public Vector3 refScale;

    public void scale(float scale)
    {
        if (mt == moveType.Scale)
        {
            if (refScale == Vector3.zero)
            {
                refScale = PlacementSystem.instance.spawnedObject.transform.localScale;
            }
            else
            {
                PlacementSystem.instance.spawnedObject.transform.localScale = refScale * scale;
            }
        }
    }

    public void reset()
    {
        PlacementSystem.instance.spawnedObject.transform.localPosition = new Vector3(0, .1f, 0);
        PlacementSystem.instance.spawnedObject.transform.localRotation = Quaternion.identity;

        if (refScale != Vector3.zero)
        {
            PlacementSystem.instance.spawnedObject.transform.localScale = refScale;
        }

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            oldMousePos = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            Vector2 delta = Input.mousePosition - oldMousePos;

            delta = delta.normalized;

            //OnSwipe?.Invoke(delta);

            switch (mt)
            {
                case moveType.Plane:

                    Transform trp = PlacementSystem.instance.spawnedObject.transform;

                    float diffrence = trp.localPosition.y;

                    trp.Translate(new Vector3(delta.x, 0, delta.y) * Time.deltaTime * .2f, Camera.main.transform);

                    diffrence -= trp.localPosition.y;

                    trp.localPosition = new Vector3(trp.localPosition.x, trp.localPosition.y + diffrence, trp.localPosition.z);


                    if (!PlacementSystem.instance.spawnedObject.name.Contains("Meeting"))
                    {
                        if (trp.localPosition.x > .5f || trp.localPosition.x < -.5f)
                        {
                            var val1 = Math.Abs(.5f - trp.localPosition.x);
                            var val2 = Math.Abs(-.5f - trp.localPosition.x);

                            trp.localPosition = new Vector3(
                                val1 < val2 ? .5f : -.5f,
                                trp.localPosition.y,
                                trp.localPosition.z);
                        }

                        if (trp.localPosition.z > .5f || trp.localPosition.z < -.5f)
                        {
                            var val1 = Math.Abs(.5f - trp.localPosition.z);
                            var val2 = Math.Abs(-.5f - trp.localPosition.z);

                            trp.localPosition = new Vector3(
                                trp.localPosition.x,
                                trp.localPosition.y,
                                val1 < val2 ? .5f : -.5f);
                        }
                    }

                    break;
                case moveType.Up:

                    Transform tru = PlacementSystem.instance.spawnedObject.transform;

                    tru.Translate(new Vector3(0, delta.y, 0) * Time.deltaTime * .5f, tru.parent);

                    if (!PlacementSystem.instance.spawnedObject.name.Contains("Meeting"))
                    {
                        if (tru.localPosition.y > .76f || tru.localPosition.y < .455f)
                        {
                            var val1 = Math.Abs(.76f - tru.localPosition.y);
                            var val2 = Math.Abs(.455f - tru.localPosition.y);

                            tru.localPosition = new Vector3(
                                tru.localPosition.x,
                                val1 < val2 ? .76f : .455f,
                                tru.localPosition.z);
                        }
                    }

                    break;
                case moveType.Horizontal:
                    degreeHor += 45;
                    PlacementSystem.instance.spawnedObject.transform.rotation = Quaternion.AngleAxis(degreeHor, Vector3.up);
                    //PlacementSystem.instance.spawnedObject.transform.Rotate(new Vector3(0, -delta.x, 0) * Time.deltaTime * 50);
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).Find("3DObject").Rotate(new Vector3(0, -delta.x, 0) * Time.deltaTime * 50, Space.World);
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).Find("Media").Rotate(new Vector3(0, -delta.x, 0) * Time.deltaTime * 50, Space.World);

                    break;
                case moveType.Vertical:
                    degreeVer += 45;
                    PlacementSystem.instance.spawnedObject.transform.rotation = Quaternion.AngleAxis(degreeVer, Vector3.down);
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).Find("3DObject").Rotate(PlacementSystem.instance.spawnedObject.transform.right * -delta.y * Time.deltaTime * 50, Space.World);
                    //PlacementSystem.instance.spawnedObject.transform.GetChild(0).Find("Media").Rotate(PlacementSystem.instance.spawnedObject.transform.right * -delta.y * Time.deltaTime * 50, Space.World);


                    break;
            }


            oldMousePos = Input.mousePosition;
        }

    }

}
