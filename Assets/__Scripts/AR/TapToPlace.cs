﻿using BasicTools.ButtonInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.Video;




[RequireComponent(typeof(ARRaycastManager))]
[RequireComponent(typeof(ARPlaneManager))]
public class TapToPlace : MonoBehaviour
{
    public GameObject gameObjectToInstantiate;
    public AudioClip clip;

    public GameObject spawnedObject;
    public GameObject spawnedObjectScene { get; set; }

    public GameObject help;

    private ARRaycastManager _arRaycastManager;
    private ARPlaneManager _arPlaneManager;

    private Vector2 touchPosition;
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
    ObjectLoader objectLoader = new ObjectLoader();
    public static TapToPlace instance;
    public static bool isPlaced  = false;

    private void Awake()
    {
        instance = this;
        //GetComponent<TapToPlace>().enabled = !SessionManager.isEvent || SessionManager.isBilboard ||  SessionManager.isBall;
        //GetComponent<PlaceMultipleObjectsOnPlane>().enabled = SessionManager.isEvent && !SessionManager.isBilboard && !SessionManager.isBall;
        //GetComponent<EventTexture>().enabled = SessionManager.isEvent && !SessionManager.isBilboard && !SessionManager.isBall;
        //GetComponentInChildren<RaycastExp>().enabled = SessionManager.isEvent && !SessionManager.isBilboard && !SessionManager.isBall;
        //GetComponentInChildren<FieldOfView>().enabled = SessionManager.isEvent && !SessionManager.isBilboard && !SessionManager.isBall;
        //history = GameObject.FindGameObjectWithTag("history");
        //ideaPool = GameObject.FindGameObjectWithTag("ideapool");
        _arRaycastManager = GetComponent<ARRaycastManager>();
        _arPlaneManager = GetComponent<ARPlaneManager>();
        _arPlaneManager.enabled = true; 
        //if (SessionManager.isBilboard)
        //    gameObjectToInstantiate = Resources.Load("bilboard", typeof(GameObject)) as GameObject;
        //else  if (SessionManager.isBall)
        //    gameObjectToInstantiate = Resources.Load("ball", typeof(GameObject)) as GameObject;
        //else if (SessionManager.isEvent)
        //    gameObjectToInstantiate = Resources.Load("gate 3", typeof(GameObject)) as GameObject;

    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.touchCount == 1)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;
    }

    private void Start()
    {
        //togglePlaneDetection();
    }

    public void togglePlaneDetection()
    {
        _arPlaneManager.enabled = !_arPlaneManager.enabled;

        setAllPlanesActive(!_arPlaneManager.enabled);

    }

    private void setAllPlanesActive(bool value)
    {
        foreach (var plane in _arPlaneManager.trackables)
        {
            plane.gameObject.SetActive(false);
        }
    }

    [Button("place table", "editorTest", true)]
    public bool button_1;

    public void editorTest()
    {
        spawnedObject = spawnedObject == gameObjectToInstantiate ? spawnedObject : Instantiate(gameObjectToInstantiate, Vector3.zero  + new Vector3(0.0009614705f, -0.6862746f, 0.9570984f), gameObjectToInstantiate.transform.rotation);

        spawnedObject.transform.position = Vector3.zero + new Vector3(0.0009614705f, -0.6862746f, 0.9570984f);

        //Debug.LogWarning(PinInput.brainstorming.table);

            //this.fetchImage(PinInput.brainstorming.table, (texture) =>
            //{
            //    Debug.LogWarning("textureputSuccess");
            //    //spawnedObject.transform.GetChild(0).GetComponentsInChildren<Renderer>().Last().material.mainTexture = texture;
            //    //spawnedObject.transform.GetChild(0).GetComponentsInChildren<Renderer>().Last().material.color = Color.white;

            //});
        
        spawnedObject.SetActive(true);
        //activateTaskbar();

        PlacementSystem.instance.spawnedObject = spawnedObject;

        PlacementSystem.instance.parent = spawnedObject;
        //if (SessionManager.isBilboard || SessionManager.isBall)
        //    Gestures.instance.GesturesReady = true;


            history.SetActive(true);
        ideaPool.SetActive(true);
        //if (SessionManager.isBilboard || SessionManager.isBall)
        //{
        //    FindObjectOfType<PostManager>().GetComponent<Button>().enabled = false;
        //}
        networkPosition x = networkPosition.instance;

        x.enabled = true;


        enabled = false;
    }

    public GameObject history,ideaPool;

    

    public static bool IsPointerOverUIObject(Vector2 position)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = position;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        Debug.Log(results.Count);
        return results.Count > 1;
    }

    RaycastHit HitInfo;
    public GameObject placeHolder;

    private void Update()
    {

        //if (!CloudControl.instance.DoneLoading) return;
#if !UNITY_WEBGL
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out HitInfo))
        {
            if (HitInfo.transform.name.ToLower().Contains("arplane"))
            {
                if (placeHolder.activeInHierarchy == false)
                    placeHolder.SetActive(true);
                placeHolder.transform.position = HitInfo.point;
            }
        }
        
        if (_arPlaneManager.trackables.count > 0 && !SessionManager.once)
        {
            helpSystem.moveDevice(true, false);
            helpSystem.tapToPlace(true,true);
            SessionManager.once = true;
        }
        if (Gestures.instance.GesturesReady) return;

        //if (!TryGetTouchPosition(out touchPosition)) { Debug.Log("try   touch"); return; }

//if (IsPointerOverUIObject(touchPosition)) return;
//Debug.Log("gesture r eady : "+ Gestures.instance.GesturesReady);

#endif
    }


    public  void placeObjectWebgl()
    {
#if UNITY_WEBGL
 spawnedObject = spawnedObject == gameObjectToInstantiate ? spawnedObject : Instantiate(gameObjectToInstantiate, Vector3.zero  + new Vector3(0.0009614705f, -0.6862746f, 0.9570984f), gameObjectToInstantiate.transform.rotation);

        spawnedObject.transform.position = Vector3.zero + new Vector3(0.0009614705f, -0.6862746f, 0.9570984f);

        //Debug.LogWarning(PinInput.brainstorming.table);

            //this.fetchImage(PinInput.brainstorming.table, (texture) =>
            //{
            //    Debug.LogWarning("textureputSuccess");
            //    //spawnedObject.transform.GetChild(0).GetComponentsInChildren<Renderer>().Last().material.mainTexture = texture;
            //    //spawnedObject.transform.GetChild(0).GetComponentsInChildren<Renderer>().Last().material.color = Color.white;

            //});
        
        spawnedObject.SetActive(true);
        //activateTaskbar();

        PlacementSystem.instance.spawnedObject = spawnedObject;

        PlacementSystem.instance.parent = spawnedObject;
        //if (SessionManager.isBilboard || SessionManager.isBall)
        //    Gestures.instance.GesturesReady = true;


            history.SetActive(true);
        ideaPool.SetActive(true);
        //if (SessionManager.isBilboard || SessionManager.isBall)
        //{
        //    FindObjectOfType<PostManager>().GetComponent<Button>().enabled = false;
        //}
        networkPosition x = networkPosition.instance;

        x.enabled = true;


        enabled = false;
#endif
    }

    public void placeObject()
    {
        
            if (spawnedObject == null || spawnedObject == gameObjectToInstantiate)
            {

                spawnedObject = spawnedObject == gameObjectToInstantiate ? spawnedObject : Instantiate(gameObjectToInstantiate, placeHolder.transform.position, placeHolder.transform.rotation);

                spawnedObject.transform.localPosition = placeHolder.transform.position;
                //spawnedObject.transform.localScale = placeHolder.transform.localScale;

                //spawnedObject.transform.localRotation = hitPose.rotation;

                Debug.LogWarning(PinInput.brainstorming.table);

                //this.fetchImage(PinInput.brainstorming.table, (texture) =>
                //{
                //    Debug.LogWarning("textureputSuccess");
                //    spawnedObject.transform.GetChild(0).GetComponentsInChildren<Renderer>().Last().material.mainTexture = texture;
                //    spawnedObject.transform.GetChild(0).GetComponentsInChildren<Renderer>().Last().material.color = Color.white;

                //});
                    int i = 0;
                
            spawnedObject.SetActive(true);

                PlacementSystem.instance.spawnedObject = spawnedObject;

                PlacementSystem.instance.parent = spawnedObject;
                //Common.instance.parentAnchor = spawnedObject;

                //help.SetActive(true);

                //togglePlaneDetection();
                ARPlaneManager ap = GetComponent<ARPlaneManager>();
                placeHolder.SetActive(false);
                ap.enabled = false;
                foreach (var plane in ap.trackables)
                    plane.gameObject.SetActive(false);

                //if (!SessionManager.instance.brainstorming.isOwner && SessionManager.instance.brainstorming.mode != 1)
                //{
                //    Common.alert(LanguageManager.isFR ? "dans ce brainstorming, l'animateur n'a pas permis à d'autres utilisateurs de placer des objets" : "in this brainstorming, the host did not allow other users to place objects");
                //}

                history.SetActive(true);
                ideaPool.SetActive(true);
                //if (SessionManager.isBilboard || SessionManager.isBall  || SessionManager.isEvent)
                //{
                //    FindObjectOfType<PostManager>().GetComponent<Button>().enabled = false;
                //}
                networkPosition x = networkPosition.instance;

                x.enabled = true;
            //if (SessionManager.isBilboard || SessionManager.isBall || SessionManager.isEvent)
            //    Gestures.instance.GesturesReady = true;

            if (helpSystem.restarted)
                    Gestures.instance.GesturesReady = false;
                
                //helpSystem.tapToPlace(true, false);
                enabled = false;

            }
    }
}
