﻿/*

*/

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimerBehaviour : Singleton<TimerBehaviour>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    Action x;
    public bool canCount = false;
    float startTime = 10;
    public float timer = 10;

    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        if (PinInput.brainstorming == null) return;

        GetComponent<Button>().enabled = PinInput.brainstorming.isOwner;

        NetworkingClient.s.on("startTimer", E =>
        {

            //print(E.data["time"].ToString());
            print(E);
            var data = new JSONObject(E.ToJSON());
            float timeImport = float.Parse(data["time"].ToString());


            timer = timeImport;

            canCount = true;

            GetComponent<Animator>().Play("start");

        });

        NetworkingClient.s.on("stopTimer", E =>
        {

            //print(E.data["time"].ToString());
            print(E);
            var data = new JSONObject(E.ToJSON());
            float timeImport = float.Parse(data["time"].ToString());

            timer = timeImport;

            canCount = false;

            GetComponent<Animator>().Play("stop");

        });

        if (PinInput.brainstorming.isOwner)
            InvokeRepeating("synchroniseEvery10Seconds", 5, 5);

        InvokeRepeating("CustomUpdate", 1, 1);
    }

    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/

    void synchroniseEvery10Seconds()
    {
        string initJson = $"^'time': {instance.timer}@";

        //print(initJson);

        string roomJson = initJson.ToJSON();

        NetworkingClient.s.emit("synchroniseTime", roomJson);

        string username = PlayerPrefs.GetString("username", "none");

        string json = $"^'time': {AccountManager.instance.timeSpoken} @".ToJSON();

        print(json);

        NetworkingClient.s.emit("timeSpoken", json);
    }

    void CustomUpdate()
    {
        if (canCount)
        {

            timer -= 1;
            transform.GetComponentInChildren<TextMeshProUGUI>().text = ((int)timer / 60).ToString("00") + ":" + ((int)timer % 60).ToString("00");

            if (timer <= 0)
            {
                x?.Invoke();
                canCount = false;
            }
        }
    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void initTimer(float time, Action p)
    {
        //float time = brainstorming.time == 0 ? 10 : brainstorming.time;

        x = p;

        time *= 60;

        startTime = time;
        timer = startTime;
        canCount = true;

    }

    public void stopTimer()
    {
        GetComponent<Animator>().Play("stop");
        canCount = false;

        string json = $"^'time': {timer} @".ToJSON();
        //print(json);
        if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
            NetworkingClient.s.emit("stopTimer", json);

    }

    public void startTimer()
    {
        GetComponent<Animator>().Play("start");

        string json = $"^'time': {timer + 2} @".ToJSON();

        NetworkingClient.s.emit("startTimer", json);

        canCount = true;
    }

    public void toggleTimer()
    {
        //stopTimer();
        AddTimeBehaviour.instance.GetComponent<Animator>().Play("show");
        AddTimeBehaviour.instance.Time = timer;


    }

}
