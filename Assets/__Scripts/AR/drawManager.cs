﻿using FreeDraw;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class drawManager : Singleton<drawManager>
{
    private void Start()
    {
        //this.transform.SetParent(GameObject.Find("MeetingArea").transform);
    }

    public void setColor(Button b)
    {
        Color c = b.image.color;

        MyDrawable.Pen_Colour = c;
    }

    public void setSize(int size)
    {
        MyDrawable.Pen_Width = size;
    }

    public void SetEraser()
    {
        MyDrawable.Pen_Colour = new Color(255f, 255f, 255f, 0f);
    }


    public void ResetCanvas()
    {
        if (MyDrawable.drawable)
        {
            Color[] cl = MyDrawable.drawable.clean_colours_array;
            RawImage sr = MyDrawable.drawable.GetComponent<RawImage>();
            ((Texture2D)sr.texture).SetPixels(cl);
            ((Texture2D)sr.texture).Apply();
            MyDrawable.drawable.transform.parent.gameObject.SetActive(false);
        }

        //Gestures.instance.GesturesReady = true;

    }

    public void save()
    {
        RawImage sr = MyDrawable.drawable.GetComponent<RawImage>();
        Texture2D dst = new Texture2D(((Texture2D)sr.texture).width, ((Texture2D)sr.texture).height, ((Texture2D)sr.texture).format, false);
        Graphics.CopyTexture(((Texture2D)sr.texture), dst);

        byte[] bt = dst.EncodeToPNG();

        string path = Application.persistentDataPath + $"/{DateTime.Now.Ticks}.png";

        //print(path);

        File.WriteAllBytes(path, bt);

        useImageToTplace(path, dst);

        ResetCanvas();
        //Gestures.instance.GesturesReady = false;
    }

    public GameObject gameObjectToInstantiate;

    private void useImageToTplace(string path, Texture2D dst)
    {
        PlacementSystem.instance.current.texture = dst;
        PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta = new Vector2(1, (int)(dst.height / dst.width)) * PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta.x;

        GameObject temp = Instantiate(gameObjectToInstantiate);

        temp.GetComponent<Renderer>().material.mainTexture = dst;
        temp.transform.localScale = new Vector3(-.1f, .005f, -.1f * (int)(dst.height / dst.width));

        temp.transform.GetChild(0).gameObject.SetActive(false);


        temp.SetActive(false);

        PlacementSystem.instance.currentType = "DRAW";

        PlacementSystem.instance.content = path;


        PlacementSystem.instance.gameObjectToInstantiate = temp;

    }
}
