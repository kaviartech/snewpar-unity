using TriLibCore;
using TriLibCore.General;
using TriLibCore.Utils;
using UnityEngine;
using System.IO;
using TMPro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ObjectLoader : MonoBehaviour
{
    // Lets the user load a new model by clicking a GUI button.
    private string extension;
    GameObject temp;
    Animation _animation;
    bool hasAnimation = false;
    public static bool isLoaded = false;
    public static GameObject cube;
    public static List<Transform> objectComponents = new List<Transform>();
    List<Texture2D> _textures;
    AssetLoaderOptions assetLoaderOptions;
  
    private void Start()
    {
        _textures = new List<Texture2D>();
        Application.lowMemory += OnLowRam2;
        cube = GameObject.Find("Cube");
        extension = Path.GetExtension(PinInput.brainstorming.table).Replace(".", "");
        //if (extension == "fbx" || extension == "obj" || extension == "glb" || extension == "gltf")
        LoadObject();
        //if (extension == "fbx")
        //    this.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }
    public void LoadObject()
    {
        helpSystem.moveDevice(true, false);
        // Displays a button to begin the model loading process.
        temp = Instantiate(Resources.Load("loading 1", typeof(GameObject))) as GameObject;

        // Creates an AssetLoaderOptions instance.
        // AssetLoaderOptions is a class used to configure many aspects of the loading process.
        // We won't change the default settings this time, so we can use the instance as it is.
        assetLoaderOptions = AssetLoader.CreateDefaultLoaderOptions();
        assetLoaderOptions.ForcePowerOfTwoTextures = false;
        
        // Creates the web-request.
        // The web-request contains information on how to download the model.
        // Let's download a model from the TriLib website.
       var webRequest = AssetDownloader.CreateWebRequest(PinInput.brainstorming.table);

        // Important: If you're downloading models from files that are not Zipped, you must pass the model extension as the last parameter from this call (Eg: "fbx")
        // Begins the model downloading.
        if(extension  != "zip")
            AssetDownloader.LoadModelFromUri(webRequest, OnLoad, OnMaterialsLoad, OnProgress, OnError, gameObject, assetLoaderOptions, null, extension);
        else
            AssetDownloader.LoadModelFromUri(webRequest, OnLoad, OnMaterialsLoad, OnProgress, OnError, gameObject, assetLoaderOptions, null, null, true);
    }

    // This event is called when the model loading progress changes.
    // You can use this event to update a loading progress-bar, for instance.
    // The "progress" value comes as a normalized float (goes from 0 to 1).
    // Platforms like UWP and WebGL don't call this method at this moment, since they don't use threads.
    private void OnProgress(AssetLoaderContext assetLoaderContext, float progress)
    {
        
        TextMeshProUGUI[] Stexts = temp.GetComponentsInChildren<TextMeshProUGUI>();
        Debug.Log(Stexts[1].name);
        Stexts[1].text = ((progress / 1f)*100f).ToString("00")+"%";

        temp.GetComponentInChildren<Slider>().value = (progress / (float)1);

        temp.GetComponentInChildren<TextMeshProUGUI>().text = LanguageManager.isFR ? $"Chargement des contenus....." : $"Loading meeting contents.....";
    }

    // This event is called when there is any critical error loading your model.
    // You can use this to show a message to the user.
    private void OnError(IContextualizedError contextualizedError)
    {
        Destroy(temp);
        Debug.Log(contextualizedError.GetContext());
        Common.error(LanguageManager.isFR ? "Une erreur est survenu lors du chargement de l'objet. Verifiez le format de l'objet et créez une nouvelle réunion." : "An error has occured while loading the object. Verify the object file format and create a new meeting. ");
    }
    
    private void OnLowRam2()
    {
        StopAllCoroutines();
        if(temp != null)
            Destroy(temp);
        Common.error(LanguageManager.isFR ? "Votre appareil manque de memoire, la qualité graphique a été automatiquement passer en SD.\n Vous poouvez changer en la qualité en HD depuis le menu principal." : "Your device is running out of memory, the graphic quality has been automatically changed to SD.\n You can change to HD quality from the main menu.");
        GraphicManager.isHD = false;
    }

    // This event is called when all model GameObjects and Meshes have been loaded.
    // There may still Materials and Textures processing at this stage.
    private void OnLoad(AssetLoaderContext assetLoaderContext)
    {
        // The root loaded GameObject is assigned to the "assetLoaderContext.RootGameObject" field.
        // If you want to make sure the GameObject will be visible only when all Materials and Textures have been loaded, you can disable it at this step.
        var myLoadedGameObject = assetLoaderContext.RootGameObject;
        if (myLoadedGameObject != null)
        {
            //myLoadedGameObject.SetActive(true);
            //PlaybackAnimation.options.Clear();
            if (assetLoaderContext.Options.AnimationType == AnimationType.Legacy)
            {
                _animation = assetLoaderContext.RootGameObject.GetComponent<Animation>();
                if (_animation != null)
                    hasAnimation = true;

            }
            //PlaybackAnimation.value = 0;
        }

       

    }


    // This event is called after OnLoad when all Materials and Textures have been loaded.
    // This event is also called after a critical loading error, so you can clean up any resource you want to.
    private void OnMaterialsLoad(AssetLoaderContext assetLoaderContext)
    {
        
        // The root loaded GameObject is assigned to the "assetLoaderContext.RootGameObject" field.
        // You can make the GameObject visible again at this step if you prefer to.
        var myLoadedGameObject = assetLoaderContext.RootGameObject;
        Destroy(temp);
        //myLoadedGameObject.SetActive(true);
        Vector3 tempSize = Vector3.zero;
        //Mesh[] meshes = (Mesh[])FindObjectsOfType(typeof(Mesh));
        Vector3 tempPos = Vector3.zero;
        Vector3 tempboundSize = Vector3.zero;
        //foreach(GameObject go in gameObjects)
        //{
        //    Debug.Log("game " + go.name);
        //}

        //foreach (Mesh mesh in meshes)
        //{
        foreach (Transform g in gameObject.transform.GetComponentsInChildren<Transform>())
        {

            if (g.GetComponent<MeshRenderer>() || g.GetComponent<SkinnedMeshRenderer>())
            {
                objectComponents.Add(g);
                g.gameObject.AddComponent<BoxCollider>();
                if (tempboundSize.x < g.gameObject.GetComponent<BoxCollider>().size.x && tempboundSize.y < g.gameObject.GetComponent<BoxCollider>().size.y && tempboundSize.z < g.gameObject.GetComponent<BoxCollider>().size.z)
                {
                    Debug.Log("temp ame " + g.name);
                    Debug.Log("temp size " + tempboundSize);
                    tempSize = g.gameObject.GetComponent<BoxCollider>().bounds.size;
                    tempboundSize = g.gameObject.GetComponent<BoxCollider>().size;
                    tempPos = g.gameObject.transform.position;
                    name = g.name;
                    Debug.Log("new ame " + g.name);
                    Debug.Log("go  size " + g.gameObject.GetComponent<BoxCollider>().size);
                }
            }
        }
        cube.transform.localScale = tempSize;
        cube.transform.position = tempPos;

        if (Mathf.Floor(Mathf.Log10(tempSize.x)) == 3 || Mathf.Floor(Mathf.Log10(tempSize.y)) == 3 || +Mathf.Floor(Mathf.Log10(tempSize.z)) == 3)
            GameObject.Find("Meeting(Clone)").transform.localScale = new Vector3(0.0001f, 0.0001f, 0.0001f);
        if (Mathf.Floor(Mathf.Log10(tempSize.x)) == 2 || Mathf.Floor(Mathf.Log10(tempSize.y)) == 2 || +Mathf.Floor(Mathf.Log10(tempSize.z)) == 2)
            GameObject.Find("Meeting(Clone)").transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        if (Mathf.Floor(Mathf.Log10(tempSize.x)) == 1 || Mathf.Floor(Mathf.Log10(tempSize.y)) == 1 || +Mathf.Floor(Mathf.Log10(tempSize.z)) == 1)
            GameObject.Find("Meeting(Clone)").transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

        //GameObject.Find("avatars").transform.localScale = new Vector3(tempSize.z, tempSize.z, tempSize.z);

        //}
        isLoaded = true;
        Gestures.instance.GesturesReady = true;
        //myLoadedGameObject.SetActive(true);
        if (hasAnimation)
            _animation.Play();
        Debug.Log(assetLoaderContext.LoadedTextures.Count);

    }
    private void Update()
    {
        if(SessionManager.isEnded)
        {
            Destroy(gameObject);
        }
    }
}