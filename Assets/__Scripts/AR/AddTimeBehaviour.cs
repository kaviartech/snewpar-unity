﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddTimeBehaviour : Singleton<AddTimeBehaviour>
{
    float time;
    public GameObject play, pause;
    public float Time {
        get
        {
            return time;
        }

        set
        {
            GetComponentsInChildren<TextMeshProUGUI>()[1].text = ((int)value / 60).ToString("00") + ":" + ((int)value % 60).ToString("00");
            time = value;
        }
    }

    public void minus()
    {
        Time -= 60;
    }

    public void plus()
    {
        Time += 60;
    }

    public void submit()
    {
        TimerBehaviour.instance.timer = Time;
        GetComponent<Animator>().Play("hide");
        //TimerBehaviour.instance.startTimer();
    }

    public void cancel()
    {
        //TimerBehaviour.instance.timer = Time;
        GetComponent<Animator>().Play("hide");
        //TimerBehaviour.instance.startTimer();
    }

    public void stop()
    {
        //TimerBehaviour.instance.timer = Time;
        GetComponent<Animator>().Play("hide");
        play.SetActive(!play.activeSelf);
        pause.SetActive(!pause.activeSelf);
        if(play.activeSelf)
            TimerBehaviour.instance.stopTimer();
        else
            TimerBehaviour.instance.startTimer();

    }
}
