﻿/*

*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public struct historyItem
{
    public GameObject g;
    public Texture t;
    public string type;

    public historyItem(GameObject g, Texture t, string type)
    {
        this.g = g;
        this.t = t;
        this.type = type;
    }
}

public class HistoryManager : Singleton<HistoryManager>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public Dictionary<string, historyItem> history = new Dictionary<string, historyItem>();


    public GameObject historyPanel;
    public GameObject historyCounter;
    public GameObject historyPrefab;

    public bool isActive;

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/
   
    public void closeOthers()
    {
        Transform parent = instance.historyPanel.transform.GetChild(0).GetChild(1).GetChild(0);

        foreach (Transform item in parent)
        {
            item.GetComponent<Animator>().Play("close");
        }
    }


    GameObject FindInActiveObjectByName(string name)
    {
        Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].hideFlags == HideFlags.None)
            {
                if (objs[i].name == name)
                {
                    return objs[i].gameObject;
                }
            }
        }
        return null;
    }

    public static void Add(string id, GameObject spawnedObject,Texture current , string type)
    {
        Transform parent = instance.historyPanel.transform.GetChild(0).GetChild(1).GetChild(0);

        GameObject temp = Instantiate(instance.historyPrefab, parent);

        temp.GetComponentInChildren<RawImage>().texture = current;

        temp.GetComponent<Button>().onClick.AddListener(delegate
        {
            instance.animate(id,temp,parent);
            
        });

        instance.history.Add(id, new historyItem(spawnedObject,current, type));

        Debug.Log("history count " + instance.history.Count);
        if (instance.history.Count != 0)
        {
            if (!instance.historyCounter.transform.GetChild(3).gameObject.activeSelf)
                instance.historyCounter.transform.GetChild(3).gameObject.SetActive(true);
            var image = instance.historyCounter.GetComponent<Image>();
            var tempColor = image.color;
            tempColor.a = 1f;
            image.color = tempColor;
            instance.historyCounter.transform.GetChild(3).gameObject.transform.GetComponentInChildren<TextMeshProUGUI>(true).text = instance.history.Count.ToString();
        }
        
    }

    private void animate(string id, GameObject temp,Transform parent)
    {
        foreach (Transform item in parent)
        {
            item.GetComponent<Animator>().Play("close");
        }

        temp.GetComponent<Animator>().Play("open");


        temp.transform.GetChild(1).GetChild(0).GetComponent<Button>().onClick.AddListener(delegate
        {

            foreach (Transform item in parent)
            {
                item.GetComponent<Animator>().Play("close");
            }

            toogleHistory();

            instance.chooseItem(id);

            


        });

        temp.transform.GetChild(1).GetChild(2).GetComponent<Button>().onClick.AddListener(delegate
        {

            foreach (Transform item in parent)
            {
                item.GetComponent<Animator>().Play("close");
            }

            toogleHistory();

            instance.react(id);

        });

        temp.transform.GetChild(1).GetChild(1).GetComponent<Button>().onClick.AddListener(delegate
        {

            foreach (Transform item in parent)
            {
                item.GetComponent<Animator>().Play("close");
            }

            instance.deleteItem(id);

            Destroy(temp);

        });
    }

    public void chooseItem(string id)
    {
        history.TryGetValue(id, out historyItem hi);

        //hi.g.GetComponentInChildren<itemEvents>().Select();

        PlacementSystem.instance.spawnedObject = hi.g;
        PlacementSystem.instance.current.texture = hi.t;
        Gestures.instance.GesturesReady = true;
        Gestures.instance.gestureType = hi.type;
    }

    private void react(string id)
    {
        history.TryGetValue(id, out historyItem hi);

        //hi.g.GetComponentInChildren<itemEvents>().Select();

       hi.g.GetComponentInChildren<itemEvents>().react();
    }

    public void deleteItem(string id)
    {
        history.TryGetValue(id, out historyItem hi);

        //Destroy(hi.g);

        //NetworkObject temp = new NetworkObject
        //{
        //    id = id
        //};

        id = "idea_" + id;

        IdeaUiBehaviour idUB = FindInActiveObjectByName(id).GetComponent<IdeaUiBehaviour>();


        NetworkingClient.s.emit("removeItem", JsonUtility.ToJson(idUB.Idea));


        PlacementSystem.instance.spawnedObject = null;
        PlacementSystem.instance.current.texture = null;
        Gestures.instance.GesturesReady = false;

        history.Remove(id);

        if (instance.history.Count == 0)
        {
            if (instance.historyCounter.transform.GetChild(3).gameObject.activeSelf)
                instance.historyCounter.transform.GetChild(3).gameObject.SetActive(false);
            var image = instance.historyCounter.GetComponent<Image>();
            var tempColor = image.color;
            tempColor.a = 0.5f;
            image.color = tempColor;
            instance.historyCounter.transform.GetChild(3).gameObject.transform.GetComponentInChildren<TextMeshProUGUI>(true).text = instance.history.Count.ToString();
        }
    }

    public void toogleHistory()
    {
        Common.deactivateAll();
        Gestures.instance.GesturesReady = false; //isActive;
        historyPanel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
        isActive = !isActive;
    }

    public void deactivate()
    {
        Gestures.instance.GesturesReady = false; //isActive;
        historyPanel.GetComponent<Animator>().Play("hide");
        isActive = false;
    }
}
