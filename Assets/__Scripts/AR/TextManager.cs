﻿using TMPro;
using UnityEngine;

public class TextManager : Singleton<TextManager>
{
    public GameObject panel;

    public bool isActive;
    private void Start()
    {
        //this.transform.SetParent(GameObject.Find("MeetingArea").transform);
    }

    public void toggleActive(bool resetIsEditing = false)
    {
        Common.deactivateAll();
        Gestures.instance.GesturesReady = false; //isActive;
        panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");

        if (!resetIsEditing)
        {
            isEditing = false;
        }

        isActive = !isActive;
    }

    public void deactivate()
    {
        Gestures.instance.GesturesReady = false; //isActive;
        panel.GetComponent<Animator>().Play("hide");
        isActive = false;
    }

    public GameObject gameObjectToInstantiate;
    public Texture2D textImage;

    public string S { get; set; }

    public bool isEditing = false;

    public void select()
    {
        if (!isEditing)
        {
            PlacementSystem.instance.current.texture = textImage;
            PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta = new Vector2(1, (int)(textImage.height / textImage.width)) * PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta.x;

            GameObject temp = Instantiate(gameObjectToInstantiate);

            temp.GetComponentInChildren<TextMeshProUGUI>(true).text = S;

            temp.transform.GetChild(1).gameObject.SetActive(false);


            temp.SetActive(false);

            PlacementSystem.instance.gameObjectToInstantiate = temp;


            PlacementSystem.instance.currentType = "Text";
            PlacementSystem.instance.content = S;

            panel.GetComponentInChildren<TMP_InputField>().text = "";

            panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
            isActive = !isActive;
        }
        else
        {

            PlacementSystem.instance.spawnedObject.GetComponentInChildren<TextMeshProUGUI>(true).text = S;

            panel.GetComponentInChildren<TMP_InputField>().text = "";

            panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
            isActive = !isActive;

            isEditing = false;
        }
        
    }
}
