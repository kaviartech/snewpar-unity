﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VectorGraphics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class SessionManager : Singleton<SessionManager>
{
    public brainstormingResponse brainstorming;
    public ARSessionOrigin arsessiondoor;
    public ARSessionOrigin arsessionoobj;
    public ARSessionOrigin currentArSession;
    public KeyValuePair<string, GameObject> tempKeyIdea;

    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/
    public static bool once;
    //public static bool isEvent;
    public static bool isBilboard;
    public static bool isEnded;


    //public TextMeshProUGUI textTimer;

    public SVGImage profile;

    public override void Awake()
    {
        base.Awake();
#if !UNITY_WEBGL
        if (currentArSession)
            DestroyImmediate(currentArSession);
#endif
        //if (isEvent)
        //   currentArSession = Instantiate(arsessiondoor);
        //else
        //  currentArSession =  Instantiate(arsessionoobj);
        Application.targetFrameRate = 30;
        int targetWidth = 500;
        Screen.SetResolution(targetWidth, Screen.height * targetWidth / Screen.width, true);
    }
    void Start()
    {
        if (PinInput.brainstorming == null)
        {
            Common.instance.load(2);
        }
        else
        {
            brainstorming = PinInput.brainstorming;
            NetworkingClient.instance.enabled = true;
            PollBehaviour.instance.enabled = true;
            AccountManager.instance.enabled = true;
            once = false;

            TimerBehaviour.instance.initTimer(brainstorming.time == 0 ? 10 : brainstorming.time, () =>
            {
                if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
                {
                    closeSession(true);
                    isEnded = true;
                }
            });

            if (AuthManager.profile)
            {
                profile.sprite = AuthManager.profile;
                //profile.fit(100, 100);
            }
            else
            {
                if (Common.getUserInfo(out string id, out string path))
                {
                    //print("loading profile");
                    if (path != "")
                    {
                        //print("loading "+ path);


                        GameObject loading = Common.instance.startLoader("session.start");

                        if (!path.Contains("avataaars"))
                        {
                            //print(path);
                            this.fetchImage(path, (texture) =>
                            {

                                profile.sprite = Common.SpriteFromTexture2D(texture);
                                //profile.fit(100, 100);
                                AuthManager.profile = Common.SpriteFromTexture2D(texture);
                                Destroy(loading);

                            },
                            (error) =>
                            {
                                profile.sprite = Common.SpriteFromTexture2D(Resources.Load<Texture2D>("profile"));
                                Destroy(loading);
                            });

                        }
                        else
                        {
                            this.getText(path, response =>
                            {
                                var sprite = Common.instance.getSvgImage(response);

                                profile.sprite = sprite;

                                Destroy(loading);

                            });
                        }
                    }
                    else
                    {
                        //print("loading default profile");
                        profile.sprite = Common.SpriteFromTexture2D(Resources.Load<Texture2D>("profile"));
                    }

                }
            }


            initSession();

        }
    }


    public void step2()
    {
        if (PinInput.brainstorming.isPoll)
        {
            //print("using polls");
            PollBehaviour.instance.showPolls();
        }
        else
        {
            VotingBehaviour.instance.launchIdeaPool();
        }
    }

    [Space(10)]
    [Header("team panels")]
    [Space(30)]
    public Transform resume;
    public Transform members, ideas, votes;

    private void initSession()
    {
        StartCoroutine(fillResume(brainstorming.title, brainstorming.objectives, 1, 0, 0, brainstorming.time));
        StartCoroutine(fillMembers());
        fillIdeas();
        fillVotes();
    }

    [Space(10)]
    [Header("prefabs")]
    [Space(30)]
    public GameObject votePrefab;
    private void fillVotes()
    {
        TextMeshProUGUI[] temp = votes.GetComponentsInChildren<TextMeshProUGUI>();

        temp[0].text = "x0";
    }

    public GameObject ideaPrefab;
    private void fillIdeas()
    {
        TextMeshProUGUI[] temp = ideas.GetComponentsInChildren<TextMeshProUGUI>();

        temp[0].text =LanguageManager.isFR ? "x0" : "x0";
    }

    public GameObject memberPrefab;

    private IEnumerator fillMembers()
    {
        yield return new WaitForSeconds(2);

        TextMeshProUGUI[] temp = members.GetComponentsInChildren<TextMeshProUGUI>();

        temp[0].text = "x" + (NetworkingClient.serverObjects.Count).ToString();
    }

    public GameObject AddMember(string id, string username, string profile, bool isOwner)
    {

        TextMeshProUGUI[] temp = members.GetComponentsInChildren<TextMeshProUGUI>();

        foreach (Transform transform in members.transform.GetChild(2).GetChild(0))
        {
            if (transform.GetComponent<memberUiBehaviour>().usernameM == username)
            {
                return null;
            }
        }

        GameObject ttt = Instantiate(memberPrefab, members.transform.GetChild(2).GetChild(0));


        ttt.GetComponent<memberUiBehaviour>().setParticipant(this, username, profile, isOwner);

        //print("_________________________________________ setting members to " + (NetworkingClient.instance.serverObjects.Count + 1));
        temp[0].text = "x" + (NetworkingClient.serverObjects.Count  +1).ToString();
        //print("_________________________________________ setting members to " + (NetworkingClient.instance.serverObjects.Count + 1));
        //print("trying to fill resume");

        fillResume(NetworkingClient.serverObjects.Count + 1, -1, -1);

        return ttt;
    }

    public void AddVote(string question, string comment, StatsAnswer[] answers)
    {

        TextMeshProUGUI[] temp = votes.GetComponentsInChildren<TextMeshProUGUI>();

        Instantiate(votePrefab, votes.transform.GetChild(2).GetChild(0)).GetComponent<voteUiBehaviour>().set(question, comment, answers);

        temp[0].text = "x"+votes.transform.GetChild(2).GetChild(0).childCount;

        fillResume(-1, -1, votes.transform.GetChild(2).GetChild(0).childCount);
    }

    public GameObject AddIdea(string username, Idea idea)
    {

        GameObject gg = Instantiate(ideaPrefab, ideas.transform.GetChild(2).GetChild(0));

        gg.GetComponent<IdeaUiBehaviour>()
            .set(this, idea, username);

        return gg;


    }
    public GameObject AddVoteBrainstorming(string username, Idea idea)
    {

        //TextMeshProUGUI[] temp = votes.GetComponentsInChildren<TextMeshProUGUI>();

        GameObject gg = Instantiate(ideaPrefab, votes.transform.GetChild(2).GetChild(0));
        gg.GetComponent<IdeaUiBehaviour>().setVote(this, idea, username);
        return gg;

    }

    Dictionary<string, GameObject> ideasDict = new Dictionary<string, GameObject>(), votesDict = new Dictionary<string, GameObject>();

    internal void RefreshIdeas(bool isDelete = false)
    {
        StartCoroutine(Refresh(isDelete));
    }

    IEnumerator Refresh(bool isDelete = false)
    {
        yield return new WaitForSeconds(2);

        int voteSum = 0;
        int ideaSum = 0;

        foreach (KeyValuePair<string, Submission> item in VotingBehaviour.instance.submissions)
        {
            foreach (var ideaSub in item.Value.ideas)
            {

                ideaSum++;
                voteSum += ideaSub.likes.Count() + ideaSub.dislikes.Count();

                GameObject ggI, ggV;

                if (ideasDict.TryGetValue(ideaSub.id, out ggI))
                {
                    ggI.GetComponent<IdeaUiBehaviour>().refreshIdea(ideaSub);
                }
                else
                {
                    ggI = AddIdea(item.Key, ideaSub);
                    ideasDict.Add(ideaSub.id, ggI);
                }

                if (votesDict.TryGetValue(ideaSub.id, out ggV))
                {
                    ggV.GetComponent<IdeaUiBehaviour>().refreshVote(ideaSub);
                }
                else
                {
                    ggV = AddVoteBrainstorming(item.Key, ideaSub);
                    votesDict.Add(ideaSub.id, ggV);
                }
            }
        }

        if (isDelete)
        {
            foreach (KeyValuePair<string, GameObject> ideaDict in ideasDict)
            {
                if (VotingBehaviour.instance.submissions.TryGetValue(ideaDict.Value.GetComponent<IdeaUiBehaviour>().owner, out Submission s))
                {
                    if(s.idea(ideaDict.Key) == null)
                    {

                        tempKeyIdea = ideaDict;
                        break;
                    }
                }
            }

            Destroy(ideasDict[tempKeyIdea.Key]);
            Destroy(votesDict[tempKeyIdea.Key]);
            ideasDict.Remove(tempKeyIdea.Key);
            votesDict.Remove(tempKeyIdea.Key);
        }

        TextMeshProUGUI[] temp = ideas.GetComponentsInChildren<TextMeshProUGUI>();
        temp[0].text = "x"+ideaSum;

        TextMeshProUGUI[] temp1 = votes.GetComponentsInChildren<TextMeshProUGUI>();

        temp1[0].text = "x" + voteSum;

        fillResume(-1, ideaSum, -1);
        fillResume(-1, -1, voteSum);

        yield return null;
    }

    private IEnumerator fillResume(string title, string objectives, int v0, int v1, int v2, int time)
    {
        TextMeshProUGUI[] temp = resume.GetComponentsInChildren<TextMeshProUGUI>();
        RawImage ri = resume.GetComponentInChildren<RawImage>();

        ri.texture = Resources.Load<Texture2D>("brainstorming");
        if (brainstorming.path == null)
        {
            print("PATH");
            brainstorming.path = "https://ui-avatars.com/api/?background=random&name=" + title + "&rounded=true&font-size=0.33";
        }
        if (!brainstorming.path.Contains("ui-avatars"))
        {
            this.fetchImage(brainstorming.path, (texture) =>
            {

                SessionInfo.instance.Init(title, objectives, time, texture, brainstorming.isPoll);

                ri.texture = texture;
            //ri.fit(300, 300);
            ri.SetNativeSize();
            });
        }
        else
        {
            SessionInfo.instance.Init(title, objectives, time, null, brainstorming.isPoll);
            ri.gameObject.SetActive(false);
        }
        Debug.Log("temps length =" + temp.Length);
        temp[0].text = title;
        temp[1].text = objectives;

        GameObject.Find("submenu").transform.GetChild(2).GetComponentInChildren<TextMeshProUGUI>(true).text = v1.ToString();
        GameObject.Find("submenu").transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>(true).text = v1.ToString();

        if (GameObject.Find("submenu").transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>(true).text != "0")
        {
            GameObject.Find("submenu").transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>(true).transform.parent.gameObject.SetActive(true);
        }

        if (GameObject.Find("submenu").transform.GetChild(2).GetComponentInChildren<TextMeshProUGUI>(true).text != "0")
        {
            GameObject.Find("submenu").transform.GetChild(2).GetComponentInChildren<TextMeshProUGUI>(true).transform.parent.gameObject.SetActive(true);
        }

        yield return new WaitForSeconds(2);

        //temp[2].text = v0.ToString() + " participants";
        Debug.Log("parti");
        Debug.Log(NetworkingClient.serverObjects.Count);
        GameObject.Find("submenu").transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>(true).text = (NetworkingClient.serverObjects.Count).ToString();

        if (GameObject.Find("submenu").transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>(true).text != "0")
        {
            GameObject.Find("submenu").transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>(true).transform.parent.gameObject.SetActive(true);
        }

        //temp[3].text = v1.ToString() + " idées";
        //temp[4].text = v2.ToString() + " votes";

    }

    public void fillResume(int members, int ideas, int votes)
    {
        TextMeshProUGUI[] temp = resume.GetComponentsInChildren<TextMeshProUGUI>(true);

        //print("_________________________________________ setting members to " + members);

        if (members >= 0)
        {
            //print("_________________________________________ setting members to " + members);
            GameObject.Find("submenu").transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>(true).text = members.ToString();

            if (GameObject.Find("submenu").transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>(true).text != "0")
            {
                GameObject.Find("submenu").transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>(true).transform.parent.gameObject.SetActive(true);
            }

            //print("_________________________________________ setting members to " + members);
        temp[2].text = LanguageManager.isFR ? "x"+members.ToString() + " participant" : "x"+members.ToString()+" Guests";
        }

        if (ideas >= 0)
        {
            GameObject.Find("submenu").transform.GetChild(2).GetComponentInChildren<TextMeshProUGUI>(true).text = ideas.ToString();

            if (GameObject.Find("submenu").transform.GetChild(2).GetComponentInChildren<TextMeshProUGUI>(true).text != "0")
            {
                resume.GetChild(3).gameObject.SetActive(true);
                GameObject.Find("submenu").transform.GetChild(2).GetComponentInChildren<TextMeshProUGUI>(true).transform.parent.gameObject.SetActive(true);
            }

        temp[3].text = LanguageManager.isFR ? "x" + ideas.ToString() + " idée" : "x" + ideas.ToString() + " idea";
        }

        if (votes >= 0)
        {
            GameObject.Find("submenu").transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>(true).text = votes.ToString();

            if (GameObject.Find("submenu").transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>(true).text != "0")
            {
                resume.GetChild(4).gameObject.SetActive(true);
                GameObject.Find("submenu").transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>(true).transform.parent.gameObject.SetActive(true);
            }
        temp[4].text = "x"+votes.ToString() + " vote";
        }



    }


    public void closeSession(bool forced = false)
    {
        if (!forced)
        {
            //Common.alert(LanguageManager.isFR ? "Voulez-vous vraiment mettre fin à cette session ?" : "Are you sure you want to end this session?", () =>
            //{

            //});

            if (brainstorming.isOwner)
            {
                //NetworkingClient.instance.Emit("endSession");
                if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
                    VotingBehaviour.instance.showEndOwner();
                else
                    Common.instance.load(2);

            }
            else
            {
                Common.alertYN(LanguageManager.isFR ? "Voulez-vous vraiment quitter cette session ?" : "Are you sure you want to quit this session?", () =>
                {
                    if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
                        NetworkingClient.s.close();
                    Common.instance.load(2);
                    isEnded = true;
                }, () => {

                });
            }

        }
        else
        {
            if (PlayerPrefs.GetInt("fromDeepLink", 0) == 1)
            {
                PlayerPrefs.SetInt("fromDeepLink", 0);
                Common.alertYN(LanguageManager.isFR ? "Vous allez être redirigé vers une nouvelle session.\n Souhaitez-vous quitter cette session maintenant ?" : "You will be redirected to a new session.\n Do you want to leave this session now ?", () =>
                {
                    forcedClose();

                },() => {
                    
                });
            }
            else
            {
                forcedClose();
            }
        }

    }
    public void  forcedClose()
    {
        if (brainstorming.isOwner)
        {
            if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
                NetworkingClient.s.emit("endSession", "");
            Common.instance.load(2);


        }
        else
        {
            if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
                NetworkingClient.s.close();
            Common.instance.load(2);
        }
    }
    private void Update()
    {
        if (PlayerPrefs.GetInt("fromDeepLink", 0) == 1)
        {
            closeSession(true);
            isEnded = true;
        }
    }


}
