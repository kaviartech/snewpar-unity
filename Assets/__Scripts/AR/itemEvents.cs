﻿/*

*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class itemEvents : MonoBehaviour
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/
    public float counter = 1;
    public bool isCounting = false;
    public List<int> reacts;
    public GameObject reactsParent;
    public string category = "";
    public List<int> test;

    public GameObject selection;


    GameObject FindInActiveObjectByName(string name)
    {
        Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].hideFlags == HideFlags.None)
            {
                if (objs[i].name == name)
                {
                    return objs[i].gameObject;
                }
            }
        }
        return null;
    }

    public void Select()
    {
        //print("selecting " + name);
        selection.SetActive(true);
    }

    public void Deselect()
    {
        selection.SetActive(false);
    }
    public List<int> Reacts
    {
        get => reacts;
        set
        {
            test = value;
            List<int> temp = value;

            List<int> inter = new List<int>();

            foreach (var item in temp)
            {
                if (item < 0)
                {
                    inter.Remove(-(item + 1));
                }
                else
                {
                    inter.Add(item);
                }
            }

            temp = inter;
            for (int i = 0; i < 3; i++)
            {
                reactsParent.transform.GetChild(i).gameObject.SetActive(false);
            }
            foreach (var item in temp)
            {
                reactsParent.transform.GetChild(2 - item).gameObject.SetActive(true);
            }

            reactsParent.GetComponentsInChildren<TextMeshProUGUI>().Last().text = temp.Count.ToString();

            string id = name == "Text" ? transform.parent.name : name;

            id = "idea_" + id;

            IdeaUiBehaviour idUB = FindInActiveObjectByName(id).GetComponent<IdeaUiBehaviour>();

            idUB.happy.transform.parent.parent.gameObject.SetActive(temp.Where(num => num == 2).Count() != 0);
            idUB.happy.text = temp.Where(num => num == 2).Count().ToString();
            //print(temp.Where(num => num == 2).Count().ToString());

            idUB.angry.transform.parent.parent.gameObject.SetActive(temp.Where(num => num == 1).Count() != 0);
            idUB.angry.text = temp.Where(num => num == 1).Count().ToString();
            //print(temp.Where(num => num == 1).Count().ToString());

            idUB.sad.transform.parent.parent.gameObject.SetActive(temp.Where(num => num == 0).Count() != 0);
            idUB.sad.text = temp.Where(num => num == 0).Count().ToString();
            //print(temp.Where(num => num == 0).Count().ToString());

            reacts = temp;
        }
    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    private void OnMouseDown()
    {
        if (!Common.IsPointerOverUIObject(Input.mousePosition))
        {

            //start counter
            counter = 1;
            isCounting = true;
        }
        //else if (name == "Text")
        //{
        //    //start counter
        //    counter = 1;
        //    isCounting = true;
        //}
    }

    private void OnMouseOver()
    {
        //increase counter
        if (!Common.IsPointerOverUIObject(Input.mousePosition))
        {
            if (isCounting)
            {
                counter -= Time.deltaTime;
            }
        }
        else if (transform.GetComponentInChildren<Canvas>().transform.name.Contains("video"))
        {
            if (isCounting)
            {
                counter -= Time.deltaTime;
            }
        }
    }

    private void OnMouseUp()
    {
        //if over 1s
        //open React panel
        if (!Common.IsPointerOverUIObject(Input.mousePosition))
        {
            //print(counter);
            if (isCounting && counter <= -1f)
            {
                //reactionBehaviour.instance.react(name == "Text" ? transform.parent.name : name);

                //print("____________________________________ choose item");

                string id = name == "Text" ? transform.parent.name : name;

                if (id.EndsWith(NetworkingClient.ClientID))
                {
                    HistoryManager.instance.chooseItem(id);
                }
                else
                {
                    Common.alert(LanguageManager.isFR ?
                        "Vous n'avez pas le droit de modifier cet élément." :
                        "You don't have the right to edit this element.");
                }
 

            }
            else if(isCounting && counter <= 0.5f)
            {
                react();
            }
            else if (isCounting && counter <= .75f)
            {
                Common.alert(LanguageManager.isFR ?
                    "Pour réagir à un élément : appuyer et maintenir enfoncé 1 sec.\n \n Pour modifier un élément: ouvrir l'historique (bouton en bas à droite) puis sélectionner l'élément à modifier ou appuyer et maintenir enfoncé 2 sec ." :
                    "To react to an element: press and hold down for 1 sec. \n \n To edit an element: open the history (button at the bottom right) then select the element to modify or press and hold down for 2 secs.");
            }
            else if (isCounting && counter <= 1f && PlayerPrefs.GetInt("isFirstPress", 1) == 1)
            {
                Common.alert(LanguageManager.isFR ?
                    "Pour réagir à un élément : appuyer et maintenir enfoncé 1 sec.\n \n Pour modifier un élément: ouvrir l'historique (bouton en bas à droite) puis sélectionner l'élément à modifier ou appuyer et maintenir enfoncé 2 sec ." :
                    "To react to an element: press and hold down for 1 sec. \n \n To edit an element: open the history (button at the bottom right) then select the element to modify or press and hold down for 2 secs.");

                PlayerPrefs.SetInt("isFirstPress", 0);
            }
            isCounting = false;
        }
        //else if(name == "Text")
        //{
        //    if (isCounting && counter <= 0.5f)
        //    {
        //        reactionBehaviour.instance.react(name == "Text" ? transform.parent.name : name);

        //        var happies = Reacts.Where(num => num == 2);
        //        var angries = Reacts.Where(num => num == 1);
        //        var sads = Reacts.Where(num => num == 0);

        //        reactionBehaviour.instance.HappyI = happies.Count();
        //        reactionBehaviour.instance.AngryI = angries.Count();
        //        reactionBehaviour.instance.SadI = sads.Count();
        //        reactionBehaviour.instance.CurrentCategory = category;

        //        string id = name == "Text" ? transform.parent.name : name;
        //        id = "idea_" + id;
        //        IdeaUiBehaviour idUB = FindInActiveObjectByName(id).GetComponent<IdeaUiBehaviour>();

        //        idUB.happy.transform.parent.parent.gameObject.SetActive(happies.Count() != 0);
        //        idUB.happy.text = happies.Count().ToString();
        //        print(happies.Count().ToString());

        //        idUB.angry.transform.parent.parent.gameObject.SetActive(angries.Count() != 0);
        //        idUB.angry.text = angries.Count().ToString();
        //        print(angries.Count().ToString());

        //        idUB.sad.transform.parent.parent.gameObject.SetActive(sads.Count() != 0);
        //        idUB.sad.text = sads.Count().ToString();
        //        print(sads.Count().ToString());


        //    }
        //    else if (isCounting && counter <= .75f)
        //    {
        //        Common.alert(LanguageManager.isFR ? "Pour réagir à un élément : appuyer et maintenir enfoncé 1 sec.\n \n Pour déplacer un élément: ouvrir l'historique (bouton en bas à droite) puis sélectionner l'élément à modifier." : "To react to an element: press and hold down for 1 sec. \n \n To move an element: open the history (button at the bottom right) then select the element to modify.");
        //    }
        //    else if (isCounting && counter <= 1f && PlayerPrefs.GetInt("isFirstPress", 1) == 1)
        //    {
        //        Common.alert(LanguageManager.isFR ? "Pour réagir à un élément : appuyer et maintenir enfoncé 1 sec.\n \n Pour déplacer un élément: ouvrir l'historique (bouton en bas à droite) puis sélectionner l'élément à modifier." : "To react to an element: press and hold down for 1 sec. \n \n To move an element: open the history (button at the bottom right) then select the element to modify.");

        //        PlayerPrefs.SetInt("isFirstPress", 0);
        //    }
        //    isCounting = false;
        //}
    }




    internal void react()
    {
        string id = name == "Text" ? transform.parent.name : name;

        id = "idea_" + id;
        IdeaUiBehaviour idUB = FindInActiveObjectByName(id).GetComponent<IdeaUiBehaviour>();

        reactionBehaviour.instance.react(idUB.Idea);

        var happies = Reacts.Where(num => num == 2);
        var angries = Reacts.Where(num => num == 1);
        var sads = Reacts.Where(num => num == 0);

        reactionBehaviour.instance.HappyI = happies.Count();
        reactionBehaviour.instance.AngryI = angries.Count();
        reactionBehaviour.instance.SadI = sads.Count();
        reactionBehaviour.instance.CurrentCategory = category;
        reactionBehaviour.instance.inputComment.text = "";

        
        

        idUB.happy.transform.parent.parent.gameObject.SetActive(happies.Count() != 0);
        idUB.happy.text = happies.Count().ToString();
        //print(happies.Count().ToString());

        idUB.angry.transform.parent.parent.gameObject.SetActive(angries.Count() != 0);
        idUB.angry.text = angries.Count().ToString();
        //print(angries.Count().ToString());

        idUB.sad.transform.parent.parent.gameObject.SetActive(sads.Count() != 0);
        idUB.sad.text = sads.Count().ToString();
        //print(sads.Count().ToString());

        //print(idUB.content);

        reactionBehaviour.instance.contentType = idUB.contentType;
        reactionBehaviour.instance.content = idUB.content;
       
    }



    public void PointerDown()
    {
        //start counter
        counter = 1;
        isCounting = true;
        //print("down");
    }

    public void PointerOver()
    {
        //print("over");
        if (isCounting)
        {
            counter -= Time.deltaTime;
        }
        //else if(name == "Text")
        //{
        //    if (isCounting)
        //    {
        //        counter -= Time.deltaTime;
        //    }
        //}
    }

    public void PointerUp()
    {
        //print(counter);
        if (isCounting && counter <= -1f)
        {
            //reactionBehaviour.instance.react(name == "Text" ? transform.parent.name : name);

            //print("____________________________________ choose item");

            string id = name == "Text" ? transform.parent.name : name;

            if (id.EndsWith(NetworkingClient.ClientID))
            {
                HistoryManager.instance.chooseItem(id);
            }
            else
            {
                Common.alert(LanguageManager.isFR ?
                    "Vous n'avez pas le droit de modifier cet élément." :
                    "You don't have the right to edit this element.");
            }

        }
        else if (isCounting && counter <= 0.5f)
        {
            react();

        }
        else if (isCounting && counter <= .75f)
        {
            Common.alert(LanguageManager.isFR ?
                "Pour réagir à un élément : appuyer et maintenir enfoncé 1 sec.\n \n Pour modifier un élément: ouvrir l'historique (bouton en bas à droite) puis sélectionner l'élément à modifier ou appuyer et maintenir enfoncé 2 sec ." :
                "To react to an element: press and hold down for 1 sec. \n \n To edit an element: open the history (button at the bottom right) then select the element to modify or press and hold down for 2 secs.");
        }
        else if (isCounting && counter <= 1f && PlayerPrefs.GetInt("isFirstPress", 1) == 1)
        {
            Common.alert(LanguageManager.isFR ?
                "Pour réagir à un élément : appuyer et maintenir enfoncé 1 sec.\n \n Pour modifier un élément: ouvrir l'historique (bouton en bas à droite) puis sélectionner l'élément à modifier ou appuyer et maintenir enfoncé 2 sec ." :
                "To react to an element: press and hold down for 1 sec. \n \n To edit an element: open the history (button at the bottom right) then select the element to modify or press and hold down for 2 secs.");

            PlayerPrefs.SetInt("isFirstPress", 0);
        }
        isCounting = false;
    }


}
