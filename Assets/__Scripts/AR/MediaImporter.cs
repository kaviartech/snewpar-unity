﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Linq;


[System.Serializable]
public class mediaResponse
{
    public string message;
    public bool success;
    public List<string> paths;
}

public class MediaImporter : MonoBehaviour
{

    public GameObject images, videos, session;
    public GameObject imagesT, videosT, sessionT;

    public GameObject pref;

    public GameObject Vidpref;

    public void getGallery()
    {
        string token = Common.getUserInfo();

        string json = $"^'token':'{token}'@".ToJSON();
        //print(json);

        if (PinInput.brainstorming != null)
        {
            foreach (var path in PinInput.brainstorming.medias)
            {
                if (path.Contains("vid_"))
                {
                    GameObject temp = Instantiate(Vidpref, session.transform);

                    RenderTexture rt = new RenderTexture(100, 100, 16, RenderTextureFormat.ARGB32);
                    rt.Create();

                    temp.GetComponentInChildren<RawImage>().texture = rt;


                    VideoPlayer videoPlayer = temp.GetComponentInChildren<VideoPlayer>();


                    videoPlayer.targetTexture = rt;

                    //print(1);

                    videoPlayer.enabled = true;

                    try
                    {
                        videoPlayer.url = path;
                        //print(2);

                        videoPlayer.playOnAwake = true;
                        //print(3);

                        videoPlayer.Play();
                        //print(4);

                        StartCoroutine(stopInAWHile(videoPlayer, temp, path));
                    }
                    catch (System.Exception)
                    {
                        print("videos from s3 cant stream in windows");
                    }

                    
                }
                else if (path != "")
                {
                    this.fetchImage(path, (texture) =>
                    {

                        GameObject temp = Instantiate(pref, session.transform);

                        RawImage im = temp.GetComponentInChildren<RawImage>();

                        //print(texture.width.ToString() + "   " + texture.height.ToString());

                        im.texture = texture;


                        //string newPath = Path.Combine(Application.persistentDataPath, $"{path.Split('/').Last().Split('.').First()}.png");

                        //path = newPath;

                        //print(path);

                        //byte[] bytes = texture.EncodeToPNG();
                        //File.WriteAllBytes(newPath, bytes);

                        im.fit((int)im.GetComponent<RectTransform>().sizeDelta.x, (int)im.GetComponent<RectTransform>().sizeDelta.y);

                        Button b = temp.AddComponent<Button>();

                        b.onClick.AddListener(delegate { MediaManager.instance.selectImage(path, texture); });
                    });
                }

                session.SetActive(true);
                sessionT.SetActive(true);

            }
        }

        this.fetchText(Common.Baseurl + "api/medias" , json , (res) => 
        {
            mediaResponse response = JsonUtility.FromJson<mediaResponse>(res);

            if (response.success)
            {
                foreach (var path in response.paths)
                {
                    if (path.Contains("vid_"))
                    {
                        GameObject temp = Instantiate(Vidpref, videos.transform);

                        RenderTexture rt = new RenderTexture(100, 100, 16, RenderTextureFormat.ARGB32);
                        rt.Create();

                        temp.GetComponentInChildren<RawImage>().texture = rt;


                        VideoPlayer videoPlayer = temp.GetComponentInChildren<VideoPlayer>();


                        videoPlayer.targetTexture = rt;


                        videoPlayer.enabled = true;

                        try
                        {
                            videoPlayer.url = path;

                            videoPlayer.playOnAwake = true;

                            videoPlayer.Play();

                            StartCoroutine(stopInAWHile(videoPlayer, temp, path));
                        }
                        catch (System.Exception)
                        {
                            print("videos from s3 cant stream in windows");
                        }

                        

                        videos.SetActive(true);
                        videosT.SetActive(true);

                    }
                    else if(path != "")
                    {
                        this.fetchImage(path, (texture) =>
                        {

                            GameObject temp = Instantiate(pref, images.transform);

                            RawImage im = temp.GetComponentInChildren<RawImage>();

                            //print(texture.width.ToString() + "   " + texture.height.ToString());

                            im.texture = texture;


                            //string newPath = Path.Combine(Application.persistentDataPath, $"{path.Split('/').Last().Split('.').First()}.png");

                            //path = newPath;

                            //print(path);

                            //byte[] bytes = texture.EncodeToPNG();
                            //File.WriteAllBytes(newPath, bytes);

                            im.fit((int)im.GetComponent<RectTransform>().sizeDelta.x, (int)im.GetComponent<RectTransform>().sizeDelta.y);

                            Button b = temp.AddComponent<Button>();

                            b.onClick.AddListener(delegate { MediaManager.instance.selectImage(path, texture); });

                            images.SetActive(true);
                            imagesT.SetActive(true);
                        });
                    }
                }
            }
            else
            {
                Debug.Log("Media imported error : " + response.message);
            }

        });

    }

    IEnumerator stopInAWHile(VideoPlayer videoPlayer,GameObject temp,string path)
    {
        yield return new WaitUntil(() => videoPlayer.isPlaying);


        yield return new WaitForSeconds(.1f);

        try
        { 
            videoPlayer.Pause();

            Button b = temp.AddComponent<Button>();

            b.onClick.AddListener(delegate { MediaManager.instance.selectVideo(path); });
            
        }
        catch (System.Exception)
        {
            print("videos from s3 cant stream in windows");
        }

        yield return null;

        videos.transform.parent.GetComponent<VerticalLayoutGroup>().enabled = false;
        videos.transform.parent.GetComponent<VerticalLayoutGroup>().enabled = true;
    }

    private void Start()
    {
        getGallery();
    }
}
