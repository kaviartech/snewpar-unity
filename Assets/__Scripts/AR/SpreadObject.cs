using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadObject : MonoBehaviour
{
    public bool isObjectSpread = false;
    public float dist = 0.0001f;
    public void spreadObject()
    {
        //var dist = 0.0001f;
        var f = dist;
        if (!isObjectSpread)
        {
            foreach (Transform t in ObjectLoader.objectComponents)
            {
                if(t.GetComponent<BoxCollider>().center.x > t.GetComponent<BoxCollider>().center.y && t.GetComponent<BoxCollider>().center.x > t.GetComponent<BoxCollider>().center.z)
                {
                    t.localPosition = new Vector3(t.localPosition.x >= 0 ? t.localPosition.x + f : t.localPosition.x - f,
                                         t.localPosition.y,
                                         t.localPosition.z);
                }
                else if(t.GetComponent<BoxCollider>().center.y > t.GetComponent<BoxCollider>().center.x && t.GetComponent<BoxCollider>().center.y > t.GetComponent<BoxCollider>().center.z)
                {
                    t.localPosition = new Vector3(t.localPosition.x,
                                         t.GetComponent<BoxCollider>().center.z >= 0 ? t.localPosition.z + f : t.localPosition.z - f,
                                         t.localPosition.y);
                }
                else
                {
                    t.localPosition = new Vector3(t.localPosition.x ,
                                         t.localPosition.z,
                                         t.GetComponent<BoxCollider>().center.y >= 0 ? t.localPosition.y + f : t.localPosition.y - f);
                }
                //t.localPosition = new Vector3(t.localPosition.x >= 0 ? t.localPosition.x + f : t.localPosition.x - f,
                //                         t.localPosition.y >= 0 ? t.localPosition.y + f : t.localPosition.y - f,
                //                         t.localPosition.z /*>= 0 ? t.localPosition.z + f : t.localPosition.z - f*/);
                f += dist;
            }
            isObjectSpread = true;
        }
        else
        {
            foreach (Transform t in ObjectLoader.objectComponents)
            {
                if (t.GetComponent<BoxCollider>().center.x > t.GetComponent<BoxCollider>().center.y && t.GetComponent<BoxCollider>().center.x > t.GetComponent<BoxCollider>().center.z)
                {
                    t.localPosition = new Vector3(t.localPosition.x >= 0 ? t.localPosition.x - f : t.localPosition.x + f,
                                         t.localPosition.y,
                                         t.localPosition.z);
                }
                else if (t.GetComponent<BoxCollider>().center.y > t.GetComponent<BoxCollider>().center.x && t.GetComponent<BoxCollider>().center.y > t.GetComponent<BoxCollider>().center.z)
                {
                    t.localPosition = new Vector3(t.localPosition.x,
                                         t.localPosition.y >= 0 ? t.localPosition.y - f : t.localPosition.y + f,
                                         t.localPosition.z);
                }
                else
                {
                    t.localPosition = new Vector3(t.localPosition.x,
                                         t.localPosition.y,
                                         t.localPosition.z >= 0 ? t.localPosition.z - f : t.localPosition.z + f);
                }
                //t.localPosition = new Vector3(t.localPosition.x >= 0 ? t.localPosition.x - f : t.localPosition.x + f,
                //                         t.localPosition.y >= 0 ? t.localPosition.y - f : t.localPosition.y + f,
                //                         t.localPosition.z /*>= 0 ? t.localPosition.z - f : t.localPosition.z + f*/);
                f += dist;
            }
            isObjectSpread = false;
        }
    }

   
}
