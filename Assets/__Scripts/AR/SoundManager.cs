﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public AudioClip moon, sahara;
    
    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void toggleSound()
    {
        if (!GetComponent<AudioSource>().isPlaying)
        {
            GetComponent<AudioSource>().Play();
        }
        else
        {
            GetComponent<AudioSource>().Stop();
        }
    }

    public void Play()
    {
        if (PinInput.brainstorming != null)
        {
            if (PinInput.brainstorming.theme == 0)
            {
                GetComponent<AudioSource>().clip = moon;
                GetComponent<AudioSource>().Play();
            }
            else if (PinInput.brainstorming.theme == 1)
            {

                GetComponent<AudioSource>().clip = sahara;
                GetComponent<AudioSource>().Play();
            }
        }
    }


}
