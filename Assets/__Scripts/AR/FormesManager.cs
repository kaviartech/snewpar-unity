﻿using UnityEngine;
using UnityEngine.UI;

public class FormesManager : Singleton<FormesManager>
{
    public GameObject panel;

    public bool isActive;
    private void Start()
    {
        //this.transform.SetParent(GameObject.Find("MeetingArea").transform);
    }

    public void toggleActive()
    {
        Common.deactivateAll();
        Gestures.instance.GesturesReady = false; //isActive;
        panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
        isActive = !isActive;
    }


    public void deactivate()
    {
        Gestures.instance.GesturesReady = false; //isActive;
        panel.GetComponent<Animator>().Play("hide");
        isActive = false;
    }

    public GameObject gameObjectToInstantiate;

    public void select(Image im)
    {
        //print("index of child is " + (im.transform.GetSiblingIndex() + 1).ToString("00"));

        Sprite sprite = im.sprite;

        Texture2D dst = new Texture2D((int)sprite.textureRect.width, (int)sprite.textureRect.height);
        var pixels = sprite.texture.GetPixels(
                                                (int)sprite.textureRect.x,
                                                (int)sprite.textureRect.y,
                                                (int)sprite.textureRect.width,
                                                (int)sprite.textureRect.height
                                              );
        dst.SetPixels(pixels);
        dst.Apply();


        PlacementSystem.instance.current.texture = dst;
        //print((dst.height / dst.width));
        PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta = new Vector2(1, ((float)dst.height / dst.width)) * PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta.x;

        GameObject temp = Instantiate(gameObjectToInstantiate);

        temp.GetComponent<Renderer>().material.mainTexture = dst;
        temp.transform.localScale = new Vector3(-.1f, .005f, -.1f * ((float)dst.height / (float)dst.width));

        temp.transform.GetChild(0).gameObject.SetActive(false);

        temp.transform.GetChild(1).GetChild(0).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
        temp.transform.GetChild(1).GetChild(1).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
        temp.transform.GetChild(1).GetChild(2).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
        temp.transform.GetChild(1).GetChild(3).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);


        temp.SetActive(false);

        panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
        isActive = !isActive;

        PlacementSystem.instance.rect = new FormeRect(
                                                        (int)sprite.textureRect.x,
                                                        (int)sprite.textureRect.y,
                                                        (int)sprite.textureRect.width,
                                                        (int)sprite.textureRect.height
                                                      );

        PlacementSystem.instance.currentType = "FORME";
        PlacementSystem.instance.content = Common.Baseurl + "shapes/" + (im.transform.GetSiblingIndex() + 1).ToString("00") + ".png";


        PlacementSystem.instance.gameObjectToInstantiate = temp;


    }
}
