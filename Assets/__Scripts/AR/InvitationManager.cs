using System.Collections;
using System.Collections.Generic;
#if !UNITY_WEBGL
using Firebase.DynamicLinks;
#endif
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using UnityEngine.UI;

using Newtonsoft.Json;
using System;

public class InvitationManager : MonoBehaviour
{
    #if !UNITY_WEBGL
    /**Const**/
    string shortLinkAPI = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyA_VUh0jL_ZvnIpbeGTZ41x9IOwntu6IL8";
    /**SHARE  Link Varaibles**/
    public GameObject shareLinkPanel;
    public TMP_InputField inviteLinkInput;
    public GameObject copy;
    public GameObject copied;
    public string inviteLink;
    /**QRCODE variables**/
    public GameObject qrCodePanel;
    public QRCodeEncodeController e_qrController;
    public RawImage qrCodeImage;
    /**Invite email functions**/
    public GameObject InvitePanel;
    public TMP_InputField inviteEmailInput;

    private void Start()
    {
        //Prepare link
        inviteLink = "https://snewpar.herokuapp.com/room/" + PinInput.brainstorming._id + "&" + PinInput.brainstorming.pin;
        var components = new DynamicLinkComponents(new Uri(inviteLink), "https://snewpar.fr");

        //Shorten  link
        string json = "{'longDynamicLink':'"+ components.LongDynamicLink.AbsoluteUri+"'}";
        this.fetchText(shortLinkAPI, json, (response) =>
        {
            print(response);
            ShortLinkJson res = JsonUtility.FromJson<ShortLinkJson>(response);
            inviteLinkInput.text = res.shortLink;
        });

        //Generate QRCode
        e_qrController.Encode(inviteLink);
    }

    public void desactivateAll()
    {
        qrCodePanel.SetActive(false);
        shareLinkPanel.SetActive(false);
        InvitePanel.SetActive(false);
        copy.SetActive(true);
        copied.SetActive(false);
    }

    /**SHARE  Link Functions**/
    public void toggleShareLink()
    {
        desactivateAll();
        shareLinkPanel.SetActive(!shareLinkPanel.activeSelf);
    }

    public void copyLinkToClipBoard()
    {
        inviteLinkInput.text.CopyToClipboard();
        copy.SetActive(!copy.activeSelf);
        copied.SetActive(!copied.activeSelf);
    }

    public void share()
    {
        new NativeShare().SetTitle("SnewpAR invitation").SetSubject(LanguageManager.isFR?"Ne tardez pas, rejoignez la reuinion !!!" : "Hurry up, join the meeting !!!").SetText(inviteLinkInput.text).Share();
        desactivateAll();
    }

    /**QRCODE functions**/
    public void toggleQrCode()
    {
        desactivateAll();
        qrCodePanel.SetActive(!qrCodePanel.activeSelf);
    }

    public void qrEncodeFinished(Texture2D tex)
    {
        int width = tex.width;
        int height = tex.height;
        float aspect = width * 1.0f / height;
        qrCodeImage.GetComponent<RectTransform>().sizeDelta = new Vector2(846, 846.0f / aspect);
        qrCodeImage.texture = tex;
    }

    /**Invite email functions**/
    public void toggleInvite()
    {
        desactivateAll();
        InvitePanel.SetActive(!InvitePanel.activeSelf);
    }

    public void SendInvite()
    {
        //StartCoroutine(invitePostRequestFunc(inviteEmailInput.text));
        InvitePostRequest invitePostRequest = new InvitePostRequest()
        {
            username = PlayerPrefs.GetString("username", ""),
            pin = PinInput.brainstorming.pin,
            id = PinInput.brainstorming._id,
            mail = inviteEmailInput.text,
            token = PlayerPrefs.GetString("token", ""),
            isdemo = false
            
        };
        
        string jsonUti = JsonUtility.ToJson(invitePostRequest);
        print(jsonUti);
        this.fetchText(Common.Baseurl+"api/brainstorming/inviteOne", jsonUti, response =>
        {
            print(response);
        });
        desactivateAll();
    }
#endif
}

[Serializable]
public class Warning
{
    public string warningCode;
    public string warningMessage;
}
[Serializable]
public class ShortLinkJson
{
    public string shortLink;
    public List<Warning> warning;
    public string previewLink;
}

// InvitePostRequest myDeserializedClass = JsonConvert.DeserializeObject<InvitePostRequest>(myJsonResponse);
[Serializable]
public class InvitePostRequest
{

    public string id;
    public string username;
    public string mail;
    public string pin;
    public string token;
    public bool isdemo;
}

[Serializable]
public static class ClipboardExtension
{
    /// <summary>
    /// Puts the string into the Clipboard.
    /// </summary>
    public static void CopyToClipboard(this string str)
    {
        GUIUtility.systemCopyBuffer = str;
    }
}