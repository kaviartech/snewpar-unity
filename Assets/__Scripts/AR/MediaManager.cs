﻿using Unity.VideoHelper;
using UnityEngine;
using UnityEngine.Video;

public class MediaManager : Singleton<MediaManager>
{

    public GameObject panel;

    public Texture2D[] texxtures;

    public bool isActive;
    private void Start()
    {
        //this.transform.SetParent(GameObject.Find("MeetingArea").transform);
    }
    public void toggleActive()
    {
        Common.deactivateAll();
        Gestures.instance.GesturesReady = false; //isActive;
        panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
        isActive = !isActive;
    }


    public void deactivate()
    {
        Gestures.instance.GesturesReady = false; //isActive;
        panel.GetComponent<Animator>().Play("hide");
        isActive = false;
    }

    public GameObject imageToInstantiate;
    public GameObject videoToInstantiate;


    public Texture2D Image, Video;


    public void selectVideo(string uri)
    {
        //Common.alert("cette fonctionnalité n'est disponible que localement .... le partage de ce video est en cours de développement");
        //return;

        PlacementSystem.instance.current.texture = Video;

        PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta = new Vector2(1, (int)(Image.height / Image.width)) * PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta.x;

        GameObject temp = Instantiate(videoToInstantiate);

        temp.transform.GetChild(0).gameObject.SetActive(false);


        //VideoPlayer vp = temp.GetComponent<VideoPlayer>();

        //vp.url = uri;

        //print(vp.url);


        //vp.prepareCompleted += (VideoPlayer source) =>
        //{
        //    Debug.Log("dimensions " + source.texture.width + " x " + source.texture.height); // do with these dimensions as you wish

        //    source.transform.localScale = new Vector3(-.1f, .005f, .1f * ((float)source.texture.height / (float)source.texture.width));

        //};

        //vp.Prepare();


        VideoController vc = temp.GetComponentInChildren<VideoController>(true);

        //this.Invoke(() => {  }, .5f);
        vc.PrepareForUrl(uri);


        //temp.transform.localScale = new Vector3(.1f, .005f, .1f * ((float)vidTex.height / (float)vidTex.width));


        temp.SetActive(false);

        PlacementSystem.instance.gameObjectToInstantiate = temp;

        PlacementSystem.instance.currentType = "VIDEO";

        PlacementSystem.instance.content = uri;

        panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
        isActive = !isActive;
    }


    public void selectImage(string path,Texture2D texture)
    {
        //Common.alert("cette fonctionnalité n'est disponible que localement .... le partage de cette image est en cours de développement");
        //return;

        PlacementSystem.instance.current.texture = Image;

        PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta = new Vector2(1, (int)(Image.height / Image.width)) * PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta.x;

        GameObject temp = Instantiate(imageToInstantiate);

        temp.GetComponent<Renderer>().material.mainTexture = texture;
        temp.transform.localScale = new Vector3(-.1f, .005f, -.1f * ((float)texture.height / (float)texture.width));

        //print(temp.GetComponentInChildren<Image>().transform.localScale);

        temp.transform.GetChild(0).gameObject.SetActive(false);

        //temp.GetComponentInChildren<Image>().GetComponent<RectTransform>().anchoredPosition = new Vector2(50, 50f / ((float)texture.height / (float)texture.width));

        temp.transform.GetChild(1).GetChild(0).localScale = new Vector3(1f * ((float)texture.height / (float)texture.width), 1, 1);
        temp.transform.GetChild(1).GetChild(1).localScale = new Vector3(1f * ((float)texture.height / (float)texture.width), 1, 1);
        temp.transform.GetChild(1).GetChild(2).localScale = new Vector3(1f * ((float)texture.height / (float)texture.width), 1, 1);
        temp.transform.GetChild(1).GetChild(3).localScale = new Vector3(1f * ((float)texture.height / (float)texture.width), 1, 1);
        //temp.transform.GetChild(1).GetChild(4).localScale = new Vector3(1f * ((float)texture.height / (float)texture.width), 1, 1);
        //temp.transform.GetChild(1).GetChild(5).localScale = new Vector3(1f * ((float)texture.height / (float)texture.width), 1, 1);


        temp.SetActive(false);

        PlacementSystem.instance.gameObjectToInstantiate = temp;


        PlacementSystem.instance.currentType = "IMAGE";

        PlacementSystem.instance.content = path;


        panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
        isActive = !isActive;
    }


   

    public void selectImage(int i)
    {

        PlacementSystem.instance.current.texture = Image;

        PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta = new Vector2(1, (int)(Image.height / Image.width)) * PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta.x;

        GameObject temp = Instantiate(imageToInstantiate);

        temp.GetComponent<Renderer>().material.mainTexture = texxtures[i];
        temp.transform.localScale = new Vector3(-.1f, .005f, -.1f * ((float)texxtures[i].height / (float)texxtures[i].width));

        temp.transform.GetChild(0).gameObject.SetActive(false);

        //print(temp.GetComponentInChildren<Image>().transform.localScale);


        //temp.GetComponentInChildren<Image>().transform.localScale = new Vector3(1f * ((float)texxtures[i].height / (float)texxtures[i].width), 1,1);
        //temp.GetComponentInChildren<Image>().GetComponent<RectTransform>().anchoredPosition = new Vector2(50, 50f / ((float)texxtures[i].height / (float)texxtures[i].width));

        temp.transform.GetChild(1).GetChild(0).localScale = new Vector3(1f * ((float)texxtures[i].height / (float)texxtures[i].width), 1, 1);
        temp.transform.GetChild(1).GetChild(1).localScale = new Vector3(1f * ((float)texxtures[i].height / (float)texxtures[i].width), 1, 1);
        temp.transform.GetChild(1).GetChild(2).localScale = new Vector3(1f * ((float)texxtures[i].height / (float)texxtures[i].width), 1, 1);
        temp.transform.GetChild(1).GetChild(3).localScale = new Vector3(1f * ((float)texxtures[i].height / (float)texxtures[i].width), 1, 1);
        //temp.transform.GetChild(1).GetChild(4).localScale = new Vector3(1f * ((float)texxtures[i].height / (float)texxtures[i].width), 1, 1);
        //temp.transform.GetChild(1).GetChild(5).localScale = new Vector3(1f * ((float)texxtures[i].height / (float)texxtures[i].width), 1, 1);


        temp.SetActive(false);

        PlacementSystem.instance.gameObjectToInstantiate = temp;

        PlacementSystem.instance.currentType = "IMAGE";

        PlacementSystem.instance.content = i.ToString();

        panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
        isActive = !isActive;
    }

}
