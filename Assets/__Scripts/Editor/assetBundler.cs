﻿
using UnityEditor;

public class assetBundler : Editor
{
    [MenuItem("Assets/Build AssetBundles")]
    static void buildAllAssetBundles()
    {
        BuildPipeline.BuildAssetBundles("Assets/assetBundles", BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.Android);
        //BuildPipeline.BuildAssetBundles(@"C:\Users\feres\Desktop\arSpotBundles", BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.iOS);
    }

}
