﻿using SocketIO;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using KyleDulce.SocketIo;

public class NetworkingClient : MonoBehaviour
{

    [Header("Network client")]
    [SerializeField]
    private Transform networkContainer;
    public static string pincode = "";

    public static string ClientID { get; private set; }

    public static NetworkingClient instance;

    public static Dictionary<string, GameObject> serverObjects = new Dictionary<string, GameObject>();

    public Animator forceQuit;
    string local;
    string hosted;
    public static bool isHosted;
    public static Socket s;
    bool joined = false;

    private void Awake()
    {
        instance = this;
        isHosted = true;
        local = "ws://localhost:5000/";
        hosted = "ws://snewpar.herokuapp.com/socket.io/?EIO=4&transport=websocket";
        s = SocketIo.establishSocketConnection(isHosted ? hosted : local);
        if (pincode == "")
        {
            Common.instance.load(2);
        }
        s.connect();
    }

    private void Start()
    {

        SetupEvents();
        //s.on("testEvent", call);
    }

    void Open(string d)
    {
        Debug.Log("the socket is open");
        string MyUsername = PlayerPrefs.GetString("username", "not logged in");

        registerUserName(MyUsername);

        Common.getUserInfo(out string none, out string profile);
        

        if (!joined)
        {
            //print("Connection made to server");


            bool access = PinInput.brainstorming.owner != "617ea675d03201002335b31f" ? PinInput.brainstorming.isOwner : true;
            string initJson = $"^'roomId': '{pincode}'," +
                                //$"'isOwner':{PinInput.brainstorming.isOwner}," +
                                $"'isOwner':'{access}'," +
                                $"'profile':'{profile}'," +
                                $"'username':'{MyUsername}'@";

            //"{'roomId': '" + pincode + "', 'isOwner':" + SessionManager.instance.brainstorming.isOwner.ToString() + ", 'username':'" + MyUsername + "' }"
            string roomJson = initJson.ToJSON();
            Debug.Log(roomJson);
            s.emit("joinRoom", roomJson);
            joined = true;

        }
    }

    void CloseSession(string E)
    {
        //print(E.data.ToString());
        Debug.Log("CLOSING SESSION");
        Debug.Log(E);
        if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
            PinInput.message = E;
        else
            PinInput.message = "";
        if (PinInput.brainstorming.owner != "617ea675d03201002335b31f")
        {
            SessionManager.instance.closeSession(true);
            SessionManager.isEnded = true;
        }
    }

    void Register(string E)
    {
        Debug.Log("Register  !!!!!!!!");
        forceQuit.enabled = true;

        //SoundManager.instance.Play();

        //print("______________________our room id is " + E.data["roomId"].ToString().RemoveQuotes());
        print(E);
        var data = new JSONObject(E.ToJSON());
        ClientID = data["id"].ToString().RemoveQuotes();
        //print("______________________our room id is " + E.data["roomId"].ToString().RemoveQuotes());

        //print($"_____________________our client id is {ClientID}");
    }

    void Spawn(string E)
    {
        Debug.Log("Spawn");
        Debug.Log(E);
        //print(E.data.ToString());
        print(E);
        var data = new JSONObject(E.ToJSON());
        string id = data["id"].ToString().RemoveQuotes();
        string username = data["username"].ToString().RemoveQuotes();
        string userprofile = data["profile"].ToString().RemoveQuotes();
        bool isOwner = data["isOwner"].ToString().ToUpper() == "TRUE";

        registerUserName(username);

        //print(E.data["isOwner"]);

        //GameObject temp = new GameObject($"server ID : {id}");
        //temp.transform.SetParent(networkContainer);

        //print("adding this id " + id + " to this object " + "not set yet");

        GameObject temp = SessionManager.instance.AddMember(id, username, userprofile, isOwner);

        if (temp)
        {
            print("adding this id " + id + " to this object " + temp.name);
            serverObjects.Add(id, temp);
        }

        //print($"just spawned {id}");
    }

    void SynchroniseTime(string E)
    {
        print("___________________ syncro");
        if (PinInput.brainstorming.isOwner)
        {
            string initJson = $"^'time': '{TimerBehaviour.instance.timer}'@";

            print(initJson);

            string roomJson = initJson.ToJSON();

            s.emit("synchroniseTime", roomJson);
        }
        else
        {
            //print(E.data["time"].ToString());
            print(E);
            var data = new JSONObject(E.ToJSON());
            this.Invoke(() =>
            {
                TimerBehaviour.instance.timer = float.Parse(data["time"].ToString()) + 2;
            }, 2);
        }

        string username = PlayerPrefs.GetString("username", "none");

        string json = $"^'time': '{AccountManager.instance.timeSpoken}' @".ToJSON();

        print(json);

        s.emit("timeSpoken", json);
    }
    void Disconnect(string E)
    {
        print(E);
        var data = new JSONObject(E.ToJSON());
        string id = data["id"].ToString().RemoveQuotes();

        print(id + " disconnected");

        GameObject temp = serverObjects[id];

        print(temp.name + " will be deleted");

        temp.GetComponent<memberUiBehaviour>().destroy();
        Destroy(temp);
        serverObjects.Remove(id);

        SessionManager.instance.fillResume(serverObjects.Count, -1, -1);
        TextMeshProUGUI[] texts = SessionManager.instance.members.GetComponentsInChildren<TextMeshProUGUI>();
        texts[0].text = (serverObjects.Count) + " Participants";


        //print($"{id} disconnected from server");
    }

    //private void Update()
    //{
    //    if (wsConnected != ws.IsConnected)
    //    {
    //        wsConnected = ws.IsConnected;
    //        if (wsConnected)
    //        {
    //            EmitEvent("connect");
    //        }
    //        else
    //        {
    //            EmitEvent("disconnect");
    //        }
    //    }
    //}

    private void SetupEvents()
    {
        s.on("open", Open);
        s.on("closeSession", CloseSession);
        s.on("register", Register);
        s.on("spawn", Spawn);
        s.on("synchroniseTime", SynchroniseTime);
        s.on("discoonnect", Disconnect);
    }

    public List<string> users;

    private void registerUserName(string user)
    {
        if (!users.Contains(user))
        {
            users.Add(user);
        }
    }
}
