﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using TMPro;
using Unity.VectorGraphics;
using Unity.VideoHelper;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[System.Serializable]
public class FormeRect
{
    public int x, y, width, height;

    public FormeRect(int x, int y, int width, int height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}

[System.Serializable]
public class NetworkObject
{
    public string owner;
    public string type;
    public int typeId;
    public string id;
    public Category category;

    public string ownerPath, ownerId, ownerUserName;

    public List<int> reacts = new List<int>();

    public string positionX;
    public string positionY;
    public string positionZ;

    public string rotationX;
    public string rotationY;
    public string rotationZ;
    public string rotationW;

    public string scaleX;
    public string scaleY;
    public string scaleZ;

    public FormeRect formeRect;

    public string content;

    internal void setPosition(Vector3 localPosition)
    {
        positionX = localPosition.x.ToString();
        positionY = localPosition.y.ToString();
        positionZ = localPosition.z.ToString();

    }

    internal Vector3 position()
    {
        return new Vector3(float.Parse(positionX), float.Parse(positionY), float.Parse(positionZ));
    }

    internal void setRotation(Quaternion localRotation)
    {
        rotationX = localRotation.x.ToString();
        rotationY = localRotation.y.ToString();
        rotationZ = localRotation.z.ToString();
        rotationW = localRotation.w.ToString();

    }

    internal Quaternion rotation()
    {
        return new Quaternion(float.Parse(rotationX), float.Parse(rotationY), float.Parse(rotationZ), float.Parse(rotationW));
    }

    internal void setScale(Vector3 localScale)
    {
        scaleX = localScale.x.ToString();
        scaleY = localScale.y.ToString();
        scaleZ = localScale.z.ToString();

    }

    internal Vector3 scale()
    {
        return new Vector3(float.Parse(scaleX), float.Parse(scaleY), float.Parse(scaleZ));
    }
}

[System.Serializable]
public class NetObjects
{
    public Dictionary<string, NetworkObject> list = new Dictionary<string, NetworkObject>();

}

public class networkPosition : Singleton<networkPosition>
{

    public Texture2D formes;
    //public NetObjects lastSendToServer;

    public Button sondage;

    public Texture2D t2d;

    GameObject FindInActiveObjectByName(string name)
    {
        Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].hideFlags == HideFlags.None)
            {
                if (objs[i].name == name)
                {
                    return objs[i].gameObject;
                }
            }
        }
        return null;
    }

    public void generateSprite(string path, Action<Sprite> callback)
    {
        if (!path.Contains("avataaars"))
        {
            //print(path);
            this.fetchImage(path, (texture) =>
            {

                callback(Common.SpriteFromTexture2D(texture));

            });

        }
        else
        {
            this.getText(path, response =>
            {
                var sprite = Common.instance.getSvgImage(response);

                callback(sprite);

            });
        }
    }

    void Start()
    {

        string username = PlayerPrefs.GetString("username", "none");

        VotingBehaviour.instance.submissions.Add(username, new Submission(username));

        InvokeRepeating("UpdatePos", 0, 1f);

        NetworkingClient.s.emit("startExperience", "");

        NetworkingClient.s.on("giveIdea", (E) =>
        {
            print(E);
            var data = new JSONObject(E.ToJSON());
            ideapoolBehaviour.instance.recalculateIdeas();

            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

            Idea idea = JsonUtility.FromJson<Idea>(data.ToString());

            //print("________________________________________________________________");

            //string x = "0.55565";

            //print("position directly : " + x);

            //print("position to string : " + x.ToString());

            //print("position to string replaced to , : " + x.ToString().Replace('.', ','));

            //print("position parsed to float with replace : " + float.Parse(x.ToString().Replace('.', ',')));

            //print("position parsed to float no replace " + float.Parse(x.ToString()));

            //idea.netObj.positionX = float.Parse(E.data["netObj"]["positionX"].ToString());
            //idea.netObj.positionY = float.Parse(E.data["netObj"]["positionY"].ToString());
            //idea.netObj.positionZ = float.Parse(E.data["netObj"]["positionZ"].ToString());

            //idea.netObj.rotationX = float.Parse(E.data["netObj"]["rotationX"].ToString());
            //idea.netObj.rotationY = float.Parse(E.data["netObj"]["rotationY"].ToString());
            //idea.netObj.rotationZ = float.Parse(E.data["netObj"]["rotationZ"].ToString());
            //idea.netObj.rotationW = float.Parse(E.data["netObj"]["rotationW"].ToString());

            //idea.netObj.scaleX = float.Parse(E.data["netObj"]["scaleX"].ToString());
            //idea.netObj.scaleY = float.Parse(E.data["netObj"]["scaleY"].ToString());
            //idea.netObj.scaleZ = float.Parse(E.data["netObj"]["scaleZ"].ToString());

            //print($"i have received an idea : {idea.id}");

            if (!idea.isFirstTime)
            {
                if (idea.username == username) return;
            }


            NetworkObject netObj = idea.netObj;


            print($"i have received an netObj : {netObj.id} ::: {netObj.type}");
            //print(E.data.ToString());
            //GameObject temp = new GameObject(netObj.id);
            if (netObj.type.ToUpper().Contains("FORME"))
            {
                GameObject temp = Instantiate(FormesManager.instance.gameObjectToInstantiate, netObj.position(), netObj.rotation(), PlacementSystem.instance.parent.transform);

                print($"i have received an position : {netObj.id} ::: {netObj.type}");

                temp.name = netObj.id;

                temp.transform.localPosition = netObj.position();
                temp.transform.localRotation = netObj.rotation();

                Texture2D dst = new Texture2D((int)netObj.formeRect.width, (int)netObj.formeRect.height);
                var pixels = formes.GetPixels(netObj.formeRect.x, netObj.formeRect.y, netObj.formeRect.width, netObj.formeRect.height);
                dst.SetPixels(pixels);
                dst.Apply();



                temp.GetComponentInChildren<Image>().transform.localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);



               generateSprite(netObj.ownerPath, sp =>
               {
                   temp.GetComponentInChildren<SVGImage>().sprite = sp;
               });

                // NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<SVGImage>().sprite;
                //temp.GetComponentInChildren<RawImage>().fit(101, 101);

                temp.transform.GetChild(1).GetChild(0).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
                temp.transform.GetChild(1).GetChild(1).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
                temp.transform.GetChild(1).GetChild(2).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
                temp.transform.GetChild(1).GetChild(3).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
                //temp.transform.GetChild(1).GetChild(4).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
                //temp.transform.GetChild(1).GetChild(5).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);

                t2d = dst;

                temp.GetComponent<Renderer>().material.mainTexture = dst;
                temp.transform.localScale = new Vector3(.1f, .005f, .1f * ((float)dst.height / (float)dst.width));
                temp.transform.localScale = netObj.scale();
                temp.SetActive(true);

            }
            else if (netObj.type.ToUpper().Contains("TEXT"))
            {

                GameObject temp = Instantiate(TextManager.instance.gameObjectToInstantiate, netObj.position(), netObj.rotation(), PlacementSystem.instance.parent.transform);
                temp.name = netObj.id;
                temp.transform.localScale = netObj.scale();
                temp.transform.localPosition = netObj.position();
                temp.transform.localRotation = netObj.rotation();
                temp.GetComponentInChildren<TextMeshProUGUI>(true).text = netObj.content;

                //temp.GetComponentInChildren<RawImage>().texture = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<RawImage>().texture;
                //temp.GetComponentInChildren<RawImage>().fit(101, 101);
                //temp.GetComponentInChildren<SVGImage>().sprite = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<SVGImage>().sprite;
                generateSprite(netObj.ownerPath, sp =>
                {
                    temp.GetComponentInChildren<SVGImage>().sprite = sp;
                });

                temp.SetActive(true);

            }else if (netObj.type.ToUpper().Contains("IDEA"))
            {

                GameObject temp = Instantiate(TextManager.instance.gameObjectToInstantiate, netObj.position(), netObj.rotation(), PlacementSystem.instance.parent.transform);
                temp.name = netObj.id;
                temp.transform.localScale = netObj.scale();
                temp.transform.localPosition = netObj.position();
                temp.transform.localRotation = netObj.rotation();
                temp.GetComponentInChildren<TextMeshProUGUI>(true).text = netObj.content;

                //temp.GetComponentInChildren<RawImage>().texture = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<RawImage>().texture;
                //temp.GetComponentInChildren<RawImage>().fit(101, 101);
                //temp.GetComponentInChildren<SVGImage>().sprite = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<SVGImage>().sprite;
                //generateSprite(netObj.ownerPath, sp =>
                //{
                //    temp.GetComponentInChildren<SVGImage>().sprite = sp;
                //});

                temp.SetActive(true);

            }
            else if (netObj.type.ToUpper().Contains("POST"))
            {
                GameObject temp = Instantiate(PostManager.instance.gameObjectToInstantiate, netObj.position(), netObj.rotation(), PlacementSystem.instance.parent.transform);
                temp.name = netObj.id;
                temp.transform.localScale = netObj.scale();
                temp.transform.localPosition = netObj.position();
                temp.transform.localRotation = netObj.rotation();
                temp.GetComponentInChildren<TextMeshProUGUI>(true).text = netObj.content;

                //temp.GetComponentInChildren<RawImage>().texture = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<RawImage>().texture;
                //temp.GetComponentInChildren<RawImage>().fit(101, 101);
                //temp.GetComponentInChildren<SVGImage>().sprite = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<SVGImage>().sprite;

                generateSprite(netObj.ownerPath, sp =>
                {
                    temp.GetComponentInChildren<SVGImage>().sprite = sp;
                });

                temp.SetActive(true);

            }
            //else if (netObj.type.ToUpper().Contains("IMAGE"))
            //{
            //    GameObject temp = Instantiate(MediaManager.instance.imageToInstantiate, netObj.position(), netObj.rotation(), PlacementSystem.instance.parent.transform);
            //    temp.name = netObj.id;

            //    temp.transform.localPosition = netObj.position();
            //    temp.transform.localRotation = netObj.rotation();
            //    temp.SetActive(false);

            //    if (int.TryParse(netObj.content, out int i))
            //    {
            //        Texture2D dst = MediaManager.instance.texxtures[i];

            //        temp.GetComponent<Renderer>().material.mainTexture = dst;
            //        temp.transform.localScale = new Vector3(.1f, .005f, .1f * ((float)dst.height / (float)dst.width));

            //        temp.GetComponentInChildren<Image>().transform.localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);

            //        //temp.GetComponentInChildren<RawImage>().texture = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<RawImage>().texture;
            //        //temp.GetComponentInChildren<RawImage>().fit(101, 101);
            //        //temp.GetComponentInChildren<SVGImage>().sprite = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<SVGImage>().sprite;

            //        generateSprite(netObj.ownerPath, sp =>
            //        {
            //            temp.GetComponentInChildren<SVGImage>().sprite = sp;
            //        });

            //        temp.transform.GetChild(1).GetChild(0).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //        temp.transform.GetChild(1).GetChild(1).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //        temp.transform.GetChild(1).GetChild(2).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //        temp.transform.GetChild(1).GetChild(3).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //        //temp.transform.GetChild(1).GetChild(4).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //        //temp.transform.GetChild(1).GetChild(5).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);

            //        temp.transform.localScale = netObj.scale();
            //        temp.SetActive(true);
            //    }
            //    else
            //    {
            //        this.fetchImage(netObj.content, (dst) =>
            //        {
            //            temp.GetComponent<Renderer>().material.mainTexture = dst;
            //            temp.transform.localScale = new Vector3(.1f, .005f, .1f * ((float)dst.height / (float)dst.width));

            //            temp.GetComponentInChildren<Image>().transform.localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);

            //            //temp.GetComponentInChildren<RawImage>().texture = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<RawImage>().texture;
            //            //temp.GetComponentInChildren<RawImage>().fit(101, 101);
            //            //temp.GetComponentInChildren<SVGImage>().sprite = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<SVGImage>().sprite;

            //            generateSprite(netObj.ownerPath, sp =>
            //            {
            //                temp.GetComponentInChildren<SVGImage>().sprite = sp;
            //            });

            //            temp.transform.GetChild(1).GetChild(0).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //            temp.transform.GetChild(1).GetChild(1).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //            temp.transform.GetChild(1).GetChild(2).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //            temp.transform.GetChild(1).GetChild(3).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //            //temp.transform.GetChild(1).GetChild(4).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);
            //            //temp.transform.GetChild(1).GetChild(5).localScale = new Vector3(1f * ((float)dst.height / (float)dst.width), 1, 1);

            //            temp.transform.localScale = netObj.scale();
            //            temp.SetActive(true);
            //        });
            //    }


            //}
            //else if (netObj.type.ToUpper().Contains("DRAW"))
            //{
            //    GameObject temp = Instantiate(MediaManager.instance.imageToInstantiate, netObj.position(), netObj.rotation(), PlacementSystem.instance.parent.transform);
            //    temp.name = netObj.id;

            //    temp.transform.localPosition = netObj.position();
            //    temp.transform.localRotation = netObj.rotation();
            //    temp.SetActive(false);

            //    this.fetchImage(netObj.content, (dst) =>
            //    {
            //        temp.GetComponent<Renderer>().material.mainTexture = dst;
            //        temp.transform.localScale = new Vector3(.1f, .005f, .1f * ((float)dst.height / (float)dst.width));

            //        //temp.GetComponentInChildren<RawImage>().texture = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<RawImage>().texture;
            //        //temp.GetComponentInChildren<RawImage>().fit(101, 101);
            //        //temp.GetComponentInChildren<SVGImage>().sprite = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<SVGImage>().sprite;

            //        generateSprite(netObj.ownerPath, sp =>
            //        {
            //            temp.GetComponentInChildren<SVGImage>().sprite = sp;
            //        });

            //        temp.transform.localScale = netObj.scale();
            //        temp.SetActive(true);
            //    });


            //}
            //else if (netObj.type.ToUpper().Contains("VIDEO"))
            //{
            //    GameObject temp = Instantiate(MediaManager.instance.videoToInstantiate, netObj.position(), netObj.rotation(), PlacementSystem.instance.parent.transform);
            //    temp.name = netObj.id;

            //    temp.transform.localPosition = netObj.position();
            //    temp.transform.localRotation = netObj.rotation();
            //    temp.SetActive(false);

            //    //temp.GetComponentInChildren<RawImage>().texture = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<RawImage>().texture;
            //    //temp.GetComponentInChildren<RawImage>().fit(101, 101);
            //    //temp.GetComponentInChildren<SVGImage>().sprite = NetworkingClient.instance.serverObjects[netObj.owner].GetComponentInChildren<SVGImage>().sprite;

            //    generateSprite(netObj.ownerPath, sp =>
            //    {
            //        temp.GetComponentInChildren<SVGImage>().sprite = sp;
            //    });

            //    //VideoPlayer vp = temp.GetComponent<VideoPlayer>();

            //    //vp.url = netObj.content;

            //    //print(vp.url);


            //    //vp.prepareCompleted += (VideoPlayer source) =>
            //    //{
            //    //    Debug.Log("dimensions " + source.texture.width + " x " + source.texture.height); // do with these dimensions as you wish

            //    //    source.transform.localScale = new Vector3(.1f, .005f, .1f * ((float)source.texture.height / (float)source.texture.width));

            //    //};

            //    //vp.Prepare();

            //    VideoController vc = temp.GetComponentInChildren<VideoController>();

            //    vc.PrepareForUrl(netObj.content);

            //    //temp.transform.localScale = new Vector3(.1f, .005f, .1f * ((float)vidTex.height / (float)vidTex.width));

            //    temp.transform.localScale = netObj.scale();
            //    temp.SetActive(true);

            //}



        });

        NetworkingClient.s.on("moveItem", (E) =>
        {
            print(E);
            var data = new JSONObject(E.ToJSON());
            ideapoolBehaviour.instance.recalculateIdeas();
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

            Idea idea = JsonUtility.FromJson<Idea>(data.ToString());

            //print("________________________________________________________________");

            //print("position directly : " + E.data["netObj"]["positionX"]);

            //print("position to string : " + E.data["netObj"]["positionX"].ToString());

            //print("position to string replaced to , : " + E.data["netObj"]["positionX"].ToString().Replace('.', ','));

            //print("position parsed to float with replace : " + float.Parse(E.data["netObj"]["positionX"].ToString().Replace('.', ',')));

            //print("position parsed to float no replace " + float.Parse(E.data["netObj"]["positionX"].ToString()));

            //idea.netObj.positionX = float.Parse(E.data["netObj"]["positionX"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);
            //idea.netObj.positionY = float.Parse(E.data["netObj"]["positionY"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);
            //idea.netObj.positionZ = float.Parse(E.data["netObj"]["positionZ"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);

            //idea.netObj.rotationX = float.Parse(E.data["netObj"]["rotationX"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);
            //idea.netObj.rotationY = float.Parse(E.data["netObj"]["rotationY"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);
            //idea.netObj.rotationZ = float.Parse(E.data["netObj"]["rotationZ"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);
            //idea.netObj.rotationW = float.Parse(E.data["netObj"]["rotationW"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);

            //idea.netObj.scaleX = float.Parse(E.data["netObj"]["scaleX"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);
            //idea.netObj.scaleY = float.Parse(E.data["netObj"]["scaleY"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);
            //idea.netObj.scaleZ = float.Parse(E.data["netObj"]["scaleZ"].ToString().Replace('.', ','), CultureInfo.CurrentCulture);


            NetworkObject netObj = idea.netObj;

            GameObject temp = PlacementSystem.instance.parent.transform.Find(idea.id).gameObject;

            //GameObject temp = GameObject.Find(netObj.id);

            if (netObj.type == "POST" || netObj.type == "TEXT")
            {
                temp.GetComponentInChildren<TextMeshProUGUI>(true).text = netObj.content;
                SessionManager.instance.RefreshIdeas();
            }

            string id = "idea_" + idea.id;

            IdeaUiBehaviour idUB = FindInActiveObjectByName(id).GetComponent<IdeaUiBehaviour>();

            idUB.Idea.netObj = idea.netObj;

            goToTarget t;

            if (!temp.TryGetComponent(out t))
            {
                t = temp.AddComponent<goToTarget>();
            }

            t.targetPos = netObj.position();
            t.targetRot = netObj.rotation();
            t.targetScale = netObj.scale();

        });


        NetworkingClient.s.on("removeItem", (E) =>
        {
            print(E);
            var data = new JSONObject(E.ToJSON());
            Idea idea = JsonUtility.FromJson<Idea>(data.ToString());

            NetworkObject netObj = idea.netObj;

            GameObject temp = PlacementSystem.instance.parent.transform.Find(idea.id).gameObject;

            Destroy(temp);

            string id = "idea_" + idea.id;

            IdeaUiBehaviour idUB = FindInActiveObjectByName(id).GetComponent<IdeaUiBehaviour>();

            Destroy(idUB.gameObject);

            //idUB.Idea.netObj.category = idea.netObj.category;

            idea.destroy();

            SessionManager.instance.RefreshIdeas(true);

            if (!SessionManager.instance.brainstorming.isPoll)
            {
                ideapoolBehaviour.instance.recalculateIdeas();
            }

            //SessionManager.instance.RefreshIdeas();

            //NetworkObject netObj = JsonUtility.FromJson<NetworkObject>(E.data.ToString());

            //Destroy(GameObject.Find(netObj.id));

        });

        NetworkingClient.s.on("reactItem", (E) =>
        {
            print(E);
            var data = new JSONObject(E.ToJSON());
            print("___________________________" + data.ToString());

            Idea idea = JsonUtility.FromJson<Idea>(data.ToString());

            NetworkObject netObj = idea.netObj;

            string id = "idea_" + idea.id;

            IdeaUiBehaviour idUB = FindInActiveObjectByName(id).GetComponent<IdeaUiBehaviour>();

            idUB.Idea.netObj.reacts = idea.netObj.reacts;
            idUB.Idea.likes = idea.likes;
            idUB.Idea.dislikes = idea.dislikes;

            GameObject temp = PlacementSystem.instance.parent.transform.Find(idea.id).gameObject;

            itemEvents t;

            if (!temp.TryGetComponent(out t))
            {
                t = temp.GetComponentInChildren<itemEvents>();
            }

            t.Reacts = netObj.reacts;

            SessionManager.instance.RefreshIdeas();

            ideapoolBehaviour.instance.recalculateIdeas();

            //SessionManager.instance.RefreshIdeas();

        });

        NetworkingClient.s.on("assignCategory", E =>
        {
            //print("___________________________" + E.data.ToString());
            print(E);
            var data = new JSONObject(E.ToJSON());
            Idea idea = JsonUtility.FromJson<Idea>(data.ToString());

            NetworkObject netObj = idea.netObj;

            GameObject temp = PlacementSystem.instance.parent.transform.Find(idea.id).gameObject;

            string id = "idea_" + idea.id;

            IdeaUiBehaviour idUB = FindInActiveObjectByName(id).GetComponent<IdeaUiBehaviour>();

            idUB.Idea.netObj.category = idea.netObj.category;

            itemEvents t;

            if (!temp.TryGetComponent(out t))
            {
                t = temp.GetComponentInChildren<itemEvents>();
            }

            t.category = netObj.category.name;

        });


    }

    public void addItem(NetworkObject item)
    {
        //print("______________________________________________________________" + JsonUtility.ToJson(item));
        //NetworkingClient.instance.Emit("addItem", new JSONObject(JsonUtility.ToJson(item)));
        //if(item.type == "TEXT" || item.type == "POST")
        //{
        //print("_____________________________________________________________________________" + item.type);
        VotingBehaviour.instance.giveIdea(item);
        //}
        //lastSendToServer.list.Add(id, item);
    }

    void UpdatePos()
    {
        //NetObjects temp = new NetObjects();

        //foreach (var netobject in netObjects.list)
        //{
        //    if (netobject.owner == NetworkingClient.ClientID)
        //    {
        //        temp.list.Add(netobject);
        //    }
        //}

        string username = PlayerPrefs.GetString("username", "none");

        foreach (Idea idea in VotingBehaviour.instance.submissions[username].ideas)
        {
            //print(idea.id);
            GameObject item = PlacementSystem.instance.parent.transform.Find(idea.id).gameObject;

            //print(item.name);

            NetworkObject netObj = idea.netObj;

            if (
                item.transform.localPosition != netObj.position() ||
                item.transform.localRotation != netObj.rotation() ||
                item.transform.localScale != netObj.scale() ||
                (
                (netObj.type == "POST" || netObj.type == "TEXT") &&
                item.GetComponentInChildren<TextMeshProUGUI>().text != netObj.content)
                )
            {
                netObj.setPosition(item.transform.localPosition);
                netObj.setRotation(item.transform.localRotation);
                netObj.setScale(item.transform.localScale);

                netObj.content = (netObj.type == "POST" || netObj.type == "TEXT") ? item.GetComponentInChildren<TextMeshProUGUI>().text : netObj.content;

                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

                NetworkingClient.s.emit("moveItem", JsonUtility.ToJson(idea));

            }
        }

        //    foreach (Transform item in PlacementSystem.instance.parent.transform)
        //    {
        //        if (item)
        //        {
        //            if (item.name.EndsWith(NetworkingClient.ClientID))
        //            {
        //                if(lastSendToServer.list.TryGetValue(item.name, out NetworkObject netObj))
        //                {
        //                    if (item.localPosition != netObj.position() || item.localRotation != netObj.rotation() || item.localScale != netObj.scale() || ((netObj.type == "POST" || netObj.type == "TEXT") && item.GetComponentInChildren<TextMeshProUGUI>().text != netObj.content))
        //                    {
        //                        netObj.setPosition(item.localPosition);
        //                        netObj.setRotation(item.localRotation);
        //                        netObj.setScale(item.localScale);

        //                        netObj.content = (netObj.type == "POST" || netObj.type == "TEXT") ? item.GetComponentInChildren<TextMeshProUGUI>().text : netObj.content;

        //                        NetworkingClient.instance.Emit("moveItem", new JSONObject(JsonUtility.ToJson(netObj)));

        //                    }
        //                }
        //                else
        //                {
        //                    print(item.name + " not found");
        //                }

        //            }
        //        }
        //    }

    }
}
