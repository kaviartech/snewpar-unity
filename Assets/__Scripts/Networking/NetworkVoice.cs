﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkVoice : Singleton<NetworkVoice>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    
    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/



    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        NetworkingClient.s.on("requestSpeech", E =>
        {
            print(E);
            var data = new JSONObject(E.ToJSON());
            string id = data["id"].ToString().RemoveQuotes();

            if (SessionManager.instance.brainstorming.isOwner)
            {
                Common.alert(LanguageManager.isFR ? $"Souhaitez-vous donner la permission de parler à {data["username"].ToString().RemoveQuotes()}" : $"Would you like to give permission to talk to {data["username"].ToString().RemoveQuotes()}", () => {
                    giveSpeech(id);
                });
            }

            NetworkingClient.serverObjects[id].GetComponentInChildren<memberUiBehaviour>().request();

        });

        NetworkingClient.s.on("giveSpeech", E =>
        {
            print(E);
            var data = new JSONObject(E.ToJSON());
            string id = data["id"].ToString().RemoveQuotes();

            if(id == NetworkingClient.ClientID)
            {
                Common.alert(LanguageManager.isFR ? "Vous avez reçu l'autorisation d'activer le micro, faites attention, ce n'est viable qu'une seule fois, puis vous devez redemander" : "You have been given permission to activate the microphone, be careful, this is only viable once, then you have to ask again");
            }

            AccountManager.instance.ableToSpeak = true;
        });

        NetworkingClient.s.on("toggleMute", E =>
        {
            print(E);
            var data = new JSONObject(E.ToJSON());
            string id = data["id"].ToString().RemoveQuotes();
            bool mute = data["mute"].ToString().ToUpper() == "TRUE";
            print(mute);

            NetworkingClient.serverObjects[id].GetComponentInChildren<memberUiBehaviour>().toggleMute(mute);
        });
    }

    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/

    void Update()
    {
        
    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void requestSpeech()
    {
        NetworkingClient.s.emit("requestSpeech","");
    }

    public void giveSpeech(string id)
    {
        string json = $"^'id':'{id}'@".ToJSON();

        NetworkingClient.s.emit("giveSpeech",json);
    }

    public void toggleMute(bool state)
    {
        string json = $"^'mute':'{state}'@".ToJSON();

        NetworkingClient.s.emit("toggleMute",json);
    }

}
