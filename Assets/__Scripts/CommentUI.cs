﻿/*

*/

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CommentUI : MonoBehaviour
{
    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/



    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        
    }

    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void set(string user, string comment, Color color)
    {
        GetComponent<Image>().color = Color.white;

        GetComponentsInChildren<TextMeshProUGUI>(true)[0].text = "@" + user;
        GetComponentsInChildren<TextMeshProUGUI>(true)[1].text = comment;

    }

}
