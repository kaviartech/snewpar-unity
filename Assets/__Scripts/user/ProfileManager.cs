using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.VectorGraphics;
using UnityEngine.Networking;
using TMPro;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using Lean.Gui;
using System.Text.RegularExpressions;


public class ProfileManager : MonoBehaviour
{
	public SVGImage profilePicture;
	public TMP_InputField firstNameText;
	public TMP_InputField lastNameText;
	public TMP_InputField passwordText;
	public TMP_InputField newPasswordText;
	public byte[] profilePictureBytes;
	string token;
	// Start is called before the first frame update
	IEnumerator Start()
    {
		
		profilePicture.sprite = AuthManager.profile;
		token = PlayerPrefs.GetString("token");
		using (UnityWebRequest request = UnityWebRequest.Get(Common.Baseurl + "auth/profile/get"))
		{
			Debug.Log(token);
			request.SetRequestHeader("Access-Control-Allow-Credentials", "true");
			request.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
			request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
			request.SetRequestHeader("Access-Control-Allow-Origin", "*");
			request.SetRequestHeader("token", token);
			
			yield return request.SendWebRequest();

			if (!request.isNetworkError && !request.isHttpError)
			{
				string result = request.downloadHandler.text;
				Debug.Log(result);
				UserInfos userInfos = JsonConvert.DeserializeObject<UserInfos>(result);
				lastNameText.text = userInfos.LastName;
				firstNameText.text = userInfos.FirstName;
			}
			else
			{
				Debug.Log(request.error);
			}
		}
		passwordText.text = PlayerPrefs.GetString("pwd");
		
	}
	public void PickImage()
	{
		NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
		{
			Debug.Log("Image path: " + path);
			if (path != null)
			{
				// Create Texture from selected image
				Texture2D texture = NativeGallery.LoadImageAtPath(path, 15000,false);
				if (texture == null)
				{
					Debug.Log("Couldn't load texture from " + path);
					return;
				}
				profilePicture.sprite = Common.SpriteFromTexture2D(texture);
				profilePictureBytes = texture.EncodeToPNG();
				Debug.Log(profilePictureBytes.Length);
			}
		});

		Debug.Log("Permission result: " + permission);
	}

	//Back  to Room
	public void Back()
    {
		Common.instance.load(2);
    }
	// Update is called once per frame
	void Update()
    {
        
    }

	public void  UpdateProfile()
    {
		StartCoroutine(SendUpdateProfileRequest());
    }

	public IEnumerator SendUpdateProfileRequest()
    {
		WWWForm form = new WWWForm();
		if (profilePictureBytes.Length != 0)
			form.AddBinaryData("image", profilePictureBytes);
		if (Regex.IsMatch(lastNameText.text, @"^[a-zA-Z ]{3,16}$"))
			form.AddField("lastName", lastNameText.text);
		if (Regex.IsMatch(firstNameText.text, @"^[a-zA-Z ]{3,16}$"))
			form.AddField("firstName", firstNameText.text);
		if (passwordText.text != PlayerPrefs.GetString("pwd"))
		{
			Common.alert(LanguageManager.isFR ? "Votre mot de passe est incorrect" : "Your password is incorrect");
        }
        else
        {
			if (newPasswordText.text.Length > 2)
			{
				form.AddField("password", newPasswordText.text);
				PlayerPrefs.SetString("pwd", newPasswordText.text);
				passwordText.text = newPasswordText.text;
				newPasswordText.text = "";

			}
			else
			{
				form.AddField("password", PlayerPrefs.GetString("pwd"));
			}

			form.AddField("token", token);

			using (UnityWebRequest request = UnityWebRequest.Post(Common.Baseurl + "auth/profile/edit", form))
			{
				request.SetRequestHeader("Access-Control-Allow-Credentials", "true");
				request.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
				request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
				request.SetRequestHeader("Access-Control-Allow-Origin", "*");
				yield return request.SendWebRequest();

				if (!request.isNetworkError && !request.isHttpError)
				{
					string result = request.downloadHandler.text;
					Debug.Log(result);
					//Common.instance.load(2);
					FindObjectOfType<LeanPulse>().Pulse();
					AuthManager.profile = profilePicture.sprite;
				}
				else
				{
					Debug.Log(request.downloadHandler.text);
					Common.alert(LanguageManager.isFR ? "Erreur de sauvegarde" : "Save Error");
				}
			}
		}
		
	}

	public void DeleteProfile()
	{
		Common.alertYN(LanguageManager.isFR ? "Voulez-vous vraiment supprimer votre compte?" : "Are you sure you want to delete your account?", () =>
		{
			StartCoroutine(SendDeleteProfilRequest());
		}, () => { });
	}

	public IEnumerator SendDeleteProfilRequest()
	{
		WWWForm form = new WWWForm();
		
		form.AddField("token", token);


		using (UnityWebRequest request = UnityWebRequest.Post(Common.Baseurl + "auth/profile/delete", form))
		{
			request.SetRequestHeader("Access-Control-Allow-Credentials", "true");
			request.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
			request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
			request.SetRequestHeader("Access-Control-Allow-Origin", "*");

			yield return request.SendWebRequest();

			if (!request.isNetworkError && !request.isHttpError)
			{

				string result = request.downloadHandler.text;
				Debug.Log(result);
				//AuthManager.profile = profilePicture.sprite;
				Common.alert(LanguageManager.isFR ? "Profile supprimé avec Succés" : "Profil deleted successfully",()=> {
					PlayerPrefs.DeleteAll();
					Common.instance.load(0);
				});
			}
			else
			{
				Debug.Log(request.downloadHandler.text);
				Common.alert(LanguageManager.isFR ? "Erreur de suppression" : "Delete Error");
			}
		}
	}
	public partial class UserInfos
	{
		[JsonProperty("success")]

		public bool Success { get; set; }

		[JsonProperty("medias")]
		public Uri[] Medias { get; set; }

		[JsonProperty("isActive")]
		public bool IsActive { get; set; }

		[JsonProperty("JoinedTeams")]
		public object[] JoinedTeams { get; set; }

		[JsonProperty("Deactivated")]
		public bool Deactivated { get; set; }

		[JsonProperty("_id")]
		public string Id { get; set; }

		[JsonProperty("firstName")]
		public string FirstName { get; set; }

		[JsonProperty("lastName")]
		public string LastName { get; set; }

		[JsonProperty("username")]
		public string Username { get; set; }

		[JsonProperty("email")]
		public string Email { get; set; }

		[JsonProperty("path")]
		public Uri Path { get; set; }

		[JsonProperty("createdAt")]
		public DateTimeOffset CreatedAt { get; set; }

		[JsonProperty("updatedAt")]
		public DateTimeOffset UpdatedAt { get; set; }

		[JsonProperty("__v")]
		public long V { get; set; }

		[JsonProperty("isAdmin")]
		public bool IsAdmin { get; set; }

		[JsonProperty("registrationToken")]
		public string RegistrationToken { get; set; }

		[JsonProperty("Role")]
		public string Role { get; set; }

		[JsonProperty("OwnedTeams")]
		public OwnedTeams OwnedTeams { get; set; }
	}

	public partial class OwnedTeams
	{
		[JsonProperty("_id")]
		public string Id { get; set; }

		[JsonProperty("participants")]
		public Participant[] Participants { get; set; }
	}

	public partial class Participant
	{
		[JsonProperty("email")]
		public string Email { get; set; }

		[JsonProperty("isAccepted")]
		public bool IsAccepted { get; set; }
	}
}
