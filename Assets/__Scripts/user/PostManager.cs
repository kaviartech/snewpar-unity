﻿using TMPro;
using UnityEngine;

public class PostManager : Singleton<PostManager>
{
    public GameObject panel;
    public TMP_InputField input;
    public bool isActive;
    public GameObject help;
    private void Start()
    {
        //this.transform.SetParent(GameObject.Find("Meeting(clone)").transform.Find("MeetingArea").transform);
    }

    public void toggleActive(bool resetIsEditing = false)
    {
        Common.deactivateAll();
        Gestures.instance.GesturesReady = false; //isActive;
        panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");

        if (!resetIsEditing)
        {
            isEditing = false;
        }

        isActive = !isActive;
    }

    public void deactivate()
    {
        Gestures.instance.GesturesReady = false; //isActive;
        panel.GetComponent<Animator>().Play("hide");
        isActive = false;
    }

    public GameObject gameObjectToInstantiate;

    private string s;

    public Texture2D textImage;

    public string S { get => s; set => s = value; }

    public bool isEditing = false;

    public void select()
    {

        if (input.text == "")
        {
            Common.alert(LanguageManager.isFR ? "Vous ne pouvez pas poser un Post-it vide." : "You cannot place an empty Post-it.");
            return;
        }
       
        if (!isEditing)
        {

            //this.transform.SetParent(GameObject.Find("Meeting(clone)").transform.Find("MeetingArea").transform);
            PlacementSystem.instance.current.texture = textImage;
            PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta = new Vector2(1, (int)(textImage.height / textImage.width)) * PlacementSystem.instance.current.GetComponent<RectTransform>().sizeDelta.x;

            GameObject temp = Instantiate(gameObjectToInstantiate);

            temp.GetComponentInChildren<TextMeshProUGUI>(true).text = s;

            temp.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);


            temp.SetActive(false);
            
            PlacementSystem.instance.gameObjectToInstantiate = temp;

            PlacementSystem.instance.currentType = "POST";
            PlacementSystem.instance.content = s;

            panel.GetComponentInChildren<TMP_InputField>().text = "";

            panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
            isActive = !isActive;
            help.SetActive(true);
        }
        else
        {

            PlacementSystem.instance.spawnedObject.GetComponentInChildren<TextMeshProUGUI>(true).text = S;

            panel.GetComponentInChildren<TMP_InputField>().text = "";

            panel.GetComponent<Animator>().Play(isActive ? "hide" : "show");
            isActive = !isActive;

            isEditing = false;
        }
    }
}
