﻿/*

*/

using Byn.Unity.Examples;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class AccountManager : Singleton<AccountManager>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public GameObject music, sound, micro;

    public bool ableToSpeak = false;

    public float timeSpoken = 0;


    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/
    public static bool once;

    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/

    bool isShown = false;
    bool isMusic = true;
    bool isSound = true;
    bool isMic = true;
    int isShownCountDown =  0; 

    

    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    public override void Awake()
    {
        base.Awake();
        
    }

    void Start()
    {
        if (PinInput.brainstorming.isOwner || PinInput.brainstorming.mode == 1)
        {
            ableToSpeak = true;
        }
        else
        {
            this.Invoke(() => toggleMicro(),1);
            toggleMenu();
        }

        InvokeRepeating("CustomUpdate", 0, 1);
        
    }

    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/

    void CustomUpdate()
    {
        timeSpoken += isMic ? 1 : 0;
        isShownCountDown += 1;
        if (isShownCountDown == 10)
        {
            if (isShown)
            {
                toggleMenu();
            }

        }
    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/


    public void toggleMenu()
    {
        isShown = !isShown;
        GetComponent<Animator>().Play(isShown ? "show" : "hide");
        isShownCountDown = 0;
    }

   
    public void toggleMicro()
    {

        //if(!isMic && ableToSpeak)
        if (!isMic)
        {
            //mute
            NetworkPoll.instance.voting.transform.parent.GetComponentsInChildren<Animator>()[2].Play("deselect");
            isMic = !isMic;
            micro.SetActive(!isMic);
            ConferenceApp.instance.toggleMute(!isMic);

            NetworkVoice.instance.toggleMute(!isMic);
            //toggleMenu();
        }
        //else if (isMic)
        else
        {
            NetworkPoll.instance.voting.transform.parent.GetComponentsInChildren<Animator>()[2].Play("select");
            //talk
            isMic = !isMic;
            micro.SetActive(!isMic);
            ConferenceApp.instance.toggleMute(!isMic);

            NetworkVoice.instance.toggleMute(!isMic);
            //toggleMenu();

            if (!PinInput.brainstorming.isOwner && PinInput.brainstorming.mode != 1)
            {
                ableToSpeak = false;
            }

        }
        //else
        //{
        //    Common.alert(LanguageManager.isFR ? "Souhaitez-vous demander l'autorisation de parole à l'animateur ?" : "Would you like to ask permission to speak from the host ?", () =>
        //      {
        //          NetworkVoice.instance.requestSpeech();
        //      });
        //}

        //SoundManager.instance.toggleSound();
    }

    public void toggleIdeaPool()
    {
        
        if (!SessionManager.instance.brainstorming.isPoll)
        {
            //if (SessionManager.instance.brainstorming.isOwner)
            //{
                //VotingBehaviour.instance.transform.GetChild(1).GetComponent<Animator>().Play("show");

            VotingBehaviour.instance.ideaPanel.SetActive(!VotingBehaviour.instance.ideaPanel.activeInHierarchy);


            VotingBehaviour.instance.ideaPanel.GetComponentsInChildren<TextMeshProUGUI>().Last().text = "";

            //}
        }
        else
        {
            if (SessionManager.instance.brainstorming.isOwner)
            {
                Common.alert(LanguageManager.isFR ? "Souhaitez-vous lancer le sondage pour tous les participants maintenant ?" : "Would you like to launch the poll for all participants now?", () =>
                {
                    PollBehaviour.instance.showPolls();
                });

            }

            //NetworkPoll.instance.voting.transform.parent.GetComponentsInChildren<Animator>()[1].Play("deselect");
            //Common.alert("ceci est un sondage ... vous ne pouvez pas donner d'idées ici ... veuillez attendre que l'animateur lance le sondage");

        }
    }

    public void restartScene()
    {
        FindObjectOfType<ARSession>().Reset();

        //Common.instance.load(3);

        this.Invoke(() =>
        {
            FindObjectOfType<ARPlaneManager>().enabled = true;
            FindObjectOfType<TapToPlace>().enabled = true;

            FindObjectOfType<TapToPlace>().gameObjectToInstantiate = FindObjectOfType<TapToPlace>().spawnedObject;
            FindObjectOfType<TapToPlace>().spawnedObject.SetActive(false);
            helpSystem.moveDevice(true, true);
            SessionManager.once = false;
            
        }, 1);
    }

    public void checkIdeas(bool close = false)
    {
        print("lauchIdeaPool");
        //if (!SessionManager.instance.brainstorming.isOwner)
        //{
        //    VotingBehaviour.instance.transform.GetChild(1).GetComponent<Animator>().Play("show");
        //}
        //else
        //{
        //    //VotingBehaviour.instance.transform.GetChild(1).GetComponent<Animator>().Play("show");

        //    VotingBehaviour.instance.ideaPanel.SetActive(!VotingBehaviour.instance.ideaPanel.activeInHierarchy);


        //    VotingBehaviour.instance.ideaPanel.GetComponentsInChildren<TextMeshProUGUI>().Last().text = "";

        //}

        if (close)
        {
            NetworkPoll.instance.voting.transform.parent.GetComponentsInChildren<Animator>()[1].Play("deselect");
            return;
        }
        if (!SessionManager.instance.brainstorming.isPoll)
        {
            VotingBehaviour.instance.transform.GetChild(1).GetComponent<Animator>().Play("show");
        }
        else
        {
            NetworkPoll.instance.voting.transform.parent.GetComponentsInChildren<Animator>()[1].Play("deselect");
            Common.alert(LanguageManager.isFR ? "ceci est un sondage ... vous ne pouvez pas donner d'idées ici ... veuillez attendre que l'animateur lance le sondage" : "this is a poll ... you can't give any ideas here ... please wait for the host to launch the poll");
        }
        //NetworkPoll.instance.voting.transform.parent.GetChild(1).GetComponent<Animator>().Play("select");
        //NetworkPoll.instance.voting.transform.parent.GetChild(0).GetComponent<Animator>().Play("deselect");
    }

    public void positionTable()
    {
        if (TapToPlace.instance.spawnedObject)
        {
            PlacementSystem.instance.spawnedObject = TapToPlace.instance.spawnedObject;
            Gestures.instance.setGesturesReady(true);
            //Common.toggleTaskbar(true, true);
        }
        else
        {
            Common.alert(LanguageManager.isFR ? "S'il vous plaît placer l'objet d'abord, vous pouvez modifier sa position" : "Please place the object first, you can change its position");
        }
    }

    public void toggleSound()
    {
        isSound = !isSound;
        sound.SetActive(!isSound);

        //Camera.main.transform.GetComponent<AudioListener>().enabled = isSound;

        ConferenceApp.instance.toggleSound(isSound);

        toggleMenu();
        //SoundManager.instance.toggleSound();
    }

    public void toggleMusic()
    {
        isMusic = !isMusic;
        music.SetActive(!isMusic);
        toggleMenu();
        SoundManager.instance.toggleSound();
    }
}
