﻿using TMPro;
using Unity.VectorGraphics;
using UnityEngine;
using UnityEngine.UI;

public class memberUiBehaviour : MonoBehaviour
{

    public Sprite mute, notMute;
    
    internal void request()
    {
        transform.GetChild(3).gameObject.SetActive(true);
        transform.GetChild(4).gameObject.SetActive(true);
    }

    public void toggleMute(bool isMute)
    {
        transform.GetChild(1).GetComponent<Image>().sprite = isMute ? mute : notMute;
        if (person)
        {
            if (PlayerPrefs.GetString("username", "not logged in") != usernameM)
            {
                person.GetComponentInChildren<Image>().sprite = isMute ? mute : notMute;
            }
        }
                
    }

    public GameObject person;
    public string usernameM;
    public void destroy()
    {
        print("________________ destroyed a person");
        if (person)
        {
            if(PlayerPrefs.GetString("username", "not logged in") != usernameM)
            {
                Destroy(person);
            }
        }
    }

    public void setParticipant(MonoBehaviour mo, string username,string profile,bool isOwner)
    {

        transform.GetChild(3).gameObject.SetActive(false);
        transform.GetChild(4).gameObject.SetActive(false);

        usernameM = username;

        GameObject loading = Common.instance.startLoader("member.setParticipant");

        if (!profile.Contains("avataaars"))
        {
            mo.fetchImage(profile, (texture) => {
                transform.GetComponentInChildren<SVGImage>().sprite = Common.SpriteFromTexture2D(texture);

                if (PlayerPrefs.GetString("username", "not logged in") != usernameM)
                    person = avatarsAroundTable.instance.addPerson(Common.SpriteFromTexture2D(texture));

                Destroy(loading);

                if (isOwner)
                {
                    toggleMute(false);
                }
                else
                {
                    mo.Invoke(() =>
                    {
                        toggleMute(SessionManager.instance.brainstorming.mode != 1);
                    }, 1);
                    
                }
            },
            error => 
            {
                Destroy(loading);
            });
        }
        else
        {
            print(username + " has a profile picture : " + profile);

            mo.getText(profile, response =>
            {
                var sprite = Common.instance.getSvgImage(response);

                transform.GetComponentInChildren<SVGImage>().sprite = sprite;

                if (PlayerPrefs.GetString("username", "not logged in") != usernameM)
                    person = avatarsAroundTable.instance.addPerson(sprite);

                Destroy(loading);

                if (isOwner)
                {
                    toggleMute(false);
                }
                else
                {
                    mo.Invoke(() =>
                    {
                        toggleMute(SessionManager.instance.brainstorming.mode != 1);
                    }, 1);
                }

            });
        }

        GetComponentInChildren<TextMeshProUGUI>().text = username;
        
        

    }

}
