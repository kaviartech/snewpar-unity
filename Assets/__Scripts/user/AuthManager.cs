﻿using System.Collections;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AuthManager : MonoBehaviour
{
    [Space(20)]
    public bool isRegister;

    [Space(20)]
    [Header("Login")]
    [Space(30)]
    [SerializeField]
    private string userOrEmail;
    [SerializeField]
    private string password;
    [SerializeField]
    private bool remeberMe;
    [Space(10)]
    public TMP_InputField loginInput;

    [Space(20)]
    [Header("Register")]
    [Space(30)]
    [SerializeField]
    private string username;
    [SerializeField]
    private string email;
    [SerializeField]
    private string firstName;
    [SerializeField]
    private string lastName;
    [SerializeField]
    private string confirmRegisterPassword;
    [SerializeField]
    private bool termsAndConditions;
    [SerializeField]
    private string registerPassword;

    private string loginPath = "auth/login";
    private string registerPath = "auth/register";

    public string UserOrEmail { get => userOrEmail; set => userOrEmail = value; }
    public string Password { get => password; set => password = value; }
    public bool RemeberMe { get => remeberMe; set => remeberMe = value; }
    public string Username { get => username; set => username = value; }
    public string Email { get => email; set => email = value; }
    public string FirstName { get => firstName; set => firstName = value; }
    public string LastName { get => lastName; set => lastName = value; }
    public string RegisterPassword { get => registerPassword; set => registerPassword = value; }
    public string ConfirmRegisterPassword { get => confirmRegisterPassword; set => confirmRegisterPassword = value; }
    public bool TermsAndConditions { get => termsAndConditions; set => termsAndConditions = value; }

    public static Sprite profile;



    private void Start()
    {

        CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

        if (Common.getUserInfo(out string id, out string path))
        {
            //print("loading profile");
            if (path != "")
            {
                //print("loading "+ path);

                GameObject loading = Common.instance.startLoader("auth.start");

                if (!path.Contains("avataaars"))
                {
                    Debug.Log("if path  = "+path);
                    this.fetchImage(path, (texture) =>
                    {

                        profile = Common.SpriteFromTexture2D(texture);
                        Destroy(loading);

                        SceneManager.LoadScene(2);
                    });
                }
                else
                {
                    Debug.Log("else ext path  = " + path);
                    this.getText(path, response =>
                    {
                        var sprite = Common.instance.getSvgImage(response);
                        profile = sprite;

                        Destroy(loading);
                        SceneManager.LoadScene(2);

                    });
                }


            }
            else
            {
                //print("loading default profile");
                Debug.Log("path  = " + path);
                profile = Common.SpriteFromTexture2D(Resources.Load<Texture2D>("profile"));
                SceneManager.LoadScene(0);
            }

        }

    }

    public void toggleAuth()
    {
        GetComponent<Animator>().Play(isRegister ? "login" : "register");
        isRegister = !isRegister;
    }


    public void resetPassword()
    {
        Debug.Log(userOrEmail);
        string jsonUti =  JsonUtility.ToJson(new resetPasswordRequestData(userOrEmail));
        print(jsonUti);
        this.fetchText(Common.Baseurl + "auth/password/reset", jsonUti, response =>
          {
              print(response);
              responseData res = JsonUtility.FromJson<responseData>(response);
              if(res.success)
                Common.alert(LanguageManager.isFR ? "Veuillez vérifier votre email pour modifier votre mot de passe" : "Please check your email inbox to change your password" );
              else
                Common.alert(LanguageManager.isFR ? "Utilisateur introuvable, vérifiez le nom d'utilisateur ou l'email renseigné." : "User not found, please check the provided username or email.");

          });
    }


    public void login()
    {
        StartCoroutine(loginEnum());
        //Common.instance.load(1);
    }
    [System.Serializable]
    struct resetPasswordRequestData
    {
        public string username;
        

        public resetPasswordRequestData(string usernameOrEmail)
        {
            this.username = usernameOrEmail;
            
        }
    }
    [System.Serializable]
    public struct responseResetData
    {
        public string  message, username;
        public bool success;
    }

    [System.Serializable]
    struct loginRequestData
    {
        public string username;
        public string password;
        public bool stayConnected;
        public string registrationToken;

        public loginRequestData(string username, string password, bool stayConnected, string registrationToken)
        {
            this.username = username;
            this.password = password;
            this.stayConnected = stayConnected;
            this.registrationToken = registrationToken;
        }
    }

    [System.Serializable]
    public struct responseData
    {
        public string token;
        public string username,message,path;
        public bool success;
    }

    IEnumerator loginEnum()
    {

        if(userOrEmail == "")
        {
            Common.alert(LanguageManager.isFR ? "Vous devez entrer votre nom d'utilisateur ou votre email" : "You must enter your username or email");
            yield break;
        }

        if (password == "")
        {
            Common.alert(LanguageManager.isFR ? "Vous devez entrer votre mot de passe" : "You must enter your password");
            yield break;
        }
#if !UNITY_WEBGL
        Debug.Log(notifications.registrationToken);
        string jsonStr = JsonUtility.ToJson(new loginRequestData(userOrEmail, password, remeberMe, notifications.registrationToken));
#else
        Debug.Log(username);
        string jsonStr = JsonUtility.ToJson(new loginRequestData(userOrEmail, password, remeberMe, ""));
#endif
        //print(jsonStr)

        GameObject loading = Common.instance.startLoader("auth.login");

        this.fetchText(Common.Baseurl + loginPath, jsonStr,
            response =>
            {
                responseData res = JsonUtility.FromJson<responseData>(response);

                print(response);

                if (res.success)
                {
                    //if(remeberMe)
                    PlayerPrefs.SetString("token", res.token);
                    PlayerPrefs.SetString("username", res.username);
                    PlayerPrefs.SetString("path", res.path);
                    PlayerPrefs.SetString("pwd", password);


                    //print(res.token);


                    //GameObject loading = Common.instance.startLoader();

                    if (!res.path.Contains("avataaars"))
                    {
                        //if (res.path.ToUpper().Contains("S3"))
                        //{
                            this.fetchImage(res.path, (texture) =>
                            {
                                Debug.Log(res.path);
                                Debug.Log(texture);
                                profile = Common.SpriteFromTexture2D(texture);
                                Destroy(loading);
                                Common.instance.load(2);


                            });
                        //}
                        //else
                        //{
                        //    this.fetchImage(res.path, (texture) =>
                        //    {

                        //        profile = Common.SpriteFromTexture2D(texture);
                        //        Destroy(loading);
                        //        Common.instance.load(1);


                        //    }, (error) =>
                        //    {
                        //        Destroy(loading);
                        //        profile = Common.SpriteFromTexture2D(Resources.Load<Texture2D>("profile"));
                        //        Common.instance.load(1);
                        //    });
                        //}
                            
                    }
                    else
                    {
                        this.getText(res.path, svgresponse =>
                        {
                            var sprite = Common.instance.getSvgImage(svgresponse);

                            Destroy(loading);
                            Common.instance.load(2);

                        });
                    }

                }
                else if (res.message.ToUpper().Contains("ACCOUNT"))
                {
                    Destroy(loading);
                    Common.alert(LanguageManager.isFR ? "Veuillez vérifier votre courrier avant de continuer." : "Please verify your mail before continuing.");
                }
                else if (res.message.ToUpper().Contains("USER"))
                {
                    Destroy(loading);
                    Common.alert(LanguageManager.isFR ? "Aucun utilisateur n'a été trouvé avec ce nom d'utilisateur ou cette adresse e-mail" : "No user was found with this username or email address") ;
                }
                else
                {
                    Destroy(loading);
                    Common.alert(LanguageManager.isFR ? "Votre mot de passe est incorrect" : "Your password is incorrect");
                }
            },
            error =>
            {
                Destroy(loading);
                Common.alert(LanguageManager.isFR ? "Assurez-vous que vous êtes connecté à Internet ... ou nos serveurs sont peut-être en maintenance ... essayez plus tard" : "Make sure you are connected to the Internet ... or our servers may be under maintenance ... try later");
            }
        );

    }


    [System.Serializable]
    struct registerRequestData
    {
        public string username, firstName, lastName, email;
        public string password;
        public string registrationToken;


        public registerRequestData(string username, string firstName, string lastName, string email, string password,  string registrationToken)
        {
            this.username = username;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.password = password;
            this.registrationToken = registrationToken;

        }
    }

    public void register()
    {
        StartCoroutine(registerEnum());
    }

    IEnumerator registerEnum()
    {
        if(registerPassword != confirmRegisterPassword)
        {
            Common.alert(LanguageManager.isFR ? "Votre mot de passe et confirmer le mot de passe de ne correspondent pas" : "Your password and confirm password do not match");
            yield break;
        }

        if (/*!Regex.IsMatch(registerPassword, @"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,32}$")*/ registerPassword.Length < 2 )
        {
            Common.alert(LanguageManager.isFR ? "Votre mot de passe doit contenir au moins 2 caractères" : "Your password must contain at least 2 characters"/*caractères et au plus 32.il doit avoir au moins 1 chiffre et 1 lettre majuscule et minuscule*/);
            yield break;
        }

        if (!Regex.IsMatch(firstName, @"^[a-zA-Z ]{3,16}$"))
        {
            Common.alert(LanguageManager.isFR ? "Votre prénom ne doit contenir que des lettres et au moins 3 caractères" : "Your first name must contain only letters and at least 3 characters");
            yield break;
        }

        if (!Regex.IsMatch(lastName, @"^[a-zA-Z ]{3,16}$"))
        {
            Common.alert(LanguageManager.isFR ?"Votre nom ne doit contenir que des lettres et au moins 3 caractères" : "Your last name must contain only letters and at least 3 characters");
            yield break;
        }

        if (!Regex.IsMatch(username, @"^[a-zA-Z0-9]{4,16}$"))
        { 
            Common.alert(LanguageManager.isFR ? "Votre nom d'utilisateur ne doit contenir que des lettres et au moins 4 caractères" : "Your username must contain only letters and at least 4 characters");
            yield break;
        }

        if (!Regex.IsMatch(email, @"[\w-\.]+@([\w-]+\.)+[\w-]{2,4}"))
        {
            Common.alert(LanguageManager.isFR ?"Votre email n'est pas valide" : "Your email is invalid");
            yield break;
        }

        if (!termsAndConditions)
        {
            Common.alert(LanguageManager.isFR ?"Veuillez accepter les termes et conditions" : "Please accept the terms and conditions");
            yield break;
        }

#if !UNITY_WEBGL
        string jsonStr = JsonUtility.ToJson(new registerRequestData(username, firstName, lastName, email, registerPassword, notifications.registrationToken));
#else
        string jsonStr = JsonUtility.ToJson(new registerRequestData(username, firstName, lastName, email, registerPassword, ""));
#endif

        /*print(jsonStr);*/

        GameObject loading = Common.instance.startLoader("auth.signup");


        this.fetchText(Common.Baseurl + registerPath, jsonStr,
            response =>
            {
                responseData res = JsonUtility.FromJson<responseData>(response);

                //print(request.downloadHandler.text);

                if (res.success)
                {
                    loginInput.text = email;
                    Common.alert(LanguageManager.isFR ? "Un email de confirmation vous a été envoyé" : "A confirmation mail was sent");

                    Destroy(loading);

                    toggleAuth();
                    //print(res.token);

                    //Common.instance.load(1);
                }
                else if (res.message.ToUpper().Contains("EXIST"))
                {
                    Destroy(loading);
                    Common.alert(LanguageManager.isFR ? "Le nom d'utilisateur ou l'adresse e-mail existent déjà, essayez de les changer" : "The username or email address already exists, try to change them");
                }
                else
                {
                    Destroy(loading);
                    Common.alert(LanguageManager.isFR ? "Assurez-vous que vous êtes connecté à Internet ... ou nos serveurs sont peut-être en maintenance ... essayez plus tard": "Make sure you are connected to the internet ... or our servers may be under maintenance ... try later");
                }
            },
            error =>
            {
                Destroy(loading);
                Common.alert(LanguageManager.isFR ? "Assurez-vous que vous êtes connecté à Internet ... ou nos serveurs sont peut-être en maintenance ... essayez plus tard": "Make sure you are connected to the internet ... or our servers may be under maintenance ... try later");
            });

       
    }



}
