﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using Unity.VectorGraphics;
using UnityEngine;

public class avatarsAroundTable : Singleton<avatarsAroundTable>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public GameObject personPrefab;
    bool first = true;
    Vector3 defaultPos;
    Vector3 defaultScale;
    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/



    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        InvokeRepeating("checkTable",3,1);
    }

    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/


    void checkTable()
    {
        if (TapToPlace.instance)
        {
            if (TapToPlace.instance.spawnedObject)
            {
                transform.parent = TapToPlace.instance.spawnedObject.transform.GetChild(0).GetChild(1);
                if (GameObject.Find("Cube"))
                {
                    if (GameObject.Find("Cube").GetComponent<BoxCollider>())
                    {
                        if (first & ObjectLoader.isLoaded)
                        {
                            defaultScale = GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.size;
                            defaultPos = GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.center;
                            first = false;
                            Debug.Log("defAult " + GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.max.z);
                            transform.localScale = new Vector3(defaultScale.z, defaultScale.z, defaultScale.z);
                            transform.position = defaultPos;

                        }
                        Debug.Log("checkjrable " + GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.max.z);
                        Debug.Log("Updatetable extents max " + GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.extents.z);

                        //transform.localPosition = new Vector3(0, 0, GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.max.z < defaultPos ? defaultPos : GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.max.z);
                    }
                }
                //else if (GameObject)
                //    transform.localPosition = new Vector3(0, 0, 1.5f);

                //transform.localScale = Vector3.one;

                foreach (Transform item in transform)
                {
                    item.gameObject.SetActive(true);
                }

            }
        }
    }
    

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    void updatePosition()
    {
        int count = transform.childCount;

        float step = 360f / count;

        float sum = 0;

        foreach (Transform item in transform)
        {
            item.localRotation = Quaternion.Euler(0,0,sum);
            //if (GameObject.Find("Cube"))
            //{
            //    if (GameObject.Find("Cube").GetComponent<BoxCollider>())
            //    {
            //        Debug.Log("Updatetable bounds max " + GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.max.z);
            //        Debug.Log("Updatetable extents max " + GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.extents.z);
            //        transform.localPosition = new Vector3(0, 0, GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.max.z < 1.5f ? 1.5f : GameObject.Find("Cube").GetComponent<BoxCollider>().bounds.max.z);
            //    }
            //}
            sum += step;
        }


    }

    public GameObject addPerson(Sprite person)
    {
        GameObject temp = Instantiate(personPrefab, transform);
        temp.GetComponentInChildren<SVGImage>().sprite = person;
        temp.SetActive(false);
        updatePosition();
        return temp;
    }

}
