﻿using BasicTools.ButtonInspector;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

public class PlacementSystem : Singleton<PlacementSystem>
{
    public RawImage current;

    public GameObject spawnedObject;

    public GameObject gameObjectToInstantiate;

    public GameObject parent;

    public FormeRect rect = null;
    public string content;

    public string currentType;

    public TextMeshProUGUI debugText;
    public GameObject placementHelp;

    private Vector2 touchPosition;


    bool TryGetTouchPosition(out Vector2 touchPosition)
    {

        if (Input.touchCount > 0)
        {
            Debug.Log("Touch");
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;

    }


    public void finishPlacement()
    {
        NetworkObject temp;

        string gid = Convert.ToBase64String(BitConverter.GetBytes(DateTime.Now.Ticks));

        //print(gid);
        gid = gid.Replace('/', 'f');
        print(gid);

        if (currentType.ToUpper().Contains("DRAW"))
        {
            //var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(DateTime.Now.Ticks.ToString());

            temp = new NetworkObject
            {
                id = gid + NetworkingClient.ClientID,
                formeRect = null,
                type = "DRAW",
                owner = NetworkingClient.ClientID,
                ownerPath = PlayerPrefs.GetString("path", ""),
                ownerUserName = PlayerPrefs.GetString("username", ""),
                ownerId = PlayerPrefs.GetString("token", ""),
                content = content
            };

            this.uploadImage(Common.Baseurl + "api/image/add", content, Common.getUserInfo(), path =>
            {

                temp.content = path;

                //print(path);

                temp.setPosition(spawnedObject.transform.localPosition);
                temp.setRotation(spawnedObject.transform.localRotation);
                temp.setScale(spawnedObject.transform.localScale);

                debugText.text = $"i have placed this object {temp.type} at position ({temp.positionX} , {temp.positionY} , {temp.positionZ}) \n real object position is ({spawnedObject.transform.localPosition.x},{spawnedObject.transform.localPosition.y},{spawnedObject.transform.localPosition.z})";

                string x = JsonUtility.ToJson(temp);

                networkPosition.instance.addItem(JsonUtility.FromJson<NetworkObject>(x));
            });

        }
        else
        {
            temp = new NetworkObject
            {
                id = gid + NetworkingClient.ClientID,
                formeRect = rect,
                type = currentType.ToUpper(),
                owner = NetworkingClient.ClientID,
                ownerPath = PlayerPrefs.GetString("path", ""),
                ownerUserName = PlayerPrefs.GetString("username", ""),
                ownerId = PlayerPrefs.GetString("token", ""),
                content = content

            };

            temp.setPosition(spawnedObject.transform.localPosition);
            temp.setRotation(spawnedObject.transform.localRotation);
            temp.setScale(spawnedObject.transform.localScale);

            debugText.text = $"i have placed this object {temp.type} at position ({temp.positionX} , {temp.positionY} , {temp.positionZ}) \n real object position is ({spawnedObject.transform.localPosition.x},{spawnedObject.transform.localPosition.y},{spawnedObject.transform.localPosition.z})";

            string x = JsonUtility.ToJson(temp);

            networkPosition.instance.addItem(JsonUtility.FromJson<NetworkObject>(x));
        }


        gameObjectToInstantiate = null;
        spawnedObject.SetActive(true);
        spawnedObject.name = temp.id;

        spawnedObject.GetComponentInChildren<itemEvents>().Select();

        HistoryManager.Add(temp.id, spawnedObject, current.texture , temp.type);

        Gestures.instance.gestureType = "";

        rect = null;
        currentType = "";
        placementHelp.SetActive(false);

    }


#if UNITY_EDITOR

    [Button("place object", "place", true)]
    public bool button_1;

    public void place()
    {

        spawnedObject = Instantiate(gameObjectToInstantiate, Vector3.zero, Quaternion.identity, parent.transform);


        //if (spawnedObject.TryGetComponent(out VideoPlayer vp))
        //{
        //    vp.prepareCompleted += (VideoPlayer source) =>
        //    {
        //        Debug.Log("dimensions " + source.texture.width + " x " + source.texture.height); // do with these dimensions as you wish

        //        source.transform.localScale = new Vector3(.1f, .005f, .1f * ((float)source.texture.height / (float)source.texture.width));

        //    };

        //    vp.Prepare();
        //}

        finishPlacement();


        Gestures.instance.GesturesReady = true;
    }

#endif


    private void Update()
    {
        helpSystem.tapToPlace(true, false);

        //if (!CloudControl.instance.DoneLoading) return;

        if (!TryGetTouchPosition(out touchPosition)) return;

        if (Common.IsPointerOverUIObject(touchPosition)) return;

        if (Gestures.instance.GesturesReady) return;

        if (gameObjectToInstantiate == null) return;

        RaycastHit hit;

        // Construct a ray from the current touch coordinates
        Ray ray = Camera.main.ScreenPointToRay(touchPosition);

        //if (_arRaycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            var hitPose = hit.point;

            //print(hitPose);


            spawnedObject = Instantiate(gameObjectToInstantiate, hitPose + new Vector3(0, 0.005f, 0),Quaternion.identity, parent.transform);

            spawnedObject.transform.rotation = Quaternion.Euler(-90,0,0);


            //spawnedObject.transform.localScale = hit.collider.transform.localScale.z < 1 ? Vector3.one : new Vector3(hit.collider.transform.localScale.z, hit.collider.transform.localScale.z, hit.collider.transform.localScale.z);
            spawnedObject.transform.localScale = ObjectLoader.cube.transform.localScale.z < 1 ? Vector3.one : new Vector3(ObjectLoader.cube.transform.localScale.z, ObjectLoader.cube.transform.localScale.z, ObjectLoader.cube.transform.localScale.z);
            //spawnedObject.transform.SetParent(parent.transform);

            //if (spawnedObject.TryGetComponent(out VideoPlayer vp))
            //{
            //    vp.prepareCompleted += (VideoPlayer source) =>
            //    {
            //        Debug.Log("dimensions " + source.texture.width + " x " + source.texture.height); // do with these dimensions as you wish

            //        source.transform.localScale = new Vector3(.1f, .005f, .1f * ((float)source.texture.height / (float)source.texture.width));

            //    };

            //    vp.Prepare();
            //}

            finishPlacement();



            Gestures.instance.GesturesReady = true;

        }

    }

    public void placeIdea(string inputIdea)
    {
        spawnedObject = Instantiate(TextManager.instance.gameObjectToInstantiate, Vector3.zero, Quaternion.identity, TapToPlace.instance.spawnedObject.transform);

        NetworkObject temp;

        string gid = Convert.ToBase64String(BitConverter.GetBytes(DateTime.Now.Ticks));

        //print(gid);

        gid = gid.Replace('/', 'f');

        temp = new NetworkObject
        {
            id = gid + NetworkingClient.ClientID,
            formeRect = null,
            type = "IDEA",
            owner = NetworkingClient.ClientID,
            ownerPath = PlayerPrefs.GetString("path", ""),
            ownerUserName = PlayerPrefs.GetString("username", ""),
            ownerId = PlayerPrefs.GetString("token", ""),
            content = inputIdea

        };

        //print(temp.content);

        spawnedObject.transform.localPosition = Vector3.zero;
        spawnedObject.transform.localScale = Vector3.zero;

        temp.setPosition(spawnedObject.transform.localPosition);
        temp.setRotation(spawnedObject.transform.localRotation);
        temp.setScale(spawnedObject.transform.localScale);

        string x = JsonUtility.ToJson(temp);

        networkPosition.instance.addItem(JsonUtility.FromJson<NetworkObject>(x));

        spawnedObject.SetActive(true);
        spawnedObject.name = temp.id;

    }
}
