﻿using System.Collections;
using UnityEngine;

public class helpSystem : Singleton<helpSystem>
{
    public RectTransform focus;

    public Transform[] Help;
    public static bool secondaryHelp = false;
    public int curretnNumber;
    private int currentType;
    public static bool restarted = false;

    public int CurrentType
    {
        get => currentType; set
        {
            currentType = value;
            ResetAll(value);
        }
    }

    IEnumerator Start()
    {
        Common.isMainHelp = true;
        yield return new WaitForSeconds(1);
        mainHelp();
    }

    public void ResetAll(int value)
    {
        foreach (Transform help in Help)
        {
            foreach (Transform child in help)
            {
                toggleActive(child, false);
            }

        }

        transform.GetChild(4 - 2 * value).gameObject.SetActive(false);
        curretnNumber = -1;
    }

    public void next()
    {
        //Common.deactivateAll();
        if (curretnNumber == Help[CurrentType].childCount - 1)
        {

            //deactivate taskbar
            if (CurrentType == 0)
            {
                if (!Common.isTaskbarActive)
                {
                    PostManager.instance.GetComponentInParent<Animator>().Play("hide");
                }
            }
            else
            {
                if (!Common.isControlsActive)
                {
                    Gestures.instance.controls.Play("hide");
                }
            }

            toggleActive(Help[CurrentType].GetChild(curretnNumber), false);

            transform.GetChild(2 + 2 * CurrentType).gameObject.SetActive(false);

            focus.gameObject.SetActive(false);
            curretnNumber = -1;

            if(!secondaryHelp)
                moveDevice(true, true);

            SessionManager.instance.enabled = true;

            PlayerPrefs.SetInt("isFirstTimeMainHelp", 1);
            Common.isMainHelp = false;

            if(currentType == 1)
            {
                Gestures.instance.setGesturesReady(false);
            }

            return;
        }

        //activate taskbar
        if (CurrentType == 0)
        {
            if (!Common.isTaskbarActive)
            {
                PostManager.instance.GetComponentInParent<Animator>().Play("show");
            }
        }
        else
        {
            if (!Common.isControlsActive)
            {
                print("showing controls");
                //Gestures.instance.controls.Play("show");
                Gestures.instance.setGesturesReady(true);
            }
        }

        if (curretnNumber >= 0) toggleActive(Help[CurrentType].GetChild(curretnNumber), false);

        

        curretnNumber++;

        if (curretnNumber == 0)
        {
            moveDevice(true, false);
        }

        toggleActive(Help[CurrentType].GetChild(curretnNumber), true);

        move(Help[CurrentType].GetChild(curretnNumber).GetComponent<RectTransform>());

    }

    public void toggleActive(Transform gameobject, bool state)
    {
        foreach (Transform child in gameobject)
        {
            child.gameObject.SetActive(state);
        }

    }

   

    public void back()
    {
        Common.deactivateAll();
        if (curretnNumber <= 0)
        {
            return;
        }

        if (CurrentType == 0)
        {
            if (!Common.isTaskbarActive)
            {
                PostManager.instance.GetComponentInParent<Animator>().Play("show");
            }
        }
        else
        {
            if (!Common.isControlsActive)
            {
                //Gestures.instance.controls.Play("show");
            }
        }

        if (curretnNumber >= 0) toggleActive(Help[CurrentType].GetChild(curretnNumber), false);

        curretnNumber--;
        toggleActive(Help[CurrentType].GetChild(curretnNumber), true);

        move(Help[CurrentType].GetChild(curretnNumber).GetComponent<RectTransform>());

    }

    public void move(RectTransform rt)
    {
        //print("activating focus");

        focus.gameObject.SetActive(true);
        StopAllCoroutines();
        StartCoroutine(moveTo(rt.position, rt.sizeDelta, rt.pivot));
    }

    IEnumerator moveTo(Vector3 position, Vector2 size, Vector2 pivot)
    {
        //print("started");

        while (((position - focus.position).magnitude > .01f) || ((size - focus.sizeDelta).magnitude > .01f) /*|| ((anchorMin - focus.anchorMin).magnitude > .01f) || ((anchorMax - focus.anchorMax).magnitude > .01f) */|| ((pivot - focus.pivot).magnitude > .01f))
        {
            focus.sizeDelta = lerp(focus.sizeDelta, size);
            focus.position = lerp(focus.position, position);

            //focus.anchorMin = lerp(focus.anchorMin,anchorMin);
            //focus.anchorMax = lerp(focus.anchorMax ,anchorMax);

            focus.pivot = lerp(focus.pivot, pivot);

            yield return null;
        }

        yield return null;

        //print("ended");
    }

    public Vector2 lerp(Vector2 a, Vector2 b)
    {
        return Vector2.Lerp(a, b, 7 * Time.deltaTime);
    }

    public Vector3 lerp(Vector3 a, Vector3 b)
    {
        return Vector3.Lerp(a, b, 7 * Time.deltaTime);
    }


    public static void tapToPlace(bool forced = false,bool state = false)
    {
        if (!forced)
        {
            instance.transform.GetChild(6).gameObject.SetActive(!instance.transform.GetChild(6).gameObject.activeInHierarchy);
        }
        else
        {
            if (instance.transform.GetChild(6).gameObject.activeInHierarchy != state)
            {
                instance.transform.GetChild(6).gameObject.SetActive(state);
            }
        }
        
    }

    public static void moveDevice(bool forced = false, bool state = false)
    {
        if (!forced)
        {
            instance.transform.GetChild(5).gameObject.SetActive(!instance.transform.GetChild(5).gameObject.activeInHierarchy);
        }
        else
        {
            if (instance.transform.GetChild(5).gameObject.activeInHierarchy != state)
            {
                instance.transform.GetChild(5).gameObject.SetActive(state);
            }
        }
    }

    public static void mainHelp()
    {

        if (PlayerPrefs.GetInt("isFirstTimeMainHelp", 0) == 0)
        {
            secondaryHelp = false;
            tapToPlace(true, false);
            moveDevice(true, false);
            instance.currentType = 0;
            instance.next();
            instance.transform.GetChild(2).gameObject.SetActive(true);
        }
        else
        {
            Common.isMainHelp = false;
            moveDevice(true, true);
            SessionManager.instance.enabled = true;
           
        }
    }

    public void skipHelp()
    {
        Common.isMainHelp = false;
        //moveDevice(true, true);
        SessionManager.instance.enabled = true;
        PlayerPrefs.SetInt("isFirstTimeMainHelp", 1);
        if (!Common.isTaskbarActive)
        {
            PostManager.instance.GetComponentInParent<Animator>().Play("hide");
        }
        toggleActive(Help[CurrentType].GetChild(curretnNumber), false);
    }

    public static void restart()
    {
        restarted = true;
        Common.isMainHelp = false;
        moveDevice(true, true);
        SessionManager.instance.enabled = true;
        Gestures.instance.GesturesReady = false; //isActive;
        Common.toggleTaskbar(false);
    }

    public static void help()
    {
        Gestures.instance.GesturesReady = false; //isActive;
        secondaryHelp = true;
        //Common.toggleControlPanel(false);
        tapToPlace(true, false);
        moveDevice(true, false);
        instance.currentType = 0;
        instance.next();
        instance.transform.GetChild(2).gameObject.SetActive(true);
    }
}
