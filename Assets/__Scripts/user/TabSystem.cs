﻿using UnityEngine;
using UnityEngine.UI;

public class TabSystem : MonoBehaviour
{
    public Transform buttons, panels;
    public CanvasGroup addUser;


    void Start()
    {
        if (!PinInput.brainstorming.isOwner)
        {
            addUser.alpha = 0;
            addUser.blocksRaycasts = false;
            addUser.interactable = false;
        }
        
        buttons.GetComponentInChildren<Animator>().Play("select");

        int i = 0;
        foreach (Transform t in buttons)
        {
            int x = i;
            t.GetComponent<Button>().onClick.AddListener(delegate { select(t.GetComponent<Button>(), x); });
            //print(i);
            i++;
        }
    }

    void select(Button b, int number)
    {
        //print(number);

        resetPanels();
        b.GetComponent<Animator>().Play("select");

        panels.GetChild(number).gameObject.SetActive(true);

    }


    public void resetPanels()
    {
        foreach (Transform t in buttons)
        {
            t.GetComponent<Animator>().Play("deselect");
        }

        foreach (Transform t in panels)
        {
            t.gameObject.SetActive(false);
        }

    }


}
