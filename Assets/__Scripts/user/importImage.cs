﻿/*

*/

using Unity.VectorGraphics;
using UnityEngine;
using TMPro;

[System.Serializable]
public class Avatar
{
    public string topType, accessoriesType, facialHairType, clotheType, eyeType, eyebrowType, mouthType, skinColor;

    private string link;
   
    public string Link { 
        get
        {
            string temp = $"https://avataaars.io/?avatarStyle=Circle&" +
                $"topType={topType}&" +
                $"accessoriesType={accessoriesType}&" +
                $"facialHairType={facialHairType}&" +
                $"clotheType={clotheType}&" +
                $"eyeType={eyeType}&" +
                $"eyebrowType={eyebrowType}&" +
                $"mouthType={mouthType}&" +
                $"skinColor={skinColor}";
            return temp; 
        }
    }
}
public class importImage : Singleton<importImage>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public SVGImage svgImage;

    public Avatar avatar;

    public GameObject loading;

    public TMP_Dropdown[] dropdowns;

    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/

    private int skinColor;
    private int topType;
    private int accessoriesType;
    private int hairColor;
    private int facialHairType;
    private int facialHairColor;
    private int clotheType;
    private int clotheColor;
    private int eyeType;
    private int eyebrowType;
    private int mouthType;

    public int TopType { get => topType; set 
        {
            switch (value)
            {
                case 0:
                    avatar.topType = "NoHair";
                    break;
                case 1:
                    avatar.topType = "Hat";
                    break;
                case 2:
                    avatar.topType = "Hijab";
                    break;
                case 3:
                    avatar.topType = "Turban";
                    break;
                case 4:
                    avatar.topType = "WinterHat1";
                    break;
                case 5:
                    avatar.topType = "WinterHat2";
                    break;
                case 6:
                    avatar.topType = "WinterHat3";
                    break;
                case 7:
                    avatar.topType = "WinterHat4";
                    break;
                case 8:
                    avatar.topType = "LongHairBigHair";
                    break;
                case 9:
                    avatar.topType = "LongHairBob";
                    break;
                case 10:
                    avatar.topType = "LongHairBun";
                    break;
                case 11:
                    avatar.topType = "LongHairCurly";
                    break;
                case 12:
                    avatar.topType = "LongHairCurvy";
                    break;
                case 13:
                    avatar.topType = "LongHairDreads";
                    break;
                case 14:
                    avatar.topType = "LongHairFrida";
                    break;
                case 15:
                    avatar.topType = "LongHairFro";
                    break;
                case 16:
                    avatar.topType = "LongHairFroBand";
                    break;
                case 17:
                    avatar.topType = "LongHairNotTooLong";
                    break;
                case 18:
                    avatar.topType = "LongHairShavedSides";
                    break;
                case 19:
                    avatar.topType = "LongHairMiaWallace";
                    break;
                case 20:
                    avatar.topType = "LongHairStraight";
                    break;
                case 21:
                    avatar.topType = "LongHairStraight2";
                    break;
                case 22:
                    avatar.topType = "LongHairStraightStrand";
                    break;
                case 23:
                    avatar.topType = "ShortHairDreads01";
                    break;
                case 24:
                    avatar.topType = "ShortHairDreads02";
                    break;
                case 25:
                    avatar.topType = "ShortHairFrizzle";
                    break;
                case 26:
                    avatar.topType = "ShortHairShaggyMullet";
                    break;
                case 27:
                    avatar.topType = "ShortHairCurly";
                    break;
                case 28:
                    avatar.topType = "ShortHairShortFlat";
                    break;
                case 29:
                    avatar.topType = "ShortHairShortRound";
                    break;
                case 30:
                    avatar.topType = "ShortHairShortWaved";
                    break;
                case 31:
                    avatar.topType = "ShortHairSides";
                    break;
                case 32:
                    avatar.topType = "ShortHairTheCaesar";
                    break;
                case 33:
                    avatar.topType = "ShortHairTheCaesarSidePart";
                    break;
                default:
                    avatar.topType = "NoHair";
                    break;
            }

            print(avatar.topType);
            loading.SetActive(true);

            dropdowns[0].value = value;

            updateImage();

            topType = value; 
        } 
    }
    public int AccessoriesType { get => accessoriesType; 
        set 
        {
            switch (value)
            {
                case 0:
                    avatar.accessoriesType = "Blank";
                    break;
                case 1:
                    avatar.accessoriesType = "Kurt";
                    break;
                case 2:
                    avatar.accessoriesType = "Prescription01";
                    break;
                case 3:
                    avatar.accessoriesType = "Round";
                    break;
                default:
                    avatar.accessoriesType = "Blank";
                    break;
            }

            loading.SetActive(true);

            dropdowns[1].value = value;

            updateImage();

            accessoriesType = value; 
        } 
    }
    //public int HairColor { get => hairColor; set => hairColor = value; }
    public int FacialHairType { get => facialHairType;
        set
        {
            switch (value)
            {
                case 0:
                    avatar.facialHairType = "Blank";
                    break;
                case 1:
                    avatar.facialHairType = "BeardMedium";
                    break;
                case 2:
                    avatar.facialHairType = "BeardLight";
                    break;
                case 3:
                    avatar.facialHairType = "BeardMagestic";
                    break;
                case 4:
                    avatar.facialHairType = "MoustacheFancy";
                    break;
                case 5:
                    avatar.facialHairType = "MoustacheMagnum";
                    break;
                default:
                    avatar.facialHairType = "Blank";
                    break;
            }

            loading.SetActive(true);

            dropdowns[2].value = value;

            updateImage();

            facialHairType = value;
        }
    }
    //public int FacialHairColor { get => facialHairColor; set => facialHairColor = value; }
    public int ClotheType { get => clotheType;
        set
        {
            switch (value)
            {
                case 0:
                    avatar.clotheType = "BlazerShirt";
                    break;
                case 1:
                    avatar.clotheType = "BlazerSweater";
                    break;
                case 2:
                    avatar.clotheType = "CollarSweater";
                    break;
                case 3:
                    avatar.clotheType = "GraphicShirt";
                    break;
                case 4:
                    avatar.clotheType = "Hoodie";
                    break;
                case 5:
                    avatar.clotheType = "Overall";
                    break;
                case 6:
                    avatar.clotheType = "ShirtCrewNeck";
                    break;
                case 7:
                    avatar.clotheType = "ShirtScoopNeck";
                    break;
                case 8:
                    avatar.clotheType = "ShirtVNeck";
                    break;
                default:
                    avatar.clotheType = "BlazerShirt";
                    break;
            }

            loading.SetActive(true);

            dropdowns[3].value = value;

            updateImage();

            clotheType = value;
        }
    }
    //public int ClotheColor { get => clotheColor; set => clotheColor = value; }
    public int EyeType { get => eyeType;
        set
        {
            switch (value)
            {
                case 0:
                    avatar.eyeType = "Default";
                    break;
                case 1:
                    avatar.eyeType = "Close";
                    break;
                case 2:
                    avatar.eyeType = "Cry";
                    break;
                case 3:
                    avatar.eyeType = "Dizzy";
                    break;
                case 4:
                    avatar.eyeType = "EyeRoll";
                    break;
                case 5:
                    avatar.eyeType = "Happy";
                    break;
                case 6:
                    avatar.eyeType = "Hearts";
                    break;
                case 7:
                    avatar.eyeType = "Side";
                    break;
                case 8:
                    avatar.eyeType = "Squint";
                    break;
                case 9:
                    avatar.eyeType = "Surprised";
                    break;
                case 10:
                    avatar.eyeType = "Wink";
                    break;
                case 11:
                    avatar.eyeType = "WinkWacky";
                    break;
                default:
                    avatar.eyeType = "Default";
                    break;
            }

            loading.SetActive(true);

            dropdowns[4].value = value;

            updateImage();

            eyeType = value;
        }
    }
    public int EyebrowType { get => eyebrowType;
        set
        {
            switch (value)
            {
                case 0:
                    avatar.eyebrowType = "Default";
                    break;
                case 1:
                    avatar.eyebrowType = "Angry";
                    break;
                case 2:
                    avatar.eyebrowType = "AngryNatural";
                    break;
                case 3:
                    avatar.eyebrowType = "DefaultNatural";
                    break;
                case 4:
                    avatar.eyebrowType = "FlatNatural";
                    break;
                case 5:
                    avatar.eyebrowType = "RaisedExcited";
                    break;
                case 6:
                    avatar.eyebrowType = "RaisedExcitedNatural";
                    break;
                case 7:
                    avatar.eyebrowType = "SadConcerned";
                    break;
                case 8:
                    avatar.eyebrowType = "SadConcernedNatural";
                    break;
                case 9:
                    avatar.eyebrowType = "UnibrowNatural";
                    break;
                case 10:
                    avatar.eyebrowType = "UpDown";
                    break;
                case 11:
                    avatar.eyebrowType = "UpDownNatural";
                    break;
                default:
                    avatar.eyebrowType = "Default";
                    break;
            }

            loading.SetActive(true);

            dropdowns[5].value = value;

            updateImage();

            eyebrowType = value;
        }
    }
    public int MouthType { get => mouthType;
        set
        {
            switch (value)
            {
                case 0:
                    avatar.mouthType = "Default";
                    break;
                case 1:
                    avatar.mouthType = "Concerned";
                    break;
                case 2:
                    avatar.mouthType = "Disbelief";
                    break;
                case 3:
                    avatar.mouthType = "Eating";
                    break;
                case 4:
                    avatar.mouthType = "Grimace";
                    break;
                case 5:
                    avatar.mouthType = "Sad";
                    break;
                case 6:
                    avatar.mouthType = "ScreamOpen";
                    break;
                case 7:
                    avatar.mouthType = "Serious";
                    break;
                case 8:
                    avatar.mouthType = "Smile";
                    break;
                case 9:
                    avatar.mouthType = "Tongue";
                    break;
                case 10:
                    avatar.mouthType = "Twinkle";
                    break;
                case 11:
                    avatar.mouthType = "Vomit";
                    break;
                default:
                    avatar.mouthType = "Default";
                    break;
            }

            loading.SetActive(true);

            dropdowns[6].value = value;

            updateImage();

            mouthType = value;
        }
    }
    public int SkinColor { get => skinColor;
        set
        {
            switch (value)
            {
                case 0:
                    avatar.skinColor = "Light";
                    break;
                case 1:
                    avatar.skinColor = "Tanned";
                    break;
                case 2:
                    avatar.skinColor = "Yellow";
                    break;
                case 3:
                    avatar.skinColor = "Pale";
                    break;
                case 4:
                    avatar.skinColor = "Brown";
                    break;
                case 5:
                    avatar.skinColor = "DarkBrown";
                    break;
                case 6:
                    avatar.skinColor = "Black";
                    break;
                default:
                    avatar.skinColor = "Light";
                    break;
            }

            loading.SetActive(true);

            dropdowns[7].value = value;

            updateImage();

            skinColor = value;
        }
    }

    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    public void Start()
    {
        //Common.instance.load(2);
        //initSVG();

        //string svg = svgAsset;

        dropdowns = svgImage.transform.parent.GetChild(1).GetComponentsInChildren<TMP_Dropdown>();

        //updateImage();

        svgImage.sprite = AuthManager.profile;

    }

    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/


    public void randomize()
    {
        TopType = UnityEngine.Random.Range(0, 5);
        StopAllCoroutines();
        AccessoriesType = UnityEngine.Random.Range(0, 4);
        StopAllCoroutines();
        FacialHairType = UnityEngine.Random.Range(0, 6);
        StopAllCoroutines();
        ClotheType = UnityEngine.Random.Range(0, 9);
        StopAllCoroutines();
        EyeType = UnityEngine.Random.Range(0, 12);
        StopAllCoroutines();
        EyebrowType = UnityEngine.Random.Range(0, 12);
        StopAllCoroutines();
        MouthType = UnityEngine.Random.Range(0, 12);
        StopAllCoroutines();
        SkinColor = UnityEngine.Random.Range(0, 7);
    }

    bool changedOnce = false;

    private void updateImage()
    {
        changedOnce = true;
        this.getText(avatar.Link, response =>
        {
            var sprite = Common.instance.getSvgImage(response);
            svgImage.sprite = sprite;

            loading.SetActive(false);

        });
    }

    public void goToRoom()
    {
        Common.instance.load(2);
    }

    string overriteProfilePath = "auth/profile/change";

    public void saveChanges()
    {
        if (changedOnce)
        {
            string token = Common.getUserInfo();

            string json = $"^'token':'{token}','path':'{avatar.Link}'@".ToJSON();

            this.fetchText(Common.Baseurl + overriteProfilePath, json,
                response =>
                {
                    AuthManager.profile = svgImage.sprite;
                    PlayerPrefs.SetString("path", avatar.Link);
                },
                error =>
                {
                    Common.alert(LanguageManager.isFR ? "assurez - vous que vous êtes connecté à Internet... ou nos serveurs sont peut - être en maintenance... essayez plus tard" : "make sure you are connected to the internet ... or our servers may be under maintenance ... try later");
                });
        }
    }

    public void logout()
    {
        PlayerPrefs.DeleteKey("token");
        PlayerPrefs.DeleteKey("username");
        Common.instance.load(0);
    }

}
