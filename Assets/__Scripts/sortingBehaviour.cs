﻿/*

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sortingBehaviour : Singleton<sortingBehaviour>
{

    /**********************************************/
    /*              PUBLIC VARIABLES              */
    /**********************************************/

    public Animator sortpanel;
    public Transform parent;

    /**********************************************/
    /*              STATIC VARIABLES              */
    /**********************************************/


    /**********************************************/
    /*              PRIVATE VARIABLES             */
    /**********************************************/

    bool isActive = false;


    /**********************************************/
    /*                   INIT                     */
    /**********************************************/

    void Start()
    {
        
    }

    /**********************************************/
    /*                   LOOP                     */
    /**********************************************/


    /**********************************************/
    /*                 METHODS                    */
    /**********************************************/

    public void toggleSortPanel() 
    {
        sortpanel.Play(!isActive ? "show" : "hide");

        isActive = !isActive;
    }

    Transform[] children { get 
        {
            List<Transform> temp = new List<Transform>();

            foreach (Transform child in parent)
            {
                temp.Add(child);
            }

            return temp.ToArray();
        } 
    }

    public void sortByTime()
    {
        Transform[] currentChilds = children;

        var itemMoved = false;
        do
        {
            itemMoved = false;
            for (int i = 0; i < currentChilds.Length - 1; i++)
            {
                if (currentChilds[i].GetComponent<IdeaUiBehaviour>().time > currentChilds[i + 1].GetComponent<IdeaUiBehaviour>().time)
                {
                    var lowerValue = currentChilds[i + 1];
                    currentChilds[i + 1] = currentChilds[i];
                    currentChilds[i] = lowerValue;
                    itemMoved = true;
                }
            }
        } while (itemMoved);

        foreach (var item in currentChilds)
        {
            item.SetAsLastSibling();
            //print(item.name);
        }
        toggleSortPanel();


    }

    public void sortBySuccess()
    {
        Transform[] currentChilds = children;


        var itemMoved = false;
        do
        {
            itemMoved = false;
            for (int i = 0; i < currentChilds.Length - 1; i++)
            {
                if (currentChilds[i].GetComponent<IdeaUiBehaviour>().likes() > currentChilds[i + 1].GetComponent<IdeaUiBehaviour>().likes())
                {
                    var lowerValue = currentChilds[i + 1];
                    currentChilds[i + 1] = currentChilds[i];
                    currentChilds[i] = lowerValue;
                    itemMoved = true;
                }
            }
        } while (itemMoved);

        foreach (var item in currentChilds)
        {
            item.SetAsLastSibling();
            //print(item.name);
        }

        toggleSortPanel();
    }

    public void sortByUsers()
    {
        Transform[] currentChilds = children;


        var itemMoved = false;
        do
        {
            itemMoved = false;
            for (int i = 0; i < currentChilds.Length - 1; i++)
            {
                if ((currentChilds[i].GetComponent<IdeaUiBehaviour>().content[0] & 31) > ( currentChilds[i + 1].GetComponent<IdeaUiBehaviour>().content[0] & 31))
                {
                    var lowerValue = currentChilds[i + 1];
                    currentChilds[i + 1] = currentChilds[i];
                    currentChilds[i] = lowerValue;
                    itemMoved = true;
                }
            }
        } while (itemMoved);

        foreach (var item in currentChilds)
        {
            item.SetAsLastSibling();
            //print(item.name);
        }

        toggleSortPanel();
    }

}
