﻿using UnityEngine;

public class ContextTesting : MonoBehaviour
{
#if UNITY_EDITOR
    /// Add a context menu named "Do Something" in the inspector
    /// of the attached script.
    [ContextMenu("Place Table")]
    void PlaceTable()
    {
        TapToPlace.instance.editorTest();
    }

    /// Add a context menu named "Do Something" in the inspector
    /// of the attached script.
    [ContextMenu("Place Object")]
    void Placeobject()
    {
        PlacementSystem.instance.place();
    }
#endif
}
