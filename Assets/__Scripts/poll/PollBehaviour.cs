﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using TextM = TMPro.TextMeshProUGUI;

[System.Serializable]
public class FileQuestion
{
    public string url = "";
}

[System.Serializable]
public class QuestionAnswered
{
    public Question question;
    public string[] answers;
    public bool hasAnswered = false;

    public QuestionAnswered(Question question)
    {
        this.question = question;
    }
}


[System.Serializable]
public class PollAnswered
{
    public string token;
    public string _id;
    public QuestionAnswered[] questions;

}


[System.Serializable]
public class Question
{
    public string question, comment;
    public FileQuestion file;
    public int type;
    public string[] answers;
}


[System.Serializable]
public class Poll
{
    public string title, objectives, _id,pin;
    public Question[] questions;
}

[System.Serializable]
public class PollResponse
{
    public Poll[] polls;
    public bool success;
}

[System.Serializable]
public class PollRequest
{
    public string token;
    public string id;

    public PollRequest(string token)
    {
        this.token = token;

    }

    public PollRequest(string token, string id) : this(token)
    {
        this.id = id;
    }
}

public class PollBehaviour : Singleton<PollBehaviour>
{

    public string pollsUrl = "api/poll/get";

    public GameObject polls, pollPrefab, QuestionPrefab, singleAnswerPrefab, multipleAnswerPrefab, freeAnswerPrefab, evalPrefab, prioPrefab;

    public PollResponse pr;
    public PollAnswered pA;


    public bool isDone = false;

    void Start()
    {
        if (!PinInput.brainstorming.isOwner)
        {
            return;
        }

        string token = Common.getUserInfo();

        if (token == "")
        {
            return;
        }

        if (PinInput.brainstorming.isPoll)
        {
            string jsonStr = JsonUtility.ToJson(new PollRequest(token,PinInput.brainstorming._id));

            this.fetchText(Common.Baseurl + pollsUrl, jsonStr, response =>
            {
                print(response);

                pr = JsonUtility.FromJson<PollResponse>(response);

                if (!pr.success)
                {
                    Common.alert(LanguageManager.isFR ? "il y a eu un problème, veuillez vous assurer que vous avez des sondages dans votre compte ou signaler le bogue, merci." : "There has been a problem, please make sure you have polls in your account or report the bug, thank you.");
                }

            });
        }
    }

    public void showPolls()
    {

        if (!PinInput.brainstorming.isOwner)
        {
            return;
        }

        if (pr.success)
        {
            Poll poll = null;

            foreach (var item in pr.polls)
            {
                if(item.pin == PinInput.brainstorming.pin)
                {
                    poll = item;
                }
            }

            if(poll == null || poll.questions.Length == 0)
            {
                Common.alert(LanguageManager.isFR ? "Il n'y a pas de questions dans ce sondage" : "There are no questions in this poll");
                return;
            }

            transform.GetChild(0).GetComponent<Animator>().Play("hide");
            Destroy(transform.GetChild(0).gameObject, 1);

            NetworkingClient.s.emit("startPoll", JsonUtility.ToJson(poll));

            startPoll(poll);
        }
        else
        {
            Common.alert(LanguageManager.isFR ? "une erreur s'est produite, réessayez plus tard" : "an error has occurred, try again later");
        }
    }

    public GameObject ideasButton;

    public void startPoll(Poll poll)
    {
        pA = new PollAnswered
        {
            _id = poll._id,
            token = Common.getUserInfo(),
            questions = new QuestionAnswered[poll.questions.Length]
        };

        ideasButton.SetActive(false);

        renderQuestion(0, poll.questions);

    }

    private void renderQuestion(int index, Question[] questions)
    {
        GameObject temp = Instantiate(QuestionPrefab, transform);

        TextM[] texts = temp.GetComponentsInChildren<TextM>(true);

        texts[0].text = questions[index].question;
        texts[1].text = questions[index].comment;
        texts[2].text = "Type: " + (questions[index].type == 1 ? "Réponse libre" : questions[index].type == 2 ? "Choix multiple" : questions[index].type == 3 ? "Choix unique" : questions[index].type == 4 ? "Priorisation" : questions[index].type == 5 ? "Evaluation" : "");

        temp.GetComponentInChildren<RawImage>().texture = Resources.Load<Texture2D>("brainstorming");

        if (questions[index].file.url == "" || !questions[index].file.url.StartsWith("http"))
        {
            temp.GetComponentInChildren<RawImage>().texture = Resources.Load<Texture2D>("brainstorming");
        }
        else
        {
            this.fetchImage(questions[index].file.url, texture => {

                temp.GetComponentInChildren<RawImage>().texture = texture;

                temp.GetComponentInChildren<RawImage>().fit(200, 200);

            });
        }

        QuestionAnswered qa = new QuestionAnswered(questions[index]);

        pA.questions[index] = qa;

        //bool forcePass = false;

        temp.GetComponentInChildren<Button>().onClick.AddListener(delegate
        {
            if (!pA.questions[index].hasAnswered)
            {
                return;
                //if (!forcePass)
                //{
                //    return;
                //}
            }

            //StopAllCoroutines();

            if (index == questions.Length - 1)
            {
                Common.alert(LanguageManager.isFR ? "Vous avez répondu à toutes les questions du sondage, veuillez attendre les autres" : "You have answered all the questions in the poll , please wait for the others");
                
                print("emmitting poll answererd");

                NetworkingClient.s.emit("pollAnswered", JsonUtility.ToJson(pA));

                isDone = true;
            }
            else
            {
                renderQuestion(index + 1, questions);
            }

            temp.GetComponent<Animator>().Play("hide");

            Destroy(temp, 1);
        });

        renderAnswers(questions[index], questions[index].type, temp.transform.GetChild(0).GetChild(3), index);



    }

    Dictionary<int, TMP_InputField> orderDict = new Dictionary<int, TMP_InputField>();
    Dictionary<string, int> evalsDict = new Dictionary<string , int>();

    private void renderAnswers(Question question, int type, Transform t, int index)
    {


        GameObject pref = type == 1 ? freeAnswerPrefab : type == 2 ? multipleAnswerPrefab : type == 3 ? singleAnswerPrefab : type == 4 ? prioPrefab : type == 5 ? evalPrefab : null;

        if (type == 1)
        {
            t.GetComponent<GridLayoutGroup>().cellSize = Vector2.one * 1000;
            GameObject temp = Instantiate(pref, t);

            temp.GetComponentInChildren<TMP_InputField>().onDeselect.AddListener(value =>
            {
                pA.questions[index].hasAnswered = true;

                string[] tempArray = new string[1];

                tempArray[0] = value;

                pA.questions[index].answers = tempArray;
            });

            return;
        }


        if (type == 4)
        {
            foreach (var answer in question.answers)
            {
                GameObject temp = Instantiate(pref, t);

                pA.questions[index].answers = new string[0];

                TMP_InputField input = temp.GetComponentInChildren<TMP_InputField>();


                input.onValueChanged.AddListener(value =>
                {

                    if (value == "")
                    {
                        return;
                    }


                    int order = int.Parse(value);

                    if (order == 0 || order > question.answers.Length)
                    {
                        input.text = "";
                        return;
                    }


                    if (!orderDict.TryGetValue(order,out TMP_InputField ti))
                    {
                        orderDict.Add(order, input);
                    }
                    else if(!ti.Equals(input))
                    {
                        orderDict.Remove(order);
                        ti.text = "";
                        orderDict.Add(order, input);
                    }
                    else
                    {
                        orderDict.Remove(order);
                        orderDict.Add(order, input);
                    }

                    //print(String.Join("_", orderDict.Keys.ToArray()));

                    if(orderDict.Count == question.answers.Length)
                    {
                        pA.questions[index].hasAnswered = true;

                        List<string> temps = new List<string>();

                        for (int i = 1; i <= question.answers.Length; i++)
                        {
                            temps.Add(i + "_" + question.answers[i-1]);
                        }

                        pA.questions[index].answers = temps.ToArray();
                    }
                    else
                    {
                        pA.questions[index].hasAnswered = false;
                        pA.questions[index].answers = new string[0];
                    }


                });

                temp.GetComponentsInChildren<TextM>().Last().text = answer;

            }


            return;
        }

        if (type == 5)
        {
            foreach (var answer in question.answers)
            {
                GameObject temp = Instantiate(pref, t);

                pA.questions[index].answers = new string[0];

                TMP_InputField input = temp.GetComponentInChildren<TMP_InputField>();


                input.onValueChanged.AddListener(value =>
                {

                    if (value == "")
                    {
                        return;
                    }


                    int eval = int.Parse(value);

                    if (eval == 0 || eval > 5)
                    {
                        input.text = "";
                        return;
                    }


                    if (!evalsDict.TryGetValue(answer,out int x))
                    {
                        evalsDict.Add(answer, eval);
                    }
                    else
                    {
                        evalsDict[answer] = eval;
                    }

                    
                    if (evalsDict.Count == question.answers.Length)
                    {
                        pA.questions[index].hasAnswered = true;

                        List<string> temps = new List<string>();

                        for (int i = 0; i < question.answers.Length; i++)
                        {
                            temps.Add(evalsDict[question.answers[i]] + "_" + question.answers[i]);
                        }

                        pA.questions[index].answers = temps.ToArray();
                    }
                    else
                    {
                        pA.questions[index].hasAnswered = false;
                        pA.questions[index].answers = new string[0];
                    }


                });

                temp.GetComponentsInChildren<TextM>().Last().text = answer;

            }


            return;
        }


        ToggleGroup tg = t.GetComponent<ToggleGroup>();


        foreach (var answer in question.answers)
        {
            GameObject temp = Instantiate(pref, t);

            pA.questions[index].answers = new string[0];

            if (type == 3)
            {
                temp.GetComponent<Toggle>().group = tg;

            }

            temp.GetComponent<Toggle>().onValueChanged.AddListener(value =>
            {
                pA.questions[index].hasAnswered = true;

                if (value)
                {

                    string[] tempArray = new string[pA.questions[index].answers.Length + 1];

                    //tempArray = pA.questions[index].answers;

                    for (int i = 0; i < tempArray.Length - 1; i++)
                    {
                        tempArray[i] = pA.questions[index].answers[i];
                    }

                    tempArray[pA.questions[index].answers.Length] = answer;

                    pA.questions[index].answers = tempArray;


                }
                else
                {
                    int indexToRemove = Array.IndexOf(pA.questions[index].answers, answer);

                    //print(indexToRemove);

                    pA.questions[index].answers = pA.questions[index].answers.Where((source, ind) => ind != indexToRemove).ToArray();
                }
            });

            temp.GetComponentInChildren<TextM>().text = answer;

        }
    }

    
}
