﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Stats
{
    public int voted;
    public bool done;
    public StatsQuestion[] stats;
}

[System.Serializable]
public class StatsQuestion
{
    public string question, comment;
    public int type;
    public StatsAnswer[] answers;
}

[System.Serializable]
public class StatsAnswer
{
    public string answer;
    public int number;
}

public class NetworkPoll : Singleton<NetworkPoll>
{
    public Stats stats;

    public Button voting;

    NetworkingClient nc;

    public GameObject votes;


    public Button votesButton;

    private void Start()
    {
        nc = NetworkingClient.instance;

        NetworkingClient.s.on("startPoll", (E) =>
         {
             print(E);
             var data = new JSONObject(E.ToJSON());
             Poll poll = JsonUtility.FromJson<Poll>(data.ToString());

             PollBehaviour.instance.startPoll(poll);


         });

        NetworkingClient.s.on("pollAnswered", (E) =>
        {
            //print(E.data.ToString());

            //print($"how many people are in the room {NetworkingClient.instance.serverObjects.Count}");
            print(E);
            var data = new JSONObject(E.ToJSON());
            stats = JsonUtility.FromJson<Stats>(data.ToString());

            if (stats.voted == NetworkingClient.serverObjects.Count)
            {
                if (PinInput.brainstorming.isOwner)
                {
                    foreach (var item in stats.stats)
                    {
                        SessionManager.instance.AddVote(item.question, item.comment, item.answers);
                    }
                }
                else
                {
                    votes.SetActive(false);
                }


                Common.alert(LanguageManager.isFR ? "Tout le monde a répondu à toutes les questions du sondage, vous pouvez trouver le résultat dans la section idées du menu Équipes" : "Everyone have answered all the questions in the poll , you can Find the result in the ideas section of the Teams menu", () =>
                {
                    if (PinInput.brainstorming.isOwner)
                    {
                        Common.instance.toggleTeamPanel();
                        votesButton.onClick.Invoke();
                    }
                });

            }
            else if (stats.voted < NetworkingClient.serverObjects.Count)
            {
                int waiting = NetworkingClient.serverObjects.Count - stats.voted;

                string s = waiting == 1 ? "" : "s";

                if (PollBehaviour.instance.isDone)
                    Common.alert(LanguageManager.isFR ? $"nous attendons toujours {waiting} autre{s} pour terminer le sondage" : $"we are still waiting for {waiting} more to complete the poll");
            }

        });


    }
}
