﻿/*

*/

#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MyMenu 
{

    [MenuItem("GameObject/Place Table", false, 10)]
    public static void PlaceTable()
    {
        TapToPlace.instance.editorTest();
    }

    [MenuItem("GameObject/Place Object", false, 10)]
    public static void PlaceObject()
    {
        PlacementSystem.instance.place();
    }

}

#endif
