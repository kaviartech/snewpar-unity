﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Networking;


public class nativeGalleryImporterEvent : MonoBehaviour
{
    string addEmail = "https://portal.kaviar.app/Apps/Gate/events_mails.php";
  
    public static Dictionary<int, Texture> texturesEvent2 =  new Dictionary<int, Texture>();
    public string[] urlsEvent2;

    private IEnumerator Start()
    {
        
            yield return StartCoroutine(getEvent2Tex(urlsEvent2));

    }
   
    public IEnumerator getEvent2Tex(string[] urls)
    {
        for (int i = 0; i < urls.Length; i++)
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(urls[i]);
            www.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            www.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            www.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            www.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return www.SendWebRequest();
#if !UNITY_WEBGL
            while (!www.isDone)
            {
                yield return www;
            }
#endif

            Texture myTexture = DownloadHandlerTexture.GetContent(www);
            if (!texturesEvent2.ContainsKey(i))
                texturesEvent2.Add(i, myTexture);

            www.Dispose();
        }
    }

  
   
    

}
