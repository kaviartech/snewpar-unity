﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SpatialTracking;
using UnityEngine.XR.ARFoundation;

public class testingBehaviour : MonoBehaviour
{

    public PlaceMultipleObjectsOnPlane p;
    public TapToPlace tp;

    public ARRaycastManager ar;

    public ARPlaneManager ap;

    public ARSessionOrigin aSe;

    public TrackedPoseDriver td;

    public ARCameraManager ac;

    public ARCameraBackground ab;


    private void Awake()
    {

        if(TryGetComponent<PlaceMultipleObjectsOnPlane>(out p))
        {
            p.enabled = true;
            Debug.Log(p.enabled);
        }
         if(TryGetComponent<TapToPlace>(out tp))
        {
            tp.enabled = true;
            Debug.Log(tp.enabled);
        }
        ar.enabled = true;

        ap.enabled = true;

        aSe.enabled = true;

        td.enabled = true;

        ac.enabled = true;

        ab.enabled = true;
       
        Debug.Log(ar.enabled);
        Debug.Log(ap.enabled);
        Debug.Log(aSe.enabled);
        Debug.Log(td.enabled);
        Debug.Log(ac.enabled);
        Debug.Log(ab.enabled);

    }

}
