using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    public float radius;
    [Range(0,360)]
    public float angle;
    [Range(0, 360)]
    public float angleOutSide;

    public GameObject[] playerRef;

    public LayerMask targetMask;
    public LayerMask obstructionMask;
   
    public bool isinside;

    private void Start()
    {
        //playerRef = GameObject.FindGameObjectsWithTag("Respawn");
        StartCoroutine(FOVRoutine());
    }

    private IEnumerator FOVRoutine()
    {
        WaitForSeconds wait = new WaitForSeconds(0.2f);

        while (true)
        {
            yield return wait;
            FieldOfViewCheck();
        }
    }

    private void FieldOfViewCheck()
    {
        playerRef = GameObject.FindGameObjectsWithTag("Hotspot");

        Collider[] rangeChecks = Physics.OverlapSphere(transform.position, radius, targetMask);

        if (rangeChecks.Length != 0)
        {
            Debug.Log(rangeChecks.Length);
            isinside = portalManager.isInside;

            if (portalManager.isInside)
            {
                for (int i = 0; i < rangeChecks.Length; i++)
                {

                    Transform target = rangeChecks[i].transform;
                    Vector3 directionToTarget = (target.position - transform.position).normalized;

                    if (Vector3.Angle(transform.forward, directionToTarget) < angle / 2)
                    {
                        float distanceToTarget = Vector3.Distance(transform.position, target.position);

                        if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, obstructionMask))
                        {
                            if (target.childCount == 0)
                                target.GetComponentInChildren<Renderer>().enabled = true;
                            else
                                foreach (Renderer renderer in target.GetComponentsInChildren<Renderer>())
                                {
                                    renderer.enabled = true;
                                }
                        }
                        else
                        {
                            if (target.childCount == 0)
                                target.GetComponentInChildren<Renderer>().enabled = false;
                            else
                                foreach (Renderer renderer in target.GetComponentsInChildren<Renderer>())
                                {
                                    renderer.enabled = false;
                                }
                        }
                    }
                    else
                    {
                        if (target.childCount == 0)
                            target.GetComponentInChildren<Renderer>().enabled = false;
                        else
                            foreach (Renderer renderer in target.GetComponentsInChildren<Renderer>())
                            {
                                renderer.enabled = false;
                            }
                    }
                }
            }else
            {
                isinside = portalManager.isInside;
                for (int i = 0; i < rangeChecks.Length; i++)
                {

                    Transform target = rangeChecks[i].transform;
                    Vector3 directionToTarget = (target.position - transform.position).normalized;

                    if (Vector3.Angle(transform.forward, directionToTarget) < angleOutSide / 2)
                    {
                        float distanceToTarget = Vector3.Distance(transform.position, target.position);

                        if (Physics.Raycast(transform.position, directionToTarget, distanceToTarget, obstructionMask))
                        {
                            if (target.childCount == 0)
                                target.GetComponentInChildren<Renderer>().enabled = true;
                            else
                                foreach (Renderer renderer in target.GetComponentsInChildren<Renderer>())
                                {
                                    renderer.enabled = true;
                                }
                        }
                        else
                        {
                            if (target.childCount == 0)
                                target.GetComponentInChildren<Renderer>().enabled = false;
                            else
                                foreach (Renderer renderer in target.GetComponentsInChildren<Renderer>())
                                {
                                    renderer.enabled = false;
                                }
                        }
                    }
                    else
                    {
                        if (target.childCount == 0)
                            target.GetComponentInChildren<Renderer>().enabled = false;
                        else
                            foreach (Renderer renderer in target.GetComponentsInChildren<Renderer>())
                            {
                                renderer.enabled = false;
                            }
                    }
                }
            }
        }
    }
}
