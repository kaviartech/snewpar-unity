using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.UI;

using UnityEngine.Rendering;




public class EventTexture : MonoBehaviour
{
    public string url;
    public string urlFr;
    public string urlUk;
    public bool raw;
    public bool img;
    public bool clip;
    public bool text;
    public bool multilang;
    public static bool mediaLoaded;
    public static int loadedMediaCount;
    // Start is called before the first frame update
    public void Awake()
    {
        loadedMediaCount = 0;
        mediaLoaded = false;
       
    }
    public IEnumerator Start()
    {
       yield return StartCoroutine(GetTexture());
    }

    public IEnumerator GetTexture()
    {
        if(text)
        {
            UnityWebRequest www = UnityWebRequest.Get(url);
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                GetComponent<TextMeshProUGUI>().text = www.downloadHandler.text;
            }
            www.Dispose();
        }
        else if (!clip)
        {
            UnityWebRequest www;

            if (multilang)
                www = UnityWebRequestTexture.GetTexture(PlayerPrefs.GetInt("isFR") == 1 ? urlFr : urlUk);
            else
                www = UnityWebRequestTexture.GetTexture(url);
            www.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            www.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            www.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            www.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return www.SendWebRequest();
#if !UNITY_WEBGL
            while (!www.isDone)
            {
                yield return www;
            }
#endif
            Texture myTexture = DownloadHandlerTexture.GetContent(www);
            foreach (var mat in GetComponent<Renderer>().materials)
            {
                mat.mainTexture = myTexture;
            }
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * (int)myTexture.height / myTexture.width);
            www.Dispose();

            
            mediaLoaded = true;
        }
        else
        {
            AudioType audioType;
            if (url.Contains("mp3"))
                audioType = AudioType.MPEG;
            else
                audioType = AudioType.WAV;
            UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(url,audioType);
            www.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            www.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            www.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            www.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return www.SendWebRequest();
#if !UNITY_WEBGL
            while (!www.isDone)
            {
                yield return www;
            }
#endif
            AudioClip myClip = DownloadHandlerAudioClip.GetContent(www);
            Debug.Log(myClip);
            GetComponent<TapToPlace>().clip = myClip;
            GetComponent<AudioSource>().clip = myClip;
            www.Dispose();
            mediaLoaded = true;
        }
    }
}
