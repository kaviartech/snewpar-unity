using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eventHelp : MonoBehaviour
{
    private void Awake()
    {
        
        if(PlayerPrefs.GetInt("eventHelp", 1) == 0)
        {
            gameObject.SetActive(false);
        }
    }

    public void dontShowAgain()
    {
        PlayerPrefs.SetInt("eventHelp", 0);
    }

    private void Update()
    {
        if (PlayerPrefs.GetInt("eventHelp", 1) == 1 && portalManager.isInside)
        {
            gameObject.SetActive(true);
        }
    }
}
