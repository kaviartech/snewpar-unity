﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.EventSystems;


[RequireComponent(typeof(ARRaycastManager))]
public class PlaceMultipleObjectsOnPlane : MonoBehaviour
{
    //public GameObject eventHelp;

    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
     GameObject m_PlacedPrefab;


    public GameObject ttt;

    public Material mt;

    public bool ableToPlace = false;

    public string type;

    public AudioClip clip;

    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject placedPrefab
    {
        get { return m_PlacedPrefab; }
        set { m_PlacedPrefab = value; }
    }

    public Texture2D texture;

    public Texture2D tp;

    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public GameObject spawnedObject { get; set; }
    public GameObject spawnedObjectScene { get; set; }
    public bool isPlaced;
    /// <summary>
    /// Invoked whenever an object is placed in on a plane.
    /// </summary>
    public static event Action onPlacedObject;

    ARRaycastManager m_RaycastManager;
    private ARPlaneManager _arPlaneManager;

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    private Vector2 touchPosition;


    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;
    }
    void Awake()
    {
        //if(SessionManager.isBilboard)
        //  placedPrefab = Resources.Load("bilboard", typeof(GameObject)) as GameObject;
        //else if (SessionManager.isBall)
        //    placedPrefab = Resources.Load("ball", typeof(GameObject)) as GameObject;

        m_RaycastManager = GetComponent<ARRaycastManager>();
        _arPlaneManager = GetComponent<ARPlaneManager>();

        _arPlaneManager.enabled =  true;

    }

    

    void Update()
    {
        if (_arPlaneManager.trackables.count > 0 && !SessionManager.once)
        {
            helpSystem.moveDevice(true, false);
            helpSystem.tapToPlace(true, true);
            ableToPlace = true;
            SessionManager.once = true;
        }
        if (!TryGetTouchPosition(out touchPosition)) return;

        if (Common.IsPointerOverUIObject(touchPosition)) return;

        if (Gestures.instance.GesturesReady) return;

        if (ableToPlace)
        {
            
            int i = 0;
#if UNITY_EDITOR
            spawnedObject = Instantiate(m_PlacedPrefab, Vector3.zero, transform.rotation);
            PlacementSystem.instance.spawnedObject = spawnedObject;

            PlacementSystem.instance.parent = spawnedObject;
           
                
                isPlaced  = true;
            //if(PlayerPrefs.GetInt("eventHelp", 1) == 1)
            //    eventHelp.SetActive(true);

            //Gestures.instance.GesturesReady = true;
            ableToPlace = false;

#endif
            if (Input.touchCount > 0)
            {

                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {

                    if (m_RaycastManager.Raycast(touch.position, s_Hits, TrackableType.PlaneWithinPolygon))
                    {
                        Pose hitPose = s_Hits[0].pose;
                        Vector3 lookDir = hitPose.position - Camera.main.transform.position * 2;
                        lookDir.y = 0;
                        spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, Quaternion.LookRotation(lookDir));
                        PlacementSystem.instance.spawnedObject = spawnedObject;

                        PlacementSystem.instance.parent = spawnedObject;
                      
                        ARPlaneManager ap =  GetComponent<ARPlaneManager>();

                        ap.enabled = false;
                        foreach (var plane in ap.trackables)
                            plane.gameObject.SetActive(false);
                        networkPosition x = networkPosition.instance;

                        x.enabled = true;

                        //Gestures.instance.GesturesReady = true;

                        helpSystem.tapToPlace(true, false);
                        ableToPlace = false;

                        if (onPlacedObject != null)
                        {
                            onPlacedObject();
                        }
                    }
                }
            }
        }
    }
}
