using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Networking;

public class goTo : MonoBehaviour, IPointerClickHandler
{
    public string url;
    // Start is called before the first frame update
    public bool go;

    public IEnumerator Start()
    {
        if (!go)
        {
            UnityWebRequest www = UnityWebRequest.Get(url);
            www.SetRequestHeader("Access-Control-Allow-Credentials", "true");
            www.SetRequestHeader("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            www.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            www.SetRequestHeader("Access-Control-Allow-Origin", "*");
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                url = www.downloadHandler.text;
            }
        }
    }

    public void goToUrl()
    {
        Application.OpenURL(url);
    }
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        //Output to console the clicked GameObject's name and the following message. You can replace this with your own actions for when clicking the GameObject.

        Debug.Log("raycasted");
        //Debug.Log(pointerEventData.pointerClick.transform.GetComponent<ChangePano>().id);
        //pointerEventData.pointerClick.transform.GetComponent<ChangePano>().activateChange();
        //pointerEventData.pointerClick.GetComponent<SphereCollider>().enabled = false;
        Debug.Log(pointerEventData.pointerClick.name + " Game Object Clicked!");
        Debug.Log(pointerEventData.pointerClick.transform.childCount);
        if (go)
            goToUrl();
        else
            pointerEventData.pointerClick.transform.GetChild(0).gameObject.SetActive(true);

    }
}
