using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RaycastExp : MonoBehaviour
{
    public Image eventTeleporteImage;
    public GameObject eventTeleporte;
    public float totalDownTime;
    public GameObject helper;
   public bool didTeleporte;
    // Start is called before the first frame update
    void Start()
    {
        didTeleporte = false;
    }

    // Update is called once per frame
    void Update()
    {

        Ray ray;
        RaycastHit hit;
#if UNITY_EDITOR
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (!didTeleporte)
            {
                if (hit.transform.name == "Floor")
                {
                    var hitObjPos = hit.transform.parent.parent.parent.transform.position;
                    eventTeleporte.gameObject.SetActive(true);
                    eventTeleporte.transform.position = new Vector3(hit.point.x, hitObjPos.y + 0.1f, hit.point.z);
                    totalDownTime += Time.deltaTime;
                    Debug.Log("hit pos");
                    Debug.Log(hit.point);
                    eventTeleporteImage.fillAmount = totalDownTime;
                    if (eventTeleporteImage.fillAmount == 1)
                    {
                        Debug.Log("scene pos before change");
                        Debug.Log(hit.transform.parent.parent.parent.transform.position);   
                        hit.transform.parent.parent.parent.transform.position = new Vector3((hitObjPos.x - hit.point.x), hitObjPos.y, (hitObjPos.z- hit.point.z));
                        eventTeleporte.gameObject.SetActive(false);
                        eventTeleporteImage.fillAmount = 0;
                        totalDownTime = 0;
                        Debug.Log("scene pos before change");
                        Debug.Log(hit.transform.parent.parent.parent.transform.position);
                        didTeleporte = true;
                    }
                }
            }
        }
#endif
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Stationary)
            {
                if (!didTeleporte)
                {
                    //var ray = new Ray(Camera.main.transform.position, transform.forward);
                    //RaycastHit hit;
                    ray = Camera.main.ScreenPointToRay(touch.position);
                    if (Physics.Raycast(ray, out hit))
                    {
                        Debug.Log(hit.transform.name);

                        if (hit.transform.name == "Floor")
                        {
                            var hitObjPos = hit.transform.parent.parent.parent.transform.position;
                            eventTeleporte.gameObject.SetActive(true);
                            eventTeleporte.transform.position = new Vector3(hit.point.x, hitObjPos.y + 0.1f, hit.point.z);
                            totalDownTime += Time.deltaTime;
                            Debug.Log("hit pos");
                            Debug.Log(hit.point);
                            eventTeleporteImage.fillAmount = totalDownTime;
                            if (eventTeleporteImage.fillAmount == 1)
                            {
                                Debug.Log("scene pos before change");
                                Debug.Log(hit.transform.parent.parent.parent.transform.position);
                                hit.transform.parent.parent.parent.transform.position = new Vector3((hitObjPos.x - hit.point.x), hitObjPos.y, (hitObjPos.z - hit.point.z));
                                eventTeleporte.gameObject.SetActive(false);
                                eventTeleporteImage.fillAmount = 0;
                                totalDownTime = 0;
                                Debug.Log("scene pos before change");
                                Debug.Log(hit.transform.parent.parent.parent.transform.position);
                                didTeleporte = true;
                            }
                        }
                    }
                }
            }
            
            if(touch.phase ==  TouchPhase.Ended)
            {
                didTeleporte = false;
                eventTeleporte.gameObject.SetActive(false);
                eventTeleporteImage.fillAmount = 0;
                totalDownTime = 0;
            }
        }
        
    }
    

}
