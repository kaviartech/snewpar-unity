using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class TextureLoader : MonoBehaviour
{
    int count;
    public Slider sd;
    public TextMeshProUGUI text;
    EventTexture[] events;
    private void Awake()
    {
        count = 0;
        events = FindObjectsOfType<EventTexture>();
        sd.value = 0;
        for (int i = 0; i < events.Length; i++)
        {
            if (events[i].img || events[i].raw)
                count++;
        }
        text.text = "0%";
    }
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("events count : " + events.Length);
        Debug.Log("events raw  img count : " + count);

       
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("update raw  img count : " + EventTexture.loadedMediaCount);
        sd.value = (float)(EventTexture.loadedMediaCount) / (float)count;
        text.text = (System.Math.Round(sd.value,3) * 100f).ToString()+"%";
        if (sd.value == 1f)
        {
            gameObject.SetActive(false);
            EventTexture.loadedMediaCount = 0;
        }
    }
}
