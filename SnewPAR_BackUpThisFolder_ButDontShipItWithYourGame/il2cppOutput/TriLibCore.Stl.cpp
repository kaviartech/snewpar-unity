﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtualFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct InterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericInterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// System.Action`1<TriLibCore.AssetLoaderContext>
struct Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226;
// System.Action`1<TriLibCore.IContextualizedError>
struct Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30;
// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
struct Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD;
// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64>
struct Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.General.CompoundMaterialKey,TriLibCore.TextureLoadingContext>
struct ConcurrentDictionary_2_t94764B51655C4F04FDAE59E1A6327AFEE05EA292;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,System.Collections.Generic.List`1<TriLibCore.MaterialRendererContext>>
struct ConcurrentDictionary_2_tA783589C825EB0CEA850D32094AAEFFBB3FD5D82;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material>
struct ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.ITexture,TriLibCore.TextureLoadingContext>
struct ConcurrentDictionary_2_tBB5915FD91B3F65141A8C3EC64A14F14EEA3905B;
// TriLibCore.General.ConcurrentDictionary`2<System.String,System.String>
struct ConcurrentDictionary_2_tCF44E0035FB42A2A1DF508A4CE0B233163C23F1E;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,TriLibCore.Interfaces.IModel>
struct Dictionary_2_tE704ACFE7C32537A046D8577F8299D1B52ED0C00;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String>
struct Dictionary_2_t15A9DEF843D5DA84170CD8536BA0EBB039EB4ADF;
// System.Collections.Generic.Dictionary`2<TriLibCore.Interfaces.IModel,UnityEngine.GameObject>
struct Dictionary_2_tADE1FC3F6C786CACD6652C2C7275C3A0FD274A9C;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]>
struct Dictionary_2_t23C2BC333CAB1901F8EC82B59264ED8D028DD1AB;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t9FA6D82CAFC18769F7515BB51D1C56DAE09381C3;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding>
struct Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_tE1603CE612C16451D1E56FF4D4859D4FE4087C28;
// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup>
struct Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983;
// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel>
struct Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710;
// System.Collections.Generic.HashSet`1<UnityEngine.Texture>
struct HashSet_1_t70836788BCAF42568800A162B9F23937F5309AE8;
// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>
struct HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640;
// System.Collections.Generic.IEnumerable`1<TriLibCore.Interfaces.IGeometryGroup>
struct IEnumerable_1_tAA0F967AC7C9F8C5F11D399E14AA456425FBE910;
// System.Collections.Generic.IEnumerable`1<TriLibCore.Interfaces.IModel>
struct IEnumerable_1_t03C6D7F3A96DA6FAE16E0D510D08496F01435357;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rendering.VertexAttribute>
struct IEqualityComparer_1_tCB8B04D567BFC1D22CB3A6BEBC86439C73A31734;
// System.Collections.Generic.IList`1<System.Char>
struct IList_1_tF23041AC58956CDAA98A1DA3D23002DBE4EBE278;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation>
struct IList_1_t7A16CD7EF0938B36E4D20182185F284ECA5F93A2;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera>
struct IList_1_t13EA3E1B6894AF8023B793D65EA2E1ED596B6E82;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>
struct IList_1_t54EA2EAA8FF287B3E144BC90047C3E635336CB4C;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight>
struct IList_1_t95B0FF72887258CDC012A1B81E66B66AF3BBE38E;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>
struct IList_1_t0662D113B996C51F1676FFC848F7B3448D818DB7;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>
struct IList_1_tBAC2F9CBFB365F17F69446225AF2802DEF7B2956;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>
struct IList_1_t2988C79E2C0A953B91ACE72118B299F94ECFEB62;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_tFB8BE2ED9A601C1259EAB8D73D1B3E96EA321FA1;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,TriLibCore.Interfaces.IGeometryGroup>
struct KeyCollection_t5F67525E552F59FDDBF77225ACD1AFE445BAE65E;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,TriLibCore.Interfaces.IModel>
struct KeyCollection_tE72CD5B28675345F9BF175796FBC9BC6302E5275;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3;
// System.Collections.Generic.List`1<System.Threading.Tasks.Task>
struct List_1_t84C257E858DDB8EA0B6269E08AAD9A2A2018A551;
// System.Collections.Generic.Queue`1<TriLibCore.Interfaces.IContextualizedAction>
struct Queue_1_t952DE88AF42216B755D09647735E4235DA7138D4;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t038245E04B5D2A80048D9F8021A23E69A0C9DBAA;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,TriLibCore.Interfaces.IGeometryGroup>
struct ValueCollection_tCCCE4A1A9D973B131015A437780697A2155EAACD;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,TriLibCore.Interfaces.IModel>
struct ValueCollection_t52D0D70A98CDE4A09BB9B7D39C2159B31ECAE027;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E;
// System.Collections.Generic.Dictionary`2/Entry<System.String,TriLibCore.Interfaces.IGeometryGroup>[]
struct EntryU5BU5D_t54FF245C9871CCC2E71905AABD358B1A89642802;
// System.Collections.Generic.Dictionary`2/Entry<System.String,TriLibCore.Interfaces.IModel>[]
struct EntryU5BU5D_t97D3428B6A02CD680A2D45BF34F1488CADFC7A7E;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96;
// System.Collections.Generic.HashSet`1/Slot<UnityEngine.Rendering.VertexAttribute>[]
struct SlotU5BU5D_tAE07F08746129C3374BC8C791AAFFE4C832AFB2A;
// TriLibCore.Mappers.AnimationClipMapper[]
struct AnimationClipMapperU5BU5D_t8E00A18562A07FD65A6E731D8BA6FF48D80BBFD8;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// TriLibCore.Interfaces.IGeometryGroup[]
struct IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711;
// TriLibCore.Interfaces.IModel[]
struct IModelU5BU5D_tE19C9CE57A4C086398F86D3E8410C416B390CC76;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// TriLibCore.Mappers.LipSyncMapper[]
struct LipSyncMapperU5BU5D_t32748FDCB493E8E7550A88244C1CBBB79E54C18A;
// TriLibCore.Mappers.MaterialMapper[]
struct MaterialMapperU5BU5D_tBD3B26C68148AE48AD6F3B44795C7B7B3EE2257B;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// TriLibCore.AssetLoaderContext
struct AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C;
// TriLibCore.AssetLoaderOptions
struct AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6;
// UnityEngine.Avatar
struct Avatar_t7861E57EEE2CF8CC61BD63C09737BA22F7ABCA0F;
// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// System.Globalization.Calendar
struct Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2;
// System.Globalization.CompareInfo
struct CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57;
// System.Globalization.CultureData
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D;
// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A;
// System.Text.Decoder
struct Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC;
// System.Text.DecoderFallback
struct DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.Text.EncoderFallback
struct EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293;
// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095;
// System.Exception
struct Exception_t;
// TriLibCore.Mappers.ExternalDataMapper
struct ExternalDataMapper_t809726D72207DAF57227F4A5D67B9D01394B760A;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// TriLibCore.Geometries.Geometry
struct Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147;
// TriLibCore.General.HumanDescription
struct HumanDescription_t0BD271EF43944EC6940A10C164E94F8C7E750481;
// TriLibCore.Mappers.HumanoidAvatarMapper
struct HumanoidAvatarMapper_t691E00A2CE4455F03562FF79A586CC717D38FB09;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.IFormatProvider
struct IFormatProvider_tC202922D43BFF3525109ABF3FB79625F5646AB52;
// TriLibCore.Interfaces.IGeometryGroup
struct IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262;
// TriLibCore.Interfaces.IModel
struct IModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB;
// TriLibCore.Interfaces.IRootModel
struct IRootModel_t83ED40397FD23448FC9A99336523CC7DE8A841BB;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472;
// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449;
// TriLibCore.Mappers.RootBoneMapper
struct RootBoneMapper_t64AE3E33364A832EE1B74D8B65BC9AA7B448DDA2;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37;
// TriLibCore.Stl.StlGeometry
struct StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF;
// TriLibCore.Stl.StlModel
struct StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA;
// TriLibCore.Stl.StlProcessor
struct StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0;
// TriLibCore.Stl.Reader.StlReader
struct StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A;
// TriLibCore.Stl.StlRootModel
struct StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142;
// TriLibCore.Stl.StlStreamReader
struct StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5;
// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE;
// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B;
// System.String
struct String_t;
// System.Threading.Tasks.Task
struct Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572;
// System.Globalization.TextInfo
struct TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4;
// TriLibCore.Mappers.TextureMapper
struct TextureMapper_tCDB3B0D28AFDBA2BA4A640F75A7227C1B2D10ADD;
// System.Type
struct Type_t;
// TriLibCore.Mappers.UserPropertiesMapper
struct UserPropertiesMapper_t8437A569EBEB9E02E364D9951BE31F9601C55714;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05;

IL2CPP_EXTERN_C RuntimeClass* BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IObject_t2E20027AB39DAC66068F50E33ECB233711D9DA5F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral3063F5F4FAEA0002A9B169DA0D12D52AC6312C1A;
IL2CPP_EXTERN_C String_t* _stringLiteral4F4B57D5A155D08E686DEA8A1E7C1992F9C3675B;
IL2CPP_EXTERN_C String_t* _stringLiteral6F5EC7239B41C242FCB23B64D91DA0070FC1C044;
IL2CPP_EXTERN_C String_t* _stringLiteral795FA97334306AE47C0C9744A2642732E6FED22D;
IL2CPP_EXTERN_C String_t* _stringLiteral8E84C7CBF5EA8FD2E1EDA86178896A7CB3F34B81;
IL2CPP_EXTERN_C String_t* _stringLiteralB0F9A5767B5106090414D118D94D8CC4F1F188C6;
IL2CPP_EXTERN_C String_t* _stringLiteralC3007E4D4C9A026A571CD47422BE823D8A32DC2A;
IL2CPP_EXTERN_C String_t* _stringLiteralE21886A8FD3908720B58122F94229E548DD3A51F;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m53D84560875155F383C73DA9027DE4CC7AC985D5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mEF9914E7A716EC8B3A7FB701A93B07E83F4C798B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_mEF0435E0E1DE72962810AC3082B1AF39B3951155_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m16C7DB0395DCD9EF714A07CDB8934C3FCAE7F50C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m5F5C68AF90E49C38EB7E07DDE14E85B7C3875390_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Values_m2CEBA9C111276D8D06435752E5C5B3598E722CBE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Values_m56D477CD7C6E64384E53F8B4C44C151CB9CADA48_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisIGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_m8A9221E5C3D95DC2EDD7FC163B30C1A3A3929D1A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisIModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB_m8A288987F91069D02641D435D479CADC11764605_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* IGeometryGroup_GetGeometry_TisStlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF_m4B86DA0DDE54E5F1A22E224B3A37C7316E2ACA27_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* StlProcessor_ParseASCII_m297D902C12F8D0B07F75397F81C1BA6A82B918ED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* StlStreamReader_ReadTokenAsFloat_m30C508ABFBF8FF2F47629BCFFB53C042F4B7F551_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* ProcessingSteps_t0ED2265911192A39BA3686E081CB2D9F4DA68078_0_0_0_var;
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_com;
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_pinvoke;
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com;
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711;
struct IModelU5BU5D_tE19C9CE57A4C086398F86D3E8410C416B390CC76;
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t02941931EB822FCAB52150CDE43126D44ACC9F7D 
{
};

// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup>
struct Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t54FF245C9871CCC2E71905AABD358B1A89642802* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t5F67525E552F59FDDBF77225ACD1AFE445BAE65E* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tCCCE4A1A9D973B131015A437780697A2155EAACD* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel>
struct Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t97D3428B6A02CD680A2D45BF34F1488CADFC7A7E* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tE72CD5B28675345F9BF175796FBC9BC6302E5275* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t52D0D70A98CDE4A09BB9B7D39C2159B31ECAE027* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>
struct HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_7;
	// System.Collections.Generic.HashSet`1/Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_tAE07F08746129C3374BC8C791AAFFE4C832AFB2A* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37* ____siInfo_14;
};

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,TriLibCore.Interfaces.IGeometryGroup>
struct ValueCollection_tCCCE4A1A9D973B131015A437780697A2155EAACD  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::_dictionary
	Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* ____dictionary_0;
};

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,TriLibCore.Interfaces.IModel>
struct ValueCollection_t52D0D70A98CDE4A09BB9B7D39C2159B31ECAE027  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::_dictionary
	Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* ____dictionary_0;
};

// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158  : public RuntimeObject
{
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC* ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;
};

// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0  : public RuntimeObject
{
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D* ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;
};
// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095  : public RuntimeObject
{
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2* ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293* ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90* ___decoderFallback_14;
};

// TriLibCore.Geometries.Geometry
struct Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147  : public RuntimeObject
{
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Geometries.Geometry::<OriginalVertexIndices>k__BackingField
	RuntimeObject* ___U3COriginalVertexIndicesU3Ek__BackingField_0;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Geometries.Geometry::<VertexDataIndices>k__BackingField
	RuntimeObject* ___U3CVertexDataIndicesU3Ek__BackingField_1;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Geometries.Geometry::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_2;
	// System.Int32 TriLibCore.Geometries.Geometry::<MaterialIndex>k__BackingField
	int32_t ___U3CMaterialIndexU3Ek__BackingField_3;
	// System.Boolean TriLibCore.Geometries.Geometry::<IsQuad>k__BackingField
	bool ___U3CIsQuadU3Ek__BackingField_4;
	// System.Int32 TriLibCore.Geometries.Geometry::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_5;
	// System.Int32 TriLibCore.Geometries.Geometry::<OriginalIndex>k__BackingField
	int32_t ___U3COriginalIndexU3Ek__BackingField_6;
	// System.Boolean TriLibCore.Geometries.Geometry::_disposed
	bool ____disposed_7;
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449  : public RuntimeObject
{
	// System.String[] TriLibCore.ReaderBase::_loadingStepEnumNames
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____loadingStepEnumNames_1;
	// TriLibCore.AssetLoaderContext TriLibCore.ReaderBase::<AssetLoaderContext>k__BackingField
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___U3CAssetLoaderContextU3Ek__BackingField_2;
	// System.String TriLibCore.ReaderBase::_filename
	String_t* ____filename_3;
	// System.Action`2<TriLibCore.AssetLoaderContext,System.Single> TriLibCore.ReaderBase::_onProgress
	Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ____onProgress_4;
	// System.Int32 TriLibCore.ReaderBase::_nameCounter
	int32_t ____nameCounter_5;
	// System.Int32 TriLibCore.ReaderBase::_materialCounter
	int32_t ____materialCounter_6;
	// System.Int32 TriLibCore.ReaderBase::_textureCounter
	int32_t ____textureCounter_7;
	// System.Int32 TriLibCore.ReaderBase::_geometryGroupCounter
	int32_t ____geometryGroupCounter_8;
	// System.Int32 TriLibCore.ReaderBase::_animationCounter
	int32_t ____animationCounter_9;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.BoneWeight
struct BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F 
{
	// System.Single UnityEngine.BoneWeight::m_Weight0
	float ___m_Weight0_0;
	// System.Single UnityEngine.BoneWeight::m_Weight1
	float ___m_Weight1_1;
	// System.Single UnityEngine.BoneWeight::m_Weight2
	float ___m_Weight2_2;
	// System.Single UnityEngine.BoneWeight::m_Weight3
	float ___m_Weight3_3;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex0
	int32_t ___m_BoneIndex0_4;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex1
	int32_t ___m_BoneIndex1_5;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex2
	int32_t ___m_BoneIndex2_6;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex3
	int32_t ___m_BoneIndex3_7;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED 
{
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::_source
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};
// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_pinvoke
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_com
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int16
struct Int16_tB8EF286A9C33492FA6E6D6E67320BE93E794A175 
{
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// TriLibCore.Stl.StlGeometry
struct StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF  : public Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147
{
};

// TriLibCore.Stl.Reader.StlReader
struct StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A  : public ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449
{
};

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05* ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2* ____asyncActiveSemaphore_4;
};

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// TriLibCore.AssetLoaderContext
struct AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C  : public RuntimeObject
{
	// TriLibCore.AssetLoaderOptions TriLibCore.AssetLoaderContext::Options
	AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* ___Options_0;
	// TriLibCore.ReaderBase TriLibCore.AssetLoaderContext::Reader
	ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* ___Reader_1;
	// System.String TriLibCore.AssetLoaderContext::Filename
	String_t* ___Filename_2;
	// System.String TriLibCore.AssetLoaderContext::FileExtension
	String_t* ___FileExtension_3;
	// System.IO.Stream TriLibCore.AssetLoaderContext::Stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Stream_4;
	// System.String TriLibCore.AssetLoaderContext::BasePath
	String_t* ___BasePath_5;
	// UnityEngine.GameObject TriLibCore.AssetLoaderContext::RootGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___RootGameObject_6;
	// TriLibCore.Interfaces.IRootModel TriLibCore.AssetLoaderContext::RootModel
	RuntimeObject* ___RootModel_7;
	// System.Collections.Generic.Dictionary`2<TriLibCore.Interfaces.IModel,UnityEngine.GameObject> TriLibCore.AssetLoaderContext::GameObjects
	Dictionary_2_tADE1FC3F6C786CACD6652C2C7275C3A0FD274A9C* ___GameObjects_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,TriLibCore.Interfaces.IModel> TriLibCore.AssetLoaderContext::Models
	Dictionary_2_tE704ACFE7C32537A046D8577F8299D1B52ED0C00* ___Models_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String> TriLibCore.AssetLoaderContext::GameObjectPaths
	Dictionary_2_t15A9DEF843D5DA84170CD8536BA0EBB039EB4ADF* ___GameObjectPaths_10;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,System.Collections.Generic.List`1<TriLibCore.MaterialRendererContext>> TriLibCore.AssetLoaderContext::MaterialRenderers
	ConcurrentDictionary_2_tA783589C825EB0CEA850D32094AAEFFBB3FD5D82* ___MaterialRenderers_11;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material> TriLibCore.AssetLoaderContext::LoadedMaterials
	ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2* ___LoadedMaterials_12;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material> TriLibCore.AssetLoaderContext::GeneratedMaterials
	ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2* ___GeneratedMaterials_13;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.ITexture,TriLibCore.TextureLoadingContext> TriLibCore.AssetLoaderContext::LoadedTextures
	ConcurrentDictionary_2_tBB5915FD91B3F65141A8C3EC64A14F14EEA3905B* ___LoadedTextures_14;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.General.CompoundMaterialKey,TriLibCore.TextureLoadingContext> TriLibCore.AssetLoaderContext::MaterialTextures
	ConcurrentDictionary_2_t94764B51655C4F04FDAE59E1A6327AFEE05EA292* ___MaterialTextures_15;
	// TriLibCore.General.ConcurrentDictionary`2<System.String,System.String> TriLibCore.AssetLoaderContext::LoadedExternalData
	ConcurrentDictionary_2_tCF44E0035FB42A2A1DF508A4CE0B233163C23F1E* ___LoadedExternalData_16;
	// System.Collections.Generic.HashSet`1<UnityEngine.Texture> TriLibCore.AssetLoaderContext::UsedTextures
	HashSet_1_t70836788BCAF42568800A162B9F23937F5309AE8* ___UsedTextures_17;
	// System.Collections.Generic.List`1<UnityEngine.Object> TriLibCore.AssetLoaderContext::Allocations
	List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3* ___Allocations_18;
	// System.Boolean TriLibCore.AssetLoaderContext::Async
	bool ___Async_19;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnLoad_20;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnMaterialsLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnMaterialsLoad_21;
	// System.Action`2<TriLibCore.AssetLoaderContext,System.Single> TriLibCore.AssetLoaderContext::OnProgress
	Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___OnProgress_22;
	// System.Action`1<TriLibCore.IContextualizedError> TriLibCore.AssetLoaderContext::OnError
	Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30* ___OnError_23;
	// System.Action`1<TriLibCore.IContextualizedError> TriLibCore.AssetLoaderContext::HandleError
	Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30* ___HandleError_24;
	// System.Object TriLibCore.AssetLoaderContext::CustomData
	RuntimeObject* ___CustomData_25;
	// System.Collections.Generic.List`1<System.Threading.Tasks.Task> TriLibCore.AssetLoaderContext::Tasks
	List_1_t84C257E858DDB8EA0B6269E08AAD9A2A2018A551* ___Tasks_26;
	// System.Threading.Tasks.Task TriLibCore.AssetLoaderContext::Task
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___Task_27;
	// System.Boolean TriLibCore.AssetLoaderContext::HaltTasks
	bool ___HaltTasks_28;
	// UnityEngine.GameObject TriLibCore.AssetLoaderContext::WrapperGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___WrapperGameObject_29;
	// System.Threading.CancellationToken TriLibCore.AssetLoaderContext::CancellationToken
	CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED ___CancellationToken_30;
	// System.Single TriLibCore.AssetLoaderContext::LoadingProgress
	float ___LoadingProgress_31;
	// System.Threading.CancellationTokenSource TriLibCore.AssetLoaderContext::CancellationTokenSource
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ___CancellationTokenSource_32;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnPreLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnPreLoad_33;
	// System.Boolean TriLibCore.AssetLoaderContext::IsZipFile
	bool ___IsZipFile_34;
	// System.String TriLibCore.AssetLoaderContext::<PersistentDataPath>k__BackingField
	String_t* ___U3CPersistentDataPathU3Ek__BackingField_35;
	// System.String TriLibCore.AssetLoaderContext::ModificationDate
	String_t* ___ModificationDate_36;
	// System.Int32 TriLibCore.AssetLoaderContext::LoadingStep
	int32_t ___LoadingStep_37;
	// System.Int32 TriLibCore.AssetLoaderContext::PreviousLoadingStep
	int32_t ___PreviousLoadingStep_38;
	// System.Collections.Generic.Queue`1<TriLibCore.Interfaces.IContextualizedAction> TriLibCore.AssetLoaderContext::<CustomDispatcherQueue>k__BackingField
	Queue_1_t952DE88AF42216B755D09647735E4235DA7138D4* ___U3CCustomDispatcherQueueU3Ek__BackingField_39;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]> TriLibCore.AssetLoaderContext::_bufferPool
	Dictionary_2_t23C2BC333CAB1901F8EC82B59264ED8D028DD1AB* ____bufferPool_40;
	// System.Boolean TriLibCore.AssetLoaderContext::<Completed>k__BackingField
	bool ___U3CCompletedU3Ek__BackingField_41;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// TriLibCore.Stl.StlModel
struct StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA  : public RuntimeObject
{
	// System.String TriLibCore.Stl.StlModel::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Stl.StlModel::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// UnityEngine.Vector3 TriLibCore.Stl.StlModel::<LocalPosition>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalPositionU3Ek__BackingField_2;
	// UnityEngine.Quaternion TriLibCore.Stl.StlModel::<LocalRotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CLocalRotationU3Ek__BackingField_3;
	// UnityEngine.Vector3 TriLibCore.Stl.StlModel::<LocalScale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalScaleU3Ek__BackingField_4;
	// System.Boolean TriLibCore.Stl.StlModel::<Visibility>k__BackingField
	bool ___U3CVisibilityU3Ek__BackingField_5;
	// TriLibCore.Interfaces.IModel TriLibCore.Stl.StlModel::<Parent>k__BackingField
	RuntimeObject* ___U3CParentU3Ek__BackingField_6;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlModel::<Children>k__BackingField
	RuntimeObject* ___U3CChildrenU3Ek__BackingField_7;
	// System.Boolean TriLibCore.Stl.StlModel::<IsBone>k__BackingField
	bool ___U3CIsBoneU3Ek__BackingField_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Stl.StlModel::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_9;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlModel::<Bones>k__BackingField
	RuntimeObject* ___U3CBonesU3Ek__BackingField_10;
	// UnityEngine.Matrix4x4[] TriLibCore.Stl.StlModel::<BindPoses>k__BackingField
	Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___U3CBindPosesU3Ek__BackingField_11;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Stl.StlModel::<MaterialIndices>k__BackingField
	RuntimeObject* ___U3CMaterialIndicesU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Stl.StlModel::<UserProperties>k__BackingField
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___U3CUserPropertiesU3Ek__BackingField_13;
};

// TriLibCore.Stl.StlProcessor
struct StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Stl.StlProcessor::_geometryGroups
	Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* ____geometryGroups_3;
	// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlProcessor::_models
	Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* ____models_4;
	// TriLibCore.Stl.Reader.StlReader TriLibCore.Stl.StlProcessor::_reader
	StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* ____reader_5;
	// System.String TriLibCore.Stl.StlProcessor::_groupName
	String_t* ____groupName_6;
	// System.String TriLibCore.Stl.StlProcessor::_lastGeometryGroupName
	String_t* ____lastGeometryGroupName_7;
	// System.Int32 TriLibCore.Stl.StlProcessor::_lastLoopNumber
	int32_t ____lastLoopNumber_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Stl.StlProcessor::_activeGeometryGroup
	RuntimeObject* ____activeGeometryGroup_9;
	// TriLibCore.Stl.StlGeometry TriLibCore.Stl.StlProcessor::_activeGeometry
	StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* ____activeGeometry_10;
	// System.Int32 TriLibCore.Stl.StlProcessor::_loopNumber
	int32_t ____loopNumber_11;
	// UnityEngine.Vector3 TriLibCore.Stl.StlProcessor::_facetNormal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ____facetNormal_12;
	// UnityEngine.Color TriLibCore.Stl.StlProcessor::_partColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ____partColor_21;
	// UnityEngine.Color TriLibCore.Stl.StlProcessor::_facetColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ____facetColor_22;
	// System.Int32 TriLibCore.Stl.StlProcessor::_floatCount
	int32_t ____floatCount_23;
	// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute> TriLibCore.Stl.StlProcessor::_vertexAttributes
	HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* ____vertexAttributes_24;
};

// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B  : public TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7
{
	// System.IO.Stream System.IO.StreamReader::_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ____stream_3;
	// System.Text.Encoding System.IO.StreamReader::_encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ____encoding_4;
	// System.Text.Decoder System.IO.StreamReader::_decoder
	Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC* ____decoder_5;
	// System.Byte[] System.IO.StreamReader::_byteBuffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____byteBuffer_6;
	// System.Char[] System.IO.StreamReader::_charBuffer
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____charBuffer_7;
	// System.Int32 System.IO.StreamReader::_charPos
	int32_t ____charPos_8;
	// System.Int32 System.IO.StreamReader::_charLen
	int32_t ____charLen_9;
	// System.Int32 System.IO.StreamReader::_byteLen
	int32_t ____byteLen_10;
	// System.Int32 System.IO.StreamReader::_bytePos
	int32_t ____bytePos_11;
	// System.Int32 System.IO.StreamReader::_maxCharsPerBuffer
	int32_t ____maxCharsPerBuffer_12;
	// System.Boolean System.IO.StreamReader::_detectEncoding
	bool ____detectEncoding_13;
	// System.Boolean System.IO.StreamReader::_checkPreamble
	bool ____checkPreamble_14;
	// System.Boolean System.IO.StreamReader::_isBlocked
	bool ____isBlocked_15;
	// System.Boolean System.IO.StreamReader::_closable
	bool ____closable_16;
	// System.Threading.Tasks.Task System.IO.StreamReader::_asyncReadTask
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ____asyncReadTask_17;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// TriLibCore.Stl.StlRootModel
struct StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142  : public StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA
{
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlRootModel::<AllModels>k__BackingField
	RuntimeObject* ___U3CAllModelsU3Ek__BackingField_14;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Stl.StlRootModel::<AllGeometryGroups>k__BackingField
	RuntimeObject* ___U3CAllGeometryGroupsU3Ek__BackingField_15;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Stl.StlRootModel::<AllAnimations>k__BackingField
	RuntimeObject* ___U3CAllAnimationsU3Ek__BackingField_16;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Stl.StlRootModel::<AllMaterials>k__BackingField
	RuntimeObject* ___U3CAllMaterialsU3Ek__BackingField_17;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Stl.StlRootModel::<AllTextures>k__BackingField
	RuntimeObject* ___U3CAllTexturesU3Ek__BackingField_18;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Stl.StlRootModel::<AllCameras>k__BackingField
	RuntimeObject* ___U3CAllCamerasU3Ek__BackingField_19;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Stl.StlRootModel::<AllLights>k__BackingField
	RuntimeObject* ___U3CAllLightsU3Ek__BackingField_20;
};

// TriLibCore.Stl.StlStreamReader
struct StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5  : public StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B
{
	// System.Int32 TriLibCore.Stl.StlStreamReader::_endOfLinePointer
	int32_t ____endOfLinePointer_22;
	// System.Char[] TriLibCore.Stl.StlStreamReader::_charStream
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____charStream_24;
	// System.String TriLibCore.Stl.StlStreamReader::_charString
	String_t* ____charString_25;
	// System.Int32 TriLibCore.Stl.StlStreamReader::_charPosition
	int32_t ____charPosition_26;
	// System.Int32 TriLibCore.Stl.StlStreamReader::<Line>k__BackingField
	int32_t ___U3CLineU3Ek__BackingField_27;
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
struct Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD  : public MulticastDelegate_t
{
};

// TriLibCore.AssetLoaderOptions
struct AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.Boolean TriLibCore.AssetLoaderOptions::UseFileScale
	bool ___UseFileScale_4;
	// System.Single TriLibCore.AssetLoaderOptions::ScaleFactor
	float ___ScaleFactor_5;
	// System.Boolean TriLibCore.AssetLoaderOptions::SortHierarchyByName
	bool ___SortHierarchyByName_6;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportVisibility
	bool ___ImportVisibility_7;
	// System.Boolean TriLibCore.AssetLoaderOptions::Static
	bool ___Static_8;
	// System.Boolean TriLibCore.AssetLoaderOptions::AddAssetUnloader
	bool ___AddAssetUnloader_9;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportMeshes
	bool ___ImportMeshes_10;
	// System.Boolean TriLibCore.AssetLoaderOptions::LimitBoneWeights
	bool ___LimitBoneWeights_11;
	// System.Boolean TriLibCore.AssetLoaderOptions::ReadEnabled
	bool ___ReadEnabled_12;
	// System.Boolean TriLibCore.AssetLoaderOptions::ReadAndWriteEnabled
	bool ___ReadAndWriteEnabled_13;
	// System.Boolean TriLibCore.AssetLoaderOptions::MarkMeshesAsDynamic
	bool ___MarkMeshesAsDynamic_14;
	// System.Boolean TriLibCore.AssetLoaderOptions::OptimizeMeshes
	bool ___OptimizeMeshes_15;
	// System.Boolean TriLibCore.AssetLoaderOptions::GenerateColliders
	bool ___GenerateColliders_16;
	// System.Boolean TriLibCore.AssetLoaderOptions::ConvexColliders
	bool ___ConvexColliders_17;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportBlendShapes
	bool ___ImportBlendShapes_18;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportColors
	bool ___ImportColors_19;
	// UnityEngine.Rendering.IndexFormat TriLibCore.AssetLoaderOptions::IndexFormat
	int32_t ___IndexFormat_20;
	// System.Single TriLibCore.AssetLoaderOptions::LODScreenRelativeTransitionHeightBase
	float ___LODScreenRelativeTransitionHeightBase_21;
	// System.Boolean TriLibCore.AssetLoaderOptions::KeepQuads
	bool ___KeepQuads_22;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportNormals
	bool ___ImportNormals_23;
	// System.Boolean TriLibCore.AssetLoaderOptions::GenerateNormals
	bool ___GenerateNormals_24;
	// System.Boolean TriLibCore.AssetLoaderOptions::GenerateTangents
	bool ___GenerateTangents_25;
	// System.Single TriLibCore.AssetLoaderOptions::SmoothingAngle
	float ___SmoothingAngle_26;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportBlendShapeNormals
	bool ___ImportBlendShapeNormals_27;
	// System.Boolean TriLibCore.AssetLoaderOptions::CalculateBlendShapeNormals
	bool ___CalculateBlendShapeNormals_28;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportTangents
	bool ___ImportTangents_29;
	// System.Boolean TriLibCore.AssetLoaderOptions::SwapUVs
	bool ___SwapUVs_30;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportMaterials
	bool ___ImportMaterials_31;
	// TriLibCore.Mappers.MaterialMapper[] TriLibCore.AssetLoaderOptions::MaterialMappers
	MaterialMapperU5BU5D_tBD3B26C68148AE48AD6F3B44795C7B7B3EE2257B* ___MaterialMappers_32;
	// System.Boolean TriLibCore.AssetLoaderOptions::AddSecondAlphaMaterial
	bool ___AddSecondAlphaMaterial_33;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportTextures
	bool ___ImportTextures_34;
	// System.Boolean TriLibCore.AssetLoaderOptions::Enforce16BitsTextures
	bool ___Enforce16BitsTextures_35;
	// System.Boolean TriLibCore.AssetLoaderOptions::ScanForAlphaPixels
	bool ___ScanForAlphaPixels_36;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseAlphaMaterials
	bool ___UseAlphaMaterials_37;
	// TriLibCore.General.AlphaMaterialMode TriLibCore.AssetLoaderOptions::AlphaMaterialMode
	int32_t ___AlphaMaterialMode_38;
	// System.Boolean TriLibCore.AssetLoaderOptions::DoubleSidedMaterials
	bool ___DoubleSidedMaterials_39;
	// TriLibCore.Mappers.TextureMapper TriLibCore.AssetLoaderOptions::TextureMapper
	TextureMapper_tCDB3B0D28AFDBA2BA4A640F75A7227C1B2D10ADD* ___TextureMapper_40;
	// TriLibCore.General.TextureCompressionQuality TriLibCore.AssetLoaderOptions::TextureCompressionQuality
	int32_t ___TextureCompressionQuality_41;
	// System.Boolean TriLibCore.AssetLoaderOptions::GenerateMipmaps
	bool ___GenerateMipmaps_42;
	// System.Boolean TriLibCore.AssetLoaderOptions::FixNormalMaps
	bool ___FixNormalMaps_43;
	// TriLibCore.General.AnimationType TriLibCore.AssetLoaderOptions::AnimationType
	int32_t ___AnimationType_44;
	// System.Boolean TriLibCore.AssetLoaderOptions::SimplifyAnimations
	bool ___SimplifyAnimations_45;
	// System.Single TriLibCore.AssetLoaderOptions::PositionThreshold
	float ___PositionThreshold_46;
	// System.Single TriLibCore.AssetLoaderOptions::RotationThreshold
	float ___RotationThreshold_47;
	// System.Single TriLibCore.AssetLoaderOptions::ScaleThreshold
	float ___ScaleThreshold_48;
	// TriLibCore.General.AvatarDefinitionType TriLibCore.AssetLoaderOptions::AvatarDefinition
	int32_t ___AvatarDefinition_49;
	// UnityEngine.Avatar TriLibCore.AssetLoaderOptions::Avatar
	Avatar_t7861E57EEE2CF8CC61BD63C09737BA22F7ABCA0F* ___Avatar_50;
	// TriLibCore.General.HumanDescription TriLibCore.AssetLoaderOptions::HumanDescription
	HumanDescription_t0BD271EF43944EC6940A10C164E94F8C7E750481* ___HumanDescription_51;
	// TriLibCore.Mappers.RootBoneMapper TriLibCore.AssetLoaderOptions::RootBoneMapper
	RootBoneMapper_t64AE3E33364A832EE1B74D8B65BC9AA7B448DDA2* ___RootBoneMapper_52;
	// TriLibCore.Mappers.HumanoidAvatarMapper TriLibCore.AssetLoaderOptions::HumanoidAvatarMapper
	HumanoidAvatarMapper_t691E00A2CE4455F03562FF79A586CC717D38FB09* ___HumanoidAvatarMapper_53;
	// TriLibCore.Mappers.LipSyncMapper[] TriLibCore.AssetLoaderOptions::LipSyncMappers
	LipSyncMapperU5BU5D_t32748FDCB493E8E7550A88244C1CBBB79E54C18A* ___LipSyncMappers_54;
	// System.Boolean TriLibCore.AssetLoaderOptions::SampleBindPose
	bool ___SampleBindPose_55;
	// System.Boolean TriLibCore.AssetLoaderOptions::EnforceTPose
	bool ___EnforceTPose_56;
	// System.Boolean TriLibCore.AssetLoaderOptions::ResampleAnimations
	bool ___ResampleAnimations_57;
	// System.Boolean TriLibCore.AssetLoaderOptions::EnforceAnimatorWithLegacyAnimations
	bool ___EnforceAnimatorWithLegacyAnimations_58;
	// System.Boolean TriLibCore.AssetLoaderOptions::AutomaticallyPlayLegacyAnimations
	bool ___AutomaticallyPlayLegacyAnimations_59;
	// System.Single TriLibCore.AssetLoaderOptions::ResampleFrequency
	float ___ResampleFrequency_60;
	// UnityEngine.WrapMode TriLibCore.AssetLoaderOptions::AnimationWrapMode
	int32_t ___AnimationWrapMode_61;
	// TriLibCore.Mappers.AnimationClipMapper[] TriLibCore.AssetLoaderOptions::AnimationClipMappers
	AnimationClipMapperU5BU5D_t8E00A18562A07FD65A6E731D8BA6FF48D80BBFD8* ___AnimationClipMappers_62;
	// TriLibCore.Mappers.ExternalDataMapper TriLibCore.AssetLoaderOptions::ExternalDataMapper
	ExternalDataMapper_t809726D72207DAF57227F4A5D67B9D01394B760A* ___ExternalDataMapper_63;
	// System.Boolean TriLibCore.AssetLoaderOptions::ShowLoadingWarnings
	bool ___ShowLoadingWarnings_64;
	// System.Boolean TriLibCore.AssetLoaderOptions::CloseStreamAutomatically
	bool ___CloseStreamAutomatically_65;
	// System.Int32 TriLibCore.AssetLoaderOptions::Timeout
	int32_t ___Timeout_66;
	// System.Boolean TriLibCore.AssetLoaderOptions::DestroyOnError
	bool ___DestroyOnError_67;
	// System.Boolean TriLibCore.AssetLoaderOptions::EnsureQuaternionContinuity
	bool ___EnsureQuaternionContinuity_68;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseMaterialKeywords
	bool ___UseMaterialKeywords_69;
	// System.Boolean TriLibCore.AssetLoaderOptions::ForceGCCollectionWhileLoading
	bool ___ForceGCCollectionWhileLoading_70;
	// System.Boolean TriLibCore.AssetLoaderOptions::MergeVertices
	bool ___MergeVertices_71;
	// System.Boolean TriLibCore.AssetLoaderOptions::MarkTexturesNoLongerReadable
	bool ___MarkTexturesNoLongerReadable_72;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseUnityNativeNormalCalculator
	bool ___UseUnityNativeNormalCalculator_73;
	// System.Single TriLibCore.AssetLoaderOptions::GCHelperCollectionInterval
	float ___GCHelperCollectionInterval_74;
	// System.Boolean TriLibCore.AssetLoaderOptions::ApplyGammaCurveToMaterialColors
	bool ___ApplyGammaCurveToMaterialColors_75;
	// System.Boolean TriLibCore.AssetLoaderOptions::LoadTexturesAsSRGB
	bool ___LoadTexturesAsSRGB_76;
	// TriLibCore.Mappers.UserPropertiesMapper TriLibCore.AssetLoaderOptions::UserPropertiesMapper
	UserPropertiesMapper_t8437A569EBEB9E02E364D9951BE31F9601C55714* ___UserPropertiesMapper_77;
	// System.Boolean TriLibCore.AssetLoaderOptions::ApplyTexturesOffsetAndScaling
	bool ___ApplyTexturesOffsetAndScaling_78;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseAutodeskInteractiveMaterials
	bool ___UseAutodeskInteractiveMaterials_79;
	// System.Boolean TriLibCore.AssetLoaderOptions::DiscardUnusedTextures
	bool ___DiscardUnusedTextures_80;
	// TriLibCore.General.PivotPosition TriLibCore.AssetLoaderOptions::PivotPosition
	int32_t ___PivotPosition_81;
	// System.Boolean TriLibCore.AssetLoaderOptions::ForcePowerOfTwoTextures
	bool ___ForcePowerOfTwoTextures_82;
	// System.Int32 TriLibCore.AssetLoaderOptions::MaxTexturesResolution
	int32_t ___MaxTexturesResolution_83;
	// System.Boolean TriLibCore.AssetLoaderOptions::EnableProfiler
	bool ___EnableProfiler_84;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseUnityNativeTextureLoader
	bool ___UseUnityNativeTextureLoader_85;
	// System.Boolean TriLibCore.AssetLoaderOptions::LoadMaterialsProgressively
	bool ___LoadMaterialsProgressively_86;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportCameras
	bool ___ImportCameras_87;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportLights
	bool ___ImportLights_88;
	// System.Boolean TriLibCore.AssetLoaderOptions::DisableObjectsRenaming
	bool ___DisableObjectsRenaming_89;
	// System.Boolean TriLibCore.AssetLoaderOptions::MergeSingleChild
	bool ___MergeSingleChild_90;
	// System.Boolean TriLibCore.AssetLoaderOptions::SetUnusedTexturePropertiesToNull
	bool ___SetUnusedTexturePropertiesToNull_91;
	// System.Boolean TriLibCore.AssetLoaderOptions::LoadPointClouds
	bool ___LoadPointClouds_92;
	// System.Boolean TriLibCore.AssetLoaderOptions::CreateVerticesAsNativeLists
	bool ___CreateVerticesAsNativeLists_93;
	// System.Boolean TriLibCore.AssetLoaderOptions::CompressMeshes
	bool ___CompressMeshes_94;
	// System.Boolean TriLibCore.AssetLoaderOptions::ExtractEmbeddedData
	bool ___ExtractEmbeddedData_95;
	// System.String TriLibCore.AssetLoaderOptions::EmbeddedDataExtractionPath
	String_t* ___EmbeddedDataExtractionPath_96;
	// System.Collections.Generic.List`1<UnityEngine.Object> TriLibCore.AssetLoaderOptions::FixedAllocations
	List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3* ___FixedAllocations_97;
	// TriLibCore.FileBufferingMode TriLibCore.AssetLoaderOptions::BufferizeFiles
	int32_t ___BufferizeFiles_98;
	// System.Boolean TriLibCore.AssetLoaderOptions::ConvertMaterialTextures
	bool ___ConvertMaterialTextures_99;
	// System.Boolean TriLibCore.AssetLoaderOptions::ConvertMaterialTexturesUsingHalfRes
	bool ___ConvertMaterialTexturesUsingHalfRes_100;
	// System.Boolean TriLibCore.AssetLoaderOptions::DisableTesselation
	bool ___DisableTesselation_101;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseCoroutines
	bool ___UseCoroutines_102;
};

// <Module>

// <Module>

// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup>

// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup>

// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel>

// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>

// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>

// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,TriLibCore.Interfaces.IGeometryGroup>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,TriLibCore.Interfaces.IGeometryGroup>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,TriLibCore.Interfaces.IModel>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,TriLibCore.Interfaces.IModel>

// System.IO.BinaryReader

// System.IO.BinaryReader

// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_StaticFields
{
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject* ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_DefaultThreadCurrentUICulture_34;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_DefaultThreadCurrentCulture_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t9FA6D82CAFC18769F7515BB51D1C56DAE09381C3* ___shared_by_number_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_tE1603CE612C16451D1E56FF4D4859D4FE4087C28* ___shared_by_name_37;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::s_UserPreferredCultureInfoInAppX
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_UserPreferredCultureInfoInAppX_38;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_39;
};

// System.Globalization.CultureInfo

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095_StaticFields
{
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___latin1Encoding_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding> modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54* ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject* ___s_InternalSyncObject_15;
};

// System.Text.Encoding

// TriLibCore.Geometries.Geometry

// TriLibCore.Geometries.Geometry

// System.MarshalByRefObject

// System.MarshalByRefObject

// System.Reflection.MemberInfo

// System.Reflection.MemberInfo

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449_StaticFields
{
	// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64> TriLibCore.ReaderBase::ProfileStepCallback
	Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2* ___ProfileStepCallback_0;
};

// TriLibCore.ReaderBase

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.ValueType

// System.ValueType

// UnityEngine.BoneWeight

// UnityEngine.BoneWeight

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_StaticFields
{
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_actionToActionObjShunt
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___s_actionToActionObjShunt_1;
};

// System.Threading.CancellationToken

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// UnityEngine.Color

// UnityEngine.Color

// System.Double

// System.Double

// System.Int16

// System.Int16

// System.Int32

// System.Int32

// System.Int64

// System.Int64

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// UnityEngine.Matrix4x4

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Quaternion

// System.Single

// System.Single

// TriLibCore.Stl.StlGeometry

// TriLibCore.Stl.StlGeometry

// TriLibCore.Stl.Reader.StlReader
struct StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields
{
	// System.Boolean TriLibCore.Stl.Reader.StlReader::FixInfacingNormals
	bool ___FixInfacingNormals_10;
	// System.Boolean TriLibCore.Stl.Reader.StlReader::LoadWithYUp
	bool ___LoadWithYUp_11;
	// System.Boolean TriLibCore.Stl.Reader.StlReader::StoreTriangleIndexInTexCoord0
	bool ___StoreTriangleIndexInTexCoord0_12;
	// System.Boolean TriLibCore.Stl.Reader.StlReader::ImportNormals
	bool ___ImportNormals_13;
};

// TriLibCore.Stl.Reader.StlReader

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE_StaticFields
{
	// System.IO.Stream System.IO.Stream::Null
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Null_1;
};

// System.IO.Stream

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7_StaticFields
{
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7* ___Null_1;
};

// System.IO.TextReader

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// UnityEngine.Vector4

// System.Void

// System.Void

// TriLibCore.AssetLoaderContext

// TriLibCore.AssetLoaderContext

// System.Delegate

// System.Delegate

// System.Exception
struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};

// System.Exception

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// System.RuntimeTypeHandle

// System.RuntimeTypeHandle

// TriLibCore.Stl.StlModel

// TriLibCore.Stl.StlModel

// TriLibCore.Stl.StlProcessor

// TriLibCore.Stl.StlProcessor

// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_StaticFields
{
	// System.IO.StreamReader System.IO.StreamReader::Null
	StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* ___Null_2;
};

// System.IO.StreamReader

// System.MulticastDelegate

// System.MulticastDelegate

// UnityEngine.ScriptableObject

// UnityEngine.ScriptableObject

// TriLibCore.Stl.StlRootModel

// TriLibCore.Stl.StlRootModel

// TriLibCore.Stl.StlStreamReader

// TriLibCore.Stl.StlStreamReader

// System.Type
struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Type

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>

// TriLibCore.AssetLoaderOptions

// TriLibCore.AssetLoaderOptions
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D  : public RuntimeArray
{
	ALIGN_FIELD (8) Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 m_Items[1];

	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB  : public RuntimeArray
{
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// TriLibCore.Interfaces.IModel[]
struct IModelU5BU5D_tE19C9CE57A4C086398F86D3E8410C416B390CC76  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// TriLibCore.Interfaces.IGeometryGroup[]
struct IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 m_Items[1];

	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_mD15380A4ED7CDEE99EA45881577D26BA9CE1B849_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_key, RuntimeObject** ___1_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ValueCollection_t038245E04B5D2A80048D9F8021A23E69A0C9DBAA* Dictionary_2_get_Values_mA0C01DEA55329E55380E96BBD04D4D228B437EC5_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Enumerable_ToArray_TisRuntimeObject_mA54265C2C8A0864929ECD300B75E4952D553D17D_gshared (RuntimeObject* ___0_source, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;

// System.Void TriLibCore.Geometries.Geometry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Geometry__ctor_m0F5EBDDAEEB9ED6F66946F16F31BAB22BE44D193 (Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147* __this, const RuntimeMethod* method) ;
// System.String TriLibCore.Stl.StlModel::get_Name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* StlModel_get_Name_m25E9B99DE29E6200C71933B5E52D158E28DCD941_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m8C940F3CFC42866709D7CA931B3D77B4BE94BCB6 (String_t* ___0_a, String_t* ___1_b, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup>::TryGetValue(TKey,TValue&)
inline bool Dictionary_2_TryGetValue_mEF0435E0E1DE72962810AC3082B1AF39B3951155 (Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* __this, String_t* ___0_key, RuntimeObject** ___1_value, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983*, String_t*, RuntimeObject**, const RuntimeMethod*))Dictionary_2_TryGetValue_mD15380A4ED7CDEE99EA45881577D26BA9CE1B849_gshared)(__this, ___0_key, ___1_value, method);
}
// TriLibCore.AssetLoaderContext TriLibCore.ReaderBase::get_AssetLoaderContext()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Geometries.FlexibleVertexDataUtils::BuildStreamGeometryGroup(System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FlexibleVertexDataUtils_BuildStreamGeometryGroup_m06F110C1559744234BEB5984BB8F228044B7F047 (HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* ___0_vertexAttributes, int32_t ___1_bytesCount, bool ___2_useHalfPrecision, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup>::Add(TKey,TValue)
inline void Dictionary_2_Add_mEF9914E7A716EC8B3A7FB701A93B07E83F4C798B (Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* __this, String_t* ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983*, String_t*, RuntimeObject*, const RuntimeMethod*))Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute> TriLibCore.Geometries.FlexibleVertexDataUtils::BuildVertexAttributesDictionary(TriLibCore.AssetLoaderContext,System.Int32&,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* FlexibleVertexDataUtils_BuildVertexAttributesDictionary_m38474C4D34ABEA85320910DAC96E8A8016B46888 (AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___0_assetLoaderContext, int32_t* ___1_bytesCount, bool ___2_hasPosition, bool ___3_hasNormal, bool ___4_hasTangent, bool ___5_hasColor, bool ___6_hasUV0, bool ___7_hasUV1, bool ___8_hasUV2, bool ___9_hasUV3, bool ___10_hasBoneWeight, bool ___11_useHalfPrecision, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Stl.StlProcessor::IsBinary(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StlProcessor_IsBinary_m4A82EE25ADBA42AEB6202DCBA35C3A72B239D920 (Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.Stl.StlProcessor::ParseBinary(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlProcessor_ParseBinary_m15DAD2962CF54D1F8C652CD1F69C56EDD19A991A (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.Stl.StlProcessor::ParseASCII(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlProcessor_ParseASCII_m297D902C12F8D0B07F75397F81C1BA6A82B918ED (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_lhs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_rhs, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) ;
// System.Void System.IO.BinaryReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryReader__ctor_m898732FE0DBEDD480B24F6DE45A9AC696E44CC0F (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_input, const RuntimeMethod* method) ;
// System.String System.String::CreateString(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mFBC28D2E3EB87D497F7E702E4FFAD65F635E44DF (String_t* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___0_val, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1 (String_t* ___0_a, String_t* ___1_b, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, float ___3_a, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Stl.StlProcessor::GetActiveGeometryGroup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlProcessor_GetActiveGeometryGroup_m7383FCD039100A7331EF5BD0528FCE078F143CDE (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlRootModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlRootModel__ctor_mF3CCCD9F9A1339A398A4FA1EAA0D00B3AD7BB737 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel__ctor_m6E2FE456538DB2C0EB1C98D3144C193CF6157D1C (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline (const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlModel::set_LocalScale(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_LocalScale_m0709587074F69EEB1DCF2F639318F4C9BE86D840_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline (const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlModel::set_LocalRotation(UnityEngine.Quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_LocalRotation_mE9C27ACA9705C49C2EEEEC359CF9195A0EADF5B9_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlModel::set_GeometryGroup(TriLibCore.Interfaces.IGeometryGroup)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_GeometryGroup_m32211C6B2E30C396961F21D3E5E81B925AAA444E_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlModel::set_Name(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_Name_mF8213C9BAD85BC9320C876C41ABBF594327F2814_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlModel::set_Visibility(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_Visibility_m635AB4624DB7C9E08470800B07ABABD332AF0E01_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, bool ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlModel::set_Parent(TriLibCore.Interfaces.IModel)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_Parent_m5FFEE1518EF3F12F1378AECEAEB41466CCA20DCE_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel>::Add(TKey,TValue)
inline void Dictionary_2_Add_m53D84560875155F383C73DA9027DE4CC7AC985D5 (Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* __this, String_t* ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B*, String_t*, RuntimeObject*, const RuntimeMethod*))Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared)(__this, ___0_key, ___1_value, method);
}
// UnityEngine.Vector3 TriLibCore.Stl.StlProcessor::ReadVector3(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87 (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_binaryReader, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) ;
// UnityEngine.Vector3 TriLibCore.Stl.StlProcessor::GetNormal(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 StlProcessor_GetNormal_mAEB43873CA99A50EFBEE382491E9C211BF3AEDC4 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_c, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_lhs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_rhs, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlProcessor::AddOutputVertex(TriLibCore.Interfaces.IGeometryGroup,System.Int32,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7 (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, RuntimeObject* ___0_geometryGroup, int32_t ___1_vertexIndex, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_vertex, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::UpdateLoadingPercentage(System.Single,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, float ___0_value, int32_t ___1_step, float ___2_maxValue, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel>::get_Values()
inline ValueCollection_t52D0D70A98CDE4A09BB9B7D39C2159B31ECAE027* Dictionary_2_get_Values_m56D477CD7C6E64384E53F8B4C44C151CB9CADA48 (Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* __this, const RuntimeMethod* method)
{
	return ((  ValueCollection_t52D0D70A98CDE4A09BB9B7D39C2159B31ECAE027* (*) (Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B*, const RuntimeMethod*))Dictionary_2_get_Values_mA0C01DEA55329E55380E96BBD04D4D228B437EC5_gshared)(__this, method);
}
// TSource[] System.Linq.Enumerable::ToArray<TriLibCore.Interfaces.IModel>(System.Collections.Generic.IEnumerable`1<TSource>)
inline IModelU5BU5D_tE19C9CE57A4C086398F86D3E8410C416B390CC76* Enumerable_ToArray_TisIModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB_m8A288987F91069D02641D435D479CADC11764605 (RuntimeObject* ___0_source, const RuntimeMethod* method)
{
	return ((  IModelU5BU5D_tE19C9CE57A4C086398F86D3E8410C416B390CC76* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_mA54265C2C8A0864929ECD300B75E4952D553D17D_gshared)(___0_source, method);
}
// System.Void TriLibCore.Stl.StlModel::set_Children(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_Children_m8C26332902AFB18940DAB4060807171A0364EA1E_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup>::get_Values()
inline ValueCollection_tCCCE4A1A9D973B131015A437780697A2155EAACD* Dictionary_2_get_Values_m2CEBA9C111276D8D06435752E5C5B3598E722CBE (Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* __this, const RuntimeMethod* method)
{
	return ((  ValueCollection_tCCCE4A1A9D973B131015A437780697A2155EAACD* (*) (Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983*, const RuntimeMethod*))Dictionary_2_get_Values_mA0C01DEA55329E55380E96BBD04D4D228B437EC5_gshared)(__this, method);
}
// TSource[] System.Linq.Enumerable::ToArray<TriLibCore.Interfaces.IGeometryGroup>(System.Collections.Generic.IEnumerable`1<TSource>)
inline IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711* Enumerable_ToArray_TisIGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_m8A9221E5C3D95DC2EDD7FC163B30C1A3A3929D1A (RuntimeObject* ___0_source, const RuntimeMethod* method)
{
	return ((  IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_mA54265C2C8A0864929ECD300B75E4952D553D17D_gshared)(___0_source, method);
}
// System.Void TriLibCore.Stl.StlRootModel::set_AllGeometryGroups(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlRootModel_set_AllGeometryGroups_m6754461757172DBEB0986B7A916BBBFFDAD9E173_inline (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlModel::get_Children()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* StlModel_get_Children_mCC99BF668D976265E2897D884E1080A14B8219BB_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlRootModel::set_AllModels(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlRootModel_set_AllModels_mED5ABBDE53F3952C9E492CC70CADE0692BA9A7BF_inline (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// TriLibCore.Stl.StlGeometry TriLibCore.Stl.StlProcessor::GetActiveGeometry(TriLibCore.Interfaces.IGeometryGroup,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* StlProcessor_GetActiveGeometry_m077EF6775E2A48555E67F72AD3A8E322A2182863 (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, RuntimeObject* ___0_geometryGroup, bool ___1_isQuad, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) ;
// System.Void TriLibCore.Geometries.FlexibleVertexDataUtils::BuildAndAddFlexibleVertexData(TriLibCore.Interfaces.IGeometryGroup,TriLibCore.AssetLoaderContext,System.Int32,TriLibCore.Geometries.Geometry,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector4,UnityEngine.Color,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.BoneWeight)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlexibleVertexDataUtils_BuildAndAddFlexibleVertexData_mC35367A0F41EE78D0D41CB73484C229156CC532F (RuntimeObject* ___0_geometryGroup, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, int32_t ___2_vertexIndex, Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147* ___3_geometry, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___4_position, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___5_normal, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___6_tangent, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___7_color, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___8_uv0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___9_uv1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___10_uv2, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___11_uv3, BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F ___12_boneWeight, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) ;
// System.Single TriLibCore.Stl.StlStreamReader::ReadTokenAsFloat(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float StlStreamReader_ReadTokenAsFloat_m30C508ABFBF8FF2F47629BCFFB53C042F4B7F551 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, bool ___0_required, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_m1DC2CC738B9E91B86FE02E6BFD2FA36770C08BE2 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Stl.StlStreamReader::ReadToken(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, bool ___0_required, bool ___1_ignoreSpaces, const RuntimeMethod* method) ;
// System.String TriLibCore.Stl.StlStreamReader::ReadTokenAsString(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StlStreamReader_ReadTokenAsString_m48318F7F8AD205DB62D73904EDFFD9EED1C7396D (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, bool ___0_required, bool ___1_ignoreSpaces, const RuntimeMethod* method) ;
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F (Exception_t* __this, String_t* ___0_message, const RuntimeMethod* method) ;
// UnityEngine.Vector3 TriLibCore.Stl.StlProcessor::ReadVector3(TriLibCore.Stl.StlStreamReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 StlProcessor_ReadVector3_mD41B92963A4FC7C7BA2170A47130B00B0A6B1973 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* ___0_stlStreamReader, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlProcessor::AddModel(TriLibCore.Interfaces.IModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlProcessor_AddModel_mD074A15E2C251A343E4D0B955563EE85DE37FAF8 (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, RuntimeObject* ___0_parent, const RuntimeMethod* method) ;
// System.Boolean System.IO.StreamReader::get_EndOfStream()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StreamReader_get_EndOfStream_mAE054431BF21158178EAA2A6872F14A9ED6A3C3E (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup>::.ctor()
inline void Dictionary_2__ctor_m16C7DB0395DCD9EF714A07CDB8934C3FCAE7F50C (Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel>::.ctor()
inline void Dictionary_2__ctor_m5F5C68AF90E49C38EB7E07DDE14E85B7C3875390 (Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline (const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Stl.StlStreamReader::GetTokenAsFloat(System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StlStreamReader_GetTokenAsFloat_mAA1D75FACB25386AF1A9ED7210239ED0369EDD3F (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, float* ___0_value, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Stl.StlStreamReader::get_Line()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t StlStreamReader_get_Line_mE94255EEFEFED4D8DED8AE024CE752F78FD174EC_inline (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Stl.StlStreamReader::get_Column()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StlStreamReader_get_Column_m78B316197DD99FD46120765F960B450AA0260A91 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987 (String_t* ___0_format, RuntimeObject* ___1_arg0, RuntimeObject* ___2_arg1, const RuntimeMethod* method) ;
// System.String TriLibCore.Stl.StlStreamReader::GetTokenAsString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StlStreamReader_GetTokenAsString_m3B5F90A8A2E6492B1F50E55127380D3CB334E611 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3 (String_t* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.String System.String::CreateString(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mB7B3AC2AF28010538650051A9000369B1CD6BAB6 (String_t* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___0_val, int32_t ___1_startIndex, int32_t ___2_length, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlStreamReader::UpdateStringFromCharString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader_UpdateStringFromCharString_m807C6BD8C68A20014717D4ED10E7A9A92F431C0D (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, const RuntimeMethod* method) ;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6 (const RuntimeMethod* method) ;
// System.Boolean System.Single::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_TryParse_mFB8CC32F0016FBB6EFCB97953CF3515767EB6431 (String_t* ___0_s, int32_t ___1_style, RuntimeObject* ___2_provider, float* ___3_result, const RuntimeMethod* method) ;
// System.Boolean System.Int32::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int32_TryParse_mB8E246A7D6D6308EF36DE3473643BDE4CF8F71FF (String_t* ___0_s, int32_t ___1_style, RuntimeObject* ___2_provider, int32_t* ___3_result, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline (int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Utils.HashUtils::GetHash(System.Collections.Generic.IList`1<System.Char>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t HashUtils_GetHash_mA8618222D626B54CD67FF88D56037B6D8024453E (RuntimeObject* ___0_chars, int32_t ___1_count, const RuntimeMethod* method) ;
// System.String System.String::CreateString(System.Char,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B (String_t* __this, Il2CppChar ___0_c, int32_t ___1_count, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_mAFA827D6D825FEC2C29C73B65C2DD1AB9076DEC7 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_m8ADB6C6B363A2B58B9BC3CB1939A1BABE0BF064A (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, bool ___1_detectEncodingFromByteOrderMarks, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream,System.Text.Encoding)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_m7712DDC735E99B6833E2666ADFD8A06CB96A58B1 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream,System.Text.Encoding,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_mE4095A4D9B6E2E82E95CE884443A51635849A740 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, bool ___2_detectEncodingFromByteOrderMarks, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream,System.Text.Encoding,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_mD3E001CD426B3FE451FFA32E7070E34AC1756673 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, bool ___2_detectEncodingFromByteOrderMarks, int32_t ___3_bufferSize, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_m08BA7049EACE030ACE06AB8A8F2CDF2E2AFB55C6 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, String_t* ___0_path, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_m0AD738DDCB9A0DE0DFD3DB6B2FE44A41C1EAE677 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, String_t* ___0_path, bool ___1_detectEncodingFromByteOrderMarks, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.String,System.Text.Encoding)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_m3C693DE567FB306355ECD44489F58699105DDE43 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, String_t* ___0_path, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.String,System.Text.Encoding,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_mB946592899E393BDD06A093CA3BB87180A590449 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, String_t* ___0_path, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, bool ___2_detectEncodingFromByteOrderMarks, const RuntimeMethod* method) ;
// System.Void System.IO.StreamReader::.ctor(System.String,System.Text.Encoding,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_mA95373EEF162CF396A1A20CDF039B29AA2D634EF (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, String_t* ___0_path, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, bool ___2_detectEncodingFromByteOrderMarks, int32_t ___3_bufferSize, const RuntimeMethod* method) ;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57 (RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___0_handle, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.ReaderBase::ReadStream(System.IO.Stream,TriLibCore.AssetLoaderContext,System.String,System.Action`2<TriLibCore.AssetLoaderContext,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReaderBase_ReadStream_m725378DF096B29E0DB3BE3FB9E5F1E37747883F4 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, String_t* ___2_filename, Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___3_onProgress, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::SetupStream(System.IO.Stream&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_SetupStream_mCDC78453E3657CB3FBB713C40FB50B4941455942 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE** ___0_stream, const RuntimeMethod* method) ;
// System.Void TriLibCore.Stl.StlProcessor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlProcessor__ctor_m900A44553FC03D3F3F05833E176C5456FF72A390 (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.Stl.StlProcessor::Process(TriLibCore.Stl.Reader.StlReader,System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlProcessor_Process_mE9579D6B475D44423897704E91BD2A780B6405CF (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* ___0_stlReader, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___1_stream, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::PostProcessModel(TriLibCore.Interfaces.IRootModel&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_PostProcessModel_mD3BB953DE8BEB5FCA65077455BDA0750F740C667 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, RuntimeObject** ___0_model, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase__ctor_m5C4FE7A4BC205B65DAB56FF3CC5202D0B04937DA (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_vector, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Stl.StlGeometry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlGeometry__ctor_m2776540496DA7C76A11B318FB88BB6C62734EE5D (StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* __this, const RuntimeMethod* method) 
{
	{
		Geometry__ctor_m0F5EBDDAEEB9ED6F66946F16F31BAB22BE44D193(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLibCore.Stl.StlModel::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StlModel_get_Name_m25E9B99DE29E6200C71933B5E52D158E28DCD941 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_Name_mF8213C9BAD85BC9320C876C41ABBF594327F2814 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Stl.StlModel::get_Used()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StlModel_get_Used_m1335C33D3F280C7BA81A4EB230C7F933DA6CAAE3 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CUsedU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_Used(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_Used_m73B3A1C81CF7F18D6D2F4B4ADC0F6819331261C2 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CUsedU3Ek__BackingField_1 = L_0;
		return;
	}
}
// UnityEngine.Vector3 TriLibCore.Stl.StlModel::get_LocalPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 StlModel_get_LocalPosition_mCB155A00E906835D4ED2E42681E71F93637B720E (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___U3CLocalPositionU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_LocalPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_LocalPosition_m5AF9262F57B556B40B23C967CF09785E3BD44C52 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->___U3CLocalPositionU3Ek__BackingField_2 = L_0;
		return;
	}
}
// UnityEngine.Quaternion TriLibCore.Stl.StlModel::get_LocalRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 StlModel_get_LocalRotation_mC94B5254507082EEF78E62C751A40BA2BE0AF3EF (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = __this->___U3CLocalRotationU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_LocalRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_LocalRotation_mE9C27ACA9705C49C2EEEEC359CF9195A0EADF5B9 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_value, const RuntimeMethod* method) 
{
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___0_value;
		__this->___U3CLocalRotationU3Ek__BackingField_3 = L_0;
		return;
	}
}
// UnityEngine.Vector3 TriLibCore.Stl.StlModel::get_LocalScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 StlModel_get_LocalScale_mA9317004E928DF3BDFC6DF9B17B203B66FC4723B (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___U3CLocalScaleU3Ek__BackingField_4;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_LocalScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_LocalScale_m0709587074F69EEB1DCF2F639318F4C9BE86D840 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->___U3CLocalScaleU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean TriLibCore.Stl.StlModel::get_Visibility()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StlModel_get_Visibility_m41D73429568E441CDE8E96D86EEE88D70D1AB266 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CVisibilityU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_Visibility(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_Visibility_m635AB4624DB7C9E08470800B07ABABD332AF0E01 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CVisibilityU3Ek__BackingField_5 = L_0;
		return;
	}
}
// TriLibCore.Interfaces.IModel TriLibCore.Stl.StlModel::get_Parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlModel_get_Parent_m86D1711C2B33FFB6A7C9B150E9F614718B18E237 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CParentU3Ek__BackingField_6;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_Parent(TriLibCore.Interfaces.IModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_Parent_m5FFEE1518EF3F12F1378AECEAEB41466CCA20DCE (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CParentU3Ek__BackingField_6 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CParentU3Ek__BackingField_6), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlModel::get_Children()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlModel_get_Children_mCC99BF668D976265E2897D884E1080A14B8219BB (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CChildrenU3Ek__BackingField_7;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_Children(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_Children_m8C26332902AFB18940DAB4060807171A0364EA1E (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CChildrenU3Ek__BackingField_7 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CChildrenU3Ek__BackingField_7), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Stl.StlModel::get_IsBone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StlModel_get_IsBone_mE00017B8AD0B85CFC52CECB34E77BFD5BB7E96D6 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsBoneU3Ek__BackingField_8;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_IsBone(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_IsBone_m3A3DC56944ED27BAF11B1AC1634FA26D80DA09E2 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CIsBoneU3Ek__BackingField_8 = L_0;
		return;
	}
}
// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Stl.StlModel::get_GeometryGroup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlModel_get_GeometryGroup_mD1F5B1F396068F485FAB980C35ED2DD19989C80F (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CGeometryGroupU3Ek__BackingField_9;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_GeometryGroup(TriLibCore.Interfaces.IGeometryGroup)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_GeometryGroup_m32211C6B2E30C396961F21D3E5E81B925AAA444E (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CGeometryGroupU3Ek__BackingField_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CGeometryGroupU3Ek__BackingField_9), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlModel::get_Bones()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlModel_get_Bones_m684557BC45E7AC791A1B30DCE1DB803CD1408BE2 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CBonesU3Ek__BackingField_10;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_Bones(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_Bones_mAA6178D9689B8D839332F71C57A59EA360C5E231 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CBonesU3Ek__BackingField_10 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CBonesU3Ek__BackingField_10), (void*)L_0);
		return;
	}
}
// UnityEngine.Matrix4x4[] TriLibCore.Stl.StlModel::get_BindPoses()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* StlModel_get_BindPoses_m9E24FE74C24EFC85B8A8582B108D2CBB119421DB (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_0 = __this->___U3CBindPosesU3Ek__BackingField_11;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_BindPoses(UnityEngine.Matrix4x4[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_BindPoses_m3E51722DF32A7EC854623CB8CC82D115AB230193 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___0_value, const RuntimeMethod* method) 
{
	{
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_0 = ___0_value;
		__this->___U3CBindPosesU3Ek__BackingField_11 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CBindPosesU3Ek__BackingField_11), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Stl.StlModel::get_MaterialIndices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlModel_get_MaterialIndices_m140AFBF45A10411222E47FB5B22AB03D0897EEEC (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CMaterialIndicesU3Ek__BackingField_12;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_MaterialIndices(System.Collections.Generic.IList`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_MaterialIndices_m324F349E20CBDA15FD0D369642D5AE3D89332281 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CMaterialIndicesU3Ek__BackingField_12 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CMaterialIndicesU3Ek__BackingField_12), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Stl.StlModel::get_UserProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* StlModel_get_UserProperties_mFC87C1859174342787C4379A620AAD8177E1BB6B (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_0 = __this->___U3CUserPropertiesU3Ek__BackingField_13;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::set_UserProperties(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel_set_UserProperties_m33ADB762D7FA9D10967DDD33FA5CE6825CCE1748 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___0_value, const RuntimeMethod* method) 
{
	{
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_0 = ___0_value;
		__this->___U3CUserPropertiesU3Ek__BackingField_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CUserPropertiesU3Ek__BackingField_13), (void*)L_0);
		return;
	}
}
// System.String TriLibCore.Stl.StlModel::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StlModel_ToString_m4565A071E9352293402EE92115472BD57BEFB879 (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0;
		L_0 = StlModel_get_Name_m25E9B99DE29E6200C71933B5E52D158E28DCD941_inline(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlModel__ctor_m6E2FE456538DB2C0EB1C98D3144C193CF6157D1C (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Stl.StlProcessor::GetActiveGeometryGroup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlProcessor_GetActiveGeometryGroup_m7383FCD039100A7331EF5BD0528FCE078F143CDE (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mEF9914E7A716EC8B3A7FB701A93B07E83F4C798B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_mEF0435E0E1DE72962810AC3082B1AF39B3951155_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IObject_t2E20027AB39DAC66068F50E33ECB233711D9DA5F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		String_t* L_0 = __this->____groupName_6;
		V_0 = L_0;
		String_t* L_1 = V_0;
		String_t* L_2 = __this->____lastGeometryGroupName_7;
		bool L_3;
		L_3 = String_op_Inequality_m8C940F3CFC42866709D7CA931B3D77B4BE94BCB6(L_1, L_2, NULL);
		if (!L_3)
		{
			goto IL_0083;
		}
	}
	{
		Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* L_4 = __this->____geometryGroups_3;
		String_t* L_5 = V_0;
		NullCheck(L_4);
		bool L_6;
		L_6 = Dictionary_2_TryGetValue_mEF0435E0E1DE72962810AC3082B1AF39B3951155(L_4, L_5, (&V_1), Dictionary_2_TryGetValue_mEF0435E0E1DE72962810AC3082B1AF39B3951155_RuntimeMethod_var);
		if (L_6)
		{
			goto IL_0075;
		}
	}
	{
		HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* L_7 = __this->____vertexAttributes_24;
		int32_t L_8 = __this->____floatCount_23;
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_9 = __this->____reader_5;
		NullCheck(L_9);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_10;
		L_10 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_9, NULL);
		NullCheck(L_10);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_11 = L_10->___Options_0;
		NullCheck(L_11);
		bool L_12 = L_11->___CompressMeshes_94;
		RuntimeObject* L_13;
		L_13 = FlexibleVertexDataUtils_BuildStreamGeometryGroup_m06F110C1559744234BEB5984BB8F228044B7F047(L_7, L_8, L_12, NULL);
		V_1 = L_13;
		RuntimeObject* L_14 = V_1;
		String_t* L_15 = V_0;
		NullCheck(L_14);
		InterfaceActionInvoker1< String_t* >::Invoke(1 /* System.Void TriLibCore.Interfaces.IObject::set_Name(System.String) */, IObject_t2E20027AB39DAC66068F50E33ECB233711D9DA5F_il2cpp_TypeInfo_var, L_14, L_15);
		RuntimeObject* L_16 = V_1;
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_17 = __this->____reader_5;
		NullCheck(L_17);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_18;
		L_18 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_17, NULL);
		NullCheck(L_16);
		InterfaceActionInvoker4< AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C*, int32_t, int32_t, int32_t >::Invoke(6 /* System.Void TriLibCore.Interfaces.IGeometryGroup::Setup(TriLibCore.AssetLoaderContext,System.Int32,System.Int32,System.Int32) */, IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var, L_16, L_18, 3, ((int32_t)32), 0);
		Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* L_19 = __this->____geometryGroups_3;
		String_t* L_20 = V_0;
		RuntimeObject* L_21 = V_1;
		NullCheck(L_19);
		Dictionary_2_Add_mEF9914E7A716EC8B3A7FB701A93B07E83F4C798B(L_19, L_20, L_21, Dictionary_2_Add_mEF9914E7A716EC8B3A7FB701A93B07E83F4C798B_RuntimeMethod_var);
	}

IL_0075:
	{
		RuntimeObject* L_22 = V_1;
		__this->____activeGeometryGroup_9 = L_22;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____activeGeometryGroup_9), (void*)L_22);
		String_t* L_23 = V_0;
		__this->____lastGeometryGroupName_7 = L_23;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____lastGeometryGroupName_7), (void*)L_23);
	}

IL_0083:
	{
		RuntimeObject* L_24 = __this->____activeGeometryGroup_9;
		return L_24;
	}
}
// TriLibCore.Stl.StlGeometry TriLibCore.Stl.StlProcessor::GetActiveGeometry(TriLibCore.Interfaces.IGeometryGroup,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* StlProcessor_GetActiveGeometry_m077EF6775E2A48555E67F72AD3A8E322A2182863 (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, RuntimeObject* ___0_geometryGroup, bool ___1_isQuad, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IGeometryGroup_GetGeometry_TisStlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF_m4B86DA0DDE54E5F1A22E224B3A37C7316E2ACA27_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->____loopNumber_11;
		int32_t L_1 = __this->____lastLoopNumber_8;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0038;
		}
	}
	{
		RuntimeObject* L_2 = ___0_geometryGroup;
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_3 = __this->____reader_5;
		NullCheck(L_3);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_4;
		L_4 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_3, NULL);
		int32_t L_5 = __this->____loopNumber_11;
		bool L_6 = ___1_isQuad;
		NullCheck(L_2);
		StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* L_7;
		L_7 = GenericInterfaceFuncInvoker3< StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF*, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C*, int32_t, bool >::Invoke(IGeometryGroup_GetGeometry_TisStlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF_m4B86DA0DDE54E5F1A22E224B3A37C7316E2ACA27_RuntimeMethod_var, L_2, L_4, L_5, L_6);
		__this->____activeGeometry_10 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____activeGeometry_10), (void*)L_7);
		int32_t L_8 = __this->____loopNumber_11;
		__this->____lastLoopNumber_8 = L_8;
	}

IL_0038:
	{
		StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* L_9 = __this->____activeGeometry_10;
		return L_9;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Stl.StlProcessor::Process(TriLibCore.Stl.Reader.StlReader,System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlProcessor_Process_mE9579D6B475D44423897704E91BD2A780B6405CF (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* ___0_stlReader, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___1_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_0 = ___0_stlReader;
		__this->____reader_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____reader_5), (void*)L_0);
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_1 = __this->____reader_5;
		NullCheck(L_1);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_2;
		L_2 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_1, NULL);
		int32_t* L_3 = (&__this->____floatCount_23);
		il2cpp_codegen_runtime_class_init_inline(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		bool L_4 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___ImportNormals_13;
		bool L_5 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___StoreTriangleIndexInTexCoord0_12;
		HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* L_6;
		L_6 = FlexibleVertexDataUtils_BuildVertexAttributesDictionary_m38474C4D34ABEA85320910DAC96E8A8016B46888(L_2, L_3, (bool)1, L_4, (bool)0, (bool)0, L_5, (bool)0, (bool)0, (bool)0, (bool)0, (bool)0, NULL);
		__this->____vertexAttributes_24 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____vertexAttributes_24), (void*)L_6);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_7 = ___1_stream;
		bool L_8;
		L_8 = StlProcessor_IsBinary_m4A82EE25ADBA42AEB6202DCBA35C3A72B239D920(L_7, NULL);
		if (!L_8)
		{
			goto IL_0045;
		}
	}
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_9 = ___1_stream;
		RuntimeObject* L_10;
		L_10 = StlProcessor_ParseBinary_m15DAD2962CF54D1F8C652CD1F69C56EDD19A991A(__this, L_9, NULL);
		return L_10;
	}

IL_0045:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_11 = ___1_stream;
		RuntimeObject* L_12;
		L_12 = StlProcessor_ParseASCII_m297D902C12F8D0B07F75397F81C1BA6A82B918ED(__this, L_11, NULL);
		return L_12;
	}
}
// UnityEngine.Vector3 TriLibCore.Stl.StlProcessor::GetNormal(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 StlProcessor_GetNormal_mAEB43873CA99A50EFBEE382491E9C211BF3AEDC4 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_c, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___1_b;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___0_a;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_0, L_1, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___2_c;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_a;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_3, L_4, NULL);
		V_0 = L_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline(L_2, L_6, NULL);
		V_1 = L_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline((&V_1), NULL);
		return L_8;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Stl.StlProcessor::ParseBinary(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlProcessor_ParseBinary_m15DAD2962CF54D1F8C652CD1F69C56EDD19A991A (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m53D84560875155F383C73DA9027DE4CC7AC985D5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Values_m2CEBA9C111276D8D06435752E5C5B3598E722CBE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Values_m56D477CD7C6E64384E53F8B4C44C151CB9CADA48_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisIGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_m8A9221E5C3D95DC2EDD7FC163B30C1A3A3929D1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisIModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB_m8A288987F91069D02641D435D479CADC11764605_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IObject_t2E20027AB39DAC66068F50E33ECB233711D9DA5F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4F4B57D5A155D08E686DEA8A1E7C1992F9C3675B);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	RuntimeObject* V_5 = NULL;
	StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* V_6 = NULL;
	StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* V_7 = NULL;
	uint8_t V_8 = 0x0;
	uint8_t V_9 = 0x0;
	uint8_t V_10 = 0x0;
	uint8_t V_11 = 0x0;
	int32_t V_12 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_13;
	memset((&V_13), 0, sizeof(V_13));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_14;
	memset((&V_14), 0, sizeof(V_14));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_15;
	memset((&V_15), 0, sizeof(V_15));
	int16_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	bool V_20 = false;
	int32_t V_21 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_22;
	memset((&V_22), 0, sizeof(V_22));
	StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* G_B16_0 = NULL;
	StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* G_B15_0 = NULL;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F G_B17_0;
	memset((&G_B17_0), 0, sizeof(G_B17_0));
	StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* G_B17_1 = NULL;
	{
		V_0 = (bool)0;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_stream;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_1 = (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158*)il2cpp_codegen_object_new(BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		BinaryReader__ctor_m898732FE0DBEDD480B24F6DE45A9AC696E44CC0F(L_1, L_0, NULL);
		V_1 = L_1;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_2 = V_1;
		NullCheck(L_2);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_3;
		L_3 = VirtualFuncInvoker1< CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*, int32_t >::Invoke(25 /* System.Char[] System.IO.BinaryReader::ReadChars(System.Int32) */, L_2, 6);
		String_t* L_4;
		L_4 = String_CreateString_mFBC28D2E3EB87D497F7E702E4FFAD65F635E44DF(NULL, L_3, NULL);
		V_2 = L_4;
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_5 = __this->____reader_5;
		NullCheck(L_5);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_6;
		L_6 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_5, NULL);
		NullCheck(L_6);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_7 = L_6->___Options_0;
		NullCheck(L_7);
		bool L_8 = L_7->___ImportColors_19;
		if (!L_8)
		{
			goto IL_0097;
		}
	}
	{
		String_t* L_9 = V_2;
		bool L_10;
		L_10 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_9, _stringLiteral4F4B57D5A155D08E686DEA8A1E7C1992F9C3675B, NULL);
		if (!L_10)
		{
			goto IL_0097;
		}
	}
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_11 = V_1;
		NullCheck(L_11);
		uint8_t L_12;
		L_12 = VirtualFuncInvoker0< uint8_t >::Invoke(11 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_11);
		V_8 = L_12;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_13 = V_1;
		NullCheck(L_13);
		uint8_t L_14;
		L_14 = VirtualFuncInvoker0< uint8_t >::Invoke(11 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_13);
		V_9 = L_14;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_15 = V_1;
		NullCheck(L_15);
		uint8_t L_16;
		L_16 = VirtualFuncInvoker0< uint8_t >::Invoke(11 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_15);
		V_10 = L_16;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_17 = V_1;
		NullCheck(L_17);
		uint8_t L_18;
		L_18 = VirtualFuncInvoker0< uint8_t >::Invoke(11 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_17);
		V_11 = L_18;
		uint8_t L_19 = V_8;
		uint8_t L_20 = V_9;
		uint8_t L_21 = V_10;
		uint8_t L_22 = V_11;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_23;
		memset((&L_23), 0, sizeof(L_23));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_23), ((float)(((float)L_19)/(255.0f))), ((float)(((float)L_20)/(255.0f))), ((float)(((float)L_21)/(255.0f))), ((float)(((float)L_22)/(255.0f))), /*hidden argument*/NULL);
		__this->____partColor_21 = L_23;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_24 = __this->____partColor_21;
		__this->____facetColor_22 = L_24;
		V_0 = (bool)1;
	}

IL_0097:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_25 = ___0_stream;
		NullCheck(L_25);
		int64_t L_26;
		L_26 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_25, ((int64_t)((int32_t)80)), 0);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_27 = V_1;
		NullCheck(L_27);
		int32_t L_28;
		L_28 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_27);
		V_3 = L_28;
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_29 = __this->____reader_5;
		NullCheck(L_29);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_30;
		L_30 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_29, NULL);
		NullCheck(L_30);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_31 = L_30->___Options_0;
		NullCheck(L_31);
		float L_32 = L_31->___ScaleFactor_5;
		V_4 = L_32;
		RuntimeObject* L_33;
		L_33 = StlProcessor_GetActiveGeometryGroup_m7383FCD039100A7331EF5BD0528FCE078F143CDE(__this, NULL);
		V_5 = L_33;
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_34 = (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142*)il2cpp_codegen_object_new(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142_il2cpp_TypeInfo_var);
		NullCheck(L_34);
		StlRootModel__ctor_mF3CCCD9F9A1339A398A4FA1EAA0D00B3AD7BB737(L_34, NULL);
		V_6 = L_34;
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_35 = (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA*)il2cpp_codegen_object_new(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA_il2cpp_TypeInfo_var);
		NullCheck(L_35);
		StlModel__ctor_m6E2FE456538DB2C0EB1C98D3144C193CF6157D1C(L_35, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_36 = L_35;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37;
		L_37 = Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline(NULL);
		NullCheck(L_36);
		StlModel_set_LocalScale_m0709587074F69EEB1DCF2F639318F4C9BE86D840_inline(L_36, L_37, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_38 = L_36;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_39;
		L_39 = Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline(NULL);
		NullCheck(L_38);
		StlModel_set_LocalRotation_mE9C27ACA9705C49C2EEEEC359CF9195A0EADF5B9_inline(L_38, L_39, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_40 = L_38;
		RuntimeObject* L_41 = V_5;
		NullCheck(L_40);
		StlModel_set_GeometryGroup_m32211C6B2E30C396961F21D3E5E81B925AAA444E_inline(L_40, L_41, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_42 = L_40;
		RuntimeObject* L_43 = V_5;
		NullCheck(L_43);
		String_t* L_44;
		L_44 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String TriLibCore.Interfaces.IObject::get_Name() */, IObject_t2E20027AB39DAC66068F50E33ECB233711D9DA5F_il2cpp_TypeInfo_var, L_43);
		NullCheck(L_42);
		StlModel_set_Name_mF8213C9BAD85BC9320C876C41ABBF594327F2814_inline(L_42, L_44, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_45 = L_42;
		NullCheck(L_45);
		StlModel_set_Visibility_m635AB4624DB7C9E08470800B07ABABD332AF0E01_inline(L_45, (bool)1, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_46 = L_45;
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_47 = V_6;
		NullCheck(L_46);
		StlModel_set_Parent_m5FFEE1518EF3F12F1378AECEAEB41466CCA20DCE_inline(L_46, L_47, NULL);
		V_7 = L_46;
		Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* L_48 = __this->____models_4;
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_49 = V_7;
		NullCheck(L_49);
		String_t* L_50;
		L_50 = StlModel_get_Name_m25E9B99DE29E6200C71933B5E52D158E28DCD941_inline(L_49, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_51 = V_7;
		NullCheck(L_48);
		Dictionary_2_Add_m53D84560875155F383C73DA9027DE4CC7AC985D5(L_48, L_50, L_51, Dictionary_2_Add_m53D84560875155F383C73DA9027DE4CC7AC985D5_RuntimeMethod_var);
		V_12 = 0;
		goto IL_0311;
	}

IL_012c:
	{
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_52 = __this->____reader_5;
		NullCheck(L_52);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_53;
		L_53 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_52, NULL);
		NullCheck(L_53);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_54 = L_53->___Options_0;
		NullCheck(L_54);
		bool L_55 = L_54->___ImportNormals_23;
		if (!L_55)
		{
			goto IL_0151;
		}
	}
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_56 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_57;
		L_57 = StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87(L_56, NULL);
		__this->____facetNormal_12 = L_57;
		goto IL_0163;
	}

IL_0151:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_58 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_59;
		L_59 = StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87(L_58, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_60;
		L_60 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		__this->____facetNormal_12 = L_60;
	}

IL_0163:
	{
		il2cpp_codegen_runtime_class_init_inline(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		bool L_61 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___LoadWithYUp_11;
		if (!L_61)
		{
			goto IL_0199;
		}
	}
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_62 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_63;
		L_63 = StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87(L_62, NULL);
		float L_64 = V_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_65;
		L_65 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_63, L_64, NULL);
		V_15 = L_65;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_66 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_67;
		L_67 = StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87(L_66, NULL);
		float L_68 = V_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_69;
		L_69 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_67, L_68, NULL);
		V_14 = L_69;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_70 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_71;
		L_71 = StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87(L_70, NULL);
		float L_72 = V_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_73;
		L_73 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_71, L_72, NULL);
		V_13 = L_73;
		goto IL_01c6;
	}

IL_0199:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_74 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_75;
		L_75 = StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87(L_74, NULL);
		float L_76 = V_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_77;
		L_77 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_75, L_76, NULL);
		V_13 = L_77;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_78 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_79;
		L_79 = StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87(L_78, NULL);
		float L_80 = V_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_81;
		L_81 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_79, L_80, NULL);
		V_14 = L_81;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_82 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_83;
		L_83 = StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87(L_82, NULL);
		float L_84 = V_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_85;
		L_85 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_83, L_84, NULL);
		V_15 = L_85;
	}

IL_01c6:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_86 = V_1;
		NullCheck(L_86);
		int16_t L_87;
		L_87 = VirtualFuncInvoker0< int16_t >::Invoke(14 /* System.Int16 System.IO.BinaryReader::ReadInt16() */, L_86);
		V_16 = L_87;
		int16_t L_88 = V_16;
		if (!L_88)
		{
			goto IL_0271;
		}
	}
	{
		bool L_89 = V_0;
		if (!L_89)
		{
			goto IL_020a;
		}
	}
	{
		int16_t L_90 = V_16;
		V_17 = ((int32_t)((int32_t)L_90&((int32_t)31)));
		int16_t L_91 = V_16;
		V_18 = ((int32_t)(((int32_t)((int32_t)L_91&((int32_t)992)))>>5));
		int16_t L_92 = V_16;
		V_19 = ((int32_t)(((int32_t)((int32_t)L_92&((int32_t)31744)))>>((int32_t)10)));
		int16_t L_93 = V_16;
		V_20 = (bool)((((int32_t)((int32_t)(((int32_t)((int32_t)L_93&((int32_t)32768)))>>((int32_t)15)))) == ((int32_t)0))? 1 : 0);
		goto IL_023a;
	}

IL_020a:
	{
		int16_t L_94 = V_16;
		V_19 = ((int32_t)(((int32_t)((int32_t)L_94&((int32_t)31744)))>>((int32_t)10)));
		int16_t L_95 = V_16;
		V_18 = ((int32_t)(((int32_t)((int32_t)L_95&((int32_t)992)))>>5));
		int16_t L_96 = V_16;
		V_17 = ((int32_t)((int32_t)L_96&((int32_t)31)));
		int16_t L_97 = V_16;
		V_20 = (bool)((((int32_t)((int32_t)(((int32_t)((int32_t)L_97&((int32_t)32768)))>>((int32_t)15)))) == ((int32_t)1))? 1 : 0);
	}

IL_023a:
	{
		bool L_98 = V_20;
		G_B15_0 = __this;
		if (L_98)
		{
			G_B16_0 = __this;
			goto IL_0247;
		}
	}
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_99 = __this->____partColor_21;
		G_B17_0 = L_99;
		G_B17_1 = G_B15_0;
		goto IL_026c;
	}

IL_0247:
	{
		int32_t L_100 = V_17;
		int32_t L_101 = V_18;
		int32_t L_102 = V_19;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_103;
		memset((&L_103), 0, sizeof(L_103));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_103), ((float)(((float)L_100)/(32.0f))), ((float)(((float)L_101)/(32.0f))), ((float)(((float)L_102)/(32.0f))), (1.0f), /*hidden argument*/NULL);
		G_B17_0 = L_103;
		G_B17_1 = G_B16_0;
	}

IL_026c:
	{
		NullCheck(G_B17_1);
		G_B17_1->____facetColor_22 = G_B17_0;
	}

IL_0271:
	{
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_104 = __this->____reader_5;
		NullCheck(L_104);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_105;
		L_105 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_104, NULL);
		NullCheck(L_105);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_106 = L_105->___Options_0;
		NullCheck(L_106);
		bool L_107 = L_106->___ImportMeshes_10;
		if (!L_107)
		{
			goto IL_030b;
		}
	}
	{
		int32_t L_108 = V_12;
		V_21 = ((int32_t)il2cpp_codegen_multiply(L_108, 3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_109 = V_13;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_110 = V_14;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_111 = V_15;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_112;
		L_112 = StlProcessor_GetNormal_mAEB43873CA99A50EFBEE382491E9C211BF3AEDC4(L_109, L_110, L_111, NULL);
		V_22 = L_112;
		il2cpp_codegen_runtime_class_init_inline(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		bool L_113 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___FixInfacingNormals_10;
		if (!L_113)
		{
			goto IL_02b9;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_114 = __this->____facetNormal_12;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_115 = V_22;
		float L_116;
		L_116 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_114, L_115, NULL);
		if ((!(((float)L_116) > ((float)(0.0f)))))
		{
			goto IL_02e3;
		}
	}

IL_02b9:
	{
		RuntimeObject* L_117 = V_5;
		int32_t L_118 = V_21;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_119 = V_15;
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_117, L_118, L_119, NULL);
		RuntimeObject* L_120 = V_5;
		int32_t L_121 = V_21;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_122 = V_14;
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_120, ((int32_t)il2cpp_codegen_add(L_121, 1)), L_122, NULL);
		RuntimeObject* L_123 = V_5;
		int32_t L_124 = V_21;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_125 = V_13;
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_123, ((int32_t)il2cpp_codegen_add(L_124, 2)), L_125, NULL);
		goto IL_030b;
	}

IL_02e3:
	{
		RuntimeObject* L_126 = V_5;
		int32_t L_127 = V_21;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_128 = V_13;
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_126, ((int32_t)il2cpp_codegen_add(L_127, 2)), L_128, NULL);
		RuntimeObject* L_129 = V_5;
		int32_t L_130 = V_21;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_131 = V_14;
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_129, ((int32_t)il2cpp_codegen_add(L_130, 1)), L_131, NULL);
		RuntimeObject* L_132 = V_5;
		int32_t L_133 = V_21;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_134 = V_15;
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_132, L_133, L_134, NULL);
	}

IL_030b:
	{
		int32_t L_135 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add(L_135, 1));
	}

IL_0311:
	{
		int32_t L_136 = V_12;
		int32_t L_137 = V_3;
		if ((((int32_t)L_136) < ((int32_t)L_137)))
		{
			goto IL_012c;
		}
	}
	{
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_138 = __this->____reader_5;
		int32_t L_139 = V_3;
		NullCheck(L_138);
		ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C(L_138, (1.0f), 1, ((float)L_139), NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_140 = V_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_141;
		L_141 = Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline(NULL);
		NullCheck(L_140);
		StlModel_set_LocalScale_m0709587074F69EEB1DCF2F639318F4C9BE86D840_inline(L_140, L_141, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_142 = V_6;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_143;
		L_143 = Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline(NULL);
		NullCheck(L_142);
		StlModel_set_LocalRotation_mE9C27ACA9705C49C2EEEEC359CF9195A0EADF5B9_inline(L_142, L_143, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_144 = V_6;
		NullCheck(L_144);
		StlModel_set_Visibility_m635AB4624DB7C9E08470800B07ABABD332AF0E01_inline(L_144, (bool)1, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_145 = V_6;
		Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* L_146 = __this->____models_4;
		NullCheck(L_146);
		ValueCollection_t52D0D70A98CDE4A09BB9B7D39C2159B31ECAE027* L_147;
		L_147 = Dictionary_2_get_Values_m56D477CD7C6E64384E53F8B4C44C151CB9CADA48(L_146, Dictionary_2_get_Values_m56D477CD7C6E64384E53F8B4C44C151CB9CADA48_RuntimeMethod_var);
		IModelU5BU5D_tE19C9CE57A4C086398F86D3E8410C416B390CC76* L_148;
		L_148 = Enumerable_ToArray_TisIModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB_m8A288987F91069D02641D435D479CADC11764605(L_147, Enumerable_ToArray_TisIModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB_m8A288987F91069D02641D435D479CADC11764605_RuntimeMethod_var);
		NullCheck(L_145);
		StlModel_set_Children_m8C26332902AFB18940DAB4060807171A0364EA1E_inline(L_145, (RuntimeObject*)L_148, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_149 = V_6;
		Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* L_150 = __this->____geometryGroups_3;
		NullCheck(L_150);
		ValueCollection_tCCCE4A1A9D973B131015A437780697A2155EAACD* L_151;
		L_151 = Dictionary_2_get_Values_m2CEBA9C111276D8D06435752E5C5B3598E722CBE(L_150, Dictionary_2_get_Values_m2CEBA9C111276D8D06435752E5C5B3598E722CBE_RuntimeMethod_var);
		IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711* L_152;
		L_152 = Enumerable_ToArray_TisIGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_m8A9221E5C3D95DC2EDD7FC163B30C1A3A3929D1A(L_151, Enumerable_ToArray_TisIGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_m8A9221E5C3D95DC2EDD7FC163B30C1A3A3929D1A_RuntimeMethod_var);
		NullCheck(L_149);
		StlRootModel_set_AllGeometryGroups_m6754461757172DBEB0986B7A916BBBFFDAD9E173_inline(L_149, (RuntimeObject*)L_152, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_153 = V_6;
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_154 = V_6;
		NullCheck(L_154);
		RuntimeObject* L_155;
		L_155 = StlModel_get_Children_mCC99BF668D976265E2897D884E1080A14B8219BB_inline(L_154, NULL);
		NullCheck(L_153);
		StlRootModel_set_AllModels_mED5ABBDE53F3952C9E492CC70CADE0692BA9A7BF_inline(L_153, L_155, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_156 = V_6;
		return L_156;
	}
}
// System.Void TriLibCore.Stl.StlProcessor::AddOutputVertex(TriLibCore.Interfaces.IGeometryGroup,System.Int32,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7 (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, RuntimeObject* ___0_geometryGroup, int32_t ___1_vertexIndex, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_vertex, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* V_0 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_3;
	memset((&V_3), 0, sizeof(V_3));
	BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B2_0;
	memset((&G_B2_0), 0, sizeof(G_B2_0));
	StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* G_B2_1 = NULL;
	int32_t G_B2_2 = 0;
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* G_B2_3 = NULL;
	RuntimeObject* G_B2_4 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B1_0;
	memset((&G_B1_0), 0, sizeof(G_B1_0));
	StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* G_B1_1 = NULL;
	int32_t G_B1_2 = 0;
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* G_B1_3 = NULL;
	RuntimeObject* G_B1_4 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B3_1;
	memset((&G_B3_1), 0, sizeof(G_B3_1));
	StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* G_B3_2 = NULL;
	int32_t G_B3_3 = 0;
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* G_B3_4 = NULL;
	RuntimeObject* G_B3_5 = NULL;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F G_B5_0;
	memset((&G_B5_0), 0, sizeof(G_B5_0));
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 G_B5_1;
	memset((&G_B5_1), 0, sizeof(G_B5_1));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B5_2;
	memset((&G_B5_2), 0, sizeof(G_B5_2));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B5_3;
	memset((&G_B5_3), 0, sizeof(G_B5_3));
	StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* G_B5_4 = NULL;
	int32_t G_B5_5 = 0;
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* G_B5_6 = NULL;
	RuntimeObject* G_B5_7 = NULL;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F G_B4_0;
	memset((&G_B4_0), 0, sizeof(G_B4_0));
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 G_B4_1;
	memset((&G_B4_1), 0, sizeof(G_B4_1));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B4_2;
	memset((&G_B4_2), 0, sizeof(G_B4_2));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B4_3;
	memset((&G_B4_3), 0, sizeof(G_B4_3));
	StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* G_B4_4 = NULL;
	int32_t G_B4_5 = 0;
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* G_B4_6 = NULL;
	RuntimeObject* G_B4_7 = NULL;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 G_B6_0;
	memset((&G_B6_0), 0, sizeof(G_B6_0));
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F G_B6_1;
	memset((&G_B6_1), 0, sizeof(G_B6_1));
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 G_B6_2;
	memset((&G_B6_2), 0, sizeof(G_B6_2));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B6_3;
	memset((&G_B6_3), 0, sizeof(G_B6_3));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B6_4;
	memset((&G_B6_4), 0, sizeof(G_B6_4));
	StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* G_B6_5 = NULL;
	int32_t G_B6_6 = 0;
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* G_B6_7 = NULL;
	RuntimeObject* G_B6_8 = NULL;
	{
		RuntimeObject* L_0 = ___0_geometryGroup;
		StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* L_1;
		L_1 = StlProcessor_GetActiveGeometry_m077EF6775E2A48555E67F72AD3A8E322A2182863(__this, L_0, (bool)0, NULL);
		V_0 = L_1;
		RuntimeObject* L_2 = ___0_geometryGroup;
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_3 = __this->____reader_5;
		NullCheck(L_3);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_4;
		L_4 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_3, NULL);
		int32_t L_5 = ___1_vertexIndex;
		StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* L_6 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = ___2_vertex;
		il2cpp_codegen_runtime_class_init_inline(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		bool L_8 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___ImportNormals_13;
		G_B1_0 = L_7;
		G_B1_1 = L_6;
		G_B1_2 = L_5;
		G_B1_3 = L_4;
		G_B1_4 = L_2;
		if (L_8)
		{
			G_B2_0 = L_7;
			G_B2_1 = L_6;
			G_B2_2 = L_5;
			G_B2_3 = L_4;
			G_B2_4 = L_2;
			goto IL_002a;
		}
	}
	{
		il2cpp_codegen_initobj((&V_1), sizeof(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9 = V_1;
		G_B3_0 = L_9;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002a:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = __this->____facetNormal_12;
		G_B3_0 = L_10;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		il2cpp_codegen_initobj((&V_2), sizeof(Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3));
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_11 = V_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_12 = __this->____facetColor_22;
		il2cpp_codegen_runtime_class_init_inline(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		bool L_13 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___StoreTriangleIndexInTexCoord0_12;
		G_B4_0 = L_12;
		G_B4_1 = L_11;
		G_B4_2 = G_B3_0;
		G_B4_3 = G_B3_1;
		G_B4_4 = G_B3_2;
		G_B4_5 = G_B3_3;
		G_B4_6 = G_B3_4;
		G_B4_7 = G_B3_5;
		if (L_13)
		{
			G_B5_0 = L_12;
			G_B5_1 = L_11;
			G_B5_2 = G_B3_0;
			G_B5_3 = G_B3_1;
			G_B5_4 = G_B3_2;
			G_B5_5 = G_B3_3;
			G_B5_6 = G_B3_4;
			G_B5_7 = G_B3_5;
			goto IL_0051;
		}
	}
	{
		il2cpp_codegen_initobj((&V_3), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_14 = V_3;
		G_B6_0 = L_14;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		G_B6_5 = G_B4_4;
		G_B6_6 = G_B4_5;
		G_B6_7 = G_B4_6;
		G_B6_8 = G_B4_7;
		goto IL_005f;
	}

IL_0051:
	{
		int32_t L_15 = ___1_vertexIndex;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_16), ((float)((int32_t)(L_15/3))), (0.0f), /*hidden argument*/NULL);
		G_B6_0 = L_16;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
		G_B6_5 = G_B5_4;
		G_B6_6 = G_B5_5;
		G_B6_7 = G_B5_6;
		G_B6_8 = G_B5_7;
	}

IL_005f:
	{
		il2cpp_codegen_initobj((&V_3), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_17 = V_3;
		il2cpp_codegen_initobj((&V_3), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_18 = V_3;
		il2cpp_codegen_initobj((&V_3), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_19 = V_3;
		il2cpp_codegen_initobj((&V_4), sizeof(BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F));
		BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F L_20 = V_4;
		FlexibleVertexDataUtils_BuildAndAddFlexibleVertexData_mC35367A0F41EE78D0D41CB73484C229156CC532F(G_B6_8, G_B6_7, G_B6_6, G_B6_5, G_B6_4, G_B6_3, G_B6_2, G_B6_1, G_B6_0, L_17, L_18, L_19, L_20, NULL);
		return;
	}
}
// UnityEngine.Vector3 TriLibCore.Stl.StlProcessor::ReadVector3(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 StlProcessor_ReadVector3_m08382B6430274ECDE456A6119A9E608EB5F92F87 (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_binaryReader, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_0 = ___0_binaryReader;
		NullCheck(L_0);
		float L_1;
		L_1 = VirtualFuncInvoker0< float >::Invoke(20 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_0);
		V_0 = L_1;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_2 = ___0_binaryReader;
		NullCheck(L_2);
		float L_3;
		L_3 = VirtualFuncInvoker0< float >::Invoke(20 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_2);
		V_1 = L_3;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = ___0_binaryReader;
		NullCheck(L_4);
		float L_5;
		L_5 = VirtualFuncInvoker0< float >::Invoke(20 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_4);
		V_2 = L_5;
		il2cpp_codegen_runtime_class_init_inline(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		bool L_6 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___LoadWithYUp_11;
		if (!L_6)
		{
			goto IL_0025;
		}
	}
	{
		float L_7 = V_0;
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_10), L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0025:
	{
		float L_11 = V_0;
		float L_12 = V_2;
		float L_13 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_14), L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Vector3 TriLibCore.Stl.StlProcessor::ReadVector3(TriLibCore.Stl.StlStreamReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 StlProcessor_ReadVector3_mD41B92963A4FC7C7BA2170A47130B00B0A6B1973 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* ___0_stlStreamReader, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_0 = ___0_stlStreamReader;
		NullCheck(L_0);
		float L_1;
		L_1 = StlStreamReader_ReadTokenAsFloat_m30C508ABFBF8FF2F47629BCFFB53C042F4B7F551(L_0, (bool)1, NULL);
		V_0 = L_1;
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_2 = ___0_stlStreamReader;
		NullCheck(L_2);
		float L_3;
		L_3 = StlStreamReader_ReadTokenAsFloat_m30C508ABFBF8FF2F47629BCFFB53C042F4B7F551(L_2, (bool)1, NULL);
		V_1 = L_3;
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_4 = ___0_stlStreamReader;
		NullCheck(L_4);
		float L_5;
		L_5 = StlStreamReader_ReadTokenAsFloat_m30C508ABFBF8FF2F47629BCFFB53C042F4B7F551(L_4, (bool)1, NULL);
		V_2 = L_5;
		il2cpp_codegen_runtime_class_init_inline(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		bool L_6 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___LoadWithYUp_11;
		if (!L_6)
		{
			goto IL_0028;
		}
	}
	{
		float L_7 = V_0;
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_10), L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0028:
	{
		float L_11 = V_0;
		float L_12 = V_2;
		float L_13 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_14), L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Stl.StlProcessor::ParseASCII(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlProcessor_ParseASCII_m297D902C12F8D0B07F75397F81C1BA6A82B918ED (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Values_m2CEBA9C111276D8D06435752E5C5B3598E722CBE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Values_m56D477CD7C6E64384E53F8B4C44C151CB9CADA48_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisIGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_m8A9221E5C3D95DC2EDD7FC163B30C1A3A3929D1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisIModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB_m8A288987F91069D02641D435D479CADC11764605_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6F5EC7239B41C242FCB23B64D91DA0070FC1C044);
		s_Il2CppMethodInitialized = true;
	}
	StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* V_0 = NULL;
	int32_t V_1 = 0;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_2 = NULL;
	bool V_3 = false;
	StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* V_4 = NULL;
	int64_t V_5 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	float V_7 = 0.0f;
	RuntimeObject* V_8 = NULL;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_13;
	memset((&V_13), 0, sizeof(V_13));
	String_t* G_B11_0 = NULL;
	StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* G_B11_1 = NULL;
	String_t* G_B10_0 = NULL;
	StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* G_B10_1 = NULL;
	{
		__this->____loopNumber_11 = 0;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_stream;
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_1 = (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5*)il2cpp_codegen_object_new(StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		StlStreamReader__ctor_m1DC2CC738B9E91B86FE02E6BFD2FA36770C08BE2(L_1, L_0, NULL);
		V_0 = L_1;
		V_1 = 0;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_2 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)3);
		V_2 = L_2;
		V_3 = (bool)0;
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_3 = (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142*)il2cpp_codegen_object_new(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		StlRootModel__ctor_mF3CCCD9F9A1339A398A4FA1EAA0D00B3AD7BB737(L_3, NULL);
		V_4 = L_3;
		goto IL_02cc;
	}

IL_0025:
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_4 = V_0;
		NullCheck(L_4);
		int64_t L_5;
		L_5 = StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822(L_4, (bool)0, (bool)0, NULL);
		V_5 = L_5;
		int64_t L_6 = V_5;
		if ((((int64_t)L_6) > ((int64_t)((int64_t)-1367968407301361207LL))))
		{
			goto IL_0071;
		}
	}
	{
		int64_t L_7 = V_5;
		if ((((int64_t)L_7) == ((int64_t)((int64_t)-5513532493766152582LL))))
		{
			goto IL_014a;
		}
	}
	{
		int64_t L_8 = V_5;
		if ((((int64_t)L_8) == ((int64_t)((int64_t)-4898810643358303851LL))))
		{
			goto IL_015d;
		}
	}
	{
		int64_t L_9 = V_5;
		if ((((int64_t)L_9) == ((int64_t)((int64_t)-1367968407301361207LL))))
		{
			goto IL_017b;
		}
	}
	{
		goto IL_02c8;
	}

IL_0071:
	{
		int64_t L_10 = V_5;
		if ((((int64_t)L_10) == ((int64_t)((int64_t)7096547112153259250LL))))
		{
			goto IL_00bc;
		}
	}
	{
		int64_t L_11 = V_5;
		if ((((int64_t)L_11) == ((int64_t)((int64_t)7096547112162183094LL))))
		{
			goto IL_0119;
		}
	}
	{
		int64_t L_12 = V_5;
		if ((!(((uint64_t)L_12) == ((uint64_t)((int64_t)7096547112165690854LL)))))
		{
			goto IL_02c8;
		}
	}
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14;
		L_14 = StlStreamReader_ReadTokenAsString_m48318F7F8AD205DB62D73904EDFFD9EED1C7396D(L_13, (bool)0, (bool)1, NULL);
		String_t* L_15 = L_14;
		G_B10_0 = L_15;
		G_B10_1 = __this;
		if (L_15)
		{
			G_B11_0 = L_15;
			G_B11_1 = __this;
			goto IL_00b0;
		}
	}
	{
		G_B11_0 = _stringLiteral6F5EC7239B41C242FCB23B64D91DA0070FC1C044;
		G_B11_1 = G_B10_1;
	}

IL_00b0:
	{
		NullCheck(G_B11_1);
		G_B11_1->____groupName_6 = G_B11_0;
		Il2CppCodeGenWriteBarrier((void**)(&G_B11_1->____groupName_6), (void*)G_B11_0);
		V_3 = (bool)0;
		goto IL_02cc;
	}

IL_00bc:
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_16 = V_0;
		NullCheck(L_16);
		int64_t L_17;
		L_17 = StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822(L_16, (bool)1, (bool)0, NULL);
		if ((((int64_t)L_17) == ((int64_t)((int64_t)-1367968407521166068LL))))
		{
			goto IL_00da;
		}
	}
	{
		Exception_t* L_18 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_18);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_18, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralE21886A8FD3908720B58122F94229E548DD3A51F)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StlProcessor_ParseASCII_m297D902C12F8D0B07F75397F81C1BA6A82B918ED_RuntimeMethod_var)));
	}

IL_00da:
	{
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_19 = __this->____reader_5;
		NullCheck(L_19);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_20;
		L_20 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_19, NULL);
		NullCheck(L_20);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_21 = L_20->___Options_0;
		NullCheck(L_21);
		bool L_22 = L_21->___ImportNormals_23;
		if (!L_22)
		{
			goto IL_0102;
		}
	}
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_23 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24;
		L_24 = StlProcessor_ReadVector3_mD41B92963A4FC7C7BA2170A47130B00B0A6B1973(L_23, NULL);
		__this->____facetNormal_12 = L_24;
		goto IL_02cc;
	}

IL_0102:
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_25 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26;
		L_26 = StlProcessor_ReadVector3_mD41B92963A4FC7C7BA2170A47130B00B0A6B1973(L_25, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27;
		L_27 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		__this->____facetNormal_12 = L_27;
		goto IL_02cc;
	}

IL_0119:
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_28 = V_0;
		NullCheck(L_28);
		int64_t L_29;
		L_29 = StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822(L_28, (bool)1, (bool)0, NULL);
		if ((((int64_t)L_29) == ((int64_t)((int64_t)6774539739450461193LL))))
		{
			goto IL_0137;
		}
	}
	{
		Exception_t* L_30 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_30);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_30, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral795FA97334306AE47C0C9744A2642732E6FED22D)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StlProcessor_ParseASCII_m297D902C12F8D0B07F75397F81C1BA6A82B918ED_RuntimeMethod_var)));
	}

IL_0137:
	{
		int32_t L_31 = __this->____loopNumber_11;
		__this->____loopNumber_11 = ((int32_t)il2cpp_codegen_add(L_31, 1));
		goto IL_02cc;
	}

IL_014a:
	{
		int32_t L_32 = __this->____loopNumber_11;
		__this->____loopNumber_11 = ((int32_t)il2cpp_codegen_subtract(L_32, 1));
		goto IL_02cc;
	}

IL_015d:
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_33 = V_0;
		NullCheck(L_33);
		int64_t L_34;
		L_34 = StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822(L_33, (bool)0, (bool)0, NULL);
		bool L_35 = V_3;
		if (L_35)
		{
			goto IL_02cc;
		}
	}
	{
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_36 = V_4;
		StlProcessor_AddModel_mD074A15E2C251A343E4D0B955563EE85DE37FAF8(__this, L_36, NULL);
		V_3 = (bool)1;
		goto IL_02cc;
	}

IL_017b:
	{
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_37 = __this->____reader_5;
		NullCheck(L_37);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_38;
		L_38 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_37, NULL);
		NullCheck(L_38);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_39 = L_38->___Options_0;
		NullCheck(L_39);
		bool L_40 = L_39->___ImportMeshes_10;
		if (!L_40)
		{
			goto IL_02cc;
		}
	}
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_41 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_42;
		L_42 = StlProcessor_ReadVector3_mD41B92963A4FC7C7BA2170A47130B00B0A6B1973(L_41, NULL);
		V_6 = L_42;
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_43 = __this->____reader_5;
		NullCheck(L_43);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_44;
		L_44 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_43, NULL);
		NullCheck(L_44);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_45 = L_44->___Options_0;
		NullCheck(L_45);
		float L_46 = L_45->___ScaleFactor_5;
		V_7 = L_46;
		RuntimeObject* L_47;
		L_47 = StlProcessor_GetActiveGeometryGroup_m7383FCD039100A7331EF5BD0528FCE078F143CDE(__this, NULL);
		V_8 = L_47;
		int32_t L_48 = V_1;
		int32_t L_49 = L_48;
		V_1 = ((int32_t)il2cpp_codegen_add(L_49, 1));
		V_9 = ((int32_t)(L_49%3));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_50 = V_2;
		int32_t L_51 = V_9;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_52 = V_6;
		float L_53 = V_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_54;
		L_54 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_52, L_53, NULL);
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_54);
		int32_t L_55 = V_9;
		if ((!(((uint32_t)L_55) == ((uint32_t)2))))
		{
			goto IL_02cc;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		bool L_56 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___LoadWithYUp_11;
		if (!L_56)
		{
			goto IL_01f0;
		}
	}
	{
		V_10 = 2;
		V_11 = 1;
		V_12 = 0;
		goto IL_01f9;
	}

IL_01f0:
	{
		V_10 = 0;
		V_11 = 1;
		V_12 = 2;
	}

IL_01f9:
	{
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_57 = V_2;
		NullCheck(L_57);
		int32_t L_58 = 0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_59 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_60 = V_2;
		NullCheck(L_60);
		int32_t L_61 = 1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_62 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_63 = V_2;
		NullCheck(L_63);
		int32_t L_64 = 2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_65 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_66;
		L_66 = StlProcessor_GetNormal_mAEB43873CA99A50EFBEE382491E9C211BF3AEDC4(L_59, L_62, L_65, NULL);
		V_13 = L_66;
		il2cpp_codegen_runtime_class_init_inline(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		bool L_67 = ((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___FixInfacingNormals_10;
		if (!L_67)
		{
			goto IL_0230;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_68 = V_13;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_69 = __this->____facetNormal_12;
		float L_70;
		L_70 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_68, L_69, NULL);
		if ((!(((float)L_70) > ((float)(0.0f)))))
		{
			goto IL_0277;
		}
	}

IL_0230:
	{
		RuntimeObject* L_71 = V_8;
		RuntimeObject* L_72 = V_8;
		NullCheck(L_72);
		int32_t L_73;
		L_73 = InterfaceFuncInvoker0< int32_t >::Invoke(46 /* System.Int32 TriLibCore.Interfaces.IGeometryGroup::get_VerticesDataCount() */, IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var, L_72);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_74 = V_2;
		int32_t L_75 = V_12;
		NullCheck(L_74);
		int32_t L_76 = L_75;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_71, L_73, L_77, NULL);
		RuntimeObject* L_78 = V_8;
		RuntimeObject* L_79 = V_8;
		NullCheck(L_79);
		int32_t L_80;
		L_80 = InterfaceFuncInvoker0< int32_t >::Invoke(46 /* System.Int32 TriLibCore.Interfaces.IGeometryGroup::get_VerticesDataCount() */, IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var, L_79);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_81 = V_2;
		int32_t L_82 = V_11;
		NullCheck(L_81);
		int32_t L_83 = L_82;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_84 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_78, L_80, L_84, NULL);
		RuntimeObject* L_85 = V_8;
		RuntimeObject* L_86 = V_8;
		NullCheck(L_86);
		int32_t L_87;
		L_87 = InterfaceFuncInvoker0< int32_t >::Invoke(46 /* System.Int32 TriLibCore.Interfaces.IGeometryGroup::get_VerticesDataCount() */, IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var, L_86);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_88 = V_2;
		int32_t L_89 = V_10;
		NullCheck(L_88);
		int32_t L_90 = L_89;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_91 = (L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90));
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_85, L_87, L_91, NULL);
		goto IL_02cc;
	}

IL_0277:
	{
		RuntimeObject* L_92 = V_8;
		RuntimeObject* L_93 = V_8;
		NullCheck(L_93);
		int32_t L_94;
		L_94 = InterfaceFuncInvoker0< int32_t >::Invoke(46 /* System.Int32 TriLibCore.Interfaces.IGeometryGroup::get_VerticesDataCount() */, IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var, L_93);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_95 = V_2;
		int32_t L_96 = V_10;
		NullCheck(L_95);
		int32_t L_97 = L_96;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_98 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_97));
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_92, L_94, L_98, NULL);
		RuntimeObject* L_99 = V_8;
		RuntimeObject* L_100 = V_8;
		NullCheck(L_100);
		int32_t L_101;
		L_101 = InterfaceFuncInvoker0< int32_t >::Invoke(46 /* System.Int32 TriLibCore.Interfaces.IGeometryGroup::get_VerticesDataCount() */, IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var, L_100);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_102 = V_2;
		int32_t L_103 = V_11;
		NullCheck(L_102);
		int32_t L_104 = L_103;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_99, L_101, L_105, NULL);
		RuntimeObject* L_106 = V_8;
		RuntimeObject* L_107 = V_8;
		NullCheck(L_107);
		int32_t L_108;
		L_108 = InterfaceFuncInvoker0< int32_t >::Invoke(46 /* System.Int32 TriLibCore.Interfaces.IGeometryGroup::get_VerticesDataCount() */, IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var, L_107);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_109 = V_2;
		int32_t L_110 = V_12;
		NullCheck(L_109);
		int32_t L_111 = L_110;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_112 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_111));
		StlProcessor_AddOutputVertex_m2625465CC93BE0B4E82EBC175F8CAAD8248D8DB7(__this, L_106, L_108, L_112, NULL);
		goto IL_02cc;
	}

IL_02be:
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_113 = V_0;
		NullCheck(L_113);
		int64_t L_114;
		L_114 = StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822(L_113, (bool)0, (bool)0, NULL);
		V_5 = L_114;
	}

IL_02c8:
	{
		int64_t L_115 = V_5;
		if (L_115)
		{
			goto IL_02be;
		}
	}

IL_02cc:
	{
		StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* L_116 = V_0;
		NullCheck(L_116);
		bool L_117;
		L_117 = StreamReader_get_EndOfStream_mAE054431BF21158178EAA2A6872F14A9ED6A3C3E(L_116, NULL);
		if (!L_117)
		{
			goto IL_0025;
		}
	}
	{
		StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* L_118 = __this->____reader_5;
		NullCheck(L_118);
		ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C(L_118, (1.0f), 0, (0.0f), NULL);
		bool L_119 = V_3;
		if (L_119)
		{
			goto IL_02f8;
		}
	}
	{
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_120 = V_4;
		StlProcessor_AddModel_mD074A15E2C251A343E4D0B955563EE85DE37FAF8(__this, L_120, NULL);
	}

IL_02f8:
	{
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_121 = V_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_122;
		L_122 = Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline(NULL);
		NullCheck(L_121);
		StlModel_set_LocalScale_m0709587074F69EEB1DCF2F639318F4C9BE86D840_inline(L_121, L_122, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_123 = V_4;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_124;
		L_124 = Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline(NULL);
		NullCheck(L_123);
		StlModel_set_LocalRotation_mE9C27ACA9705C49C2EEEEC359CF9195A0EADF5B9_inline(L_123, L_124, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_125 = V_4;
		NullCheck(L_125);
		StlModel_set_Visibility_m635AB4624DB7C9E08470800B07ABABD332AF0E01_inline(L_125, (bool)1, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_126 = V_4;
		Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* L_127 = __this->____models_4;
		NullCheck(L_127);
		ValueCollection_t52D0D70A98CDE4A09BB9B7D39C2159B31ECAE027* L_128;
		L_128 = Dictionary_2_get_Values_m56D477CD7C6E64384E53F8B4C44C151CB9CADA48(L_127, Dictionary_2_get_Values_m56D477CD7C6E64384E53F8B4C44C151CB9CADA48_RuntimeMethod_var);
		IModelU5BU5D_tE19C9CE57A4C086398F86D3E8410C416B390CC76* L_129;
		L_129 = Enumerable_ToArray_TisIModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB_m8A288987F91069D02641D435D479CADC11764605(L_128, Enumerable_ToArray_TisIModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB_m8A288987F91069D02641D435D479CADC11764605_RuntimeMethod_var);
		NullCheck(L_126);
		StlModel_set_Children_m8C26332902AFB18940DAB4060807171A0364EA1E_inline(L_126, (RuntimeObject*)L_129, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_130 = V_4;
		Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* L_131 = __this->____geometryGroups_3;
		NullCheck(L_131);
		ValueCollection_tCCCE4A1A9D973B131015A437780697A2155EAACD* L_132;
		L_132 = Dictionary_2_get_Values_m2CEBA9C111276D8D06435752E5C5B3598E722CBE(L_131, Dictionary_2_get_Values_m2CEBA9C111276D8D06435752E5C5B3598E722CBE_RuntimeMethod_var);
		IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711* L_133;
		L_133 = Enumerable_ToArray_TisIGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_m8A9221E5C3D95DC2EDD7FC163B30C1A3A3929D1A(L_132, Enumerable_ToArray_TisIGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_m8A9221E5C3D95DC2EDD7FC163B30C1A3A3929D1A_RuntimeMethod_var);
		NullCheck(L_130);
		StlRootModel_set_AllGeometryGroups_m6754461757172DBEB0986B7A916BBBFFDAD9E173_inline(L_130, (RuntimeObject*)L_133, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_134 = V_4;
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_135 = V_4;
		NullCheck(L_135);
		RuntimeObject* L_136;
		L_136 = StlModel_get_Children_mCC99BF668D976265E2897D884E1080A14B8219BB_inline(L_135, NULL);
		NullCheck(L_134);
		StlRootModel_set_AllModels_mED5ABBDE53F3952C9E492CC70CADE0692BA9A7BF_inline(L_134, L_136, NULL);
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_137 = V_4;
		return L_137;
	}
}
// System.Void TriLibCore.Stl.StlProcessor::AddModel(TriLibCore.Interfaces.IModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlProcessor_AddModel_mD074A15E2C251A343E4D0B955563EE85DE37FAF8 (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, RuntimeObject* ___0_parent, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m53D84560875155F383C73DA9027DE4CC7AC985D5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IObject_t2E20027AB39DAC66068F50E33ECB233711D9DA5F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* V_1 = NULL;
	{
		RuntimeObject* L_0;
		L_0 = StlProcessor_GetActiveGeometryGroup_m7383FCD039100A7331EF5BD0528FCE078F143CDE(__this, NULL);
		V_0 = L_0;
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_1 = (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA*)il2cpp_codegen_object_new(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		StlModel__ctor_m6E2FE456538DB2C0EB1C98D3144C193CF6157D1C(L_1, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_2 = L_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline(NULL);
		NullCheck(L_2);
		StlModel_set_LocalScale_m0709587074F69EEB1DCF2F639318F4C9BE86D840_inline(L_2, L_3, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_4 = L_2;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5;
		L_5 = Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline(NULL);
		NullCheck(L_4);
		StlModel_set_LocalRotation_mE9C27ACA9705C49C2EEEEC359CF9195A0EADF5B9_inline(L_4, L_5, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_6 = L_4;
		RuntimeObject* L_7 = V_0;
		NullCheck(L_6);
		StlModel_set_GeometryGroup_m32211C6B2E30C396961F21D3E5E81B925AAA444E_inline(L_6, L_7, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_8 = L_6;
		RuntimeObject* L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10;
		L_10 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String TriLibCore.Interfaces.IObject::get_Name() */, IObject_t2E20027AB39DAC66068F50E33ECB233711D9DA5F_il2cpp_TypeInfo_var, L_9);
		NullCheck(L_8);
		StlModel_set_Name_mF8213C9BAD85BC9320C876C41ABBF594327F2814_inline(L_8, L_10, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_11 = L_8;
		NullCheck(L_11);
		StlModel_set_Visibility_m635AB4624DB7C9E08470800B07ABABD332AF0E01_inline(L_11, (bool)1, NULL);
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_12 = L_11;
		RuntimeObject* L_13 = ___0_parent;
		NullCheck(L_12);
		StlModel_set_Parent_m5FFEE1518EF3F12F1378AECEAEB41466CCA20DCE_inline(L_12, L_13, NULL);
		V_1 = L_12;
		Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* L_14 = __this->____models_4;
		String_t* L_15 = __this->____groupName_6;
		StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* L_16 = V_1;
		NullCheck(L_14);
		Dictionary_2_Add_m53D84560875155F383C73DA9027DE4CC7AC985D5(L_14, L_15, L_16, Dictionary_2_Add_m53D84560875155F383C73DA9027DE4CC7AC985D5_RuntimeMethod_var);
		return;
	}
}
// System.Boolean TriLibCore.Stl.StlProcessor::IsBinary(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StlProcessor_IsBinary_m4A82EE25ADBA42AEB6202DCBA35C3A72B239D920 (Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		V_0 = (bool)0;
	}

IL_0002:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_stream;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtualFuncInvoker0< int32_t >::Invoke(36 /* System.Int32 System.IO.Stream::ReadByte() */, L_0);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)32))))
		{
			goto IL_0002;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)13))))
		{
			goto IL_0002;
		}
	}
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)10))))
		{
			goto IL_0002;
		}
	}
	{
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)9))))
		{
			goto IL_0002;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0023:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_7 = ___0_stream;
		NullCheck(L_7);
		int64_t L_8;
		L_8 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_7, ((int64_t)0), 0);
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Void TriLibCore.Stl.StlProcessor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlProcessor__ctor_m900A44553FC03D3F3F05833E176C5456FF72A390 (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m16C7DB0395DCD9EF714A07CDB8934C3FCAE7F50C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m5F5C68AF90E49C38EB7E07DDE14E85B7C3875390_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6F5EC7239B41C242FCB23B64D91DA0070FC1C044);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* L_0 = (Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983*)il2cpp_codegen_object_new(Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Dictionary_2__ctor_m16C7DB0395DCD9EF714A07CDB8934C3FCAE7F50C(L_0, Dictionary_2__ctor_m16C7DB0395DCD9EF714A07CDB8934C3FCAE7F50C_RuntimeMethod_var);
		__this->____geometryGroups_3 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____geometryGroups_3), (void*)L_0);
		Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* L_1 = (Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B*)il2cpp_codegen_object_new(Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		Dictionary_2__ctor_m5F5C68AF90E49C38EB7E07DDE14E85B7C3875390(L_1, Dictionary_2__ctor_m5F5C68AF90E49C38EB7E07DDE14E85B7C3875390_RuntimeMethod_var);
		__this->____models_4 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____models_4), (void*)L_1);
		__this->____groupName_6 = _stringLiteral6F5EC7239B41C242FCB23B64D91DA0070FC1C044;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____groupName_6), (void*)_stringLiteral6F5EC7239B41C242FCB23B64D91DA0070FC1C044);
		__this->____lastLoopNumber_8 = (-1);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2;
		L_2 = Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline(NULL);
		__this->____partColor_21 = L_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_3;
		L_3 = Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline(NULL);
		__this->____facetColor_22 = L_3;
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlRootModel::get_AllModels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlRootModel_get_AllModels_m7B400E2A938FBA1B713F5B638F4D4E5DD1F41320 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllModelsU3Ek__BackingField_14;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlRootModel::set_AllModels(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlRootModel_set_AllModels_mED5ABBDE53F3952C9E492CC70CADE0692BA9A7BF (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllModelsU3Ek__BackingField_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllModelsU3Ek__BackingField_14), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Stl.StlRootModel::get_AllGeometryGroups()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlRootModel_get_AllGeometryGroups_m894A6349DF34C591526ED2C3C6E518769A460D57 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllGeometryGroupsU3Ek__BackingField_15;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlRootModel::set_AllGeometryGroups(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlRootModel_set_AllGeometryGroups_m6754461757172DBEB0986B7A916BBBFFDAD9E173 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllGeometryGroupsU3Ek__BackingField_15 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllGeometryGroupsU3Ek__BackingField_15), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Stl.StlRootModel::get_AllAnimations()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlRootModel_get_AllAnimations_m983D6197DF21C9AFC9C4ED531E3C3D15CE29A43F (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllAnimationsU3Ek__BackingField_16;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlRootModel::set_AllAnimations(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlRootModel_set_AllAnimations_mF3974A24AAF1B9E02553DB61087FC2B444AB6B16 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllAnimationsU3Ek__BackingField_16 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllAnimationsU3Ek__BackingField_16), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Stl.StlRootModel::get_AllMaterials()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlRootModel_get_AllMaterials_m369A2E061EEEBB5FA02C4C55D3E6CAC8536D4677 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllMaterialsU3Ek__BackingField_17;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlRootModel::set_AllMaterials(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlRootModel_set_AllMaterials_mB225832F17CF5D1032E1916BB6FACA683EDB2E84 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllMaterialsU3Ek__BackingField_17 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllMaterialsU3Ek__BackingField_17), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Stl.StlRootModel::get_AllTextures()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlRootModel_get_AllTextures_m43171DBA9C24AB03600029495AF55C695B14C930 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllTexturesU3Ek__BackingField_18;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlRootModel::set_AllTextures(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlRootModel_set_AllTextures_mC60F360CDA417F4354120731CD90A23491BDA6C3 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllTexturesU3Ek__BackingField_18 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllTexturesU3Ek__BackingField_18), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Stl.StlRootModel::get_AllCameras()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlRootModel_get_AllCameras_m75A7DE8FE24C05CA5C891F1CED351704E86CB7BF (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllCamerasU3Ek__BackingField_19;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlRootModel::set_AllCameras(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlRootModel_set_AllCameras_m2E66426E7AB81C6911695E7B947735D484DB9431 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllCamerasU3Ek__BackingField_19 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllCamerasU3Ek__BackingField_19), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Stl.StlRootModel::get_AllLights()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlRootModel_get_AllLights_m2BE07C476BA4C56E4167C5C2A1D7449E6F60AEBD (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllLightsU3Ek__BackingField_20;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlRootModel::set_AllLights(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlRootModel_set_AllLights_m44C346619F80A9FCFA5979F3BF8033F020FE68A2 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllLightsU3Ek__BackingField_20 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllLightsU3Ek__BackingField_20), (void*)L_0);
		return;
	}
}
// System.Void TriLibCore.Stl.StlRootModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlRootModel__ctor_mF3CCCD9F9A1339A398A4FA1EAA0D00B3AD7BB737 (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, const RuntimeMethod* method) 
{
	{
		StlModel__ctor_m6E2FE456538DB2C0EB1C98D3144C193CF6157D1C(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 TriLibCore.Stl.StlStreamReader::get_Line()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StlStreamReader_get_Line_mE94255EEFEFED4D8DED8AE024CE752F78FD174EC (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CLineU3Ek__BackingField_27;
		return L_0;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::set_Line(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader_set_Line_mBAF946CC6C4CB5BDC48AC3D5050570286FD0C628 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CLineU3Ek__BackingField_27 = L_0;
		return;
	}
}
// System.Int32 TriLibCore.Stl.StlStreamReader::get_Column()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StlStreamReader_get_Column_m78B316197DD99FD46120765F960B450AA0260A91 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0;
		L_0 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(18 /* System.IO.Stream System.IO.StreamReader::get_BaseStream() */, __this);
		NullCheck(L_0);
		int64_t L_1;
		L_1 = VirtualFuncInvoker0< int64_t >::Invoke(12 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		int32_t L_2 = __this->____endOfLinePointer_22;
		return ((int32_t)((int64_t)il2cpp_codegen_subtract(L_1, ((int64_t)L_2))));
	}
}
// System.Single TriLibCore.Stl.StlStreamReader::ReadTokenAsFloat(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float StlStreamReader_ReadTokenAsFloat_m30C508ABFBF8FF2F47629BCFFB53C042F4B7F551 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, bool ___0_required, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		bool L_0 = ___0_required;
		int64_t L_1;
		L_1 = StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822(__this, L_0, (bool)0, NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return (-1.0f);
	}

IL_0010:
	{
		bool L_2;
		L_2 = StlStreamReader_GetTokenAsFloat_mAA1D75FACB25386AF1A9ED7210239ED0369EDD3F(__this, (&V_0), NULL);
		if (L_2)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_3;
		L_3 = StlStreamReader_get_Line_mE94255EEFEFED4D8DED8AE024CE752F78FD174EC_inline(__this, NULL);
		int32_t L_4 = L_3;
		RuntimeObject* L_5 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_4);
		int32_t L_6;
		L_6 = StlStreamReader_get_Column_m78B316197DD99FD46120765F960B450AA0260A91(__this, NULL);
		int32_t L_7 = L_6;
		RuntimeObject* L_8 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_7);
		String_t* L_9;
		L_9 = String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral8E84C7CBF5EA8FD2E1EDA86178896A7CB3F34B81)), L_5, L_8, NULL);
		Exception_t* L_10 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_10);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_10, L_9, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StlStreamReader_ReadTokenAsFloat_m30C508ABFBF8FF2F47629BCFFB53C042F4B7F551_RuntimeMethod_var)));
	}

IL_0040:
	{
		float L_11 = V_0;
		return L_11;
	}
}
// System.String TriLibCore.Stl.StlStreamReader::ReadTokenAsString(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StlStreamReader_ReadTokenAsString_m48318F7F8AD205DB62D73904EDFFD9EED1C7396D (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, bool ___0_required, bool ___1_ignoreSpaces, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_required;
		bool L_1 = ___1_ignoreSpaces;
		int64_t L_2;
		L_2 = StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822(__this, L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_3;
		L_3 = StlStreamReader_GetTokenAsString_m3B5F90A8A2E6492B1F50E55127380D3CB334E611(__this, NULL);
		return L_3;
	}

IL_0011:
	{
		return (String_t*)NULL;
	}
}
// System.Boolean TriLibCore.Stl.StlStreamReader::TokenStartsWith(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StlStreamReader_TokenStartsWith_mA82610E51F82E2B87E0ADE90E499875143738B38 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___0_value;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_0, NULL);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_2 = __this->____charStream_24;
		NullCheck(L_2);
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length)))))
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}

IL_0012:
	{
		V_0 = 0;
		goto IL_002d;
	}

IL_0016:
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_3 = __this->____charStream_24;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint16_t L_6 = (uint16_t)(L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		String_t* L_7 = ___0_value;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		Il2CppChar L_9;
		L_9 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_7, L_8, NULL);
		if ((((int32_t)L_6) == ((int32_t)L_9)))
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_10, 1));
	}

IL_002d:
	{
		int32_t L_11 = V_0;
		String_t* L_12 = ___0_value;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_12, NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}
}
// System.Int32 TriLibCore.Stl.StlStreamReader::GetCharAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StlStreamReader_GetCharAt_m25F369E411A67D8A5D9B05DAB07A212085346505 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = __this->____charStream_24;
		int32_t L_1 = ___0_index;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		uint16_t L_3 = (uint16_t)(L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.String TriLibCore.Stl.StlStreamReader::GetTokenAsString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StlStreamReader_GetTokenAsString_m3B5F90A8A2E6492B1F50E55127380D3CB334E611 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, const RuntimeMethod* method) 
{
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = __this->____charStream_24;
		int32_t L_1 = __this->____charPosition_26;
		String_t* L_2;
		L_2 = String_CreateString_mB7B3AC2AF28010538650051A9000369B1CD6BAB6(NULL, L_0, 0, L_1, NULL);
		return L_2;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::UpdateStringFromCharString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader_UpdateStringFromCharString_m807C6BD8C68A20014717D4ED10E7A9A92F431C0D (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, const RuntimeMethod* method) 
{
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = __this->____charStream_24;
		int32_t L_1 = __this->____charPosition_26;
		String_t* L_2;
		L_2 = String_CreateString_mB7B3AC2AF28010538650051A9000369B1CD6BAB6(NULL, L_0, 0, L_1, NULL);
		__this->____charString_25 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_2);
		return;
	}
}
// System.Boolean TriLibCore.Stl.StlStreamReader::GetTokenAsFloat(System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StlStreamReader_GetTokenAsFloat_mAA1D75FACB25386AF1A9ED7210239ED0369EDD3F (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, float* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StlStreamReader_UpdateStringFromCharString_m807C6BD8C68A20014717D4ED10E7A9A92F431C0D(__this, NULL);
		String_t* L_0 = __this->____charString_25;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_1;
		L_1 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		float* L_2 = ___0_value;
		bool L_3;
		L_3 = Single_TryParse_mFB8CC32F0016FBB6EFCB97953CF3515767EB6431(L_0, ((int32_t)511), L_1, L_2, NULL);
		return L_3;
	}
}
// System.Boolean TriLibCore.Stl.StlStreamReader::GetTokenAsInt(System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StlStreamReader_GetTokenAsInt_m4C6E688A16834814001B55475E31CBF196856775 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, int32_t* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StlStreamReader_UpdateStringFromCharString_m807C6BD8C68A20014717D4ED10E7A9A92F431C0D(__this, NULL);
		String_t* L_0 = __this->____charString_25;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_1;
		L_1 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		int32_t* L_2 = ___0_value;
		bool L_3;
		L_3 = Int32_TryParse_mB8E246A7D6D6308EF36DE3473643BDE4CF8F71FF(L_0, ((int32_t)511), L_1, L_2, NULL);
		return L_3;
	}
}
// System.Int64 TriLibCore.Stl.StlStreamReader::ReadToken(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, bool ___0_required, bool ___1_ignoreSpaces, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	int32_t V_4 = 0;
	Il2CppChar V_5 = 0x0;
	int32_t V_6 = 0;
	int32_t G_B13_0 = 0;
	{
		__this->____charPosition_26 = 0;
		V_0 = (bool)1;
		V_1 = (bool)0;
		goto IL_005b;
	}

IL_000d:
	{
		int32_t L_0;
		L_0 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, __this);
		V_2 = L_0;
		int32_t L_1 = V_2;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_2 = V_2;
		V_3 = ((int32_t)(uint16_t)L_2);
		Il2CppChar L_3 = V_3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_3, ((int32_t)9))))
		{
			case 0:
			{
				goto IL_004c;
			}
			case 1:
			{
				goto IL_003f;
			}
			case 2:
			{
				goto IL_0055;
			}
			case 3:
			{
				goto IL_0055;
			}
			case 4:
			{
				goto IL_004c;
			}
		}
	}
	{
		Il2CppChar L_4 = V_3;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)32))))
		{
			goto IL_004c;
		}
	}
	{
		goto IL_0055;
	}

IL_003f:
	{
		int32_t L_5;
		L_5 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		V_1 = (bool)1;
		V_0 = (bool)0;
		goto IL_005b;
	}

IL_004c:
	{
		int32_t L_6;
		L_6 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		goto IL_005b;
	}

IL_0055:
	{
		V_0 = (bool)0;
		goto IL_005b;
	}

IL_0059:
	{
		V_0 = (bool)0;
	}

IL_005b:
	{
		bool L_7 = V_0;
		if (L_7)
		{
			goto IL_000d;
		}
	}
	{
		bool L_8 = V_1;
		if (L_8)
		{
			goto IL_006c;
		}
	}
	{
		bool L_9;
		L_9 = StreamReader_get_EndOfStream_mAE054431BF21158178EAA2A6872F14A9ED6A3C3E(__this, NULL);
		G_B13_0 = ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_006d;
	}

IL_006c:
	{
		G_B13_0 = 0;
	}

IL_006d:
	{
		V_0 = (bool)G_B13_0;
		goto IL_0110;
	}

IL_0073:
	{
		int32_t L_10;
		L_10 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, __this);
		V_4 = L_10;
		int32_t L_11 = V_4;
		if ((((int32_t)L_11) < ((int32_t)0)))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_12 = V_4;
		V_5 = ((int32_t)(uint16_t)L_12);
		Il2CppChar L_13 = V_5;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_13, ((int32_t)9))))
		{
			case 0:
			{
				goto IL_00bb;
			}
			case 1:
			{
				goto IL_00ae;
			}
			case 2:
			{
				goto IL_00e8;
			}
			case 3:
			{
				goto IL_00e8;
			}
			case 4:
			{
				goto IL_00b2;
			}
		}
	}
	{
		Il2CppChar L_14 = V_5;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_00bb;
		}
	}
	{
		goto IL_00e8;
	}

IL_00ae:
	{
		V_0 = (bool)0;
		goto IL_0110;
	}

IL_00b2:
	{
		int32_t L_15;
		L_15 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		goto IL_0110;
	}

IL_00bb:
	{
		bool L_16 = ___1_ignoreSpaces;
		if (!L_16)
		{
			goto IL_00e4;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_17 = __this->____charStream_24;
		int32_t L_18 = __this->____charPosition_26;
		V_6 = L_18;
		int32_t L_19 = V_6;
		__this->____charPosition_26 = ((int32_t)il2cpp_codegen_add(L_19, 1));
		int32_t L_20 = V_6;
		Il2CppChar L_21 = V_5;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (Il2CppChar)L_21);
		int32_t L_22;
		L_22 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		goto IL_0110;
	}

IL_00e4:
	{
		V_0 = (bool)0;
		goto IL_0110;
	}

IL_00e8:
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_23 = __this->____charStream_24;
		int32_t L_24 = __this->____charPosition_26;
		V_6 = L_24;
		int32_t L_25 = V_6;
		__this->____charPosition_26 = ((int32_t)il2cpp_codegen_add(L_25, 1));
		int32_t L_26 = V_6;
		Il2CppChar L_27 = V_5;
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (Il2CppChar)L_27);
		int32_t L_28;
		L_28 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		goto IL_0110;
	}

IL_010e:
	{
		V_0 = (bool)0;
	}

IL_0110:
	{
		bool L_29 = V_0;
		if (L_29)
		{
			goto IL_0073;
		}
	}
	{
		bool L_30 = ___0_required;
		if (!L_30)
		{
			goto IL_0147;
		}
	}
	{
		int32_t L_31 = __this->____charPosition_26;
		if (L_31)
		{
			goto IL_0147;
		}
	}
	{
		int32_t L_32;
		L_32 = StlStreamReader_get_Line_mE94255EEFEFED4D8DED8AE024CE752F78FD174EC_inline(__this, NULL);
		int32_t L_33 = L_32;
		RuntimeObject* L_34 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_33);
		int32_t L_35;
		L_35 = StlStreamReader_get_Column_m78B316197DD99FD46120765F960B450AA0260A91(__this, NULL);
		int32_t L_36 = L_35;
		RuntimeObject* L_37 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_36);
		String_t* L_38;
		L_38 = String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral3063F5F4FAEA0002A9B169DA0D12D52AC6312C1A)), L_34, L_37, NULL);
		Exception_t* L_39 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_39);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_39, L_38, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_39, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StlStreamReader_ReadToken_m66B4713DAD087A09068A8EF495F3CA3FD9B16822_RuntimeMethod_var)));
	}

IL_0147:
	{
		int32_t L_40 = __this->____charPosition_26;
		if (!L_40)
		{
			goto IL_016b;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_41 = __this->____charStream_24;
		int32_t L_42 = __this->____charPosition_26;
		int32_t L_43;
		L_43 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(L_42, ((int32_t)1024), NULL);
		int64_t L_44;
		L_44 = HashUtils_GetHash_mA8618222D626B54CD67FF88D56037B6D8024453E((RuntimeObject*)L_41, L_43, NULL);
		return L_44;
	}

IL_016b:
	{
		return ((int64_t)0);
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_m1DC2CC738B9E91B86FE02E6BFD2FA36770C08BE2 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2 = ___0_stream;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_mAFA827D6D825FEC2C29C73B65C2DD1AB9076DEC7(__this, L_2, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.IO.Stream,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_m034B0FC2FA03335804B0CA1E4543333C6D55F5D2 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, bool ___1_detectEncodingFromByteOrderMarks, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2 = ___0_stream;
		bool L_3 = ___1_detectEncodingFromByteOrderMarks;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_m8ADB6C6B363A2B58B9BC3CB1939A1BABE0BF064A(__this, L_2, L_3, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.IO.Stream,System.Text.Encoding)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_m439C0BC91DA0056CFE99333A3F46003F3BA249FA (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2 = ___0_stream;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_3 = ___1_encoding;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_m7712DDC735E99B6833E2666ADFD8A06CB96A58B1(__this, L_2, L_3, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.IO.Stream,System.Text.Encoding,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_m32AD5AE244673E0BC38ADAD24556915A774A89F8 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, bool ___2_detectEncodingFromByteOrderMarks, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2 = ___0_stream;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_3 = ___1_encoding;
		bool L_4 = ___2_detectEncodingFromByteOrderMarks;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_mE4095A4D9B6E2E82E95CE884443A51635849A740(__this, L_2, L_3, L_4, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.IO.Stream,System.Text.Encoding,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_m91248D2367CA954255DC01B02F049CC8771E9669 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, bool ___2_detectEncodingFromByteOrderMarks, int32_t ___3_bufferSize, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2 = ___0_stream;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_3 = ___1_encoding;
		bool L_4 = ___2_detectEncodingFromByteOrderMarks;
		int32_t L_5 = ___3_bufferSize;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_mD3E001CD426B3FE451FFA32E7070E34AC1756673(__this, L_2, L_3, L_4, L_5, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_mCA067EB4C12A2A6CBC25626D9FA7B2D91120B776 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, String_t* ___0_path, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		String_t* L_2 = ___0_path;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_m08BA7049EACE030ACE06AB8A8F2CDF2E2AFB55C6(__this, L_2, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_m3A735D43486C306A2273D84EE67B895D4FACCF03 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, String_t* ___0_path, bool ___1_detectEncodingFromByteOrderMarks, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		String_t* L_2 = ___0_path;
		bool L_3 = ___1_detectEncodingFromByteOrderMarks;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_m0AD738DDCB9A0DE0DFD3DB6B2FE44A41C1EAE677(__this, L_2, L_3, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.String,System.Text.Encoding)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_mE73A348052046582BB63BFA2C531243D92703C81 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, String_t* ___0_path, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		String_t* L_2 = ___0_path;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_3 = ___1_encoding;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_m3C693DE567FB306355ECD44489F58699105DDE43(__this, L_2, L_3, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.String,System.Text.Encoding,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_mA0A64140AFA965AD7ED23BBEB39083ECFCCF11D8 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, String_t* ___0_path, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, bool ___2_detectEncodingFromByteOrderMarks, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		String_t* L_2 = ___0_path;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_3 = ___1_encoding;
		bool L_4 = ___2_detectEncodingFromByteOrderMarks;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_mB946592899E393BDD06A093CA3BB87180A590449(__this, L_2, L_3, L_4, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.StlStreamReader::.ctor(System.String,System.Text.Encoding,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlStreamReader__ctor_mD9FB81837EDD35FDEA4EBEEF111A39F0A01E8D83 (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, String_t* ___0_path, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, bool ___2_detectEncodingFromByteOrderMarks, int32_t ___3_bufferSize, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->____charStream_24 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_24), (void*)L_0);
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)1024), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		String_t* L_2 = ___0_path;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_3 = ___1_encoding;
		bool L_4 = ___2_detectEncodingFromByteOrderMarks;
		int32_t L_5 = ___3_bufferSize;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_mA95373EEF162CF396A1A20CDF039B29AA2D634EF(__this, L_2, L_3, L_4, L_5, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String[] TriLibCore.Stl.Reader.StlReader::GetExtensions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* StlReader_GetExtensions_m1CCD113694B899BE07A468EDFE97255DF9032DD9 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0F9A5767B5106090414D118D94D8CC4F1F188C6);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralB0F9A5767B5106090414D118D94D8CC4F1F188C6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralB0F9A5767B5106090414D118D94D8CC4F1F188C6);
		return L_1;
	}
}
// System.String TriLibCore.Stl.Reader.StlReader::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StlReader_get_Name_m61B4C05B95CA2ADBE486CD79411580E97CD901BD (StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC3007E4D4C9A026A571CD47422BE823D8A32DC2A);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteralC3007E4D4C9A026A571CD47422BE823D8A32DC2A;
	}
}
// System.Type TriLibCore.Stl.Reader.StlReader::get_LoadingStepEnumType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* StlReader_get_LoadingStepEnumType_mC352EED7670738FB3BFC9EBB6C98FFC961207830 (StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProcessingSteps_t0ED2265911192A39BA3686E081CB2D9F4DA68078_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_0 = { reinterpret_cast<intptr_t> (ProcessingSteps_t0ED2265911192A39BA3686E081CB2D9F4DA68078_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_1;
		L_1 = Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57(L_0, NULL);
		return L_1;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Stl.Reader.StlReader::ReadStream(System.IO.Stream,TriLibCore.AssetLoaderContext,System.String,System.Action`2<TriLibCore.AssetLoaderContext,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlReader_ReadStream_m5C6B1D87BD9D396ACAB6F7BDDFC0E3CA6FFA075C (StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, String_t* ___2_filename, Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___3_onProgress, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_stream;
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_1 = ___1_assetLoaderContext;
		String_t* L_2 = ___2_filename;
		Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* L_3 = ___3_onProgress;
		RuntimeObject* L_4;
		L_4 = ReaderBase_ReadStream_m725378DF096B29E0DB3BE3FB9E5F1E37747883F4(__this, L_0, L_1, L_2, L_3, NULL);
		ReaderBase_SetupStream_mCDC78453E3657CB3FBB713C40FB50B4941455942(__this, (&___0_stream), NULL);
		StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0* L_5 = (StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0*)il2cpp_codegen_object_new(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		StlProcessor__ctor_m900A44553FC03D3F3F05833E176C5456FF72A390(L_5, NULL);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_6 = ___0_stream;
		NullCheck(L_5);
		RuntimeObject* L_7;
		L_7 = StlProcessor_Process_mE9579D6B475D44423897704E91BD2A780B6405CF(L_5, __this, L_6, NULL);
		V_0 = L_7;
		ReaderBase_PostProcessModel_mD3BB953DE8BEB5FCA65077455BDA0750F740C667(__this, (&V_0), NULL);
		RuntimeObject* L_8 = V_0;
		return L_8;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Stl.Reader.StlReader::CreateRootModel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StlReader_CreateRootModel_m950B39752C2801E33B58DDD686FF8B2DB6F070EC (StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* L_0 = (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142*)il2cpp_codegen_object_new(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		StlRootModel__ctor_mF3CCCD9F9A1339A398A4FA1EAA0D00B3AD7BB737(L_0, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Stl.Reader.StlReader::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlReader__ctor_m18E86780078062C2974BBA8AF955AFDDD791CAF4 (StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* __this, const RuntimeMethod* method) 
{
	{
		ReaderBase__ctor_m5C4FE7A4BC205B65DAB56FF3CC5202D0B04937DA(__this, NULL);
		return;
	}
}
// System.Void TriLibCore.Stl.Reader.StlReader::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StlReader__cctor_m36423879784E47346E20D042A50BA986672DE63F (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___FixInfacingNormals_10 = (bool)0;
		((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___LoadWithYUp_11 = (bool)0;
		((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___StoreTriangleIndexInTexCoord0_12 = (bool)1;
		((StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields*)il2cpp_codegen_static_fields_for(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_il2cpp_TypeInfo_var))->___ImportNormals_13 = (bool)1;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* StlModel_get_Name_m25E9B99DE29E6200C71933B5E52D158E28DCD941_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, const RuntimeMethod* method) 
{
	{
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_0 = __this->___U3CAssetLoaderContextU3Ek__BackingField_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___1_b;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_a;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_b;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_a;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_b;
		float L_11 = L_10.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_12), ((float)il2cpp_codegen_subtract(L_1, L_3)), ((float)il2cpp_codegen_subtract(L_5, L_7)), ((float)il2cpp_codegen_subtract(L_9, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_lhs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_rhs, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_lhs;
		float L_1 = L_0.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___1_rhs;
		float L_3 = L_2.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_lhs;
		float L_5 = L_4.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_rhs;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_lhs;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_rhs;
		float L_11 = L_10.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12 = ___0_lhs;
		float L_13 = L_12.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14 = ___1_rhs;
		float L_15 = L_14.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = ___0_lhs;
		float L_17 = L_16.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18 = ___1_rhs;
		float L_19 = L_18.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_20 = ___0_lhs;
		float L_21 = L_20.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22 = ___1_rhs;
		float L_23 = L_22.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24;
		memset((&L_24), 0, sizeof(L_24));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_24), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_9, L_11)), ((float)il2cpp_codegen_multiply(L_13, L_15)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_17, L_19)), ((float)il2cpp_codegen_multiply(L_21, L_23)))), /*hidden argument*/NULL);
		V_0 = L_24;
		goto IL_005a;
	}

IL_005a:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25 = V_0;
		return L_25;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)__this);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline(L_0, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, float ___3_a, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_r;
		__this->___r_0 = L_0;
		float L_1 = ___1_g;
		__this->___g_1 = L_1;
		float L_2 = ___2_b;
		__this->___b_2 = L_2;
		float L_3 = ___3_a;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___oneVector_6;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_LocalScale_m0709587074F69EEB1DCF2F639318F4C9BE86D840_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->___U3CLocalScaleU3Ek__BackingField_4 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ((Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var))->___identityQuaternion_4;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_LocalRotation_mE9C27ACA9705C49C2EEEEC359CF9195A0EADF5B9_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_value, const RuntimeMethod* method) 
{
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___0_value;
		__this->___U3CLocalRotationU3Ek__BackingField_3 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_GeometryGroup_m32211C6B2E30C396961F21D3E5E81B925AAA444E_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CGeometryGroupU3Ek__BackingField_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CGeometryGroupU3Ek__BackingField_9), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_Name_mF8213C9BAD85BC9320C876C41ABBF594327F2814_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_Visibility_m635AB4624DB7C9E08470800B07ABABD332AF0E01_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CVisibilityU3Ek__BackingField_5 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_Parent_m5FFEE1518EF3F12F1378AECEAEB41466CCA20DCE_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CParentU3Ek__BackingField_6 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CParentU3Ek__BackingField_6), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___zeroVector_5;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		float L_2 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___0_a;
		float L_4 = L_3.___y_3;
		float L_5 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_a;
		float L_7 = L_6.___z_4;
		float L_8 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_lhs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_rhs, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_lhs;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___1_rhs;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_lhs;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_rhs;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_lhs;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_rhs;
		float L_11 = L_10.___z_4;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11))));
		goto IL_002d;
	}

IL_002d:
	{
		float L_12 = V_0;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlModel_set_Children_m8C26332902AFB18940DAB4060807171A0364EA1E_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CChildrenU3Ek__BackingField_7 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CChildrenU3Ek__BackingField_7), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlRootModel_set_AllGeometryGroups_m6754461757172DBEB0986B7A916BBBFFDAD9E173_inline (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllGeometryGroupsU3Ek__BackingField_15 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllGeometryGroupsU3Ek__BackingField_15), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* StlModel_get_Children_mCC99BF668D976265E2897D884E1080A14B8219BB_inline (StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CChildrenU3Ek__BackingField_7;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StlRootModel_set_AllModels_mED5ABBDE53F3952C9E492CC70CADE0692BA9A7BF_inline (StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllModelsU3Ek__BackingField_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllModelsU3Ek__BackingField_14), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x_0 = L_0;
		float L_1 = ___1_y;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x_2 = L_0;
		float L_1 = ___1_y;
		__this->___y_3 = L_1;
		float L_2 = ___2_z;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t StlStreamReader_get_Line_mE94255EEFEFED4D8DED8AE024CE752F78FD174EC_inline (StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CLineU3Ek__BackingField_27;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline (int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___0_a;
		int32_t L_1 = ___1_b;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_2 = ___1_b;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		int32_t L_3 = ___0_a;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		float L_1;
		L_1 = Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline(L_0, NULL);
		V_0 = L_1;
		float L_2 = V_0;
		V_1 = (bool)((((float)L_2) > ((float)(9.99999975E-06f)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_value;
		float L_5 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline(L_4, L_5, NULL);
		V_2 = L_6;
		goto IL_0026;
	}

IL_001e:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		V_2 = L_7;
		goto IL_0026;
	}

IL_0026:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = V_2;
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_vector, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_vector;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___0_vector;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_vector;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_vector;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_vector;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___0_vector;
		float L_11 = L_10.___z_4;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_12;
		L_12 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11))))));
		V_0 = ((float)L_12);
		goto IL_0034;
	}

IL_0034:
	{
		float L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		float L_2 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___0_a;
		float L_4 = L_3.___y_3;
		float L_5 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_a;
		float L_7 = L_6.___z_4;
		float L_8 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)(L_1/L_2)), ((float)(L_4/L_5)), ((float)(L_7/L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
