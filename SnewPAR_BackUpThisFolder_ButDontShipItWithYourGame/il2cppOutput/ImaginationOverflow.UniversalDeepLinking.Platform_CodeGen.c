﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 ImaginationOverflow.UniversalDeepLinking.ILinkProvider ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory::GetProvider(System.Boolean)
extern void LinkProviderFactory_GetProvider_m9BC8331A4AE58F5C2B86B075B629E958C09BEA37 (void);
// 0x00000002 System.Void ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory::.ctor()
extern void LinkProviderFactory__ctor_m24F28D8A98298F29F9203C929D619259BC19DAEA (void);
// 0x00000003 System.Boolean ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::Initialize()
extern void AndroidLinkProvider_Initialize_mE00543E64903990CBC490DAF278CC00A8B96957D (void);
// 0x00000004 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::VerifyIfHasOpenByLink()
extern void AndroidLinkProvider_VerifyIfHasOpenByLink_m7700C95E926B7B58DFB9294E61CDA070AC193BF8 (void);
// 0x00000005 System.String ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::safeCallStringMethod(UnityEngine.AndroidJavaObject,System.String,System.Object[])
extern void AndroidLinkProvider_safeCallStringMethod_mB2FE3D6D2E8221EBEC890103D5BA220BE46F3EB8 (void);
// 0x00000006 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::add__linkReceived(System.Action`1<System.String>)
extern void AndroidLinkProvider_add__linkReceived_mCE2D5D378F879214CD5211269E73485E84C1E9BF (void);
// 0x00000007 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::remove__linkReceived(System.Action`1<System.String>)
extern void AndroidLinkProvider_remove__linkReceived_m84211CF759508734D12F3088244D33AAAB956B6D (void);
// 0x00000008 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::add_LinkReceived(System.Action`1<System.String>)
extern void AndroidLinkProvider_add_LinkReceived_mA6625270E4958C870B3BD51E4A96F31511B7BAA7 (void);
// 0x00000009 System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::remove_LinkReceived(System.Action`1<System.String>)
extern void AndroidLinkProvider_remove_LinkReceived_mEFFB43FEBC21ED256180514ED9CA61003CB20272 (void);
// 0x0000000A System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::PollInfoAfterPause()
extern void AndroidLinkProvider_PollInfoAfterPause_mF03166599D3B39446E74CA934A18A0DC7437FD37 (void);
// 0x0000000B System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::OnLinkReceived(System.String)
extern void AndroidLinkProvider_OnLinkReceived_mEDA7B3F1C8D407222CA0237A153A196A0CCBB78C (void);
// 0x0000000C System.Void ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::.ctor()
extern void AndroidLinkProvider__ctor_mEFE00E9939285B6789A849464364E6BA3702D04C (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	LinkProviderFactory_GetProvider_m9BC8331A4AE58F5C2B86B075B629E958C09BEA37,
	LinkProviderFactory__ctor_m24F28D8A98298F29F9203C929D619259BC19DAEA,
	AndroidLinkProvider_Initialize_mE00543E64903990CBC490DAF278CC00A8B96957D,
	AndroidLinkProvider_VerifyIfHasOpenByLink_m7700C95E926B7B58DFB9294E61CDA070AC193BF8,
	AndroidLinkProvider_safeCallStringMethod_mB2FE3D6D2E8221EBEC890103D5BA220BE46F3EB8,
	AndroidLinkProvider_add__linkReceived_mCE2D5D378F879214CD5211269E73485E84C1E9BF,
	AndroidLinkProvider_remove__linkReceived_m84211CF759508734D12F3088244D33AAAB956B6D,
	AndroidLinkProvider_add_LinkReceived_mA6625270E4958C870B3BD51E4A96F31511B7BAA7,
	AndroidLinkProvider_remove_LinkReceived_mEFFB43FEBC21ED256180514ED9CA61003CB20272,
	AndroidLinkProvider_PollInfoAfterPause_mF03166599D3B39446E74CA934A18A0DC7437FD37,
	AndroidLinkProvider_OnLinkReceived_mEDA7B3F1C8D407222CA0237A153A196A0CCBB78C,
	AndroidLinkProvider__ctor_mEFE00E9939285B6789A849464364E6BA3702D04C,
};
static const int32_t s_InvokerIndices[12] = 
{
	6723,
	9442,
	9161,
	9442,
	11811,
	7597,
	7597,
	7597,
	7597,
	9442,
	7597,
	9442,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_ImaginationOverflow_UniversalDeepLinking_Platform_CodeGenModule;
const Il2CppCodeGenModule g_ImaginationOverflow_UniversalDeepLinking_Platform_CodeGenModule = 
{
	"ImaginationOverflow.UniversalDeepLinking.Platform.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
