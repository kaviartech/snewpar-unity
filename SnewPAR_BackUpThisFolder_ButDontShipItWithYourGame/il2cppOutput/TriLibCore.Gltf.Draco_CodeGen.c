﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::ReleaseDracoMesh(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh**)
extern void DracoMeshLoader_ReleaseDracoMesh_m1D28A1C2FD9697F6519CCC8A46B814C4412EF720 (void);
// 0x00000002 System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::ReleaseDracoAttribute(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
extern void DracoMeshLoader_ReleaseDracoAttribute_m567DB1E14B44341C552B2591AA3507E8016745C2 (void);
// 0x00000003 System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::ReleaseDracoData(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData**)
extern void DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D (void);
// 0x00000004 System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader::DecodeDracoMesh(System.Byte[],System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh**)
extern void DracoMeshLoader_DecodeDracoMesh_m784E16569E6411367F94E2A3B4209C96F9B1FCD4 (void);
// 0x00000005 System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttribute(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
extern void DracoMeshLoader_GetAttribute_m3E63384A6101D48EA6CB7A14803655CB7555E13D (void);
// 0x00000006 System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttributeByType(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,TriLibCore.Gltf.Draco.DracoMeshLoader/AttributeType,System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
extern void DracoMeshLoader_GetAttributeByType_mAA4A9B33E2E5845D5EB425D5313DF7EAC29BEA1F (void);
// 0x00000007 System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttributeByUniqueId(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
extern void DracoMeshLoader_GetAttributeByUniqueId_m9127E962BFF3F8760126BBB7609CF6148AF74A93 (void);
// 0x00000008 System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetMeshIndices(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData**)
extern void DracoMeshLoader_GetMeshIndices_m9F3F739E294271FEDF72A776E237BF9A5E5A4B0D (void);
// 0x00000009 System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttributeData(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute*,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData**)
extern void DracoMeshLoader_GetAttributeData_mE86815972918FF808B0B27A5F03DEEF6ED977725 (void);
// 0x0000000A TriLibCore.Gltf.GltfTempGeometryGroup TriLibCore.Gltf.Draco.DracoMeshLoader::CreateUnityMesh(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*)
extern void DracoMeshLoader_CreateUnityMesh_m831B6AC86DF643827205BF92DFF1AA89590E6BFC (void);
// 0x0000000B System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader::DataTypeSize(TriLibCore.Gltf.Draco.DracoMeshLoader/DataType)
extern void DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42 (void);
// 0x0000000C System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader::ConvertDracoMeshToGltfGeometryGroup(System.Byte[],TriLibCore.Gltf.GltfTempGeometryGroup&)
extern void DracoMeshLoader_ConvertDracoMeshToGltfGeometryGroup_m3FB7E227DCB4224E60D5AB3472D706DDDDC1A543 (void);
// 0x0000000D TriLibCore.Gltf.GltfTempGeometryGroup TriLibCore.Gltf.Draco.DracoMeshLoader::DracoDecompressorCallback(System.Byte[])
extern void DracoMeshLoader_DracoDecompressorCallback_mF8DECDD739C6273FBC1CAE78688EDAC1C31F93DA (void);
// 0x0000000E System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::.ctor()
extern void DracoMeshLoader__ctor_mAC5A9167F1E64EE1E75675021B80DAC498B7CA58 (void);
static Il2CppMethodPointer s_methodPointers[14] = 
{
	DracoMeshLoader_ReleaseDracoMesh_m1D28A1C2FD9697F6519CCC8A46B814C4412EF720,
	DracoMeshLoader_ReleaseDracoAttribute_m567DB1E14B44341C552B2591AA3507E8016745C2,
	DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D,
	DracoMeshLoader_DecodeDracoMesh_m784E16569E6411367F94E2A3B4209C96F9B1FCD4,
	DracoMeshLoader_GetAttribute_m3E63384A6101D48EA6CB7A14803655CB7555E13D,
	DracoMeshLoader_GetAttributeByType_mAA4A9B33E2E5845D5EB425D5313DF7EAC29BEA1F,
	DracoMeshLoader_GetAttributeByUniqueId_m9127E962BFF3F8760126BBB7609CF6148AF74A93,
	DracoMeshLoader_GetMeshIndices_m9F3F739E294271FEDF72A776E237BF9A5E5A4B0D,
	DracoMeshLoader_GetAttributeData_mE86815972918FF808B0B27A5F03DEEF6ED977725,
	DracoMeshLoader_CreateUnityMesh_m831B6AC86DF643827205BF92DFF1AA89590E6BFC,
	DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42,
	DracoMeshLoader_ConvertDracoMeshToGltfGeometryGroup_m3FB7E227DCB4224E60D5AB3472D706DDDDC1A543,
	DracoMeshLoader_DracoDecompressorCallback_mF8DECDD739C6273FBC1CAE78688EDAC1C31F93DA,
	DracoMeshLoader__ctor_mAC5A9167F1E64EE1E75675021B80DAC498B7CA58,
};
static const int32_t s_InvokerIndices[14] = 
{
	14235,
	14235,
	14235,
	11667,
	11490,
	10719,
	11490,
	12357,
	11481,
	6718,
	6319,
	3160,
	13922,
	9442,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_TriLibCore_Gltf_Draco_CodeGenModule;
const Il2CppCodeGenModule g_TriLibCore_Gltf_Draco_CodeGenModule = 
{
	"TriLibCore.Gltf.Draco.dll",
	14,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
