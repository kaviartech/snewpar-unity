﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtualFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtualFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtualFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};

// System.Action`1<TriLibCore.AssetLoaderContext>
struct Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226;
// System.Action`1<TriLibCore.IContextualizedError>
struct Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30;
// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
struct Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD;
// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64>
struct Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2;
// TriLibCore.Pooling.ArrayPoolBase`1<System.Byte>
struct ArrayPoolBase_1_tF9FBF9B378E1EB8245DFE9DE66684B1AFB0EBFE8;
// TriLibCore.Pooling.ArrayPool`1<System.Byte>
struct ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086;
// System.Collections.Generic.Comparer`1<System.Byte>
struct Comparer_1_t49F23FD0F51B7B3F17D30558E0A425107523CC30;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.General.CompoundMaterialKey,TriLibCore.TextureLoadingContext>
struct ConcurrentDictionary_2_t94764B51655C4F04FDAE59E1A6327AFEE05EA292;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,System.Collections.Generic.List`1<TriLibCore.MaterialRendererContext>>
struct ConcurrentDictionary_2_tA783589C825EB0CEA850D32094AAEFFBB3FD5D82;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material>
struct ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.ITexture,TriLibCore.TextureLoadingContext>
struct ConcurrentDictionary_2_tBB5915FD91B3F65141A8C3EC64A14F14EEA3905B;
// TriLibCore.General.ConcurrentDictionary`2<System.String,System.String>
struct ConcurrentDictionary_2_tCF44E0035FB42A2A1DF508A4CE0B233163C23F1E;
// System.Collections.Generic.Dictionary`2<TriLibCore.Fbx.FBXAnimationLayer,System.Collections.Generic.SortedSet`1<System.Int64>>
struct Dictionary_2_tB49177EDEA5ECFF24C13CCA52CD1A58D58131EB5;
// System.Collections.Generic.Dictionary`2<TriLibCore.Fbx.FBXAnimationStack,System.Collections.Generic.SortedSet`1<TriLibCore.Fbx.FBXAnimationLayer>>
struct Dictionary_2_tB3587CF77361D420D1DC7210EA1A4B3B312BB628;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,TriLibCore.Interfaces.IModel>
struct Dictionary_2_tE704ACFE7C32537A046D8577F8299D1B52ED0C00;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String>
struct Dictionary_2_t15A9DEF843D5DA84170CD8536BA0EBB039EB4ADF;
// System.Collections.Generic.Dictionary`2<TriLibCore.Interfaces.IModel,UnityEngine.GameObject>
struct Dictionary_2_tADE1FC3F6C786CACD6652C2C7275C3A0FD274A9C;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]>
struct Dictionary_2_t23C2BC333CAB1901F8EC82B59264ED8D028DD1AB;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding>
struct Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54;
// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Fbx.IFBXObject>
struct Dictionary_2_tE8D0E51AE256AFF2CE801667465D9F47A030908B;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710;
// System.Collections.Generic.HashSet`1<UnityEngine.Texture>
struct HashSet_1_t70836788BCAF42568800A162B9F23937F5309AE8;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t9CC3C47C67E4184F7F1B8B0AFAEF692B9EDDDF05;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t606011D801075A525E02DDE2533126EA96831FCF;
// System.Collections.Generic.IEnumerator`1<System.Double>
struct IEnumerator_1_tC485938B2065194F37223DC3FA35818439FD0233;
// System.Collections.Generic.IEnumerator`1<System.Int16>
struct IEnumerator_1_tE63A1850605587CAFA3FEB2AE63BEDADF9E336A6;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_tD6A90A7446DA8E6CF865EDFBBF18C1200BB6D452;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t96C69FC6D3BFF990C18CFAF5A304D4D336E5DD32;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t736E9F8BD2FD38A5E9EA2E8A510AFED788D05010;
// System.Collections.Generic.IList`1<System.Byte>
struct IList_1_t958E1E27F44A1E57CF73D43A68F1EF1A1E07FE0C;
// System.Collections.Generic.IList`1<System.Char>
struct IList_1_tF23041AC58956CDAA98A1DA3D23002DBE4EBE278;
// System.Collections.Generic.IList`1<System.Double>
struct IList_1_t37F866E68615F10FC2515F9CD18666C31188DE6F;
// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXAnimationCurve>
struct IList_1_t4F9822737279E2BDE1357E44A994D6426F5ABCD2;
// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXAnimationCurveNode>
struct IList_1_t1DD4697533A64016232970F03E1172ABE2F6D9AD;
// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXAnimationLayer>
struct IList_1_t2619CD52AE93483CDD60F2C725373481AF34C3CA;
// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXBlendShapeGeometryGroup>
struct IList_1_t8EC0E267712FBA478C82643C1C886197D33BF3D3;
// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXDeformer>
struct IList_1_t46DD05C1E1ACC8DDEB74BD29CFFDC72BB2600396;
// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXModel>
struct IList_1_t42C8F0899EF9E073009A4304B50DFA100C654666;
// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXSubDeformer>
struct IList_1_tC84475B43EB88641B712D0B00BC4DBFEBBEA12A9;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation>
struct IList_1_t7A16CD7EF0938B36E4D20182185F284ECA5F93A2;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera>
struct IList_1_t13EA3E1B6894AF8023B793D65EA2E1ED596B6E82;
// System.Collections.Generic.IList`1<TriLibCore.Fbx.IFBXMesh>
struct IList_1_t2926C707BF885DE3E8D8941440E5950FD45B7D97;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>
struct IList_1_t54EA2EAA8FF287B3E144BC90047C3E635336CB4C;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight>
struct IList_1_t95B0FF72887258CDC012A1B81E66B66AF3BBE38E;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>
struct IList_1_t0662D113B996C51F1676FFC848F7B3448D818DB7;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>
struct IList_1_tBAC2F9CBFB365F17F69446225AF2802DEF7B2956;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>
struct IList_1_t2988C79E2C0A953B91ACE72118B299F94ECFEB62;
// System.Collections.Generic.IList`1<System.Int16>
struct IList_1_t33D4A6F17EB7EE6F64F4B2D34463698DAD680B55;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_tFB8BE2ED9A601C1259EAB8D73D1B3E96EA321FA1;
// System.Collections.Generic.IList`1<System.Int64>
struct IList_1_tD5BEB2F8A25EFA71E6448F7109F425405B633B1F;
// System.Collections.Generic.IList`1<UnityEngine.Matrix4x4>
struct IList_1_t18ED0F81A86D2B52A6C90C7CBA5971934D55A6F0;
// System.Collections.Generic.IList`1<System.Single>
struct IList_1_t8E7546B74FA2018BF4FA7562464581B7A2E149EA;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7;
// System.Collections.Generic.List`1<TriLibCore.Fbx.FBXNode>
struct List_1_tFF42A872CA59D1F4CA2CE1FE8274BEF1B1C07A99;
// System.Collections.Generic.List`1<TriLibCore.Fbx.FBXProperty>
struct List_1_t0DA797EB977FE200B9C3A8ADDDEF676135603112;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3;
// System.Collections.Generic.List`1<System.Threading.Tasks.Task>
struct List_1_t84C257E858DDB8EA0B6269E08AAD9A2A2018A551;
// System.Collections.Generic.Queue`1<TriLibCore.Interfaces.IContextualizedAction>
struct Queue_1_t952DE88AF42216B755D09647735E4235DA7138D4;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t4C228DE57804012969575431CFF12D57C875552D;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Double[]
struct DoubleU5BU5D_tCC308475BD3B8229DB2582938669EF2F9ECC1FEE;
// System.Int16[]
struct Int16U5BU5D_t8175CE8DD9C9F9FB0CF4F58E45BC570575B43CFB;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.Int64[]
struct Int64U5BU5D_tAEDFCBDB5414E2A140A6F34C0538BF97FCF67A1D;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// ICSharpCode.SharpZipLib.Checksum.Adler32
struct Adler32_t719B695D8A1A84E2A465697391C35131CA4FB1CD;
// TriLibCore.AssetLoaderContext
struct AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C;
// TriLibCore.AssetLoaderOptions
struct AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6;
// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2;
// System.Text.Decoder
struct Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC;
// System.Text.DecoderFallback
struct DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.Text.EncoderFallback
struct EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293;
// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095;
// System.Exception
struct Exception_t;
// TriLibCore.Fbx.ASCII.FBXASCIIReader
struct FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B;
// TriLibCore.Fbx.ASCII.FBXASCIITokenizer
struct FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E;
// TriLibCore.Fbx.FBXAnimationCurve
struct FBXAnimationCurve_t231EE593FF73ECE0908ED97A303F7EAFC5D4D45C;
// TriLibCore.Fbx.FBXAnimationCurveNode
struct FBXAnimationCurveNode_t576192536BD712D1A036BD4CA26E65E2AF91D5BB;
// TriLibCore.Fbx.FBXAnimationLayer
struct FBXAnimationLayer_t691446B83D4B681F5922098EBD0E0B2F30A1CB50;
// TriLibCore.Fbx.FBXAnimationStack
struct FBXAnimationStack_t1951A8FC41247D031C5FB2058F47DFFB6FEB4826;
// TriLibCore.Fbx.Binary.FBXBinaryReader
struct FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50;
// TriLibCore.Fbx.FBXBlendShapeGeometryGroup
struct FBXBlendShapeGeometryGroup_t7F48F7377B8F0CEBAFADB748FCECFDA63D99949C;
// TriLibCore.Fbx.FBXBlendShapeSubDeformer
struct FBXBlendShapeSubDeformer_t8ED024CF120BB5964980D664E3E4A8814DC873A9;
// TriLibCore.Fbx.FBXDeformer
struct FBXDeformer_t6205A00C20A32DA310DE01502EDE739CD8AD9BC8;
// TriLibCore.Fbx.FBXDocument
struct FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834;
// TriLibCore.Fbx.FBXGlobalSettings
struct FBXGlobalSettings_t0C07F154075AAF25F09B10FF36073B5A01B8BC28;
// TriLibCore.Fbx.FBXMaterial
struct FBXMaterial_tF187223B7488940E1773EC2749D36BDB16F9C944;
// TriLibCore.Fbx.FBXMatrices
struct FBXMatrices_tDBC0EE205D2B2D729DBA25A18CF3A67388C5030D;
// TriLibCore.Fbx.FBXMatrixBuffer
struct FBXMatrixBuffer_tF111AF67C063DB9EFE066B3E92AB4391B05BAF1F;
// TriLibCore.Fbx.FBXModel
struct FBXModel_t5D448CD22472ADEA4C42B7A9F299C393769E08A4;
// TriLibCore.Fbx.FBXNodeAttribute
struct FBXNodeAttribute_tD64A5CE0BD58841F114311413B61A7AE29C6C593;
// TriLibCore.Fbx.FBXPose
struct FBXPose_t58C887C0A441F315857FDA99BBB3FF3A2E3158C3;
// TriLibCore.Fbx.FBXProcessor
struct FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4;
// TriLibCore.Fbx.FBXProperties
struct FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418;
// TriLibCore.Fbx.FBXRootModel
struct FBXRootModel_t1960D19A6733168B2F9CF5A6235362FD25861775;
// TriLibCore.Fbx.FBXSubDeformer
struct FBXSubDeformer_tD0AE3226DCA22276BBDE337EEBAD3789F1143FDC;
// TriLibCore.Fbx.FBXTexture
struct FBXTexture_t144C224F07D8C26F18E312F7A530F5C15AF1D79D;
// TriLibCore.Fbx.FBXVideo
struct FBXVideo_t5E3B9EE50CF8C48C1694A512180A2CB465E89CD2;
// TriLibCore.Fbx.Reader.FbxReader
struct FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// TriLibCore.Fbx.IFBXMesh
struct IFBXMesh_t41F9E3476B1263908A318584EC48D6D408E66756;
// TriLibCore.Interfaces.IGeometryGroup
struct IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262;
// TriLibCore.Interfaces.IModel
struct IModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB;
// TriLibCore.Interfaces.IRootModel
struct IRootModel_t83ED40397FD23448FC9A99336523CC7DE8A841BB;
// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct InflaterDynHeader_tB69F561C81D8F6D536510477A1FC9EFD39DE4EFF;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_tB7309822439A9E51BA1EF5544BF706DB30F7C71F;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t947724C1CD4D44B75584D59D1ECA71C121D62129;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream
struct InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.IO.MemoryStream
struct MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.NotImplementedException
struct NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_tAA96562C1DB1269206CBF4CDFC8ACE36C26A99E8;
// TriLibCore.Fbx.PropertyAccessorByte
struct PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899;
// TriLibCore.Fbx.PropertyAccessorDouble
struct PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5;
// TriLibCore.Fbx.PropertyAccessorFloat
struct PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B;
// TriLibCore.Fbx.PropertyAccessorIEE754
struct PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C;
// TriLibCore.Fbx.PropertyAccessorInt
struct PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3;
// TriLibCore.Fbx.PropertyAccessorLong
struct PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7;
// TriLibCore.Fbx.PropertyAccessorShort
struct PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8;
// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2;
// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t626C6631E1D9BD1EAA5FDB4987C79A2F34A92689;
// System.String
struct String_t;
// System.Threading.Tasks.Task
struct Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572;
// System.Type
struct Type_t;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05;

IL2CPP_EXTERN_C RuntimeClass* ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PropertyAccessorIEnumerator_1_t507F53FFDEAE6189449897018F3198188410DEB2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PropertyAccessorIEnumerator_1_t81284489B090B3086F8560CFF14FCFA37CD50F96_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PropertyAccessorIEnumerator_1_t99D6ABB7ECAA7EDD17341582A28BF20268F5C71D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PropertyAccessorIEnumerator_1_tA87B54345A2924A4BB0DE1D0D4BE147D24B9056D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PropertyAccessorIEnumerator_1_tC80F58CC2A95871A7E8891567EEE4DAAC636E0A6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0C18F04EBBB957042CCC2056B36799D79564A2DB;
IL2CPP_EXTERN_C String_t* _stringLiteral4A6331918BBD32293C65EC7C39BE183F558B0111;
IL2CPP_EXTERN_C String_t* _stringLiteral59A855EBCC658B736BF700F291E16BCDE82179CC;
IL2CPP_EXTERN_C String_t* _stringLiteral91DD86FA4DF0E44AA795B032277AB18EF9404D3E;
IL2CPP_EXTERN_C String_t* _stringLiteral931D7B3EADF2ABC99B546F64E928640DD28E17B2;
IL2CPP_EXTERN_C String_t* _stringLiteralAF9B10C2E201D9B3CE0452AB6969B5912DBD0D42;
IL2CPP_EXTERN_C String_t* _stringLiteralB94DD1963BC6E8F27D446B420BEA01FE03FD8C2B;
IL2CPP_EXTERN_C String_t* _stringLiteralC1E07B7875C9A8699F7BE475BDAF491BCBC03B04;
IL2CPP_EXTERN_C String_t* _stringLiteralD5C5B51FDBB849EDCED35D213FC930CC6B328429;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralEE17426A533F8CAB2BECCE4D29123F6B09106045;
IL2CPP_EXTERN_C const RuntimeMethod* ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FBXASCIIReader_ReadNode_mD0B13F651A7A4B2C320F97780487910AC89131A5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FBXASCIITokenizer_ReadToken_m308AD080344BD120BDE757662F380A80C8867DC3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FBXBinaryReader_ReadDocument_m27AFF3DF4C3E9C70713109A0A2FB0FA293674E64_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FBXBinaryReader_ReadNode_m7B76CA3D4E6C170ADDBC95526EEB5A823900C92E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FBXBinaryReader_ReadStringEx_m9FC9DD7CD2CBFCD8D4F15FC652C5B99423BF8390_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FBXBinaryReader_SkipStringEx_mCC66E0E7BDF4C7F65EF9BB62CE92B8CBFA874148_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mF3227B0AAB9F7FED1883246395F1CEA0D0B06DC5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m0586B319F89682059DD157C1EDC282A2888ECB9B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mC679EFF5E634878F1897D71DC5160A96EA719E82_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorByte_Add_mAFC49C1DBE7203D3A9DBFAB3ABA5BA0BE13904DE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorByte_Clear_m648D5D7C9EC8B28DA5C419E96E28A69BD6E1998C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorByte_Contains_m6D9A922B2C0C224FD1A8324B3D7328E2A189D6B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorByte_IndexOf_mDEB7FFF72A7C72084150E7E51A98A3B46FA06D10_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorByte_Insert_m6104AD8296D35F593835A492E2707DEDCA0189A0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorByte_RemoveAt_mD003C4326C542792EE62A67D518B44C08EA5DE16_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorByte_Remove_m7774F101D57F6543F3057BD4BAF6C5A2D7CE6241_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorByte_set_Item_mA767343463C886938EE75EAFB259B969AF6FE184_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorDouble_Add_mAE1735D14F3AA39C070B7CFF7D2D376453851ACE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorDouble_Clear_mEF8B26DAEC1AA884F3174D820B0BB4933AD5F4D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorDouble_Contains_m35D360656DAFE79EA025141CA19D86EF834E7EE3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorDouble_IndexOf_m7DA059EE56C40EB0703D3859F3053D074C923311_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorDouble_Insert_mEB967E3D782C721D44C788453CD124976B0A11D8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorDouble_RemoveAt_m7932C49E6F6D9D1D96E5F7655639650EEDDEB64A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorDouble_Remove_mDAD3E51DCDFAF51CC1045AE05EDED18355A8B6BF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorDouble_set_Item_m8CA12BC5B805E259A5A91BB1A7A98EC44FF517DA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorFloat_Add_mCCAD275FD92B9DD4E171845E9B1E4B3E8D9255ED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorFloat_Clear_m3D2A56B0A4BBCC34D46ADD148EEAB561A5C9FBD6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorFloat_Contains_mA19DBB936B8A684E19E995DFEC0629E6BFAA9C8C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorFloat_IndexOf_m08F29119B0358F6727AEB21AC99F6A06D91FAAA8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorFloat_Insert_m60FADFA183D69CE64EE67C9C2DA05C1AE28215F4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorFloat_RemoveAt_mCD6754C94D601525E93F0BD570898E2AFB886C9F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorFloat_Remove_mF265557EEFE8866385B6280859AF5C87402A1022_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorFloat_set_Item_m030F64C16AAC2FDD5A546AC3FC8307C53C06B79D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEE754_Add_mBD9C6FB19DD5EE7A9054A642FE6C17AF7D11226D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEE754_Clear_mA0A2150C21DFCC7E74C4A68D00CE206A20F48433_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEE754_Contains_m64AC75313103A83FA655C79D6D1643789CC85426_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEE754_IndexOf_m59FB427A9B4BC4FF9B3DF2AA2B14BF5BFF16E552_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEE754_Insert_m46AA71328F9D5F4E36474318EC72B32FAC57130C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEE754_RemoveAt_mDC0DC08B153E2A2F2954ECB13281376EA30E2D91_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEE754_Remove_mF9938772EEFE6996F36F4AD6E2CB22DDD0357F76_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEE754_set_Item_m14570F3B3A020CD7D15485930032DC6C9F55D687_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEnumerator_1__ctor_m10ADEEA2B95198A97E5327E47BFAB5720EA81883_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEnumerator_1__ctor_m13F6A2E033696DC706279AE3A91D02A64206AC4D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEnumerator_1__ctor_m52017FEBE78CFF4A3918E710C90AFBB0AE6B434F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEnumerator_1__ctor_m698626BC437191FDC212DB020833A50A9A35807A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEnumerator_1__ctor_m807FB04952574C23B9ECCFF282D11027CB4022CD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorInt_Add_m1C14F9F8818E6843CC0DD32437D07C3394B6F4E7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorInt_Clear_m696554C189B6C8E1769E5967C00C121F2D127D11_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorInt_Contains_m0212669CC333E58F25DC8371F45DEB094EE8170E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorInt_IndexOf_m4BD1C6F598845DC91FBB46DA5E391E2272BF946D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorInt_Insert_mBD22A1188780CA0DA1B4F63F6113CB9091E6E14C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorInt_RemoveAt_m5C75A6097020E302ADB4B78EB3321412B7EAAE00_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorInt_Remove_mD200B41DF3E14BDBF1AA25CB049E348300AA2DE5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorInt_set_Item_mE396544801031052CB78036FA969BBFB4DC019DD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorLong_Add_m5E36E9519465EFB1B6C70C0D6BFF6B18C365714D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorLong_Clear_m5CDEF0C6CE3BFDB9815CB8CC76D19BFE5E7D531E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorLong_Contains_mA2EF2BA51E321E94EC45365F3C3FAB3EE6E9C378_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorLong_IndexOf_m049151611E62CCC45723DE7E25636EDDA7397F19_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorLong_Insert_m6E624085B34F1BE033200EEF0134ACD783519358_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorLong_RemoveAt_mE5EC5A508E67591F480F9FFE2AB04D55B1431F78_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorLong_Remove_m2023A6FD815BDF767964553127FCBC4CF8DA1851_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorLong_set_Item_m4A792FFF42650916E4AB115B9EC9830A0F8ABE11_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorShort_Add_mB213E015451E9AA91BFACD19CD43522DDFAB8DF5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorShort_Clear_m7879EF8C9C6A06B16EAF6C22BD0518C5C21079C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorShort_Contains_mD53684A5299F2257D5D6F7C1E333FDCD8CAFBF1A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorShort_IndexOf_mDC14357ADFD90AC26E6006E2E64BD1AF8C11C6AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorShort_Insert_m7EC96C6A89F2DEB37371B8FFE955F742C6998326_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorShort_RemoveAt_m55AA2E6A236358D101272E6CBC782A334533C8AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorShort_Remove_m0FE173DE48A774FC2C5717100CA47A72CC0E968C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PropertyAccessorShort_set_Item_mA9E40465930AB75103C53BA6F29209913482988C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* String_Concat_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_m691C56B3F5A4F992794166066A4B0D10B8B74461_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* ProcessingSteps_t650107FDF5C3F52BB4A5B9D62AA93F417D0E5316_0_0_0_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct DoubleU5BU5D_tCC308475BD3B8229DB2582938669EF2F9ECC1FEE;
struct Int16U5BU5D_t8175CE8DD9C9F9FB0CF4F58E45BC570575B43CFB;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct Int64U5BU5D_tAEDFCBDB5414E2A140A6F34C0538BF97FCF67A1D;
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriLibCore.Pooling.ArrayPool`1<System.Byte>
struct ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086  : public RuntimeObject
{
	// T[] TriLibCore.Pooling.ArrayPool`1::<Array>k__BackingField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___U3CArrayU3Ek__BackingField_0;
	// System.Collections.Generic.Comparer`1<T> TriLibCore.Pooling.ArrayPool`1::_comparer
	Comparer_1_t49F23FD0F51B7B3F17D30558E0A425107523CC30* ____comparer_1;
	// TriLibCore.Pooling.ArrayPoolBase`1<T> TriLibCore.Pooling.ArrayPool`1::_pool
	ArrayPoolBase_1_tF9FBF9B378E1EB8245DFE9DE66684B1AFB0EBFE8* ____pool_2;
	// System.Int32 TriLibCore.Pooling.ArrayPool`1::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_3;
};

// System.Collections.Generic.List`1<System.Char>
struct List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t88E38A44A76B866A9AA5C7EC673E6DE694C7E5BD  : public RuntimeObject
{
};

// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158  : public RuntimeObject
{
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC* ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;
};

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095  : public RuntimeObject
{
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t52460FA30AE37F4F26ACB81055E58002262F19F2* ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tD2C40CE114AA9D8E1F7196608B2D088548015293* ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t7324102215E4ED41EC065C02EB501CB0BC23CD90* ___decoderFallback_14;
};

// TriLibCore.Fbx.FBXObject
struct FBXObject_t955A55BC5B93846C9089D1A44A2DC3F65FC05671  : public RuntimeObject
{
	// System.String TriLibCore.Fbx.FBXObject::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Fbx.FBXObject::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// TriLibCore.Fbx.FBXDocument TriLibCore.Fbx.FBXObject::<Document>k__BackingField
	FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* ___U3CDocumentU3Ek__BackingField_2;
	// System.Int64 TriLibCore.Fbx.FBXObject::<Id>k__BackingField
	int64_t ___U3CIdU3Ek__BackingField_3;
	// TriLibCore.Fbx.FBXObjectType TriLibCore.Fbx.FBXObject::<ObjectType>k__BackingField
	int32_t ___U3CObjectTypeU3Ek__BackingField_4;
	// System.String TriLibCore.Fbx.FBXObject::<Class>k__BackingField
	String_t* ___U3CClassU3Ek__BackingField_5;
	// TriLibCore.Fbx.FBXProperties TriLibCore.Fbx.FBXObject::<Properties>k__BackingField
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___U3CPropertiesU3Ek__BackingField_6;
};

// TriLibCore.Fbx.FBXProcessor
struct FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4  : public RuntimeObject
{
	// TriLibCore.Fbx.FBXDocument TriLibCore.Fbx.FBXProcessor::Document
	FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* ___Document_13;
	// System.String[] TriLibCore.Fbx.FBXProcessor::_values
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____values_14;
	// TriLibCore.Fbx.Reader.FbxReader TriLibCore.Fbx.FBXProcessor::Reader
	FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* ___Reader_15;
	// System.Int32 TriLibCore.Fbx.FBXProcessor::ActiveBasePropertyIndex
	int32_t ___ActiveBasePropertyIndex_16;
	// System.Int32 TriLibCore.Fbx.FBXProcessor::ActiveDataSize
	int32_t ___ActiveDataSize_17;
	// System.Int32 TriLibCore.Fbx.FBXProcessor::ActiveSubDataSize
	int32_t ___ActiveSubDataSize_18;
	// System.IO.BinaryReader TriLibCore.Fbx.FBXProcessor::ActiveBinaryReader
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___ActiveBinaryReader_19;
	// TriLibCore.Fbx.ASCII.FBXASCIIReader TriLibCore.Fbx.FBXProcessor::ActiveASCIIReader
	FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* ___ActiveASCIIReader_20;
	// TriLibCore.Fbx.Binary.FBXBinaryReader TriLibCore.Fbx.FBXProcessor::FBXBinaryReader
	FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* ___FBXBinaryReader_21;
	// System.Char TriLibCore.Fbx.FBXProcessor::ActivePropertyType
	Il2CppChar ___ActivePropertyType_22;
	// System.Boolean TriLibCore.Fbx.FBXProcessor::ActivePropertyEncoded
	bool ___ActivePropertyEncoded_23;
	// System.Collections.Generic.List`1<TriLibCore.Fbx.FBXProperty> TriLibCore.Fbx.FBXProcessor::Properties
	List_1_t0DA797EB977FE200B9C3A8ADDDEF676135603112* ___Properties_24;
	// System.Collections.Generic.List`1<TriLibCore.Fbx.FBXNode> TriLibCore.Fbx.FBXProcessor::Nodes
	List_1_tFF42A872CA59D1F4CA2CE1FE8274BEF1B1C07A99* ___Nodes_25;
	// System.Int32[] TriLibCore.Fbx.FBXProcessor::_vertexIndicesBuffer
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____vertexIndicesBuffer_128;
};

// TriLibCore.Fbx.FBXProperties
struct FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418  : public RuntimeObject
{
	// System.Int32 TriLibCore.Fbx.FBXProperties::<PropertiesCount>k__BackingField
	int32_t ___U3CPropertiesCountU3Ek__BackingField_0;
	// TriLibCore.Fbx.FBXProcessor TriLibCore.Fbx.FBXProperties::<Processor>k__BackingField
	FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ___U3CProcessorU3Ek__BackingField_1;
	// System.Int32 TriLibCore.Fbx.FBXProperties::BasePropertyIndex
	int32_t ___BasePropertyIndex_2;
	// System.Int32 TriLibCore.Fbx.FBXProperties::ArrayLength
	int32_t ___ArrayLength_3;
};

// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494  : public RuntimeObject
{
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::mode
	int32_t ___mode_17;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::readAdler
	int32_t ___readAdler_18;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::neededBits
	int32_t ___neededBits_19;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::repLength
	int32_t ___repLength_20;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::repDist
	int32_t ___repDist_21;
	// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::uncomprLen
	int32_t ___uncomprLen_22;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::isLastBlock
	bool ___isLastBlock_23;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::totalOut
	int64_t ___totalOut_24;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::totalIn
	int64_t ___totalIn_25;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::noHeader
	bool ___noHeader_26;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator ICSharpCode.SharpZipLib.Zip.Compression.Inflater::input
	StreamManipulator_t626C6631E1D9BD1EAA5FDB4987C79A2F34A92689* ___input_27;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow ICSharpCode.SharpZipLib.Zip.Compression.Inflater::outputWindow
	OutputWindow_tAA96562C1DB1269206CBF4CDFC8ACE36C26A99E8* ___outputWindow_28;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader ICSharpCode.SharpZipLib.Zip.Compression.Inflater::dynHeader
	InflaterDynHeader_tB69F561C81D8F6D536510477A1FC9EFD39DE4EFF* ___dynHeader_29;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.Inflater::litlenTree
	InflaterHuffmanTree_tB7309822439A9E51BA1EF5544BF706DB30F7C71F* ___litlenTree_30;
	// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.Inflater::distTree
	InflaterHuffmanTree_tB7309822439A9E51BA1EF5544BF706DB30F7C71F* ___distTree_31;
	// ICSharpCode.SharpZipLib.Checksum.Adler32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::adler
	Adler32_t719B695D8A1A84E2A465697391C35131CA4FB1CD* ___adler_32;
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// TriLibCore.Fbx.PropertyAccessorByte
struct PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899  : public RuntimeObject
{
	// TriLibCore.Fbx.FBXProperties TriLibCore.Fbx.PropertyAccessorByte::_properties
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ____properties_0;
	// TriLibCore.Pooling.ArrayPool`1<System.Byte> TriLibCore.Fbx.PropertyAccessorByte::_decoded
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* ____decoded_1;
	// System.IO.MemoryStream TriLibCore.Fbx.PropertyAccessorByte::_decodedMemoryStream
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* ____decodedMemoryStream_2;
	// System.IO.BinaryReader TriLibCore.Fbx.PropertyAccessorByte::_decodedBinaryReader
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____decodedBinaryReader_3;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorByte::_count
	int32_t ____count_4;
	// System.Boolean TriLibCore.Fbx.PropertyAccessorByte::<IsReadOnly>k__BackingField
	bool ___U3CIsReadOnlyU3Ek__BackingField_5;
};

// TriLibCore.Fbx.PropertyAccessorDouble
struct PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5  : public RuntimeObject
{
	// TriLibCore.Fbx.FBXProperties TriLibCore.Fbx.PropertyAccessorDouble::_properties
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ____properties_0;
	// TriLibCore.Pooling.ArrayPool`1<System.Byte> TriLibCore.Fbx.PropertyAccessorDouble::_decoded
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* ____decoded_1;
	// System.IO.MemoryStream TriLibCore.Fbx.PropertyAccessorDouble::_decodedMemoryStream
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* ____decodedMemoryStream_2;
	// System.IO.BinaryReader TriLibCore.Fbx.PropertyAccessorDouble::_decodedBinaryReader
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____decodedBinaryReader_3;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorDouble::_count
	int32_t ____count_4;
	// System.Boolean TriLibCore.Fbx.PropertyAccessorDouble::<IsReadOnly>k__BackingField
	bool ___U3CIsReadOnlyU3Ek__BackingField_5;
};

// TriLibCore.Fbx.PropertyAccessorFloat
struct PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B  : public RuntimeObject
{
	// TriLibCore.Fbx.FBXProperties TriLibCore.Fbx.PropertyAccessorFloat::_properties
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ____properties_0;
	// TriLibCore.Pooling.ArrayPool`1<System.Byte> TriLibCore.Fbx.PropertyAccessorFloat::_decoded
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* ____decoded_1;
	// System.IO.MemoryStream TriLibCore.Fbx.PropertyAccessorFloat::_decodedMemoryStream
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* ____decodedMemoryStream_2;
	// System.IO.BinaryReader TriLibCore.Fbx.PropertyAccessorFloat::_decodedBinaryReader
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____decodedBinaryReader_3;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorFloat::_count
	int32_t ____count_4;
	// System.Boolean TriLibCore.Fbx.PropertyAccessorFloat::<IsReadOnly>k__BackingField
	bool ___U3CIsReadOnlyU3Ek__BackingField_5;
};

// TriLibCore.Fbx.PropertyAccessorIEE754
struct PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C  : public RuntimeObject
{
	// TriLibCore.Fbx.FBXProperties TriLibCore.Fbx.PropertyAccessorIEE754::_properties
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ____properties_0;
	// TriLibCore.Pooling.ArrayPool`1<System.Byte> TriLibCore.Fbx.PropertyAccessorIEE754::_decoded
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* ____decoded_1;
	// System.IO.MemoryStream TriLibCore.Fbx.PropertyAccessorIEE754::_decodedMemoryStream
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* ____decodedMemoryStream_2;
	// System.IO.BinaryReader TriLibCore.Fbx.PropertyAccessorIEE754::_decodedBinaryReader
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____decodedBinaryReader_3;
	// System.Byte[] TriLibCore.Fbx.PropertyAccessorIEE754::_bytesInt
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____bytesInt_4;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorIEE754::_count
	int32_t ____count_5;
	// System.Boolean TriLibCore.Fbx.PropertyAccessorIEE754::<IsReadOnly>k__BackingField
	bool ___U3CIsReadOnlyU3Ek__BackingField_6;
};

// TriLibCore.Fbx.PropertyAccessorInt
struct PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3  : public RuntimeObject
{
	// TriLibCore.Fbx.FBXProperties TriLibCore.Fbx.PropertyAccessorInt::_properties
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ____properties_0;
	// TriLibCore.Pooling.ArrayPool`1<System.Byte> TriLibCore.Fbx.PropertyAccessorInt::_decoded
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* ____decoded_1;
	// System.IO.MemoryStream TriLibCore.Fbx.PropertyAccessorInt::_decodedMemoryStream
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* ____decodedMemoryStream_2;
	// System.IO.BinaryReader TriLibCore.Fbx.PropertyAccessorInt::_decodedBinaryReader
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____decodedBinaryReader_3;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorInt::_count
	int32_t ____count_4;
	// System.Boolean TriLibCore.Fbx.PropertyAccessorInt::<IsReadOnly>k__BackingField
	bool ___U3CIsReadOnlyU3Ek__BackingField_5;
};

// TriLibCore.Fbx.PropertyAccessorLong
struct PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7  : public RuntimeObject
{
	// TriLibCore.Fbx.FBXProperties TriLibCore.Fbx.PropertyAccessorLong::_properties
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ____properties_0;
	// TriLibCore.Pooling.ArrayPool`1<System.Byte> TriLibCore.Fbx.PropertyAccessorLong::_decoded
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* ____decoded_1;
	// System.IO.MemoryStream TriLibCore.Fbx.PropertyAccessorLong::_decodedMemoryStream
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* ____decodedMemoryStream_2;
	// System.IO.BinaryReader TriLibCore.Fbx.PropertyAccessorLong::_decodedBinaryReader
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____decodedBinaryReader_3;
	// System.Boolean TriLibCore.Fbx.PropertyAccessorLong::_isTime
	bool ____isTime_4;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorLong::_count
	int32_t ____count_5;
	// System.Boolean TriLibCore.Fbx.PropertyAccessorLong::<IsReadOnly>k__BackingField
	bool ___U3CIsReadOnlyU3Ek__BackingField_6;
};

// TriLibCore.Fbx.PropertyAccessorShort
struct PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8  : public RuntimeObject
{
	// TriLibCore.Fbx.FBXProperties TriLibCore.Fbx.PropertyAccessorShort::_properties
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ____properties_0;
	// TriLibCore.Pooling.ArrayPool`1<System.Byte> TriLibCore.Fbx.PropertyAccessorShort::_decoded
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* ____decoded_1;
	// System.IO.MemoryStream TriLibCore.Fbx.PropertyAccessorShort::_decodedMemoryStream
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* ____decodedMemoryStream_2;
	// System.IO.BinaryReader TriLibCore.Fbx.PropertyAccessorShort::_decodedBinaryReader
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____decodedBinaryReader_3;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorShort::_count
	int32_t ____count_4;
	// System.Boolean TriLibCore.Fbx.PropertyAccessorShort::<IsReadOnly>k__BackingField
	bool ___U3CIsReadOnlyU3Ek__BackingField_5;
};

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449  : public RuntimeObject
{
	// System.String[] TriLibCore.ReaderBase::_loadingStepEnumNames
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____loadingStepEnumNames_1;
	// TriLibCore.AssetLoaderContext TriLibCore.ReaderBase::<AssetLoaderContext>k__BackingField
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___U3CAssetLoaderContextU3Ek__BackingField_2;
	// System.String TriLibCore.ReaderBase::_filename
	String_t* ____filename_3;
	// System.Action`2<TriLibCore.AssetLoaderContext,System.Single> TriLibCore.ReaderBase::_onProgress
	Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ____onProgress_4;
	// System.Int32 TriLibCore.ReaderBase::_nameCounter
	int32_t ____nameCounter_5;
	// System.Int32 TriLibCore.ReaderBase::_materialCounter
	int32_t ____materialCounter_6;
	// System.Int32 TriLibCore.ReaderBase::_textureCounter
	int32_t ____textureCounter_7;
	// System.Int32 TriLibCore.ReaderBase::_geometryGroupCounter
	int32_t ____geometryGroupCounter_8;
	// System.Int32 TriLibCore.ReaderBase::_animationCounter
	int32_t ____animationCounter_9;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Nullable`1<System.Double>
struct Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	double ___value_1;
};

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Byte>
struct PropertyAccessorIEnumerator_1_t507F53FFDEAE6189449897018F3198188410DEB2 
{
	// System.Collections.Generic.IList`1<T> TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_list
	RuntimeObject* ____list_0;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_listIndex
	int32_t ____listIndex_1;
};

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Double>
struct PropertyAccessorIEnumerator_1_tA87B54345A2924A4BB0DE1D0D4BE147D24B9056D 
{
	// System.Collections.Generic.IList`1<T> TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_list
	RuntimeObject* ____list_0;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_listIndex
	int32_t ____listIndex_1;
};

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int16>
struct PropertyAccessorIEnumerator_1_t99D6ABB7ECAA7EDD17341582A28BF20268F5C71D 
{
	// System.Collections.Generic.IList`1<T> TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_list
	RuntimeObject* ____list_0;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_listIndex
	int32_t ____listIndex_1;
};

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int32>
struct PropertyAccessorIEnumerator_1_tC80F58CC2A95871A7E8891567EEE4DAAC636E0A6 
{
	// System.Collections.Generic.IList`1<T> TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_list
	RuntimeObject* ____list_0;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_listIndex
	int32_t ____listIndex_1;
};

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int64>
struct PropertyAccessorIEnumerator_1_t81284489B090B3086F8560CFF14FCFA37CD50F96 
{
	// System.Collections.Generic.IList`1<T> TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_list
	RuntimeObject* ____list_0;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_listIndex
	int32_t ____listIndex_1;
};

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Single>
struct PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D 
{
	// System.Collections.Generic.IList`1<T> TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_list
	RuntimeObject* ____list_0;
	// System.Int32 TriLibCore.Fbx.PropertyAccessorIEnumerator`1::_listIndex
	int32_t ____listIndex_1;
};

// TriLibCore.Fbx.ASCII.ASCIIValueEnumerator
struct ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271 
{
	// System.IO.BinaryReader TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::_reader
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____reader_0;
	// System.Byte TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::<Current>k__BackingField
	uint8_t ___U3CCurrentU3Ek__BackingField_1;
};
// Native definition for P/Invoke marshalling of TriLibCore.Fbx.ASCII.ASCIIValueEnumerator
struct ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshaled_pinvoke
{
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____reader_0;
	uint8_t ___U3CCurrentU3Ek__BackingField_1;
};
// Native definition for COM marshalling of TriLibCore.Fbx.ASCII.ASCIIValueEnumerator
struct ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshaled_com
{
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ____reader_0;
	uint8_t ___U3CCurrentU3Ek__BackingField_1;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED 
{
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::_source
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};
// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_pinvoke
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_com
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// TriLibCore.Fbx.ASCII.FBXASCIITokenizer
struct FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E  : public BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158
{
	// System.Char[] TriLibCore.Fbx.ASCII.FBXASCIITokenizer::_charStream
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____charStream_23;
	// System.Char[] TriLibCore.Fbx.ASCII.FBXASCIITokenizer::_numberByteStream
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____numberByteStream_25;
	// System.String TriLibCore.Fbx.ASCII.FBXASCIITokenizer::_numberString
	String_t* ____numberString_26;
	// System.Char[] TriLibCore.Fbx.ASCII.FBXASCIITokenizer::_stringByteStream
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____stringByteStream_27;
	// System.Int32 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::_charPosition
	int32_t ____charPosition_28;
	// System.Int64 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::_tokenInitialPosition
	int64_t ____tokenInitialPosition_29;
	// System.Int64 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::_lastToken
	int64_t ____lastToken_30;
	// System.Int64 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::CharHash
	int64_t ___CharHash_35;
	// System.Boolean TriLibCore.Fbx.ASCII.FBXASCIITokenizer::_overflow
	bool ____overflow_36;
};

// TriLibCore.Fbx.Binary.FBXBinaryReader
struct FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50  : public BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158
{
	// System.Char[] TriLibCore.Fbx.Binary.FBXBinaryReader::Chars
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___Chars_11;
	// System.Byte[] TriLibCore.Fbx.Binary.FBXBinaryReader::Buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Buffer_12;
	// System.Int64 TriLibCore.Fbx.Binary.FBXBinaryReader::CharsHash
	int64_t ___CharsHash_13;
	// TriLibCore.ReaderBase TriLibCore.Fbx.Binary.FBXBinaryReader::_reader
	ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* ____reader_14;
	// System.Boolean TriLibCore.Fbx.Binary.FBXBinaryReader::_is64Bits
	bool ____is64Bits_15;
	// System.Int32 TriLibCore.Fbx.Binary.FBXBinaryReader::_version
	int32_t ____version_16;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream TriLibCore.Fbx.Binary.FBXBinaryReader::InflaterInputStream
	InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2* ___InflaterInputStream_17;
	// ICSharpCode.SharpZipLib.Zip.Compression.Inflater TriLibCore.Fbx.Binary.FBXBinaryReader::Inflater
	Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494* ___Inflater_18;
	// TriLibCore.Fbx.FBXProcessor TriLibCore.Fbx.Binary.FBXBinaryReader::_processor
	FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ____processor_19;
};

// TriLibCore.Fbx.FBXNode
struct FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D 
{
	// TriLibCore.Fbx.FBXProperties TriLibCore.Fbx.FBXNode::Properties
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___Properties_0;
	// System.Int64 TriLibCore.Fbx.FBXNode::NameHashCode
	int64_t ___NameHashCode_1;
	// System.Int32 TriLibCore.Fbx.FBXNode::NextNodeIndex
	int32_t ___NextNodeIndex_2;
	// System.Int32 TriLibCore.Fbx.FBXNode::FirstNodeIndex
	int32_t ___FirstNodeIndex_3;
	// TriLibCore.Fbx.FBXProcessor TriLibCore.Fbx.FBXNode::_processor
	FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ____processor_4;
};
// Native definition for P/Invoke marshalling of TriLibCore.Fbx.FBXNode
struct FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D_marshaled_pinvoke
{
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___Properties_0;
	int64_t ___NameHashCode_1;
	int32_t ___NextNodeIndex_2;
	int32_t ___FirstNodeIndex_3;
	FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ____processor_4;
};
// Native definition for COM marshalling of TriLibCore.Fbx.FBXNode
struct FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D_marshaled_com
{
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___Properties_0;
	int64_t ___NameHashCode_1;
	int32_t ___NextNodeIndex_2;
	int32_t ___FirstNodeIndex_3;
	FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ____processor_4;
};

// TriLibCore.Fbx.FBXProperty
struct FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 
{
	// System.Int64 TriLibCore.Fbx.FBXProperty::Position
	int64_t ___Position_0;
	// System.UInt16 TriLibCore.Fbx.FBXProperty::StringCharLength
	uint16_t ___StringCharLength_1;
	// System.Boolean TriLibCore.Fbx.FBXProperty::Overflow
	bool ___Overflow_2;
};
// Native definition for P/Invoke marshalling of TriLibCore.Fbx.FBXProperty
struct FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8_marshaled_pinvoke
{
	int64_t ___Position_0;
	uint16_t ___StringCharLength_1;
	int32_t ___Overflow_2;
};
// Native definition for COM marshalling of TriLibCore.Fbx.FBXProperty
struct FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8_marshaled_com
{
	int64_t ___Position_0;
	uint16_t ___StringCharLength_1;
	int32_t ___Overflow_2;
};

// System.Int16
struct Int16_tB8EF286A9C33492FA6E6D6E67320BE93E794A175 
{
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05* ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2* ____asyncActiveSemaphore_4;
};

// System.UInt16
struct UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455 
{
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// TriLibCore.AssetLoaderContext
struct AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C  : public RuntimeObject
{
	// TriLibCore.AssetLoaderOptions TriLibCore.AssetLoaderContext::Options
	AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* ___Options_0;
	// TriLibCore.ReaderBase TriLibCore.AssetLoaderContext::Reader
	ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* ___Reader_1;
	// System.String TriLibCore.AssetLoaderContext::Filename
	String_t* ___Filename_2;
	// System.String TriLibCore.AssetLoaderContext::FileExtension
	String_t* ___FileExtension_3;
	// System.IO.Stream TriLibCore.AssetLoaderContext::Stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Stream_4;
	// System.String TriLibCore.AssetLoaderContext::BasePath
	String_t* ___BasePath_5;
	// UnityEngine.GameObject TriLibCore.AssetLoaderContext::RootGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___RootGameObject_6;
	// TriLibCore.Interfaces.IRootModel TriLibCore.AssetLoaderContext::RootModel
	RuntimeObject* ___RootModel_7;
	// System.Collections.Generic.Dictionary`2<TriLibCore.Interfaces.IModel,UnityEngine.GameObject> TriLibCore.AssetLoaderContext::GameObjects
	Dictionary_2_tADE1FC3F6C786CACD6652C2C7275C3A0FD274A9C* ___GameObjects_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,TriLibCore.Interfaces.IModel> TriLibCore.AssetLoaderContext::Models
	Dictionary_2_tE704ACFE7C32537A046D8577F8299D1B52ED0C00* ___Models_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String> TriLibCore.AssetLoaderContext::GameObjectPaths
	Dictionary_2_t15A9DEF843D5DA84170CD8536BA0EBB039EB4ADF* ___GameObjectPaths_10;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,System.Collections.Generic.List`1<TriLibCore.MaterialRendererContext>> TriLibCore.AssetLoaderContext::MaterialRenderers
	ConcurrentDictionary_2_tA783589C825EB0CEA850D32094AAEFFBB3FD5D82* ___MaterialRenderers_11;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material> TriLibCore.AssetLoaderContext::LoadedMaterials
	ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2* ___LoadedMaterials_12;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material> TriLibCore.AssetLoaderContext::GeneratedMaterials
	ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2* ___GeneratedMaterials_13;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.ITexture,TriLibCore.TextureLoadingContext> TriLibCore.AssetLoaderContext::LoadedTextures
	ConcurrentDictionary_2_tBB5915FD91B3F65141A8C3EC64A14F14EEA3905B* ___LoadedTextures_14;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.General.CompoundMaterialKey,TriLibCore.TextureLoadingContext> TriLibCore.AssetLoaderContext::MaterialTextures
	ConcurrentDictionary_2_t94764B51655C4F04FDAE59E1A6327AFEE05EA292* ___MaterialTextures_15;
	// TriLibCore.General.ConcurrentDictionary`2<System.String,System.String> TriLibCore.AssetLoaderContext::LoadedExternalData
	ConcurrentDictionary_2_tCF44E0035FB42A2A1DF508A4CE0B233163C23F1E* ___LoadedExternalData_16;
	// System.Collections.Generic.HashSet`1<UnityEngine.Texture> TriLibCore.AssetLoaderContext::UsedTextures
	HashSet_1_t70836788BCAF42568800A162B9F23937F5309AE8* ___UsedTextures_17;
	// System.Collections.Generic.List`1<UnityEngine.Object> TriLibCore.AssetLoaderContext::Allocations
	List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3* ___Allocations_18;
	// System.Boolean TriLibCore.AssetLoaderContext::Async
	bool ___Async_19;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnLoad_20;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnMaterialsLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnMaterialsLoad_21;
	// System.Action`2<TriLibCore.AssetLoaderContext,System.Single> TriLibCore.AssetLoaderContext::OnProgress
	Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___OnProgress_22;
	// System.Action`1<TriLibCore.IContextualizedError> TriLibCore.AssetLoaderContext::OnError
	Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30* ___OnError_23;
	// System.Action`1<TriLibCore.IContextualizedError> TriLibCore.AssetLoaderContext::HandleError
	Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30* ___HandleError_24;
	// System.Object TriLibCore.AssetLoaderContext::CustomData
	RuntimeObject* ___CustomData_25;
	// System.Collections.Generic.List`1<System.Threading.Tasks.Task> TriLibCore.AssetLoaderContext::Tasks
	List_1_t84C257E858DDB8EA0B6269E08AAD9A2A2018A551* ___Tasks_26;
	// System.Threading.Tasks.Task TriLibCore.AssetLoaderContext::Task
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___Task_27;
	// System.Boolean TriLibCore.AssetLoaderContext::HaltTasks
	bool ___HaltTasks_28;
	// UnityEngine.GameObject TriLibCore.AssetLoaderContext::WrapperGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___WrapperGameObject_29;
	// System.Threading.CancellationToken TriLibCore.AssetLoaderContext::CancellationToken
	CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED ___CancellationToken_30;
	// System.Single TriLibCore.AssetLoaderContext::LoadingProgress
	float ___LoadingProgress_31;
	// System.Threading.CancellationTokenSource TriLibCore.AssetLoaderContext::CancellationTokenSource
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ___CancellationTokenSource_32;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnPreLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnPreLoad_33;
	// System.Boolean TriLibCore.AssetLoaderContext::IsZipFile
	bool ___IsZipFile_34;
	// System.String TriLibCore.AssetLoaderContext::<PersistentDataPath>k__BackingField
	String_t* ___U3CPersistentDataPathU3Ek__BackingField_35;
	// System.String TriLibCore.AssetLoaderContext::ModificationDate
	String_t* ___ModificationDate_36;
	// System.Int32 TriLibCore.AssetLoaderContext::LoadingStep
	int32_t ___LoadingStep_37;
	// System.Int32 TriLibCore.AssetLoaderContext::PreviousLoadingStep
	int32_t ___PreviousLoadingStep_38;
	// System.Collections.Generic.Queue`1<TriLibCore.Interfaces.IContextualizedAction> TriLibCore.AssetLoaderContext::<CustomDispatcherQueue>k__BackingField
	Queue_1_t952DE88AF42216B755D09647735E4235DA7138D4* ___U3CCustomDispatcherQueueU3Ek__BackingField_39;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]> TriLibCore.AssetLoaderContext::_bufferPool
	Dictionary_2_t23C2BC333CAB1901F8EC82B59264ED8D028DD1AB* ____bufferPool_40;
	// System.Boolean TriLibCore.AssetLoaderContext::<Completed>k__BackingField
	bool ___U3CCompletedU3Ek__BackingField_41;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// TriLibCore.Fbx.ASCII.FBXASCIIReader
struct FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B  : public FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E
{
	// TriLibCore.Fbx.FBXProcessor TriLibCore.Fbx.ASCII.FBXASCIIReader::_processor
	FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ____processor_37;
	// System.Collections.Generic.List`1<System.Char> TriLibCore.Fbx.ASCII.FBXASCIIReader::_characters
	List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* ____characters_38;
};

// TriLibCore.Fbx.FBXModel
struct FBXModel_t5D448CD22472ADEA4C42B7A9F299C393769E08A4  : public FBXObject_t955A55BC5B93846C9089D1A44A2DC3F65FC05671
{
	// TriLibCore.Fbx.FBXRotationOrder TriLibCore.Fbx.FBXModel::RotationOrder
	int32_t ___RotationOrder_7;
	// TriLibCore.Fbx.FBXInheritType TriLibCore.Fbx.FBXModel::InheritType
	int32_t ___InheritType_8;
	// System.Boolean TriLibCore.Fbx.FBXModel::<Visibility>k__BackingField
	bool ___U3CVisibilityU3Ek__BackingField_9;
	// System.Boolean TriLibCore.Fbx.FBXModel::VisibilityInheritance
	bool ___VisibilityInheritance_10;
	// TriLibCore.Fbx.FBXMatrices TriLibCore.Fbx.FBXModel::Matrices
	FBXMatrices_tDBC0EE205D2B2D729DBA25A18CF3A67388C5030D* ___Matrices_11;
	// UnityEngine.Vector3 TriLibCore.Fbx.FBXModel::_localPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ____localPosition_12;
	// UnityEngine.Quaternion TriLibCore.Fbx.FBXModel::_localRotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ____localRotation_13;
	// UnityEngine.Vector3 TriLibCore.Fbx.FBXModel::_localScale
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ____localScale_14;
	// System.Boolean TriLibCore.Fbx.FBXModel::<IsBone>k__BackingField
	bool ___U3CIsBoneU3Ek__BackingField_15;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Fbx.FBXModel::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_16;
	// TriLibCore.Interfaces.IModel TriLibCore.Fbx.FBXModel::_parent
	RuntimeObject* ____parent_17;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Fbx.FBXModel::<Children>k__BackingField
	RuntimeObject* ___U3CChildrenU3Ek__BackingField_18;
	// System.Int32 TriLibCore.Fbx.FBXModel::ChildrenCount
	int32_t ___ChildrenCount_19;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Fbx.FBXModel::<Bones>k__BackingField
	RuntimeObject* ___U3CBonesU3Ek__BackingField_20;
	// System.Int32 TriLibCore.Fbx.FBXModel::BonesCount
	int32_t ___BonesCount_21;
	// System.Collections.Generic.IList`1<UnityEngine.Matrix4x4> TriLibCore.Fbx.FBXModel::BindPosesList
	RuntimeObject* ___BindPosesList_22;
	// UnityEngine.Matrix4x4[] TriLibCore.Fbx.FBXModel::<BindPoses>k__BackingField
	Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___U3CBindPosesU3Ek__BackingField_23;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Fbx.FBXModel::<MaterialIndices>k__BackingField
	RuntimeObject* ___U3CMaterialIndicesU3Ek__BackingField_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Fbx.FBXModel::<UserProperties>k__BackingField
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___U3CUserPropertiesU3Ek__BackingField_25;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Fbx.FBXModel::AllMaterialIndices
	RuntimeObject* ___AllMaterialIndices_26;
	// System.Int32 TriLibCore.Fbx.FBXModel::AllMaterialIndicesCount
	int32_t ___AllMaterialIndicesCount_27;
	// TriLibCore.Fbx.FBXTexture TriLibCore.Fbx.FBXModel::DiffuseTexture
	FBXTexture_t144C224F07D8C26F18E312F7A530F5C15AF1D79D* ___DiffuseTexture_28;
	// UnityEngine.Vector3 TriLibCore.Fbx.FBXModel::Pivot
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___Pivot_29;
	// System.Collections.Generic.Dictionary`2<TriLibCore.Fbx.FBXAnimationStack,System.Collections.Generic.SortedSet`1<TriLibCore.Fbx.FBXAnimationLayer>> TriLibCore.Fbx.FBXModel::Layers
	Dictionary_2_tB3587CF77361D420D1DC7210EA1A4B3B312BB628* ___Layers_30;
	// System.Collections.Generic.Dictionary`2<TriLibCore.Fbx.FBXAnimationLayer,System.Collections.Generic.SortedSet`1<System.Int64>> TriLibCore.Fbx.FBXModel::AnimatedTimes
	Dictionary_2_tB49177EDEA5ECFF24C13CCA52CD1A58D58131EB5* ___AnimatedTimes_31;
};

// TriLibCore.Fbx.Reader.FbxReader
struct FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E  : public ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449
{
	// System.Byte[] TriLibCore.Fbx.Reader.FbxReader::_fileBuffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____fileBuffer_17;
};

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream
struct InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2  : public Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE
{
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::<IsStreamOwner>k__BackingField
	bool ___U3CIsStreamOwnerU3Ek__BackingField_5;
	// ICSharpCode.SharpZipLib.Zip.Compression.Inflater ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inf
	Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494* ___inf_6;
	// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inputBuffer
	InflaterInputBuffer_t947724C1CD4D44B75584D59D1ECA71C121D62129* ___inputBuffer_7;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::baseInputStream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___baseInputStream_8;
	// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::csize
	int64_t ___csize_9;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::isClosed
	bool ___isClosed_10;
};

// System.IO.MemoryStream
struct MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2  : public Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE
{
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____buffer_5;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_6;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_7;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_8;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_9;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_10;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_11;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_12;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_13;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t4C228DE57804012969575431CFF12D57C875552D* ____lastReadTask_14;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// TriLibCore.Fbx.FBXRootModel
struct FBXRootModel_t1960D19A6733168B2F9CF5A6235362FD25861775  : public FBXModel_t5D448CD22472ADEA4C42B7A9F299C393769E08A4
{
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Fbx.FBXRootModel::<AllModels>k__BackingField
	RuntimeObject* ___U3CAllModelsU3Ek__BackingField_32;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Fbx.FBXRootModel::<AllAnimations>k__BackingField
	RuntimeObject* ___U3CAllAnimationsU3Ek__BackingField_33;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Fbx.FBXRootModel::<AllMaterials>k__BackingField
	RuntimeObject* ___U3CAllMaterialsU3Ek__BackingField_34;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Fbx.FBXRootModel::<AllGeometryGroups>k__BackingField
	RuntimeObject* ___U3CAllGeometryGroupsU3Ek__BackingField_35;
	// System.Collections.Generic.IList`1<TriLibCore.Fbx.IFBXMesh> TriLibCore.Fbx.FBXRootModel::AllMeshes
	RuntimeObject* ___AllMeshes_36;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Fbx.FBXRootModel::<AllTextures>k__BackingField
	RuntimeObject* ___U3CAllTexturesU3Ek__BackingField_37;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Fbx.FBXRootModel::<AllCameras>k__BackingField
	RuntimeObject* ___U3CAllCamerasU3Ek__BackingField_38;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Fbx.FBXRootModel::<AllLights>k__BackingField
	RuntimeObject* ___U3CAllLightsU3Ek__BackingField_39;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
struct Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD  : public MulticastDelegate_t
{
};

// TriLibCore.Fbx.FBXDocument
struct FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834  : public FBXRootModel_t1960D19A6733168B2F9CF5A6235362FD25861775
{
	// System.Int32 TriLibCore.Fbx.FBXDocument::Version
	int32_t ___Version_42;
	// System.Boolean TriLibCore.Fbx.FBXDocument::IsBinary
	bool ___IsBinary_43;
	// TriLibCore.Fbx.FBXGlobalSettings TriLibCore.Fbx.FBXDocument::GlobalSettings
	FBXGlobalSettings_t0C07F154075AAF25F09B10FF36073B5A01B8BC28* ___GlobalSettings_44;
	// TriLibCore.Fbx.FBXModel TriLibCore.Fbx.FBXDocument::ModelDefinition
	FBXModel_t5D448CD22472ADEA4C42B7A9F299C393769E08A4* ___ModelDefinition_45;
	// TriLibCore.Fbx.FBXAnimationLayer TriLibCore.Fbx.FBXDocument::AnimationLayerDefinition
	FBXAnimationLayer_t691446B83D4B681F5922098EBD0E0B2F30A1CB50* ___AnimationLayerDefinition_46;
	// TriLibCore.Fbx.FBXAnimationCurve TriLibCore.Fbx.FBXDocument::AnimationCurveDefinition
	FBXAnimationCurve_t231EE593FF73ECE0908ED97A303F7EAFC5D4D45C* ___AnimationCurveDefinition_47;
	// TriLibCore.Fbx.FBXAnimationCurveNode TriLibCore.Fbx.FBXDocument::AnimationCurveNodeDefinition
	FBXAnimationCurveNode_t576192536BD712D1A036BD4CA26E65E2AF91D5BB* ___AnimationCurveNodeDefinition_48;
	// TriLibCore.Fbx.FBXAnimationStack TriLibCore.Fbx.FBXDocument::AnimationStackDefinition
	FBXAnimationStack_t1951A8FC41247D031C5FB2058F47DFFB6FEB4826* ___AnimationStackDefinition_49;
	// TriLibCore.Fbx.FBXDeformer TriLibCore.Fbx.FBXDocument::DeformerDefinition
	FBXDeformer_t6205A00C20A32DA310DE01502EDE739CD8AD9BC8* ___DeformerDefinition_50;
	// TriLibCore.Fbx.FBXPose TriLibCore.Fbx.FBXDocument::PoseDefinition
	FBXPose_t58C887C0A441F315857FDA99BBB3FF3A2E3158C3* ___PoseDefinition_51;
	// TriLibCore.Fbx.FBXMaterial TriLibCore.Fbx.FBXDocument::MaterialDefinition
	FBXMaterial_tF187223B7488940E1773EC2749D36BDB16F9C944* ___MaterialDefinition_52;
	// TriLibCore.Fbx.FBXVideo TriLibCore.Fbx.FBXDocument::VideoDefinition
	FBXVideo_t5E3B9EE50CF8C48C1694A512180A2CB465E89CD2* ___VideoDefinition_53;
	// TriLibCore.Fbx.FBXTexture TriLibCore.Fbx.FBXDocument::TextureDefinition
	FBXTexture_t144C224F07D8C26F18E312F7A530F5C15AF1D79D* ___TextureDefinition_54;
	// TriLibCore.Fbx.FBXBlendShapeGeometryGroup TriLibCore.Fbx.FBXDocument::BlendShapeGeometryGroupDefinition
	FBXBlendShapeGeometryGroup_t7F48F7377B8F0CEBAFADB748FCECFDA63D99949C* ___BlendShapeGeometryGroupDefinition_55;
	// TriLibCore.Fbx.IFBXMesh TriLibCore.Fbx.FBXDocument::GeometryGroupDefinition
	RuntimeObject* ___GeometryGroupDefinition_56;
	// TriLibCore.Fbx.FBXBlendShapeSubDeformer TriLibCore.Fbx.FBXDocument::BlendShapeChannelDefinition
	FBXBlendShapeSubDeformer_t8ED024CF120BB5964980D664E3E4A8814DC873A9* ___BlendShapeChannelDefinition_57;
	// TriLibCore.Fbx.FBXSubDeformer TriLibCore.Fbx.FBXDocument::SubDeformerDefinition
	FBXSubDeformer_tD0AE3226DCA22276BBDE337EEBAD3789F1143FDC* ___SubDeformerDefinition_58;
	// TriLibCore.Fbx.FBXNodeAttribute TriLibCore.Fbx.FBXDocument::NodeAttributeDefinition
	FBXNodeAttribute_tD64A5CE0BD58841F114311413B61A7AE29C6C593* ___NodeAttributeDefinition_59;
	// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Fbx.IFBXObject> TriLibCore.Fbx.FBXDocument::Objects
	Dictionary_2_tE8D0E51AE256AFF2CE801667465D9F47A030908B* ___Objects_60;
	// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXModel> TriLibCore.Fbx.FBXDocument::Models
	RuntimeObject* ___Models_61;
	// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXDeformer> TriLibCore.Fbx.FBXDocument::Deformers
	RuntimeObject* ___Deformers_62;
	// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXSubDeformer> TriLibCore.Fbx.FBXDocument::SubDeformers
	RuntimeObject* ___SubDeformers_63;
	// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXAnimationCurveNode> TriLibCore.Fbx.FBXDocument::AnimationCurveNodes
	RuntimeObject* ___AnimationCurveNodes_64;
	// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXAnimationCurve> TriLibCore.Fbx.FBXDocument::AnimationCurves
	RuntimeObject* ___AnimationCurves_65;
	// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXAnimationLayer> TriLibCore.Fbx.FBXDocument::AnimationLayers
	RuntimeObject* ___AnimationLayers_66;
	// System.Collections.Generic.IList`1<TriLibCore.Fbx.FBXBlendShapeGeometryGroup> TriLibCore.Fbx.FBXDocument::BlendShapeGeometryGroups
	RuntimeObject* ___BlendShapeGeometryGroups_67;
	// System.Int32 TriLibCore.Fbx.FBXDocument::ModelsCount
	int32_t ___ModelsCount_68;
	// System.Int32 TriLibCore.Fbx.FBXDocument::MaterialsCount
	int32_t ___MaterialsCount_69;
	// System.Int32 TriLibCore.Fbx.FBXDocument::VideosCount
	int32_t ___VideosCount_70;
	// System.Int32 TriLibCore.Fbx.FBXDocument::TexturesCount
	int32_t ___TexturesCount_71;
	// System.Int32 TriLibCore.Fbx.FBXDocument::LayeredTexturesCount
	int32_t ___LayeredTexturesCount_72;
	// System.Int32 TriLibCore.Fbx.FBXDocument::GeometriesCount
	int32_t ___GeometriesCount_73;
	// System.Int32 TriLibCore.Fbx.FBXDocument::AnimationStacksCount
	int32_t ___AnimationStacksCount_74;
	// System.Int32 TriLibCore.Fbx.FBXDocument::AnimationLayersCount
	int32_t ___AnimationLayersCount_75;
	// System.Int32 TriLibCore.Fbx.FBXDocument::AnimationCurvesCount
	int32_t ___AnimationCurvesCount_76;
	// System.Int32 TriLibCore.Fbx.FBXDocument::AnimationCurveNodesCount
	int32_t ___AnimationCurveNodesCount_77;
	// System.Int32 TriLibCore.Fbx.FBXDocument::DeformersCount
	int32_t ___DeformersCount_78;
	// System.Int32 TriLibCore.Fbx.FBXDocument::SubDeformersCount
	int32_t ___SubDeformersCount_79;
	// System.Int32 TriLibCore.Fbx.FBXDocument::ObjectsCount
	int32_t ___ObjectsCount_80;
	// System.Int32 TriLibCore.Fbx.FBXDocument::ConnectedGeometriesCount
	int32_t ___ConnectedGeometriesCount_81;
	// System.Int32 TriLibCore.Fbx.FBXDocument::CamerasCount
	int32_t ___CamerasCount_82;
	// System.Int32 TriLibCore.Fbx.FBXDocument::LightsCount
	int32_t ___LightsCount_83;
	// TriLibCore.Fbx.FBXMatrixBuffer TriLibCore.Fbx.FBXDocument::MatrixBuffer
	FBXMatrixBuffer_tF111AF67C063DB9EFE066B3E92AB4391B05BAF1F* ___MatrixBuffer_84;
	// System.Boolean TriLibCore.Fbx.FBXDocument::NewTC
	bool ___NewTC_85;
	// TriLibCore.Fbx.Reader.FbxReader TriLibCore.Fbx.FBXDocument::Reader
	FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* ___Reader_86;
};

// System.NotImplementedException
struct NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// TriLibCore.Pooling.ArrayPool`1<System.Byte>

// TriLibCore.Pooling.ArrayPool`1<System.Byte>

// System.Collections.Generic.List`1<System.Char>
struct List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Char>

// <PrivateImplementationDetails>

// <PrivateImplementationDetails>

// System.IO.BinaryReader

// System.IO.BinaryReader

// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095_StaticFields
{
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___latin1Encoding_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Text.Encoding> modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Dictionary_2_t87EDE08B2E48F793A22DE50D6B3CC2E7EBB2DB54* ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject* ___s_InternalSyncObject_15;
};

// System.Text.Encoding

// TriLibCore.Fbx.FBXObject

// TriLibCore.Fbx.FBXObject

// TriLibCore.Fbx.FBXProcessor

// TriLibCore.Fbx.FBXProcessor

// TriLibCore.Fbx.FBXProperties

// TriLibCore.Fbx.FBXProperties

// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494_StaticFields
{
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPLENS
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___CPLENS_0;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPLEXT
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___CPLEXT_1;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPDIST
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___CPDIST_2;
	// System.Int32[] ICSharpCode.SharpZipLib.Zip.Compression.Inflater::CPDEXT
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___CPDEXT_3;
};

// ICSharpCode.SharpZipLib.Zip.Compression.Inflater

// System.MarshalByRefObject

// System.MarshalByRefObject

// System.Reflection.MemberInfo

// System.Reflection.MemberInfo

// TriLibCore.Fbx.PropertyAccessorByte

// TriLibCore.Fbx.PropertyAccessorByte

// TriLibCore.Fbx.PropertyAccessorDouble

// TriLibCore.Fbx.PropertyAccessorDouble

// TriLibCore.Fbx.PropertyAccessorFloat

// TriLibCore.Fbx.PropertyAccessorFloat

// TriLibCore.Fbx.PropertyAccessorIEE754

// TriLibCore.Fbx.PropertyAccessorIEE754

// TriLibCore.Fbx.PropertyAccessorInt

// TriLibCore.Fbx.PropertyAccessorInt

// TriLibCore.Fbx.PropertyAccessorLong

// TriLibCore.Fbx.PropertyAccessorLong

// TriLibCore.Fbx.PropertyAccessorShort

// TriLibCore.Fbx.PropertyAccessorShort

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449_StaticFields
{
	// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64> TriLibCore.ReaderBase::ProfileStepCallback
	Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2* ___ProfileStepCallback_0;
};

// TriLibCore.ReaderBase

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.ValueType

// System.ValueType

// System.Nullable`1<System.Double>

// System.Nullable`1<System.Double>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Byte>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Byte>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Double>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Double>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int16>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int16>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int32>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int32>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int64>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int64>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Single>

// TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Single>

// TriLibCore.Fbx.ASCII.ASCIIValueEnumerator

// TriLibCore.Fbx.ASCII.ASCIIValueEnumerator

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_StaticFields
{
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_actionToActionObjShunt
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___s_actionToActionObjShunt_1;
};

// System.Threading.CancellationToken

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// System.Double

// System.Double

// TriLibCore.Fbx.ASCII.FBXASCIITokenizer

// TriLibCore.Fbx.ASCII.FBXASCIITokenizer

// TriLibCore.Fbx.Binary.FBXBinaryReader

// TriLibCore.Fbx.Binary.FBXBinaryReader

// TriLibCore.Fbx.FBXNode

// TriLibCore.Fbx.FBXNode

// TriLibCore.Fbx.FBXProperty

// TriLibCore.Fbx.FBXProperty

// System.Int16

// System.Int16

// System.Int32

// System.Int32

// System.Int64

// System.Int64

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Quaternion

// System.Single

// System.Single

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE_StaticFields
{
	// System.IO.Stream System.IO.Stream::Null
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Null_1;
};

// System.IO.Stream

// System.UInt16

// System.UInt16

// System.UInt32

// System.UInt32

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3

// System.Void

// System.Void

// TriLibCore.AssetLoaderContext

// TriLibCore.AssetLoaderContext

// System.Delegate

// System.Delegate

// System.Exception
struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};

// System.Exception

// TriLibCore.Fbx.ASCII.FBXASCIIReader

// TriLibCore.Fbx.ASCII.FBXASCIIReader

// TriLibCore.Fbx.FBXModel

// TriLibCore.Fbx.FBXModel

// TriLibCore.Fbx.Reader.FbxReader
struct FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_StaticFields
{
	// System.Boolean TriLibCore.Fbx.Reader.FbxReader::FBXCompatibilityMode
	bool ___FBXCompatibilityMode_10;
	// TriLibCore.Fbx.Reader.FBXPivotMode TriLibCore.Fbx.Reader.FbxReader::PivotMode
	int32_t ___PivotMode_11;
	// System.Boolean TriLibCore.Fbx.Reader.FbxReader::EnableSpecularMaterials
	bool ___EnableSpecularMaterials_12;
	// System.Nullable`1<System.Double> TriLibCore.Fbx.Reader.FbxReader::FBXConversionPrecision
	Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 ___FBXConversionPrecision_13;
	// System.Boolean TriLibCore.Fbx.Reader.FbxReader::MergeSingleChildDocument
	bool ___MergeSingleChildDocument_14;
	// System.Boolean TriLibCore.Fbx.Reader.FbxReader::BufferizeStream
	bool ___BufferizeStream_15;
	// System.Boolean TriLibCore.Fbx.Reader.FbxReader::ApplyAmbientColor
	bool ___ApplyAmbientColor_16;
};

// TriLibCore.Fbx.Reader.FbxReader

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream

// System.IO.MemoryStream

// System.IO.MemoryStream

// System.RuntimeTypeHandle

// System.RuntimeTypeHandle

// TriLibCore.Fbx.FBXRootModel

// TriLibCore.Fbx.FBXRootModel

// System.MulticastDelegate

// System.MulticastDelegate

// System.SystemException

// System.SystemException

// System.Type
struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Type

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>

// TriLibCore.Fbx.FBXDocument

// TriLibCore.Fbx.FBXDocument

// System.NotImplementedException

// System.NotImplementedException
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int16[]
struct Int16U5BU5D_t8175CE8DD9C9F9FB0CF4F58E45BC570575B43CFB  : public RuntimeArray
{
	ALIGN_FIELD (8) int16_t m_Items[1];

	inline int16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int16_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C  : public RuntimeArray
{
	ALIGN_FIELD (8) float m_Items[1];

	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C  : public RuntimeArray
{
	ALIGN_FIELD (8) int32_t m_Items[1];

	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int64[]
struct Int64U5BU5D_tAEDFCBDB5414E2A140A6F34C0538BF97FCF67A1D  : public RuntimeArray
{
	ALIGN_FIELD (8) int64_t m_Items[1];

	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// System.Double[]
struct DoubleU5BU5D_tCC308475BD3B8229DB2582938669EF2F9ECC1FEE  : public RuntimeArray
{
	ALIGN_FIELD (8) double m_Items[1];

	inline double GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline double* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, double value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline double GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline double* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, double value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB  : public RuntimeArray
{
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};


// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Byte>::.ctor(System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEnumerator_1__ctor_m13F6A2E033696DC706279AE3A91D02A64206AC4D_gshared (PropertyAccessorIEnumerator_1_t507F53FFDEAE6189449897018F3198188410DEB2* __this, RuntimeObject* ___0_list, const RuntimeMethod* method) ;
// System.Void TriLibCore.Pooling.ArrayPool`1<System.Byte>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_gshared (ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int16>::.ctor(System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEnumerator_1__ctor_m10ADEEA2B95198A97E5327E47BFAB5720EA81883_gshared (PropertyAccessorIEnumerator_1_t99D6ABB7ECAA7EDD17341582A28BF20268F5C71D* __this, RuntimeObject* ___0_list, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Single>::.ctor(System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC_gshared (PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D* __this, RuntimeObject* ___0_list, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int32>::.ctor(System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEnumerator_1__ctor_m698626BC437191FDC212DB020833A50A9A35807A_gshared (PropertyAccessorIEnumerator_1_tC80F58CC2A95871A7E8891567EEE4DAAC636E0A6* __this, RuntimeObject* ___0_list, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int64>::.ctor(System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEnumerator_1__ctor_m52017FEBE78CFF4A3918E710C90AFBB0AE6B434F_gshared (PropertyAccessorIEnumerator_1_t81284489B090B3086F8560CFF14FCFA37CD50F96* __this, RuntimeObject* ___0_list, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Double>::.ctor(System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEnumerator_1__ctor_m807FB04952574C23B9ECCFF282D11027CB4022CD_gshared (PropertyAccessorIEnumerator_1_tA87B54345A2924A4BB0DE1D0D4BE147D24B9056D* __this, RuntimeObject* ___0_list, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Char>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC679EFF5E634878F1897D71DC5160A96EA719E82_gshared (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Char>::Clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m0586B319F89682059DD157C1EDC282A2888ECB9B_gshared_inline (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Char>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mF3227B0AAB9F7FED1883246395F1CEA0D0B06DC5_gshared_inline (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* __this, Il2CppChar ___0_item, const RuntimeMethod* method) ;
// System.String System.String::Concat<System.Char>(System.Collections.Generic.IEnumerable`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_m691C56B3F5A4F992794166066A4B0D10B8B74461_gshared (RuntimeObject* ___0_values, const RuntimeMethod* method) ;

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.FBXProperties::GetPropertyArrayLength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FBXProperties_GetPropertyArrayLength_mD43333FAA9443D29CC2A86C914E75F7D2DF57EC9 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Byte>::.ctor(System.Collections.Generic.IList`1<T>)
inline void PropertyAccessorIEnumerator_1__ctor_m13F6A2E033696DC706279AE3A91D02A64206AC4D (PropertyAccessorIEnumerator_1_t507F53FFDEAE6189449897018F3198188410DEB2* __this, RuntimeObject* ___0_list, const RuntimeMethod* method)
{
	((  void (*) (PropertyAccessorIEnumerator_1_t507F53FFDEAE6189449897018F3198188410DEB2*, RuntimeObject*, const RuntimeMethod*))PropertyAccessorIEnumerator_1__ctor_m13F6A2E033696DC706279AE3A91D02A64206AC4D_gshared)(__this, ___0_list, method);
}
// System.Collections.Generic.IEnumerator`1<System.Byte> TriLibCore.Fbx.PropertyAccessorByte::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorByte_GetEnumerator_m0CEC0EE1DFBA9926652B838388F72EB35D64CD8C (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, const RuntimeMethod* method) ;
// System.Void System.NotImplementedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* __this, const RuntimeMethod* method) ;
// System.Byte TriLibCore.Fbx.PropertyAccessorByte::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t PropertyAccessorByte_GetElement_mBAF530E2CA25A3BD699AD74B7793BFC14481DCFB (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, int32_t ___0_i, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.PropertyAccessorByte::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorByte_get_Count_m248A77D652A9BF16734BE54219C5F5EAC9D3C360_inline (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Fbx.FBXProperties::get_IsASCII()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FBXProperties_get_IsASCII_m412715B4396F75F7B1127FD4A4B44AC3BC963420 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, const RuntimeMethod* method) ;
// System.Byte TriLibCore.Fbx.FBXProperties::ASCIIGetByteValue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t FBXProperties_ASCIIGetByteValue_mEE859B6C1DC74635CE3954D26BF992EEFBB22F72 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// TriLibCore.Fbx.FBXProcessor TriLibCore.Fbx.FBXProperties::get_Processor()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, const RuntimeMethod* method) ;
// TriLibCore.Fbx.FBXProperty TriLibCore.Fbx.FBXProcessor::LoadArrayProperty(TriLibCore.Fbx.FBXProperties,TriLibCore.Pooling.ArrayPool`1<System.Byte>&,System.IO.MemoryStream&,System.IO.BinaryReader&,System.Char&,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 FBXProcessor_LoadArrayProperty_m97367395AB157AA3F3BA7EAF59C2D24777438FC5 (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* __this, FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___0_properties, ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086** ___1_decoded, MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2** ___2_decodedMemoryStream, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158** ___3_decodedBinaryReader, Il2CppChar* ___4_propertyType, bool* ___5_encoded, const RuntimeMethod* method) ;
// System.Byte TriLibCore.Fbx.FBXProperties::BinaryConvertByteValue(System.IO.BinaryReader,TriLibCore.Fbx.FBXProperty,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t FBXProperties_BinaryConvertByteValue_mF77E2540B4A28F588C419E541177CF79D2258E1E (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_binaryReader, FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 ___1_binaryProperty, Il2CppChar ___2_propertyType, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.FBXProcessor::ReleaseActiveBinaryReader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXProcessor_ReleaseActiveBinaryReader_mC144B788F2FFC4093884077E12EE33B4DEC6E461 (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Pooling.ArrayPool`1<System.Byte>::Dispose()
inline void ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2 (ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* __this, const RuntimeMethod* method)
{
	((  void (*) (ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086*, const RuntimeMethod*))ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_gshared)(__this, method);
}
// System.Void System.IO.Stream::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stream_Dispose_mCDB42F32A17541CCA6D3A5906827A401570B07A8 (Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* __this, const RuntimeMethod* method) ;
// System.Void System.IO.BinaryReader::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryReader_Dispose_mAFF1A9CE9A73D148270FFA1F896992EB52D36078 (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int16>::.ctor(System.Collections.Generic.IList`1<T>)
inline void PropertyAccessorIEnumerator_1__ctor_m10ADEEA2B95198A97E5327E47BFAB5720EA81883 (PropertyAccessorIEnumerator_1_t99D6ABB7ECAA7EDD17341582A28BF20268F5C71D* __this, RuntimeObject* ___0_list, const RuntimeMethod* method)
{
	((  void (*) (PropertyAccessorIEnumerator_1_t99D6ABB7ECAA7EDD17341582A28BF20268F5C71D*, RuntimeObject*, const RuntimeMethod*))PropertyAccessorIEnumerator_1__ctor_m10ADEEA2B95198A97E5327E47BFAB5720EA81883_gshared)(__this, ___0_list, method);
}
// System.Collections.Generic.IEnumerator`1<System.Int16> TriLibCore.Fbx.PropertyAccessorShort::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorShort_GetEnumerator_m9C4184C792895C2EF4BABD954964E416C9FC0DA4 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, const RuntimeMethod* method) ;
// System.Int16 TriLibCore.Fbx.PropertyAccessorShort::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t PropertyAccessorShort_GetElement_m21FD1368E6B6DEF31B2D03E006DA07E7A6657778 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int32_t ___0_i, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.PropertyAccessorShort::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorShort_get_Count_m7778AE2F440E033D26ECAC2174D79C054CCEDCE3_inline (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, const RuntimeMethod* method) ;
// System.Int16 TriLibCore.Fbx.FBXProperties::ASCIIGetShortValue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t FBXProperties_ASCIIGetShortValue_m51DDC9F9C39A3A7DC82D1C1482928EE416CD9B45 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Int16 TriLibCore.Fbx.FBXProperties::BinaryConvertShortValue(System.IO.BinaryReader,TriLibCore.Fbx.FBXProperty,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t FBXProperties_BinaryConvertShortValue_mF594EF256C56FFC7C4E1107570F0156484A66A29 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_binaryReader, FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 ___1_binaryProperty, Il2CppChar ___2_propertyType, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Clamp_m4DC36EEFDBE5F07C16249DA568023C5ECCFF0E7B_inline (int32_t ___0_value, int32_t ___1_min, int32_t ___2_max, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Single>::.ctor(System.Collections.Generic.IList`1<T>)
inline void PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC (PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D* __this, RuntimeObject* ___0_list, const RuntimeMethod* method)
{
	((  void (*) (PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D*, RuntimeObject*, const RuntimeMethod*))PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC_gshared)(__this, ___0_list, method);
}
// System.Collections.Generic.IEnumerator`1<System.Single> TriLibCore.Fbx.PropertyAccessorIEE754::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorIEE754_GetEnumerator_mA6FECF69D50702B86D10F00937E845DB91DE1171 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, const RuntimeMethod* method) ;
// System.Single TriLibCore.Fbx.PropertyAccessorIEE754::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PropertyAccessorIEE754_GetElement_mAEDF6D2F614A73FF653B089BD2FD07F4D809D893 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, int32_t ___0_i, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.PropertyAccessorIEE754::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorIEE754_get_Count_m3DBE759D8CC11737529D4AA5739849E4AEE4A54C_inline (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.FBXProperties::ASCIIGetIntValue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FBXProperties_ASCIIGetIntValue_mD5B6A00346736642E5944098176C6A9715E60592 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Boolean System.Single::IsNaN(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Single_IsNaN_mFE637F6ECA9F7697CE8EFF56427858F4C5EDF75D_inline (float ___0_f, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int32>::.ctor(System.Collections.Generic.IList`1<T>)
inline void PropertyAccessorIEnumerator_1__ctor_m698626BC437191FDC212DB020833A50A9A35807A (PropertyAccessorIEnumerator_1_tC80F58CC2A95871A7E8891567EEE4DAAC636E0A6* __this, RuntimeObject* ___0_list, const RuntimeMethod* method)
{
	((  void (*) (PropertyAccessorIEnumerator_1_tC80F58CC2A95871A7E8891567EEE4DAAC636E0A6*, RuntimeObject*, const RuntimeMethod*))PropertyAccessorIEnumerator_1__ctor_m698626BC437191FDC212DB020833A50A9A35807A_gshared)(__this, ___0_list, method);
}
// System.Collections.Generic.IEnumerator`1<System.Int32> TriLibCore.Fbx.PropertyAccessorInt::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorInt_GetEnumerator_mBB1428C9E22A3A29F1D9ECFCA21FD6F77301720F (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.PropertyAccessorInt::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorInt_GetElement_m601D35A0D8860914D136AE3CD434BE51E404C7FB (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_i, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.PropertyAccessorInt::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorInt_get_Count_m0C89ED9BBF123AF37C9647E9D08743150C453D88_inline (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.FBXProperties::BinaryConvertIntValue(System.IO.BinaryReader,TriLibCore.Fbx.FBXProperty,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FBXProperties_BinaryConvertIntValue_mEB71EF9DBBBA4E9DBFDAEC2A6A90C9FDCEE73CFA (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_binaryReader, FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 ___1_binaryProperty, Il2CppChar ___2_propertyType, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Int64>::.ctor(System.Collections.Generic.IList`1<T>)
inline void PropertyAccessorIEnumerator_1__ctor_m52017FEBE78CFF4A3918E710C90AFBB0AE6B434F (PropertyAccessorIEnumerator_1_t81284489B090B3086F8560CFF14FCFA37CD50F96* __this, RuntimeObject* ___0_list, const RuntimeMethod* method)
{
	((  void (*) (PropertyAccessorIEnumerator_1_t81284489B090B3086F8560CFF14FCFA37CD50F96*, RuntimeObject*, const RuntimeMethod*))PropertyAccessorIEnumerator_1__ctor_m52017FEBE78CFF4A3918E710C90AFBB0AE6B434F_gshared)(__this, ___0_list, method);
}
// System.Collections.Generic.IEnumerator`1<System.Int64> TriLibCore.Fbx.PropertyAccessorLong::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorLong_GetEnumerator_m12B3301EE1B107A05C091AB0E1CC76E0C3804458 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Fbx.PropertyAccessorLong::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t PropertyAccessorLong_GetElement_m296C57EACB6F02ABEB4471B17EB24503791DE6D0 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int32_t ___0_i, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.PropertyAccessorLong::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorLong_get_Count_m4848D70EC610CBA6E351059DFA38F3D565B89DF9_inline (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Fbx.FBXProperties::ASCIIGetLongValue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t FBXProperties_ASCIIGetLongValue_m7AAAE670097E8A2E4D8CC45E10D2FB8825731B15 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Single TriLibCore.Fbx.FBXDocument::ConvertFromFBXTime(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FBXDocument_ConvertFromFBXTime_mCA9983CE579F4C635D9C0E7D61A5E2094DACD9BE (FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* __this, int64_t ___0_time, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Fbx.FBXDocument::ConvertToFBXTime(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t FBXDocument_ConvertToFBXTime_m523702DB9568E55B1493A765B9641FFE84AF6CF8 (FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* __this, double ___0_time, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Fbx.FBXProperties::BinaryConvertLongValue(System.IO.BinaryReader,TriLibCore.Fbx.FBXProperty,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t FBXProperties_BinaryConvertLongValue_mB90FC3F21C16EC402D0C7D428417106E66E65BF3 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_binaryReader, FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 ___1_binaryProperty, Il2CppChar ___2_propertyType, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerator`1<System.Single> TriLibCore.Fbx.PropertyAccessorFloat::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorFloat_GetEnumerator_mC7CA3AD0597A0B85F4037019EFF259A7BBF1D425 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, const RuntimeMethod* method) ;
// System.Single TriLibCore.Fbx.PropertyAccessorFloat::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PropertyAccessorFloat_GetElement_mA56ED47BACBFA0BBA492E270FD5AA697850A5EF0 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, int32_t ___0_i, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.PropertyAccessorFloat::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorFloat_get_Count_m0AA67B6688957CA7F3B2E57EACE0882A5A05879C_inline (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, const RuntimeMethod* method) ;
// System.Single TriLibCore.Fbx.FBXProperties::ASCIIGetFloatValue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FBXProperties_ASCIIGetFloatValue_m73F67724B2EFDA0EF8AAA9D64DC918E2436A40BA (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Single TriLibCore.Fbx.FBXProperties::BinaryConvertFloatValue(System.IO.BinaryReader,TriLibCore.Fbx.FBXProperty,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FBXProperties_BinaryConvertFloatValue_m5B4BCE0F186119B0C876D7D7E562109827608880 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_binaryReader, FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 ___1_binaryProperty, Il2CppChar ___2_propertyType, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.PropertyAccessorIEnumerator`1<System.Double>::.ctor(System.Collections.Generic.IList`1<T>)
inline void PropertyAccessorIEnumerator_1__ctor_m807FB04952574C23B9ECCFF282D11027CB4022CD (PropertyAccessorIEnumerator_1_tA87B54345A2924A4BB0DE1D0D4BE147D24B9056D* __this, RuntimeObject* ___0_list, const RuntimeMethod* method)
{
	((  void (*) (PropertyAccessorIEnumerator_1_tA87B54345A2924A4BB0DE1D0D4BE147D24B9056D*, RuntimeObject*, const RuntimeMethod*))PropertyAccessorIEnumerator_1__ctor_m807FB04952574C23B9ECCFF282D11027CB4022CD_gshared)(__this, ___0_list, method);
}
// System.Collections.Generic.IEnumerator`1<System.Double> TriLibCore.Fbx.PropertyAccessorDouble::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorDouble_GetEnumerator_m9F6C9FED59EA7039C0F615AAD3881AD89F9A6694 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, const RuntimeMethod* method) ;
// System.Double TriLibCore.Fbx.PropertyAccessorDouble::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double PropertyAccessorDouble_GetElement_m23682071BF27DDD51AA30507127971F691D4DFAE (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, int32_t ___0_i, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.PropertyAccessorDouble::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorDouble_get_Count_mD1726F34A29A7ACEF3A1CF4C1CB12FABE08D7197_inline (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, const RuntimeMethod* method) ;
// System.Double TriLibCore.Fbx.FBXProperties::ASCIIGetDoubleValue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double FBXProperties_ASCIIGetDoubleValue_m80150CE66E655F21CB5F37689DF79FF9822C0C07 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Double TriLibCore.Fbx.FBXProperties::BinaryConvertDoubleValue(System.IO.BinaryReader,TriLibCore.Fbx.FBXProperty,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double FBXProperties_BinaryConvertDoubleValue_m70C6B2D77AF7105A1AFF6E4E3CE0812DEE30A962 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___0_binaryReader, FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 ___1_binaryProperty, Il2CppChar ___2_propertyType, const RuntimeMethod* method) ;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57 (RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___0_handle, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.ReaderBase::ReadStream(System.IO.Stream,TriLibCore.AssetLoaderContext,System.String,System.Action`2<TriLibCore.AssetLoaderContext,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReaderBase_ReadStream_m725378DF096B29E0DB3BE3FB9E5F1E37747883F4 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, String_t* ___2_filename, Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___3_onProgress, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::SetupStream(System.IO.Stream&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_SetupStream_mCDC78453E3657CB3FBB713C40FB50B4941455942 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE** ___0_stream, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Fbx.Reader.FbxReader::IsBinary(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FbxReader_IsBinary_m8D593E28171C3E908C7FDAE60CA21BBDE49A4C56 (Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.Fbx.Reader.FbxReader::ParseBinary(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FbxReader_ParseBinary_m8B56277ECB7EEE23D9D8477378311DAC78DD5515 (FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::PostProcessModel(TriLibCore.Interfaces.IRootModel&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_PostProcessModel_mD3BB953DE8BEB5FCA65077455BDA0750F740C667 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, RuntimeObject** ___0_model, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Fbx.Reader.FbxReader::IsASCII(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FbxReader_IsASCII_m1FA268B6126E6613ABDAFD1CBB5599C67BDC10AF (Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.Fbx.Reader.FbxReader::ParseASCII(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FbxReader_ParseASCII_m9B2DF8EBC9701E3DA1CAFC1067057815F487066F (FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.FBXDocument::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXDocument__ctor_m725644F1B4CBB463A84BA6C11C0B8990847B2390 (FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.FBXProcessor::.ctor(TriLibCore.Fbx.Reader.FbxReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXProcessor__ctor_m9CE53C2B543FA782A9F365B4EDFC3DB87BFB7977 (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* __this, FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* ___0_reader, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.Binary.FBXBinaryReader::.ctor(System.IO.Stream,TriLibCore.ReaderBase,TriLibCore.Fbx.FBXProcessor)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXBinaryReader__ctor_mE643C5E24AF0209662FD47E0FA56FFCF9CB56554 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_inputStream, ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* ___1_reader, FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ___2_processor, const RuntimeMethod* method) ;
// TriLibCore.Fbx.FBXNode TriLibCore.Fbx.Binary.FBXBinaryReader::ReadDocument()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D FBXBinaryReader_ReadDocument_m27AFF3DF4C3E9C70713109A0A2FB0FA293674E64 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) ;
// TriLibCore.Fbx.FBXRootModel TriLibCore.Fbx.FBXProcessor::Process(TriLibCore.Fbx.FBXNode,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXRootModel_t1960D19A6733168B2F9CF5A6235362FD25861775* FBXProcessor_Process_mD196AFB65D13AA7C75FE1409F327F5913214E902 (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* __this, FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D ___0_rootNode, bool ___1_isBinary, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.Binary.FBXBinaryReader::Unload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXBinaryReader_Unload_m849A9163DF9496C688291EA217710B09C28E0E00 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.ASCII.FBXASCIIReader::.ctor(TriLibCore.Fbx.FBXProcessor,System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIIReader__ctor_mEB75301BADCB18697945E53B545B3A5B5E9787C7 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ___0_processor, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___1_stream, const RuntimeMethod* method) ;
// TriLibCore.Fbx.FBXNode TriLibCore.Fbx.ASCII.FBXASCIIReader::ReadDocument()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D FBXASCIIReader_ReadDocument_m42A06BB831534E44B0CFDE179264545AC4875724 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::UpdateLoadingPercentage(System.Single,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, float ___0_value, int32_t ___1_step, float ___2_maxValue, const RuntimeMethod* method) ;
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* Encoding_get_UTF8_m9FA98A53CE96FD6D02982625C5246DD36C1235C9 (const RuntimeMethod* method) ;
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1 (String_t* ___0_a, String_t* ___1_b, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Extensions.StreamExtensions::MatchRegex(System.IO.Stream,System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StreamExtensions_MatchRegex_m4D785C0985D867DC78F422F0DF28EDFDA23A5905 (Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___1_patterns, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase__ctor_m5C4FE7A4BC205B65DAB56FF3CC5202D0B04937DA (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, const RuntimeMethod* method) ;
// System.Void System.IO.BinaryReader::.ctor(System.IO.Stream,System.Text.Encoding,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryReader__ctor_m5B206ED513B0AECC14E4AF5A7B42AE5C4885334E (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_input, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, bool ___2_leaveOpen, const RuntimeMethod* method) ;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Inflater__ctor_m5183A7AAD2E39FFDCEE42A83E207181960B95FF9 (Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494* __this, bool ___0_noHeader, const RuntimeMethod* method) ;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream__ctor_mAEA971E711654A0AFB588ABE8512960D41F3E3D0 (InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_baseInputStream, Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494* ___1_inf, const RuntimeMethod* method) ;
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F (Exception_t* __this, String_t* ___0_message, const RuntimeMethod* method) ;
// TriLibCore.Fbx.FBXNode TriLibCore.Fbx.Binary.FBXBinaryReader::ReadRootNode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D FBXBinaryReader_ReadRootNode_m588BA96B26A31C30F9A70229130034A770D39CD6 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.Binary.FBXBinaryReader::SkipStringEx()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXBinaryReader_SkipStringEx_mCC66E0E7BDF4C7F65EF9BB62CE92B8CBFA874148 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Fbx.Binary.FBXBinaryReader::CountChildNodes(System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FBXBinaryReader_CountChildNodes_mCBAAD8D4DBBECD855F5BCC757BA5E061B374C4DF (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, int32_t* ___0_childCount, int32_t* ___1_propertiesCount, const RuntimeMethod* method) ;
// System.Int32 System.IO.BinaryReader::Read7BitEncodedInt()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BinaryReader_Read7BitEncodedInt_mAC30887A2BB23F481A73FA61A487159F855D34F5 (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, const RuntimeMethod* method) ;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB (RuntimeArray* ___0_array, int32_t ___1_index, int32_t ___2_length, const RuntimeMethod* method) ;
// System.String System.String::CreateString(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mB7B3AC2AF28010538650051A9000369B1CD6BAB6 (String_t* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___0_val, int32_t ___1_startIndex, int32_t ___2_length, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.Binary.FBXBinaryReader::ReadNode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FBXBinaryReader_ReadNode_m7B76CA3D4E6C170ADDBC95526EEB5A823900C92E (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.FBXProcessor::SetNodeNext(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXProcessor_SetNodeNext_m8630F0DDE868E67E1C3EBE3BE0F8ABBA8BF5484C (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* __this, int32_t ___0_nodeIndex, int32_t ___1_nextNodeIndex, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.FBXProperties::.ctor(TriLibCore.Fbx.FBXProcessor,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXProperties__ctor_m9FAEE6F003B0285D473D3E241974FC3C9EBFE67C (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ___0_processor, int32_t ___1_propertiesCount, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.FBXNode::.ctor(TriLibCore.Fbx.FBXProcessor,System.String,TriLibCore.Fbx.FBXProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXNode__ctor_m3D406E7ADA2DA527D69C7573E4866196EE3DD631 (FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D* __this, FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ___0_processor, String_t* ___1_name, FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___2_properties, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.FBXProcessor::AddNode(TriLibCore.Fbx.FBXNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FBXProcessor_AddNode_m39374DBF94E11AB90240A841029D230D62AC872D (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* __this, FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D ___0_node, const RuntimeMethod* method) ;
// System.String TriLibCore.Fbx.Binary.FBXBinaryReader::ReadStringEx()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FBXBinaryReader_ReadStringEx_m9FC9DD7CD2CBFCD8D4F15FC652C5B99423BF8390 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8 (String_t* ___0_format, RuntimeObject* ___1_arg0, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.FBXProcessor::AddProperty(TriLibCore.Fbx.FBXProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXProcessor_AddProperty_m013216FFEAA69120137545F668B3CA08405BDCA3 (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* __this, FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 ___0_property, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478 (String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::set_Current(System.Byte)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ASCIIValueEnumerator_set_Current_m5723F350D36F2E6F7378B24D0C439317DE57BD2C_inline (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, uint8_t ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::.ctor(TriLibCore.Fbx.ASCII.FBXASCIIReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ASCIIValueEnumerator__ctor_mCC8BD1780C632806437D4E4F3D8559D85EF066EA (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* ___0_reader, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ASCIIValueEnumerator_Dispose_mA5A3C9128607CC4D986FB2EA55603AB7DB0B941A (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) ;
// System.Byte TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t ASCIIValueEnumerator_get_Current_m777B03023C16D40AE16CCC8B89B55902BA9DBBC1_inline (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ASCIIValueEnumerator_MoveNext_mAAAA36FDA3818102E5E8ADE87DD90DD1F68B4D6B (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ASCIIValueEnumerator_Reset_m13657F8750510D0255DF1E87944D5053E202073A (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) ;
// System.Object TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ASCIIValueEnumerator_System_Collections_IEnumerator_get_Current_m4DF5316DE6B01D44743EB8FD8D492C49BAAA9931 (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.ASCII.FBXASCIIReader::ReadAllNodes(System.Int32&,System.Int32&,System.Int32&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FBXASCIIReader_ReadAllNodes_mD1FE99994685F3FCCEF4B906535C7DD60EE833FB (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, int32_t* ___0_nodesCount, int32_t* ___1_propertiesCount, int32_t* ___2_firstNodeIndex, bool ___3_countOnly, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Char>::.ctor()
inline void List_1__ctor_mC679EFF5E634878F1897D71DC5160A96EA719E82 (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7*, const RuntimeMethod*))List_1__ctor_mC679EFF5E634878F1897D71DC5160A96EA719E82_gshared)(__this, method);
}
// System.Void TriLibCore.Fbx.ASCII.FBXASCIITokenizer::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIITokenizer__ctor_mAEBC0B5E80C2125EA25B93C7A667B5A32E973EF3 (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.ASCII.FBXASCIITokenizer::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIITokenizer_Reset_m34277EFD55E370A438B73A32DF14ECDDF517B491 (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.ASCII.FBXASCIIReader::ReadAndProcessNode(System.Int32&,System.Int32&,System.Int32&,System.Boolean,System.Boolean,System.Int32&,System.Int32&,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIIReader_ReadAndProcessNode_mB0246287DC757CB4D5B4762FE8A833CB091EAA38 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, int32_t* ___0_nodesCount, int32_t* ___1_propertiesCount, int32_t* ___2_firstNodeIndex, bool ___3_countOnly, bool ___4_isArrayParent, int32_t* ___5_lastNodeIndex, int32_t* ___6_basePropertyIndex, String_t* ___7_path, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Fbx.ASCII.FBXASCIITokenizer::get_EndOfStream()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FBXASCIITokenizer_get_EndOfStream_m5BD0A303ABE71F24333565BD20C1AE51ED737D3A (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::ReadNextValidToken(System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t FBXASCIITokenizer_ReadNextValidToken_m1837A0CD8AD6CCAE906F9B4CE0C1D3C42B191A6C (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, bool ___0_required, bool ___1_ignoreSpace, bool ___2_ignoreEndOfLine, const RuntimeMethod* method) ;
// TriLibCore.Fbx.FBXNode TriLibCore.Fbx.ASCII.FBXASCIIReader::ReadNode(System.Int32&,System.Int32&,System.Boolean,System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D FBXASCIIReader_ReadNode_mD0B13F651A7A4B2C320F97780487910AC89131A5 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, int32_t* ___0_nodesCount, int32_t* ___1_propertiesCount, bool ___2_countOnly, bool ___3_isArrayChild, String_t* ___4_path, const RuntimeMethod* method) ;
// System.String TriLibCore.Fbx.ASCII.FBXASCIITokenizer::GetTokenAsString(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FBXASCIITokenizer_GetTokenAsString_m09A7D0E5F1E872CC4125CA77E8310FD54DCD42B7 (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, bool ___0_isString, const RuntimeMethod* method) ;
// System.Boolean System.Int32::TryParse(System.String,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int32_TryParse_mC928DE2FEC1C35ED5298BDDCA9868076E94B8A21 (String_t* ___0_s, int32_t* ___1_result, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Fbx.FBXProcessor::get_PropertiesCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FBXProcessor_get_PropertiesCount_mD3C7369713E84F01A93BE54947EE62B2FC2C05DE (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* __this, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::ReadToken(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t FBXASCIITokenizer_ReadToken_m308AD080344BD120BDE757662F380A80C8867DC3 (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, bool ___0_required, bool ___1_ignoreSpaces, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.FBXProperties::.ctor(TriLibCore.Fbx.FBXProcessor,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXProperties__ctor_m26D28D238479767B133F249512E1A3BB2EBC5D60 (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ___0_processor, int32_t ___1_propertiesCount, int32_t ___2_basePropertyIndex, int32_t ___3_arrayLength, const RuntimeMethod* method) ;
// System.Int32 System.Runtime.CompilerServices.RuntimeHelpers::get_OffsetToStringData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RuntimeHelpers_get_OffsetToStringData_m90A5D27EF88BE9432BF7093B7D7E7A0ACB0A8FBD (const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemSet(System.Void*,System.Byte,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_MemSet_m4CD74CD43260EB2962A46F57E0D93DD5C332FC2B (void* ___0_destination, uint8_t ___1_value, int64_t ___2_size, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline (int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) ;
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemCpy(System.Void*,System.Void*,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177 (void* ___0_destination, void* ___1_source, int64_t ___2_size, const RuntimeMethod* method) ;
// System.Int32 System.IO.BinaryReader::Read(System.Char[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BinaryReader_Read_mB4A2BC93CA8D038475030D490E9732CD1B8ABDBA (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___0_buffer, int32_t ___1_index, int32_t ___2_count, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Utils.HashUtils::GetHash(System.Collections.Generic.IList`1<System.Char>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t HashUtils_GetHash_mA8618222D626B54CD67FF88D56037B6D8024453E (RuntimeObject* ___0_chars, int32_t ___1_count, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.ASCII.FBXASCIIReader::CopyCharStreamToString(System.String,System.Char[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIIReader_CopyCharStreamToString_m464EF2AAC5CAD52B3343EDB7FC564F32D4DA33A3 (String_t* ___0_s, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___1_cs, int32_t ___2_charCount, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Char>::Clear()
inline void List_1_Clear_m0586B319F89682059DD157C1EDC282A2888ECB9B_inline (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7*, const RuntimeMethod*))List_1_Clear_m0586B319F89682059DD157C1EDC282A2888ECB9B_gshared_inline)(__this, method);
}
// System.Char System.IO.BinaryReader::ReadChar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar BinaryReader_ReadChar_mBFCF423F230C6281515CC26769EBBEC92C18EC6D (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Char>::Add(T)
inline void List_1_Add_mF3227B0AAB9F7FED1883246395F1CEA0D0B06DC5_inline (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* __this, Il2CppChar ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7*, Il2CppChar, const RuntimeMethod*))List_1_Add_mF3227B0AAB9F7FED1883246395F1CEA0D0B06DC5_gshared_inline)(__this, ___0_item, method);
}
// System.String System.String::Concat<System.Char>(System.Collections.Generic.IEnumerable`1<T>)
inline String_t* String_Concat_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_m691C56B3F5A4F992794166066A4B0D10B8B74461 (RuntimeObject* ___0_values, const RuntimeMethod* method)
{
	return ((  String_t* (*) (RuntimeObject*, const RuntimeMethod*))String_Concat_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_m691C56B3F5A4F992794166066A4B0D10B8B74461_gshared)(___0_values, method);
}
// System.String System.String::CreateString(System.Char,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B (String_t* __this, Il2CppChar ___0_c, int32_t ___1_count, const RuntimeMethod* method) ;
// System.Void System.IO.BinaryReader::.ctor(System.IO.Stream,System.Text.Encoding)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryReader__ctor_mD85F293A64917055AA78D504B87E5F7B81E4FD46 (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_input, Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ___1_encoding, const RuntimeMethod* method) ;
// System.Int32 System.IO.BinaryReader::PeekChar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BinaryReader_PeekChar_mA6B7481DCCB2A265E54D1F200ED8BEAD00CD971A (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Fbx.ASCII.FBXASCIITokenizer::SetCharAndAdvance(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIITokenizer_SetCharAndAdvance_m29226B42C00CD73A6A1CB0C706F046AB3729DF4F (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, Il2CppChar ___0_character, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::get_Position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t FBXASCIITokenizer_get_Position_mA20527663B6B3E9A3FB744DAA0FDB910ADD8F5ED (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, const RuntimeMethod* method) ;
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3 (String_t* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Int32 System.BitConverter::SingleToInt32Bits(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitConverter_SingleToInt32Bits_mC760C7CFC89725E3CF68DC45BE3A9A42A7E7DA73_inline (float ___0_value, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Fbx.PropertyAccessorByte::.ctor(TriLibCore.Fbx.FBXProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorByte__ctor_mB6083D373AFC2DDD5CBA40F17171D753DECE1AFD (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___0_properties, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_0 = ___0_properties;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = FBXProperties_GetPropertyArrayLength_mD43333FAA9443D29CC2A86C914E75F7D2DF57EC9(L_0, NULL);
		__this->____count_4 = L_1;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_2 = ___0_properties;
		__this->____properties_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____properties_0), (void*)L_2);
		__this->___U3CIsReadOnlyU3Ek__BackingField_5 = (bool)1;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Byte> TriLibCore.Fbx.PropertyAccessorByte::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorByte_GetEnumerator_m0CEC0EE1DFBA9926652B838388F72EB35D64CD8C (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1__ctor_m13F6A2E033696DC706279AE3A91D02A64206AC4D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1_t507F53FFDEAE6189449897018F3198188410DEB2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyAccessorIEnumerator_1_t507F53FFDEAE6189449897018F3198188410DEB2 L_0;
		memset((&L_0), 0, sizeof(L_0));
		PropertyAccessorIEnumerator_1__ctor_m13F6A2E033696DC706279AE3A91D02A64206AC4D((&L_0), __this, /*hidden argument*/PropertyAccessorIEnumerator_1__ctor_m13F6A2E033696DC706279AE3A91D02A64206AC4D_RuntimeMethod_var);
		PropertyAccessorIEnumerator_1_t507F53FFDEAE6189449897018F3198188410DEB2 L_1 = L_0;
		RuntimeObject* L_2 = Box(PropertyAccessorIEnumerator_1_t507F53FFDEAE6189449897018F3198188410DEB2_il2cpp_TypeInfo_var, &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator TriLibCore.Fbx.PropertyAccessorByte::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorByte_System_Collections_IEnumerable_GetEnumerator_m1F9C2BF570D9AE2771AFF51424BE71C29A4F59EF (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = PropertyAccessorByte_GetEnumerator_m0CEC0EE1DFBA9926652B838388F72EB35D64CD8C(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorByte::Add(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorByte_Add_mAFC49C1DBE7203D3A9DBFAB3ABA5BA0BE13904DE (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, uint8_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorByte_Add_mAFC49C1DBE7203D3A9DBFAB3ABA5BA0BE13904DE_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorByte::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorByte_Clear_m648D5D7C9EC8B28DA5C419E96E28A69BD6E1998C (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorByte_Clear_m648D5D7C9EC8B28DA5C419E96E28A69BD6E1998C_RuntimeMethod_var)));
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorByte::Contains(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorByte_Contains_m6D9A922B2C0C224FD1A8324B3D7328E2A189D6B7 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, uint8_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorByte_Contains_m6D9A922B2C0C224FD1A8324B3D7328E2A189D6B7_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorByte::CopyTo(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorByte_CopyTo_mE7B98D0B7093A33BC42C34A2AFF43AC25D366DE3 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_array, int32_t ___1_arrayIndex, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0004:
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_array;
		int32_t L_1 = ___1_arrayIndex;
		int32_t L_2 = V_0;
		int32_t L_3 = V_0;
		uint8_t L_4;
		L_4 = PropertyAccessorByte_GetElement_mBAF530E2CA25A3BD699AD74B7793BFC14481DCFB(__this, L_3, NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_1, L_2))), (uint8_t)L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0014:
	{
		int32_t L_6 = V_0;
		int32_t L_7;
		L_7 = PropertyAccessorByte_get_Count_m248A77D652A9BF16734BE54219C5F5EAC9D3C360_inline(__this, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorByte::Remove(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorByte_Remove_m7774F101D57F6543F3057BD4BAF6C5A2D7CE6241 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, uint8_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorByte_Remove_m7774F101D57F6543F3057BD4BAF6C5A2D7CE6241_RuntimeMethod_var)));
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorByte::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorByte_get_Count_m248A77D652A9BF16734BE54219C5F5EAC9D3C360 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorByte::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorByte_get_IsReadOnly_m4127380762B968FF48D568BE9240CDBD0FD0721B (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsReadOnlyU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorByte::IndexOf(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorByte_IndexOf_mDEB7FFF72A7C72084150E7E51A98A3B46FA06D10 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, uint8_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorByte_IndexOf_mDEB7FFF72A7C72084150E7E51A98A3B46FA06D10_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorByte::Insert(System.Int32,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorByte_Insert_m6104AD8296D35F593835A492E2707DEDCA0189A0 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, int32_t ___0_index, uint8_t ___1_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorByte_Insert_m6104AD8296D35F593835A492E2707DEDCA0189A0_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorByte::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorByte_RemoveAt_mD003C4326C542792EE62A67D518B44C08EA5DE16 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorByte_RemoveAt_mD003C4326C542792EE62A67D518B44C08EA5DE16_RuntimeMethod_var)));
	}
}
// System.Byte TriLibCore.Fbx.PropertyAccessorByte::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t PropertyAccessorByte_get_Item_m80339FDA2466F488EC14C592F2F0B9CA56DD6A44 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_index;
		uint8_t L_1;
		L_1 = PropertyAccessorByte_GetElement_mBAF530E2CA25A3BD699AD74B7793BFC14481DCFB(__this, L_0, NULL);
		return L_1;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorByte::set_Item(System.Int32,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorByte_set_Item_mA767343463C886938EE75EAFB259B969AF6FE184 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, int32_t ___0_index, uint8_t ___1_value, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorByte_set_Item_mA767343463C886938EE75EAFB259B969AF6FE184_RuntimeMethod_var)));
	}
}
// System.Byte TriLibCore.Fbx.PropertyAccessorByte::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t PropertyAccessorByte_GetElement_mBAF530E2CA25A3BD699AD74B7793BFC14481DCFB (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, int32_t ___0_i, const RuntimeMethod* method) 
{
	FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	{
		int32_t L_0 = ___0_i;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___0_i;
		int32_t L_2;
		L_2 = PropertyAccessorByte_get_Count_m248A77D652A9BF16734BE54219C5F5EAC9D3C360_inline(__this, NULL);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_000f;
		}
	}

IL_000d:
	{
		return (uint8_t)0;
	}

IL_000f:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_3 = __this->____properties_0;
		NullCheck(L_3);
		bool L_4;
		L_4 = FBXProperties_get_IsASCII_m412715B4396F75F7B1127FD4A4B44AC3BC963420(L_3, NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_5 = __this->____properties_0;
		int32_t L_6 = ___0_i;
		NullCheck(L_5);
		uint8_t L_7;
		L_7 = FBXProperties_ASCIIGetByteValue_mEE859B6C1DC74635CE3954D26BF992EEFBB22F72(L_5, L_6, NULL);
		return L_7;
	}

IL_0029:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_8 = __this->____properties_0;
		NullCheck(L_8);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_9;
		L_9 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_8, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_10 = __this->____properties_0;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086** L_11 = (&__this->____decoded_1);
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2** L_12 = (&__this->____decodedMemoryStream_2);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158** L_13 = (&__this->____decodedBinaryReader_3);
		NullCheck(L_9);
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_14;
		L_14 = FBXProcessor_LoadArrayProperty_m97367395AB157AA3F3BA7EAF59C2D24777438FC5(L_9, L_10, L_11, L_12, L_13, (&V_1), (&V_2), NULL);
		V_0 = L_14;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_15 = __this->____properties_0;
		NullCheck(L_15);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_16;
		L_16 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_15, NULL);
		NullCheck(L_16);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_17 = L_16->___ActiveBinaryReader_19;
		NullCheck(L_17);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_18;
		L_18 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_17);
		int32_t L_19 = ___0_i;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_20 = __this->____properties_0;
		NullCheck(L_20);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_21;
		L_21 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_20, NULL);
		NullCheck(L_21);
		int32_t L_22 = L_21->___ActiveSubDataSize_18;
		NullCheck(L_18);
		int64_t L_23;
		L_23 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_18, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_19, L_22))), 1);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_24 = __this->____properties_0;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_25 = __this->____properties_0;
		NullCheck(L_25);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_26;
		L_26 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_25, NULL);
		NullCheck(L_26);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_27 = L_26->___ActiveBinaryReader_19;
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_28 = V_0;
		Il2CppChar L_29 = V_1;
		NullCheck(L_24);
		uint8_t L_30;
		L_30 = FBXProperties_BinaryConvertByteValue_mF77E2540B4A28F588C419E541177CF79D2258E1E(L_24, L_27, L_28, L_29, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_31 = __this->____properties_0;
		NullCheck(L_31);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_32;
		L_32 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_31, NULL);
		NullCheck(L_32);
		FBXProcessor_ReleaseActiveBinaryReader_mC144B788F2FFC4093884077E12EE33B4DEC6E461(L_32, NULL);
		return L_30;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorByte::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorByte_Dispose_mB96B5AB11B6793528A6C257A2698BA2648DB7FF1 (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B2_0 = NULL;
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B1_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B5_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B4_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B8_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B7_0 = NULL;
	{
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_0 = __this->____decoded_1;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000c;
		}
	}
	{
		goto IL_0011;
	}

IL_000c:
	{
		NullCheck(G_B2_0);
		ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2(G_B2_0, ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
	}

IL_0011:
	{
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_2 = __this->____decodedMemoryStream_2;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_3 = L_2;
		G_B4_0 = L_3;
		if (L_3)
		{
			G_B5_0 = L_3;
			goto IL_001d;
		}
	}
	{
		goto IL_0022;
	}

IL_001d:
	{
		NullCheck(G_B5_0);
		Stream_Dispose_mCDB42F32A17541CCA6D3A5906827A401570B07A8(G_B5_0, NULL);
	}

IL_0022:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = __this->____decodedBinaryReader_3;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_5 = L_4;
		G_B7_0 = L_5;
		if (L_5)
		{
			G_B8_0 = L_5;
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		NullCheck(G_B8_0);
		BinaryReader_Dispose_mAFF1A9CE9A73D148270FFA1F896992EB52D36078(G_B8_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Fbx.PropertyAccessorShort::.ctor(TriLibCore.Fbx.FBXProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorShort__ctor_mA56DBCAE811AE75EA8A266C911AD817D3A153EFB (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___0_properties, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_0 = ___0_properties;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = FBXProperties_GetPropertyArrayLength_mD43333FAA9443D29CC2A86C914E75F7D2DF57EC9(L_0, NULL);
		__this->____count_4 = L_1;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_2 = ___0_properties;
		__this->____properties_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____properties_0), (void*)L_2);
		__this->___U3CIsReadOnlyU3Ek__BackingField_5 = (bool)1;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Int16> TriLibCore.Fbx.PropertyAccessorShort::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorShort_GetEnumerator_m9C4184C792895C2EF4BABD954964E416C9FC0DA4 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1__ctor_m10ADEEA2B95198A97E5327E47BFAB5720EA81883_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1_t99D6ABB7ECAA7EDD17341582A28BF20268F5C71D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyAccessorIEnumerator_1_t99D6ABB7ECAA7EDD17341582A28BF20268F5C71D L_0;
		memset((&L_0), 0, sizeof(L_0));
		PropertyAccessorIEnumerator_1__ctor_m10ADEEA2B95198A97E5327E47BFAB5720EA81883((&L_0), __this, /*hidden argument*/PropertyAccessorIEnumerator_1__ctor_m10ADEEA2B95198A97E5327E47BFAB5720EA81883_RuntimeMethod_var);
		PropertyAccessorIEnumerator_1_t99D6ABB7ECAA7EDD17341582A28BF20268F5C71D L_1 = L_0;
		RuntimeObject* L_2 = Box(PropertyAccessorIEnumerator_1_t99D6ABB7ECAA7EDD17341582A28BF20268F5C71D_il2cpp_TypeInfo_var, &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator TriLibCore.Fbx.PropertyAccessorShort::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorShort_System_Collections_IEnumerable_GetEnumerator_m97825D060E965352052DD076B642116F9A17F344 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = PropertyAccessorShort_GetEnumerator_m9C4184C792895C2EF4BABD954964E416C9FC0DA4(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorShort::Add(System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorShort_Add_mB213E015451E9AA91BFACD19CD43522DDFAB8DF5 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int16_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorShort_Add_mB213E015451E9AA91BFACD19CD43522DDFAB8DF5_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorShort::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorShort_Clear_m7879EF8C9C6A06B16EAF6C22BD0518C5C21079C3 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorShort_Clear_m7879EF8C9C6A06B16EAF6C22BD0518C5C21079C3_RuntimeMethod_var)));
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorShort::Contains(System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorShort_Contains_mD53684A5299F2257D5D6F7C1E333FDCD8CAFBF1A (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int16_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorShort_Contains_mD53684A5299F2257D5D6F7C1E333FDCD8CAFBF1A_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorShort::CopyTo(System.Int16[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorShort_CopyTo_m2126F92A594CCAF29A6AC2E10BA8C7F066490D19 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, Int16U5BU5D_t8175CE8DD9C9F9FB0CF4F58E45BC570575B43CFB* ___0_array, int32_t ___1_arrayIndex, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0004:
	{
		Int16U5BU5D_t8175CE8DD9C9F9FB0CF4F58E45BC570575B43CFB* L_0 = ___0_array;
		int32_t L_1 = ___1_arrayIndex;
		int32_t L_2 = V_0;
		int32_t L_3 = V_0;
		int16_t L_4;
		L_4 = PropertyAccessorShort_GetElement_m21FD1368E6B6DEF31B2D03E006DA07E7A6657778(__this, L_3, NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_1, L_2))), (int16_t)L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0014:
	{
		int32_t L_6 = V_0;
		int32_t L_7;
		L_7 = PropertyAccessorShort_get_Count_m7778AE2F440E033D26ECAC2174D79C054CCEDCE3_inline(__this, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorShort::Remove(System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorShort_Remove_m0FE173DE48A774FC2C5717100CA47A72CC0E968C (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int16_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorShort_Remove_m0FE173DE48A774FC2C5717100CA47A72CC0E968C_RuntimeMethod_var)));
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorShort::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorShort_get_Count_m7778AE2F440E033D26ECAC2174D79C054CCEDCE3 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorShort::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorShort_get_IsReadOnly_m6B8831FFD75277EE503547ACD9E6B71417C3EDA6 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsReadOnlyU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorShort::IndexOf(System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorShort_IndexOf_mDC14357ADFD90AC26E6006E2E64BD1AF8C11C6AB (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int16_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorShort_IndexOf_mDC14357ADFD90AC26E6006E2E64BD1AF8C11C6AB_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorShort::Insert(System.Int32,System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorShort_Insert_m7EC96C6A89F2DEB37371B8FFE955F742C6998326 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int32_t ___0_index, int16_t ___1_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorShort_Insert_m7EC96C6A89F2DEB37371B8FFE955F742C6998326_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorShort::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorShort_RemoveAt_m55AA2E6A236358D101272E6CBC782A334533C8AA (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorShort_RemoveAt_m55AA2E6A236358D101272E6CBC782A334533C8AA_RuntimeMethod_var)));
	}
}
// System.Int16 TriLibCore.Fbx.PropertyAccessorShort::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t PropertyAccessorShort_get_Item_m9E20D77EF7BEC76B888373054C2341C0F7471C0C (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_index;
		int16_t L_1;
		L_1 = PropertyAccessorShort_GetElement_m21FD1368E6B6DEF31B2D03E006DA07E7A6657778(__this, L_0, NULL);
		return L_1;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorShort::set_Item(System.Int32,System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorShort_set_Item_mA9E40465930AB75103C53BA6F29209913482988C (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int32_t ___0_index, int16_t ___1_value, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorShort_set_Item_mA9E40465930AB75103C53BA6F29209913482988C_RuntimeMethod_var)));
	}
}
// System.Int16 TriLibCore.Fbx.PropertyAccessorShort::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t PropertyAccessorShort_GetElement_m21FD1368E6B6DEF31B2D03E006DA07E7A6657778 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, int32_t ___0_i, const RuntimeMethod* method) 
{
	FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	{
		int32_t L_0 = ___0_i;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___0_i;
		int32_t L_2;
		L_2 = PropertyAccessorShort_get_Count_m7778AE2F440E033D26ECAC2174D79C054CCEDCE3_inline(__this, NULL);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_000f;
		}
	}

IL_000d:
	{
		return (int16_t)0;
	}

IL_000f:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_3 = __this->____properties_0;
		NullCheck(L_3);
		bool L_4;
		L_4 = FBXProperties_get_IsASCII_m412715B4396F75F7B1127FD4A4B44AC3BC963420(L_3, NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_5 = __this->____properties_0;
		int32_t L_6 = ___0_i;
		NullCheck(L_5);
		int16_t L_7;
		L_7 = FBXProperties_ASCIIGetShortValue_m51DDC9F9C39A3A7DC82D1C1482928EE416CD9B45(L_5, L_6, NULL);
		return L_7;
	}

IL_0029:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_8 = __this->____properties_0;
		NullCheck(L_8);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_9;
		L_9 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_8, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_10 = __this->____properties_0;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086** L_11 = (&__this->____decoded_1);
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2** L_12 = (&__this->____decodedMemoryStream_2);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158** L_13 = (&__this->____decodedBinaryReader_3);
		NullCheck(L_9);
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_14;
		L_14 = FBXProcessor_LoadArrayProperty_m97367395AB157AA3F3BA7EAF59C2D24777438FC5(L_9, L_10, L_11, L_12, L_13, (&V_1), (&V_2), NULL);
		V_0 = L_14;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_15 = __this->____properties_0;
		NullCheck(L_15);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_16;
		L_16 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_15, NULL);
		NullCheck(L_16);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_17 = L_16->___ActiveBinaryReader_19;
		NullCheck(L_17);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_18;
		L_18 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_17);
		int32_t L_19 = ___0_i;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_20 = __this->____properties_0;
		NullCheck(L_20);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_21;
		L_21 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_20, NULL);
		NullCheck(L_21);
		int32_t L_22 = L_21->___ActiveSubDataSize_18;
		NullCheck(L_18);
		int64_t L_23;
		L_23 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_18, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_19, L_22))), 1);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_24 = __this->____properties_0;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_25 = __this->____properties_0;
		NullCheck(L_25);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_26;
		L_26 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_25, NULL);
		NullCheck(L_26);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_27 = L_26->___ActiveBinaryReader_19;
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_28 = V_0;
		Il2CppChar L_29 = V_1;
		NullCheck(L_24);
		int16_t L_30;
		L_30 = FBXProperties_BinaryConvertShortValue_mF594EF256C56FFC7C4E1107570F0156484A66A29(L_24, L_27, L_28, L_29, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_31 = __this->____properties_0;
		NullCheck(L_31);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_32;
		L_32 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_31, NULL);
		NullCheck(L_32);
		FBXProcessor_ReleaseActiveBinaryReader_mC144B788F2FFC4093884077E12EE33B4DEC6E461(L_32, NULL);
		return L_30;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorShort::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorShort_Dispose_m49C5E5D76AE0EFF271807A619D7BF8445658E4B1 (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B2_0 = NULL;
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B1_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B5_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B4_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B8_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B7_0 = NULL;
	{
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_0 = __this->____decoded_1;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000c;
		}
	}
	{
		goto IL_0011;
	}

IL_000c:
	{
		NullCheck(G_B2_0);
		ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2(G_B2_0, ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
	}

IL_0011:
	{
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_2 = __this->____decodedMemoryStream_2;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_3 = L_2;
		G_B4_0 = L_3;
		if (L_3)
		{
			G_B5_0 = L_3;
			goto IL_001d;
		}
	}
	{
		goto IL_0022;
	}

IL_001d:
	{
		NullCheck(G_B5_0);
		Stream_Dispose_mCDB42F32A17541CCA6D3A5906827A401570B07A8(G_B5_0, NULL);
	}

IL_0022:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = __this->____decodedBinaryReader_3;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_5 = L_4;
		G_B7_0 = L_5;
		if (L_5)
		{
			G_B8_0 = L_5;
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		NullCheck(G_B8_0);
		BinaryReader_Dispose_mAFF1A9CE9A73D148270FFA1F896992EB52D36078(G_B8_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] TriLibCore.Fbx.PropertyAccessorIEE754::GetBytesInt(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* PropertyAccessorIEE754_GetBytesInt_m1809276B604FF35FE75043EF51F3B2E063952CEB (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, int32_t ___0_value, int32_t ___1_offset, const RuntimeMethod* method) 
{
	uint8_t* V_0 = NULL;
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = __this->____bytesInt_4;
		int32_t L_1 = ___1_offset;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2 = __this->____bytesInt_4;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = Mathf_Clamp_m4DC36EEFDBE5F07C16249DA568023C5ECCFF0E7B_inline(L_1, 0, ((int32_t)(((RuntimeArray*)L_2)->max_length)), NULL);
		___1_offset = L_3;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_4 = L_0;
		int32_t L_5 = ___1_offset;
		NullCheck(L_4);
		V_0 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)));
		uint8_t* L_6 = V_0;
		int32_t L_7 = ___0_value;
		*((int32_t*)((uintptr_t)L_6)) = (int32_t)L_7;
		V_0 = (uint8_t*)((uintptr_t)0);
		return L_4;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorIEE754::.ctor(TriLibCore.Fbx.FBXProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEE754__ctor_m194700C45AB9DF1C4FB3E4225C8D97781375FA76 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___0_properties, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_0 = ___0_properties;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = FBXProperties_GetPropertyArrayLength_mD43333FAA9443D29CC2A86C914E75F7D2DF57EC9(L_0, NULL);
		__this->____count_5 = L_1;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_2 = ___0_properties;
		__this->____properties_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____properties_0), (void*)L_2);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)4);
		__this->____bytesInt_4 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____bytesInt_4), (void*)L_3);
		__this->___U3CIsReadOnlyU3Ek__BackingField_6 = (bool)1;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> TriLibCore.Fbx.PropertyAccessorIEE754::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorIEE754_GetEnumerator_mA6FECF69D50702B86D10F00937E845DB91DE1171 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D L_0;
		memset((&L_0), 0, sizeof(L_0));
		PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC((&L_0), __this, /*hidden argument*/PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC_RuntimeMethod_var);
		PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D L_1 = L_0;
		RuntimeObject* L_2 = Box(PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D_il2cpp_TypeInfo_var, &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator TriLibCore.Fbx.PropertyAccessorIEE754::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorIEE754_System_Collections_IEnumerable_GetEnumerator_mF45E5F4B40F28B53C541CD941DC7B42452D19EE2 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = PropertyAccessorIEE754_GetEnumerator_mA6FECF69D50702B86D10F00937E845DB91DE1171(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorIEE754::Add(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEE754_Add_mBD9C6FB19DD5EE7A9054A642FE6C17AF7D11226D (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, float ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorIEE754_Add_mBD9C6FB19DD5EE7A9054A642FE6C17AF7D11226D_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorIEE754::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEE754_Clear_mA0A2150C21DFCC7E74C4A68D00CE206A20F48433 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorIEE754_Clear_mA0A2150C21DFCC7E74C4A68D00CE206A20F48433_RuntimeMethod_var)));
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorIEE754::Contains(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorIEE754_Contains_m64AC75313103A83FA655C79D6D1643789CC85426 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, float ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorIEE754_Contains_m64AC75313103A83FA655C79D6D1643789CC85426_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorIEE754::CopyTo(System.Single[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEE754_CopyTo_m271FAC18BF3DB75E313D829A2E96AD7CD9C9BA17 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___0_array, int32_t ___1_arrayIndex, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0004:
	{
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_0 = ___0_array;
		int32_t L_1 = ___1_arrayIndex;
		int32_t L_2 = V_0;
		int32_t L_3 = V_0;
		float L_4;
		L_4 = PropertyAccessorIEE754_GetElement_mAEDF6D2F614A73FF653B089BD2FD07F4D809D893(__this, L_3, NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_1, L_2))), (float)L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0014:
	{
		int32_t L_6 = V_0;
		int32_t L_7;
		L_7 = PropertyAccessorIEE754_get_Count_m3DBE759D8CC11737529D4AA5739849E4AEE4A54C_inline(__this, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorIEE754::Remove(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorIEE754_Remove_mF9938772EEFE6996F36F4AD6E2CB22DDD0357F76 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, float ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorIEE754_Remove_mF9938772EEFE6996F36F4AD6E2CB22DDD0357F76_RuntimeMethod_var)));
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorIEE754::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorIEE754_get_Count_m3DBE759D8CC11737529D4AA5739849E4AEE4A54C (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_5;
		return L_0;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorIEE754::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorIEE754_get_IsReadOnly_m1DE49AB9D1CC5778DAAE8DE5D097618279D1F7DB (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsReadOnlyU3Ek__BackingField_6;
		return L_0;
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorIEE754::IndexOf(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorIEE754_IndexOf_m59FB427A9B4BC4FF9B3DF2AA2B14BF5BFF16E552 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, float ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorIEE754_IndexOf_m59FB427A9B4BC4FF9B3DF2AA2B14BF5BFF16E552_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorIEE754::Insert(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEE754_Insert_m46AA71328F9D5F4E36474318EC72B32FAC57130C (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, int32_t ___0_index, float ___1_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorIEE754_Insert_m46AA71328F9D5F4E36474318EC72B32FAC57130C_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorIEE754::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEE754_RemoveAt_mDC0DC08B153E2A2F2954ECB13281376EA30E2D91 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorIEE754_RemoveAt_mDC0DC08B153E2A2F2954ECB13281376EA30E2D91_RuntimeMethod_var)));
	}
}
// System.Single TriLibCore.Fbx.PropertyAccessorIEE754::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PropertyAccessorIEE754_get_Item_m909582579E2C29416B48FF985E00677C55C2D8C6 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_index;
		float L_1;
		L_1 = PropertyAccessorIEE754_GetElement_mAEDF6D2F614A73FF653B089BD2FD07F4D809D893(__this, L_0, NULL);
		return L_1;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorIEE754::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEE754_set_Item_m14570F3B3A020CD7D15485930032DC6C9F55D687 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, int32_t ___0_index, float ___1_value, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorIEE754_set_Item_m14570F3B3A020CD7D15485930032DC6C9F55D687_RuntimeMethod_var)));
	}
}
// System.Single TriLibCore.Fbx.PropertyAccessorIEE754::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PropertyAccessorIEE754_GetElement_mAEDF6D2F614A73FF653B089BD2FD07F4D809D893 (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, int32_t ___0_i, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	Il2CppChar V_2 = 0x0;
	bool V_3 = false;
	{
		int32_t L_0 = ___0_i;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___0_i;
		int32_t L_2;
		L_2 = PropertyAccessorIEE754_get_Count_m3DBE759D8CC11737529D4AA5739849E4AEE4A54C_inline(__this, NULL);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0013;
		}
	}

IL_000d:
	{
		return (0.0f);
	}

IL_0013:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_3 = __this->____properties_0;
		NullCheck(L_3);
		bool L_4;
		L_4 = FBXProperties_get_IsASCII_m412715B4396F75F7B1127FD4A4B44AC3BC963420(L_3, NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_5 = __this->____properties_0;
		int32_t L_6 = ___0_i;
		NullCheck(L_5);
		int32_t L_7;
		L_7 = FBXProperties_ASCIIGetIntValue_mD5B6A00346736642E5944098176C6A9715E60592(L_5, L_6, NULL);
		V_0 = L_7;
		goto IL_00b4;
	}

IL_0032:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_8 = __this->____properties_0;
		NullCheck(L_8);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_9;
		L_9 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_8, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_10 = __this->____properties_0;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086** L_11 = (&__this->____decoded_1);
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2** L_12 = (&__this->____decodedMemoryStream_2);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158** L_13 = (&__this->____decodedBinaryReader_3);
		NullCheck(L_9);
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_14;
		L_14 = FBXProcessor_LoadArrayProperty_m97367395AB157AA3F3BA7EAF59C2D24777438FC5(L_9, L_10, L_11, L_12, L_13, (&V_2), (&V_3), NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_15 = __this->____properties_0;
		NullCheck(L_15);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_16;
		L_16 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_15, NULL);
		NullCheck(L_16);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_17 = L_16->___ActiveBinaryReader_19;
		NullCheck(L_17);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_18;
		L_18 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_17);
		int32_t L_19 = ___0_i;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_20 = __this->____properties_0;
		NullCheck(L_20);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_21;
		L_21 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_20, NULL);
		NullCheck(L_21);
		int32_t L_22 = L_21->___ActiveSubDataSize_18;
		NullCheck(L_18);
		int64_t L_23;
		L_23 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_18, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_19, L_22))), 1);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_24 = __this->____properties_0;
		NullCheck(L_24);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_25;
		L_25 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_24, NULL);
		NullCheck(L_25);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_26 = L_25->___ActiveBinaryReader_19;
		NullCheck(L_26);
		int32_t L_27;
		L_27 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_26);
		V_0 = L_27;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_28 = __this->____properties_0;
		NullCheck(L_28);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_29;
		L_29 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_28, NULL);
		NullCheck(L_29);
		FBXProcessor_ReleaseActiveBinaryReader_mC144B788F2FFC4093884077E12EE33B4DEC6E461(L_29, NULL);
	}

IL_00b4:
	{
		float L_30 = *((float*)((uintptr_t)(&V_0)));
		V_1 = L_30;
		float L_31 = V_1;
		bool L_32;
		L_32 = Single_IsNaN_mFE637F6ECA9F7697CE8EFF56427858F4C5EDF75D_inline(L_31, NULL);
		if (!L_32)
		{
			goto IL_00c7;
		}
	}
	{
		V_1 = (0.0f);
	}

IL_00c7:
	{
		float L_33 = V_1;
		return L_33;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorIEE754::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorIEE754_Dispose_mD56F795D4D0F36A477B36B6F0FC247DBBE65108F (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B2_0 = NULL;
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B1_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B5_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B4_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B8_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B7_0 = NULL;
	{
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_0 = __this->____decoded_1;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000c;
		}
	}
	{
		goto IL_0011;
	}

IL_000c:
	{
		NullCheck(G_B2_0);
		ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2(G_B2_0, ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
	}

IL_0011:
	{
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_2 = __this->____decodedMemoryStream_2;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_3 = L_2;
		G_B4_0 = L_3;
		if (L_3)
		{
			G_B5_0 = L_3;
			goto IL_001d;
		}
	}
	{
		goto IL_0022;
	}

IL_001d:
	{
		NullCheck(G_B5_0);
		Stream_Dispose_mCDB42F32A17541CCA6D3A5906827A401570B07A8(G_B5_0, NULL);
	}

IL_0022:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = __this->____decodedBinaryReader_3;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_5 = L_4;
		G_B7_0 = L_5;
		if (L_5)
		{
			G_B8_0 = L_5;
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		NullCheck(G_B8_0);
		BinaryReader_Dispose_mAFF1A9CE9A73D148270FFA1F896992EB52D36078(G_B8_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Fbx.PropertyAccessorInt::.ctor(TriLibCore.Fbx.FBXProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorInt__ctor_m7639D3DA555591A505C37F34802397E0DAD858D7 (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___0_properties, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_0 = ___0_properties;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = FBXProperties_GetPropertyArrayLength_mD43333FAA9443D29CC2A86C914E75F7D2DF57EC9(L_0, NULL);
		__this->____count_4 = L_1;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_2 = ___0_properties;
		__this->____properties_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____properties_0), (void*)L_2);
		__this->___U3CIsReadOnlyU3Ek__BackingField_5 = (bool)1;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Int32> TriLibCore.Fbx.PropertyAccessorInt::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorInt_GetEnumerator_mBB1428C9E22A3A29F1D9ECFCA21FD6F77301720F (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1__ctor_m698626BC437191FDC212DB020833A50A9A35807A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1_tC80F58CC2A95871A7E8891567EEE4DAAC636E0A6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyAccessorIEnumerator_1_tC80F58CC2A95871A7E8891567EEE4DAAC636E0A6 L_0;
		memset((&L_0), 0, sizeof(L_0));
		PropertyAccessorIEnumerator_1__ctor_m698626BC437191FDC212DB020833A50A9A35807A((&L_0), __this, /*hidden argument*/PropertyAccessorIEnumerator_1__ctor_m698626BC437191FDC212DB020833A50A9A35807A_RuntimeMethod_var);
		PropertyAccessorIEnumerator_1_tC80F58CC2A95871A7E8891567EEE4DAAC636E0A6 L_1 = L_0;
		RuntimeObject* L_2 = Box(PropertyAccessorIEnumerator_1_tC80F58CC2A95871A7E8891567EEE4DAAC636E0A6_il2cpp_TypeInfo_var, &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator TriLibCore.Fbx.PropertyAccessorInt::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorInt_System_Collections_IEnumerable_GetEnumerator_m0DE7CE0BEF3548208B36D570524D7E01E2BD7E11 (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = PropertyAccessorInt_GetEnumerator_mBB1428C9E22A3A29F1D9ECFCA21FD6F77301720F(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorInt::Add(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorInt_Add_m1C14F9F8818E6843CC0DD32437D07C3394B6F4E7 (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorInt_Add_m1C14F9F8818E6843CC0DD32437D07C3394B6F4E7_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorInt::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorInt_Clear_m696554C189B6C8E1769E5967C00C121F2D127D11 (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorInt_Clear_m696554C189B6C8E1769E5967C00C121F2D127D11_RuntimeMethod_var)));
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorInt::Contains(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorInt_Contains_m0212669CC333E58F25DC8371F45DEB094EE8170E (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorInt_Contains_m0212669CC333E58F25DC8371F45DEB094EE8170E_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorInt::CopyTo(System.Int32[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorInt_CopyTo_mD5BA2DF764C9E239F1B72451A1F75930994E2DAC (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___0_array, int32_t ___1_arrayIndex, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0004:
	{
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_0 = ___0_array;
		int32_t L_1 = ___1_arrayIndex;
		int32_t L_2 = V_0;
		int32_t L_3 = V_0;
		int32_t L_4;
		L_4 = PropertyAccessorInt_GetElement_m601D35A0D8860914D136AE3CD434BE51E404C7FB(__this, L_3, NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_1, L_2))), (int32_t)L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0014:
	{
		int32_t L_6 = V_0;
		int32_t L_7;
		L_7 = PropertyAccessorInt_get_Count_m0C89ED9BBF123AF37C9647E9D08743150C453D88_inline(__this, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorInt::Remove(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorInt_Remove_mD200B41DF3E14BDBF1AA25CB049E348300AA2DE5 (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorInt_Remove_mD200B41DF3E14BDBF1AA25CB049E348300AA2DE5_RuntimeMethod_var)));
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorInt::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorInt_get_Count_m0C89ED9BBF123AF37C9647E9D08743150C453D88 (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorInt::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorInt_get_IsReadOnly_m20707F1D40D5580A816E2D8009E2CF245AAE7C3D (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsReadOnlyU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorInt::IndexOf(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorInt_IndexOf_m4BD1C6F598845DC91FBB46DA5E391E2272BF946D (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorInt_IndexOf_m4BD1C6F598845DC91FBB46DA5E391E2272BF946D_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorInt::Insert(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorInt_Insert_mBD22A1188780CA0DA1B4F63F6113CB9091E6E14C (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_index, int32_t ___1_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorInt_Insert_mBD22A1188780CA0DA1B4F63F6113CB9091E6E14C_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorInt::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorInt_RemoveAt_m5C75A6097020E302ADB4B78EB3321412B7EAAE00 (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorInt_RemoveAt_m5C75A6097020E302ADB4B78EB3321412B7EAAE00_RuntimeMethod_var)));
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorInt::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorInt_get_Item_mD8E6C79896E17011626DD51BC640FB81A8752C48 (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_index;
		int32_t L_1;
		L_1 = PropertyAccessorInt_GetElement_m601D35A0D8860914D136AE3CD434BE51E404C7FB(__this, L_0, NULL);
		return L_1;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorInt::set_Item(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorInt_set_Item_mE396544801031052CB78036FA969BBFB4DC019DD (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_index, int32_t ___1_value, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorInt_set_Item_mE396544801031052CB78036FA969BBFB4DC019DD_RuntimeMethod_var)));
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorInt::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorInt_GetElement_m601D35A0D8860914D136AE3CD434BE51E404C7FB (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, int32_t ___0_i, const RuntimeMethod* method) 
{
	FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	{
		int32_t L_0 = ___0_i;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___0_i;
		int32_t L_2;
		L_2 = PropertyAccessorInt_get_Count_m0C89ED9BBF123AF37C9647E9D08743150C453D88_inline(__this, NULL);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_000f;
		}
	}

IL_000d:
	{
		return 0;
	}

IL_000f:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_3 = __this->____properties_0;
		NullCheck(L_3);
		bool L_4;
		L_4 = FBXProperties_get_IsASCII_m412715B4396F75F7B1127FD4A4B44AC3BC963420(L_3, NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_5 = __this->____properties_0;
		int32_t L_6 = ___0_i;
		NullCheck(L_5);
		int32_t L_7;
		L_7 = FBXProperties_ASCIIGetIntValue_mD5B6A00346736642E5944098176C6A9715E60592(L_5, L_6, NULL);
		return L_7;
	}

IL_0029:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_8 = __this->____properties_0;
		NullCheck(L_8);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_9;
		L_9 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_8, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_10 = __this->____properties_0;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086** L_11 = (&__this->____decoded_1);
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2** L_12 = (&__this->____decodedMemoryStream_2);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158** L_13 = (&__this->____decodedBinaryReader_3);
		NullCheck(L_9);
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_14;
		L_14 = FBXProcessor_LoadArrayProperty_m97367395AB157AA3F3BA7EAF59C2D24777438FC5(L_9, L_10, L_11, L_12, L_13, (&V_1), (&V_2), NULL);
		V_0 = L_14;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_15 = __this->____properties_0;
		NullCheck(L_15);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_16;
		L_16 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_15, NULL);
		NullCheck(L_16);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_17 = L_16->___ActiveBinaryReader_19;
		NullCheck(L_17);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_18;
		L_18 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_17);
		int32_t L_19 = ___0_i;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_20 = __this->____properties_0;
		NullCheck(L_20);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_21;
		L_21 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_20, NULL);
		NullCheck(L_21);
		int32_t L_22 = L_21->___ActiveSubDataSize_18;
		NullCheck(L_18);
		int64_t L_23;
		L_23 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_18, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_19, L_22))), 1);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_24 = __this->____properties_0;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_25 = __this->____properties_0;
		NullCheck(L_25);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_26;
		L_26 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_25, NULL);
		NullCheck(L_26);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_27 = L_26->___ActiveBinaryReader_19;
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_28 = V_0;
		Il2CppChar L_29 = V_1;
		NullCheck(L_24);
		int32_t L_30;
		L_30 = FBXProperties_BinaryConvertIntValue_mEB71EF9DBBBA4E9DBFDAEC2A6A90C9FDCEE73CFA(L_24, L_27, L_28, L_29, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_31 = __this->____properties_0;
		NullCheck(L_31);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_32;
		L_32 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_31, NULL);
		NullCheck(L_32);
		FBXProcessor_ReleaseActiveBinaryReader_mC144B788F2FFC4093884077E12EE33B4DEC6E461(L_32, NULL);
		return L_30;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorInt::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorInt_Dispose_mC83D3B4B3BFEE657B9D50F673FF7856F590E204F (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B2_0 = NULL;
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B1_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B5_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B4_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B8_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B7_0 = NULL;
	{
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_0 = __this->____decoded_1;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000c;
		}
	}
	{
		goto IL_0011;
	}

IL_000c:
	{
		NullCheck(G_B2_0);
		ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2(G_B2_0, ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
	}

IL_0011:
	{
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_2 = __this->____decodedMemoryStream_2;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_3 = L_2;
		G_B4_0 = L_3;
		if (L_3)
		{
			G_B5_0 = L_3;
			goto IL_001d;
		}
	}
	{
		goto IL_0022;
	}

IL_001d:
	{
		NullCheck(G_B5_0);
		Stream_Dispose_mCDB42F32A17541CCA6D3A5906827A401570B07A8(G_B5_0, NULL);
	}

IL_0022:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = __this->____decodedBinaryReader_3;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_5 = L_4;
		G_B7_0 = L_5;
		if (L_5)
		{
			G_B8_0 = L_5;
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		NullCheck(G_B8_0);
		BinaryReader_Dispose_mAFF1A9CE9A73D148270FFA1F896992EB52D36078(G_B8_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Fbx.PropertyAccessorLong::.ctor(TriLibCore.Fbx.FBXProperties,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorLong__ctor_m482EB037759E9CDDCC5BD3EDDE7AB2FB2A1B7458 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___0_properties, bool ___1_isTime, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_0 = ___0_properties;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = FBXProperties_GetPropertyArrayLength_mD43333FAA9443D29CC2A86C914E75F7D2DF57EC9(L_0, NULL);
		__this->____count_5 = L_1;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_2 = ___0_properties;
		__this->____properties_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____properties_0), (void*)L_2);
		bool L_3 = ___1_isTime;
		__this->____isTime_4 = L_3;
		__this->___U3CIsReadOnlyU3Ek__BackingField_6 = (bool)1;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Int64> TriLibCore.Fbx.PropertyAccessorLong::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorLong_GetEnumerator_m12B3301EE1B107A05C091AB0E1CC76E0C3804458 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1__ctor_m52017FEBE78CFF4A3918E710C90AFBB0AE6B434F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1_t81284489B090B3086F8560CFF14FCFA37CD50F96_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyAccessorIEnumerator_1_t81284489B090B3086F8560CFF14FCFA37CD50F96 L_0;
		memset((&L_0), 0, sizeof(L_0));
		PropertyAccessorIEnumerator_1__ctor_m52017FEBE78CFF4A3918E710C90AFBB0AE6B434F((&L_0), __this, /*hidden argument*/PropertyAccessorIEnumerator_1__ctor_m52017FEBE78CFF4A3918E710C90AFBB0AE6B434F_RuntimeMethod_var);
		PropertyAccessorIEnumerator_1_t81284489B090B3086F8560CFF14FCFA37CD50F96 L_1 = L_0;
		RuntimeObject* L_2 = Box(PropertyAccessorIEnumerator_1_t81284489B090B3086F8560CFF14FCFA37CD50F96_il2cpp_TypeInfo_var, &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator TriLibCore.Fbx.PropertyAccessorLong::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorLong_System_Collections_IEnumerable_GetEnumerator_m3DE414DE6F36E9C0F1266E201FEEE86258C8CAE9 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = PropertyAccessorLong_GetEnumerator_m12B3301EE1B107A05C091AB0E1CC76E0C3804458(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorLong::Add(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorLong_Add_m5E36E9519465EFB1B6C70C0D6BFF6B18C365714D (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int64_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorLong_Add_m5E36E9519465EFB1B6C70C0D6BFF6B18C365714D_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorLong::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorLong_Clear_m5CDEF0C6CE3BFDB9815CB8CC76D19BFE5E7D531E (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorLong_Clear_m5CDEF0C6CE3BFDB9815CB8CC76D19BFE5E7D531E_RuntimeMethod_var)));
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorLong::Contains(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorLong_Contains_mA2EF2BA51E321E94EC45365F3C3FAB3EE6E9C378 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int64_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorLong_Contains_mA2EF2BA51E321E94EC45365F3C3FAB3EE6E9C378_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorLong::CopyTo(System.Int64[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorLong_CopyTo_m35BFF45BC5FDA872D5DC851642A4879B3D36E88D (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, Int64U5BU5D_tAEDFCBDB5414E2A140A6F34C0538BF97FCF67A1D* ___0_array, int32_t ___1_arrayIndex, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0004:
	{
		Int64U5BU5D_tAEDFCBDB5414E2A140A6F34C0538BF97FCF67A1D* L_0 = ___0_array;
		int32_t L_1 = ___1_arrayIndex;
		int32_t L_2 = V_0;
		int32_t L_3 = V_0;
		int64_t L_4;
		L_4 = PropertyAccessorLong_GetElement_m296C57EACB6F02ABEB4471B17EB24503791DE6D0(__this, L_3, NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_1, L_2))), (int64_t)L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0014:
	{
		int32_t L_6 = V_0;
		int32_t L_7;
		L_7 = PropertyAccessorLong_get_Count_m4848D70EC610CBA6E351059DFA38F3D565B89DF9_inline(__this, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorLong::Remove(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorLong_Remove_m2023A6FD815BDF767964553127FCBC4CF8DA1851 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int64_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorLong_Remove_m2023A6FD815BDF767964553127FCBC4CF8DA1851_RuntimeMethod_var)));
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorLong::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorLong_get_Count_m4848D70EC610CBA6E351059DFA38F3D565B89DF9 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_5;
		return L_0;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorLong::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorLong_get_IsReadOnly_mB47A8FC70ACC61267C10345748AD9C8FEB1795F4 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsReadOnlyU3Ek__BackingField_6;
		return L_0;
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorLong::IndexOf(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorLong_IndexOf_m049151611E62CCC45723DE7E25636EDDA7397F19 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int64_t ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorLong_IndexOf_m049151611E62CCC45723DE7E25636EDDA7397F19_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorLong::Insert(System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorLong_Insert_m6E624085B34F1BE033200EEF0134ACD783519358 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int32_t ___0_index, int64_t ___1_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorLong_Insert_m6E624085B34F1BE033200EEF0134ACD783519358_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorLong::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorLong_RemoveAt_mE5EC5A508E67591F480F9FFE2AB04D55B1431F78 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorLong_RemoveAt_mE5EC5A508E67591F480F9FFE2AB04D55B1431F78_RuntimeMethod_var)));
	}
}
// System.Int64 TriLibCore.Fbx.PropertyAccessorLong::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t PropertyAccessorLong_get_Item_m53A56EF50B7D0D0581BA0762A7630F6B4A517A68 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_index;
		int64_t L_1;
		L_1 = PropertyAccessorLong_GetElement_m296C57EACB6F02ABEB4471B17EB24503791DE6D0(__this, L_0, NULL);
		return L_1;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorLong::set_Item(System.Int32,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorLong_set_Item_m4A792FFF42650916E4AB115B9EC9830A0F8ABE11 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int32_t ___0_index, int64_t ___1_value, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorLong_set_Item_m4A792FFF42650916E4AB115B9EC9830A0F8ABE11_RuntimeMethod_var)));
	}
}
// System.Int64 TriLibCore.Fbx.PropertyAccessorLong::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t PropertyAccessorLong_GetElement_m296C57EACB6F02ABEB4471B17EB24503791DE6D0 (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, int32_t ___0_i, const RuntimeMethod* method) 
{
	int64_t V_0 = 0;
	float V_1 = 0.0f;
	FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Il2CppChar V_3 = 0x0;
	bool V_4 = false;
	int64_t V_5 = 0;
	float V_6 = 0.0f;
	{
		int32_t L_0 = ___0_i;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___0_i;
		int32_t L_2;
		L_2 = PropertyAccessorLong_get_Count_m4848D70EC610CBA6E351059DFA38F3D565B89DF9_inline(__this, NULL);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0010;
		}
	}

IL_000d:
	{
		return ((int64_t)0);
	}

IL_0010:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_3 = __this->____properties_0;
		NullCheck(L_3);
		bool L_4;
		L_4 = FBXProperties_get_IsASCII_m412715B4396F75F7B1127FD4A4B44AC3BC963420(L_3, NULL);
		if (!L_4)
		{
			goto IL_0099;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_5 = __this->____properties_0;
		int32_t L_6 = ___0_i;
		NullCheck(L_5);
		int64_t L_7;
		L_7 = FBXProperties_ASCIIGetLongValue_m7AAAE670097E8A2E4D8CC45E10D2FB8825731B15(L_5, L_6, NULL);
		V_0 = L_7;
		bool L_8 = __this->____isTime_4;
		if (!L_8)
		{
			goto IL_0097;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_9 = __this->____properties_0;
		NullCheck(L_9);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_10;
		L_10 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_9, NULL);
		NullCheck(L_10);
		FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* L_11 = L_10->___Document_13;
		int64_t L_12 = V_0;
		NullCheck(L_11);
		float L_13;
		L_13 = FBXDocument_ConvertFromFBXTime_mCA9983CE579F4C635D9C0E7D61A5E2094DACD9BE(L_11, L_12, NULL);
		V_1 = L_13;
		float L_14 = V_1;
		if ((!(((float)L_14) > ((float)(100000.0f)))))
		{
			goto IL_0070;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_15 = __this->____properties_0;
		NullCheck(L_15);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_16;
		L_16 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_15, NULL);
		NullCheck(L_16);
		FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* L_17 = L_16->___Document_13;
		NullCheck(L_17);
		int64_t L_18;
		L_18 = FBXDocument_ConvertToFBXTime_m523702DB9568E55B1493A765B9641FFE84AF6CF8(L_17, (100000.0), NULL);
		return L_18;
	}

IL_0070:
	{
		float L_19 = V_1;
		if ((!(((float)L_19) < ((float)(-100000.0f)))))
		{
			goto IL_0097;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_20 = __this->____properties_0;
		NullCheck(L_20);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_21;
		L_21 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_20, NULL);
		NullCheck(L_21);
		FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* L_22 = L_21->___Document_13;
		NullCheck(L_22);
		int64_t L_23;
		L_23 = FBXDocument_ConvertToFBXTime_m523702DB9568E55B1493A765B9641FFE84AF6CF8(L_22, (-100000.0), NULL);
		return L_23;
	}

IL_0097:
	{
		int64_t L_24 = V_0;
		return L_24;
	}

IL_0099:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_25 = __this->____properties_0;
		NullCheck(L_25);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_26;
		L_26 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_25, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_27 = __this->____properties_0;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086** L_28 = (&__this->____decoded_1);
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2** L_29 = (&__this->____decodedMemoryStream_2);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158** L_30 = (&__this->____decodedBinaryReader_3);
		NullCheck(L_26);
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_31;
		L_31 = FBXProcessor_LoadArrayProperty_m97367395AB157AA3F3BA7EAF59C2D24777438FC5(L_26, L_27, L_28, L_29, L_30, (&V_3), (&V_4), NULL);
		V_2 = L_31;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_32 = __this->____properties_0;
		NullCheck(L_32);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_33;
		L_33 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_32, NULL);
		NullCheck(L_33);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_34 = L_33->___ActiveBinaryReader_19;
		NullCheck(L_34);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_35;
		L_35 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_34);
		int32_t L_36 = ___0_i;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_37 = __this->____properties_0;
		NullCheck(L_37);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_38;
		L_38 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_37, NULL);
		NullCheck(L_38);
		int32_t L_39 = L_38->___ActiveSubDataSize_18;
		NullCheck(L_35);
		int64_t L_40;
		L_40 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_35, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_36, L_39))), 1);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_41 = __this->____properties_0;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_42 = __this->____properties_0;
		NullCheck(L_42);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_43;
		L_43 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_42, NULL);
		NullCheck(L_43);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_44 = L_43->___ActiveBinaryReader_19;
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_45 = V_2;
		Il2CppChar L_46 = V_3;
		NullCheck(L_41);
		int64_t L_47;
		L_47 = FBXProperties_BinaryConvertLongValue_mB90FC3F21C16EC402D0C7D428417106E66E65BF3(L_41, L_44, L_45, L_46, NULL);
		V_5 = L_47;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_48 = __this->____properties_0;
		NullCheck(L_48);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_49;
		L_49 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_48, NULL);
		NullCheck(L_49);
		FBXProcessor_ReleaseActiveBinaryReader_mC144B788F2FFC4093884077E12EE33B4DEC6E461(L_49, NULL);
		bool L_50 = __this->____isTime_4;
		if (!L_50)
		{
			goto IL_0195;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_51 = __this->____properties_0;
		NullCheck(L_51);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_52;
		L_52 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_51, NULL);
		NullCheck(L_52);
		FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* L_53 = L_52->___Document_13;
		int64_t L_54 = V_5;
		NullCheck(L_53);
		float L_55;
		L_55 = FBXDocument_ConvertFromFBXTime_mCA9983CE579F4C635D9C0E7D61A5E2094DACD9BE(L_53, L_54, NULL);
		V_6 = L_55;
		float L_56 = V_6;
		if ((!(((float)L_56) > ((float)(100000.0f)))))
		{
			goto IL_016d;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_57 = __this->____properties_0;
		NullCheck(L_57);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_58;
		L_58 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_57, NULL);
		NullCheck(L_58);
		FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* L_59 = L_58->___Document_13;
		NullCheck(L_59);
		int64_t L_60;
		L_60 = FBXDocument_ConvertToFBXTime_m523702DB9568E55B1493A765B9641FFE84AF6CF8(L_59, (100000.0), NULL);
		return L_60;
	}

IL_016d:
	{
		float L_61 = V_6;
		if ((!(((float)L_61) < ((float)(-100000.0f)))))
		{
			goto IL_0195;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_62 = __this->____properties_0;
		NullCheck(L_62);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_63;
		L_63 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_62, NULL);
		NullCheck(L_63);
		FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* L_64 = L_63->___Document_13;
		NullCheck(L_64);
		int64_t L_65;
		L_65 = FBXDocument_ConvertToFBXTime_m523702DB9568E55B1493A765B9641FFE84AF6CF8(L_64, (-100000.0), NULL);
		return L_65;
	}

IL_0195:
	{
		int64_t L_66 = V_5;
		return L_66;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorLong::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorLong_Dispose_mF9C2D2AB16FC636B737CB3EC385E6C2AD533548C (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B2_0 = NULL;
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B1_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B5_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B4_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B8_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B7_0 = NULL;
	{
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_0 = __this->____decoded_1;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000c;
		}
	}
	{
		goto IL_0011;
	}

IL_000c:
	{
		NullCheck(G_B2_0);
		ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2(G_B2_0, ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
	}

IL_0011:
	{
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_2 = __this->____decodedMemoryStream_2;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_3 = L_2;
		G_B4_0 = L_3;
		if (L_3)
		{
			G_B5_0 = L_3;
			goto IL_001d;
		}
	}
	{
		goto IL_0022;
	}

IL_001d:
	{
		NullCheck(G_B5_0);
		Stream_Dispose_mCDB42F32A17541CCA6D3A5906827A401570B07A8(G_B5_0, NULL);
	}

IL_0022:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = __this->____decodedBinaryReader_3;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_5 = L_4;
		G_B7_0 = L_5;
		if (L_5)
		{
			G_B8_0 = L_5;
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		NullCheck(G_B8_0);
		BinaryReader_Dispose_mAFF1A9CE9A73D148270FFA1F896992EB52D36078(G_B8_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Fbx.PropertyAccessorFloat::.ctor(TriLibCore.Fbx.FBXProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorFloat__ctor_mBF196988D5427DC01A7AE6DBBAD01197658973E1 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___0_properties, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_0 = ___0_properties;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = FBXProperties_GetPropertyArrayLength_mD43333FAA9443D29CC2A86C914E75F7D2DF57EC9(L_0, NULL);
		__this->____count_4 = L_1;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_2 = ___0_properties;
		__this->____properties_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____properties_0), (void*)L_2);
		__this->___U3CIsReadOnlyU3Ek__BackingField_5 = (bool)1;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Single> TriLibCore.Fbx.PropertyAccessorFloat::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorFloat_GetEnumerator_mC7CA3AD0597A0B85F4037019EFF259A7BBF1D425 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D L_0;
		memset((&L_0), 0, sizeof(L_0));
		PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC((&L_0), __this, /*hidden argument*/PropertyAccessorIEnumerator_1__ctor_m885514EA1BD585E7096456986DE47D98FFC4D9DC_RuntimeMethod_var);
		PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D L_1 = L_0;
		RuntimeObject* L_2 = Box(PropertyAccessorIEnumerator_1_t132A7FDC9F7EEB025D6C21FF07F9E037A5D8412D_il2cpp_TypeInfo_var, &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator TriLibCore.Fbx.PropertyAccessorFloat::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorFloat_System_Collections_IEnumerable_GetEnumerator_mBDDBDC474AF4DF6387659F4BAFA992106606869E (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = PropertyAccessorFloat_GetEnumerator_mC7CA3AD0597A0B85F4037019EFF259A7BBF1D425(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorFloat::Add(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorFloat_Add_mCCAD275FD92B9DD4E171845E9B1E4B3E8D9255ED (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, float ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorFloat_Add_mCCAD275FD92B9DD4E171845E9B1E4B3E8D9255ED_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorFloat::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorFloat_Clear_m3D2A56B0A4BBCC34D46ADD148EEAB561A5C9FBD6 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorFloat_Clear_m3D2A56B0A4BBCC34D46ADD148EEAB561A5C9FBD6_RuntimeMethod_var)));
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorFloat::Contains(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorFloat_Contains_mA19DBB936B8A684E19E995DFEC0629E6BFAA9C8C (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, float ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorFloat_Contains_mA19DBB936B8A684E19E995DFEC0629E6BFAA9C8C_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorFloat::CopyTo(System.Single[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorFloat_CopyTo_mCBD4A7064D7E5BF47D2D310D1F9CCD122CEEBE36 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___0_array, int32_t ___1_arrayIndex, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0004:
	{
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_0 = ___0_array;
		int32_t L_1 = ___1_arrayIndex;
		int32_t L_2 = V_0;
		int32_t L_3 = V_0;
		float L_4;
		L_4 = PropertyAccessorFloat_GetElement_mA56ED47BACBFA0BBA492E270FD5AA697850A5EF0(__this, L_3, NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_1, L_2))), (float)L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0014:
	{
		int32_t L_6 = V_0;
		int32_t L_7;
		L_7 = PropertyAccessorFloat_get_Count_m0AA67B6688957CA7F3B2E57EACE0882A5A05879C_inline(__this, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorFloat::Remove(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorFloat_Remove_mF265557EEFE8866385B6280859AF5C87402A1022 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, float ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorFloat_Remove_mF265557EEFE8866385B6280859AF5C87402A1022_RuntimeMethod_var)));
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorFloat::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorFloat_get_Count_m0AA67B6688957CA7F3B2E57EACE0882A5A05879C (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorFloat::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorFloat_get_IsReadOnly_m9CECFEC5DD0B62BD32B7D9DBEF717F6B1F98F36B (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsReadOnlyU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorFloat::IndexOf(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorFloat_IndexOf_m08F29119B0358F6727AEB21AC99F6A06D91FAAA8 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, float ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorFloat_IndexOf_m08F29119B0358F6727AEB21AC99F6A06D91FAAA8_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorFloat::Insert(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorFloat_Insert_m60FADFA183D69CE64EE67C9C2DA05C1AE28215F4 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, int32_t ___0_index, float ___1_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorFloat_Insert_m60FADFA183D69CE64EE67C9C2DA05C1AE28215F4_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorFloat::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorFloat_RemoveAt_mCD6754C94D601525E93F0BD570898E2AFB886C9F (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorFloat_RemoveAt_mCD6754C94D601525E93F0BD570898E2AFB886C9F_RuntimeMethod_var)));
	}
}
// System.Single TriLibCore.Fbx.PropertyAccessorFloat::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PropertyAccessorFloat_get_Item_m41B0954400C5C9746AE847DB5620C0DBFE8206A6 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_index;
		float L_1;
		L_1 = PropertyAccessorFloat_GetElement_mA56ED47BACBFA0BBA492E270FD5AA697850A5EF0(__this, L_0, NULL);
		return L_1;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorFloat::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorFloat_set_Item_m030F64C16AAC2FDD5A546AC3FC8307C53C06B79D (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, int32_t ___0_index, float ___1_value, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorFloat_set_Item_m030F64C16AAC2FDD5A546AC3FC8307C53C06B79D_RuntimeMethod_var)));
	}
}
// System.Single TriLibCore.Fbx.PropertyAccessorFloat::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PropertyAccessorFloat_GetElement_mA56ED47BACBFA0BBA492E270FD5AA697850A5EF0 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, int32_t ___0_i, const RuntimeMethod* method) 
{
	FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	{
		int32_t L_0 = ___0_i;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___0_i;
		int32_t L_2;
		L_2 = PropertyAccessorFloat_get_Count_m0AA67B6688957CA7F3B2E57EACE0882A5A05879C_inline(__this, NULL);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0013;
		}
	}

IL_000d:
	{
		return (0.0f);
	}

IL_0013:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_3 = __this->____properties_0;
		NullCheck(L_3);
		bool L_4;
		L_4 = FBXProperties_get_IsASCII_m412715B4396F75F7B1127FD4A4B44AC3BC963420(L_3, NULL);
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_5 = __this->____properties_0;
		int32_t L_6 = ___0_i;
		NullCheck(L_5);
		float L_7;
		L_7 = FBXProperties_ASCIIGetFloatValue_m73F67724B2EFDA0EF8AAA9D64DC918E2436A40BA(L_5, L_6, NULL);
		return L_7;
	}

IL_002d:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_8 = __this->____properties_0;
		NullCheck(L_8);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_9;
		L_9 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_8, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_10 = __this->____properties_0;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086** L_11 = (&__this->____decoded_1);
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2** L_12 = (&__this->____decodedMemoryStream_2);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158** L_13 = (&__this->____decodedBinaryReader_3);
		NullCheck(L_9);
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_14;
		L_14 = FBXProcessor_LoadArrayProperty_m97367395AB157AA3F3BA7EAF59C2D24777438FC5(L_9, L_10, L_11, L_12, L_13, (&V_1), (&V_2), NULL);
		V_0 = L_14;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_15 = __this->____properties_0;
		NullCheck(L_15);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_16;
		L_16 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_15, NULL);
		NullCheck(L_16);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_17 = L_16->___ActiveBinaryReader_19;
		NullCheck(L_17);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_18;
		L_18 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_17);
		int32_t L_19 = ___0_i;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_20 = __this->____properties_0;
		NullCheck(L_20);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_21;
		L_21 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_20, NULL);
		NullCheck(L_21);
		int32_t L_22 = L_21->___ActiveSubDataSize_18;
		NullCheck(L_18);
		int64_t L_23;
		L_23 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_18, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_19, L_22))), 1);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_24 = __this->____properties_0;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_25 = __this->____properties_0;
		NullCheck(L_25);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_26;
		L_26 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_25, NULL);
		NullCheck(L_26);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_27 = L_26->___ActiveBinaryReader_19;
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_28 = V_0;
		Il2CppChar L_29 = V_1;
		NullCheck(L_24);
		float L_30;
		L_30 = FBXProperties_BinaryConvertFloatValue_m5B4BCE0F186119B0C876D7D7E562109827608880(L_24, L_27, L_28, L_29, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_31 = __this->____properties_0;
		NullCheck(L_31);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_32;
		L_32 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_31, NULL);
		NullCheck(L_32);
		FBXProcessor_ReleaseActiveBinaryReader_mC144B788F2FFC4093884077E12EE33B4DEC6E461(L_32, NULL);
		return L_30;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorFloat::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorFloat_Dispose_mA04CF71EB0F996F33BC5E383B0C9A5C7C2CA4FB3 (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B2_0 = NULL;
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B1_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B5_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B4_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B8_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B7_0 = NULL;
	{
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_0 = __this->____decoded_1;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000c;
		}
	}
	{
		goto IL_0011;
	}

IL_000c:
	{
		NullCheck(G_B2_0);
		ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2(G_B2_0, ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
	}

IL_0011:
	{
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_2 = __this->____decodedMemoryStream_2;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_3 = L_2;
		G_B4_0 = L_3;
		if (L_3)
		{
			G_B5_0 = L_3;
			goto IL_001d;
		}
	}
	{
		goto IL_0022;
	}

IL_001d:
	{
		NullCheck(G_B5_0);
		Stream_Dispose_mCDB42F32A17541CCA6D3A5906827A401570B07A8(G_B5_0, NULL);
	}

IL_0022:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = __this->____decodedBinaryReader_3;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_5 = L_4;
		G_B7_0 = L_5;
		if (L_5)
		{
			G_B8_0 = L_5;
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		NullCheck(G_B8_0);
		BinaryReader_Dispose_mAFF1A9CE9A73D148270FFA1F896992EB52D36078(G_B8_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Fbx.PropertyAccessorDouble::.ctor(TriLibCore.Fbx.FBXProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorDouble__ctor_m204CA79674ECFA30CBDE5FF0736E78E9DE64B0F6 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* ___0_properties, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_0 = ___0_properties;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = FBXProperties_GetPropertyArrayLength_mD43333FAA9443D29CC2A86C914E75F7D2DF57EC9(L_0, NULL);
		__this->____count_4 = L_1;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_2 = ___0_properties;
		__this->____properties_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____properties_0), (void*)L_2);
		__this->___U3CIsReadOnlyU3Ek__BackingField_5 = (bool)1;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Double> TriLibCore.Fbx.PropertyAccessorDouble::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorDouble_GetEnumerator_m9F6C9FED59EA7039C0F615AAD3881AD89F9A6694 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1__ctor_m807FB04952574C23B9ECCFF282D11027CB4022CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PropertyAccessorIEnumerator_1_tA87B54345A2924A4BB0DE1D0D4BE147D24B9056D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PropertyAccessorIEnumerator_1_tA87B54345A2924A4BB0DE1D0D4BE147D24B9056D L_0;
		memset((&L_0), 0, sizeof(L_0));
		PropertyAccessorIEnumerator_1__ctor_m807FB04952574C23B9ECCFF282D11027CB4022CD((&L_0), __this, /*hidden argument*/PropertyAccessorIEnumerator_1__ctor_m807FB04952574C23B9ECCFF282D11027CB4022CD_RuntimeMethod_var);
		PropertyAccessorIEnumerator_1_tA87B54345A2924A4BB0DE1D0D4BE147D24B9056D L_1 = L_0;
		RuntimeObject* L_2 = Box(PropertyAccessorIEnumerator_1_tA87B54345A2924A4BB0DE1D0D4BE147D24B9056D_il2cpp_TypeInfo_var, &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator TriLibCore.Fbx.PropertyAccessorDouble::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PropertyAccessorDouble_System_Collections_IEnumerable_GetEnumerator_mF554D30A3A35A764449AF428496417B6EF1068FA (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0;
		L_0 = PropertyAccessorDouble_GetEnumerator_m9F6C9FED59EA7039C0F615AAD3881AD89F9A6694(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorDouble::Add(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorDouble_Add_mAE1735D14F3AA39C070B7CFF7D2D376453851ACE (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, double ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorDouble_Add_mAE1735D14F3AA39C070B7CFF7D2D376453851ACE_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorDouble::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorDouble_Clear_mEF8B26DAEC1AA884F3174D820B0BB4933AD5F4D1 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorDouble_Clear_mEF8B26DAEC1AA884F3174D820B0BB4933AD5F4D1_RuntimeMethod_var)));
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorDouble::Contains(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorDouble_Contains_m35D360656DAFE79EA025141CA19D86EF834E7EE3 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, double ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorDouble_Contains_m35D360656DAFE79EA025141CA19D86EF834E7EE3_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorDouble::CopyTo(System.Double[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorDouble_CopyTo_mB756B573A525006E9A9372D455DEEF1653371CC3 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, DoubleU5BU5D_tCC308475BD3B8229DB2582938669EF2F9ECC1FEE* ___0_array, int32_t ___1_arrayIndex, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0004:
	{
		DoubleU5BU5D_tCC308475BD3B8229DB2582938669EF2F9ECC1FEE* L_0 = ___0_array;
		int32_t L_1 = ___1_arrayIndex;
		int32_t L_2 = V_0;
		int32_t L_3 = V_0;
		double L_4;
		L_4 = PropertyAccessorDouble_GetElement_m23682071BF27DDD51AA30507127971F691D4DFAE(__this, L_3, NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(L_1, L_2))), (double)L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0014:
	{
		int32_t L_6 = V_0;
		int32_t L_7;
		L_7 = PropertyAccessorDouble_get_Count_mD1726F34A29A7ACEF3A1CF4C1CB12FABE08D7197_inline(__this, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorDouble::Remove(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorDouble_Remove_mDAD3E51DCDFAF51CC1045AE05EDED18355A8B6BF (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, double ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorDouble_Remove_mDAD3E51DCDFAF51CC1045AE05EDED18355A8B6BF_RuntimeMethod_var)));
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorDouble::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorDouble_get_Count_mD1726F34A29A7ACEF3A1CF4C1CB12FABE08D7197 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
// System.Boolean TriLibCore.Fbx.PropertyAccessorDouble::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PropertyAccessorDouble_get_IsReadOnly_mB5AB1148785DC104CA1D709790A6FD9DF011911A (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsReadOnlyU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Int32 TriLibCore.Fbx.PropertyAccessorDouble::IndexOf(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PropertyAccessorDouble_IndexOf_m7DA059EE56C40EB0703D3859F3053D074C923311 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, double ___0_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorDouble_IndexOf_m7DA059EE56C40EB0703D3859F3053D074C923311_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorDouble::Insert(System.Int32,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorDouble_Insert_mEB967E3D782C721D44C788453CD124976B0A11D8 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, int32_t ___0_index, double ___1_item, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorDouble_Insert_mEB967E3D782C721D44C788453CD124976B0A11D8_RuntimeMethod_var)));
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorDouble::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorDouble_RemoveAt_m7932C49E6F6D9D1D96E5F7655639650EEDDEB64A (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorDouble_RemoveAt_m7932C49E6F6D9D1D96E5F7655639650EEDDEB64A_RuntimeMethod_var)));
	}
}
// System.Double TriLibCore.Fbx.PropertyAccessorDouble::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double PropertyAccessorDouble_get_Item_mA01ED6FAB9576B6B28167140E58FCFE97084234A (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_index;
		double L_1;
		L_1 = PropertyAccessorDouble_GetElement_m23682071BF27DDD51AA30507127971F691D4DFAE(__this, L_0, NULL);
		return L_1;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorDouble::set_Item(System.Int32,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorDouble_set_Item_m8CA12BC5B805E259A5A91BB1A7A98EC44FF517DA (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, int32_t ___0_index, double ___1_value, const RuntimeMethod* method) 
{
	{
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_0 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotImplementedException__ctor_mDAB47BC6BD0E342E8F2171E5CABE3E67EA049F1C(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PropertyAccessorDouble_set_Item_m8CA12BC5B805E259A5A91BB1A7A98EC44FF517DA_RuntimeMethod_var)));
	}
}
// System.Double TriLibCore.Fbx.PropertyAccessorDouble::GetElement(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double PropertyAccessorDouble_GetElement_m23682071BF27DDD51AA30507127971F691D4DFAE (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, int32_t ___0_i, const RuntimeMethod* method) 
{
	FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	{
		int32_t L_0 = ___0_i;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___0_i;
		int32_t L_2;
		L_2 = PropertyAccessorDouble_get_Count_mD1726F34A29A7ACEF3A1CF4C1CB12FABE08D7197_inline(__this, NULL);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0017;
		}
	}

IL_000d:
	{
		return (0.0);
	}

IL_0017:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_3 = __this->____properties_0;
		NullCheck(L_3);
		bool L_4;
		L_4 = FBXProperties_get_IsASCII_m412715B4396F75F7B1127FD4A4B44AC3BC963420(L_3, NULL);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_5 = __this->____properties_0;
		int32_t L_6 = ___0_i;
		NullCheck(L_5);
		double L_7;
		L_7 = FBXProperties_ASCIIGetDoubleValue_m80150CE66E655F21CB5F37689DF79FF9822C0C07(L_5, L_6, NULL);
		return L_7;
	}

IL_0031:
	{
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_8 = __this->____properties_0;
		NullCheck(L_8);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_9;
		L_9 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_8, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_10 = __this->____properties_0;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086** L_11 = (&__this->____decoded_1);
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2** L_12 = (&__this->____decodedMemoryStream_2);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158** L_13 = (&__this->____decodedBinaryReader_3);
		NullCheck(L_9);
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_14;
		L_14 = FBXProcessor_LoadArrayProperty_m97367395AB157AA3F3BA7EAF59C2D24777438FC5(L_9, L_10, L_11, L_12, L_13, (&V_1), (&V_2), NULL);
		V_0 = L_14;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_15 = __this->____properties_0;
		NullCheck(L_15);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_16;
		L_16 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_15, NULL);
		NullCheck(L_16);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_17 = L_16->___ActiveBinaryReader_19;
		NullCheck(L_17);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_18;
		L_18 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_17);
		int32_t L_19 = ___0_i;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_20 = __this->____properties_0;
		NullCheck(L_20);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_21;
		L_21 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_20, NULL);
		NullCheck(L_21);
		int32_t L_22 = L_21->___ActiveSubDataSize_18;
		NullCheck(L_18);
		int64_t L_23;
		L_23 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_18, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_19, L_22))), 1);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_24 = __this->____properties_0;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_25 = __this->____properties_0;
		NullCheck(L_25);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_26;
		L_26 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_25, NULL);
		NullCheck(L_26);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_27 = L_26->___ActiveBinaryReader_19;
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_28 = V_0;
		Il2CppChar L_29 = V_1;
		NullCheck(L_24);
		double L_30;
		L_30 = FBXProperties_BinaryConvertDoubleValue_m70C6B2D77AF7105A1AFF6E4E3CE0812DEE30A962(L_24, L_27, L_28, L_29, NULL);
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_31 = __this->____properties_0;
		NullCheck(L_31);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_32;
		L_32 = FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline(L_31, NULL);
		NullCheck(L_32);
		FBXProcessor_ReleaseActiveBinaryReader_mC144B788F2FFC4093884077E12EE33B4DEC6E461(L_32, NULL);
		return L_30;
	}
}
// System.Void TriLibCore.Fbx.PropertyAccessorDouble::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropertyAccessorDouble_Dispose_m9436B2E7E0FC73C96F757BF44C54907385406643 (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B2_0 = NULL;
	ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* G_B1_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B5_0 = NULL;
	MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* G_B4_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B8_0 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* G_B7_0 = NULL;
	{
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_0 = __this->____decoded_1;
		ArrayPool_1_tA814D32D7C3112020287CCE898F896980ADC2086* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000c;
		}
	}
	{
		goto IL_0011;
	}

IL_000c:
	{
		NullCheck(G_B2_0);
		ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2(G_B2_0, ArrayPool_1_Dispose_mAC6C93E3CF80F99A63D348FBD96F52291354AAF2_RuntimeMethod_var);
	}

IL_0011:
	{
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_2 = __this->____decodedMemoryStream_2;
		MemoryStream_tAAED1B42172E3390584E4194308AB878E786AAC2* L_3 = L_2;
		G_B4_0 = L_3;
		if (L_3)
		{
			G_B5_0 = L_3;
			goto IL_001d;
		}
	}
	{
		goto IL_0022;
	}

IL_001d:
	{
		NullCheck(G_B5_0);
		Stream_Dispose_mCDB42F32A17541CCA6D3A5906827A401570B07A8(G_B5_0, NULL);
	}

IL_0022:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_4 = __this->____decodedBinaryReader_3;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_5 = L_4;
		G_B7_0 = L_5;
		if (L_5)
		{
			G_B8_0 = L_5;
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		NullCheck(G_B8_0);
		BinaryReader_Dispose_mAFF1A9CE9A73D148270FFA1F896992EB52D36078(G_B8_0, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Type TriLibCore.Fbx.Reader.FbxReader::get_LoadingStepEnumType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* FbxReader_get_LoadingStepEnumType_mAA1DD08EA42C888CEDABF8FB1679A40BC5E9B327 (FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProcessingSteps_t650107FDF5C3F52BB4A5B9D62AA93F417D0E5316_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_0 = { reinterpret_cast<intptr_t> (ProcessingSteps_t650107FDF5C3F52BB4A5B9D62AA93F417D0E5316_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_1;
		L_1 = Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57(L_0, NULL);
		return L_1;
	}
}
// System.String[] TriLibCore.Fbx.Reader.FbxReader::GetExtensions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* FbxReader_GetExtensions_mDCF1E4A3F5C10FB8AD18442C93B41DD523C5B0B0 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5C5B51FDBB849EDCED35D213FC930CC6B328429);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralD5C5B51FDBB849EDCED35D213FC930CC6B328429);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralD5C5B51FDBB849EDCED35D213FC930CC6B328429);
		return L_1;
	}
}
// System.String TriLibCore.Fbx.Reader.FbxReader::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FbxReader_get_Name_m66680BDEDCDC00D0814FE590CF8100F541CEBA73 (FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAF9B10C2E201D9B3CE0452AB6969B5912DBD0D42);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteralAF9B10C2E201D9B3CE0452AB6969B5912DBD0D42;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Fbx.Reader.FbxReader::ReadStream(System.IO.Stream,TriLibCore.AssetLoaderContext,System.String,System.Action`2<TriLibCore.AssetLoaderContext,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FbxReader_ReadStream_m0011DE05358203A69A741D2EA6EC9DAE5FD742E0 (FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, String_t* ___2_filename, Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___3_onProgress, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_stream;
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_1 = ___1_assetLoaderContext;
		String_t* L_2 = ___2_filename;
		Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* L_3 = ___3_onProgress;
		RuntimeObject* L_4;
		L_4 = ReaderBase_ReadStream_m725378DF096B29E0DB3BE3FB9E5F1E37747883F4(__this, L_0, L_1, L_2, L_3, NULL);
		ReaderBase_SetupStream_mCDC78453E3657CB3FBB713C40FB50B4941455942(__this, (&___0_stream), NULL);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_5 = ___0_stream;
		il2cpp_codegen_runtime_class_init_inline(FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = FbxReader_IsBinary_m8D593E28171C3E908C7FDAE60CA21BBDE49A4C56(L_5, NULL);
		if (!L_6)
		{
			goto IL_002e;
		}
	}
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_7 = ___0_stream;
		RuntimeObject* L_8;
		L_8 = FbxReader_ParseBinary_m8B56277ECB7EEE23D9D8477378311DAC78DD5515(__this, L_7, NULL);
		V_0 = L_8;
		ReaderBase_PostProcessModel_mD3BB953DE8BEB5FCA65077455BDA0750F740C667(__this, (&V_0), NULL);
		RuntimeObject* L_9 = V_0;
		return L_9;
	}

IL_002e:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_10 = ___0_stream;
		il2cpp_codegen_runtime_class_init_inline(FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = FbxReader_IsASCII_m1FA268B6126E6613ABDAFD1CBB5599C67BDC10AF(L_10, NULL);
		if (!L_11)
		{
			goto IL_0048;
		}
	}
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_12 = ___0_stream;
		RuntimeObject* L_13;
		L_13 = FbxReader_ParseASCII_m9B2DF8EBC9701E3DA1CAFC1067057815F487066F(__this, L_12, NULL);
		V_1 = L_13;
		ReaderBase_PostProcessModel_mD3BB953DE8BEB5FCA65077455BDA0750F740C667(__this, (&V_1), NULL);
		RuntimeObject* L_14 = V_1;
		return L_14;
	}

IL_0048:
	{
		return (RuntimeObject*)NULL;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Fbx.Reader.FbxReader::CreateRootModel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FbxReader_CreateRootModel_mA714ABFC798FE1605825E6FDF57844C6781C10C6 (FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* L_0 = (FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834*)il2cpp_codegen_object_new(FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		FBXDocument__ctor_m725644F1B4CBB463A84BA6C11C0B8990847B2390(L_0, NULL);
		FBXDocument_tA0771F5AD083A394E4AC5D57E4C612BFA26ED834* L_1 = L_0;
		NullCheck(L_1);
		L_1->___Reader_86 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___Reader_86), (void*)__this);
		return L_1;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Fbx.Reader.FbxReader::ParseBinary(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FbxReader_ParseBinary_m8B56277ECB7EEE23D9D8477378311DAC78DD5515 (FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* V_0 = NULL;
	FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* V_1 = NULL;
	FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_0 = (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4*)il2cpp_codegen_object_new(FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		FBXProcessor__ctor_m9CE53C2B543FA782A9F365B4EDFC3DB87BFB7977(L_0, __this, NULL);
		V_0 = L_0;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_1 = ___0_stream;
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_2 = V_0;
		FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* L_3 = (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50*)il2cpp_codegen_object_new(FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		FBXBinaryReader__ctor_mE643C5E24AF0209662FD47E0FA56FFCF9CB56554(L_3, L_1, __this, L_2, NULL);
		V_1 = L_3;
		FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* L_4 = V_1;
		NullCheck(L_4);
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_5;
		L_5 = FBXBinaryReader_ReadDocument_m27AFF3DF4C3E9C70713109A0A2FB0FA293674E64(L_4, NULL);
		V_2 = L_5;
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_6 = V_0;
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_7 = V_2;
		NullCheck(L_6);
		FBXRootModel_t1960D19A6733168B2F9CF5A6235362FD25861775* L_8;
		L_8 = FBXProcessor_Process_mD196AFB65D13AA7C75FE1409F327F5913214E902(L_6, L_7, (bool)1, NULL);
		FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* L_9 = V_1;
		NullCheck(L_9);
		FBXBinaryReader_Unload_m849A9163DF9496C688291EA217710B09C28E0E00(L_9, NULL);
		FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* L_10 = V_1;
		NullCheck(L_10);
		BinaryReader_Dispose_mAFF1A9CE9A73D148270FFA1F896992EB52D36078(L_10, NULL);
		return L_8;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Fbx.Reader.FbxReader::ParseASCII(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FbxReader_ParseASCII_m9B2DF8EBC9701E3DA1CAFC1067057815F487066F (FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_0 = (FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4*)il2cpp_codegen_object_new(FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		FBXProcessor__ctor_m9CE53C2B543FA782A9F365B4EDFC3DB87BFB7977(L_0, __this, NULL);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_1 = L_0;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2 = ___0_stream;
		FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* L_3 = (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B*)il2cpp_codegen_object_new(FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		FBXASCIIReader__ctor_mEB75301BADCB18697945E53B545B3A5B5E9787C7(L_3, L_1, L_2, NULL);
		NullCheck(L_3);
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_4;
		L_4 = FBXASCIIReader_ReadDocument_m42A06BB831534E44B0CFDE179264545AC4875724(L_3, NULL);
		V_0 = L_4;
		ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C(__this, (1.0f), 0, (0.0f), NULL);
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_5 = V_0;
		NullCheck(L_1);
		FBXRootModel_t1960D19A6733168B2F9CF5A6235362FD25861775* L_6;
		L_6 = FBXProcessor_Process_mD196AFB65D13AA7C75FE1409F327F5913214E902(L_1, L_5, (bool)0, NULL);
		return L_6;
	}
}
// System.Boolean TriLibCore.Fbx.Reader.FbxReader::IsBinary(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FbxReader_IsBinary_m8D593E28171C3E908C7FDAE60CA21BBDE49A4C56 (Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEE17426A533F8CAB2BECCE4D29123F6B09106045);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_1 = NULL;
	{
		V_0 = (bool)0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)18));
		V_1 = L_0;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_1 = ___0_stream;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3;
		L_3 = VirtualFuncInvoker3< int32_t, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(34 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, 0, ((int32_t)18));
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_4;
		L_4 = Encoding_get_UTF8_m9FA98A53CE96FD6D02982625C5246DD36C1235C9(NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_5 = V_1;
		NullCheck(L_4);
		String_t* L_6;
		L_6 = VirtualFuncInvoker1< String_t*, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* >::Invoke(37 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_4, L_5);
		bool L_7;
		L_7 = String_op_Equality_m030E1B219352228970A076136E455C4E568C02C1(L_6, _stringLiteralEE17426A533F8CAB2BECCE4D29123F6B09106045, NULL);
		if (!L_7)
		{
			goto IL_002e;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_002e:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_8 = ___0_stream;
		NullCheck(L_8);
		int64_t L_9;
		L_9 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_8, ((int64_t)0), 0);
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Boolean TriLibCore.Fbx.Reader.FbxReader::IsASCII(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FbxReader_IsASCII_m1FA268B6126E6613ABDAFD1CBB5599C67BDC10AF (Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4A6331918BBD32293C65EC7C39BE183F558B0111);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_stream;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral4A6331918BBD32293C65EC7C39BE183F558B0111);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4A6331918BBD32293C65EC7C39BE183F558B0111);
		bool L_3;
		L_3 = StreamExtensions_MatchRegex_m4D785C0985D867DC78F422F0DF28EDFDA23A5905(L_0, L_2, NULL);
		return L_3;
	}
}
// System.Void TriLibCore.Fbx.Reader.FbxReader::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FbxReader__ctor_mE5B6FD7107C7FA9E7D7D8BA04D0F5925EB16966D (FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E* __this, const RuntimeMethod* method) 
{
	{
		ReaderBase__ctor_m5C4FE7A4BC205B65DAB56FF3CC5202D0B04937DA(__this, NULL);
		return;
	}
}
// System.Void TriLibCore.Fbx.Reader.FbxReader::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FbxReader__cctor_m4D30BED47356BD2FFDDB27B45DA7D670133F8440 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_StaticFields*)il2cpp_codegen_static_fields_for(FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var))->___FBXCompatibilityMode_10 = (bool)1;
		((FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_StaticFields*)il2cpp_codegen_static_fields_for(FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var))->___PivotMode_11 = 1;
		((FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_StaticFields*)il2cpp_codegen_static_fields_for(FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var))->___EnableSpecularMaterials_12 = (bool)0;
		((FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_StaticFields*)il2cpp_codegen_static_fields_for(FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var))->___MergeSingleChildDocument_14 = (bool)0;
		((FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_StaticFields*)il2cpp_codegen_static_fields_for(FbxReader_tE700A81D60B645F9D6FB1F9EC41B7DA7E644AF4E_il2cpp_TypeInfo_var))->___BufferizeStream_15 = (bool)1;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Fbx.Binary.FBXBinaryReader::Unload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXBinaryReader_Unload_m849A9163DF9496C688291EA217710B09C28E0E00 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) 
{
	{
		InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2* L_0 = __this->___InflaterInputStream_17;
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2* L_1 = __this->___InflaterInputStream_17;
		NullCheck(L_1);
		Stream_Dispose_mCDB42F32A17541CCA6D3A5906827A401570B07A8(L_1, NULL);
		__this->___InflaterInputStream_17 = (InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___InflaterInputStream_17), (void*)(InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2*)NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Void TriLibCore.Fbx.Binary.FBXBinaryReader::.ctor(System.IO.Stream,TriLibCore.ReaderBase,TriLibCore.Fbx.FBXProcessor)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXBinaryReader__ctor_mE643C5E24AF0209662FD47E0FA56FFCF9CB56554 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_inputStream, ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* ___1_reader, FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ___2_processor, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->___Chars_11 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Chars_11), (void*)L_0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		__this->___Buffer_12 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Buffer_12), (void*)L_1);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2 = ___0_inputStream;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_3;
		L_3 = Encoding_get_UTF8_m9FA98A53CE96FD6D02982625C5246DD36C1235C9(NULL);
		BinaryReader__ctor_m5B206ED513B0AECC14E4AF5A7B42AE5C4885334E(__this, L_2, L_3, (bool)1, NULL);
		ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* L_4 = ___1_reader;
		__this->____reader_14 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____reader_14), (void*)L_4);
		Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494* L_5 = (Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494*)il2cpp_codegen_object_new(Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		Inflater__ctor_m5183A7AAD2E39FFDCEE42A83E207181960B95FF9(L_5, (bool)1, NULL);
		__this->___Inflater_18 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Inflater_18), (void*)L_5);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_6 = ___0_inputStream;
		Inflater_t2F41C3E2608A77A69EDAB071BCA9752305595494* L_7 = __this->___Inflater_18;
		InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2* L_8 = (InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2*)il2cpp_codegen_object_new(InflaterInputStream_t25453CA4D454447A9D30F0011CCE23037B465EA2_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		InflaterInputStream__ctor_mAEA971E711654A0AFB588ABE8512960D41F3E3D0(L_8, L_6, L_7, NULL);
		__this->___InflaterInputStream_17 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___InflaterInputStream_17), (void*)L_8);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_9 = ___2_processor;
		__this->____processor_19 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____processor_19), (void*)L_9);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_10 = __this->____processor_19;
		NullCheck(L_10);
		L_10->___FBXBinaryReader_21 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_10->___FBXBinaryReader_21), (void*)__this);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_11 = __this->____processor_19;
		NullCheck(L_11);
		FBXProcessor_ReleaseActiveBinaryReader_mC144B788F2FFC4093884077E12EE33B4DEC6E461(L_11, NULL);
		return;
	}
}
// TriLibCore.Fbx.FBXNode TriLibCore.Fbx.Binary.FBXBinaryReader::ReadDocument()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D FBXBinaryReader_ReadDocument_m27AFF3DF4C3E9C70713109A0A2FB0FA293674E64 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0;
		L_0 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_0);
		int64_t L_1;
		L_1 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_0, ((int64_t)((int32_t)23)), 0);
		int32_t L_2;
		L_2 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		__this->____version_16 = L_2;
		int32_t L_3 = __this->____version_16;
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)7000))))
		{
			goto IL_0034;
		}
	}
	{
		Exception_t* L_4 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_4);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_4, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral91DD86FA4DF0E44AA795B032277AB18EF9404D3E)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FBXBinaryReader_ReadDocument_m27AFF3DF4C3E9C70713109A0A2FB0FA293674E64_RuntimeMethod_var)));
	}

IL_0034:
	{
		int32_t L_5 = __this->____version_16;
		__this->____is64Bits_15 = (bool)((((int32_t)((((int32_t)L_5) < ((int32_t)((int32_t)7500)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_6;
		L_6 = FBXBinaryReader_ReadRootNode_m588BA96B26A31C30F9A70229130034A770D39CD6(__this, NULL);
		return L_6;
	}
}
// System.Boolean TriLibCore.Fbx.Binary.FBXBinaryReader::CountChildNodes(System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FBXBinaryReader_CountChildNodes_mCBAAD8D4DBBECD855F5BCC757BA5E061B374C4DF (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, int32_t* ___0_childCount, int32_t* ___1_propertiesCount, const RuntimeMethod* method) 
{
	int64_t V_0 = 0;
	int64_t V_1 = 0;
	int64_t V_2 = 0;
	{
		bool L_0 = __this->____is64Bits_15;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		int64_t L_1;
		L_1 = VirtualFuncInvoker0< int64_t >::Invoke(18 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, __this);
		V_0 = L_1;
		int64_t L_2;
		L_2 = VirtualFuncInvoker0< int64_t >::Invoke(18 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, __this);
		V_1 = L_2;
		int64_t L_3;
		L_3 = VirtualFuncInvoker0< int64_t >::Invoke(18 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, __this);
		V_2 = L_3;
		goto IL_0037;
	}

IL_001f:
	{
		int32_t L_4;
		L_4 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		V_0 = ((int64_t)L_4);
		int32_t L_5;
		L_5 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		V_1 = ((int64_t)L_5);
		int32_t L_6;
		L_6 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		V_2 = ((int64_t)L_6);
	}

IL_0037:
	{
		int32_t* L_7 = ___1_propertiesCount;
		int32_t* L_8 = ___1_propertiesCount;
		int32_t L_9 = *((int32_t*)L_8);
		int64_t L_10 = V_1;
		*((int32_t*)L_7) = (int32_t)((int32_t)il2cpp_codegen_add(L_9, ((int32_t)L_10)));
		FBXBinaryReader_SkipStringEx_mCC66E0E7BDF4C7F65EF9BB62CE92B8CBFA874148(__this, NULL);
		int64_t L_11 = V_0;
		if (L_11)
		{
			goto IL_004f;
		}
	}
	{
		int64_t L_12 = V_1;
		if (L_12)
		{
			goto IL_004f;
		}
	}
	{
		int64_t L_13 = V_2;
		if (L_13)
		{
			goto IL_004f;
		}
	}
	{
		return (bool)0;
	}

IL_004f:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_14;
		L_14 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		int64_t L_15 = V_2;
		NullCheck(L_14);
		int64_t L_16;
		L_16 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_14, L_15, 1);
		goto IL_0069;
	}

IL_005f:
	{
		int32_t* L_17 = ___0_childCount;
		int32_t* L_18 = ___1_propertiesCount;
		bool L_19;
		L_19 = FBXBinaryReader_CountChildNodes_mCBAAD8D4DBBECD855F5BCC757BA5E061B374C4DF(__this, L_17, L_18, NULL);
		if (!L_19)
		{
			goto IL_0077;
		}
	}

IL_0069:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_20;
		L_20 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_20);
		int64_t L_21;
		L_21 = VirtualFuncInvoker0< int64_t >::Invoke(12 /* System.Int64 System.IO.Stream::get_Position() */, L_20);
		int64_t L_22 = V_0;
		if ((((int64_t)L_21) < ((int64_t)L_22)))
		{
			goto IL_005f;
		}
	}

IL_0077:
	{
		int32_t* L_23 = ___0_childCount;
		int32_t* L_24 = ___0_childCount;
		int32_t L_25 = *((int32_t*)L_24);
		*((int32_t*)L_23) = (int32_t)((int32_t)il2cpp_codegen_add(L_25, 1));
		return (bool)1;
	}
}
// System.Void TriLibCore.Fbx.Binary.FBXBinaryReader::SkipStringEx()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXBinaryReader_SkipStringEx_mCC66E0E7BDF4C7F65EF9BB62CE92B8CBFA874148 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0;
		L_0 = BinaryReader_Read7BitEncodedInt_mAC30887A2BB23F481A73FA61A487159F855D34F5(__this, NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)1024))))
		{
			goto IL_001a;
		}
	}
	{
		Exception_t* L_2 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_2);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral59A855EBCC658B736BF700F291E16BCDE82179CC)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FBXBinaryReader_SkipStringEx_mCC66E0E7BDF4C7F65EF9BB62CE92B8CBFA874148_RuntimeMethod_var)));
	}

IL_001a:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_3;
		L_3 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int64_t L_5;
		L_5 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_3, ((int64_t)L_4), 1);
		return;
	}
}
// System.String TriLibCore.Fbx.Binary.FBXBinaryReader::ReadStringEx()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FBXBinaryReader_ReadStringEx_m9FC9DD7CD2CBFCD8D4F15FC652C5B99423BF8390 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t G_B8_0 = 0;
	{
		int32_t L_0;
		L_0 = BinaryReader_Read7BitEncodedInt_mAC30887A2BB23F481A73FA61A487159F855D34F5(__this, NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000d:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)1024))))
		{
			goto IL_0020;
		}
	}
	{
		Exception_t* L_3 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_3);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral59A855EBCC658B736BF700F291E16BCDE82179CC)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FBXBinaryReader_ReadStringEx_m9FC9DD7CD2CBFCD8D4F15FC652C5B99423BF8390_RuntimeMethod_var)));
	}

IL_0020:
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_4 = __this->___Buffer_12;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_5 = __this->___Buffer_12;
		NullCheck(L_5);
		Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB((RuntimeArray*)L_4, 0, ((int32_t)(((RuntimeArray*)L_5)->max_length)), NULL);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_6 = __this->___Chars_11;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_7 = __this->___Chars_11;
		NullCheck(L_7);
		Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB((RuntimeArray*)L_6, 0, ((int32_t)(((RuntimeArray*)L_7)->max_length)), NULL);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_8;
		L_8 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_9 = __this->___Buffer_12;
		int32_t L_10 = V_0;
		NullCheck(L_8);
		int32_t L_11;
		L_11 = VirtualFuncInvoker3< int32_t, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t >::Invoke(34 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_8, L_9, 0, L_10);
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_12;
		L_12 = Encoding_get_UTF8_m9FA98A53CE96FD6D02982625C5246DD36C1235C9(NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_13 = __this->___Buffer_12;
		int32_t L_14 = V_0;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_15 = __this->___Chars_11;
		NullCheck(L_12);
		int32_t L_16;
		L_16 = VirtualFuncInvoker5< int32_t, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*, int32_t, int32_t, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*, int32_t >::Invoke(28 /* System.Int32 System.Text.Encoding::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32) */, L_12, L_13, 0, L_14, L_15, 0);
		V_1 = 0;
		V_2 = 0;
	}

IL_007a:
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_17 = __this->___Chars_11;
		int32_t L_18 = V_1;
		int32_t L_19 = L_18;
		V_1 = ((int32_t)il2cpp_codegen_add(L_19, 1));
		NullCheck(L_17);
		int32_t L_20 = L_19;
		uint16_t L_21 = (uint16_t)(L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		if (!L_21)
		{
			goto IL_008b;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_008e;
	}

IL_008b:
	{
		int32_t L_22 = V_2;
		G_B8_0 = ((int32_t)il2cpp_codegen_add(L_22, 1));
	}

IL_008e:
	{
		V_2 = G_B8_0;
		int32_t L_23 = V_1;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_24 = __this->___Buffer_12;
		NullCheck(L_24);
		if ((((int32_t)L_23) >= ((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length)))))
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_25 = V_2;
		if ((((int32_t)L_25) < ((int32_t)2)))
		{
			goto IL_007a;
		}
	}

IL_009e:
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_26 = __this->___Chars_11;
		int32_t L_27 = V_1;
		String_t* L_28;
		L_28 = String_CreateString_mB7B3AC2AF28010538650051A9000369B1CD6BAB6(NULL, L_26, 0, ((int32_t)il2cpp_codegen_subtract(L_27, 2)), NULL);
		return L_28;
	}
}
// TriLibCore.Fbx.FBXNode TriLibCore.Fbx.Binary.FBXBinaryReader::ReadRootNode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D FBXBinaryReader_ReadRootNode_m588BA96B26A31C30F9A70229130034A770D39CD6 (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB94DD1963BC6E8F27D446B420BEA01FE03FD8C2B);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	{
		V_0 = (-1);
		V_1 = (-1);
		goto IL_0054;
	}

IL_0006:
	{
		ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* L_0 = __this->____reader_14;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_1;
		L_1 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_1);
		int64_t L_2;
		L_2 = VirtualFuncInvoker0< int64_t >::Invoke(12 /* System.Int64 System.IO.Stream::get_Position() */, L_1);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_3;
		L_3 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_3);
		int64_t L_4;
		L_4 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Length() */, L_3);
		NullCheck(L_0);
		ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C(L_0, ((float)(((float)L_2)/((float)L_4))), 0, (0.0f), NULL);
		int32_t L_5;
		L_5 = FBXBinaryReader_ReadNode_m7B76CA3D4E6C170ADDBC95526EEB5A823900C92E(__this, NULL);
		V_3 = L_5;
		int32_t L_6 = V_3;
		if ((((int32_t)L_6) == ((int32_t)(-1))))
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)(-1)))))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_8 = V_3;
		V_0 = L_8;
	}

IL_0041:
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_0052;
		}
	}
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_10 = __this->____processor_19;
		int32_t L_11 = V_1;
		int32_t L_12 = V_3;
		NullCheck(L_10);
		FBXProcessor_SetNodeNext_m8630F0DDE868E67E1C3EBE3BE0F8ABBA8BF5484C(L_10, L_11, L_12, NULL);
	}

IL_0052:
	{
		int32_t L_13 = V_3;
		V_1 = L_13;
	}

IL_0054:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_14;
		L_14 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_14);
		int64_t L_15;
		L_15 = VirtualFuncInvoker0< int64_t >::Invoke(12 /* System.Int64 System.IO.Stream::get_Position() */, L_14);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_16;
		L_16 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_16);
		int64_t L_17;
		L_17 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Length() */, L_16);
		if ((((int64_t)L_15) < ((int64_t)L_17)))
		{
			goto IL_0006;
		}
	}

IL_006c:
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_18 = __this->____processor_19;
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_19 = __this->____processor_19;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_20 = (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418*)il2cpp_codegen_object_new(FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418_il2cpp_TypeInfo_var);
		NullCheck(L_20);
		FBXProperties__ctor_m9FAEE6F003B0285D473D3E241974FC3C9EBFE67C(L_20, L_19, 0, NULL);
		FBXNode__ctor_m3D406E7ADA2DA527D69C7573E4866196EE3DD631((&V_2), L_18, _stringLiteralB94DD1963BC6E8F27D446B420BEA01FE03FD8C2B, L_20, NULL);
		int32_t L_21 = V_0;
		if ((((int32_t)L_21) == ((int32_t)(-1))))
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_0;
		(&V_2)->___FirstNodeIndex_3 = L_22;
	}

IL_0096:
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_23 = __this->____processor_19;
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_24 = V_2;
		NullCheck(L_23);
		int32_t L_25;
		L_25 = FBXProcessor_AddNode_m39374DBF94E11AB90240A841029D230D62AC872D(L_23, L_24, NULL);
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_26 = V_2;
		return L_26;
	}
}
// System.Int32 TriLibCore.Fbx.Binary.FBXBinaryReader::ReadNode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FBXBinaryReader_ReadNode_m7B76CA3D4E6C170ADDBC95526EEB5A823900C92E (FBXBinaryReader_t110389465EA442F21954581FA8C9774171074C50* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	int64_t V_1 = 0;
	String_t* V_2 = NULL;
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 V_7;
	memset((&V_7), 0, sizeof(V_7));
	Il2CppChar V_8 = 0x0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D V_13;
	memset((&V_13), 0, sizeof(V_13));
	{
		bool L_0 = __this->____is64Bits_15;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		int64_t L_1;
		L_1 = VirtualFuncInvoker0< int64_t >::Invoke(18 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, __this);
		V_0 = L_1;
		int64_t L_2;
		L_2 = VirtualFuncInvoker0< int64_t >::Invoke(18 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, __this);
		V_1 = L_2;
		int64_t L_3;
		L_3 = VirtualFuncInvoker0< int64_t >::Invoke(18 /* System.Int64 System.IO.BinaryReader::ReadInt64() */, __this);
		goto IL_0036;
	}

IL_001f:
	{
		int32_t L_4;
		L_4 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		V_0 = ((int64_t)L_4);
		int32_t L_5;
		L_5 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		V_1 = ((int64_t)L_5);
		int32_t L_6;
		L_6 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
	}

IL_0036:
	{
		String_t* L_7;
		L_7 = FBXBinaryReader_ReadStringEx_m9FC9DD7CD2CBFCD8D4F15FC652C5B99423BF8390(__this, NULL);
		V_2 = L_7;
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_8 = __this->____processor_19;
		int64_t L_9 = V_1;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_10 = (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418*)il2cpp_codegen_object_new(FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		FBXProperties__ctor_m9FAEE6F003B0285D473D3E241974FC3C9EBFE67C(L_10, L_8, ((int32_t)L_9), NULL);
		V_3 = L_10;
		int64_t L_11 = V_1;
		if ((((int64_t)L_11) <= ((int64_t)((int64_t)0))))
		{
			goto IL_01ac;
		}
	}
	{
		V_6 = 0;
		goto IL_01a3;
	}

IL_005b:
	{
		il2cpp_codegen_initobj((&V_7), sizeof(FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8));
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_12;
		L_12 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_12);
		int64_t L_13;
		L_13 = VirtualFuncInvoker0< int64_t >::Invoke(12 /* System.Int64 System.IO.Stream::get_Position() */, L_12);
		(&V_7)->___Position_0 = L_13;
		Il2CppChar L_14;
		L_14 = VirtualFuncInvoker0< Il2CppChar >::Invoke(13 /* System.Char System.IO.BinaryReader::ReadChar() */, __this);
		V_8 = L_14;
		Il2CppChar L_15 = V_8;
		if ((!(((uint32_t)L_15) <= ((uint32_t)((int32_t)83)))))
		{
			goto IL_0094;
		}
	}
	{
		Il2CppChar L_16 = V_8;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)82))))
		{
			goto IL_00d2;
		}
	}
	{
		Il2CppChar L_17 = V_8;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)83))))
		{
			goto IL_00d2;
		}
	}
	{
		goto IL_0117;
	}

IL_0094:
	{
		Il2CppChar L_18 = V_8;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_18, ((int32_t)98))))
		{
			case 0:
			{
				goto IL_00ef;
			}
			case 1:
			{
				goto IL_00ef;
			}
			case 2:
			{
				goto IL_00ef;
			}
			case 3:
			{
				goto IL_0117;
			}
			case 4:
			{
				goto IL_00ef;
			}
			case 5:
			{
				goto IL_0117;
			}
			case 6:
			{
				goto IL_0117;
			}
			case 7:
			{
				goto IL_00ef;
			}
			case 8:
			{
				goto IL_0117;
			}
			case 9:
			{
				goto IL_0117;
			}
			case 10:
			{
				goto IL_00ef;
			}
		}
	}
	{
		Il2CppChar L_19 = V_8;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)121))))
		{
			goto IL_00ef;
		}
	}
	{
		goto IL_0117;
	}

IL_00d2:
	{
		int32_t L_20;
		L_20 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		V_9 = L_20;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_21;
		L_21 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		int32_t L_22 = V_9;
		NullCheck(L_21);
		int64_t L_23;
		L_23 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_21, ((int64_t)L_22), 1);
		goto IL_0190;
	}

IL_00ef:
	{
		int32_t L_24;
		L_24 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		int32_t L_25;
		L_25 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		int32_t L_26;
		L_26 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, __this);
		V_10 = L_26;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_27;
		L_27 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		int32_t L_28 = V_10;
		NullCheck(L_27);
		int64_t L_29;
		L_29 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_27, ((int64_t)L_28), 1);
		goto IL_0190;
	}

IL_0117:
	{
		Il2CppChar L_30 = V_8;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_30, ((int32_t)66))))
		{
			case 0:
			{
				goto IL_0155;
			}
			case 1:
			{
				goto IL_0155;
			}
			case 2:
			{
				goto IL_0164;
			}
			case 3:
			{
				goto IL_0169;
			}
			case 4:
			{
				goto IL_015f;
			}
			case 5:
			{
				goto IL_0169;
			}
			case 6:
			{
				goto IL_0169;
			}
			case 7:
			{
				goto IL_015f;
			}
			case 8:
			{
				goto IL_0169;
			}
			case 9:
			{
				goto IL_0169;
			}
			case 10:
			{
				goto IL_0164;
			}
		}
	}
	{
		Il2CppChar L_31 = V_8;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)89))))
		{
			goto IL_015a;
		}
	}
	{
		goto IL_0169;
	}

IL_0155:
	{
		V_11 = 1;
		goto IL_0180;
	}

IL_015a:
	{
		V_11 = 2;
		goto IL_0180;
	}

IL_015f:
	{
		V_11 = 4;
		goto IL_0180;
	}

IL_0164:
	{
		V_11 = 8;
		goto IL_0180;
	}

IL_0169:
	{
		Il2CppChar L_32 = V_8;
		Il2CppChar L_33 = L_32;
		RuntimeObject* L_34 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Char_t521A6F19B456D956AF452D926C32709DC03D6B17_il2cpp_TypeInfo_var)), &L_33);
		String_t* L_35;
		L_35 = String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral931D7B3EADF2ABC99B546F64E928640DD28E17B2)), L_34, NULL);
		Exception_t* L_36 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_36);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_36, L_35, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_36, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FBXBinaryReader_ReadNode_m7B76CA3D4E6C170ADDBC95526EEB5A823900C92E_RuntimeMethod_var)));
	}

IL_0180:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_37;
		L_37 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		int32_t L_38 = V_11;
		NullCheck(L_37);
		int64_t L_39;
		L_39 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_37, ((int64_t)L_38), 1);
	}

IL_0190:
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_40 = __this->____processor_19;
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_41 = V_7;
		NullCheck(L_40);
		FBXProcessor_AddProperty_m013216FFEAA69120137545F668B3CA08405BDCA3(L_40, L_41, NULL);
		int32_t L_42 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_42, 1));
	}

IL_01a3:
	{
		int32_t L_43 = V_6;
		int64_t L_44 = V_1;
		if ((((int64_t)((int64_t)L_43)) < ((int64_t)L_44)))
		{
			goto IL_005b;
		}
	}

IL_01ac:
	{
		V_4 = (-1);
		V_5 = (-1);
		goto IL_01e2;
	}

IL_01b4:
	{
		int32_t L_45;
		L_45 = FBXBinaryReader_ReadNode_m7B76CA3D4E6C170ADDBC95526EEB5A823900C92E(__this, NULL);
		V_12 = L_45;
		int32_t L_46 = V_12;
		if ((((int32_t)L_46) == ((int32_t)(-1))))
		{
			goto IL_01f0;
		}
	}
	{
		int32_t L_47 = V_4;
		if ((!(((uint32_t)L_47) == ((uint32_t)(-1)))))
		{
			goto IL_01ca;
		}
	}
	{
		int32_t L_48 = V_12;
		V_4 = L_48;
	}

IL_01ca:
	{
		int32_t L_49 = V_5;
		if ((((int32_t)L_49) == ((int32_t)(-1))))
		{
			goto IL_01de;
		}
	}
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_50 = __this->____processor_19;
		int32_t L_51 = V_5;
		int32_t L_52 = V_12;
		NullCheck(L_50);
		FBXProcessor_SetNodeNext_m8630F0DDE868E67E1C3EBE3BE0F8ABBA8BF5484C(L_50, L_51, L_52, NULL);
	}

IL_01de:
	{
		int32_t L_53 = V_12;
		V_5 = L_53;
	}

IL_01e2:
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_54;
		L_54 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_54);
		int64_t L_55;
		L_55 = VirtualFuncInvoker0< int64_t >::Invoke(12 /* System.Int64 System.IO.Stream::get_Position() */, L_54);
		int64_t L_56 = V_0;
		if ((((int64_t)L_55) < ((int64_t)L_56)))
		{
			goto IL_01b4;
		}
	}

IL_01f0:
	{
		String_t* L_57 = V_2;
		bool L_58;
		L_58 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_57, NULL);
		if (L_58)
		{
			goto IL_0223;
		}
	}
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_59 = __this->____processor_19;
		String_t* L_60 = V_2;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_61 = V_3;
		FBXNode__ctor_m3D406E7ADA2DA527D69C7573E4866196EE3DD631((&V_13), L_59, L_60, L_61, NULL);
		int32_t L_62 = V_4;
		if ((((int32_t)L_62) == ((int32_t)(-1))))
		{
			goto IL_0215;
		}
	}
	{
		int32_t L_63 = V_4;
		(&V_13)->___FirstNodeIndex_3 = L_63;
	}

IL_0215:
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_64 = __this->____processor_19;
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_65 = V_13;
		NullCheck(L_64);
		int32_t L_66;
		L_66 = FBXProcessor_AddNode_m39374DBF94E11AB90240A841029D230D62AC872D(L_64, L_65, NULL);
		return L_66;
	}

IL_0223:
	{
		return (-1);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: TriLibCore.Fbx.ASCII.ASCIIValueEnumerator
IL2CPP_EXTERN_C void ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshal_pinvoke(const ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271& unmarshaled, ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshaled_pinvoke& marshaled)
{
	Exception_t* ____reader_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '_reader' of type 'ASCIIValueEnumerator': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(____reader_0Exception, NULL);
}
IL2CPP_EXTERN_C void ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshal_pinvoke_back(const ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshaled_pinvoke& marshaled, ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271& unmarshaled)
{
	Exception_t* ____reader_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '_reader' of type 'ASCIIValueEnumerator': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(____reader_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: TriLibCore.Fbx.ASCII.ASCIIValueEnumerator
IL2CPP_EXTERN_C void ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshal_pinvoke_cleanup(ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: TriLibCore.Fbx.ASCII.ASCIIValueEnumerator
IL2CPP_EXTERN_C void ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshal_com(const ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271& unmarshaled, ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshaled_com& marshaled)
{
	Exception_t* ____reader_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '_reader' of type 'ASCIIValueEnumerator': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(____reader_0Exception, NULL);
}
IL2CPP_EXTERN_C void ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshal_com_back(const ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshaled_com& marshaled, ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271& unmarshaled)
{
	Exception_t* ____reader_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '_reader' of type 'ASCIIValueEnumerator': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(____reader_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: TriLibCore.Fbx.ASCII.ASCIIValueEnumerator
IL2CPP_EXTERN_C void ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshal_com_cleanup(ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_marshaled_com& marshaled)
{
}
// System.Void TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::.ctor(TriLibCore.Fbx.ASCII.FBXASCIIReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ASCIIValueEnumerator__ctor_mCC8BD1780C632806437D4E4F3D8559D85EF066EA (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* ___0_reader, const RuntimeMethod* method) 
{
	{
		FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* L_0 = ___0_reader;
		__this->____reader_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____reader_0), (void*)L_0);
		ASCIIValueEnumerator_set_Current_m5723F350D36F2E6F7378B24D0C439317DE57BD2C_inline(__this, (uint8_t)0, NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void ASCIIValueEnumerator__ctor_mCC8BD1780C632806437D4E4F3D8559D85EF066EA_AdjustorThunk (RuntimeObject* __this, FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* ___0_reader, const RuntimeMethod* method)
{
	ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271*>(__this + _offset);
	ASCIIValueEnumerator__ctor_mCC8BD1780C632806437D4E4F3D8559D85EF066EA(_thisAdjusted, ___0_reader, method);
}
// System.Void TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ASCIIValueEnumerator_Dispose_mA5A3C9128607CC4D986FB2EA55603AB7DB0B941A (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
IL2CPP_EXTERN_C  void ASCIIValueEnumerator_Dispose_mA5A3C9128607CC4D986FB2EA55603AB7DB0B941A_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271*>(__this + _offset);
	ASCIIValueEnumerator_Dispose_mA5A3C9128607CC4D986FB2EA55603AB7DB0B941A(_thisAdjusted, method);
}
// System.Boolean TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ASCIIValueEnumerator_MoveNext_mAAAA36FDA3818102E5E8ADE87DD90DD1F68B4D6B (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) 
{
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_0 = __this->____reader_0;
		NullCheck(L_0);
		uint8_t L_1;
		L_1 = VirtualFuncInvoker0< uint8_t >::Invoke(11 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_0);
		ASCIIValueEnumerator_set_Current_m5723F350D36F2E6F7378B24D0C439317DE57BD2C_inline(__this, L_1, NULL);
		uint8_t L_2;
		L_2 = ASCIIValueEnumerator_get_Current_m777B03023C16D40AE16CCC8B89B55902BA9DBBC1_inline(__this, NULL);
		return (bool)((((int32_t)((((int32_t)L_2) == ((int32_t)((int32_t)34)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
IL2CPP_EXTERN_C  bool ASCIIValueEnumerator_MoveNext_mAAAA36FDA3818102E5E8ADE87DD90DD1F68B4D6B_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271*>(__this + _offset);
	bool _returnValue;
	_returnValue = ASCIIValueEnumerator_MoveNext_mAAAA36FDA3818102E5E8ADE87DD90DD1F68B4D6B(_thisAdjusted, method);
	return _returnValue;
}
// System.Void TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ASCIIValueEnumerator_Reset_m13657F8750510D0255DF1E87944D5053E202073A (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
IL2CPP_EXTERN_C  void ASCIIValueEnumerator_Reset_m13657F8750510D0255DF1E87944D5053E202073A_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271*>(__this + _offset);
	ASCIIValueEnumerator_Reset_m13657F8750510D0255DF1E87944D5053E202073A(_thisAdjusted, method);
}
// System.Byte TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t ASCIIValueEnumerator_get_Current_m777B03023C16D40AE16CCC8B89B55902BA9DBBC1 (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) 
{
	{
		uint8_t L_0 = __this->___U3CCurrentU3Ek__BackingField_1;
		return L_0;
	}
}
IL2CPP_EXTERN_C  uint8_t ASCIIValueEnumerator_get_Current_m777B03023C16D40AE16CCC8B89B55902BA9DBBC1_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271*>(__this + _offset);
	uint8_t _returnValue;
	_returnValue = ASCIIValueEnumerator_get_Current_m777B03023C16D40AE16CCC8B89B55902BA9DBBC1_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.Void TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::set_Current(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ASCIIValueEnumerator_set_Current_m5723F350D36F2E6F7378B24D0C439317DE57BD2C (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, uint8_t ___0_value, const RuntimeMethod* method) 
{
	{
		uint8_t L_0 = ___0_value;
		__this->___U3CCurrentU3Ek__BackingField_1 = L_0;
		return;
	}
}
IL2CPP_EXTERN_C  void ASCIIValueEnumerator_set_Current_m5723F350D36F2E6F7378B24D0C439317DE57BD2C_AdjustorThunk (RuntimeObject* __this, uint8_t ___0_value, const RuntimeMethod* method)
{
	ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271*>(__this + _offset);
	ASCIIValueEnumerator_set_Current_m5723F350D36F2E6F7378B24D0C439317DE57BD2C_inline(_thisAdjusted, ___0_value, method);
}
// System.Object TriLibCore.Fbx.ASCII.ASCIIValueEnumerator::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ASCIIValueEnumerator_System_Collections_IEnumerator_get_Current_m4DF5316DE6B01D44743EB8FD8D492C49BAAA9931 (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint8_t L_0;
		L_0 = ASCIIValueEnumerator_get_Current_m777B03023C16D40AE16CCC8B89B55902BA9DBBC1_inline(__this, NULL);
		uint8_t L_1 = L_0;
		RuntimeObject* L_2 = Box(Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ASCIIValueEnumerator_System_Collections_IEnumerator_get_Current_m4DF5316DE6B01D44743EB8FD8D492C49BAAA9931_AdjustorThunk (RuntimeObject* __this, const RuntimeMethod* method)
{
	ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271*>(__this + _offset);
	RuntimeObject* _returnValue;
	_returnValue = ASCIIValueEnumerator_System_Collections_IEnumerator_get_Current_m4DF5316DE6B01D44743EB8FD8D492C49BAAA9931(_thisAdjusted, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TriLibCore.Fbx.FBXNode TriLibCore.Fbx.ASCII.FBXASCIIReader::ReadDocument()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D FBXASCIIReader_ReadDocument_m42A06BB831534E44B0CFDE179264545AC4875724 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB94DD1963BC6E8F27D446B420BEA01FE03FD8C2B);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = (-1);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_0 = __this->____processor_37;
		NullCheck(L_0);
		L_0->___ActiveASCIIReader_20 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_0->___ActiveASCIIReader_20), (void*)__this);
		V_0 = 1;
		V_1 = 0;
		V_2 = (-1);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_1 = __this->____processor_37;
		FBXNode__ctor_m3D406E7ADA2DA527D69C7573E4866196EE3DD631((&V_3), L_1, _stringLiteralB94DD1963BC6E8F27D446B420BEA01FE03FD8C2B, (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418*)NULL, NULL);
		int32_t L_2;
		L_2 = FBXASCIIReader_ReadAllNodes_mD1FE99994685F3FCCEF4B906535C7DD60EE833FB(__this, (&V_0), (&V_1), (&V_2), (bool)0, NULL);
		int32_t L_3 = V_2;
		(&V_3)->___FirstNodeIndex_3 = L_3;
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_4 = __this->____processor_37;
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6;
		L_6 = FBXProcessor_AddNode_m39374DBF94E11AB90240A841029D230D62AC872D(L_4, L_5, NULL);
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_7 = V_3;
		return L_7;
	}
}
// System.Void TriLibCore.Fbx.ASCII.FBXASCIIReader::.ctor(TriLibCore.Fbx.FBXProcessor,System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIIReader__ctor_mEB75301BADCB18697945E53B545B3A5B5E9787C7 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* ___0_processor, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___1_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mC679EFF5E634878F1897D71DC5160A96EA719E82_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* L_0 = (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7*)il2cpp_codegen_object_new(List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_mC679EFF5E634878F1897D71DC5160A96EA719E82(L_0, List_1__ctor_mC679EFF5E634878F1897D71DC5160A96EA719E82_RuntimeMethod_var);
		__this->____characters_38 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____characters_38), (void*)L_0);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_1 = ___1_stream;
		FBXASCIITokenizer__ctor_mAEBC0B5E80C2125EA25B93C7A667B5A32E973EF3(__this, L_1, NULL);
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_2 = ___0_processor;
		__this->____processor_37 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____processor_37), (void*)L_2);
		return;
	}
}
// System.Int32 TriLibCore.Fbx.ASCII.FBXASCIIReader::ReadAllNodes(System.Int32&,System.Int32&,System.Int32&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FBXASCIIReader_ReadAllNodes_mD1FE99994685F3FCCEF4B906535C7DD60EE833FB (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, int32_t* ___0_nodesCount, int32_t* ___1_propertiesCount, int32_t* ___2_firstNodeIndex, bool ___3_countOnly, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	{
		FBXASCIITokenizer_Reset_m34277EFD55E370A438B73A32DF14ECDDF517B491(__this, NULL);
		V_0 = (-1);
		V_1 = 0;
		V_2 = (-1);
		V_3 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		goto IL_0025;
	}

IL_0014:
	{
		int32_t* L_0 = ___0_nodesCount;
		int32_t* L_1 = ___1_propertiesCount;
		int32_t* L_2 = ___2_firstNodeIndex;
		bool L_3 = ___3_countOnly;
		String_t* L_4 = V_3;
		FBXASCIIReader_ReadAndProcessNode_mB0246287DC757CB4D5B4762FE8A833CB091EAA38(__this, L_0, L_1, L_2, L_3, (bool)0, (&V_0), (&V_2), L_4, NULL);
	}

IL_0025:
	{
		bool L_5;
		L_5 = FBXASCIITokenizer_get_EndOfStream_m5BD0A303ABE71F24333565BD20C1AE51ED737D3A(__this, NULL);
		if (!L_5)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_6 = V_1;
		return L_6;
	}
}
// System.Void TriLibCore.Fbx.ASCII.FBXASCIIReader::ReadAndProcessNode(System.Int32&,System.Int32&,System.Int32&,System.Boolean,System.Boolean,System.Int32&,System.Int32&,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIIReader_ReadAndProcessNode_mB0246287DC757CB4D5B4762FE8A833CB091EAA38 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, int32_t* ___0_nodesCount, int32_t* ___1_propertiesCount, int32_t* ___2_firstNodeIndex, bool ___3_countOnly, bool ___4_isArrayParent, int32_t* ___5_lastNodeIndex, int32_t* ___6_basePropertyIndex, String_t* ___7_path, const RuntimeMethod* method) 
{
	FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		int64_t L_0;
		L_0 = FBXASCIITokenizer_ReadNextValidToken_m1837A0CD8AD6CCAE906F9B4CE0C1D3C42B191A6C(__this, (bool)0, (bool)0, (bool)0, NULL);
		int64_t L_1 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if ((((int64_t)L_1) == ((int64_t)((int64_t)34902897112120632LL))))
		{
			goto IL_0023;
		}
	}
	{
		bool L_2;
		L_2 = FBXASCIITokenizer_get_EndOfStream_m5BD0A303ABE71F24333565BD20C1AE51ED737D3A(__this, NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}

IL_0023:
	{
		return;
	}

IL_0024:
	{
		int32_t* L_3 = ___0_nodesCount;
		int32_t* L_4 = ___1_propertiesCount;
		bool L_5 = ___3_countOnly;
		bool L_6 = ___4_isArrayParent;
		String_t* L_7 = ___7_path;
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_8;
		L_8 = FBXASCIIReader_ReadNode_mD0B13F651A7A4B2C320F97780487910AC89131A5(__this, L_3, L_4, L_5, L_6, L_7, NULL);
		V_0 = L_8;
		bool L_9 = ___3_countOnly;
		if (L_9)
		{
			goto IL_0074;
		}
	}
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_10 = __this->____processor_37;
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12;
		L_12 = FBXProcessor_AddNode_m39374DBF94E11AB90240A841029D230D62AC872D(L_10, L_11, NULL);
		V_1 = L_12;
		int32_t* L_13 = ___5_lastNodeIndex;
		int32_t L_14 = *((int32_t*)L_13);
		if ((!(((uint32_t)L_14) == ((uint32_t)(-1)))))
		{
			goto IL_0061;
		}
	}
	{
		bool L_15 = ___4_isArrayParent;
		if (!L_15)
		{
			goto IL_005c;
		}
	}
	{
		int32_t* L_16 = ___6_basePropertyIndex;
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_17 = V_0;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_18 = L_17.___Properties_0;
		NullCheck(L_18);
		int32_t L_19 = L_18->___BasePropertyIndex_2;
		*((int32_t*)L_16) = (int32_t)L_19;
	}

IL_005c:
	{
		int32_t* L_20 = ___2_firstNodeIndex;
		int32_t L_21 = V_1;
		*((int32_t*)L_20) = (int32_t)L_21;
		goto IL_0070;
	}

IL_0061:
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_22 = __this->____processor_37;
		int32_t* L_23 = ___5_lastNodeIndex;
		int32_t L_24 = *((int32_t*)L_23);
		int32_t L_25 = V_1;
		NullCheck(L_22);
		FBXProcessor_SetNodeNext_m8630F0DDE868E67E1C3EBE3BE0F8ABBA8BF5484C(L_22, L_24, L_25, NULL);
	}

IL_0070:
	{
		int32_t* L_26 = ___5_lastNodeIndex;
		int32_t L_27 = V_1;
		*((int32_t*)L_26) = (int32_t)L_27;
	}

IL_0074:
	{
		return;
	}
}
// TriLibCore.Fbx.FBXNode TriLibCore.Fbx.ASCII.FBXASCIIReader::ReadNode(System.Int32&,System.Int32&,System.Boolean,System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D FBXASCIIReader_ReadNode_mD0B13F651A7A4B2C320F97780487910AC89131A5 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, int32_t* ___0_nodesCount, int32_t* ___1_propertiesCount, bool ___2_countOnly, bool ___3_isArrayChild, String_t* ___4_path, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	bool V_4 = false;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	bool V_7 = false;
	FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D V_8;
	memset((&V_8), 0, sizeof(V_8));
	bool V_9 = false;
	FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 V_10;
	memset((&V_10), 0, sizeof(V_10));
	bool V_11 = false;
	FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* V_12 = NULL;
	{
		String_t* L_0;
		L_0 = FBXASCIITokenizer_GetTokenAsString_m09A7D0E5F1E872CC4125CA77E8310FD54DCD42B7(__this, (bool)0, NULL);
		V_0 = L_0;
		int64_t L_1;
		L_1 = FBXASCIITokenizer_ReadNextValidToken_m1837A0CD8AD6CCAE906F9B4CE0C1D3C42B191A6C(__this, (bool)1, (bool)0, (bool)0, NULL);
		int64_t L_2 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if ((((int64_t)L_2) == ((int64_t)((int64_t)34902897112120565LL))))
		{
			goto IL_002e;
		}
	}
	{
		Exception_t* L_3 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_3);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC1E07B7875C9A8699F7BE475BDAF491BCBC03B04)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FBXASCIIReader_ReadNode_mD0B13F651A7A4B2C320F97780487910AC89131A5_RuntimeMethod_var)));
	}

IL_002e:
	{
		V_1 = (-1);
		V_2 = (-1);
		V_3 = 0;
		V_4 = (bool)0;
		int64_t L_4;
		L_4 = FBXASCIITokenizer_ReadNextValidToken_m1837A0CD8AD6CCAE906F9B4CE0C1D3C42B191A6C(__this, (bool)0, (bool)0, (bool)1, NULL);
		goto IL_0170;
	}

IL_0046:
	{
		int64_t L_5 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if ((((int64_t)L_5) == ((int64_t)((int64_t)34902897112120551LL))))
		{
			goto IL_0148;
		}
	}
	{
		int64_t L_6 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if (!L_6)
		{
			goto IL_0148;
		}
	}
	{
		bool L_7 = ___3_isArrayChild;
		if (L_7)
		{
			goto IL_0099;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_8 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____charStream_23;
		NullCheck(L_8);
		int32_t L_9 = 0;
		uint16_t L_10 = (uint16_t)(L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)42)))))
		{
			goto IL_0099;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_11 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____charStream_23;
		int32_t L_12 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____charPosition_28;
		String_t* L_13;
		L_13 = String_CreateString_mB7B3AC2AF28010538650051A9000369B1CD6BAB6(NULL, L_11, 1, ((int32_t)il2cpp_codegen_subtract(L_12, 1)), NULL);
		bool L_14;
		L_14 = Int32_TryParse_mC928DE2FEC1C35ED5298BDDCA9868076E94B8A21(L_13, (&V_1), NULL);
		V_4 = (bool)1;
		goto IL_0148;
	}

IL_0099:
	{
		bool L_15 = ___2_countOnly;
		if (L_15)
		{
			goto IL_0144;
		}
	}
	{
		il2cpp_codegen_initobj((&V_10), sizeof(FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8));
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_16 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____charStream_23;
		NullCheck(L_16);
		int32_t L_17 = 0;
		uint16_t L_18 = (uint16_t)(L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_11 = (bool)((((int32_t)L_18) == ((int32_t)((int32_t)34)))? 1 : 0);
		bool L_19 = V_11;
		if (!L_19)
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_20 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____charPosition_28;
		if ((((int32_t)L_20) > ((int32_t)1)))
		{
			goto IL_00c6;
		}
	}

IL_00c2:
	{
		bool L_21 = V_11;
		if (L_21)
		{
			goto IL_0127;
		}
	}

IL_00c6:
	{
		int32_t L_22 = V_1;
		if ((!(((uint32_t)L_22) == ((uint32_t)(-1)))))
		{
			goto IL_00cc;
		}
	}
	{
		V_1 = 1;
	}

IL_00cc:
	{
		bool L_23 = V_11;
		if (!L_23)
		{
			goto IL_00ff;
		}
	}
	{
		int64_t L_24 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____tokenInitialPosition_29;
		(&V_10)->___Position_0 = ((int64_t)il2cpp_codegen_add(L_24, ((int64_t)1)));
		int32_t L_25 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____charPosition_28;
		(&V_10)->___StringCharLength_1 = (uint16_t)((int32_t)(uint16_t)((int32_t)il2cpp_codegen_subtract(L_25, 2)));
		bool L_26 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____overflow_36;
		(&V_10)->___Overflow_2 = L_26;
		goto IL_0127;
	}

IL_00ff:
	{
		int64_t L_27 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____tokenInitialPosition_29;
		(&V_10)->___Position_0 = L_27;
		int32_t L_28 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____charPosition_28;
		(&V_10)->___StringCharLength_1 = (uint16_t)((int32_t)(uint16_t)L_28);
		bool L_29 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____overflow_36;
		(&V_10)->___Overflow_2 = L_29;
	}

IL_0127:
	{
		int32_t L_30 = V_2;
		if ((!(((uint32_t)L_30) == ((uint32_t)(-1)))))
		{
			goto IL_0137;
		}
	}
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_31 = __this->____processor_37;
		NullCheck(L_31);
		int32_t L_32;
		L_32 = FBXProcessor_get_PropertiesCount_mD3C7369713E84F01A93BE54947EE62B2FC2C05DE(L_31, NULL);
		V_2 = L_32;
	}

IL_0137:
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_33 = __this->____processor_37;
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_34 = V_10;
		NullCheck(L_33);
		FBXProcessor_AddProperty_m013216FFEAA69120137545F668B3CA08405BDCA3(L_33, L_34, NULL);
	}

IL_0144:
	{
		int32_t L_35 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_35, 1));
	}

IL_0148:
	{
		int64_t L_36 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		V_9 = (bool)((((int64_t)L_36) == ((int64_t)((int64_t)34902897112120551LL)))? 1 : 0);
	}

IL_015b:
	{
		int64_t L_37;
		L_37 = FBXASCIITokenizer_ReadToken_m308AD080344BD120BDE757662F380A80C8867DC3(__this, (bool)0, (bool)0, NULL);
		bool L_38 = V_9;
		if (!L_38)
		{
			goto IL_0170;
		}
	}
	{
		int64_t L_39 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if (!L_39)
		{
			goto IL_015b;
		}
	}

IL_0170:
	{
		bool L_40 = ___3_isArrayChild;
		if (!L_40)
		{
			goto IL_0188;
		}
	}
	{
		int64_t L_41 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if ((!(((uint64_t)L_41) == ((uint64_t)((int64_t)34902897112120632LL)))))
		{
			goto IL_0046;
		}
	}

IL_0188:
	{
		bool L_42 = ___3_isArrayChild;
		if (L_42)
		{
			goto IL_01a8;
		}
	}
	{
		int64_t L_43 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if (!L_43)
		{
			goto IL_01a8;
		}
	}
	{
		int64_t L_44 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if ((!(((uint64_t)L_44) == ((uint64_t)((int64_t)34902897112120630LL)))))
		{
			goto IL_0046;
		}
	}

IL_01a8:
	{
		int32_t* L_45 = ___1_propertiesCount;
		int32_t* L_46 = ___1_propertiesCount;
		int32_t L_47 = *((int32_t*)L_46);
		int32_t L_48 = V_3;
		*((int32_t*)L_45) = (int32_t)((int32_t)il2cpp_codegen_add(L_47, L_48));
		V_5 = (-1);
		V_6 = (-1);
		int64_t L_49 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		V_7 = (bool)((((int64_t)L_49) == ((int64_t)((int64_t)34902897112120630LL)))? 1 : 0);
		bool L_50 = ___3_isArrayChild;
		if (L_50)
		{
			goto IL_0209;
		}
	}
	{
		goto IL_01e0;
	}

IL_01cd:
	{
		int32_t* L_51 = ___0_nodesCount;
		int32_t* L_52 = ___1_propertiesCount;
		bool L_53 = ___2_countOnly;
		bool L_54 = V_4;
		String_t* L_55 = ___4_path;
		FBXASCIIReader_ReadAndProcessNode_mB0246287DC757CB4D5B4762FE8A833CB091EAA38(__this, L_51, L_52, (&V_5), L_53, L_54, (&V_6), (&V_2), L_55, NULL);
	}

IL_01e0:
	{
		bool L_56;
		L_56 = FBXASCIITokenizer_get_EndOfStream_m5BD0A303ABE71F24333565BD20C1AE51ED737D3A(__this, NULL);
		if (L_56)
		{
			goto IL_0209;
		}
	}
	{
		bool L_57 = V_7;
		if (!L_57)
		{
			goto IL_01fd;
		}
	}
	{
		int64_t L_58 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if ((!(((uint64_t)L_58) == ((uint64_t)((int64_t)34902897112120632LL)))))
		{
			goto IL_01cd;
		}
	}

IL_01fd:
	{
		bool L_59 = V_7;
		if (L_59)
		{
			goto IL_0209;
		}
	}
	{
		int64_t L_60 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if (L_60)
		{
			goto IL_01cd;
		}
	}

IL_0209:
	{
		il2cpp_codegen_initobj((&V_8), sizeof(FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D));
		bool L_61 = ___2_countOnly;
		if (L_61)
		{
			goto IL_023d;
		}
	}
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_62 = __this->____processor_37;
		int32_t L_63 = V_3;
		int32_t L_64 = V_2;
		int32_t L_65 = V_1;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_66 = (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418*)il2cpp_codegen_object_new(FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418_il2cpp_TypeInfo_var);
		NullCheck(L_66);
		FBXProperties__ctor_m26D28D238479767B133F249512E1A3BB2EBC5D60(L_66, L_62, L_63, L_64, L_65, NULL);
		V_12 = L_66;
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_67 = __this->____processor_37;
		String_t* L_68 = V_0;
		FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* L_69 = V_12;
		FBXNode__ctor_m3D406E7ADA2DA527D69C7573E4866196EE3DD631((&V_8), L_67, L_68, L_69, NULL);
		int32_t L_70 = V_5;
		(&V_8)->___FirstNodeIndex_3 = L_70;
	}

IL_023d:
	{
		bool L_71 = V_7;
		if (!L_71)
		{
			goto IL_025a;
		}
	}
	{
		int64_t L_72 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30;
		if ((!(((uint64_t)L_72) == ((uint64_t)((int64_t)34902897112120632LL)))))
		{
			goto IL_025a;
		}
	}
	{
		((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____lastToken_30 = ((int64_t)0);
	}

IL_025a:
	{
		int32_t* L_73 = ___0_nodesCount;
		int32_t* L_74 = ___0_nodesCount;
		int32_t L_75 = *((int32_t*)L_74);
		*((int32_t*)L_73) = (int32_t)((int32_t)il2cpp_codegen_add(L_75, 1));
		FBXNode_t54F7549A02850CCDB0C55E0896BEE3EDFE679A1D L_76 = V_8;
		return L_76;
	}
}
// System.Void TriLibCore.Fbx.ASCII.FBXASCIIReader::CopyCharStreamToString(System.String,System.Char[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIIReader_CopyCharStreamToString_m464EF2AAC5CAD52B3343EDB7FC564F32D4DA33A3 (String_t* ___0_s, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___1_cs, int32_t ___2_charCount, const RuntimeMethod* method) 
{
	Il2CppChar* V_0 = NULL;
	String_t* V_1 = NULL;
	Il2CppChar* V_2 = NULL;
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* V_3 = NULL;
	{
		String_t* L_0 = ___0_s;
		V_1 = L_0;
		String_t* L_1 = V_1;
		V_0 = (Il2CppChar*)((uintptr_t)L_1);
		Il2CppChar* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		Il2CppChar* L_3 = V_0;
		int32_t L_4;
		L_4 = RuntimeHelpers_get_OffsetToStringData_m90A5D27EF88BE9432BF7093B7D7E7A0ACB0A8FBD(NULL);
		V_0 = ((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_3, L_4));
	}

IL_0010:
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_5 = ___1_cs;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_6 = L_5;
		V_3 = L_6;
		if (!L_6)
		{
			goto IL_001a;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_7 = V_3;
		NullCheck(L_7);
		if (((int32_t)(((RuntimeArray*)L_7)->max_length)))
		{
			goto IL_001f;
		}
	}

IL_001a:
	{
		V_2 = (Il2CppChar*)((uintptr_t)0);
		goto IL_0028;
	}

IL_001f:
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_8 = V_3;
		NullCheck(L_8);
		V_2 = (Il2CppChar*)((uintptr_t)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
	}

IL_0028:
	{
		Il2CppChar* L_9 = V_0;
		String_t* L_10 = ___0_s;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_10, NULL);
		UnsafeUtility_MemSet_m4CD74CD43260EB2962A46F57E0D93DD5C332FC2B((void*)L_9, (uint8_t)0, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_11, 2))), NULL);
		Il2CppChar* L_12 = V_0;
		Il2CppChar* L_13 = V_2;
		int32_t L_14 = ___2_charCount;
		String_t* L_15 = ___0_s;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_15, NULL);
		int32_t L_17;
		L_17 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(L_14, L_16, NULL);
		UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177((void*)L_12, (void*)L_13, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_17, 2))), NULL);
		V_3 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)NULL;
		V_1 = (String_t*)NULL;
		return;
	}
}
// System.String TriLibCore.Fbx.ASCII.FBXASCIIReader::GetPropertyStringValue(TriLibCore.Fbx.FBXProperty,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FBXASCIIReader_GetPropertyStringValue_mAB7F8381F40834393878E78F0E0C647862027044 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 ___0_property, bool ___1_isNumber, bool ___2_hashOnly, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF3227B0AAB9F7FED1883246395F1CEA0D0B06DC5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m0586B319F89682059DD157C1EDC282A2888ECB9B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_Concat_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_m691C56B3F5A4F992794166066A4B0D10B8B74461_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0;
		L_0 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_1 = ___0_property;
		int64_t L_2 = L_1.___Position_0;
		NullCheck(L_0);
		VirtualActionInvoker1< int64_t >::Invoke(13 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_0, L_2);
		bool L_3 = ___1_isNumber;
		if (!L_3)
		{
			goto IL_0065;
		}
	}
	{
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_4 = ___0_property;
		uint16_t L_5 = L_4.___StringCharLength_1;
		int32_t L_6;
		L_6 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(L_5, ((int32_t)24), NULL);
		V_0 = L_6;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_7 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____numberByteStream_25;
		int32_t L_8 = V_0;
		int32_t L_9;
		L_9 = BinaryReader_Read_mB4A2BC93CA8D038475030D490E9732CD1B8ABDBA(__this, L_7, 0, L_8, NULL);
		V_1 = L_9;
		bool L_10 = ___2_hashOnly;
		if (!L_10)
		{
			goto IL_004c;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_11 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____numberByteStream_25;
		int32_t L_12 = V_1;
		int64_t L_13;
		L_13 = HashUtils_GetHash_mA8618222D626B54CD67FF88D56037B6D8024453E((RuntimeObject*)L_11, L_12, NULL);
		((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->___CharHash_35 = L_13;
		String_t* L_14 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->___Empty_6;
		return L_14;
	}

IL_004c:
	{
		String_t* L_15 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____numberString_26;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_16 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____numberByteStream_25;
		int32_t L_17 = V_1;
		FBXASCIIReader_CopyCharStreamToString_m464EF2AAC5CAD52B3343EDB7FC564F32D4DA33A3(L_15, L_16, L_17, NULL);
		String_t* L_18 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____numberString_26;
		return L_18;
	}

IL_0065:
	{
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_19 = ___0_property;
		bool L_20 = L_19.___Overflow_2;
		if (!L_20)
		{
			goto IL_009e;
		}
	}
	{
		List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* L_21 = __this->____characters_38;
		NullCheck(L_21);
		List_1_Clear_m0586B319F89682059DD157C1EDC282A2888ECB9B_inline(L_21, List_1_Clear_m0586B319F89682059DD157C1EDC282A2888ECB9B_RuntimeMethod_var);
	}

IL_0078:
	{
		Il2CppChar L_22;
		L_22 = BinaryReader_ReadChar_mBFCF423F230C6281515CC26769EBBEC92C18EC6D(__this, NULL);
		V_2 = L_22;
		Il2CppChar L_23 = V_2;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)34))))
		{
			goto IL_0092;
		}
	}
	{
		List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* L_24 = __this->____characters_38;
		Il2CppChar L_25 = V_2;
		NullCheck(L_24);
		List_1_Add_mF3227B0AAB9F7FED1883246395F1CEA0D0B06DC5_inline(L_24, L_25, List_1_Add_mF3227B0AAB9F7FED1883246395F1CEA0D0B06DC5_RuntimeMethod_var);
		goto IL_0078;
	}

IL_0092:
	{
		List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* L_26 = __this->____characters_38;
		String_t* L_27;
		L_27 = String_Concat_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_m691C56B3F5A4F992794166066A4B0D10B8B74461(L_26, String_Concat_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_m691C56B3F5A4F992794166066A4B0D10B8B74461_RuntimeMethod_var);
		return L_27;
	}

IL_009e:
	{
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_28 = ___0_property;
		uint16_t L_29 = L_28.___StringCharLength_1;
		int32_t L_30;
		L_30 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(L_29, ((int32_t)4096), NULL);
		V_3 = L_30;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_31 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____stringByteStream_27;
		int32_t L_32 = V_3;
		int32_t L_33;
		L_33 = BinaryReader_Read_mB4A2BC93CA8D038475030D490E9732CD1B8ABDBA(__this, L_31, 0, L_32, NULL);
		V_4 = L_33;
		bool L_34 = ___2_hashOnly;
		if (!L_34)
		{
			goto IL_00db;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_35 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____stringByteStream_27;
		int32_t L_36 = V_4;
		int64_t L_37;
		L_37 = HashUtils_GetHash_mA8618222D626B54CD67FF88D56037B6D8024453E((RuntimeObject*)L_35, L_36, NULL);
		((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->___CharHash_35 = L_37;
		String_t* L_38 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->___Empty_6;
		return L_38;
	}

IL_00db:
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_39 = ((FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E*)__this)->____stringByteStream_27;
		int32_t L_40 = V_4;
		String_t* L_41;
		L_41 = String_CreateString_mB7B3AC2AF28010538650051A9000369B1CD6BAB6(NULL, L_39, 0, L_40, NULL);
		return L_41;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Byte> TriLibCore.Fbx.ASCII.FBXASCIIReader::GetPropertyStringValueEnumerator(TriLibCore.Fbx.FBXProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FBXASCIIReader_GetPropertyStringValueEnumerator_m77DC722886D5BA9620EDDC4C3BF0665024D0DF25 (FBXASCIIReader_t708E1B27365A8EA390B9216EDCE3A9A84A359C3B* __this, FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 ___0_property, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0;
		L_0 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		FBXProperty_t4A335F9B7A2971C2B2AA897B793C06A1849E9BD8 L_1 = ___0_property;
		int64_t L_2 = L_1.___Position_0;
		NullCheck(L_0);
		VirtualActionInvoker1< int64_t >::Invoke(13 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_0, L_2);
		ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271 L_3;
		memset((&L_3), 0, sizeof(L_3));
		ASCIIValueEnumerator__ctor_mCC8BD1780C632806437D4E4F3D8559D85EF066EA((&L_3), __this, /*hidden argument*/NULL);
		ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271 L_4 = L_3;
		RuntimeObject* L_5 = Box(ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271_il2cpp_TypeInfo_var, &L_4);
		return (RuntimeObject*)L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int64 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::get_Position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t FBXASCIITokenizer_get_Position_mA20527663B6B3E9A3FB744DAA0FDB910ADD8F5ED (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0;
		L_0 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_0);
		int64_t L_1;
		L_1 = VirtualFuncInvoker0< int64_t >::Invoke(12 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		return L_1;
	}
}
// System.Boolean TriLibCore.Fbx.ASCII.FBXASCIITokenizer::get_EndOfStream()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FBXASCIITokenizer_get_EndOfStream_m5BD0A303ABE71F24333565BD20C1AE51ED737D3A (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0;
		L_0 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_0);
		int64_t L_1;
		L_1 = VirtualFuncInvoker0< int64_t >::Invoke(12 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2;
		L_2 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_2);
		int64_t L_3;
		L_3 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Length() */, L_2);
		return (bool)((((int32_t)((((int64_t)L_1) < ((int64_t)L_3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void TriLibCore.Fbx.ASCII.FBXASCIITokenizer::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIITokenizer__ctor_mAEBC0B5E80C2125EA25B93C7A667B5A32E973EF3 (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)4096));
		__this->____charStream_23 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charStream_23), (void*)L_0);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_1 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24));
		__this->____numberByteStream_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____numberByteStream_25), (void*)L_1);
		String_t* L_2;
		L_2 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)24), NULL);
		__this->____numberString_26 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____numberString_26), (void*)L_2);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_3 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)4096));
		__this->____stringByteStream_27 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____stringByteStream_27), (void*)L_3);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_4 = ___0_stream;
		Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* L_5;
		L_5 = Encoding_get_UTF8_m9FA98A53CE96FD6D02982625C5246DD36C1235C9(NULL);
		BinaryReader__ctor_mD85F293A64917055AA78D504B87E5F7B81E4FD46(__this, L_4, L_5, NULL);
		return;
	}
}
// System.Void TriLibCore.Fbx.ASCII.FBXASCIITokenizer::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIITokenizer_Reset_m34277EFD55E370A438B73A32DF14ECDDF517B491 (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0;
		L_0 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, __this);
		NullCheck(L_0);
		int64_t L_1;
		L_1 = VirtualFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(32 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_0, ((int64_t)0), 0);
		return;
	}
}
// System.Int64 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::ReadNextValidToken(System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t FBXASCIITokenizer_ReadNextValidToken_m1837A0CD8AD6CCAE906F9B4CE0C1D3C42B191A6C (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, bool ___0_required, bool ___1_ignoreSpace, bool ___2_ignoreEndOfLine, const RuntimeMethod* method) 
{
	int64_t V_0 = 0;

IL_0000:
	{
		bool L_0 = ___1_ignoreSpace;
		int64_t L_1;
		L_1 = FBXASCIITokenizer_ReadToken_m308AD080344BD120BDE757662F380A80C8867DC3(__this, (bool)0, L_0, NULL);
		V_0 = L_1;
		bool L_2 = ___2_ignoreEndOfLine;
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		int64_t L_3 = V_0;
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		bool L_4;
		L_4 = FBXASCIITokenizer_get_EndOfStream_m5BD0A303ABE71F24333565BD20C1AE51ED737D3A(__this, NULL);
		if (!L_4)
		{
			goto IL_0000;
		}
	}

IL_0017:
	{
		int64_t L_5 = V_0;
		return L_5;
	}
}
// System.Int64 TriLibCore.Fbx.ASCII.FBXASCIITokenizer::ReadToken(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t FBXASCIITokenizer_ReadToken_m308AD080344BD120BDE757662F380A80C8867DC3 (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, bool ___0_required, bool ___1_ignoreSpaces, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	int64_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppChar V_5 = 0x0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	Il2CppChar V_8 = 0x0;
	int32_t G_B32_0 = 0;
	int64_t G_B69_0 = 0;
	{
		__this->____charPosition_28 = 0;
		__this->____overflow_36 = (bool)0;
	}

IL_000e:
	{
		V_0 = (bool)0;
		V_1 = (bool)1;
		V_2 = (bool)0;
		goto IL_00d9;
	}

IL_0019:
	{
		int32_t L_0;
		L_0 = BinaryReader_PeekChar_mA6B7481DCCB2A265E54D1F200ED8BEAD00CD971A(__this, NULL);
		V_4 = L_0;
		int32_t L_1 = V_4;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_2 = V_4;
		V_5 = ((int32_t)(uint16_t)L_2);
		Il2CppChar L_3 = V_5;
		if ((!(((uint32_t)L_3) <= ((uint32_t)((int32_t)34)))))
		{
			goto IL_006f;
		}
	}
	{
		Il2CppChar L_4 = V_5;
		if ((!(((uint32_t)L_4) <= ((uint32_t)((int32_t)13)))))
		{
			goto IL_0061;
		}
	}
	{
		Il2CppChar L_5 = V_5;
		if (!L_5)
		{
			goto IL_00ca;
		}
	}
	{
		Il2CppChar L_6 = V_5;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, ((int32_t)9))))
		{
			case 0:
			{
				goto IL_00ca;
			}
			case 1:
			{
				goto IL_0097;
			}
			case 2:
			{
				goto IL_00d3;
			}
			case 3:
			{
				goto IL_00d3;
			}
			case 4:
			{
				goto IL_00ca;
			}
		}
	}
	{
		goto IL_00d3;
	}

IL_0061:
	{
		Il2CppChar L_7 = V_5;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)32))))
		{
			goto IL_00ca;
		}
	}
	{
		Il2CppChar L_8 = V_5;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)34))))
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00d3;
	}

IL_006f:
	{
		Il2CppChar L_9 = V_5;
		if ((!(((uint32_t)L_9) <= ((uint32_t)((int32_t)58)))))
		{
			goto IL_0083;
		}
	}
	{
		Il2CppChar L_10 = V_5;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)44))))
		{
			goto IL_00aa;
		}
	}
	{
		Il2CppChar L_11 = V_5;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)58))))
		{
			goto IL_00aa;
		}
	}
	{
		goto IL_00d3;
	}

IL_0083:
	{
		Il2CppChar L_12 = V_5;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)59))))
		{
			goto IL_00ae;
		}
	}
	{
		Il2CppChar L_13 = V_5;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)123))))
		{
			goto IL_00aa;
		}
	}
	{
		Il2CppChar L_14 = V_5;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)125))))
		{
			goto IL_00aa;
		}
	}
	{
		goto IL_00d3;
	}

IL_0097:
	{
		int32_t L_15;
		L_15 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.BinaryReader::Read() */, __this);
		V_2 = (bool)1;
		V_1 = (bool)0;
		goto IL_00d9;
	}

IL_00a4:
	{
		V_0 = (bool)1;
		V_1 = (bool)0;
		goto IL_00d9;
	}

IL_00aa:
	{
		V_1 = (bool)0;
		goto IL_00d9;
	}

IL_00ae:
	{
		int32_t L_16;
		L_16 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.BinaryReader::Read() */, __this);
		V_6 = L_16;
		int32_t L_17 = V_6;
		if ((((int32_t)L_17) < ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		int32_t L_18 = V_6;
		V_5 = ((int32_t)(uint16_t)L_18);
		Il2CppChar L_19 = V_5;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00ae;
		}
	}

IL_00c6:
	{
		V_1 = (bool)0;
		goto IL_00d9;
	}

IL_00ca:
	{
		int32_t L_20;
		L_20 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.BinaryReader::Read() */, __this);
		goto IL_00d9;
	}

IL_00d3:
	{
		V_1 = (bool)0;
		goto IL_00d9;
	}

IL_00d7:
	{
		V_1 = (bool)0;
	}

IL_00d9:
	{
		bool L_21 = V_1;
		if (L_21)
		{
			goto IL_0019;
		}
	}
	{
		bool L_22 = V_2;
		if (L_22)
		{
			goto IL_00ed;
		}
	}
	{
		bool L_23;
		L_23 = FBXASCIITokenizer_get_EndOfStream_m5BD0A303ABE71F24333565BD20C1AE51ED737D3A(__this, NULL);
		G_B32_0 = ((((int32_t)L_23) == ((int32_t)0))? 1 : 0);
		goto IL_00ee;
	}

IL_00ed:
	{
		G_B32_0 = 0;
	}

IL_00ee:
	{
		V_1 = (bool)G_B32_0;
		goto IL_01cc;
	}

IL_00f4:
	{
		int32_t L_24;
		L_24 = BinaryReader_PeekChar_mA6B7481DCCB2A265E54D1F200ED8BEAD00CD971A(__this, NULL);
		V_7 = L_24;
		bool L_25 = V_0;
		if (L_25)
		{
			goto IL_0108;
		}
	}
	{
		int32_t L_26 = V_7;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)59))))
		{
			goto IL_000e;
		}
	}

IL_0108:
	{
		int32_t L_27 = V_7;
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_01ca;
		}
	}
	{
		int32_t L_28 = V_7;
		V_8 = ((int32_t)(uint16_t)L_28);
		bool L_29 = V_0;
		if (!L_29)
		{
			goto IL_013c;
		}
	}
	{
		Il2CppChar L_30 = V_8;
		FBXASCIITokenizer_SetCharAndAdvance_m29226B42C00CD73A6A1CB0C706F046AB3729DF4F(__this, L_30, NULL);
		Il2CppChar L_31 = V_8;
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_01cc;
		}
	}
	{
		int32_t L_32 = __this->____charPosition_28;
		if ((((int32_t)L_32) <= ((int32_t)1)))
		{
			goto IL_01cc;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_01cc;
	}

IL_013c:
	{
		Il2CppChar L_33 = V_8;
		if ((!(((uint32_t)L_33) <= ((uint32_t)((int32_t)32)))))
		{
			goto IL_016c;
		}
	}
	{
		Il2CppChar L_34 = V_8;
		if (!L_34)
		{
			goto IL_01a6;
		}
	}
	{
		Il2CppChar L_35 = V_8;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_35, ((int32_t)9))))
		{
			case 0:
			{
				goto IL_01af;
			}
			case 1:
			{
				goto IL_018e;
			}
			case 2:
			{
				goto IL_01c0;
			}
			case 3:
			{
				goto IL_01c0;
			}
			case 4:
			{
				goto IL_01a6;
			}
		}
	}
	{
		Il2CppChar L_36 = V_8;
		if ((((int32_t)L_36) == ((int32_t)((int32_t)32))))
		{
			goto IL_01af;
		}
	}
	{
		goto IL_01c0;
	}

IL_016c:
	{
		Il2CppChar L_37 = V_8;
		if ((!(((uint32_t)L_37) <= ((uint32_t)((int32_t)58)))))
		{
			goto IL_0180;
		}
	}
	{
		Il2CppChar L_38 = V_8;
		if ((((int32_t)L_38) == ((int32_t)((int32_t)44))))
		{
			goto IL_0192;
		}
	}
	{
		Il2CppChar L_39 = V_8;
		if ((((int32_t)L_39) == ((int32_t)((int32_t)58))))
		{
			goto IL_0192;
		}
	}
	{
		goto IL_01c0;
	}

IL_0180:
	{
		Il2CppChar L_40 = V_8;
		if ((((int32_t)L_40) == ((int32_t)((int32_t)123))))
		{
			goto IL_0192;
		}
	}
	{
		Il2CppChar L_41 = V_8;
		if ((((int32_t)L_41) == ((int32_t)((int32_t)125))))
		{
			goto IL_0192;
		}
	}
	{
		goto IL_01c0;
	}

IL_018e:
	{
		V_1 = (bool)0;
		goto IL_01cc;
	}

IL_0192:
	{
		int32_t L_42 = __this->____charPosition_28;
		if (L_42)
		{
			goto IL_01a2;
		}
	}
	{
		Il2CppChar L_43 = V_8;
		FBXASCIITokenizer_SetCharAndAdvance_m29226B42C00CD73A6A1CB0C706F046AB3729DF4F(__this, L_43, NULL);
	}

IL_01a2:
	{
		V_1 = (bool)0;
		goto IL_01cc;
	}

IL_01a6:
	{
		int32_t L_44;
		L_44 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.BinaryReader::Read() */, __this);
		goto IL_01cc;
	}

IL_01af:
	{
		bool L_45 = ___1_ignoreSpaces;
		if (!L_45)
		{
			goto IL_01bc;
		}
	}
	{
		Il2CppChar L_46 = V_8;
		FBXASCIITokenizer_SetCharAndAdvance_m29226B42C00CD73A6A1CB0C706F046AB3729DF4F(__this, L_46, NULL);
		goto IL_01cc;
	}

IL_01bc:
	{
		V_1 = (bool)0;
		goto IL_01cc;
	}

IL_01c0:
	{
		Il2CppChar L_47 = V_8;
		FBXASCIITokenizer_SetCharAndAdvance_m29226B42C00CD73A6A1CB0C706F046AB3729DF4F(__this, L_47, NULL);
		goto IL_01cc;
	}

IL_01ca:
	{
		V_1 = (bool)0;
	}

IL_01cc:
	{
		bool L_48 = V_1;
		if (L_48)
		{
			goto IL_00f4;
		}
	}
	{
		bool L_49 = ___0_required;
		if (!L_49)
		{
			goto IL_01f8;
		}
	}
	{
		int32_t L_50 = __this->____charPosition_28;
		if (L_50)
		{
			goto IL_01f8;
		}
	}
	{
		int64_t L_51;
		L_51 = FBXASCIITokenizer_get_Position_mA20527663B6B3E9A3FB744DAA0FDB910ADD8F5ED(__this, NULL);
		int64_t L_52 = L_51;
		RuntimeObject* L_53 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3_il2cpp_TypeInfo_var)), &L_52);
		String_t* L_54;
		L_54 = String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral0C18F04EBBB957042CCC2056B36799D79564A2DB)), L_53, NULL);
		Exception_t* L_55 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_55);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_55, L_54, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_55, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FBXASCIITokenizer_ReadToken_m308AD080344BD120BDE757662F380A80C8867DC3_RuntimeMethod_var)));
	}

IL_01f8:
	{
		int32_t L_56 = __this->____charPosition_28;
		if (!L_56)
		{
			goto IL_021d;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_57 = __this->____charStream_23;
		int32_t L_58 = __this->____charPosition_28;
		int32_t L_59;
		L_59 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(L_58, ((int32_t)4096), NULL);
		int64_t L_60;
		L_60 = HashUtils_GetHash_mA8618222D626B54CD67FF88D56037B6D8024453E((RuntimeObject*)L_57, L_59, NULL);
		G_B69_0 = L_60;
		goto IL_021f;
	}

IL_021d:
	{
		G_B69_0 = ((int64_t)0);
	}

IL_021f:
	{
		V_3 = G_B69_0;
		int64_t L_61 = V_3;
		__this->____lastToken_30 = L_61;
		int64_t L_62 = V_3;
		return L_62;
	}
}
// System.Void TriLibCore.Fbx.ASCII.FBXASCIITokenizer::SetCharAndAdvance(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBXASCIITokenizer_SetCharAndAdvance_m29226B42C00CD73A6A1CB0C706F046AB3729DF4F (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, Il2CppChar ___0_character, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->____charPosition_28;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		int64_t L_1;
		L_1 = FBXASCIITokenizer_get_Position_mA20527663B6B3E9A3FB744DAA0FDB910ADD8F5ED(__this, NULL);
		__this->____tokenInitialPosition_29 = L_1;
	}

IL_0014:
	{
		int32_t L_2 = __this->____charPosition_28;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_3 = __this->____charStream_23;
		NullCheck(L_3);
		if ((((int32_t)L_2) < ((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length)))))
		{
			goto IL_003b;
		}
	}
	{
		__this->____overflow_36 = (bool)1;
		int32_t L_4 = __this->____charPosition_28;
		__this->____charPosition_28 = ((int32_t)il2cpp_codegen_add(L_4, 1));
		goto IL_0054;
	}

IL_003b:
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_5 = __this->____charStream_23;
		int32_t L_6 = __this->____charPosition_28;
		V_0 = L_6;
		int32_t L_7 = V_0;
		__this->____charPosition_28 = ((int32_t)il2cpp_codegen_add(L_7, 1));
		int32_t L_8 = V_0;
		Il2CppChar L_9 = ___0_character;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Il2CppChar)L_9);
	}

IL_0054:
	{
		int32_t L_10;
		L_10 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.BinaryReader::Read() */, __this);
		return;
	}
}
// System.String TriLibCore.Fbx.ASCII.FBXASCIITokenizer::GetTokenAsString(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FBXASCIITokenizer_GetTokenAsString_m09A7D0E5F1E872CC4125CA77E8310FD54DCD42B7 (FBXASCIITokenizer_tC5E9B470CFC6446E4BAD64BD0CD9BFAF0A9E249E* __this, bool ___0_isString, const RuntimeMethod* method) 
{
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* G_B2_0 = NULL;
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* G_B3_1 = NULL;
	int32_t G_B5_0 = 0;
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* G_B4_1 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* G_B6_2 = NULL;
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = __this->____charStream_23;
		bool L_1 = ___0_isString;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_000d:
	{
		bool L_2 = ___0_isString;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		if (L_2)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			goto IL_0018;
		}
	}
	{
		int32_t L_3 = __this->____charPosition_28;
		G_B6_0 = L_3;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_0020;
	}

IL_0018:
	{
		int32_t L_4 = __this->____charPosition_28;
		G_B6_0 = ((int32_t)il2cpp_codegen_subtract(L_4, 2));
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_0020:
	{
		String_t* L_5;
		L_5 = String_CreateString_mB7B3AC2AF28010538650051A9000369B1CD6BAB6(NULL, G_B6_2, G_B6_1, G_B6_0, NULL);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t U3CPrivateImplementationDetailsU3E_ComputeStringHash_m1AE8873D6A123606BE145B27B7463BCCC4741085 (String_t* ___0_s, const RuntimeMethod* method) 
{
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___0_s;
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		V_0 = ((int32_t)-2128831035);
		V_1 = 0;
		goto IL_0021;
	}

IL_000d:
	{
		String_t* L_1 = ___0_s;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Il2CppChar L_3;
		L_3 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_1, L_2, NULL);
		uint32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_multiply(((int32_t)((int32_t)L_3^(int32_t)L_4)), ((int32_t)16777619)));
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0021:
	{
		int32_t L_6 = V_1;
		String_t* L_7 = ___0_s;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_7, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_000d;
		}
	}

IL_002a:
	{
		uint32_t L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorByte_get_Count_m248A77D652A9BF16734BE54219C5F5EAC9D3C360_inline (PropertyAccessorByte_t3A1E04FFC4110354C2453EFA806F97FC6CEDB899* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* FBXProperties_get_Processor_mBFD247CE63F4102E29CC43CF5AC1625FBFD1B75A_inline (FBXProperties_t5943184AD6922C0B01A0D71557CB584C3CFCB418* __this, const RuntimeMethod* method) 
{
	{
		FBXProcessor_t0DFB3E71E578C546A42206E0D5CAE78764C563F4* L_0 = __this->___U3CProcessorU3Ek__BackingField_1;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorShort_get_Count_m7778AE2F440E033D26ECAC2174D79C054CCEDCE3_inline (PropertyAccessorShort_t8DF6A20D36578B83FD65BEF805562CF32F999DC8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Clamp_m4DC36EEFDBE5F07C16249DA568023C5ECCFF0E7B_inline (int32_t ___0_value, int32_t ___1_min, int32_t ___2_max, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___0_value;
		int32_t L_1 = ___1_min;
		V_0 = (bool)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_3 = ___1_min;
		___0_value = L_3;
		goto IL_0019;
	}

IL_000e:
	{
		int32_t L_4 = ___0_value;
		int32_t L_5 = ___2_max;
		V_1 = (bool)((((int32_t)L_4) > ((int32_t)L_5))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_7 = ___2_max;
		___0_value = L_7;
	}

IL_0019:
	{
		int32_t L_8 = ___0_value;
		V_2 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		int32_t L_9 = V_2;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorIEE754_get_Count_m3DBE759D8CC11737529D4AA5739849E4AEE4A54C_inline (PropertyAccessorIEE754_t0C46BCCB2920674DF4B0DA353E76ECECBC30819C* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_5;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Single_IsNaN_mFE637F6ECA9F7697CE8EFF56427858F4C5EDF75D_inline (float ___0_f, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___0_f;
		il2cpp_codegen_runtime_class_init_inline(BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = BitConverter_SingleToInt32Bits_mC760C7CFC89725E3CF68DC45BE3A9A42A7E7DA73_inline(L_0, NULL);
		return (bool)((((int32_t)((int32_t)(L_1&((int32_t)2147483647LL)))) > ((int32_t)((int32_t)2139095040)))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorInt_get_Count_m0C89ED9BBF123AF37C9647E9D08743150C453D88_inline (PropertyAccessorInt_t24E4E4DFF80422C4D86DD9E20FB16D192D7343F3* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorLong_get_Count_m4848D70EC610CBA6E351059DFA38F3D565B89DF9_inline (PropertyAccessorLong_t9A62C22AD09C973ACA178BE1BAC3A8FFB805C8A7* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_5;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorFloat_get_Count_m0AA67B6688957CA7F3B2E57EACE0882A5A05879C_inline (PropertyAccessorFloat_t1442C7AC1ECC61647B8B16289FAD0AC5A072D96B* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PropertyAccessorDouble_get_Count_mD1726F34A29A7ACEF3A1CF4C1CB12FABE08D7197_inline (PropertyAccessorDouble_t189D059F98C03914177E9547477777DC527180B5* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____count_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ASCIIValueEnumerator_set_Current_m5723F350D36F2E6F7378B24D0C439317DE57BD2C_inline (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, uint8_t ___0_value, const RuntimeMethod* method) 
{
	{
		uint8_t L_0 = ___0_value;
		__this->___U3CCurrentU3Ek__BackingField_1 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t ASCIIValueEnumerator_get_Current_m777B03023C16D40AE16CCC8B89B55902BA9DBBC1_inline (ASCIIValueEnumerator_tEE2DCF9F29327F0DB064132D982C3380C782F271* __this, const RuntimeMethod* method) 
{
	{
		uint8_t L_0 = __this->___U3CCurrentU3Ek__BackingField_1;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline (int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___0_a;
		int32_t L_1 = ___1_b;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_2 = ___1_b;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		int32_t L_3 = ___0_a;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m0586B319F89682059DD157C1EDC282A2888ECB9B_gshared_inline (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		if (!false)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->____size_2;
		V_0 = L_1;
		__this->____size_2 = 0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_3 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)__this->____items_1;
		int32_t L_4 = V_0;
		Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB((RuntimeArray*)L_3, 0, L_4, NULL);
		return;
	}

IL_0035:
	{
		__this->____size_2 = 0;
	}

IL_003c:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mF3227B0AAB9F7FED1883246395F1CEA0D0B06DC5_gshared_inline (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7* __this, Il2CppChar ___0_item, const RuntimeMethod* method) 
{
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_1 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_6 = V_0;
		int32_t L_7 = V_1;
		Il2CppChar L_8 = ___0_item;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (Il2CppChar)L_8);
		return;
	}

IL_0034:
	{
		Il2CppChar L_9 = ___0_item;
		((  void (*) (List_1_t1FDED00FA37F39564E09D01CFDF33C22BCF5C8A7*, Il2CppChar, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitConverter_SingleToInt32Bits_mC760C7CFC89725E3CF68DC45BE3A9A42A7E7DA73_inline (float ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = *((int32_t*)((uintptr_t)(&___0_value)));
		return L_0;
	}
}
