﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Int32 Byn.Awrtc.ByteArrayBuffer::get_PositionWriteRelative()
extern void ByteArrayBuffer_get_PositionWriteRelative_m2CC7D7CDA5AABB1D8FBA5C95F4C7D5D9B6AC611F (void);
// 0x00000002 System.Void Byn.Awrtc.ByteArrayBuffer::set_PositionWriteRelative(System.Int32)
extern void ByteArrayBuffer_set_PositionWriteRelative_m404083D7F5F4D5AC26EB3329E6BEC515DEE9CFB1 (void);
// 0x00000003 System.Int32 Byn.Awrtc.ByteArrayBuffer::get_PositionWriteAbsolute()
extern void ByteArrayBuffer_get_PositionWriteAbsolute_mA19A3E432DF05415EB2D82F69E07F006BDB43152 (void);
// 0x00000004 System.Void Byn.Awrtc.ByteArrayBuffer::set_PositionWriteAbsolute(System.Int32)
extern void ByteArrayBuffer_set_PositionWriteAbsolute_mA7982BB7772E5CE9A6495EB75DED9287D47DAF41 (void);
// 0x00000005 System.Int32 Byn.Awrtc.ByteArrayBuffer::get_PositionReadRelative()
extern void ByteArrayBuffer_get_PositionReadRelative_m6744691413C85626915E746913A3C1A66DC6C53E (void);
// 0x00000006 System.Int32 Byn.Awrtc.ByteArrayBuffer::get_PositionReadAbsolute()
extern void ByteArrayBuffer_get_PositionReadAbsolute_m73983E515DD8910D8F29F1C99AB33A702EA43588 (void);
// 0x00000007 System.Int32 Byn.Awrtc.ByteArrayBuffer::get_Offset()
extern void ByteArrayBuffer_get_Offset_m8D76EF91272F63C71B625CCD459816C32F15C8D8 (void);
// 0x00000008 System.Byte[] Byn.Awrtc.ByteArrayBuffer::get_Buffer()
extern void ByteArrayBuffer_get_Buffer_mF7982705EFA0C018C06567ACD5274EF307B9078E (void);
// 0x00000009 System.Int32 Byn.Awrtc.ByteArrayBuffer::get_ContentLength()
extern void ByteArrayBuffer_get_ContentLength_m3C66B9598E0E265F0EBA86D36EEED3E5BDAC0C54 (void);
// 0x0000000A System.Void Byn.Awrtc.ByteArrayBuffer::set_ContentLength(System.Int32)
extern void ByteArrayBuffer_set_ContentLength_m130FC5A57AC41C623C502434E692EF2118F64429 (void);
// 0x0000000B System.Boolean Byn.Awrtc.ByteArrayBuffer::get_IsDisposed()
extern void ByteArrayBuffer_get_IsDisposed_m2076E51C5E0C308249E6BA3A59824452D8C51AA9 (void);
// 0x0000000C System.Void Byn.Awrtc.ByteArrayBuffer::.ctor(System.Int32)
extern void ByteArrayBuffer__ctor_m48EB301B03B6ACF33C919674178BA6B3385468B9 (void);
// 0x0000000D System.Void Byn.Awrtc.ByteArrayBuffer::.ctor(System.Byte[])
extern void ByteArrayBuffer__ctor_m0DCB1EE93324961765BB0B71BB3EE8895D523B2A (void);
// 0x0000000E System.Void Byn.Awrtc.ByteArrayBuffer::.ctor(System.Byte[],System.Int32,System.Int32)
extern void ByteArrayBuffer__ctor_m72DD481534B3487AE567EF47BD69EFA178E22CF0 (void);
// 0x0000000F System.Void Byn.Awrtc.ByteArrayBuffer::Reset()
extern void ByteArrayBuffer_Reset_mC5250194BCEC35BC3A14C97FDAB601926E8DA1BD (void);
// 0x00000010 System.Void Byn.Awrtc.ByteArrayBuffer::Finalize()
extern void ByteArrayBuffer_Finalize_mF1AB93F0C7A628788DCEC758F9E5688FBB6CC79C (void);
// 0x00000011 System.Void Byn.Awrtc.ByteArrayBuffer::CopyFrom(System.Byte[],System.Int32,System.Int32)
extern void ByteArrayBuffer_CopyFrom_m1FC3E903E65FEB98A5940190026178273D63EBB2 (void);
// 0x00000012 System.Void Byn.Awrtc.ByteArrayBuffer::.cctor()
extern void ByteArrayBuffer__cctor_m1B236CCC4486A29EF105A70BD2288EB494D1556C (void);
// 0x00000013 System.Int32 Byn.Awrtc.ByteArrayBuffer::GetPower(System.UInt32)
extern void ByteArrayBuffer_GetPower_mC394B35C54440081DE7696251C46634A76D8F6BA (void);
// 0x00000014 System.UInt32 Byn.Awrtc.ByteArrayBuffer::NextPowerOfTwo(System.UInt32)
extern void ByteArrayBuffer_NextPowerOfTwo_m0A222D7741963842F06925AF6241627873E7E1AC (void);
// 0x00000015 Byn.Awrtc.ByteArrayBuffer Byn.Awrtc.ByteArrayBuffer::Get(System.Int32,System.Boolean)
extern void ByteArrayBuffer_Get_m3281A3A589DC27B7AA164600AE7B34ED6FF814D2 (void);
// 0x00000016 System.Void Byn.Awrtc.ByteArrayBuffer::Dispose()
extern void ByteArrayBuffer_Dispose_mBDCB25D21BC4CD54EF9DC430E7C5CCE183C5200E (void);
// 0x00000017 System.Void Byn.Awrtc.CallEventHandler::.ctor(System.Object,System.IntPtr)
extern void CallEventHandler__ctor_m3F755A51343911494EB71D72D73E6A0C3E6F8665 (void);
// 0x00000018 System.Void Byn.Awrtc.CallEventHandler::Invoke(System.Object,Byn.Awrtc.CallEventArgs)
extern void CallEventHandler_Invoke_m8035B1A16326D15017A8EEE4583C2CBBA6BA96AD (void);
// 0x00000019 System.IAsyncResult Byn.Awrtc.CallEventHandler::BeginInvoke(System.Object,Byn.Awrtc.CallEventArgs,System.AsyncCallback,System.Object)
extern void CallEventHandler_BeginInvoke_mA193AF7DAD6723410124F2B5FBBB1810353939CC (void);
// 0x0000001A System.Void Byn.Awrtc.CallEventHandler::EndInvoke(System.IAsyncResult)
extern void CallEventHandler_EndInvoke_m48FAEFFA9E06DE23834865FEC86C816233AD3FC3 (void);
// 0x0000001B Byn.Awrtc.CallEventType Byn.Awrtc.CallEventArgs::get_Type()
extern void CallEventArgs_get_Type_m6C6F57DE5DB32EE8C7473324218610ECF1BDF3E7 (void);
// 0x0000001C System.Void Byn.Awrtc.CallEventArgs::.ctor(Byn.Awrtc.CallEventType)
extern void CallEventArgs__ctor_m27835C1E1A8C79D0D5B5FF0B13440DD7A41C761B (void);
// 0x0000001D Byn.Awrtc.ConnectionId Byn.Awrtc.CallAcceptedEventArgs::get_ConnectionId()
extern void CallAcceptedEventArgs_get_ConnectionId_mAE7329DBB7BB9A90355C2801D5BDC7A02595D602 (void);
// 0x0000001E System.Void Byn.Awrtc.CallAcceptedEventArgs::.ctor(Byn.Awrtc.ConnectionId)
extern void CallAcceptedEventArgs__ctor_mA3AF7F3E7716C822F53FC736B3A3139484DFC7CF (void);
// 0x0000001F Byn.Awrtc.ConnectionId Byn.Awrtc.CallEndedEventArgs::get_ConnectionId()
extern void CallEndedEventArgs_get_ConnectionId_m87C0B0E802AFDFE36F425B0146279CD0B85393AA (void);
// 0x00000020 System.Void Byn.Awrtc.CallEndedEventArgs::.ctor(Byn.Awrtc.ConnectionId)
extern void CallEndedEventArgs__ctor_mD50EFA3DEB25C0EFB4AA06C66126E158F0DC6E6A (void);
// 0x00000021 Byn.Awrtc.CallErrorType Byn.Awrtc.ErrorEventArgs::get_ErrorType()
extern void ErrorEventArgs_get_ErrorType_m982253FA5159350383DEA9701B168CB9429A1694 (void);
// 0x00000022 Byn.Awrtc.ErrorInfo Byn.Awrtc.ErrorEventArgs::get_Info()
extern void ErrorEventArgs_get_Info_m08384B451C8A93717E483A2F1A44DAF795FFDC38 (void);
// 0x00000023 System.String Byn.Awrtc.ErrorEventArgs::get_ErrorMessage()
extern void ErrorEventArgs_get_ErrorMessage_mF43B741F5BB7348DF6D0B72EA6E7148C72432F3A (void);
// 0x00000024 System.Void Byn.Awrtc.ErrorEventArgs::.ctor(Byn.Awrtc.CallEventType,Byn.Awrtc.ErrorInfo)
extern void ErrorEventArgs__ctor_mBB04F7DB056528287DF7910A3D3AF4CAB53F0EDA (void);
// 0x00000025 System.String Byn.Awrtc.ErrorEventArgs::GuessError()
extern void ErrorEventArgs_GuessError_m7710C874D20B2204951BAA3887139759651AB522 (void);
// 0x00000026 System.String Byn.Awrtc.WaitForIncomingCallEventArgs::get_Address()
extern void WaitForIncomingCallEventArgs_get_Address_m0C3DC8A9F6AED43652C69208A2B3A8146CDB91A9 (void);
// 0x00000027 System.Void Byn.Awrtc.WaitForIncomingCallEventArgs::.ctor(System.String)
extern void WaitForIncomingCallEventArgs__ctor_mC379C8D451FF05E7B0250165CD88B07E06C2930C (void);
// 0x00000028 Byn.Awrtc.ConnectionId Byn.Awrtc.MessageEventArgs::get_ConnectionId()
extern void MessageEventArgs_get_ConnectionId_m4D273224E183CB8F3DB086F8FD1678338512EF30 (void);
// 0x00000029 System.Boolean Byn.Awrtc.MessageEventArgs::get_Reliable()
extern void MessageEventArgs_get_Reliable_m0995B9A96300FDB8DA6C35ADE43D0CAF044FA723 (void);
// 0x0000002A System.String Byn.Awrtc.MessageEventArgs::get_Content()
extern void MessageEventArgs_get_Content_m5B902F8AD684D4D6651FE626DBBDB9C7192E7A4C (void);
// 0x0000002B System.Void Byn.Awrtc.MessageEventArgs::.ctor(Byn.Awrtc.ConnectionId,System.String,System.Boolean)
extern void MessageEventArgs__ctor_m7647C6440347809EFC77AB64C1275BAD6E098248 (void);
// 0x0000002C Byn.Awrtc.ConnectionId Byn.Awrtc.DataMessageEventArgs::get_ConnectionId()
extern void DataMessageEventArgs_get_ConnectionId_mDC1012FBD065ECEE59239C4E87506229480182E9 (void);
// 0x0000002D System.Byte[] Byn.Awrtc.DataMessageEventArgs::get_Content()
extern void DataMessageEventArgs_get_Content_mCE51E8F23487973A1A66FAA50F0BE0F4E0CAC035 (void);
// 0x0000002E System.Boolean Byn.Awrtc.DataMessageEventArgs::get_Reliable()
extern void DataMessageEventArgs_get_Reliable_m7BD08AE994F41D5663D9BD6F1605FEC7FD6E11A9 (void);
// 0x0000002F System.Void Byn.Awrtc.DataMessageEventArgs::.ctor(Byn.Awrtc.ConnectionId,System.Byte[],System.Boolean)
extern void DataMessageEventArgs__ctor_m124D5E4A61259583FEB20A7F07E7279FD4C23CD9 (void);
// 0x00000030 Byn.Awrtc.FramePixelFormat Byn.Awrtc.FrameUpdateEventArgs::get_Format()
extern void FrameUpdateEventArgs_get_Format_m7C36A48116A63B09974E6B1FC1029D593B50AF47 (void);
// 0x00000031 Byn.Awrtc.ConnectionId Byn.Awrtc.FrameUpdateEventArgs::get_ConnectionId()
extern void FrameUpdateEventArgs_get_ConnectionId_mC99AEE297CC99FB8D057ABBA64F1E29444775BB3 (void);
// 0x00000032 System.Int32 Byn.Awrtc.FrameUpdateEventArgs::get_TrackId()
extern void FrameUpdateEventArgs_get_TrackId_mFC3C40311E8D65F380C772F7F2A35DA92172333A (void);
// 0x00000033 System.Boolean Byn.Awrtc.FrameUpdateEventArgs::get_IsRemote()
extern void FrameUpdateEventArgs_get_IsRemote_m85DF3814C0760433082C563BBE9AA4BB01B4FCB9 (void);
// 0x00000034 Byn.Awrtc.IFrame Byn.Awrtc.FrameUpdateEventArgs::get_Frame()
extern void FrameUpdateEventArgs_get_Frame_mDD9525664672FCEDDDC6F0CBC37E69137BDE0BB1 (void);
// 0x00000035 System.Void Byn.Awrtc.FrameUpdateEventArgs::.ctor(Byn.Awrtc.ConnectionId,Byn.Awrtc.IFrame)
extern void FrameUpdateEventArgs__ctor_m05F8E94F9619CA434E3D798995808AAC2115EB16 (void);
// 0x00000036 System.Void Byn.Awrtc.ConnectionId::.ctor(System.Int16)
extern void ConnectionId__ctor_mD6FB77426F0ACC9BFB11034E4C2CAA6052DE5FA7 (void);
// 0x00000037 System.Boolean Byn.Awrtc.ConnectionId::Equals(System.Object)
extern void ConnectionId_Equals_m35743E4DD4A06BBB041AA3038196950D6956EDF6 (void);
// 0x00000038 System.Boolean Byn.Awrtc.ConnectionId::IsValid()
extern void ConnectionId_IsValid_m5D1825E8CAF6F48D4C8B64AF60AC46FDF24E7011 (void);
// 0x00000039 System.Int32 Byn.Awrtc.ConnectionId::GetHashCode()
extern void ConnectionId_GetHashCode_m0401767F6FD490AE5F6F04D3AAD3ABFA59010AB3 (void);
// 0x0000003A System.Boolean Byn.Awrtc.ConnectionId::op_Equality(Byn.Awrtc.ConnectionId,Byn.Awrtc.ConnectionId)
extern void ConnectionId_op_Equality_m0707CBDAC4D84A59240D98D4AE259F9F8DB143BD (void);
// 0x0000003B System.Boolean Byn.Awrtc.ConnectionId::op_Inequality(Byn.Awrtc.ConnectionId,Byn.Awrtc.ConnectionId)
extern void ConnectionId_op_Inequality_m4E23A23BA6AEFE873ECA1D197175B68F0617DC32 (void);
// 0x0000003C System.Boolean Byn.Awrtc.ConnectionId::op_LessThan(Byn.Awrtc.ConnectionId,Byn.Awrtc.ConnectionId)
extern void ConnectionId_op_LessThan_mB9BA51DEAB8BE0B3C441167C81F0769AE37481E4 (void);
// 0x0000003D System.Boolean Byn.Awrtc.ConnectionId::op_GreaterThan(Byn.Awrtc.ConnectionId,Byn.Awrtc.ConnectionId)
extern void ConnectionId_op_GreaterThan_m2FB347127031EEDD5FF60C6443E2792F26D98B63 (void);
// 0x0000003E System.Boolean Byn.Awrtc.ConnectionId::op_LessThanOrEqual(Byn.Awrtc.ConnectionId,Byn.Awrtc.ConnectionId)
extern void ConnectionId_op_LessThanOrEqual_mE40F4560DC361DD2CD72921C10152DF8533BA8B1 (void);
// 0x0000003F System.Boolean Byn.Awrtc.ConnectionId::op_GreaterThanOrEqual(Byn.Awrtc.ConnectionId,Byn.Awrtc.ConnectionId)
extern void ConnectionId_op_GreaterThanOrEqual_mD519C82C8441A6D704675BA477782453F0F98FC5 (void);
// 0x00000040 System.String Byn.Awrtc.ConnectionId::ToString()
extern void ConnectionId_ToString_mCE02582732D141880A5883AC6A8D852A5C756652 (void);
// 0x00000041 System.Void Byn.Awrtc.ConnectionId::.cctor()
extern void ConnectionId__cctor_m36862D4A6968E2877DEE3B696641EF46BFF24A7E (void);
// 0x00000042 System.Boolean Byn.Awrtc.DefaultValues::get_AuthenticateAsClientBugWorkaround()
extern void DefaultValues_get_AuthenticateAsClientBugWorkaround_m1044AA3F64367F01016184FDBB36CB0803E20DBD (void);
// 0x00000043 System.Void Byn.Awrtc.DefaultValues::set_AuthenticateAsClientBugWorkaround(System.Boolean)
extern void DefaultValues_set_AuthenticateAsClientBugWorkaround_m16AB83A61C784B0A9DE38A6566991E218761CAD6 (void);
// 0x00000044 System.Void Byn.Awrtc.DefaultValues::.cctor()
extern void DefaultValues__cctor_mE86D74461A396D1F29B93850EE7397A33301E145 (void);
// 0x00000045 System.String Byn.Awrtc.ErrorInfo::get_ErrorMessage()
extern void ErrorInfo_get_ErrorMessage_mE918B75DB5A1A316BAAB7B16D03260F2C132C6E8 (void);
// 0x00000046 System.Void Byn.Awrtc.ErrorInfo::.ctor(System.String)
extern void ErrorInfo__ctor_m3AEA9E29713388D1F4298105AB270AFAAC9149D9 (void);
// 0x00000047 System.String Byn.Awrtc.ErrorInfo::ToString()
extern void ErrorInfo_ToString_mE2C7093F3AA3F5527E679C6EF149F6F96B87427D (void);
// 0x00000048 System.Void Byn.Awrtc.ErrorInfo::.cctor()
extern void ErrorInfo__cctor_m7BE334369910CB5370367E71D39CCA87355169DA (void);
// 0x00000049 System.Boolean Byn.Awrtc.INetwork::Dequeue(Byn.Awrtc.NetworkEvent&)
// 0x0000004A System.Boolean Byn.Awrtc.INetwork::Peek(Byn.Awrtc.NetworkEvent&)
// 0x0000004B System.Void Byn.Awrtc.INetwork::Flush()
// 0x0000004C System.Boolean Byn.Awrtc.INetwork::SendData(Byn.Awrtc.ConnectionId,System.Byte[],System.Int32,System.Int32,System.Boolean)
// 0x0000004D System.Void Byn.Awrtc.INetwork::Disconnect(Byn.Awrtc.ConnectionId)
// 0x0000004E System.Void Byn.Awrtc.INetwork::Shutdown()
// 0x0000004F System.Void Byn.Awrtc.INetwork::Update()
// 0x00000050 System.Void Byn.Awrtc.IBasicNetwork::StartServer(System.String)
// 0x00000051 System.Void Byn.Awrtc.IBasicNetwork::StopServer()
// 0x00000052 Byn.Awrtc.ConnectionId Byn.Awrtc.IBasicNetwork::Connect(System.String)
// 0x00000053 System.Int32 Byn.Awrtc.IWebRtcNetwork::GetBufferedAmount(Byn.Awrtc.ConnectionId,System.Boolean)
// 0x00000054 System.Void Byn.Awrtc.ICall::add_CallEvent(Byn.Awrtc.CallEventHandler)
// 0x00000055 System.Void Byn.Awrtc.ICall::remove_CallEvent(Byn.Awrtc.CallEventHandler)
// 0x00000056 System.Void Byn.Awrtc.ICall::Configure(Byn.Awrtc.MediaConfig)
// 0x00000057 System.Void Byn.Awrtc.ICall::Listen(System.String)
// 0x00000058 System.Void Byn.Awrtc.ICall::Call(System.String)
// 0x00000059 System.Void Byn.Awrtc.ICall::Send(System.String)
// 0x0000005A System.Void Byn.Awrtc.ICall::Send(System.String,System.Boolean)
// 0x0000005B System.Boolean Byn.Awrtc.ICall::Send(System.String,System.Boolean,Byn.Awrtc.ConnectionId)
// 0x0000005C System.Void Byn.Awrtc.ICall::Send(System.Byte[],System.Boolean)
// 0x0000005D System.Boolean Byn.Awrtc.ICall::Send(System.Byte[],System.Boolean,Byn.Awrtc.ConnectionId)
// 0x0000005E System.Void Byn.Awrtc.ICall::Update()
// 0x0000005F System.Void Byn.Awrtc.ICall::SetVolume(System.Double,Byn.Awrtc.ConnectionId)
// 0x00000060 System.Boolean Byn.Awrtc.ICall::get_LocalFrameEvents()
// 0x00000061 System.Void Byn.Awrtc.ICall::set_LocalFrameEvents(System.Boolean)
// 0x00000062 System.Boolean Byn.Awrtc.ICall::HasAudioTrack(Byn.Awrtc.ConnectionId)
// 0x00000063 System.Boolean Byn.Awrtc.ICall::HasVideoTrack(Byn.Awrtc.ConnectionId)
// 0x00000064 System.Boolean Byn.Awrtc.ICall::IsMute()
// 0x00000065 System.Void Byn.Awrtc.ICall::SetMute(System.Boolean)
// 0x00000066 System.Int32 Byn.Awrtc.ICall::GetBufferedAmount(Byn.Awrtc.ConnectionId,System.Boolean)
// 0x00000067 Byn.Awrtc.ICall Byn.Awrtc.IAwrtcFactory::CreateCall(Byn.Awrtc.NetworkConfig)
// 0x00000068 Byn.Awrtc.ICall Byn.Awrtc.IAwrtcFactory::CreateCall(Byn.Awrtc.NetworkConfig,Byn.Awrtc.IBasicNetwork)
// 0x00000069 Byn.Awrtc.IMediaNetwork Byn.Awrtc.IAwrtcFactory::CreateMediaNetwork(Byn.Awrtc.NetworkConfig)
// 0x0000006A Byn.Awrtc.IWebRtcNetwork Byn.Awrtc.IAwrtcFactory::CreateBasicNetwork(System.String,Byn.Awrtc.IceServer[])
// 0x0000006B System.String[] Byn.Awrtc.IAwrtcFactory::GetVideoDevices()
// 0x0000006C System.Boolean Byn.Awrtc.IAwrtcFactory::CanSelectVideoDevice()
// 0x0000006D System.Collections.Generic.List`1<System.String> Byn.Awrtc.IceServer::get_Urls()
extern void IceServer_get_Urls_mC05D2A925F50F12F4133F06AF30E516C4698F7A8 (void);
// 0x0000006E System.String Byn.Awrtc.IceServer::get_Username()
extern void IceServer_get_Username_m57F16DE3845C8634519ACC62B0E0E82D86356EA4 (void);
// 0x0000006F System.String Byn.Awrtc.IceServer::get_Credential()
extern void IceServer_get_Credential_m63F420875D884F2BE9E1B162566D5BBFE4A51969 (void);
// 0x00000070 System.Void Byn.Awrtc.IceServer::.ctor(System.Collections.Generic.List`1<System.String>,System.String,System.String)
extern void IceServer__ctor_mAEE12F88B4F3DB2EF8F87C8FE820E4AFFE52184D (void);
// 0x00000071 System.Void Byn.Awrtc.IceServer::.ctor(System.String,System.String,System.String)
extern void IceServer__ctor_m83B96F2CE5A6DF8315B20FA0421B4C1259CC984A (void);
// 0x00000072 System.String Byn.Awrtc.IceServer::ToString()
extern void IceServer_ToString_mCCE817EF4221CCA3E14DD419EBA604945AB74B2E (void);
// 0x00000073 System.Void Byn.Awrtc.IMediaNetwork::Configure(Byn.Awrtc.MediaConfig)
// 0x00000074 Byn.Awrtc.MediaConfigurationState Byn.Awrtc.IMediaNetwork::GetConfigurationState()
// 0x00000075 System.String Byn.Awrtc.IMediaNetwork::GetConfigurationError()
// 0x00000076 System.Void Byn.Awrtc.IMediaNetwork::ResetConfiguration()
// 0x00000077 Byn.Awrtc.IFrame Byn.Awrtc.IMediaNetwork::TryGetFrame(Byn.Awrtc.ConnectionId)
// 0x00000078 System.Void Byn.Awrtc.IMediaNetwork::SetVolume(System.Double,Byn.Awrtc.ConnectionId)
// 0x00000079 System.Boolean Byn.Awrtc.IMediaNetwork::HasAudioTrack(Byn.Awrtc.ConnectionId)
// 0x0000007A System.Boolean Byn.Awrtc.IMediaNetwork::HasVideoTrack(Byn.Awrtc.ConnectionId)
// 0x0000007B System.Boolean Byn.Awrtc.IMediaNetwork::IsMute()
// 0x0000007C System.Void Byn.Awrtc.IMediaNetwork::SetMute(System.Boolean)
// 0x0000007D System.Collections.Generic.IList`1<Byn.Awrtc.ConnectionId> Byn.Awrtc.LocalNetwork::get_Connections()
extern void LocalNetwork_get_Connections_mBC573749B211E04B522D55565BD393907D593CA0 (void);
// 0x0000007E System.Boolean Byn.Awrtc.LocalNetwork::get_IsServer()
extern void LocalNetwork_get_IsServer_mFD664AA82D5E75A636F4AB0E12505C986E4296EC (void);
// 0x0000007F System.Void Byn.Awrtc.LocalNetwork::.ctor()
extern void LocalNetwork__ctor_m446005B764F5446EF80EA73C89DF7B4A20BE1C05 (void);
// 0x00000080 System.Boolean Byn.Awrtc.LocalNetwork::IsAddressInUse(System.String)
extern void LocalNetwork_IsAddressInUse_m54FC6264EDB8DA4C313C021B83406ACA472B49F2 (void);
// 0x00000081 System.Void Byn.Awrtc.LocalNetwork::StartServer(System.String)
extern void LocalNetwork_StartServer_m7538F43D30967C3E976532B27F871FE21C52604C (void);
// 0x00000082 System.Void Byn.Awrtc.LocalNetwork::StopServer()
extern void LocalNetwork_StopServer_mF5A3A792986747E1576556C93BC3A92DE2A7AA0F (void);
// 0x00000083 Byn.Awrtc.ConnectionId Byn.Awrtc.LocalNetwork::Connect(System.String)
extern void LocalNetwork_Connect_mC2404307ACA3A9F5291E112FBF65D12C6794C5F9 (void);
// 0x00000084 System.Void Byn.Awrtc.LocalNetwork::Shutdown()
extern void LocalNetwork_Shutdown_m61DCCABD52523F6B6B41E7EC7CB5D51ACD0DED7D (void);
// 0x00000085 System.Boolean Byn.Awrtc.LocalNetwork::SendData(Byn.Awrtc.ConnectionId,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void LocalNetwork_SendData_mF43832101833AF4B559F58C44FA621BAD2857E44 (void);
// 0x00000086 System.Void Byn.Awrtc.LocalNetwork::Update()
extern void LocalNetwork_Update_m9BFE98591FA7F3752F757811AA4EBFED527D58D3 (void);
// 0x00000087 System.Boolean Byn.Awrtc.LocalNetwork::Dequeue(Byn.Awrtc.NetworkEvent&)
extern void LocalNetwork_Dequeue_m94CCF44350CAB6C4F0C87A7B044C7EDA32C1FB52 (void);
// 0x00000088 System.Boolean Byn.Awrtc.LocalNetwork::Peek(Byn.Awrtc.NetworkEvent&)
extern void LocalNetwork_Peek_m4C517A2583D0F728A28E2F5B5DC18D44DA0B03BE (void);
// 0x00000089 System.Void Byn.Awrtc.LocalNetwork::Flush()
extern void LocalNetwork_Flush_m98622DCC29611BA16117EC3011E2DC9E37D9A375 (void);
// 0x0000008A System.Void Byn.Awrtc.LocalNetwork::Disconnect(Byn.Awrtc.ConnectionId)
extern void LocalNetwork_Disconnect_mE56724C49BFD5E68C1206022EE65CA2400477ABF (void);
// 0x0000008B Byn.Awrtc.ConnectionId Byn.Awrtc.LocalNetwork::FindConnectionId(Byn.Awrtc.LocalNetwork)
extern void LocalNetwork_FindConnectionId_mD267F95EF0E2B7E31B36F5D308A9776E68401CE6 (void);
// 0x0000008C Byn.Awrtc.ConnectionId Byn.Awrtc.LocalNetwork::NextConnectionId()
extern void LocalNetwork_NextConnectionId_m8345C778767F0E42CDB94BDA1AA44BFFC242F2F2 (void);
// 0x0000008D System.Void Byn.Awrtc.LocalNetwork::ConnectClient(Byn.Awrtc.LocalNetwork)
extern void LocalNetwork_ConnectClient_m5FFC02A41B091E2860F22390B600F829A6F87AFF (void);
// 0x0000008E System.Void Byn.Awrtc.LocalNetwork::Enqueue(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,Byn.Awrtc.ByteArrayBuffer)
extern void LocalNetwork_Enqueue_m4A77E55C0245AB61F4F376849EE45E4E39CEDA93 (void);
// 0x0000008F System.Void Byn.Awrtc.LocalNetwork::Enqueue(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,Byn.Awrtc.ErrorInfo)
extern void LocalNetwork_Enqueue_m45BE9C3606718262C2F53E4A58240BAF02483E56 (void);
// 0x00000090 System.Void Byn.Awrtc.LocalNetwork::Enqueue(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId)
extern void LocalNetwork_Enqueue_m4F1AAF7838CCE29B6C0D9BFB057D613A0A1E1DE3 (void);
// 0x00000091 System.Void Byn.Awrtc.LocalNetwork::Enqueue(Byn.Awrtc.NetworkEvent)
extern void LocalNetwork_Enqueue_mE0C344F12B9881B8ECF9588E29DBAD951EE4CFEE (void);
// 0x00000092 System.Void Byn.Awrtc.LocalNetwork::ReceiveData(Byn.Awrtc.LocalNetwork,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void LocalNetwork_ReceiveData_m00F9F3310B9CCFE3664436D80876EF4CF315F71A (void);
// 0x00000093 System.Void Byn.Awrtc.LocalNetwork::InternalDisconnect(Byn.Awrtc.ConnectionId)
extern void LocalNetwork_InternalDisconnect_m3A0804F62396EAE4542423762B673D66DCFF90E4 (void);
// 0x00000094 System.Void Byn.Awrtc.LocalNetwork::InternalDisconnect(Byn.Awrtc.LocalNetwork)
extern void LocalNetwork_InternalDisconnect_m7316956C07DC620C73581EEA376A6CF8409AB52D (void);
// 0x00000095 System.Void Byn.Awrtc.LocalNetwork::CleanupWreakReferences()
extern void LocalNetwork_CleanupWreakReferences_mFD6E2A04AD629BFA51ABD6E55CBA75D45130A224 (void);
// 0x00000096 System.Void Byn.Awrtc.LocalNetwork::Dispose(System.Boolean)
extern void LocalNetwork_Dispose_m79DB60F2EE5FC3E5270EA018FCE213C011F1D7C1 (void);
// 0x00000097 System.Void Byn.Awrtc.LocalNetwork::Dispose()
extern void LocalNetwork_Dispose_m0620EEA27F4A384E04CD5AC02E7ECA21B30F9935 (void);
// 0x00000098 System.Void Byn.Awrtc.LocalNetwork::.cctor()
extern void LocalNetwork__cctor_mEF1B802D7B2E5E29FBA13E0344712564CE3B66F1 (void);
// 0x00000099 System.Void Byn.Awrtc.LocalNetwork/WeakRef`1::.ctor(T)
// 0x0000009A T Byn.Awrtc.LocalNetwork/WeakRef`1::Get()
// 0x0000009B System.Boolean Byn.Awrtc.MediaConfig::get_Audio()
extern void MediaConfig_get_Audio_m89FD434CCC612229BCDDC0E88DE76178E44F6565 (void);
// 0x0000009C System.Void Byn.Awrtc.MediaConfig::set_Audio(System.Boolean)
extern void MediaConfig_set_Audio_mEF0C5C20C8453EC52D1367AF5186887877F59FC5 (void);
// 0x0000009D System.Boolean Byn.Awrtc.MediaConfig::get_Video()
extern void MediaConfig_get_Video_m311BF103BC16DAC4FBD5BF9E024F97CB84853AA1 (void);
// 0x0000009E System.Void Byn.Awrtc.MediaConfig::set_Video(System.Boolean)
extern void MediaConfig_set_Video_mFC087E206C5146CB1BA577180D2097532E6F83A5 (void);
// 0x0000009F System.String Byn.Awrtc.MediaConfig::get_VideoDeviceName()
extern void MediaConfig_get_VideoDeviceName_m7FEDCBE565DF6AF9E2D7EF19BB271733CFEDEC36 (void);
// 0x000000A0 System.Void Byn.Awrtc.MediaConfig::set_VideoDeviceName(System.String)
extern void MediaConfig_set_VideoDeviceName_m5BCD59DD1606D48BF5CCFEC65ADD5E17C9F81B4D (void);
// 0x000000A1 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MinWidth()
extern void MediaConfig_get_MinWidth_mF5F15AFF3F4A17A1033D0A2C801514C873919DD0 (void);
// 0x000000A2 System.Void Byn.Awrtc.MediaConfig::set_MinWidth(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MinWidth_m06C6DB20CD6A0B1294E7F206792A521D11811036 (void);
// 0x000000A3 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MinHeight()
extern void MediaConfig_get_MinHeight_m4156959DF52443FA36E16EAC3EB05999810D8B42 (void);
// 0x000000A4 System.Void Byn.Awrtc.MediaConfig::set_MinHeight(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MinHeight_mF05DBA14BF42ED493757C99EBBA968EEF8D26797 (void);
// 0x000000A5 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MaxWidth()
extern void MediaConfig_get_MaxWidth_m34CF68FB36197450790E683FC427B25E1D2F2086 (void);
// 0x000000A6 System.Void Byn.Awrtc.MediaConfig::set_MaxWidth(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MaxWidth_mF8403DB8C41AB346AB9E70E3E2C9C6C09DAAABDE (void);
// 0x000000A7 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MaxHeight()
extern void MediaConfig_get_MaxHeight_m21C71ACC779DACE97A3CAB0BD4B7E4B72E8B699D (void);
// 0x000000A8 System.Void Byn.Awrtc.MediaConfig::set_MaxHeight(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MaxHeight_m4735EB7449E77AB14C015C55179380CAE2C5561E (void);
// 0x000000A9 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_IdealWidth()
extern void MediaConfig_get_IdealWidth_m3DC647D6B62BAF6E940EB07C85A691C911354A27 (void);
// 0x000000AA System.Void Byn.Awrtc.MediaConfig::set_IdealWidth(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_IdealWidth_m92830DF9D19AADD5CEE89B93ED97F9F5128D5A74 (void);
// 0x000000AB System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_IdealHeight()
extern void MediaConfig_get_IdealHeight_m616D7CC7D06BB2DE1D8AE6F26265A805DAF7061B (void);
// 0x000000AC System.Void Byn.Awrtc.MediaConfig::set_IdealHeight(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_IdealHeight_mD91A0B16B8BCC9A96DF2505702CA925412BCBE86 (void);
// 0x000000AD System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_IdealFrameRate()
extern void MediaConfig_get_IdealFrameRate_mC2B7C270E6810762AC70D3B3C538AB5FBD86ADBF (void);
// 0x000000AE System.Void Byn.Awrtc.MediaConfig::set_IdealFrameRate(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_IdealFrameRate_m74FA8F1894EC9AF551EE8926D3994B8DDA44F272 (void);
// 0x000000AF System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MinFrameRate()
extern void MediaConfig_get_MinFrameRate_m6065442D0F6D8EF9AB58EE5FD55B3F0813B4570A (void);
// 0x000000B0 System.Void Byn.Awrtc.MediaConfig::set_MinFrameRate(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MinFrameRate_mBD7CFC2C605D162BB00A4A656A42D416905A388B (void);
// 0x000000B1 System.Nullable`1<System.Int32> Byn.Awrtc.MediaConfig::get_MaxFrameRate()
extern void MediaConfig_get_MaxFrameRate_m40B63D9FAA3A24E0E38F78291232711771588470 (void);
// 0x000000B2 System.Void Byn.Awrtc.MediaConfig::set_MaxFrameRate(System.Nullable`1<System.Int32>)
extern void MediaConfig_set_MaxFrameRate_mD2C4DAE4D77785F6A0B24B643B903224B7DA1C91 (void);
// 0x000000B3 Byn.Awrtc.FramePixelFormat Byn.Awrtc.MediaConfig::get_Format()
extern void MediaConfig_get_Format_m5329E45BBD240B4684AB4CD69EAF36323BCC2795 (void);
// 0x000000B4 System.Void Byn.Awrtc.MediaConfig::set_Format(Byn.Awrtc.FramePixelFormat)
extern void MediaConfig_set_Format_mDBA17E7125E65329DCE64F305C558DE9CC46BF2D (void);
// 0x000000B5 System.Void Byn.Awrtc.MediaConfig::.ctor()
extern void MediaConfig__ctor_m74B48595BB33375E761C1712B675F646CDE9C7EA (void);
// 0x000000B6 System.Void Byn.Awrtc.MediaConfig::.ctor(Byn.Awrtc.MediaConfig)
extern void MediaConfig__ctor_m2E9B827C59D0E09C52CCAA02793EC90072E30A65 (void);
// 0x000000B7 Byn.Awrtc.MediaConfig Byn.Awrtc.MediaConfig::DeepClone()
extern void MediaConfig_DeepClone_m6F71EFBDA05F8E8DBDD0D6410AC174C745737AB5 (void);
// 0x000000B8 System.String Byn.Awrtc.MediaConfig::ToString()
extern void MediaConfig_ToString_mA0EF133ECE94DB6D4E883799CF6698EC579C9E76 (void);
// 0x000000B9 System.Byte[] Byn.Awrtc.MessageDataBuffer::get_Buffer()
// 0x000000BA System.Int32 Byn.Awrtc.MessageDataBuffer::get_Offset()
// 0x000000BB System.Int32 Byn.Awrtc.MessageDataBuffer::get_ContentLength()
// 0x000000BC System.String Byn.Awrtc.MessageDataBufferExt::AsStringUnicode(Byn.Awrtc.MessageDataBuffer)
extern void MessageDataBufferExt_AsStringUnicode_m370A6E9D2474F44FED29CEFE09DFFF19D8AD8D58 (void);
// 0x000000BD System.Byte[] Byn.Awrtc.MessageDataBufferExt::Copy(Byn.Awrtc.MessageDataBuffer)
extern void MessageDataBufferExt_Copy_m6628C408FF9B181C8AD9737AF5A8FBA0C03BC8EC (void);
// 0x000000BE System.Collections.Generic.List`1<Byn.Awrtc.IceServer> Byn.Awrtc.NetworkConfig::get_IceServers()
extern void NetworkConfig_get_IceServers_m4BCDF260A6752D59C158653947B7249082E4C840 (void);
// 0x000000BF System.Void Byn.Awrtc.NetworkConfig::set_IceServers(System.Collections.Generic.List`1<Byn.Awrtc.IceServer>)
extern void NetworkConfig_set_IceServers_mADC0293C8A6ECF63990157DE40D6DD2AF4C0E76F (void);
// 0x000000C0 System.String Byn.Awrtc.NetworkConfig::get_SignalingUrl()
extern void NetworkConfig_get_SignalingUrl_mFB0516DF1BC404414AC3490A36F3B65465255CD2 (void);
// 0x000000C1 System.Void Byn.Awrtc.NetworkConfig::set_SignalingUrl(System.String)
extern void NetworkConfig_set_SignalingUrl_mEEBC8B90F3497F8BBBB86DF5BD3A148FE20501F1 (void);
// 0x000000C2 System.Boolean Byn.Awrtc.NetworkConfig::get_AllowRenegotiation()
extern void NetworkConfig_get_AllowRenegotiation_m19C17F8A7D7AC17DF1D412C1A420C18B0A176837 (void);
// 0x000000C3 System.Void Byn.Awrtc.NetworkConfig::set_AllowRenegotiation(System.Boolean)
extern void NetworkConfig_set_AllowRenegotiation_m0F6E94F27EBD3F2A7B5B1454C3DCB1A2D9F0AE0D (void);
// 0x000000C4 System.Boolean Byn.Awrtc.NetworkConfig::get_IsConference()
extern void NetworkConfig_get_IsConference_mCEB7079529140541805C1E187121429FE6390697 (void);
// 0x000000C5 System.Void Byn.Awrtc.NetworkConfig::set_IsConference(System.Boolean)
extern void NetworkConfig_set_IsConference_mD1E8F61D68F5D7B50D6E2EBAAFC93D9483D7B7A8 (void);
// 0x000000C6 System.String Byn.Awrtc.NetworkConfig::ToString()
extern void NetworkConfig_ToString_m4848DD3B8369705B20F2A2B023BF449CBF07B03D (void);
// 0x000000C7 System.Void Byn.Awrtc.NetworkConfig::.ctor()
extern void NetworkConfig__ctor_m0AE0FDBD4ECDD5658C2D5844463A9F9D61F47EB5 (void);
// 0x000000C8 Byn.Awrtc.NetEventType Byn.Awrtc.NetworkEvent::get_Type()
extern void NetworkEvent_get_Type_m749301699A91615CD355389E04E3E64032CFCB11 (void);
// 0x000000C9 Byn.Awrtc.ConnectionId Byn.Awrtc.NetworkEvent::get_ConnectionId()
extern void NetworkEvent_get_ConnectionId_m90682B9C1F478D36004F6EDE99AB2F3A44E87515 (void);
// 0x000000CA System.Object Byn.Awrtc.NetworkEvent::get_RawData()
extern void NetworkEvent_get_RawData_m010612D39AA930FDD4260A0D07FB12C9804E0192 (void);
// 0x000000CB Byn.Awrtc.MessageDataBuffer Byn.Awrtc.NetworkEvent::get_MessageData()
extern void NetworkEvent_get_MessageData_m7D424D00C7BA3B525623276580847793E27ECF59 (void);
// 0x000000CC System.Byte[] Byn.Awrtc.NetworkEvent::GetDataAsByteArray()
extern void NetworkEvent_GetDataAsByteArray_m568204908708933BC4A21BEC55ABC5305E6C2697 (void);
// 0x000000CD System.String Byn.Awrtc.NetworkEvent::get_Info()
extern void NetworkEvent_get_Info_m0CD887E4E39567B9DBAAEBC46879985482E9D0BE (void);
// 0x000000CE Byn.Awrtc.ErrorInfo Byn.Awrtc.NetworkEvent::get_ErrorInfo()
extern void NetworkEvent_get_ErrorInfo_m790CDBC78B6D32DC7B3ECC1D6E8CF7F149FE014F (void);
// 0x000000CF System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType)
extern void NetworkEvent__ctor_mE67FC5AD4E0D46E8CE4DDA096620A94556B33F09 (void);
// 0x000000D0 System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId)
extern void NetworkEvent__ctor_mEF44539E731EBBD4FAF7274A349D34F1126FD9E2 (void);
// 0x000000D1 System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,Byn.Awrtc.MessageDataBuffer)
extern void NetworkEvent__ctor_m2D27BD640BE50379B1C26B8F26E252CF6156E10D (void);
// 0x000000D2 System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,System.Object)
extern void NetworkEvent__ctor_m0CBD870B498BDA850F660472770077423A85B7FB (void);
// 0x000000D3 System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,System.String)
extern void NetworkEvent__ctor_mB231B79419FA9D4FD449D7829F0E00D14CA380CD (void);
// 0x000000D4 System.Void Byn.Awrtc.NetworkEvent::.ctor(Byn.Awrtc.NetEventType,Byn.Awrtc.ConnectionId,Byn.Awrtc.ErrorInfo)
extern void NetworkEvent__ctor_mA46FFF771F9CD6E1C8DDFF476C0BD117007D5C80 (void);
// 0x000000D5 System.String Byn.Awrtc.NetworkEvent::ToString()
extern void NetworkEvent_ToString_mF0B49EAA98C221A242FCC1504B5566E7858F672C (void);
// 0x000000D6 System.Boolean Byn.Awrtc.NetworkEvent::IsMetaEvent(System.Byte[])
extern void NetworkEvent_IsMetaEvent_m488FA24505BD443DFF5E9D2952600893D365A418 (void);
// 0x000000D7 Byn.Awrtc.NetworkEvent Byn.Awrtc.NetworkEvent::FromByteArray(System.Byte[])
extern void NetworkEvent_FromByteArray_m7FFAB5917075130FBA65506BE2CE3FED4C6CE5CD (void);
// 0x000000D8 System.Byte[] Byn.Awrtc.NetworkEvent::ToByteArray(Byn.Awrtc.NetworkEvent)
extern void NetworkEvent_ToByteArray_m59E5C5E4CE57E1EB70CED4896D8ABA7595D4B216 (void);
// 0x000000D9 System.Void Byn.Awrtc.NetworkEvent::AttachError(Byn.Awrtc.ErrorInfo)
extern void NetworkEvent_AttachError_mF50D6D3BFBA8020CC7A93662FE52890703208DD4 (void);
// 0x000000DA System.Byte[] Byn.Awrtc.RawFrame::get_Buffer()
// 0x000000DB System.Boolean Byn.Awrtc.RawFrame::get_Buffered()
// 0x000000DC System.Int32 Byn.Awrtc.RawFrame::get_Height()
// 0x000000DD System.Int32 Byn.Awrtc.RawFrame::get_Width()
// 0x000000DE System.Int32 Byn.Awrtc.RawFrame::get_Rotation()
// 0x000000DF System.Boolean Byn.Awrtc.RawFrame::get_IsTopRowFirst()
// 0x000000E0 Byn.Awrtc.FramePixelFormat Byn.Awrtc.RawFrame::get_Format()
// 0x000000E1 System.IntPtr Byn.Awrtc.IDirectMemoryFrame::GetIntPtr()
// 0x000000E2 System.Int32 Byn.Awrtc.IDirectMemoryFrame::GetSize()
// 0x000000E3 System.Byte[] Byn.Awrtc.BufferedFrame::get_Buffer()
extern void BufferedFrame_get_Buffer_m2DC1E4D6558256FBE65AE72ABB2BF6B510F55E7C (void);
// 0x000000E4 System.Boolean Byn.Awrtc.BufferedFrame::get_Buffered()
extern void BufferedFrame_get_Buffered_m036F73951959B01958AC9C87E6A56F2CD6E0648B (void);
// 0x000000E5 System.Int32 Byn.Awrtc.BufferedFrame::get_Width()
extern void BufferedFrame_get_Width_m1D042156CEFFA7E4BAE4F06E928F9DE7F2D2FBDC (void);
// 0x000000E6 System.Int32 Byn.Awrtc.BufferedFrame::get_Height()
extern void BufferedFrame_get_Height_m527372652D682AB4AAB832A94CF8E26CB99FA53A (void);
// 0x000000E7 System.Int32 Byn.Awrtc.BufferedFrame::get_Rotation()
extern void BufferedFrame_get_Rotation_mC35A7A47EF7FFBF1225E3C5A8CA86D3ED2B90374 (void);
// 0x000000E8 System.Void Byn.Awrtc.BufferedFrame::set_Rotation(System.Int32)
extern void BufferedFrame_set_Rotation_mC873E609856C03CFC69158A31263CE2EF55F6B24 (void);
// 0x000000E9 System.Boolean Byn.Awrtc.BufferedFrame::get_IsTopRowFirst()
extern void BufferedFrame_get_IsTopRowFirst_m124498F816E79AE7F73EFF212C21489B6A83669D (void);
// 0x000000EA Byn.Awrtc.FramePixelFormat Byn.Awrtc.BufferedFrame::get_Format()
extern void BufferedFrame_get_Format_m57DD4AA4A3694ECCC14343545EA292BD9F6955C8 (void);
// 0x000000EB System.Void Byn.Awrtc.BufferedFrame::.ctor(System.Byte[],System.Int32,System.Int32,Byn.Awrtc.FramePixelFormat,System.Int32,System.Boolean)
extern void BufferedFrame__ctor_m3C0223D8411940E1C73D7804A340BD357C0985E8 (void);
// 0x000000EC System.Void Byn.Awrtc.BufferedFrame::Dispose()
extern void BufferedFrame_Dispose_mC7728357990326B087D1AE0E857F3A16FDC6F7C3 (void);
// 0x000000ED System.Void Byn.Awrtc.SLog::SetLogger(System.Action`2<System.Object,System.String[]>)
extern void SLog_SetLogger_m16D93C917F17C91971C8226369C56D322B5CEBA3 (void);
// 0x000000EE System.Void Byn.Awrtc.SLog::LogException(System.Object,System.String[])
extern void SLog_LogException_mE70D0D801089CC84D895987FDC26DD12B8BD4624 (void);
// 0x000000EF System.Void Byn.Awrtc.SLog::LE(System.Object,System.String[])
extern void SLog_LE_m9311AF7DA8AE883A95CEF0751ED30D23AA829B94 (void);
// 0x000000F0 System.Void Byn.Awrtc.SLog::LW(System.Object,System.String[])
extern void SLog_LW_m223C61D8BB1637F0AAC0AFD0FD940208B1412356 (void);
// 0x000000F1 System.Void Byn.Awrtc.SLog::LV(System.Object,System.String[])
extern void SLog_LV_m8ADFC439117C96B581699C9A745C42803C7ED830 (void);
// 0x000000F2 System.Void Byn.Awrtc.SLog::LD(System.Object,System.String[])
extern void SLog_LD_m93EBB0A73E255F5BAB5041BE21918104DC356190 (void);
// 0x000000F3 System.Void Byn.Awrtc.SLog::L(System.Object,System.String[])
extern void SLog_L_m29F9D573958A70843D02EBB5A988FFD3D6023195 (void);
// 0x000000F4 System.String[] Byn.Awrtc.SLog::MergeTags(System.String[],System.String[])
extern void SLog_MergeTags_m39B4A76935379D71637AF195C96080BEC8446790 (void);
// 0x000000F5 System.Void Byn.Awrtc.SLog::LogArray(System.Object,System.String[])
extern void SLog_LogArray_m770776BC1B04DFC6AB22EFECD69502618FD451A8 (void);
// 0x000000F6 System.Void Byn.Awrtc.SLog::.cctor()
extern void SLog__cctor_mA7C925C138233A2168DCCDC1C948C7DDE239B9C3 (void);
// 0x000000F7 System.Void Byn.Awrtc.Base.AWebRtcCall::add_CallEvent(Byn.Awrtc.CallEventHandler)
extern void AWebRtcCall_add_CallEvent_mF5E2043A18A81779B81E7B71D79B66E6A858E5E9 (void);
// 0x000000F8 System.Void Byn.Awrtc.Base.AWebRtcCall::remove_CallEvent(Byn.Awrtc.CallEventHandler)
extern void AWebRtcCall_remove_CallEvent_m415EF2CA82CC47DE3F08FC15C6D44A23FE46DA29 (void);
// 0x000000F9 System.Boolean Byn.Awrtc.Base.AWebRtcCall::get_IsDisposed()
extern void AWebRtcCall_get_IsDisposed_mBFAD74A5A449B98C53F78349CFAAFE6CCFFA1967 (void);
// 0x000000FA Byn.Awrtc.Base.AWebRtcCall/CallState Byn.Awrtc.Base.AWebRtcCall::get_State()
extern void AWebRtcCall_get_State_mC06D1CCFEB1DCCE3A79B64685AFBB220EE649E90 (void);
// 0x000000FB System.Boolean Byn.Awrtc.Base.AWebRtcCall::get_LocalFrameEvents()
extern void AWebRtcCall_get_LocalFrameEvents_m5E6717880FDDAE5E981EFC5FD1E4BD22E613A03C (void);
// 0x000000FC System.Void Byn.Awrtc.Base.AWebRtcCall::set_LocalFrameEvents(System.Boolean)
extern void AWebRtcCall_set_LocalFrameEvents_m0F4C60FD1DEB09A683E4D4B759262749B75C4B90 (void);
// 0x000000FD System.Void Byn.Awrtc.Base.AWebRtcCall::.ctor(Byn.Awrtc.NetworkConfig)
extern void AWebRtcCall__ctor_mEE582AB1E17117B48C1E2F309C072D8642442F2E (void);
// 0x000000FE System.Void Byn.Awrtc.Base.AWebRtcCall::Initialize(Byn.Awrtc.IMediaNetwork)
extern void AWebRtcCall_Initialize_m186CA4FF92DD35F0CF1A187388C93FD56F5FACDF (void);
// 0x000000FF System.Void Byn.Awrtc.Base.AWebRtcCall::Configure(Byn.Awrtc.MediaConfig)
extern void AWebRtcCall_Configure_mBAB34E64CD89C3D1A45DA1189A8D68567CD0E703 (void);
// 0x00000100 System.Void Byn.Awrtc.Base.AWebRtcCall::Call(System.String)
extern void AWebRtcCall_Call_m7AD1C0845C10BCB4385A2E8A11CF35BD4DAE8CF0 (void);
// 0x00000101 System.Void Byn.Awrtc.Base.AWebRtcCall::Listen(System.String)
extern void AWebRtcCall_Listen_m479C190B936DFBCDF396CF6A8B0921709FA6CB87 (void);
// 0x00000102 System.Void Byn.Awrtc.Base.AWebRtcCall::Send(System.String)
extern void AWebRtcCall_Send_m442434E182EF4AE1DA94CB9E318C697590304AEC (void);
// 0x00000103 System.Void Byn.Awrtc.Base.AWebRtcCall::Send(System.String,System.Boolean)
extern void AWebRtcCall_Send_m02ED4A8CFD3A119AAAD018DB68049D449702CC7D (void);
// 0x00000104 System.Boolean Byn.Awrtc.Base.AWebRtcCall::Send(System.String,System.Boolean,Byn.Awrtc.ConnectionId)
extern void AWebRtcCall_Send_mABD05B4BCBB16C7FEB8B7CDEBCEC93658227F6E4 (void);
// 0x00000105 System.Boolean Byn.Awrtc.Base.AWebRtcCall::IsStringMsg(System.Byte[])
extern void AWebRtcCall_IsStringMsg_mF6FD7F7E50E5BA64659CE3B0D0C415A39CB1494E (void);
// 0x00000106 System.Byte[] Byn.Awrtc.Base.AWebRtcCall::PackStringMsg(System.String)
extern void AWebRtcCall_PackStringMsg_m541C916DFFE349A27ABBCAB140307EC120F152E9 (void);
// 0x00000107 System.String Byn.Awrtc.Base.AWebRtcCall::UnpackStringMsg(System.Byte[])
extern void AWebRtcCall_UnpackStringMsg_mB0CFF7A6E1A0AEC6D71E858CEB3F319A82663DFF (void);
// 0x00000108 System.Void Byn.Awrtc.Base.AWebRtcCall::Send(System.Byte[],System.Boolean)
extern void AWebRtcCall_Send_m1DDEE2CCA960758A3E7644CA581D10DF5BC7F479 (void);
// 0x00000109 System.Boolean Byn.Awrtc.Base.AWebRtcCall::Send(System.Byte[],System.Boolean,Byn.Awrtc.ConnectionId)
extern void AWebRtcCall_Send_mE8B14947EAD420755DE0F063556C50560C6B5C7B (void);
// 0x0000010A System.Boolean Byn.Awrtc.Base.AWebRtcCall::IsDataMsg(System.Byte[])
extern void AWebRtcCall_IsDataMsg_m854E78A88BCBC5ECEA5F9D6D9C32988C59C7BD2E (void);
// 0x0000010B System.Byte[] Byn.Awrtc.Base.AWebRtcCall::PackDataMsg(System.Byte[])
extern void AWebRtcCall_PackDataMsg_m543EB718C49FE11C9E29DB123C8EF7C416A1D536 (void);
// 0x0000010C System.Byte[] Byn.Awrtc.Base.AWebRtcCall::UnpackDataMsg(System.Byte[])
extern void AWebRtcCall_UnpackDataMsg_m8E6178BC303306E10C298AE83585FEDAAF5AE304 (void);
// 0x0000010D System.Void Byn.Awrtc.Base.AWebRtcCall::Update()
extern void AWebRtcCall_Update_m26AD9EE7A5838EA667BB7F6A9229EDF9C1B4D0F2 (void);
// 0x0000010E System.Void Byn.Awrtc.Base.AWebRtcCall::HandleMediaEvents()
extern void AWebRtcCall_HandleMediaEvents_mA3DC62D29AB1CD7D3405200F1C5C594BC6FEA440 (void);
// 0x0000010F System.Void Byn.Awrtc.Base.AWebRtcCall::HandleNetworkEvent(Byn.Awrtc.NetworkEvent)
extern void AWebRtcCall_HandleNetworkEvent_mF0E4D34AF018B51031B80D19CBA7F3C822164AEF (void);
// 0x00000110 System.Void Byn.Awrtc.Base.AWebRtcCall::PendingCall(System.String)
extern void AWebRtcCall_PendingCall_m552EB57809B7C78DE965B00A0C2D2447A5CA9525 (void);
// 0x00000111 System.Void Byn.Awrtc.Base.AWebRtcCall::ProcessCall(System.String)
extern void AWebRtcCall_ProcessCall_m2EA3C171A75C255710FD4E301EDCAE973714330D (void);
// 0x00000112 System.Void Byn.Awrtc.Base.AWebRtcCall::PendingListen(System.String)
extern void AWebRtcCall_PendingListen_m68D65334F0F12E0BB3C85F4ACB0794B3394C714D (void);
// 0x00000113 System.Void Byn.Awrtc.Base.AWebRtcCall::ProcessListen(System.String)
extern void AWebRtcCall_ProcessListen_m942E1723FDA5BFB8D96C67AF2CFAE0A76693FC31 (void);
// 0x00000114 System.Void Byn.Awrtc.Base.AWebRtcCall::DoPending()
extern void AWebRtcCall_DoPending_m3D7190BD67EF62312E0189A439762F880B108FEB (void);
// 0x00000115 System.Void Byn.Awrtc.Base.AWebRtcCall::ClearPending()
extern void AWebRtcCall_ClearPending_m2E8CD1E54BB41FF79F840F768969FE6479DD2271 (void);
// 0x00000116 System.Void Byn.Awrtc.Base.AWebRtcCall::CheckDisposed()
extern void AWebRtcCall_CheckDisposed_mD72D8B22A9516A1CDB74FEE6438E202C3D86F47E (void);
// 0x00000117 System.Void Byn.Awrtc.Base.AWebRtcCall::EnsureConfiguration()
extern void AWebRtcCall_EnsureConfiguration_m10234AA21678F6EC618D59034E311D7ABEB09186 (void);
// 0x00000118 System.Void Byn.Awrtc.Base.AWebRtcCall::TriggerCallEvent(Byn.Awrtc.CallEventArgs)
extern void AWebRtcCall_TriggerCallEvent_mD513FF5D24DC4C1DC4F7409C267E239EDF71270F (void);
// 0x00000119 System.Void Byn.Awrtc.Base.AWebRtcCall::OnConfigurationComplete()
extern void AWebRtcCall_OnConfigurationComplete_m5C34524B1CE3482E6E21B5A326F3A31FA55B123F (void);
// 0x0000011A System.Void Byn.Awrtc.Base.AWebRtcCall::OnConfigurationFailed(System.String)
extern void AWebRtcCall_OnConfigurationFailed_m224529A37E7AF75501895C4778192A724CE4669A (void);
// 0x0000011B System.Void Byn.Awrtc.Base.AWebRtcCall::Dispose(System.Boolean)
extern void AWebRtcCall_Dispose_m8BBA38E44EFE6864C542234F1C5B8B7FCA9AB302 (void);
// 0x0000011C System.Void Byn.Awrtc.Base.AWebRtcCall::Dispose()
extern void AWebRtcCall_Dispose_m4EF37AAFAC8AC5FC0F40C78310645CB9E8F38053 (void);
// 0x0000011D System.Void Byn.Awrtc.Base.AWebRtcCall::SetVolume(System.Double,Byn.Awrtc.ConnectionId)
extern void AWebRtcCall_SetVolume_m7E9F191FE0A0BA66A8BAB99D1EAC1DBBB875FB05 (void);
// 0x0000011E System.Boolean Byn.Awrtc.Base.AWebRtcCall::HasAudioTrack(Byn.Awrtc.ConnectionId)
extern void AWebRtcCall_HasAudioTrack_m1F61BD2A192DEC289A296765886084070646EA2A (void);
// 0x0000011F System.Boolean Byn.Awrtc.Base.AWebRtcCall::HasVideoTrack(Byn.Awrtc.ConnectionId)
extern void AWebRtcCall_HasVideoTrack_m145EF1FFAB3F20B66361802DEEB493D80E621626 (void);
// 0x00000120 System.Boolean Byn.Awrtc.Base.AWebRtcCall::IsMute()
extern void AWebRtcCall_IsMute_mCE992965B66543C8CEF3C190ADF9057842605EC3 (void);
// 0x00000121 System.Void Byn.Awrtc.Base.AWebRtcCall::SetMute(System.Boolean)
extern void AWebRtcCall_SetMute_mEC72F131218DC1ED27E25950FF43C564AA88970D (void);
// 0x00000122 System.Int32 Byn.Awrtc.Base.AWebRtcCall::GetBufferedAmount(Byn.Awrtc.ConnectionId,System.Boolean)
extern void AWebRtcCall_GetBufferedAmount_mBDC0FD55B00C098BBBFD82FF564F1F6EFF12D0CA (void);
// 0x00000123 System.Void Byn.Awrtc.Base.AWebRtcCall::.cctor()
extern void AWebRtcCall__cctor_m84DF3FE9298FC726481005367736FE6069C185E3 (void);
static Il2CppMethodPointer s_methodPointers[291] = 
{
	ByteArrayBuffer_get_PositionWriteRelative_m2CC7D7CDA5AABB1D8FBA5C95F4C7D5D9B6AC611F,
	ByteArrayBuffer_set_PositionWriteRelative_m404083D7F5F4D5AC26EB3329E6BEC515DEE9CFB1,
	ByteArrayBuffer_get_PositionWriteAbsolute_mA19A3E432DF05415EB2D82F69E07F006BDB43152,
	ByteArrayBuffer_set_PositionWriteAbsolute_mA7982BB7772E5CE9A6495EB75DED9287D47DAF41,
	ByteArrayBuffer_get_PositionReadRelative_m6744691413C85626915E746913A3C1A66DC6C53E,
	ByteArrayBuffer_get_PositionReadAbsolute_m73983E515DD8910D8F29F1C99AB33A702EA43588,
	ByteArrayBuffer_get_Offset_m8D76EF91272F63C71B625CCD459816C32F15C8D8,
	ByteArrayBuffer_get_Buffer_mF7982705EFA0C018C06567ACD5274EF307B9078E,
	ByteArrayBuffer_get_ContentLength_m3C66B9598E0E265F0EBA86D36EEED3E5BDAC0C54,
	ByteArrayBuffer_set_ContentLength_m130FC5A57AC41C623C502434E692EF2118F64429,
	ByteArrayBuffer_get_IsDisposed_m2076E51C5E0C308249E6BA3A59824452D8C51AA9,
	ByteArrayBuffer__ctor_m48EB301B03B6ACF33C919674178BA6B3385468B9,
	ByteArrayBuffer__ctor_m0DCB1EE93324961765BB0B71BB3EE8895D523B2A,
	ByteArrayBuffer__ctor_m72DD481534B3487AE567EF47BD69EFA178E22CF0,
	ByteArrayBuffer_Reset_mC5250194BCEC35BC3A14C97FDAB601926E8DA1BD,
	ByteArrayBuffer_Finalize_mF1AB93F0C7A628788DCEC758F9E5688FBB6CC79C,
	ByteArrayBuffer_CopyFrom_m1FC3E903E65FEB98A5940190026178273D63EBB2,
	ByteArrayBuffer__cctor_m1B236CCC4486A29EF105A70BD2288EB494D1556C,
	ByteArrayBuffer_GetPower_mC394B35C54440081DE7696251C46634A76D8F6BA,
	ByteArrayBuffer_NextPowerOfTwo_m0A222D7741963842F06925AF6241627873E7E1AC,
	ByteArrayBuffer_Get_m3281A3A589DC27B7AA164600AE7B34ED6FF814D2,
	ByteArrayBuffer_Dispose_mBDCB25D21BC4CD54EF9DC430E7C5CCE183C5200E,
	CallEventHandler__ctor_m3F755A51343911494EB71D72D73E6A0C3E6F8665,
	CallEventHandler_Invoke_m8035B1A16326D15017A8EEE4583C2CBBA6BA96AD,
	CallEventHandler_BeginInvoke_mA193AF7DAD6723410124F2B5FBBB1810353939CC,
	CallEventHandler_EndInvoke_m48FAEFFA9E06DE23834865FEC86C816233AD3FC3,
	CallEventArgs_get_Type_m6C6F57DE5DB32EE8C7473324218610ECF1BDF3E7,
	CallEventArgs__ctor_m27835C1E1A8C79D0D5B5FF0B13440DD7A41C761B,
	CallAcceptedEventArgs_get_ConnectionId_mAE7329DBB7BB9A90355C2801D5BDC7A02595D602,
	CallAcceptedEventArgs__ctor_mA3AF7F3E7716C822F53FC736B3A3139484DFC7CF,
	CallEndedEventArgs_get_ConnectionId_m87C0B0E802AFDFE36F425B0146279CD0B85393AA,
	CallEndedEventArgs__ctor_mD50EFA3DEB25C0EFB4AA06C66126E158F0DC6E6A,
	ErrorEventArgs_get_ErrorType_m982253FA5159350383DEA9701B168CB9429A1694,
	ErrorEventArgs_get_Info_m08384B451C8A93717E483A2F1A44DAF795FFDC38,
	ErrorEventArgs_get_ErrorMessage_mF43B741F5BB7348DF6D0B72EA6E7148C72432F3A,
	ErrorEventArgs__ctor_mBB04F7DB056528287DF7910A3D3AF4CAB53F0EDA,
	ErrorEventArgs_GuessError_m7710C874D20B2204951BAA3887139759651AB522,
	WaitForIncomingCallEventArgs_get_Address_m0C3DC8A9F6AED43652C69208A2B3A8146CDB91A9,
	WaitForIncomingCallEventArgs__ctor_mC379C8D451FF05E7B0250165CD88B07E06C2930C,
	MessageEventArgs_get_ConnectionId_m4D273224E183CB8F3DB086F8FD1678338512EF30,
	MessageEventArgs_get_Reliable_m0995B9A96300FDB8DA6C35ADE43D0CAF044FA723,
	MessageEventArgs_get_Content_m5B902F8AD684D4D6651FE626DBBDB9C7192E7A4C,
	MessageEventArgs__ctor_m7647C6440347809EFC77AB64C1275BAD6E098248,
	DataMessageEventArgs_get_ConnectionId_mDC1012FBD065ECEE59239C4E87506229480182E9,
	DataMessageEventArgs_get_Content_mCE51E8F23487973A1A66FAA50F0BE0F4E0CAC035,
	DataMessageEventArgs_get_Reliable_m7BD08AE994F41D5663D9BD6F1605FEC7FD6E11A9,
	DataMessageEventArgs__ctor_m124D5E4A61259583FEB20A7F07E7279FD4C23CD9,
	FrameUpdateEventArgs_get_Format_m7C36A48116A63B09974E6B1FC1029D593B50AF47,
	FrameUpdateEventArgs_get_ConnectionId_mC99AEE297CC99FB8D057ABBA64F1E29444775BB3,
	FrameUpdateEventArgs_get_TrackId_mFC3C40311E8D65F380C772F7F2A35DA92172333A,
	FrameUpdateEventArgs_get_IsRemote_m85DF3814C0760433082C563BBE9AA4BB01B4FCB9,
	FrameUpdateEventArgs_get_Frame_mDD9525664672FCEDDDC6F0CBC37E69137BDE0BB1,
	FrameUpdateEventArgs__ctor_m05F8E94F9619CA434E3D798995808AAC2115EB16,
	ConnectionId__ctor_mD6FB77426F0ACC9BFB11034E4C2CAA6052DE5FA7,
	ConnectionId_Equals_m35743E4DD4A06BBB041AA3038196950D6956EDF6,
	ConnectionId_IsValid_m5D1825E8CAF6F48D4C8B64AF60AC46FDF24E7011,
	ConnectionId_GetHashCode_m0401767F6FD490AE5F6F04D3AAD3ABFA59010AB3,
	ConnectionId_op_Equality_m0707CBDAC4D84A59240D98D4AE259F9F8DB143BD,
	ConnectionId_op_Inequality_m4E23A23BA6AEFE873ECA1D197175B68F0617DC32,
	ConnectionId_op_LessThan_mB9BA51DEAB8BE0B3C441167C81F0769AE37481E4,
	ConnectionId_op_GreaterThan_m2FB347127031EEDD5FF60C6443E2792F26D98B63,
	ConnectionId_op_LessThanOrEqual_mE40F4560DC361DD2CD72921C10152DF8533BA8B1,
	ConnectionId_op_GreaterThanOrEqual_mD519C82C8441A6D704675BA477782453F0F98FC5,
	ConnectionId_ToString_mCE02582732D141880A5883AC6A8D852A5C756652,
	ConnectionId__cctor_m36862D4A6968E2877DEE3B696641EF46BFF24A7E,
	DefaultValues_get_AuthenticateAsClientBugWorkaround_m1044AA3F64367F01016184FDBB36CB0803E20DBD,
	DefaultValues_set_AuthenticateAsClientBugWorkaround_m16AB83A61C784B0A9DE38A6566991E218761CAD6,
	DefaultValues__cctor_mE86D74461A396D1F29B93850EE7397A33301E145,
	ErrorInfo_get_ErrorMessage_mE918B75DB5A1A316BAAB7B16D03260F2C132C6E8,
	ErrorInfo__ctor_m3AEA9E29713388D1F4298105AB270AFAAC9149D9,
	ErrorInfo_ToString_mE2C7093F3AA3F5527E679C6EF149F6F96B87427D,
	ErrorInfo__cctor_m7BE334369910CB5370367E71D39CCA87355169DA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IceServer_get_Urls_mC05D2A925F50F12F4133F06AF30E516C4698F7A8,
	IceServer_get_Username_m57F16DE3845C8634519ACC62B0E0E82D86356EA4,
	IceServer_get_Credential_m63F420875D884F2BE9E1B162566D5BBFE4A51969,
	IceServer__ctor_mAEE12F88B4F3DB2EF8F87C8FE820E4AFFE52184D,
	IceServer__ctor_m83B96F2CE5A6DF8315B20FA0421B4C1259CC984A,
	IceServer_ToString_mCCE817EF4221CCA3E14DD419EBA604945AB74B2E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocalNetwork_get_Connections_mBC573749B211E04B522D55565BD393907D593CA0,
	LocalNetwork_get_IsServer_mFD664AA82D5E75A636F4AB0E12505C986E4296EC,
	LocalNetwork__ctor_m446005B764F5446EF80EA73C89DF7B4A20BE1C05,
	LocalNetwork_IsAddressInUse_m54FC6264EDB8DA4C313C021B83406ACA472B49F2,
	LocalNetwork_StartServer_m7538F43D30967C3E976532B27F871FE21C52604C,
	LocalNetwork_StopServer_mF5A3A792986747E1576556C93BC3A92DE2A7AA0F,
	LocalNetwork_Connect_mC2404307ACA3A9F5291E112FBF65D12C6794C5F9,
	LocalNetwork_Shutdown_m61DCCABD52523F6B6B41E7EC7CB5D51ACD0DED7D,
	LocalNetwork_SendData_mF43832101833AF4B559F58C44FA621BAD2857E44,
	LocalNetwork_Update_m9BFE98591FA7F3752F757811AA4EBFED527D58D3,
	LocalNetwork_Dequeue_m94CCF44350CAB6C4F0C87A7B044C7EDA32C1FB52,
	LocalNetwork_Peek_m4C517A2583D0F728A28E2F5B5DC18D44DA0B03BE,
	LocalNetwork_Flush_m98622DCC29611BA16117EC3011E2DC9E37D9A375,
	LocalNetwork_Disconnect_mE56724C49BFD5E68C1206022EE65CA2400477ABF,
	LocalNetwork_FindConnectionId_mD267F95EF0E2B7E31B36F5D308A9776E68401CE6,
	LocalNetwork_NextConnectionId_m8345C778767F0E42CDB94BDA1AA44BFFC242F2F2,
	LocalNetwork_ConnectClient_m5FFC02A41B091E2860F22390B600F829A6F87AFF,
	LocalNetwork_Enqueue_m4A77E55C0245AB61F4F376849EE45E4E39CEDA93,
	LocalNetwork_Enqueue_m45BE9C3606718262C2F53E4A58240BAF02483E56,
	LocalNetwork_Enqueue_m4F1AAF7838CCE29B6C0D9BFB057D613A0A1E1DE3,
	LocalNetwork_Enqueue_mE0C344F12B9881B8ECF9588E29DBAD951EE4CFEE,
	LocalNetwork_ReceiveData_m00F9F3310B9CCFE3664436D80876EF4CF315F71A,
	LocalNetwork_InternalDisconnect_m3A0804F62396EAE4542423762B673D66DCFF90E4,
	LocalNetwork_InternalDisconnect_m7316956C07DC620C73581EEA376A6CF8409AB52D,
	LocalNetwork_CleanupWreakReferences_mFD6E2A04AD629BFA51ABD6E55CBA75D45130A224,
	LocalNetwork_Dispose_m79DB60F2EE5FC3E5270EA018FCE213C011F1D7C1,
	LocalNetwork_Dispose_m0620EEA27F4A384E04CD5AC02E7ECA21B30F9935,
	LocalNetwork__cctor_mEF1B802D7B2E5E29FBA13E0344712564CE3B66F1,
	NULL,
	NULL,
	MediaConfig_get_Audio_m89FD434CCC612229BCDDC0E88DE76178E44F6565,
	MediaConfig_set_Audio_mEF0C5C20C8453EC52D1367AF5186887877F59FC5,
	MediaConfig_get_Video_m311BF103BC16DAC4FBD5BF9E024F97CB84853AA1,
	MediaConfig_set_Video_mFC087E206C5146CB1BA577180D2097532E6F83A5,
	MediaConfig_get_VideoDeviceName_m7FEDCBE565DF6AF9E2D7EF19BB271733CFEDEC36,
	MediaConfig_set_VideoDeviceName_m5BCD59DD1606D48BF5CCFEC65ADD5E17C9F81B4D,
	MediaConfig_get_MinWidth_mF5F15AFF3F4A17A1033D0A2C801514C873919DD0,
	MediaConfig_set_MinWidth_m06C6DB20CD6A0B1294E7F206792A521D11811036,
	MediaConfig_get_MinHeight_m4156959DF52443FA36E16EAC3EB05999810D8B42,
	MediaConfig_set_MinHeight_mF05DBA14BF42ED493757C99EBBA968EEF8D26797,
	MediaConfig_get_MaxWidth_m34CF68FB36197450790E683FC427B25E1D2F2086,
	MediaConfig_set_MaxWidth_mF8403DB8C41AB346AB9E70E3E2C9C6C09DAAABDE,
	MediaConfig_get_MaxHeight_m21C71ACC779DACE97A3CAB0BD4B7E4B72E8B699D,
	MediaConfig_set_MaxHeight_m4735EB7449E77AB14C015C55179380CAE2C5561E,
	MediaConfig_get_IdealWidth_m3DC647D6B62BAF6E940EB07C85A691C911354A27,
	MediaConfig_set_IdealWidth_m92830DF9D19AADD5CEE89B93ED97F9F5128D5A74,
	MediaConfig_get_IdealHeight_m616D7CC7D06BB2DE1D8AE6F26265A805DAF7061B,
	MediaConfig_set_IdealHeight_mD91A0B16B8BCC9A96DF2505702CA925412BCBE86,
	MediaConfig_get_IdealFrameRate_mC2B7C270E6810762AC70D3B3C538AB5FBD86ADBF,
	MediaConfig_set_IdealFrameRate_m74FA8F1894EC9AF551EE8926D3994B8DDA44F272,
	MediaConfig_get_MinFrameRate_m6065442D0F6D8EF9AB58EE5FD55B3F0813B4570A,
	MediaConfig_set_MinFrameRate_mBD7CFC2C605D162BB00A4A656A42D416905A388B,
	MediaConfig_get_MaxFrameRate_m40B63D9FAA3A24E0E38F78291232711771588470,
	MediaConfig_set_MaxFrameRate_mD2C4DAE4D77785F6A0B24B643B903224B7DA1C91,
	MediaConfig_get_Format_m5329E45BBD240B4684AB4CD69EAF36323BCC2795,
	MediaConfig_set_Format_mDBA17E7125E65329DCE64F305C558DE9CC46BF2D,
	MediaConfig__ctor_m74B48595BB33375E761C1712B675F646CDE9C7EA,
	MediaConfig__ctor_m2E9B827C59D0E09C52CCAA02793EC90072E30A65,
	MediaConfig_DeepClone_m6F71EFBDA05F8E8DBDD0D6410AC174C745737AB5,
	MediaConfig_ToString_mA0EF133ECE94DB6D4E883799CF6698EC579C9E76,
	NULL,
	NULL,
	NULL,
	MessageDataBufferExt_AsStringUnicode_m370A6E9D2474F44FED29CEFE09DFFF19D8AD8D58,
	MessageDataBufferExt_Copy_m6628C408FF9B181C8AD9737AF5A8FBA0C03BC8EC,
	NetworkConfig_get_IceServers_m4BCDF260A6752D59C158653947B7249082E4C840,
	NetworkConfig_set_IceServers_mADC0293C8A6ECF63990157DE40D6DD2AF4C0E76F,
	NetworkConfig_get_SignalingUrl_mFB0516DF1BC404414AC3490A36F3B65465255CD2,
	NetworkConfig_set_SignalingUrl_mEEBC8B90F3497F8BBBB86DF5BD3A148FE20501F1,
	NetworkConfig_get_AllowRenegotiation_m19C17F8A7D7AC17DF1D412C1A420C18B0A176837,
	NetworkConfig_set_AllowRenegotiation_m0F6E94F27EBD3F2A7B5B1454C3DCB1A2D9F0AE0D,
	NetworkConfig_get_IsConference_mCEB7079529140541805C1E187121429FE6390697,
	NetworkConfig_set_IsConference_mD1E8F61D68F5D7B50D6E2EBAAFC93D9483D7B7A8,
	NetworkConfig_ToString_m4848DD3B8369705B20F2A2B023BF449CBF07B03D,
	NetworkConfig__ctor_m0AE0FDBD4ECDD5658C2D5844463A9F9D61F47EB5,
	NetworkEvent_get_Type_m749301699A91615CD355389E04E3E64032CFCB11,
	NetworkEvent_get_ConnectionId_m90682B9C1F478D36004F6EDE99AB2F3A44E87515,
	NetworkEvent_get_RawData_m010612D39AA930FDD4260A0D07FB12C9804E0192,
	NetworkEvent_get_MessageData_m7D424D00C7BA3B525623276580847793E27ECF59,
	NetworkEvent_GetDataAsByteArray_m568204908708933BC4A21BEC55ABC5305E6C2697,
	NetworkEvent_get_Info_m0CD887E4E39567B9DBAAEBC46879985482E9D0BE,
	NetworkEvent_get_ErrorInfo_m790CDBC78B6D32DC7B3ECC1D6E8CF7F149FE014F,
	NetworkEvent__ctor_mE67FC5AD4E0D46E8CE4DDA096620A94556B33F09,
	NetworkEvent__ctor_mEF44539E731EBBD4FAF7274A349D34F1126FD9E2,
	NetworkEvent__ctor_m2D27BD640BE50379B1C26B8F26E252CF6156E10D,
	NetworkEvent__ctor_m0CBD870B498BDA850F660472770077423A85B7FB,
	NetworkEvent__ctor_mB231B79419FA9D4FD449D7829F0E00D14CA380CD,
	NetworkEvent__ctor_mA46FFF771F9CD6E1C8DDFF476C0BD117007D5C80,
	NetworkEvent_ToString_mF0B49EAA98C221A242FCC1504B5566E7858F672C,
	NetworkEvent_IsMetaEvent_m488FA24505BD443DFF5E9D2952600893D365A418,
	NetworkEvent_FromByteArray_m7FFAB5917075130FBA65506BE2CE3FED4C6CE5CD,
	NetworkEvent_ToByteArray_m59E5C5E4CE57E1EB70CED4896D8ABA7595D4B216,
	NetworkEvent_AttachError_mF50D6D3BFBA8020CC7A93662FE52890703208DD4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BufferedFrame_get_Buffer_m2DC1E4D6558256FBE65AE72ABB2BF6B510F55E7C,
	BufferedFrame_get_Buffered_m036F73951959B01958AC9C87E6A56F2CD6E0648B,
	BufferedFrame_get_Width_m1D042156CEFFA7E4BAE4F06E928F9DE7F2D2FBDC,
	BufferedFrame_get_Height_m527372652D682AB4AAB832A94CF8E26CB99FA53A,
	BufferedFrame_get_Rotation_mC35A7A47EF7FFBF1225E3C5A8CA86D3ED2B90374,
	BufferedFrame_set_Rotation_mC873E609856C03CFC69158A31263CE2EF55F6B24,
	BufferedFrame_get_IsTopRowFirst_m124498F816E79AE7F73EFF212C21489B6A83669D,
	BufferedFrame_get_Format_m57DD4AA4A3694ECCC14343545EA292BD9F6955C8,
	BufferedFrame__ctor_m3C0223D8411940E1C73D7804A340BD357C0985E8,
	BufferedFrame_Dispose_mC7728357990326B087D1AE0E857F3A16FDC6F7C3,
	SLog_SetLogger_m16D93C917F17C91971C8226369C56D322B5CEBA3,
	SLog_LogException_mE70D0D801089CC84D895987FDC26DD12B8BD4624,
	SLog_LE_m9311AF7DA8AE883A95CEF0751ED30D23AA829B94,
	SLog_LW_m223C61D8BB1637F0AAC0AFD0FD940208B1412356,
	SLog_LV_m8ADFC439117C96B581699C9A745C42803C7ED830,
	SLog_LD_m93EBB0A73E255F5BAB5041BE21918104DC356190,
	SLog_L_m29F9D573958A70843D02EBB5A988FFD3D6023195,
	SLog_MergeTags_m39B4A76935379D71637AF195C96080BEC8446790,
	SLog_LogArray_m770776BC1B04DFC6AB22EFECD69502618FD451A8,
	SLog__cctor_mA7C925C138233A2168DCCDC1C948C7DDE239B9C3,
	AWebRtcCall_add_CallEvent_mF5E2043A18A81779B81E7B71D79B66E6A858E5E9,
	AWebRtcCall_remove_CallEvent_m415EF2CA82CC47DE3F08FC15C6D44A23FE46DA29,
	AWebRtcCall_get_IsDisposed_mBFAD74A5A449B98C53F78349CFAAFE6CCFFA1967,
	AWebRtcCall_get_State_mC06D1CCFEB1DCCE3A79B64685AFBB220EE649E90,
	AWebRtcCall_get_LocalFrameEvents_m5E6717880FDDAE5E981EFC5FD1E4BD22E613A03C,
	AWebRtcCall_set_LocalFrameEvents_m0F4C60FD1DEB09A683E4D4B759262749B75C4B90,
	AWebRtcCall__ctor_mEE582AB1E17117B48C1E2F309C072D8642442F2E,
	AWebRtcCall_Initialize_m186CA4FF92DD35F0CF1A187388C93FD56F5FACDF,
	AWebRtcCall_Configure_mBAB34E64CD89C3D1A45DA1189A8D68567CD0E703,
	AWebRtcCall_Call_m7AD1C0845C10BCB4385A2E8A11CF35BD4DAE8CF0,
	AWebRtcCall_Listen_m479C190B936DFBCDF396CF6A8B0921709FA6CB87,
	AWebRtcCall_Send_m442434E182EF4AE1DA94CB9E318C697590304AEC,
	AWebRtcCall_Send_m02ED4A8CFD3A119AAAD018DB68049D449702CC7D,
	AWebRtcCall_Send_mABD05B4BCBB16C7FEB8B7CDEBCEC93658227F6E4,
	AWebRtcCall_IsStringMsg_mF6FD7F7E50E5BA64659CE3B0D0C415A39CB1494E,
	AWebRtcCall_PackStringMsg_m541C916DFFE349A27ABBCAB140307EC120F152E9,
	AWebRtcCall_UnpackStringMsg_mB0CFF7A6E1A0AEC6D71E858CEB3F319A82663DFF,
	AWebRtcCall_Send_m1DDEE2CCA960758A3E7644CA581D10DF5BC7F479,
	AWebRtcCall_Send_mE8B14947EAD420755DE0F063556C50560C6B5C7B,
	AWebRtcCall_IsDataMsg_m854E78A88BCBC5ECEA5F9D6D9C32988C59C7BD2E,
	AWebRtcCall_PackDataMsg_m543EB718C49FE11C9E29DB123C8EF7C416A1D536,
	AWebRtcCall_UnpackDataMsg_m8E6178BC303306E10C298AE83585FEDAAF5AE304,
	AWebRtcCall_Update_m26AD9EE7A5838EA667BB7F6A9229EDF9C1B4D0F2,
	AWebRtcCall_HandleMediaEvents_mA3DC62D29AB1CD7D3405200F1C5C594BC6FEA440,
	AWebRtcCall_HandleNetworkEvent_mF0E4D34AF018B51031B80D19CBA7F3C822164AEF,
	AWebRtcCall_PendingCall_m552EB57809B7C78DE965B00A0C2D2447A5CA9525,
	AWebRtcCall_ProcessCall_m2EA3C171A75C255710FD4E301EDCAE973714330D,
	AWebRtcCall_PendingListen_m68D65334F0F12E0BB3C85F4ACB0794B3394C714D,
	AWebRtcCall_ProcessListen_m942E1723FDA5BFB8D96C67AF2CFAE0A76693FC31,
	AWebRtcCall_DoPending_m3D7190BD67EF62312E0189A439762F880B108FEB,
	AWebRtcCall_ClearPending_m2E8CD1E54BB41FF79F840F768969FE6479DD2271,
	AWebRtcCall_CheckDisposed_mD72D8B22A9516A1CDB74FEE6438E202C3D86F47E,
	AWebRtcCall_EnsureConfiguration_m10234AA21678F6EC618D59034E311D7ABEB09186,
	AWebRtcCall_TriggerCallEvent_mD513FF5D24DC4C1DC4F7409C267E239EDF71270F,
	AWebRtcCall_OnConfigurationComplete_m5C34524B1CE3482E6E21B5A326F3A31FA55B123F,
	AWebRtcCall_OnConfigurationFailed_m224529A37E7AF75501895C4778192A724CE4669A,
	AWebRtcCall_Dispose_m8BBA38E44EFE6864C542234F1C5B8B7FCA9AB302,
	AWebRtcCall_Dispose_m4EF37AAFAC8AC5FC0F40C78310645CB9E8F38053,
	AWebRtcCall_SetVolume_m7E9F191FE0A0BA66A8BAB99D1EAC1DBBB875FB05,
	AWebRtcCall_HasAudioTrack_m1F61BD2A192DEC289A296765886084070646EA2A,
	AWebRtcCall_HasVideoTrack_m145EF1FFAB3F20B66361802DEEB493D80E621626,
	AWebRtcCall_IsMute_mCE992965B66543C8CEF3C190ADF9057842605EC3,
	AWebRtcCall_SetMute_mEC72F131218DC1ED27E25950FF43C564AA88970D,
	AWebRtcCall_GetBufferedAmount_mBDC0FD55B00C098BBBFD82FF564F1F6EFF12D0CA,
	AWebRtcCall__cctor_m84DF3FE9298FC726481005367736FE6069C185E3,
};
extern void ConnectionId__ctor_mD6FB77426F0ACC9BFB11034E4C2CAA6052DE5FA7_AdjustorThunk (void);
extern void ConnectionId_Equals_m35743E4DD4A06BBB041AA3038196950D6956EDF6_AdjustorThunk (void);
extern void ConnectionId_IsValid_m5D1825E8CAF6F48D4C8B64AF60AC46FDF24E7011_AdjustorThunk (void);
extern void ConnectionId_GetHashCode_m0401767F6FD490AE5F6F04D3AAD3ABFA59010AB3_AdjustorThunk (void);
extern void ConnectionId_ToString_mCE02582732D141880A5883AC6A8D852A5C756652_AdjustorThunk (void);
extern void NetworkEvent_get_Type_m749301699A91615CD355389E04E3E64032CFCB11_AdjustorThunk (void);
extern void NetworkEvent_get_ConnectionId_m90682B9C1F478D36004F6EDE99AB2F3A44E87515_AdjustorThunk (void);
extern void NetworkEvent_get_RawData_m010612D39AA930FDD4260A0D07FB12C9804E0192_AdjustorThunk (void);
extern void NetworkEvent_get_MessageData_m7D424D00C7BA3B525623276580847793E27ECF59_AdjustorThunk (void);
extern void NetworkEvent_GetDataAsByteArray_m568204908708933BC4A21BEC55ABC5305E6C2697_AdjustorThunk (void);
extern void NetworkEvent_get_Info_m0CD887E4E39567B9DBAAEBC46879985482E9D0BE_AdjustorThunk (void);
extern void NetworkEvent_get_ErrorInfo_m790CDBC78B6D32DC7B3ECC1D6E8CF7F149FE014F_AdjustorThunk (void);
extern void NetworkEvent__ctor_mE67FC5AD4E0D46E8CE4DDA096620A94556B33F09_AdjustorThunk (void);
extern void NetworkEvent__ctor_mEF44539E731EBBD4FAF7274A349D34F1126FD9E2_AdjustorThunk (void);
extern void NetworkEvent__ctor_m2D27BD640BE50379B1C26B8F26E252CF6156E10D_AdjustorThunk (void);
extern void NetworkEvent__ctor_m0CBD870B498BDA850F660472770077423A85B7FB_AdjustorThunk (void);
extern void NetworkEvent__ctor_mB231B79419FA9D4FD449D7829F0E00D14CA380CD_AdjustorThunk (void);
extern void NetworkEvent__ctor_mA46FFF771F9CD6E1C8DDFF476C0BD117007D5C80_AdjustorThunk (void);
extern void NetworkEvent_ToString_mF0B49EAA98C221A242FCC1504B5566E7858F672C_AdjustorThunk (void);
extern void NetworkEvent_AttachError_mF50D6D3BFBA8020CC7A93662FE52890703208DD4_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[20] = 
{
	{ 0x06000036, ConnectionId__ctor_mD6FB77426F0ACC9BFB11034E4C2CAA6052DE5FA7_AdjustorThunk },
	{ 0x06000037, ConnectionId_Equals_m35743E4DD4A06BBB041AA3038196950D6956EDF6_AdjustorThunk },
	{ 0x06000038, ConnectionId_IsValid_m5D1825E8CAF6F48D4C8B64AF60AC46FDF24E7011_AdjustorThunk },
	{ 0x06000039, ConnectionId_GetHashCode_m0401767F6FD490AE5F6F04D3AAD3ABFA59010AB3_AdjustorThunk },
	{ 0x06000040, ConnectionId_ToString_mCE02582732D141880A5883AC6A8D852A5C756652_AdjustorThunk },
	{ 0x060000C8, NetworkEvent_get_Type_m749301699A91615CD355389E04E3E64032CFCB11_AdjustorThunk },
	{ 0x060000C9, NetworkEvent_get_ConnectionId_m90682B9C1F478D36004F6EDE99AB2F3A44E87515_AdjustorThunk },
	{ 0x060000CA, NetworkEvent_get_RawData_m010612D39AA930FDD4260A0D07FB12C9804E0192_AdjustorThunk },
	{ 0x060000CB, NetworkEvent_get_MessageData_m7D424D00C7BA3B525623276580847793E27ECF59_AdjustorThunk },
	{ 0x060000CC, NetworkEvent_GetDataAsByteArray_m568204908708933BC4A21BEC55ABC5305E6C2697_AdjustorThunk },
	{ 0x060000CD, NetworkEvent_get_Info_m0CD887E4E39567B9DBAAEBC46879985482E9D0BE_AdjustorThunk },
	{ 0x060000CE, NetworkEvent_get_ErrorInfo_m790CDBC78B6D32DC7B3ECC1D6E8CF7F149FE014F_AdjustorThunk },
	{ 0x060000CF, NetworkEvent__ctor_mE67FC5AD4E0D46E8CE4DDA096620A94556B33F09_AdjustorThunk },
	{ 0x060000D0, NetworkEvent__ctor_mEF44539E731EBBD4FAF7274A349D34F1126FD9E2_AdjustorThunk },
	{ 0x060000D1, NetworkEvent__ctor_m2D27BD640BE50379B1C26B8F26E252CF6156E10D_AdjustorThunk },
	{ 0x060000D2, NetworkEvent__ctor_m0CBD870B498BDA850F660472770077423A85B7FB_AdjustorThunk },
	{ 0x060000D3, NetworkEvent__ctor_mB231B79419FA9D4FD449D7829F0E00D14CA380CD_AdjustorThunk },
	{ 0x060000D4, NetworkEvent__ctor_mA46FFF771F9CD6E1C8DDFF476C0BD117007D5C80_AdjustorThunk },
	{ 0x060000D5, NetworkEvent_ToString_mF0B49EAA98C221A242FCC1504B5566E7858F672C_AdjustorThunk },
	{ 0x060000D9, NetworkEvent_AttachError_mF50D6D3BFBA8020CC7A93662FE52890703208DD4_AdjustorThunk },
};
static const int32_t s_InvokerIndices[291] = 
{
	9247,
	7557,
	9247,
	7557,
	9247,
	9247,
	9247,
	9289,
	9247,
	7557,
	9161,
	7557,
	7597,
	2387,
	9442,
	9442,
	2387,
	14502,
	13757,
	14175,
	12839,
	9442,
	4362,
	4368,
	1462,
	7597,
	9247,
	7557,
	9173,
	7483,
	9173,
	7483,
	9247,
	9289,
	9289,
	4037,
	9289,
	9289,
	7597,
	9173,
	9161,
	9289,
	2256,
	9173,
	9289,
	9161,
	2256,
	9247,
	9173,
	9247,
	9161,
	9289,
	3674,
	7556,
	5478,
	9161,
	9247,
	12413,
	12413,
	12413,
	12413,
	12413,
	12413,
	9289,
	14502,
	14423,
	14242,
	14502,
	9289,
	7597,
	9289,
	14502,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9289,
	9289,
	9289,
	2423,
	2423,
	9289,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9289,
	9161,
	9442,
	5478,
	7597,
	9442,
	5860,
	9442,
	443,
	9442,
	5304,
	5304,
	9442,
	7483,
	5860,
	9173,
	7597,
	2246,
	2246,
	3660,
	7596,
	845,
	7483,
	7597,
	9442,
	7469,
	9442,
	14502,
	0,
	0,
	9161,
	7469,
	9161,
	7469,
	9289,
	7597,
	8996,
	7312,
	8996,
	7312,
	8996,
	7312,
	8996,
	7312,
	8996,
	7312,
	8996,
	7312,
	8996,
	7312,
	8996,
	7312,
	8996,
	7312,
	9247,
	7557,
	9442,
	7597,
	9289,
	9289,
	0,
	0,
	0,
	13922,
	13922,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9161,
	7469,
	9289,
	9442,
	9161,
	9173,
	9289,
	9289,
	9289,
	9289,
	9289,
	7469,
	3660,
	2246,
	2246,
	2246,
	2246,
	9289,
	13600,
	13822,
	13921,
	7597,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9289,
	9161,
	9247,
	9247,
	9247,
	7557,
	9161,
	9247,
	378,
	9442,
	14256,
	13266,
	13266,
	13266,
	13266,
	13266,
	13266,
	12869,
	13266,
	14502,
	7597,
	7597,
	9161,
	9247,
	9161,
	7469,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	4345,
	1857,
	13600,
	13922,
	13922,
	4345,
	1857,
	13600,
	13922,
	13922,
	9442,
	9442,
	7596,
	7597,
	7597,
	7597,
	7597,
	9442,
	9442,
	9442,
	9442,
	7597,
	9442,
	7597,
	7469,
	9442,
	3682,
	5375,
	5375,
	9161,
	7469,
	3074,
	14502,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0200001A, { 0, 1 } },
};
extern const uint32_t g_rgctx_T_t04D08A141CFB361E30E3564B8319B4048A35FC24;
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t04D08A141CFB361E30E3564B8319B4048A35FC24 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Byn_Awrtc_CodeGenModule;
const Il2CppCodeGenModule g_Byn_Awrtc_CodeGenModule = 
{
	"Byn.Awrtc.dll",
	291,
	s_methodPointers,
	20,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
