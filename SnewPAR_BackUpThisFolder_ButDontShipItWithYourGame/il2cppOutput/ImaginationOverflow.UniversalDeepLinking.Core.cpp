﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


struct VirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericVirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InvokerActionInvoker1;
template <typename T1>
struct InvokerActionInvoker1<T1*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1)
	{
		void* params[1] = { p1 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename T1, typename T2>
struct InvokerActionInvoker2;
template <typename T1, typename T2>
struct InvokerActionInvoker2<T1*, T2*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1, T2* p2)
	{
		void* params[2] = { p1, p2 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83;
// System.Func`2<System.Object,System.Object>
struct Func_2_tACBF5A1656250800CE861707354491F0611F6624;
// System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>>
struct Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_tAB0692B406AF1455ADB5F518BF283E084B5E8566;
// System.Func`3<System.String,System.String,System.String>
struct Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342;
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>
struct List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t238D0D2427C6B841A01F522A41540165A2C4AE76;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t1AF33AD0B7330843448956EC4277517081658AE7;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// ImaginationOverflow.UniversalDeepLinking.LinkInformation[]
struct LinkInformationU5BU5D_tB214D07770439029C30C56F02D0D3F8506663F2E;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[]
struct PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration
struct AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D;
// System.ArgumentNullException
struct ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129;
// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C;
// System.Diagnostics.AsyncStreamReader
struct AsyncStreamReader_tAC4F4C9FDA0C1A8D21F2B1C838C08C0E83AE871B;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage
struct ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738;
// System.Diagnostics.DataReceivedEventHandler
struct DataReceivedEventHandler_t9E71CD00F9E67F5049738A8A67CD54B145C70BDC;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.IO.DirectoryInfo
struct DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2;
// System.EventHandler
struct EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t057D7531265C1DF014C8C83AF251E908D1A0B1C8;
// System.Diagnostics.FileVersionInfo
struct FileVersionInfo_t9EBE91A5AB1AA47C86C4AF23ECDA7D17B3635109;
// System.IAsyncResult
struct IAsyncResult_t7B9B5A0ECB35DCEC31B8A8122C37D687369253B5;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.ComponentModel.ISite
struct ISite_t4BB2A7E2B477FC6B1AF9D0554FF8B07204356E93;
// System.ComponentModel.ISynchronizeInvoke
struct ISynchronizeInvoke_t94542FC52B3B1FCA7BC4D8CC518FC2EF9870861F;
// ImaginationOverflow.UniversalDeepLinking.LinkActivation
struct LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9;
// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler
struct LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B;
// ImaginationOverflow.UniversalDeepLinking.LinkInformation
struct LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.OperatingSystem
struct OperatingSystem_t08A94435A5C7D999B5553B6C58763A6F2E3C8557;
// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration
struct PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D;
// System.Diagnostics.Process
struct Process_tF74794C64BCE464912BF158980B347CE66AF203B;
// System.Diagnostics.ProcessModule
struct ProcessModule_t7CCBC3E3C3382F1A431C2CE645F06224FC47BC16;
// System.Diagnostics.ProcessModuleCollection
struct ProcessModuleCollection_tB2EBC893262A796A0182EDF0022F0B08E30461EE;
// System.Diagnostics.ProcessStartInfo
struct ProcessStartInfo_t03E06D8098D3DC01CDACE23EE2D308BDA8E41D3C;
// System.Diagnostics.ProcessThreadCollection
struct ProcessThreadCollection_t9E9F6B3EB7E8031736898D77DD88F2BD29740971;
// System.Threading.RegisteredWaitHandle
struct RegisteredWaitHandle_t5AEE89AB4B4A54EAC5B66A72A0D7D2EF8C82EC86;
// Microsoft.Win32.SafeHandles.SafeProcessHandle
struct SafeProcessHandle_tA260D4420C5F481A5DA030FFB19D038BBF8A63CB;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B;
// System.IO.StreamWriter
struct StreamWriter_t6E7DF7D524AA3C018A65F62EE80779873ED4D1E4;
// System.String
struct String_t;
// UnityEngine.TextAsset
struct TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69;
// System.Diagnostics.TraceSwitch
struct TraceSwitch_t199A0DB240149B5CDCF2754FC0E11CCBF51224B4;
// System.Type
struct Type_t;
// ImaginationOverflow.UniversalDeepLinking.UniversalLinkCallback
struct UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.Threading.WaitHandle
struct WaitHandle_t08F8DB54593B241FE32E0DD0BD3D82785D3AE3D8;
// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c
struct U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44;

IL2CPP_EXTERN_C RuntimeClass* AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral02693BE56D23D49FBADB98D7AF6D95B6C888CA51;
IL2CPP_EXTERN_C String_t* _stringLiteral1068D3F8264E2E8E66BC90F6D5203CBCD4000B13;
IL2CPP_EXTERN_C String_t* _stringLiteral78E86F042D371FD16F0D696EF476DAAA4953E187;
IL2CPP_EXTERN_C String_t* _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1;
IL2CPP_EXTERN_C String_t* _stringLiteralD2247DCC2B2C53AFB97450C7F94158072595172B;
IL2CPP_EXTERN_C String_t* _stringLiteralD5C4163563614243E6B5BF17A7ABA2017D1E7A0F;
IL2CPP_EXTERN_C const RuntimeMethod* ConfigurationStorage_CombinePaths_m12F876CBC435A7CE33249507E6C4FCAAB4867964_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Aggregate_TisString_t_mE91CF40C639FFB958F5EA190CCEE0EABE463D577_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* JsonUtility_FromJson_TisAppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_m38FB935DF4F56E2A675A82EAD5B59345C17CA750_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m5A31C469EDACD8D924FD141764D621183C50AFCA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mAE6DE6DEE0AA43171FDCED497C360002EB7B6471_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_Load_TisTextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69_m55E770DF81AB6D40763121667DA5E743EF1036B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CGetPlatformDeepLinkingProtocolsU3Eb__21_0_m0E722DDA2D24082FEA9E76FC570D0AA1E3B2319A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CGetPlatformDomainProtocolsU3Eb__22_0_m9D834A1E561FB5A6C0D92ACF8F69EB644BD23C71_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* SupportedPlatforms_tDF89E98E29751314B96DA5BEE3F6B7BF112EC5EF_0_0_0_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t6EA2A9AAAC630F93D7D1EEA029EFB53F6D37F301 
{
};

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t1AF33AD0B7330843448956EC4277517081658AE7* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t238D0D2427C6B841A01F522A41540165A2C4AE76* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>
struct List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	LinkInformationU5BU5D_tB214D07770439029C30C56F02D0D3F8506663F2E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration
struct AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D  : public RuntimeObject
{
	// System.String ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::_steamId
	String_t* ____steamId_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::_displayName
	String_t* ____displayName_1;
	// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::_globalConfiguration
	PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* ____globalConfiguration_2;
	// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[] ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::_customDeepLinkingProtocols
	PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* ____customDeepLinkingProtocols_3;
};

// ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage
struct ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738  : public RuntimeObject
{
};

// ImaginationOverflow.UniversalDeepLinking.LinkActivation
struct LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9  : public RuntimeObject
{
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::<Uri>k__BackingField
	String_t* ___U3CUriU3Ek__BackingField_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::<RawQueryString>k__BackingField
	String_t* ___U3CRawQueryStringU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.LinkActivation::<QueryString>k__BackingField
	Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___U3CQueryStringU3Ek__BackingField_2;
};

// ImaginationOverflow.UniversalDeepLinking.LinkInformation
struct LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A  : public RuntimeObject
{
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkInformation::_scheme
	String_t* ____scheme_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkInformation::_host
	String_t* ____host_1;
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkInformation::_path
	String_t* ____path_2;
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// System.IO.Path
struct Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC  : public RuntimeObject
{
};

// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration
struct PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D  : public RuntimeObject
{
	// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::_domainProtocols
	List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ____domainProtocols_0;
	// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::_deepLinkingProtocols
	List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ____deepLinkingProtocols_1;
	// System.Boolean ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::_initialized
	bool ____initialized_2;
};

// ImaginationOverflow.UniversalDeepLinking.ProviderHelpers
struct ProviderHelpers_t3F3861DF783A4558F177D667AD0CD503F80F1D5C  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c
struct U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44  : public RuntimeObject
{
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// System.ComponentModel.Component
struct Component_t7DA251DAA9E59801CC5FE8E27F37027143BED083  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
	// System.ComponentModel.ISite System.ComponentModel.Component::site
	RuntimeObject* ___site_2;
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::events
	EventHandlerList_t057D7531265C1DF014C8C83AF251E908D1A0B1C8* ___events_3;
};

// System.DateTime
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D 
{
	// System.UInt64 System.DateTime::_dateData
	uint64_t ____dateData_46;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// Interop/Sys/FileStatus
struct FileStatus_tCB96EDE0D0F945F685B9BBED6DBF0731207458C2 
{
	// Interop/Sys/FileStatusFlags Interop/Sys/FileStatus::Flags
	int32_t ___Flags_0;
	// System.Int32 Interop/Sys/FileStatus::Mode
	int32_t ___Mode_1;
	// System.UInt32 Interop/Sys/FileStatus::Uid
	uint32_t ___Uid_2;
	// System.UInt32 Interop/Sys/FileStatus::Gid
	uint32_t ___Gid_3;
	// System.Int64 Interop/Sys/FileStatus::Size
	int64_t ___Size_4;
	// System.Int64 Interop/Sys/FileStatus::ATime
	int64_t ___ATime_5;
	// System.Int64 Interop/Sys/FileStatus::ATimeNsec
	int64_t ___ATimeNsec_6;
	// System.Int64 Interop/Sys/FileStatus::MTime
	int64_t ___MTime_7;
	// System.Int64 Interop/Sys/FileStatus::MTimeNsec
	int64_t ___MTimeNsec_8;
	// System.Int64 Interop/Sys/FileStatus::CTime
	int64_t ___CTime_9;
	// System.Int64 Interop/Sys/FileStatus::CTimeNsec
	int64_t ___CTimeNsec_10;
	// System.Int64 Interop/Sys/FileStatus::BirthTime
	int64_t ___BirthTime_11;
	// System.Int64 Interop/Sys/FileStatus::BirthTimeNsec
	int64_t ___BirthTimeNsec_12;
	// System.Int64 Interop/Sys/FileStatus::Dev
	int64_t ___Dev_13;
	// System.Int64 Interop/Sys/FileStatus::Ino
	int64_t ___Ino_14;
	// System.UInt32 Interop/Sys/FileStatus::UserFlags
	uint32_t ___UserFlags_15;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// System.IO.FileStatus
struct FileStatus_tABB5F252F1E597EC95E9041035DC424EF66712A5 
{
	// Interop/Sys/FileStatus System.IO.FileStatus::_fileStatus
	FileStatus_tCB96EDE0D0F945F685B9BBED6DBF0731207458C2 ____fileStatus_0;
	// System.Int32 System.IO.FileStatus::_fileStatusInitialized
	int32_t ____fileStatusInitialized_1;
	// System.Boolean System.IO.FileStatus::<InitiallyDirectory>k__BackingField
	bool ___U3CInitiallyDirectoryU3Ek__BackingField_2;
	// System.Boolean System.IO.FileStatus::_isDirectory
	bool ____isDirectory_3;
	// System.Boolean System.IO.FileStatus::_exists
	bool ____exists_4;
};
// Native definition for P/Invoke marshalling of System.IO.FileStatus
struct FileStatus_tABB5F252F1E597EC95E9041035DC424EF66712A5_marshaled_pinvoke
{
	FileStatus_tCB96EDE0D0F945F685B9BBED6DBF0731207458C2 ____fileStatus_0;
	int32_t ____fileStatusInitialized_1;
	int32_t ___U3CInitiallyDirectoryU3Ek__BackingField_2;
	int32_t ____isDirectory_3;
	int32_t ____exists_4;
};
// Native definition for COM marshalling of System.IO.FileStatus
struct FileStatus_tABB5F252F1E597EC95E9041035DC424EF66712A5_marshaled_com
{
	FileStatus_tCB96EDE0D0F945F685B9BBED6DBF0731207458C2 ____fileStatus_0;
	int32_t ____fileStatusInitialized_1;
	int32_t ___U3CInitiallyDirectoryU3Ek__BackingField_2;
	int32_t ____isDirectory_3;
	int32_t ____exists_4;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.Diagnostics.Process
struct Process_tF74794C64BCE464912BF158980B347CE66AF203B  : public Component_t7DA251DAA9E59801CC5FE8E27F37027143BED083
{
	// System.Boolean System.Diagnostics.Process::haveProcessId
	bool ___haveProcessId_4;
	// System.Int32 System.Diagnostics.Process::processId
	int32_t ___processId_5;
	// System.Boolean System.Diagnostics.Process::haveProcessHandle
	bool ___haveProcessHandle_6;
	// Microsoft.Win32.SafeHandles.SafeProcessHandle System.Diagnostics.Process::m_processHandle
	SafeProcessHandle_tA260D4420C5F481A5DA030FFB19D038BBF8A63CB* ___m_processHandle_7;
	// System.Boolean System.Diagnostics.Process::isRemoteMachine
	bool ___isRemoteMachine_8;
	// System.String System.Diagnostics.Process::machineName
	String_t* ___machineName_9;
	// System.Int32 System.Diagnostics.Process::m_processAccess
	int32_t ___m_processAccess_10;
	// System.Diagnostics.ProcessThreadCollection System.Diagnostics.Process::threads
	ProcessThreadCollection_t9E9F6B3EB7E8031736898D77DD88F2BD29740971* ___threads_11;
	// System.Diagnostics.ProcessModuleCollection System.Diagnostics.Process::modules
	ProcessModuleCollection_tB2EBC893262A796A0182EDF0022F0B08E30461EE* ___modules_12;
	// System.Boolean System.Diagnostics.Process::haveWorkingSetLimits
	bool ___haveWorkingSetLimits_13;
	// System.IntPtr System.Diagnostics.Process::minWorkingSet
	intptr_t ___minWorkingSet_14;
	// System.IntPtr System.Diagnostics.Process::maxWorkingSet
	intptr_t ___maxWorkingSet_15;
	// System.Boolean System.Diagnostics.Process::havePriorityClass
	bool ___havePriorityClass_16;
	// System.Diagnostics.ProcessPriorityClass System.Diagnostics.Process::priorityClass
	int32_t ___priorityClass_17;
	// System.Diagnostics.ProcessStartInfo System.Diagnostics.Process::startInfo
	ProcessStartInfo_t03E06D8098D3DC01CDACE23EE2D308BDA8E41D3C* ___startInfo_18;
	// System.Boolean System.Diagnostics.Process::watchForExit
	bool ___watchForExit_19;
	// System.Boolean System.Diagnostics.Process::watchingForExit
	bool ___watchingForExit_20;
	// System.EventHandler System.Diagnostics.Process::onExited
	EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82* ___onExited_21;
	// System.Boolean System.Diagnostics.Process::exited
	bool ___exited_22;
	// System.Int32 System.Diagnostics.Process::exitCode
	int32_t ___exitCode_23;
	// System.Boolean System.Diagnostics.Process::signaled
	bool ___signaled_24;
	// System.DateTime System.Diagnostics.Process::exitTime
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___exitTime_25;
	// System.Boolean System.Diagnostics.Process::haveExitTime
	bool ___haveExitTime_26;
	// System.Boolean System.Diagnostics.Process::raisedOnExited
	bool ___raisedOnExited_27;
	// System.Threading.RegisteredWaitHandle System.Diagnostics.Process::registeredWaitHandle
	RegisteredWaitHandle_t5AEE89AB4B4A54EAC5B66A72A0D7D2EF8C82EC86* ___registeredWaitHandle_28;
	// System.Threading.WaitHandle System.Diagnostics.Process::waitHandle
	WaitHandle_t08F8DB54593B241FE32E0DD0BD3D82785D3AE3D8* ___waitHandle_29;
	// System.ComponentModel.ISynchronizeInvoke System.Diagnostics.Process::synchronizingObject
	RuntimeObject* ___synchronizingObject_30;
	// System.IO.StreamReader System.Diagnostics.Process::standardOutput
	StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* ___standardOutput_31;
	// System.IO.StreamWriter System.Diagnostics.Process::standardInput
	StreamWriter_t6E7DF7D524AA3C018A65F62EE80779873ED4D1E4* ___standardInput_32;
	// System.IO.StreamReader System.Diagnostics.Process::standardError
	StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* ___standardError_33;
	// System.OperatingSystem System.Diagnostics.Process::operatingSystem
	OperatingSystem_t08A94435A5C7D999B5553B6C58763A6F2E3C8557* ___operatingSystem_34;
	// System.Boolean System.Diagnostics.Process::disposed
	bool ___disposed_35;
	// System.Diagnostics.Process/StreamReadMode System.Diagnostics.Process::outputStreamReadMode
	int32_t ___outputStreamReadMode_36;
	// System.Diagnostics.Process/StreamReadMode System.Diagnostics.Process::errorStreamReadMode
	int32_t ___errorStreamReadMode_37;
	// System.Diagnostics.Process/StreamReadMode System.Diagnostics.Process::inputStreamReadMode
	int32_t ___inputStreamReadMode_38;
	// System.Diagnostics.DataReceivedEventHandler System.Diagnostics.Process::OutputDataReceived
	DataReceivedEventHandler_t9E71CD00F9E67F5049738A8A67CD54B145C70BDC* ___OutputDataReceived_39;
	// System.Diagnostics.DataReceivedEventHandler System.Diagnostics.Process::ErrorDataReceived
	DataReceivedEventHandler_t9E71CD00F9E67F5049738A8A67CD54B145C70BDC* ___ErrorDataReceived_40;
	// System.Diagnostics.AsyncStreamReader System.Diagnostics.Process::output
	AsyncStreamReader_tAC4F4C9FDA0C1A8D21F2B1C838C08C0E83AE871B* ___output_41;
	// System.Diagnostics.AsyncStreamReader System.Diagnostics.Process::error
	AsyncStreamReader_tAC4F4C9FDA0C1A8D21F2B1C838C08C0E83AE871B* ___error_42;
	// System.Boolean System.Diagnostics.Process::pendingOutputRead
	bool ___pendingOutputRead_43;
	// System.Boolean System.Diagnostics.Process::pendingErrorRead
	bool ___pendingErrorRead_44;
	// System.String System.Diagnostics.Process::process_name
	String_t* ___process_name_46;
};

// System.Diagnostics.ProcessModule
struct ProcessModule_t7CCBC3E3C3382F1A431C2CE645F06224FC47BC16  : public Component_t7DA251DAA9E59801CC5FE8E27F37027143BED083
{
	// System.IntPtr System.Diagnostics.ProcessModule::baseaddr
	intptr_t ___baseaddr_4;
	// System.IntPtr System.Diagnostics.ProcessModule::entryaddr
	intptr_t ___entryaddr_5;
	// System.String System.Diagnostics.ProcessModule::filename
	String_t* ___filename_6;
	// System.Diagnostics.FileVersionInfo System.Diagnostics.ProcessModule::version_info
	FileVersionInfo_t9EBE91A5AB1AA47C86C4AF23ECDA7D17B3635109* ___version_info_7;
	// System.Int32 System.Diagnostics.ProcessModule::memory_size
	int32_t ___memory_size_8;
	// System.String System.Diagnostics.ProcessModule::modulename
	String_t* ___modulename_9;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// System.IO.FileSystemInfo
struct FileSystemInfo_tE3063B9229F46B05A5F6D018C8C4CA510104E8E9  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
	// System.IO.FileStatus System.IO.FileSystemInfo::_fileStatus
	FileStatus_tABB5F252F1E597EC95E9041035DC424EF66712A5 ____fileStatus_1;
	// System.String System.IO.FileSystemInfo::FullPath
	String_t* ___FullPath_2;
	// System.String System.IO.FileSystemInfo::OriginalPath
	String_t* ___OriginalPath_3;
	// System.String System.IO.FileSystemInfo::_name
	String_t* ____name_4;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.TextAsset
struct TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

// System.Func`2<System.Object,System.Object>
struct Func_2_tACBF5A1656250800CE861707354491F0611F6624  : public MulticastDelegate_t
{
};

// System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>>
struct Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E  : public MulticastDelegate_t
{
};

// System.Func`3<System.String,System.String,System.String>
struct Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA  : public MulticastDelegate_t
{
};

// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
	// System.String System.ArgumentException::_paramName
	String_t* ____paramName_18;
};

// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C  : public MulticastDelegate_t
{
};

// System.IO.DirectoryInfo
struct DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2  : public FileSystemInfo_tE3063B9229F46B05A5F6D018C8C4CA510104E8E9
{
};

// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler
struct LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B  : public MulticastDelegate_t
{
};

// ImaginationOverflow.UniversalDeepLinking.UniversalLinkCallback
struct UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0  : public MulticastDelegate_t
{
};

// System.ArgumentNullException
struct ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129  : public ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263
{
};

// <Module>

// <Module>

// System.Collections.Generic.Dictionary`2<System.String,System.String>

// System.Collections.Generic.Dictionary`2<System.String,System.String>

// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>
struct List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	LinkInformationU5BU5D_tB214D07770439029C30C56F02D0D3F8506663F2E* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration

// ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage
struct ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields
{
	// System.String[] ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::SaveFolders
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___SaveFolders_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::SaveFile
	String_t* ___SaveFile_1;
	// System.String ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::_fileLocation
	String_t* ____fileLocation_2;
};

// ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage

// ImaginationOverflow.UniversalDeepLinking.LinkActivation

// ImaginationOverflow.UniversalDeepLinking.LinkActivation

// ImaginationOverflow.UniversalDeepLinking.LinkInformation

// ImaginationOverflow.UniversalDeepLinking.LinkInformation

// System.MarshalByRefObject

// System.MarshalByRefObject

// System.Reflection.MemberInfo

// System.Reflection.MemberInfo

// System.IO.Path
struct Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_StaticFields
{
	// System.Char[] System.IO.Path::InvalidPathChars
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___InvalidPathChars_0;
	// System.Char System.IO.Path::AltDirectorySeparatorChar
	Il2CppChar ___AltDirectorySeparatorChar_1;
	// System.Char System.IO.Path::DirectorySeparatorChar
	Il2CppChar ___DirectorySeparatorChar_2;
	// System.Char System.IO.Path::PathSeparator
	Il2CppChar ___PathSeparator_3;
	// System.String System.IO.Path::DirectorySeparatorStr
	String_t* ___DirectorySeparatorStr_4;
	// System.Char System.IO.Path::VolumeSeparatorChar
	Il2CppChar ___VolumeSeparatorChar_5;
	// System.Char[] System.IO.Path::PathSeparatorChars
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___PathSeparatorChars_6;
	// System.Boolean System.IO.Path::dirEqualsVolume
	bool ___dirEqualsVolume_7;
	// System.Char[] System.IO.Path::trimEndCharsWindows
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___trimEndCharsWindows_8;
	// System.Char[] System.IO.Path::trimEndCharsUnix
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___trimEndCharsUnix_9;
};

// System.IO.Path

// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration

// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration

// ImaginationOverflow.UniversalDeepLinking.ProviderHelpers

// ImaginationOverflow.UniversalDeepLinking.ProviderHelpers

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.ValueType

// System.ValueType

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c
struct U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields
{
	// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::<>9
	U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44* ___U3CU3E9_0;
	// System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::<>9__21_0
	Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* ___U3CU3E9__21_0_1;
	// System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::<>9__22_0
	Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* ___U3CU3E9__22_0_2;
};

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// System.ComponentModel.Component
struct Component_t7DA251DAA9E59801CC5FE8E27F37027143BED083_StaticFields
{
	// System.Object System.ComponentModel.Component::EventDisposed
	RuntimeObject* ___EventDisposed_1;
};

// System.ComponentModel.Component

// System.DateTime
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_StaticFields
{
	// System.Int32[] System.DateTime::s_daysToMonth365
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth365_30;
	// System.Int32[] System.DateTime::s_daysToMonth366
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth366_31;
	// System.DateTime System.DateTime::MinValue
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MinValue_32;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MaxValue_33;
	// System.DateTime System.DateTime::UnixEpoch
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___UnixEpoch_34;
};

// System.DateTime

// System.Int32

// System.Int32

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// System.Void

// System.Void

// Interop/Sys/FileStatus

// Interop/Sys/FileStatus

// System.Delegate

// System.Delegate

// System.Exception
struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};

// System.Exception

// System.IO.FileStatus

// System.IO.FileStatus

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// System.Diagnostics.Process
struct Process_tF74794C64BCE464912BF158980B347CE66AF203B_StaticFields
{
	// System.Diagnostics.TraceSwitch System.Diagnostics.Process::processTracing
	TraceSwitch_t199A0DB240149B5CDCF2754FC0E11CCBF51224B4* ___processTracing_45;
	// System.Diagnostics.ProcessModule System.Diagnostics.Process::current_main_module
	ProcessModule_t7CCBC3E3C3382F1A431C2CE645F06224FC47BC16* ___current_main_module_47;
};

// System.Diagnostics.Process

// System.Diagnostics.ProcessModule

// System.Diagnostics.ProcessModule

// System.RuntimeTypeHandle

// System.RuntimeTypeHandle

// System.IO.FileSystemInfo

// System.IO.FileSystemInfo

// System.MulticastDelegate

// System.MulticastDelegate

// System.SystemException

// System.SystemException

// UnityEngine.TextAsset

// UnityEngine.TextAsset

// System.Type
struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Type

// System.Func`2<System.Object,System.Object>

// System.Func`2<System.Object,System.Object>

// System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>>

// System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>>

// System.Func`3<System.String,System.String,System.String>

// System.Func`3<System.String,System.String,System.String>

// System.ArgumentException

// System.ArgumentException

// System.AsyncCallback

// System.AsyncCallback

// System.IO.DirectoryInfo

// System.IO.DirectoryInfo

// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler

// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler

// ImaginationOverflow.UniversalDeepLinking.UniversalLinkCallback

// ImaginationOverflow.UniversalDeepLinking.UniversalLinkCallback

// System.ArgumentNullException

// System.ArgumentNullException
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[]
struct PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C  : public RuntimeArray
{
	ALIGN_FIELD (8) PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* m_Items[1];

	inline PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771  : public RuntimeArray
{
	ALIGN_FIELD (8) Delegate_t* m_Items[1];

	inline Delegate_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared (Func_2_tACBF5A1656250800CE861707354491F0611F6624* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) ;
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Func_2_Invoke_mDBA25DA5DA5B7E056FB9B026AF041F1385FB58A9_gshared_inline (Func_2_tACBF5A1656250800CE861707354491F0611F6624* __this, RuntimeObject* ___0_arg, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// T UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* JsonUtility_FromJson_TisRuntimeObject_m0CCF0FE109BF4C85AECC9C5D0DBB43422A24FB40_gshared (String_t* ___0_json, const RuntimeMethod* method) ;
// T UnityEngine.Resources::Load<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Resources_Load_TisRuntimeObject_mD1AF6299B14F87ED1D1A6199A51480919F7C79D7_gshared (String_t* ___0_path, const RuntimeMethod* method) ;
// System.Void System.Func`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_3__ctor_m7A3CDF8CC909FAEEA005D42C71F113B505F766DD_gshared (Func_3_tAB0692B406AF1455ADB5F518BF283E084B5E8566* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) ;
// TSource System.Linq.Enumerable::Aggregate<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,TSource,TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Aggregate_TisRuntimeObject_mA6E191832F9B71EC31D2C3115CADC5F16EE18182_gshared (RuntimeObject* ___0_source, Func_3_tAB0692B406AF1455ADB5F518BF283E084B5E8566* ___1_func, const RuntimeMethod* method) ;

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>::.ctor()
inline void List_1__ctor_mAE6DE6DEE0AA43171FDCED497C360002EB7B6471 (List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::set_DeepLinkingProtocols(System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration_set_DeepLinkingProtocols_m80CA6C00E0DF7DF62C13917F48FB5D76FD8E7950_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___0_value, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::set_DomainProtocols(System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration_set_DomainProtocols_m2C70E312EA840E54AAEA0804B0F8D56F21628345_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___0_value, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::get_DeepLinkingProtocols()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* PlatformLinkingConfiguration_get_DeepLinkingProtocols_m640ED35F65E1CF82EB519515AD89FA1C0A065EBE_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::get_DomainProtocols()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* PlatformLinkingConfiguration_get_DomainProtocols_m0A8AA114FAB59AAD0021165117A89B652262B853_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, const RuntimeMethod* method) ;
// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[] ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::EnsureAllPlats(ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* AppLinkingConfiguration_EnsureAllPlats_mFC658CFBDBC8D8931F964E56AE4ECA9F39BECFFF (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* ___0_value, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration__ctor_m019E37CFC7696ED6817CF4C76B55F0F79223BD07 (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, bool ___0_init, const RuntimeMethod* method) ;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57 (RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___0_handle, const RuntimeMethod* method) ;
// System.Array System.Enum::GetValues(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeArray* Enum_GetValues_m803B9D68C367FAABC5AFB6B5B52775C8A573CEF9 (Type_t* ___0_enumType, const RuntimeMethod* method) ;
// System.Int32 System.Array::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Array_get_Length_m361285FB7CF44045DC369834D1CD01F72F94EF57 (RuntimeArray* __this, const RuntimeMethod* method) ;
// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[] ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::get_CustomDeepLinkingProtocols()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* AppLinkingConfiguration_get_CustomDeepLinkingProtocols_m2F50EDD2311D022262163A4B95A7E0CA70E888FB_inline (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::set_CustomDeepLinkingProtocols(ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_set_CustomDeepLinkingProtocols_mE5309BA35444E52AE002D0846840BF22609D982A (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* ___0_value, const RuntimeMethod* method) ;
// System.Void System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m0818E709D2E27AAA03D41D8155881B84BAE12923 (Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method)
{
	((  void (*) (Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m7F8A01C0B02BC1D4063F4EB1E817F7A48562A398_gshared)(__this, ___0_object, ___1_method, method);
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::get_DeepLinkingProtocols()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_get_DeepLinkingProtocols_m50073E6B86058B448D9110FE7B1CAB4D9D8AC260 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::GetCustomOrDefault(ImaginationOverflow.UniversalDeepLinking.SupportedPlatforms,System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>>,System.Boolean,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_GetCustomOrDefault_m16FADC2C379C973ABD46E5FCC07DCD21B074AF98 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, int32_t ___0_plat, Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* ___1_func, bool ___2_includeDefault, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___3_global, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::get_DomainProtocols()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_get_DomainProtocols_mFDF7686402812050EBCA7ECC700ACEC070FDAB16 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) ;
// System.Boolean ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::get_IsInitialized()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlatformLinkingConfiguration_get_IsInitialized_m883564B2DC7DA019B9D4E71D0E7B868339E68981_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, const RuntimeMethod* method) ;
// TResult System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>>::Invoke(T)
inline List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* Func_2_Invoke_mE2C35CEEAE1603CA239414F1D6642941FFF30362_inline (Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* __this, PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* ___0_arg, const RuntimeMethod* method)
{
	return ((  List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* (*) (Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E*, PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D*, const RuntimeMethod*))Func_2_Invoke_mDBA25DA5DA5B7E056FB9B026AF041F1385FB58A9_gshared_inline)(__this, ___0_arg, method);
}
// System.Void ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::set_IsInitialized(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration_set_IsInitialized_m16B4E7BE13C785D3127BA332D67EAF73319428FE_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, bool ___0_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>::Clear()
inline void List_1_Clear_m5A31C469EDACD8D924FD141764D621183C50AFCA_inline (List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8*, const RuntimeMethod*))List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline)(__this, method);
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mEA99269F7F948B44A55CB8F52410069A4D7F0252 (U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44* __this, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::set_Uri(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LinkActivation_set_Uri_m57367A6018A51D18C68BCA49E0CD41EEBF932C89_inline (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::set_RawQueryString(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LinkActivation_set_RawQueryString_m53B1488C42F6FD0891C168105A6164D616891234_inline (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::set_QueryString(System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LinkActivation_set_QueryString_m0D508E667298D0717BF7661D1A96D416D517343D_inline (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___0_value, const RuntimeMethod* method) ;
// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::get_Uri()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* LinkActivation_get_Uri_mC6D342ABF0ACE38B8D2725A1AF77BC0379B73980_inline (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, const RuntimeMethod* method) ;
// System.Diagnostics.Process System.Diagnostics.Process::GetCurrentProcess()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Process_tF74794C64BCE464912BF158980B347CE66AF203B* Process_GetCurrentProcess_mB9E146001302DA6A60946152A09E8205E5FD2F0E (const RuntimeMethod* method) ;
// System.Diagnostics.ProcessModule System.Diagnostics.Process::get_MainModule()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ProcessModule_t7CCBC3E3C3382F1A431C2CE645F06224FC47BC16* Process_get_MainModule_mE2A05EDF7EF5A24F11EED96E9F33D58C01ADEB06 (Process_tF74794C64BCE464912BF158980B347CE66AF203B* __this, const RuntimeMethod* method) ;
// System.String System.Diagnostics.ProcessModule::get_FileName()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ProcessModule_get_FileName_mC1F3C43F68F8671C9B6A053B7A33C63BC4C2D03B_inline (ProcessModule_t7CCBC3E3C3382F1A431C2CE645F06224FC47BC16* __this, const RuntimeMethod* method) ;
// System.String[] System.Environment::GetCommandLineArgs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* Environment_GetCommandLineArgs_mD29CFA1CD3C84F9BD91152E70302E908114A831D (const RuntimeMethod* method) ;
// System.String UnityEngine.Application::get_dataPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_dataPath_m4C8412CBEE4EAAAB6711CC9BEFFA73CEE5BDBEF7 (const RuntimeMethod* method) ;
// System.IO.DirectoryInfo System.IO.Directory::GetParent(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* Directory_GetParent_mDD1F4DD5F95A5D6676009FB76E5016EDD127FFB2 (String_t* ___0_path, const RuntimeMethod* method) ;
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3 (String_t* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.String System.String::Replace(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_m86403DC5F422D8D5E1CFAAF255B103CB807EDAAF (String_t* __this, Il2CppChar ___0_oldChar, Il2CppChar ___1_newChar, const RuntimeMethod* method) ;
// System.Boolean System.String::StartsWith(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_StartsWith_mF75DBA1EB709811E711B44E26FF919C88A8E65C0 (String_t* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.String System.IO.Path::GetFileName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetFileName_mB1A8CE314EE250B06E3D33142315E2BD3A75D1D6 (String_t* ___0_path, const RuntimeMethod* method) ;
// System.String System.IO.Path::Combine(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE (String_t* ___0_path1, String_t* ___1_path2, const RuntimeMethod* method) ;
// System.String UnityEngine.JsonUtility::ToJson(System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* JsonUtility_ToJson_m53A1FEE0D388CF3A629E093C04B5E1A6D5463B53 (RuntimeObject* ___0_obj, bool ___1_prettyPrint, const RuntimeMethod* method) ;
// System.String ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::GetConfigurationLocation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ConfigurationStorage_GetConfigurationLocation_m25E918D2A5F76D2B4716BA377F44A73C3CA7D76F (const RuntimeMethod* method) ;
// System.Boolean System.IO.File::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool File_Exists_m95E329ABBE3EAD6750FE1989BBA6884457136D4A (String_t* ___0_path, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::EnsureDirectories()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurationStorage_EnsureDirectories_m79BC983C75F13BB1BB27F70060461D2144A9BA84 (const RuntimeMethod* method) ;
// System.Void System.IO.File::WriteAllText(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_WriteAllText_m8AE8932A417928EF1E86F1E6B37C7A41904614D1 (String_t* ___0_path, String_t* ___1_contents, const RuntimeMethod* method) ;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138 (const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration__ctor_m846C80409906F345F78FB43CC0190E2F3A82C623 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) ;
// System.String System.IO.File::ReadAllText(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* File_ReadAllText_mA4A939F853D573379F7129AFDC469B91E9747BAA (String_t* ___0_path, const RuntimeMethod* method) ;
// T UnityEngine.JsonUtility::FromJson<ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration>(System.String)
inline AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* JsonUtility_FromJson_TisAppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_m38FB935DF4F56E2A675A82EAD5B59345C17CA750 (String_t* ___0_json, const RuntimeMethod* method)
{
	return ((  AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* (*) (String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisRuntimeObject_m0CCF0FE109BF4C85AECC9C5D0DBB43422A24FB40_gshared)(___0_json, method);
}
// T UnityEngine.Resources::Load<UnityEngine.TextAsset>(System.String)
inline TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69* Resources_Load_TisTextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69_m55E770DF81AB6D40763121667DA5E743EF1036B2 (String_t* ___0_path, const RuntimeMethod* method)
{
	return ((  TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69* (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_mD1AF6299B14F87ED1D1A6199A51480919F7C79D7_gshared)(___0_path, method);
}
// System.String UnityEngine.TextAsset::get_text()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TextAsset_get_text_m36846042E3CF3D9DD337BF3F8B2B1902D10C8FD9 (TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69* __this, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::EnsureAllPlats()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_EnsureAllPlats_m128A417C948CBFAEAC897214F0C290E1B9CD1B6E (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) ;
// System.Boolean System.IO.Directory::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Directory_Exists_m3D125E9E88C291CF11113444F961A64DD83AE1C7 (String_t* ___0_path, const RuntimeMethod* method) ;
// System.IO.DirectoryInfo System.IO.Directory::CreateDirectory(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* Directory_CreateDirectory_m16EC5CE8561A997C6635E06DC24C77590F29D94F (String_t* ___0_path, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478 (String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m444AE141157E333844FC1A9500224C2F9FD24F4B (ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129* __this, String_t* ___0_paramName, const RuntimeMethod* method) ;
// System.Void System.Func`3<System.String,System.String,System.String>::.ctor(System.Object,System.IntPtr)
inline void Func_3__ctor_m275CCA1C836EECF7962B6DFFCA1EF52FD862A6EC (Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method)
{
	((  void (*) (Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_3__ctor_m7A3CDF8CC909FAEEA005D42C71F113B505F766DD_gshared)(__this, ___0_object, ___1_method, method);
}
// TSource System.Linq.Enumerable::Aggregate<System.String>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,TSource,TSource>)
inline String_t* Enumerable_Aggregate_TisString_t_mE91CF40C639FFB958F5EA190CCEE0EABE463D577 (RuntimeObject* ___0_source, Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA* ___1_func, const RuntimeMethod* method)
{
	return ((  String_t* (*) (RuntimeObject*, Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA*, const RuntimeMethod*))Enumerable_Aggregate_TisRuntimeObject_mA6E191832F9B71EC31D2C3115CADC5F16EE18182_gshared)(___0_source, ___1_func, method);
}
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB (RuntimeArray* ___0_array, int32_t ___1_index, int32_t ___2_length, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::get_IsInitialized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlatformLinkingConfiguration_get_IsInitialized_m883564B2DC7DA019B9D4E71D0E7B868339E68981 (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->____initialized_2;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::set_IsInitialized(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration_set_IsInitialized_m16B4E7BE13C785D3127BA332D67EAF73319428FE (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->____initialized_2 = L_0;
		return;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::get_DeepLinkingProtocols()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* PlatformLinkingConfiguration_get_DeepLinkingProtocols_m640ED35F65E1CF82EB519515AD89FA1C0A065EBE (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, const RuntimeMethod* method) 
{
	{
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_0 = __this->____deepLinkingProtocols_1;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::set_DeepLinkingProtocols(System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration_set_DeepLinkingProtocols_m80CA6C00E0DF7DF62C13917F48FB5D76FD8E7950 (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___0_value, const RuntimeMethod* method) 
{
	{
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_0 = ___0_value;
		__this->____deepLinkingProtocols_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____deepLinkingProtocols_1), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::get_DomainProtocols()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* PlatformLinkingConfiguration_get_DomainProtocols_m0A8AA114FAB59AAD0021165117A89B652262B853 (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, const RuntimeMethod* method) 
{
	{
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_0 = __this->____domainProtocols_0;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::set_DomainProtocols(System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration_set_DomainProtocols_m2C70E312EA840E54AAEA0804B0F8D56F21628345 (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___0_value, const RuntimeMethod* method) 
{
	{
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_0 = ___0_value;
		__this->____domainProtocols_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____domainProtocols_0), (void*)L_0);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration__ctor_m019E37CFC7696ED6817CF4C76B55F0F79223BD07 (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, bool ___0_init, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mAE6DE6DEE0AA43171FDCED497C360002EB7B6471_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		bool L_0 = ___0_init;
		__this->____initialized_2 = L_0;
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_1 = (List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8*)il2cpp_codegen_object_new(List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_mAE6DE6DEE0AA43171FDCED497C360002EB7B6471(L_1, List_1__ctor_mAE6DE6DEE0AA43171FDCED497C360002EB7B6471_RuntimeMethod_var);
		PlatformLinkingConfiguration_set_DeepLinkingProtocols_m80CA6C00E0DF7DF62C13917F48FB5D76FD8E7950_inline(__this, L_1, NULL);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_2 = (List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8*)il2cpp_codegen_object_new(List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		List_1__ctor_mAE6DE6DEE0AA43171FDCED497C360002EB7B6471(L_2, List_1__ctor_mAE6DE6DEE0AA43171FDCED497C360002EB7B6471_RuntimeMethod_var);
		PlatformLinkingConfiguration_set_DomainProtocols_m2C70E312EA840E54AAEA0804B0F8D56F21628345_inline(__this, L_2, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::get_SteamId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppLinkingConfiguration_get_SteamId_m2B98A9F759EF14CFFC0B0FCE63E6EA661C72D325 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->____steamId_0;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::set_SteamId(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_set_SteamId_m3D8803FDB0D43ED6DBBA4DC351C325F6306DF918 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->____steamId_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____steamId_0), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::get_DeepLinkingProtocols()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_get_DeepLinkingProtocols_m50073E6B86058B448D9110FE7B1CAB4D9D8AC260 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_0 = __this->____globalConfiguration_2;
		NullCheck(L_0);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_1;
		L_1 = PlatformLinkingConfiguration_get_DeepLinkingProtocols_m640ED35F65E1CF82EB519515AD89FA1C0A065EBE_inline(L_0, NULL);
		return L_1;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::set_DeepLinkingProtocols(System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_set_DeepLinkingProtocols_mC2251425C086487C3BDC88B731F5A8882010AEB1 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___0_value, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_0 = __this->____globalConfiguration_2;
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_1 = ___0_value;
		NullCheck(L_0);
		PlatformLinkingConfiguration_set_DeepLinkingProtocols_m80CA6C00E0DF7DF62C13917F48FB5D76FD8E7950_inline(L_0, L_1, NULL);
		return;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::get_DomainProtocols()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_get_DomainProtocols_mFDF7686402812050EBCA7ECC700ACEC070FDAB16 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_0 = __this->____globalConfiguration_2;
		NullCheck(L_0);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_1;
		L_1 = PlatformLinkingConfiguration_get_DomainProtocols_m0A8AA114FAB59AAD0021165117A89B652262B853_inline(L_0, NULL);
		return L_1;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::set_DomainProtocols(System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_set_DomainProtocols_m41434C0D356B95B458DC27304A9721DD330F0AAD (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___0_value, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_0 = __this->____globalConfiguration_2;
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_1 = ___0_value;
		NullCheck(L_0);
		PlatformLinkingConfiguration_set_DomainProtocols_m2C70E312EA840E54AAEA0804B0F8D56F21628345_inline(L_0, L_1, NULL);
		return;
	}
}
// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[] ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::get_CustomDeepLinkingProtocols()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* AppLinkingConfiguration_get_CustomDeepLinkingProtocols_m2F50EDD2311D022262163A4B95A7E0CA70E888FB (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_0 = __this->____customDeepLinkingProtocols_3;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::set_CustomDeepLinkingProtocols(ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_set_CustomDeepLinkingProtocols_mE5309BA35444E52AE002D0846840BF22609D982A (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* ___0_value, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_0 = ___0_value;
		__this->____customDeepLinkingProtocols_3 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____customDeepLinkingProtocols_3), (void*)L_0);
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_1 = ___0_value;
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_2;
		L_2 = AppLinkingConfiguration_EnsureAllPlats_mFC658CFBDBC8D8931F964E56AE4ECA9F39BECFFF(__this, L_1, NULL);
		__this->____customDeepLinkingProtocols_3 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____customDeepLinkingProtocols_3), (void*)L_2);
		return;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::get_DisplayName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppLinkingConfiguration_get_DisplayName_mF276FDE89647631E9FBD5D38D972062396E93517 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->____displayName_1;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::set_DisplayName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_set_DisplayName_mA1333C39EE74F839CB73EF02CDBEF2B57A6A2B25 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->____displayName_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____displayName_1), (void*)L_0);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration__ctor_m846C80409906F345F78FB43CC0190E2F3A82C623 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SupportedPlatforms_tDF89E98E29751314B96DA5BEE3F6B7BF112EC5EF_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_0 = (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D*)il2cpp_codegen_object_new(PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		PlatformLinkingConfiguration__ctor_m019E37CFC7696ED6817CF4C76B55F0F79223BD07(L_0, (bool)0, NULL);
		__this->____globalConfiguration_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____globalConfiguration_2), (void*)L_0);
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_1 = { reinterpret_cast<intptr_t> (SupportedPlatforms_tDF89E98E29751314B96DA5BEE3F6B7BF112EC5EF_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_2;
		L_2 = Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57(L_1, NULL);
		il2cpp_codegen_runtime_class_init_inline(Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_il2cpp_TypeInfo_var);
		RuntimeArray* L_3;
		L_3 = Enum_GetValues_m803B9D68C367FAABC5AFB6B5B52775C8A573CEF9(L_2, NULL);
		NullCheck(L_3);
		int32_t L_4;
		L_4 = Array_get_Length_m361285FB7CF44045DC369834D1CD01F72F94EF57(L_3, NULL);
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_5 = (PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C*)(PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C*)SZArrayNew(PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C_il2cpp_TypeInfo_var, (uint32_t)L_4);
		__this->____customDeepLinkingProtocols_3 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____customDeepLinkingProtocols_3), (void*)L_5);
		V_0 = 0;
		goto IL_0047;
	}

IL_0035:
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_6 = __this->____customDeepLinkingProtocols_3;
		int32_t L_7 = V_0;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_8 = (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D*)il2cpp_codegen_object_new(PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		PlatformLinkingConfiguration__ctor_m019E37CFC7696ED6817CF4C76B55F0F79223BD07(L_8, (bool)0, NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D*)L_8);
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_0047:
	{
		int32_t L_10 = V_0;
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_11 = __this->____customDeepLinkingProtocols_3;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))
		{
			goto IL_0035;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::EnsureAllPlats()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_EnsureAllPlats_m128A417C948CBFAEAC897214F0C290E1B9CD1B6E (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_0;
		L_0 = AppLinkingConfiguration_get_CustomDeepLinkingProtocols_m2F50EDD2311D022262163A4B95A7E0CA70E888FB_inline(__this, NULL);
		AppLinkingConfiguration_set_CustomDeepLinkingProtocols_mE5309BA35444E52AE002D0846840BF22609D982A(__this, L_0, NULL);
		return;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::GetPlatformDeepLinkingProtocols(ImaginationOverflow.UniversalDeepLinking.SupportedPlatforms,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_GetPlatformDeepLinkingProtocols_m7CADF95280392766290DFF44AEE1665A57251DC4 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, int32_t ___0_plat, bool ___1_includeDefault, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CGetPlatformDeepLinkingProtocolsU3Eb__21_0_m0E722DDA2D24082FEA9E76FC570D0AA1E3B2319A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* G_B2_0 = NULL;
	int32_t G_B2_1 = 0;
	AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* G_B2_2 = NULL;
	Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* G_B1_0 = NULL;
	int32_t G_B1_1 = 0;
	AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* G_B1_2 = NULL;
	{
		int32_t L_0 = ___0_plat;
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var);
		Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* L_1 = ((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9__21_0_1;
		Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		G_B1_2 = __this;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			G_B2_2 = __this;
			goto IL_0021;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var);
		U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44* L_3 = ((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* L_4 = (Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E*)il2cpp_codegen_object_new(Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_2__ctor_m0818E709D2E27AAA03D41D8155881B84BAE12923(L_4, L_3, (intptr_t)((void*)U3CU3Ec_U3CGetPlatformDeepLinkingProtocolsU3Eb__21_0_m0E722DDA2D24082FEA9E76FC570D0AA1E3B2319A_RuntimeMethod_var), NULL);
		Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* L_5 = L_4;
		((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9__21_0_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9__21_0_1), (void*)L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0021:
	{
		bool L_6 = ___1_includeDefault;
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_7;
		L_7 = AppLinkingConfiguration_get_DeepLinkingProtocols_m50073E6B86058B448D9110FE7B1CAB4D9D8AC260(__this, NULL);
		NullCheck(G_B2_2);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_8;
		L_8 = AppLinkingConfiguration_GetCustomOrDefault_m16FADC2C379C973ABD46E5FCC07DCD21B074AF98(G_B2_2, G_B2_1, G_B2_0, L_6, L_7, NULL);
		return L_8;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::GetPlatformDomainProtocols(ImaginationOverflow.UniversalDeepLinking.SupportedPlatforms,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_GetPlatformDomainProtocols_m52B699BD61680A1A0FEE10A2AE09073B91860D93 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, int32_t ___0_plat, bool ___1_includeDefault, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CGetPlatformDomainProtocolsU3Eb__22_0_m9D834A1E561FB5A6C0D92ACF8F69EB644BD23C71_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* G_B2_0 = NULL;
	int32_t G_B2_1 = 0;
	AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* G_B2_2 = NULL;
	Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* G_B1_0 = NULL;
	int32_t G_B1_1 = 0;
	AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* G_B1_2 = NULL;
	{
		int32_t L_0 = ___0_plat;
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var);
		Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* L_1 = ((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9__22_0_2;
		Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		G_B1_2 = __this;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			G_B2_2 = __this;
			goto IL_0021;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var);
		U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44* L_3 = ((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* L_4 = (Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E*)il2cpp_codegen_object_new(Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_2__ctor_m0818E709D2E27AAA03D41D8155881B84BAE12923(L_4, L_3, (intptr_t)((void*)U3CU3Ec_U3CGetPlatformDomainProtocolsU3Eb__22_0_m9D834A1E561FB5A6C0D92ACF8F69EB644BD23C71_RuntimeMethod_var), NULL);
		Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* L_5 = L_4;
		((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9__22_0_2 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9__22_0_2), (void*)L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0021:
	{
		bool L_6 = ___1_includeDefault;
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_7;
		L_7 = AppLinkingConfiguration_get_DomainProtocols_mFDF7686402812050EBCA7ECC700ACEC070FDAB16(__this, NULL);
		NullCheck(G_B2_2);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_8;
		L_8 = AppLinkingConfiguration_GetCustomOrDefault_m16FADC2C379C973ABD46E5FCC07DCD21B074AF98(G_B2_2, G_B2_1, G_B2_0, L_6, L_7, NULL);
		return L_8;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::GetCustomOrDefault(ImaginationOverflow.UniversalDeepLinking.SupportedPlatforms,System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>>,System.Boolean,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_GetCustomOrDefault_m16FADC2C379C973ABD46E5FCC07DCD21B074AF98 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, int32_t ___0_plat, Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* ___1_func, bool ___2_includeDefault, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___3_global, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* V_1 = NULL;
	{
		int32_t L_0 = ___0_plat;
		V_0 = L_0;
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_1;
		L_1 = AppLinkingConfiguration_get_CustomDeepLinkingProtocols_m2F50EDD2311D022262163A4B95A7E0CA70E888FB_inline(__this, NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_5 = V_1;
		if (!L_5)
		{
			goto IL_001e;
		}
	}
	{
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_6 = V_1;
		NullCheck(L_6);
		bool L_7;
		L_7 = PlatformLinkingConfiguration_get_IsInitialized_m883564B2DC7DA019B9D4E71D0E7B868339E68981_inline(L_6, NULL);
		if (!L_7)
		{
			goto IL_001e;
		}
	}
	{
		Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* L_8 = ___1_func;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_9 = V_1;
		NullCheck(L_8);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_10;
		L_10 = Func_2_Invoke_mE2C35CEEAE1603CA239414F1D6642941FFF30362_inline(L_8, L_9, NULL);
		return L_10;
	}

IL_001e:
	{
		bool L_11 = ___2_includeDefault;
		if (!L_11)
		{
			goto IL_0024;
		}
	}
	{
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_12 = ___3_global;
		return L_12;
	}

IL_0024:
	{
		return (List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8*)NULL;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::GetCustomDeepLinkingProtocols(ImaginationOverflow.UniversalDeepLinking.SupportedPlatforms)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_GetCustomDeepLinkingProtocols_m030B564F97B94E602A9258173ED237E750BEA150 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_0;
		L_0 = AppLinkingConfiguration_get_CustomDeepLinkingProtocols_m2F50EDD2311D022262163A4B95A7E0CA70E888FB_inline(__this, NULL);
		int32_t L_1 = ___0_value;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_4;
		L_4 = PlatformLinkingConfiguration_get_DeepLinkingProtocols_m640ED35F65E1CF82EB519515AD89FA1C0A065EBE_inline(L_3, NULL);
		return L_4;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::GetCustomDomainAssociation(ImaginationOverflow.UniversalDeepLinking.SupportedPlatforms)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* AppLinkingConfiguration_GetCustomDomainAssociation_mD6D9EE3153DAB2ACABEB77CC5C8B48C223AD943F (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_0;
		L_0 = AppLinkingConfiguration_get_CustomDeepLinkingProtocols_m2F50EDD2311D022262163A4B95A7E0CA70E888FB_inline(__this, NULL);
		int32_t L_1 = ___0_value;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_4;
		L_4 = PlatformLinkingConfiguration_get_DomainProtocols_m0A8AA114FAB59AAD0021165117A89B652262B853_inline(L_3, NULL);
		return L_4;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::ActivatePlatformOverride(ImaginationOverflow.UniversalDeepLinking.SupportedPlatforms)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_ActivatePlatformOverride_m0F41DF6653EF916343FB374DC855322F04B6CFC6 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_0 = __this->____customDeepLinkingProtocols_3;
		int32_t L_1 = ___0_value;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		PlatformLinkingConfiguration_set_IsInitialized_m16B4E7BE13C785D3127BA332D67EAF73319428FE_inline(L_3, (bool)1, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::DeactivatePlatformOverride(ImaginationOverflow.UniversalDeepLinking.SupportedPlatforms)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppLinkingConfiguration_DeactivatePlatformOverride_m429D646BF5FD5A3CB84380A7D0FE7C0C65AD27E2 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m5A31C469EDACD8D924FD141764D621183C50AFCA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_0 = __this->____customDeepLinkingProtocols_3;
		int32_t L_1 = ___0_value;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		PlatformLinkingConfiguration_set_IsInitialized_m16B4E7BE13C785D3127BA332D67EAF73319428FE_inline(L_3, (bool)0, NULL);
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_4 = __this->____customDeepLinkingProtocols_3;
		int32_t L_5 = ___0_value;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_8;
		L_8 = PlatformLinkingConfiguration_get_DeepLinkingProtocols_m640ED35F65E1CF82EB519515AD89FA1C0A065EBE_inline(L_7, NULL);
		NullCheck(L_8);
		List_1_Clear_m5A31C469EDACD8D924FD141764D621183C50AFCA_inline(L_8, List_1_Clear_m5A31C469EDACD8D924FD141764D621183C50AFCA_RuntimeMethod_var);
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_9 = __this->____customDeepLinkingProtocols_3;
		int32_t L_10 = ___0_value;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_13;
		L_13 = PlatformLinkingConfiguration_get_DomainProtocols_m0A8AA114FAB59AAD0021165117A89B652262B853_inline(L_12, NULL);
		NullCheck(L_13);
		List_1_Clear_m5A31C469EDACD8D924FD141764D621183C50AFCA_inline(L_13, List_1_Clear_m5A31C469EDACD8D924FD141764D621183C50AFCA_RuntimeMethod_var);
		return;
	}
}
// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[] ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::EnsureAllPlats(ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* AppLinkingConfiguration_EnsureAllPlats_mFC658CFBDBC8D8931F964E56AE4ECA9F39BECFFF (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SupportedPlatforms_tDF89E98E29751314B96DA5BEE3F6B7BF112EC5EF_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* V_1 = NULL;
	int32_t V_2 = 0;
	{
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_0 = { reinterpret_cast<intptr_t> (SupportedPlatforms_tDF89E98E29751314B96DA5BEE3F6B7BF112EC5EF_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_1;
		L_1 = Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57(L_0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_il2cpp_TypeInfo_var);
		RuntimeArray* L_2;
		L_2 = Enum_GetValues_m803B9D68C367FAABC5AFB6B5B52775C8A573CEF9(L_1, NULL);
		NullCheck(L_2);
		int32_t L_3;
		L_3 = Array_get_Length_m361285FB7CF44045DC369834D1CD01F72F94EF57(L_2, NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_5 = ___0_value;
		NullCheck(L_5);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))))))
		{
			goto IL_001d;
		}
	}
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_6 = ___0_value;
		return L_6;
	}

IL_001d:
	{
		int32_t L_7 = V_0;
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_8 = (PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C*)(PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C*)SZArrayNew(PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C_il2cpp_TypeInfo_var, (uint32_t)L_7);
		V_1 = L_8;
		V_2 = 0;
		goto IL_0043;
	}

IL_0028:
	{
		int32_t L_9 = V_2;
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_10 = ___0_value;
		NullCheck(L_10);
		if ((((int32_t)L_9) >= ((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length)))))
		{
			goto IL_0036;
		}
	}
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_11 = V_1;
		int32_t L_12 = V_2;
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_13 = ___0_value;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_16);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D*)L_16);
		goto IL_003f;
	}

IL_0036:
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_17 = V_1;
		int32_t L_18 = V_2;
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_19 = (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D*)il2cpp_codegen_object_new(PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D_il2cpp_TypeInfo_var);
		NullCheck(L_19);
		PlatformLinkingConfiguration__ctor_m019E37CFC7696ED6817CF4C76B55F0F79223BD07(L_19, (bool)0, NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D*)L_19);
	}

IL_003f:
	{
		int32_t L_20 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_20, 1));
	}

IL_0043:
	{
		int32_t L_21 = V_2;
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length)))))
		{
			goto IL_0028;
		}
	}
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_23 = V_1;
		return L_23;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mB4C212F1EB7FE78B951B05DDC6D54857F16C8390 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44* L_0 = (U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44*)il2cpp_codegen_object_new(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__ctor_mEA99269F7F948B44A55CB8F52410069A4D7F0252(L_0, NULL);
		((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_il2cpp_TypeInfo_var))->___U3CU3E9_0), (void*)L_0);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mEA99269F7F948B44A55CB8F52410069A4D7F0252 (U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::<GetPlatformDeepLinkingProtocols>b__21_0(ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* U3CU3Ec_U3CGetPlatformDeepLinkingProtocolsU3Eb__21_0_m0E722DDA2D24082FEA9E76FC570D0AA1E3B2319A (U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44* __this, PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* ___0_c, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_0 = ___0_c;
		NullCheck(L_0);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_1;
		L_1 = PlatformLinkingConfiguration_get_DeepLinkingProtocols_m640ED35F65E1CF82EB519515AD89FA1C0A065EBE_inline(L_0, NULL);
		return L_1;
	}
}
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::<GetPlatformDomainProtocols>b__22_0(ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* U3CU3Ec_U3CGetPlatformDomainProtocolsU3Eb__22_0_m9D834A1E561FB5A6C0D92ACF8F69EB644BD23C71 (U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44* __this, PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* ___0_c, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* L_0 = ___0_c;
		NullCheck(L_0);
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_1;
		L_1 = PlatformLinkingConfiguration_get_DomainProtocols_m0A8AA114FAB59AAD0021165117A89B652262B853_inline(L_0, NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_Multicast(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* currentDelegate = reinterpret_cast<LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___0_s, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenInst(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method)
{
	NullCheck(___0_s);
	typedef void (*FunctionPointerType) (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_s, method);
}
void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenStatic(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_s, method);
}
void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenStaticInvoker(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method)
{
	InvokerActionInvoker1< LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* >::Invoke(__this->___method_ptr_0, method, NULL, ___0_s);
}
void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_ClosedStaticInvoker(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___0_s);
}
void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenVirtual(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method)
{
	NullCheck(___0_s);
	VirtualActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(method), ___0_s);
}
void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenInterface(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method)
{
	NullCheck(___0_s);
	InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(method), il2cpp_codegen_method_get_declaring_type(method), ___0_s);
}
void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenGenericVirtual(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method)
{
	NullCheck(___0_s);
	GenericVirtualActionInvoker0::Invoke(method, ___0_s);
}
void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenGenericInterface(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method)
{
	NullCheck(___0_s);
	GenericInterfaceActionInvoker0::Invoke(method, ___0_s);
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivationHandler__ctor_m88CE460FC3680E0811986CB95B3937112B2623A2 (LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___1_method);
	__this->___method_3 = ___1_method;
	__this->___m_target_2 = ___0_object;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___0_object);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___1_method);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___1_method))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___1_method))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			if (__this->___method_is_virtual_12)
			{
				if (il2cpp_codegen_method_is_generic_instance_method((RuntimeMethod*)___1_method))
					if (il2cpp_codegen_method_is_interface_method((RuntimeMethod*)___1_method))
						__this->___invoke_impl_1 = (intptr_t)&LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenGenericInterface;
					else
						__this->___invoke_impl_1 = (intptr_t)&LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenGenericVirtual;
				else
					if (il2cpp_codegen_method_is_interface_method((RuntimeMethod*)___1_method))
						__this->___invoke_impl_1 = (intptr_t)&LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenInterface;
					else
						__this->___invoke_impl_1 = (intptr_t)&LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenVirtual;
			}
			else
			{
				__this->___invoke_impl_1 = (intptr_t)&LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_OpenInst;
			}
		}
		else
		{
			if (___0_object == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_Multicast;
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::Invoke(ImaginationOverflow.UniversalDeepLinking.LinkActivation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7 (LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_s, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::BeginInvoke(ImaginationOverflow.UniversalDeepLinking.LinkActivation,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LinkActivationHandler_BeginInvoke_mCACB5C6C602721A1FBFB9C183D15AFFEE009692B (LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___1_callback, RuntimeObject* ___2_object, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___0_s;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___1_callback, (RuntimeObject*)___2_object);
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivationHandler_EndInvoke_m670201F58399E3B0E18E43EF3BED9FE513E90930 (LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, RuntimeObject* ___0_result, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___0_result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_Multicast(UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, String_t* ___0_link, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* currentDelegate = reinterpret_cast<UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___0_link, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_OpenInst(UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, String_t* ___0_link, const RuntimeMethod* method)
{
	NullCheck(___0_link);
	typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_link, method);
}
void UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_OpenStatic(UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, String_t* ___0_link, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___0_link, method);
}
void UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_OpenStaticInvoker(UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, String_t* ___0_link, const RuntimeMethod* method)
{
	InvokerActionInvoker1< String_t* >::Invoke(__this->___method_ptr_0, method, NULL, ___0_link);
}
void UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_ClosedStaticInvoker(UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, String_t* ___0_link, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, String_t* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___0_link);
}
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0 (UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, String_t* ___0_link, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_reverse_pinvoke_function_ptr(__this));
	// Marshaling of parameter '___0_link' to native representation
	char* ____0_link_marshaled = NULL;
	____0_link_marshaled = il2cpp_codegen_marshal_string(___0_link);

	// Native function invocation
	il2cppPInvokeFunc(____0_link_marshaled);

	// Marshaling cleanup of parameter '___0_link' native representation
	il2cpp_codegen_marshal_free(____0_link_marshaled);
	____0_link_marshaled = NULL;

}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalLinkCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalLinkCallback__ctor_mB766B75ACF48A6CB5003B16A07A6A8B3490E49A1 (UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___1_method);
	__this->___method_3 = ___1_method;
	__this->___m_target_2 = ___0_object;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___0_object);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___1_method);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___1_method))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___1_method))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_OpenInst;
		}
		else
		{
			if (___0_object == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0_Multicast;
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalLinkCallback::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalLinkCallback_Invoke_mF8812B37C163408A0FD30D7B05D7B32901663FB0 (UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, String_t* ___0_link, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_link, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
// System.IAsyncResult ImaginationOverflow.UniversalDeepLinking.UniversalLinkCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UniversalLinkCallback_BeginInvoke_m594D843E0A21F7E7B866B2D85AC67C18C510DDBF (UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, String_t* ___0_link, AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C* ___1_callback, RuntimeObject* ___2_object, const RuntimeMethod* method) 
{
	void *__d_args[2] = {0};
	__d_args[0] = ___0_link;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___1_callback, (RuntimeObject*)___2_object);
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalLinkCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalLinkCallback_EndInvoke_m15286D6D15423AD50F67706D2EF6DE458A7AC425 (UniversalLinkCallback_t606BDFD38C6DCAAFE10A0C69652783CBB4E76BE0* __this, RuntimeObject* ___0_result, const RuntimeMethod* method) 
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___0_result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::get_Uri()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* LinkActivation_get_Uri_mC6D342ABF0ACE38B8D2725A1AF77BC0379B73980 (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CUriU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::set_Uri(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivation_set_Uri_m57367A6018A51D18C68BCA49E0CD41EEBF932C89 (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CUriU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CUriU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::get_RawQueryString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* LinkActivation_get_RawQueryString_mE998F8B2F04969CAA54D066597CD8FAEB8FFAE2F (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CRawQueryStringU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::set_RawQueryString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivation_set_RawQueryString_m53B1488C42F6FD0891C168105A6164D616891234 (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CRawQueryStringU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CRawQueryStringU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.LinkActivation::get_QueryString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* LinkActivation_get_QueryString_m937F12C1C4E14F6CF5593903BE7892B5AC2B9C0D (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, const RuntimeMethod* method) 
{
	{
		Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* L_0 = __this->___U3CQueryStringU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::set_QueryString(System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivation_set_QueryString_m0D508E667298D0717BF7661D1A96D416D517343D (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___0_value, const RuntimeMethod* method) 
{
	{
		Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* L_0 = ___0_value;
		__this->___U3CQueryStringU3Ek__BackingField_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CQueryStringU3Ek__BackingField_2), (void*)L_0);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::.ctor(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivation__ctor_m758C8A28F4C84308C360404913D6FE6A6D849C89 (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, String_t* ___0_uri, String_t* ___1_rawQueryString, Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___2_queryStringParams, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		String_t* L_0 = ___0_uri;
		LinkActivation_set_Uri_m57367A6018A51D18C68BCA49E0CD41EEBF932C89_inline(__this, L_0, NULL);
		String_t* L_1 = ___1_rawQueryString;
		LinkActivation_set_RawQueryString_m53B1488C42F6FD0891C168105A6164D616891234_inline(__this, L_1, NULL);
		Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* L_2 = ___2_queryStringParams;
		LinkActivation_set_QueryString_m0D508E667298D0717BF7661D1A96D416D517343D_inline(__this, L_2, NULL);
		return;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* LinkActivation_ToString_m121D035EEB995E7490268DA8C9CBF3D8913D0F7A (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0;
		L_0 = LinkActivation_get_Uri_mC6D342ABF0ACE38B8D2725A1AF77BC0379B73980_inline(__this, NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String ImaginationOverflow.UniversalDeepLinking.LinkInformation::get_Scheme()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* LinkInformation_get_Scheme_m488A9DEA7E291B6CF548C6AC043733ADF7D5D2AD (LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->____scheme_0;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkInformation::set_Scheme(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkInformation_set_Scheme_m4A520B593F4F6B3C8811FB1928FA9829E1DF57C6 (LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->____scheme_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____scheme_0), (void*)L_0);
		return;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.LinkInformation::get_Host()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* LinkInformation_get_Host_mB826043E996DA0EEF3A581A8670C826B23ADE730 (LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->____host_1;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkInformation::set_Host(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkInformation_set_Host_m9E10C5CA621F46E6B3F0BB2400C60A43BAACCD6B (LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->____host_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____host_1), (void*)L_0);
		return;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.LinkInformation::get_Path()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* LinkInformation_get_Path_mE67AFE141359A26D4811FEF90D2D3AA75A4B6969 (LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->____path_2;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkInformation::set_Path(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkInformation_set_Path_mEBC261DC986DE40EBA4634940A433C9425D8C189 (LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->____path_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____path_2), (void*)L_0);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkInformation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkInformation__ctor_m39AED60726269BB268D94E3E7D0BF01FCA18995E (LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->___Empty_6;
		__this->____scheme_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____scheme_0), (void*)L_0);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->___Empty_6;
		__this->____host_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____host_1), (void*)L_1);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->___Empty_6;
		__this->____path_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____path_2), (void*)L_2);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String ImaginationOverflow.UniversalDeepLinking.ProviderHelpers::GetExecutingPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ProviderHelpers_GetExecutingPath_mA94AD0432AB485B479A4B02B52C022F264F0C501 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	try
	{// begin try (depth: 1)
		Process_tF74794C64BCE464912BF158980B347CE66AF203B* L_0;
		L_0 = Process_GetCurrentProcess_mB9E146001302DA6A60946152A09E8205E5FD2F0E(NULL);
		NullCheck(L_0);
		ProcessModule_t7CCBC3E3C3382F1A431C2CE645F06224FC47BC16* L_1;
		L_1 = Process_get_MainModule_mE2A05EDF7EF5A24F11EED96E9F33D58C01ADEB06(L_0, NULL);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = ProcessModule_get_FileName_mC1F3C43F68F8671C9B6A053B7A33C63BC4C2D03B_inline(L_1, NULL);
		V_2 = L_2;
		goto IL_0070;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0012;
		}
		throw e;
	}

CATCH_0012:
	{// begin catch(System.Exception)
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_0015;
	}// end catch (depth: 1)

IL_0015:
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_3;
		L_3 = Environment_GetCommandLineArgs_mD29CFA1CD3C84F9BD91152E70302E908114A831D(NULL);
		NullCheck(L_3);
		int32_t L_4 = 0;
		String_t* L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		String_t* L_6;
		L_6 = Application_get_dataPath_m4C8412CBEE4EAAAB6711CC9BEFFA73CEE5BDBEF7(NULL);
		DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* L_7;
		L_7 = Directory_GetParent_mDD1F4DD5F95A5D6676009FB76E5016EDD127FFB2(L_6, NULL);
		NullCheck(L_7);
		String_t* L_8;
		L_8 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.IO.FileSystemInfo::get_FullName() */, L_7);
		V_1 = L_8;
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10;
		L_10 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_9, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1, NULL);
		if (!L_10)
		{
			goto IL_004a;
		}
	}
	{
		String_t* L_11 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		Il2CppChar L_12 = ((Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_StaticFields*)il2cpp_codegen_static_fields_for(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var))->___PathSeparator_3;
		NullCheck(L_11);
		String_t* L_13;
		L_13 = String_Replace_m86403DC5F422D8D5E1CFAAF255B103CB807EDAAF(L_11, ((int32_t)92), L_12, NULL);
		V_1 = L_13;
		goto IL_0058;
	}

IL_004a:
	{
		String_t* L_14 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		Il2CppChar L_15 = ((Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_StaticFields*)il2cpp_codegen_static_fields_for(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var))->___PathSeparator_3;
		NullCheck(L_14);
		String_t* L_16;
		L_16 = String_Replace_m86403DC5F422D8D5E1CFAAF255B103CB807EDAAF(L_14, ((int32_t)47), L_15, NULL);
		V_1 = L_16;
	}

IL_0058:
	{
		String_t* L_17 = V_0;
		String_t* L_18 = V_1;
		NullCheck(L_17);
		bool L_19;
		L_19 = String_StartsWith_mF75DBA1EB709811E711B44E26FF919C88A8E65C0(L_17, L_18, NULL);
		if (!L_19)
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_20 = V_0;
		return L_20;
	}

IL_0063:
	{
		String_t* L_21 = V_1;
		String_t* L_22 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_23;
		L_23 = Path_GetFileName_mB1A8CE314EE250B06E3D33142315E2BD3A75D1D6(L_22, NULL);
		String_t* L_24;
		L_24 = Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE(L_21, L_23, NULL);
		return L_24;
	}

IL_0070:
	{
		String_t* L_25 = V_2;
		return L_25;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::Save(ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurationStorage_Save_m1499E0B5CB2F5EF116810AAA2682E8D29366A4F0 (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* ___0_config, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* L_0 = ___0_config;
		String_t* L_1;
		L_1 = JsonUtility_ToJson_m53A1FEE0D388CF3A629E093C04B5E1A6D5463B53(L_0, (bool)0, NULL);
		V_0 = L_1;
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		String_t* L_2;
		L_2 = ConfigurationStorage_GetConfigurationLocation_m25E918D2A5F76D2B4716BA377F44A73C3CA7D76F(NULL);
		String_t* L_3 = L_2;
		bool L_4;
		L_4 = File_Exists_m95E329ABBE3EAD6750FE1989BBA6884457136D4A(L_3, NULL);
		G_B1_0 = L_3;
		if (L_4)
		{
			G_B2_0 = L_3;
			goto IL_001a;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		ConfigurationStorage_EnsureDirectories_m79BC983C75F13BB1BB27F70060461D2144A9BA84(NULL);
		G_B2_0 = G_B1_0;
	}

IL_001a:
	{
		String_t* L_5 = V_0;
		File_WriteAllText_m8AE8932A417928EF1E86F1E6B37C7A41904614D1(G_B2_0, L_5, NULL);
		return;
	}
}
// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::Load()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* ConfigurationStorage_Load_m7E53A726C443D9CB8F1DFBD4E422947BEAB1B537 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonUtility_FromJson_TisAppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_m38FB935DF4F56E2A675A82EAD5B59345C17CA750_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisTextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69_m55E770DF81AB6D40763121667DA5E743EF1036B2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1068D3F8264E2E8E66BC90F6D5203CBCD4000B13);
		s_Il2CppMethodInitialized = true;
	}
	AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		int32_t L_0;
		L_0 = Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138(NULL);
		if ((((int32_t)L_0) == ((int32_t)7)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1;
		L_1 = Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138(NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2;
		L_2 = Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138(NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_003a;
		}
	}

IL_0018:
	{
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		String_t* L_3;
		L_3 = ConfigurationStorage_GetConfigurationLocation_m25E918D2A5F76D2B4716BA377F44A73C3CA7D76F(NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		bool L_5;
		L_5 = File_Exists_m95E329ABBE3EAD6750FE1989BBA6884457136D4A(L_4, NULL);
		if (L_5)
		{
			goto IL_002c;
		}
	}
	{
		AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* L_6 = (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D*)il2cpp_codegen_object_new(AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		AppLinkingConfiguration__ctor_m846C80409906F345F78FB43CC0190E2F3A82C623(L_6, NULL);
		return L_6;
	}

IL_002c:
	{
		String_t* L_7 = V_1;
		String_t* L_8;
		L_8 = File_ReadAllText_mA4A939F853D573379F7129AFDC469B91E9747BAA(L_7, NULL);
		AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* L_9;
		L_9 = JsonUtility_FromJson_TisAppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_m38FB935DF4F56E2A675A82EAD5B59345C17CA750(L_8, JsonUtility_FromJson_TisAppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_m38FB935DF4F56E2A675A82EAD5B59345C17CA750_RuntimeMethod_var);
		V_0 = L_9;
		goto IL_004f;
	}

IL_003a:
	{
		TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69* L_10;
		L_10 = Resources_Load_TisTextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69_m55E770DF81AB6D40763121667DA5E743EF1036B2(_stringLiteral1068D3F8264E2E8E66BC90F6D5203CBCD4000B13, Resources_Load_TisTextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69_m55E770DF81AB6D40763121667DA5E743EF1036B2_RuntimeMethod_var);
		NullCheck(L_10);
		String_t* L_11;
		L_11 = TextAsset_get_text_m36846042E3CF3D9DD337BF3F8B2B1902D10C8FD9(L_10, NULL);
		AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* L_12;
		L_12 = JsonUtility_FromJson_TisAppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_m38FB935DF4F56E2A675A82EAD5B59345C17CA750(L_11, JsonUtility_FromJson_TisAppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D_m38FB935DF4F56E2A675A82EAD5B59345C17CA750_RuntimeMethod_var);
		V_0 = L_12;
	}

IL_004f:
	{
		AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* L_13 = V_0;
		NullCheck(L_13);
		AppLinkingConfiguration_EnsureAllPlats_m128A417C948CBFAEAC897214F0C290E1B9CD1B6E(L_13, NULL);
		AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* L_14 = V_0;
		return L_14;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::EnsureDirectories()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurationStorage_EnsureDirectories_m79BC983C75F13BB1BB27F70060461D2144A9BA84 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0;
		L_0 = Application_get_dataPath_m4C8412CBEE4EAAAB6711CC9BEFFA73CEE5BDBEF7(NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_002b;
	}

IL_000a:
	{
		String_t* L_1 = V_0;
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = ((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->___SaveFolders_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		String_t* L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_6;
		L_6 = Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE(L_1, L_5, NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		bool L_8;
		L_8 = Directory_Exists_m3D125E9E88C291CF11113444F961A64DD83AE1C7(L_7, NULL);
		if (L_8)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_9 = V_0;
		DirectoryInfo_tEAEEC018EB49B4A71907FFEAFE935FAA8F9C1FE2* L_10;
		L_10 = Directory_CreateDirectory_m16EC5CE8561A997C6635E06DC24C77590F29D94F(L_9, NULL);
	}

IL_0027:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_11, 1));
	}

IL_002b:
	{
		int32_t L_12 = V_1;
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_13 = ((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->___SaveFolders_0;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length)))))
		{
			goto IL_000a;
		}
	}
	{
		return;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::GetConfigurationLocation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ConfigurationStorage_GetConfigurationLocation_m25E918D2A5F76D2B4716BA377F44A73C3CA7D76F (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		String_t* L_0 = ((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->____fileLocation_2;
		bool L_1;
		L_1 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_0, NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		String_t* L_2 = ((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->____fileLocation_2;
		return L_2;
	}

IL_0012:
	{
		String_t* L_3;
		L_3 = Application_get_dataPath_m4C8412CBEE4EAAAB6711CC9BEFFA73CEE5BDBEF7(NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_002e;
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = ((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->___SaveFolders_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_9;
		L_9 = Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE(L_4, L_8, NULL);
		V_0 = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_10, 1));
	}

IL_002e:
	{
		int32_t L_11 = V_1;
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = ((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->___SaveFolders_0;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_13 = V_0;
		il2cpp_codegen_runtime_class_init_inline(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		String_t* L_14 = ((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->___SaveFile_1;
		il2cpp_codegen_runtime_class_init_inline(Path_t8A38A801D0219E8209C1B1D90D82D4D755D998BC_il2cpp_TypeInfo_var);
		String_t* L_15;
		L_15 = Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE(L_13, L_14, NULL);
		String_t* L_16 = L_15;
		((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->____fileLocation_2 = L_16;
		Il2CppCodeGenWriteBarrier((void**)(&((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->____fileLocation_2), (void*)L_16);
		return L_16;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::CombinePaths(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ConfigurationStorage_CombinePaths_m12F876CBC435A7CE33249507E6C4FCAAB4867964 (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___0_paths, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Aggregate_TisString_t_mE91CF40C639FFB958F5EA190CCEE0EABE463D577_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = ___0_paths;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129* L_1 = (ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129_il2cpp_TypeInfo_var)));
		NullCheck(L_1);
		ArgumentNullException__ctor_m444AE141157E333844FC1A9500224C2F9FD24F4B(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral02693BE56D23D49FBADB98D7AF6D95B6C888CA51)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ConfigurationStorage_CombinePaths_m12F876CBC435A7CE33249507E6C4FCAAB4867964_RuntimeMethod_var)));
	}

IL_000e:
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = ___0_paths;
		Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA* L_3 = (Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA*)il2cpp_codegen_object_new(Func_3_t939A6EACCC1AF5BEBF329B74AD1D13109632A3DA_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		Func_3__ctor_m275CCA1C836EECF7962B6DFFCA1EF52FD862A6EC(L_3, NULL, (intptr_t)((void*)Path_Combine_m1ADAC05CDA2D1D61B172DF65A81E86592696BEAE_RuntimeMethod_var), NULL);
		String_t* L_4;
		L_4 = Enumerable_Aggregate_TisString_t_mE91CF40C639FFB958F5EA190CCEE0EABE463D577((RuntimeObject*)L_2, L_3, Enumerable_Aggregate_TisString_t_mE91CF40C639FFB958F5EA190CCEE0EABE463D577_RuntimeMethod_var);
		return L_4;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurationStorage__ctor_m014DCD80FA2154109FF58129EBA3BA2C75869CD5 (ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurationStorage__cctor_m918228B2B6F592E226DD0F9739ED66F03B460755 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral78E86F042D371FD16F0D696EF476DAAA4953E187);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2247DCC2B2C53AFB97450C7F94158072595172B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5C4163563614243E6B5BF17A7ABA2017D1E7A0F);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)2);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral78E86F042D371FD16F0D696EF476DAAA4953E187);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral78E86F042D371FD16F0D696EF476DAAA4953E187);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteralD5C4163563614243E6B5BF17A7ABA2017D1E7A0F);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralD5C4163563614243E6B5BF17A7ABA2017D1E7A0F);
		((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->___SaveFolders_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->___SaveFolders_0), (void*)L_2);
		((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->___SaveFile_1 = _stringLiteralD2247DCC2B2C53AFB97450C7F94158072595172B;
		Il2CppCodeGenWriteBarrier((void**)(&((ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields*)il2cpp_codegen_static_fields_for(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_il2cpp_TypeInfo_var))->___SaveFile_1), (void*)_stringLiteralD2247DCC2B2C53AFB97450C7F94158072595172B);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration_set_DeepLinkingProtocols_m80CA6C00E0DF7DF62C13917F48FB5D76FD8E7950_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___0_value, const RuntimeMethod* method) 
{
	{
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_0 = ___0_value;
		__this->____deepLinkingProtocols_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____deepLinkingProtocols_1), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration_set_DomainProtocols_m2C70E312EA840E54AAEA0804B0F8D56F21628345_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ___0_value, const RuntimeMethod* method) 
{
	{
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_0 = ___0_value;
		__this->____domainProtocols_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____domainProtocols_0), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* PlatformLinkingConfiguration_get_DeepLinkingProtocols_m640ED35F65E1CF82EB519515AD89FA1C0A065EBE_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, const RuntimeMethod* method) 
{
	{
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_0 = __this->____deepLinkingProtocols_1;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* PlatformLinkingConfiguration_get_DomainProtocols_m0A8AA114FAB59AAD0021165117A89B652262B853_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, const RuntimeMethod* method) 
{
	{
		List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* L_0 = __this->____domainProtocols_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* AppLinkingConfiguration_get_CustomDeepLinkingProtocols_m2F50EDD2311D022262163A4B95A7E0CA70E888FB_inline (AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D* __this, const RuntimeMethod* method) 
{
	{
		PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* L_0 = __this->____customDeepLinkingProtocols_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlatformLinkingConfiguration_get_IsInitialized_m883564B2DC7DA019B9D4E71D0E7B868339E68981_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->____initialized_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlatformLinkingConfiguration_set_IsInitialized_m16B4E7BE13C785D3127BA332D67EAF73319428FE_inline (PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->____initialized_2 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LinkActivation_set_Uri_m57367A6018A51D18C68BCA49E0CD41EEBF932C89_inline (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CUriU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CUriU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LinkActivation_set_RawQueryString_m53B1488C42F6FD0891C168105A6164D616891234_inline (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CRawQueryStringU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CRawQueryStringU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LinkActivation_set_QueryString_m0D508E667298D0717BF7661D1A96D416D517343D_inline (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___0_value, const RuntimeMethod* method) 
{
	{
		Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* L_0 = ___0_value;
		__this->___U3CQueryStringU3Ek__BackingField_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CQueryStringU3Ek__BackingField_2), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* LinkActivation_get_Uri_mC6D342ABF0ACE38B8D2725A1AF77BC0379B73980_inline (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CUriU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* ProcessModule_get_FileName_mC1F3C43F68F8671C9B6A053B7A33C63BC4C2D03B_inline (ProcessModule_t7CCBC3E3C3382F1A431C2CE645F06224FC47BC16* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___filename_6;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Func_2_Invoke_mDBA25DA5DA5B7E056FB9B026AF041F1385FB58A9_gshared_inline (Func_2_tACBF5A1656250800CE861707354491F0611F6624* __this, RuntimeObject* ___0_arg, const RuntimeMethod* method) 
{
	typedef RuntimeObject* (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_arg, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		if (!true)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->____size_2;
		V_0 = L_1;
		__this->____size_2 = 0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		int32_t L_4 = V_0;
		Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB((RuntimeArray*)L_3, 0, L_4, NULL);
		return;
	}

IL_0035:
	{
		__this->____size_2 = 0;
	}

IL_003c:
	{
		return;
	}
}
