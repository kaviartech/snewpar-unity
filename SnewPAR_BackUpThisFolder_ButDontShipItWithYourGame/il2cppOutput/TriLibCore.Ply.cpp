﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct InterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericInterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// System.Action`1<TriLibCore.AssetLoaderContext>
struct Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226;
// System.Action`1<TriLibCore.IContextualizedError>
struct Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30;
// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
struct Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD;
// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64>
struct Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.General.CompoundMaterialKey,TriLibCore.TextureLoadingContext>
struct ConcurrentDictionary_2_t94764B51655C4F04FDAE59E1A6327AFEE05EA292;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,System.Collections.Generic.List`1<TriLibCore.MaterialRendererContext>>
struct ConcurrentDictionary_2_tA783589C825EB0CEA850D32094AAEFFBB3FD5D82;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material>
struct ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.ITexture,TriLibCore.TextureLoadingContext>
struct ConcurrentDictionary_2_tBB5915FD91B3F65141A8C3EC64A14F14EEA3905B;
// TriLibCore.General.ConcurrentDictionary`2<System.String,System.String>
struct ConcurrentDictionary_2_tCF44E0035FB42A2A1DF508A4CE0B233163C23F1E;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,TriLibCore.Interfaces.IModel>
struct Dictionary_2_tE704ACFE7C32537A046D8577F8299D1B52ED0C00;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String>
struct Dictionary_2_t15A9DEF843D5DA84170CD8536BA0EBB039EB4ADF;
// System.Collections.Generic.Dictionary`2<TriLibCore.Interfaces.IModel,UnityEngine.GameObject>
struct Dictionary_2_tADE1FC3F6C786CACD6652C2C7275C3A0FD274A9C;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]>
struct Dictionary_2_t23C2BC333CAB1901F8EC82B59264ED8D028DD1AB;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t9FA6D82CAFC18769F7515BB51D1C56DAE09381C3;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Object>
struct Dictionary_2_t4A0148843FDD82FE00634A604A772FC4EE3A0379;
// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyElement>
struct Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC;
// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>
struct Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_tE1603CE612C16451D1E56FF4D4859D4FE4087C28;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710;
// System.Collections.Generic.HashSet`1<UnityEngine.Texture>
struct HashSet_1_t70836788BCAF42568800A162B9F23937F5309AE8;
// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>
struct HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t823399AD16F88CDEB25958D8CB61771376A5ADED;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t79D4ADB15B238AC117DF72982FEA3C42EF5AFA19;
// System.Collections.Generic.IEqualityComparer`1<System.Int64>
struct IEqualityComparer_1_tDBA96AAC21C7C21D26B68A9F19E6AE4E015D2316;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rendering.VertexAttribute>
struct IEqualityComparer_1_tCB8B04D567BFC1D22CB3A6BEBC86439C73A31734;
// System.Collections.Generic.IList`1<UnityEngine.Color>
struct IList_1_t78DB7CACF5BDC17685CA41C8A5615F4AE760CB59;
// System.Collections.Generic.IList`1<LibTessDotNet.ContourVertex>
struct IList_1_t7B08130BDC464CC2BCD7D73811799DD84589F08D;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation>
struct IList_1_t7A16CD7EF0938B36E4D20182185F284ECA5F93A2;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera>
struct IList_1_t13EA3E1B6894AF8023B793D65EA2E1ED596B6E82;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>
struct IList_1_t54EA2EAA8FF287B3E144BC90047C3E635336CB4C;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight>
struct IList_1_t95B0FF72887258CDC012A1B81E66B66AF3BBE38E;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>
struct IList_1_t0662D113B996C51F1676FFC848F7B3448D818DB7;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>
struct IList_1_tBAC2F9CBFB365F17F69446225AF2802DEF7B2956;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>
struct IList_1_t2988C79E2C0A953B91ACE72118B299F94ECFEB62;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_tFB8BE2ED9A601C1259EAB8D73D1B3E96EA321FA1;
// System.Collections.Generic.IList`1<UnityEngine.Vector2>
struct IList_1_t0DF1E5F56EE58E1A7F1FE26A676FC9FBF4D52A07;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t4EEE459A249DDE104FA2E88234C593389EE5D291;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,TriLibCore.Ply.PlyElement>
struct KeyCollection_t7C7D7EEF9A007C010262940B4AEC7E3061ECFF27;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,TriLibCore.Ply.PlyProperty>
struct KeyCollection_t68BDB70AC2E0E8829F8F9519A28D13E563CF4250;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>
struct List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF;
// System.Collections.Generic.List`1<TriLibCore.Interfaces.ITexture>
struct List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3;
// System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>
struct List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB;
// System.Collections.Generic.List`1<System.Threading.Tasks.Task>
struct List_1_t84C257E858DDB8EA0B6269E08AAD9A2A2018A551;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B;
// System.Collections.Generic.Queue`1<TriLibCore.Interfaces.IContextualizedAction>
struct Queue_1_t952DE88AF42216B755D09647735E4235DA7138D4;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>
struct ValueCollection_tE8095E528D5C491DA5BF2C8694B3FEF0733A8BEB;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyElement>
struct ValueCollection_t16B888ACAE62FB5B6EB90AB83B113259C434EC38;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyProperty>
struct ValueCollection_t17A15458500B928C18C734F4964D798B1C114EE9;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E;
// System.Action`1<TriLibCore.TextureLoadingContext>[]
struct Action_1U5BU5D_t7706604B0FB5F3A4270EB313972114755AE7A123;
// System.Collections.Generic.Dictionary`2/Entry<System.Int64,TriLibCore.Ply.PlyElement>[]
struct EntryU5BU5D_t7E06B5474CC82D600EDDE8FCB80C8C94FB5F17F0;
// System.Collections.Generic.Dictionary`2/Entry<System.Int64,TriLibCore.Ply.PlyProperty>[]
struct EntryU5BU5D_t91203F7AE9706253C76AD809F11CF3AADD8E3624;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96;
// System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>[]
struct List_1U5BU5D_tA62C7ADB73E706494A104910CBF69BD1EF05FD21;
// System.Collections.Generic.HashSet`1/Slot<UnityEngine.Rendering.VertexAttribute>[]
struct SlotU5BU5D_tAE07F08746129C3374BC8C791AAFFE4C832AFB2A;
// TriLibCore.Mappers.AnimationClipMapper[]
struct AnimationClipMapperU5BU5D_t8E00A18562A07FD65A6E731D8BA6FF48D80BBFD8;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// UnityEngine.Color[]
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// TriLibCore.Interfaces.ITexture[]
struct ITextureU5BU5D_t4FDE2B940C74FD3BE4608C8EBF531CFA2D51FFFA;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// TriLibCore.Mappers.LipSyncMapper[]
struct LipSyncMapperU5BU5D_t32748FDCB493E8E7550A88244C1CBBB79E54C18A;
// TriLibCore.Mappers.MaterialMapper[]
struct MaterialMapperU5BU5D_tBD3B26C68148AE48AD6F3B44795C7B7B3EE2257B;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// TriLibCore.Ply.PlyValue[]
struct PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// TriLibCore.AssetLoaderContext
struct AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C;
// TriLibCore.AssetLoaderOptions
struct AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6;
// UnityEngine.Avatar
struct Avatar_t7861E57EEE2CF8CC61BD63C09737BA22F7ABCA0F;
// TriLibCore.Utils.BigEndianBinaryReader
struct BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721;
// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// System.Globalization.Calendar
struct Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
// System.Globalization.CompareInfo
struct CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57;
// System.Globalization.CultureData
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D;
// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A;
// System.Text.Decoder
struct Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095;
// System.Exception
struct Exception_t;
// TriLibCore.Mappers.ExternalDataMapper
struct ExternalDataMapper_t809726D72207DAF57227F4A5D67B9D01394B760A;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// TriLibCore.Geometries.Geometry
struct Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147;
// TriLibCore.General.HumanDescription
struct HumanDescription_t0BD271EF43944EC6940A10C164E94F8C7E750481;
// TriLibCore.Mappers.HumanoidAvatarMapper
struct HumanoidAvatarMapper_t691E00A2CE4455F03562FF79A586CC717D38FB09;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.IFormatProvider
struct IFormatProvider_tC202922D43BFF3525109ABF3FB79625F5646AB52;
// TriLibCore.Interfaces.IGeometryGroup
struct IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262;
// TriLibCore.Interfaces.IMaterial
struct IMaterial_t803D44AB6CC3544CCD396131BAE42FB12B128EE5;
// TriLibCore.Interfaces.IModel
struct IModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB;
// TriLibCore.Interfaces.IRootModel
struct IRootModel_t83ED40397FD23448FC9A99336523CC7DE8A841BB;
// TriLibCore.Interfaces.ITexture
struct ITexture_t4CD71425D2DAB0C38B4E57E909DEAC9A9AC89FE8;
// TriLibCore.Geometries.IVertexData
struct IVertexData_t0722DCB350D35DAB7A2596998EFC2AD210C14C75;
// TriLibCore.Geometries.InterpolatedVertex
struct InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// TriLibCore.Mappers.MaterialMapper
struct MaterialMapper_t5FE12658D8C551EE66D3441CFBDEDFAE5B85E692;
// TriLibCore.MaterialMapperContext
struct MaterialMapperContext_t2BDF775C916A28A411960E3787DC933D2E9F0042;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472;
// TriLibCore.Ply.PlyElement
struct PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8;
// TriLibCore.Ply.PlyGeometry
struct PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277;
// TriLibCore.Ply.PlyListProperty
struct PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E;
// TriLibCore.Ply.PlyMaterial
struct PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6;
// TriLibCore.Ply.PlyModel
struct PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2;
// TriLibCore.Ply.PlyProcessor
struct PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F;
// TriLibCore.Ply.PlyProperty
struct PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362;
// TriLibCore.Ply.Reader.PlyReader
struct PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F;
// TriLibCore.Ply.PlyRootModel
struct PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237;
// TriLibCore.Ply.PlyStreamReader
struct PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2;
// TriLibCore.Ply.PlyTexture
struct PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C;
// TriLibCore.Ply.Program
struct Program_tF221987E986ED204179045A85D41CBDBA2C0B625;
// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449;
// TriLibCore.Mappers.RootBoneMapper
struct RootBoneMapper_t64AE3E33364A832EE1B74D8B65BC9AA7B448DDA2;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37;
// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE;
// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B;
// System.String
struct String_t;
// System.Threading.Tasks.Task
struct Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572;
// System.Globalization.TextInfo
struct TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4;
// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7;
// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
// TriLibCore.TextureDataContext
struct TextureDataContext_tFA6C9B1525B47F9A11A34ACBF3BE7D238187B72D;
// TriLibCore.TextureLoadingContext
struct TextureLoadingContext_t0DB0AE7751B1A9E54C1612F112D2A295ED84D879;
// TriLibCore.Mappers.TextureMapper
struct TextureMapper_tCDB3B0D28AFDBA2BA4A640F75A7227C1B2D10ADD;
// System.Type
struct Type_t;
// TriLibCore.Mappers.UserPropertiesMapper
struct UserPropertiesMapper_t8437A569EBEB9E02E364D9951BE31F9601C55714;
// TriLibCore.General.VirtualMaterial
struct VirtualMaterial_t0610B7A6460E24DADF06569B4DA1D9FC487A52DB;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05;

IL2CPP_EXTERN_C RuntimeClass* BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Color_tD001788D726C3A7F1379BEED0260B9591F440C1F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_t77739521790EB7E3F514D44638D90AB775EDE8ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_tD4699121F1986A1CF0F28D10E91389DD1C7FDE69_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_tEF23B8A78E9122DAA952DB1517D31EB7EC90563B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_tFC51DD83E1437529AA19214EFADD5EE83B73CD6E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDictionary_2_t79D4ADB15B238AC117DF72982FEA3C42EF5AFA19_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IList_1_t0662D113B996C51F1676FFC848F7B3448D818DB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IList_1_t4EEE459A249DDE104FA2E88234C593389EE5D291_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ITexture_t4CD71425D2DAB0C38B4E57E909DEAC9A9AC89FE8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral2E7978A9177533BBDC5A8F6FE599F9D9A09593AF;
IL2CPP_EXTERN_C String_t* _stringLiteral3063F5F4FAEA0002A9B169DA0D12D52AC6312C1A;
IL2CPP_EXTERN_C String_t* _stringLiteral31A9B06BE46B0E2BB27797EDC5BAE6C2BFA4ABF0;
IL2CPP_EXTERN_C String_t* _stringLiteral4BEF98DAC2D5CE8B3F877FFD83A459125B80DA8F;
IL2CPP_EXTERN_C String_t* _stringLiteral5D934867D69D7E986A4C224DB49CF270468DE64D;
IL2CPP_EXTERN_C String_t* _stringLiteral711AA001951412D09872DB5FA0B90EA6875A17F9;
IL2CPP_EXTERN_C String_t* _stringLiteral7A12A09E76D9C6C6FAD55C385BB87538EF591395;
IL2CPP_EXTERN_C String_t* _stringLiteralB44FE4F8F197AF89FF50ABAD9C3E395B5E887251;
IL2CPP_EXTERN_C String_t* _stringLiteralE045B603FBBCEA228EE757E3C5C11BD708A2FA34;
IL2CPP_EXTERN_C String_t* _stringLiteralF0A80239664746BB5A8F67D47C690B1D476A5F49;
IL2CPP_EXTERN_C String_t* _stringLiteralF85DFEE8E7ED0F83CF41E7E6F08E0DAACA5F3C3B;
IL2CPP_EXTERN_C const RuntimeMethod* DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m13BA45BD072C072ECDC20ED17980959D50DCDEBE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m7E3EF4351105771F2D6EB167BE29E7868CFC84F0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m181BAACEB7905BC572C38343BF7DDA866E9415FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m4795CFCFA1CA4807CD85E1818CEC2A7EBC2E336C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m9E31627A68563218A84912174B1A3CCFF5D9D3DB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Count_m270BB8BF6E2728FF1BE69D236B3C1BB5E5D60BFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Values_m66C7C3835140D396B2941C5FF726A8435575E421_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Values_mD47C7DCD10B474F8DB55896C6B5F276ACA9E2BC1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m16443C4F8A0449761F7E9532DA13EE62BC66A959_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mFB43E311F648679BE046A19C9351234B9C43A1CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m355F0050D35C5CECAB616FAD6BD09BCDF75ED2AD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m3A6AF59F5158004E0E8F5D30912F0B3D578BAD20_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m16B76A6F6611FB82F01BB3EA7B36F6BE9AC3B558_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mA527966D30259A4C4C030B11F4DFEE3029C1076F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* IGeometryGroup_GetGeometry_TisPlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277_mD3FEA40366BC6279E62F5984369E846ADFC1D016_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m096BE2FF8568855341E87C1DE6291916F89A8203_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m7C99F2FA69D684BD5B7E22B8A115DA258EA04CB2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m179CF1BDD503F830F79CF1A81033083C1BB19E05_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m27475592735C318973899086F95036A18B6D7E39_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m71DCE2DD53C6EFD6DF99B2592A347DFD0F9106C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m7E6E0C4AE37ACBC1E9BDA9E56A8A7D8ACED3FD33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mDBC3F8F2846CD821DA096BECD6300438E2409BC3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mEEA3C6B55707EC6755329113F4C364960AA66CEF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeArrayUnsafeUtility_GetUnsafePtr_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_mDDC6F7A9E98335D0828894600921FCF3A934DB0A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeArray_1_Dispose_m11184C5FB1A1F3809B982476408F08F599F377F0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeArray_1__ctor_m01092412782EBC32088D2077868A10700AC7FE61_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlyProcessor_Process_m5EC73F6D1F63A911A1C86747BB95E7948D0D0763_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ValueCollection_GetEnumerator_m7A2633FD76AC8804D68EEB036B996CAF5FDC7754_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ValueCollection_GetEnumerator_mD927D6AC4BE78DAED8A83775F0D1FEF91A756B4D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* ProcessingSteps_t66470AEDB7538A34AC55F1AB85D626C1264892F4_0_0_0_var;
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_com;
struct CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_pinvoke;
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com;
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF;
struct IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711;
struct IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tCB3F017DD498D759E5FCD537C27107D77CF57E80 
{
};

// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyElement>
struct Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t7E06B5474CC82D600EDDE8FCB80C8C94FB5F17F0* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t7C7D7EEF9A007C010262940B4AEC7E3061ECFF27* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t16B888ACAE62FB5B6EB90AB83B113259C434EC38* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>
struct Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t91203F7AE9706253C76AD809F11CF3AADD8E3624* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t68BDB70AC2E0E8829F8F9519A28D13E563CF4250* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t17A15458500B928C18C734F4964D798B1C114EE9* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>
struct HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_7;
	// System.Collections.Generic.HashSet`1/Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_tAE07F08746129C3374BC8C791AAFFE4C832AFB2A* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37* ____siInfo_14;
};

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>
struct List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	List_1U5BU5D_tA62C7ADB73E706494A104910CBF69BD1EF05FD21* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<TriLibCore.Interfaces.ITexture>
struct List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ITextureU5BU5D_t4FDE2B940C74FD3BE4608C8EBF531CFA2D51FFFA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>
struct List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyElement>
struct ValueCollection_t16B888ACAE62FB5B6EB90AB83B113259C434EC38  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::_dictionary
	Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* ____dictionary_0;
};

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyProperty>
struct ValueCollection_t17A15458500B928C18C734F4964D798B1C114EE9  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::_dictionary
	Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* ____dictionary_0;
};

// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158  : public RuntimeObject
{
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC* ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;
};

// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0  : public RuntimeObject
{
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D* ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;
};
// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t8E26808B202927FEBF9064FCFEEA4D6E076E6472* ___numInfo_10;
	DateTimeFormatInfo_t0457520F9FA7B5C8EAAEB3AD50413B6AEEB7458A* ___dateTimeInfo_11;
	TextInfo_tD3BAFCFD77418851E7D5CB8D2588F47019E414B4* ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t1B1A6AC3486B570C76ABA52149C9BD4CD82F9E57* ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t0A117CC7532A54C17188C2EFEA1F79DB20DF3A3B* ___calendar_24;
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_tEEFDCF4ECA1BBF6C0C8C94EB3541657245598F9D_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// TriLibCore.Geometries.Geometry
struct Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147  : public RuntimeObject
{
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Geometries.Geometry::<OriginalVertexIndices>k__BackingField
	RuntimeObject* ___U3COriginalVertexIndicesU3Ek__BackingField_0;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Geometries.Geometry::<VertexDataIndices>k__BackingField
	RuntimeObject* ___U3CVertexDataIndicesU3Ek__BackingField_1;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Geometries.Geometry::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_2;
	// System.Int32 TriLibCore.Geometries.Geometry::<MaterialIndex>k__BackingField
	int32_t ___U3CMaterialIndexU3Ek__BackingField_3;
	// System.Boolean TriLibCore.Geometries.Geometry::<IsQuad>k__BackingField
	bool ___U3CIsQuadU3Ek__BackingField_4;
	// System.Int32 TriLibCore.Geometries.Geometry::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_5;
	// System.Int32 TriLibCore.Geometries.Geometry::<OriginalIndex>k__BackingField
	int32_t ___U3COriginalIndexU3Ek__BackingField_6;
	// System.Boolean TriLibCore.Geometries.Geometry::_disposed
	bool ____disposed_7;
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// TriLibCore.MaterialMapperContext
struct MaterialMapperContext_t2BDF775C916A28A411960E3787DC933D2E9F0042  : public RuntimeObject
{
	// TriLibCore.Interfaces.IMaterial TriLibCore.MaterialMapperContext::Material
	RuntimeObject* ___Material_0;
	// TriLibCore.General.VirtualMaterial TriLibCore.MaterialMapperContext::VirtualMaterial
	VirtualMaterial_t0610B7A6460E24DADF06569B4DA1D9FC487A52DB* ___VirtualMaterial_1;
	// UnityEngine.Material TriLibCore.MaterialMapperContext::UnityMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___UnityMaterial_2;
	// TriLibCore.AssetLoaderContext TriLibCore.MaterialMapperContext::<Context>k__BackingField
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___U3CContextU3Ek__BackingField_3;
	// UnityEngine.Material TriLibCore.MaterialMapperContext::AlphaMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___AlphaMaterial_4;
	// TriLibCore.Mappers.MaterialMapper TriLibCore.MaterialMapperContext::MaterialMapper
	MaterialMapper_t5FE12658D8C551EE66D3441CFBDEDFAE5B85E692* ___MaterialMapper_5;
	// System.Int32 TriLibCore.MaterialMapperContext::Index
	int32_t ___Index_6;
	// System.Boolean TriLibCore.MaterialMapperContext::<Completed>k__BackingField
	bool ___U3CCompletedU3Ek__BackingField_7;
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// TriLibCore.Ply.PlyElement
struct PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty> TriLibCore.Ply.PlyElement::Properties
	Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* ___Properties_0;
	// System.Int32 TriLibCore.Ply.PlyElement::Count
	int32_t ___Count_1;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>> TriLibCore.Ply.PlyElement::Data
	List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* ___Data_2;
};

// TriLibCore.Ply.PlyMaterial
struct PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6  : public RuntimeObject
{
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> TriLibCore.Ply.PlyMaterial::_properties
	RuntimeObject* ____properties_0;
	// System.String TriLibCore.Ply.PlyMaterial::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Boolean TriLibCore.Ply.PlyMaterial::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_2;
	// System.Int32 TriLibCore.Ply.PlyMaterial::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_3;
	// System.Boolean TriLibCore.Ply.PlyMaterial::<Processing>k__BackingField
	bool ___U3CProcessingU3Ek__BackingField_4;
	// System.Boolean TriLibCore.Ply.PlyMaterial::<Processed>k__BackingField
	bool ___U3CProcessedU3Ek__BackingField_5;
};

// TriLibCore.Ply.PlyProcessor
struct PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F  : public RuntimeObject
{
	// TriLibCore.Ply.Reader.PlyReader TriLibCore.Ply.PlyProcessor::_reader
	PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* ____reader_2;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Ply.PlyProcessor::_allVertices
	RuntimeObject* ____allVertices_3;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Ply.PlyProcessor::_allNormals
	RuntimeObject* ____allNormals_4;
	// System.Collections.Generic.IList`1<UnityEngine.Color> TriLibCore.Ply.PlyProcessor::_allColors
	RuntimeObject* ____allColors_5;
	// System.Collections.Generic.IList`1<UnityEngine.Vector2> TriLibCore.Ply.PlyProcessor::_allUVs
	RuntimeObject* ____allUVs_6;
	// System.Int32 TriLibCore.Ply.PlyProcessor::_floatCount
	int32_t ____floatCount_7;
	// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute> TriLibCore.Ply.PlyProcessor::_vertexAttributes
	HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* ____vertexAttributes_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Ply.PlyProcessor::_geometryGroup
	RuntimeObject* ____geometryGroup_9;
};

// TriLibCore.Ply.PlyProperty
struct PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362  : public RuntimeObject
{
	// TriLibCore.Ply.PlyPropertyType TriLibCore.Ply.PlyProperty::Type
	int32_t ___Type_0;
	// System.Int32 TriLibCore.Ply.PlyProperty::Offset
	int32_t ___Offset_1;
};

// TriLibCore.Ply.Program
struct Program_tF221987E986ED204179045A85D41CBDBA2C0B625  : public RuntimeObject
{
};

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449  : public RuntimeObject
{
	// System.String[] TriLibCore.ReaderBase::_loadingStepEnumNames
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____loadingStepEnumNames_1;
	// TriLibCore.AssetLoaderContext TriLibCore.ReaderBase::<AssetLoaderContext>k__BackingField
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___U3CAssetLoaderContextU3Ek__BackingField_2;
	// System.String TriLibCore.ReaderBase::_filename
	String_t* ____filename_3;
	// System.Action`2<TriLibCore.AssetLoaderContext,System.Single> TriLibCore.ReaderBase::_onProgress
	Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ____onProgress_4;
	// System.Int32 TriLibCore.ReaderBase::_nameCounter
	int32_t ____nameCounter_5;
	// System.Int32 TriLibCore.ReaderBase::_materialCounter
	int32_t ____materialCounter_6;
	// System.Int32 TriLibCore.ReaderBase::_textureCounter
	int32_t ____textureCounter_7;
	// System.Int32 TriLibCore.ReaderBase::_geometryGroupCounter
	int32_t ____geometryGroupCounter_8;
	// System.Int32 TriLibCore.ReaderBase::_animationCounter
	int32_t ____animationCounter_9;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// TriLibCore.TextureLoadingContext
struct TextureLoadingContext_t0DB0AE7751B1A9E54C1612F112D2A295ED84D879  : public RuntimeObject
{
	// TriLibCore.General.TextureType TriLibCore.TextureLoadingContext::TextureType
	int32_t ___TextureType_0;
	// TriLibCore.TextureDataContext TriLibCore.TextureLoadingContext::TextureDataContext
	TextureDataContext_tFA6C9B1525B47F9A11A34ACBF3BE7D238187B72D* ___TextureDataContext_1;
	// TriLibCore.MaterialMapperContext TriLibCore.TextureLoadingContext::MaterialMapperContext
	MaterialMapperContext_t2BDF775C916A28A411960E3787DC933D2E9F0042* ___MaterialMapperContext_2;
	// System.Boolean TriLibCore.TextureLoadingContext::TextureProcessed
	bool ___TextureProcessed_3;
	// System.Boolean TriLibCore.TextureLoadingContext::<Completed>k__BackingField
	bool ___U3CCompletedU3Ek__BackingField_4;
	// TriLibCore.AssetLoaderContext TriLibCore.TextureLoadingContext::<Context>k__BackingField
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___U3CContextU3Ek__BackingField_5;
	// UnityEngine.Texture TriLibCore.TextureLoadingContext::<UnityTexture>k__BackingField
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___U3CUnityTextureU3Ek__BackingField_6;
	// System.Int32 TriLibCore.TextureLoadingContext::CreationBytesPerPixel
	int32_t ___CreationBytesPerPixel_7;
	// System.Action`1<TriLibCore.TextureLoadingContext>[] TriLibCore.TextureLoadingContext::OnTextureProcessed
	Action_1U5BU5D_t7706604B0FB5F3A4270EB313972114755AE7A123* ___OnTextureProcessed_8;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>
struct Enumerator_t101EECDB54BD1F67FD91D0D649333AC62D79665D 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_dictionary
	Dictionary_2_t4A0148843FDD82FE00634A604A772FC4EE3A0379* ____dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_version
	int32_t ____version_2;
	// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_currentValue
	RuntimeObject* ____currentValue_3;
};

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyElement>
struct Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_dictionary
	Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* ____dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_version
	int32_t ____version_2;
	// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_currentValue
	PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* ____currentValue_3;
};

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyProperty>
struct Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_dictionary
	Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* ____dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_version
	int32_t ____version_2;
	// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::_currentValue
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ____currentValue_3;
};

// Unity.Collections.NativeArray`1<System.Char>
struct NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A 
{
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;
};

// TriLibCore.Utils.BigEndianBinaryReader
struct BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721  : public BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158
{
	// System.Byte[] TriLibCore.Utils.BigEndianBinaryReader::_data2
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____data2_10;
	// System.Byte[] TriLibCore.Utils.BigEndianBinaryReader::_data4
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____data4_11;
	// System.Byte[] TriLibCore.Utils.BigEndianBinaryReader::_data8
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____data8_12;
};

// UnityEngine.BoneWeight
struct BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F 
{
	// System.Single UnityEngine.BoneWeight::m_Weight0
	float ___m_Weight0_0;
	// System.Single UnityEngine.BoneWeight::m_Weight1
	float ___m_Weight1_1;
	// System.Single UnityEngine.BoneWeight::m_Weight2
	float ___m_Weight2_2;
	// System.Single UnityEngine.BoneWeight::m_Weight3
	float ___m_Weight3_3;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex0
	int32_t ___m_BoneIndex0_4;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex1
	int32_t ___m_BoneIndex1_5;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex2
	int32_t ___m_BoneIndex2_6;
	// System.Int32 UnityEngine.BoneWeight::m_BoneIndex3
	int32_t ___m_BoneIndex3_7;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED 
{
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::_source
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};
// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_pinvoke
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_com
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int16
struct Int16_tB8EF286A9C33492FA6E6D6E67320BE93E794A175 
{
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

// TriLibCore.Ply.PlyGeometry
struct PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277  : public Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147
{
};

// TriLibCore.Ply.PlyListProperty
struct PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E  : public PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362
{
	// TriLibCore.Ply.PlyPropertyType TriLibCore.Ply.PlyListProperty::CounterType
	int32_t ___CounterType_2;
	// TriLibCore.Ply.PlyPropertyType TriLibCore.Ply.PlyListProperty::ItemType
	int32_t ___ItemType_3;
};

// TriLibCore.Ply.Reader.PlyReader
struct PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F  : public ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449
{
};

// TriLibCore.Ply.PlyValue
struct PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 
{
	// System.UInt32 TriLibCore.Ply.PlyValue::_intValue
	uint32_t ____intValue_0;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

// System.SByte
struct SByte_tFEFFEF5D2FEBF5207950AE6FAC150FC53B668DB5 
{
	// System.SByte System.SByte::m_value
	int8_t ___m_value_0;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05* ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2* ____asyncActiveSemaphore_4;
};

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
};

// System.UInt16
struct UInt16_tF4C148C876015C212FD72652D0B6ED8CC247A455 
{
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// LibTessDotNet.Vec3
struct Vec3_t7AA9166B9BB818308F0807E2023584038C0104F6 
{
	// System.Single LibTessDotNet.Vec3::X
	float ___X_1;
	// System.Single LibTessDotNet.Vec3::Y
	float ___Y_2;
	// System.Single LibTessDotNet.Vec3::Z
	float ___Z_3;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// TriLibCore.AssetLoaderContext
struct AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C  : public RuntimeObject
{
	// TriLibCore.AssetLoaderOptions TriLibCore.AssetLoaderContext::Options
	AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* ___Options_0;
	// TriLibCore.ReaderBase TriLibCore.AssetLoaderContext::Reader
	ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* ___Reader_1;
	// System.String TriLibCore.AssetLoaderContext::Filename
	String_t* ___Filename_2;
	// System.String TriLibCore.AssetLoaderContext::FileExtension
	String_t* ___FileExtension_3;
	// System.IO.Stream TriLibCore.AssetLoaderContext::Stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Stream_4;
	// System.String TriLibCore.AssetLoaderContext::BasePath
	String_t* ___BasePath_5;
	// UnityEngine.GameObject TriLibCore.AssetLoaderContext::RootGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___RootGameObject_6;
	// TriLibCore.Interfaces.IRootModel TriLibCore.AssetLoaderContext::RootModel
	RuntimeObject* ___RootModel_7;
	// System.Collections.Generic.Dictionary`2<TriLibCore.Interfaces.IModel,UnityEngine.GameObject> TriLibCore.AssetLoaderContext::GameObjects
	Dictionary_2_tADE1FC3F6C786CACD6652C2C7275C3A0FD274A9C* ___GameObjects_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,TriLibCore.Interfaces.IModel> TriLibCore.AssetLoaderContext::Models
	Dictionary_2_tE704ACFE7C32537A046D8577F8299D1B52ED0C00* ___Models_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String> TriLibCore.AssetLoaderContext::GameObjectPaths
	Dictionary_2_t15A9DEF843D5DA84170CD8536BA0EBB039EB4ADF* ___GameObjectPaths_10;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,System.Collections.Generic.List`1<TriLibCore.MaterialRendererContext>> TriLibCore.AssetLoaderContext::MaterialRenderers
	ConcurrentDictionary_2_tA783589C825EB0CEA850D32094AAEFFBB3FD5D82* ___MaterialRenderers_11;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material> TriLibCore.AssetLoaderContext::LoadedMaterials
	ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2* ___LoadedMaterials_12;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material> TriLibCore.AssetLoaderContext::GeneratedMaterials
	ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2* ___GeneratedMaterials_13;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.ITexture,TriLibCore.TextureLoadingContext> TriLibCore.AssetLoaderContext::LoadedTextures
	ConcurrentDictionary_2_tBB5915FD91B3F65141A8C3EC64A14F14EEA3905B* ___LoadedTextures_14;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.General.CompoundMaterialKey,TriLibCore.TextureLoadingContext> TriLibCore.AssetLoaderContext::MaterialTextures
	ConcurrentDictionary_2_t94764B51655C4F04FDAE59E1A6327AFEE05EA292* ___MaterialTextures_15;
	// TriLibCore.General.ConcurrentDictionary`2<System.String,System.String> TriLibCore.AssetLoaderContext::LoadedExternalData
	ConcurrentDictionary_2_tCF44E0035FB42A2A1DF508A4CE0B233163C23F1E* ___LoadedExternalData_16;
	// System.Collections.Generic.HashSet`1<UnityEngine.Texture> TriLibCore.AssetLoaderContext::UsedTextures
	HashSet_1_t70836788BCAF42568800A162B9F23937F5309AE8* ___UsedTextures_17;
	// System.Collections.Generic.List`1<UnityEngine.Object> TriLibCore.AssetLoaderContext::Allocations
	List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3* ___Allocations_18;
	// System.Boolean TriLibCore.AssetLoaderContext::Async
	bool ___Async_19;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnLoad_20;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnMaterialsLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnMaterialsLoad_21;
	// System.Action`2<TriLibCore.AssetLoaderContext,System.Single> TriLibCore.AssetLoaderContext::OnProgress
	Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___OnProgress_22;
	// System.Action`1<TriLibCore.IContextualizedError> TriLibCore.AssetLoaderContext::OnError
	Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30* ___OnError_23;
	// System.Action`1<TriLibCore.IContextualizedError> TriLibCore.AssetLoaderContext::HandleError
	Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30* ___HandleError_24;
	// System.Object TriLibCore.AssetLoaderContext::CustomData
	RuntimeObject* ___CustomData_25;
	// System.Collections.Generic.List`1<System.Threading.Tasks.Task> TriLibCore.AssetLoaderContext::Tasks
	List_1_t84C257E858DDB8EA0B6269E08AAD9A2A2018A551* ___Tasks_26;
	// System.Threading.Tasks.Task TriLibCore.AssetLoaderContext::Task
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___Task_27;
	// System.Boolean TriLibCore.AssetLoaderContext::HaltTasks
	bool ___HaltTasks_28;
	// UnityEngine.GameObject TriLibCore.AssetLoaderContext::WrapperGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___WrapperGameObject_29;
	// System.Threading.CancellationToken TriLibCore.AssetLoaderContext::CancellationToken
	CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED ___CancellationToken_30;
	// System.Single TriLibCore.AssetLoaderContext::LoadingProgress
	float ___LoadingProgress_31;
	// System.Threading.CancellationTokenSource TriLibCore.AssetLoaderContext::CancellationTokenSource
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ___CancellationTokenSource_32;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnPreLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnPreLoad_33;
	// System.Boolean TriLibCore.AssetLoaderContext::IsZipFile
	bool ___IsZipFile_34;
	// System.String TriLibCore.AssetLoaderContext::<PersistentDataPath>k__BackingField
	String_t* ___U3CPersistentDataPathU3Ek__BackingField_35;
	// System.String TriLibCore.AssetLoaderContext::ModificationDate
	String_t* ___ModificationDate_36;
	// System.Int32 TriLibCore.AssetLoaderContext::LoadingStep
	int32_t ___LoadingStep_37;
	// System.Int32 TriLibCore.AssetLoaderContext::PreviousLoadingStep
	int32_t ___PreviousLoadingStep_38;
	// System.Collections.Generic.Queue`1<TriLibCore.Interfaces.IContextualizedAction> TriLibCore.AssetLoaderContext::<CustomDispatcherQueue>k__BackingField
	Queue_1_t952DE88AF42216B755D09647735E4235DA7138D4* ___U3CCustomDispatcherQueueU3Ek__BackingField_39;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]> TriLibCore.AssetLoaderContext::_bufferPool
	Dictionary_2_t23C2BC333CAB1901F8EC82B59264ED8D028DD1AB* ____bufferPool_40;
	// System.Boolean TriLibCore.AssetLoaderContext::<Completed>k__BackingField
	bool ___U3CCompletedU3Ek__BackingField_41;
};

// LibTessDotNet.ContourVertex
struct ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F 
{
	// LibTessDotNet.Vec3 LibTessDotNet.ContourVertex::Position
	Vec3_t7AA9166B9BB818308F0807E2023584038C0104F6 ___Position_0;
	// System.Object LibTessDotNet.ContourVertex::Data
	RuntimeObject* ___Data_1;
};
// Native definition for P/Invoke marshalling of LibTessDotNet.ContourVertex
struct ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F_marshaled_pinvoke
{
	Vec3_t7AA9166B9BB818308F0807E2023584038C0104F6 ___Position_0;
	Il2CppIUnknown* ___Data_1;
};
// Native definition for COM marshalling of LibTessDotNet.ContourVertex
struct ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F_marshaled_com
{
	Vec3_t7AA9166B9BB818308F0807E2023584038C0104F6 ___Position_0;
	Il2CppIUnknown* ___Data_1;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// TriLibCore.Geometries.InterpolatedVertex
struct InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5  : public RuntimeObject
{
	// UnityEngine.Vector3 TriLibCore.Geometries.InterpolatedVertex::_position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ____position_0;
	// UnityEngine.Vector3 TriLibCore.Geometries.InterpolatedVertex::_normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ____normal_1;
	// UnityEngine.Vector4 TriLibCore.Geometries.InterpolatedVertex::_tangent
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ____tangent_2;
	// UnityEngine.Color TriLibCore.Geometries.InterpolatedVertex::_color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ____color_3;
	// UnityEngine.Vector2 TriLibCore.Geometries.InterpolatedVertex::_uv0
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ____uv0_4;
	// UnityEngine.Vector2 TriLibCore.Geometries.InterpolatedVertex::_uv1
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ____uv1_5;
	// UnityEngine.Vector2 TriLibCore.Geometries.InterpolatedVertex::_uv2
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ____uv2_6;
	// UnityEngine.Vector2 TriLibCore.Geometries.InterpolatedVertex::_uv3
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ____uv3_7;
	// UnityEngine.BoneWeight TriLibCore.Geometries.InterpolatedVertex::_boneWeight
	BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F ____boneWeight_8;
	// System.Int32 TriLibCore.Geometries.InterpolatedVertex::_vertexIndex
	int32_t ____vertexIndex_9;
	// System.Int32 TriLibCore.Geometries.InterpolatedVertex::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_10;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// TriLibCore.Ply.PlyModel
struct PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2  : public RuntimeObject
{
	// System.String TriLibCore.Ply.PlyModel::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Ply.PlyModel::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// UnityEngine.Vector3 TriLibCore.Ply.PlyModel::<LocalPosition>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalPositionU3Ek__BackingField_2;
	// UnityEngine.Quaternion TriLibCore.Ply.PlyModel::<LocalRotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CLocalRotationU3Ek__BackingField_3;
	// UnityEngine.Vector3 TriLibCore.Ply.PlyModel::<LocalScale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalScaleU3Ek__BackingField_4;
	// System.Boolean TriLibCore.Ply.PlyModel::<Visibility>k__BackingField
	bool ___U3CVisibilityU3Ek__BackingField_5;
	// TriLibCore.Interfaces.IModel TriLibCore.Ply.PlyModel::<Parent>k__BackingField
	RuntimeObject* ___U3CParentU3Ek__BackingField_6;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Ply.PlyModel::<Children>k__BackingField
	RuntimeObject* ___U3CChildrenU3Ek__BackingField_7;
	// System.Boolean TriLibCore.Ply.PlyModel::<IsBone>k__BackingField
	bool ___U3CIsBoneU3Ek__BackingField_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Ply.PlyModel::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_9;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Ply.PlyModel::<Bones>k__BackingField
	RuntimeObject* ___U3CBonesU3Ek__BackingField_10;
	// UnityEngine.Matrix4x4[] TriLibCore.Ply.PlyModel::<BindPoses>k__BackingField
	Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___U3CBindPosesU3Ek__BackingField_11;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Ply.PlyModel::<MaterialIndices>k__BackingField
	RuntimeObject* ___U3CMaterialIndicesU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Ply.PlyModel::<UserProperties>k__BackingField
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___U3CUserPropertiesU3Ek__BackingField_13;
};

// TriLibCore.Ply.PlyTexture
struct PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C  : public RuntimeObject
{
	// System.String TriLibCore.Ply.PlyTexture::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Ply.PlyTexture::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// System.Byte[] TriLibCore.Ply.PlyTexture::<Data>k__BackingField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___U3CDataU3Ek__BackingField_2;
	// System.IO.Stream TriLibCore.Ply.PlyTexture::<DataStream>k__BackingField
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___U3CDataStreamU3Ek__BackingField_3;
	// System.String TriLibCore.Ply.PlyTexture::<Filename>k__BackingField
	String_t* ___U3CFilenameU3Ek__BackingField_4;
	// UnityEngine.TextureWrapMode TriLibCore.Ply.PlyTexture::<WrapModeU>k__BackingField
	int32_t ___U3CWrapModeUU3Ek__BackingField_5;
	// UnityEngine.TextureWrapMode TriLibCore.Ply.PlyTexture::<WrapModeV>k__BackingField
	int32_t ___U3CWrapModeVU3Ek__BackingField_6;
	// UnityEngine.Vector2 TriLibCore.Ply.PlyTexture::<Tiling>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CTilingU3Ek__BackingField_7;
	// UnityEngine.Vector2 TriLibCore.Ply.PlyTexture::<Offset>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3COffsetU3Ek__BackingField_8;
	// System.Int32 TriLibCore.Ply.PlyTexture::<TextureId>k__BackingField
	int32_t ___U3CTextureIdU3Ek__BackingField_9;
	// System.String TriLibCore.Ply.PlyTexture::<ResolvedFilename>k__BackingField
	String_t* ___U3CResolvedFilenameU3Ek__BackingField_10;
	// System.Boolean TriLibCore.Ply.PlyTexture::<HasAlpha>k__BackingField
	bool ___U3CHasAlphaU3Ek__BackingField_11;
	// TriLibCore.General.TextureFormat TriLibCore.Ply.PlyTexture::<TextureFormat>k__BackingField
	int32_t ___U3CTextureFormatU3Ek__BackingField_12;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B  : public TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7
{
	// System.IO.Stream System.IO.StreamReader::_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ____stream_3;
	// System.Text.Encoding System.IO.StreamReader::_encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ____encoding_4;
	// System.Text.Decoder System.IO.StreamReader::_decoder
	Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC* ____decoder_5;
	// System.Byte[] System.IO.StreamReader::_byteBuffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____byteBuffer_6;
	// System.Char[] System.IO.StreamReader::_charBuffer
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____charBuffer_7;
	// System.Int32 System.IO.StreamReader::_charPos
	int32_t ____charPos_8;
	// System.Int32 System.IO.StreamReader::_charLen
	int32_t ____charLen_9;
	// System.Int32 System.IO.StreamReader::_byteLen
	int32_t ____byteLen_10;
	// System.Int32 System.IO.StreamReader::_bytePos
	int32_t ____bytePos_11;
	// System.Int32 System.IO.StreamReader::_maxCharsPerBuffer
	int32_t ____maxCharsPerBuffer_12;
	// System.Boolean System.IO.StreamReader::_detectEncoding
	bool ____detectEncoding_13;
	// System.Boolean System.IO.StreamReader::_checkPreamble
	bool ____checkPreamble_14;
	// System.Boolean System.IO.StreamReader::_isBlocked
	bool ____isBlocked_15;
	// System.Boolean System.IO.StreamReader::_closable
	bool ____closable_16;
	// System.Threading.Tasks.Task System.IO.StreamReader::_asyncReadTask
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ____asyncReadTask_17;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// TriLibCore.Ply.PlyRootModel
struct PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237  : public PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2
{
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Ply.PlyRootModel::<AllModels>k__BackingField
	RuntimeObject* ___U3CAllModelsU3Ek__BackingField_14;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Ply.PlyRootModel::<AllGeometryGroups>k__BackingField
	RuntimeObject* ___U3CAllGeometryGroupsU3Ek__BackingField_15;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Ply.PlyRootModel::<AllAnimations>k__BackingField
	RuntimeObject* ___U3CAllAnimationsU3Ek__BackingField_16;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Ply.PlyRootModel::<AllMaterials>k__BackingField
	RuntimeObject* ___U3CAllMaterialsU3Ek__BackingField_17;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Ply.PlyRootModel::<AllTextures>k__BackingField
	RuntimeObject* ___U3CAllTexturesU3Ek__BackingField_18;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Ply.PlyRootModel::<AllCameras>k__BackingField
	RuntimeObject* ___U3CAllCamerasU3Ek__BackingField_19;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Ply.PlyRootModel::<AllLights>k__BackingField
	RuntimeObject* ___U3CAllLightsU3Ek__BackingField_20;
};

// TriLibCore.Ply.PlyStreamReader
struct PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2  : public StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B
{
	// Unity.Collections.NativeArray`1<System.Char> TriLibCore.Ply.PlyStreamReader::_charStream
	NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A ____charStream_24;
	// System.String TriLibCore.Ply.PlyStreamReader::_charString
	String_t* ____charString_25;
	// System.Int32 TriLibCore.Ply.PlyStreamReader::_charPosition
	int32_t ____charPosition_26;
	// System.Int64 TriLibCore.Ply.PlyStreamReader::<Position>k__BackingField
	int64_t ___U3CPositionU3Ek__BackingField_27;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
struct Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD  : public MulticastDelegate_t
{
};

// TriLibCore.AssetLoaderOptions
struct AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.Boolean TriLibCore.AssetLoaderOptions::UseFileScale
	bool ___UseFileScale_4;
	// System.Single TriLibCore.AssetLoaderOptions::ScaleFactor
	float ___ScaleFactor_5;
	// System.Boolean TriLibCore.AssetLoaderOptions::SortHierarchyByName
	bool ___SortHierarchyByName_6;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportVisibility
	bool ___ImportVisibility_7;
	// System.Boolean TriLibCore.AssetLoaderOptions::Static
	bool ___Static_8;
	// System.Boolean TriLibCore.AssetLoaderOptions::AddAssetUnloader
	bool ___AddAssetUnloader_9;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportMeshes
	bool ___ImportMeshes_10;
	// System.Boolean TriLibCore.AssetLoaderOptions::LimitBoneWeights
	bool ___LimitBoneWeights_11;
	// System.Boolean TriLibCore.AssetLoaderOptions::ReadEnabled
	bool ___ReadEnabled_12;
	// System.Boolean TriLibCore.AssetLoaderOptions::ReadAndWriteEnabled
	bool ___ReadAndWriteEnabled_13;
	// System.Boolean TriLibCore.AssetLoaderOptions::MarkMeshesAsDynamic
	bool ___MarkMeshesAsDynamic_14;
	// System.Boolean TriLibCore.AssetLoaderOptions::OptimizeMeshes
	bool ___OptimizeMeshes_15;
	// System.Boolean TriLibCore.AssetLoaderOptions::GenerateColliders
	bool ___GenerateColliders_16;
	// System.Boolean TriLibCore.AssetLoaderOptions::ConvexColliders
	bool ___ConvexColliders_17;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportBlendShapes
	bool ___ImportBlendShapes_18;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportColors
	bool ___ImportColors_19;
	// UnityEngine.Rendering.IndexFormat TriLibCore.AssetLoaderOptions::IndexFormat
	int32_t ___IndexFormat_20;
	// System.Single TriLibCore.AssetLoaderOptions::LODScreenRelativeTransitionHeightBase
	float ___LODScreenRelativeTransitionHeightBase_21;
	// System.Boolean TriLibCore.AssetLoaderOptions::KeepQuads
	bool ___KeepQuads_22;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportNormals
	bool ___ImportNormals_23;
	// System.Boolean TriLibCore.AssetLoaderOptions::GenerateNormals
	bool ___GenerateNormals_24;
	// System.Boolean TriLibCore.AssetLoaderOptions::GenerateTangents
	bool ___GenerateTangents_25;
	// System.Single TriLibCore.AssetLoaderOptions::SmoothingAngle
	float ___SmoothingAngle_26;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportBlendShapeNormals
	bool ___ImportBlendShapeNormals_27;
	// System.Boolean TriLibCore.AssetLoaderOptions::CalculateBlendShapeNormals
	bool ___CalculateBlendShapeNormals_28;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportTangents
	bool ___ImportTangents_29;
	// System.Boolean TriLibCore.AssetLoaderOptions::SwapUVs
	bool ___SwapUVs_30;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportMaterials
	bool ___ImportMaterials_31;
	// TriLibCore.Mappers.MaterialMapper[] TriLibCore.AssetLoaderOptions::MaterialMappers
	MaterialMapperU5BU5D_tBD3B26C68148AE48AD6F3B44795C7B7B3EE2257B* ___MaterialMappers_32;
	// System.Boolean TriLibCore.AssetLoaderOptions::AddSecondAlphaMaterial
	bool ___AddSecondAlphaMaterial_33;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportTextures
	bool ___ImportTextures_34;
	// System.Boolean TriLibCore.AssetLoaderOptions::Enforce16BitsTextures
	bool ___Enforce16BitsTextures_35;
	// System.Boolean TriLibCore.AssetLoaderOptions::ScanForAlphaPixels
	bool ___ScanForAlphaPixels_36;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseAlphaMaterials
	bool ___UseAlphaMaterials_37;
	// TriLibCore.General.AlphaMaterialMode TriLibCore.AssetLoaderOptions::AlphaMaterialMode
	int32_t ___AlphaMaterialMode_38;
	// System.Boolean TriLibCore.AssetLoaderOptions::DoubleSidedMaterials
	bool ___DoubleSidedMaterials_39;
	// TriLibCore.Mappers.TextureMapper TriLibCore.AssetLoaderOptions::TextureMapper
	TextureMapper_tCDB3B0D28AFDBA2BA4A640F75A7227C1B2D10ADD* ___TextureMapper_40;
	// TriLibCore.General.TextureCompressionQuality TriLibCore.AssetLoaderOptions::TextureCompressionQuality
	int32_t ___TextureCompressionQuality_41;
	// System.Boolean TriLibCore.AssetLoaderOptions::GenerateMipmaps
	bool ___GenerateMipmaps_42;
	// System.Boolean TriLibCore.AssetLoaderOptions::FixNormalMaps
	bool ___FixNormalMaps_43;
	// TriLibCore.General.AnimationType TriLibCore.AssetLoaderOptions::AnimationType
	int32_t ___AnimationType_44;
	// System.Boolean TriLibCore.AssetLoaderOptions::SimplifyAnimations
	bool ___SimplifyAnimations_45;
	// System.Single TriLibCore.AssetLoaderOptions::PositionThreshold
	float ___PositionThreshold_46;
	// System.Single TriLibCore.AssetLoaderOptions::RotationThreshold
	float ___RotationThreshold_47;
	// System.Single TriLibCore.AssetLoaderOptions::ScaleThreshold
	float ___ScaleThreshold_48;
	// TriLibCore.General.AvatarDefinitionType TriLibCore.AssetLoaderOptions::AvatarDefinition
	int32_t ___AvatarDefinition_49;
	// UnityEngine.Avatar TriLibCore.AssetLoaderOptions::Avatar
	Avatar_t7861E57EEE2CF8CC61BD63C09737BA22F7ABCA0F* ___Avatar_50;
	// TriLibCore.General.HumanDescription TriLibCore.AssetLoaderOptions::HumanDescription
	HumanDescription_t0BD271EF43944EC6940A10C164E94F8C7E750481* ___HumanDescription_51;
	// TriLibCore.Mappers.RootBoneMapper TriLibCore.AssetLoaderOptions::RootBoneMapper
	RootBoneMapper_t64AE3E33364A832EE1B74D8B65BC9AA7B448DDA2* ___RootBoneMapper_52;
	// TriLibCore.Mappers.HumanoidAvatarMapper TriLibCore.AssetLoaderOptions::HumanoidAvatarMapper
	HumanoidAvatarMapper_t691E00A2CE4455F03562FF79A586CC717D38FB09* ___HumanoidAvatarMapper_53;
	// TriLibCore.Mappers.LipSyncMapper[] TriLibCore.AssetLoaderOptions::LipSyncMappers
	LipSyncMapperU5BU5D_t32748FDCB493E8E7550A88244C1CBBB79E54C18A* ___LipSyncMappers_54;
	// System.Boolean TriLibCore.AssetLoaderOptions::SampleBindPose
	bool ___SampleBindPose_55;
	// System.Boolean TriLibCore.AssetLoaderOptions::EnforceTPose
	bool ___EnforceTPose_56;
	// System.Boolean TriLibCore.AssetLoaderOptions::ResampleAnimations
	bool ___ResampleAnimations_57;
	// System.Boolean TriLibCore.AssetLoaderOptions::EnforceAnimatorWithLegacyAnimations
	bool ___EnforceAnimatorWithLegacyAnimations_58;
	// System.Boolean TriLibCore.AssetLoaderOptions::AutomaticallyPlayLegacyAnimations
	bool ___AutomaticallyPlayLegacyAnimations_59;
	// System.Single TriLibCore.AssetLoaderOptions::ResampleFrequency
	float ___ResampleFrequency_60;
	// UnityEngine.WrapMode TriLibCore.AssetLoaderOptions::AnimationWrapMode
	int32_t ___AnimationWrapMode_61;
	// TriLibCore.Mappers.AnimationClipMapper[] TriLibCore.AssetLoaderOptions::AnimationClipMappers
	AnimationClipMapperU5BU5D_t8E00A18562A07FD65A6E731D8BA6FF48D80BBFD8* ___AnimationClipMappers_62;
	// TriLibCore.Mappers.ExternalDataMapper TriLibCore.AssetLoaderOptions::ExternalDataMapper
	ExternalDataMapper_t809726D72207DAF57227F4A5D67B9D01394B760A* ___ExternalDataMapper_63;
	// System.Boolean TriLibCore.AssetLoaderOptions::ShowLoadingWarnings
	bool ___ShowLoadingWarnings_64;
	// System.Boolean TriLibCore.AssetLoaderOptions::CloseStreamAutomatically
	bool ___CloseStreamAutomatically_65;
	// System.Int32 TriLibCore.AssetLoaderOptions::Timeout
	int32_t ___Timeout_66;
	// System.Boolean TriLibCore.AssetLoaderOptions::DestroyOnError
	bool ___DestroyOnError_67;
	// System.Boolean TriLibCore.AssetLoaderOptions::EnsureQuaternionContinuity
	bool ___EnsureQuaternionContinuity_68;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseMaterialKeywords
	bool ___UseMaterialKeywords_69;
	// System.Boolean TriLibCore.AssetLoaderOptions::ForceGCCollectionWhileLoading
	bool ___ForceGCCollectionWhileLoading_70;
	// System.Boolean TriLibCore.AssetLoaderOptions::MergeVertices
	bool ___MergeVertices_71;
	// System.Boolean TriLibCore.AssetLoaderOptions::MarkTexturesNoLongerReadable
	bool ___MarkTexturesNoLongerReadable_72;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseUnityNativeNormalCalculator
	bool ___UseUnityNativeNormalCalculator_73;
	// System.Single TriLibCore.AssetLoaderOptions::GCHelperCollectionInterval
	float ___GCHelperCollectionInterval_74;
	// System.Boolean TriLibCore.AssetLoaderOptions::ApplyGammaCurveToMaterialColors
	bool ___ApplyGammaCurveToMaterialColors_75;
	// System.Boolean TriLibCore.AssetLoaderOptions::LoadTexturesAsSRGB
	bool ___LoadTexturesAsSRGB_76;
	// TriLibCore.Mappers.UserPropertiesMapper TriLibCore.AssetLoaderOptions::UserPropertiesMapper
	UserPropertiesMapper_t8437A569EBEB9E02E364D9951BE31F9601C55714* ___UserPropertiesMapper_77;
	// System.Boolean TriLibCore.AssetLoaderOptions::ApplyTexturesOffsetAndScaling
	bool ___ApplyTexturesOffsetAndScaling_78;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseAutodeskInteractiveMaterials
	bool ___UseAutodeskInteractiveMaterials_79;
	// System.Boolean TriLibCore.AssetLoaderOptions::DiscardUnusedTextures
	bool ___DiscardUnusedTextures_80;
	// TriLibCore.General.PivotPosition TriLibCore.AssetLoaderOptions::PivotPosition
	int32_t ___PivotPosition_81;
	// System.Boolean TriLibCore.AssetLoaderOptions::ForcePowerOfTwoTextures
	bool ___ForcePowerOfTwoTextures_82;
	// System.Int32 TriLibCore.AssetLoaderOptions::MaxTexturesResolution
	int32_t ___MaxTexturesResolution_83;
	// System.Boolean TriLibCore.AssetLoaderOptions::EnableProfiler
	bool ___EnableProfiler_84;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseUnityNativeTextureLoader
	bool ___UseUnityNativeTextureLoader_85;
	// System.Boolean TriLibCore.AssetLoaderOptions::LoadMaterialsProgressively
	bool ___LoadMaterialsProgressively_86;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportCameras
	bool ___ImportCameras_87;
	// System.Boolean TriLibCore.AssetLoaderOptions::ImportLights
	bool ___ImportLights_88;
	// System.Boolean TriLibCore.AssetLoaderOptions::DisableObjectsRenaming
	bool ___DisableObjectsRenaming_89;
	// System.Boolean TriLibCore.AssetLoaderOptions::MergeSingleChild
	bool ___MergeSingleChild_90;
	// System.Boolean TriLibCore.AssetLoaderOptions::SetUnusedTexturePropertiesToNull
	bool ___SetUnusedTexturePropertiesToNull_91;
	// System.Boolean TriLibCore.AssetLoaderOptions::LoadPointClouds
	bool ___LoadPointClouds_92;
	// System.Boolean TriLibCore.AssetLoaderOptions::CreateVerticesAsNativeLists
	bool ___CreateVerticesAsNativeLists_93;
	// System.Boolean TriLibCore.AssetLoaderOptions::CompressMeshes
	bool ___CompressMeshes_94;
	// System.Boolean TriLibCore.AssetLoaderOptions::ExtractEmbeddedData
	bool ___ExtractEmbeddedData_95;
	// System.String TriLibCore.AssetLoaderOptions::EmbeddedDataExtractionPath
	String_t* ___EmbeddedDataExtractionPath_96;
	// System.Collections.Generic.List`1<UnityEngine.Object> TriLibCore.AssetLoaderOptions::FixedAllocations
	List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3* ___FixedAllocations_97;
	// TriLibCore.FileBufferingMode TriLibCore.AssetLoaderOptions::BufferizeFiles
	int32_t ___BufferizeFiles_98;
	// System.Boolean TriLibCore.AssetLoaderOptions::ConvertMaterialTextures
	bool ___ConvertMaterialTextures_99;
	// System.Boolean TriLibCore.AssetLoaderOptions::ConvertMaterialTexturesUsingHalfRes
	bool ___ConvertMaterialTexturesUsingHalfRes_100;
	// System.Boolean TriLibCore.AssetLoaderOptions::DisableTesselation
	bool ___DisableTesselation_101;
	// System.Boolean TriLibCore.AssetLoaderOptions::UseCoroutines
	bool ___UseCoroutines_102;
};

// <Module>

// <Module>

// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyElement>

// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyElement>

// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>

// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>

// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>

// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>
struct List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	List_1U5BU5D_tA62C7ADB73E706494A104910CBF69BD1EF05FD21* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>

// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.Color>

// System.Collections.Generic.List`1<TriLibCore.Interfaces.ITexture>
struct List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ITextureU5BU5D_t4FDE2B940C74FD3BE4608C8EBF531CFA2D51FFFA* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<TriLibCore.Interfaces.ITexture>

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>

// System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>
struct List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.Vector2>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.Vector3>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyElement>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyElement>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyProperty>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyProperty>

// System.IO.BinaryReader

// System.IO.BinaryReader

// System.Globalization.CultureInfo
struct CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_StaticFields
{
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject* ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_DefaultThreadCurrentUICulture_34;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_DefaultThreadCurrentCulture_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t9FA6D82CAFC18769F7515BB51D1C56DAE09381C3* ___shared_by_number_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_tE1603CE612C16451D1E56FF4D4859D4FE4087C28* ___shared_by_name_37;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::s_UserPreferredCultureInfoInAppX
	CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* ___s_UserPreferredCultureInfoInAppX_38;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_39;
};

// System.Globalization.CultureInfo

// TriLibCore.Geometries.Geometry

// TriLibCore.Geometries.Geometry

// System.MarshalByRefObject

// System.MarshalByRefObject

// TriLibCore.MaterialMapperContext

// TriLibCore.MaterialMapperContext

// System.Reflection.MemberInfo

// System.Reflection.MemberInfo

// TriLibCore.Ply.PlyElement

// TriLibCore.Ply.PlyElement

// TriLibCore.Ply.PlyMaterial

// TriLibCore.Ply.PlyMaterial

// TriLibCore.Ply.PlyProcessor

// TriLibCore.Ply.PlyProcessor

// TriLibCore.Ply.PlyProperty

// TriLibCore.Ply.PlyProperty

// TriLibCore.Ply.Program

// TriLibCore.Ply.Program

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449_StaticFields
{
	// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64> TriLibCore.ReaderBase::ProfileStepCallback
	Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2* ___ProfileStepCallback_0;
};

// TriLibCore.ReaderBase

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// TriLibCore.TextureLoadingContext

// TriLibCore.TextureLoadingContext

// System.ValueType

// System.ValueType

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyElement>

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyElement>

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyProperty>

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyProperty>

// Unity.Collections.NativeArray`1<System.Char>

// Unity.Collections.NativeArray`1<System.Char>

// TriLibCore.Utils.BigEndianBinaryReader

// TriLibCore.Utils.BigEndianBinaryReader

// UnityEngine.BoneWeight

// UnityEngine.BoneWeight

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_StaticFields
{
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_actionToActionObjShunt
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___s_actionToActionObjShunt_1;
};

// System.Threading.CancellationToken

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// UnityEngine.Color

// UnityEngine.Color

// System.Double

// System.Double

// System.Int16

// System.Int16

// System.Int32

// System.Int32

// System.Int64

// System.Int64

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// UnityEngine.Matrix4x4

// TriLibCore.Ply.PlyGeometry

// TriLibCore.Ply.PlyGeometry

// TriLibCore.Ply.PlyListProperty

// TriLibCore.Ply.PlyListProperty

// TriLibCore.Ply.Reader.PlyReader

// TriLibCore.Ply.Reader.PlyReader

// TriLibCore.Ply.PlyValue
struct PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41_StaticFields
{
	// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::Unknown
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___Unknown_1;
};

// TriLibCore.Ply.PlyValue

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Quaternion

// System.SByte

// System.SByte

// System.Single

// System.Single

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE_StaticFields
{
	// System.IO.Stream System.IO.Stream::Null
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Null_1;
};

// System.IO.Stream

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7_StaticFields
{
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7* ___Null_1;
};

// System.IO.TextReader

// System.UInt16

// System.UInt16

// System.UInt32

// System.UInt32

// LibTessDotNet.Vec3
struct Vec3_t7AA9166B9BB818308F0807E2023584038C0104F6_StaticFields
{
	// LibTessDotNet.Vec3 LibTessDotNet.Vec3::Zero
	Vec3_t7AA9166B9BB818308F0807E2023584038C0104F6 ___Zero_0;
};

// LibTessDotNet.Vec3

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// UnityEngine.Vector4

// System.Void

// System.Void

// TriLibCore.AssetLoaderContext

// TriLibCore.AssetLoaderContext

// LibTessDotNet.ContourVertex

// LibTessDotNet.ContourVertex

// System.Delegate

// System.Delegate

// System.Exception
struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};

// System.Exception

// TriLibCore.Geometries.InterpolatedVertex

// TriLibCore.Geometries.InterpolatedVertex

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// TriLibCore.Ply.PlyModel

// TriLibCore.Ply.PlyModel

// TriLibCore.Ply.PlyTexture

// TriLibCore.Ply.PlyTexture

// System.RuntimeTypeHandle

// System.RuntimeTypeHandle

// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_StaticFields
{
	// System.IO.StreamReader System.IO.StreamReader::Null
	StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* ___Null_2;
};

// System.IO.StreamReader

// System.MulticastDelegate

// System.MulticastDelegate

// TriLibCore.Ply.PlyRootModel

// TriLibCore.Ply.PlyRootModel

// TriLibCore.Ply.PlyStreamReader

// TriLibCore.Ply.PlyStreamReader

// UnityEngine.ScriptableObject

// UnityEngine.ScriptableObject

// System.Type
struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Type

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>

// TriLibCore.AssetLoaderOptions

// TriLibCore.AssetLoaderOptions
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D  : public RuntimeArray
{
	ALIGN_FIELD (8) Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 m_Items[1];

	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 value)
	{
		m_Items[index] = value;
	}
};
// LibTessDotNet.ContourVertex[]
struct ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF  : public RuntimeArray
{
	ALIGN_FIELD (8) ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F m_Items[1];

	inline ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Data_1), (void*)NULL);
	}
	inline ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Data_1), (void*)NULL);
	}
};
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C  : public RuntimeArray
{
	ALIGN_FIELD (8) int32_t m_Items[1];

	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// TriLibCore.Interfaces.IMaterial[]
struct IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// TriLibCore.Interfaces.IGeometryGroup[]
struct IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// TriLibCore.Ply.PlyValue[]
struct PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382  : public RuntimeArray
{
	ALIGN_FIELD (8) PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 m_Items[1];

	inline PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 value)
	{
		m_Items[index] = value;
	}
};


// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::TryGetValue(TKey,TValue&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m9A4D60A27A7CAA492BE4A9AC1EB250802FDDF5A4_gshared (Dictionary_2_t4A0148843FDD82FE00634A604A772FC4EE3A0379* __this, int64_t ___0_key, RuntimeObject** ___1_value, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_gshared (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m6E4700E09E9816E4C0C88C90C5BB8FAF312B8481_gshared (Dictionary_2_t4A0148843FDD82FE00634A604A772FC4EE3A0379* __this, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Extensions.DictionaryExtensions::TryGetValueSafe<System.Object,System.Object>(System.Collections.Generic.IDictionary`2<TKey,TValue>,TKey,TValue&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DictionaryExtensions_TryGetValueSafe_TisRuntimeObject_TisRuntimeObject_m31FA8212F5094FA954F0351646B0B012D9736B0B_gshared (RuntimeObject* ___0_dictionary, RuntimeObject* ___1_key, RuntimeObject** ___2_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7E6E0C4AE37ACBC1E9BDA9E56A8A7D8ACED3FD33_gshared (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, int32_t ___0_capacity, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mDBC3F8F2846CD821DA096BECD6300438E2409BC3_gshared (List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF* __this, int32_t ___0_capacity, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m27475592735C318973899086F95036A18B6D7E39_gshared (List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B* __this, int32_t ___0_capacity, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::Add(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_mF6ABB6369C9A6394AE57424BE839FE87809B4102_gshared (Dictionary_2_t4A0148843FDD82FE00634A604A772FC4EE3A0379* __this, int64_t ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Count_mF62FCAE02B490CD3CA263E1578D1F37A10B4491A_gshared (Dictionary_2_t4A0148843FDD82FE00634A604A772FC4EE3A0379* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::get_Values()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ValueCollection_tE8095E528D5C491DA5BF2C8694B3FEF0733A8BEB* Dictionary_2_get_Values_m08D4959355E350045D1F2B2673B420FB2EA9A04B_gshared (Dictionary_2_t4A0148843FDD82FE00634A604A772FC4EE3A0379* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t101EECDB54BD1F67FD91D0D649333AC62D79665D ValueCollection_GetEnumerator_m7698A37B76C5FA07C5C51C43A040FB1B832757E0_gshared (ValueCollection_tE8095E528D5C491DA5BF2C8694B3FEF0733A8BEB* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m88D8DFB7CF8747F1F8EBA722C32E87978FAA3041_gshared (Enumerator_t101EECDB54BD1F67FD91D0D649333AC62D79665D* __this, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_mE396DC57C362877713861B0AD2C88728648A8364_gshared_inline (Enumerator_t101EECDB54BD1F67FD91D0D649333AC62D79665D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m76CBBC3E2F0583F5AD30CE592CEA1225C06A0428_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___0_capacity, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122_gshared (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, int32_t ___0_capacity, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_gshared_inline (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_item, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mCBADFB9B05D6004606931CA8EF7A85A4618D30A2_gshared (Enumerator_t101EECDB54BD1F67FD91D0D649333AC62D79665D* __this, const RuntimeMethod* method) ;
// T TriLibCore.Utils.ListUtils::FixIndex<UnityEngine.Vector3>(System.Int32,System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_gshared (int32_t ___0_index, RuntimeObject* ___1_list, const RuntimeMethod* method) ;
// T TriLibCore.Utils.ListUtils::FixIndex<UnityEngine.Color>(System.Int32,System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447_gshared (int32_t ___0_index, RuntimeObject* ___1_list, const RuntimeMethod* method) ;
// T TriLibCore.Utils.ListUtils::FixIndex<UnityEngine.Vector2>(System.Int32,System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E_gshared (int32_t ___0_index, RuntimeObject* ___1_list, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_gshared_inline (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, const RuntimeMethod* method) ;
// System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafePtr<System.Char>(Unity.Collections.NativeArray`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void* NativeArrayUnsafeUtility_GetUnsafePtr_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_mDDC6F7A9E98335D0828894600921FCF3A934DB0A_gshared (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A ___0_nativeArray, const RuntimeMethod* method) ;
// System.Void Unity.Collections.NativeArray`1<System.Char>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeArray_1__ctor_m01092412782EBC32088D2077868A10700AC7FE61_gshared (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A* __this, int32_t ___0_length, int32_t ___1_allocator, int32_t ___2_options, const RuntimeMethod* method) ;
// System.Void Unity.Collections.NativeArray`1<System.Char>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeArray_1_Dispose_m11184C5FB1A1F3809B982476408F08F599F377F0_gshared (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A* __this, const RuntimeMethod* method) ;

// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>::TryGetValue(TKey,TValue&)
inline bool Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5 (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* __this, int64_t ___0_key, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362** ___1_value, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680*, int64_t, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362**, const RuntimeMethod*))Dictionary_2_TryGetValue_m9A4D60A27A7CAA492BE4A9AC1EB250802FDDF5A4_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>::get_Count()
inline int32_t List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_inline (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// T System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>::get_Item(System.Int32)
inline List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641 (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* __this, int32_t ___0_index, const RuntimeMethod* method)
{
	return ((  List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* (*) (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___0_index, method);
}
// T System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>::get_Item(System.Int32)
inline PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36 (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, int32_t ___0_index, const RuntimeMethod* method)
{
	return ((  PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 (*) (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*, int32_t, const RuntimeMethod*))List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_gshared)(__this, ___0_index, method);
}
// System.Int32 TriLibCore.Ply.PlyValue::GetIntValue(TriLibCore.Ply.PlyValue,TriLibCore.Ply.PlyPropertyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E (PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_value, int32_t ___1_propertyType, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Ply.PlyValue::GetIntValue(TriLibCore.Ply.PlyValue,TriLibCore.Ply.PlyProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyValue_GetIntValue_mC5D8E3A04354E7AD052C4E0DA2DDC26DADC58ABD (PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_value, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___1_property, const RuntimeMethod* method) ;
// System.Single TriLibCore.Ply.PlyValue::GetFloatValue(TriLibCore.Ply.PlyValue,TriLibCore.Ply.PlyProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyValue_GetFloatValue_mCE0211B08C054DE0848AEB2EF5E728F6D28E9830 (PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_value, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___1_property, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>::.ctor()
inline void Dictionary_2__ctor_m9E31627A68563218A84912174B1A3CCFF5D9D3DB (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680*, const RuntimeMethod*))Dictionary_2__ctor_m6E4700E09E9816E4C0C88C90C5BB8FAF312B8481_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Geometries.Geometry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Geometry__ctor_m0F5EBDDAEEB9ED6F66946F16F31BAB22BE44D193 (Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyProperty::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyProperty__ctor_mB9404ABEA64DE21D9C16B8FCF8FA95FEA5C8221F (PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* __this, const RuntimeMethod* method) ;
// UnityEngine.Color TriLibCore.Ply.PlyMaterial::GetGenericColorValue(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F PlyMaterial_GetGenericColorValue_mDAB3E44868BCA99887DF188037E7A7E9A48E36E4 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_materialProperty, const RuntimeMethod* method) ;
// System.Single TriLibCore.Ply.PlyMaterial::GetGenericFloatValue(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyMaterial_GetGenericFloatValue_m5BC4612B3D15C6C0ABE4E7B0A14CE34CA6EAF21F (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_materialProperty, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Ply.PlyMaterial::HasProperty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyMaterial_HasProperty_mB36FB6DBB9CF41BD74A5C793E011AF62FA370EF4 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) ;
// System.Single TriLibCore.Ply.PlyMaterial::GetFloatValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyMaterial_GetFloatValue_mB021B41CE867CAC9BC3A68FD86F956FFBC9566D3 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) ;
// System.String TriLibCore.Ply.PlyMaterial::GetGenericPropertyName(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyMaterial_GetGenericPropertyName_m41CB8C3D4FBAC86A36411C2405D56BA48F3819ED (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_genericMaterialProperty, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Ply.PlyMaterial::GetIntValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyMaterial_GetIntValue_m7D4FA27F9231A957F97E091EAB1D9A7C546D88BD (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) ;
// System.String TriLibCore.Ply.PlyMaterial::GetStringValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyMaterial_GetStringValue_mB6CCF7EB09045885C1FFECC7C84CBB6CEDE49F8D (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) ;
// UnityEngine.Vector3 TriLibCore.Ply.PlyMaterial::GetVector3Value(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 PlyMaterial_GetVector3Value_m2A6EE04EE31489DDD7A1003E00BF79C86A81D504 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) ;
// UnityEngine.Vector4 TriLibCore.Ply.PlyMaterial::GetVector4Value(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 PlyMaterial_GetVector4Value_m1AC8F1CBBF1703484EEB7833978B1CFC20F4FAB3 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) ;
// UnityEngine.Color TriLibCore.Ply.PlyMaterial::GetColorValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F PlyMaterial_GetColorValue_m8B4EA23BBCF1DF22DDFA1A4D1DC3E389A317151D (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mB50217951591A045844C61E7FF31EEE3FEF16737_inline (const RuntimeMethod* method) ;
// TriLibCore.Interfaces.ITexture TriLibCore.Ply.PlyMaterial::GetTextureValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyMaterial_GetTextureValue_mCCE6C1448D7F8075D1560B44FFC67773456373BD (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Extensions.DictionaryExtensions::TryGetValueSafe<System.String,System.Object>(System.Collections.Generic.IDictionary`2<TKey,TValue>,TKey,TValue&)
inline bool DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9 (RuntimeObject* ___0_dictionary, String_t* ___1_key, RuntimeObject** ___2_value, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject*, String_t*, RuntimeObject**, const RuntimeMethod*))DictionaryExtensions_TryGetValueSafe_TisRuntimeObject_TisRuntimeObject_m31FA8212F5094FA954F0351646B0B012D9736B0B_gshared)(___0_dictionary, ___1_key, ___2_value, method);
}
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6 (const RuntimeMethod* method) ;
// System.Single System.Convert::ToSingle(System.Object,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Convert_ToSingle_mEC65F60A081FBBB8ACBCD8747183FEDDAEC034C9 (RuntimeObject* ___0_value, RuntimeObject* ___1_provider, const RuntimeMethod* method) ;
// System.Int32 System.Convert::ToInt32(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Convert_ToInt32_m9FEA65DB96264479B5268014F10754787382D297 (RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.String System.Convert::ToString(System.Object,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Convert_ToString_m833ABF9C901B707B28FB10DEBFCC511A87E6C827 (RuntimeObject* ___0_value, RuntimeObject* ___1_provider, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline (const RuntimeMethod* method) ;
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Vector4_get_zero_m3D61F5FA9483CD9C08977D9D8852FB448B4CE6D1_inline (const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor()
inline void Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9 (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.String TriLibCore.Ply.PlyModel::get_Name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PlyModel_get_Name_mF0BDCE3B91DCB49F6E648F411F06E052FF253F89_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
inline void List_1__ctor_m7E6E0C4AE37ACBC1E9BDA9E56A8A7D8ACED3FD33 (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, int32_t ___0_capacity, const RuntimeMethod* method)
{
	((  void (*) (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*, int32_t, const RuntimeMethod*))List_1__ctor_m7E6E0C4AE37ACBC1E9BDA9E56A8A7D8ACED3FD33_gshared)(__this, ___0_capacity, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor(System.Int32)
inline void List_1__ctor_mDBC3F8F2846CD821DA096BECD6300438E2409BC3 (List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF* __this, int32_t ___0_capacity, const RuntimeMethod* method)
{
	((  void (*) (List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF*, int32_t, const RuntimeMethod*))List_1__ctor_mDBC3F8F2846CD821DA096BECD6300438E2409BC3_gshared)(__this, ___0_capacity, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
inline void List_1__ctor_m27475592735C318973899086F95036A18B6D7E39 (List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B* __this, int32_t ___0_capacity, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B*, int32_t, const RuntimeMethod*))List_1__ctor_m27475592735C318973899086F95036A18B6D7E39_gshared)(__this, ___0_capacity, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyElement>::.ctor()
inline void Dictionary_2__ctor_m4795CFCFA1CA4807CD85E1818CEC2A7EBC2E336C (Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC*, const RuntimeMethod*))Dictionary_2__ctor_m6E4700E09E9816E4C0C88C90C5BB8FAF312B8481_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>::.ctor()
inline void List_1__ctor_mEEA3C6B55707EC6755329113F4C364960AA66CEF (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<TriLibCore.Interfaces.ITexture>::.ctor()
inline void List_1__ctor_m71DCE2DD53C6EFD6DF99B2592A347DFD0F9106C3 (List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void TriLibCore.Ply.PlyStreamReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyStreamReader__ctor_mBCDE6CC4B0352A1622093F74DBC3FE0CFDC678F0 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Ply.PlyStreamReader::ReadToken(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, bool ___0_required, bool ___1_ignoreSpaces, const RuntimeMethod* method) ;
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F (Exception_t* __this, String_t* ___0_message, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Ply.PlyStreamReader::ToInt32NoValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyStreamReader_ToInt32NoValue_m3D62B7AC2B68E0C65837ED4B738D26A02C897086 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyElement::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyElement__ctor_mCA04905EABE9EB079C06C05A9EBB680CF4A4DBE3 (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyElement>::Add(TKey,TValue)
inline void Dictionary_2_Add_m7E3EF4351105771F2D6EB167BE29E7868CFC84F0 (Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* __this, int64_t ___0_key, PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC*, int64_t, PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8*, const RuntimeMethod*))Dictionary_2_Add_mF6ABB6369C9A6394AE57424BE839FE87809B4102_gshared)(__this, ___0_key, ___1_value, method);
}
// TriLibCore.Ply.PlyPropertyType TriLibCore.Ply.PlyProcessor::GetPropertyType(TriLibCore.Ply.PlyStreamReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyProcessor_GetPropertyType_mF17237DE1F943422F7DBEB3435F43FD29CFB2A46 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* ___0_plyStreamReader, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyListProperty::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyListProperty__ctor_m913A103F9505DB2EC35D68434C7B1676D0F26196 (PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* __this, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>::get_Count()
inline int32_t Dictionary_2_get_Count_m270BB8BF6E2728FF1BE69D236B3C1BB5E5D60BFD (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680*, const RuntimeMethod*))Dictionary_2_get_Count_mF62FCAE02B490CD3CA263E1578D1F37A10B4491A_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>::Add(TKey,TValue)
inline void Dictionary_2_Add_m13BA45BD072C072ECDC20ED17980959D50DCDEBE (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* __this, int64_t ___0_key, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680*, int64_t, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362*, const RuntimeMethod*))Dictionary_2_Add_mF6ABB6369C9A6394AE57424BE839FE87809B4102_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Void TriLibCore.Ply.PlyTexture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture__ctor_m038AAFCCC1A0BFA24B9DB741380AA1D0CDE29AFD (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) ;
// System.String TriLibCore.Ply.PlyStreamReader::GetTokenAsString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyStreamReader_GetTokenAsString_m7E04224D04CDC8D8F4791995529AB17E0CEEDCAA (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyTexture::set_Filename(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyTexture_set_Filename_m7FA26BB14F1E9F04652E891D67858519F9040BD8_inline (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<TriLibCore.Interfaces.ITexture>::Add(T)
inline void List_1_Add_m096BE2FF8568855341E87C1DE6291916F89A8203_inline (List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31* __this, RuntimeObject* ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31*, RuntimeObject*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___0_item, method);
}
// System.Boolean System.IO.StreamReader::get_EndOfStream()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StreamReader_get_EndOfStream_mAE054431BF21158178EAA2A6872F14A9ED6A3C3E (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::UpdateLoadingPercentage(System.Single,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, float ___0_value, int32_t ___1_step, float ___2_maxValue, const RuntimeMethod* method) ;
// System.Void System.IO.BinaryReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryReader__ctor_m898732FE0DBEDD480B24F6DE45A9AC696E44CC0F (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_input, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Ply.PlyStreamReader::get_Position()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int64_t PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B_inline (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Utils.BigEndianBinaryReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BigEndianBinaryReader__ctor_m9E745E5732B70DC31E50C8C6B8DE7DB5956D0EBA (BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyElement>::get_Values()
inline ValueCollection_t16B888ACAE62FB5B6EB90AB83B113259C434EC38* Dictionary_2_get_Values_mD47C7DCD10B474F8DB55896C6B5F276ACA9E2BC1 (Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* __this, const RuntimeMethod* method)
{
	return ((  ValueCollection_t16B888ACAE62FB5B6EB90AB83B113259C434EC38* (*) (Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC*, const RuntimeMethod*))Dictionary_2_get_Values_m08D4959355E350045D1F2B2673B420FB2EA9A04B_gshared)(__this, method);
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyElement>::GetEnumerator()
inline Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901 ValueCollection_GetEnumerator_mD927D6AC4BE78DAED8A83775F0D1FEF91A756B4D (ValueCollection_t16B888ACAE62FB5B6EB90AB83B113259C434EC38* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901 (*) (ValueCollection_t16B888ACAE62FB5B6EB90AB83B113259C434EC38*, const RuntimeMethod*))ValueCollection_GetEnumerator_m7698A37B76C5FA07C5C51C43A040FB1B832757E0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyElement>::Dispose()
inline void Enumerator_Dispose_m16443C4F8A0449761F7E9532DA13EE62BC66A959 (Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901*, const RuntimeMethod*))Enumerator_Dispose_m88D8DFB7CF8747F1F8EBA722C32E87978FAA3041_gshared)(__this, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyElement>::get_Current()
inline PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* Enumerator_get_Current_m16B76A6F6611FB82F01BB3EA7B36F6BE9AC3B558_inline (Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901* __this, const RuntimeMethod* method)
{
	return ((  PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* (*) (Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901*, const RuntimeMethod*))Enumerator_get_Current_mE396DC57C362877713861B0AD2C88728648A8364_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>::.ctor(System.Int32)
inline void List_1__ctor_m179CF1BDD503F830F79CF1A81033083C1BB19E05 (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* __this, int32_t ___0_capacity, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5*, int32_t, const RuntimeMethod*))List_1__ctor_m76CBBC3E2F0583F5AD30CE592CEA1225C06A0428_gshared)(__this, ___0_capacity, method);
}
// System.Void System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>::.ctor(System.Int32)
inline void List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122 (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, int32_t ___0_capacity, const RuntimeMethod* method)
{
	((  void (*) (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*, int32_t, const RuntimeMethod*))List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122_gshared)(__this, ___0_capacity, method);
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>::get_Values()
inline ValueCollection_t17A15458500B928C18C734F4964D798B1C114EE9* Dictionary_2_get_Values_m66C7C3835140D396B2941C5FF726A8435575E421 (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* __this, const RuntimeMethod* method)
{
	return ((  ValueCollection_t17A15458500B928C18C734F4964D798B1C114EE9* (*) (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680*, const RuntimeMethod*))Dictionary_2_get_Values_m08D4959355E350045D1F2B2673B420FB2EA9A04B_gshared)(__this, method);
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,TriLibCore.Ply.PlyProperty>::GetEnumerator()
inline Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8 ValueCollection_GetEnumerator_m7A2633FD76AC8804D68EEB036B996CAF5FDC7754 (ValueCollection_t17A15458500B928C18C734F4964D798B1C114EE9* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8 (*) (ValueCollection_t17A15458500B928C18C734F4964D798B1C114EE9*, const RuntimeMethod*))ValueCollection_GetEnumerator_m7698A37B76C5FA07C5C51C43A040FB1B832757E0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyProperty>::Dispose()
inline void Enumerator_Dispose_mFB43E311F648679BE046A19C9351234B9C43A1CF (Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8*, const RuntimeMethod*))Enumerator_Dispose_m88D8DFB7CF8747F1F8EBA722C32E87978FAA3041_gshared)(__this, method);
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyProperty>::get_Current()
inline PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* Enumerator_get_Current_mA527966D30259A4C4C030B11F4DFEE3029C1076F_inline (Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8* __this, const RuntimeMethod* method)
{
	return ((  PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* (*) (Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8*, const RuntimeMethod*))Enumerator_get_Current_mE396DC57C362877713861B0AD2C88728648A8364_gshared_inline)(__this, method);
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyProcessor::ReadElementData(TriLibCore.Ply.PlyProperty,TriLibCore.Ply.PlyPropertyType,System.Boolean,System.IO.BinaryReader,System.Boolean,TriLibCore.Ply.PlyStreamReader,TriLibCore.Utils.BigEndianBinaryReader,System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyProcessor_ReadElementData_m295BBB280885FD705DF14E08D8FE375B03CB984B (PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___0_property, int32_t ___1_propertyType, bool ___2_littleEndian, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___3_binaryReader, bool ___4_bigEndian, PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* ___5_plyStreamReader, BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* ___6_bigEndianBinaryReader, List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* ___7_lists, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>::Add(T)
inline void List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_inline (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*, PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41, const RuntimeMethod*))List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_gshared_inline)(__this, ___0_item, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyProperty>::MoveNext()
inline bool Enumerator_MoveNext_m355F0050D35C5CECAB616FAD6BD09BCDF75ED2AD (Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8*, const RuntimeMethod*))Enumerator_MoveNext_mCBADFB9B05D6004606931CA8EF7A85A4618D30A2_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>::Add(T)
inline void List_1_Add_m7C99F2FA69D684BD5B7E22B8A115DA258EA04CB2_inline (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* __this, List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5*, List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___0_item, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int64,TriLibCore.Ply.PlyElement>::MoveNext()
inline bool Enumerator_MoveNext_m3A6AF59F5158004E0E8F5D30912F0B3D578BAD20 (Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901*, const RuntimeMethod*))Enumerator_MoveNext_mCBADFB9B05D6004606931CA8EF7A85A4618D30A2_gshared)(__this, method);
}
// TriLibCore.AssetLoaderContext TriLibCore.ReaderBase::get_AssetLoaderContext()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyElement>::TryGetValue(TKey,TValue&)
inline bool Dictionary_2_TryGetValue_m181BAACEB7905BC572C38343BF7DDA866E9415FD (Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* __this, int64_t ___0_key, PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8** ___1_value, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC*, int64_t, PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8**, const RuntimeMethod*))Dictionary_2_TryGetValue_m9A4D60A27A7CAA492BE4A9AC1EB250802FDDF5A4_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Void TriLibCore.Ply.PlyMaterial::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyMaterial__ctor_m9669AF4D43AE12DEEBD8F46D99D869AC68470623 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) ;
// System.Single TriLibCore.Ply.PlyElement::GetPropertyFloatValue(System.Int64,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713 (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, int64_t ___0_propertyName, int32_t ___1_elementIndex, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyMaterial::AddProperty(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyMaterial_AddProperty_m4FE8AE6161D498A95286AC015BD2E1B2EC41E3B4 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, RuntimeObject* ___1_propertyValue, bool ___2_isTexture, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyProperty TriLibCore.Ply.PlyElement::GetProperty(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280 (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, int64_t ___0_propertyName, const RuntimeMethod* method) ;
// System.Single TriLibCore.Ply.PlyElement::GetPropertyFloatValue(TriLibCore.Ply.PlyProperty,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___0_property, int32_t ___1_elementIndex, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyMaterial::set_Index(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyMaterial_set_Index_mEEC9E37FB853F46E7A19C7A52BE8EACD41464726_inline (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, float ___3_a, const RuntimeMethod* method) ;
// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute> TriLibCore.Geometries.FlexibleVertexDataUtils::BuildVertexAttributesDictionary(TriLibCore.AssetLoaderContext,System.Int32&,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* FlexibleVertexDataUtils_BuildVertexAttributesDictionary_m38474C4D34ABEA85320910DAC96E8A8016B46888 (AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___0_assetLoaderContext, int32_t* ___1_bytesCount, bool ___2_hasPosition, bool ___3_hasNormal, bool ___4_hasTangent, bool ___5_hasColor, bool ___6_hasUV0, bool ___7_hasUV1, bool ___8_hasUV2, bool ___9_hasUV3, bool ___10_hasBoneWeight, bool ___11_useHalfPrecision, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Geometries.FlexibleVertexDataUtils::BuildStreamGeometryGroup(System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FlexibleVertexDataUtils_BuildStreamGeometryGroup_m06F110C1559744234BEB5984BB8F228044B7F047 (HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* ___0_vertexAttributes, int32_t ___1_bytesCount, bool ___2_useHalfPrecision, const RuntimeMethod* method) ;
// T TriLibCore.Utils.ListUtils::FixIndex<UnityEngine.Vector3>(System.Int32,System.Collections.Generic.IList`1<T>)
inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE (int32_t ___0_index, RuntimeObject* ___1_list, const RuntimeMethod* method)
{
	return ((  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 (*) (int32_t, RuntimeObject*, const RuntimeMethod*))ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_gshared)(___0_index, ___1_list, method);
}
// T TriLibCore.Utils.ListUtils::FixIndex<UnityEngine.Color>(System.Int32,System.Collections.Generic.IList`1<T>)
inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447 (int32_t ___0_index, RuntimeObject* ___1_list, const RuntimeMethod* method)
{
	return ((  Color_tD001788D726C3A7F1379BEED0260B9591F440C1F (*) (int32_t, RuntimeObject*, const RuntimeMethod*))ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447_gshared)(___0_index, ___1_list, method);
}
// T TriLibCore.Utils.ListUtils::FixIndex<UnityEngine.Vector2>(System.Int32,System.Collections.Generic.IList`1<T>)
inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E (int32_t ___0_index, RuntimeObject* ___1_list, const RuntimeMethod* method)
{
	return ((  Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 (*) (int32_t, RuntimeObject*, const RuntimeMethod*))ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E_gshared)(___0_index, ___1_list, method);
}
// System.Void TriLibCore.Geometries.FlexibleVertexDataUtils::BuildAndAddFlexibleVertexData(TriLibCore.Interfaces.IGeometryGroup,TriLibCore.AssetLoaderContext,System.Int32,TriLibCore.Geometries.Geometry,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector4,UnityEngine.Color,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.BoneWeight)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlexibleVertexDataUtils_BuildAndAddFlexibleVertexData_mC35367A0F41EE78D0D41CB73484C229156CC532F (RuntimeObject* ___0_geometryGroup, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, int32_t ___2_vertexIndex, Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147* ___3_geometry, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___4_position, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___5_normal, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___6_tangent, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___7_color, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___8_uv0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___9_uv1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___10_uv2, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___11_uv3, BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F ___12_boneWeight, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Ply.PlyElement::GetListIndex(TriLibCore.Ply.PlyListProperty,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyElement_GetListIndex_m2725B97E4D677B68667107956C9D7E8A2C2565AF (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* ___0_property, int32_t ___1_elementIndex, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue> TriLibCore.Ply.PlyValue::GetListValue(System.Int32,System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* PlyValue_GetListValue_m0917530BCED9A22F01949BB493407DCC83C71C2F (int32_t ___0_index, List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* ___1_lists, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Ply.PlyElement::GetPropertyIntValue(TriLibCore.Ply.PlyProperty,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyElement_GetPropertyIntValue_m350C36F63BFB7DB71BBDE9219AE28BC400EAC124 (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___0_property, int32_t ___1_elementIndex, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>::get_Count()
inline int32_t List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_inline (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*, const RuntimeMethod*))List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_gshared_inline)(__this, method);
}
// System.Void TriLibCore.Ply.PlyProcessor::AddVertex(System.Int32,TriLibCore.Ply.PlyGeometry,System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>,System.Int32,TriLibCore.Ply.PlyProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556 (PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F* __this, int32_t ___0_vertexIndex, PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* ___1_geometry, List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* ___2_texCoord, int32_t ___3_texCoordIndex, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___4_texCoordProp, const RuntimeMethod* method) ;
// System.Void LibTessDotNet.Vec3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vec3__ctor_m937D1DD303AF7D61662A455E5F208BDB81106B7C (Vec3_t7AA9166B9BB818308F0807E2023584038C0104F6* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) ;
// TriLibCore.Geometries.IVertexData TriLibCore.Ply.PlyProcessor::BuildVertexData(TriLibCore.Ply.PlyGeometry,TriLibCore.Interfaces.IGeometryGroup,System.Int32,System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>,TriLibCore.Ply.PlyListProperty,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyProcessor_BuildVertexData_mB82746725B313510493FDBBBCD2A960E434566B9 (PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F* __this, PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* ___0_triGeometry, RuntimeObject* ___1_geometryGroup, int32_t ___2_vertexIndex, List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* ___3_texCoord, PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* ___4_texCoordProp, int32_t ___5_texCoordIndex, const RuntimeMethod* method) ;
// System.Void LibTessDotNet.ContourVertex::.ctor(LibTessDotNet.Vec3,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContourVertex__ctor_mB49AE90A9B85CA37EC7EE928BC9EE585D35C8C03 (ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F* __this, Vec3_t7AA9166B9BB818308F0807E2023584038C0104F6 ___0_position, RuntimeObject* ___1_data, const RuntimeMethod* method) ;
// System.Void LibTessDotNet.Helpers::Tesselate(System.Collections.Generic.IList`1<LibTessDotNet.ContourVertex>,TriLibCore.AssetLoaderContext,TriLibCore.Geometries.Geometry,TriLibCore.Interfaces.IGeometryGroup,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Helpers_Tesselate_m52BC0268EF5AEEEECF07746E8B25E59026DAC804 (RuntimeObject* ___0_contourVertices, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147* ___2_geometry, RuntimeObject* ___3_geometryGroup, bool ___4_counterClockwise, const RuntimeMethod* method) ;
// System.Void System.IO.TextReader::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextReader_Dispose_mDCB332EFA06970A9CC7EC4596FCC5220B9512616 (TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyRootModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyRootModel__ctor_m70F96F0795DD6B57ADAEE28C419A56033958CEED (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyModel::set_Visibility(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_Visibility_mB2F9D43D393D00418E2B4A999974A1E18512D0A5_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, bool ___0_value, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline (const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyModel::set_LocalScale(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_LocalScale_m8055103EC8ADECCC5D3C535C098B33AA2E0275A8_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline (const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyModel::set_LocalRotation(UnityEngine.Quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_LocalRotation_mA70C0956246A032C3C1C7EF5DE31A303ACE92BCB_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyModel::set_GeometryGroup(TriLibCore.Interfaces.IGeometryGroup)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_GeometryGroup_mC40A2CC9BD75EBA9305AA0FC6E9F1FFD51FDA055_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyRootModel::set_AllMaterials(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyRootModel_set_AllMaterials_m30733607EFAAACE9110C2452A299D1D47D799C53_inline (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyRootModel::set_AllTextures(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyRootModel_set_AllTextures_m7646524ECF24096720D3C48A840F554283D6212E_inline (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyRootModel::set_AllGeometryGroups(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyRootModel_set_AllGeometryGroups_m44BF8C7B4B75BCAA1C25B38D31F5FB90019FD3F1_inline (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyModel::set_MaterialIndices(System.Collections.Generic.IList`1<System.Int32>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_MaterialIndices_m87FB1DB52736B0D10EBF7CA05C04C3B1A321F670_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Geometries.InterpolatedVertex::.ctor(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InterpolatedVertex__ctor_mA3F5FBB1EACDDB0F2B33E893A45FB89113C6787C (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, const RuntimeMethod* method) ;
// System.Void TriLibCore.Geometries.InterpolatedVertex::SetPosition(UnityEngine.Vector3,TriLibCore.Interfaces.IGeometryGroup)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InterpolatedVertex_SetPosition_m51A3B70131151C51E738A6EDCD4E5EBEF7C80E2C_inline (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, RuntimeObject* ___1_geometryGroup, const RuntimeMethod* method) ;
// System.Void TriLibCore.Geometries.InterpolatedVertex::SetNormal(UnityEngine.Vector3,TriLibCore.Interfaces.IGeometryGroup)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InterpolatedVertex_SetNormal_m48EE21D1CFF1D80F3EAAD2689ABEE84102882ADC_inline (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, RuntimeObject* ___1_geometryGroup, const RuntimeMethod* method) ;
// System.Void TriLibCore.Geometries.InterpolatedVertex::SetColor(UnityEngine.Color,TriLibCore.Interfaces.IGeometryGroup)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InterpolatedVertex_SetColor_m01F89204D2B248BE23E94620AE338F73D8C90AE5_inline (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, RuntimeObject* ___1_geometryGroup, const RuntimeMethod* method) ;
// System.Void TriLibCore.Geometries.InterpolatedVertex::SetUV1(UnityEngine.Vector2,TriLibCore.Interfaces.IGeometryGroup)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InterpolatedVertex_SetUV1_mA7C291101768A0EE2734CD2589D815DCC2454551_inline (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, RuntimeObject* ___1_geometryGroup, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToSByte()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToSByte_mE0CBD4AA736167D961A334A686A4851A2B107FD8 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.SByte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m97A5AB6B334327E421C58927E78C773B6D809B53 (int8_t ___0_other, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToByte()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToByte_mC8D36F8FD97FEBF03092492FCCFC77F27C669B31 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m9E7C391E8C4BAEF13B69CC7B0CEC87F250BD46A6 (uint8_t ___0_other, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToInt16()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToInt16_m15E305684870C3A4B737A95DA4212701C45830F9 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.Int16 TriLibCore.Utils.BigEndianBinaryReader::ReadInt16()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t BigEndianBinaryReader_ReadInt16_m2646AC659077E17786022320078FBBDB8DCBC707 (BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* __this, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m78378AA8D6DDCEC09907E0927A06ED49418D4635 (int16_t ___0_other, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToUInt16()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToUInt16_m5E5F5DDAFA6CBF735A852FED4B1712FC914301AD (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m8B2B7375CC217E6E3345C812C647F7BB51FC0DC6 (uint16_t ___0_other, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToInt32()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToInt32_m9F2C9B5DB1E8C1E858934D60E8365C24B5F96E05 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_mCBC1637AE004264B6DC685D489B3DABF7C40CE8B (int32_t ___0_other, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToUInt32()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToUInt32_mFE918D4E08D1EBBB095A75F4A6F4B0B0FF31E7AA (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.UInt32 TriLibCore.Utils.BigEndianBinaryReader::ReadUInt32()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t BigEndianBinaryReader_ReadUInt32_mE3E235D63AC4B3633B15C0C70AAFA771DB825823 (BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* __this, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m29A5DE8027F986F1251009072C203966CA6B2CE4 (uint32_t ___0_other, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToSingle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToSingle_m90ED33B30955513C54C102F1CF12293B87ED840D (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.Single TriLibCore.Utils.BigEndianBinaryReader::ReadSingle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BigEndianBinaryReader_ReadSingle_m5BE3F923D636289CE8D32BD988A36AF1D507BC8E (BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* __this, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_mABA4998D1201D014DFC2948ED728D84579823AB2 (float ___0_other, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToDouble()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToDouble_m24378FB1F25B30F14E4F1C4EDE8A61FE4AB2C467 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.Double TriLibCore.Utils.BigEndianBinaryReader::ReadDouble()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double BigEndianBinaryReader_ReadDouble_m807A1A1D9EE248DD7637AB40276E88CADDCAB32F (BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* __this, const RuntimeMethod* method) ;
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m1A3B9DCC94933A941A05360494BF9C1D6A1F5FE0 (double ___0_other, const RuntimeMethod* method) ;
// System.Single TriLibCore.Ply.PlyValue::GetFloatValue(TriLibCore.Ply.PlyValue,TriLibCore.Ply.PlyPropertyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyValue_GetFloatValue_m03C511897C1577C7BAE5C96F005AB99B7EEEE57D (PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_value, int32_t ___1_propertyType, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel__ctor_mEBFCADB0E140C2324D5944AD85D4BED4CE3BC725 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyStreamReader::CopyCharStreamToString(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyStreamReader_CopyCharStreamToString_mDEACAF07AF4EE3BA8B0378893465CF46BBEC3434 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, String_t* ___0_s, int32_t ___1_maxChars, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline (int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) ;
// System.Int32 System.Runtime.CompilerServices.RuntimeHelpers::get_OffsetToStringData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RuntimeHelpers_get_OffsetToStringData_m90A5D27EF88BE9432BF7093B7D7E7A0ACB0A8FBD (const RuntimeMethod* method) ;
// System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafePtr<System.Char>(Unity.Collections.NativeArray`1<T>)
inline void* NativeArrayUnsafeUtility_GetUnsafePtr_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_mDDC6F7A9E98335D0828894600921FCF3A934DB0A (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A ___0_nativeArray, const RuntimeMethod* method)
{
	return ((  void* (*) (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A, const RuntimeMethod*))NativeArrayUnsafeUtility_GetUnsafePtr_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_mDDC6F7A9E98335D0828894600921FCF3A934DB0A_gshared)(___0_nativeArray, method);
}
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemSet(System.Void*,System.Byte,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_MemSet_m4CD74CD43260EB2962A46F57E0D93DD5C332FC2B (void* ___0_destination, uint8_t ___1_value, int64_t ___2_size, const RuntimeMethod* method) ;
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemCpy(System.Void*,System.Void*,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177 (void* ___0_destination, void* ___1_source, int64_t ___2_size, const RuntimeMethod* method) ;
// System.String System.String::CreateString(System.Char,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B (String_t* __this, Il2CppChar ___0_c, int32_t ___1_count, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyStreamReader::set_Position(System.Int64)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyStreamReader_set_Position_m763DA88CC440E9EAA48B2638BC5D5E717BB8C3F4_inline (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, int64_t ___0_value, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Ply.PlyStreamReader::get_Line()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyStreamReader_get_Line_m85F1A411C379BDFFA2DF9B1DD0BAFEDB96C54351 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Ply.PlyStreamReader::get_Column()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyStreamReader_get_Column_mCF0D5AB5A8D9AF918DE62EC471363E144ECD6441 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987 (String_t* ___0_format, RuntimeObject* ___1_arg0, RuntimeObject* ___2_arg1, const RuntimeMethod* method) ;
// System.Int64 TriLibCore.Utils.HashUtils::GetHash(Unity.Collections.NativeArray`1<System.Char>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t HashUtils_GetHash_mCEFE61677CF3517B02E25BA0BC632C98C62F8C4E (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A ___0_chars, int32_t ___1_count, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyStreamReader::UpdateStringFromCharString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyStreamReader_UpdateStringFromCharString_m48E78B576C5792EE9BEEA23388553D70A8D0D9EF (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.Void Unity.Collections.NativeArray`1<System.Char>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
inline void NativeArray_1__ctor_m01092412782EBC32088D2077868A10700AC7FE61 (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A* __this, int32_t ___0_length, int32_t ___1_allocator, int32_t ___2_options, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A*, int32_t, int32_t, int32_t, const RuntimeMethod*))NativeArray_1__ctor_m01092412782EBC32088D2077868A10700AC7FE61_gshared)(__this, ___0_length, ___1_allocator, ___2_options, method);
}
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader__ctor_mAFA827D6D825FEC2C29C73B65C2DD1AB9076DEC7 (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// System.String TriLibCore.Ply.PlyStreamReader::ReadValidTokenAsString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) ;
// System.Boolean System.SByte::TryParse(System.String,System.SByte&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SByte_TryParse_m9C205D94AB4FF1CA82EA082E38DD01A493A77ED6 (String_t* ___0_s, int8_t* ___1_result, const RuntimeMethod* method) ;
// System.Boolean System.Byte::TryParse(System.String,System.Byte&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Byte_TryParse_mB1716E3B6714F20DF6C1FEDDC4A76AA78D5EA87B (String_t* ___0_s, uint8_t* ___1_result, const RuntimeMethod* method) ;
// System.Boolean System.Int16::TryParse(System.String,System.Int16&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int16_TryParse_m7190AF18437CE1B43990B99E5D992E31485E77AE (String_t* ___0_s, int16_t* ___1_result, const RuntimeMethod* method) ;
// System.Boolean System.UInt16::TryParse(System.String,System.UInt16&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UInt16_TryParse_m02DD9A625527B4019B32ACC9A5D3B09D72B2FBB6 (String_t* ___0_s, uint16_t* ___1_result, const RuntimeMethod* method) ;
// System.Int32 System.Convert::ToInt32(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Convert_ToInt32_m0C3F3778B1D646778F41B6912138AEEEE6BEB9D4 (String_t* ___0_value, const RuntimeMethod* method) ;
// System.Boolean System.Int32::TryParse(System.String,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int32_TryParse_mC928DE2FEC1C35ED5298BDDCA9868076E94B8A21 (String_t* ___0_s, int32_t* ___1_result, const RuntimeMethod* method) ;
// System.Boolean System.UInt32::TryParse(System.String,System.UInt32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UInt32_TryParse_mD470E3BAC9F792AB0BC616510AE3FA78C3CCB1E9 (String_t* ___0_s, uint32_t* ___1_result, const RuntimeMethod* method) ;
// System.Single System.Convert::ToSingle(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Convert_ToSingle_m8416CDFFC7641BD79BE63F39D5FAEE28986FC636 (String_t* ___0_value, RuntimeObject* ___1_provider, const RuntimeMethod* method) ;
// System.Double System.Convert::ToDouble(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double Convert_ToDouble_mAA66A3AA3A6E53529E4F632BC69582B4B70D32B7 (String_t* ___0_value, RuntimeObject* ___1_provider, const RuntimeMethod* method) ;
// System.Void Unity.Collections.NativeArray`1<System.Char>::Dispose()
inline void NativeArray_1_Dispose_m11184C5FB1A1F3809B982476408F08F599F377F0 (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A* __this, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A*, const RuntimeMethod*))NativeArray_1_Dispose_m11184C5FB1A1F3809B982476408F08F599F377F0_gshared)(__this, method);
}
// System.Void System.IO.StreamReader::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamReader_Dispose_mB7BA2F3F47444F6D00457E04462BC097EEE6D27C (StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* __this, bool ___0_disposing, const RuntimeMethod* method) ;
// System.String TriLibCore.Ply.PlyTexture::get_Filename()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PlyTexture_get_Filename_mC7B533AE00B2654EC706CFF6F3DC659B7EA9E58D_inline (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478 (String_t* ___0_value, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Utils.TextureComparators::TextureEquals(TriLibCore.Interfaces.ITexture,TriLibCore.Interfaces.ITexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TextureComparators_TextureEquals_m566CCA88570A7A060514DCAEF48AE3170D743679 (RuntimeObject* ___0_a, RuntimeObject* ___1_b, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Utils.TextureComparators::Equals(TriLibCore.Interfaces.ITexture,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TextureComparators_Equals_mA1D187553F7AC8EB27F3C8D0F2D1316C5E05E4AC (RuntimeObject* ___0_a, RuntimeObject* ___1_b, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Utils.TextureComparators::GetHashCode(TriLibCore.Interfaces.ITexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextureComparators_GetHashCode_mF57C0A300F03E349E694DB594CA2FF73427BECA3 (RuntimeObject* ___0_a, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline (const RuntimeMethod* method) ;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57 (RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___0_handle, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.ReaderBase::ReadStream(System.IO.Stream,TriLibCore.AssetLoaderContext,System.String,System.Action`2<TriLibCore.AssetLoaderContext,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReaderBase_ReadStream_m725378DF096B29E0DB3BE3FB9E5F1E37747883F4 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, String_t* ___2_filename, Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___3_onProgress, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::SetupStream(System.IO.Stream&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_SetupStream_mCDC78453E3657CB3FBB713C40FB50B4941455942 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE** ___0_stream, const RuntimeMethod* method) ;
// System.Void TriLibCore.Ply.PlyProcessor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyProcessor__ctor_mEFE18083343802C21877A430538A1304EC1DC5E7 (PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F* __this, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.Ply.PlyProcessor::Process(TriLibCore.Ply.Reader.PlyReader,System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyProcessor_Process_m5EC73F6D1F63A911A1C86747BB95E7948D0D0763 (PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F* __this, PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* ___0_plyReader, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___1_stream, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::PostProcessModel(TriLibCore.Interfaces.IRootModel&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_PostProcessModel_mD3BB953DE8BEB5FCA65077455BDA0750F740C667 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, RuntimeObject** ___0_model, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase__ctor_m5C4FE7A4BC205B65DAB56FF3CC5202D0B04937DA (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TriLibCore.Ply.PlyProperty TriLibCore.Ply.PlyElement::GetProperty(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280 (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, int64_t ___0_propertyName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_0 = NULL;
	{
		Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_0 = __this->___Properties_0;
		int64_t L_1 = ___0_propertyName;
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5(L_0, L_1, (&V_0), Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_3 = V_0;
		return L_3;
	}

IL_0012:
	{
		return (PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362*)NULL;
	}
}
// System.Int32 TriLibCore.Ply.PlyElement::GetListIndex(TriLibCore.Ply.PlyListProperty,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyElement_GetListIndex_m2725B97E4D677B68667107956C9D7E8A2C2565AF (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* ___0_property, int32_t ___1_elementIndex, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___1_elementIndex;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_1 = __this->___Data_2;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_inline(L_1, List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_002c;
		}
	}
	{
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_3 = __this->___Data_2;
		int32_t L_4 = ___1_elementIndex;
		NullCheck(L_3);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_5;
		L_5 = List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641(L_3, L_4, List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_6 = ___0_property;
		NullCheck(L_6);
		int32_t L_7 = ((PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362*)L_6)->___Offset_1;
		NullCheck(L_5);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_8;
		L_8 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_5, L_7, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		int32_t L_9;
		L_9 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_8, 4, NULL);
		return L_9;
	}

IL_002c:
	{
		return 0;
	}
}
// System.Int32 TriLibCore.Ply.PlyElement::GetPropertyIntValue(TriLibCore.Ply.PlyProperty,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyElement_GetPropertyIntValue_m350C36F63BFB7DB71BBDE9219AE28BC400EAC124 (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___0_property, int32_t ___1_elementIndex, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___1_elementIndex;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_1 = __this->___Data_2;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_inline(L_1, List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_002c;
		}
	}
	{
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_3 = __this->___Data_2;
		int32_t L_4 = ___1_elementIndex;
		NullCheck(L_3);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_5;
		L_5 = List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641(L_3, L_4, List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_6 = ___0_property;
		NullCheck(L_6);
		int32_t L_7 = L_6->___Offset_1;
		NullCheck(L_5);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_8;
		L_8 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_5, L_7, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_9 = ___0_property;
		int32_t L_10;
		L_10 = PlyValue_GetIntValue_mC5D8E3A04354E7AD052C4E0DA2DDC26DADC58ABD(L_8, L_9, NULL);
		return L_10;
	}

IL_002c:
	{
		return 0;
	}
}
// System.Single TriLibCore.Ply.PlyElement::GetPropertyFloatValue(TriLibCore.Ply.PlyProperty,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___0_property, int32_t ___1_elementIndex, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___1_elementIndex;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_1 = __this->___Data_2;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_inline(L_1, List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_002c;
		}
	}
	{
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_3 = __this->___Data_2;
		int32_t L_4 = ___1_elementIndex;
		NullCheck(L_3);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_5;
		L_5 = List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641(L_3, L_4, List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_6 = ___0_property;
		NullCheck(L_6);
		int32_t L_7 = L_6->___Offset_1;
		NullCheck(L_5);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_8;
		L_8 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_5, L_7, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_9 = ___0_property;
		float L_10;
		L_10 = PlyValue_GetFloatValue_mCE0211B08C054DE0848AEB2EF5E728F6D28E9830(L_8, L_9, NULL);
		return L_10;
	}

IL_002c:
	{
		return (0.0f);
	}
}
// System.Int32 TriLibCore.Ply.PlyElement::GetPropertyIntValue(System.Int64,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyElement_GetPropertyIntValue_m549C727BE63693831642CA3ADA3E87B879575681 (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, int64_t ___0_propertyName, int32_t ___1_elementIndex, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_0 = NULL;
	{
		Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_0 = __this->___Properties_0;
		int64_t L_1 = ___0_propertyName;
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5(L_0, L_1, (&V_0), Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_3 = ___1_elementIndex;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_4 = __this->___Data_2;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_inline(L_4, List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		if ((((int32_t)L_3) >= ((int32_t)L_5)))
		{
			goto IL_003c;
		}
	}
	{
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_6 = __this->___Data_2;
		int32_t L_7 = ___1_elementIndex;
		NullCheck(L_6);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_8;
		L_8 = List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641(L_6, L_7, List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->___Offset_1;
		NullCheck(L_8);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_11;
		L_11 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_8, L_10, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_12 = V_0;
		int32_t L_13;
		L_13 = PlyValue_GetIntValue_mC5D8E3A04354E7AD052C4E0DA2DDC26DADC58ABD(L_11, L_12, NULL);
		return L_13;
	}

IL_003c:
	{
		return 0;
	}
}
// System.Single TriLibCore.Ply.PlyElement::GetPropertyFloatValue(System.Int64,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713 (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, int64_t ___0_propertyName, int32_t ___1_elementIndex, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_0 = NULL;
	{
		Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_0 = __this->___Properties_0;
		int64_t L_1 = ___0_propertyName;
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5(L_0, L_1, (&V_0), Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_3 = ___1_elementIndex;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_4 = __this->___Data_2;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_inline(L_4, List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		if ((((int32_t)L_3) >= ((int32_t)L_5)))
		{
			goto IL_003c;
		}
	}
	{
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_6 = __this->___Data_2;
		int32_t L_7 = ___1_elementIndex;
		NullCheck(L_6);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_8;
		L_8 = List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641(L_6, L_7, List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->___Offset_1;
		NullCheck(L_8);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_11;
		L_11 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_8, L_10, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_12 = V_0;
		float L_13;
		L_13 = PlyValue_GetFloatValue_mCE0211B08C054DE0848AEB2EF5E728F6D28E9830(L_11, L_12, NULL);
		return L_13;
	}

IL_003c:
	{
		return (0.0f);
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyElement::GetPropertyValue(System.Int64,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyElement_GetPropertyValue_m54D889CCBE95674ACF860EF9F81AF8B2432AD8AE (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, int64_t ___0_propertyName, int32_t ___1_elementIndex, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_0 = NULL;
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_0 = __this->___Properties_0;
		int64_t L_1 = ___0_propertyName;
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5(L_0, L_1, (&V_0), Dictionary_2_TryGetValue_m843741FC841291E930E4C6AC02FFA6A75E4479D5_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = ___1_elementIndex;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_4 = __this->___Data_2;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_inline(L_4, List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		if ((((int32_t)L_3) >= ((int32_t)L_5)))
		{
			goto IL_0036;
		}
	}
	{
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_6 = __this->___Data_2;
		int32_t L_7 = ___1_elementIndex;
		NullCheck(L_6);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_8;
		L_8 = List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641(L_6, L_7, List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->___Offset_1;
		NullCheck(L_8);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_11;
		L_11 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_8, L_10, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		return L_11;
	}

IL_0036:
	{
		il2cpp_codegen_initobj((&V_1), sizeof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41));
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_12 = V_1;
		return L_12;
	}
}
// System.Void TriLibCore.Ply.PlyElement::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyElement__ctor_mCA04905EABE9EB079C06C05A9EBB680CF4A4DBE3 (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m9E31627A68563218A84912174B1A3CCFF5D9D3DB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_0 = (Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680*)il2cpp_codegen_object_new(Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Dictionary_2__ctor_m9E31627A68563218A84912174B1A3CCFF5D9D3DB(L_0, Dictionary_2__ctor_m9E31627A68563218A84912174B1A3CCFF5D9D3DB_RuntimeMethod_var);
		__this->___Properties_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Properties_0), (void*)L_0);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Ply.PlyGeometry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyGeometry__ctor_mAB017E5BE0639E760A21FFAE52BC5E61E0C2DAB3 (PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* __this, const RuntimeMethod* method) 
{
	{
		Geometry__ctor_m0F5EBDDAEEB9ED6F66946F16F31BAB22BE44D193(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Ply.PlyListProperty::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyListProperty__ctor_m913A103F9505DB2EC35D68434C7B1676D0F26196 (PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* __this, const RuntimeMethod* method) 
{
	{
		PlyProperty__ctor_mB9404ABEA64DE21D9C16B8FCF8FA95FEA5C8221F(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLibCore.Ply.PlyMaterial::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyMaterial_get_Name_m54A587097E8F94AFB2B1D87C1DEE2D511BE7F301 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyMaterial::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyMaterial_set_Name_m19C50592D6E7B2401AB6B929B781ADA4A5173275 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyMaterial::get_Used()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyMaterial_get_Used_m279786A7462AB5FF3B8FF1AF7E2B5F211BD28CD7 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CUsedU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyMaterial::set_Used(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyMaterial_set_Used_m196A9298FD117D668A9434E8F4CC5D453C6AE2FE (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CUsedU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyMaterial::get_UsesAlpha()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyMaterial_get_UsesAlpha_m10730988369C15A5C7D903525DFA0BBE07573EC4 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) 
{
	{
		return (bool)0;
	}
}
// System.Boolean TriLibCore.Ply.PlyMaterial::ApplyOffsetAndScale(TriLibCore.TextureLoadingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyMaterial_ApplyOffsetAndScale_m5B80D869ED2D450CAB555B1F122A743F493B189B (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, TextureLoadingContext_t0DB0AE7751B1A9E54C1612F112D2A295ED84D879* ___0_textureLoadingContext, const RuntimeMethod* method) 
{
	{
		return (bool)0;
	}
}
// System.Void TriLibCore.Ply.PlyMaterial::AddProperty(System.String,System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyMaterial_AddProperty_m4FE8AE6161D498A95286AC015BD2E1B2EC41E3B4 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, RuntimeObject* ___1_propertyValue, bool ___2_isTexture, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDictionary_2_t79D4ADB15B238AC117DF72982FEA3C42EF5AFA19_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->____properties_0;
		String_t* L_1 = ___0_propertyName;
		RuntimeObject* L_2 = ___1_propertyValue;
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, RuntimeObject* >::Invoke(1 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::set_Item(TKey,TValue) */, IDictionary_2_t79D4ADB15B238AC117DF72982FEA3C42EF5AFA19_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.String TriLibCore.Ply.PlyMaterial::GetGenericPropertyName(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyMaterial_GetGenericPropertyName_m41CB8C3D4FBAC86A36411C2405D56BA48F3819ED (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_genericMaterialProperty, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral31A9B06BE46B0E2BB27797EDC5BAE6C2BFA4ABF0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D934867D69D7E986A4C224DB49CF270468DE64D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral711AA001951412D09872DB5FA0B90EA6875A17F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7A12A09E76D9C6C6FAD55C385BB87538EF591395);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___0_genericMaterialProperty;
		switch (L_0)
		{
			case 0:
			{
				goto IL_003c;
			}
			case 1:
			{
				goto IL_0042;
			}
			case 2:
			{
				goto IL_0048;
			}
			case 3:
			{
				goto IL_0054;
			}
			case 4:
			{
				goto IL_0054;
			}
			case 5:
			{
				goto IL_004e;
			}
			case 6:
			{
				goto IL_0054;
			}
			case 7:
			{
				goto IL_0054;
			}
			case 8:
			{
				goto IL_0054;
			}
			case 9:
			{
				goto IL_0054;
			}
			case 10:
			{
				goto IL_0054;
			}
			case 11:
			{
				goto IL_0054;
			}
			case 12:
			{
				goto IL_0054;
			}
		}
	}
	{
		goto IL_0054;
	}

IL_003c:
	{
		return _stringLiteral31A9B06BE46B0E2BB27797EDC5BAE6C2BFA4ABF0;
	}

IL_0042:
	{
		return _stringLiteral7A12A09E76D9C6C6FAD55C385BB87538EF591395;
	}

IL_0048:
	{
		return _stringLiteral711AA001951412D09872DB5FA0B90EA6875A17F9;
	}

IL_004e:
	{
		return _stringLiteral5D934867D69D7E986A4C224DB49CF270468DE64D;
	}

IL_0054:
	{
		return (String_t*)NULL;
	}
}
// UnityEngine.Color TriLibCore.Ply.PlyMaterial::GetGenericColorValueMultiplied(TriLibCore.General.GenericMaterialProperty,TriLibCore.MaterialMapperContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F PlyMaterial_GetGenericColorValueMultiplied_mEA66D711A7AEFBDEB2F136A37E7A2C593B98ED70 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_genericMaterialProperty, MaterialMapperContext_t2BDF775C916A28A411960E3787DC933D2E9F0042* ___1_materialMapperContext, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_genericMaterialProperty;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1;
		L_1 = PlyMaterial_GetGenericColorValue_mDAB3E44868BCA99887DF188037E7A7E9A48E36E4(__this, L_0, NULL);
		return L_1;
	}
}
// System.Single TriLibCore.Ply.PlyMaterial::GetGenericFloatValueMultiplied(TriLibCore.General.GenericMaterialProperty,TriLibCore.MaterialMapperContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyMaterial_GetGenericFloatValueMultiplied_mEED07914F8FCDF325559890980BCDE7FED5EC363 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_genericMaterialProperty, MaterialMapperContext_t2BDF775C916A28A411960E3787DC933D2E9F0042* ___1_materialMapperContext, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF0A80239664746BB5A8F67D47C690B1D476A5F49);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF85DFEE8E7ED0F83CF41E7E6F08E0DAACA5F3C3B);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___0_genericMaterialProperty;
		float L_1;
		L_1 = PlyMaterial_GetGenericFloatValue_m5BC4612B3D15C6C0ABE4E7B0A14CE34CA6EAF21F(__this, L_0, NULL);
		V_0 = L_1;
		int32_t L_2 = ___0_genericMaterialProperty;
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = ___0_genericMaterialProperty;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_002e;
		}
	}
	{
		goto IL_004b;
	}

IL_0011:
	{
		bool L_4;
		L_4 = PlyMaterial_HasProperty_mB36FB6DBB9CF41BD74A5C793E011AF62FA370EF4(__this, _stringLiteralF85DFEE8E7ED0F83CF41E7E6F08E0DAACA5F3C3B, NULL);
		if (L_4)
		{
			goto IL_0020;
		}
	}
	{
		float L_5 = V_0;
		return L_5;
	}

IL_0020:
	{
		float L_6;
		L_6 = PlyMaterial_GetFloatValue_mB021B41CE867CAC9BC3A68FD86F956FFBC9566D3(__this, _stringLiteralF85DFEE8E7ED0F83CF41E7E6F08E0DAACA5F3C3B, NULL);
		float L_7 = V_0;
		return ((float)il2cpp_codegen_multiply(L_6, L_7));
	}

IL_002e:
	{
		bool L_8;
		L_8 = PlyMaterial_HasProperty_mB36FB6DBB9CF41BD74A5C793E011AF62FA370EF4(__this, _stringLiteralF0A80239664746BB5A8F67D47C690B1D476A5F49, NULL);
		if (L_8)
		{
			goto IL_003d;
		}
	}
	{
		float L_9 = V_0;
		return L_9;
	}

IL_003d:
	{
		float L_10;
		L_10 = PlyMaterial_GetFloatValue_mB021B41CE867CAC9BC3A68FD86F956FFBC9566D3(__this, _stringLiteralF0A80239664746BB5A8F67D47C690B1D476A5F49, NULL);
		float L_11 = V_0;
		return ((float)il2cpp_codegen_multiply(L_10, L_11));
	}

IL_004b:
	{
		float L_12 = V_0;
		return L_12;
	}
}
// System.Single TriLibCore.Ply.PlyMaterial::GetGenericFloatValue(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyMaterial_GetGenericFloatValue_m5BC4612B3D15C6C0ABE4E7B0A14CE34CA6EAF21F (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_materialProperty, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___0_materialProperty;
		String_t* L_1;
		L_1 = PlyMaterial_GetGenericPropertyName_m41CB8C3D4FBAC86A36411C2405D56BA48F3819ED(__this, L_0, NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3;
		L_3 = PlyMaterial_HasProperty_mB36FB6DBB9CF41BD74A5C793E011AF62FA370EF4(__this, L_2, NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_4 = V_0;
		float L_5;
		L_5 = PlyMaterial_GetFloatValue_mB021B41CE867CAC9BC3A68FD86F956FFBC9566D3(__this, L_4, NULL);
		return L_5;
	}

IL_0019:
	{
		int32_t L_6 = ___0_materialProperty;
		if ((((int32_t)L_6) == ((int32_t)5)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_7 = ___0_materialProperty;
		if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_7, ((int32_t)14)))) <= ((uint32_t)1))))
		{
			goto IL_002a;
		}
	}

IL_0024:
	{
		return (1.0f);
	}

IL_002a:
	{
		return (0.0f);
	}
}
// System.Int32 TriLibCore.Ply.PlyMaterial::GetGenericIntValue(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyMaterial_GetGenericIntValue_m1A8A5B1C7E490BB77374C57FB051030D07106365 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_materialProperty, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___0_materialProperty;
		String_t* L_1;
		L_1 = PlyMaterial_GetGenericPropertyName_m41CB8C3D4FBAC86A36411C2405D56BA48F3819ED(__this, L_0, NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		int32_t L_3;
		L_3 = PlyMaterial_GetIntValue_m7D4FA27F9231A957F97E091EAB1D9A7C546D88BD(__this, L_2, NULL);
		return L_3;
	}
}
// System.String TriLibCore.Ply.PlyMaterial::GetGenericStringValue(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyMaterial_GetGenericStringValue_m773F3DC4FFF0053AA1FBDEAA1BBBA4F78CE749B2 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_materialProperty, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___0_materialProperty;
		String_t* L_1;
		L_1 = PlyMaterial_GetGenericPropertyName_m41CB8C3D4FBAC86A36411C2405D56BA48F3819ED(__this, L_0, NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		String_t* L_3;
		L_3 = PlyMaterial_GetStringValue_mB6CCF7EB09045885C1FFECC7C84CBB6CEDE49F8D(__this, L_2, NULL);
		return L_3;
	}
}
// UnityEngine.Vector3 TriLibCore.Ply.PlyMaterial::GetGenericVector3Value(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 PlyMaterial_GetGenericVector3Value_m290D51A7F75621453CE0B08A3BDA449B4DB5339A (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_materialProperty, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___0_materialProperty;
		String_t* L_1;
		L_1 = PlyMaterial_GetGenericPropertyName_m41CB8C3D4FBAC86A36411C2405D56BA48F3819ED(__this, L_0, NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = PlyMaterial_GetVector3Value_m2A6EE04EE31489DDD7A1003E00BF79C86A81D504(__this, L_2, NULL);
		return L_3;
	}
}
// UnityEngine.Vector4 TriLibCore.Ply.PlyMaterial::GetGenericVector4Value(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 PlyMaterial_GetGenericVector4Value_mFD7F4A61ACCD901CD5C4536BA4887495624BA7B4 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_materialProperty, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___0_materialProperty;
		String_t* L_1;
		L_1 = PlyMaterial_GetGenericPropertyName_m41CB8C3D4FBAC86A36411C2405D56BA48F3819ED(__this, L_0, NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_3;
		L_3 = PlyMaterial_GetVector4Value_m1AC8F1CBBF1703484EEB7833978B1CFC20F4FAB3(__this, L_2, NULL);
		return L_3;
	}
}
// UnityEngine.Color TriLibCore.Ply.PlyMaterial::GetGenericColorValue(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F PlyMaterial_GetGenericColorValue_mDAB3E44868BCA99887DF188037E7A7E9A48E36E4 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_materialProperty, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___0_materialProperty;
		String_t* L_1;
		L_1 = PlyMaterial_GetGenericPropertyName_m41CB8C3D4FBAC86A36411C2405D56BA48F3819ED(__this, L_0, NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3;
		L_3 = PlyMaterial_HasProperty_mB36FB6DBB9CF41BD74A5C793E011AF62FA370EF4(__this, L_2, NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_4 = V_0;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_5;
		L_5 = PlyMaterial_GetColorValue_m8B4EA23BBCF1DF22DDFA1A4D1DC3E389A317151D(__this, L_4, NULL);
		return L_5;
	}

IL_0019:
	{
		int32_t L_6 = ___0_materialProperty;
		if ((((int32_t)L_6) == ((int32_t)8)))
		{
			goto IL_0023;
		}
	}
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_7;
		L_7 = Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline(NULL);
		return L_7;
	}

IL_0023:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8;
		L_8 = Color_get_black_mB50217951591A045844C61E7FF31EEE3FEF16737_inline(NULL);
		return L_8;
	}
}
// TriLibCore.Interfaces.ITexture TriLibCore.Ply.PlyMaterial::GetGenericTextureValue(TriLibCore.General.GenericMaterialProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyMaterial_GetGenericTextureValue_m714CD0589E2C233733BD63C60CEAF32EC2AFB41B (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_materialProperty, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___0_materialProperty;
		String_t* L_1;
		L_1 = PlyMaterial_GetGenericPropertyName_m41CB8C3D4FBAC86A36411C2405D56BA48F3819ED(__this, L_0, NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		RuntimeObject* L_3;
		L_3 = PlyMaterial_GetTextureValue_mCCE6C1448D7F8075D1560B44FFC67773456373BD(__this, L_2, NULL);
		return L_3;
	}
}
// System.Single TriLibCore.Ply.PlyMaterial::GetFloatValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyMaterial_GetFloatValue_mB021B41CE867CAC9BC3A68FD86F956FFBC9566D3 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->____properties_0;
		String_t* L_1 = ___0_propertyName;
		bool L_2;
		L_2 = DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9(L_0, L_1, (&V_0), DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_4;
		L_4 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		float L_5;
		L_5 = Convert_ToSingle_mEC65F60A081FBBB8ACBCD8747183FEDDAEC034C9(L_3, L_4, NULL);
		return L_5;
	}

IL_001c:
	{
		return (0.0f);
	}
}
// System.Int32 TriLibCore.Ply.PlyMaterial::GetIntValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyMaterial_GetIntValue_m7D4FA27F9231A957F97E091EAB1D9A7C546D88BD (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->____properties_0;
		String_t* L_1 = ___0_propertyName;
		bool L_2;
		L_2 = DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9(L_0, L_1, (&V_0), DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		int32_t L_4;
		L_4 = Convert_ToInt32_m9FEA65DB96264479B5268014F10754787382D297(L_3, NULL);
		return L_4;
	}

IL_0017:
	{
		return 0;
	}
}
// System.String TriLibCore.Ply.PlyMaterial::GetStringValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyMaterial_GetStringValue_mB6CCF7EB09045885C1FFECC7C84CBB6CEDE49F8D (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->____properties_0;
		String_t* L_1 = ___0_propertyName;
		bool L_2;
		L_2 = DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9(L_0, L_1, (&V_0), DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_4;
		L_4 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		String_t* L_5;
		L_5 = Convert_ToString_m833ABF9C901B707B28FB10DEBFCC511A87E6C827(L_3, L_4, NULL);
		return L_5;
	}

IL_001c:
	{
		return (String_t*)NULL;
	}
}
// UnityEngine.Vector3 TriLibCore.Ply.PlyMaterial::GetVector3Value(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 PlyMaterial_GetVector3Value_m2A6EE04EE31489DDD7A1003E00BF79C86A81D504 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->____properties_0;
		String_t* L_1 = ___0_propertyName;
		bool L_2;
		L_2 = DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9(L_0, L_1, (&V_0), DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		return ((*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)UnBox(L_3, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))));
	}

IL_0017:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		return L_4;
	}
}
// UnityEngine.Vector4 TriLibCore.Ply.PlyMaterial::GetVector4Value(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 PlyMaterial_GetVector4Value_m1AC8F1CBBF1703484EEB7833978B1CFC20F4FAB3 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->____properties_0;
		String_t* L_1 = ___0_propertyName;
		bool L_2;
		L_2 = DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9(L_0, L_1, (&V_0), DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		return ((*(Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3*)((Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3*)(Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3*)UnBox(L_3, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var))));
	}

IL_0017:
	{
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_4;
		L_4 = Vector4_get_zero_m3D61F5FA9483CD9C08977D9D8852FB448B4CE6D1_inline(NULL);
		return L_4;
	}
}
// UnityEngine.Color TriLibCore.Ply.PlyMaterial::GetColorValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F PlyMaterial_GetColorValue_m8B4EA23BBCF1DF22DDFA1A4D1DC3E389A317151D (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Color_tD001788D726C3A7F1379BEED0260B9591F440C1F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->____properties_0;
		String_t* L_1 = ___0_propertyName;
		bool L_2;
		L_2 = DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9(L_0, L_1, (&V_0), DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		return ((*(Color_tD001788D726C3A7F1379BEED0260B9591F440C1F*)((Color_tD001788D726C3A7F1379BEED0260B9591F440C1F*)(Color_tD001788D726C3A7F1379BEED0260B9591F440C1F*)UnBox(L_3, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F_il2cpp_TypeInfo_var))));
	}

IL_0017:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_4;
		L_4 = Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline(NULL);
		return L_4;
	}
}
// TriLibCore.Interfaces.ITexture TriLibCore.Ply.PlyMaterial::GetTextureValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyMaterial_GetTextureValue_mCCE6C1448D7F8075D1560B44FFC67773456373BD (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ITexture_t4CD71425D2DAB0C38B4E57E909DEAC9A9AC89FE8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = __this->____properties_0;
		String_t* L_1 = ___0_propertyName;
		bool L_2;
		L_2 = DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9(L_0, L_1, (&V_0), DictionaryExtensions_TryGetValueSafe_TisString_t_TisRuntimeObject_m3A141C0F88D55709D928005262A7173CBC7CE8E9_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		RuntimeObject* L_3 = V_0;
		return ((RuntimeObject*)Castclass((RuntimeObject*)L_3, ITexture_t4CD71425D2DAB0C38B4E57E909DEAC9A9AC89FE8_il2cpp_TypeInfo_var));
	}

IL_0017:
	{
		return (RuntimeObject*)NULL;
	}
}
// System.Boolean TriLibCore.Ply.PlyMaterial::HasProperty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyMaterial_HasProperty_mB36FB6DBB9CF41BD74A5C793E011AF62FA370EF4 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, String_t* ___0_propertyName, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDictionary_2_t79D4ADB15B238AC117DF72982FEA3C42EF5AFA19_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___0_propertyName;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		RuntimeObject* L_1 = __this->____properties_0;
		String_t* L_2 = ___0_propertyName;
		NullCheck(L_1);
		bool L_3;
		L_3 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(4 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::ContainsKey(TKey) */, IDictionary_2_t79D4ADB15B238AC117DF72982FEA3C42EF5AFA19_il2cpp_TypeInfo_var, L_1, L_2);
		return L_3;
	}
}
// System.Boolean TriLibCore.Ply.PlyMaterial::PostProcessTexture(TriLibCore.TextureLoadingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyMaterial_PostProcessTexture_mC3A114926CED68DB064AD5C3E0B4B9897B90A7A7 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, TextureLoadingContext_t0DB0AE7751B1A9E54C1612F112D2A295ED84D879* ___0_textureLoadingContext, const RuntimeMethod* method) 
{
	{
		return (bool)0;
	}
}
// TriLibCore.General.MaterialShadingSetup TriLibCore.Ply.PlyMaterial::get_MaterialShadingSetup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyMaterial_get_MaterialShadingSetup_mD3E1C18BB9009824CB19AE3C9E2C5336B88EDF86 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) 
{
	{
		return (int32_t)(0);
	}
}
// System.Int32 TriLibCore.Ply.PlyMaterial::get_Index()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyMaterial_get_Index_m86296D07FEC2B7328E79B65F27F2CCF3FBF52CB6 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CIndexU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyMaterial::set_Index(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyMaterial_set_Index_mEEC9E37FB853F46E7A19C7A52BE8EACD41464726 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CIndexU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyMaterial::get_IsAutodeskInteractive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyMaterial_get_IsAutodeskInteractive_m93EF914605E808CC015BC296A6264189CA1723BF (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) 
{
	{
		return (bool)0;
	}
}
// System.Boolean TriLibCore.Ply.PlyMaterial::get_Processing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyMaterial_get_Processing_m0802899F8D77404838C1A03656F7D8194647EDE0 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CProcessingU3Ek__BackingField_4;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyMaterial::set_Processing(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyMaterial_set_Processing_m8B3AD98212FFFEF97178367ED53A6721C7EB3A30 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CProcessingU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyMaterial::get_Processed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyMaterial_get_Processed_m4A229D7F2F4D561E967843FA02944C6577B2A3B1 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CProcessedU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyMaterial::set_Processed(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyMaterial_set_Processed_mB61CBCE093EEDFBDEDB552EE699D828246D02D53 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CProcessedU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Void TriLibCore.Ply.PlyMaterial::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyMaterial__ctor_m9669AF4D43AE12DEEBD8F46D99D869AC68470623 (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_0 = (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*)il2cpp_codegen_object_new(Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9(L_0, Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9_RuntimeMethod_var);
		__this->____properties_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____properties_0), (void*)L_0);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLibCore.Ply.PlyModel::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyModel_get_Name_mF0BDCE3B91DCB49F6E648F411F06E052FF253F89 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_Name_mB0545A2AC135FF460356C404C341D07492F5A947 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyModel::get_Used()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyModel_get_Used_mC5E3586114B0170C2F1ED7E0DA54F41C02C00CDA (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CUsedU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_Used(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_Used_m28EE472AF323C8624DFAC59921796B5D4EA466E4 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CUsedU3Ek__BackingField_1 = L_0;
		return;
	}
}
// UnityEngine.Vector3 TriLibCore.Ply.PlyModel::get_LocalPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 PlyModel_get_LocalPosition_mE3FF50F7187BD62507B5D3784D095D8C9B35E4A5 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___U3CLocalPositionU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_LocalPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_LocalPosition_m4EE2F1140E54628187D835C46B714786CA91A00E (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->___U3CLocalPositionU3Ek__BackingField_2 = L_0;
		return;
	}
}
// UnityEngine.Quaternion TriLibCore.Ply.PlyModel::get_LocalRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 PlyModel_get_LocalRotation_m1078D001C1CBD948043CD523CACC269309714801 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = __this->___U3CLocalRotationU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_LocalRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_LocalRotation_mA70C0956246A032C3C1C7EF5DE31A303ACE92BCB (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_value, const RuntimeMethod* method) 
{
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___0_value;
		__this->___U3CLocalRotationU3Ek__BackingField_3 = L_0;
		return;
	}
}
// UnityEngine.Vector3 TriLibCore.Ply.PlyModel::get_LocalScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 PlyModel_get_LocalScale_mFD0963A88F33509830230E45F28455908AA5AF8B (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___U3CLocalScaleU3Ek__BackingField_4;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_LocalScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_LocalScale_m8055103EC8ADECCC5D3C535C098B33AA2E0275A8 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->___U3CLocalScaleU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyModel::get_Visibility()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyModel_get_Visibility_m05DD6DA57D686D28227A8ECEAF1F9C9AD38BEFC0 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CVisibilityU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_Visibility(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_Visibility_mB2F9D43D393D00418E2B4A999974A1E18512D0A5 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CVisibilityU3Ek__BackingField_5 = L_0;
		return;
	}
}
// TriLibCore.Interfaces.IModel TriLibCore.Ply.PlyModel::get_Parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyModel_get_Parent_mF779F71B9255A3EF8AB620C52F9D76CE58B5CEC2 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CParentU3Ek__BackingField_6;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_Parent(TriLibCore.Interfaces.IModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_Parent_m99F48AE75EB3BB67A8F0FE3F66FF6FE36D967632 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CParentU3Ek__BackingField_6 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CParentU3Ek__BackingField_6), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Ply.PlyModel::get_Children()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyModel_get_Children_m26E002636866B1E9AA9AB811F8561851268713A0 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CChildrenU3Ek__BackingField_7;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_Children(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_Children_m5974B6A0462643BBB03B474D8B385C8229E3D8E7 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CChildrenU3Ek__BackingField_7 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CChildrenU3Ek__BackingField_7), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyModel::get_IsBone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyModel_get_IsBone_mC5839F63DF0EB6236BB3B56E3269000B5CD3C967 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsBoneU3Ek__BackingField_8;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_IsBone(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_IsBone_m92089BEA168813ED0EE77D6DBABD52DA0EF08BED (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CIsBoneU3Ek__BackingField_8 = L_0;
		return;
	}
}
// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Ply.PlyModel::get_GeometryGroup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyModel_get_GeometryGroup_m87508AF996BC6F6F32703130D5CB2ADEF2591D72 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CGeometryGroupU3Ek__BackingField_9;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_GeometryGroup(TriLibCore.Interfaces.IGeometryGroup)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_GeometryGroup_mC40A2CC9BD75EBA9305AA0FC6E9F1FFD51FDA055 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CGeometryGroupU3Ek__BackingField_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CGeometryGroupU3Ek__BackingField_9), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Ply.PlyModel::get_Bones()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyModel_get_Bones_m277FE79B58C61B6C3EE2C072DC040138E20EF09C (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CBonesU3Ek__BackingField_10;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_Bones(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_Bones_m177A9FF5F6AE222F6509F1FBD882EBC6627D2D7D (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CBonesU3Ek__BackingField_10 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CBonesU3Ek__BackingField_10), (void*)L_0);
		return;
	}
}
// UnityEngine.Matrix4x4[] TriLibCore.Ply.PlyModel::get_BindPoses()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* PlyModel_get_BindPoses_mC76BFFB52EF866ADA3BBCFA4D0A8AC325948ADA3 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_0 = __this->___U3CBindPosesU3Ek__BackingField_11;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_BindPoses(UnityEngine.Matrix4x4[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_BindPoses_mA237F6AD2E675FAED03AF15C13A6FADDF32129D0 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___0_value, const RuntimeMethod* method) 
{
	{
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_0 = ___0_value;
		__this->___U3CBindPosesU3Ek__BackingField_11 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CBindPosesU3Ek__BackingField_11), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Ply.PlyModel::get_MaterialIndices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyModel_get_MaterialIndices_m64F59A9A38D7B2B58C7E9F5DE334FC7DB05875F8 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CMaterialIndicesU3Ek__BackingField_12;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_MaterialIndices(System.Collections.Generic.IList`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_MaterialIndices_m87FB1DB52736B0D10EBF7CA05C04C3B1A321F670 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CMaterialIndicesU3Ek__BackingField_12 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CMaterialIndicesU3Ek__BackingField_12), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Ply.PlyModel::get_UserProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* PlyModel_get_UserProperties_m748D4198E6502894B72A6D19A47D1205315DEB99 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_0 = __this->___U3CUserPropertiesU3Ek__BackingField_13;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::set_UserProperties(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel_set_UserProperties_m9FE64E4C5A0D07266A7CBE9878294B9A825EE0E3 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___0_value, const RuntimeMethod* method) 
{
	{
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_0 = ___0_value;
		__this->___U3CUserPropertiesU3Ek__BackingField_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CUserPropertiesU3Ek__BackingField_13), (void*)L_0);
		return;
	}
}
// System.String TriLibCore.Ply.PlyModel::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyModel_ToString_mD5566D539F1B85F3C6F029F9A3B521463D9C2A84 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0;
		L_0 = PlyModel_get_Name_mF0BDCE3B91DCB49F6E648F411F06E052FF253F89_inline(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyModel__ctor_mEBFCADB0E140C2324D5944AD85D4BED4CE3BC725 (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TriLibCore.Interfaces.IRootModel TriLibCore.Ply.PlyProcessor::Process(TriLibCore.Ply.Reader.PlyReader,System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyProcessor_Process_m5EC73F6D1F63A911A1C86747BB95E7948D0D0763 (PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F* __this, PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* ___0_plyReader, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___1_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Color_tD001788D726C3A7F1379BEED0260B9591F440C1F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m13BA45BD072C072ECDC20ED17980959D50DCDEBE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m7E3EF4351105771F2D6EB167BE29E7868CFC84F0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m181BAACEB7905BC572C38343BF7DDA866E9415FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m4795CFCFA1CA4807CD85E1818CEC2A7EBC2E336C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Count_m270BB8BF6E2728FF1BE69D236B3C1BB5E5D60BFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Values_m66C7C3835140D396B2941C5FF726A8435575E421_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Values_mD47C7DCD10B474F8DB55896C6B5F276ACA9E2BC1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m16443C4F8A0449761F7E9532DA13EE62BC66A959_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mFB43E311F648679BE046A19C9351234B9C43A1CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m355F0050D35C5CECAB616FAD6BD09BCDF75ED2AD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m3A6AF59F5158004E0E8F5D30912F0B3D578BAD20_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m16B76A6F6611FB82F01BB3EA7B36F6BE9AC3B558_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mA527966D30259A4C4C030B11F4DFEE3029C1076F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_1_t77739521790EB7E3F514D44638D90AB775EDE8ED_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_1_tD4699121F1986A1CF0F28D10E91389DD1C7FDE69_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_1_tEF23B8A78E9122DAA952DB1517D31EB7EC90563B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_1_tFC51DD83E1437529AA19214EFADD5EE83B73CD6E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IGeometryGroup_GetGeometry_TisPlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277_mD3FEA40366BC6279E62F5984369E846ADFC1D016_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_1_t0662D113B996C51F1676FFC848F7B3448D818DB7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_1_t4EEE459A249DDE104FA2E88234C593389EE5D291_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m096BE2FF8568855341E87C1DE6291916F89A8203_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m7C99F2FA69D684BD5B7E22B8A115DA258EA04CB2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m179CF1BDD503F830F79CF1A81033083C1BB19E05_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m27475592735C318973899086F95036A18B6D7E39_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m71DCE2DD53C6EFD6DF99B2592A347DFD0F9106C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m7E6E0C4AE37ACBC1E9BDA9E56A8A7D8ACED3FD33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mDBC3F8F2846CD821DA096BECD6300438E2409BC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mEEA3C6B55707EC6755329113F4C364960AA66CEF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ValueCollection_GetEnumerator_m7A2633FD76AC8804D68EEB036B996CAF5FDC7754_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ValueCollection_GetEnumerator_mD927D6AC4BE78DAED8A83775F0D1FEF91A756B4D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral31A9B06BE46B0E2BB27797EDC5BAE6C2BFA4ABF0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D934867D69D7E986A4C224DB49CF270468DE64D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral711AA001951412D09872DB5FA0B90EA6875A17F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7A12A09E76D9C6C6FAD55C385BB87538EF591395);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* V_0 = NULL;
	List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* V_1 = NULL;
	PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* V_6 = NULL;
	List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31* V_7 = NULL;
	bool V_8 = false;
	bool V_9 = false;
	PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* V_10 = NULL;
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* V_11 = NULL;
	BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* V_12 = NULL;
	RuntimeObject* V_13 = NULL;
	PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* V_14 = NULL;
	PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* V_15 = NULL;
	int64_t V_16 = 0;
	int64_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int64_t V_22 = 0;
	PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* V_23 = NULL;
	int64_t V_24 = 0;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_25 = NULL;
	Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901 V_26;
	memset((&V_26), 0, sizeof(V_26));
	PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* V_27 = NULL;
	List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* V_28 = NULL;
	int32_t V_29 = 0;
	List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* V_30 = NULL;
	Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8 V_31;
	memset((&V_31), 0, sizeof(V_31));
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_32;
	memset((&V_32), 0, sizeof(V_32));
	PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* V_33 = NULL;
	int32_t V_34 = 0;
	PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* V_35 = NULL;
	float V_36 = 0.0f;
	float V_37 = 0.0f;
	float V_38 = 0.0f;
	float V_39 = 0.0f;
	float V_40 = 0.0f;
	float V_41 = 0.0f;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_42 = NULL;
	float V_43 = 0.0f;
	PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* V_44 = NULL;
	int32_t V_45 = 0;
	float V_46 = 0.0f;
	float V_47 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_48;
	memset((&V_48), 0, sizeof(V_48));
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_49 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_50 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_51 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_52 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_53 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_54 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_55 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_56 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_57 = NULL;
	float V_58 = 0.0f;
	float V_59 = 0.0f;
	float V_60 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_61;
	memset((&V_61), 0, sizeof(V_61));
	float V_62 = 0.0f;
	float V_63 = 0.0f;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_64;
	memset((&V_64), 0, sizeof(V_64));
	float V_65 = 0.0f;
	float V_66 = 0.0f;
	float V_67 = 0.0f;
	float V_68 = 0.0f;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_69;
	memset((&V_69), 0, sizeof(V_69));
	PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* V_70 = NULL;
	int32_t V_71 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_72;
	memset((&V_72), 0, sizeof(V_72));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_73;
	memset((&V_73), 0, sizeof(V_73));
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_74;
	memset((&V_74), 0, sizeof(V_74));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_75;
	memset((&V_75), 0, sizeof(V_75));
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_76;
	memset((&V_76), 0, sizeof(V_76));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_77;
	memset((&V_77), 0, sizeof(V_77));
	BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F V_78;
	memset((&V_78), 0, sizeof(V_78));
	PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* V_79 = NULL;
	int32_t V_80 = 0;
	List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* V_81 = NULL;
	PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* V_82 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* V_83 = NULL;
	int32_t V_84 = 0;
	PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* V_85 = NULL;
	PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* V_86 = NULL;
	PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* V_87 = NULL;
	List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* V_88 = NULL;
	int32_t V_89 = 0;
	ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF* V_90 = NULL;
	int32_t V_91 = 0;
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_92;
	memset((&V_92), 0, sizeof(V_92));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_93;
	memset((&V_93), 0, sizeof(V_93));
	PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* V_94 = NULL;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_95 = NULL;
	int32_t V_96 = 0;
	float G_B59_0 = 0.0f;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* G_B77_0 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* G_B75_0 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* G_B76_0 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* G_B80_0 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* G_B78_0 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* G_B79_0 = NULL;
	float G_B90_0 = 0.0f;
	float G_B93_0 = 0.0f;
	float G_B96_0 = 0.0f;
	float G_B99_0 = 0.0f;
	int32_t G_B117_0 = 0;
	PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* G_B120_0 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* G_B122_0 = NULL;
	PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* G_B121_0 = NULL;
	{
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_0 = (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*)il2cpp_codegen_object_new(List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_m7E6E0C4AE37ACBC1E9BDA9E56A8A7D8ACED3FD33(L_0, 3, List_1__ctor_m7E6E0C4AE37ACBC1E9BDA9E56A8A7D8ACED3FD33_RuntimeMethod_var);
		__this->____allVertices_3 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____allVertices_3), (void*)L_0);
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_1 = (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*)il2cpp_codegen_object_new(List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_m7E6E0C4AE37ACBC1E9BDA9E56A8A7D8ACED3FD33(L_1, 3, List_1__ctor_m7E6E0C4AE37ACBC1E9BDA9E56A8A7D8ACED3FD33_RuntimeMethod_var);
		__this->____allNormals_4 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____allNormals_4), (void*)L_1);
		List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF* L_2 = (List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF*)il2cpp_codegen_object_new(List_1_t242CDEAEC9C92000DA96982CDB9D592DDE2AADAF_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		List_1__ctor_mDBC3F8F2846CD821DA096BECD6300438E2409BC3(L_2, 3, List_1__ctor_mDBC3F8F2846CD821DA096BECD6300438E2409BC3_RuntimeMethod_var);
		__this->____allColors_5 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____allColors_5), (void*)L_2);
		List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B* L_3 = (List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B*)il2cpp_codegen_object_new(List_1_t8F3790B7F8C471B3A1336522C7415FB0AC36D47B_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		List_1__ctor_m27475592735C318973899086F95036A18B6D7E39(L_3, 3, List_1__ctor_m27475592735C318973899086F95036A18B6D7E39_RuntimeMethod_var);
		__this->____allUVs_6 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____allUVs_6), (void*)L_3);
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_4 = ___0_plyReader;
		__this->____reader_2 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____reader_2), (void*)L_4);
		Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* L_5 = (Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC*)il2cpp_codegen_object_new(Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		Dictionary_2__ctor_m4795CFCFA1CA4807CD85E1818CEC2A7EBC2E336C(L_5, Dictionary_2__ctor_m4795CFCFA1CA4807CD85E1818CEC2A7EBC2E336C_RuntimeMethod_var);
		V_0 = L_5;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_6 = (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5*)il2cpp_codegen_object_new(List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		List_1__ctor_mEEA3C6B55707EC6755329113F4C364960AA66CEF(L_6, List_1__ctor_mEEA3C6B55707EC6755329113F4C364960AA66CEF_RuntimeMethod_var);
		V_1 = L_6;
		V_2 = (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8*)NULL;
		V_3 = (bool)0;
		V_4 = (bool)0;
		V_5 = (bool)0;
		V_6 = (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C*)NULL;
		List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31* L_7 = (List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31*)il2cpp_codegen_object_new(List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		List_1__ctor_m71DCE2DD53C6EFD6DF99B2592A347DFD0F9106C3(L_7, List_1__ctor_m71DCE2DD53C6EFD6DF99B2592A347DFD0F9106C3_RuntimeMethod_var);
		V_7 = L_7;
		V_8 = (bool)0;
		V_9 = (bool)0;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_8 = ___1_stream;
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_9 = (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2*)il2cpp_codegen_object_new(PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2_il2cpp_TypeInfo_var);
		NullCheck(L_9);
		PlyStreamReader__ctor_mBCDE6CC4B0352A1622093F74DBC3FE0CFDC678F0(L_9, L_8, NULL);
		V_10 = L_9;
		goto IL_027d;
	}

IL_006a:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_10 = V_10;
		NullCheck(L_10);
		int64_t L_11;
		L_11 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_10, (bool)0, (bool)0, NULL);
		V_16 = L_11;
		int64_t L_12 = V_16;
		if ((((int64_t)L_12) > ((int64_t)((int64_t)-4898810336857675590LL))))
		{
			goto IL_00b7;
		}
	}
	{
		int64_t L_13 = V_16;
		if ((((int64_t)L_13) == ((int64_t)((int64_t)-5513532495504198950LL))))
		{
			goto IL_0268;
		}
	}
	{
		int64_t L_14 = V_16;
		if ((((int64_t)L_14) == ((int64_t)((int64_t)-5513532493822467209LL))))
		{
			goto IL_013d;
		}
	}
	{
		int64_t L_15 = V_16;
		if ((((int64_t)L_15) == ((int64_t)((int64_t)-4898810336857675590LL))))
		{
			goto IL_016d;
		}
	}
	{
		goto IL_0279;
	}

IL_00b7:
	{
		int64_t L_16 = V_16;
		if ((((int64_t)L_16) == ((int64_t)((int64_t)-3837289489351516138LL))))
		{
			goto IL_00ea;
		}
	}
	{
		int64_t L_17 = V_16;
		if ((((int64_t)L_17) == ((int64_t)((int64_t)-3351804022671184904LL))))
		{
			goto IL_00e3;
		}
	}
	{
		int64_t L_18 = V_16;
		if ((((int64_t)L_18) == ((int64_t)((int64_t)-1367968407750199268LL))))
		{
			goto IL_010a;
		}
	}
	{
		goto IL_0279;
	}

IL_00e3:
	{
		V_3 = (bool)1;
		goto IL_027d;
	}

IL_00ea:
	{
		bool L_19 = V_3;
		if (L_19)
		{
			goto IL_00f8;
		}
	}
	{
		Exception_t* L_20 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_20);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_20, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralB44FE4F8F197AF89FF50ABAD9C3E395B5E887251)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PlyProcessor_Process_m5EC73F6D1F63A911A1C86747BB95E7948D0D0763_RuntimeMethod_var)));
	}

IL_00f8:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_21 = V_10;
		NullCheck(L_21);
		int64_t L_22;
		L_22 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_21, (bool)0, (bool)0, NULL);
		V_4 = (bool)1;
		goto IL_027d;
	}

IL_010a:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_23 = V_10;
		NullCheck(L_23);
		int64_t L_24;
		L_24 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_23, (bool)1, (bool)0, NULL);
		int64_t L_25 = L_24;
		V_8 = (bool)((((int64_t)L_25) == ((int64_t)((int64_t)6570124500183046315LL)))? 1 : 0);
		V_9 = (bool)((((int64_t)L_25) == ((int64_t)((int64_t)2560988747252765043LL)))? 1 : 0);
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_26 = V_10;
		NullCheck(L_26);
		int64_t L_27;
		L_27 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_26, (bool)1, (bool)0, NULL);
		goto IL_027d;
	}

IL_013d:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_28 = V_10;
		NullCheck(L_28);
		int64_t L_29;
		L_29 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_28, (bool)1, (bool)0, NULL);
		V_17 = L_29;
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_30 = V_10;
		NullCheck(L_30);
		int32_t L_31;
		L_31 = PlyStreamReader_ToInt32NoValue_m3D62B7AC2B68E0C65837ED4B738D26A02C897086(L_30, NULL);
		V_18 = L_31;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_32 = (PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8*)il2cpp_codegen_object_new(PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8_il2cpp_TypeInfo_var);
		NullCheck(L_32);
		PlyElement__ctor_mCA04905EABE9EB079C06C05A9EBB680CF4A4DBE3(L_32, NULL);
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_33 = L_32;
		int32_t L_34 = V_18;
		NullCheck(L_33);
		L_33->___Count_1 = L_34;
		V_2 = L_33;
		Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* L_35 = V_0;
		int64_t L_36 = V_17;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_37 = V_2;
		NullCheck(L_35);
		Dictionary_2_Add_m7E3EF4351105771F2D6EB167BE29E7868CFC84F0(L_35, L_36, L_37, Dictionary_2_Add_m7E3EF4351105771F2D6EB167BE29E7868CFC84F0_RuntimeMethod_var);
		goto IL_027d;
	}

IL_016d:
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_38 = V_2;
		if (L_38)
		{
			goto IL_017b;
		}
	}
	{
		Exception_t* L_39 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_39);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_39, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral2E7978A9177533BBDC5A8F6FE599F9D9A09593AF)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_39, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PlyProcessor_Process_m5EC73F6D1F63A911A1C86747BB95E7948D0D0763_RuntimeMethod_var)));
	}

IL_017b:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_40 = V_10;
		int32_t L_41;
		L_41 = PlyProcessor_GetPropertyType_mF17237DE1F943422F7DBEB3435F43FD29CFB2A46(L_40, NULL);
		V_19 = L_41;
		int32_t L_42 = V_19;
		if ((!(((uint32_t)L_42) == ((uint32_t)8))))
		{
			goto IL_01ea;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_43 = V_10;
		int32_t L_44;
		L_44 = PlyProcessor_GetPropertyType_mF17237DE1F943422F7DBEB3435F43FD29CFB2A46(L_43, NULL);
		V_20 = L_44;
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_45 = V_10;
		int32_t L_46;
		L_46 = PlyProcessor_GetPropertyType_mF17237DE1F943422F7DBEB3435F43FD29CFB2A46(L_45, NULL);
		V_21 = L_46;
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_47 = V_10;
		NullCheck(L_47);
		int64_t L_48;
		L_48 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_47, (bool)1, (bool)0, NULL);
		V_22 = L_48;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_49 = (PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E*)il2cpp_codegen_object_new(PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E_il2cpp_TypeInfo_var);
		NullCheck(L_49);
		PlyListProperty__ctor_m913A103F9505DB2EC35D68434C7B1676D0F26196(L_49, NULL);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_50 = L_49;
		int32_t L_51 = V_19;
		NullCheck(L_50);
		((PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362*)L_50)->___Type_0 = L_51;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_52 = L_50;
		int32_t L_53 = V_20;
		NullCheck(L_52);
		L_52->___CounterType_2 = L_53;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_54 = L_52;
		int32_t L_55 = V_21;
		NullCheck(L_54);
		L_54->___ItemType_3 = L_55;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_56 = L_54;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_57 = V_2;
		NullCheck(L_57);
		Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_58 = L_57->___Properties_0;
		NullCheck(L_58);
		int32_t L_59;
		L_59 = Dictionary_2_get_Count_m270BB8BF6E2728FF1BE69D236B3C1BB5E5D60BFD(L_58, Dictionary_2_get_Count_m270BB8BF6E2728FF1BE69D236B3C1BB5E5D60BFD_RuntimeMethod_var);
		NullCheck(L_56);
		((PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362*)L_56)->___Offset_1 = L_59;
		V_23 = L_56;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_60 = V_2;
		NullCheck(L_60);
		Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_61 = L_60->___Properties_0;
		int64_t L_62 = V_22;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_63 = V_23;
		NullCheck(L_61);
		Dictionary_2_Add_m13BA45BD072C072ECDC20ED17980959D50DCDEBE(L_61, L_62, L_63, Dictionary_2_Add_m13BA45BD072C072ECDC20ED17980959D50DCDEBE_RuntimeMethod_var);
		goto IL_027d;
	}

IL_01ea:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_64 = V_10;
		NullCheck(L_64);
		int64_t L_65;
		L_65 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_64, (bool)1, (bool)0, NULL);
		V_24 = L_65;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_66 = (PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362*)il2cpp_codegen_object_new(PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362_il2cpp_TypeInfo_var);
		NullCheck(L_66);
		PlyProperty__ctor_mB9404ABEA64DE21D9C16B8FCF8FA95FEA5C8221F(L_66, NULL);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_67 = L_66;
		int32_t L_68 = V_19;
		NullCheck(L_67);
		L_67->___Type_0 = L_68;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_69 = L_67;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_70 = V_2;
		NullCheck(L_70);
		Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_71 = L_70->___Properties_0;
		NullCheck(L_71);
		int32_t L_72;
		L_72 = Dictionary_2_get_Count_m270BB8BF6E2728FF1BE69D236B3C1BB5E5D60BFD(L_71, Dictionary_2_get_Count_m270BB8BF6E2728FF1BE69D236B3C1BB5E5D60BFD_RuntimeMethod_var);
		NullCheck(L_69);
		L_69->___Offset_1 = L_72;
		V_25 = L_69;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_73 = V_2;
		NullCheck(L_73);
		Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_74 = L_73->___Properties_0;
		int64_t L_75 = V_24;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_76 = V_25;
		NullCheck(L_74);
		Dictionary_2_Add_m13BA45BD072C072ECDC20ED17980959D50DCDEBE(L_74, L_75, L_76, Dictionary_2_Add_m13BA45BD072C072ECDC20ED17980959D50DCDEBE_RuntimeMethod_var);
		goto IL_027d;
	}

IL_0226:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_77 = V_10;
		NullCheck(L_77);
		int64_t L_78;
		L_78 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_77, (bool)0, (bool)0, NULL);
		V_16 = L_78;
		bool L_79 = V_5;
		if (!L_79)
		{
			goto IL_0258;
		}
	}
	{
		PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* L_80 = (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C*)il2cpp_codegen_object_new(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C_il2cpp_TypeInfo_var);
		NullCheck(L_80);
		PlyTexture__ctor_m038AAFCCC1A0BFA24B9DB741380AA1D0CDE29AFD(L_80, NULL);
		V_6 = L_80;
		PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* L_81 = V_6;
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_82 = V_10;
		NullCheck(L_82);
		String_t* L_83;
		L_83 = PlyStreamReader_GetTokenAsString_m7E04224D04CDC8D8F4791995529AB17E0CEEDCAA(L_82, NULL);
		NullCheck(L_81);
		PlyTexture_set_Filename_m7FA26BB14F1E9F04652E891D67858519F9040BD8_inline(L_81, L_83, NULL);
		V_5 = (bool)0;
		List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31* L_84 = V_7;
		PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* L_85 = V_6;
		NullCheck(L_84);
		List_1_Add_m096BE2FF8568855341E87C1DE6291916F89A8203_inline(L_84, L_85, List_1_Add_m096BE2FF8568855341E87C1DE6291916F89A8203_RuntimeMethod_var);
		goto IL_0268;
	}

IL_0258:
	{
		int64_t L_86 = V_16;
		if ((!(((uint64_t)L_86) == ((uint64_t)((int64_t)-8289663717619634158LL)))))
		{
			goto IL_0268;
		}
	}
	{
		V_5 = (bool)1;
	}

IL_0268:
	{
		int64_t L_87 = V_16;
		if (L_87)
		{
			goto IL_0226;
		}
	}
	{
		goto IL_027d;
	}

IL_026e:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_88 = V_10;
		NullCheck(L_88);
		int64_t L_89;
		L_89 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_88, (bool)0, (bool)0, NULL);
		V_16 = L_89;
	}

IL_0279:
	{
		int64_t L_90 = V_16;
		if (L_90)
		{
			goto IL_026e;
		}
	}

IL_027d:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_91 = V_10;
		NullCheck(L_91);
		bool L_92;
		L_92 = StreamReader_get_EndOfStream_mAE054431BF21158178EAA2A6872F14A9ED6A3C3E(L_91, NULL);
		if (L_92)
		{
			goto IL_028d;
		}
	}
	{
		bool L_93 = V_4;
		if (!L_93)
		{
			goto IL_006a;
		}
	}

IL_028d:
	{
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_94 = ___0_plyReader;
		NullCheck(L_94);
		ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C(L_94, (1.0f), 0, (0.0f), NULL);
		bool L_95 = V_8;
		if (!L_95)
		{
			goto IL_02c5;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_96 = V_10;
		NullCheck(L_96);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_97;
		L_97 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(18 /* System.IO.Stream System.IO.StreamReader::get_BaseStream() */, L_96);
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_98 = (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158*)il2cpp_codegen_object_new(BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158_il2cpp_TypeInfo_var);
		NullCheck(L_98);
		BinaryReader__ctor_m898732FE0DBEDD480B24F6DE45A9AC696E44CC0F(L_98, L_97, NULL);
		V_11 = L_98;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_99 = V_11;
		NullCheck(L_99);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_100;
		L_100 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_99);
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_101 = V_10;
		NullCheck(L_101);
		int64_t L_102;
		L_102 = PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B_inline(L_101, NULL);
		NullCheck(L_100);
		VirtualActionInvoker1< int64_t >::Invoke(13 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_100, L_102);
		goto IL_02c8;
	}

IL_02c5:
	{
		V_11 = (BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158*)NULL;
	}

IL_02c8:
	{
		bool L_103 = V_9;
		if (!L_103)
		{
			goto IL_02ef;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_104 = V_10;
		NullCheck(L_104);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_105;
		L_105 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(18 /* System.IO.Stream System.IO.StreamReader::get_BaseStream() */, L_104);
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_106 = (BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721*)il2cpp_codegen_object_new(BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721_il2cpp_TypeInfo_var);
		NullCheck(L_106);
		BigEndianBinaryReader__ctor_m9E745E5732B70DC31E50C8C6B8DE7DB5956D0EBA(L_106, L_105, NULL);
		V_12 = L_106;
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_107 = V_12;
		NullCheck(L_107);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_108;
		L_108 = VirtualFuncInvoker0< Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* >::Invoke(5 /* System.IO.Stream System.IO.BinaryReader::get_BaseStream() */, L_107);
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_109 = V_10;
		NullCheck(L_109);
		int64_t L_110;
		L_110 = PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B_inline(L_109, NULL);
		NullCheck(L_108);
		VirtualActionInvoker1< int64_t >::Invoke(13 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_108, L_110);
		goto IL_02f2;
	}

IL_02ef:
	{
		V_12 = (BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721*)NULL;
	}

IL_02f2:
	{
		Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* L_111 = V_0;
		NullCheck(L_111);
		ValueCollection_t16B888ACAE62FB5B6EB90AB83B113259C434EC38* L_112;
		L_112 = Dictionary_2_get_Values_mD47C7DCD10B474F8DB55896C6B5F276ACA9E2BC1(L_111, Dictionary_2_get_Values_mD47C7DCD10B474F8DB55896C6B5F276ACA9E2BC1_RuntimeMethod_var);
		NullCheck(L_112);
		Enumerator_t69077E1CF02F307242D159C9DA30E7B9A2D5C901 L_113;
		L_113 = ValueCollection_GetEnumerator_mD927D6AC4BE78DAED8A83775F0D1FEF91A756B4D(L_112, ValueCollection_GetEnumerator_mD927D6AC4BE78DAED8A83775F0D1FEF91A756B4D_RuntimeMethod_var);
		V_26 = L_113;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_03bd:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m16443C4F8A0449761F7E9532DA13EE62BC66A959((&V_26), Enumerator_Dispose_m16443C4F8A0449761F7E9532DA13EE62BC66A959_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_03af_1;
			}

IL_0304_1:
			{
				PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_114;
				L_114 = Enumerator_get_Current_m16B76A6F6611FB82F01BB3EA7B36F6BE9AC3B558_inline((&V_26), Enumerator_get_Current_m16B76A6F6611FB82F01BB3EA7B36F6BE9AC3B558_RuntimeMethod_var);
				V_27 = L_114;
				PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_115 = V_27;
				NullCheck(L_115);
				int32_t L_116 = L_115->___Count_1;
				List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_117 = (List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5*)il2cpp_codegen_object_new(List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5_il2cpp_TypeInfo_var);
				NullCheck(L_117);
				List_1__ctor_m179CF1BDD503F830F79CF1A81033083C1BB19E05(L_117, L_116, List_1__ctor_m179CF1BDD503F830F79CF1A81033083C1BB19E05_RuntimeMethod_var);
				V_28 = L_117;
				V_29 = 0;
				goto IL_0398_1;
			}

IL_0320_1:
			{
				PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_118 = V_27;
				NullCheck(L_118);
				Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_119 = L_118->___Properties_0;
				NullCheck(L_119);
				int32_t L_120;
				L_120 = Dictionary_2_get_Count_m270BB8BF6E2728FF1BE69D236B3C1BB5E5D60BFD(L_119, Dictionary_2_get_Count_m270BB8BF6E2728FF1BE69D236B3C1BB5E5D60BFD_RuntimeMethod_var);
				List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_121 = (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*)il2cpp_codegen_object_new(List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB_il2cpp_TypeInfo_var);
				NullCheck(L_121);
				List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122(L_121, L_120, List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122_RuntimeMethod_var);
				V_30 = L_121;
				PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_122 = V_27;
				NullCheck(L_122);
				Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* L_123 = L_122->___Properties_0;
				NullCheck(L_123);
				ValueCollection_t17A15458500B928C18C734F4964D798B1C114EE9* L_124;
				L_124 = Dictionary_2_get_Values_m66C7C3835140D396B2941C5FF726A8435575E421(L_123, Dictionary_2_get_Values_m66C7C3835140D396B2941C5FF726A8435575E421_RuntimeMethod_var);
				NullCheck(L_124);
				Enumerator_t7D873153703A01D83B21E276B8C4436FC283A3C8 L_125;
				L_125 = ValueCollection_GetEnumerator_m7A2633FD76AC8804D68EEB036B996CAF5FDC7754(L_124, ValueCollection_GetEnumerator_m7A2633FD76AC8804D68EEB036B996CAF5FDC7754_RuntimeMethod_var);
				V_31 = L_125;
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_037b_1:
					{// begin finally (depth: 2)
						Enumerator_Dispose_mFB43E311F648679BE046A19C9351234B9C43A1CF((&V_31), Enumerator_Dispose_mFB43E311F648679BE046A19C9351234B9C43A1CF_RuntimeMethod_var);
						return;
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					{
						goto IL_0370_2;
					}

IL_0348_2:
					{
						PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_126;
						L_126 = Enumerator_get_Current_mA527966D30259A4C4C030B11F4DFEE3029C1076F_inline((&V_31), Enumerator_get_Current_mA527966D30259A4C4C030B11F4DFEE3029C1076F_RuntimeMethod_var);
						PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_127 = L_126;
						NullCheck(L_127);
						int32_t L_128 = L_127->___Type_0;
						bool L_129 = V_8;
						BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_130 = V_11;
						bool L_131 = V_9;
						PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_132 = V_10;
						BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_133 = V_12;
						List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_134 = V_1;
						PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_135;
						L_135 = PlyProcessor_ReadElementData_m295BBB280885FD705DF14E08D8FE375B03CB984B(L_127, L_128, L_129, L_130, L_131, L_132, L_133, L_134, NULL);
						V_32 = L_135;
						List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_136 = V_30;
						PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_137 = V_32;
						NullCheck(L_136);
						List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_inline(L_136, L_137, List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_RuntimeMethod_var);
					}

IL_0370_2:
					{
						bool L_138;
						L_138 = Enumerator_MoveNext_m355F0050D35C5CECAB616FAD6BD09BCDF75ED2AD((&V_31), Enumerator_MoveNext_m355F0050D35C5CECAB616FAD6BD09BCDF75ED2AD_RuntimeMethod_var);
						if (L_138)
						{
							goto IL_0348_2;
						}
					}
					{
						goto IL_0389_1;
					}
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}

IL_0389_1:
			{
				List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_139 = V_28;
				List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_140 = V_30;
				NullCheck(L_139);
				List_1_Add_m7C99F2FA69D684BD5B7E22B8A115DA258EA04CB2_inline(L_139, L_140, List_1_Add_m7C99F2FA69D684BD5B7E22B8A115DA258EA04CB2_RuntimeMethod_var);
				int32_t L_141 = V_29;
				V_29 = ((int32_t)il2cpp_codegen_add(L_141, 1));
			}

IL_0398_1:
			{
				int32_t L_142 = V_29;
				PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_143 = V_27;
				NullCheck(L_143);
				int32_t L_144 = L_143->___Count_1;
				if ((((int32_t)L_142) < ((int32_t)L_144)))
				{
					goto IL_0320_1;
				}
			}
			{
				PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_145 = V_27;
				List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_146 = V_28;
				NullCheck(L_145);
				L_145->___Data_2 = L_146;
				Il2CppCodeGenWriteBarrier((void**)(&L_145->___Data_2), (void*)L_146);
			}

IL_03af_1:
			{
				bool L_147;
				L_147 = Enumerator_MoveNext_m3A6AF59F5158004E0E8F5D30912F0B3D578BAD20((&V_26), Enumerator_MoveNext_m3A6AF59F5158004E0E8F5D30912F0B3D578BAD20_RuntimeMethod_var);
				if (L_147)
				{
					goto IL_0304_1;
				}
			}
			{
				goto IL_03cb;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_03cb:
	{
		V_13 = (RuntimeObject*)NULL;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_148 = ___0_plyReader;
		NullCheck(L_148);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_149;
		L_149 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_148, NULL);
		NullCheck(L_149);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_150 = L_149->___Options_0;
		NullCheck(L_150);
		bool L_151 = L_150->___LoadPointClouds_92;
		if (L_151)
		{
			goto IL_05b6;
		}
	}
	{
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_152 = ___0_plyReader;
		NullCheck(L_152);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_153;
		L_153 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_152, NULL);
		NullCheck(L_153);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_154 = L_153->___Options_0;
		NullCheck(L_154);
		bool L_155 = L_154->___ImportMaterials_31;
		if (!L_155)
		{
			goto IL_05b6;
		}
	}
	{
		Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* L_156 = V_0;
		NullCheck(L_156);
		bool L_157;
		L_157 = Dictionary_2_TryGetValue_m181BAACEB7905BC572C38343BF7DDA866E9415FD(L_156, ((int64_t)-4898810434349715444LL), (&V_33), Dictionary_2_TryGetValue_m181BAACEB7905BC572C38343BF7DDA866E9415FD_RuntimeMethod_var);
		if (!L_157)
		{
			goto IL_057a;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_158 = V_33;
		NullCheck(L_158);
		int32_t L_159 = L_158->___Count_1;
		IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2* L_160 = (IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2*)(IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2*)SZArrayNew(IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2_il2cpp_TypeInfo_var, (uint32_t)L_159);
		V_13 = (RuntimeObject*)L_160;
		V_34 = 0;
		goto IL_056c;
	}

IL_0424:
	{
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_161 = (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6*)il2cpp_codegen_object_new(PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6_il2cpp_TypeInfo_var);
		NullCheck(L_161);
		PlyMaterial__ctor_m9669AF4D43AE12DEEBD8F46D99D869AC68470623(L_161, NULL);
		V_35 = L_161;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_162 = V_33;
		int32_t L_163 = V_34;
		NullCheck(L_162);
		float L_164;
		L_164 = PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713(L_162, ((int64_t)-8276459643725187793LL), L_163, NULL);
		V_36 = ((float)((255.0f)/L_164));
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_165 = V_33;
		int32_t L_166 = V_34;
		NullCheck(L_165);
		float L_167;
		L_167 = PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713(L_165, ((int64_t)-3131021851098489823LL), L_166, NULL);
		V_37 = ((float)((255.0f)/L_167));
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_168 = V_33;
		int32_t L_169 = V_34;
		NullCheck(L_168);
		float L_170;
		L_170 = PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713(L_168, ((int64_t)1684168076452431740LL), L_169, NULL);
		V_38 = ((float)((255.0f)/L_170));
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_171 = V_35;
		float L_172 = V_36;
		float L_173 = V_37;
		float L_174 = V_38;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_175;
		memset((&L_175), 0, sizeof(L_175));
		Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline((&L_175), L_172, L_173, L_174, /*hidden argument*/NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_176 = L_175;
		RuntimeObject* L_177 = Box(Color_tD001788D726C3A7F1379BEED0260B9591F440C1F_il2cpp_TypeInfo_var, &L_176);
		NullCheck(L_171);
		PlyMaterial_AddProperty_m4FE8AE6161D498A95286AC015BD2E1B2EC41E3B4(L_171, _stringLiteral31A9B06BE46B0E2BB27797EDC5BAE6C2BFA4ABF0, L_177, (bool)0, NULL);
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_178 = V_33;
		int32_t L_179 = V_34;
		NullCheck(L_178);
		float L_180;
		L_180 = PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713(L_178, ((int64_t)2071003623302517242LL), L_179, NULL);
		V_39 = ((float)((255.0f)/L_180));
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_181 = V_33;
		int32_t L_182 = V_34;
		NullCheck(L_181);
		float L_183;
		L_183 = PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713(L_181, ((int64_t)-2013877966922272212LL), L_182, NULL);
		V_40 = ((float)((255.0f)/L_183));
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_184 = V_33;
		int32_t L_185 = V_34;
		NullCheck(L_184);
		float L_186;
		L_186 = PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713(L_184, ((int64_t)8860880101248910353LL), L_185, NULL);
		V_41 = ((float)((255.0f)/L_186));
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_187 = V_35;
		float L_188 = V_39;
		float L_189 = V_40;
		float L_190 = V_41;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_191;
		memset((&L_191), 0, sizeof(L_191));
		Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline((&L_191), L_188, L_189, L_190, /*hidden argument*/NULL);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_192 = L_191;
		RuntimeObject* L_193 = Box(Color_tD001788D726C3A7F1379BEED0260B9591F440C1F_il2cpp_TypeInfo_var, &L_192);
		NullCheck(L_187);
		PlyMaterial_AddProperty_m4FE8AE6161D498A95286AC015BD2E1B2EC41E3B4(L_187, _stringLiteral711AA001951412D09872DB5FA0B90EA6875A17F9, L_193, (bool)0, NULL);
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_194 = V_33;
		NullCheck(L_194);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_195;
		L_195 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_194, ((int64_t)-5513532484836901754LL), NULL);
		V_42 = L_195;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_196 = V_42;
		if (!L_196)
		{
			goto IL_0524;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_197 = V_33;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_198 = V_42;
		int32_t L_199 = V_34;
		NullCheck(L_197);
		float L_200;
		L_200 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_197, L_198, L_199, NULL);
		G_B59_0 = L_200;
		goto IL_0529;
	}

IL_0524:
	{
		G_B59_0 = (1.0f);
	}

IL_0529:
	{
		V_43 = G_B59_0;
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_201 = V_35;
		float L_202 = V_43;
		float L_203 = L_202;
		RuntimeObject* L_204 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_203);
		NullCheck(L_201);
		PlyMaterial_AddProperty_m4FE8AE6161D498A95286AC015BD2E1B2EC41E3B4(L_201, _stringLiteral5D934867D69D7E986A4C224DB49CF270468DE64D, L_204, (bool)0, NULL);
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_205 = V_35;
		int32_t L_206 = V_34;
		NullCheck(L_205);
		PlyMaterial_set_Index_mEEC9E37FB853F46E7A19C7A52BE8EACD41464726_inline(L_205, L_206, NULL);
		PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* L_207 = V_6;
		if (!L_207)
		{
			goto IL_055b;
		}
	}
	{
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_208 = V_35;
		PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* L_209 = V_6;
		NullCheck(L_208);
		PlyMaterial_AddProperty_m4FE8AE6161D498A95286AC015BD2E1B2EC41E3B4(L_208, _stringLiteral7A12A09E76D9C6C6FAD55C385BB87538EF591395, L_209, (bool)1, NULL);
	}

IL_055b:
	{
		RuntimeObject* L_210 = V_13;
		int32_t L_211 = V_34;
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_212 = V_35;
		NullCheck(L_210);
		InterfaceActionInvoker2< int32_t, RuntimeObject* >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>::set_Item(System.Int32,T) */, IList_1_t0662D113B996C51F1676FFC848F7B3448D818DB7_il2cpp_TypeInfo_var, L_210, L_211, L_212);
		int32_t L_213 = V_34;
		V_34 = ((int32_t)il2cpp_codegen_add(L_213, 1));
	}

IL_056c:
	{
		int32_t L_214 = V_34;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_215 = V_33;
		NullCheck(L_215);
		int32_t L_216 = L_215->___Count_1;
		if ((((int32_t)L_214) < ((int32_t)L_216)))
		{
			goto IL_0424;
		}
	}

IL_057a:
	{
		RuntimeObject* L_217 = V_13;
		if (L_217)
		{
			goto IL_05a5;
		}
	}
	{
		PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* L_218 = V_6;
		if (!L_218)
		{
			goto IL_05a5;
		}
	}
	{
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_219 = (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6*)il2cpp_codegen_object_new(PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6_il2cpp_TypeInfo_var);
		NullCheck(L_219);
		PlyMaterial__ctor_m9669AF4D43AE12DEEBD8F46D99D869AC68470623(L_219, NULL);
		V_44 = L_219;
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_220 = V_44;
		PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* L_221 = V_6;
		NullCheck(L_220);
		PlyMaterial_AddProperty_m4FE8AE6161D498A95286AC015BD2E1B2EC41E3B4(L_220, _stringLiteral7A12A09E76D9C6C6FAD55C385BB87538EF591395, L_221, (bool)1, NULL);
		IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2* L_222 = (IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2*)(IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2*)SZArrayNew(IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2_il2cpp_TypeInfo_var, (uint32_t)1);
		IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2* L_223 = L_222;
		PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* L_224 = V_44;
		NullCheck(L_223);
		ArrayElementTypeCheck (L_223, L_224);
		(L_223)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_224);
		V_13 = (RuntimeObject*)L_223;
	}

IL_05a5:
	{
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_225 = ___0_plyReader;
		NullCheck(L_225);
		ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C(L_225, (1.0f), 1, (0.0f), NULL);
	}

IL_05b6:
	{
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_226 = ___0_plyReader;
		NullCheck(L_226);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_227;
		L_227 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_226, NULL);
		NullCheck(L_227);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_228 = L_227->___Options_0;
		NullCheck(L_228);
		bool L_229 = L_228->___ImportMeshes_10;
		if (!L_229)
		{
			goto IL_089b;
		}
	}
	{
		Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* L_230 = V_0;
		NullCheck(L_230);
		bool L_231;
		L_231 = Dictionary_2_TryGetValue_m181BAACEB7905BC572C38343BF7DDA866E9415FD(L_230, ((int64_t)-1367968407301361207LL), (&V_14), Dictionary_2_TryGetValue_m181BAACEB7905BC572C38343BF7DDA866E9415FD_RuntimeMethod_var);
		if (!L_231)
		{
			goto IL_089b;
		}
	}
	{
		V_45 = 0;
		goto IL_087c;
	}

IL_05e9:
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_232 = V_14;
		int32_t L_233 = V_45;
		NullCheck(L_232);
		float L_234;
		L_234 = PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713(L_232, ((int64_t)34902897112120627LL), L_233, NULL);
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_235 = V_14;
		int32_t L_236 = V_45;
		NullCheck(L_235);
		float L_237;
		L_237 = PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713(L_235, ((int64_t)34902897112120628LL), L_236, NULL);
		V_46 = L_237;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_238 = V_14;
		int32_t L_239 = V_45;
		NullCheck(L_238);
		float L_240;
		L_240 = PlyElement_GetPropertyFloatValue_m9EEC47397083DC6B0C2DB937F43F12288EBF5713(L_238, ((int64_t)34902897112120629LL), L_239, NULL);
		V_47 = L_240;
		float L_241 = V_46;
		float L_242 = V_47;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_243;
		memset((&L_243), 0, sizeof(L_243));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_243), ((-L_234)), L_241, L_242, /*hidden argument*/NULL);
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_244 = __this->____reader_2;
		NullCheck(L_244);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_245;
		L_245 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_244, NULL);
		NullCheck(L_245);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_246 = L_245->___Options_0;
		NullCheck(L_246);
		float L_247 = L_246->___ScaleFactor_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_248;
		L_248 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_243, L_247, NULL);
		V_48 = L_248;
		RuntimeObject* L_249 = __this->____allVertices_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_250 = V_48;
		NullCheck(L_249);
		InterfaceActionInvoker1< Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Add(T) */, ICollection_1_tEF23B8A78E9122DAA952DB1517D31EB7EC90563B_il2cpp_TypeInfo_var, L_249, L_250);
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_251 = V_14;
		NullCheck(L_251);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_252;
		L_252 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_251, ((int64_t)1081989810475739247LL), NULL);
		V_49 = L_252;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_253 = V_14;
		NullCheck(L_253);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_254;
		L_254 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_253, ((int64_t)1081989810475739248LL), NULL);
		V_50 = L_254;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_255 = V_14;
		NullCheck(L_255);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_256;
		L_256 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_255, ((int64_t)1081989810475739249LL), NULL);
		V_51 = L_256;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_257 = V_49;
		if (!L_257)
		{
			goto IL_06d9;
		}
	}
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_258 = V_50;
		if (!L_258)
		{
			goto IL_06d9;
		}
	}
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_259 = V_51;
		if (!L_259)
		{
			goto IL_06d9;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_260 = V_14;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_261 = V_49;
		int32_t L_262 = V_45;
		NullCheck(L_260);
		float L_263;
		L_263 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_260, L_261, L_262, NULL);
		V_58 = L_263;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_264 = V_14;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_265 = V_50;
		int32_t L_266 = V_45;
		NullCheck(L_264);
		float L_267;
		L_267 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_264, L_265, L_266, NULL);
		V_59 = L_267;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_268 = V_14;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_269 = V_51;
		int32_t L_270 = V_45;
		NullCheck(L_268);
		float L_271;
		L_271 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_268, L_269, L_270, NULL);
		V_60 = L_271;
		float L_272 = V_58;
		float L_273 = V_59;
		float L_274 = V_60;
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&V_61), L_272, L_273, L_274, NULL);
		RuntimeObject* L_275 = __this->____allNormals_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_276 = V_61;
		NullCheck(L_275);
		InterfaceActionInvoker1< Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::Add(T) */, ICollection_1_tEF23B8A78E9122DAA952DB1517D31EB7EC90563B_il2cpp_TypeInfo_var, L_275, L_276);
	}

IL_06d9:
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_277 = V_14;
		NullCheck(L_277);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_278;
		L_278 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_277, ((int64_t)34902897112120624LL), NULL);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_279 = L_278;
		G_B75_0 = L_279;
		if (L_279)
		{
			G_B77_0 = L_279;
			goto IL_0711;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_280 = V_14;
		NullCheck(L_280);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_281;
		L_281 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_280, ((int64_t)34902897112120622LL), NULL);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_282 = L_281;
		G_B76_0 = L_282;
		if (L_282)
		{
			G_B77_0 = L_282;
			goto IL_0711;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_283 = V_14;
		NullCheck(L_283);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_284;
		L_284 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_283, ((int64_t)-4289164790894553076LL), NULL);
		G_B77_0 = L_284;
	}

IL_0711:
	{
		V_52 = G_B77_0;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_285 = V_14;
		NullCheck(L_285);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_286;
		L_286 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_285, ((int64_t)34902897112120625LL), NULL);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_287 = L_286;
		G_B78_0 = L_287;
		if (L_287)
		{
			G_B80_0 = L_287;
			goto IL_074b;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_288 = V_14;
		NullCheck(L_288);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_289;
		L_289 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_288, ((int64_t)34902897112120623LL), NULL);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_290 = L_289;
		G_B79_0 = L_290;
		if (L_290)
		{
			G_B80_0 = L_290;
			goto IL_074b;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_291 = V_14;
		NullCheck(L_291);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_292;
		L_292 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_291, ((int64_t)-4289164790894553075LL), NULL);
		G_B80_0 = L_292;
	}

IL_074b:
	{
		V_53 = G_B80_0;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_293 = V_52;
		if (!L_293)
		{
			goto IL_0787;
		}
	}
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_294 = V_53;
		if (!L_294)
		{
			goto IL_0787;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_295 = V_14;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_296 = V_52;
		int32_t L_297 = V_45;
		NullCheck(L_295);
		float L_298;
		L_298 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_295, L_296, L_297, NULL);
		V_62 = L_298;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_299 = V_14;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_300 = V_53;
		int32_t L_301 = V_45;
		NullCheck(L_299);
		float L_302;
		L_302 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_299, L_300, L_301, NULL);
		V_63 = L_302;
		float L_303 = V_62;
		float L_304 = V_63;
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_64), L_303, L_304, NULL);
		RuntimeObject* L_305 = __this->____allUVs_6;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_306 = V_64;
		NullCheck(L_305);
		InterfaceActionInvoker1< Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::Add(T) */, ICollection_1_t77739521790EB7E3F514D44638D90AB775EDE8ED_il2cpp_TypeInfo_var, L_305, L_306);
	}

IL_0787:
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_307 = V_14;
		NullCheck(L_307);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_308;
		L_308 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_307, ((int64_t)-3351804022671183220LL), NULL);
		V_54 = L_308;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_309 = V_14;
		NullCheck(L_309);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_310;
		L_310 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_309, ((int64_t)7096547112154691134LL), NULL);
		V_55 = L_310;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_311 = V_14;
		NullCheck(L_311);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_312;
		L_312 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_311, ((int64_t)6774539739450160575LL), NULL);
		V_56 = L_312;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_313 = V_14;
		NullCheck(L_313);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_314;
		L_314 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_313, ((int64_t)7096547112148981913LL), NULL);
		V_57 = L_314;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_315 = V_54;
		if (L_315)
		{
			goto IL_07e2;
		}
	}
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_316 = V_55;
		if (L_316)
		{
			goto IL_07e2;
		}
	}
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_317 = V_56;
		if (L_317)
		{
			goto IL_07e2;
		}
	}
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_318 = V_57;
		if (!L_318)
		{
			goto IL_0876;
		}
	}

IL_07e2:
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_319 = V_54;
		if (!L_319)
		{
			goto IL_07f3;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_320 = V_14;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_321 = V_54;
		int32_t L_322 = V_45;
		NullCheck(L_320);
		float L_323;
		L_323 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_320, L_321, L_322, NULL);
		G_B90_0 = L_323;
		goto IL_07f8;
	}

IL_07f3:
	{
		G_B90_0 = (1.0f);
	}

IL_07f8:
	{
		V_65 = G_B90_0;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_324 = V_55;
		if (!L_324)
		{
			goto IL_080b;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_325 = V_14;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_326 = V_55;
		int32_t L_327 = V_45;
		NullCheck(L_325);
		float L_328;
		L_328 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_325, L_326, L_327, NULL);
		G_B93_0 = L_328;
		goto IL_0810;
	}

IL_080b:
	{
		G_B93_0 = (1.0f);
	}

IL_0810:
	{
		V_66 = G_B93_0;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_329 = V_56;
		if (!L_329)
		{
			goto IL_0823;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_330 = V_14;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_331 = V_56;
		int32_t L_332 = V_45;
		NullCheck(L_330);
		float L_333;
		L_333 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_330, L_331, L_332, NULL);
		G_B96_0 = L_333;
		goto IL_0828;
	}

IL_0823:
	{
		G_B96_0 = (1.0f);
	}

IL_0828:
	{
		V_67 = G_B96_0;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_334 = V_57;
		if (!L_334)
		{
			goto IL_083b;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_335 = V_14;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_336 = V_57;
		int32_t L_337 = V_45;
		NullCheck(L_335);
		float L_338;
		L_338 = PlyElement_GetPropertyFloatValue_m7E87CAEA23D5D33F126AE7A44F4D6B28CD2283DC(L_335, L_336, L_337, NULL);
		G_B99_0 = L_338;
		goto IL_0840;
	}

IL_083b:
	{
		G_B99_0 = (1.0f);
	}

IL_0840:
	{
		V_68 = G_B99_0;
		float L_339 = V_65;
		float L_340 = V_66;
		float L_341 = V_67;
		float L_342 = V_68;
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&V_69), ((float)(L_339/(255.0f))), ((float)(L_340/(255.0f))), ((float)(L_341/(255.0f))), ((float)(L_342/(255.0f))), NULL);
		RuntimeObject* L_343 = __this->____allColors_5;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_344 = V_69;
		NullCheck(L_343);
		InterfaceActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<UnityEngine.Color>::Add(T) */, ICollection_1_tFC51DD83E1437529AA19214EFADD5EE83B73CD6E_il2cpp_TypeInfo_var, L_343, L_344);
	}

IL_0876:
	{
		int32_t L_345 = V_45;
		V_45 = ((int32_t)il2cpp_codegen_add(L_345, 1));
	}

IL_087c:
	{
		int32_t L_346 = V_45;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_347 = V_14;
		NullCheck(L_347);
		int32_t L_348 = L_347->___Count_1;
		if ((((int32_t)L_346) < ((int32_t)L_348)))
		{
			goto IL_05e9;
		}
	}
	{
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_349 = ___0_plyReader;
		NullCheck(L_349);
		ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C(L_349, (1.0f), 2, (0.0f), NULL);
	}

IL_089b:
	{
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_350 = __this->____reader_2;
		NullCheck(L_350);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_351;
		L_351 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_350, NULL);
		int32_t* L_352 = (&__this->____floatCount_7);
		RuntimeObject* L_353 = __this->____allNormals_4;
		NullCheck(L_353);
		int32_t L_354;
		L_354 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_Count() */, ICollection_1_tEF23B8A78E9122DAA952DB1517D31EB7EC90563B_il2cpp_TypeInfo_var, L_353);
		RuntimeObject* L_355 = __this->____allColors_5;
		NullCheck(L_355);
		int32_t L_356;
		L_356 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Color>::get_Count() */, ICollection_1_tFC51DD83E1437529AA19214EFADD5EE83B73CD6E_il2cpp_TypeInfo_var, L_355);
		RuntimeObject* L_357 = __this->____allUVs_6;
		NullCheck(L_357);
		int32_t L_358;
		L_358 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector2>::get_Count() */, ICollection_1_t77739521790EB7E3F514D44638D90AB775EDE8ED_il2cpp_TypeInfo_var, L_357);
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_359 = __this->____reader_2;
		NullCheck(L_359);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_360;
		L_360 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_359, NULL);
		NullCheck(L_360);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_361 = L_360->___Options_0;
		NullCheck(L_361);
		bool L_362 = L_361->___CompressMeshes_94;
		HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* L_363;
		L_363 = FlexibleVertexDataUtils_BuildVertexAttributesDictionary_m38474C4D34ABEA85320910DAC96E8A8016B46888(L_351, L_352, (bool)1, (bool)((((int32_t)L_354) > ((int32_t)0))? 1 : 0), (bool)0, (bool)((((int32_t)L_356) > ((int32_t)0))? 1 : 0), (bool)((((int32_t)L_358) > ((int32_t)0))? 1 : 0), L_362, (bool)0, (bool)0, (bool)0, (bool)0, NULL);
		__this->____vertexAttributes_8 = L_363;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____vertexAttributes_8), (void*)L_363);
		HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* L_364 = __this->____vertexAttributes_8;
		int32_t L_365 = __this->____floatCount_7;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_366 = __this->____reader_2;
		NullCheck(L_366);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_367;
		L_367 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_366, NULL);
		NullCheck(L_367);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_368 = L_367->___Options_0;
		NullCheck(L_368);
		bool L_369 = L_368->___CompressMeshes_94;
		RuntimeObject* L_370;
		L_370 = FlexibleVertexDataUtils_BuildStreamGeometryGroup_m06F110C1559744234BEB5984BB8F228044B7F047(L_364, L_365, L_369, NULL);
		__this->____geometryGroup_9 = L_370;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____geometryGroup_9), (void*)L_370);
		RuntimeObject* L_371 = __this->____geometryGroup_9;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_372 = __this->____reader_2;
		NullCheck(L_372);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_373;
		L_373 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_372, NULL);
		NullCheck(L_371);
		InterfaceActionInvoker4< AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C*, int32_t, int32_t, int32_t >::Invoke(6 /* System.Void TriLibCore.Interfaces.IGeometryGroup::Setup(TriLibCore.AssetLoaderContext,System.Int32,System.Int32,System.Int32) */, IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262_il2cpp_TypeInfo_var, L_371, L_373, 3, 1, 0);
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_374 = ___0_plyReader;
		NullCheck(L_374);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_375;
		L_375 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_374, NULL);
		NullCheck(L_375);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_376 = L_375->___Options_0;
		NullCheck(L_376);
		bool L_377 = L_376->___LoadPointClouds_92;
		if (!L_377)
		{
			goto IL_0a20;
		}
	}
	{
		RuntimeObject* L_378 = __this->____geometryGroup_9;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_379 = __this->____reader_2;
		NullCheck(L_379);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_380;
		L_380 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_379, NULL);
		NullCheck(L_378);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_381;
		L_381 = GenericInterfaceFuncInvoker3< PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277*, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C*, int32_t, bool >::Invoke(IGeometryGroup_GetGeometry_TisPlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277_mD3FEA40366BC6279E62F5984369E846ADFC1D016_RuntimeMethod_var, L_378, L_380, 0, (bool)0);
		V_70 = L_381;
		V_71 = 0;
		goto IL_0a09;
	}

IL_0978:
	{
		RuntimeObject* L_382 = __this->____allVertices_3;
		int32_t L_383 = V_71;
		NullCheck(L_382);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_384;
		L_384 = InterfaceFuncInvoker1< Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, int32_t >::Invoke(0 /* T System.Collections.Generic.IList`1<UnityEngine.Vector3>::get_Item(System.Int32) */, IList_1_t4EEE459A249DDE104FA2E88234C593389EE5D291_il2cpp_TypeInfo_var, L_382, L_383);
		V_72 = L_384;
		int32_t L_385 = V_71;
		RuntimeObject* L_386 = __this->____allNormals_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_387;
		L_387 = ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE(L_385, L_386, ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_RuntimeMethod_var);
		V_73 = L_387;
		int32_t L_388 = V_71;
		RuntimeObject* L_389 = __this->____allColors_5;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_390;
		L_390 = ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447(L_388, L_389, ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447_RuntimeMethod_var);
		V_74 = L_390;
		int32_t L_391 = V_71;
		RuntimeObject* L_392 = __this->____allUVs_6;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_393;
		L_393 = ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E(L_391, L_392, ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E_RuntimeMethod_var);
		V_75 = L_393;
		RuntimeObject* L_394 = __this->____geometryGroup_9;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_395 = ___0_plyReader;
		NullCheck(L_395);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_396;
		L_396 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_395, NULL);
		int32_t L_397 = V_71;
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_398 = V_70;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_399 = V_72;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_400 = V_73;
		il2cpp_codegen_initobj((&V_76), sizeof(Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3));
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_401 = V_76;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_402 = V_74;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_403 = V_75;
		il2cpp_codegen_initobj((&V_77), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_404 = V_77;
		il2cpp_codegen_initobj((&V_77), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_405 = V_77;
		il2cpp_codegen_initobj((&V_77), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_406 = V_77;
		il2cpp_codegen_initobj((&V_78), sizeof(BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F));
		BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F L_407 = V_78;
		FlexibleVertexDataUtils_BuildAndAddFlexibleVertexData_mC35367A0F41EE78D0D41CB73484C229156CC532F(L_394, L_396, L_397, L_398, L_399, L_400, L_401, L_402, L_403, L_404, L_405, L_406, L_407, NULL);
		int32_t L_408 = V_71;
		V_71 = ((int32_t)il2cpp_codegen_add(L_408, 1));
	}

IL_0a09:
	{
		int32_t L_409 = V_71;
		RuntimeObject* L_410 = __this->____allVertices_3;
		NullCheck(L_410);
		int32_t L_411;
		L_411 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Vector3>::get_Count() */, ICollection_1_tEF23B8A78E9122DAA952DB1517D31EB7EC90563B_il2cpp_TypeInfo_var, L_410);
		if ((((int32_t)L_409) < ((int32_t)L_411)))
		{
			goto IL_0978;
		}
	}
	{
		goto IL_0dc2;
	}

IL_0a20:
	{
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_412 = ___0_plyReader;
		NullCheck(L_412);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_413;
		L_413 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_412, NULL);
		NullCheck(L_413);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_414 = L_413->___Options_0;
		NullCheck(L_414);
		bool L_415 = L_414->___ImportMeshes_10;
		if (!L_415)
		{
			goto IL_0dc2;
		}
	}
	{
		Dictionary_2_t123D9C83AF38157E7001ECFB13C7D3DA29E465DC* L_416 = V_0;
		NullCheck(L_416);
		bool L_417;
		L_417 = Dictionary_2_TryGetValue_m181BAACEB7905BC572C38343BF7DDA866E9415FD(L_416, ((int64_t)6774539739450268610LL), (&V_79), Dictionary_2_TryGetValue_m181BAACEB7905BC572C38343BF7DDA866E9415FD_RuntimeMethod_var);
		if (!L_417)
		{
			goto IL_0dc2;
		}
	}
	{
		V_80 = 0;
		goto IL_0da3;
	}

IL_0a53:
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_418 = V_79;
		NullCheck(L_418);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_419;
		L_419 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_418, ((int64_t)-4898810238098815469LL), NULL);
		V_82 = ((PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E*)IsInstClass((RuntimeObject*)L_419, PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E_il2cpp_TypeInfo_var));
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_420 = V_82;
		if (!L_420)
		{
			goto IL_0a83;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_421 = V_79;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_422 = V_82;
		int32_t L_423 = V_80;
		NullCheck(L_421);
		int32_t L_424;
		L_424 = PlyElement_GetListIndex_m2725B97E4D677B68667107956C9D7E8A2C2565AF(L_421, L_422, L_423, NULL);
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_425 = V_1;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_426;
		L_426 = PlyValue_GetListValue_m0917530BCED9A22F01949BB493407DCC83C71C2F(L_424, L_425, NULL);
		V_81 = L_426;
		goto IL_0a86;
	}

IL_0a83:
	{
		V_81 = (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*)NULL;
	}

IL_0a86:
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_427 = V_79;
		NullCheck(L_427);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_428;
		L_428 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_427, ((int64_t)3766120907217874982LL), NULL);
		V_83 = L_428;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_429 = V_83;
		if (L_429)
		{
			goto IL_0a9f;
		}
	}
	{
		G_B117_0 = 0;
		goto IL_0aaa;
	}

IL_0a9f:
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_430 = V_79;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_431 = V_83;
		int32_t L_432 = V_80;
		NullCheck(L_430);
		int32_t L_433;
		L_433 = PlyElement_GetPropertyIntValue_m350C36F63BFB7DB71BBDE9219AE28BC400EAC124(L_430, L_431, L_432, NULL);
		G_B117_0 = L_433;
	}

IL_0aaa:
	{
		V_84 = G_B117_0;
		RuntimeObject* L_434 = __this->____geometryGroup_9;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_435 = __this->____reader_2;
		NullCheck(L_435);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_436;
		L_436 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_435, NULL);
		int32_t L_437 = V_84;
		NullCheck(L_434);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_438;
		L_438 = GenericInterfaceFuncInvoker3< PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277*, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C*, int32_t, bool >::Invoke(IGeometryGroup_GetGeometry_TisPlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277_mD3FEA40366BC6279E62F5984369E846ADFC1D016_RuntimeMethod_var, L_434, L_436, L_437, (bool)0);
		V_85 = L_438;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_439 = __this->____reader_2;
		NullCheck(L_439);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_440;
		L_440 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_439, NULL);
		NullCheck(L_440);
		AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* L_441 = L_440->___Options_0;
		NullCheck(L_441);
		bool L_442 = L_441->___KeepQuads_22;
		if (L_442)
		{
			goto IL_0ae1;
		}
	}
	{
		G_B120_0 = ((PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277*)(NULL));
		goto IL_0afa;
	}

IL_0ae1:
	{
		RuntimeObject* L_443 = __this->____geometryGroup_9;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_444 = __this->____reader_2;
		NullCheck(L_444);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_445;
		L_445 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_444, NULL);
		int32_t L_446 = V_84;
		NullCheck(L_443);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_447;
		L_447 = GenericInterfaceFuncInvoker3< PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277*, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C*, int32_t, bool >::Invoke(IGeometryGroup_GetGeometry_TisPlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277_mD3FEA40366BC6279E62F5984369E846ADFC1D016_RuntimeMethod_var, L_443, L_445, L_446, (bool)1);
		G_B120_0 = L_447;
	}

IL_0afa:
	{
		V_86 = G_B120_0;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_448 = V_79;
		NullCheck(L_448);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_449;
		L_449 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_448, ((int64_t)7578069101293603633LL), NULL);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_450 = L_449;
		G_B121_0 = L_450;
		if (L_450)
		{
			G_B122_0 = L_450;
			goto IL_0b20;
		}
	}
	{
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_451 = V_79;
		NullCheck(L_451);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_452;
		L_452 = PlyElement_GetProperty_m59AEFD258B013232C34B22302790095BB47E3280(L_451, ((int64_t)2138570927453749980LL), NULL);
		G_B122_0 = L_452;
	}

IL_0b20:
	{
		V_87 = ((PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E*)IsInstClass((RuntimeObject*)G_B122_0, PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E_il2cpp_TypeInfo_var));
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_453 = V_79;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_454 = V_87;
		int32_t L_455 = V_80;
		NullCheck(L_453);
		int32_t L_456;
		L_456 = PlyElement_GetListIndex_m2725B97E4D677B68667107956C9D7E8A2C2565AF(L_453, L_454, L_455, NULL);
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_457 = V_1;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_458;
		L_458 = PlyValue_GetListValue_m0917530BCED9A22F01949BB493407DCC83C71C2F(L_456, L_457, NULL);
		V_88 = L_458;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_459 = V_88;
		NullCheck(L_459);
		int32_t L_460;
		L_460 = List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_inline(L_459, List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_RuntimeMethod_var);
		if ((!(((uint32_t)L_460) == ((uint32_t)3))))
		{
			goto IL_0b7c;
		}
	}
	{
		V_89 = 2;
		goto IL_0b72;
	}

IL_0b49:
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_461 = V_88;
		int32_t L_462 = V_89;
		NullCheck(L_461);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_463;
		L_463 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_461, L_462, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_464 = V_87;
		NullCheck(L_464);
		int32_t L_465 = L_464->___ItemType_3;
		int32_t L_466;
		L_466 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_463, L_465, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_467 = V_85;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_468 = V_81;
		int32_t L_469 = V_89;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_470 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_466, L_467, L_468, L_469, L_470, NULL);
		int32_t L_471 = V_89;
		V_89 = ((int32_t)il2cpp_codegen_subtract(L_471, 1));
	}

IL_0b72:
	{
		int32_t L_472 = V_89;
		if ((((int32_t)L_472) >= ((int32_t)0)))
		{
			goto IL_0b49;
		}
	}
	{
		goto IL_0d9d;
	}

IL_0b7c:
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_473 = V_88;
		NullCheck(L_473);
		int32_t L_474;
		L_474 = List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_inline(L_473, List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_RuntimeMethod_var);
		if ((!(((uint32_t)L_474) == ((uint32_t)4))))
		{
			goto IL_0ce4;
		}
	}
	{
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_475 = V_86;
		if (!L_475)
		{
			goto IL_0c19;
		}
	}
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_476 = V_88;
		NullCheck(L_476);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_477;
		L_477 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_476, 3, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_478 = V_87;
		NullCheck(L_478);
		int32_t L_479 = L_478->___ItemType_3;
		int32_t L_480;
		L_480 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_477, L_479, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_481 = V_86;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_482 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_483 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_480, L_481, L_482, 3, L_483, NULL);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_484 = V_88;
		NullCheck(L_484);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_485;
		L_485 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_484, 2, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_486 = V_87;
		NullCheck(L_486);
		int32_t L_487 = L_486->___ItemType_3;
		int32_t L_488;
		L_488 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_485, L_487, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_489 = V_86;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_490 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_491 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_488, L_489, L_490, 2, L_491, NULL);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_492 = V_88;
		NullCheck(L_492);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_493;
		L_493 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_492, 1, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_494 = V_87;
		NullCheck(L_494);
		int32_t L_495 = L_494->___ItemType_3;
		int32_t L_496;
		L_496 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_493, L_495, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_497 = V_86;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_498 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_499 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_496, L_497, L_498, 1, L_499, NULL);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_500 = V_88;
		NullCheck(L_500);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_501;
		L_501 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_500, 0, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_502 = V_87;
		NullCheck(L_502);
		int32_t L_503 = L_502->___ItemType_3;
		int32_t L_504;
		L_504 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_501, L_503, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_505 = V_86;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_506 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_507 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_504, L_505, L_506, 0, L_507, NULL);
		goto IL_0d9d;
	}

IL_0c19:
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_508 = V_88;
		NullCheck(L_508);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_509;
		L_509 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_508, 0, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_510 = V_87;
		NullCheck(L_510);
		int32_t L_511 = L_510->___ItemType_3;
		int32_t L_512;
		L_512 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_509, L_511, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_513 = V_85;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_514 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_515 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_512, L_513, L_514, 0, L_515, NULL);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_516 = V_88;
		NullCheck(L_516);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_517;
		L_517 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_516, 3, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_518 = V_87;
		NullCheck(L_518);
		int32_t L_519 = L_518->___ItemType_3;
		int32_t L_520;
		L_520 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_517, L_519, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_521 = V_85;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_522 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_523 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_520, L_521, L_522, 3, L_523, NULL);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_524 = V_88;
		NullCheck(L_524);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_525;
		L_525 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_524, 2, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_526 = V_87;
		NullCheck(L_526);
		int32_t L_527 = L_526->___ItemType_3;
		int32_t L_528;
		L_528 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_525, L_527, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_529 = V_85;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_530 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_531 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_528, L_529, L_530, 2, L_531, NULL);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_532 = V_88;
		NullCheck(L_532);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_533;
		L_533 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_532, 2, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_534 = V_87;
		NullCheck(L_534);
		int32_t L_535 = L_534->___ItemType_3;
		int32_t L_536;
		L_536 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_533, L_535, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_537 = V_85;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_538 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_539 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_536, L_537, L_538, 2, L_539, NULL);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_540 = V_88;
		NullCheck(L_540);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_541;
		L_541 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_540, 1, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_542 = V_87;
		NullCheck(L_542);
		int32_t L_543 = L_542->___ItemType_3;
		int32_t L_544;
		L_544 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_541, L_543, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_545 = V_85;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_546 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_547 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_544, L_545, L_546, 1, L_547, NULL);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_548 = V_88;
		NullCheck(L_548);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_549;
		L_549 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_548, 0, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_550 = V_87;
		NullCheck(L_550);
		int32_t L_551 = L_550->___ItemType_3;
		int32_t L_552;
		L_552 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_549, L_551, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_553 = V_85;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_554 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_555 = V_82;
		PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556(__this, L_552, L_553, L_554, 0, L_555, NULL);
		goto IL_0d9d;
	}

IL_0ce4:
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_556 = V_88;
		NullCheck(L_556);
		int32_t L_557;
		L_557 = List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_inline(L_556, List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_RuntimeMethod_var);
		ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF* L_558 = (ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF*)(ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF*)SZArrayNew(ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF_il2cpp_TypeInfo_var, (uint32_t)L_557);
		V_90 = L_558;
		V_91 = 0;
		goto IL_0d74;
	}

IL_0cf7:
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_559 = V_88;
		int32_t L_560 = V_91;
		NullCheck(L_559);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_561;
		L_561 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_559, L_560, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		V_92 = L_561;
		RuntimeObject* L_562 = __this->____allVertices_3;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_563 = V_92;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_564 = V_87;
		NullCheck(L_564);
		int32_t L_565 = L_564->___ItemType_3;
		int32_t L_566;
		L_566 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_563, L_565, NULL);
		NullCheck(L_562);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_567;
		L_567 = InterfaceFuncInvoker1< Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, int32_t >::Invoke(0 /* T System.Collections.Generic.IList`1<UnityEngine.Vector3>::get_Item(System.Int32) */, IList_1_t4EEE459A249DDE104FA2E88234C593389EE5D291_il2cpp_TypeInfo_var, L_562, L_566);
		V_93 = L_567;
		ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF* L_568 = V_90;
		int32_t L_569 = V_91;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_570 = V_93;
		float L_571 = L_570.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_572 = V_93;
		float L_573 = L_572.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_574 = V_93;
		float L_575 = L_574.___z_4;
		Vec3_t7AA9166B9BB818308F0807E2023584038C0104F6 L_576;
		memset((&L_576), 0, sizeof(L_576));
		Vec3__ctor_m937D1DD303AF7D61662A455E5F208BDB81106B7C((&L_576), L_571, L_573, L_575, /*hidden argument*/NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_577 = V_85;
		RuntimeObject* L_578 = __this->____geometryGroup_9;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_579 = V_88;
		int32_t L_580 = V_91;
		NullCheck(L_579);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_581;
		L_581 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_579, L_580, List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_582 = V_87;
		NullCheck(L_582);
		int32_t L_583 = L_582->___ItemType_3;
		int32_t L_584;
		L_584 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_581, L_583, NULL);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_585 = V_81;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_586 = V_82;
		int32_t L_587 = V_91;
		RuntimeObject* L_588;
		L_588 = PlyProcessor_BuildVertexData_mB82746725B313510493FDBBBCD2A960E434566B9(__this, L_577, L_578, L_584, L_585, L_586, L_587, NULL);
		ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F L_589;
		memset((&L_589), 0, sizeof(L_589));
		ContourVertex__ctor_mB49AE90A9B85CA37EC7EE928BC9EE585D35C8C03((&L_589), L_576, L_588, /*hidden argument*/NULL);
		NullCheck(L_568);
		(L_568)->SetAt(static_cast<il2cpp_array_size_t>(L_569), (ContourVertex_t7039525BFF24D8B6501A2105EA311A90C86ABB5F)L_589);
		int32_t L_590 = V_91;
		V_91 = ((int32_t)il2cpp_codegen_add(L_590, 1));
	}

IL_0d74:
	{
		int32_t L_591 = V_91;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_592 = V_88;
		NullCheck(L_592);
		int32_t L_593;
		L_593 = List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_inline(L_592, List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_RuntimeMethod_var);
		if ((((int32_t)L_591) < ((int32_t)L_593)))
		{
			goto IL_0cf7;
		}
	}
	{
		ContourVertexU5BU5D_tAA6EDE0FE3A9428E4FA7E8E6115E2F65C82259CF* L_594 = V_90;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_595 = __this->____reader_2;
		NullCheck(L_595);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_596;
		L_596 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_595, NULL);
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_597 = V_85;
		RuntimeObject* L_598 = __this->____geometryGroup_9;
		Helpers_Tesselate_m52BC0268EF5AEEEECF07746E8B25E59026DAC804((RuntimeObject*)L_594, L_596, L_597, L_598, (bool)1, NULL);
	}

IL_0d9d:
	{
		int32_t L_599 = V_80;
		V_80 = ((int32_t)il2cpp_codegen_add(L_599, 1));
	}

IL_0da3:
	{
		int32_t L_600 = V_80;
		PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8* L_601 = V_79;
		NullCheck(L_601);
		int32_t L_602 = L_601->___Count_1;
		if ((((int32_t)L_600) < ((int32_t)L_602)))
		{
			goto IL_0a53;
		}
	}
	{
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_603 = ___0_plyReader;
		NullCheck(L_603);
		ReaderBase_UpdateLoadingPercentage_m10601D3AC7D2C0CD2F66F204E89ED0430E312D0C(L_603, (1.0f), 3, (0.0f), NULL);
	}

IL_0dc2:
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_604 = V_10;
		NullCheck(L_604);
		TextReader_Dispose_mDCB332EFA06970A9CC7EC4596FCC5220B9512616(L_604, NULL);
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_605 = (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237*)il2cpp_codegen_object_new(PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237_il2cpp_TypeInfo_var);
		NullCheck(L_605);
		PlyRootModel__ctor_m70F96F0795DD6B57ADAEE28C419A56033958CEED(L_605, NULL);
		V_94 = L_605;
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_606 = V_94;
		NullCheck(L_606);
		PlyModel_set_Visibility_mB2F9D43D393D00418E2B4A999974A1E18512D0A5_inline(L_606, (bool)1, NULL);
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_607 = V_94;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_608;
		L_608 = Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline(NULL);
		NullCheck(L_607);
		PlyModel_set_LocalScale_m8055103EC8ADECCC5D3C535C098B33AA2E0275A8_inline(L_607, L_608, NULL);
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_609 = V_94;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_610;
		L_610 = Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline(NULL);
		NullCheck(L_609);
		PlyModel_set_LocalRotation_mA70C0956246A032C3C1C7EF5DE31A303ACE92BCB_inline(L_609, L_610, NULL);
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_611 = V_94;
		RuntimeObject* L_612 = __this->____geometryGroup_9;
		NullCheck(L_611);
		PlyModel_set_GeometryGroup_mC40A2CC9BD75EBA9305AA0FC6E9F1FFD51FDA055_inline(L_611, L_612, NULL);
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_613 = V_94;
		RuntimeObject* L_614 = V_13;
		NullCheck(L_613);
		PlyRootModel_set_AllMaterials_m30733607EFAAACE9110C2452A299D1D47D799C53_inline(L_613, L_614, NULL);
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_615 = V_94;
		List_1_t0A7598F2321AB14249B7E7EBBC916064FB9DBC31* L_616 = V_7;
		NullCheck(L_615);
		PlyRootModel_set_AllTextures_m7646524ECF24096720D3C48A840F554283D6212E_inline(L_615, L_616, NULL);
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_617 = V_94;
		IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711* L_618 = (IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711*)(IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711*)SZArrayNew(IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711_il2cpp_TypeInfo_var, (uint32_t)1);
		IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711* L_619 = L_618;
		RuntimeObject* L_620 = __this->____geometryGroup_9;
		NullCheck(L_619);
		ArrayElementTypeCheck (L_619, L_620);
		(L_619)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_620);
		NullCheck(L_617);
		PlyRootModel_set_AllGeometryGroups_m44BF8C7B4B75BCAA1C25B38D31F5FB90019FD3F1_inline(L_617, (RuntimeObject*)L_619, NULL);
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_621 = V_94;
		V_15 = L_621;
		RuntimeObject* L_622 = V_13;
		if (!L_622)
		{
			goto IL_0e5e;
		}
	}
	{
		RuntimeObject* L_623 = V_13;
		NullCheck(L_623);
		int32_t L_624;
		L_624 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<TriLibCore.Interfaces.IMaterial>::get_Count() */, ICollection_1_tD4699121F1986A1CF0F28D10E91389DD1C7FDE69_il2cpp_TypeInfo_var, L_623);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_625 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)L_624);
		V_95 = L_625;
		V_96 = 0;
		goto IL_0e4d;
	}

IL_0e40:
	{
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_626 = V_95;
		int32_t L_627 = V_96;
		int32_t L_628 = V_96;
		NullCheck(L_626);
		(L_626)->SetAt(static_cast<il2cpp_array_size_t>(L_627), (int32_t)L_628);
		int32_t L_629 = V_96;
		V_96 = ((int32_t)il2cpp_codegen_add(L_629, 1));
	}

IL_0e4d:
	{
		int32_t L_630 = V_96;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_631 = V_95;
		NullCheck(L_631);
		if ((((int32_t)L_630) < ((int32_t)((int32_t)(((RuntimeArray*)L_631)->max_length)))))
		{
			goto IL_0e40;
		}
	}
	{
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_632 = V_15;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_633 = V_95;
		NullCheck(L_632);
		PlyModel_set_MaterialIndices_m87FB1DB52736B0D10EBF7CA05C04C3B1A321F670_inline(L_632, (RuntimeObject*)L_633, NULL);
	}

IL_0e5e:
	{
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_634 = V_15;
		return L_634;
	}
}
// TriLibCore.Geometries.IVertexData TriLibCore.Ply.PlyProcessor::BuildVertexData(TriLibCore.Ply.PlyGeometry,TriLibCore.Interfaces.IGeometryGroup,System.Int32,System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>,TriLibCore.Ply.PlyListProperty,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyProcessor_BuildVertexData_mB82746725B313510493FDBBBCD2A960E434566B9 (PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F* __this, PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* ___0_triGeometry, RuntimeObject* ___1_geometryGroup, int32_t ___2_vertexIndex, List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* ___3_texCoord, PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* ___4_texCoordProp, int32_t ___5_texCoordIndex, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_3;
	memset((&V_3), 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_0 = ___3_texCoord;
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_1 = ___3_texCoord;
		int32_t L_2 = ___5_texCoordIndex;
		NullCheck(L_1);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_3;
		L_3 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_1, ((int32_t)il2cpp_codegen_multiply(L_2, 2)), List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_4 = ___4_texCoordProp;
		float L_5;
		L_5 = PlyValue_GetFloatValue_mCE0211B08C054DE0848AEB2EF5E728F6D28E9830(L_3, L_4, NULL);
		V_4 = L_5;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_6 = ___3_texCoord;
		int32_t L_7 = ___5_texCoordIndex;
		NullCheck(L_6);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_8;
		L_8 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_6, ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_7, 2)), 1)), List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_9 = ___4_texCoordProp;
		float L_10;
		L_10 = PlyValue_GetFloatValue_mCE0211B08C054DE0848AEB2EF5E728F6D28E9830(L_8, L_9, NULL);
		V_5 = L_10;
		float L_11 = V_4;
		float L_12 = V_5;
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_0), L_11, L_12, NULL);
		goto IL_0048;
	}

IL_003b:
	{
		int32_t L_13 = ___2_vertexIndex;
		RuntimeObject* L_14 = __this->____allUVs_6;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15;
		L_15 = ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E(L_13, L_14, ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E_RuntimeMethod_var);
		V_0 = L_15;
	}

IL_0048:
	{
		int32_t L_16 = ___2_vertexIndex;
		RuntimeObject* L_17 = __this->____allVertices_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18;
		L_18 = ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE(L_16, L_17, ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_RuntimeMethod_var);
		V_1 = L_18;
		int32_t L_19 = ___2_vertexIndex;
		RuntimeObject* L_20 = __this->____allNormals_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21;
		L_21 = ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE(L_19, L_20, ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_RuntimeMethod_var);
		V_2 = L_21;
		int32_t L_22 = ___2_vertexIndex;
		RuntimeObject* L_23 = __this->____allColors_5;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_24;
		L_24 = ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447(L_22, L_23, ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447_RuntimeMethod_var);
		V_3 = L_24;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25 = V_1;
		InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* L_26 = (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5*)il2cpp_codegen_object_new(InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5_il2cpp_TypeInfo_var);
		NullCheck(L_26);
		InterpolatedVertex__ctor_mA3F5FBB1EACDDB0F2B33E893A45FB89113C6787C(L_26, L_25, NULL);
		InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* L_27 = L_26;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28 = V_1;
		RuntimeObject* L_29 = ___1_geometryGroup;
		NullCheck(L_27);
		InterpolatedVertex_SetPosition_m51A3B70131151C51E738A6EDCD4E5EBEF7C80E2C_inline(L_27, L_28, L_29, NULL);
		InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* L_30 = L_27;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_31 = V_2;
		RuntimeObject* L_32 = ___1_geometryGroup;
		NullCheck(L_30);
		InterpolatedVertex_SetNormal_m48EE21D1CFF1D80F3EAAD2689ABEE84102882ADC_inline(L_30, L_31, L_32, NULL);
		InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* L_33 = L_30;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_34 = V_3;
		RuntimeObject* L_35 = ___1_geometryGroup;
		NullCheck(L_33);
		InterpolatedVertex_SetColor_m01F89204D2B248BE23E94620AE338F73D8C90AE5_inline(L_33, L_34, L_35, NULL);
		InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* L_36 = L_33;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_37 = V_0;
		RuntimeObject* L_38 = ___1_geometryGroup;
		NullCheck(L_36);
		InterpolatedVertex_SetUV1_mA7C291101768A0EE2734CD2589D815DCC2454551_inline(L_36, L_37, L_38, NULL);
		return L_36;
	}
}
// System.Void TriLibCore.Ply.PlyProcessor::AddVertex(System.Int32,TriLibCore.Ply.PlyGeometry,System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>,System.Int32,TriLibCore.Ply.PlyProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyProcessor_AddVertex_m5B75D5BE24B3E796AF09BD492E09542B81F47556 (PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F* __this, int32_t ___0_vertexIndex, PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* ___1_geometry, List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* ___2_texCoord, int32_t ___3_texCoordIndex, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___4_texCoordProp, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_4;
	memset((&V_4), 0, sizeof(V_4));
	BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F V_5;
	memset((&V_5), 0, sizeof(V_5));
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_0 = ___2_texCoord;
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_1 = ___2_texCoord;
		int32_t L_2 = ___3_texCoordIndex;
		NullCheck(L_1);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_3;
		L_3 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_1, ((int32_t)il2cpp_codegen_multiply(L_2, 2)), List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_4 = ___4_texCoordProp;
		float L_5;
		L_5 = PlyValue_GetFloatValue_mCE0211B08C054DE0848AEB2EF5E728F6D28E9830(L_3, L_4, NULL);
		V_1 = L_5;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_6 = ___2_texCoord;
		int32_t L_7 = ___3_texCoordIndex;
		NullCheck(L_6);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_8;
		L_8 = List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36(L_6, ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_7, 2)), 1)), List_1_get_Item_m962F28DEDCA02D0ECAACE71A6DB856C93D226E36_RuntimeMethod_var);
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_9 = ___4_texCoordProp;
		float L_10;
		L_10 = PlyValue_GetFloatValue_mCE0211B08C054DE0848AEB2EF5E728F6D28E9830(L_8, L_9, NULL);
		V_2 = L_10;
		float L_11 = V_1;
		float L_12 = V_2;
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_0), L_11, L_12, NULL);
		goto IL_0041;
	}

IL_0034:
	{
		int32_t L_13 = ___0_vertexIndex;
		RuntimeObject* L_14 = __this->____allUVs_6;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15;
		L_15 = ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E(L_13, L_14, ListUtils_FixIndex_TisVector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_m80E31C1F387DAA4115F14B5463CADE1DEF297D2E_RuntimeMethod_var);
		V_0 = L_15;
	}

IL_0041:
	{
		RuntimeObject* L_16 = __this->____geometryGroup_9;
		PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* L_17 = __this->____reader_2;
		NullCheck(L_17);
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_18;
		L_18 = ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline(L_17, NULL);
		int32_t L_19 = ___0_vertexIndex;
		PlyGeometry_tA0C162EFF46BFAFFE018583C0AA8B48295E78277* L_20 = ___1_geometry;
		int32_t L_21 = ___0_vertexIndex;
		RuntimeObject* L_22 = __this->____allVertices_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23;
		L_23 = ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE(L_21, L_22, ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_RuntimeMethod_var);
		int32_t L_24 = ___0_vertexIndex;
		RuntimeObject* L_25 = __this->____allNormals_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26;
		L_26 = ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE(L_24, L_25, ListUtils_FixIndex_TisVector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_mD45AFFDDC575B7D4965A6B58B98B7D88ED4323EE_RuntimeMethod_var);
		il2cpp_codegen_initobj((&V_3), sizeof(Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3));
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_27 = V_3;
		int32_t L_28 = ___0_vertexIndex;
		RuntimeObject* L_29 = __this->____allColors_5;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_30;
		L_30 = ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447(L_28, L_29, ListUtils_FixIndex_TisColor_tD001788D726C3A7F1379BEED0260B9591F440C1F_mA7050D5E386674C2B99B38CF8EE1F3E237BAC447_RuntimeMethod_var);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_31 = V_0;
		il2cpp_codegen_initobj((&V_4), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_32 = V_4;
		il2cpp_codegen_initobj((&V_4), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_33 = V_4;
		il2cpp_codegen_initobj((&V_4), sizeof(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_34 = V_4;
		il2cpp_codegen_initobj((&V_5), sizeof(BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F));
		BoneWeight_t7E7DACA0D0F56698E05EBBD839D1D343502EA11F L_35 = V_5;
		FlexibleVertexDataUtils_BuildAndAddFlexibleVertexData_mC35367A0F41EE78D0D41CB73484C229156CC532F(L_16, L_18, L_19, L_20, L_23, L_26, L_27, L_30, L_31, L_32, L_33, L_34, L_35, NULL);
		return;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyProcessor::ReadElementData(TriLibCore.Ply.PlyProperty,TriLibCore.Ply.PlyPropertyType,System.Boolean,System.IO.BinaryReader,System.Boolean,TriLibCore.Ply.PlyStreamReader,TriLibCore.Utils.BigEndianBinaryReader,System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyProcessor_ReadElementData_m295BBB280885FD705DF14E08D8FE375B03CB984B (PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___0_property, int32_t ___1_propertyType, bool ___2_littleEndian, BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___3_binaryReader, bool ___4_bigEndian, PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* ___5_plyStreamReader, BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* ___6_bigEndianBinaryReader, List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* ___7_lists, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m7C99F2FA69D684BD5B7E22B8A115DA258EA04CB2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_0;
	memset((&V_0), 0, sizeof(V_0));
	PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* V_3 = NULL;
	int32_t V_4 = 0;
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_5;
	memset((&V_5), 0, sizeof(V_5));
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 G_B7_0;
	memset((&G_B7_0), 0, sizeof(G_B7_0));
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 G_B13_0;
	memset((&G_B13_0), 0, sizeof(G_B13_0));
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 G_B19_0;
	memset((&G_B19_0), 0, sizeof(G_B19_0));
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 G_B25_0;
	memset((&G_B25_0), 0, sizeof(G_B25_0));
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 G_B31_0;
	memset((&G_B31_0), 0, sizeof(G_B31_0));
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 G_B37_0;
	memset((&G_B37_0), 0, sizeof(G_B37_0));
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 G_B43_0;
	memset((&G_B43_0), 0, sizeof(G_B43_0));
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 G_B49_0;
	memset((&G_B49_0), 0, sizeof(G_B49_0));
	{
		int32_t L_0 = ___1_propertyType;
		switch (L_0)
		{
			case 0:
			{
				goto IL_002f;
			}
			case 1:
			{
				goto IL_005e;
			}
			case 2:
			{
				goto IL_008d;
			}
			case 3:
			{
				goto IL_00bc;
			}
			case 4:
			{
				goto IL_00eb;
			}
			case 5:
			{
				goto IL_011a;
			}
			case 6:
			{
				goto IL_0149;
			}
			case 7:
			{
				goto IL_0178;
			}
			case 8:
			{
				goto IL_01a4;
			}
		}
	}
	{
		goto IL_021b;
	}

IL_002f:
	{
		bool L_1 = ___2_littleEndian;
		if (L_1)
		{
			goto IL_004d;
		}
	}
	{
		bool L_2 = ___4_bigEndian;
		if (L_2)
		{
			goto IL_003f;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_3 = ___5_plyStreamReader;
		NullCheck(L_3);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_4;
		L_4 = PlyStreamReader_ToSByte_mE0CBD4AA736167D961A334A686A4851A2B107FD8(L_3, NULL);
		G_B7_0 = L_4;
		goto IL_0058;
	}

IL_003f:
	{
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_5 = ___6_bigEndianBinaryReader;
		NullCheck(L_5);
		int8_t L_6;
		L_6 = VirtualFuncInvoker0< int8_t >::Invoke(12 /* System.SByte System.IO.BinaryReader::ReadSByte() */, L_5);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_7;
		L_7 = PlyValue_op_Implicit_m97A5AB6B334327E421C58927E78C773B6D809B53(L_6, NULL);
		G_B7_0 = L_7;
		goto IL_0058;
	}

IL_004d:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_8 = ___3_binaryReader;
		NullCheck(L_8);
		int8_t L_9;
		L_9 = VirtualFuncInvoker0< int8_t >::Invoke(12 /* System.SByte System.IO.BinaryReader::ReadSByte() */, L_8);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_10;
		L_10 = PlyValue_op_Implicit_m97A5AB6B334327E421C58927E78C773B6D809B53(L_9, NULL);
		G_B7_0 = L_10;
	}

IL_0058:
	{
		V_0 = G_B7_0;
		goto IL_0221;
	}

IL_005e:
	{
		bool L_11 = ___2_littleEndian;
		if (L_11)
		{
			goto IL_007c;
		}
	}
	{
		bool L_12 = ___4_bigEndian;
		if (L_12)
		{
			goto IL_006e;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_13 = ___5_plyStreamReader;
		NullCheck(L_13);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_14;
		L_14 = PlyStreamReader_ToByte_mC8D36F8FD97FEBF03092492FCCFC77F27C669B31(L_13, NULL);
		G_B13_0 = L_14;
		goto IL_0087;
	}

IL_006e:
	{
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_15 = ___6_bigEndianBinaryReader;
		NullCheck(L_15);
		uint8_t L_16;
		L_16 = VirtualFuncInvoker0< uint8_t >::Invoke(11 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_15);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_17;
		L_17 = PlyValue_op_Implicit_m9E7C391E8C4BAEF13B69CC7B0CEC87F250BD46A6(L_16, NULL);
		G_B13_0 = L_17;
		goto IL_0087;
	}

IL_007c:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_18 = ___3_binaryReader;
		NullCheck(L_18);
		uint8_t L_19;
		L_19 = VirtualFuncInvoker0< uint8_t >::Invoke(11 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_18);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_20;
		L_20 = PlyValue_op_Implicit_m9E7C391E8C4BAEF13B69CC7B0CEC87F250BD46A6(L_19, NULL);
		G_B13_0 = L_20;
	}

IL_0087:
	{
		V_0 = G_B13_0;
		goto IL_0221;
	}

IL_008d:
	{
		bool L_21 = ___2_littleEndian;
		if (L_21)
		{
			goto IL_00ab;
		}
	}
	{
		bool L_22 = ___4_bigEndian;
		if (L_22)
		{
			goto IL_009d;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_23 = ___5_plyStreamReader;
		NullCheck(L_23);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_24;
		L_24 = PlyStreamReader_ToInt16_m15E305684870C3A4B737A95DA4212701C45830F9(L_23, NULL);
		G_B19_0 = L_24;
		goto IL_00b6;
	}

IL_009d:
	{
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_25 = ___6_bigEndianBinaryReader;
		NullCheck(L_25);
		int16_t L_26;
		L_26 = BigEndianBinaryReader_ReadInt16_m2646AC659077E17786022320078FBBDB8DCBC707(L_25, NULL);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_27;
		L_27 = PlyValue_op_Implicit_m78378AA8D6DDCEC09907E0927A06ED49418D4635(L_26, NULL);
		G_B19_0 = L_27;
		goto IL_00b6;
	}

IL_00ab:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_28 = ___3_binaryReader;
		NullCheck(L_28);
		int16_t L_29;
		L_29 = VirtualFuncInvoker0< int16_t >::Invoke(14 /* System.Int16 System.IO.BinaryReader::ReadInt16() */, L_28);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_30;
		L_30 = PlyValue_op_Implicit_m78378AA8D6DDCEC09907E0927A06ED49418D4635(L_29, NULL);
		G_B19_0 = L_30;
	}

IL_00b6:
	{
		V_0 = G_B19_0;
		goto IL_0221;
	}

IL_00bc:
	{
		bool L_31 = ___2_littleEndian;
		if (L_31)
		{
			goto IL_00da;
		}
	}
	{
		bool L_32 = ___4_bigEndian;
		if (L_32)
		{
			goto IL_00cc;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_33 = ___5_plyStreamReader;
		NullCheck(L_33);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_34;
		L_34 = PlyStreamReader_ToUInt16_m5E5F5DDAFA6CBF735A852FED4B1712FC914301AD(L_33, NULL);
		G_B25_0 = L_34;
		goto IL_00e5;
	}

IL_00cc:
	{
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_35 = ___6_bigEndianBinaryReader;
		NullCheck(L_35);
		uint16_t L_36;
		L_36 = VirtualFuncInvoker0< uint16_t >::Invoke(15 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_35);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_37;
		L_37 = PlyValue_op_Implicit_m8B2B7375CC217E6E3345C812C647F7BB51FC0DC6(L_36, NULL);
		G_B25_0 = L_37;
		goto IL_00e5;
	}

IL_00da:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_38 = ___3_binaryReader;
		NullCheck(L_38);
		uint16_t L_39;
		L_39 = VirtualFuncInvoker0< uint16_t >::Invoke(15 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_38);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_40;
		L_40 = PlyValue_op_Implicit_m8B2B7375CC217E6E3345C812C647F7BB51FC0DC6(L_39, NULL);
		G_B25_0 = L_40;
	}

IL_00e5:
	{
		V_0 = G_B25_0;
		goto IL_0221;
	}

IL_00eb:
	{
		bool L_41 = ___2_littleEndian;
		if (L_41)
		{
			goto IL_0109;
		}
	}
	{
		bool L_42 = ___4_bigEndian;
		if (L_42)
		{
			goto IL_00fb;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_43 = ___5_plyStreamReader;
		NullCheck(L_43);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_44;
		L_44 = PlyStreamReader_ToInt32_m9F2C9B5DB1E8C1E858934D60E8365C24B5F96E05(L_43, NULL);
		G_B31_0 = L_44;
		goto IL_0114;
	}

IL_00fb:
	{
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_45 = ___6_bigEndianBinaryReader;
		NullCheck(L_45);
		int32_t L_46;
		L_46 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_45);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_47;
		L_47 = PlyValue_op_Implicit_mCBC1637AE004264B6DC685D489B3DABF7C40CE8B(L_46, NULL);
		G_B31_0 = L_47;
		goto IL_0114;
	}

IL_0109:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_48 = ___3_binaryReader;
		NullCheck(L_48);
		int32_t L_49;
		L_49 = VirtualFuncInvoker0< int32_t >::Invoke(16 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_48);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_50;
		L_50 = PlyValue_op_Implicit_mCBC1637AE004264B6DC685D489B3DABF7C40CE8B(L_49, NULL);
		G_B31_0 = L_50;
	}

IL_0114:
	{
		V_0 = G_B31_0;
		goto IL_0221;
	}

IL_011a:
	{
		bool L_51 = ___2_littleEndian;
		if (L_51)
		{
			goto IL_0138;
		}
	}
	{
		bool L_52 = ___4_bigEndian;
		if (L_52)
		{
			goto IL_012a;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_53 = ___5_plyStreamReader;
		NullCheck(L_53);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_54;
		L_54 = PlyStreamReader_ToUInt32_mFE918D4E08D1EBBB095A75F4A6F4B0B0FF31E7AA(L_53, NULL);
		G_B37_0 = L_54;
		goto IL_0143;
	}

IL_012a:
	{
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_55 = ___6_bigEndianBinaryReader;
		NullCheck(L_55);
		uint32_t L_56;
		L_56 = BigEndianBinaryReader_ReadUInt32_mE3E235D63AC4B3633B15C0C70AAFA771DB825823(L_55, NULL);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_57;
		L_57 = PlyValue_op_Implicit_m29A5DE8027F986F1251009072C203966CA6B2CE4(L_56, NULL);
		G_B37_0 = L_57;
		goto IL_0143;
	}

IL_0138:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_58 = ___3_binaryReader;
		NullCheck(L_58);
		uint32_t L_59;
		L_59 = VirtualFuncInvoker0< uint32_t >::Invoke(17 /* System.UInt32 System.IO.BinaryReader::ReadUInt32() */, L_58);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_60;
		L_60 = PlyValue_op_Implicit_m29A5DE8027F986F1251009072C203966CA6B2CE4(L_59, NULL);
		G_B37_0 = L_60;
	}

IL_0143:
	{
		V_0 = G_B37_0;
		goto IL_0221;
	}

IL_0149:
	{
		bool L_61 = ___2_littleEndian;
		if (L_61)
		{
			goto IL_0167;
		}
	}
	{
		bool L_62 = ___4_bigEndian;
		if (L_62)
		{
			goto IL_0159;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_63 = ___5_plyStreamReader;
		NullCheck(L_63);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_64;
		L_64 = PlyStreamReader_ToSingle_m90ED33B30955513C54C102F1CF12293B87ED840D(L_63, NULL);
		G_B43_0 = L_64;
		goto IL_0172;
	}

IL_0159:
	{
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_65 = ___6_bigEndianBinaryReader;
		NullCheck(L_65);
		float L_66;
		L_66 = BigEndianBinaryReader_ReadSingle_m5BE3F923D636289CE8D32BD988A36AF1D507BC8E(L_65, NULL);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_67;
		L_67 = PlyValue_op_Implicit_mABA4998D1201D014DFC2948ED728D84579823AB2(L_66, NULL);
		G_B43_0 = L_67;
		goto IL_0172;
	}

IL_0167:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_68 = ___3_binaryReader;
		NullCheck(L_68);
		float L_69;
		L_69 = VirtualFuncInvoker0< float >::Invoke(20 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_68);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_70;
		L_70 = PlyValue_op_Implicit_mABA4998D1201D014DFC2948ED728D84579823AB2(L_69, NULL);
		G_B43_0 = L_70;
	}

IL_0172:
	{
		V_0 = G_B43_0;
		goto IL_0221;
	}

IL_0178:
	{
		bool L_71 = ___2_littleEndian;
		if (L_71)
		{
			goto IL_0196;
		}
	}
	{
		bool L_72 = ___4_bigEndian;
		if (L_72)
		{
			goto IL_0188;
		}
	}
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_73 = ___5_plyStreamReader;
		NullCheck(L_73);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_74;
		L_74 = PlyStreamReader_ToDouble_m24378FB1F25B30F14E4F1C4EDE8A61FE4AB2C467(L_73, NULL);
		G_B49_0 = L_74;
		goto IL_01a1;
	}

IL_0188:
	{
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_75 = ___6_bigEndianBinaryReader;
		NullCheck(L_75);
		double L_76;
		L_76 = BigEndianBinaryReader_ReadDouble_m807A1A1D9EE248DD7637AB40276E88CADDCAB32F(L_75, NULL);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_77;
		L_77 = PlyValue_op_Implicit_m1A3B9DCC94933A941A05360494BF9C1D6A1F5FE0(L_76, NULL);
		G_B49_0 = L_77;
		goto IL_01a1;
	}

IL_0196:
	{
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_78 = ___3_binaryReader;
		NullCheck(L_78);
		double L_79;
		L_79 = VirtualFuncInvoker0< double >::Invoke(21 /* System.Double System.IO.BinaryReader::ReadDouble() */, L_78);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_80;
		L_80 = PlyValue_op_Implicit_m1A3B9DCC94933A941A05360494BF9C1D6A1F5FE0(L_79, NULL);
		G_B49_0 = L_80;
	}

IL_01a1:
	{
		V_0 = G_B49_0;
		goto IL_0221;
	}

IL_01a4:
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_81 = ___0_property;
		V_1 = ((PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E*)CastclassClass((RuntimeObject*)L_81, PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E_il2cpp_TypeInfo_var));
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_82 = ___0_property;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_83 = V_1;
		NullCheck(L_83);
		int32_t L_84 = L_83->___CounterType_2;
		bool L_85 = ___2_littleEndian;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_86 = ___3_binaryReader;
		bool L_87 = ___4_bigEndian;
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_88 = ___5_plyStreamReader;
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_89 = ___6_bigEndianBinaryReader;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_90 = ___7_lists;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_91;
		L_91 = PlyProcessor_ReadElementData_m295BBB280885FD705DF14E08D8FE375B03CB984B(L_82, L_84, L_85, L_86, L_87, L_88, L_89, L_90, NULL);
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_92 = V_1;
		NullCheck(L_92);
		int32_t L_93 = L_92->___CounterType_2;
		int32_t L_94;
		L_94 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_91, L_93, NULL);
		V_2 = L_94;
		int32_t L_95 = V_2;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_96 = (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*)il2cpp_codegen_object_new(List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB_il2cpp_TypeInfo_var);
		NullCheck(L_96);
		List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122(L_96, L_95, List_1__ctor_m893153D76F05AFF53207ABCA86290836BCC7B122_RuntimeMethod_var);
		V_3 = L_96;
		V_4 = 0;
		goto IL_01ff;
	}

IL_01d9:
	{
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_97 = ___0_property;
		PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E* L_98 = V_1;
		NullCheck(L_98);
		int32_t L_99 = L_98->___ItemType_3;
		bool L_100 = ___2_littleEndian;
		BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* L_101 = ___3_binaryReader;
		bool L_102 = ___4_bigEndian;
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_103 = ___5_plyStreamReader;
		BigEndianBinaryReader_t4A46AFC4734E1DFBB57EDC318136A57FDDBD9721* L_104 = ___6_bigEndianBinaryReader;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_105 = ___7_lists;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_106;
		L_106 = PlyProcessor_ReadElementData_m295BBB280885FD705DF14E08D8FE375B03CB984B(L_97, L_99, L_100, L_101, L_102, L_103, L_104, L_105, NULL);
		V_5 = L_106;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_107 = V_3;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_108 = V_5;
		NullCheck(L_107);
		List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_inline(L_107, L_108, List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_RuntimeMethod_var);
		int32_t L_109 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_109, 1));
	}

IL_01ff:
	{
		int32_t L_110 = V_4;
		int32_t L_111 = V_2;
		if ((((int32_t)L_110) < ((int32_t)L_111)))
		{
			goto IL_01d9;
		}
	}
	{
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_112 = ___7_lists;
		NullCheck(L_112);
		int32_t L_113;
		L_113 = List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_inline(L_112, List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_114;
		L_114 = PlyValue_op_Implicit_mCBC1637AE004264B6DC685D489B3DABF7C40CE8B(L_113, NULL);
		V_0 = L_114;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_115 = ___7_lists;
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_116 = V_3;
		NullCheck(L_115);
		List_1_Add_m7C99F2FA69D684BD5B7E22B8A115DA258EA04CB2_inline(L_115, L_116, List_1_Add_m7C99F2FA69D684BD5B7E22B8A115DA258EA04CB2_RuntimeMethod_var);
		goto IL_0221;
	}

IL_021b:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_117 = ((PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41_StaticFields*)il2cpp_codegen_static_fields_for(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41_il2cpp_TypeInfo_var))->___Unknown_1;
		V_0 = L_117;
	}

IL_0221:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_118 = V_0;
		return L_118;
	}
}
// System.Int64 TriLibCore.Ply.PlyProcessor::DataSize(TriLibCore.Ply.PlyPropertyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t PlyProcessor_DataSize_m43C82832553876645CA5A1484F5B4D294428298D (int32_t ___0_valueType, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_valueType;
		switch (L_0)
		{
			case 0:
			{
				goto IL_002c;
			}
			case 1:
			{
				goto IL_002c;
			}
			case 2:
			{
				goto IL_002f;
			}
			case 3:
			{
				goto IL_002f;
			}
			case 4:
			{
				goto IL_0032;
			}
			case 5:
			{
				goto IL_0032;
			}
			case 6:
			{
				goto IL_0032;
			}
			case 7:
			{
				goto IL_0035;
			}
			case 8:
			{
				goto IL_0038;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_002c:
	{
		return ((int64_t)1);
	}

IL_002f:
	{
		return ((int64_t)2);
	}

IL_0032:
	{
		return ((int64_t)4);
	}

IL_0035:
	{
		return ((int64_t)8);
	}

IL_0038:
	{
		return ((int64_t)4);
	}

IL_003b:
	{
		return ((int64_t)0);
	}
}
// TriLibCore.Ply.PlyPropertyType TriLibCore.Ply.PlyProcessor::GetPropertyType(TriLibCore.Ply.PlyStreamReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyProcessor_GetPropertyType_mF17237DE1F943422F7DBEB3435F43FD29CFB2A46 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* ___0_plyStreamReader, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int64_t V_1 = 0;
	{
		PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* L_0 = ___0_plyStreamReader;
		NullCheck(L_0);
		int64_t L_1;
		L_1 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(L_0, (bool)1, (bool)0, NULL);
		V_1 = L_1;
		int64_t L_2 = V_1;
		if ((((int64_t)L_2) > ((int64_t)((int64_t)6774539739450185915LL))))
		{
			goto IL_00c8;
		}
	}
	{
		int64_t L_3 = V_1;
		if ((((int64_t)L_3) > ((int64_t)((int64_t)-1367968407807378442LL))))
		{
			goto IL_0076;
		}
	}
	{
		int64_t L_4 = V_1;
		if ((((int64_t)L_4) > ((int64_t)((int64_t)-5513532492926073195LL))))
		{
			goto IL_0053;
		}
	}
	{
		int64_t L_5 = V_1;
		if ((((int64_t)L_5) == ((int64_t)((int64_t)-5513532492926073290LL))))
		{
			goto IL_017e;
		}
	}
	{
		int64_t L_6 = V_1;
		if ((((int64_t)L_6) == ((int64_t)((int64_t)-5513532492926073195LL))))
		{
			goto IL_0182;
		}
	}
	{
		goto IL_018a;
	}

IL_0053:
	{
		int64_t L_7 = V_1;
		if ((((int64_t)L_7) == ((int64_t)((int64_t)-3351804022671191574LL))))
		{
			goto IL_0176;
		}
	}
	{
		int64_t L_8 = V_1;
		if ((((int64_t)L_8) == ((int64_t)((int64_t)-1367968407807378442LL))))
		{
			goto IL_0182;
		}
	}
	{
		goto IL_018a;
	}

IL_0076:
	{
		int64_t L_9 = V_1;
		if ((((int64_t)L_9) > ((int64_t)((int64_t)-1367968407326417058LL))))
		{
			goto IL_00a5;
		}
	}
	{
		int64_t L_10 = V_1;
		if ((((int64_t)L_10) == ((int64_t)((int64_t)-1367968407326417116LL))))
		{
			goto IL_0172;
		}
	}
	{
		int64_t L_11 = V_1;
		if ((((int64_t)L_11) == ((int64_t)((int64_t)-1367968407326417058LL))))
		{
			goto IL_017a;
		}
	}
	{
		goto IL_018a;
	}

IL_00a5:
	{
		int64_t L_12 = V_1;
		if ((((int64_t)L_12) == ((int64_t)((int64_t)-1367968407317363380LL))))
		{
			goto IL_0172;
		}
	}
	{
		int64_t L_13 = V_1;
		if ((((int64_t)L_13) == ((int64_t)((int64_t)6774539739450185915LL))))
		{
			goto IL_0166;
		}
	}
	{
		goto IL_018a;
	}

IL_00c8:
	{
		int64_t L_14 = V_1;
		if ((((int64_t)L_14) > ((int64_t)((int64_t)7096547112153598359LL))))
		{
			goto IL_011a;
		}
	}
	{
		int64_t L_15 = V_1;
		if ((((int64_t)L_15) > ((int64_t)((int64_t)6774539739450455555LL))))
		{
			goto IL_0100;
		}
	}
	{
		int64_t L_16 = V_1;
		if ((((int64_t)L_16) == ((int64_t)((int64_t)6774539739450370958LL))))
		{
			goto IL_0166;
		}
	}
	{
		int64_t L_17 = V_1;
		if ((((int64_t)L_17) == ((int64_t)((int64_t)6774539739450455555LL))))
		{
			goto IL_0186;
		}
	}
	{
		goto IL_018a;
	}

IL_0100:
	{
		int64_t L_18 = V_1;
		if ((((int64_t)L_18) == ((int64_t)((int64_t)6774539739450723519LL))))
		{
			goto IL_017a;
		}
	}
	{
		int64_t L_19 = V_1;
		if ((((int64_t)L_19) == ((int64_t)((int64_t)7096547112153598359LL))))
		{
			goto IL_017e;
		}
	}
	{
		goto IL_018a;
	}

IL_011a:
	{
		int64_t L_20 = V_1;
		if ((((int64_t)L_20) > ((int64_t)((int64_t)7096547112156431817LL))))
		{
			goto IL_0140;
		}
	}
	{
		int64_t L_21 = V_1;
		if ((((int64_t)L_21) == ((int64_t)((int64_t)7096547112156431759LL))))
		{
			goto IL_016e;
		}
	}
	{
		int64_t L_22 = V_1;
		if ((((int64_t)L_22) == ((int64_t)((int64_t)7096547112156431817LL))))
		{
			goto IL_0176;
		}
	}
	{
		goto IL_018a;
	}

IL_0140:
	{
		int64_t L_23 = V_1;
		if ((((int64_t)L_23) == ((int64_t)((int64_t)7096547112165485495LL))))
		{
			goto IL_016e;
		}
	}
	{
		int64_t L_24 = V_1;
		if ((((int64_t)L_24) == ((int64_t)((int64_t)7096547112167176326LL))))
		{
			goto IL_016a;
		}
	}
	{
		int64_t L_25 = V_1;
		if ((((int64_t)L_25) == ((int64_t)((int64_t)7096547112167361369LL))))
		{
			goto IL_016a;
		}
	}
	{
		goto IL_018a;
	}

IL_0166:
	{
		V_0 = 0;
		goto IL_018d;
	}

IL_016a:
	{
		V_0 = 1;
		goto IL_018d;
	}

IL_016e:
	{
		V_0 = 2;
		goto IL_018d;
	}

IL_0172:
	{
		V_0 = 3;
		goto IL_018d;
	}

IL_0176:
	{
		V_0 = 4;
		goto IL_018d;
	}

IL_017a:
	{
		V_0 = 5;
		goto IL_018d;
	}

IL_017e:
	{
		V_0 = 6;
		goto IL_018d;
	}

IL_0182:
	{
		V_0 = 7;
		goto IL_018d;
	}

IL_0186:
	{
		V_0 = 8;
		goto IL_018d;
	}

IL_018a:
	{
		V_0 = ((int32_t)9);
	}

IL_018d:
	{
		int32_t L_26 = V_0;
		return L_26;
	}
}
// System.Void TriLibCore.Ply.PlyProcessor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyProcessor__ctor_mEFE18083343802C21877A430538A1304EC1DC5E7 (PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.SByte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m97A5AB6B334327E421C58927E78C773B6D809B53 (int8_t ___0_other, const RuntimeMethod* method) 
{
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41));
		int8_t L_0 = ___0_other;
		(&V_0)->____intValue_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, ((int32_t)127)));
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_1 = V_0;
		return L_1;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m9E7C391E8C4BAEF13B69CC7B0CEC87F250BD46A6 (uint8_t ___0_other, const RuntimeMethod* method) 
{
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41));
		uint8_t L_0 = ___0_other;
		(&V_0)->____intValue_0 = L_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_1 = V_0;
		return L_1;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m8B2B7375CC217E6E3345C812C647F7BB51FC0DC6 (uint16_t ___0_other, const RuntimeMethod* method) 
{
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41));
		uint16_t L_0 = ___0_other;
		(&V_0)->____intValue_0 = L_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_1 = V_0;
		return L_1;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m78378AA8D6DDCEC09907E0927A06ED49418D4635 (int16_t ___0_other, const RuntimeMethod* method) 
{
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41));
		int16_t L_0 = ___0_other;
		(&V_0)->____intValue_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, ((int32_t)32767)));
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_1 = V_0;
		return L_1;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m29A5DE8027F986F1251009072C203966CA6B2CE4 (uint32_t ___0_other, const RuntimeMethod* method) 
{
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41));
		uint32_t L_0 = ___0_other;
		(&V_0)->____intValue_0 = L_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_1 = V_0;
		return L_1;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_mCBC1637AE004264B6DC685D489B3DABF7C40CE8B (int32_t ___0_other, const RuntimeMethod* method) 
{
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41));
		int32_t L_0 = ___0_other;
		(&V_0)->____intValue_0 = ((int32_t)il2cpp_codegen_add(L_0, ((int32_t)2147483647LL)));
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_1 = V_0;
		return L_1;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_mABA4998D1201D014DFC2948ED728D84579823AB2 (float ___0_other, const RuntimeMethod* method) 
{
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41));
		int32_t L_0 = *((int32_t*)((uintptr_t)(&___0_other)));
		(&V_0)->____intValue_0 = L_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_1 = V_0;
		return L_1;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::op_Implicit(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyValue_op_Implicit_m1A3B9DCC94933A941A05360494BF9C1D6A1F5FE0 (double ___0_other, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		double L_0 = ___0_other;
		V_0 = ((float)L_0);
		il2cpp_codegen_initobj((&V_1), sizeof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41));
		int32_t L_1 = *((int32_t*)((uintptr_t)(&V_0)));
		(&V_1)->____intValue_0 = L_1;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_2 = V_1;
		return L_2;
	}
}
// System.Int32 TriLibCore.Ply.PlyValue::GetIntValue(TriLibCore.Ply.PlyValue,TriLibCore.Ply.PlyProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyValue_GetIntValue_mC5D8E3A04354E7AD052C4E0DA2DDC26DADC58ABD (PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_value, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___1_property, const RuntimeMethod* method) 
{
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_0 = ___0_value;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_1 = ___1_property;
		NullCheck(L_1);
		int32_t L_2 = L_1->___Type_0;
		int32_t L_3;
		L_3 = PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E(L_0, L_2, NULL);
		return L_3;
	}
}
// System.Int32 TriLibCore.Ply.PlyValue::GetIntValue(TriLibCore.Ply.PlyValue,TriLibCore.Ply.PlyPropertyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyValue_GetIntValue_mBAD141B59D1599BCFDE8A6B19BA4649A8BE5E40E (PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_value, int32_t ___1_propertyType, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___1_propertyType;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0037;
			}
			case 1:
			{
				goto IL_0030;
			}
			case 2:
			{
				goto IL_0044;
			}
			case 3:
			{
				goto IL_0030;
			}
			case 4:
			{
				goto IL_0054;
			}
			case 5:
			{
				goto IL_0030;
			}
			case 6:
			{
				goto IL_0061;
			}
			case 7:
			{
				goto IL_0061;
			}
			case 8:
			{
				goto IL_006c;
			}
			case 9:
			{
				goto IL_006c;
			}
		}
	}
	{
		goto IL_006e;
	}

IL_0030:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_1 = ___0_value;
		uint32_t L_2 = L_1.____intValue_0;
		return L_2;
	}

IL_0037:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_3 = ___0_value;
		uint32_t L_4 = L_3.____intValue_0;
		return ((int32_t)((int64_t)il2cpp_codegen_subtract(((int64_t)(uint64_t)L_4), ((int64_t)((int32_t)127)))));
	}

IL_0044:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_5 = ___0_value;
		uint32_t L_6 = L_5.____intValue_0;
		return ((int32_t)((int64_t)il2cpp_codegen_subtract(((int64_t)(uint64_t)L_6), ((int64_t)((int32_t)32767)))));
	}

IL_0054:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_7 = ___0_value;
		uint32_t L_8 = L_7.____intValue_0;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, ((int32_t)2147483647LL)));
	}

IL_0061:
	{
		uint32_t* L_9 = (&(&___0_value)->____intValue_0);
		float L_10 = *((float*)((uintptr_t)L_9));
		return il2cpp_codegen_cast_double_to_int<int32_t>(L_10);
	}

IL_006c:
	{
		return 0;
	}

IL_006e:
	{
		return 0;
	}
}
// System.Single TriLibCore.Ply.PlyValue::GetFloatValue(TriLibCore.Ply.PlyValue,TriLibCore.Ply.PlyProperty)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyValue_GetFloatValue_mCE0211B08C054DE0848AEB2EF5E728F6D28E9830 (PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_value, PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* ___1_property, const RuntimeMethod* method) 
{
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_0 = ___0_value;
		PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* L_1 = ___1_property;
		NullCheck(L_1);
		int32_t L_2 = L_1->___Type_0;
		float L_3;
		L_3 = PlyValue_GetFloatValue_m03C511897C1577C7BAE5C96F005AB99B7EEEE57D(L_0, L_2, NULL);
		return L_3;
	}
}
// System.Single TriLibCore.Ply.PlyValue::GetFloatValue(TriLibCore.Ply.PlyValue,TriLibCore.Ply.PlyPropertyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyValue_GetFloatValue_m03C511897C1577C7BAE5C96F005AB99B7EEEE57D (PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_value, int32_t ___1_propertyType, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___1_propertyType;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0039;
			}
			case 1:
			{
				goto IL_0030;
			}
			case 2:
			{
				goto IL_0046;
			}
			case 3:
			{
				goto IL_0030;
			}
			case 4:
			{
				goto IL_0056;
			}
			case 5:
			{
				goto IL_0030;
			}
			case 6:
			{
				goto IL_0065;
			}
			case 7:
			{
				goto IL_0065;
			}
			case 8:
			{
				goto IL_006f;
			}
			case 9:
			{
				goto IL_006f;
			}
		}
	}
	{
		goto IL_0075;
	}

IL_0030:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_1 = ___0_value;
		uint32_t L_2 = L_1.____intValue_0;
		return ((float)((double)(uint32_t)L_2));
	}

IL_0039:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_3 = ___0_value;
		uint32_t L_4 = L_3.____intValue_0;
		return ((float)((int64_t)il2cpp_codegen_subtract(((int64_t)(uint64_t)L_4), ((int64_t)((int32_t)127)))));
	}

IL_0046:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_5 = ___0_value;
		uint32_t L_6 = L_5.____intValue_0;
		return ((float)((int64_t)il2cpp_codegen_subtract(((int64_t)(uint64_t)L_6), ((int64_t)((int32_t)32767)))));
	}

IL_0056:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_7 = ___0_value;
		uint32_t L_8 = L_7.____intValue_0;
		return ((float)((double)(uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_8, ((int32_t)2147483647LL)))));
	}

IL_0065:
	{
		uint32_t* L_9 = (&(&___0_value)->____intValue_0);
		float L_10 = *((float*)((uintptr_t)L_9));
		return L_10;
	}

IL_006f:
	{
		return (0.0f);
	}

IL_0075:
	{
		return (0.0f);
	}
}
// System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue> TriLibCore.Ply.PlyValue::GetListValue(System.Int32,System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* PlyValue_GetListValue_m0917530BCED9A22F01949BB493407DCC83C71C2F (int32_t ___0_index, List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* ___1_lists, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___0_index;
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_1 = ___1_lists;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_inline(L_1, List_1_get_Count_m2FEB3A28B3B3FAF4D9F34AF11D4D88D2DEBF6B58_RuntimeMethod_var);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_000b;
		}
	}
	{
		return (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*)NULL;
	}

IL_000b:
	{
		List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* L_3 = ___1_lists;
		int32_t L_4 = ___0_index;
		NullCheck(L_3);
		List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* L_5;
		L_5 = List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641(L_3, L_4, List_1_get_Item_m7158E76526E37E4FB84F740CB3B89804AAEAA641_RuntimeMethod_var);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Ply.PlyProperty::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyProperty__ctor_mB9404ABEA64DE21D9C16B8FCF8FA95FEA5C8221F (PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Ply.PlyRootModel::get_AllModels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyRootModel_get_AllModels_m8BA54C38D62A91E6EA514CB8BEF435C900EDCA94 (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllModelsU3Ek__BackingField_14;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyRootModel::set_AllModels(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyRootModel_set_AllModels_mE90ADC9ED99A62E095C884039C960256DBB04C3F (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllModelsU3Ek__BackingField_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllModelsU3Ek__BackingField_14), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Ply.PlyRootModel::get_AllGeometryGroups()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyRootModel_get_AllGeometryGroups_m21829E15CD622B14B2013F12CCBD3B6CCAC25FCE (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllGeometryGroupsU3Ek__BackingField_15;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyRootModel::set_AllGeometryGroups(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyRootModel_set_AllGeometryGroups_m44BF8C7B4B75BCAA1C25B38D31F5FB90019FD3F1 (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllGeometryGroupsU3Ek__BackingField_15 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllGeometryGroupsU3Ek__BackingField_15), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Ply.PlyRootModel::get_AllAnimations()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyRootModel_get_AllAnimations_m80B1C9A7DFEB22568B6F14424FFFBB6B6A849CFB (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllAnimationsU3Ek__BackingField_16;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyRootModel::set_AllAnimations(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyRootModel_set_AllAnimations_mD0FA305B4006E1D24C396B6616E8D00FE1C40BC0 (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllAnimationsU3Ek__BackingField_16 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllAnimationsU3Ek__BackingField_16), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Ply.PlyRootModel::get_AllMaterials()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyRootModel_get_AllMaterials_m1FFB3F75554502875DFB152BD2066E4975392DA4 (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllMaterialsU3Ek__BackingField_17;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyRootModel::set_AllMaterials(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyRootModel_set_AllMaterials_m30733607EFAAACE9110C2452A299D1D47D799C53 (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllMaterialsU3Ek__BackingField_17 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllMaterialsU3Ek__BackingField_17), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Ply.PlyRootModel::get_AllTextures()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyRootModel_get_AllTextures_mB07F64685BC1D4D272E488255C175DACC7DCF369 (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllTexturesU3Ek__BackingField_18;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyRootModel::set_AllTextures(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyRootModel_set_AllTextures_m7646524ECF24096720D3C48A840F554283D6212E (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllTexturesU3Ek__BackingField_18 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllTexturesU3Ek__BackingField_18), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Ply.PlyRootModel::get_AllCameras()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyRootModel_get_AllCameras_mB6F19E15EA2D7C834771991CF8372FF417C830FB (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllCamerasU3Ek__BackingField_19;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyRootModel::set_AllCameras(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyRootModel_set_AllCameras_m20E71DBACF3E11B29CE064CDDC9EF5AD255F3A6E (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllCamerasU3Ek__BackingField_19 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllCamerasU3Ek__BackingField_19), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Ply.PlyRootModel::get_AllLights()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyRootModel_get_AllLights_mB73ABF8C75AD11B2AE5345AB5AA85B99A2C0F949 (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllLightsU3Ek__BackingField_20;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyRootModel::set_AllLights(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyRootModel_set_AllLights_mE52C8D811F086721B5BCC0C8FAB4B863A9BBA0E8 (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllLightsU3Ek__BackingField_20 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllLightsU3Ek__BackingField_20), (void*)L_0);
		return;
	}
}
// System.Void TriLibCore.Ply.PlyRootModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyRootModel__ctor_m70F96F0795DD6B57ADAEE28C419A56033958CEED (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, const RuntimeMethod* method) 
{
	{
		PlyModel__ctor_mEBFCADB0E140C2324D5944AD85D4BED4CE3BC725(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 TriLibCore.Ply.PlyStreamReader::get_Line()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyStreamReader_get_Line_m85F1A411C379BDFFA2DF9B1DD0BAFEDB96C54351 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	{
		return 0;
	}
}
// System.Int32 TriLibCore.Ply.PlyStreamReader::get_Column()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyStreamReader_get_Column_mCF0D5AB5A8D9AF918DE62EC471363E144ECD6441 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	{
		return 0;
	}
}
// System.Int64 TriLibCore.Ply.PlyStreamReader::get_Position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	{
		int64_t L_0 = __this->___U3CPositionU3Ek__BackingField_27;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyStreamReader::set_Position(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyStreamReader_set_Position_m763DA88CC440E9EAA48B2638BC5D5E717BB8C3F4 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, int64_t ___0_value, const RuntimeMethod* method) 
{
	{
		int64_t L_0 = ___0_value;
		__this->___U3CPositionU3Ek__BackingField_27 = L_0;
		return;
	}
}
// System.Void TriLibCore.Ply.PlyStreamReader::UpdateStringFromCharString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyStreamReader_UpdateStringFromCharString_m48E78B576C5792EE9BEEA23388553D70A8D0D9EF (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->____charString_25;
		PlyStreamReader_CopyCharStreamToString_mDEACAF07AF4EE3BA8B0378893465CF46BBEC3434(__this, L_0, ((int32_t)24), NULL);
		return;
	}
}
// System.Void TriLibCore.Ply.PlyStreamReader::CopyCharStreamToString(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyStreamReader_CopyCharStreamToString_mDEACAF07AF4EE3BA8B0378893465CF46BBEC3434 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, String_t* ___0_s, int32_t ___1_maxChars, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeArrayUnsafeUtility_GetUnsafePtr_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_mDDC6F7A9E98335D0828894600921FCF3A934DB0A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar* V_0 = NULL;
	String_t* V_1 = NULL;
	void* V_2 = NULL;
	{
		String_t* L_0 = ___0_s;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_0, NULL);
		int32_t L_2 = ___1_maxChars;
		int32_t L_3;
		L_3 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(L_1, L_2, NULL);
		___1_maxChars = L_3;
		String_t* L_4 = ___0_s;
		V_1 = L_4;
		String_t* L_5 = V_1;
		V_0 = (Il2CppChar*)((uintptr_t)L_5);
		Il2CppChar* L_6 = V_0;
		if (!L_6)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppChar* L_7 = V_0;
		int32_t L_8;
		L_8 = RuntimeHelpers_get_OffsetToStringData_m90A5D27EF88BE9432BF7093B7D7E7A0ACB0A8FBD(NULL);
		V_0 = ((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_7, L_8));
	}

IL_001e:
	{
		NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A L_9 = __this->____charStream_24;
		void* L_10;
		L_10 = NativeArrayUnsafeUtility_GetUnsafePtr_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_mDDC6F7A9E98335D0828894600921FCF3A934DB0A(L_9, NativeArrayUnsafeUtility_GetUnsafePtr_TisChar_t521A6F19B456D956AF452D926C32709DC03D6B17_mDDC6F7A9E98335D0828894600921FCF3A934DB0A_RuntimeMethod_var);
		V_2 = L_10;
		Il2CppChar* L_11 = V_0;
		int32_t L_12 = ___1_maxChars;
		UnsafeUtility_MemSet_m4CD74CD43260EB2962A46F57E0D93DD5C332FC2B((void*)L_11, (uint8_t)0, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_12, 2))), NULL);
		Il2CppChar* L_13 = V_0;
		void* L_14 = V_2;
		int32_t L_15 = __this->____charPosition_26;
		int32_t L_16 = ___1_maxChars;
		int32_t L_17;
		L_17 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(L_15, L_16, NULL);
		UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177((void*)L_13, L_14, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_17, 2))), NULL);
		V_1 = (String_t*)NULL;
		return;
	}
}
// System.String TriLibCore.Ply.PlyStreamReader::GetTokenAsString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyStreamReader_GetTokenAsString_m7E04224D04CDC8D8F4791995529AB17E0CEEDCAA (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->____charPosition_26;
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, L_0, NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		int32_t L_3 = __this->____charPosition_26;
		PlyStreamReader_CopyCharStreamToString_mDEACAF07AF4EE3BA8B0378893465CF46BBEC3434(__this, L_2, L_3, NULL);
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.String TriLibCore.Ply.PlyStreamReader::ReadTokenAsString(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyStreamReader_ReadTokenAsString_m61AD7477C239499B45C3B1A777871A564718F853 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, bool ___0_required, bool ___1_ignoreLineWrapChar, bool ___2_ignoreElementSeparatorChar, bool ___3_ignoreSpaces, bool ___4_stopOnHyphen, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_required;
		bool L_1 = ___3_ignoreSpaces;
		int64_t L_2;
		L_2 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(__this, L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_3;
		L_3 = PlyStreamReader_GetTokenAsString_m7E04224D04CDC8D8F4791995529AB17E0CEEDCAA(__this, NULL);
		return L_3;
	}

IL_0012:
	{
		return (String_t*)NULL;
	}
}
// System.Int64 TriLibCore.Ply.PlyStreamReader::ReadToken(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, bool ___0_required, bool ___1_ignoreSpaces, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	int64_t V_4 = 0;
	int32_t V_5 = 0;
	Il2CppChar V_6 = 0x0;
	int32_t V_7 = 0;
	int32_t G_B13_0 = 0;
	{
		__this->____charPosition_26 = 0;
		V_0 = (bool)1;
		V_1 = (bool)0;
		goto IL_0081;
	}

IL_000d:
	{
		int32_t L_0;
		L_0 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, __this);
		V_2 = L_0;
		int32_t L_1 = V_2;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_2 = V_2;
		V_3 = ((int32_t)(uint16_t)L_2);
		Il2CppChar L_3 = V_3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_3, ((int32_t)9))))
		{
			case 0:
			{
				goto IL_005f;
			}
			case 1:
			{
				goto IL_003f;
			}
			case 2:
			{
				goto IL_007b;
			}
			case 3:
			{
				goto IL_007b;
			}
			case 4:
			{
				goto IL_005f;
			}
		}
	}
	{
		Il2CppChar L_4 = V_3;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)32))))
		{
			goto IL_005f;
		}
	}
	{
		goto IL_007b;
	}

IL_003f:
	{
		int32_t L_5;
		L_5 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		int64_t L_6;
		L_6 = PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B_inline(__this, NULL);
		V_4 = L_6;
		int64_t L_7 = V_4;
		PlyStreamReader_set_Position_m763DA88CC440E9EAA48B2638BC5D5E717BB8C3F4_inline(__this, ((int64_t)il2cpp_codegen_add(L_7, ((int64_t)1))), NULL);
		V_1 = (bool)1;
		V_0 = (bool)0;
		goto IL_0081;
	}

IL_005f:
	{
		int32_t L_8;
		L_8 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		int64_t L_9;
		L_9 = PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B_inline(__this, NULL);
		V_4 = L_9;
		int64_t L_10 = V_4;
		PlyStreamReader_set_Position_m763DA88CC440E9EAA48B2638BC5D5E717BB8C3F4_inline(__this, ((int64_t)il2cpp_codegen_add(L_10, ((int64_t)1))), NULL);
		goto IL_0081;
	}

IL_007b:
	{
		V_0 = (bool)0;
		goto IL_0081;
	}

IL_007f:
	{
		V_0 = (bool)0;
	}

IL_0081:
	{
		bool L_11 = V_0;
		if (L_11)
		{
			goto IL_000d;
		}
	}
	{
		bool L_12 = V_1;
		if (L_12)
		{
			goto IL_0092;
		}
	}
	{
		bool L_13;
		L_13 = StreamReader_get_EndOfStream_mAE054431BF21158178EAA2A6872F14A9ED6A3C3E(__this, NULL);
		G_B13_0 = ((((int32_t)L_13) == ((int32_t)0))? 1 : 0);
		goto IL_0093;
	}

IL_0092:
	{
		G_B13_0 = 0;
	}

IL_0093:
	{
		V_0 = (bool)G_B13_0;
		goto IL_017d;
	}

IL_0099:
	{
		int32_t L_14;
		L_14 = VirtualFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, __this);
		V_5 = L_14;
		int32_t L_15 = V_5;
		if ((((int32_t)L_15) < ((int32_t)0)))
		{
			goto IL_017b;
		}
	}
	{
		int32_t L_16 = V_5;
		V_6 = ((int32_t)(uint16_t)L_16);
		Il2CppChar L_17 = V_6;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_17, ((int32_t)9))))
		{
			case 0:
			{
				goto IL_00fa;
			}
			case 1:
			{
				goto IL_00d4;
			}
			case 2:
			{
				goto IL_013e;
			}
			case 3:
			{
				goto IL_013e;
			}
			case 4:
			{
				goto IL_00db;
			}
		}
	}
	{
		Il2CppChar L_18 = V_6;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)32))))
		{
			goto IL_00fa;
		}
	}
	{
		goto IL_013e;
	}

IL_00d4:
	{
		V_0 = (bool)0;
		goto IL_017d;
	}

IL_00db:
	{
		int32_t L_19;
		L_19 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		int64_t L_20;
		L_20 = PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B_inline(__this, NULL);
		V_4 = L_20;
		int64_t L_21 = V_4;
		PlyStreamReader_set_Position_m763DA88CC440E9EAA48B2638BC5D5E717BB8C3F4_inline(__this, ((int64_t)il2cpp_codegen_add(L_21, ((int64_t)1))), NULL);
		goto IL_017d;
	}

IL_00fa:
	{
		bool L_22 = ___1_ignoreSpaces;
		if (!L_22)
		{
			goto IL_013a;
		}
	}
	{
		NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A* L_23 = (&__this->____charStream_24);
		int32_t L_24 = __this->____charPosition_26;
		V_7 = L_24;
		int32_t L_25 = V_7;
		__this->____charPosition_26 = ((int32_t)il2cpp_codegen_add(L_25, 1));
		int32_t L_26 = V_7;
		Il2CppChar L_27 = V_6;
		IL2CPP_NATIVEARRAY_SET_ITEM(Il2CppChar, (L_23)->___m_Buffer_0, L_26, (L_27));
		int32_t L_28;
		L_28 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		int64_t L_29;
		L_29 = PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B_inline(__this, NULL);
		V_4 = L_29;
		int64_t L_30 = V_4;
		PlyStreamReader_set_Position_m763DA88CC440E9EAA48B2638BC5D5E717BB8C3F4_inline(__this, ((int64_t)il2cpp_codegen_add(L_30, ((int64_t)1))), NULL);
		goto IL_017d;
	}

IL_013a:
	{
		V_0 = (bool)0;
		goto IL_017d;
	}

IL_013e:
	{
		NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A* L_31 = (&__this->____charStream_24);
		int32_t L_32 = __this->____charPosition_26;
		V_7 = L_32;
		int32_t L_33 = V_7;
		__this->____charPosition_26 = ((int32_t)il2cpp_codegen_add(L_33, 1));
		int32_t L_34 = V_7;
		Il2CppChar L_35 = V_6;
		IL2CPP_NATIVEARRAY_SET_ITEM(Il2CppChar, (L_31)->___m_Buffer_0, L_34, (L_35));
		int32_t L_36;
		L_36 = VirtualFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, __this);
		int64_t L_37;
		L_37 = PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B_inline(__this, NULL);
		V_4 = L_37;
		int64_t L_38 = V_4;
		PlyStreamReader_set_Position_m763DA88CC440E9EAA48B2638BC5D5E717BB8C3F4_inline(__this, ((int64_t)il2cpp_codegen_add(L_38, ((int64_t)1))), NULL);
		goto IL_017d;
	}

IL_017b:
	{
		V_0 = (bool)0;
	}

IL_017d:
	{
		bool L_39 = V_0;
		if (L_39)
		{
			goto IL_0099;
		}
	}
	{
		bool L_40 = ___0_required;
		if (!L_40)
		{
			goto IL_01b4;
		}
	}
	{
		int32_t L_41 = __this->____charPosition_26;
		if (L_41)
		{
			goto IL_01b4;
		}
	}
	{
		int32_t L_42;
		L_42 = PlyStreamReader_get_Line_m85F1A411C379BDFFA2DF9B1DD0BAFEDB96C54351(__this, NULL);
		int32_t L_43 = L_42;
		RuntimeObject* L_44 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_43);
		int32_t L_45;
		L_45 = PlyStreamReader_get_Column_mCF0D5AB5A8D9AF918DE62EC471363E144ECD6441(__this, NULL);
		int32_t L_46 = L_45;
		RuntimeObject* L_47 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var)), &L_46);
		String_t* L_48;
		L_48 = String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral3063F5F4FAEA0002A9B169DA0D12D52AC6312C1A)), L_44, L_47, NULL);
		Exception_t* L_49 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_49);
		Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_49, L_48, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_49, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2_RuntimeMethod_var)));
	}

IL_01b4:
	{
		int32_t L_50 = __this->____charPosition_26;
		if (!L_50)
		{
			goto IL_01d8;
		}
	}
	{
		NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A L_51 = __this->____charStream_24;
		int32_t L_52 = __this->____charPosition_26;
		int32_t L_53;
		L_53 = Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline(L_52, ((int32_t)1024), NULL);
		int64_t L_54;
		L_54 = HashUtils_GetHash_mCEFE61677CF3517B02E25BA0BC632C98C62F8C4E(L_51, L_53, NULL);
		return L_54;
	}

IL_01d8:
	{
		return ((int64_t)0);
	}
}
// System.String TriLibCore.Ply.PlyStreamReader::ReadValidTokenAsString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	int64_t V_0 = 0;

IL_0000:
	{
		int64_t L_0;
		L_0 = PlyStreamReader_ReadToken_mEBF2512D8BD8968953BD95286A765B35D270C7D2(__this, (bool)0, (bool)0, NULL);
		V_0 = L_0;
		int64_t L_1 = V_0;
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		bool L_2;
		L_2 = StreamReader_get_EndOfStream_mAE054431BF21158178EAA2A6872F14A9ED6A3C3E(__this, NULL);
		if (!L_2)
		{
			goto IL_0000;
		}
	}

IL_0014:
	{
		PlyStreamReader_UpdateStringFromCharString_m48E78B576C5792EE9BEEA23388553D70A8D0D9EF(__this, NULL);
		String_t* L_3 = __this->____charString_25;
		return L_3;
	}
}
// System.Void TriLibCore.Ply.PlyStreamReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyStreamReader__ctor_mBCDE6CC4B0352A1622093F74DBC3FE0CFDC678F0 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeArray_1__ctor_m01092412782EBC32088D2077868A10700AC7FE61_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A L_0;
		memset((&L_0), 0, sizeof(L_0));
		NativeArray_1__ctor_m01092412782EBC32088D2077868A10700AC7FE61((&L_0), ((int32_t)1024), 4, 1, /*hidden argument*/NativeArray_1__ctor_m01092412782EBC32088D2077868A10700AC7FE61_RuntimeMethod_var);
		__this->____charStream_24 = L_0;
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, ((int32_t)24), NULL);
		__this->____charString_25 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_25), (void*)L_1);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2 = ___0_stream;
		il2cpp_codegen_runtime_class_init_inline(StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_il2cpp_TypeInfo_var);
		StreamReader__ctor_mAFA827D6D825FEC2C29C73B65C2DD1AB9076DEC7(__this, L_2, NULL);
		return;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToSByte()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToSByte_mE0CBD4AA736167D961A334A686A4851A2B107FD8 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	int8_t V_0 = 0x0;
	{
		String_t* L_0;
		L_0 = PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD(__this, NULL);
		String_t* L_1 = __this->____charString_25;
		bool L_2;
		L_2 = SByte_TryParse_m9C205D94AB4FF1CA82EA082E38DD01A493A77ED6(L_1, (&V_0), NULL);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_3;
		L_3 = PlyValue_op_Implicit_m97A5AB6B334327E421C58927E78C773B6D809B53((int8_t)((int32_t)127), NULL);
		return L_3;
	}

IL_001e:
	{
		int8_t L_4 = V_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_5;
		L_5 = PlyValue_op_Implicit_m97A5AB6B334327E421C58927E78C773B6D809B53(L_4, NULL);
		return L_5;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToByte()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToByte_mC8D36F8FD97FEBF03092492FCCFC77F27C669B31 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	uint8_t V_0 = 0x0;
	{
		String_t* L_0;
		L_0 = PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD(__this, NULL);
		String_t* L_1 = __this->____charString_25;
		bool L_2;
		L_2 = Byte_TryParse_mB1716E3B6714F20DF6C1FEDDC4A76AA78D5EA87B(L_1, (&V_0), NULL);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_3;
		L_3 = PlyValue_op_Implicit_m9E7C391E8C4BAEF13B69CC7B0CEC87F250BD46A6((uint8_t)((int32_t)255), NULL);
		return L_3;
	}

IL_0021:
	{
		uint8_t L_4 = V_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_5;
		L_5 = PlyValue_op_Implicit_m9E7C391E8C4BAEF13B69CC7B0CEC87F250BD46A6(L_4, NULL);
		return L_5;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToInt16()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToInt16_m15E305684870C3A4B737A95DA4212701C45830F9 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	int16_t V_0 = 0;
	{
		String_t* L_0;
		L_0 = PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD(__this, NULL);
		String_t* L_1 = __this->____charString_25;
		bool L_2;
		L_2 = Int16_TryParse_m7190AF18437CE1B43990B99E5D992E31485E77AE(L_1, (&V_0), NULL);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_3;
		L_3 = PlyValue_op_Implicit_m78378AA8D6DDCEC09907E0927A06ED49418D4635((int16_t)((int32_t)32767), NULL);
		return L_3;
	}

IL_0021:
	{
		int16_t L_4 = V_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_5;
		L_5 = PlyValue_op_Implicit_m78378AA8D6DDCEC09907E0927A06ED49418D4635(L_4, NULL);
		return L_5;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToUInt16()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToUInt16_m5E5F5DDAFA6CBF735A852FED4B1712FC914301AD (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	uint16_t V_0 = 0;
	{
		String_t* L_0;
		L_0 = PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD(__this, NULL);
		String_t* L_1 = __this->____charString_25;
		bool L_2;
		L_2 = UInt16_TryParse_m02DD9A625527B4019B32ACC9A5D3B09D72B2FBB6(L_1, (&V_0), NULL);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_3;
		L_3 = PlyValue_op_Implicit_m8B2B7375CC217E6E3345C812C647F7BB51FC0DC6((uint16_t)((int32_t)65535), NULL);
		return L_3;
	}

IL_0021:
	{
		uint16_t L_4 = V_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_5;
		L_5 = PlyValue_op_Implicit_m8B2B7375CC217E6E3345C812C647F7BB51FC0DC6(L_4, NULL);
		return L_5;
	}
}
// System.Int32 TriLibCore.Ply.PlyStreamReader::ToInt32NoValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyStreamReader_ToInt32NoValue_m3D62B7AC2B68E0C65837ED4B738D26A02C897086 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0;
		L_0 = PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD(__this, NULL);
		String_t* L_1 = __this->____charString_25;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		int32_t L_2;
		L_2 = Convert_ToInt32_m0C3F3778B1D646778F41B6912138AEEEE6BEB9D4(L_1, NULL);
		return L_2;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToInt32()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToInt32_m9F2C9B5DB1E8C1E858934D60E8365C24B5F96E05 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = __this->____charString_25;
		bool L_1;
		L_1 = Int32_TryParse_mC928DE2FEC1C35ED5298BDDCA9868076E94B8A21(L_0, (&V_0), NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_2;
		L_2 = PlyValue_op_Implicit_mCBC1637AE004264B6DC685D489B3DABF7C40CE8B(((int32_t)2147483647LL), NULL);
		return L_2;
	}

IL_001a:
	{
		int32_t L_3 = V_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_4;
		L_4 = PlyValue_op_Implicit_mCBC1637AE004264B6DC685D489B3DABF7C40CE8B(L_3, NULL);
		return L_4;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToUInt32()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToUInt32_mFE918D4E08D1EBBB095A75F4A6F4B0B0FF31E7AA (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	uint32_t V_0 = 0;
	{
		String_t* L_0;
		L_0 = PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD(__this, NULL);
		String_t* L_1 = __this->____charString_25;
		bool L_2;
		L_2 = UInt32_TryParse_mD470E3BAC9F792AB0BC616510AE3FA78C3CCB1E9(L_1, (&V_0), NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_3;
		L_3 = PlyValue_op_Implicit_m29A5DE8027F986F1251009072C203966CA6B2CE4((-1), NULL);
		return L_3;
	}

IL_001d:
	{
		uint32_t L_4 = V_0;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_5;
		L_5 = PlyValue_op_Implicit_m29A5DE8027F986F1251009072C203966CA6B2CE4(L_4, NULL);
		return L_5;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToSingle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToSingle_m90ED33B30955513C54C102F1CF12293B87ED840D (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0;
		L_0 = PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD(__this, NULL);
		String_t* L_1 = __this->____charString_25;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_2;
		L_2 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		float L_3;
		L_3 = Convert_ToSingle_m8416CDFFC7641BD79BE63F39D5FAEE28986FC636(L_1, L_2, NULL);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_4;
		L_4 = PlyValue_op_Implicit_mABA4998D1201D014DFC2948ED728D84579823AB2(L_3, NULL);
		return L_4;
	}
}
// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyStreamReader::ToDouble()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 PlyStreamReader_ToDouble_m24378FB1F25B30F14E4F1C4EDE8A61FE4AB2C467 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0;
		L_0 = PlyStreamReader_ReadValidTokenAsString_mFCC30978B59DB8B8F0398C4E771EDB6B32CA4AFD(__this, NULL);
		String_t* L_1 = __this->____charString_25;
		il2cpp_codegen_runtime_class_init_inline(CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0_il2cpp_TypeInfo_var);
		CultureInfo_t9BA817D41AD55AC8BD07480DD8AC22F8FFA378E0* L_2;
		L_2 = CultureInfo_get_InvariantCulture_mD1E96DC845E34B10F78CB744B0CB5D7D63CEB1E6(NULL);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		double L_3;
		L_3 = Convert_ToDouble_mAA66A3AA3A6E53529E4F632BC69582B4B70D32B7(L_1, L_2, NULL);
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_4;
		L_4 = PlyValue_op_Implicit_m1A3B9DCC94933A941A05360494BF9C1D6A1F5FE0(L_3, NULL);
		return L_4;
	}
}
// System.Void TriLibCore.Ply.PlyStreamReader::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyStreamReader_Dispose_m2E082C502267A1DE9BE817116BFF6F3B1B64EB69 (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, bool ___0_disposing, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeArray_1_Dispose_m11184C5FB1A1F3809B982476408F08F599F377F0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A* L_0 = (&__this->____charStream_24);
		NativeArray_1_Dispose_m11184C5FB1A1F3809B982476408F08F599F377F0(L_0, NativeArray_1_Dispose_m11184C5FB1A1F3809B982476408F08F599F377F0_RuntimeMethod_var);
		bool L_1 = ___0_disposing;
		StreamReader_Dispose_mB7BA2F3F47444F6D00457E04462BC097EEE6D27C(__this, L_1, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLibCore.Ply.PlyTexture::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyTexture_get_Name_mBEFD4F597E9EFA732438C3FF7C61017F48D30B7E (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_Name_m2A4A4F5872D9CCDCA7F8501216FD640B07E8CB94 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyTexture::get_Used()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyTexture_get_Used_mB6D66B20FF9AC7347214BBCFC146B38E60420B43 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CUsedU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_Used(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_Used_m2158084FEC62D1A067BE6B97270C3CA8F3CF31D1 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CUsedU3Ek__BackingField_1 = L_0;
		return;
	}
}
// TriLibCore.Interfaces.ITexture TriLibCore.Ply.PlyTexture::GetSubTexture(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyTexture_GetSubTexture_m3E72C0D206B1A6F808BEA3B7B1266AEB3FB3392A (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		return __this;
	}
}
// System.Int32 TriLibCore.Ply.PlyTexture::GetSubTextureCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyTexture_GetSubTextureCount_m4538FFA4B3A00E6D9E364BE8F4DC15CA466C9128 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		return 0;
	}
}
// System.Single TriLibCore.Ply.PlyTexture::GetWeight(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlyTexture_GetWeight_mFFDC9B3528C1820C1017E51AC31FBA0E55365FC8 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		return (1.0f);
	}
}
// System.Void TriLibCore.Ply.PlyTexture::AddTexture(TriLibCore.Interfaces.ITexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_AddTexture_m73D9A56E388C91ABB83779F754A5998C5001F474 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, RuntimeObject* ___0_texture, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Byte[] TriLibCore.Ply.PlyTexture::get_Data()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* PlyTexture_get_Data_mD852605F1E6DA0235B754942A52CF54434F46332 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = __this->___U3CDataU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_Data(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_Data_mBDBC63F40B5E78210872E05F5A9FBC585ACD278B (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_value, const RuntimeMethod* method) 
{
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_value;
		__this->___U3CDataU3Ek__BackingField_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CDataU3Ek__BackingField_2), (void*)L_0);
		return;
	}
}
// System.IO.Stream TriLibCore.Ply.PlyTexture::get_DataStream()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* PlyTexture_get_DataStream_m5A17DA76F612299803469FAEFDEA709C98107401 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = __this->___U3CDataStreamU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_DataStream(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_DataStream_mCCB91D79559CFEC8DFC72830801BB5B7FC969AEA (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_value, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_value;
		__this->___U3CDataStreamU3Ek__BackingField_3 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CDataStreamU3Ek__BackingField_3), (void*)L_0);
		return;
	}
}
// System.String TriLibCore.Ply.PlyTexture::get_Filename()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyTexture_get_Filename_mC7B533AE00B2654EC706CFF6F3DC659B7EA9E58D (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CFilenameU3Ek__BackingField_4;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_Filename(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_Filename_m7FA26BB14F1E9F04652E891D67858519F9040BD8 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CFilenameU3Ek__BackingField_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CFilenameU3Ek__BackingField_4), (void*)L_0);
		return;
	}
}
// UnityEngine.TextureWrapMode TriLibCore.Ply.PlyTexture::get_WrapModeU()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyTexture_get_WrapModeU_mD309B33EE013C6901560746D7587D1928E5EA424 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CWrapModeUU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_WrapModeU(UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_WrapModeU_m6A8A5818FA81F54CE6928BA4AAC3B789C23AB721 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CWrapModeUU3Ek__BackingField_5 = L_0;
		return;
	}
}
// UnityEngine.TextureWrapMode TriLibCore.Ply.PlyTexture::get_WrapModeV()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyTexture_get_WrapModeV_mC4A07729631007DA9D3E4C70B17EDD36C71A821D (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CWrapModeVU3Ek__BackingField_6;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_WrapModeV(UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_WrapModeV_m89975611FF6BA75906DCC8F7180E2B8FDAA80738 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CWrapModeVU3Ek__BackingField_6 = L_0;
		return;
	}
}
// UnityEngine.Vector2 TriLibCore.Ply.PlyTexture::get_Tiling()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 PlyTexture_get_Tiling_m6F18FB67137D31A66961AB895D5BC3BBB0D89382 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___U3CTilingU3Ek__BackingField_7;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_Tiling(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_Tiling_m558A662EBFEC677CAA22BBAA7394CCA7A27C59A0 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_value;
		__this->___U3CTilingU3Ek__BackingField_7 = L_0;
		return;
	}
}
// UnityEngine.Vector2 TriLibCore.Ply.PlyTexture::get_Offset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 PlyTexture_get_Offset_mC60B9CD51C47BDD9ACBD1E96772CA66776EEA6F7 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___U3COffsetU3Ek__BackingField_8;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_Offset(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_Offset_m01706A91BF5644A3F4C87D38F8DA69E63614B0EB (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_value;
		__this->___U3COffsetU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.Int32 TriLibCore.Ply.PlyTexture::get_TextureId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyTexture_get_TextureId_mBFF620CF2B87395C2336C48C3993CF2F68C3E7AE (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CTextureIdU3Ek__BackingField_9;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_TextureId(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_TextureId_m82D3E193C192B94A0CB70BAD0DD4D379CCFCE93D (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CTextureIdU3Ek__BackingField_9 = L_0;
		return;
	}
}
// System.String TriLibCore.Ply.PlyTexture::get_ResolvedFilename()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyTexture_get_ResolvedFilename_mCC29A905ACAEBB24C79F3058B53DE90D983B7433 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CResolvedFilenameU3Ek__BackingField_10;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_ResolvedFilename(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_ResolvedFilename_m6CFA9390509ECD95FC2AF54598A802CEFD9ED02B (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CResolvedFilenameU3Ek__BackingField_10 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CResolvedFilenameU3Ek__BackingField_10), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyTexture::get_IsValid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyTexture_get_IsValid_m81D09C71AD5586D2BCF45EAD378C33CD5CFBF8E4 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0;
		L_0 = PlyTexture_get_Filename_mC7B533AE00B2654EC706CFF6F3DC659B7EA9E58D_inline(__this, NULL);
		bool L_1;
		L_1 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_0, NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean TriLibCore.Ply.PlyTexture::get_HasAlpha()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyTexture_get_HasAlpha_mA3F510A77B3BB8BF37E7BE0830C63118BB2EEB52 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CHasAlphaU3Ek__BackingField_11;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_HasAlpha(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_HasAlpha_mFE4A6378088857B93F1C9643E936D697134BC1FA (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CHasAlphaU3Ek__BackingField_11 = L_0;
		return;
	}
}
// TriLibCore.General.TextureFormat TriLibCore.Ply.PlyTexture::get_TextureFormat()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyTexture_get_TextureFormat_m5892960AFA4FCAFC1615AF75C20588A62964835B (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CTextureFormatU3Ek__BackingField_12;
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::set_TextureFormat(TriLibCore.General.TextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture_set_TextureFormat_mA335B2FEF86388906CDE6E9D5E27CF67B47E49F4 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CTextureFormatU3Ek__BackingField_12 = L_0;
		return;
	}
}
// System.Boolean TriLibCore.Ply.PlyTexture::Equals(TriLibCore.Interfaces.ITexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyTexture_Equals_mF5DDD90F3482EE47AF94B0B5C612A8664E339346 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, RuntimeObject* ___0_other, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_other;
		bool L_1;
		L_1 = TextureComparators_TextureEquals_m566CCA88570A7A060514DCAEF48AE3170D743679(__this, L_0, NULL);
		return L_1;
	}
}
// System.Boolean TriLibCore.Ply.PlyTexture::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlyTexture_Equals_m8C4120AD837109F0CEF20033A00072AC3ECEC8BB (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_obj;
		bool L_1;
		L_1 = TextureComparators_Equals_mA1D187553F7AC8EB27F3C8D0F2D1316C5E05E4AC(__this, L_0, NULL);
		return L_1;
	}
}
// System.Int32 TriLibCore.Ply.PlyTexture::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlyTexture_GetHashCode_mD9596B678E88C65D4A87005AF2DBADDF35F7E169 (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0;
		L_0 = TextureComparators_GetHashCode_mF57C0A300F03E349E694DB594CA2FF73427BECA3(__this, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Ply.PlyTexture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyTexture__ctor_m038AAFCCC1A0BFA24B9DB741380AA1D0CDE29AFD (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		L_0 = Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline(NULL);
		__this->___U3CTilingU3Ek__BackingField_7 = L_0;
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Ply.Program::Main(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Program_Main_m9CFEA665C57B50AFD2C57ADF4534A642D295646B (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___0_args, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Void TriLibCore.Ply.Program::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Program__ctor_m65C48C6E4A2BAA4D6AC13AE1AC7BCED07F458779 (Program_tF221987E986ED204179045A85D41CBDBA2C0B625* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String[] TriLibCore.Ply.Reader.PlyReader::GetExtensions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* PlyReader_GetExtensions_mADB809B44873C6BAB6EB3974C5B294FD0BA7808F (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4BEF98DAC2D5CE8B3F877FFD83A459125B80DA8F);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral4BEF98DAC2D5CE8B3F877FFD83A459125B80DA8F);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4BEF98DAC2D5CE8B3F877FFD83A459125B80DA8F);
		return L_1;
	}
}
// System.String TriLibCore.Ply.Reader.PlyReader::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlyReader_get_Name_m8A13FB02194F4DAC2F7FEB1B50242E3C28273811 (PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE045B603FBBCEA228EE757E3C5C11BD708A2FA34);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteralE045B603FBBCEA228EE757E3C5C11BD708A2FA34;
	}
}
// System.Type TriLibCore.Ply.Reader.PlyReader::get_LoadingStepEnumType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* PlyReader_get_LoadingStepEnumType_mDD3C69A2F46EE7D798ABD8FF82BDC1334251ED40 (PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProcessingSteps_t66470AEDB7538A34AC55F1AB85D626C1264892F4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_0 = { reinterpret_cast<intptr_t> (ProcessingSteps_t66470AEDB7538A34AC55F1AB85D626C1264892F4_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_1;
		L_1 = Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57(L_0, NULL);
		return L_1;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Ply.Reader.PlyReader::ReadStream(System.IO.Stream,TriLibCore.AssetLoaderContext,System.String,System.Action`2<TriLibCore.AssetLoaderContext,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyReader_ReadStream_mC803A450BD72518AE6C3C7EF624D694F5E9FCFA7 (PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, String_t* ___2_filename, Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___3_onProgress, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_stream;
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_1 = ___1_assetLoaderContext;
		String_t* L_2 = ___2_filename;
		Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* L_3 = ___3_onProgress;
		RuntimeObject* L_4;
		L_4 = ReaderBase_ReadStream_m725378DF096B29E0DB3BE3FB9E5F1E37747883F4(__this, L_0, L_1, L_2, L_3, NULL);
		ReaderBase_SetupStream_mCDC78453E3657CB3FBB713C40FB50B4941455942(__this, (&___0_stream), NULL);
		PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F* L_5 = (PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F*)il2cpp_codegen_object_new(PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		PlyProcessor__ctor_mEFE18083343802C21877A430538A1304EC1DC5E7(L_5, NULL);
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_6 = ___0_stream;
		NullCheck(L_5);
		RuntimeObject* L_7;
		L_7 = PlyProcessor_Process_m5EC73F6D1F63A911A1C86747BB95E7948D0D0763(L_5, __this, L_6, NULL);
		V_0 = L_7;
		ReaderBase_PostProcessModel_mD3BB953DE8BEB5FCA65077455BDA0750F740C667(__this, (&V_0), NULL);
		RuntimeObject* L_8 = V_0;
		return L_8;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Ply.Reader.PlyReader::CreateRootModel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlyReader_CreateRootModel_m42A94C2A8E4CF0D4129310F170C1B33D232272BD (PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* L_0 = (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237*)il2cpp_codegen_object_new(PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		PlyRootModel__ctor_m70F96F0795DD6B57ADAEE28C419A56033958CEED(L_0, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Ply.Reader.PlyReader::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlyReader__ctor_mCFE77FCB6113FDA8EF94DE3F7516694E1C75469C (PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* __this, const RuntimeMethod* method) 
{
	{
		ReaderBase__ctor_m5C4FE7A4BC205B65DAB56FF3CC5202D0B04937DA(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mB50217951591A045844C61E7FF31EEE3FEF16737_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___zeroVector_5;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Vector4_get_zero_m3D61F5FA9483CD9C08977D9D8852FB448B4CE6D1_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_0 = ((Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var))->___zeroVector_5;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PlyModel_get_Name_mF0BDCE3B91DCB49F6E648F411F06E052FF253F89_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyTexture_set_Filename_m7FA26BB14F1E9F04652E891D67858519F9040BD8_inline (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CFilenameU3Ek__BackingField_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CFilenameU3Ek__BackingField_4), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int64_t PlyStreamReader_get_Position_m9211CA1BF5C622AACC7B1C9688FE42EB58006B5B_inline (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, const RuntimeMethod* method) 
{
	{
		int64_t L_0 = __this->___U3CPositionU3Ek__BackingField_27;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ReaderBase_get_AssetLoaderContext_mAEAA12FFAAC1C276F35397E563CB157D7CFB36BA_inline (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, const RuntimeMethod* method) 
{
	{
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_0 = __this->___U3CAssetLoaderContextU3Ek__BackingField_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_mCD6889CDE39F18704CD6EA8E2EFBFA48BA3E13B0_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_r;
		__this->___r_0 = L_0;
		float L_1 = ___1_g;
		__this->___g_1 = L_1;
		float L_2 = ___2_b;
		__this->___b_2 = L_2;
		__this->___a_3 = (1.0f);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyMaterial_set_Index_mEEC9E37FB853F46E7A19C7A52BE8EACD41464726_inline (PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CIndexU3Ek__BackingField_3 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x_2 = L_0;
		float L_1 = ___1_y;
		__this->___y_3 = L_1;
		float L_2 = ___2_z;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		float L_2 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___0_a;
		float L_4 = L_3.___y_3;
		float L_5 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_a;
		float L_7 = L_6.___z_4;
		float L_8 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x_0 = L_0;
		float L_1 = ___1_y;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, float ___3_a, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_r;
		__this->___r_0 = L_0;
		float L_1 = ___1_g;
		__this->___g_1 = L_1;
		float L_2 = ___2_b;
		__this->___b_2 = L_2;
		float L_3 = ___3_a;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_Visibility_mB2F9D43D393D00418E2B4A999974A1E18512D0A5_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CVisibilityU3Ek__BackingField_5 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_one_mC9B289F1E15C42C597180C9FE6FB492495B51D02_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___oneVector_6;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_LocalScale_m8055103EC8ADECCC5D3C535C098B33AA2E0275A8_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->___U3CLocalScaleU3Ek__BackingField_4 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_m7E701AE095ED10FD5EA0B50ABCFDE2EEFF2173A5_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ((Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var))->___identityQuaternion_4;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_LocalRotation_mA70C0956246A032C3C1C7EF5DE31A303ACE92BCB_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_value, const RuntimeMethod* method) 
{
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___0_value;
		__this->___U3CLocalRotationU3Ek__BackingField_3 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_GeometryGroup_mC40A2CC9BD75EBA9305AA0FC6E9F1FFD51FDA055_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CGeometryGroupU3Ek__BackingField_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CGeometryGroupU3Ek__BackingField_9), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyRootModel_set_AllMaterials_m30733607EFAAACE9110C2452A299D1D47D799C53_inline (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllMaterialsU3Ek__BackingField_17 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllMaterialsU3Ek__BackingField_17), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyRootModel_set_AllTextures_m7646524ECF24096720D3C48A840F554283D6212E_inline (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllTexturesU3Ek__BackingField_18 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllTexturesU3Ek__BackingField_18), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyRootModel_set_AllGeometryGroups_m44BF8C7B4B75BCAA1C25B38D31F5FB90019FD3F1_inline (PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllGeometryGroupsU3Ek__BackingField_15 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllGeometryGroupsU3Ek__BackingField_15), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyModel_set_MaterialIndices_m87FB1DB52736B0D10EBF7CA05C04C3B1A321F670_inline (PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CMaterialIndicesU3Ek__BackingField_12 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CMaterialIndicesU3Ek__BackingField_12), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InterpolatedVertex_SetPosition_m51A3B70131151C51E738A6EDCD4E5EBEF7C80E2C_inline (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, RuntimeObject* ___1_geometryGroup, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->____position_0 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InterpolatedVertex_SetNormal_m48EE21D1CFF1D80F3EAAD2689ABEE84102882ADC_inline (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, RuntimeObject* ___1_geometryGroup, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->____normal_1 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InterpolatedVertex_SetColor_m01F89204D2B248BE23E94620AE338F73D8C90AE5_inline (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, RuntimeObject* ___1_geometryGroup, const RuntimeMethod* method) 
{
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___0_value;
		__this->____color_3 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InterpolatedVertex_SetUV1_mA7C291101768A0EE2734CD2589D815DCC2454551_inline (InterpolatedVertex_t57E0770B25FEB89C3BF25E3D491AD3B00C9171B5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, RuntimeObject* ___1_geometryGroup, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_value;
		__this->____uv0_4 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Min_m888083F74FF5655778F0403BB5E9608BEFDEA8CB_inline (int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___0_a;
		int32_t L_1 = ___1_b;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_2 = ___1_b;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		int32_t L_3 = ___0_a;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlyStreamReader_set_Position_m763DA88CC440E9EAA48B2638BC5D5E717BB8C3F4_inline (PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2* __this, int64_t ___0_value, const RuntimeMethod* method) 
{
	{
		int64_t L_0 = ___0_value;
		__this->___U3CPositionU3Ek__BackingField_27 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PlyTexture_get_Filename_mC7B533AE00B2654EC706CFF6F3DC659B7EA9E58D_inline (PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CFilenameU3Ek__BackingField_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___oneVector_3;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___0_item;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___0_item;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_mE396DC57C362877713861B0AD2C88728648A8364_gshared_inline (Enumerator_t101EECDB54BD1F67FD91D0D649333AC62D79665D* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->____currentValue_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m37A1197D84857A9512633D2FB5380FCAC7AB381E_gshared_inline (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___0_item, const RuntimeMethod* method) 
{
	PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382* L_1 = (PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		PlyValueU5BU5D_tE3742EA175D697AD95C6B319F1C6FC18D7BE0382* L_6 = V_0;
		int32_t L_7 = V_1;
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_8 = ___0_item;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41)L_8);
		return;
	}

IL_0034:
	{
		PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 L_9 = ___0_item;
		((  void (*) (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB*, PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m884B5579B75AACDA563355ED79C1E26F7580A820_gshared_inline (List_1_t96CD26CA074FBF4F15AF92A0DA34300AF589BECB* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
