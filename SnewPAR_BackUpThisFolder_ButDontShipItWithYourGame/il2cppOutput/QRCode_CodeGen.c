﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void BigIntegerLibrary.Base10BigInteger::set_NumberSign(BigIntegerLibrary.Sign)
extern void Base10BigInteger_set_NumberSign_m3CBEEB960CA88981C33703FBB58BF37D455B9112 (void);
// 0x00000002 System.Void BigIntegerLibrary.Base10BigInteger::.ctor()
extern void Base10BigInteger__ctor_m2273BDB906101307D44FCCE9F52F4CE93698065B (void);
// 0x00000003 System.Void BigIntegerLibrary.Base10BigInteger::.ctor(System.Int64)
extern void Base10BigInteger__ctor_m1E816033C5D0510D1A8C59F50FA9F29049B57486 (void);
// 0x00000004 System.Void BigIntegerLibrary.Base10BigInteger::.ctor(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger__ctor_m29460F91765C229C67A0ADCA792E11B169962E65 (void);
// 0x00000005 System.Boolean BigIntegerLibrary.Base10BigInteger::Equals(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Equals_mC3ADDEDC54C6F21ED5C83D80CE693A49BF13D817 (void);
// 0x00000006 System.Boolean BigIntegerLibrary.Base10BigInteger::Equals(System.Object)
extern void Base10BigInteger_Equals_m1D61683BB526AE54AE5BAC5E6B230DB8D0A2242D (void);
// 0x00000007 System.Int32 BigIntegerLibrary.Base10BigInteger::GetHashCode()
extern void Base10BigInteger_GetHashCode_mCEA6B96D8A87B2F01BF230EF4CECE2358CB54B98 (void);
// 0x00000008 System.String BigIntegerLibrary.Base10BigInteger::ToString()
extern void Base10BigInteger_ToString_m546FE1B2DCB8DC94B32B26611ECF4A0C21962469 (void);
// 0x00000009 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Opposite(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Opposite_m9627239D3B2B420630A87CB0086D1B3644FB5C03 (void);
// 0x0000000A System.Boolean BigIntegerLibrary.Base10BigInteger::Greater(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Greater_m991E62E0384D05EAA0F4720873CC0F776C3463BD (void);
// 0x0000000B System.Boolean BigIntegerLibrary.Base10BigInteger::GreaterOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_GreaterOrEqual_mFAF54BF8FE925C04AC08362E2326231702202162 (void);
// 0x0000000C System.Boolean BigIntegerLibrary.Base10BigInteger::Smaller(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Smaller_mDEE792A5B1CDB479264345D0BC74A96590745ABF (void);
// 0x0000000D System.Boolean BigIntegerLibrary.Base10BigInteger::SmallerOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_SmallerOrEqual_m02A3F782241E20BBD27F16A3F9E6C28FC98E49C3 (void);
// 0x0000000E BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Abs(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Abs_mDAE66DB7F5058341955697D88026326A18FAA4CE (void);
// 0x0000000F BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Addition(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Addition_mB911FF3FE09C408E7C0CEC109B9E833722DEC87A (void);
// 0x00000010 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Subtraction(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Subtraction_m9ADCFEB634BC865E5ECAE9785CF64D0327D8A8D6 (void);
// 0x00000011 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Multiplication(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Multiplication_mB76DAC17641C7456EE6B1F9A72206178A2389326 (void);
// 0x00000012 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Implicit(System.Int64)
extern void Base10BigInteger_op_Implicit_m0685F5AC655D7C13DF0A1FE975EEF1CAF25A00D7 (void);
// 0x00000013 System.Boolean BigIntegerLibrary.Base10BigInteger::op_Equality(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Equality_m13F7E4701245AAB73FCC8CE8AA59DB5C0A04B381 (void);
// 0x00000014 System.Boolean BigIntegerLibrary.Base10BigInteger::op_Inequality(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Inequality_m6F8661DF01213A719228ECD03782A5125AE89BA0 (void);
// 0x00000015 System.Boolean BigIntegerLibrary.Base10BigInteger::op_GreaterThan(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_GreaterThan_mE75DE386018D276F75E49EF0F2A150125B43FAF9 (void);
// 0x00000016 System.Boolean BigIntegerLibrary.Base10BigInteger::op_LessThan(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_LessThan_m96BB5D23C35202C986D40212555A807AE47582C1 (void);
// 0x00000017 System.Boolean BigIntegerLibrary.Base10BigInteger::op_GreaterThanOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_GreaterThanOrEqual_mD7D739D19B279A8B7C6B4C02071BF325B393E35E (void);
// 0x00000018 System.Boolean BigIntegerLibrary.Base10BigInteger::op_LessThanOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_LessThanOrEqual_m6DCB27A0960D64EB89A2F821BEB4901A96ED47C1 (void);
// 0x00000019 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_UnaryNegation(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_UnaryNegation_mCF591899D7E4535C8D4939FEB7200BDFAE92B486 (void);
// 0x0000001A BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Addition(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Addition_m1BC10FBF7D3701ABC3F4ACA195C13210C187682A (void);
// 0x0000001B BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Subtraction(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Subtraction_mDA18AB789365062AB84967A3A1C187B24D60BC39 (void);
// 0x0000001C BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Multiply(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Multiply_m1222CC74F9C22E5E362DCC19F879B41F527F7585 (void);
// 0x0000001D BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Increment(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Increment_m2822BBD11A7DE88A9C433B70C9E27C1299FC4F16 (void);
// 0x0000001E BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Decrement(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Decrement_mD8FF66EF3FBAB883813C156F2C51F67999BBFA2A (void);
// 0x0000001F BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Add(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Add_m1E7A26B05EFAD56D157BF8B97A2A3AC4C1FEB6E5 (void);
// 0x00000020 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Subtract(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Subtract_m9D67EBA5C58A646EAAF3DCEC844DAD1D97B6AFB7 (void);
// 0x00000021 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Multiply(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Multiply_mE06CD7BCE17363F991E9F9690F528E27151CDEFE (void);
// 0x00000022 System.Void BigIntegerLibrary.Base10BigInteger::.cctor()
extern void Base10BigInteger__cctor_mD76CAC1761B36FD8B409DA0FD94D1125B5D38A4E (void);
// 0x00000023 System.Void BigIntegerLibrary.Base10BigInteger/DigitContainer::.ctor()
extern void DigitContainer__ctor_mE96E0E4FD9ADD5F5504976750E8E93C09D212A05 (void);
// 0x00000024 System.Int64 BigIntegerLibrary.Base10BigInteger/DigitContainer::get_Item(System.Int32)
extern void DigitContainer_get_Item_m2002FDF39EE1E5CA730DC3A5D99CFA60C2681D11 (void);
// 0x00000025 System.Void BigIntegerLibrary.Base10BigInteger/DigitContainer::set_Item(System.Int32,System.Int64)
extern void DigitContainer_set_Item_m7BD6A5D93F25D8C7939C957D7122AD90B9E10D3C (void);
// 0x00000026 System.Void BigIntegerLibrary.BigInteger::.ctor()
extern void BigInteger__ctor_mF7BC073C14A4A5BF155D50E331C36BA3A79E6FDA (void);
// 0x00000027 System.Void BigIntegerLibrary.BigInteger::.ctor(System.Int64)
extern void BigInteger__ctor_m3A4B616C14071AFD89475CAEF3469D2F9DB29780 (void);
// 0x00000028 System.Void BigIntegerLibrary.BigInteger::.ctor(BigIntegerLibrary.BigInteger)
extern void BigInteger__ctor_m4922D6DC6EFE1906B6EEC64E1025E100AF76B17C (void);
// 0x00000029 System.Void BigIntegerLibrary.BigInteger::.ctor(System.String)
extern void BigInteger__ctor_m0F1DA20ED66B2C4C0909E39F21728D5D5A6A2F71 (void);
// 0x0000002A System.Void BigIntegerLibrary.BigInteger::.ctor(System.Byte[])
extern void BigInteger__ctor_mCC52D4B4631B1485836B76CF4CA284477409CDC2 (void);
// 0x0000002B System.Boolean BigIntegerLibrary.BigInteger::Equals(BigIntegerLibrary.BigInteger)
extern void BigInteger_Equals_m593E60E9D0A83A0494C9DC3E367242EF1FBDCD76 (void);
// 0x0000002C System.Boolean BigIntegerLibrary.BigInteger::Equals(System.Object)
extern void BigInteger_Equals_mCABABD5A521E597E5A43AD0230463FA3E743FA5B (void);
// 0x0000002D System.Int32 BigIntegerLibrary.BigInteger::GetHashCode()
extern void BigInteger_GetHashCode_mCB7D4CE28031AC0090F1666B13437756D81B1F8B (void);
// 0x0000002E System.String BigIntegerLibrary.BigInteger::ToString()
extern void BigInteger_ToString_mC96A57AF89EF8E4D88CE5BB235FD887C44354C01 (void);
// 0x0000002F BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Parse(System.String)
extern void BigInteger_Parse_m0A604796C78F95B8C6971D89FF61DB8E2D1DF5FD (void);
// 0x00000030 System.Int32 BigIntegerLibrary.BigInteger::CompareTo(BigIntegerLibrary.BigInteger)
extern void BigInteger_CompareTo_m3F8D479A0DDAED3839208EF468C8847234EF15D7 (void);
// 0x00000031 System.Int32 BigIntegerLibrary.BigInteger::CompareTo(System.Object)
extern void BigInteger_CompareTo_mA0D23C7174780E7A3501DE1118E778E1F46AEAC2 (void);
// 0x00000032 System.Int32 BigIntegerLibrary.BigInteger::SizeInBinaryDigits(BigIntegerLibrary.BigInteger)
extern void BigInteger_SizeInBinaryDigits_m6A34C73BA5239B1DEBB82FFD83E2FEB54DC42584 (void);
// 0x00000033 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Opposite(BigIntegerLibrary.BigInteger)
extern void BigInteger_Opposite_mC3D6369A4C36F62603D9C77A7E9796542BE6DB46 (void);
// 0x00000034 System.Boolean BigIntegerLibrary.BigInteger::Greater(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Greater_m2631B31035E3E95B344027E7B2D37ADB79702726 (void);
// 0x00000035 System.Boolean BigIntegerLibrary.BigInteger::GreaterOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_GreaterOrEqual_m040D3711542E6F723D9389182AAEAB8D04F3A7B6 (void);
// 0x00000036 System.Boolean BigIntegerLibrary.BigInteger::Smaller(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Smaller_m94B9E580C5825DBF94DB1699B20D90E03815A6FD (void);
// 0x00000037 System.Boolean BigIntegerLibrary.BigInteger::SmallerOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_SmallerOrEqual_mFCC06F698E308A2ADCFEF55D68716D7D48E1669A (void);
// 0x00000038 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Abs(BigIntegerLibrary.BigInteger)
extern void BigInteger_Abs_m0A83710115BA607534160F5B9578325AAEBEE626 (void);
// 0x00000039 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Addition(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Addition_m5BA4AF480D8A852A7E0E9F54A064F38601B78EFE (void);
// 0x0000003A BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Subtraction(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Subtraction_m70F5F79695B71209BD4F7474380DD2AFDF38EB0C (void);
// 0x0000003B BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Multiplication(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Multiplication_m304F2E0FF33954FB8E426CC17CE3CE7E4ECD1309 (void);
// 0x0000003C BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Division(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Division_m58107FB745BED586C69CA97BD205D61181282D0A (void);
// 0x0000003D BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Modulo(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Modulo_m44E945B7593ED7D1EACA32BDCCFD002D77C30471 (void);
// 0x0000003E BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Power(BigIntegerLibrary.BigInteger,System.Int32)
extern void BigInteger_Power_mF525705FBD5DFEF89D8840E763041FA6B6CB8834 (void);
// 0x0000003F BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::IntegerSqrt(BigIntegerLibrary.BigInteger)
extern void BigInteger_IntegerSqrt_mF4565B60C64FB6AE57A55A52B2C1BFA216B0B841 (void);
// 0x00000040 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Gcd(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Gcd_m75759765A38289F3174DBE922113703D87A9CD3B (void);
// 0x00000041 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::ExtendedEuclidGcd(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger&,BigIntegerLibrary.BigInteger&)
extern void BigInteger_ExtendedEuclidGcd_mA1898B2F872FD74D51C12AC377CDF57C814FC5B6 (void);
// 0x00000042 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::ModularInverse(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_ModularInverse_mD966998BDA1E35139D271A4DDB13579FAB3A7FC7 (void);
// 0x00000043 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::ModularExponentiation(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_ModularExponentiation_m324AAB8BD416479D2920D6AA3D77D39D01166F99 (void);
// 0x00000044 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Implicit(System.Int64)
extern void BigInteger_op_Implicit_m92DFC46C439922D1C2169726D7D01E9937A63EE7 (void);
// 0x00000045 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Implicit(System.Int32)
extern void BigInteger_op_Implicit_mEE89EC87D3631AFA44476A240B5916EE9047E37B (void);
// 0x00000046 System.Int32 BigIntegerLibrary.BigInteger::op_Explicit(BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Explicit_mDB4A52576F2E40D6A396EF06603547D359389884 (void);
// 0x00000047 System.UInt64 BigIntegerLibrary.BigInteger::op_Explicit(BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Explicit_m918C4F76430F57F0589E122D1C000D7C0879F37F (void);
// 0x00000048 System.Boolean BigIntegerLibrary.BigInteger::op_Equality(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Equality_mC64F6BC20A3586F38CDB2E5719E3CAB922DAF060 (void);
// 0x00000049 System.Boolean BigIntegerLibrary.BigInteger::op_Inequality(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Inequality_mF07EE7023BB167AA89B9C5CB40C1578E33B0B3FC (void);
// 0x0000004A System.Boolean BigIntegerLibrary.BigInteger::op_GreaterThan(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_GreaterThan_m414F57022733FC732A74EF035D2A0C7E147F808F (void);
// 0x0000004B System.Boolean BigIntegerLibrary.BigInteger::op_LessThan(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_LessThan_m61607ED5FB0FA6C6BF7ADDA54151414B9F0AC719 (void);
// 0x0000004C System.Boolean BigIntegerLibrary.BigInteger::op_GreaterThanOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_GreaterThanOrEqual_m43827EA51AC5682E7377862A8F622275F35A2C9B (void);
// 0x0000004D System.Boolean BigIntegerLibrary.BigInteger::op_LessThanOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_LessThanOrEqual_m5DFFECAC95C64FDF3171FF270ACFD62CA7ACBDA8 (void);
// 0x0000004E BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_UnaryNegation(BigIntegerLibrary.BigInteger)
extern void BigInteger_op_UnaryNegation_m6DC2B6390DED160704AC4B32ABC0BF45E6E48961 (void);
// 0x0000004F BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Addition(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Addition_mF91A9CF94AF1349C601942CD1135FB7810D62DA6 (void);
// 0x00000050 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Subtraction(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Subtraction_m04B0E1F0677A56A57767B6ECDBD29A64DEB4788C (void);
// 0x00000051 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Multiply(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Multiply_mB31F216B74A4442DF3E7E55C96B103EA2FF66A73 (void);
// 0x00000052 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Division(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Division_m397F1F0F1B51A04E6E79A149BF6F8928A3DB5319 (void);
// 0x00000053 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Modulus(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Modulus_mE06DD76AF860B482C94EFF95173AA8FE00C734BC (void);
// 0x00000054 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Increment(BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Increment_mD47A731032CD6572BDC6A585E3AED71A610EE1AF (void);
// 0x00000055 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Decrement(BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Decrement_m471BB568AB209CC3B9DC59C0CDDE0BB7B00D6BE8 (void);
// 0x00000056 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Add(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Add_m488ABB1B939F38012AF9A6E085B706EB512DA538 (void);
// 0x00000057 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Subtract(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Subtract_m570C3F53425F8673CF1A8FAF32E7054A895F7FB1 (void);
// 0x00000058 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Multiply(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Multiply_m9F382102DC23B1B7499543BAEF29E6027530D5B0 (void);
// 0x00000059 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::DivideByOneDigitNumber(BigIntegerLibrary.BigInteger,System.Int64)
extern void BigInteger_DivideByOneDigitNumber_m6C7A70A338F34FF5F5739A4179488C868F240D50 (void);
// 0x0000005A BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::DivideByBigNumber(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_DivideByBigNumber_m6111F0FAD547C58B23880A9F0C44E068457DDE5E (void);
// 0x0000005B System.Boolean BigIntegerLibrary.BigInteger::DivideByBigNumberSmaller(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern void BigInteger_DivideByBigNumberSmaller_m1D55B72BE40B8E24E80F56FE38834D4E51E61434 (void);
// 0x0000005C System.Void BigIntegerLibrary.BigInteger::Difference(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern void BigInteger_Difference_m207942CDDDCC2FE903B33185A49AEEC6A10F71E1 (void);
// 0x0000005D System.Int64 BigIntegerLibrary.BigInteger::Trial(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern void BigInteger_Trial_m832DF33375318AC2C976D1B237E5DFEF41067D2D (void);
// 0x0000005E System.Void BigIntegerLibrary.BigInteger::.cctor()
extern void BigInteger__cctor_m5C2306EC90F556C8B01A65A991CD735EFDD4AA35 (void);
// 0x0000005F System.Void BigIntegerLibrary.BigInteger/DigitContainer::.ctor()
extern void DigitContainer__ctor_m4E1C715EFB266E166E880E9D25D7D44E1C239D49 (void);
// 0x00000060 System.Int64 BigIntegerLibrary.BigInteger/DigitContainer::get_Item(System.Int32)
extern void DigitContainer_get_Item_m19B7181151F0D6F301C3DA5F3045D95C86C256E2 (void);
// 0x00000061 System.Void BigIntegerLibrary.BigInteger/DigitContainer::set_Item(System.Int32,System.Int64)
extern void DigitContainer_set_Item_m7493A35A896C467B7F46E8F722E90626A11FFCED (void);
// 0x00000062 System.Void BigIntegerLibrary.BigIntegerException::.ctor(System.String,System.Exception)
extern void BigIntegerException__ctor_mF132DC3F3DF515B8363B0EEFFBA61C6F630EEF3B (void);
// 0x00000063 System.Void ZXing.BarcodeReader::.ctor()
extern void BarcodeReader__ctor_m3BFF43D19C843D8342FEBB9FBD933AFDC7ACAFE3 (void);
// 0x00000064 System.Void ZXing.BarcodeReader::.ctor(ZXing.Reader,System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>)
extern void BarcodeReader__ctor_mEA1B103E92743F4810E2D5EBF2FEFDB4844D6FD6 (void);
// 0x00000065 System.Void ZXing.BarcodeReader::.ctor(ZXing.Reader,System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>,System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>)
extern void BarcodeReader__ctor_m875C63EBB5CBE0E29CDBFC7A2CA1F621B1EC2856 (void);
// 0x00000066 System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReader::get_CreateLuminanceSource()
extern void BarcodeReader_get_CreateLuminanceSource_mEC53C67706472E1B070BBC96C9E1CE46973C9316 (void);
// 0x00000067 ZXing.Result ZXing.BarcodeReader::Decode(UnityEngine.Color32[],System.Int32,System.Int32)
extern void BarcodeReader_Decode_m95E2A7827061249D702871705584449403395CBA (void);
// 0x00000068 ZXing.Result[] ZXing.BarcodeReader::DecodeMultiple(UnityEngine.Color32[],System.Int32,System.Int32)
extern void BarcodeReader_DecodeMultiple_mABC648C145A9C4A70634AC36CF0DA1C7858B1DA5 (void);
// 0x00000069 System.Void ZXing.BarcodeReader::.cctor()
extern void BarcodeReader__cctor_m84BEA0972D0D5EB0B0156C6ED9915672642A86DD (void);
// 0x0000006A System.Void ZXing.BarcodeReader/<>c::.cctor()
extern void U3CU3Ec__cctor_m6812D4B1A3BD71E61074C4B021B576D21FEB178D (void);
// 0x0000006B System.Void ZXing.BarcodeReader/<>c::.ctor()
extern void U3CU3Ec__ctor_m44A8C0A014F3CB23B1145F46A2ADDE46E8A766DB (void);
// 0x0000006C ZXing.LuminanceSource ZXing.BarcodeReader/<>c::<.cctor>b__9_0(UnityEngine.Color32[],System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__9_0_mB20CDC75F0A38586D237D370E782FE764E9510B3 (void);
// 0x0000006D System.Void ZXing.BarcodeReader`1::.ctor(System.Func`2<T,ZXing.LuminanceSource>)
// 0x0000006E System.Void ZXing.BarcodeReader`1::.ctor(ZXing.Reader,System.Func`2<T,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>)
// 0x0000006F System.Void ZXing.BarcodeReader`1::.ctor(ZXing.Reader,System.Func`2<T,ZXing.LuminanceSource>,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>,System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>)
// 0x00000070 System.Void ZXing.BarcodeReader`1::.ctor(ZXing.Reader,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>,System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>)
// 0x00000071 System.Func`2<T,ZXing.LuminanceSource> ZXing.BarcodeReader`1::get_CreateLuminanceSource()
// 0x00000072 ZXing.Result ZXing.BarcodeReader`1::Decode(T)
// 0x00000073 ZXing.Result[] ZXing.BarcodeReader`1::DecodeMultiple(T)
// 0x00000074 ZXing.Common.DecodingOptions ZXing.BarcodeReaderGeneric::get_Options()
extern void BarcodeReaderGeneric_get_Options_mD9BF02898ABAA6D47217DF1A0E0573A9191242B2 (void);
// 0x00000075 System.Void ZXing.BarcodeReaderGeneric::set_Options(ZXing.Common.DecodingOptions)
extern void BarcodeReaderGeneric_set_Options_mC29AB244E4C57779A8036F7C3B075E34603B8BC6 (void);
// 0x00000076 ZXing.Reader ZXing.BarcodeReaderGeneric::get_Reader()
extern void BarcodeReaderGeneric_get_Reader_m2D3499BA7F4EE99F3BBEB1ABD527CA194D308808 (void);
// 0x00000077 System.Void ZXing.BarcodeReaderGeneric::add_ResultPointFound(System.Action`1<ZXing.ResultPoint>)
extern void BarcodeReaderGeneric_add_ResultPointFound_m19E2B2F32FA793343AF6D8FB0E3011BAB03ECB90 (void);
// 0x00000078 System.Void ZXing.BarcodeReaderGeneric::remove_ResultPointFound(System.Action`1<ZXing.ResultPoint>)
extern void BarcodeReaderGeneric_remove_ResultPointFound_mAB0FFAE72E90AF8CF01F52A90428FCCA3156841B (void);
// 0x00000079 System.Void ZXing.BarcodeReaderGeneric::add_explicitResultPointFound(System.Action`1<ZXing.ResultPoint>)
extern void BarcodeReaderGeneric_add_explicitResultPointFound_m4FEC1DF4002E66FC305A16838741F61893610E55 (void);
// 0x0000007A System.Void ZXing.BarcodeReaderGeneric::remove_explicitResultPointFound(System.Action`1<ZXing.ResultPoint>)
extern void BarcodeReaderGeneric_remove_explicitResultPointFound_m44C172C9EB6BE860EDA26B3CA82263E32E66705F (void);
// 0x0000007B System.Void ZXing.BarcodeReaderGeneric::add_ResultFound(System.Action`1<ZXing.Result>)
extern void BarcodeReaderGeneric_add_ResultFound_m75CD43C0E19066A35E412B6F1AE99D312C796FE4 (void);
// 0x0000007C System.Void ZXing.BarcodeReaderGeneric::remove_ResultFound(System.Action`1<ZXing.Result>)
extern void BarcodeReaderGeneric_remove_ResultFound_mA33EAA5B673FCAB3B568891AB81AF0C8F213B9C6 (void);
// 0x0000007D System.Boolean ZXing.BarcodeReaderGeneric::get_AutoRotate()
extern void BarcodeReaderGeneric_get_AutoRotate_mE2849C96B5915AC6DE1924217E4F7E4B5A12975D (void);
// 0x0000007E System.Void ZXing.BarcodeReaderGeneric::set_AutoRotate(System.Boolean)
extern void BarcodeReaderGeneric_set_AutoRotate_mA1394EB1D47270602573A2D3AB4C0E5CE3864413 (void);
// 0x0000007F System.Boolean ZXing.BarcodeReaderGeneric::get_TryInverted()
extern void BarcodeReaderGeneric_get_TryInverted_m1AC18A6A6B1E1F5EDF3C0F3675847D3A80C56F4D (void);
// 0x00000080 System.Void ZXing.BarcodeReaderGeneric::set_TryInverted(System.Boolean)
extern void BarcodeReaderGeneric_set_TryInverted_mED17767A8D1F3CA261F320EFA1D6B6F1DA99F26E (void);
// 0x00000081 System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric::get_CreateBinarizer()
extern void BarcodeReaderGeneric_get_CreateBinarizer_m96EE5FBB3EFBADE7BA7C2094C7307D6DFC63A1BA (void);
// 0x00000082 System.Void ZXing.BarcodeReaderGeneric::.ctor()
extern void BarcodeReaderGeneric__ctor_m9055324AD56FEE4AAEE0B58BA0D9E16FB79DA2E7 (void);
// 0x00000083 System.Void ZXing.BarcodeReaderGeneric::.ctor(ZXing.Reader,System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>,System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>)
extern void BarcodeReaderGeneric__ctor_mA06E10FDA65C709B91935303F5B64C0B5C6AC84A (void);
// 0x00000084 ZXing.Result ZXing.BarcodeReaderGeneric::Decode(ZXing.LuminanceSource)
extern void BarcodeReaderGeneric_Decode_m67D479F5AB9CEBDC506698862C45F0901FC47EE6 (void);
// 0x00000085 ZXing.Result[] ZXing.BarcodeReaderGeneric::DecodeMultiple(ZXing.LuminanceSource)
extern void BarcodeReaderGeneric_DecodeMultiple_mA6AFDC8A9E8FD6964F6B9661284EEB5CB453AFB6 (void);
// 0x00000086 System.Void ZXing.BarcodeReaderGeneric::OnResultsFound(System.Collections.Generic.IEnumerable`1<ZXing.Result>)
extern void BarcodeReaderGeneric_OnResultsFound_m0BD8C3CE10D8E42B3DC01EF9EE0F2AABC1A26DE8 (void);
// 0x00000087 System.Void ZXing.BarcodeReaderGeneric::OnResultFound(ZXing.Result)
extern void BarcodeReaderGeneric_OnResultFound_mC7F86B8BD1DD9ED684A2A3DF498B3157200AFF1C (void);
// 0x00000088 System.Void ZXing.BarcodeReaderGeneric::OnResultPointFound(ZXing.ResultPoint)
extern void BarcodeReaderGeneric_OnResultPointFound_m8574FB86B3F8AE5F0CBE4B2557E8D87FE0C52DD7 (void);
// 0x00000089 ZXing.Result ZXing.BarcodeReaderGeneric::Decode(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
extern void BarcodeReaderGeneric_Decode_m9776ADD1C954A9BD09218116E2DAE2B911D7B152 (void);
// 0x0000008A ZXing.Result[] ZXing.BarcodeReaderGeneric::DecodeMultiple(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
extern void BarcodeReaderGeneric_DecodeMultiple_m6554BBB3EBC00EED1ECAEC3ACF0F87744ACEE26C (void);
// 0x0000008B System.Void ZXing.BarcodeReaderGeneric::.cctor()
extern void BarcodeReaderGeneric__cctor_mB364ED63233DEA3B4C7D244D763D0442787B4987 (void);
// 0x0000008C System.Void ZXing.BarcodeReaderGeneric::<get_Options>b__8_0(System.Object,System.EventArgs)
extern void BarcodeReaderGeneric_U3Cget_OptionsU3Eb__8_0_m421EC6A145647F46D16CD02C2C06B0A9DBFB6059 (void);
// 0x0000008D System.Void ZXing.BarcodeReaderGeneric::<set_Options>b__9_0(System.Object,System.EventArgs)
extern void BarcodeReaderGeneric_U3Cset_OptionsU3Eb__9_0_m8F0B1A58EB5E97529342B0E2F575C5D69CF104CB (void);
// 0x0000008E System.Void ZXing.BarcodeReaderGeneric/<>c::.cctor()
extern void U3CU3Ec__cctor_m92C164218948DD06E8ABFE45468D4DA62C37075C (void);
// 0x0000008F System.Void ZXing.BarcodeReaderGeneric/<>c::.ctor()
extern void U3CU3Ec__ctor_mD3C7F7A78B1B43765D003098FA36FCC144986E7D (void);
// 0x00000090 ZXing.Binarizer ZXing.BarcodeReaderGeneric/<>c::<.cctor>b__40_0(ZXing.LuminanceSource)
extern void U3CU3Ec_U3C_cctorU3Eb__40_0_m61C0FD1FB8759C43E9D3B6F06F2E9E74F94D1AA1 (void);
// 0x00000091 ZXing.LuminanceSource ZXing.BarcodeReaderGeneric/<>c::<.cctor>b__40_1(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
extern void U3CU3Ec_U3C_cctorU3Eb__40_1_m8C6C73B7D34623F2C975EB64EEA2335FFD6EBCE3 (void);
// 0x00000092 System.Void ZXing.BarcodeWriter::.ctor()
extern void BarcodeWriter__ctor_m7E7FD1037C1CABBAF1908C061EE2721A76873721 (void);
// 0x00000093 System.Void ZXing.BarcodeWriterPixelData::.ctor()
extern void BarcodeWriterPixelData__ctor_mF897BD7CBB8C08B4A6BADB0F9956E753B4A31C48 (void);
// 0x00000094 System.Void ZXing.BarcodeWriterSvg::.ctor()
extern void BarcodeWriterSvg__ctor_m733D1E0185F0C03964F17BA577B7AD742AB4FD17 (void);
// 0x00000095 ZXing.Rendering.IBarcodeRenderer`1<TOutput> ZXing.BarcodeWriter`1::get_Renderer()
// 0x00000096 System.Void ZXing.BarcodeWriter`1::set_Renderer(ZXing.Rendering.IBarcodeRenderer`1<TOutput>)
// 0x00000097 TOutput ZXing.BarcodeWriter`1::Write(System.String)
// 0x00000098 TOutput ZXing.BarcodeWriter`1::Write(ZXing.Common.BitMatrix)
// 0x00000099 System.Void ZXing.BarcodeWriter`1::.ctor()
// 0x0000009A ZXing.BarcodeFormat ZXing.BarcodeWriterGeneric::get_Format()
extern void BarcodeWriterGeneric_get_Format_mF32FF0494D7F5F5030CEF380D88248ED3253BDF4 (void);
// 0x0000009B System.Void ZXing.BarcodeWriterGeneric::set_Format(ZXing.BarcodeFormat)
extern void BarcodeWriterGeneric_set_Format_m69EE4AE9F27C4512C0191D782E1E8061076B16CF (void);
// 0x0000009C ZXing.Common.EncodingOptions ZXing.BarcodeWriterGeneric::get_Options()
extern void BarcodeWriterGeneric_get_Options_m715D94C872B467A3E9F758B235694228A9D9887D (void);
// 0x0000009D System.Void ZXing.BarcodeWriterGeneric::set_Options(ZXing.Common.EncodingOptions)
extern void BarcodeWriterGeneric_set_Options_mAC402EE1F1CBDF8253E0170BED87AEDE7726F811 (void);
// 0x0000009E ZXing.Writer ZXing.BarcodeWriterGeneric::get_Encoder()
extern void BarcodeWriterGeneric_get_Encoder_m9C226073013475A81FC45D47A1769800BA2D1FB1 (void);
// 0x0000009F System.Void ZXing.BarcodeWriterGeneric::set_Encoder(ZXing.Writer)
extern void BarcodeWriterGeneric_set_Encoder_mDAD625A99A1DDCC6D8528D398A2663B28117CE39 (void);
// 0x000000A0 System.Void ZXing.BarcodeWriterGeneric::.ctor()
extern void BarcodeWriterGeneric__ctor_m686D39585FD5CADE9A80E24B8AB4160ECCA0A755 (void);
// 0x000000A1 System.Void ZXing.BarcodeWriterGeneric::.ctor(ZXing.Writer)
extern void BarcodeWriterGeneric__ctor_m489D4D870CBB5F2E8E61C22C903F1BBD5E6DAA19 (void);
// 0x000000A2 ZXing.Common.BitMatrix ZXing.BarcodeWriterGeneric::Encode(System.String)
extern void BarcodeWriterGeneric_Encode_m57EB69FFF0BD8EE06699C7098546E28B5AF107C4 (void);
// 0x000000A3 System.Void ZXing.BaseLuminanceSource::.ctor(System.Int32,System.Int32)
extern void BaseLuminanceSource__ctor_m4948B3C2224D1EF73B788862AD932C11C70E3B5F (void);
// 0x000000A4 System.Void ZXing.BaseLuminanceSource::.ctor(System.Byte[],System.Int32,System.Int32)
extern void BaseLuminanceSource__ctor_m940CF5BFD91AA908F9449071E948DCD6FEA3436C (void);
// 0x000000A5 System.Byte[] ZXing.BaseLuminanceSource::getRow(System.Int32,System.Byte[])
extern void BaseLuminanceSource_getRow_mF57A4D1FF3101438BC125979DAA5DB45A57C7AA7 (void);
// 0x000000A6 System.Byte[] ZXing.BaseLuminanceSource::get_Matrix()
extern void BaseLuminanceSource_get_Matrix_m0D2AD194D5474301A772C816122F029000357D81 (void);
// 0x000000A7 ZXing.LuminanceSource ZXing.BaseLuminanceSource::rotateCounterClockwise()
extern void BaseLuminanceSource_rotateCounterClockwise_m1FB02A981589690E07EF47C4ED3122735353373D (void);
// 0x000000A8 ZXing.LuminanceSource ZXing.BaseLuminanceSource::rotateCounterClockwise45()
extern void BaseLuminanceSource_rotateCounterClockwise45_mB9F94F58D5BA0569A1F693EEFC46641C3458F988 (void);
// 0x000000A9 System.Boolean ZXing.BaseLuminanceSource::get_RotateSupported()
extern void BaseLuminanceSource_get_RotateSupported_mBB4BFDBFDD974DA13AA2628093BDEE6889CB68F3 (void);
// 0x000000AA ZXing.LuminanceSource ZXing.BaseLuminanceSource::crop(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BaseLuminanceSource_crop_m2D9C767F95A74FA98B4A2DC07CC556BB270E79AB (void);
// 0x000000AB System.Boolean ZXing.BaseLuminanceSource::get_CropSupported()
extern void BaseLuminanceSource_get_CropSupported_mB4D710560FE33F82D427A1C48D32EF8DCA8664BE (void);
// 0x000000AC System.Boolean ZXing.BaseLuminanceSource::get_InversionSupported()
extern void BaseLuminanceSource_get_InversionSupported_m6CE99DEA2CAB24D95A38F865DD78DCDE5AFA7E1D (void);
// 0x000000AD ZXing.LuminanceSource ZXing.BaseLuminanceSource::invert()
extern void BaseLuminanceSource_invert_m5FEA3F7FD45BFA682FF21D1704152033A373A6B6 (void);
// 0x000000AE ZXing.LuminanceSource ZXing.BaseLuminanceSource::CreateLuminanceSource(System.Byte[],System.Int32,System.Int32)
// 0x000000AF System.Void ZXing.Binarizer::.ctor(ZXing.LuminanceSource)
extern void Binarizer__ctor_m79D01156F2A73236AFFBA6F6198956CB3AFCC986 (void);
// 0x000000B0 ZXing.LuminanceSource ZXing.Binarizer::get_LuminanceSource()
extern void Binarizer_get_LuminanceSource_m675C074D5408ED1A04E5A251AF33DDF76C6F513B (void);
// 0x000000B1 ZXing.Common.BitArray ZXing.Binarizer::getBlackRow(System.Int32,ZXing.Common.BitArray)
// 0x000000B2 ZXing.Common.BitMatrix ZXing.Binarizer::get_BlackMatrix()
// 0x000000B3 ZXing.Binarizer ZXing.Binarizer::createBinarizer(ZXing.LuminanceSource)
// 0x000000B4 System.Int32 ZXing.Binarizer::get_Width()
extern void Binarizer_get_Width_mEAC5749410E1726F30AA13CE052CE5B47B4C6A3B (void);
// 0x000000B5 System.Int32 ZXing.Binarizer::get_Height()
extern void Binarizer_get_Height_m72ACB476B8E8899C4CD01C6CEF809AC859D3DE40 (void);
// 0x000000B6 System.Void ZXing.BinaryBitmap::.ctor(ZXing.Binarizer)
extern void BinaryBitmap__ctor_m24A978B33C375EEFE711F4178FCD1C0A2BBAEA24 (void);
// 0x000000B7 System.Void ZXing.BinaryBitmap::.ctor(ZXing.Common.BitMatrix)
extern void BinaryBitmap__ctor_mC5E9C48ECADC45B110A07A91763CD2F5E3147FE0 (void);
// 0x000000B8 System.Int32 ZXing.BinaryBitmap::get_Width()
extern void BinaryBitmap_get_Width_mFA0AF5931276B5C64B36CEBA33786621B4C1DA84 (void);
// 0x000000B9 System.Int32 ZXing.BinaryBitmap::get_Height()
extern void BinaryBitmap_get_Height_m6632FFEE5D997D628C1D469DDD366466BAE457F0 (void);
// 0x000000BA ZXing.Common.BitArray ZXing.BinaryBitmap::getBlackRow(System.Int32,ZXing.Common.BitArray)
extern void BinaryBitmap_getBlackRow_mD0EFD03C83D0606EFDB3B4D8F0A3177A5C905728 (void);
// 0x000000BB ZXing.Common.BitMatrix ZXing.BinaryBitmap::get_BlackMatrix()
extern void BinaryBitmap_get_BlackMatrix_mA911AEC810AA216B50A2E796883ACF47DCA60893 (void);
// 0x000000BC System.Boolean ZXing.BinaryBitmap::get_CropSupported()
extern void BinaryBitmap_get_CropSupported_mA67B3824321364DBBC7E5837EA4B4337AC84014B (void);
// 0x000000BD ZXing.BinaryBitmap ZXing.BinaryBitmap::crop(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BinaryBitmap_crop_m9F755850C6482C5F353FEF39A202CE951181DAEB (void);
// 0x000000BE System.Boolean ZXing.BinaryBitmap::get_RotateSupported()
extern void BinaryBitmap_get_RotateSupported_m3645A172D5F6CB687725AB91DCBFA83034BDF6E4 (void);
// 0x000000BF ZXing.BinaryBitmap ZXing.BinaryBitmap::rotateCounterClockwise()
extern void BinaryBitmap_rotateCounterClockwise_m35EA73A7E7DF3C1BD87763AEAAC3C05EA0E64D38 (void);
// 0x000000C0 ZXing.BinaryBitmap ZXing.BinaryBitmap::rotateCounterClockwise45()
extern void BinaryBitmap_rotateCounterClockwise45_mBA30EC468FAC62DD0C1D312854E4F58340A0FAFB (void);
// 0x000000C1 System.String ZXing.BinaryBitmap::ToString()
extern void BinaryBitmap_ToString_m5D282826FEFD8D4452C667497E9E816A7E5CFFDF (void);
// 0x000000C2 System.Void ZXing.Dimension::.ctor(System.Int32,System.Int32)
extern void Dimension__ctor_m1B90F4D393D0982951AC28A9B43D4986830C4F94 (void);
// 0x000000C3 System.Int32 ZXing.Dimension::get_Width()
extern void Dimension_get_Width_m2CDBF4CC5D74D2B0CF89FF8B38A39CC93721EB20 (void);
// 0x000000C4 System.Int32 ZXing.Dimension::get_Height()
extern void Dimension_get_Height_mD28B2DE83EE6E872951D1E4A3B5D4F528E143334 (void);
// 0x000000C5 System.Boolean ZXing.Dimension::Equals(System.Object)
extern void Dimension_Equals_m28F79A32D4E174544AB9A8D5A7F56EC2A95CEAC7 (void);
// 0x000000C6 System.Int32 ZXing.Dimension::GetHashCode()
extern void Dimension_GetHashCode_m995F6C69F4036D066E207BA9FA859378E6FA3A71 (void);
// 0x000000C7 System.String ZXing.Dimension::ToString()
extern void Dimension_ToString_mAE6154A4BA4DCA8BA8DE152E3C8A1BEA002891B5 (void);
// 0x000000C8 System.Void ZXing.FormatException::.ctor()
extern void FormatException__ctor_m2299A3FD33ADF78B372EF9C915E48FF07EF46941 (void);
// 0x000000C9 System.Void ZXing.FormatException::.ctor(System.String)
extern void FormatException__ctor_m12BB44AE2A5E1A41727C33A562FADC9D9ABF0967 (void);
// 0x000000CA System.Void ZXing.FormatException::.ctor(System.Exception)
extern void FormatException__ctor_m70C9CA46F36F4CF8A04A0DE3B2E9720B1EF68249 (void);
// 0x000000CB System.Void ZXing.FormatException::.ctor(System.String,System.Exception)
extern void FormatException__ctor_m3CD02C31D612567306005AA1C017843CE94CCACA (void);
// 0x000000CC ZXing.Result ZXing.IBarcodeReader::Decode(UnityEngine.Color32[],System.Int32,System.Int32)
// 0x000000CD System.Void ZXing.IBarcodeReader::add_ResultPointFound(System.Action`1<ZXing.ResultPoint>)
// 0x000000CE System.Void ZXing.IBarcodeReader::remove_ResultPointFound(System.Action`1<ZXing.ResultPoint>)
// 0x000000CF System.Void ZXing.IBarcodeReader::add_ResultFound(System.Action`1<ZXing.Result>)
// 0x000000D0 System.Void ZXing.IBarcodeReader::remove_ResultFound(System.Action`1<ZXing.Result>)
// 0x000000D1 ZXing.Common.DecodingOptions ZXing.IBarcodeReader::get_Options()
// 0x000000D2 System.Void ZXing.IBarcodeReader::set_Options(ZXing.Common.DecodingOptions)
// 0x000000D3 ZXing.Result ZXing.IBarcodeReader::Decode(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
// 0x000000D4 ZXing.Result ZXing.IBarcodeReader::Decode(ZXing.LuminanceSource)
// 0x000000D5 ZXing.Result[] ZXing.IBarcodeReader::DecodeMultiple(UnityEngine.Color32[],System.Int32,System.Int32)
// 0x000000D6 ZXing.Result[] ZXing.IBarcodeReader::DecodeMultiple(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
// 0x000000D7 ZXing.Result[] ZXing.IBarcodeReader::DecodeMultiple(ZXing.LuminanceSource)
// 0x000000D8 ZXing.Result ZXing.IBarcodeReader`1::Decode(T)
// 0x000000D9 ZXing.Result[] ZXing.IBarcodeReader`1::DecodeMultiple(T)
// 0x000000DA System.Void ZXing.IBarcodeReaderGeneric::add_ResultPointFound(System.Action`1<ZXing.ResultPoint>)
// 0x000000DB System.Void ZXing.IBarcodeReaderGeneric::remove_ResultPointFound(System.Action`1<ZXing.ResultPoint>)
// 0x000000DC System.Void ZXing.IBarcodeReaderGeneric::add_ResultFound(System.Action`1<ZXing.Result>)
// 0x000000DD System.Void ZXing.IBarcodeReaderGeneric::remove_ResultFound(System.Action`1<ZXing.Result>)
// 0x000000DE ZXing.Common.DecodingOptions ZXing.IBarcodeReaderGeneric::get_Options()
// 0x000000DF System.Void ZXing.IBarcodeReaderGeneric::set_Options(ZXing.Common.DecodingOptions)
// 0x000000E0 ZXing.Result ZXing.IBarcodeReaderGeneric::Decode(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
// 0x000000E1 ZXing.Result ZXing.IBarcodeReaderGeneric::Decode(ZXing.LuminanceSource)
// 0x000000E2 ZXing.Result[] ZXing.IBarcodeReaderGeneric::DecodeMultiple(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
// 0x000000E3 ZXing.Result[] ZXing.IBarcodeReaderGeneric::DecodeMultiple(ZXing.LuminanceSource)
// 0x000000E4 UnityEngine.Color32[] ZXing.IBarcodeWriter::Write(System.String)
// 0x000000E5 UnityEngine.Color32[] ZXing.IBarcodeWriter::Write(ZXing.Common.BitMatrix)
// 0x000000E6 ZXing.BarcodeFormat ZXing.IBarcodeWriter::get_Format()
// 0x000000E7 System.Void ZXing.IBarcodeWriter::set_Format(ZXing.BarcodeFormat)
// 0x000000E8 ZXing.Common.EncodingOptions ZXing.IBarcodeWriter::get_Options()
// 0x000000E9 System.Void ZXing.IBarcodeWriter::set_Options(ZXing.Common.EncodingOptions)
// 0x000000EA ZXing.Writer ZXing.IBarcodeWriter::get_Encoder()
// 0x000000EB System.Void ZXing.IBarcodeWriter::set_Encoder(ZXing.Writer)
// 0x000000EC ZXing.Common.BitMatrix ZXing.IBarcodeWriter::Encode(System.String)
// 0x000000ED ZXing.Rendering.PixelData ZXing.IBarcodeWriterPixelData::Write(System.String)
// 0x000000EE ZXing.Rendering.PixelData ZXing.IBarcodeWriterPixelData::Write(ZXing.Common.BitMatrix)
// 0x000000EF ZXing.Rendering.SvgRenderer/SvgImage ZXing.IBarcodeWriterSvg::Write(System.String)
// 0x000000F0 ZXing.Rendering.SvgRenderer/SvgImage ZXing.IBarcodeWriterSvg::Write(ZXing.Common.BitMatrix)
// 0x000000F1 TOutput ZXing.IBarcodeWriter`1::Write(System.String)
// 0x000000F2 TOutput ZXing.IBarcodeWriter`1::Write(ZXing.Common.BitMatrix)
// 0x000000F3 ZXing.BarcodeFormat ZXing.IBarcodeWriterGeneric::get_Format()
// 0x000000F4 System.Void ZXing.IBarcodeWriterGeneric::set_Format(ZXing.BarcodeFormat)
// 0x000000F5 ZXing.Common.EncodingOptions ZXing.IBarcodeWriterGeneric::get_Options()
// 0x000000F6 System.Void ZXing.IBarcodeWriterGeneric::set_Options(ZXing.Common.EncodingOptions)
// 0x000000F7 ZXing.Writer ZXing.IBarcodeWriterGeneric::get_Encoder()
// 0x000000F8 System.Void ZXing.IBarcodeWriterGeneric::set_Encoder(ZXing.Writer)
// 0x000000F9 ZXing.Common.BitMatrix ZXing.IBarcodeWriterGeneric::Encode(System.String)
// 0x000000FA System.Void ZXing.InvertedLuminanceSource::.ctor(ZXing.LuminanceSource)
extern void InvertedLuminanceSource__ctor_mDEDE56B3C11E9C2EA492B554865252E16021D27D (void);
// 0x000000FB System.Byte[] ZXing.InvertedLuminanceSource::getRow(System.Int32,System.Byte[])
extern void InvertedLuminanceSource_getRow_mA26BFD34ED8E7F9DE79B768217F5F140852BEBCF (void);
// 0x000000FC System.Byte[] ZXing.InvertedLuminanceSource::get_Matrix()
extern void InvertedLuminanceSource_get_Matrix_mE7DADC33FA3A789BF10F063E243AFEDFE2D3A979 (void);
// 0x000000FD System.Boolean ZXing.InvertedLuminanceSource::get_CropSupported()
extern void InvertedLuminanceSource_get_CropSupported_m6F3A6DD3DA05673F8DB75C67BA9B173B1DBAFB94 (void);
// 0x000000FE ZXing.LuminanceSource ZXing.InvertedLuminanceSource::crop(System.Int32,System.Int32,System.Int32,System.Int32)
extern void InvertedLuminanceSource_crop_mD64707A2BB43E2F475566E3093FE31B653BB1876 (void);
// 0x000000FF System.Boolean ZXing.InvertedLuminanceSource::get_RotateSupported()
extern void InvertedLuminanceSource_get_RotateSupported_m6D26479C4527FE26D8996A391BE844BDA5C9BFA6 (void);
// 0x00000100 ZXing.LuminanceSource ZXing.InvertedLuminanceSource::invert()
extern void InvertedLuminanceSource_invert_m99EB3B276DECB2A12C991A35DCCF07243D29E6D5 (void);
// 0x00000101 ZXing.LuminanceSource ZXing.InvertedLuminanceSource::rotateCounterClockwise()
extern void InvertedLuminanceSource_rotateCounterClockwise_m936CFE848798F29038902D49978547E21B6509EC (void);
// 0x00000102 ZXing.LuminanceSource ZXing.InvertedLuminanceSource::rotateCounterClockwise45()
extern void InvertedLuminanceSource_rotateCounterClockwise45_m6AA8D1089AB1591261D3F019063170982DFDD664 (void);
// 0x00000103 System.Void ZXing.LuminanceSource::.ctor(System.Int32,System.Int32)
extern void LuminanceSource__ctor_mC5B34877E69DB2093EA96E77FAB2FF17E8BA5259 (void);
// 0x00000104 System.Byte[] ZXing.LuminanceSource::getRow(System.Int32,System.Byte[])
// 0x00000105 System.Byte[] ZXing.LuminanceSource::get_Matrix()
// 0x00000106 System.Int32 ZXing.LuminanceSource::get_Width()
extern void LuminanceSource_get_Width_m84327B26782881AAEC0E6EE26BB6CBC021345A5F (void);
// 0x00000107 System.Void ZXing.LuminanceSource::set_Width(System.Int32)
extern void LuminanceSource_set_Width_m062CCF31850F16C3863C756F1C2AF967ADA5336A (void);
// 0x00000108 System.Int32 ZXing.LuminanceSource::get_Height()
extern void LuminanceSource_get_Height_m33DCC17920FE073B5C0B6DD08C6BEBE457032919 (void);
// 0x00000109 System.Void ZXing.LuminanceSource::set_Height(System.Int32)
extern void LuminanceSource_set_Height_m6B0F847CC5930324115DBB4EAD0909F5B0A9B260 (void);
// 0x0000010A System.Boolean ZXing.LuminanceSource::get_CropSupported()
extern void LuminanceSource_get_CropSupported_mF8590DDAAC672FAD5C5199C86D272A3D01ADBED7 (void);
// 0x0000010B ZXing.LuminanceSource ZXing.LuminanceSource::crop(System.Int32,System.Int32,System.Int32,System.Int32)
extern void LuminanceSource_crop_m7A1A029A200FD45E1276F358D314192DE08934D4 (void);
// 0x0000010C System.Boolean ZXing.LuminanceSource::get_RotateSupported()
extern void LuminanceSource_get_RotateSupported_mA5160C3B5C063204BB3C7A3D8B44379BDE6248D6 (void);
// 0x0000010D ZXing.LuminanceSource ZXing.LuminanceSource::rotateCounterClockwise()
extern void LuminanceSource_rotateCounterClockwise_m31059FB64855B6BCC469B9EABE981469A7CA302D (void);
// 0x0000010E ZXing.LuminanceSource ZXing.LuminanceSource::rotateCounterClockwise45()
extern void LuminanceSource_rotateCounterClockwise45_m760493DC91121972B324BCAA83413EC102E38E7B (void);
// 0x0000010F System.Boolean ZXing.LuminanceSource::get_InversionSupported()
extern void LuminanceSource_get_InversionSupported_mA6F920BBEC71351252B95C20A5464B9F7702C486 (void);
// 0x00000110 ZXing.LuminanceSource ZXing.LuminanceSource::invert()
extern void LuminanceSource_invert_m37B2CC69CE06BA09BC6851C47CF21060BB0DC955 (void);
// 0x00000111 System.String ZXing.LuminanceSource::ToString()
extern void LuminanceSource_ToString_mF93D29F0CFF0A30F8D58EEA2D6DEFB0875D5B8B7 (void);
// 0x00000112 ZXing.Result ZXing.MultiFormatReader::decode(ZXing.BinaryBitmap)
extern void MultiFormatReader_decode_m9B66B16BBF5D169EA2C9BE66B034A83B246CE4E7 (void);
// 0x00000113 ZXing.Result ZXing.MultiFormatReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatReader_decode_m990A108ABCA2F463E7F37587B4957D64ED6F2DEE (void);
// 0x00000114 ZXing.Result ZXing.MultiFormatReader::decodeWithState(ZXing.BinaryBitmap)
extern void MultiFormatReader_decodeWithState_m20C2C3CD5BAF03460B6A42C946CACFFD0A0B2110 (void);
// 0x00000115 System.Void ZXing.MultiFormatReader::set_Hints(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatReader_set_Hints_m281E68BE04E40DB50CF511F2CB10B405D3F5E133 (void);
// 0x00000116 System.Void ZXing.MultiFormatReader::reset()
extern void MultiFormatReader_reset_mF573C78C8A05F98F3C6337DC4997E8EA7C807FD2 (void);
// 0x00000117 ZXing.Result ZXing.MultiFormatReader::decodeInternal(ZXing.BinaryBitmap)
extern void MultiFormatReader_decodeInternal_mF6D3DF95DAC8D9166579017CF6EE3677C4B0B66C (void);
// 0x00000118 System.Void ZXing.MultiFormatReader::.ctor()
extern void MultiFormatReader__ctor_m22C7456990B33553DE6937E091E153945A9524B5 (void);
// 0x00000119 System.Void ZXing.MultiFormatWriter::.cctor()
extern void MultiFormatWriter__cctor_m0697C5683DB8545839AD96C1F003DA4D18444D93 (void);
// 0x0000011A System.Collections.Generic.ICollection`1<ZXing.BarcodeFormat> ZXing.MultiFormatWriter::get_SupportedWriters()
extern void MultiFormatWriter_get_SupportedWriters_m9672B8364CE64634CB72014105F06305FA415A80 (void);
// 0x0000011B ZXing.Common.BitMatrix ZXing.MultiFormatWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32)
extern void MultiFormatWriter_encode_m8C3B2D89242BC893C60C930D3E886423D257014C (void);
// 0x0000011C ZXing.Common.BitMatrix ZXing.MultiFormatWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void MultiFormatWriter_encode_m9A0374384417F935C41E5373CA72490AF8936B34 (void);
// 0x0000011D System.Void ZXing.MultiFormatWriter::.ctor()
extern void MultiFormatWriter__ctor_m9E53BAE0AA700CD2EB27D857D834B9C9C612EBA6 (void);
// 0x0000011E System.Void ZXing.MultiFormatWriter/<>c::.cctor()
extern void U3CU3Ec__cctor_m515C3844F2D295BE3A4D6DD14A364788A6C27C2B (void);
// 0x0000011F System.Void ZXing.MultiFormatWriter/<>c::.ctor()
extern void U3CU3Ec__ctor_m1113E1EAD43E52CE329EF60AF3B315DD0080D956 (void);
// 0x00000120 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_0()
extern void U3CU3Ec_U3C_cctorU3Eb__1_0_m5F62DA180AA9D5C7140F63F8EF64CCE5030FC461 (void);
// 0x00000121 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_1()
extern void U3CU3Ec_U3C_cctorU3Eb__1_1_mB7123D61F4F943F7BC6BFE6D9BA74D33E786AC3C (void);
// 0x00000122 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_2()
extern void U3CU3Ec_U3C_cctorU3Eb__1_2_mBBA1CEC393E4D452E1B29BD35D563765F23499A9 (void);
// 0x00000123 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_3()
extern void U3CU3Ec_U3C_cctorU3Eb__1_3_m6FAED573CA72608F6BB66FFC5F6F9A8170D6877A (void);
// 0x00000124 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_4()
extern void U3CU3Ec_U3C_cctorU3Eb__1_4_mA4DBE2D702DDDC34B365418E4A0B5AD160C6EAEB (void);
// 0x00000125 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_5()
extern void U3CU3Ec_U3C_cctorU3Eb__1_5_m06C377090CB76FBCFF6D88B9A768C4A6343C88E8 (void);
// 0x00000126 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_6()
extern void U3CU3Ec_U3C_cctorU3Eb__1_6_mFE356E54E936FCD2F5ED92385FA8E5180BD16BBE (void);
// 0x00000127 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_7()
extern void U3CU3Ec_U3C_cctorU3Eb__1_7_m3D48593CB730558EA8D89BE4A62AF56DFE75BEBC (void);
// 0x00000128 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_8()
extern void U3CU3Ec_U3C_cctorU3Eb__1_8_mBA430395DF4E02A5255F5819BB58A1DF31918039 (void);
// 0x00000129 ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_9()
extern void U3CU3Ec_U3C_cctorU3Eb__1_9_mA0B728176F45510D7610771AFAB44696523F726D (void);
// 0x0000012A ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_10()
extern void U3CU3Ec_U3C_cctorU3Eb__1_10_m82B89598C6D1236CF09943726EDE770F57508EAC (void);
// 0x0000012B ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_11()
extern void U3CU3Ec_U3C_cctorU3Eb__1_11_m74A8CA6CC5DF3C8CE0249607F5022E9DE6537CC3 (void);
// 0x0000012C ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_12()
extern void U3CU3Ec_U3C_cctorU3Eb__1_12_mD1FD14DD31D34B4FC7D32B6A6F9934D64EF060F6 (void);
// 0x0000012D ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_13()
extern void U3CU3Ec_U3C_cctorU3Eb__1_13_mF6431123BA8681AA8F02D0345BC73B5244611C24 (void);
// 0x0000012E ZXing.Writer ZXing.MultiFormatWriter/<>c::<.cctor>b__1_14()
extern void U3CU3Ec_U3C_cctorU3Eb__1_14_m4798768D38463B839ED1F294E8A6AD41D6311CB2 (void);
// 0x0000012F System.Void ZXing.PlanarYUVLuminanceSource::.ctor(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void PlanarYUVLuminanceSource__ctor_m370700CA786C78CD59237D28BC9C4C45DE97D768 (void);
// 0x00000130 System.Void ZXing.PlanarYUVLuminanceSource::.ctor(System.Byte[],System.Int32,System.Int32)
extern void PlanarYUVLuminanceSource__ctor_mB258DD3B0DB01184F19D395836B3D7D6B8257378 (void);
// 0x00000131 System.Byte[] ZXing.PlanarYUVLuminanceSource::getRow(System.Int32,System.Byte[])
extern void PlanarYUVLuminanceSource_getRow_mFCFC814FC46D7161699208F0B537AFE53731CBB6 (void);
// 0x00000132 System.Byte[] ZXing.PlanarYUVLuminanceSource::get_Matrix()
extern void PlanarYUVLuminanceSource_get_Matrix_m7F3557B5CA3E4254BCED3A8598D4B52472AB0062 (void);
// 0x00000133 System.Boolean ZXing.PlanarYUVLuminanceSource::get_CropSupported()
extern void PlanarYUVLuminanceSource_get_CropSupported_mBF9F3E3AB801BFE2BCA42E03A9EF754AB151A0C7 (void);
// 0x00000134 ZXing.LuminanceSource ZXing.PlanarYUVLuminanceSource::crop(System.Int32,System.Int32,System.Int32,System.Int32)
extern void PlanarYUVLuminanceSource_crop_mB661406A7B13ED56501696FA0FE0D33954C5E991 (void);
// 0x00000135 System.Int32[] ZXing.PlanarYUVLuminanceSource::renderThumbnail()
extern void PlanarYUVLuminanceSource_renderThumbnail_mAE867636A187F9F0E36E7D2D6C332CE328D8EBD6 (void);
// 0x00000136 System.Int32 ZXing.PlanarYUVLuminanceSource::get_ThumbnailWidth()
extern void PlanarYUVLuminanceSource_get_ThumbnailWidth_m48AD72588131430A5514BC8D0A84DCD2C8FF8A75 (void);
// 0x00000137 System.Int32 ZXing.PlanarYUVLuminanceSource::get_ThumbnailHeight()
extern void PlanarYUVLuminanceSource_get_ThumbnailHeight_m7C995F5DEFF9C40EE9794970AE86B879D8321C9E (void);
// 0x00000138 System.Void ZXing.PlanarYUVLuminanceSource::reverseHorizontal(System.Int32,System.Int32)
extern void PlanarYUVLuminanceSource_reverseHorizontal_mCD165AF8C02720DB0935FDC943D1EFF52FD75ADB (void);
// 0x00000139 ZXing.LuminanceSource ZXing.PlanarYUVLuminanceSource::CreateLuminanceSource(System.Byte[],System.Int32,System.Int32)
extern void PlanarYUVLuminanceSource_CreateLuminanceSource_mE2D061D9E85D8D00A0A534844714A85936617523 (void);
// 0x0000013A ZXing.Result ZXing.Reader::decode(ZXing.BinaryBitmap)
// 0x0000013B ZXing.Result ZXing.Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
// 0x0000013C System.Void ZXing.Reader::reset()
// 0x0000013D System.Void ZXing.ReaderException::.ctor()
extern void ReaderException__ctor_mE42867457E0429A7A295C947AF882E4F2EEABEA4 (void);
// 0x0000013E System.Void ZXing.ReaderException::.ctor(System.String)
extern void ReaderException__ctor_mBBF97FA1D52C173F000C225F7A3519469E0CE56F (void);
// 0x0000013F System.Void ZXing.ReaderException::.ctor(System.Exception)
extern void ReaderException__ctor_m996848175E1DBF82EFC27D5ED8EE65669798DDF3 (void);
// 0x00000140 System.Void ZXing.ReaderException::.ctor(System.String,System.Exception)
extern void ReaderException__ctor_m7F3F950E443CF1B903CF9A325B8B9EB61F2C78AF (void);
// 0x00000141 System.String ZXing.Result::get_Text()
extern void Result_get_Text_mEAE1D660EEF3788AA89F312D7D7AD0E94BB70D66 (void);
// 0x00000142 System.Void ZXing.Result::set_Text(System.String)
extern void Result_set_Text_m57B698D4925803C2DE47487571BBECE9708A645F (void);
// 0x00000143 System.Byte[] ZXing.Result::get_RawBytes()
extern void Result_get_RawBytes_m3AA4BB3EB5D378E63BE07AA1C8ADAE508079CEE7 (void);
// 0x00000144 System.Void ZXing.Result::set_RawBytes(System.Byte[])
extern void Result_set_RawBytes_mAEC8114897C03331968B1445B0EA60B91947A41F (void);
// 0x00000145 ZXing.ResultPoint[] ZXing.Result::get_ResultPoints()
extern void Result_get_ResultPoints_mA678D102FD830C51E332C355DA5004D4072DE8C4 (void);
// 0x00000146 System.Void ZXing.Result::set_ResultPoints(ZXing.ResultPoint[])
extern void Result_set_ResultPoints_mBB21E4578841276156438C4765BED52DCB0ED648 (void);
// 0x00000147 ZXing.BarcodeFormat ZXing.Result::get_BarcodeFormat()
extern void Result_get_BarcodeFormat_m69C929547E7704672F93EA848162EA432BB4CD08 (void);
// 0x00000148 System.Void ZXing.Result::set_BarcodeFormat(ZXing.BarcodeFormat)
extern void Result_set_BarcodeFormat_mCC1690B0810F0233A339DE2FC05BC3119B4F0223 (void);
// 0x00000149 System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.Result::get_ResultMetadata()
extern void Result_get_ResultMetadata_m0D09D838B9025B8BA001433A6035EB9D5D717A4F (void);
// 0x0000014A System.Void ZXing.Result::set_ResultMetadata(System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>)
extern void Result_set_ResultMetadata_m14E1B6A9768815184132D2EA521BDD8670BD0333 (void);
// 0x0000014B System.Int64 ZXing.Result::get_Timestamp()
extern void Result_get_Timestamp_m796F9523169BBE842086F9D67119B717AAAA6F48 (void);
// 0x0000014C System.Void ZXing.Result::set_Timestamp(System.Int64)
extern void Result_set_Timestamp_m998601E38F9A53D803D0247099A66B7ADDFCDE1E (void);
// 0x0000014D System.Int32 ZXing.Result::get_NumBits()
extern void Result_get_NumBits_mFB09449F2BF146ADF0EED87DE0339289AEBAEF85 (void);
// 0x0000014E System.Void ZXing.Result::set_NumBits(System.Int32)
extern void Result_set_NumBits_mCA75BDE5F42932D0FCF842C1BD2C20F2B6BB2CFE (void);
// 0x0000014F System.Void ZXing.Result::.ctor(System.String,System.Byte[],ZXing.ResultPoint[],ZXing.BarcodeFormat)
extern void Result__ctor_m1E14F77F240EC3A52D1AEC392FB262EE148AC711 (void);
// 0x00000150 System.Void ZXing.Result::.ctor(System.String,System.Byte[],System.Int32,ZXing.ResultPoint[],ZXing.BarcodeFormat)
extern void Result__ctor_m7E17E9F04AA01FD00884E74FB0E57C42B5D24025 (void);
// 0x00000151 System.Void ZXing.Result::.ctor(System.String,System.Byte[],ZXing.ResultPoint[],ZXing.BarcodeFormat,System.Int64)
extern void Result__ctor_mC9223509BE8427A7DC2F4AFA391470FEBF8F36F2 (void);
// 0x00000152 System.Void ZXing.Result::.ctor(System.String,System.Byte[],System.Int32,ZXing.ResultPoint[],ZXing.BarcodeFormat,System.Int64)
extern void Result__ctor_mF7A88874108B5DB5DBF378CCB139D1AB4ED828B7 (void);
// 0x00000153 System.Void ZXing.Result::putMetadata(ZXing.ResultMetadataType,System.Object)
extern void Result_putMetadata_mB20B623DCEEC41116D938EC71C16DECF4B599B0D (void);
// 0x00000154 System.Void ZXing.Result::putAllMetadata(System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>)
extern void Result_putAllMetadata_m02E5A99E31170EBEE1319DD5536577CD81E0A582 (void);
// 0x00000155 System.Void ZXing.Result::addResultPoints(ZXing.ResultPoint[])
extern void Result_addResultPoints_mA8A8E3DC1BDA2F05C4B5DB5A13B51B9AA01CF132 (void);
// 0x00000156 System.String ZXing.Result::ToString()
extern void Result_ToString_mCB80B9FE72A5F1F1299238BDAD9F79B4D5EF586D (void);
// 0x00000157 System.Void ZXing.ResultPoint::.ctor()
extern void ResultPoint__ctor_m79F46362C8325C7AE4D7F123CD3A0A4CAF79330D (void);
// 0x00000158 System.Void ZXing.ResultPoint::.ctor(System.Single,System.Single)
extern void ResultPoint__ctor_mA455E826856C2C0705F54F02FAF007E9459DAE79 (void);
// 0x00000159 System.Single ZXing.ResultPoint::get_X()
extern void ResultPoint_get_X_m11301DCBBA25E7B6BC968B991988AE9A1C33FAD4 (void);
// 0x0000015A System.Single ZXing.ResultPoint::get_Y()
extern void ResultPoint_get_Y_m1628B0C2874884F4981174FC694348C1C1785243 (void);
// 0x0000015B System.Boolean ZXing.ResultPoint::Equals(System.Object)
extern void ResultPoint_Equals_m6D30D902F0D3720FF78334908DDA6B0598FC5E5C (void);
// 0x0000015C System.Int32 ZXing.ResultPoint::GetHashCode()
extern void ResultPoint_GetHashCode_m6C964DF85CE64D06EF4D74E4728CAF117AA61F52 (void);
// 0x0000015D System.String ZXing.ResultPoint::ToString()
extern void ResultPoint_ToString_m047E6F1C7CAD358AD67F4D429073B14038FFFFD7 (void);
// 0x0000015E System.Void ZXing.ResultPoint::orderBestPatterns(ZXing.ResultPoint[])
extern void ResultPoint_orderBestPatterns_m4FD0B61781E4A2FD3FA7B832F34641EA85D08373 (void);
// 0x0000015F System.Single ZXing.ResultPoint::distance(ZXing.ResultPoint,ZXing.ResultPoint)
extern void ResultPoint_distance_mACAD8E6DD21BD6059EC7D836B68B8273DE9BE22C (void);
// 0x00000160 System.Single ZXing.ResultPoint::crossProductZ(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void ResultPoint_crossProductZ_m1A1C09FE1521F24A29DE80B4EF4257243CF33840 (void);
// 0x00000161 System.Void ZXing.ResultPointCallback::.ctor(System.Object,System.IntPtr)
extern void ResultPointCallback__ctor_mAE7B4A4417E2D1CB6D91BB05B5ECC863017BB1C5 (void);
// 0x00000162 System.Void ZXing.ResultPointCallback::Invoke(ZXing.ResultPoint)
extern void ResultPointCallback_Invoke_m7DAE0E818EB77473A75096BD148F63A218CA04A3 (void);
// 0x00000163 System.IAsyncResult ZXing.ResultPointCallback::BeginInvoke(ZXing.ResultPoint,System.AsyncCallback,System.Object)
extern void ResultPointCallback_BeginInvoke_m25BF2A89E01347344DC9CFFE8652E182B23EA2B3 (void);
// 0x00000164 System.Void ZXing.ResultPointCallback::EndInvoke(System.IAsyncResult)
extern void ResultPointCallback_EndInvoke_m00BB9E46B22BE030A86EE45703C17759705C2E43 (void);
// 0x00000165 System.Void ZXing.RGBLuminanceSource::.ctor(System.Int32,System.Int32)
extern void RGBLuminanceSource__ctor_m51CA2D10F091552757973B45E36F13590BDF0BC9 (void);
// 0x00000166 System.Void ZXing.RGBLuminanceSource::.ctor(System.Byte[],System.Int32,System.Int32)
extern void RGBLuminanceSource__ctor_m92099E26A903D8C8F574A1E3E24F8D44BE316F8A (void);
// 0x00000167 System.Void ZXing.RGBLuminanceSource::.ctor(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void RGBLuminanceSource__ctor_m3BEE113C47BAB2C5F432D656EE8235EA65A0C8C1 (void);
// 0x00000168 System.Void ZXing.RGBLuminanceSource::.ctor(System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat)
extern void RGBLuminanceSource__ctor_mC49758F8C48164277A9F2C3E2F5633F0FC18B556 (void);
// 0x00000169 ZXing.LuminanceSource ZXing.RGBLuminanceSource::CreateLuminanceSource(System.Byte[],System.Int32,System.Int32)
extern void RGBLuminanceSource_CreateLuminanceSource_m5B1EFEBDB768B542A80269B11DCEE69412CB2132 (void);
// 0x0000016A ZXing.RGBLuminanceSource/BitmapFormat ZXing.RGBLuminanceSource::DetermineBitmapFormat(System.Byte[],System.Int32,System.Int32)
extern void RGBLuminanceSource_DetermineBitmapFormat_m2F09D25A7DE54E59227B813A1F9B46D49B2469AF (void);
// 0x0000016B System.Void ZXing.RGBLuminanceSource::CalculateLuminance(System.Byte[],ZXing.RGBLuminanceSource/BitmapFormat)
extern void RGBLuminanceSource_CalculateLuminance_mC58CD7E48A8275962F37D3B6F0597A150A26891A (void);
// 0x0000016C System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGB565(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceRGB565_mE4787D83B47CBE19EABC7789C3BF9E9BBC03C38A (void);
// 0x0000016D System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGB24(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceRGB24_mDAC50668CB229AACD5DA5336CF5839FDAD79C019 (void);
// 0x0000016E System.Void ZXing.RGBLuminanceSource::CalculateLuminanceBGR24(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceBGR24_mBB4424D4580B9E0A087A57AF7A06167D7C7CEB11 (void);
// 0x0000016F System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGB32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceRGB32_m5FDC80197850993728E43D106CA497A4A6151C06 (void);
// 0x00000170 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceBGR32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceBGR32_m545AC2ABC4ADFE5BFEA926F5F33A9D0C302BF82D (void);
// 0x00000171 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceBGRA32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceBGRA32_m7C1E312BFF6ABB3391B2B5B4DF5DC5E64FFC5A48 (void);
// 0x00000172 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceRGBA32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceRGBA32_mE7ACE0463E9204B4A2BF96BBA8C8FFC8550982A6 (void);
// 0x00000173 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceARGB32(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceARGB32_m8DC3EC9E02E41506476F0592691E565F69B3D2D5 (void);
// 0x00000174 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceUYVY(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceUYVY_m4D7971B512BC85BF862A56F52CF7A24839F97444 (void);
// 0x00000175 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceYUYV(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceYUYV_m2D1C404C1AE1E6223B148BF52C52201BD1A2B35E (void);
// 0x00000176 System.Void ZXing.RGBLuminanceSource::CalculateLuminanceGray16(System.Byte[])
extern void RGBLuminanceSource_CalculateLuminanceGray16_m17BF7897E7F9F2654C696E2E4449F813BFB3FBD4 (void);
// 0x00000177 System.Void ZXing.SupportClass::GetCharsFromString(System.String,System.Int32,System.Int32,System.Char[],System.Int32)
extern void SupportClass_GetCharsFromString_m426BDB3F99FE11711DB087125D43E86566FFCBC0 (void);
// 0x00000178 System.Void ZXing.SupportClass::SetCapacity(System.Collections.Generic.IList`1<T>,System.Int32)
// 0x00000179 System.String[] ZXing.SupportClass::toStringArray(System.Collections.Generic.ICollection`1<System.String>)
extern void SupportClass_toStringArray_m08D2FB8EEE0D5F1ED85245282AA33A07E3BCDDFA (void);
// 0x0000017A System.String ZXing.SupportClass::Join(System.String,System.Collections.Generic.IEnumerable`1<T>)
// 0x0000017B System.Void ZXing.SupportClass::Fill(T[],T)
// 0x0000017C System.Void ZXing.SupportClass::Fill(T[],System.Int32,System.Int32,T)
// 0x0000017D System.String ZXing.SupportClass::ToBinaryString(System.Int32)
extern void SupportClass_ToBinaryString_mCB5BBF13C75F4ACAEB0506A2E7D69DA42F9A20C8 (void);
// 0x0000017E System.Int32 ZXing.SupportClass::bitCount(System.Int32)
extern void SupportClass_bitCount_m21473D5D1DA67A40A5F24674791A233A63788E8A (void);
// 0x0000017F T ZXing.SupportClass::GetValue(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,ZXing.DecodeHintType,T)
// 0x00000180 System.Void ZXing.Color32LuminanceSource::.ctor(System.Int32,System.Int32)
extern void Color32LuminanceSource__ctor_m97737C90E0BE9EC855EB71BC8B5DBEBA04B01288 (void);
// 0x00000181 System.Void ZXing.Color32LuminanceSource::.ctor(UnityEngine.Color32[],System.Int32,System.Int32)
extern void Color32LuminanceSource__ctor_mEC35DFEB9A578281FE68158040DB4B31F7C40A24 (void);
// 0x00000182 System.Void ZXing.Color32LuminanceSource::SetPixels(UnityEngine.Color32[])
extern void Color32LuminanceSource_SetPixels_mA89C21BBC7D2815DEDCDAB0E600187B4D3184229 (void);
// 0x00000183 ZXing.LuminanceSource ZXing.Color32LuminanceSource::CreateLuminanceSource(System.Byte[],System.Int32,System.Int32)
extern void Color32LuminanceSource_CreateLuminanceSource_m65C7C5A82776457193130328F2685061E01F4C40 (void);
// 0x00000184 UnityEngine.Color32 ZXing.Color32Renderer::get_Foreground()
extern void Color32Renderer_get_Foreground_m74E1B20041CCCD76F715171DFEC058BF45531150 (void);
// 0x00000185 System.Void ZXing.Color32Renderer::set_Foreground(UnityEngine.Color32)
extern void Color32Renderer_set_Foreground_m9225E2481B5DF15FBDABFD3A5F2B8D7FA0BA2C36 (void);
// 0x00000186 UnityEngine.Color32 ZXing.Color32Renderer::get_Background()
extern void Color32Renderer_get_Background_mAB232887582459E31BDD62429EFB69CD960FDDDA (void);
// 0x00000187 System.Void ZXing.Color32Renderer::set_Background(UnityEngine.Color32)
extern void Color32Renderer_set_Background_m21CB6390A36918D1F3025B1EED887EEAE004807F (void);
// 0x00000188 System.Void ZXing.Color32Renderer::.ctor()
extern void Color32Renderer__ctor_mC5A13A842C980C6B82C98E46A03B99193474DED3 (void);
// 0x00000189 UnityEngine.Color32[] ZXing.Color32Renderer::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String)
extern void Color32Renderer_Render_m351C5D62D12FCC130C012CA52F64E7DA04B82094 (void);
// 0x0000018A UnityEngine.Color32[] ZXing.Color32Renderer::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String,ZXing.Common.EncodingOptions)
extern void Color32Renderer_Render_mD07481C8167DE11C1FDF6F1942B81F29E4C50E43 (void);
// 0x0000018B ZXing.Common.BitMatrix ZXing.Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32)
// 0x0000018C ZXing.Common.BitMatrix ZXing.Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
// 0x0000018D System.Void ZXing.WriterException::.ctor()
extern void WriterException__ctor_mFC6879418452293BFBE57C7AA829AC46365E7611 (void);
// 0x0000018E System.Void ZXing.WriterException::.ctor(System.String)
extern void WriterException__ctor_mD352B904167FB22C64B85ACF91DF020B8267EF30 (void);
// 0x0000018F System.Void ZXing.WriterException::.ctor(System.String,System.Exception)
extern void WriterException__ctor_mEC7412EB2A678F72EB5CEB68B484006807A62C03 (void);
// 0x00000190 TOutput ZXing.Rendering.IBarcodeRenderer`1::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String)
// 0x00000191 TOutput ZXing.Rendering.IBarcodeRenderer`1::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String,ZXing.Common.EncodingOptions)
// 0x00000192 UnityEngine.Color32[] ZXing.Rendering.PixelData::ToColor32()
extern void PixelData_ToColor32_mCB6EF3582563538106EAD34C85351F07D0E03D25 (void);
// 0x00000193 System.Void ZXing.Rendering.PixelData::.ctor(System.Int32,System.Int32,System.Byte[])
extern void PixelData__ctor_m698C4C322EA5FE08465693CBDBE97AF20865453E (void);
// 0x00000194 System.Byte[] ZXing.Rendering.PixelData::get_Pixels()
extern void PixelData_get_Pixels_m6C6C44024CE81793733E7C73BD28FDB075CFA1FD (void);
// 0x00000195 System.Void ZXing.Rendering.PixelData::set_Pixels(System.Byte[])
extern void PixelData_set_Pixels_m64F136781F0B4786BA25DC8C8215D6CDA69CB68C (void);
// 0x00000196 System.Int32 ZXing.Rendering.PixelData::get_Width()
extern void PixelData_get_Width_mE0FC5755D330A797E7BCE338D7A2C0EB243353DF (void);
// 0x00000197 System.Void ZXing.Rendering.PixelData::set_Width(System.Int32)
extern void PixelData_set_Width_m24A7249700CA98A02482A4040E2C6DDBB840A288 (void);
// 0x00000198 System.Int32 ZXing.Rendering.PixelData::get_Height()
extern void PixelData_get_Height_mDF58650306B5EF68901AD2EAE1ABBF3A67C52B4B (void);
// 0x00000199 System.Void ZXing.Rendering.PixelData::set_Height(System.Int32)
extern void PixelData_set_Height_mC73A24A10B1DCFFE2C3943BED3E893C8D80819DF (void);
// 0x0000019A UnityEngine.Color32 ZXing.Rendering.PixelDataRenderer::get_Foreground()
extern void PixelDataRenderer_get_Foreground_mB2D6865AB8A1AB7508DA28DE0E9EA0357369F1C5 (void);
// 0x0000019B System.Void ZXing.Rendering.PixelDataRenderer::set_Foreground(UnityEngine.Color32)
extern void PixelDataRenderer_set_Foreground_mF7894FD354D7A4C99A19DBC6C9CA7843E20EAE14 (void);
// 0x0000019C UnityEngine.Color32 ZXing.Rendering.PixelDataRenderer::get_Background()
extern void PixelDataRenderer_get_Background_mBF55401F10C4DAA22A585FB25569F32B2915B74F (void);
// 0x0000019D System.Void ZXing.Rendering.PixelDataRenderer::set_Background(UnityEngine.Color32)
extern void PixelDataRenderer_set_Background_mF702170EBCA864367767EB0891D6B1752EAFDC89 (void);
// 0x0000019E System.Void ZXing.Rendering.PixelDataRenderer::.ctor()
extern void PixelDataRenderer__ctor_m8C1845EC562DD8A5F1DEC899B3B7E0FC4A53A69B (void);
// 0x0000019F ZXing.Rendering.PixelData ZXing.Rendering.PixelDataRenderer::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String)
extern void PixelDataRenderer_Render_m9CC2948427EAFDBDF578FE78905937CC104BE8D9 (void);
// 0x000001A0 ZXing.Rendering.PixelData ZXing.Rendering.PixelDataRenderer::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String,ZXing.Common.EncodingOptions)
extern void PixelDataRenderer_Render_mD61F5FB224B8D818C8CB60B829A51F8E7D8B12E1 (void);
// 0x000001A1 UnityEngine.Color32 ZXing.Rendering.SvgRenderer::get_Foreground()
extern void SvgRenderer_get_Foreground_m312A2472FE39F6044EAC40DC3B6326FC0917219E (void);
// 0x000001A2 System.Void ZXing.Rendering.SvgRenderer::set_Foreground(UnityEngine.Color32)
extern void SvgRenderer_set_Foreground_m024CE081587D1B60F003EC990256D06B38B97B1F (void);
// 0x000001A3 UnityEngine.Color32 ZXing.Rendering.SvgRenderer::get_Background()
extern void SvgRenderer_get_Background_m2D66426AD1C9FC6C654C617661EB24CE93ED7D77 (void);
// 0x000001A4 System.Void ZXing.Rendering.SvgRenderer::set_Background(UnityEngine.Color32)
extern void SvgRenderer_set_Background_mEE254344A04EF967BD67F388E684D269B975E664 (void);
// 0x000001A5 System.String ZXing.Rendering.SvgRenderer::get_FontName()
extern void SvgRenderer_get_FontName_m677A2FEADB0F12C8B52BF98B1991723A6A136CD8 (void);
// 0x000001A6 System.Void ZXing.Rendering.SvgRenderer::set_FontName(System.String)
extern void SvgRenderer_set_FontName_m4A434249869E31E2807CBA42A2193B9DD8CA864A (void);
// 0x000001A7 System.Int32 ZXing.Rendering.SvgRenderer::get_FontSize()
extern void SvgRenderer_get_FontSize_mB38E7688F85B9AFBCBEDE3BEFC44D27D08EF3DDB (void);
// 0x000001A8 System.Void ZXing.Rendering.SvgRenderer::set_FontSize(System.Int32)
extern void SvgRenderer_set_FontSize_mB1197775E856C8A63B7C5938B5E1F84632DB2EB6 (void);
// 0x000001A9 System.Void ZXing.Rendering.SvgRenderer::.ctor()
extern void SvgRenderer__ctor_mAAD5A231E7D7487B3EC3B74182D1508EFCE1ACAF (void);
// 0x000001AA ZXing.Rendering.SvgRenderer/SvgImage ZXing.Rendering.SvgRenderer::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String)
extern void SvgRenderer_Render_m877711F5255B0A28F77F34FBAA5D94057632F8C9 (void);
// 0x000001AB ZXing.Rendering.SvgRenderer/SvgImage ZXing.Rendering.SvgRenderer::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String,ZXing.Common.EncodingOptions)
extern void SvgRenderer_Render_m8A425E3542A0A3D853C15812BDD07C2CDC9C54AC (void);
// 0x000001AC System.Void ZXing.Rendering.SvgRenderer::Create(ZXing.Rendering.SvgRenderer/SvgImage,ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String,ZXing.Common.EncodingOptions)
extern void SvgRenderer_Create_mC593A01CA3EE86F1B878681E721E7018509D2C9E (void);
// 0x000001AD System.String ZXing.Rendering.SvgRenderer::ModifyContentDependingOnBarcodeFormat(ZXing.BarcodeFormat,System.String)
extern void SvgRenderer_ModifyContentDependingOnBarcodeFormat_m7B6DD3974BA4817D83F3B444B252687766C81665 (void);
// 0x000001AE System.Void ZXing.Rendering.SvgRenderer::AppendDarkCell(ZXing.Rendering.SvgRenderer/SvgImage,ZXing.Common.BitMatrix,System.Int32,System.Int32)
extern void SvgRenderer_AppendDarkCell_mA93AD3C3F42F338D1A1B207E9EAA99CFF8C2A789 (void);
// 0x000001AF System.Void ZXing.Rendering.SvgRenderer::FindMaximumRectangle(ZXing.Common.BitMatrix,ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Int32&)
extern void SvgRenderer_FindMaximumRectangle_m7199F76561F59CCB28F3A7BB6BECD294392E4FAE (void);
// 0x000001B0 System.String ZXing.Rendering.SvgRenderer/SvgImage::get_Content()
extern void SvgImage_get_Content_mEBAF6604E3C9E899D96750356C36FF9A0D012FB9 (void);
// 0x000001B1 System.Void ZXing.Rendering.SvgRenderer/SvgImage::set_Content(System.String)
extern void SvgImage_set_Content_m37C27169A98EC442269FE296F2A66D4747946288 (void);
// 0x000001B2 System.Int32 ZXing.Rendering.SvgRenderer/SvgImage::get_Height()
extern void SvgImage_get_Height_m4A4C1EFE7FB4225C26E9E790DBC9B30E29775008 (void);
// 0x000001B3 System.Void ZXing.Rendering.SvgRenderer/SvgImage::set_Height(System.Int32)
extern void SvgImage_set_Height_m917C5B6E223C02A68FFBB3E31A12981D226DBCB8 (void);
// 0x000001B4 System.Int32 ZXing.Rendering.SvgRenderer/SvgImage::get_Width()
extern void SvgImage_get_Width_m89DA3BCF8ABB02CB5E9E0ACB26A6BDE7634AF15B (void);
// 0x000001B5 System.Void ZXing.Rendering.SvgRenderer/SvgImage::set_Width(System.Int32)
extern void SvgImage_set_Width_m62DA718D5E8F51D5ACC6924E3D5BAF506E03A122 (void);
// 0x000001B6 System.Void ZXing.Rendering.SvgRenderer/SvgImage::.ctor()
extern void SvgImage__ctor_m607C867740BBB03520D59A9AD9AE52724A7DBD19 (void);
// 0x000001B7 System.Void ZXing.Rendering.SvgRenderer/SvgImage::.ctor(System.Int32,System.Int32)
extern void SvgImage__ctor_mA1F5BBA10752AE10EA738E1E3CAA75EC4A03C466 (void);
// 0x000001B8 System.Void ZXing.Rendering.SvgRenderer/SvgImage::.ctor(System.String)
extern void SvgImage__ctor_m9373E58CE93221AB26051B448FF64EAA3C900B06 (void);
// 0x000001B9 System.String ZXing.Rendering.SvgRenderer/SvgImage::ToString()
extern void SvgImage_ToString_m3E153761F4573411F389B46FCF7A35877846DF24 (void);
// 0x000001BA System.Void ZXing.Rendering.SvgRenderer/SvgImage::AddHeader()
extern void SvgImage_AddHeader_mD39A343469014FDCC5E0A82AF0697F7FD499BADD (void);
// 0x000001BB System.Void ZXing.Rendering.SvgRenderer/SvgImage::AddEnd()
extern void SvgImage_AddEnd_m7638A2A8011A6E30E2FE8991C306B450DA0274CA (void);
// 0x000001BC System.Void ZXing.Rendering.SvgRenderer/SvgImage::AddTag(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color,UnityEngine.Color)
extern void SvgImage_AddTag_mD330B99C47734C5501990D2A58BADAC5955E9238 (void);
// 0x000001BD System.Void ZXing.Rendering.SvgRenderer/SvgImage::AddText(System.String,System.String,System.Int32)
extern void SvgImage_AddText_m2F7E6C50BB674D20325CD52749A728B17C95725D (void);
// 0x000001BE System.Void ZXing.Rendering.SvgRenderer/SvgImage::AddRec(System.Int32,System.Int32,System.Int32,System.Int32)
extern void SvgImage_AddRec_mA7FF92AFAF64CCF404A7BA5AEC058F1E4EA6FFA8 (void);
// 0x000001BF System.Double ZXing.Rendering.SvgRenderer/SvgImage::ConvertAlpha(UnityEngine.Color32)
extern void SvgImage_ConvertAlpha_mB1AFDD4295A8A95C45BA550311E54A62B146CCB5 (void);
// 0x000001C0 System.String ZXing.Rendering.SvgRenderer/SvgImage::GetBackgroundStyle(UnityEngine.Color32)
extern void SvgImage_GetBackgroundStyle_mA52CFF48E56A77F72045F7B4D73BC43A65D3C69B (void);
// 0x000001C1 System.String ZXing.Rendering.SvgRenderer/SvgImage::GetColorRgb(UnityEngine.Color32)
extern void SvgImage_GetColorRgb_mFFB8E32730DDC3A6E34838D054A4B53C9C895716 (void);
// 0x000001C2 System.String ZXing.Rendering.SvgRenderer/SvgImage::GetColorRgba(UnityEngine.Color32)
extern void SvgImage_GetColorRgba_m8F7A7CCC4325FEBF66F48800C747D23E6E27336E (void);
// 0x000001C3 ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.QrCodeEncodingOptions::get_ErrorCorrection()
extern void QrCodeEncodingOptions_get_ErrorCorrection_mEF81A8B3681653749276CC482737E557EFC74CD0 (void);
// 0x000001C4 System.Void ZXing.QrCode.QrCodeEncodingOptions::set_ErrorCorrection(ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void QrCodeEncodingOptions_set_ErrorCorrection_m6B92CA1620CCEEE5242720D102F33BF3232D620A (void);
// 0x000001C5 System.String ZXing.QrCode.QrCodeEncodingOptions::get_CharacterSet()
extern void QrCodeEncodingOptions_get_CharacterSet_m90CD3FC6325B7C5B1E01C0DC1E344F1DD33E1BA3 (void);
// 0x000001C6 System.Void ZXing.QrCode.QrCodeEncodingOptions::set_CharacterSet(System.String)
extern void QrCodeEncodingOptions_set_CharacterSet_m1970C5095AE0B2235A2790910F286BAC6F701B99 (void);
// 0x000001C7 System.Boolean ZXing.QrCode.QrCodeEncodingOptions::get_DisableECI()
extern void QrCodeEncodingOptions_get_DisableECI_m6D1B561F53C2883207C70FC029D0983CFBE302FD (void);
// 0x000001C8 System.Void ZXing.QrCode.QrCodeEncodingOptions::set_DisableECI(System.Boolean)
extern void QrCodeEncodingOptions_set_DisableECI_m1FA72A2F6BB60211C6EB1C776AD173C38DE6A854 (void);
// 0x000001C9 System.Nullable`1<System.Int32> ZXing.QrCode.QrCodeEncodingOptions::get_QrVersion()
extern void QrCodeEncodingOptions_get_QrVersion_m5F9631B670EACDF6F34C16C05260CE4AFD424CC7 (void);
// 0x000001CA System.Void ZXing.QrCode.QrCodeEncodingOptions::set_QrVersion(System.Nullable`1<System.Int32>)
extern void QrCodeEncodingOptions_set_QrVersion_m09FBBB137AD5684E6C76AD3DC9743B64E0B7BB7C (void);
// 0x000001CB System.Void ZXing.QrCode.QrCodeEncodingOptions::.ctor()
extern void QrCodeEncodingOptions__ctor_mC5EF7BCD9427BE7EFAECF143D51D72A6D487A199 (void);
// 0x000001CC ZXing.QrCode.Internal.Decoder ZXing.QrCode.QRCodeReader::getDecoder()
extern void QRCodeReader_getDecoder_mFE6BFE5ADACFF7573EC009995E5915519ABBD049 (void);
// 0x000001CD ZXing.Result ZXing.QrCode.QRCodeReader::decode(ZXing.BinaryBitmap)
extern void QRCodeReader_decode_m24DEA81B154867B51E2E0CF0714DC01E44D6835D (void);
// 0x000001CE ZXing.Result ZXing.QrCode.QRCodeReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void QRCodeReader_decode_m282FC933773D428FB2AD20E41DA218EEA7456914 (void);
// 0x000001CF System.Void ZXing.QrCode.QRCodeReader::reset()
extern void QRCodeReader_reset_mCC520C963D9370429051F693F1BE92A1765E3098 (void);
// 0x000001D0 ZXing.Common.BitMatrix ZXing.QrCode.QRCodeReader::extractPureBits(ZXing.Common.BitMatrix)
extern void QRCodeReader_extractPureBits_mFDDDACD1F15811EF4AA40FC96B151FA5BACAE44A (void);
// 0x000001D1 System.Boolean ZXing.QrCode.QRCodeReader::moduleSize(System.Int32[],ZXing.Common.BitMatrix,System.Single&)
extern void QRCodeReader_moduleSize_m07CA733B2B9832022773BDF944358FB294EE0F61 (void);
// 0x000001D2 System.Void ZXing.QrCode.QRCodeReader::.ctor()
extern void QRCodeReader__ctor_m1EC9E46F30B90F1EF2FC21A74CE8520DA13433E3 (void);
// 0x000001D3 System.Void ZXing.QrCode.QRCodeReader::.cctor()
extern void QRCodeReader__cctor_mF81281A4A5216AC0CC4D4AC1A5CDBEE8E1B7CF31 (void);
// 0x000001D4 ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32)
extern void QRCodeWriter_encode_mAD1D48443F04C98087A57085AE06312BDC50810D (void);
// 0x000001D5 ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void QRCodeWriter_encode_m1BFFB91DC50E617B19BA3E5043C3E0E185EDFCCD (void);
// 0x000001D6 ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::renderResult(ZXing.QrCode.Internal.QRCode,System.Int32,System.Int32,System.Int32)
extern void QRCodeWriter_renderResult_mB0748A5E248F44C3687E4E735A9C2D6140E3C186 (void);
// 0x000001D7 System.Void ZXing.QrCode.QRCodeWriter::.ctor()
extern void QRCodeWriter__ctor_mD3B4389B0968B1E371BFA71E1C175D9CB01C677E (void);
// 0x000001D8 ZXing.QrCode.Internal.BitMatrixParser ZXing.QrCode.Internal.BitMatrixParser::createBitMatrixParser(ZXing.Common.BitMatrix)
extern void BitMatrixParser_createBitMatrixParser_m0C35E4E2699B59EF14FD4B6CDDA39596E22932FD (void);
// 0x000001D9 System.Void ZXing.QrCode.Internal.BitMatrixParser::.ctor(ZXing.Common.BitMatrix)
extern void BitMatrixParser__ctor_mD44AE281FD8928634F8A8902216F43D6C7C9A166 (void);
// 0x000001DA ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.BitMatrixParser::readFormatInformation()
extern void BitMatrixParser_readFormatInformation_m2FBD0758585E8AB776DE313A265265B4D67C7C1E (void);
// 0x000001DB ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.BitMatrixParser::readVersion()
extern void BitMatrixParser_readVersion_mFE6EBB447E4E73ED7A33BB0B3AAE0E07ACB10C66 (void);
// 0x000001DC System.Int32 ZXing.QrCode.Internal.BitMatrixParser::copyBit(System.Int32,System.Int32,System.Int32)
extern void BitMatrixParser_copyBit_mE5B3281BA1E762825467ADDD4F9935EAA4573DDD (void);
// 0x000001DD System.Byte[] ZXing.QrCode.Internal.BitMatrixParser::readCodewords()
extern void BitMatrixParser_readCodewords_m10235A7384448A064F57D8B085D1367B1B9667DB (void);
// 0x000001DE System.Void ZXing.QrCode.Internal.BitMatrixParser::remask()
extern void BitMatrixParser_remask_m02ECE3CAE197FF831DAA57B8BE93D62B788984F7 (void);
// 0x000001DF System.Void ZXing.QrCode.Internal.BitMatrixParser::setMirror(System.Boolean)
extern void BitMatrixParser_setMirror_m5A572BEF2347709247EBD6D4C20FA1093FEEB1FB (void);
// 0x000001E0 System.Void ZXing.QrCode.Internal.BitMatrixParser::mirror()
extern void BitMatrixParser_mirror_m2627CE903118AD1D50668EE09E44ED7EA65E8BE5 (void);
// 0x000001E1 System.Void ZXing.QrCode.Internal.DataBlock::.ctor(System.Int32,System.Byte[])
extern void DataBlock__ctor_m417EBE9EAF769C833FD469357B2B990DF06F038B (void);
// 0x000001E2 ZXing.QrCode.Internal.DataBlock[] ZXing.QrCode.Internal.DataBlock::getDataBlocks(System.Byte[],ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void DataBlock_getDataBlocks_m09C2CB0E9E517D14A75A4018280276A8A4B635F5 (void);
// 0x000001E3 System.Int32 ZXing.QrCode.Internal.DataBlock::get_NumDataCodewords()
extern void DataBlock_get_NumDataCodewords_mDA2B9378F5878724714B5FDB555797255A61EE54 (void);
// 0x000001E4 System.Byte[] ZXing.QrCode.Internal.DataBlock::get_Codewords()
extern void DataBlock_get_Codewords_m893B7358A589634CBF04810E94DF42BBC2ABA6FA (void);
// 0x000001E5 System.Void ZXing.QrCode.Internal.DataMask::unmaskBitMatrix(System.Int32,ZXing.Common.BitMatrix,System.Int32)
extern void DataMask_unmaskBitMatrix_m6BDE78A99AB905FD832A0E4A226C23D401C0AE00 (void);
// 0x000001E6 System.Void ZXing.QrCode.Internal.DataMask::.cctor()
extern void DataMask__cctor_mE44E06C960D185CE302E72CE983FC6A3689A6C5A (void);
// 0x000001E7 System.Void ZXing.QrCode.Internal.DataMask/<>c::.cctor()
extern void U3CU3Ec__cctor_mA0B6321A2B6A36815A86724DE41665FEAA1ABABD (void);
// 0x000001E8 System.Void ZXing.QrCode.Internal.DataMask/<>c::.ctor()
extern void U3CU3Ec__ctor_m556702C7E95C4F77530429D5BA643B8802C72D36 (void);
// 0x000001E9 System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_0(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_0_mFBCF966E297BA04C73BA8C517CA6926121786CEC (void);
// 0x000001EA System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_1(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_1_m4AB5E7BE0BDE8C54A771F326D7C00277241C0F04 (void);
// 0x000001EB System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_2(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_2_m19FE764F48328392BE3BE9EAA653F1EC84C9BD60 (void);
// 0x000001EC System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_3(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_3_mB37B2A94BDE52A330E0778A96B671B464A7DB6B1 (void);
// 0x000001ED System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_4(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_4_m7813575439FDF5AAC2BF3AA4F25834506B326038 (void);
// 0x000001EE System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_5(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_5_m9E7BDD970B53ED12F4FDC26C09A119815FC3CBBA (void);
// 0x000001EF System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_6(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_6_m7A92D2F9CCD2E141967ABD8E652D8BC78E0548EA (void);
// 0x000001F0 System.Boolean ZXing.QrCode.Internal.DataMask/<>c::<.cctor>b__2_7(System.Int32,System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__2_7_mDE1A7A2D9550E4A755FBC7B6B5B186A6283DFD74 (void);
// 0x000001F1 ZXing.Common.DecoderResult ZXing.QrCode.Internal.DecodedBitStreamParser::decode(System.Byte[],ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void DecodedBitStreamParser_decode_m7466DA93B0B4DA2BF16811DB96DC10D887F13E0B (void);
// 0x000001F2 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeHanziSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32)
extern void DecodedBitStreamParser_decodeHanziSegment_mBBF7C0A4BBF1080F52B87297BE2E1162EDA05B83 (void);
// 0x000001F3 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeKanjiSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32)
extern void DecodedBitStreamParser_decodeKanjiSegment_mCCBB64E1AC00744605EEFB90D7071A65B146540E (void);
// 0x000001F4 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeByteSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32,ZXing.Common.CharacterSetECI,System.Collections.Generic.IList`1<System.Byte[]>,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void DecodedBitStreamParser_decodeByteSegment_m18FECF2FC312D5AE766D5940C8C4307708B4F8DD (void);
// 0x000001F5 System.Char ZXing.QrCode.Internal.DecodedBitStreamParser::toAlphaNumericChar(System.Int32)
extern void DecodedBitStreamParser_toAlphaNumericChar_mCC688581B9FDE61C9C24216BAFF687A0878A4695 (void);
// 0x000001F6 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeAlphanumericSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32,System.Boolean)
extern void DecodedBitStreamParser_decodeAlphanumericSegment_m5BCF1F3FFE802DAE2552B833104F6D5083FBD61C (void);
// 0x000001F7 System.Boolean ZXing.QrCode.Internal.DecodedBitStreamParser::decodeNumericSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Int32)
extern void DecodedBitStreamParser_decodeNumericSegment_mB6A7AD95007BE58FFC3F487A810C2685703A85CB (void);
// 0x000001F8 System.Int32 ZXing.QrCode.Internal.DecodedBitStreamParser::parseECIValue(ZXing.Common.BitSource)
extern void DecodedBitStreamParser_parseECIValue_mF160C40D8B3FD522E2FAEAF60D058B4D3C9EE184 (void);
// 0x000001F9 System.Void ZXing.QrCode.Internal.DecodedBitStreamParser::.cctor()
extern void DecodedBitStreamParser__cctor_mACDF5C4B85C83F30294824652F16BEF436CFBC67 (void);
// 0x000001FA System.Void ZXing.QrCode.Internal.Decoder::.ctor()
extern void Decoder__ctor_m16ADDFAE3DDE51F59CC33BD4BAC8B093B59A8D3C (void);
// 0x000001FB ZXing.Common.DecoderResult ZXing.QrCode.Internal.Decoder::decode(System.Boolean[][],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Decoder_decode_m81B7FE4F44CCBD41F83412B490CB84CDAB56A9C1 (void);
// 0x000001FC ZXing.Common.DecoderResult ZXing.QrCode.Internal.Decoder::decode(ZXing.Common.BitMatrix,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Decoder_decode_mD7898B72ADCA02F9706EEE378187EB899AC7ADAE (void);
// 0x000001FD ZXing.Common.DecoderResult ZXing.QrCode.Internal.Decoder::decode(ZXing.QrCode.Internal.BitMatrixParser,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Decoder_decode_m3616EE0B80C3B5C2BD2DAD4D3863EAB51C75949D (void);
// 0x000001FE System.Boolean ZXing.QrCode.Internal.Decoder::correctErrors(System.Byte[],System.Int32)
extern void Decoder_correctErrors_m380591C25A6BD9296CC9BFCFA3E1C43BD72FB3EB (void);
// 0x000001FF System.Void ZXing.QrCode.Internal.ErrorCorrectionLevel::.ctor(System.Int32,System.Int32,System.String)
extern void ErrorCorrectionLevel__ctor_m71B8348BA6486EE615792F076F53F8DAF501D2F6 (void);
// 0x00000200 System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::get_Bits()
extern void ErrorCorrectionLevel_get_Bits_m7765C510F012BE59471A5958DD5FDF978E1045A2 (void);
// 0x00000201 System.String ZXing.QrCode.Internal.ErrorCorrectionLevel::get_Name()
extern void ErrorCorrectionLevel_get_Name_m6FD65C9C806F63D8295F9A575264D270FA646812 (void);
// 0x00000202 System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::ordinal()
extern void ErrorCorrectionLevel_ordinal_m340F3DD63E0FB50C6482A072868F5D4BA2D01003 (void);
// 0x00000203 System.String ZXing.QrCode.Internal.ErrorCorrectionLevel::ToString()
extern void ErrorCorrectionLevel_ToString_m77247B26B89A369067873DD008E7B6E951894D79 (void);
// 0x00000204 ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::forBits(System.Int32)
extern void ErrorCorrectionLevel_forBits_m190CF75EDFAAD1DC6CA65F5A57984FC56CC0FD13 (void);
// 0x00000205 System.Void ZXing.QrCode.Internal.ErrorCorrectionLevel::.cctor()
extern void ErrorCorrectionLevel__cctor_m9BD213C961105B66D7E70697753295465042CD27 (void);
// 0x00000206 System.Void ZXing.QrCode.Internal.FormatInformation::.ctor(System.Int32)
extern void FormatInformation__ctor_m705630BBE9CD33F82EC54B6C82B5C41EAE4BCEC1 (void);
// 0x00000207 System.Int32 ZXing.QrCode.Internal.FormatInformation::numBitsDiffering(System.Int32,System.Int32)
extern void FormatInformation_numBitsDiffering_m03E1BAAF42556E828A2D610B28D6197C9BE066AC (void);
// 0x00000208 ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.FormatInformation::decodeFormatInformation(System.Int32,System.Int32)
extern void FormatInformation_decodeFormatInformation_m0776EB2551C6A1A0FFE8CAAD3C4BB1393F6629DF (void);
// 0x00000209 ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.FormatInformation::doDecodeFormatInformation(System.Int32,System.Int32)
extern void FormatInformation_doDecodeFormatInformation_m79CE9CADF3D3D0AB56932CFAE9020519FDA95F2A (void);
// 0x0000020A ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.FormatInformation::get_ErrorCorrectionLevel()
extern void FormatInformation_get_ErrorCorrectionLevel_m2C82275BE86395DED1EEF3CFD724BC170DA93B36 (void);
// 0x0000020B System.Byte ZXing.QrCode.Internal.FormatInformation::get_DataMask()
extern void FormatInformation_get_DataMask_mF4640F81E0039269CAC251D7064E3CB13EDEA0F4 (void);
// 0x0000020C System.Int32 ZXing.QrCode.Internal.FormatInformation::GetHashCode()
extern void FormatInformation_GetHashCode_m064DB3EBA0C409DCEF4A245863879D16CC020D0B (void);
// 0x0000020D System.Boolean ZXing.QrCode.Internal.FormatInformation::Equals(System.Object)
extern void FormatInformation_Equals_m2462A5E3B9F6FE5EEEABFF52B77EB38867CECD7A (void);
// 0x0000020E System.Void ZXing.QrCode.Internal.FormatInformation::.cctor()
extern void FormatInformation__cctor_m7A04DB3D7D75E1914D26CBCBDEE7842CB55FE21F (void);
// 0x0000020F ZXing.QrCode.Internal.Mode/Names ZXing.QrCode.Internal.Mode::get_Name()
extern void Mode_get_Name_m8589A71CD40010EB376CF548C5E2A68B81ADE220 (void);
// 0x00000210 System.Void ZXing.QrCode.Internal.Mode::set_Name(ZXing.QrCode.Internal.Mode/Names)
extern void Mode_set_Name_mA322F9B2A69E018DA2C0C00728951DAD9C1B0AA5 (void);
// 0x00000211 System.Void ZXing.QrCode.Internal.Mode::.ctor(System.Int32[],System.Int32,ZXing.QrCode.Internal.Mode/Names)
extern void Mode__ctor_mD5D384B0689300761D261562B95FDE1E9AE05F9B (void);
// 0x00000212 ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::forBits(System.Int32)
extern void Mode_forBits_m5733DD8E754E6E625B86A864D22C465A66A023C9 (void);
// 0x00000213 System.Int32 ZXing.QrCode.Internal.Mode::getCharacterCountBits(ZXing.QrCode.Internal.Version)
extern void Mode_getCharacterCountBits_m0CF7362524C53F2B1030643EFFFDEC312D91E586 (void);
// 0x00000214 System.Int32 ZXing.QrCode.Internal.Mode::get_Bits()
extern void Mode_get_Bits_m7D132DB5475D33B0684FF74556DED9ED217078E8 (void);
// 0x00000215 System.Void ZXing.QrCode.Internal.Mode::set_Bits(System.Int32)
extern void Mode_set_Bits_m2D38555F80B0E2B3DCD0850233B3E3F549620942 (void);
// 0x00000216 System.String ZXing.QrCode.Internal.Mode::ToString()
extern void Mode_ToString_m5F213C2613769D061B85CF780ED6E653E0944BD5 (void);
// 0x00000217 System.Void ZXing.QrCode.Internal.Mode::.cctor()
extern void Mode__cctor_mE5C404377B748A0E467F7EEFDB572CF6A7C8C414 (void);
// 0x00000218 System.Void ZXing.QrCode.Internal.QRCodeDecoderMetaData::.ctor(System.Boolean)
extern void QRCodeDecoderMetaData__ctor_mFE0CD9D642985F21F08437774A0442973A4B0B78 (void);
// 0x00000219 System.Boolean ZXing.QrCode.Internal.QRCodeDecoderMetaData::get_IsMirrored()
extern void QRCodeDecoderMetaData_get_IsMirrored_m7A00F5F719BF1D172FAFB00277C1DB3B49AA4A84 (void);
// 0x0000021A System.Void ZXing.QrCode.Internal.QRCodeDecoderMetaData::applyMirroredCorrection(ZXing.ResultPoint[])
extern void QRCodeDecoderMetaData_applyMirroredCorrection_m41BC45AAD2B641CDC73D998CAA5741ACE6B5B348 (void);
// 0x0000021B System.Void ZXing.QrCode.Internal.Version::.ctor(System.Int32,System.Int32[],ZXing.QrCode.Internal.Version/ECBlocks[])
extern void Version__ctor_m01721E975A63B7874D29F6E7AD23733118FD1200 (void);
// 0x0000021C System.Int32 ZXing.QrCode.Internal.Version::get_VersionNumber()
extern void Version_get_VersionNumber_mB6347F35E8C2A8F49282ABBB8D43A233A8D8C8FD (void);
// 0x0000021D System.Int32[] ZXing.QrCode.Internal.Version::get_AlignmentPatternCenters()
extern void Version_get_AlignmentPatternCenters_m2BB7540E510C292C8ECC115CB3082763D9A2946C (void);
// 0x0000021E System.Int32 ZXing.QrCode.Internal.Version::get_TotalCodewords()
extern void Version_get_TotalCodewords_m0F5246C6C80C4274A970BB18F39C8C8DEA3877C6 (void);
// 0x0000021F System.Int32 ZXing.QrCode.Internal.Version::get_DimensionForVersion()
extern void Version_get_DimensionForVersion_m737D1DAB2ECADCF500A4A840912554F320DFC319 (void);
// 0x00000220 ZXing.QrCode.Internal.Version/ECBlocks ZXing.QrCode.Internal.Version::getECBlocksForLevel(ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void Version_getECBlocksForLevel_m1377C6C059A2F33E6055DCAE5FBF35D5FAE0BFD4 (void);
// 0x00000221 ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getProvisionalVersionForDimension(System.Int32)
extern void Version_getProvisionalVersionForDimension_m2A81BC5D0CF18D93D2518DAB6FC2270D2AE7FAB3 (void);
// 0x00000222 ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getVersionForNumber(System.Int32)
extern void Version_getVersionForNumber_m09318039C3DF1A3108442E66639DA7871ED34D4A (void);
// 0x00000223 ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::decodeVersionInformation(System.Int32)
extern void Version_decodeVersionInformation_mFCF49CCEE179EADEC292876CD7FAF9A437A3A0FC (void);
// 0x00000224 ZXing.Common.BitMatrix ZXing.QrCode.Internal.Version::buildFunctionPattern()
extern void Version_buildFunctionPattern_m4201B1377FD8F4AF63AF2C60BADBC0854BB6E2BD (void);
// 0x00000225 System.String ZXing.QrCode.Internal.Version::ToString()
extern void Version_ToString_m034F18F2F0993A74ADC1C9C8E9673FF68806DE26 (void);
// 0x00000226 ZXing.QrCode.Internal.Version[] ZXing.QrCode.Internal.Version::buildVersions()
extern void Version_buildVersions_m88A9C36BF7ACC745F662B4E07913D34168EBFDF5 (void);
// 0x00000227 System.Void ZXing.QrCode.Internal.Version::.cctor()
extern void Version__cctor_m4DD86CFA355EC2ED3663016E34576E12C7667CA4 (void);
// 0x00000228 System.Void ZXing.QrCode.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.QrCode.Internal.Version/ECB[])
extern void ECBlocks__ctor_m8A1F8D46EBC500F090667507F9C128CC84CEA84F (void);
// 0x00000229 System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_ECCodewordsPerBlock()
extern void ECBlocks_get_ECCodewordsPerBlock_m40304ECA66F4757EB3C8CD9876C1BB81186816A5 (void);
// 0x0000022A System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_NumBlocks()
extern void ECBlocks_get_NumBlocks_m08D47F57DE75E8A8571809E3843EB5837F27D110 (void);
// 0x0000022B System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_TotalECCodewords()
extern void ECBlocks_get_TotalECCodewords_m9D1B9D8AE087E26CAD4945547757B61F97858326 (void);
// 0x0000022C ZXing.QrCode.Internal.Version/ECB[] ZXing.QrCode.Internal.Version/ECBlocks::getECBlocks()
extern void ECBlocks_getECBlocks_mE3D4CB94FE7220D415BCE709BDA30D783BBAABC4 (void);
// 0x0000022D System.Void ZXing.QrCode.Internal.Version/ECB::.ctor(System.Int32,System.Int32)
extern void ECB__ctor_mA6F6B9D5ED3E489AEE67B99C06273667CFCD35A9 (void);
// 0x0000022E System.Int32 ZXing.QrCode.Internal.Version/ECB::get_Count()
extern void ECB_get_Count_mD1AFECFCF9690ED0A538935236B3F40650628746 (void);
// 0x0000022F System.Int32 ZXing.QrCode.Internal.Version/ECB::get_DataCodewords()
extern void ECB_get_DataCodewords_mC411D76846C30483ED5244B759AD405DF6CDDA02 (void);
// 0x00000230 System.Void ZXing.QrCode.Internal.AlignmentPattern::.ctor(System.Single,System.Single,System.Single)
extern void AlignmentPattern__ctor_m7CE66A97BBA514C435318F43E8E15C5219152C7C (void);
// 0x00000231 System.Boolean ZXing.QrCode.Internal.AlignmentPattern::aboutEquals(System.Single,System.Single,System.Single)
extern void AlignmentPattern_aboutEquals_m729A633AEBF05B27CB4ECABBCABD1CD064E859C3 (void);
// 0x00000232 ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.AlignmentPattern::combineEstimate(System.Single,System.Single,System.Single)
extern void AlignmentPattern_combineEstimate_m93E40979572ACEB6EDE6E9C91D482ED21B5E9788 (void);
// 0x00000233 System.Void ZXing.QrCode.Internal.AlignmentPatternFinder::.ctor(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Int32,System.Single,ZXing.ResultPointCallback)
extern void AlignmentPatternFinder__ctor_m8CD8E694DBBD61D6BBD9E40DF10644BEA4B70741 (void);
// 0x00000234 ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.AlignmentPatternFinder::find()
extern void AlignmentPatternFinder_find_mC8C35A13562E9C40DB2367DBC2C3EF2B353B0FFC (void);
// 0x00000235 System.Nullable`1<System.Single> ZXing.QrCode.Internal.AlignmentPatternFinder::centerFromEnd(System.Int32[],System.Int32)
extern void AlignmentPatternFinder_centerFromEnd_mAB3FB3393A67FDCEAE1DFE0FFBDA5034236366DB (void);
// 0x00000236 System.Boolean ZXing.QrCode.Internal.AlignmentPatternFinder::foundPatternCross(System.Int32[])
extern void AlignmentPatternFinder_foundPatternCross_m0628F63F67F5FFE83A62D81DA0C69796E71E48F7 (void);
// 0x00000237 System.Nullable`1<System.Single> ZXing.QrCode.Internal.AlignmentPatternFinder::crossCheckVertical(System.Int32,System.Int32,System.Int32,System.Int32)
extern void AlignmentPatternFinder_crossCheckVertical_mA29FA61B6FC8C2238BBAB3B5F65560889F73256B (void);
// 0x00000238 ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.AlignmentPatternFinder::handlePossibleCenter(System.Int32[],System.Int32,System.Int32)
extern void AlignmentPatternFinder_handlePossibleCenter_m182FFCEC7642FDAED0135C9DC414AD23A5BB8FC6 (void);
// 0x00000239 System.Void ZXing.QrCode.Internal.Detector::.ctor(ZXing.Common.BitMatrix)
extern void Detector__ctor_m8E2D1BEAA7A021F5AC78FE188CA0A7072CDE1972 (void);
// 0x0000023A ZXing.Common.BitMatrix ZXing.QrCode.Internal.Detector::get_Image()
extern void Detector_get_Image_mAEB5ED2E17DC8BCFF962A37FB3E93344A8025E6C (void);
// 0x0000023B ZXing.ResultPointCallback ZXing.QrCode.Internal.Detector::get_ResultPointCallback()
extern void Detector_get_ResultPointCallback_mDEB29B0A5D00F575C8E9DF6B2AED795744E85CE1 (void);
// 0x0000023C ZXing.Common.DetectorResult ZXing.QrCode.Internal.Detector::detect()
extern void Detector_detect_m48373A9D7AEBA18012FEC37EE755F9216BD8FE9B (void);
// 0x0000023D ZXing.Common.DetectorResult ZXing.QrCode.Internal.Detector::detect(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Detector_detect_mF4B2B8E970BA235B2984116F56960F2A35E03214 (void);
// 0x0000023E ZXing.Common.DetectorResult ZXing.QrCode.Internal.Detector::processFinderPatternInfo(ZXing.QrCode.Internal.FinderPatternInfo)
extern void Detector_processFinderPatternInfo_mC2780BE3A2CE5A3514F31BBF780151252D07F9F0 (void);
// 0x0000023F ZXing.Common.PerspectiveTransform ZXing.QrCode.Internal.Detector::createTransform(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern void Detector_createTransform_m98985C87F283E6B9B8C529420BA2131348795277 (void);
// 0x00000240 ZXing.Common.BitMatrix ZXing.QrCode.Internal.Detector::sampleGrid(ZXing.Common.BitMatrix,ZXing.Common.PerspectiveTransform,System.Int32)
extern void Detector_sampleGrid_m65F3282647A51158C6F1B430D0DDA1478B78A1B6 (void);
// 0x00000241 System.Boolean ZXing.QrCode.Internal.Detector::computeDimension(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Single,System.Int32&)
extern void Detector_computeDimension_mFBECCCC18E55FDB4B648176F29F8CE9F2BD5BBE8 (void);
// 0x00000242 System.Single ZXing.QrCode.Internal.Detector::calculateModuleSize(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_calculateModuleSize_mCD9395157D1216CF81071C0AF137B7C056EDD522 (void);
// 0x00000243 System.Single ZXing.QrCode.Internal.Detector::calculateModuleSizeOneWay(ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_calculateModuleSizeOneWay_mAB8A5292B6D2F6BE02C3CFF67CDA73732B955905 (void);
// 0x00000244 System.Single ZXing.QrCode.Internal.Detector::sizeOfBlackWhiteBlackRunBothWays(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Detector_sizeOfBlackWhiteBlackRunBothWays_m2018D14F79014C77B491D1C649C9AB523B87CE95 (void);
// 0x00000245 System.Single ZXing.QrCode.Internal.Detector::sizeOfBlackWhiteBlackRun(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Detector_sizeOfBlackWhiteBlackRun_m8393D64FBFA03DDA266FE28D6786DA81F46A5120 (void);
// 0x00000246 ZXing.QrCode.Internal.AlignmentPattern ZXing.QrCode.Internal.Detector::findAlignmentInRegion(System.Single,System.Int32,System.Int32,System.Single)
extern void Detector_findAlignmentInRegion_m43D0A6C3B53CDB8FCAB8D19014CD54D224CB2156 (void);
// 0x00000247 System.Void ZXing.QrCode.Internal.FinderPattern::.ctor(System.Single,System.Single,System.Single)
extern void FinderPattern__ctor_m1CC6D872C8006BF44F64AC1BE2350849109CC2C2 (void);
// 0x00000248 System.Void ZXing.QrCode.Internal.FinderPattern::.ctor(System.Single,System.Single,System.Single,System.Int32)
extern void FinderPattern__ctor_m772EFBC6648743232AA622178CE9E4EF5757DA38 (void);
// 0x00000249 System.Single ZXing.QrCode.Internal.FinderPattern::get_EstimatedModuleSize()
extern void FinderPattern_get_EstimatedModuleSize_m1B0E82FB5A544C487124C7FA0EE786C312F98296 (void);
// 0x0000024A System.Int32 ZXing.QrCode.Internal.FinderPattern::get_Count()
extern void FinderPattern_get_Count_m3CACE523C00E436C3B064F4B93968FE059E631C4 (void);
// 0x0000024B System.Boolean ZXing.QrCode.Internal.FinderPattern::aboutEquals(System.Single,System.Single,System.Single)
extern void FinderPattern_aboutEquals_mDF31E9A495CCCFF59AD738EC081863442077CC38 (void);
// 0x0000024C ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPattern::combineEstimate(System.Single,System.Single,System.Single)
extern void FinderPattern_combineEstimate_m43F2BBD287E591DAE94643E74C36D3B182C57809 (void);
// 0x0000024D System.Void ZXing.QrCode.Internal.FinderPatternFinder::.ctor(ZXing.Common.BitMatrix)
extern void FinderPatternFinder__ctor_m527030167D37D090043073D26C1FE7D07A9E0CF7 (void);
// 0x0000024E System.Void ZXing.QrCode.Internal.FinderPatternFinder::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPointCallback)
extern void FinderPatternFinder__ctor_m5DD3CF355405AA15A0A247D7CBF7477E018C9FFF (void);
// 0x0000024F ZXing.Common.BitMatrix ZXing.QrCode.Internal.FinderPatternFinder::get_Image()
extern void FinderPatternFinder_get_Image_mD42C2619D374FA00B93178930BCF7038E4B7FD74 (void);
// 0x00000250 System.Collections.Generic.List`1<ZXing.QrCode.Internal.FinderPattern> ZXing.QrCode.Internal.FinderPatternFinder::get_PossibleCenters()
extern void FinderPatternFinder_get_PossibleCenters_m966A7A1B015D250A168B101543ED66B54BF728B5 (void);
// 0x00000251 ZXing.QrCode.Internal.FinderPatternInfo ZXing.QrCode.Internal.FinderPatternFinder::find(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void FinderPatternFinder_find_mE06B229CC5034B374536A100FDB13B03C044595A (void);
// 0x00000252 System.Nullable`1<System.Single> ZXing.QrCode.Internal.FinderPatternFinder::centerFromEnd(System.Int32[],System.Int32)
extern void FinderPatternFinder_centerFromEnd_mDA32205C1A160D4A35801ADAE541FC342285A08B (void);
// 0x00000253 System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::foundPatternCross(System.Int32[])
extern void FinderPatternFinder_foundPatternCross_mB3A8133B9E721CFA7A338ECE0D8C0152EFD6D537 (void);
// 0x00000254 System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::foundPatternDiagonal(System.Int32[])
extern void FinderPatternFinder_foundPatternDiagonal_m27AC1C6DD1655BB29D1DFAFA085EB1C52C488D5A (void);
// 0x00000255 System.Int32[] ZXing.QrCode.Internal.FinderPatternFinder::get_CrossCheckStateCount()
extern void FinderPatternFinder_get_CrossCheckStateCount_mB873931F7B0F45CDE0FFB328FC0C5447918B2DB8 (void);
// 0x00000256 System.Void ZXing.QrCode.Internal.FinderPatternFinder::clearCounts(System.Int32[])
extern void FinderPatternFinder_clearCounts_m3AFC88F8CFE32C39CE9F29AACA67E5DB40CF9EDC (void);
// 0x00000257 System.Void ZXing.QrCode.Internal.FinderPatternFinder::shiftCounts2(System.Int32[])
extern void FinderPatternFinder_shiftCounts2_mD3FBE67518360AE63932A0A5DD0ADE30F784D078 (void);
// 0x00000258 System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::crossCheckDiagonal(System.Int32,System.Int32)
extern void FinderPatternFinder_crossCheckDiagonal_m3DA4A839D4D19B6640BFBCE2DB4855D8FF35F5A6 (void);
// 0x00000259 System.Nullable`1<System.Single> ZXing.QrCode.Internal.FinderPatternFinder::crossCheckVertical(System.Int32,System.Int32,System.Int32,System.Int32)
extern void FinderPatternFinder_crossCheckVertical_mE14E0C18420DC270342C824845A17CB55D77E7E4 (void);
// 0x0000025A System.Nullable`1<System.Single> ZXing.QrCode.Internal.FinderPatternFinder::crossCheckHorizontal(System.Int32,System.Int32,System.Int32,System.Int32)
extern void FinderPatternFinder_crossCheckHorizontal_mE8A73C3349AFEAD0FE46B0D56B0E7100CCD17157 (void);
// 0x0000025B System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::handlePossibleCenter(System.Int32[],System.Int32,System.Int32,System.Boolean)
extern void FinderPatternFinder_handlePossibleCenter_m0AFF4756351D688FFFBC1E9F9E121D235DC4D313 (void);
// 0x0000025C System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::handlePossibleCenter(System.Int32[],System.Int32,System.Int32)
extern void FinderPatternFinder_handlePossibleCenter_mFB68686FC8F78FF30E7860004B05925B5C422172 (void);
// 0x0000025D System.Int32 ZXing.QrCode.Internal.FinderPatternFinder::findRowSkip()
extern void FinderPatternFinder_findRowSkip_m1DF9BDB418139E949BC8F00E5541F900A0B2616A (void);
// 0x0000025E System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::haveMultiplyConfirmedCenters()
extern void FinderPatternFinder_haveMultiplyConfirmedCenters_m553C3186A4248066A58A46D4F3F44E3082BF4325 (void);
// 0x0000025F System.Double ZXing.QrCode.Internal.FinderPatternFinder::squaredDistance(ZXing.QrCode.Internal.FinderPattern,ZXing.QrCode.Internal.FinderPattern)
extern void FinderPatternFinder_squaredDistance_m3C8EA70986326F3477E40CAAB604AB41A13F0957 (void);
// 0x00000260 ZXing.QrCode.Internal.FinderPattern[] ZXing.QrCode.Internal.FinderPatternFinder::selectBestPatterns()
extern void FinderPatternFinder_selectBestPatterns_m5089DD3AB6B63A94B0B33AABC3B656134A5BB54C (void);
// 0x00000261 System.Void ZXing.QrCode.Internal.FinderPatternFinder::.cctor()
extern void FinderPatternFinder__cctor_m278B05EE253660E72B1036B7613C3F4AD9D9A85D (void);
// 0x00000262 System.Int32 ZXing.QrCode.Internal.FinderPatternFinder/EstimatedModuleComparator::Compare(ZXing.QrCode.Internal.FinderPattern,ZXing.QrCode.Internal.FinderPattern)
extern void EstimatedModuleComparator_Compare_m88757B47B409641AB1B40044699EC0B954EDF610 (void);
// 0x00000263 System.Void ZXing.QrCode.Internal.FinderPatternFinder/EstimatedModuleComparator::.ctor()
extern void EstimatedModuleComparator__ctor_m937FB610C02C84DAE4727ED5C9BEDBFDE7762BE9 (void);
// 0x00000264 System.Void ZXing.QrCode.Internal.FinderPatternInfo::.ctor(ZXing.QrCode.Internal.FinderPattern[])
extern void FinderPatternInfo__ctor_mBA50C87571AD4D6F9E2C710C96D80F91D4EAF7E8 (void);
// 0x00000265 ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::get_BottomLeft()
extern void FinderPatternInfo_get_BottomLeft_mCEBF02A4BBD4834A6BC9D36B6E93AFCF23186833 (void);
// 0x00000266 ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::get_TopLeft()
extern void FinderPatternInfo_get_TopLeft_mD5214011F1E4736720CE9C4F1D1DAA79E3239B2B (void);
// 0x00000267 ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::get_TopRight()
extern void FinderPatternInfo_get_TopRight_m62A70525270F592E71086352F83903C3EE7F8D64 (void);
// 0x00000268 System.Void ZXing.QrCode.Internal.BlockPair::.ctor(System.Byte[],System.Byte[])
extern void BlockPair__ctor_m93FF79F4EDFA331594D3412886A6F2B334438999 (void);
// 0x00000269 System.Byte[] ZXing.QrCode.Internal.BlockPair::get_DataBytes()
extern void BlockPair_get_DataBytes_mD344E280745399520B450B2BBD3F57FEDA15FBDB (void);
// 0x0000026A System.Byte[] ZXing.QrCode.Internal.BlockPair::get_ErrorCorrectionBytes()
extern void BlockPair_get_ErrorCorrectionBytes_m7DC12B63785AACA63D6FC92FF85E21B685EFA9E9 (void);
// 0x0000026B System.Void ZXing.QrCode.Internal.ByteMatrix::.ctor(System.Int32,System.Int32)
extern void ByteMatrix__ctor_m5BF5D0D2EF8A1CFE1F8D2CDF0B5027450DFBDA86 (void);
// 0x0000026C System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Height()
extern void ByteMatrix_get_Height_m0BBB2D4EE0A0E8AA14FBF6B96BC56DB623A99B87 (void);
// 0x0000026D System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Width()
extern void ByteMatrix_get_Width_m3A2778DEC4FC41F240C99517D777011D30C829D2 (void);
// 0x0000026E System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Item(System.Int32,System.Int32)
extern void ByteMatrix_get_Item_m52DDC8DFBA6DF2647F55C8F0A9454C4D8914C5D0 (void);
// 0x0000026F System.Void ZXing.QrCode.Internal.ByteMatrix::set_Item(System.Int32,System.Int32,System.Int32)
extern void ByteMatrix_set_Item_m0E0BCEAC4814389F016510426A994611EFD21E86 (void);
// 0x00000270 System.Byte[][] ZXing.QrCode.Internal.ByteMatrix::get_Array()
extern void ByteMatrix_get_Array_m4E75E3F844C818CD59B197D7D437C2A13764226A (void);
// 0x00000271 System.Void ZXing.QrCode.Internal.ByteMatrix::set(System.Int32,System.Int32,System.Byte)
extern void ByteMatrix_set_mA012E64ADBD2CABF526DD51EA03AB265B5180320 (void);
// 0x00000272 System.Void ZXing.QrCode.Internal.ByteMatrix::set(System.Int32,System.Int32,System.Boolean)
extern void ByteMatrix_set_m325F76586AC96AC087720F79435805CB950DF95E (void);
// 0x00000273 System.Void ZXing.QrCode.Internal.ByteMatrix::clear(System.Byte)
extern void ByteMatrix_clear_m3CA557DCE9875F9831794BB83A0306E6BFD4CA3A (void);
// 0x00000274 System.String ZXing.QrCode.Internal.ByteMatrix::ToString()
extern void ByteMatrix_ToString_mAF14DC1D72F44ABE6426066E837F596E069D654E (void);
// 0x00000275 System.Int32 ZXing.QrCode.Internal.Encoder::calculateMaskPenalty(ZXing.QrCode.Internal.ByteMatrix)
extern void Encoder_calculateMaskPenalty_m58C32A9DA15698F26C62B50D8BCA663C3F05AD61 (void);
// 0x00000276 ZXing.QrCode.Internal.QRCode ZXing.QrCode.Internal.Encoder::encode(System.String,ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void Encoder_encode_m16DCE8022FD5465578A8FC9CC5AA4A5DA7E28806 (void);
// 0x00000277 ZXing.QrCode.Internal.QRCode ZXing.QrCode.Internal.Encoder::encode(System.String,ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void Encoder_encode_m2EDC052563393CA571307B755ED422FAE19458F9 (void);
// 0x00000278 ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Encoder::recommendVersion(ZXing.QrCode.Internal.ErrorCorrectionLevel,ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray,ZXing.Common.BitArray)
extern void Encoder_recommendVersion_m65FD0A8C726F31CC55606174B9D832826C90AF79 (void);
// 0x00000279 System.Int32 ZXing.QrCode.Internal.Encoder::calculateBitsNeeded(ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray,ZXing.Common.BitArray,ZXing.QrCode.Internal.Version)
extern void Encoder_calculateBitsNeeded_m8DE3C9254970D62C185CCF804318DBD8362910D4 (void);
// 0x0000027A System.Int32 ZXing.QrCode.Internal.Encoder::getAlphanumericCode(System.Int32)
extern void Encoder_getAlphanumericCode_m29EEF61669B24D1B35B0F218792BB8E77EDF0471 (void);
// 0x0000027B ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Encoder::chooseMode(System.String)
extern void Encoder_chooseMode_mC5F801AB2E2C001A5AA994BC180432A699747830 (void);
// 0x0000027C ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Encoder::chooseMode(System.String,System.String)
extern void Encoder_chooseMode_m91157097AE47F60FB5E3B29251EF661AAC46216F (void);
// 0x0000027D System.Boolean ZXing.QrCode.Internal.Encoder::isOnlyDoubleByteKanji(System.String)
extern void Encoder_isOnlyDoubleByteKanji_mD93E6DC8D3450410B178B955887E83FCC32BDA3B (void);
// 0x0000027E System.Int32 ZXing.QrCode.Internal.Encoder::chooseMaskPattern(ZXing.Common.BitArray,ZXing.QrCode.Internal.ErrorCorrectionLevel,ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern void Encoder_chooseMaskPattern_m6CA486445C0C8302F4A08B006C10C72F893C8870 (void);
// 0x0000027F ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Encoder::chooseVersion(System.Int32,ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void Encoder_chooseVersion_m93EE361D43E1B381DAE74A28BECE13856CBA9ECD (void);
// 0x00000280 System.Boolean ZXing.QrCode.Internal.Encoder::willFit(System.Int32,ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void Encoder_willFit_m0DC3977B915D3DD1756BD77294A98CD3EA2E7F9A (void);
// 0x00000281 System.Void ZXing.QrCode.Internal.Encoder::terminateBits(System.Int32,ZXing.Common.BitArray)
extern void Encoder_terminateBits_m1C287F51019811A61E12F472C6DD197B03C5FB44 (void);
// 0x00000282 System.Void ZXing.QrCode.Internal.Encoder::getNumDataBytesAndNumECBytesForBlockID(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[])
extern void Encoder_getNumDataBytesAndNumECBytesForBlockID_m9FDEE191A5220B5BAD61F7A8BA773C830218CBBD (void);
// 0x00000283 ZXing.Common.BitArray ZXing.QrCode.Internal.Encoder::interleaveWithECBytes(ZXing.Common.BitArray,System.Int32,System.Int32,System.Int32)
extern void Encoder_interleaveWithECBytes_mEC4BCEE18D1297AE876773A1A618B03105ED0D30 (void);
// 0x00000284 System.Byte[] ZXing.QrCode.Internal.Encoder::generateECBytes(System.Byte[],System.Int32)
extern void Encoder_generateECBytes_mE87BE80F78CAB53859068946BBFE636653AA1E4C (void);
// 0x00000285 System.Void ZXing.QrCode.Internal.Encoder::appendModeInfo(ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray)
extern void Encoder_appendModeInfo_m94BA3417A7294041D8D86C49D23E4132C15A7418 (void);
// 0x00000286 System.Void ZXing.QrCode.Internal.Encoder::appendLengthInfo(System.Int32,ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray)
extern void Encoder_appendLengthInfo_mEF6D1EAD7D6E485CEAA493B9524A48B395BF3690 (void);
// 0x00000287 System.Void ZXing.QrCode.Internal.Encoder::appendBytes(System.String,ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray,System.String)
extern void Encoder_appendBytes_mDF8D49360EB44961B534720C246E103C96C9C56A (void);
// 0x00000288 System.Void ZXing.QrCode.Internal.Encoder::appendNumericBytes(System.String,ZXing.Common.BitArray)
extern void Encoder_appendNumericBytes_m15BDE8CBE3332A462ACB50E63477748CB250F7B3 (void);
// 0x00000289 System.Void ZXing.QrCode.Internal.Encoder::appendAlphanumericBytes(System.String,ZXing.Common.BitArray)
extern void Encoder_appendAlphanumericBytes_m2DD5F75AEF17F0439915B31B9C632C237D310DAE (void);
// 0x0000028A System.Void ZXing.QrCode.Internal.Encoder::append8BitBytes(System.String,ZXing.Common.BitArray,System.String)
extern void Encoder_append8BitBytes_mA8F7621BA16AABEF89F9EC649C3719851E8734A0 (void);
// 0x0000028B System.Void ZXing.QrCode.Internal.Encoder::appendKanjiBytes(System.String,ZXing.Common.BitArray)
extern void Encoder_appendKanjiBytes_m21F62C25EEAC689F69484A1AB50A7325D7205BD8 (void);
// 0x0000028C System.Void ZXing.QrCode.Internal.Encoder::appendECI(ZXing.Common.CharacterSetECI,ZXing.Common.BitArray)
extern void Encoder_appendECI_m703055417EF7905A3C2330D0756DFE141AFAB1F2 (void);
// 0x0000028D System.Void ZXing.QrCode.Internal.Encoder::.cctor()
extern void Encoder__cctor_m34AA7E628D44AE462CD56E872CF91B9E7521A685 (void);
// 0x0000028E System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule1(ZXing.QrCode.Internal.ByteMatrix)
extern void MaskUtil_applyMaskPenaltyRule1_m118D7DEFE94A221A27CC0A1A59021E7C8FA957BE (void);
// 0x0000028F System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule2(ZXing.QrCode.Internal.ByteMatrix)
extern void MaskUtil_applyMaskPenaltyRule2_m54CCDA0A12F79111CA3F583D44F3A661D2F7CB25 (void);
// 0x00000290 System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule3(ZXing.QrCode.Internal.ByteMatrix)
extern void MaskUtil_applyMaskPenaltyRule3_m729EBC01D28C8E3C4F0764D4847775B6AFD2ADD0 (void);
// 0x00000291 System.Boolean ZXing.QrCode.Internal.MaskUtil::isWhiteHorizontal(System.Byte[],System.Int32,System.Int32)
extern void MaskUtil_isWhiteHorizontal_m1D2DD3BEB589D6FFDD8C90876A3B636A822BB0E1 (void);
// 0x00000292 System.Boolean ZXing.QrCode.Internal.MaskUtil::isWhiteVertical(System.Byte[][],System.Int32,System.Int32,System.Int32)
extern void MaskUtil_isWhiteVertical_mD6DCCE743C307E52BEBA598A27F03272AC5C7F11 (void);
// 0x00000293 System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule4(ZXing.QrCode.Internal.ByteMatrix)
extern void MaskUtil_applyMaskPenaltyRule4_mCB592A6461315D9BC4E767D5233DC7148DB7C7D4 (void);
// 0x00000294 System.Boolean ZXing.QrCode.Internal.MaskUtil::getDataMaskBit(System.Int32,System.Int32,System.Int32)
extern void MaskUtil_getDataMaskBit_m4308305BE0795ECBD5250F1EF95341CA79DC8DA7 (void);
// 0x00000295 System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule1Internal(ZXing.QrCode.Internal.ByteMatrix,System.Boolean)
extern void MaskUtil_applyMaskPenaltyRule1Internal_m5B1782066035E7CD2C533034AA9D5C307B320691 (void);
// 0x00000296 System.Void ZXing.QrCode.Internal.MatrixUtil::clearMatrix(ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_clearMatrix_m76203C366F022DA0E9D483F6AE16D1DE11694326 (void);
// 0x00000297 System.Void ZXing.QrCode.Internal.MatrixUtil::buildMatrix(ZXing.Common.BitArray,ZXing.QrCode.Internal.ErrorCorrectionLevel,ZXing.QrCode.Internal.Version,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_buildMatrix_m230915868BF31910745EC7ED79C0D719B349639F (void);
// 0x00000298 System.Void ZXing.QrCode.Internal.MatrixUtil::embedBasicPatterns(ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedBasicPatterns_m1C5195D5DB505BE01BF58CEE8BDFDCFE5CD3464D (void);
// 0x00000299 System.Void ZXing.QrCode.Internal.MatrixUtil::embedTypeInfo(ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedTypeInfo_mE7455DA7ED8C599AD7EF65F3AAFE0065824E7AB0 (void);
// 0x0000029A System.Void ZXing.QrCode.Internal.MatrixUtil::maybeEmbedVersionInfo(ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_maybeEmbedVersionInfo_mE95AF355A6514EA4685461DBBC0AD96AC3909833 (void);
// 0x0000029B System.Void ZXing.QrCode.Internal.MatrixUtil::embedDataBits(ZXing.Common.BitArray,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedDataBits_m9945A435818BB5DB8636063CF48001E413E32FA3 (void);
// 0x0000029C System.Int32 ZXing.QrCode.Internal.MatrixUtil::findMSBSet(System.Int32)
extern void MatrixUtil_findMSBSet_mFAC1CC728C031E91247A4D99B3EE9E1C281F60E6 (void);
// 0x0000029D System.Int32 ZXing.QrCode.Internal.MatrixUtil::calculateBCHCode(System.Int32,System.Int32)
extern void MatrixUtil_calculateBCHCode_mB950ADB64DF8D3C251E27637107D9786A5340501 (void);
// 0x0000029E System.Void ZXing.QrCode.Internal.MatrixUtil::makeTypeInfoBits(ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Int32,ZXing.Common.BitArray)
extern void MatrixUtil_makeTypeInfoBits_m39D1F82B61A659DBDD88B0396C01F06FDC52A37B (void);
// 0x0000029F System.Void ZXing.QrCode.Internal.MatrixUtil::makeVersionInfoBits(ZXing.QrCode.Internal.Version,ZXing.Common.BitArray)
extern void MatrixUtil_makeVersionInfoBits_m5D50719A867B255C04F1567E51D6822C87908F52 (void);
// 0x000002A0 System.Boolean ZXing.QrCode.Internal.MatrixUtil::isEmpty(System.Int32)
extern void MatrixUtil_isEmpty_m792DE59DE23F8F1DFDAA023424FA6E16D1714024 (void);
// 0x000002A1 System.Void ZXing.QrCode.Internal.MatrixUtil::embedTimingPatterns(ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedTimingPatterns_m931C0712DC8B3DB50EED4CF99D058A830F67F493 (void);
// 0x000002A2 System.Void ZXing.QrCode.Internal.MatrixUtil::embedDarkDotAtLeftBottomCorner(ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedDarkDotAtLeftBottomCorner_m8FE6D90E7284544A23599D6895F2F6A0538E9279 (void);
// 0x000002A3 System.Void ZXing.QrCode.Internal.MatrixUtil::embedHorizontalSeparationPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedHorizontalSeparationPattern_m3EF28A03F5048E44A6FDC47A7C3CC1C2A4B59C5D (void);
// 0x000002A4 System.Void ZXing.QrCode.Internal.MatrixUtil::embedVerticalSeparationPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedVerticalSeparationPattern_mD04D28A9463F4B93B798A277509766476514D971 (void);
// 0x000002A5 System.Void ZXing.QrCode.Internal.MatrixUtil::embedPositionAdjustmentPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedPositionAdjustmentPattern_m363FD8D4D54E2717EE23A71AE775797AA4D73B3B (void);
// 0x000002A6 System.Void ZXing.QrCode.Internal.MatrixUtil::embedPositionDetectionPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedPositionDetectionPattern_m65488C11A6D2955CB8E9946212D409E8574FB773 (void);
// 0x000002A7 System.Void ZXing.QrCode.Internal.MatrixUtil::embedPositionDetectionPatternsAndSeparators(ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedPositionDetectionPatternsAndSeparators_m293B0210618A820E038D6ED52D62DED5F4E9A580 (void);
// 0x000002A8 System.Void ZXing.QrCode.Internal.MatrixUtil::maybeEmbedPositionAdjustmentPatterns(ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_maybeEmbedPositionAdjustmentPatterns_mAC137467DDFB07FAC342C7FA85FC8E64600E2FB1 (void);
// 0x000002A9 System.Void ZXing.QrCode.Internal.MatrixUtil::.cctor()
extern void MatrixUtil__cctor_m581EDA972D692161E5AFEAE9EDB8A1F91C896885 (void);
// 0x000002AA System.Void ZXing.QrCode.Internal.QRCode::.ctor()
extern void QRCode__ctor_mFD9833E91FDC7E5124792F72EFD522284FCDDA7E (void);
// 0x000002AB ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.QRCode::get_Mode()
extern void QRCode_get_Mode_mDFD69D6029CB9FBDD24B2317856B72748262D2D4 (void);
// 0x000002AC System.Void ZXing.QrCode.Internal.QRCode::set_Mode(ZXing.QrCode.Internal.Mode)
extern void QRCode_set_Mode_m13F333C6F87BAC2B46499A5445A2E7B2B99AD33F (void);
// 0x000002AD ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.QRCode::get_ECLevel()
extern void QRCode_get_ECLevel_m4275719EB90C16F5AAC16AABDEDD703C1CD12808 (void);
// 0x000002AE System.Void ZXing.QrCode.Internal.QRCode::set_ECLevel(ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void QRCode_set_ECLevel_mE437CC64DFDF14A07449558EDC6475323B675DA1 (void);
// 0x000002AF ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.QRCode::get_Version()
extern void QRCode_get_Version_m0657005C1B9D0A0EA2031B7EEDA53F6A00881613 (void);
// 0x000002B0 System.Void ZXing.QrCode.Internal.QRCode::set_Version(ZXing.QrCode.Internal.Version)
extern void QRCode_set_Version_m8A7BCE20A6E637FD7BBA585224A7610F7785436B (void);
// 0x000002B1 System.Int32 ZXing.QrCode.Internal.QRCode::get_MaskPattern()
extern void QRCode_get_MaskPattern_m7BEB1378FAC83BB501BEAF3395164171E912BE0F (void);
// 0x000002B2 System.Void ZXing.QrCode.Internal.QRCode::set_MaskPattern(System.Int32)
extern void QRCode_set_MaskPattern_mFDA72D45D1A371AE16AC14079C920A176DFABA41 (void);
// 0x000002B3 ZXing.QrCode.Internal.ByteMatrix ZXing.QrCode.Internal.QRCode::get_Matrix()
extern void QRCode_get_Matrix_mAC1C4484FC449938B1B7565350BAD8CBB7D659E8 (void);
// 0x000002B4 System.Void ZXing.QrCode.Internal.QRCode::set_Matrix(ZXing.QrCode.Internal.ByteMatrix)
extern void QRCode_set_Matrix_m713783A882672E34A41040C552C82CEA73835D03 (void);
// 0x000002B5 System.String ZXing.QrCode.Internal.QRCode::ToString()
extern void QRCode_ToString_m3889DA28EDDC6B5B70A05B99CE69280127146D51 (void);
// 0x000002B6 System.Boolean ZXing.QrCode.Internal.QRCode::isValidMaskPattern(System.Int32)
extern void QRCode_isValidMaskPattern_m16439B31E554F87DE9CC1B4AB3FA11B734A2218E (void);
// 0x000002B7 System.Void ZXing.QrCode.Internal.QRCode::.cctor()
extern void QRCode__cctor_mA0DF61B3743A4B5F3A1FF26BF318C2B727E74B68 (void);
// 0x000002B8 System.Boolean ZXing.PDF417.PDF417EncodingOptions::get_Compact()
extern void PDF417EncodingOptions_get_Compact_m3E1CE4A5842692CC0FA25775032D5CF18A5CD03C (void);
// 0x000002B9 System.Void ZXing.PDF417.PDF417EncodingOptions::set_Compact(System.Boolean)
extern void PDF417EncodingOptions_set_Compact_m555FB76EED76DEE43CC740D0BF76610AA173F597 (void);
// 0x000002BA ZXing.PDF417.Internal.Compaction ZXing.PDF417.PDF417EncodingOptions::get_Compaction()
extern void PDF417EncodingOptions_get_Compaction_mE34020C07FE7AD9F7EE3A1D87137E387AE392207 (void);
// 0x000002BB System.Void ZXing.PDF417.PDF417EncodingOptions::set_Compaction(ZXing.PDF417.Internal.Compaction)
extern void PDF417EncodingOptions_set_Compaction_mABA2A2ACEC9E9F14979A2A02A99E38164E521D0C (void);
// 0x000002BC ZXing.PDF417.Internal.Dimensions ZXing.PDF417.PDF417EncodingOptions::get_Dimensions()
extern void PDF417EncodingOptions_get_Dimensions_m306F453AC0CF7706B6C7C776AC349CF7D738B7B0 (void);
// 0x000002BD System.Void ZXing.PDF417.PDF417EncodingOptions::set_Dimensions(ZXing.PDF417.Internal.Dimensions)
extern void PDF417EncodingOptions_set_Dimensions_m92B936D730940D4AF2AF0D43667FA1B9F01C4EB5 (void);
// 0x000002BE ZXing.PDF417.Internal.PDF417ErrorCorrectionLevel ZXing.PDF417.PDF417EncodingOptions::get_ErrorCorrection()
extern void PDF417EncodingOptions_get_ErrorCorrection_m9AD796E82C172D7939B1284CE269C08BF83DCDAB (void);
// 0x000002BF System.Void ZXing.PDF417.PDF417EncodingOptions::set_ErrorCorrection(ZXing.PDF417.Internal.PDF417ErrorCorrectionLevel)
extern void PDF417EncodingOptions_set_ErrorCorrection_m3745A9A6B8F51703BFEBB13D56D9C92817CAFCBD (void);
// 0x000002C0 ZXing.PDF417.Internal.PDF417AspectRatio ZXing.PDF417.PDF417EncodingOptions::get_AspectRatio()
extern void PDF417EncodingOptions_get_AspectRatio_m1066D23D9D817921D3F4DDAD9B2AFFB5972B6512 (void);
// 0x000002C1 System.Void ZXing.PDF417.PDF417EncodingOptions::set_AspectRatio(ZXing.PDF417.Internal.PDF417AspectRatio)
extern void PDF417EncodingOptions_set_AspectRatio_m2776D36E056F64A76AE3E0538B11E44F21308AC7 (void);
// 0x000002C2 System.Single ZXing.PDF417.PDF417EncodingOptions::get_ImageAspectRatio()
extern void PDF417EncodingOptions_get_ImageAspectRatio_mAE08E3B79D3BD95BBE7B99A4F62D1FF450B14405 (void);
// 0x000002C3 System.Void ZXing.PDF417.PDF417EncodingOptions::set_ImageAspectRatio(System.Single)
extern void PDF417EncodingOptions_set_ImageAspectRatio_m461E475218DDDE3185674546675B85FA2396BBB1 (void);
// 0x000002C4 System.String ZXing.PDF417.PDF417EncodingOptions::get_CharacterSet()
extern void PDF417EncodingOptions_get_CharacterSet_mAFE31CE2C2A40A295678CB143CBDE3403F3CFA1E (void);
// 0x000002C5 System.Void ZXing.PDF417.PDF417EncodingOptions::set_CharacterSet(System.String)
extern void PDF417EncodingOptions_set_CharacterSet_m2EBD90AD6D1EA5C6F448DEFA56DA3FD032AE7587 (void);
// 0x000002C6 System.Boolean ZXing.PDF417.PDF417EncodingOptions::get_DisableECI()
extern void PDF417EncodingOptions_get_DisableECI_m478ED4020C6B05AF8B2CE700256CAEF900820623 (void);
// 0x000002C7 System.Void ZXing.PDF417.PDF417EncodingOptions::set_DisableECI(System.Boolean)
extern void PDF417EncodingOptions_set_DisableECI_mCD4184BA964A45651D478CA9F4B9975D1122E89B (void);
// 0x000002C8 System.Void ZXing.PDF417.PDF417EncodingOptions::.ctor()
extern void PDF417EncodingOptions__ctor_m1B361DD4C051CBB4D181116098B87F07DBB530FC (void);
// 0x000002C9 System.Int32 ZXing.PDF417.PDF417Common::getBitCountSum(System.Int32[])
extern void PDF417Common_getBitCountSum_m952005D503D7704115E58E3258562D8F4844406C (void);
// 0x000002CA System.Int32[] ZXing.PDF417.PDF417Common::toIntArray(System.Collections.Generic.ICollection`1<System.Int32>)
extern void PDF417Common_toIntArray_m3A1F6C8BBB2FC0013A35B4347C06A9FBEF25655E (void);
// 0x000002CB System.Int32 ZXing.PDF417.PDF417Common::getCodeword(System.Int64)
extern void PDF417Common_getCodeword_m26B9A4512313D82703ECC8F6563079350EABCB5E (void);
// 0x000002CC System.Void ZXing.PDF417.PDF417Common::.cctor()
extern void PDF417Common__cctor_mAF815F45BDC5B572DF7CCF20777AADA427910299 (void);
// 0x000002CD ZXing.Result ZXing.PDF417.PDF417Reader::decode(ZXing.BinaryBitmap)
extern void PDF417Reader_decode_m990942AB905A37F97D27134FA85CB9DDFF7AE5C6 (void);
// 0x000002CE ZXing.Result ZXing.PDF417.PDF417Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void PDF417Reader_decode_m2778DD0C14B24179B28A8C1B97E200447FB0FA58 (void);
// 0x000002CF ZXing.Result[] ZXing.PDF417.PDF417Reader::decodeMultiple(ZXing.BinaryBitmap)
extern void PDF417Reader_decodeMultiple_m034FBB434D29F6D1D022845A846DC191F4F4AF97 (void);
// 0x000002D0 ZXing.Result[] ZXing.PDF417.PDF417Reader::decodeMultiple(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void PDF417Reader_decodeMultiple_m1E2259E50B8AEEB57EC8CC399E7CD43410B1554C (void);
// 0x000002D1 ZXing.Result[] ZXing.PDF417.PDF417Reader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,System.Boolean)
extern void PDF417Reader_decode_m94F3916D2005FD7F6C57494E3E16260C0129EDC3 (void);
// 0x000002D2 System.Int32 ZXing.PDF417.PDF417Reader::getMaxWidth(ZXing.ResultPoint,ZXing.ResultPoint)
extern void PDF417Reader_getMaxWidth_mD09F058726B3626BF8434E518AAF8588F82A629C (void);
// 0x000002D3 System.Int32 ZXing.PDF417.PDF417Reader::getMinWidth(ZXing.ResultPoint,ZXing.ResultPoint)
extern void PDF417Reader_getMinWidth_m2F2D24938ECAC197E24B35DB6BBB39394B25CC43 (void);
// 0x000002D4 System.Int32 ZXing.PDF417.PDF417Reader::getMaxCodewordWidth(ZXing.ResultPoint[])
extern void PDF417Reader_getMaxCodewordWidth_mA8F3D4A2ED2E02DB5663CE2EFD39D2A63D7C8A6E (void);
// 0x000002D5 System.Int32 ZXing.PDF417.PDF417Reader::getMinCodewordWidth(ZXing.ResultPoint[])
extern void PDF417Reader_getMinCodewordWidth_m764C5F80539CE6984597C17A94F60DA913A86989 (void);
// 0x000002D6 System.Void ZXing.PDF417.PDF417Reader::reset()
extern void PDF417Reader_reset_mEEC77569470E6B3BE824CEAC172C993038FEA2BE (void);
// 0x000002D7 System.Void ZXing.PDF417.PDF417Reader::.ctor()
extern void PDF417Reader__ctor_m596E6F0533DA264F1B6B577A3F80800DC581C16D (void);
// 0x000002D8 System.Int32 ZXing.PDF417.PDF417ResultMetadata::get_SegmentIndex()
extern void PDF417ResultMetadata_get_SegmentIndex_m06EA835E15417B33292F13B373E8A95E7C10A8BA (void);
// 0x000002D9 System.Void ZXing.PDF417.PDF417ResultMetadata::set_SegmentIndex(System.Int32)
extern void PDF417ResultMetadata_set_SegmentIndex_m5A6032E9D639F0976FDFAE8048440D82ED4F876E (void);
// 0x000002DA System.String ZXing.PDF417.PDF417ResultMetadata::get_FileId()
extern void PDF417ResultMetadata_get_FileId_m15E585971DD4CE7DA67463E817D755611903930A (void);
// 0x000002DB System.Void ZXing.PDF417.PDF417ResultMetadata::set_FileId(System.String)
extern void PDF417ResultMetadata_set_FileId_mBB175B15EBF9886CF49BC6B1E52DB79B962BD695 (void);
// 0x000002DC System.Int32[] ZXing.PDF417.PDF417ResultMetadata::get_OptionalData()
extern void PDF417ResultMetadata_get_OptionalData_mBC406EF8FF0FED08E3044C4584D83EBEBD8B14BA (void);
// 0x000002DD System.Void ZXing.PDF417.PDF417ResultMetadata::set_OptionalData(System.Int32[])
extern void PDF417ResultMetadata_set_OptionalData_mB0277715FBB425B74EB74230BB7C835FDB26386A (void);
// 0x000002DE System.Boolean ZXing.PDF417.PDF417ResultMetadata::get_IsLastSegment()
extern void PDF417ResultMetadata_get_IsLastSegment_mE769D207C480F95469DE0A82A0B7567F60676141 (void);
// 0x000002DF System.Void ZXing.PDF417.PDF417ResultMetadata::set_IsLastSegment(System.Boolean)
extern void PDF417ResultMetadata_set_IsLastSegment_m3F58AF03654B2D319784BE686D32D84ECDEFFF79 (void);
// 0x000002E0 System.Int32 ZXing.PDF417.PDF417ResultMetadata::get_SegmentCount()
extern void PDF417ResultMetadata_get_SegmentCount_mA66CFCA0A8ED5DBF8F0C28A97D15431392F9F4E7 (void);
// 0x000002E1 System.Void ZXing.PDF417.PDF417ResultMetadata::set_SegmentCount(System.Int32)
extern void PDF417ResultMetadata_set_SegmentCount_m78FA914FA3FFA5E4EBB20BAF3A96F537D232EB00 (void);
// 0x000002E2 System.String ZXing.PDF417.PDF417ResultMetadata::get_Sender()
extern void PDF417ResultMetadata_get_Sender_mF97741B8219A424A95C974FA89D51C998A2FBE46 (void);
// 0x000002E3 System.Void ZXing.PDF417.PDF417ResultMetadata::set_Sender(System.String)
extern void PDF417ResultMetadata_set_Sender_mC09700E8431758B0E9E7DD4D4FE6EEC763B18FE3 (void);
// 0x000002E4 System.String ZXing.PDF417.PDF417ResultMetadata::get_Addressee()
extern void PDF417ResultMetadata_get_Addressee_m122257853479B599ADE3E9558FD167235D1590B7 (void);
// 0x000002E5 System.Void ZXing.PDF417.PDF417ResultMetadata::set_Addressee(System.String)
extern void PDF417ResultMetadata_set_Addressee_mED0799341CAB6E149D624117AA37C0FB3508F2D6 (void);
// 0x000002E6 System.String ZXing.PDF417.PDF417ResultMetadata::get_FileName()
extern void PDF417ResultMetadata_get_FileName_m7FB66E88FFF62DA92B989B96016EEA1CBEC43A22 (void);
// 0x000002E7 System.Void ZXing.PDF417.PDF417ResultMetadata::set_FileName(System.String)
extern void PDF417ResultMetadata_set_FileName_mB33ECE8A929AA992D61F7F3C9F5CE8E44A1F1DE2 (void);
// 0x000002E8 System.Int64 ZXing.PDF417.PDF417ResultMetadata::get_FileSize()
extern void PDF417ResultMetadata_get_FileSize_m6C605A98FF3AC2D87B3AC43D739622A95F903957 (void);
// 0x000002E9 System.Void ZXing.PDF417.PDF417ResultMetadata::set_FileSize(System.Int64)
extern void PDF417ResultMetadata_set_FileSize_mE1E1B13785E6CFF3833F8BF84083DFB32048336D (void);
// 0x000002EA System.Int32 ZXing.PDF417.PDF417ResultMetadata::get_Checksum()
extern void PDF417ResultMetadata_get_Checksum_mAD269218DE02E7B2A10BD86E33D13C611BC1AF31 (void);
// 0x000002EB System.Void ZXing.PDF417.PDF417ResultMetadata::set_Checksum(System.Int32)
extern void PDF417ResultMetadata_set_Checksum_m1BEA43FBA191166F432634B5D50BF307315E9337 (void);
// 0x000002EC System.Int64 ZXing.PDF417.PDF417ResultMetadata::get_Timestamp()
extern void PDF417ResultMetadata_get_Timestamp_m39CB03064C51F18EE47AFF53FC1B231D3BFDDEF8 (void);
// 0x000002ED System.Void ZXing.PDF417.PDF417ResultMetadata::set_Timestamp(System.Int64)
extern void PDF417ResultMetadata_set_Timestamp_m3A4859104DB07DD217E73D985584DBD9493962CC (void);
// 0x000002EE System.Void ZXing.PDF417.PDF417ResultMetadata::.ctor()
extern void PDF417ResultMetadata__ctor_mE13E5C0F4A88D5EA98D198FBE3A45CDD9A46188A (void);
// 0x000002EF ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void PDF417Writer_encode_m01FD5727BFE7D2B1773CEF35DB7250FCF68412DB (void);
// 0x000002F0 ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32)
extern void PDF417Writer_encode_m0525C38078AFDAD9CED97AB704CD8C38268C5FDB (void);
// 0x000002F1 ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::bitMatrixFromEncoder(ZXing.PDF417.Internal.PDF417,System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void PDF417Writer_bitMatrixFromEncoder_m1BBF4BE26C84A080E30676751DDDB3362654E106 (void);
// 0x000002F2 ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::bitMatrixFromBitArray(System.SByte[][],System.Int32)
extern void PDF417Writer_bitMatrixFromBitArray_m13261225DE128B354B9C74371DCA8826A06F7B4E (void);
// 0x000002F3 System.SByte[][] ZXing.PDF417.PDF417Writer::rotateArray(System.SByte[][])
extern void PDF417Writer_rotateArray_mF309A9FDD0873C3BA76E2DF148AB601EBFEE58BE (void);
// 0x000002F4 System.Void ZXing.PDF417.PDF417Writer::.ctor()
extern void PDF417Writer__ctor_mB4C34696EDEE36E58E1E1318CD1115B70888CF91 (void);
// 0x000002F5 System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_ColumnCount()
extern void BarcodeMetadata_get_ColumnCount_m495DD133C9AC8FF93A6F9E783AE1DD0F2CEBBEBB (void);
// 0x000002F6 System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_ColumnCount(System.Int32)
extern void BarcodeMetadata_set_ColumnCount_mD288D76895F245577A3E5E1118B7F5E30925E9F7 (void);
// 0x000002F7 System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_ErrorCorrectionLevel()
extern void BarcodeMetadata_get_ErrorCorrectionLevel_m053AF2ADB6E7AF297637A82C358565CC5EE68926 (void);
// 0x000002F8 System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_ErrorCorrectionLevel(System.Int32)
extern void BarcodeMetadata_set_ErrorCorrectionLevel_m7E14F433CA257C46D64EC5EC2006CBF1C2C6DEE8 (void);
// 0x000002F9 System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_RowCountUpper()
extern void BarcodeMetadata_get_RowCountUpper_m46E6E91B328FE48C850011A42E3082A0C1BF0212 (void);
// 0x000002FA System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_RowCountUpper(System.Int32)
extern void BarcodeMetadata_set_RowCountUpper_m4D34A5D2DEEAB2C70FDCCB4DDD745D2F5EDB77CF (void);
// 0x000002FB System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_RowCountLower()
extern void BarcodeMetadata_get_RowCountLower_mA8D9AF30B653BD86D4EF352D4C18F4A39B7AB97E (void);
// 0x000002FC System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_RowCountLower(System.Int32)
extern void BarcodeMetadata_set_RowCountLower_m105DFED692DA7CA9AC33E9F12B738385FEBEDF83 (void);
// 0x000002FD System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::get_RowCount()
extern void BarcodeMetadata_get_RowCount_m59CB0D2FD39454348D476973BF6BCA4BE95B1A91 (void);
// 0x000002FE System.Void ZXing.PDF417.Internal.BarcodeMetadata::set_RowCount(System.Int32)
extern void BarcodeMetadata_set_RowCount_m81E4F7B1256A34C6C21B58F71F39DAEE99F18268 (void);
// 0x000002FF System.Void ZXing.PDF417.Internal.BarcodeMetadata::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BarcodeMetadata__ctor_m1167102FDE574005D14927FADDA35C11CD9C4FC6 (void);
// 0x00000300 System.Void ZXing.PDF417.Internal.BarcodeValue::setValue(System.Int32)
extern void BarcodeValue_setValue_mB3018B60837C4B804A096A5331AC71CDED2EAA4F (void);
// 0x00000301 System.Int32[] ZXing.PDF417.Internal.BarcodeValue::getValue()
extern void BarcodeValue_getValue_mC84B99ED8375B69C4380CFA82ECB75CB4AB85198 (void);
// 0x00000302 System.Int32 ZXing.PDF417.Internal.BarcodeValue::getConfidence(System.Int32)
extern void BarcodeValue_getConfidence_m99163A780C379C698822C39643DE59616F1CD569 (void);
// 0x00000303 System.Void ZXing.PDF417.Internal.BarcodeValue::.ctor()
extern void BarcodeValue__ctor_m53812DF904735195921B590140AB002A4B81AB47 (void);
// 0x00000304 ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_TopLeft()
extern void BoundingBox_get_TopLeft_m809A799D2D1817F6F1207C942E0E5EB0E8DD829F (void);
// 0x00000305 System.Void ZXing.PDF417.Internal.BoundingBox::set_TopLeft(ZXing.ResultPoint)
extern void BoundingBox_set_TopLeft_m4D4F353BE27281800269EAC9177562F780A0E7B7 (void);
// 0x00000306 ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_TopRight()
extern void BoundingBox_get_TopRight_mAFF6562680BC771AD1549197076470B3C455562B (void);
// 0x00000307 System.Void ZXing.PDF417.Internal.BoundingBox::set_TopRight(ZXing.ResultPoint)
extern void BoundingBox_set_TopRight_mE3E435F55B76E39889FFC9B1A727CC1ADC846536 (void);
// 0x00000308 ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_BottomLeft()
extern void BoundingBox_get_BottomLeft_m414DD57EB4143B38B7656AE71C15F747AB3B3461 (void);
// 0x00000309 System.Void ZXing.PDF417.Internal.BoundingBox::set_BottomLeft(ZXing.ResultPoint)
extern void BoundingBox_set_BottomLeft_mCA0A10257D4EB7FFB89466168CE807F4CF705531 (void);
// 0x0000030A ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::get_BottomRight()
extern void BoundingBox_get_BottomRight_m143B41C9B2CE26F54311527CE9068DD1A49906AA (void);
// 0x0000030B System.Void ZXing.PDF417.Internal.BoundingBox::set_BottomRight(ZXing.ResultPoint)
extern void BoundingBox_set_BottomRight_mDD7B1B526B4F0C55D4A9FB6D36F923E07912A445 (void);
// 0x0000030C System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MinX()
extern void BoundingBox_get_MinX_m1E0CF6C6E0F24A7052C3FED772E0E468898A67FC (void);
// 0x0000030D System.Void ZXing.PDF417.Internal.BoundingBox::set_MinX(System.Int32)
extern void BoundingBox_set_MinX_m47EC61F03B8F44AD57581D33BA25BC778CAAD6FF (void);
// 0x0000030E System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MaxX()
extern void BoundingBox_get_MaxX_mB7E6B98110BE682F8EFDB280E873C2C3D0B76530 (void);
// 0x0000030F System.Void ZXing.PDF417.Internal.BoundingBox::set_MaxX(System.Int32)
extern void BoundingBox_set_MaxX_m92FA78242CCA06564C2D2F6FA6FC734E8EB9B9DB (void);
// 0x00000310 System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MinY()
extern void BoundingBox_get_MinY_mBF249C1D47764636FC69ADE98E83E0DA20091373 (void);
// 0x00000311 System.Void ZXing.PDF417.Internal.BoundingBox::set_MinY(System.Int32)
extern void BoundingBox_set_MinY_mA97675214D9A2864A9C14471C6A857C9E30919F9 (void);
// 0x00000312 System.Int32 ZXing.PDF417.Internal.BoundingBox::get_MaxY()
extern void BoundingBox_get_MaxY_mFE45D2C1249C4AED90E11F6EDC0D7AAA0CF10034 (void);
// 0x00000313 System.Void ZXing.PDF417.Internal.BoundingBox::set_MaxY(System.Int32)
extern void BoundingBox_set_MaxY_mB9314DB37878A9D8994BA861166983357E892548 (void);
// 0x00000314 ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::Create(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void BoundingBox_Create_m95BA0F393F3DC1136BCCF8C36B0F30E13142B984 (void);
// 0x00000315 ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::Create(ZXing.PDF417.Internal.BoundingBox)
extern void BoundingBox_Create_m07FF6A30C11FAC700A5AD6D52702D435946C638C (void);
// 0x00000316 System.Void ZXing.PDF417.Internal.BoundingBox::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void BoundingBox__ctor_m16EBD238EDEE9F2E0C68D36AD04DC008A08E1929 (void);
// 0x00000317 System.Void ZXing.PDF417.Internal.BoundingBox::.ctor(ZXing.PDF417.Internal.BoundingBox)
extern void BoundingBox__ctor_mCED9D07198DFD85E2B842F2185AF839CFD1D3C70 (void);
// 0x00000318 ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::merge(ZXing.PDF417.Internal.BoundingBox,ZXing.PDF417.Internal.BoundingBox)
extern void BoundingBox_merge_m546EA21DEB263E7D6A8BD0AE022B6DBBC08CB3CA (void);
// 0x00000319 ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.BoundingBox::addMissingRows(System.Int32,System.Int32,System.Boolean)
extern void BoundingBox_addMissingRows_mDD24DBF58916375029FDC43FFE2254D7D04719A8 (void);
// 0x0000031A System.Int32 ZXing.PDF417.Internal.Codeword::get_StartX()
extern void Codeword_get_StartX_m1D0114919F24F5C6004531BF6306D92820AFD1A2 (void);
// 0x0000031B System.Void ZXing.PDF417.Internal.Codeword::set_StartX(System.Int32)
extern void Codeword_set_StartX_mD20972ABC4A9D72AEFD38095A5329EC3936E5111 (void);
// 0x0000031C System.Int32 ZXing.PDF417.Internal.Codeword::get_EndX()
extern void Codeword_get_EndX_m7DEDE2045D8192CCB527417E2E1F2910C319FDD4 (void);
// 0x0000031D System.Void ZXing.PDF417.Internal.Codeword::set_EndX(System.Int32)
extern void Codeword_set_EndX_m45ED6E4F856E61BD0FCABDBA5D12A3D176F0F7B8 (void);
// 0x0000031E System.Int32 ZXing.PDF417.Internal.Codeword::get_Bucket()
extern void Codeword_get_Bucket_mC3EAAE2CF85CC24CE3EF1B4B05FA8C74642CBE18 (void);
// 0x0000031F System.Void ZXing.PDF417.Internal.Codeword::set_Bucket(System.Int32)
extern void Codeword_set_Bucket_mB1D82B0CB6BADCB796101189FD2B4751A7F2A067 (void);
// 0x00000320 System.Int32 ZXing.PDF417.Internal.Codeword::get_Value()
extern void Codeword_get_Value_m26AF7BAB4CDC25EDCF6622CD2485D6FBAC4165E5 (void);
// 0x00000321 System.Void ZXing.PDF417.Internal.Codeword::set_Value(System.Int32)
extern void Codeword_set_Value_m514E6D3BFE3730E1EF61D57EDB02F475A9B6A0B9 (void);
// 0x00000322 System.Int32 ZXing.PDF417.Internal.Codeword::get_RowNumber()
extern void Codeword_get_RowNumber_mFF2FB6F1667309924D0A5FFF1EE976AE7D1F999F (void);
// 0x00000323 System.Void ZXing.PDF417.Internal.Codeword::set_RowNumber(System.Int32)
extern void Codeword_set_RowNumber_m0F59B62A19ECAEDCDE96BFAD662C3D78C9FEEE09 (void);
// 0x00000324 System.Void ZXing.PDF417.Internal.Codeword::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Codeword__ctor_m5A8BCFF187983FB2872EF8ECC7FE077959EE55A0 (void);
// 0x00000325 System.Int32 ZXing.PDF417.Internal.Codeword::get_Width()
extern void Codeword_get_Width_m74648550B4B42E891AB00DCE50913A7E57D12FF1 (void);
// 0x00000326 System.Boolean ZXing.PDF417.Internal.Codeword::get_HasValidRowNumber()
extern void Codeword_get_HasValidRowNumber_m43D09D2258856407C7054D82AB161093D69AD28F (void);
// 0x00000327 System.Boolean ZXing.PDF417.Internal.Codeword::IsValidRowNumber(System.Int32)
extern void Codeword_IsValidRowNumber_m66435A557794FDF3A43E64A8B8EFCC00C261F894 (void);
// 0x00000328 System.Void ZXing.PDF417.Internal.Codeword::setRowNumberAsRowIndicatorColumn()
extern void Codeword_setRowNumberAsRowIndicatorColumn_mD7DB556D68985B744BC6B68E8839818A3F9162A2 (void);
// 0x00000329 System.String ZXing.PDF417.Internal.Codeword::ToString()
extern void Codeword_ToString_mF8BCC87A8D9E1824EE1040C67F2FA0C42A81611D (void);
// 0x0000032A System.Void ZXing.PDF417.Internal.Codeword::.cctor()
extern void Codeword__cctor_mA05017E5A686CC5E97DC8E116FAC4B600175CF47 (void);
// 0x0000032B System.Void ZXing.PDF417.Internal.DecodedBitStreamParser::.cctor()
extern void DecodedBitStreamParser__cctor_mD955CB3A083DA0072081DA3563EF9D1A02ABBB12 (void);
// 0x0000032C ZXing.Common.DecoderResult ZXing.PDF417.Internal.DecodedBitStreamParser::decode(System.Int32[],System.String)
extern void DecodedBitStreamParser_decode_mB87D4F07F351AE372FA629EA32EC2BBE0787F230 (void);
// 0x0000032D System.Text.Encoding ZXing.PDF417.Internal.DecodedBitStreamParser::getEncoding(System.String)
extern void DecodedBitStreamParser_getEncoding_m260D2ADBD2482A32672BD2BEB35F2D2DD782AE15 (void);
// 0x0000032E System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::decodeMacroBlock(System.Int32[],System.Int32,ZXing.PDF417.PDF417ResultMetadata)
extern void DecodedBitStreamParser_decodeMacroBlock_m47DA022C962C6D8A25E066CF5062DE88B04A68C7 (void);
// 0x0000032F System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::textCompaction(System.Int32[],System.Int32,System.Text.StringBuilder)
extern void DecodedBitStreamParser_textCompaction_m450831274EA30B192E02BAFFFE01594E4FD37D98 (void);
// 0x00000330 System.Void ZXing.PDF417.Internal.DecodedBitStreamParser::decodeTextCompaction(System.Int32[],System.Int32[],System.Int32,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeTextCompaction_m063EDA5987DAA7B8E4CF8EAA6486F4771F869820 (void);
// 0x00000331 System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::byteCompaction(System.Int32,System.Int32[],System.Text.Encoding,System.Int32,System.Text.StringBuilder)
extern void DecodedBitStreamParser_byteCompaction_m07C768D9425A42F74E57C41736CB1BDD22C27EF4 (void);
// 0x00000332 System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser::numericCompaction(System.Int32[],System.Int32,System.Text.StringBuilder)
extern void DecodedBitStreamParser_numericCompaction_m4BB71B3FA378F91BAEBA2DE3DBD78A9CFA889CD2 (void);
// 0x00000333 System.String ZXing.PDF417.Internal.DecodedBitStreamParser::decodeBase900toBase10(System.Int32[],System.Int32)
extern void DecodedBitStreamParser_decodeBase900toBase10_mDF6453F799AE86ECEA1E9F0E02073CDC1CE7693B (void);
// 0x00000334 ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.DetectionResult::get_Metadata()
extern void DetectionResult_get_Metadata_mCAC06316DF7AA38C4138D8C19B3D415BB3787F97 (void);
// 0x00000335 System.Void ZXing.PDF417.Internal.DetectionResult::set_Metadata(ZXing.PDF417.Internal.BarcodeMetadata)
extern void DetectionResult_set_Metadata_mFDC1EB9D7EE04E3ECC1015FC757FC181DD1F339A (void);
// 0x00000336 ZXing.PDF417.Internal.DetectionResultColumn[] ZXing.PDF417.Internal.DetectionResult::get_DetectionResultColumns()
extern void DetectionResult_get_DetectionResultColumns_mDEF605B743BE0044505226C69D8098B2BC94F34E (void);
// 0x00000337 System.Void ZXing.PDF417.Internal.DetectionResult::set_DetectionResultColumns(ZXing.PDF417.Internal.DetectionResultColumn[])
extern void DetectionResult_set_DetectionResultColumns_mEDA4568663EDC7AF66DEB44FA7E4ED0974E1820E (void);
// 0x00000338 ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResult::get_Box()
extern void DetectionResult_get_Box_m45A7970A731A686CF5849071D5EB10ECDFA294A3 (void);
// 0x00000339 System.Void ZXing.PDF417.Internal.DetectionResult::set_Box(ZXing.PDF417.Internal.BoundingBox)
extern void DetectionResult_set_Box_mEC9C5C08D9A50C0F986A3EC2A9E5FE048653019B (void);
// 0x0000033A System.Int32 ZXing.PDF417.Internal.DetectionResult::get_ColumnCount()
extern void DetectionResult_get_ColumnCount_m79C72C6B91A03AE326628092E51430ED6F3E41BE (void);
// 0x0000033B System.Void ZXing.PDF417.Internal.DetectionResult::set_ColumnCount(System.Int32)
extern void DetectionResult_set_ColumnCount_m8EB8FBAB718F027FF7653F35618759684883C46E (void);
// 0x0000033C System.Int32 ZXing.PDF417.Internal.DetectionResult::get_RowCount()
extern void DetectionResult_get_RowCount_mF55E2A93BD3626245C4D9AD013B21F6A71CBB176 (void);
// 0x0000033D System.Int32 ZXing.PDF417.Internal.DetectionResult::get_ErrorCorrectionLevel()
extern void DetectionResult_get_ErrorCorrectionLevel_mF0DC649E4C8286CB20BD5960E7BD7A71008ABA0B (void);
// 0x0000033E System.Void ZXing.PDF417.Internal.DetectionResult::.ctor(ZXing.PDF417.Internal.BarcodeMetadata,ZXing.PDF417.Internal.BoundingBox)
extern void DetectionResult__ctor_m53CF2376F4F5885A7B6340B5383632729BC1EC1B (void);
// 0x0000033F ZXing.PDF417.Internal.DetectionResultColumn[] ZXing.PDF417.Internal.DetectionResult::getDetectionResultColumns()
extern void DetectionResult_getDetectionResultColumns_mA26963D25AFD334969B7D0D761BB7709E80490B8 (void);
// 0x00000340 System.Void ZXing.PDF417.Internal.DetectionResult::adjustIndicatorColumnRowNumbers(ZXing.PDF417.Internal.DetectionResultColumn)
extern void DetectionResult_adjustIndicatorColumnRowNumbers_m6327AD854FECCE295423F3D512518CA5990EF546 (void);
// 0x00000341 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbers()
extern void DetectionResult_adjustRowNumbers_mAEC24ECB95C463D4E7690B64A6FDE3D7595BBAC6 (void);
// 0x00000342 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersByRow()
extern void DetectionResult_adjustRowNumbersByRow_mCA34679CBB8CA72B411AB880633CCF1744192F5B (void);
// 0x00000343 System.Void ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersFromBothRI()
extern void DetectionResult_adjustRowNumbersFromBothRI_m7FE2C96EA378B5577C26713219CB17342C283962 (void);
// 0x00000344 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersFromRRI()
extern void DetectionResult_adjustRowNumbersFromRRI_m199CFB51CF20984F15F38C29F41A195817CC5069 (void);
// 0x00000345 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumbersFromLRI()
extern void DetectionResult_adjustRowNumbersFromLRI_m17AB24E049CB2E44CF6D9337A9DB61712CD279CA (void);
// 0x00000346 System.Int32 ZXing.PDF417.Internal.DetectionResult::adjustRowNumberIfValid(System.Int32,System.Int32,ZXing.PDF417.Internal.Codeword)
extern void DetectionResult_adjustRowNumberIfValid_m45CC5FB7A7DCB9548AF83278C8227EC48A803745 (void);
// 0x00000347 System.Void ZXing.PDF417.Internal.DetectionResult::adjustRowNumbers(System.Int32,System.Int32,ZXing.PDF417.Internal.Codeword[])
extern void DetectionResult_adjustRowNumbers_m764DA9A1F56555D706DFE13F19C085208691B9B2 (void);
// 0x00000348 System.Boolean ZXing.PDF417.Internal.DetectionResult::adjustRowNumber(ZXing.PDF417.Internal.Codeword,ZXing.PDF417.Internal.Codeword)
extern void DetectionResult_adjustRowNumber_mD74ECBE2E9E19C78F455904E45BAD2CBBADC1416 (void);
// 0x00000349 System.String ZXing.PDF417.Internal.DetectionResult::ToString()
extern void DetectionResult_ToString_m4E3966F79B6C225345028DEC67C0C0BAC1477665 (void);
// 0x0000034A ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResultColumn::get_Box()
extern void DetectionResultColumn_get_Box_m495172EFF137B324BCC742A3C033B1B41DD2FC7E (void);
// 0x0000034B System.Void ZXing.PDF417.Internal.DetectionResultColumn::set_Box(ZXing.PDF417.Internal.BoundingBox)
extern void DetectionResultColumn_set_Box_mD828F00C972EE065B1AD151B2BA591E120AD6ECC (void);
// 0x0000034C ZXing.PDF417.Internal.Codeword[] ZXing.PDF417.Internal.DetectionResultColumn::get_Codewords()
extern void DetectionResultColumn_get_Codewords_mB3B530850F25B8A51BF07F28F5871D47B60BDB14 (void);
// 0x0000034D System.Void ZXing.PDF417.Internal.DetectionResultColumn::set_Codewords(ZXing.PDF417.Internal.Codeword[])
extern void DetectionResultColumn_set_Codewords_mADE7E294F5BD95EB764D1C5B87F63D39D18B562A (void);
// 0x0000034E System.Void ZXing.PDF417.Internal.DetectionResultColumn::.ctor(ZXing.PDF417.Internal.BoundingBox)
extern void DetectionResultColumn__ctor_mDC6656F6204664325067BDD4C276FCB57C79CF65 (void);
// 0x0000034F System.Int32 ZXing.PDF417.Internal.DetectionResultColumn::IndexForRow(System.Int32)
extern void DetectionResultColumn_IndexForRow_m7B10BA3BC25D6DDE8C0BEC67E6D80F7502314EBB (void);
// 0x00000350 System.Int32 ZXing.PDF417.Internal.DetectionResultColumn::RowForIndex(System.Int32)
extern void DetectionResultColumn_RowForIndex_mA64382A3F30FAB7B45DDE63C6673EEFA1C19FF81 (void);
// 0x00000351 ZXing.PDF417.Internal.Codeword ZXing.PDF417.Internal.DetectionResultColumn::getCodeword(System.Int32)
extern void DetectionResultColumn_getCodeword_m065483E9E288F3BAED18758AC9EC72E77C7AFDA0 (void);
// 0x00000352 ZXing.PDF417.Internal.Codeword ZXing.PDF417.Internal.DetectionResultColumn::getCodewordNearby(System.Int32)
extern void DetectionResultColumn_getCodewordNearby_mCC0A95DCC92A3FC15AD54FAEF20160ACD906730C (void);
// 0x00000353 System.Int32 ZXing.PDF417.Internal.DetectionResultColumn::imageRowToCodewordIndex(System.Int32)
extern void DetectionResultColumn_imageRowToCodewordIndex_m0E54C5321AF45E191F115C519B8A875FBE1610F6 (void);
// 0x00000354 System.Void ZXing.PDF417.Internal.DetectionResultColumn::setCodeword(System.Int32,ZXing.PDF417.Internal.Codeword)
extern void DetectionResultColumn_setCodeword_mB2EE392DA7025ADEEBAFEAEE027B5B4476CDB9D3 (void);
// 0x00000355 System.String ZXing.PDF417.Internal.DetectionResultColumn::ToString()
extern void DetectionResultColumn_ToString_mDCFDC4D16B67678A79E2D512310A160F705A7CE6 (void);
// 0x00000356 System.Boolean ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::get_IsLeft()
extern void DetectionResultRowIndicatorColumn_get_IsLeft_mCB7F3ABA649CCDBE5B8C92C75B20424485D52939 (void);
// 0x00000357 System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::set_IsLeft(System.Boolean)
extern void DetectionResultRowIndicatorColumn_set_IsLeft_mD0CB7FAF258463DC6DA6DA3627FC60BF28D9BABC (void);
// 0x00000358 System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::.ctor(ZXing.PDF417.Internal.BoundingBox,System.Boolean)
extern void DetectionResultRowIndicatorColumn__ctor_m2DAECFE168232DEC20C8FBADA84BD59A26B2D532 (void);
// 0x00000359 System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::setRowNumbers()
extern void DetectionResultRowIndicatorColumn_setRowNumbers_m1BD12E880A6EB3BAE5C89872D61CFCD6F3C91BEE (void);
// 0x0000035A System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::adjustCompleteIndicatorColumnRowNumbers(ZXing.PDF417.Internal.BarcodeMetadata)
extern void DetectionResultRowIndicatorColumn_adjustCompleteIndicatorColumnRowNumbers_mFAA83340B35261DBB8C83C711F123EB9F512460A (void);
// 0x0000035B System.Int32[] ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::getRowHeights()
extern void DetectionResultRowIndicatorColumn_getRowHeights_m54525EC66F6CFA69132DF96DDB9384101140D2FC (void);
// 0x0000035C System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::adjustIncompleteIndicatorColumnRowNumbers(ZXing.PDF417.Internal.BarcodeMetadata)
extern void DetectionResultRowIndicatorColumn_adjustIncompleteIndicatorColumnRowNumbers_mF75561B54EF8CF4956CB445ECD4AD5684741650F (void);
// 0x0000035D ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::getBarcodeMetadata()
extern void DetectionResultRowIndicatorColumn_getBarcodeMetadata_m651F23D4821E2748E5D042D2057EA8BFE22247F4 (void);
// 0x0000035E System.Void ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::removeIncorrectCodewords(ZXing.PDF417.Internal.Codeword[],ZXing.PDF417.Internal.BarcodeMetadata)
extern void DetectionResultRowIndicatorColumn_removeIncorrectCodewords_m9EC6C89FF130BE3544A5BA7CA17F5997E7565EC2 (void);
// 0x0000035F System.String ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::ToString()
extern void DetectionResultRowIndicatorColumn_ToString_m2323834957F6B2D1E8D8956E1141716DF5330C5B (void);
// 0x00000360 System.Void ZXing.PDF417.Internal.PDF417CodewordDecoder::.cctor()
extern void PDF417CodewordDecoder__cctor_m18A6F45D8DEF81ED8AC15F484789A58825712A90 (void);
// 0x00000361 System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getDecodedValue(System.Int32[])
extern void PDF417CodewordDecoder_getDecodedValue_mA279CC7655E2AF8563734E2BA7E9DE4798B8E354 (void);
// 0x00000362 System.Int32[] ZXing.PDF417.Internal.PDF417CodewordDecoder::sampleBitCounts(System.Int32[])
extern void PDF417CodewordDecoder_sampleBitCounts_m4C2CB28247AFED5F494F9E35EF174C10ED79473B (void);
// 0x00000363 System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getDecodedCodewordValue(System.Int32[])
extern void PDF417CodewordDecoder_getDecodedCodewordValue_m1FEE14F3B48E4D6D7CDA92939EEF39E557A2D244 (void);
// 0x00000364 System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getBitValue(System.Int32[])
extern void PDF417CodewordDecoder_getBitValue_m70328F64C085CBA68C92C9A35B9BBBF0CE835746 (void);
// 0x00000365 System.Int32 ZXing.PDF417.Internal.PDF417CodewordDecoder::getClosestDecodedValue(System.Int32[])
extern void PDF417CodewordDecoder_getClosestDecodedValue_mAF9B5E81599BC2C18748691F8E4F1A76F8D98D38 (void);
// 0x00000366 ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::decode(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_decode_mB9C5C2C952F6469289964B3504D4DC9F088AA9DB (void);
// 0x00000367 ZXing.PDF417.Internal.DetectionResult ZXing.PDF417.Internal.PDF417ScanningDecoder::merge(ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn,ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn)
extern void PDF417ScanningDecoder_merge_m287BAB2103F12EECD23533EE439C5FF3FD1F6B53 (void);
// 0x00000368 ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.PDF417ScanningDecoder::adjustBoundingBox(ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn)
extern void PDF417ScanningDecoder_adjustBoundingBox_mE101105DCC3BB8776CCA562845E7FD56FCF09A78 (void);
// 0x00000369 System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getMax(System.Int32[])
extern void PDF417ScanningDecoder_getMax_m7325291BFC46BBC8846D3A9E8460081ED78AD4B0 (void);
// 0x0000036A ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.PDF417ScanningDecoder::getBarcodeMetadata(ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn,ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn)
extern void PDF417ScanningDecoder_getBarcodeMetadata_m93005D71624DBF8A046808651DB2699B37ED02CB (void);
// 0x0000036B ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn ZXing.PDF417.Internal.PDF417ScanningDecoder::getRowIndicatorColumn(ZXing.Common.BitMatrix,ZXing.PDF417.Internal.BoundingBox,ZXing.ResultPoint,System.Boolean,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_getRowIndicatorColumn_mFA3BB1C3DD905D9E097746CD37FFF3ACE112CFE6 (void);
// 0x0000036C System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::adjustCodewordCount(ZXing.PDF417.Internal.DetectionResult,ZXing.PDF417.Internal.BarcodeValue[][])
extern void PDF417ScanningDecoder_adjustCodewordCount_m462AEFBA30BFF693B0757F094020C904C75B126B (void);
// 0x0000036D ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::createDecoderResult(ZXing.PDF417.Internal.DetectionResult)
extern void PDF417ScanningDecoder_createDecoderResult_mF68EF4D433A753A48228AA3E2247800BD50286A7 (void);
// 0x0000036E ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::createDecoderResultFromAmbiguousValues(System.Int32,System.Int32[],System.Int32[],System.Int32[],System.Int32[][])
extern void PDF417ScanningDecoder_createDecoderResultFromAmbiguousValues_mBB37E47398BE55F38392709DC61D68AFFBCD557B (void);
// 0x0000036F ZXing.PDF417.Internal.BarcodeValue[][] ZXing.PDF417.Internal.PDF417ScanningDecoder::createBarcodeMatrix(ZXing.PDF417.Internal.DetectionResult)
extern void PDF417ScanningDecoder_createBarcodeMatrix_m0031B1F9A607D8D180525F0321B9528712A90B92 (void);
// 0x00000370 System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::isValidBarcodeColumn(ZXing.PDF417.Internal.DetectionResult,System.Int32)
extern void PDF417ScanningDecoder_isValidBarcodeColumn_m3EA1D87F26C127A67571238C9F852B36826970FE (void);
// 0x00000371 System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getStartColumn(ZXing.PDF417.Internal.DetectionResult,System.Int32,System.Int32,System.Boolean)
extern void PDF417ScanningDecoder_getStartColumn_m7083F7E9FB84D6F2B8550D15B98B5B1EE0E5F79F (void);
// 0x00000372 ZXing.PDF417.Internal.Codeword ZXing.PDF417.Internal.PDF417ScanningDecoder::detectCodeword(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_detectCodeword_m417D253D5DD32B64884944214C49D8E78283AC7B (void);
// 0x00000373 System.Int32[] ZXing.PDF417.Internal.PDF417ScanningDecoder::getModuleBitCount(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Boolean,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_getModuleBitCount_m7302588876C803EDE03E04D5E704FB1B4A3F232C (void);
// 0x00000374 System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getNumberOfECCodeWords(System.Int32)
extern void PDF417ScanningDecoder_getNumberOfECCodeWords_mD5215D60C9BA8BC8980956441832DFE52161DBDA (void);
// 0x00000375 System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::adjustCodewordStartColumn(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Boolean,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_adjustCodewordStartColumn_m08D180DF936D2F9A23A6D88A869542531F3E980D (void);
// 0x00000376 System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::checkCodewordSkew(System.Int32,System.Int32,System.Int32)
extern void PDF417ScanningDecoder_checkCodewordSkew_mEA0E382D9F3F2683E2489DBBCB8B21D93CDA53FD (void);
// 0x00000377 ZXing.Common.DecoderResult ZXing.PDF417.Internal.PDF417ScanningDecoder::decodeCodewords(System.Int32[],System.Int32,System.Int32[])
extern void PDF417ScanningDecoder_decodeCodewords_mEF38AE2872478FB1E8CB7736A268175067AD49FB (void);
// 0x00000378 System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::correctErrors(System.Int32[],System.Int32[],System.Int32)
extern void PDF417ScanningDecoder_correctErrors_mB45F03F4B2344AE589CFFE3CCD332270E62D72E1 (void);
// 0x00000379 System.Boolean ZXing.PDF417.Internal.PDF417ScanningDecoder::verifyCodewordCount(System.Int32[],System.Int32)
extern void PDF417ScanningDecoder_verifyCodewordCount_mB0537CC59012D4870A7DAD3E4B26BF91D7D9BE55 (void);
// 0x0000037A System.Int32[] ZXing.PDF417.Internal.PDF417ScanningDecoder::getBitCountForCodeword(System.Int32)
extern void PDF417ScanningDecoder_getBitCountForCodeword_mE3D5E08553D1A210AC2502627DB5D57815C0E16D (void);
// 0x0000037B System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getCodewordBucketNumber(System.Int32)
extern void PDF417ScanningDecoder_getCodewordBucketNumber_mA20A663455305059C14FC445CAFF40E2EB949BC6 (void);
// 0x0000037C System.Int32 ZXing.PDF417.Internal.PDF417ScanningDecoder::getCodewordBucketNumber(System.Int32[])
extern void PDF417ScanningDecoder_getCodewordBucketNumber_mD8D55368016ACEBFD805D791900F4DC659011962 (void);
// 0x0000037D System.String ZXing.PDF417.Internal.PDF417ScanningDecoder::ToString(ZXing.PDF417.Internal.BarcodeValue[][])
extern void PDF417ScanningDecoder_ToString_m5F200BFBA87F568B82ADAC2192EB41618C61B192 (void);
// 0x0000037E System.Void ZXing.PDF417.Internal.PDF417ScanningDecoder::.cctor()
extern void PDF417ScanningDecoder__cctor_m12203AF6128BD4141EC5D463CE8A839915A2DAB8 (void);
// 0x0000037F ZXing.PDF417.Internal.PDF417DetectorResult ZXing.PDF417.Internal.Detector::detect(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,System.Boolean)
extern void Detector_detect_mD5B08CB51DDB6A110B43C8C9C1D3BD929DD9A8B2 (void);
// 0x00000380 System.Collections.Generic.List`1<ZXing.ResultPoint[]> ZXing.PDF417.Internal.Detector::detect(System.Boolean,ZXing.Common.BitMatrix)
extern void Detector_detect_m320C25A464C25B6DF88310915B525B16B378AD7E (void);
// 0x00000381 ZXing.ResultPoint[] ZXing.PDF417.Internal.Detector::findVertices(ZXing.Common.BitMatrix,System.Int32,System.Int32)
extern void Detector_findVertices_m1BD74BB15BCF96EC821CBB3900DEDD69B8548203 (void);
// 0x00000382 System.Void ZXing.PDF417.Internal.Detector::copyToResult(ZXing.ResultPoint[],ZXing.ResultPoint[],System.Int32[])
extern void Detector_copyToResult_m23C82D6732944677F0A9891BE09C3ED2774E3E59 (void);
// 0x00000383 ZXing.ResultPoint[] ZXing.PDF417.Internal.Detector::findRowsWithPattern(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern void Detector_findRowsWithPattern_mC55194C1182052DCC63516B3179E9CE18BD44ACF (void);
// 0x00000384 System.Int32[] ZXing.PDF417.Internal.Detector::findGuardPattern(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32,System.Boolean,System.Int32[],System.Int32[])
extern void Detector_findGuardPattern_m8F090EE16E8A5948D47C9B5A20131CAA9D4ED3B6 (void);
// 0x00000385 System.Int32 ZXing.PDF417.Internal.Detector::patternMatchVariance(System.Int32[],System.Int32[],System.Int32)
extern void Detector_patternMatchVariance_mA9965D7434FF7B607A76E184DB0907D50E94535C (void);
// 0x00000386 System.Void ZXing.PDF417.Internal.Detector::.ctor()
extern void Detector__ctor_m6E13EAD989DE10A2828A18CFFF658C2E18C23D79 (void);
// 0x00000387 System.Void ZXing.PDF417.Internal.Detector::.cctor()
extern void Detector__cctor_mE873B9BAA0D9E694A2E9C00958867807ACD980F8 (void);
// 0x00000388 ZXing.Common.BitMatrix ZXing.PDF417.Internal.PDF417DetectorResult::get_Bits()
extern void PDF417DetectorResult_get_Bits_m9BFEB179EAA8DA65BEEAD6A75C5F79FE676F7357 (void);
// 0x00000389 System.Void ZXing.PDF417.Internal.PDF417DetectorResult::set_Bits(ZXing.Common.BitMatrix)
extern void PDF417DetectorResult_set_Bits_m05D78DC749C240F4868352AF37376EA0C511EF34 (void);
// 0x0000038A System.Collections.Generic.List`1<ZXing.ResultPoint[]> ZXing.PDF417.Internal.PDF417DetectorResult::get_Points()
extern void PDF417DetectorResult_get_Points_mCC688776DBC06D17D300BC006C8F708B8127CB3F (void);
// 0x0000038B System.Void ZXing.PDF417.Internal.PDF417DetectorResult::set_Points(System.Collections.Generic.List`1<ZXing.ResultPoint[]>)
extern void PDF417DetectorResult_set_Points_m6882E23DA70AAF83E193C61315C6CEF642F6BDF1 (void);
// 0x0000038C System.Void ZXing.PDF417.Internal.PDF417DetectorResult::.ctor(ZXing.Common.BitMatrix,System.Collections.Generic.List`1<ZXing.ResultPoint[]>)
extern void PDF417DetectorResult__ctor_m57D6DD5548CB8072FD8DFCB29CB277E92AEEAE38 (void);
// 0x0000038D System.Void ZXing.PDF417.Internal.BarcodeMatrix::.ctor(System.Int32,System.Int32,System.Boolean)
extern void BarcodeMatrix__ctor_m3FE79388A37B82EAC2C6E1ECF074E91BE2CE4886 (void);
// 0x0000038E System.Void ZXing.PDF417.Internal.BarcodeMatrix::set(System.Int32,System.Int32,System.SByte)
extern void BarcodeMatrix_set_mA057BECB8D1937B0702630A613278331821B6480 (void);
// 0x0000038F System.Void ZXing.PDF417.Internal.BarcodeMatrix::startRow()
extern void BarcodeMatrix_startRow_m055DE6EB3D7DDB8A1F930BB4CB17EB7D024D83BE (void);
// 0x00000390 ZXing.PDF417.Internal.BarcodeRow ZXing.PDF417.Internal.BarcodeMatrix::getCurrentRow()
extern void BarcodeMatrix_getCurrentRow_m2C51A115A9DCFF92897C4E174BEE9A15B0EF5B1B (void);
// 0x00000391 System.SByte[][] ZXing.PDF417.Internal.BarcodeMatrix::getMatrix()
extern void BarcodeMatrix_getMatrix_m0F3658D17C54BB1C81DAF0132A4DC9C0704C22E9 (void);
// 0x00000392 System.SByte[][] ZXing.PDF417.Internal.BarcodeMatrix::getScaledMatrix(System.Int32,System.Int32)
extern void BarcodeMatrix_getScaledMatrix_m357C195E4D1E888FF4CC2A222A008E7C740F5A2F (void);
// 0x00000393 System.Void ZXing.PDF417.Internal.BarcodeRow::.ctor(System.Int32)
extern void BarcodeRow__ctor_m37C2E3D8DE46503396AD0A305C3CB1E90F273891 (void);
// 0x00000394 System.SByte ZXing.PDF417.Internal.BarcodeRow::get_Item(System.Int32)
extern void BarcodeRow_get_Item_m7892C96D2F02D4D440EF7345FF27B0C6699F8842 (void);
// 0x00000395 System.Void ZXing.PDF417.Internal.BarcodeRow::set_Item(System.Int32,System.SByte)
extern void BarcodeRow_set_Item_mBB489746AFC98B730045C87FA8C0451884F9AFF8 (void);
// 0x00000396 System.Void ZXing.PDF417.Internal.BarcodeRow::set(System.Int32,System.Boolean)
extern void BarcodeRow_set_mE5E497A54AF4C7B8DE244D5124F6333DE4E20BC9 (void);
// 0x00000397 System.Void ZXing.PDF417.Internal.BarcodeRow::addBar(System.Boolean,System.Int32)
extern void BarcodeRow_addBar_m051B25AE3638032E21DF84D5BAD84872C1CB0457 (void);
// 0x00000398 System.SByte[] ZXing.PDF417.Internal.BarcodeRow::getScaledRow(System.Int32)
extern void BarcodeRow_getScaledRow_mFD8E8EC92989D81E3B1FA6DB8E31509DDF918801 (void);
// 0x00000399 System.Void ZXing.PDF417.Internal.Dimensions::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Dimensions__ctor_m1113F7DDF01CC6FE92EB2DC5629AFEF6E42AC562 (void);
// 0x0000039A System.Int32 ZXing.PDF417.Internal.Dimensions::get_MinCols()
extern void Dimensions_get_MinCols_m580F2409A2C9C791F8708FEC52E5318C5354E6FE (void);
// 0x0000039B System.Int32 ZXing.PDF417.Internal.Dimensions::get_MaxCols()
extern void Dimensions_get_MaxCols_m2C50370953C5C0984C807D0A5CA0CA563BDCD017 (void);
// 0x0000039C System.Int32 ZXing.PDF417.Internal.Dimensions::get_MinRows()
extern void Dimensions_get_MinRows_m09446D25C3BE695966CE2D456F9BD1F53EE5BD4D (void);
// 0x0000039D System.Int32 ZXing.PDF417.Internal.Dimensions::get_MaxRows()
extern void Dimensions_get_MaxRows_mA2D5B26AFECDA435728FC4574DE05FC0120FE91A (void);
// 0x0000039E System.Void ZXing.PDF417.Internal.PDF417::.ctor()
extern void PDF417__ctor_m2D1F66BE06CA44ACC1C50DC97360EDD8BD162EF0 (void);
// 0x0000039F System.Void ZXing.PDF417.Internal.PDF417::.ctor(System.Boolean)
extern void PDF417__ctor_m8F53FE0E1610185CDBDB1C1E3EB68D92C89133F5 (void);
// 0x000003A0 ZXing.PDF417.Internal.BarcodeMatrix ZXing.PDF417.Internal.PDF417::get_BarcodeMatrix()
extern void PDF417_get_BarcodeMatrix_mD2BA9DD09F45938AE401A391D7CC7BEB79FE8192 (void);
// 0x000003A1 System.Int32 ZXing.PDF417.Internal.PDF417::calculateNumberOfRows(System.Int32,System.Int32,System.Int32)
extern void PDF417_calculateNumberOfRows_mD5C1F20CB7803632BA72ED7716899043F106F378 (void);
// 0x000003A2 System.Int32 ZXing.PDF417.Internal.PDF417::getNumberOfPadCodewords(System.Int32,System.Int32,System.Int32,System.Int32)
extern void PDF417_getNumberOfPadCodewords_m4D5B543E6E8BFE5BB92A620B7F63D6BD8304F461 (void);
// 0x000003A3 System.Void ZXing.PDF417.Internal.PDF417::encodeChar(System.Int32,System.Int32,ZXing.PDF417.Internal.BarcodeRow)
extern void PDF417_encodeChar_m33598EE9026E5B47775357D7D470BA7B29FED36F (void);
// 0x000003A4 System.Void ZXing.PDF417.Internal.PDF417::encodeLowLevel(System.String,System.Int32,System.Int32,System.Int32,ZXing.PDF417.Internal.BarcodeMatrix)
extern void PDF417_encodeLowLevel_m7A71FD34665B4F40BABC635AC4FD81F31DD6A32F (void);
// 0x000003A5 System.Void ZXing.PDF417.Internal.PDF417::generateBarcodeLogic(System.String,System.Int32,System.Int32,System.Int32,System.Int32&)
extern void PDF417_generateBarcodeLogic_m52A0D6F360E3843691AAC8CAEF021995543E60E6 (void);
// 0x000003A6 System.Int32[] ZXing.PDF417.Internal.PDF417::determineDimensions(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32&)
extern void PDF417_determineDimensions_mCFD1E7B2E4E9FCFCFDE96F10162BB34021B9C61C (void);
// 0x000003A7 System.Void ZXing.PDF417.Internal.PDF417::setDesiredAspectRatio(System.Single)
extern void PDF417_setDesiredAspectRatio_m2EEEDD9842C7493404D54A4D9576D4648AFE26F0 (void);
// 0x000003A8 System.Void ZXing.PDF417.Internal.PDF417::setDimensions(System.Int32,System.Int32,System.Int32,System.Int32)
extern void PDF417_setDimensions_m81D43D0E0478FB6FE887F90CEC269C3468953FC0 (void);
// 0x000003A9 System.Void ZXing.PDF417.Internal.PDF417::setCompaction(ZXing.PDF417.Internal.Compaction)
extern void PDF417_setCompaction_m27EA88717186013B204CDB010804AB9E5E2660F0 (void);
// 0x000003AA System.Void ZXing.PDF417.Internal.PDF417::setCompact(System.Boolean)
extern void PDF417_setCompact_m8D5966900B8A1872E749E40E9C2BC8B44F4DC290 (void);
// 0x000003AB System.Void ZXing.PDF417.Internal.PDF417::setEncoding(System.String)
extern void PDF417_setEncoding_m5E1C981C3EF502DF48B6CE934C743A79BC21739F (void);
// 0x000003AC System.Void ZXing.PDF417.Internal.PDF417::setDisableEci(System.Boolean)
extern void PDF417_setDisableEci_m66B50298509B1B219B15E6975B1E6CAC9CAC0C08 (void);
// 0x000003AD System.Void ZXing.PDF417.Internal.PDF417::.cctor()
extern void PDF417__cctor_mDBFBD00A09B42B1EAF3BCF728BDB7C9BD3363437 (void);
// 0x000003AE System.Int32 ZXing.PDF417.Internal.PDF417ErrorCorrection::getErrorCorrectionCodewordCount(System.Int32)
extern void PDF417ErrorCorrection_getErrorCorrectionCodewordCount_m182CF1B790FEB40433456CB47BE7501054545DA7 (void);
// 0x000003AF System.Int32 ZXing.PDF417.Internal.PDF417ErrorCorrection::getErrorCorrectionLevel(System.Int32,System.Int32)
extern void PDF417ErrorCorrection_getErrorCorrectionLevel_m950BFF7FFB747B18F787EDA404DF68300B107653 (void);
// 0x000003B0 System.Int32 ZXing.PDF417.Internal.PDF417ErrorCorrection::getRecommendedMinimumErrorCorrectionLevel(System.Int32)
extern void PDF417ErrorCorrection_getRecommendedMinimumErrorCorrectionLevel_m12EA19EB49A91FB39BE7D40062757F68EBA74117 (void);
// 0x000003B1 System.String ZXing.PDF417.Internal.PDF417ErrorCorrection::generateErrorCorrection(System.String,System.Int32)
extern void PDF417ErrorCorrection_generateErrorCorrection_mEB7203E725BCAF4CAE5DA38A5EBEF5D876BF4763 (void);
// 0x000003B2 System.Void ZXing.PDF417.Internal.PDF417ErrorCorrection::.cctor()
extern void PDF417ErrorCorrection__cctor_m2793F40DD450E7539A948BA9B77377E43857DB21 (void);
// 0x000003B3 System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::.cctor()
extern void PDF417HighLevelEncoder__cctor_mED45AC858EB6DD672448508DA46631D951C76E52 (void);
// 0x000003B4 System.String ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeHighLevel(System.String,ZXing.PDF417.Internal.Compaction,System.Text.Encoding,System.Boolean)
extern void PDF417HighLevelEncoder_encodeHighLevel_m482E99741FBE267C49824536D83C1230A3AAB524 (void);
// 0x000003B5 System.Text.Encoding ZXing.PDF417.Internal.PDF417HighLevelEncoder::getEncoder(System.Text.Encoding)
extern void PDF417HighLevelEncoder_getEncoder_m49689C8000DF02FD41759F54B5CE197C1430BF9F (void);
// 0x000003B6 System.Byte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::toBytes(System.String,System.Text.Encoding)
extern void PDF417HighLevelEncoder_toBytes_m8677D2A91FA1618C6FD780271F7AE77787FF28EE (void);
// 0x000003B7 System.Byte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::toBytes(System.Char,System.Text.Encoding)
extern void PDF417HighLevelEncoder_toBytes_m035D38B1DF6BD4B01F24AAFC4E60B36CE354FE44 (void);
// 0x000003B8 System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeText(System.String,System.Int32,System.Int32,System.Text.StringBuilder,System.Int32)
extern void PDF417HighLevelEncoder_encodeText_m41AA2DE93569779052EEACA6A16689C482503050 (void);
// 0x000003B9 System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeBinary(System.Byte[],System.Int32,System.Int32,System.Int32,System.Text.StringBuilder)
extern void PDF417HighLevelEncoder_encodeBinary_mA3E1609B86A393FA5FC8320DFC13AB111F0BEA08 (void);
// 0x000003BA System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeNumeric(System.String,System.Int32,System.Int32,System.Text.StringBuilder)
extern void PDF417HighLevelEncoder_encodeNumeric_mB134F709D9925E93F123462A0CB3CD8A63CA9087 (void);
// 0x000003BB System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isDigit(System.Char)
extern void PDF417HighLevelEncoder_isDigit_m8B31340B29F9A5BED9CA5CC8B0638C66FFD89B0F (void);
// 0x000003BC System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isAlphaUpper(System.Char)
extern void PDF417HighLevelEncoder_isAlphaUpper_m8F4BD9C63823AF6E4D73B75ED2A883E069299196 (void);
// 0x000003BD System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isAlphaLower(System.Char)
extern void PDF417HighLevelEncoder_isAlphaLower_mB7AD7CF1A6C0E194D1B16E741DCBFAF28251D0F3 (void);
// 0x000003BE System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isMixed(System.Char)
extern void PDF417HighLevelEncoder_isMixed_m07240A3B54A4A96713B0C7CA5D12733EE6425036 (void);
// 0x000003BF System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isPunctuation(System.Char)
extern void PDF417HighLevelEncoder_isPunctuation_m059ED04416EF6482131B1A9D5370A2EC08120252 (void);
// 0x000003C0 System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isText(System.Char)
extern void PDF417HighLevelEncoder_isText_m839743B68A0B60AEC486E15689C75765A37A9FD4 (void);
// 0x000003C1 System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::determineConsecutiveDigitCount(System.String,System.Int32)
extern void PDF417HighLevelEncoder_determineConsecutiveDigitCount_mE36926C8E31E349FE4BD96DDE7609EE9E57622A7 (void);
// 0x000003C2 System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::determineConsecutiveTextCount(System.String,System.Int32)
extern void PDF417HighLevelEncoder_determineConsecutiveTextCount_mCF597FC8C9D8585C4558018ADC5A1867CDBB3C39 (void);
// 0x000003C3 System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::determineConsecutiveBinaryCount(System.String,System.Byte[],System.Int32,System.Text.Encoding,System.Int32&)
extern void PDF417HighLevelEncoder_determineConsecutiveBinaryCount_m35F18E91E07C1BB4036A0172DD8EAFBE1F247F10 (void);
// 0x000003C4 System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodingECI(System.Int32,System.Text.StringBuilder)
extern void PDF417HighLevelEncoder_encodingECI_m93E596FC29CC8DAA55DD3584B918A9D3576AA55A (void);
// 0x000003C5 System.Void ZXing.PDF417.Internal.EC.ErrorCorrection::.ctor()
extern void ErrorCorrection__ctor_mF0AC86ACCFA509DDE4A856601B35B3456231505D (void);
// 0x000003C6 System.Boolean ZXing.PDF417.Internal.EC.ErrorCorrection::decode(System.Int32[],System.Int32,System.Int32[],System.Int32&)
extern void ErrorCorrection_decode_m1089E3BCEF64CCB99A4E7DE4D488BEA41A008026 (void);
// 0x000003C7 ZXing.PDF417.Internal.EC.ModulusPoly[] ZXing.PDF417.Internal.EC.ErrorCorrection::runEuclideanAlgorithm(ZXing.PDF417.Internal.EC.ModulusPoly,ZXing.PDF417.Internal.EC.ModulusPoly,System.Int32)
extern void ErrorCorrection_runEuclideanAlgorithm_m3EFF5753705A530D077A28B1C313CF5C6BAC1489 (void);
// 0x000003C8 System.Int32[] ZXing.PDF417.Internal.EC.ErrorCorrection::findErrorLocations(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ErrorCorrection_findErrorLocations_mEACA2A4EB73B8E75B85859A4AA437ABEBAD8A3C8 (void);
// 0x000003C9 System.Int32[] ZXing.PDF417.Internal.EC.ErrorCorrection::findErrorMagnitudes(ZXing.PDF417.Internal.EC.ModulusPoly,ZXing.PDF417.Internal.EC.ModulusPoly,System.Int32[])
extern void ErrorCorrection_findErrorMagnitudes_mE0294AEBE952B7924D0FE86F6EC5BF89F6D6A305 (void);
// 0x000003CA ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::get_Zero()
extern void ModulusGF_get_Zero_m863E0E297D8952146A1033987BAAAF3B20362D6A (void);
// 0x000003CB System.Void ZXing.PDF417.Internal.EC.ModulusGF::set_Zero(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusGF_set_Zero_m7771EABE3F3AA76773960AF47478F4A578E353F1 (void);
// 0x000003CC ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::get_One()
extern void ModulusGF_get_One_m0855E3521E13DAACB705044BB0FB2AA3C2BF3D49 (void);
// 0x000003CD System.Void ZXing.PDF417.Internal.EC.ModulusGF::set_One(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusGF_set_One_m58E56880F2B41E5CA3F0F80E9D5334FA592A9A16 (void);
// 0x000003CE System.Void ZXing.PDF417.Internal.EC.ModulusGF::.ctor(System.Int32,System.Int32)
extern void ModulusGF__ctor_m5747FD9A82E764BD6886CC2D6AB4BD9B8CC1C73D (void);
// 0x000003CF ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::buildMonomial(System.Int32,System.Int32)
extern void ModulusGF_buildMonomial_m7B6967B736DCDAC0711EAF70E0FFD69E30DFFFB7 (void);
// 0x000003D0 System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::add(System.Int32,System.Int32)
extern void ModulusGF_add_m99B1CD80199ED5607F3C7D0F62D03B429009B0E8 (void);
// 0x000003D1 System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::subtract(System.Int32,System.Int32)
extern void ModulusGF_subtract_mBAC1890BA40C2B104C6F726E2E2F1C705761333E (void);
// 0x000003D2 System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::exp(System.Int32)
extern void ModulusGF_exp_mA2A69B87DE69A0CC46676C5A830227FC4A95D6D8 (void);
// 0x000003D3 System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::log(System.Int32)
extern void ModulusGF_log_m8C3CD9E481DA8DE8E0C6BA9C4FD04EFE262B3682 (void);
// 0x000003D4 System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::inverse(System.Int32)
extern void ModulusGF_inverse_m9047A6009C43D57BF3612EB8CC2B729DEEC8C210 (void);
// 0x000003D5 System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::multiply(System.Int32,System.Int32)
extern void ModulusGF_multiply_m425B2400D37567ACD2138FE13A58D591FE71F962 (void);
// 0x000003D6 System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::get_Size()
extern void ModulusGF_get_Size_mB39AB719BCC1423CD2F979552536CBBD18C80492 (void);
// 0x000003D7 System.Void ZXing.PDF417.Internal.EC.ModulusGF::.cctor()
extern void ModulusGF__cctor_mEF358A95DBB5D70851CFA53476089BFEC259AD4B (void);
// 0x000003D8 System.Void ZXing.PDF417.Internal.EC.ModulusPoly::.ctor(ZXing.PDF417.Internal.EC.ModulusGF,System.Int32[])
extern void ModulusPoly__ctor_m25FE3FC1B5202FD32E10B82800D117962B2D2093 (void);
// 0x000003D9 System.Int32[] ZXing.PDF417.Internal.EC.ModulusPoly::get_Coefficients()
extern void ModulusPoly_get_Coefficients_mCCF5E99E9A6B2217DBA075BB68AC8DF3507BDC5A (void);
// 0x000003DA System.Int32 ZXing.PDF417.Internal.EC.ModulusPoly::get_Degree()
extern void ModulusPoly_get_Degree_m67D79A67D205CB53D827116D2567FBB8679F2624 (void);
// 0x000003DB System.Boolean ZXing.PDF417.Internal.EC.ModulusPoly::get_isZero()
extern void ModulusPoly_get_isZero_m8E5409ECC7C51F38DB75C053851A9AC0B868DA69 (void);
// 0x000003DC System.Int32 ZXing.PDF417.Internal.EC.ModulusPoly::getCoefficient(System.Int32)
extern void ModulusPoly_getCoefficient_m6C022ECC4B92A51C89DB39927E188809C60EAC7A (void);
// 0x000003DD System.Int32 ZXing.PDF417.Internal.EC.ModulusPoly::evaluateAt(System.Int32)
extern void ModulusPoly_evaluateAt_m8FD3AB342A478A924B9A1F3EDAC0FC397AD5F16F (void);
// 0x000003DE ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::add(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusPoly_add_mC7938D09C20B77C6732F3AEC5D92E22BA6B1029B (void);
// 0x000003DF ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::subtract(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusPoly_subtract_mED658EEB9DEA2CB830DD7C1529308A933A9787DF (void);
// 0x000003E0 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::multiply(ZXing.PDF417.Internal.EC.ModulusPoly)
extern void ModulusPoly_multiply_m3F3662B4812C05A0830796A3142DAE6C16D06B70 (void);
// 0x000003E1 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::getNegative()
extern void ModulusPoly_getNegative_m636F7115F47A3DC1FD2FD9DAF6EBB73FE7C11675 (void);
// 0x000003E2 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::multiply(System.Int32)
extern void ModulusPoly_multiply_m63B561E5A1A1789F8AB99499B30E5F352859F87B (void);
// 0x000003E3 ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusPoly::multiplyByMonomial(System.Int32,System.Int32)
extern void ModulusPoly_multiplyByMonomial_m8E0E2453137AABEB75E234F5EE6451C27DF0868E (void);
// 0x000003E4 System.String ZXing.PDF417.Internal.EC.ModulusPoly::ToString()
extern void ModulusPoly_ToString_mB713218C8814B1FA88292F9EB2324A2B35934FC5 (void);
// 0x000003E5 System.Void ZXing.OneD.CodaBarReader::.ctor()
extern void CodaBarReader__ctor_m3BEA06B4996710156CA5D513E5664112A25FE2B8 (void);
// 0x000003E6 ZXing.Result ZXing.OneD.CodaBarReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void CodaBarReader_decodeRow_m94BC56D21EB2729468619AB7C4ECA0A122865868 (void);
// 0x000003E7 System.Boolean ZXing.OneD.CodaBarReader::validatePattern(System.Int32)
extern void CodaBarReader_validatePattern_mD24E9887BB98FECFE00F0E03E50C398410D242A2 (void);
// 0x000003E8 System.Boolean ZXing.OneD.CodaBarReader::setCounters(ZXing.Common.BitArray)
extern void CodaBarReader_setCounters_m1056F8DC042F325B7CE66C74487F83CC8CAEF0A0 (void);
// 0x000003E9 System.Void ZXing.OneD.CodaBarReader::counterAppend(System.Int32)
extern void CodaBarReader_counterAppend_m84A441A4DC31F86F75EE5CECC3B3CDB6AEA48C72 (void);
// 0x000003EA System.Int32 ZXing.OneD.CodaBarReader::findStartPattern()
extern void CodaBarReader_findStartPattern_m235D5451C9C2D12C0DB780FDC9E074F35BBBFDBA (void);
// 0x000003EB System.Boolean ZXing.OneD.CodaBarReader::arrayContains(System.Char[],System.Char)
extern void CodaBarReader_arrayContains_m05D85B2AC1DB1431392C43D052100A74CA02819A (void);
// 0x000003EC System.Int32 ZXing.OneD.CodaBarReader::toNarrowWidePattern(System.Int32)
extern void CodaBarReader_toNarrowWidePattern_m246B2E1A57E2534EA41509E8A878815B26C9FB59 (void);
// 0x000003ED System.Void ZXing.OneD.CodaBarReader::.cctor()
extern void CodaBarReader__cctor_mCF60F4D0B8386A047014F56931815F249F434B2D (void);
// 0x000003EE System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.CodaBarWriter::get_SupportedWriteFormats()
extern void CodaBarWriter_get_SupportedWriteFormats_mFB097B05365959C1CF08E693364D0EE5194DED58 (void);
// 0x000003EF System.Boolean[] ZXing.OneD.CodaBarWriter::encode(System.String)
extern void CodaBarWriter_encode_mE886A588978F1218B5F1F544113F8C4CB8F4B425 (void);
// 0x000003F0 System.Void ZXing.OneD.CodaBarWriter::.ctor()
extern void CodaBarWriter__ctor_m51A18492D97F3238D6F929A99395C376A56996CD (void);
// 0x000003F1 System.Void ZXing.OneD.CodaBarWriter::.cctor()
extern void CodaBarWriter__cctor_mD4880D7425E3A4294EB6AC639090DCAD73B265BB (void);
// 0x000003F2 System.Boolean ZXing.OneD.Code128EncodingOptions::get_ForceCodesetB()
extern void Code128EncodingOptions_get_ForceCodesetB_mC56272CBFC9BCAA855CEF2314298F7106C1B80E0 (void);
// 0x000003F3 System.Void ZXing.OneD.Code128EncodingOptions::set_ForceCodesetB(System.Boolean)
extern void Code128EncodingOptions_set_ForceCodesetB_m5B0299C63DEA04770F92F3B94B00326F5AE91D1A (void);
// 0x000003F4 System.Void ZXing.OneD.Code128EncodingOptions::.ctor()
extern void Code128EncodingOptions__ctor_m028F25714DB73AEA6AC5F78A4E1FC4F85EB60EDC (void);
// 0x000003F5 System.Int32[] ZXing.OneD.Code128Reader::findStartPattern(ZXing.Common.BitArray)
extern void Code128Reader_findStartPattern_m8BE7E46342579879675FAA1C6B28A210AA2BF337 (void);
// 0x000003F6 System.Boolean ZXing.OneD.Code128Reader::decodeCode(ZXing.Common.BitArray,System.Int32[],System.Int32,System.Int32&)
extern void Code128Reader_decodeCode_mD5329CE5D63BBF7BD26B0C3169FD1A4071125387 (void);
// 0x000003F7 ZXing.Result ZXing.OneD.Code128Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Code128Reader_decodeRow_mB671902EEDA2484C8AD8A60CE6577877CCFBD01B (void);
// 0x000003F8 System.Void ZXing.OneD.Code128Reader::.ctor()
extern void Code128Reader__ctor_m0B01C09151CD3ADBFDAD0385FC892025E05D6D24 (void);
// 0x000003F9 System.Void ZXing.OneD.Code128Reader::.cctor()
extern void Code128Reader__cctor_m63B729960536D600D1053A3F54109A56470161D1 (void);
// 0x000003FA System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.Code128Writer::get_SupportedWriteFormats()
extern void Code128Writer_get_SupportedWriteFormats_m4E94A0424E71611B6EBBDCE376370E624218E882 (void);
// 0x000003FB ZXing.Common.BitMatrix ZXing.OneD.Code128Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void Code128Writer_encode_m51A63FAF506ECE8FBF6278FDEE1F9DDCD27AC1E8 (void);
// 0x000003FC System.Boolean[] ZXing.OneD.Code128Writer::encode(System.String)
extern void Code128Writer_encode_m946DF90812EE9004BA6C77FBF32730D80C80CF25 (void);
// 0x000003FD ZXing.OneD.Code128Writer/CType ZXing.OneD.Code128Writer::findCType(System.String,System.Int32)
extern void Code128Writer_findCType_m73D3F8C2576514CA23981EB6079849F6251DC345 (void);
// 0x000003FE System.Int32 ZXing.OneD.Code128Writer::chooseCode(System.String,System.Int32,System.Int32)
extern void Code128Writer_chooseCode_mDD7C044CFCA7E998A34B9E86AD333EF8D4194304 (void);
// 0x000003FF System.Void ZXing.OneD.Code128Writer::.ctor()
extern void Code128Writer__ctor_m03C0D410B67030AD59FD2078C89EE6D2B8C32B02 (void);
// 0x00000400 System.Void ZXing.OneD.Code128Writer::.cctor()
extern void Code128Writer__cctor_m85F977AA7EDDB7E1271D94943301022718B49531 (void);
// 0x00000401 System.String ZXing.OneD.Code39Reader::get_Alphabet()
extern void Code39Reader_get_Alphabet_m4F82DDD17D865C85A796B4174B372D07AA16476E (void);
// 0x00000402 System.Void ZXing.OneD.Code39Reader::.ctor()
extern void Code39Reader__ctor_mE2A4C036D0071280910B2CACE7F10C7A539A1DC1 (void);
// 0x00000403 System.Void ZXing.OneD.Code39Reader::.ctor(System.Boolean)
extern void Code39Reader__ctor_m79C4AC6791A1D5104B1AE99F78F0BB3AC17F158C (void);
// 0x00000404 System.Void ZXing.OneD.Code39Reader::.ctor(System.Boolean,System.Boolean)
extern void Code39Reader__ctor_mF5EB3BA77062694C2FB251A2698074AECE818EF3 (void);
// 0x00000405 ZXing.Result ZXing.OneD.Code39Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Code39Reader_decodeRow_mFB6F83D4DD66A04458EEC597B984547042FBCCEE (void);
// 0x00000406 System.Int32[] ZXing.OneD.Code39Reader::findAsteriskPattern(ZXing.Common.BitArray,System.Int32[])
extern void Code39Reader_findAsteriskPattern_mD1E73F37DB210DC50C996E5B7CC2EF7BF9A021C8 (void);
// 0x00000407 System.Int32 ZXing.OneD.Code39Reader::toNarrowWidePattern(System.Int32[])
extern void Code39Reader_toNarrowWidePattern_m189754A5668BB134015580C44BB6792F42EA45E4 (void);
// 0x00000408 System.Boolean ZXing.OneD.Code39Reader::patternToChar(System.Int32,System.Char&)
extern void Code39Reader_patternToChar_m780CFCE8C7B9B6FC0F50BF61FE35DAD3A95C2E47 (void);
// 0x00000409 System.String ZXing.OneD.Code39Reader::decodeExtended(System.String)
extern void Code39Reader_decodeExtended_m9CE4355C87FF553ED4936DB2A552E9182BADBE49 (void);
// 0x0000040A System.Void ZXing.OneD.Code39Reader::.cctor()
extern void Code39Reader__cctor_m683DA26FE3240119861C4BC42B6D3FC013F459DA (void);
// 0x0000040B System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.Code39Writer::get_SupportedWriteFormats()
extern void Code39Writer_get_SupportedWriteFormats_mA2BC915438E6962D292413F57B2FCA5F2DA5914A (void);
// 0x0000040C System.Boolean[] ZXing.OneD.Code39Writer::encode(System.String)
extern void Code39Writer_encode_mC6C64C5C4D547DE2926C3C14F77ADFF0A63022DA (void);
// 0x0000040D System.Void ZXing.OneD.Code39Writer::toIntArray(System.Int32,System.Int32[])
extern void Code39Writer_toIntArray_mDC5FDD25AD2FB3A03AB5A7040691890B867E1FFC (void);
// 0x0000040E System.String ZXing.OneD.Code39Writer::tryToConvertToExtendedMode(System.String)
extern void Code39Writer_tryToConvertToExtendedMode_m37099563E131D7D560A225AF9308114791755FCB (void);
// 0x0000040F System.Void ZXing.OneD.Code39Writer::.ctor()
extern void Code39Writer__ctor_m32AB26CD681085CFEB302F065055F32B05202D13 (void);
// 0x00000410 System.Void ZXing.OneD.Code39Writer::.cctor()
extern void Code39Writer__cctor_mEEEBFFB404983081171A016FFDDB4D570D017D06 (void);
// 0x00000411 System.Void ZXing.OneD.Code93Reader::.ctor()
extern void Code93Reader__ctor_mD104B27A7B99357604ED3266F4E9B14F7288D7F9 (void);
// 0x00000412 ZXing.Result ZXing.OneD.Code93Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Code93Reader_decodeRow_m12943EEC5E6DE93A01C2BE48E36DB7729FDF3B81 (void);
// 0x00000413 System.Int32[] ZXing.OneD.Code93Reader::findAsteriskPattern(ZXing.Common.BitArray)
extern void Code93Reader_findAsteriskPattern_m723184C62F219710E38BCC5395FB2A4A510B9735 (void);
// 0x00000414 System.Int32 ZXing.OneD.Code93Reader::toPattern(System.Int32[])
extern void Code93Reader_toPattern_mC8FBC15F8644382A00420116075C5DC92150024F (void);
// 0x00000415 System.Boolean ZXing.OneD.Code93Reader::patternToChar(System.Int32,System.Char&)
extern void Code93Reader_patternToChar_mE0D4B7D07AAE60772BA9E1A719CC49668AC8330C (void);
// 0x00000416 System.String ZXing.OneD.Code93Reader::decodeExtended(System.Text.StringBuilder)
extern void Code93Reader_decodeExtended_m28B866D7E254463A346B9D0B676A4BF4D9ACE05F (void);
// 0x00000417 System.Boolean ZXing.OneD.Code93Reader::checkChecksums(System.Text.StringBuilder)
extern void Code93Reader_checkChecksums_m040416DE7B98ECE9C777A2EDB7CFD6418DE7B683 (void);
// 0x00000418 System.Boolean ZXing.OneD.Code93Reader::checkOneChecksum(System.Text.StringBuilder,System.Int32,System.Int32)
extern void Code93Reader_checkOneChecksum_mDF2707754E965BCA0BDB275A7E3FB40973AA7B8A (void);
// 0x00000419 System.Void ZXing.OneD.Code93Reader::.cctor()
extern void Code93Reader__cctor_m113446777B0810F256D93B3CA9AF82D4CFB30E8D (void);
// 0x0000041A System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.Code93Writer::get_SupportedWriteFormats()
extern void Code93Writer_get_SupportedWriteFormats_m98FF58A6366F453175D2C987425CF403ABF43C9D (void);
// 0x0000041B System.Boolean[] ZXing.OneD.Code93Writer::encode(System.String)
extern void Code93Writer_encode_m6FC094DE1E0B87B8EE7663F460F7E102557B46F7 (void);
// 0x0000041C System.Int32 ZXing.OneD.Code93Writer::appendPattern(System.Boolean[],System.Int32,System.Int32[],System.Boolean)
extern void Code93Writer_appendPattern_mF631EE31B3857808711A9D1B75BFFCFCD2373DE7 (void);
// 0x0000041D System.Int32 ZXing.OneD.Code93Writer::appendPattern(System.Boolean[],System.Int32,System.Int32)
extern void Code93Writer_appendPattern_m0A28A95F2265F2FC54C262F5CEAA5F49AC899CDE (void);
// 0x0000041E System.Int32 ZXing.OneD.Code93Writer::computeChecksumIndex(System.String,System.Int32)
extern void Code93Writer_computeChecksumIndex_m7AB33F564C3841EB67D4292A0092CC4D25C7EBE5 (void);
// 0x0000041F System.String ZXing.OneD.Code93Writer::convertToExtended(System.String)
extern void Code93Writer_convertToExtended_mC97764F2A89FB1A5BB8FAE1BA8E7304FF6EBFBAE (void);
// 0x00000420 System.Void ZXing.OneD.Code93Writer::.ctor()
extern void Code93Writer__ctor_mDAC91EC164DDCE2D295F57384EE110471CF971AE (void);
// 0x00000421 System.Void ZXing.OneD.Code93Writer::.cctor()
extern void Code93Writer__cctor_mA44AC9D3211484785CC5AB02CCC077FF87ABEF4E (void);
// 0x00000422 System.Void ZXing.OneD.EAN13Reader::.ctor()
extern void EAN13Reader__ctor_m22D6E926FA4F232637A64ADB918C8C68CB1A1435 (void);
// 0x00000423 System.Int32 ZXing.OneD.EAN13Reader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void EAN13Reader_decodeMiddle_mE80E5BD6784144251320D86694F19F865B7D7E18 (void);
// 0x00000424 ZXing.BarcodeFormat ZXing.OneD.EAN13Reader::get_BarcodeFormat()
extern void EAN13Reader_get_BarcodeFormat_m6E31601906454EDCE34E277224C206ED17C06282 (void);
// 0x00000425 System.Boolean ZXing.OneD.EAN13Reader::determineFirstDigit(System.Text.StringBuilder,System.Int32)
extern void EAN13Reader_determineFirstDigit_mB926FCB753C9E158BD4BE779891764A94A76C85E (void);
// 0x00000426 System.Void ZXing.OneD.EAN13Reader::.cctor()
extern void EAN13Reader__cctor_mCC2C0034CE113625E5529941C6AE21E3A8C51639 (void);
// 0x00000427 System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.EAN13Writer::get_SupportedWriteFormats()
extern void EAN13Writer_get_SupportedWriteFormats_mF86E0477490E7DE8254C50FAED51EAA12CDFA113 (void);
// 0x00000428 System.Boolean[] ZXing.OneD.EAN13Writer::encode(System.String)
extern void EAN13Writer_encode_m0A0284D6588CD40EDFB87080C6DC05870E2B4116 (void);
// 0x00000429 System.Void ZXing.OneD.EAN13Writer::.ctor()
extern void EAN13Writer__ctor_m9C4D4AED6A2EEE42E66EC9D40D993DAACF6FCC39 (void);
// 0x0000042A System.Void ZXing.OneD.EAN13Writer::.cctor()
extern void EAN13Writer__cctor_m6926480610FD72797D09269A6D667C2C9F719E19 (void);
// 0x0000042B System.Void ZXing.OneD.EAN8Reader::.ctor()
extern void EAN8Reader__ctor_m6E3B814D52B0A5ED2E9ABE664362C8668915EB96 (void);
// 0x0000042C System.Int32 ZXing.OneD.EAN8Reader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void EAN8Reader_decodeMiddle_mEF5AE8CDB21E07AD5FA0D2ED1CF7E2F906919030 (void);
// 0x0000042D ZXing.BarcodeFormat ZXing.OneD.EAN8Reader::get_BarcodeFormat()
extern void EAN8Reader_get_BarcodeFormat_mF28C56A8411A4DCA74A565A7EF1586527A9FC1A3 (void);
// 0x0000042E System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.EAN8Writer::get_SupportedWriteFormats()
extern void EAN8Writer_get_SupportedWriteFormats_mB4F1BF6257A7E850EADB7B2A38BEA35A9D17EF7E (void);
// 0x0000042F System.Boolean[] ZXing.OneD.EAN8Writer::encode(System.String)
extern void EAN8Writer_encode_m43B7560E5C695A6EF5AEEA40AE2A65819E538A38 (void);
// 0x00000430 System.Void ZXing.OneD.EAN8Writer::.ctor()
extern void EAN8Writer__ctor_m030610A728505DF891C32E0F5C997BFD5046B643 (void);
// 0x00000431 System.Void ZXing.OneD.EAN8Writer::.cctor()
extern void EAN8Writer__cctor_m21AF1FB5C8D8AFFC3650EB8A2D981C1B92E0FDD9 (void);
// 0x00000432 System.String ZXing.OneD.EANManufacturerOrgSupport::lookupCountryIdentifier(System.String)
extern void EANManufacturerOrgSupport_lookupCountryIdentifier_m47B81C7F764371EDFC05F46F9E2D1BF813C56008 (void);
// 0x00000433 System.Void ZXing.OneD.EANManufacturerOrgSupport::add(System.Int32[],System.String)
extern void EANManufacturerOrgSupport_add_m969371E77A295DCE664C4376816605E45954F1D6 (void);
// 0x00000434 System.Void ZXing.OneD.EANManufacturerOrgSupport::initIfNeeded()
extern void EANManufacturerOrgSupport_initIfNeeded_mD83A5BA9701C07D6312FAA1A73537F7A9FF59179 (void);
// 0x00000435 System.Void ZXing.OneD.EANManufacturerOrgSupport::.ctor()
extern void EANManufacturerOrgSupport__ctor_m3F9E01BC9025941B93E9F70DDA97AB5804C50A0B (void);
// 0x00000436 ZXing.Result ZXing.OneD.ITFReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void ITFReader_decodeRow_m22E033F6D0433161615F4553AD6D83FBD088680A (void);
// 0x00000437 System.Boolean ZXing.OneD.ITFReader::decodeMiddle(ZXing.Common.BitArray,System.Int32,System.Int32,System.Text.StringBuilder)
extern void ITFReader_decodeMiddle_mECC4A1B9438B016100648650EAD9C048F6445343 (void);
// 0x00000438 System.Int32[] ZXing.OneD.ITFReader::decodeStart(ZXing.Common.BitArray)
extern void ITFReader_decodeStart_m6AE1F290E6456DAAE138E03ACA290BC8243F74B5 (void);
// 0x00000439 System.Boolean ZXing.OneD.ITFReader::validateQuietZone(ZXing.Common.BitArray,System.Int32)
extern void ITFReader_validateQuietZone_m31442A2E97AD81270AAD0CA301FDF8645C1E6377 (void);
// 0x0000043A System.Int32 ZXing.OneD.ITFReader::skipWhiteSpace(ZXing.Common.BitArray)
extern void ITFReader_skipWhiteSpace_mFF7E082544C33FC9256EAA3AA6AED862317DF4DB (void);
// 0x0000043B System.Int32[] ZXing.OneD.ITFReader::decodeEnd(ZXing.Common.BitArray)
extern void ITFReader_decodeEnd_m90406A2EA906E16EECBA65DABE4ABBBDD780CC69 (void);
// 0x0000043C System.Int32[] ZXing.OneD.ITFReader::findGuardPattern(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern void ITFReader_findGuardPattern_m9CB621D4D8F5229E0A46CC0FA65940B20AA18A2A (void);
// 0x0000043D System.Boolean ZXing.OneD.ITFReader::decodeDigit(System.Int32[],System.Int32&)
extern void ITFReader_decodeDigit_mE56B7EE512CB25E6A1CE92ABD658705C6FE76E23 (void);
// 0x0000043E System.Void ZXing.OneD.ITFReader::.ctor()
extern void ITFReader__ctor_mFD51E19DBD8BC4E9D0269EFC658AAB9FE0A7D472 (void);
// 0x0000043F System.Void ZXing.OneD.ITFReader::.cctor()
extern void ITFReader__cctor_mCD1E2778CF9172F7F81225CE8D9E16076C0087FC (void);
// 0x00000440 System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.ITFWriter::get_SupportedWriteFormats()
extern void ITFWriter_get_SupportedWriteFormats_m7C33CF8AA79E6E8C12A986D579081DCFC6184277 (void);
// 0x00000441 System.Boolean[] ZXing.OneD.ITFWriter::encode(System.String)
extern void ITFWriter_encode_mFCC08A914DA9C560B107F84782432432E880ABBD (void);
// 0x00000442 System.Void ZXing.OneD.ITFWriter::.ctor()
extern void ITFWriter__ctor_m609B819419AF1081E7E5666AADE5E5676FBBDB7E (void);
// 0x00000443 System.Void ZXing.OneD.ITFWriter::.cctor()
extern void ITFWriter__cctor_mC163DDC775F5AFB7E978CF7A9FD961903560D26F (void);
// 0x00000444 System.Void ZXing.OneD.MSIReader::.ctor()
extern void MSIReader__ctor_m8E09C6EC273D2D5A52DCDBE0936251B26644ACE9 (void);
// 0x00000445 System.Void ZXing.OneD.MSIReader::.ctor(System.Boolean)
extern void MSIReader__ctor_mF6202DFD5B5572595FD81DFF374FFA4B171EBE0A (void);
// 0x00000446 ZXing.Result ZXing.OneD.MSIReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MSIReader_decodeRow_m2C22FB1AFF62E312556F2D92DBABF03E1217B0E8 (void);
// 0x00000447 System.Int32[] ZXing.OneD.MSIReader::findStartPattern(ZXing.Common.BitArray,System.Int32[])
extern void MSIReader_findStartPattern_m57189F8044E2EDFDEF0EC8203B5C2E642E536B41 (void);
// 0x00000448 System.Int32[] ZXing.OneD.MSIReader::findEndPattern(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern void MSIReader_findEndPattern_m9C07665F4FDDA340895959B15CD03883C54C0472 (void);
// 0x00000449 System.Void ZXing.OneD.MSIReader::calculateAverageCounterWidth(System.Int32[],System.Int32)
extern void MSIReader_calculateAverageCounterWidth_m1783EB674921CDABF86385772B87D25B16CE5EC7 (void);
// 0x0000044A System.Int32 ZXing.OneD.MSIReader::toPattern(System.Int32[],System.Int32)
extern void MSIReader_toPattern_mCF6D7F8F8C5567C8584BD46ACEA28EBDB3C64D90 (void);
// 0x0000044B System.Boolean ZXing.OneD.MSIReader::patternToChar(System.Int32,System.Char&)
extern void MSIReader_patternToChar_mC0363ECBD351636D44E9116F95B1D1CED7ED8BBB (void);
// 0x0000044C System.Int32 ZXing.OneD.MSIReader::CalculateChecksumLuhn(System.String)
extern void MSIReader_CalculateChecksumLuhn_mBB8FBB9E7CA179870E6D7021704DC19BFB51F7C2 (void);
// 0x0000044D System.Void ZXing.OneD.MSIReader::.cctor()
extern void MSIReader__cctor_mC0C84786D1B8F65D243C3F1D733E272A90BA5480 (void);
// 0x0000044E System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.MSIWriter::get_SupportedWriteFormats()
extern void MSIWriter_get_SupportedWriteFormats_m86D821A50C6B332101D87F103821DE1E50DF5AC8 (void);
// 0x0000044F System.Boolean[] ZXing.OneD.MSIWriter::encode(System.String)
extern void MSIWriter_encode_mD0BF92D8A76112216305FE8371578955C4162232 (void);
// 0x00000450 System.Void ZXing.OneD.MSIWriter::.ctor()
extern void MSIWriter__ctor_mBEC018413E5CF3A2A852A609AD45FA917E6DC6E1 (void);
// 0x00000451 System.Void ZXing.OneD.MSIWriter::.cctor()
extern void MSIWriter__cctor_m9828D66C74D5CBD16107995DA76165C202C5CC1E (void);
// 0x00000452 System.Void ZXing.OneD.MultiFormatOneDReader::.ctor(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatOneDReader__ctor_mA7FB46F719134F412591B0F837E59EF6275284CF (void);
// 0x00000453 ZXing.Result ZXing.OneD.MultiFormatOneDReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatOneDReader_decodeRow_m142432FB5BC25CB5705FE21BD103B0B27D786FBB (void);
// 0x00000454 System.Void ZXing.OneD.MultiFormatOneDReader::reset()
extern void MultiFormatOneDReader_reset_m210731053461A4410FF44486C7D9B5190D28EAD2 (void);
// 0x00000455 System.Void ZXing.OneD.MultiFormatUPCEANReader::.ctor(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatUPCEANReader__ctor_m804B8AAC5D80FAD4F6F9CFF44B834318EC0FC6ED (void);
// 0x00000456 ZXing.Result ZXing.OneD.MultiFormatUPCEANReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFormatUPCEANReader_decodeRow_m1F2B5088C1FA7C726B5C0A6D9F3A492244E5DE7C (void);
// 0x00000457 System.Void ZXing.OneD.MultiFormatUPCEANReader::reset()
extern void MultiFormatUPCEANReader_reset_mB67FD3C8C06EFEC7B9EC305F10FE6656DC3FBB65 (void);
// 0x00000458 System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.OneDimensionalCodeWriter::get_SupportedWriteFormats()
// 0x00000459 ZXing.Common.BitMatrix ZXing.OneD.OneDimensionalCodeWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32)
extern void OneDimensionalCodeWriter_encode_mD71076189BD3C9F57C8C0B9AD38325591C70CA38 (void);
// 0x0000045A ZXing.Common.BitMatrix ZXing.OneD.OneDimensionalCodeWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void OneDimensionalCodeWriter_encode_m362F7E046D1F23863719F9789C0013D5E34E4A78 (void);
// 0x0000045B ZXing.Common.BitMatrix ZXing.OneD.OneDimensionalCodeWriter::renderResult(System.Boolean[],System.Int32,System.Int32,System.Int32)
extern void OneDimensionalCodeWriter_renderResult_m6E22D5CD5A7C52F4FEC963D9DE1423E2AC4F5EC8 (void);
// 0x0000045C System.Void ZXing.OneD.OneDimensionalCodeWriter::checkNumeric(System.String)
extern void OneDimensionalCodeWriter_checkNumeric_m25DA6B8DF0D43D0697EA7398C8B51EE3040A4958 (void);
// 0x0000045D System.Int32 ZXing.OneD.OneDimensionalCodeWriter::appendPattern(System.Boolean[],System.Int32,System.Int32[],System.Boolean)
extern void OneDimensionalCodeWriter_appendPattern_mA9B29292D329CFD3608783AE81DDF03E545FBAF1 (void);
// 0x0000045E System.Int32 ZXing.OneD.OneDimensionalCodeWriter::get_DefaultMargin()
extern void OneDimensionalCodeWriter_get_DefaultMargin_mCC735551FA33496906924C3439AAFDABEDD8A6AF (void);
// 0x0000045F System.Boolean[] ZXing.OneD.OneDimensionalCodeWriter::encode(System.String)
// 0x00000460 System.String ZXing.OneD.OneDimensionalCodeWriter::CalculateChecksumDigitModulo10(System.String)
extern void OneDimensionalCodeWriter_CalculateChecksumDigitModulo10_m53712C80DD6DDE42B2BB32FA36CF6568A0B4474E (void);
// 0x00000461 System.Void ZXing.OneD.OneDimensionalCodeWriter::.ctor()
extern void OneDimensionalCodeWriter__ctor_m67DD81464EEC4332E8A5641591441EE922327B81 (void);
// 0x00000462 System.Void ZXing.OneD.OneDimensionalCodeWriter::.cctor()
extern void OneDimensionalCodeWriter__cctor_m31C8768C1A0F673F848F57B53CEF5AB075A5CE7A (void);
// 0x00000463 ZXing.Result ZXing.OneD.OneDReader::decode(ZXing.BinaryBitmap)
extern void OneDReader_decode_m0D7756159D80880AFFC4F88CBAE45F91795E73CB (void);
// 0x00000464 ZXing.Result ZXing.OneD.OneDReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void OneDReader_decode_m914C584C3CDF37B1C5339FA196CF318102FE512D (void);
// 0x00000465 System.Void ZXing.OneD.OneDReader::reset()
extern void OneDReader_reset_m512511FE6DF0807BA98EAC80D2AE44E1B3CFDB26 (void);
// 0x00000466 ZXing.Result ZXing.OneD.OneDReader::doDecode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void OneDReader_doDecode_m8F0D3580006D9D4E3974BE0FFF085FEDD63ACD86 (void);
// 0x00000467 System.Boolean ZXing.OneD.OneDReader::recordPattern(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern void OneDReader_recordPattern_m36231F2E6967A5F110AC144CDC7F66821E881F15 (void);
// 0x00000468 System.Boolean ZXing.OneD.OneDReader::recordPattern(ZXing.Common.BitArray,System.Int32,System.Int32[],System.Int32)
extern void OneDReader_recordPattern_m3261EDD976AF08CC3D5EBCF5AD0D6AF70C9BD722 (void);
// 0x00000469 System.Boolean ZXing.OneD.OneDReader::recordPatternInReverse(ZXing.Common.BitArray,System.Int32,System.Int32[])
extern void OneDReader_recordPatternInReverse_mB51AFCA11C01A7A0362E776C7EC97763AD2D030A (void);
// 0x0000046A System.Int32 ZXing.OneD.OneDReader::patternMatchVariance(System.Int32[],System.Int32[],System.Int32)
extern void OneDReader_patternMatchVariance_m8D5A7E8EDD59EACDF5F6F6C11B6478148B023DDC (void);
// 0x0000046B ZXing.Result ZXing.OneD.OneDReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
// 0x0000046C System.Void ZXing.OneD.OneDReader::.ctor()
extern void OneDReader__ctor_mDCBD4E0D37075143C0AB48A5297BDC70E8D694AF (void);
// 0x0000046D System.Void ZXing.OneD.OneDReader::.cctor()
extern void OneDReader__cctor_mB688BF74A4957B7A1C647D13F9566BF44D33243A (void);
// 0x0000046E System.Double ZXing.OneD.PharmaCodeReader::mean(System.Double[])
extern void PharmaCodeReader_mean_m2B7BFB78DE54F8787CA2011C36D7779BF97C64D2 (void);
// 0x0000046F ZXing.Result ZXing.OneD.PharmaCodeReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void PharmaCodeReader_decodeRow_m716B6B8DF47E51841A2DB3BC3ABC7EAE351C67CC (void);
// 0x00000470 System.Nullable`1<System.Int32> ZXing.OneD.PharmaCodeReader::finalProcessing(System.Collections.Generic.List`1<ZXing.OneD.PharmaCodeReader/PixelInterval>)
extern void PharmaCodeReader_finalProcessing_mAF72DD4B1D3D97E7FBBFFD43C0FAC1E35D437108 (void);
// 0x00000471 System.Void ZXing.OneD.PharmaCodeReader::.ctor()
extern void PharmaCodeReader__ctor_m15CA9D2962AE0D136E61FF8BA6DEF2E6842C75F1 (void);
// 0x00000472 System.Void ZXing.OneD.PharmaCodeReader::.cctor()
extern void PharmaCodeReader__cctor_m0DACF042E0928CD4727BF1EA9D16120DFD82ACCB (void);
// 0x00000473 System.Void ZXing.OneD.PharmaCodeReader/PixelInterval::.ctor(System.Boolean,System.Int32)
extern void PixelInterval__ctor_m3ED84D1259A738FC06CE08339A886CCAB16B8C9C (void);
// 0x00000474 System.Boolean ZXing.OneD.PharmaCodeReader/PixelInterval::get_Color()
extern void PixelInterval_get_Color_m439B9190309D6A4251CC8805CB27FEC320C363C6 (void);
// 0x00000475 System.Void ZXing.OneD.PharmaCodeReader/PixelInterval::set_Color(System.Boolean)
extern void PixelInterval_set_Color_mC220877A724E4FCBB78D8E52A1BA00306EEE430A (void);
// 0x00000476 System.Int32 ZXing.OneD.PharmaCodeReader/PixelInterval::get_Length()
extern void PixelInterval_get_Length_m00B95F4A6BC347B50E9989B79F514373B8E31B37 (void);
// 0x00000477 System.Void ZXing.OneD.PharmaCodeReader/PixelInterval::set_Length(System.Int32)
extern void PixelInterval_set_Length_m1370FA226169CCC71DAE6731DAA83B3F6F82EE1C (void);
// 0x00000478 System.Int32 ZXing.OneD.PharmaCodeReader/PixelInterval::get_Similar()
extern void PixelInterval_get_Similar_m524C9D1ABBE9D611ED9F4398631AC092E9D5E850 (void);
// 0x00000479 System.Void ZXing.OneD.PharmaCodeReader/PixelInterval::set_Similar(System.Int32)
extern void PixelInterval_set_Similar_mEF2DE4A008093D8736D4FB2D7669ECEC3C4F0D6A (void);
// 0x0000047A System.Int32 ZXing.OneD.PharmaCodeReader/PixelInterval::get_Small()
extern void PixelInterval_get_Small_mD9E36F141851F9B14E652D208AB58EF7D7D9E686 (void);
// 0x0000047B System.Void ZXing.OneD.PharmaCodeReader/PixelInterval::set_Small(System.Int32)
extern void PixelInterval_set_Small_m4BBED5287169F46B3E144E0BCF9CACCD9A3AA70A (void);
// 0x0000047C System.Int32 ZXing.OneD.PharmaCodeReader/PixelInterval::get_Large()
extern void PixelInterval_get_Large_m0E4A27AC439AD47B106F5E83D9FE63B91E321283 (void);
// 0x0000047D System.Void ZXing.OneD.PharmaCodeReader/PixelInterval::set_Large(System.Int32)
extern void PixelInterval_set_Large_m856F3990583C8D8F877EFDA6CD2BB6703B6AD3C0 (void);
// 0x0000047E System.Void ZXing.OneD.PharmaCodeReader/PixelInterval::incSimilar()
extern void PixelInterval_incSimilar_m043A80ED43CF8AE588F35D6EE1A7F0F5E6EB5886 (void);
// 0x0000047F System.Void ZXing.OneD.PharmaCodeReader/PixelInterval::incSmall()
extern void PixelInterval_incSmall_mC1067C2683E372B3D04745406C8CC3C701CBDA0E (void);
// 0x00000480 System.Void ZXing.OneD.PharmaCodeReader/PixelInterval::incLarge()
extern void PixelInterval_incLarge_m9EF5E32389201B6FD51E1F4FA4FA8E35F96FD4F1 (void);
// 0x00000481 System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.PlesseyWriter::get_SupportedWriteFormats()
extern void PlesseyWriter_get_SupportedWriteFormats_mCDD5C60088F578EAF7C3061665EBCD53B70DE655 (void);
// 0x00000482 System.Boolean[] ZXing.OneD.PlesseyWriter::encode(System.String)
extern void PlesseyWriter_encode_m11E3DEB39B1788C5B332BAF64AB251D340A69696 (void);
// 0x00000483 System.Void ZXing.OneD.PlesseyWriter::.ctor()
extern void PlesseyWriter__ctor_mBAD22A528173A07A6E886EC74E9143E9902FE35B (void);
// 0x00000484 System.Void ZXing.OneD.PlesseyWriter::.cctor()
extern void PlesseyWriter__cctor_mDD383227C496495F0B77F3704C005036F7122CDC (void);
// 0x00000485 ZXing.Result ZXing.OneD.UPCAReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCAReader_decodeRow_m55870FFE11E12BDCA62D33F89581D35E23403D6C (void);
// 0x00000486 ZXing.Result ZXing.OneD.UPCAReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCAReader_decodeRow_m49F8AC4BBB564C255F16D879A923F1F44C9B20DA (void);
// 0x00000487 ZXing.Result ZXing.OneD.UPCAReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCAReader_decode_mDFB9D147DD664E3F84007F8BFE2DF3BFB7289E30 (void);
// 0x00000488 ZXing.BarcodeFormat ZXing.OneD.UPCAReader::get_BarcodeFormat()
extern void UPCAReader_get_BarcodeFormat_mAEF19838FC214383B1CDB43B72A018FB1FA47DB7 (void);
// 0x00000489 System.Int32 ZXing.OneD.UPCAReader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void UPCAReader_decodeMiddle_mF5402D33F26BF7A6EAC1C330B3CF8E085A895DE9 (void);
// 0x0000048A ZXing.Result ZXing.OneD.UPCAReader::maybeReturnResult(ZXing.Result)
extern void UPCAReader_maybeReturnResult_mAF92CDB04BDB0241309DD2198A5BCEC4D5EA16E0 (void);
// 0x0000048B System.Void ZXing.OneD.UPCAReader::.ctor()
extern void UPCAReader__ctor_m55697E4C701AB2874295E9F31B6423B2130D3F60 (void);
// 0x0000048C ZXing.Common.BitMatrix ZXing.OneD.UPCAWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32)
extern void UPCAWriter_encode_m5F15128CF0A86DD8DFC2844962F9D40CF9A8AC1F (void);
// 0x0000048D ZXing.Common.BitMatrix ZXing.OneD.UPCAWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void UPCAWriter_encode_m85F0F50EEEF4B8D3A814E8DC4EE728E5FAD006C3 (void);
// 0x0000048E System.Void ZXing.OneD.UPCAWriter::.ctor()
extern void UPCAWriter__ctor_m642BF9362116F576C52D7A9965AC5DFE3F8F7CE9 (void);
// 0x0000048F ZXing.Result ZXing.OneD.UPCEANExtension2Support::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[])
extern void UPCEANExtension2Support_decodeRow_m5D69D7FB6B9883012B8C006D4E7119E05C928195 (void);
// 0x00000490 System.Int32 ZXing.OneD.UPCEANExtension2Support::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void UPCEANExtension2Support_decodeMiddle_m7D23683EE8B9011A050E7FCB2912A3C2C1D7E698 (void);
// 0x00000491 System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.OneD.UPCEANExtension2Support::parseExtensionString(System.String)
extern void UPCEANExtension2Support_parseExtensionString_mC0E4F98644464C43CF000C741AB29ADB94496E8A (void);
// 0x00000492 System.Void ZXing.OneD.UPCEANExtension2Support::.ctor()
extern void UPCEANExtension2Support__ctor_m2D8E8B3F30FCF468C481D71BAF74115263ABBD5A (void);
// 0x00000493 ZXing.Result ZXing.OneD.UPCEANExtension5Support::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[])
extern void UPCEANExtension5Support_decodeRow_m5D3A775468AB9B4AF402EDBEA7B90584A089E03D (void);
// 0x00000494 System.Int32 ZXing.OneD.UPCEANExtension5Support::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void UPCEANExtension5Support_decodeMiddle_mD37FA70ABC47AB5E788C94B1BE775BF13DB5A8F3 (void);
// 0x00000495 System.Int32 ZXing.OneD.UPCEANExtension5Support::extensionChecksum(System.String)
extern void UPCEANExtension5Support_extensionChecksum_m63C8AE815828EE71C4AA70F6F2D2A9B48938E636 (void);
// 0x00000496 System.Boolean ZXing.OneD.UPCEANExtension5Support::determineCheckDigit(System.Int32,System.Int32&)
extern void UPCEANExtension5Support_determineCheckDigit_mE77F6A1636FAE27D8D18196C84C51DFC090B9BE5 (void);
// 0x00000497 System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.OneD.UPCEANExtension5Support::parseExtensionString(System.String)
extern void UPCEANExtension5Support_parseExtensionString_mD01076D53D961F327592ED3B74471B0C38ED5994 (void);
// 0x00000498 System.String ZXing.OneD.UPCEANExtension5Support::parseExtension5String(System.String)
extern void UPCEANExtension5Support_parseExtension5String_mA8EDCB54FF5D50DA075711BE15A8E009A67EDD35 (void);
// 0x00000499 System.Void ZXing.OneD.UPCEANExtension5Support::.ctor()
extern void UPCEANExtension5Support__ctor_mFFA2277F4C2B698BD430C8BCFE5418762E18F284 (void);
// 0x0000049A System.Void ZXing.OneD.UPCEANExtension5Support::.cctor()
extern void UPCEANExtension5Support__cctor_mEA65DB2E0F731553F10B07EBEE858CCD6650ACEC (void);
// 0x0000049B ZXing.Result ZXing.OneD.UPCEANExtensionSupport::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32)
extern void UPCEANExtensionSupport_decodeRow_mD76AAFBF1D3431876412E4D6EA2CB070AD5FA245 (void);
// 0x0000049C System.Void ZXing.OneD.UPCEANExtensionSupport::.ctor()
extern void UPCEANExtensionSupport__ctor_mFF7D8E10EDB00D8C17A7868C9A12AC06E44A5BE8 (void);
// 0x0000049D System.Void ZXing.OneD.UPCEANExtensionSupport::.cctor()
extern void UPCEANExtensionSupport__cctor_m8CC363D21EFCA9628EA08E8840690491704C4D4A (void);
// 0x0000049E System.Void ZXing.OneD.UPCEANReader::.cctor()
extern void UPCEANReader__cctor_mFEAE8E8813716E1A78570B6922E65CA4D5D2C3F9 (void);
// 0x0000049F System.Void ZXing.OneD.UPCEANReader::.ctor()
extern void UPCEANReader__ctor_m86D233D6C25C83F8DB3A78546E743A3706DD9FC3 (void);
// 0x000004A0 System.Int32[] ZXing.OneD.UPCEANReader::findStartGuardPattern(ZXing.Common.BitArray)
extern void UPCEANReader_findStartGuardPattern_mE3184D96AD82B8ABFC6185409F7859A2AC98073F (void);
// 0x000004A1 ZXing.Result ZXing.OneD.UPCEANReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCEANReader_decodeRow_m124E7FB0E2BFAF0DB4DB9C48F629FF52FC93E6B5 (void);
// 0x000004A2 ZXing.Result ZXing.OneD.UPCEANReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Int32[],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void UPCEANReader_decodeRow_mA74368C50FABECA1FFFE523FBBAB638ECEE6FA9A (void);
// 0x000004A3 System.Boolean ZXing.OneD.UPCEANReader::checkChecksum(System.String)
extern void UPCEANReader_checkChecksum_m5FCEB77BEECD0363704ACED4D4B56FF8F3E75170 (void);
// 0x000004A4 System.Boolean ZXing.OneD.UPCEANReader::checkStandardUPCEANChecksum(System.String)
extern void UPCEANReader_checkStandardUPCEANChecksum_mB1503E08FC1ED7546DB111E71C7AE73B1363F707 (void);
// 0x000004A5 System.Nullable`1<System.Int32> ZXing.OneD.UPCEANReader::getStandardUPCEANChecksum(System.String)
extern void UPCEANReader_getStandardUPCEANChecksum_m0B533161CA0B4D028BA64D79E2464A5E3F24E616 (void);
// 0x000004A6 System.Int32[] ZXing.OneD.UPCEANReader::decodeEnd(ZXing.Common.BitArray,System.Int32)
extern void UPCEANReader_decodeEnd_mAA1710C2E0F8CDCC2C4D98337D84D4D7190CDA62 (void);
// 0x000004A7 System.Int32[] ZXing.OneD.UPCEANReader::findGuardPattern(ZXing.Common.BitArray,System.Int32,System.Boolean,System.Int32[])
extern void UPCEANReader_findGuardPattern_mABE86B29333FA6346CB06628E6BF6AD935160EA6 (void);
// 0x000004A8 System.Int32[] ZXing.OneD.UPCEANReader::findGuardPattern(ZXing.Common.BitArray,System.Int32,System.Boolean,System.Int32[],System.Int32[])
extern void UPCEANReader_findGuardPattern_mDF210EB3D057E8E18B51113785014C9F5B714585 (void);
// 0x000004A9 System.Boolean ZXing.OneD.UPCEANReader::decodeDigit(ZXing.Common.BitArray,System.Int32[],System.Int32,System.Int32[][],System.Int32&)
extern void UPCEANReader_decodeDigit_m9F15C9A22192A991097A3C97FF4628A94E1DC059 (void);
// 0x000004AA ZXing.BarcodeFormat ZXing.OneD.UPCEANReader::get_BarcodeFormat()
// 0x000004AB System.Int32 ZXing.OneD.UPCEANReader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
// 0x000004AC System.Int32 ZXing.OneD.UPCEANWriter::get_DefaultMargin()
extern void UPCEANWriter_get_DefaultMargin_m500ECB2FE865C042C681922EC23AE920F2A182F6 (void);
// 0x000004AD System.Void ZXing.OneD.UPCEANWriter::.ctor()
extern void UPCEANWriter__ctor_m31782810F11C6446DD274987BB205326A02539CA (void);
// 0x000004AE System.Void ZXing.OneD.UPCEReader::.ctor()
extern void UPCEReader__ctor_mFE8B3EB6C62ACD63962C700D79D425F499EED7EA (void);
// 0x000004AF System.Int32 ZXing.OneD.UPCEReader::decodeMiddle(ZXing.Common.BitArray,System.Int32[],System.Text.StringBuilder)
extern void UPCEReader_decodeMiddle_m0DDAEA20247913FEC214F538F78DC70D1996C8B6 (void);
// 0x000004B0 System.Int32[] ZXing.OneD.UPCEReader::decodeEnd(ZXing.Common.BitArray,System.Int32)
extern void UPCEReader_decodeEnd_m25C57AE0513F6733446597C522E6E74236CF2328 (void);
// 0x000004B1 System.Boolean ZXing.OneD.UPCEReader::checkChecksum(System.String)
extern void UPCEReader_checkChecksum_m3C02FC19CF5CE90A3FBA0BF639F58E5EEF8BC6A3 (void);
// 0x000004B2 System.Boolean ZXing.OneD.UPCEReader::determineNumSysAndCheckDigit(System.Text.StringBuilder,System.Int32)
extern void UPCEReader_determineNumSysAndCheckDigit_mF1339E97B0C9B8300C6339DEAC605D46165B8C97 (void);
// 0x000004B3 ZXing.BarcodeFormat ZXing.OneD.UPCEReader::get_BarcodeFormat()
extern void UPCEReader_get_BarcodeFormat_m0422536F16EC2884579534C2E1DCD9C3027BDE98 (void);
// 0x000004B4 System.String ZXing.OneD.UPCEReader::convertUPCEtoUPCA(System.String)
extern void UPCEReader_convertUPCEtoUPCA_m6EEB800C4533CFF024F8B24F36F3A0C6BD062036 (void);
// 0x000004B5 System.Void ZXing.OneD.UPCEReader::.cctor()
extern void UPCEReader__cctor_m7363D4EFDE62630F5089E48C742EB901B699AE06 (void);
// 0x000004B6 System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.OneD.UPCEWriter::get_SupportedWriteFormats()
extern void UPCEWriter_get_SupportedWriteFormats_m53768EC43F0858FDA3A8346630322874B6DEFF8D (void);
// 0x000004B7 System.Boolean[] ZXing.OneD.UPCEWriter::encode(System.String)
extern void UPCEWriter_encode_m6BDA755FECB6D152CA1743DCE76D5C0B3A16E5E3 (void);
// 0x000004B8 System.Void ZXing.OneD.UPCEWriter::.ctor()
extern void UPCEWriter__ctor_m4CC21F3B200BDB1C7A868AB6C3F6C7D71F5A1D96 (void);
// 0x000004B9 System.Void ZXing.OneD.UPCEWriter::.cctor()
extern void UPCEWriter__cctor_mF3FF2F97642F2F8139D1E3343E4CB204410429C8 (void);
// 0x000004BA System.Void ZXing.OneD.RSS.AbstractRSSReader::.ctor()
extern void AbstractRSSReader__ctor_m7442DD5A2EA646170C03F234483531030CA099FF (void);
// 0x000004BB System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getDecodeFinderCounters()
extern void AbstractRSSReader_getDecodeFinderCounters_m79C0A2B24352B0F799407F3F6CF0F632E534ECAF (void);
// 0x000004BC System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getDataCharacterCounters()
extern void AbstractRSSReader_getDataCharacterCounters_mB5E2B8E45336DD163FA037B03D78EBC68D278762 (void);
// 0x000004BD System.Single[] ZXing.OneD.RSS.AbstractRSSReader::getOddRoundingErrors()
extern void AbstractRSSReader_getOddRoundingErrors_mB589C919393D950C449F88ECE9F815B0E144BA41 (void);
// 0x000004BE System.Single[] ZXing.OneD.RSS.AbstractRSSReader::getEvenRoundingErrors()
extern void AbstractRSSReader_getEvenRoundingErrors_mD6ABB8DC653BF9805335A4BE503C511FF03C2A97 (void);
// 0x000004BF System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getOddCounts()
extern void AbstractRSSReader_getOddCounts_mFB6556C24F5DCAFCCCAD1E29FF4884305080CF5F (void);
// 0x000004C0 System.Int32[] ZXing.OneD.RSS.AbstractRSSReader::getEvenCounts()
extern void AbstractRSSReader_getEvenCounts_m5A7CDD6AD88262C44FAB2BA8688DB40602AB28FC (void);
// 0x000004C1 System.Boolean ZXing.OneD.RSS.AbstractRSSReader::parseFinderValue(System.Int32[],System.Int32[][],System.Int32&)
extern void AbstractRSSReader_parseFinderValue_m65508BBD4CEE1D9B225709CA03970922E44B3942 (void);
// 0x000004C2 System.Int32 ZXing.OneD.RSS.AbstractRSSReader::count(System.Int32[])
extern void AbstractRSSReader_count_mDF59E3135522721189C039A5D39EC6B8A15C20D6 (void);
// 0x000004C3 System.Void ZXing.OneD.RSS.AbstractRSSReader::increment(System.Int32[],System.Single[])
extern void AbstractRSSReader_increment_m0F1DD2A2719AF31638A067B91BC6893021104C3B (void);
// 0x000004C4 System.Void ZXing.OneD.RSS.AbstractRSSReader::decrement(System.Int32[],System.Single[])
extern void AbstractRSSReader_decrement_mDAE74644C0AFA0603B984998FBAF3E1F86448868 (void);
// 0x000004C5 System.Boolean ZXing.OneD.RSS.AbstractRSSReader::isFinderPattern(System.Int32[])
extern void AbstractRSSReader_isFinderPattern_m37F7BE21135C38BEEF9B079FAEC694AF9BF9EB5B (void);
// 0x000004C6 System.Void ZXing.OneD.RSS.AbstractRSSReader::.cctor()
extern void AbstractRSSReader__cctor_mA30B8A36CEAE1BD3D4D3D4D35E669261A4FD53C8 (void);
// 0x000004C7 System.Int32 ZXing.OneD.RSS.DataCharacter::get_Value()
extern void DataCharacter_get_Value_m60DBB849017F13B1D771C24E907849C00497D709 (void);
// 0x000004C8 System.Void ZXing.OneD.RSS.DataCharacter::set_Value(System.Int32)
extern void DataCharacter_set_Value_m2B073FE2A86BE0365F25333A84DD35B993E8A5CA (void);
// 0x000004C9 System.Int32 ZXing.OneD.RSS.DataCharacter::get_ChecksumPortion()
extern void DataCharacter_get_ChecksumPortion_mD840CD43FD8ABDC679D01D19A698755EAA30D4CD (void);
// 0x000004CA System.Void ZXing.OneD.RSS.DataCharacter::set_ChecksumPortion(System.Int32)
extern void DataCharacter_set_ChecksumPortion_m6A5DA959D12F0A52405946B457B0298BA6314490 (void);
// 0x000004CB System.Void ZXing.OneD.RSS.DataCharacter::.ctor(System.Int32,System.Int32)
extern void DataCharacter__ctor_m39FAE9A09CE6A41019B6C5563F94A60D21DDDC8B (void);
// 0x000004CC System.String ZXing.OneD.RSS.DataCharacter::ToString()
extern void DataCharacter_ToString_mC7BCB0F3FFCAE11838CC42A9332DE8319CE31B89 (void);
// 0x000004CD System.Boolean ZXing.OneD.RSS.DataCharacter::Equals(System.Object)
extern void DataCharacter_Equals_m62E3086C2E2402D246AE720247F4A4624E5B191E (void);
// 0x000004CE System.Int32 ZXing.OneD.RSS.DataCharacter::GetHashCode()
extern void DataCharacter_GetHashCode_mF1D0EAEAAC95D459E6BE9D564BF9B254183D864B (void);
// 0x000004CF System.Int32 ZXing.OneD.RSS.FinderPattern::get_Value()
extern void FinderPattern_get_Value_m3A0645BC0EBC8C72D990FD4F2E454E6BEA262C1E (void);
// 0x000004D0 System.Void ZXing.OneD.RSS.FinderPattern::set_Value(System.Int32)
extern void FinderPattern_set_Value_m3C1C7073A3AE8B490A856DB9687D70075E00FB73 (void);
// 0x000004D1 System.Int32[] ZXing.OneD.RSS.FinderPattern::get_StartEnd()
extern void FinderPattern_get_StartEnd_mF89D5ABA5B62368CE7B8B2180AB8927307B76356 (void);
// 0x000004D2 System.Void ZXing.OneD.RSS.FinderPattern::set_StartEnd(System.Int32[])
extern void FinderPattern_set_StartEnd_mB6505DB19671BF37C4DE88CF0E9466EB589403E3 (void);
// 0x000004D3 ZXing.ResultPoint[] ZXing.OneD.RSS.FinderPattern::get_ResultPoints()
extern void FinderPattern_get_ResultPoints_m27A97739CB2E560692FE8D24834304231AD90217 (void);
// 0x000004D4 System.Void ZXing.OneD.RSS.FinderPattern::set_ResultPoints(ZXing.ResultPoint[])
extern void FinderPattern_set_ResultPoints_mA31D84D4C7B9364531D2520D39B9C6138A480E7B (void);
// 0x000004D5 System.Void ZXing.OneD.RSS.FinderPattern::.ctor(System.Int32,System.Int32[],System.Int32,System.Int32,System.Int32)
extern void FinderPattern__ctor_m18CE10A307FE0B805C1E829538F6C58C4571D4BB (void);
// 0x000004D6 System.Boolean ZXing.OneD.RSS.FinderPattern::Equals(System.Object)
extern void FinderPattern_Equals_m9B923E01E26F281612C3A1C972E08FBFC1ADFC59 (void);
// 0x000004D7 System.Int32 ZXing.OneD.RSS.FinderPattern::GetHashCode()
extern void FinderPattern_GetHashCode_m784A81D9A39F03EAB9C33978423A244A149331EC (void);
// 0x000004D8 ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Pair::get_FinderPattern()
extern void Pair_get_FinderPattern_m4C6E68F603523C6B38FA1DAE2DC7B9BDA3557F1C (void);
// 0x000004D9 System.Void ZXing.OneD.RSS.Pair::set_FinderPattern(ZXing.OneD.RSS.FinderPattern)
extern void Pair_set_FinderPattern_m00BFF71B29FA5FB88B85626E1C7579497451FB69 (void);
// 0x000004DA System.Int32 ZXing.OneD.RSS.Pair::get_Count()
extern void Pair_get_Count_m73CA5A5EF831E1078558DCC37ADD134F0D2AD79A (void);
// 0x000004DB System.Void ZXing.OneD.RSS.Pair::set_Count(System.Int32)
extern void Pair_set_Count_m87E259D87A7D1356B5F6B9CBE171FD594836F8A1 (void);
// 0x000004DC System.Void ZXing.OneD.RSS.Pair::.ctor(System.Int32,System.Int32,ZXing.OneD.RSS.FinderPattern)
extern void Pair__ctor_m5DED42D78363EA925B1DD8338F21195734868463 (void);
// 0x000004DD System.Void ZXing.OneD.RSS.Pair::incrementCount()
extern void Pair_incrementCount_m4089C852B621FEC665A6B312D1992FF633199251 (void);
// 0x000004DE System.Void ZXing.OneD.RSS.RSS14Reader::.ctor()
extern void RSS14Reader__ctor_mDD7F0495B29828CAB1B0D6BF5F06E729F6D546E1 (void);
// 0x000004DF ZXing.Result ZXing.OneD.RSS.RSS14Reader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void RSS14Reader_decodeRow_mC0ABFE466E9666A88D4B87550A049C7FC72E505C (void);
// 0x000004E0 System.Void ZXing.OneD.RSS.RSS14Reader::addOrTally(System.Collections.Generic.IList`1<ZXing.OneD.RSS.Pair>,ZXing.OneD.RSS.Pair)
extern void RSS14Reader_addOrTally_m13D69117825796401A745AD7C27F0A6D673E3860 (void);
// 0x000004E1 System.Void ZXing.OneD.RSS.RSS14Reader::reset()
extern void RSS14Reader_reset_mA389232DE62C1BB9F05535189528093860BC8E35 (void);
// 0x000004E2 ZXing.Result ZXing.OneD.RSS.RSS14Reader::constructResult(ZXing.OneD.RSS.Pair,ZXing.OneD.RSS.Pair)
extern void RSS14Reader_constructResult_m07F22DB5C3163A6DDD72D6A7178DC131515FC99B (void);
// 0x000004E3 System.Boolean ZXing.OneD.RSS.RSS14Reader::checkChecksum(ZXing.OneD.RSS.Pair,ZXing.OneD.RSS.Pair)
extern void RSS14Reader_checkChecksum_mE71657476A58F451533CEB56FCD62F7CC848A42B (void);
// 0x000004E4 ZXing.OneD.RSS.Pair ZXing.OneD.RSS.RSS14Reader::decodePair(ZXing.Common.BitArray,System.Boolean,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void RSS14Reader_decodePair_m055785C6AD3E4CC823E58EB95C219456B378AE11 (void);
// 0x000004E5 ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.RSS14Reader::decodeDataCharacter(ZXing.Common.BitArray,ZXing.OneD.RSS.FinderPattern,System.Boolean)
extern void RSS14Reader_decodeDataCharacter_m2E7C1DDBE209BD932D6A35A4395A86528FDB4D29 (void);
// 0x000004E6 System.Int32[] ZXing.OneD.RSS.RSS14Reader::findFinderPattern(ZXing.Common.BitArray,System.Boolean)
extern void RSS14Reader_findFinderPattern_m7C6232348ABB28DB3C92F21CB6497FDA31D4C2AE (void);
// 0x000004E7 ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.RSS14Reader::parseFoundFinderPattern(ZXing.Common.BitArray,System.Int32,System.Boolean,System.Int32[])
extern void RSS14Reader_parseFoundFinderPattern_mC62D6CA489B9662306A5D0DD9A943BAB7CFC5A2F (void);
// 0x000004E8 System.Boolean ZXing.OneD.RSS.RSS14Reader::adjustOddEvenCounts(System.Boolean,System.Int32)
extern void RSS14Reader_adjustOddEvenCounts_m580669B07EE4470AC2AFFE4C9CB5E722E48C4CE5 (void);
// 0x000004E9 System.Void ZXing.OneD.RSS.RSS14Reader::.cctor()
extern void RSS14Reader__cctor_m3F54AC9DB55AD8EECE0101E3540E45805D56EEE1 (void);
// 0x000004EA System.Int32 ZXing.OneD.RSS.RSSUtils::getRSSvalue(System.Int32[],System.Int32,System.Boolean)
extern void RSSUtils_getRSSvalue_m954F809E4F6ABB6B1F59EB52A79D39D8770E3543 (void);
// 0x000004EB System.Int32 ZXing.OneD.RSS.RSSUtils::combins(System.Int32,System.Int32)
extern void RSSUtils_combins_m856C8BC3DD9CEE5CB91FE185A98867F58941C908 (void);
// 0x000004EC ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.BitArrayBuilder::buildBitArray(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void BitArrayBuilder_buildBitArray_m75CDADD7CD1A3D516E1FDF483D53AC6909EFF981 (void);
// 0x000004ED System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::get_MayBeLast()
extern void ExpandedPair_get_MayBeLast_mDEEC63E98670E3EAD9EAD0D81CCBA82F676F9528 (void);
// 0x000004EE System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_MayBeLast(System.Boolean)
extern void ExpandedPair_set_MayBeLast_mD1345D00041D749799128E122E2496C419DEEEA1 (void);
// 0x000004EF ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::get_LeftChar()
extern void ExpandedPair_get_LeftChar_m2A34B6E652E541B2E944EEB939B7830207374532 (void);
// 0x000004F0 System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_LeftChar(ZXing.OneD.RSS.DataCharacter)
extern void ExpandedPair_set_LeftChar_m4B9BB8060792611F7C799153DF391402112FEEDF (void);
// 0x000004F1 ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.ExpandedPair::get_RightChar()
extern void ExpandedPair_get_RightChar_mC504EBE9DB513B577DFB9868AA4AED67C8711EFA (void);
// 0x000004F2 System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_RightChar(ZXing.OneD.RSS.DataCharacter)
extern void ExpandedPair_set_RightChar_m09256C170B4B33144A81F724C23BA9425DBAB8D7 (void);
// 0x000004F3 ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Expanded.ExpandedPair::get_FinderPattern()
extern void ExpandedPair_get_FinderPattern_mD465D0901090E9FD91A3C4F1D57662139614F4E9 (void);
// 0x000004F4 System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::set_FinderPattern(ZXing.OneD.RSS.FinderPattern)
extern void ExpandedPair_set_FinderPattern_m41DEB17AF11FE89854A25582A55E032A426DD7B9 (void);
// 0x000004F5 System.Void ZXing.OneD.RSS.Expanded.ExpandedPair::.ctor(ZXing.OneD.RSS.DataCharacter,ZXing.OneD.RSS.DataCharacter,ZXing.OneD.RSS.FinderPattern)
extern void ExpandedPair__ctor_mAA2B89881E05C1587AA5CD90078FB8C1A395EE98 (void);
// 0x000004F6 System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::get_MustBeLast()
extern void ExpandedPair_get_MustBeLast_m4F63330084A3B4A61C6EDE172F94C39C7ED2A5DE (void);
// 0x000004F7 System.String ZXing.OneD.RSS.Expanded.ExpandedPair::ToString()
extern void ExpandedPair_ToString_m6BCF55F1944B6352F4E4BD5BBD5D7B79BFFD1AFE (void);
// 0x000004F8 System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::Equals(System.Object)
extern void ExpandedPair_Equals_m1A1DCFC7B34A083FB641447539C6A259814B2D32 (void);
// 0x000004F9 System.Boolean ZXing.OneD.RSS.Expanded.ExpandedPair::EqualsOrNull(System.Object,System.Object)
extern void ExpandedPair_EqualsOrNull_m24E584E00CE2508B34BE621670E8B9F5BEEF3B4C (void);
// 0x000004FA System.Int32 ZXing.OneD.RSS.Expanded.ExpandedPair::GetHashCode()
extern void ExpandedPair_GetHashCode_mFD91B216166AFDF22AA75F1BD379EA0BDE8026B0 (void);
// 0x000004FB System.Int32 ZXing.OneD.RSS.Expanded.ExpandedPair::hashNotNull(System.Object)
extern void ExpandedPair_hashNotNull_mCD9324E7358320B6C1F9D6CC515F5514F8F8F101 (void);
// 0x000004FC System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::.ctor(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Int32,System.Boolean)
extern void ExpandedRow__ctor_mD7D483A05A80B41E98727B16A00EA5AFBC3F6D35 (void);
// 0x000004FD System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.ExpandedRow::get_Pairs()
extern void ExpandedRow_get_Pairs_m773AD1334B5029D4246571C0197B59E3CC5973FD (void);
// 0x000004FE System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::set_Pairs(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void ExpandedRow_set_Pairs_m111B020A54781C87F28CEA41F81F42E98F438AFC (void);
// 0x000004FF System.Int32 ZXing.OneD.RSS.Expanded.ExpandedRow::get_RowNumber()
extern void ExpandedRow_get_RowNumber_m6E5CA96945FD6C80B5CF3BB5ED7FB9C51FF43837 (void);
// 0x00000500 System.Void ZXing.OneD.RSS.Expanded.ExpandedRow::set_RowNumber(System.Int32)
extern void ExpandedRow_set_RowNumber_m8FD41BF6861C044AD174F95E602F5FE67DF04018 (void);
// 0x00000501 System.Boolean ZXing.OneD.RSS.Expanded.ExpandedRow::IsEquivalent(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void ExpandedRow_IsEquivalent_mA7F8A895B8DEC50582B2EE21941BA843B53A449B (void);
// 0x00000502 System.String ZXing.OneD.RSS.Expanded.ExpandedRow::ToString()
extern void ExpandedRow_ToString_mFF6A9D2B9B272ABA260306B4059A33145E398645 (void);
// 0x00000503 System.Boolean ZXing.OneD.RSS.Expanded.ExpandedRow::Equals(System.Object)
extern void ExpandedRow_Equals_m7418228BB6EF6528EB5696AB81818026D903F02D (void);
// 0x00000504 System.Int32 ZXing.OneD.RSS.Expanded.ExpandedRow::GetHashCode()
extern void ExpandedRow_GetHashCode_m3F438835732ADC7DD53454D7E03AEDAB518DCECF (void);
// 0x00000505 System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.RSSExpandedReader::get_Pairs()
extern void RSSExpandedReader_get_Pairs_mC8DF1D8DC65C63EC10BD89F46B522F8F54E548AC (void);
// 0x00000506 ZXing.Result ZXing.OneD.RSS.Expanded.RSSExpandedReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void RSSExpandedReader_decodeRow_mEED65BC509A20E80753B59D47BCE1673D4ED6091 (void);
// 0x00000507 System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::reset()
extern void RSSExpandedReader_reset_m40774DB7711C71C753DC8E3E4809EB7AFDA9EB0B (void);
// 0x00000508 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::decodeRow2pairs(System.Int32,ZXing.Common.BitArray)
extern void RSSExpandedReader_decodeRow2pairs_m32FBF346869303961B237BFA200F10D12CE36D98 (void);
// 0x00000509 System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.RSSExpandedReader::checkRows(System.Boolean)
extern void RSSExpandedReader_checkRows_m5197D7FA2D2CD303AE56D86A83BEF92672B06351 (void);
// 0x0000050A System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair> ZXing.OneD.RSS.Expanded.RSSExpandedReader::checkRows(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow>,System.Int32)
extern void RSSExpandedReader_checkRows_mCC057C063CFDA09896BD4AA62DAA9608B7FD3516 (void);
// 0x0000050B System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::isValidSequence(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void RSSExpandedReader_isValidSequence_mC8DE2CF465238E90F785A9C85A3DF466187B9042 (void);
// 0x0000050C System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::storeRow(System.Int32,System.Boolean)
extern void RSSExpandedReader_storeRow_mF70DAE9080CBB444CA1EC32593B98C873CC0EBE9 (void);
// 0x0000050D System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::removePartialRows(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow>)
extern void RSSExpandedReader_removePartialRows_mF6A5277FA8983147CF04BA721CB2C51C978585AD (void);
// 0x0000050E System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::isPartialRow(System.Collections.Generic.IEnumerable`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Collections.Generic.IEnumerable`1<ZXing.OneD.RSS.Expanded.ExpandedRow>)
extern void RSSExpandedReader_isPartialRow_m5BCD933001D553396D5FC4F99B46B00858F4F120 (void);
// 0x0000050F System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedRow> ZXing.OneD.RSS.Expanded.RSSExpandedReader::get_Rows()
extern void RSSExpandedReader_get_Rows_mA24D47E18B1D176279224E87EF865C1EAA9921F6 (void);
// 0x00000510 ZXing.Result ZXing.OneD.RSS.Expanded.RSSExpandedReader::constructResult(System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>)
extern void RSSExpandedReader_constructResult_mCD2C146B8F64E3CAE15AEBE18F5D317FBE19496A (void);
// 0x00000511 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::checkChecksum()
extern void RSSExpandedReader_checkChecksum_mDB81344E8E2F137E8D461667D0DE44580AA6BC76 (void);
// 0x00000512 System.Int32 ZXing.OneD.RSS.Expanded.RSSExpandedReader::getNextSecondBar(ZXing.Common.BitArray,System.Int32)
extern void RSSExpandedReader_getNextSecondBar_m366EBB26FC87D1F590765D9CE7B8A3692E820B46 (void);
// 0x00000513 ZXing.OneD.RSS.Expanded.ExpandedPair ZXing.OneD.RSS.Expanded.RSSExpandedReader::retrieveNextPair(ZXing.Common.BitArray,System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Int32)
extern void RSSExpandedReader_retrieveNextPair_m74D13489DF4090508B9F8F1E13EEFAD69CE49976 (void);
// 0x00000514 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::findNextPair(ZXing.Common.BitArray,System.Collections.Generic.List`1<ZXing.OneD.RSS.Expanded.ExpandedPair>,System.Int32)
extern void RSSExpandedReader_findNextPair_mD42022C11DC0B6C3F8C65E2122908A2617AA1ED9 (void);
// 0x00000515 System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::reverseCounters(System.Int32[])
extern void RSSExpandedReader_reverseCounters_mA91F8A2834A1B636F7BCA46024A7F108E740D1D2 (void);
// 0x00000516 ZXing.OneD.RSS.FinderPattern ZXing.OneD.RSS.Expanded.RSSExpandedReader::parseFoundFinderPattern(ZXing.Common.BitArray,System.Int32,System.Boolean)
extern void RSSExpandedReader_parseFoundFinderPattern_m282307979515A4224F7DF00D7A11E62AA7AAC0D2 (void);
// 0x00000517 ZXing.OneD.RSS.DataCharacter ZXing.OneD.RSS.Expanded.RSSExpandedReader::decodeDataCharacter(ZXing.Common.BitArray,ZXing.OneD.RSS.FinderPattern,System.Boolean,System.Boolean)
extern void RSSExpandedReader_decodeDataCharacter_m305668EB123EA575CE3765DAD9682B314E7CC45B (void);
// 0x00000518 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::isNotA1left(ZXing.OneD.RSS.FinderPattern,System.Boolean,System.Boolean)
extern void RSSExpandedReader_isNotA1left_mA2E3477E9B8B61D7AF94EF9C5E0FA1F733ED36F1 (void);
// 0x00000519 System.Boolean ZXing.OneD.RSS.Expanded.RSSExpandedReader::adjustOddEvenCounts(System.Int32)
extern void RSSExpandedReader_adjustOddEvenCounts_m000B4A540D7ED89B024DCA731E97993B25FF1D34 (void);
// 0x0000051A System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::.ctor()
extern void RSSExpandedReader__ctor_mB70A81ABAD6834435A906B891A424AC0C984FCF1 (void);
// 0x0000051B System.Void ZXing.OneD.RSS.Expanded.RSSExpandedReader::.cctor()
extern void RSSExpandedReader__cctor_m32BDCCD1DB72A2E8DDD3EF491EA9E460FAFCE32F (void);
// 0x0000051C System.Void ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::.ctor(ZXing.Common.BitArray)
extern void AbstractExpandedDecoder__ctor_mBB1B3E222CEA936145C0E893557A3526AB26E432 (void);
// 0x0000051D ZXing.Common.BitArray ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::getInformation()
extern void AbstractExpandedDecoder_getInformation_mF824E4822198393A925050EC92465441AA8EFC3A (void);
// 0x0000051E ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::getGeneralDecoder()
extern void AbstractExpandedDecoder_getGeneralDecoder_m7D36E37B49F12AF10B6B87486562E72C32F7505B (void);
// 0x0000051F System.String ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::parseInformation()
// 0x00000520 ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder ZXing.OneD.RSS.Expanded.Decoders.AbstractExpandedDecoder::createDecoder(ZXing.Common.BitArray)
extern void AbstractExpandedDecoder_createDecoder_mC63D4488C616AB3BB0FC4A5D4E5A63741BD7EB61 (void);
// 0x00000521 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder::.ctor(ZXing.Common.BitArray)
extern void AI013103decoder__ctor_mB4D0A5A6EE5E88AC5EEEF5BDBFE763072875A5BF (void);
// 0x00000522 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder::addWeightCode(System.Text.StringBuilder,System.Int32)
extern void AI013103decoder_addWeightCode_mD57C994E081D1BB1DCC92F3937D530FAD0E86797 (void);
// 0x00000523 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013103decoder::checkWeight(System.Int32)
extern void AI013103decoder_checkWeight_m6E3D79E07F48CBC37B2F6299E24A4202663E66D7 (void);
// 0x00000524 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01320xDecoder::.ctor(ZXing.Common.BitArray)
extern void AI01320xDecoder__ctor_m4AD46F3BFE12FF4EDF19CBABC87976A5E33B0AA1 (void);
// 0x00000525 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01320xDecoder::addWeightCode(System.Text.StringBuilder,System.Int32)
extern void AI01320xDecoder_addWeightCode_m78652EAAECBB4E85E587FA865511A3977FE38B67 (void);
// 0x00000526 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01320xDecoder::checkWeight(System.Int32)
extern void AI01320xDecoder_checkWeight_m21130C4617763518BB2A76CBBD103629E74DB255 (void);
// 0x00000527 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01392xDecoder::.ctor(ZXing.Common.BitArray)
extern void AI01392xDecoder__ctor_m263ACBCBDFF9206A8E41EE60613769BD4324B03E (void);
// 0x00000528 System.String ZXing.OneD.RSS.Expanded.Decoders.AI01392xDecoder::parseInformation()
extern void AI01392xDecoder_parseInformation_mCF373CB672B6DA156096895A25FE10283FDDD70B (void);
// 0x00000529 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::.ctor(ZXing.Common.BitArray)
extern void AI01393xDecoder__ctor_m2D9E860B37866C11902A9BA47A84747D2D22E6B8 (void);
// 0x0000052A System.String ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::parseInformation()
extern void AI01393xDecoder_parseInformation_m7616FD3924449023D1B771B632F7105A429CC31E (void);
// 0x0000052B System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01393xDecoder::.cctor()
extern void AI01393xDecoder__cctor_m3694D6D50646FE982669689DED98377EED699D21 (void);
// 0x0000052C System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::.ctor(ZXing.Common.BitArray,System.String,System.String)
extern void AI013x0x1xDecoder__ctor_mEFD9461A3724C0CC178D40B9D85EF96F4297B2EC (void);
// 0x0000052D System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::parseInformation()
extern void AI013x0x1xDecoder_parseInformation_m39DF0A42AD24E960A4C00A257C2FDC30EC777620 (void);
// 0x0000052E System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::encodeCompressedDate(System.Text.StringBuilder,System.Int32)
extern void AI013x0x1xDecoder_encodeCompressedDate_mDEE0EC9C29E801B9100A0B970F28BE795F9B3B94 (void);
// 0x0000052F System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::addWeightCode(System.Text.StringBuilder,System.Int32)
extern void AI013x0x1xDecoder_addWeightCode_m7693C9C9EA1B5EFB12E983DBEB8B08C0BECA5589 (void);
// 0x00000530 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::checkWeight(System.Int32)
extern void AI013x0x1xDecoder_checkWeight_m88CDE391946929FAA54DFC25E87D1BB1A7C14F7C (void);
// 0x00000531 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0x1xDecoder::.cctor()
extern void AI013x0x1xDecoder__cctor_m2FC0EE28ED5A97DB53C631C2D80E8F15D0BFAF38 (void);
// 0x00000532 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::.ctor(ZXing.Common.BitArray)
extern void AI013x0xDecoder__ctor_m3E5D9577944AB56127AED5CAFFACDDC048BC9957 (void);
// 0x00000533 System.String ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::parseInformation()
extern void AI013x0xDecoder_parseInformation_mC72E61AA9A1C92C89C220FEDE8D7B0252A9EF74F (void);
// 0x00000534 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI013x0xDecoder::.cctor()
extern void AI013x0xDecoder__cctor_mE53DF1837E302E14F14D45FCD89E3FDF7B5C76DA (void);
// 0x00000535 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::.ctor(ZXing.Common.BitArray)
extern void AI01AndOtherAIs__ctor_mB6F2CAF30F2486CC244CA5C4B80C4A38E3799785 (void);
// 0x00000536 System.String ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::parseInformation()
extern void AI01AndOtherAIs_parseInformation_mCAC698F33C5BCC697E3E63481C412BC79C632256 (void);
// 0x00000537 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01AndOtherAIs::.cctor()
extern void AI01AndOtherAIs__cctor_m9A4F14FAA74413314C92C1BD52A8E639FF81D830 (void);
// 0x00000538 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::.ctor(ZXing.Common.BitArray)
extern void AI01decoder__ctor_m4350FF8DCE7EFAC08F06E25513A79C476A38C249 (void);
// 0x00000539 System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::encodeCompressedGtin(System.Text.StringBuilder,System.Int32)
extern void AI01decoder_encodeCompressedGtin_m3CE89DCC6CCF6D08B735F6BF7DA8D53E604633EB (void);
// 0x0000053A System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::encodeCompressedGtinWithoutAI(System.Text.StringBuilder,System.Int32,System.Int32)
extern void AI01decoder_encodeCompressedGtinWithoutAI_mCBDF766FE45277B453B21A90A2286CF652D581F4 (void);
// 0x0000053B System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::appendCheckDigit(System.Text.StringBuilder,System.Int32)
extern void AI01decoder_appendCheckDigit_m41CE3FB19E4A200E731E19EBC59E32EAD5CAA76E (void);
// 0x0000053C System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01decoder::.cctor()
extern void AI01decoder__cctor_m5C1722D1E0BDD9F53A0F249106FE06D4B9C25B55 (void);
// 0x0000053D System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::.ctor(ZXing.Common.BitArray)
extern void AI01weightDecoder__ctor_mF32AB02F3115046F6FBF702A384D2C45C16CFC08 (void);
// 0x0000053E System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::encodeCompressedWeight(System.Text.StringBuilder,System.Int32,System.Int32)
extern void AI01weightDecoder_encodeCompressedWeight_m7501C476349F6259F4882B7C6EAE62217CF2147B (void);
// 0x0000053F System.Void ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::addWeightCode(System.Text.StringBuilder,System.Int32)
// 0x00000540 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.AI01weightDecoder::checkWeight(System.Int32)
// 0x00000541 System.Void ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::.ctor(ZXing.Common.BitArray)
extern void AnyAIDecoder__ctor_mB22F1D487F9F185EB4D67C7ACF88CA67FDBAFBFD (void);
// 0x00000542 System.String ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::parseInformation()
extern void AnyAIDecoder_parseInformation_m0887B37E12AF783107DE46C47543E7EBF43197D2 (void);
// 0x00000543 System.Void ZXing.OneD.RSS.Expanded.Decoders.AnyAIDecoder::.cctor()
extern void AnyAIDecoder__cctor_mF0F937FDBBA18F040CAD63A6A393DFCC2A19DDE2 (void);
// 0x00000544 System.Void ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::.ctor(System.Boolean)
extern void BlockParsedResult__ctor_m7008583A7874816E7AB2406B71CF1E563C199493 (void);
// 0x00000545 System.Void ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::.ctor(ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation,System.Boolean)
extern void BlockParsedResult__ctor_m1616FC6AAF1B6E1C4C8ED5A79F83AD4D2D7B144F (void);
// 0x00000546 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::getDecodedInformation()
extern void BlockParsedResult_getDecodedInformation_mEDC5DD26A8504DE8870BD356D4C84F5059C02623 (void);
// 0x00000547 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult::isFinished()
extern void BlockParsedResult_isFinished_mE4E102B61FBE11FAB8D1915FD85DB93809334848 (void);
// 0x00000548 System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::.ctor()
extern void CurrentParsingState__ctor_m0D654924041BE81BC993EB44E458B338BF4436BA (void);
// 0x00000549 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::getPosition()
extern void CurrentParsingState_getPosition_mE905D11AC452ECF19B2B3C7281DC937299284DB5 (void);
// 0x0000054A System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setPosition(System.Int32)
extern void CurrentParsingState_setPosition_mDD68F687E15D99F731A1F83E2724DD3C2EB9C142 (void);
// 0x0000054B System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::incrementPosition(System.Int32)
extern void CurrentParsingState_incrementPosition_m46BE6180A4757A84CE868CC17CDE83681AC58F2E (void);
// 0x0000054C System.Boolean ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::isAlpha()
extern void CurrentParsingState_isAlpha_mE9616B2B2030644E02FAF5711C2503CC35C0AEC4 (void);
// 0x0000054D System.Boolean ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::isNumeric()
extern void CurrentParsingState_isNumeric_mA7D0650985648E1AB7813763F786B56BA2B3E86F (void);
// 0x0000054E System.Boolean ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::isIsoIec646()
extern void CurrentParsingState_isIsoIec646_m38A0BFCA5BD9F39E1BFE189BE703B1F88DCC1C54 (void);
// 0x0000054F System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setNumeric()
extern void CurrentParsingState_setNumeric_mB4890E2837DBCB8A59C03A0F953BF4E077F21DBF (void);
// 0x00000550 System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setAlpha()
extern void CurrentParsingState_setAlpha_m3A114FF6C9F6D1A3A5F4E17F793CDF77423DAA40 (void);
// 0x00000551 System.Void ZXing.OneD.RSS.Expanded.Decoders.CurrentParsingState::setIsoIec646()
extern void CurrentParsingState_setIsoIec646_m0CA1B7A33D5AE9E5FC675298BF47015EDEFD15DA (void);
// 0x00000552 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::.ctor(System.Int32,System.Char)
extern void DecodedChar__ctor_m4ACD230659EDDE1D263ED37F351E900B5543AB9C (void);
// 0x00000553 System.Char ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::getValue()
extern void DecodedChar_getValue_mDC614458975467F1EC662E04B8806C56BA576CB9 (void);
// 0x00000554 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::isFNC1()
extern void DecodedChar_isFNC1_m55BC10B8535F425F8EDD328AA33E70A4C5ECF76E (void);
// 0x00000555 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedChar::.cctor()
extern void DecodedChar__cctor_m14D32C03D7F0DBEB91372D926226B555104495A8 (void);
// 0x00000556 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::.ctor(System.Int32,System.String)
extern void DecodedInformation__ctor_m14F0A67C61F1CFA1AE8DFD3B326408B4DA0D96A2 (void);
// 0x00000557 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::.ctor(System.Int32,System.String,System.Int32)
extern void DecodedInformation__ctor_m45AD0C128F665B4FD45A8AA8B2DEFDE68185414D (void);
// 0x00000558 System.String ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::getNewString()
extern void DecodedInformation_getNewString_m2111407862B5AC807879C717A87BB9DC1749A615 (void);
// 0x00000559 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::isRemaining()
extern void DecodedInformation_isRemaining_m92D8CC45E2DAD69EC38802581AE355D786D05BB9 (void);
// 0x0000055A System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation::getRemainingValue()
extern void DecodedInformation_getRemainingValue_mB1655DFFCDC8F569E13430F009C5C93EFE0FADB4 (void);
// 0x0000055B System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::.ctor(System.Int32,System.Int32,System.Int32)
extern void DecodedNumeric__ctor_m11E0096502B96C1AB40D91789A8C6073F3093AED (void);
// 0x0000055C System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::getFirstDigit()
extern void DecodedNumeric_getFirstDigit_mD8B952C89C43DAD9EBFB383353BAF71276526670 (void);
// 0x0000055D System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::getSecondDigit()
extern void DecodedNumeric_getSecondDigit_mDC760D5BFE6D2250BCC2BE38F199AB2BC88B65AE (void);
// 0x0000055E System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::getValue()
extern void DecodedNumeric_getValue_m49EA4EF5A10BC57CD906D80E6B29C24A9B037DC9 (void);
// 0x0000055F System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::isFirstDigitFNC1()
extern void DecodedNumeric_isFirstDigitFNC1_mD7EC5CAB758F5ED4704E391195B6B72298394CDC (void);
// 0x00000560 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::isSecondDigitFNC1()
extern void DecodedNumeric_isSecondDigitFNC1_mE6D32C215B87E0F45EFF1FDFE684F846F83D7098 (void);
// 0x00000561 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric::.cctor()
extern void DecodedNumeric__cctor_m3D3F764A854DEB245DBCD15E8D78C96CD6D4B506 (void);
// 0x00000562 System.Int32 ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::get_NewPosition()
extern void DecodedObject_get_NewPosition_m6661B582A9AA428F4A1928FEE5697EA0DED4E9AE (void);
// 0x00000563 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::set_NewPosition(System.Int32)
extern void DecodedObject_set_NewPosition_mD5F05B001E48031279B3FBF785E585C881005708 (void);
// 0x00000564 System.Void ZXing.OneD.RSS.Expanded.Decoders.DecodedObject::.ctor(System.Int32)
extern void DecodedObject__ctor_mD0F5992038C972F0882F0686D1FCC17CCE69CB70 (void);
// 0x00000565 System.Void ZXing.OneD.RSS.Expanded.Decoders.FieldParser::.cctor()
extern void FieldParser__cctor_m52563E3F5E39EFFAF9F666353F46B4FAE474B24C (void);
// 0x00000566 System.String ZXing.OneD.RSS.Expanded.Decoders.FieldParser::parseFieldsInGeneralPurpose(System.String)
extern void FieldParser_parseFieldsInGeneralPurpose_mA6AD30303EAEA45EA7C677AC1359FE175B9E3264 (void);
// 0x00000567 System.String ZXing.OneD.RSS.Expanded.Decoders.FieldParser::processFixedAI(System.Int32,System.Int32,System.String)
extern void FieldParser_processFixedAI_m4DF47E04DFDAA5D48E0DBF3DA7CBA10C379DEB25 (void);
// 0x00000568 System.String ZXing.OneD.RSS.Expanded.Decoders.FieldParser::processVariableAI(System.Int32,System.Int32,System.String)
extern void FieldParser_processVariableAI_m2FA71FC7883104D5225EEE1966F52883A3454840 (void);
// 0x00000569 System.Void ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::.ctor(ZXing.Common.BitArray)
extern void GeneralAppIdDecoder__ctor_m2AF7B7306EA26CEBAB2EC73695160A07D55F0246 (void);
// 0x0000056A System.String ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeAllCodes(System.Text.StringBuilder,System.Int32)
extern void GeneralAppIdDecoder_decodeAllCodes_m25AD982DAC4682BB19FD67B3E3569330D82F7B39 (void);
// 0x0000056B System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isStillNumeric(System.Int32)
extern void GeneralAppIdDecoder_isStillNumeric_m3E7449B9F87C18D4A3DF9243620C9FA5CBD97820 (void);
// 0x0000056C ZXing.OneD.RSS.Expanded.Decoders.DecodedNumeric ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeNumeric(System.Int32)
extern void GeneralAppIdDecoder_decodeNumeric_mE25F62A6236DCF826663DE57E79B49DB874983E3 (void);
// 0x0000056D System.Int32 ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::extractNumericValueFromBitArray(System.Int32,System.Int32)
extern void GeneralAppIdDecoder_extractNumericValueFromBitArray_m01B60D593D7168ADA36EC09C3CFB1D1E2E03066C (void);
// 0x0000056E System.Int32 ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::extractNumericValueFromBitArray(ZXing.Common.BitArray,System.Int32,System.Int32)
extern void GeneralAppIdDecoder_extractNumericValueFromBitArray_mBE16ECC0479492D03F407539CD0126213D21E15D (void);
// 0x0000056F ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeGeneralPurposeField(System.Int32,System.String)
extern void GeneralAppIdDecoder_decodeGeneralPurposeField_mA5A24B57F3A223F26CCAB22F654A92B51AEEB638 (void);
// 0x00000570 ZXing.OneD.RSS.Expanded.Decoders.DecodedInformation ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseBlocks()
extern void GeneralAppIdDecoder_parseBlocks_m86E51BEACAB43FF27C9553BC4A0F6FD8FA130EA0 (void);
// 0x00000571 ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseNumericBlock()
extern void GeneralAppIdDecoder_parseNumericBlock_mD6DFAE70980A0DFFA33C65ADFE572CE3E03E30B5 (void);
// 0x00000572 ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseIsoIec646Block()
extern void GeneralAppIdDecoder_parseIsoIec646Block_m0E0D3A5D2D2C04B54881847F1DD92E8A3B35D59E (void);
// 0x00000573 ZXing.OneD.RSS.Expanded.Decoders.BlockParsedResult ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::parseAlphaBlock()
extern void GeneralAppIdDecoder_parseAlphaBlock_mBA72BDA3D6215106164942B923A7C338932BD21C (void);
// 0x00000574 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isStillIsoIec646(System.Int32)
extern void GeneralAppIdDecoder_isStillIsoIec646_m752106E72C6B97FB651A89945750A1C8E5D8C214 (void);
// 0x00000575 ZXing.OneD.RSS.Expanded.Decoders.DecodedChar ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeIsoIec646(System.Int32)
extern void GeneralAppIdDecoder_decodeIsoIec646_m76F366C18B4B94519A67A9DFE0FEBF64FF63AC50 (void);
// 0x00000576 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isStillAlpha(System.Int32)
extern void GeneralAppIdDecoder_isStillAlpha_m2600D102EF86CA0C40C87F2474CA36CC62C233A2 (void);
// 0x00000577 ZXing.OneD.RSS.Expanded.Decoders.DecodedChar ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::decodeAlphanumeric(System.Int32)
extern void GeneralAppIdDecoder_decodeAlphanumeric_m35282F7E1E1C42587809E03C3DE8457757CAD122 (void);
// 0x00000578 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isAlphaTo646ToAlphaLatch(System.Int32)
extern void GeneralAppIdDecoder_isAlphaTo646ToAlphaLatch_m10A4E09629D26B9A23EB987BE45CD326A6974903 (void);
// 0x00000579 System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isAlphaOr646ToNumericLatch(System.Int32)
extern void GeneralAppIdDecoder_isAlphaOr646ToNumericLatch_mF9F98BA1D0F74FCA7929F0352E60C3F1C599F97B (void);
// 0x0000057A System.Boolean ZXing.OneD.RSS.Expanded.Decoders.GeneralAppIdDecoder::isNumericToAlphaNumericLatch(System.Int32)
extern void GeneralAppIdDecoder_isNumericToAlphaNumericLatch_m1696C53567BD18DA707815455484F3576ACFD7C7 (void);
// 0x0000057B System.Void ZXing.Multi.ByQuadrantReader::.ctor(ZXing.Reader)
extern void ByQuadrantReader__ctor_m587BCE0E8852D76BB3895B70316883D7C3C6331B (void);
// 0x0000057C ZXing.Result ZXing.Multi.ByQuadrantReader::decode(ZXing.BinaryBitmap)
extern void ByQuadrantReader_decode_m2075834B22CBEE68A98ABC6F83AB3ED636A7510F (void);
// 0x0000057D ZXing.Result ZXing.Multi.ByQuadrantReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void ByQuadrantReader_decode_mBB8324ED103B78BF0767E309C480C8469D8F8CD1 (void);
// 0x0000057E System.Void ZXing.Multi.ByQuadrantReader::reset()
extern void ByQuadrantReader_reset_mEDCBC22EDA0A588E10F700E287D34231031645EA (void);
// 0x0000057F System.Void ZXing.Multi.ByQuadrantReader::makeAbsolute(ZXing.ResultPoint[],System.Int32,System.Int32)
extern void ByQuadrantReader_makeAbsolute_mB90847985F30BE15697F647FC8BB81988114029B (void);
// 0x00000580 System.Void ZXing.Multi.GenericMultipleBarcodeReader::.ctor(ZXing.Reader)
extern void GenericMultipleBarcodeReader__ctor_mA7EC203EACCB76AABC0A70DA30DEF4A0418BDBCD (void);
// 0x00000581 ZXing.Result[] ZXing.Multi.GenericMultipleBarcodeReader::decodeMultiple(ZXing.BinaryBitmap)
extern void GenericMultipleBarcodeReader_decodeMultiple_m171BA20B09F8CF8E6AB18615643107170BD08B3E (void);
// 0x00000582 ZXing.Result[] ZXing.Multi.GenericMultipleBarcodeReader::decodeMultiple(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void GenericMultipleBarcodeReader_decodeMultiple_mA71B236451E4B1323A9A6F4EFDC6AB2B5AEA0A89 (void);
// 0x00000583 System.Void ZXing.Multi.GenericMultipleBarcodeReader::doDecodeMultiple(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>,System.Collections.Generic.IList`1<ZXing.Result>,System.Int32,System.Int32,System.Int32)
extern void GenericMultipleBarcodeReader_doDecodeMultiple_m56A5C9EE86AE478E87FA2855AB9B623F288382D0 (void);
// 0x00000584 ZXing.Result ZXing.Multi.GenericMultipleBarcodeReader::translateResultPoints(ZXing.Result,System.Int32,System.Int32)
extern void GenericMultipleBarcodeReader_translateResultPoints_mBDE02279A4290B78D5C3D4418A818DE7E0B88467 (void);
// 0x00000585 ZXing.Result ZXing.Multi.GenericMultipleBarcodeReader::decode(ZXing.BinaryBitmap)
extern void GenericMultipleBarcodeReader_decode_mB3C53F5AD41AA4C5FDA24CAE7BE4F4B469B2191C (void);
// 0x00000586 ZXing.Result ZXing.Multi.GenericMultipleBarcodeReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void GenericMultipleBarcodeReader_decode_m688C9CD4F0A7CEDE6900210F844FA5C2FBD0E16C (void);
// 0x00000587 System.Void ZXing.Multi.GenericMultipleBarcodeReader::reset()
extern void GenericMultipleBarcodeReader_reset_m0BE56AD34166F48A3B7B4031597EE2417C0106DD (void);
// 0x00000588 ZXing.Result[] ZXing.Multi.MultipleBarcodeReader::decodeMultiple(ZXing.BinaryBitmap)
// 0x00000589 ZXing.Result[] ZXing.Multi.MultipleBarcodeReader::decodeMultiple(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
// 0x0000058A ZXing.Result[] ZXing.Multi.QrCode.QRCodeMultiReader::decodeMultiple(ZXing.BinaryBitmap)
extern void QRCodeMultiReader_decodeMultiple_mABBCE57DB00B74DBFC27F09501D176F51B005444 (void);
// 0x0000058B ZXing.Result[] ZXing.Multi.QrCode.QRCodeMultiReader::decodeMultiple(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void QRCodeMultiReader_decodeMultiple_mF06B4A7196D5E6159173697D9DECEEFA992C1892 (void);
// 0x0000058C System.Collections.Generic.List`1<ZXing.Result> ZXing.Multi.QrCode.QRCodeMultiReader::ProcessStructuredAppend(System.Collections.Generic.List`1<ZXing.Result>)
extern void QRCodeMultiReader_ProcessStructuredAppend_m0763E6EDDA2F6C568418F690749083FECF418AA7 (void);
// 0x0000058D System.Int32 ZXing.Multi.QrCode.QRCodeMultiReader::SaSequenceSort(ZXing.Result,ZXing.Result)
extern void QRCodeMultiReader_SaSequenceSort_m7E8230B1C12AEE2EB0FB967C406D69A7DB3A0816 (void);
// 0x0000058E System.Void ZXing.Multi.QrCode.QRCodeMultiReader::.ctor()
extern void QRCodeMultiReader__ctor_mA645C1D5C68109C7B83EEE198FA8FF5ECE0FFDFD (void);
// 0x0000058F System.Void ZXing.Multi.QrCode.QRCodeMultiReader::.cctor()
extern void QRCodeMultiReader__cctor_m7940B87F9491A93EF2CBBFCB303B69E837376B45 (void);
// 0x00000590 System.Void ZXing.Multi.QrCode.Internal.MultiDetector::.ctor(ZXing.Common.BitMatrix)
extern void MultiDetector__ctor_m87C735618A0D0E15E1280B212E5949A8EEEFFAE5 (void);
// 0x00000591 ZXing.Common.DetectorResult[] ZXing.Multi.QrCode.Internal.MultiDetector::detectMulti(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiDetector_detectMulti_m6E750ED533AC8C71F4DDF4752A68A746905392B7 (void);
// 0x00000592 System.Void ZXing.Multi.QrCode.Internal.MultiDetector::.cctor()
extern void MultiDetector__cctor_mA49F47DE36402C6D92D48D8B3D945CEE31068285 (void);
// 0x00000593 System.Void ZXing.Multi.QrCode.Internal.MultiFinderPatternFinder::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPointCallback)
extern void MultiFinderPatternFinder__ctor_m582E8448778E37F0AE910C3D231DFC83ED17FCE3 (void);
// 0x00000594 ZXing.QrCode.Internal.FinderPattern[][] ZXing.Multi.QrCode.Internal.MultiFinderPatternFinder::selectMultipleBestPatterns()
extern void MultiFinderPatternFinder_selectMultipleBestPatterns_m1EF946A27BDB5140FB17F33B599392B751112EB3 (void);
// 0x00000595 ZXing.QrCode.Internal.FinderPatternInfo[] ZXing.Multi.QrCode.Internal.MultiFinderPatternFinder::findMulti(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MultiFinderPatternFinder_findMulti_m51A729C27260587D8195B1C64FB530BF1D5B839A (void);
// 0x00000596 System.Void ZXing.Multi.QrCode.Internal.MultiFinderPatternFinder::.cctor()
extern void MultiFinderPatternFinder__cctor_m0763043CBBCCD9FBBF7AE6C069A643C74970F0D0 (void);
// 0x00000597 System.Int32 ZXing.Multi.QrCode.Internal.MultiFinderPatternFinder/ModuleSizeComparator::Compare(ZXing.QrCode.Internal.FinderPattern,ZXing.QrCode.Internal.FinderPattern)
extern void ModuleSizeComparator_Compare_m6AC37A4356EA4CD59D90B81CE64CDDB17C7DF57E (void);
// 0x00000598 System.Void ZXing.Multi.QrCode.Internal.MultiFinderPatternFinder/ModuleSizeComparator::.ctor()
extern void ModuleSizeComparator__ctor_m3AF6535F92E114F29CD5E9ABBE64CC9C99676A40 (void);
// 0x00000599 ZXing.Result ZXing.Maxicode.MaxiCodeReader::decode(ZXing.BinaryBitmap)
extern void MaxiCodeReader_decode_mD71AF3C834F5AA114A4445272B6E7C8518D85715 (void);
// 0x0000059A ZXing.Result ZXing.Maxicode.MaxiCodeReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void MaxiCodeReader_decode_m730A0FAB41B5EC323A4D45C7B1E7089A5547C6D7 (void);
// 0x0000059B System.Void ZXing.Maxicode.MaxiCodeReader::reset()
extern void MaxiCodeReader_reset_m98C2685C959A696973CF4B2DE01A6AC9FDC32442 (void);
// 0x0000059C ZXing.Common.BitMatrix ZXing.Maxicode.MaxiCodeReader::extractPureBits(ZXing.Common.BitMatrix)
extern void MaxiCodeReader_extractPureBits_mE878FC5711EA47220B6923D197CA4016A9803DEF (void);
// 0x0000059D System.Void ZXing.Maxicode.MaxiCodeReader::.ctor()
extern void MaxiCodeReader__ctor_m3309C54636F333D09FD215CF6C9B6479667EF4B5 (void);
// 0x0000059E System.Void ZXing.Maxicode.MaxiCodeReader::.cctor()
extern void MaxiCodeReader__cctor_mAFC8509B7C0BC9027C40B1BE0F47CDB80C11868B (void);
// 0x0000059F System.Void ZXing.Maxicode.Internal.BitMatrixParser::.ctor(ZXing.Common.BitMatrix)
extern void BitMatrixParser__ctor_mE20181816ABDA5877569B8977AC0E4E04F4C23F2 (void);
// 0x000005A0 System.Byte[] ZXing.Maxicode.Internal.BitMatrixParser::readCodewords()
extern void BitMatrixParser_readCodewords_m789CE1C02BCB632366F8B80EDF85D76085D73723 (void);
// 0x000005A1 System.Void ZXing.Maxicode.Internal.BitMatrixParser::.cctor()
extern void BitMatrixParser__cctor_m1AB5E78EE0175BAC16B59F86D4AEA75BCBB1E046 (void);
// 0x000005A2 ZXing.Common.DecoderResult ZXing.Maxicode.Internal.DecodedBitStreamParser::decode(System.Byte[],System.Int32)
extern void DecodedBitStreamParser_decode_m87384708552DB6C158866A09FE9D6327B1D20887 (void);
// 0x000005A3 System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getBit(System.Int32,System.Byte[])
extern void DecodedBitStreamParser_getBit_m5A13627BF7B9771AEBAAAA854F3AEB58A4740836 (void);
// 0x000005A4 System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getInt(System.Byte[],System.Byte[])
extern void DecodedBitStreamParser_getInt_m0AB19A2556B318D5EB825F8C4FEBDE25A3E99EC4 (void);
// 0x000005A5 System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getCountry(System.Byte[])
extern void DecodedBitStreamParser_getCountry_m27288041061FFDF7D757C6AD7D2BFBD2A9419885 (void);
// 0x000005A6 System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getServiceClass(System.Byte[])
extern void DecodedBitStreamParser_getServiceClass_m23DB86FA7CE9155AE5077747642345DC3B20B59D (void);
// 0x000005A7 System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getPostCode2Length(System.Byte[])
extern void DecodedBitStreamParser_getPostCode2Length_mC74C6ABCC0C90A7B7952E67E0805D77D60EFC3CE (void);
// 0x000005A8 System.Int32 ZXing.Maxicode.Internal.DecodedBitStreamParser::getPostCode2(System.Byte[])
extern void DecodedBitStreamParser_getPostCode2_m2D42E8C8248B7D1F26B7097FE6FF071F87F3460F (void);
// 0x000005A9 System.String ZXing.Maxicode.Internal.DecodedBitStreamParser::getPostCode3(System.Byte[])
extern void DecodedBitStreamParser_getPostCode3_mDEF6245C182FBFAD251F8994D387693199AAEA70 (void);
// 0x000005AA System.String ZXing.Maxicode.Internal.DecodedBitStreamParser::getMessage(System.Byte[],System.Int32,System.Int32)
extern void DecodedBitStreamParser_getMessage_m3B5C534BB3F77ABE34432042FA6FB7FCC25F85AC (void);
// 0x000005AB System.Void ZXing.Maxicode.Internal.DecodedBitStreamParser::.cctor()
extern void DecodedBitStreamParser__cctor_m150A7ACC8803C679534DE48B4208DC9FE874A62F (void);
// 0x000005AC System.Void ZXing.Maxicode.Internal.Decoder::.ctor()
extern void Decoder__ctor_m67D9A559880C0DD66930FCE08070230E550C3988 (void);
// 0x000005AD ZXing.Common.DecoderResult ZXing.Maxicode.Internal.Decoder::decode(ZXing.Common.BitMatrix)
extern void Decoder_decode_mB723528CE96D3B7F7E2A427E73AC8AD8E951309A (void);
// 0x000005AE ZXing.Common.DecoderResult ZXing.Maxicode.Internal.Decoder::decode(ZXing.Common.BitMatrix,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void Decoder_decode_mEA1DEBF419A79FBDAFE8EB3CA0749471FD2AA63B (void);
// 0x000005AF System.Boolean ZXing.Maxicode.Internal.Decoder::correctErrors(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32)
extern void Decoder_correctErrors_mCF27E615A343FCC7FCFE46956DB45407F1DDFF18 (void);
// 0x000005B0 System.Void ZXing.IMB.IMBReader::.cctor()
extern void IMBReader__cctor_m154E808405E34843AB0D5AB06A6C461D32C2FF80 (void);
// 0x000005B1 ZXing.Result ZXing.IMB.IMBReader::doDecode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void IMBReader_doDecode_m2F0E3A95E2A683F49EB38759E58831A14122D56D (void);
// 0x000005B2 System.Void ZXing.IMB.IMBReader::reset()
extern void IMBReader_reset_m57C18978BEAE3CFADAC18BD37E258968DC76DB02 (void);
// 0x000005B3 System.UInt16 ZXing.IMB.IMBReader::binaryStringToDec(System.String)
extern void IMBReader_binaryStringToDec_m1E6B4FA27D8011E4DF97DA27F31EFDFD19AEB1A6 (void);
// 0x000005B4 System.String ZXing.IMB.IMBReader::invertedBinaryString(System.String)
extern void IMBReader_invertedBinaryString_m97B3AA2EE5E9AE0AC4DE259CBE0244E20E33358E (void);
// 0x000005B5 System.Boolean ZXing.IMB.IMBReader::getCodeWords(System.Int32[]&,System.String,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>,System.Int32[][],System.Char[][])
extern void IMBReader_getCodeWords_m5213F8081647393BDAEBEA7FE614A807B755C8FD (void);
// 0x000005B6 System.String ZXing.IMB.IMBReader::getTrackingNumber(System.String)
extern void IMBReader_getTrackingNumber_mDC9BFA36B8F6F02FF22820CD265B85CF8CEF8768 (void);
// 0x000005B7 System.Void ZXing.IMB.IMBReader::fillLists(ZXing.Common.BitArray,ZXing.Common.BitArray,ZXing.Common.BitArray,System.Collections.Generic.List`1<System.Int32>&,System.Collections.Generic.List`1<System.Int32>&,System.Collections.Generic.List`1<System.Int32>&,System.Int32,System.Int32)
extern void IMBReader_fillLists_m61C87F31D568329B66FD27C47BFDE1A47AB86B08 (void);
// 0x000005B8 System.Int32 ZXing.IMB.IMBReader::isIMB(ZXing.Common.BitArray,System.Int32&,System.Int32&,System.Int32&)
extern void IMBReader_isIMB_mED7DF4B013F2CD489A065E815141075089AF4408 (void);
// 0x000005B9 System.Int32 ZXing.IMB.IMBReader::getNumberBars(ZXing.Common.BitArray,System.Int32,System.Int32,System.Int32)
extern void IMBReader_getNumberBars_mB0086A92D383931429C6D93683BEC5F50342D3E8 (void);
// 0x000005BA ZXing.Result ZXing.IMB.IMBReader::decodeRow(System.Int32,ZXing.Common.BitArray,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void IMBReader_decodeRow_m4E715BD69F16F15EFFDEA6808D92408803F4F73F (void);
// 0x000005BB System.Void ZXing.IMB.IMBReader::.ctor()
extern void IMBReader__ctor_m9F38E54F9F7E044BBE28650B49A2763EE85C1189 (void);
// 0x000005BC ZXing.Result ZXing.Datamatrix.DataMatrixReader::decode(ZXing.BinaryBitmap)
extern void DataMatrixReader_decode_m680149836C044D3E055979BBFF61FDE59EEF58E3 (void);
// 0x000005BD ZXing.Result ZXing.Datamatrix.DataMatrixReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void DataMatrixReader_decode_m47350828549BDAF16CF2E7963F4769AE2E02F593 (void);
// 0x000005BE System.Void ZXing.Datamatrix.DataMatrixReader::reset()
extern void DataMatrixReader_reset_m4B2F552B5BEF16C14F74EFF67614DEAE5A61C660 (void);
// 0x000005BF ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixReader::extractPureBits(ZXing.Common.BitMatrix)
extern void DataMatrixReader_extractPureBits_m9E0BA73AAD73E5092B8289E9273BECDD8F1F1A19 (void);
// 0x000005C0 System.Boolean ZXing.Datamatrix.DataMatrixReader::moduleSize(System.Int32[],ZXing.Common.BitMatrix,System.Int32&)
extern void DataMatrixReader_moduleSize_m8A385B2D5644A0BFE9DE90B2A7B4D225AE9672E1 (void);
// 0x000005C1 System.Void ZXing.Datamatrix.DataMatrixReader::.ctor()
extern void DataMatrixReader__ctor_m4B01836FA21E00C62ADA2A653CB2B569E617A185 (void);
// 0x000005C2 System.Void ZXing.Datamatrix.DataMatrixReader::.cctor()
extern void DataMatrixReader__cctor_mEA43C442BF5C99A5A0855D32B2789071A8384748 (void);
// 0x000005C3 ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32)
extern void DataMatrixWriter_encode_m89AC74733EEEB39C54704D641F31DBA669983178 (void);
// 0x000005C4 ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void DataMatrixWriter_encode_mE8BEC0BCEC42AB7CB8EF1F191C305E88AB449EF8 (void);
// 0x000005C5 ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::encodeLowLevel(ZXing.Datamatrix.Encoder.DefaultPlacement,ZXing.Datamatrix.Encoder.SymbolInfo,System.Int32,System.Int32)
extern void DataMatrixWriter_encodeLowLevel_m06DFA4AE7541587A40B649EEC1F3AB22323AD03F (void);
// 0x000005C6 ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::convertByteMatrixToBitMatrix(ZXing.QrCode.Internal.ByteMatrix,System.Int32,System.Int32)
extern void DataMatrixWriter_convertByteMatrixToBitMatrix_m856357FB7B0F295AECC589AFA67F9A460FE1237E (void);
// 0x000005C7 System.Void ZXing.Datamatrix.DataMatrixWriter::.ctor()
extern void DataMatrixWriter__ctor_mDF8F61AD5D30A55A36FC17A0A9A8313B1388BBB3 (void);
// 0x000005C8 System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint> ZXing.Datamatrix.DatamatrixEncodingOptions::get_SymbolShape()
extern void DatamatrixEncodingOptions_get_SymbolShape_m914646B1E4B47D4AFA15143570919B495DD11DDF (void);
// 0x000005C9 System.Void ZXing.Datamatrix.DatamatrixEncodingOptions::set_SymbolShape(System.Nullable`1<ZXing.Datamatrix.Encoder.SymbolShapeHint>)
extern void DatamatrixEncodingOptions_set_SymbolShape_mA583FE99C76D1678F294A62F3C46A04673F01976 (void);
// 0x000005CA ZXing.Dimension ZXing.Datamatrix.DatamatrixEncodingOptions::get_MinSize()
extern void DatamatrixEncodingOptions_get_MinSize_mADC06B94795A51E631CF3CD45DCD0B6D863677A5 (void);
// 0x000005CB System.Void ZXing.Datamatrix.DatamatrixEncodingOptions::set_MinSize(ZXing.Dimension)
extern void DatamatrixEncodingOptions_set_MinSize_m3A1871549778C96894FB85A078AC422A09BBD67D (void);
// 0x000005CC ZXing.Dimension ZXing.Datamatrix.DatamatrixEncodingOptions::get_MaxSize()
extern void DatamatrixEncodingOptions_get_MaxSize_m21522EF87A9998E595119EADB40B28DF6B7CFC7C (void);
// 0x000005CD System.Void ZXing.Datamatrix.DatamatrixEncodingOptions::set_MaxSize(ZXing.Dimension)
extern void DatamatrixEncodingOptions_set_MaxSize_m01E7D4676DA9E00607288BDECB9BC4DF14165170 (void);
// 0x000005CE System.Nullable`1<System.Int32> ZXing.Datamatrix.DatamatrixEncodingOptions::get_DefaultEncodation()
extern void DatamatrixEncodingOptions_get_DefaultEncodation_mD60FAD9806CC0BF8FCB7F06F617A7AF3E60E45D1 (void);
// 0x000005CF System.Void ZXing.Datamatrix.DatamatrixEncodingOptions::set_DefaultEncodation(System.Nullable`1<System.Int32>)
extern void DatamatrixEncodingOptions_set_DefaultEncodation_m13F82D8391B5785E13074B1063F0976F6DF16C14 (void);
// 0x000005D0 System.Void ZXing.Datamatrix.DatamatrixEncodingOptions::.ctor()
extern void DatamatrixEncodingOptions__ctor_m2F8A24CD4BEF623D33894F88F6B32FBF54CCEFBF (void);
// 0x000005D1 System.Int32 ZXing.Datamatrix.Encoder.ASCIIEncoder::get_EncodingMode()
extern void ASCIIEncoder_get_EncodingMode_mB3FD8167FA8BC7CCF6B7E828D21A7640E7B3BBD5 (void);
// 0x000005D2 System.Void ZXing.Datamatrix.Encoder.ASCIIEncoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void ASCIIEncoder_encode_mF9905BF3C9CEB5FFA039A69D4F2A366E8FBFAED3 (void);
// 0x000005D3 System.Char ZXing.Datamatrix.Encoder.ASCIIEncoder::encodeASCIIDigits(System.Char,System.Char)
extern void ASCIIEncoder_encodeASCIIDigits_m12E8E0DB272078CACDE4C6ADEE65A6B84584A380 (void);
// 0x000005D4 System.Void ZXing.Datamatrix.Encoder.ASCIIEncoder::.ctor()
extern void ASCIIEncoder__ctor_mE4317540C046ACC7281EDC032AB3727F526CD8EA (void);
// 0x000005D5 System.Int32 ZXing.Datamatrix.Encoder.Base256Encoder::get_EncodingMode()
extern void Base256Encoder_get_EncodingMode_m615F040653359B2506ECA6DB9F565C2CF71D252F (void);
// 0x000005D6 System.Void ZXing.Datamatrix.Encoder.Base256Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void Base256Encoder_encode_m2F8061D6B0CBB7B736D186583692F24B84BFAAF2 (void);
// 0x000005D7 System.Char ZXing.Datamatrix.Encoder.Base256Encoder::randomize255State(System.Char,System.Int32)
extern void Base256Encoder_randomize255State_m7AB5A958F77625BDC1436969867585F83E8127E9 (void);
// 0x000005D8 System.Void ZXing.Datamatrix.Encoder.Base256Encoder::.ctor()
extern void Base256Encoder__ctor_m12BFEF901AEFF0FEA185CCF4A23FD660C931A212 (void);
// 0x000005D9 System.Int32 ZXing.Datamatrix.Encoder.C40Encoder::get_EncodingMode()
extern void C40Encoder_get_EncodingMode_m721D9EB23989D30921E4BF1FB09A4DB3119675AF (void);
// 0x000005DA System.Void ZXing.Datamatrix.Encoder.C40Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void C40Encoder_encode_mA374CE97501CA1647E0563075FC1666CD1CD2D87 (void);
// 0x000005DB System.Int32 ZXing.Datamatrix.Encoder.C40Encoder::backtrackOneCharacter(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder,System.Text.StringBuilder,System.Int32)
extern void C40Encoder_backtrackOneCharacter_m7F7B5FAB798E99C5C5A0B8D23F15D0BBF9C89236 (void);
// 0x000005DC System.Void ZXing.Datamatrix.Encoder.C40Encoder::writeNextTriplet(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern void C40Encoder_writeNextTriplet_m2FB9D1BD13C372A7DB92A233F923E8F04A76FEB1 (void);
// 0x000005DD System.Void ZXing.Datamatrix.Encoder.C40Encoder::handleEOD(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern void C40Encoder_handleEOD_mB68864C35FBB992B231C86EBBFD7E22C706C0098 (void);
// 0x000005DE System.Int32 ZXing.Datamatrix.Encoder.C40Encoder::encodeChar(System.Char,System.Text.StringBuilder)
extern void C40Encoder_encodeChar_mC7946AFB8A04C547D3E84F52A7B75C9BD54F8C35 (void);
// 0x000005DF System.String ZXing.Datamatrix.Encoder.C40Encoder::encodeToCodewords(System.Text.StringBuilder,System.Int32)
extern void C40Encoder_encodeToCodewords_m7CE295FB46B3139D3301B0AFD92532C1C674B84F (void);
// 0x000005E0 System.Void ZXing.Datamatrix.Encoder.C40Encoder::.ctor()
extern void C40Encoder__ctor_mDCE4C825212972EC2EEB9A34C0ABF3D36E27E3B7 (void);
// 0x000005E1 System.Void ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144::.ctor()
extern void DataMatrixSymbolInfo144__ctor_mC437363A4E22A5A6595CE073C0D51FA89A114540 (void);
// 0x000005E2 System.Int32 ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144::getInterleavedBlockCount()
extern void DataMatrixSymbolInfo144_getInterleavedBlockCount_mBE710D72C18676F2E9A293EA6F552F1F769AA647 (void);
// 0x000005E3 System.Int32 ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144::getDataLengthForInterleavedBlock(System.Int32)
extern void DataMatrixSymbolInfo144_getDataLengthForInterleavedBlock_m8A8303EBCA4680FBBA92D66D642D1007865D25A2 (void);
// 0x000005E4 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::.ctor(System.String,System.Int32,System.Int32)
extern void DefaultPlacement__ctor_m297E56E3796FD82FF30D955D074EB2DE0EBF90E4 (void);
// 0x000005E5 System.Int32 ZXing.Datamatrix.Encoder.DefaultPlacement::get_Numrows()
extern void DefaultPlacement_get_Numrows_m7CC74A53D4805FC3AA832C003AFFC6995A815BBC (void);
// 0x000005E6 System.Int32 ZXing.Datamatrix.Encoder.DefaultPlacement::get_Numcols()
extern void DefaultPlacement_get_Numcols_mEB9F9D953AFF1BBF52B89DE78BB6CD3207D841D8 (void);
// 0x000005E7 System.Byte[] ZXing.Datamatrix.Encoder.DefaultPlacement::get_Bits()
extern void DefaultPlacement_get_Bits_mDC6B617E55C0CB0C324588F0026F4AB422D4A190 (void);
// 0x000005E8 System.Boolean ZXing.Datamatrix.Encoder.DefaultPlacement::getBit(System.Int32,System.Int32)
extern void DefaultPlacement_getBit_m635974B2179B729BC03FE95A5D5C2F8279EDED56 (void);
// 0x000005E9 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::setBit(System.Int32,System.Int32,System.Boolean)
extern void DefaultPlacement_setBit_m9DAEA022C6231A85945A9F2DB56F4943D013A177 (void);
// 0x000005EA System.Boolean ZXing.Datamatrix.Encoder.DefaultPlacement::noBit(System.Int32,System.Int32)
extern void DefaultPlacement_noBit_m5B0102E87A04D9819690EAFF06FAFFE604E6DB3B (void);
// 0x000005EB System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::place()
extern void DefaultPlacement_place_m8F16677902BF1478B3BA834873639D9FF5B8F926 (void);
// 0x000005EC System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::module(System.Int32,System.Int32,System.Int32,System.Int32)
extern void DefaultPlacement_module_m898D35F0109EACCABCFC8CC0E679A18DE5AAC6B5 (void);
// 0x000005ED System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::utah(System.Int32,System.Int32,System.Int32)
extern void DefaultPlacement_utah_mEE89A156D30BEB3AD4211D6B7AF73E9D8C6C7235 (void);
// 0x000005EE System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner1(System.Int32)
extern void DefaultPlacement_corner1_mAE1C394FF59F8D177FA92DD1789919937432B725 (void);
// 0x000005EF System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner2(System.Int32)
extern void DefaultPlacement_corner2_m880CD8D061466AACAEA3BD2DE8763FAB87E74AAA (void);
// 0x000005F0 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner3(System.Int32)
extern void DefaultPlacement_corner3_m4DC378F924CB5DC31F42C33F29B5172F28E4954E (void);
// 0x000005F1 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner4(System.Int32)
extern void DefaultPlacement_corner4_m099DB9D3A75EF29D142E75403E9D63331FC2ED22 (void);
// 0x000005F2 System.Int32 ZXing.Datamatrix.Encoder.EdifactEncoder::get_EncodingMode()
extern void EdifactEncoder_get_EncodingMode_m09A4625E2D3A8AC14F1C5DBA4949B9C6CF10075F (void);
// 0x000005F3 System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void EdifactEncoder_encode_mD15CE61AE174EA1FABAF882CE3045B1CBAC98A9A (void);
// 0x000005F4 System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::handleEOD(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern void EdifactEncoder_handleEOD_mBE811765A0A556D58435AAB569D7BDD7955AF5B1 (void);
// 0x000005F5 System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::encodeChar(System.Char,System.Text.StringBuilder)
extern void EdifactEncoder_encodeChar_m73DE86DCB647341C91FCA211A17F6302C05A2A4C (void);
// 0x000005F6 System.String ZXing.Datamatrix.Encoder.EdifactEncoder::encodeToCodewords(System.Text.StringBuilder,System.Int32)
extern void EdifactEncoder_encodeToCodewords_m413A7062BFE181ECD6B17FAA2898FD17686693FA (void);
// 0x000005F7 System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::.ctor()
extern void EdifactEncoder__ctor_m4601FE839B37AD8A9E531DCCFCF95A11C531A416 (void);
// 0x000005F8 System.Void ZXing.Datamatrix.Encoder.Encodation::.ctor()
extern void Encodation__ctor_mAE4959052BB100A1FE3A31728DA4A881EB5AB6C2 (void);
// 0x000005F9 System.Int32 ZXing.Datamatrix.Encoder.Encoder::get_EncodingMode()
// 0x000005FA System.Void ZXing.Datamatrix.Encoder.Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
// 0x000005FB System.Void ZXing.Datamatrix.Encoder.EncoderContext::.cctor()
extern void EncoderContext__cctor_mE8F317B0A62F6CB72F2D3C2E6875A1D251F7CF9E (void);
// 0x000005FC System.Void ZXing.Datamatrix.Encoder.EncoderContext::.ctor(System.String)
extern void EncoderContext__ctor_m3A9D1904BBC631385A86D6ECE5A9B42CE14CF040 (void);
// 0x000005FD System.Void ZXing.Datamatrix.Encoder.EncoderContext::setSymbolShape(ZXing.Datamatrix.Encoder.SymbolShapeHint)
extern void EncoderContext_setSymbolShape_m10DC861EC36EDCC4FC1D770258DF8F9B4EECE63C (void);
// 0x000005FE System.Void ZXing.Datamatrix.Encoder.EncoderContext::setSizeConstraints(ZXing.Dimension,ZXing.Dimension)
extern void EncoderContext_setSizeConstraints_mBEEA9308933DFE9B57E1CA4B5F696AECED3D751E (void);
// 0x000005FF System.Void ZXing.Datamatrix.Encoder.EncoderContext::setSkipAtEnd(System.Int32)
extern void EncoderContext_setSkipAtEnd_m50BE20756B70956262BEA812C6C2305688594DDF (void);
// 0x00000600 System.Char ZXing.Datamatrix.Encoder.EncoderContext::get_CurrentChar()
extern void EncoderContext_get_CurrentChar_mE84D6717C4E049CC3423F528A03F68EA10056E3A (void);
// 0x00000601 System.Char ZXing.Datamatrix.Encoder.EncoderContext::get_Current()
extern void EncoderContext_get_Current_m46D5FC6A2DD2155F83F74B8BC0A1E275FD7E8A33 (void);
// 0x00000602 System.Void ZXing.Datamatrix.Encoder.EncoderContext::writeCodewords(System.String)
extern void EncoderContext_writeCodewords_mF04897B2BE6D9A0F130A9C01126FB97A2939F18F (void);
// 0x00000603 System.Void ZXing.Datamatrix.Encoder.EncoderContext::writeCodeword(System.Char)
extern void EncoderContext_writeCodeword_mDF4D6B7C08E2922A8C9E8B56A3C8930AA42C6FCC (void);
// 0x00000604 System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_CodewordCount()
extern void EncoderContext_get_CodewordCount_mD0CA58B49F7199DC3DE45721DDE2F753D0427830 (void);
// 0x00000605 System.Void ZXing.Datamatrix.Encoder.EncoderContext::signalEncoderChange(System.Int32)
extern void EncoderContext_signalEncoderChange_mBD24176BD2C05E133A92C7BD4D708E5C66F8D9B2 (void);
// 0x00000606 System.Void ZXing.Datamatrix.Encoder.EncoderContext::resetEncoderSignal()
extern void EncoderContext_resetEncoderSignal_m7FB4A82E70B338FCDF2601B9EA36ED84B79A1117 (void);
// 0x00000607 System.Boolean ZXing.Datamatrix.Encoder.EncoderContext::get_HasMoreCharacters()
extern void EncoderContext_get_HasMoreCharacters_m1AAF96EF23CDBEE32B44D08A31B0DAC397C43C72 (void);
// 0x00000608 System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_TotalMessageCharCount()
extern void EncoderContext_get_TotalMessageCharCount_m85C3032DBAB9020207FAF99E4DCDA6D2D5E00D6A (void);
// 0x00000609 System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_RemainingCharacters()
extern void EncoderContext_get_RemainingCharacters_mEAC8FAC64FF422FE0D8ED1675DE41874BEB57819 (void);
// 0x0000060A System.Void ZXing.Datamatrix.Encoder.EncoderContext::updateSymbolInfo()
extern void EncoderContext_updateSymbolInfo_m5F70C79A1E8DEB6A061F7E467C784C365C885DC2 (void);
// 0x0000060B System.Void ZXing.Datamatrix.Encoder.EncoderContext::updateSymbolInfo(System.Int32)
extern void EncoderContext_updateSymbolInfo_mD234191E900456A14B9BD7B164BF58CB33D3F39C (void);
// 0x0000060C System.Void ZXing.Datamatrix.Encoder.EncoderContext::resetSymbolInfo()
extern void EncoderContext_resetSymbolInfo_m288D569C1F238C80B6861222018A953B0EF27629 (void);
// 0x0000060D System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_Pos()
extern void EncoderContext_get_Pos_m190B315DEEB38598F4D38E008CBB3AABF0020150 (void);
// 0x0000060E System.Void ZXing.Datamatrix.Encoder.EncoderContext::set_Pos(System.Int32)
extern void EncoderContext_set_Pos_mBEEA215DF89CCCAA273268F5A398760963ACC672 (void);
// 0x0000060F System.Text.StringBuilder ZXing.Datamatrix.Encoder.EncoderContext::get_Codewords()
extern void EncoderContext_get_Codewords_m5F219B3DAA2082F1F029D4F09A578C16E8E4965E (void);
// 0x00000610 ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.EncoderContext::get_SymbolInfo()
extern void EncoderContext_get_SymbolInfo_mFB0DAB05E41AE895D9C07B997009860E00DB7AD9 (void);
// 0x00000611 System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_NewEncoding()
extern void EncoderContext_get_NewEncoding_mB7D2C10FB5FCB774EF8E1701504F2E0791312010 (void);
// 0x00000612 System.String ZXing.Datamatrix.Encoder.EncoderContext::get_Message()
extern void EncoderContext_get_Message_m1E332A566A2D3792A23598E4A7A38F6BF2E628D6 (void);
// 0x00000613 System.Boolean ZXing.Datamatrix.Encoder.EncoderContext::get_Fnc1CodewordIsWritten()
extern void EncoderContext_get_Fnc1CodewordIsWritten_m0A3B6778EADB1F88543B1C57244A9DC4690FE6FB (void);
// 0x00000614 System.Void ZXing.Datamatrix.Encoder.EncoderContext::set_Fnc1CodewordIsWritten(System.Boolean)
extern void EncoderContext_set_Fnc1CodewordIsWritten_m64AAE62F4D46BBCAC6CCD3CA6E1D371B426E1FCB (void);
// 0x00000615 System.Void ZXing.Datamatrix.Encoder.ErrorCorrection::.cctor()
extern void ErrorCorrection__cctor_m07DBD8901D6818C9B85677B73A0102BAD0722BC3 (void);
// 0x00000616 System.String ZXing.Datamatrix.Encoder.ErrorCorrection::encodeECC200(System.String,ZXing.Datamatrix.Encoder.SymbolInfo)
extern void ErrorCorrection_encodeECC200_m09F6C2FD040036E2218F24DBE887C66045AF16B4 (void);
// 0x00000617 System.String ZXing.Datamatrix.Encoder.ErrorCorrection::createECCBlock(System.String,System.Int32)
extern void ErrorCorrection_createECCBlock_m1D24779EDC2CD8491C87395A0DE337FEF82F0F07 (void);
// 0x00000618 System.String ZXing.Datamatrix.Encoder.ErrorCorrection::createECCBlock(System.String,System.Int32,System.Int32,System.Int32)
extern void ErrorCorrection_createECCBlock_m53D97940002E0C53F4BEE3AF9162F26E73B8B815 (void);
// 0x00000619 System.Char ZXing.Datamatrix.Encoder.HighLevelEncoder::randomize253State(System.Char,System.Int32)
extern void HighLevelEncoder_randomize253State_mDE1FFC45043EF18FD4475C45828FF0720207CB70 (void);
// 0x0000061A System.String ZXing.Datamatrix.Encoder.HighLevelEncoder::encodeHighLevel(System.String)
extern void HighLevelEncoder_encodeHighLevel_m64AE463C282BD80D2ECA1950A25BC2CC85FD261E (void);
// 0x0000061B System.String ZXing.Datamatrix.Encoder.HighLevelEncoder::encodeHighLevel(System.String,ZXing.Datamatrix.Encoder.SymbolShapeHint,ZXing.Dimension,ZXing.Dimension,System.Int32)
extern void HighLevelEncoder_encodeHighLevel_m1D35F724AB1F61FD937D94801557E55EEF202CE3 (void);
// 0x0000061C System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::lookAheadTest(System.String,System.Int32,System.Int32)
extern void HighLevelEncoder_lookAheadTest_m06B7BD49ECBE30A5625FD6EC6F6FA880A38328E5 (void);
// 0x0000061D System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::findMinimums(System.Single[],System.Int32[],System.Int32,System.Byte[])
extern void HighLevelEncoder_findMinimums_mE7B8B2850A7D2C968EE47D7A452868DACA3562D1 (void);
// 0x0000061E System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::getMinimumCount(System.Byte[])
extern void HighLevelEncoder_getMinimumCount_m67DF4D33CB29C56C5212C8FFABFF9C22D14B7A4D (void);
// 0x0000061F System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isDigit(System.Char)
extern void HighLevelEncoder_isDigit_m2752D8C2999FE044D4D27EF1AB17382BAD5D7B11 (void);
// 0x00000620 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isExtendedASCII(System.Char)
extern void HighLevelEncoder_isExtendedASCII_m0B78F51A4669782CA0B90886DEC7534B1E1B8178 (void);
// 0x00000621 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeC40(System.Char)
extern void HighLevelEncoder_isNativeC40_m604E3300097651DF0B2A6E9111BDAEF5780B7060 (void);
// 0x00000622 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeText(System.Char)
extern void HighLevelEncoder_isNativeText_mCDC2A8A6537209929A38607972000CC9C08103A0 (void);
// 0x00000623 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeX12(System.Char)
extern void HighLevelEncoder_isNativeX12_m812EF23F11D13ECDD0B3E9C8773F169A7294DC45 (void);
// 0x00000624 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isX12TermSep(System.Char)
extern void HighLevelEncoder_isX12TermSep_m0524DDE5DA3F738AB79E4BD263E6161A1ADA7FB1 (void);
// 0x00000625 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeEDIFACT(System.Char)
extern void HighLevelEncoder_isNativeEDIFACT_mBD0CA9AAC8E38AE302B262F7C0CA5642F6AF184B (void);
// 0x00000626 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isSpecialB256(System.Char)
extern void HighLevelEncoder_isSpecialB256_m2D32F9D933B7438A8A5421346A8B98F96668FE9A (void);
// 0x00000627 System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::determineConsecutiveDigitCount(System.String,System.Int32)
extern void HighLevelEncoder_determineConsecutiveDigitCount_m596276F6B096456459117767D870C7839EA7D105 (void);
// 0x00000628 System.Void ZXing.Datamatrix.Encoder.HighLevelEncoder::illegalCharacter(System.Char)
extern void HighLevelEncoder_illegalCharacter_m03D2BC640651D7B2E2C6BF3B419BB7465192E3D6 (void);
// 0x00000629 System.Void ZXing.Datamatrix.Encoder.SymbolInfo::overrideSymbolSet(ZXing.Datamatrix.Encoder.SymbolInfo[])
extern void SymbolInfo_overrideSymbolSet_mCBE5737F02EA2C9EA0E8F8F84F13486D993522FC (void);
// 0x0000062A System.Void ZXing.Datamatrix.Encoder.SymbolInfo::.ctor(System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void SymbolInfo__ctor_mB40B5354F4C9CB4C844A24F108407DF6EFB67BCB (void);
// 0x0000062B System.Void ZXing.Datamatrix.Encoder.SymbolInfo::.ctor(System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void SymbolInfo__ctor_m3AFEB74FEF954B550F225B52EB044F9D5DC7E62E (void);
// 0x0000062C ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.SymbolInfo::lookup(System.Int32)
extern void SymbolInfo_lookup_mE06B4674441FA22A97D600272BCE450F20200E7C (void);
// 0x0000062D ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.SymbolInfo::lookup(System.Int32,ZXing.Datamatrix.Encoder.SymbolShapeHint)
extern void SymbolInfo_lookup_mB1F91F2A206456DDDB2D50CFFC018F5163799FBC (void);
// 0x0000062E ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.SymbolInfo::lookup(System.Int32,System.Boolean,System.Boolean)
extern void SymbolInfo_lookup_m9FF5D72C3E3E9FCBF697034361F3EE2E453F4FFD (void);
// 0x0000062F ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.SymbolInfo::lookup(System.Int32,ZXing.Datamatrix.Encoder.SymbolShapeHint,System.Boolean)
extern void SymbolInfo_lookup_mB3693FD6A3DACE99F749415B0A5EFD6160F00414 (void);
// 0x00000630 ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.SymbolInfo::lookup(System.Int32,ZXing.Datamatrix.Encoder.SymbolShapeHint,ZXing.Dimension,ZXing.Dimension,System.Boolean)
extern void SymbolInfo_lookup_m0BB75A135073754A1719F292339C5C71AAC18FC4 (void);
// 0x00000631 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getHorizontalDataRegions()
extern void SymbolInfo_getHorizontalDataRegions_m0067F9B1B5E1D1A900067411DA8855FF07875DCC (void);
// 0x00000632 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getVerticalDataRegions()
extern void SymbolInfo_getVerticalDataRegions_mD6AFF12B03A3EEA5AD9F02BBD8D79CBFE7272864 (void);
// 0x00000633 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolDataWidth()
extern void SymbolInfo_getSymbolDataWidth_mE996BA35548C931627CFB869C4768B4AE5DC26D6 (void);
// 0x00000634 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolDataHeight()
extern void SymbolInfo_getSymbolDataHeight_mC022AE234B64A1F5467982D879CB168ADA0AE0B8 (void);
// 0x00000635 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolWidth()
extern void SymbolInfo_getSymbolWidth_m14AFC048263EA0E994A8F0CE82E9A9BA5EA8B8B9 (void);
// 0x00000636 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolHeight()
extern void SymbolInfo_getSymbolHeight_mDD67722D36075F0A2CEC3C22687C1C47A976946A (void);
// 0x00000637 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getCodewordCount()
extern void SymbolInfo_getCodewordCount_m2FEDFC18126BBDEE809884E38C27C89F88837F7B (void);
// 0x00000638 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getInterleavedBlockCount()
extern void SymbolInfo_getInterleavedBlockCount_mF95A015E0832907159367AA9FC7F3ED6DD0C8F5A (void);
// 0x00000639 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getDataLengthForInterleavedBlock(System.Int32)
extern void SymbolInfo_getDataLengthForInterleavedBlock_mAF876E5947FD821A8DDCADD7E419763721369E21 (void);
// 0x0000063A System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getErrorLengthForInterleavedBlock(System.Int32)
extern void SymbolInfo_getErrorLengthForInterleavedBlock_m91A53BFEEDD75A4A303B7B786973DC49651E6D74 (void);
// 0x0000063B System.String ZXing.Datamatrix.Encoder.SymbolInfo::ToString()
extern void SymbolInfo_ToString_mD35C8CACC8C9E5ABFF73FCF70752A73283E51E09 (void);
// 0x0000063C System.Void ZXing.Datamatrix.Encoder.SymbolInfo::.cctor()
extern void SymbolInfo__cctor_m43D08D9425D79B104F8AD2E583CA7F884EDED931 (void);
// 0x0000063D System.Int32 ZXing.Datamatrix.Encoder.TextEncoder::get_EncodingMode()
extern void TextEncoder_get_EncodingMode_m86CE43681A201DDDEAFE054D5951384BC5D28610 (void);
// 0x0000063E System.Int32 ZXing.Datamatrix.Encoder.TextEncoder::encodeChar(System.Char,System.Text.StringBuilder)
extern void TextEncoder_encodeChar_m60A6BD53E34F4514AD33ED8EDE9F88E331A509F3 (void);
// 0x0000063F System.Void ZXing.Datamatrix.Encoder.TextEncoder::.ctor()
extern void TextEncoder__ctor_m5C7FABC1619AEAB813C7982AFB3251E4871BD293 (void);
// 0x00000640 System.Int32 ZXing.Datamatrix.Encoder.X12Encoder::get_EncodingMode()
extern void X12Encoder_get_EncodingMode_mB92BD65DF7759C0CE0D46FF6674A816CF4EFA880 (void);
// 0x00000641 System.Void ZXing.Datamatrix.Encoder.X12Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void X12Encoder_encode_m932A8D147591F3BF8E98F33EA1A2123EE11F7052 (void);
// 0x00000642 System.Int32 ZXing.Datamatrix.Encoder.X12Encoder::encodeChar(System.Char,System.Text.StringBuilder)
extern void X12Encoder_encodeChar_m47C3B6BFF7826194E2AA71747AE7A84021B54E05 (void);
// 0x00000643 System.Void ZXing.Datamatrix.Encoder.X12Encoder::handleEOD(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern void X12Encoder_handleEOD_m806DE8C5FCF97FB7405AACEDAC8C7801C757ABF9 (void);
// 0x00000644 System.Void ZXing.Datamatrix.Encoder.X12Encoder::.ctor()
extern void X12Encoder__ctor_mAFCDC0F3E3A4A18A5C96B987C63A1E412C368804 (void);
// 0x00000645 System.Void ZXing.Datamatrix.Internal.BitMatrixParser::.ctor(ZXing.Common.BitMatrix)
extern void BitMatrixParser__ctor_m8952E0E88DC362C362CE0B2BD6C7AC8EC4E91D33 (void);
// 0x00000646 ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.BitMatrixParser::get_Version()
extern void BitMatrixParser_get_Version_m8F32D9720384DE258671764AAE1D7613C66203E6 (void);
// 0x00000647 ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.BitMatrixParser::readVersion(ZXing.Common.BitMatrix)
extern void BitMatrixParser_readVersion_m5630022D1A1AD702C636EFE2014C776A62B3640B (void);
// 0x00000648 System.Byte[] ZXing.Datamatrix.Internal.BitMatrixParser::readCodewords()
extern void BitMatrixParser_readCodewords_m69EA88D99E046E1437765E8A89F441FCDC01B425 (void);
// 0x00000649 System.Boolean ZXing.Datamatrix.Internal.BitMatrixParser::readModule(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BitMatrixParser_readModule_mDB212F050A4C3D72161466D4A82218863C020561 (void);
// 0x0000064A System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readUtah(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BitMatrixParser_readUtah_m0072D25025BF0BADA850204CE3E8C6F62C35D4FE (void);
// 0x0000064B System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner1(System.Int32,System.Int32)
extern void BitMatrixParser_readCorner1_mD87683297F236F957DBF1DA738A6150798C1A69D (void);
// 0x0000064C System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner2(System.Int32,System.Int32)
extern void BitMatrixParser_readCorner2_m4E54E03558FE070DC680DE15E207C6D67CACB32C (void);
// 0x0000064D System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner3(System.Int32,System.Int32)
extern void BitMatrixParser_readCorner3_m127DB08776F3CEEA642B16028031B62FA017965B (void);
// 0x0000064E System.Int32 ZXing.Datamatrix.Internal.BitMatrixParser::readCorner4(System.Int32,System.Int32)
extern void BitMatrixParser_readCorner4_mC73041C15CF445EEB5035B958BD5AB35899E9DC6 (void);
// 0x0000064F ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.BitMatrixParser::extractDataRegion(ZXing.Common.BitMatrix)
extern void BitMatrixParser_extractDataRegion_m1E33D355D38F5E3DBBC5630DE6B12EDC2A85322D (void);
// 0x00000650 System.Void ZXing.Datamatrix.Internal.DataBlock::.ctor(System.Int32,System.Byte[])
extern void DataBlock__ctor_mA5BFE004E88059E0C0F1B7FB6786DDC9A34AD6E2 (void);
// 0x00000651 ZXing.Datamatrix.Internal.DataBlock[] ZXing.Datamatrix.Internal.DataBlock::getDataBlocks(System.Byte[],ZXing.Datamatrix.Internal.Version)
extern void DataBlock_getDataBlocks_mC36A098321916BAFCE360F900D4F8236DD863D00 (void);
// 0x00000652 System.Int32 ZXing.Datamatrix.Internal.DataBlock::get_NumDataCodewords()
extern void DataBlock_get_NumDataCodewords_m9F64EFEA06811087B55167D4183F0E834DD50243 (void);
// 0x00000653 System.Byte[] ZXing.Datamatrix.Internal.DataBlock::get_Codewords()
extern void DataBlock_get_Codewords_m2AB2987D09216947E94CBE3C758AD8AA43647542 (void);
// 0x00000654 ZXing.Common.DecoderResult ZXing.Datamatrix.Internal.DecodedBitStreamParser::decode(System.Byte[])
extern void DecodedBitStreamParser_decode_mC42A1A597D6F83C3D2EA2DD56044515B3D1D3228 (void);
// 0x00000655 System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeAsciiSegment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Text.StringBuilder,ZXing.Datamatrix.Internal.DecodedBitStreamParser/Mode&)
extern void DecodedBitStreamParser_decodeAsciiSegment_mF9FC8418831991136F70F5534E67D82981B44396 (void);
// 0x00000656 System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeC40Segment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeC40Segment_mB07959B45DD2A80738ADFAB482A02C11300FC9F2 (void);
// 0x00000657 System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeTextSegment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeTextSegment_mAD8EE51BB4BA37355C4CC88D3734B08B4AB805DD (void);
// 0x00000658 System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeAnsiX12Segment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeAnsiX12Segment_m8A5FA0295AB280FEA001DEDA7181647F3FF03DAB (void);
// 0x00000659 System.Void ZXing.Datamatrix.Internal.DecodedBitStreamParser::parseTwoBytes(System.Int32,System.Int32,System.Int32[])
extern void DecodedBitStreamParser_parseTwoBytes_m4D22E0393C04E7D4444B0E255B4CAE85DBFF62A2 (void);
// 0x0000065A System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeEdifactSegment(ZXing.Common.BitSource,System.Text.StringBuilder)
extern void DecodedBitStreamParser_decodeEdifactSegment_m6A7E257C5A130E3BD4686292BCC6680A337CD50F (void);
// 0x0000065B System.Boolean ZXing.Datamatrix.Internal.DecodedBitStreamParser::decodeBase256Segment(ZXing.Common.BitSource,System.Text.StringBuilder,System.Collections.Generic.IList`1<System.Byte[]>)
extern void DecodedBitStreamParser_decodeBase256Segment_m08BD48C08FC2EB3C964BE078E6F9FFC230C9A5EF (void);
// 0x0000065C System.Int32 ZXing.Datamatrix.Internal.DecodedBitStreamParser::unrandomize255State(System.Int32,System.Int32)
extern void DecodedBitStreamParser_unrandomize255State_m2134D435230458DBCD53E1DA81C19C682E0082BB (void);
// 0x0000065D System.Void ZXing.Datamatrix.Internal.DecodedBitStreamParser::.cctor()
extern void DecodedBitStreamParser__cctor_mC3982E18509A8B8AD8BF7D737FDB990D72B1CA4A (void);
// 0x0000065E System.Void ZXing.Datamatrix.Internal.Decoder::.ctor()
extern void Decoder__ctor_mCE27E19EBA3EC6D417FA001B6F8F39EF33B277FF (void);
// 0x0000065F ZXing.Common.DecoderResult ZXing.Datamatrix.Internal.Decoder::decode(System.Boolean[][])
extern void Decoder_decode_mE3A200FA5A94E67A034B6D26DA8D0937DA9301F1 (void);
// 0x00000660 ZXing.Common.DecoderResult ZXing.Datamatrix.Internal.Decoder::decode(ZXing.Common.BitMatrix)
extern void Decoder_decode_m8C1552577C4DFD72959E5DDB5BB0DD0BEEB36A48 (void);
// 0x00000661 System.Boolean ZXing.Datamatrix.Internal.Decoder::correctErrors(System.Byte[],System.Int32)
extern void Decoder_correctErrors_m29FB1BFA4667F72A28FF099CDA99D02D87F555DA (void);
// 0x00000662 System.Void ZXing.Datamatrix.Internal.Version::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,ZXing.Datamatrix.Internal.Version/ECBlocks)
extern void Version__ctor_m65718E372B88E852EAA44C01115EEFE076C9E13B (void);
// 0x00000663 System.Int32 ZXing.Datamatrix.Internal.Version::getVersionNumber()
extern void Version_getVersionNumber_m26FEA40A5844657C39A421008E71CCF743EF6F6E (void);
// 0x00000664 System.Int32 ZXing.Datamatrix.Internal.Version::getSymbolSizeRows()
extern void Version_getSymbolSizeRows_m23844A5AE8E3506918165AAE3DDA4DB0520242A6 (void);
// 0x00000665 System.Int32 ZXing.Datamatrix.Internal.Version::getSymbolSizeColumns()
extern void Version_getSymbolSizeColumns_m0B558F3EDDC8913D604DB11F95B496DA575211AB (void);
// 0x00000666 System.Int32 ZXing.Datamatrix.Internal.Version::getDataRegionSizeRows()
extern void Version_getDataRegionSizeRows_m14AC2D3F28AAB5A7C6A59FCD3451FF245C5B970C (void);
// 0x00000667 System.Int32 ZXing.Datamatrix.Internal.Version::getDataRegionSizeColumns()
extern void Version_getDataRegionSizeColumns_m246F25ED57270649AD405B2D88B5D6258D7848FB (void);
// 0x00000668 System.Int32 ZXing.Datamatrix.Internal.Version::getTotalCodewords()
extern void Version_getTotalCodewords_m781492734B917EB3CE3E3405E70362E10C8C8A64 (void);
// 0x00000669 ZXing.Datamatrix.Internal.Version/ECBlocks ZXing.Datamatrix.Internal.Version::getECBlocks()
extern void Version_getECBlocks_mCEF65FD7DF21697AD864E85488CF2982B6B03B3A (void);
// 0x0000066A ZXing.Datamatrix.Internal.Version ZXing.Datamatrix.Internal.Version::getVersionForDimensions(System.Int32,System.Int32)
extern void Version_getVersionForDimensions_mCE55D833283B1B93E1A4B853EF66C0A81A3BD377 (void);
// 0x0000066B System.String ZXing.Datamatrix.Internal.Version::ToString()
extern void Version_ToString_m5A61BBB35CEFB2637C1FB35814EC51175E1A1574 (void);
// 0x0000066C ZXing.Datamatrix.Internal.Version[] ZXing.Datamatrix.Internal.Version::buildVersions()
extern void Version_buildVersions_m8D0E5EC42DD8E037EF541416C873E327C9CFCB0A (void);
// 0x0000066D System.Void ZXing.Datamatrix.Internal.Version::.cctor()
extern void Version__cctor_m2C9DE7C5868569CF8F3792D09F4AB926446BED38 (void);
// 0x0000066E System.Void ZXing.Datamatrix.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.Datamatrix.Internal.Version/ECB)
extern void ECBlocks__ctor_m72399DDDDCC7680722E07BEF4882FF89146788D0 (void);
// 0x0000066F System.Void ZXing.Datamatrix.Internal.Version/ECBlocks::.ctor(System.Int32,ZXing.Datamatrix.Internal.Version/ECB,ZXing.Datamatrix.Internal.Version/ECB)
extern void ECBlocks__ctor_mD3FF3BC5DD32245AC797BCAE9556AAC6014C9046 (void);
// 0x00000670 System.Int32 ZXing.Datamatrix.Internal.Version/ECBlocks::get_ECCodewords()
extern void ECBlocks_get_ECCodewords_mDE5C53421D5EB90BE2BD02348140ED446A03F6A8 (void);
// 0x00000671 ZXing.Datamatrix.Internal.Version/ECB[] ZXing.Datamatrix.Internal.Version/ECBlocks::get_ECBlocksValue()
extern void ECBlocks_get_ECBlocksValue_m4FFC95413150D9AF6922B3B610979655D16B6819 (void);
// 0x00000672 System.Void ZXing.Datamatrix.Internal.Version/ECB::.ctor(System.Int32,System.Int32)
extern void ECB__ctor_m3D0EF7A0EDD041B2074894B095618C52262B5366 (void);
// 0x00000673 System.Int32 ZXing.Datamatrix.Internal.Version/ECB::get_Count()
extern void ECB_get_Count_m500674EE11B169A8F76F80EEC5964E590141E595 (void);
// 0x00000674 System.Int32 ZXing.Datamatrix.Internal.Version/ECB::get_DataCodewords()
extern void ECB_get_DataCodewords_m422E72F5DB34EC2057E9880BC624E13B018C1F5B (void);
// 0x00000675 System.Void ZXing.Datamatrix.Internal.Detector::.ctor(ZXing.Common.BitMatrix)
extern void Detector__ctor_m8F0920DEB6E3BDE35A96ADED7D1E7C917F638B3F (void);
// 0x00000676 ZXing.Common.DetectorResult ZXing.Datamatrix.Internal.Detector::detect()
extern void Detector_detect_m7ADA5EF4E17BA7D0D5E5712ADB4A69B22BC88B22 (void);
// 0x00000677 ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector::shiftPoint(ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern void Detector_shiftPoint_mFF4D54A50A84008FE3E3BD51448D04F67D0FAAC3 (void);
// 0x00000678 ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector::moveAway(ZXing.ResultPoint,System.Single,System.Single)
extern void Detector_moveAway_m2CF1B41044B7946D63C173230875D5920B688618 (void);
// 0x00000679 ZXing.ResultPoint[] ZXing.Datamatrix.Internal.Detector::detectSolid1(ZXing.ResultPoint[])
extern void Detector_detectSolid1_m058793E2CBB758EB58FA3F19AF40D391D8E17A94 (void);
// 0x0000067A ZXing.ResultPoint[] ZXing.Datamatrix.Internal.Detector::detectSolid2(ZXing.ResultPoint[])
extern void Detector_detectSolid2_m55943E6660061417B230F44C26DD592AB5EE8726 (void);
// 0x0000067B ZXing.ResultPoint ZXing.Datamatrix.Internal.Detector::correctTopRight(ZXing.ResultPoint[])
extern void Detector_correctTopRight_m4AFBAF12BAF3197213A1E4383B02645061489DCB (void);
// 0x0000067C ZXing.ResultPoint[] ZXing.Datamatrix.Internal.Detector::shiftToModuleCenter(ZXing.ResultPoint[])
extern void Detector_shiftToModuleCenter_m97187AAD9E4AC894F5D0272D57B1570FC0A76BD7 (void);
// 0x0000067D System.Boolean ZXing.Datamatrix.Internal.Detector::isValid(ZXing.ResultPoint)
extern void Detector_isValid_m4EC00E0C6FCCE52B8AE95E6837478D17EFE5206C (void);
// 0x0000067E ZXing.Common.BitMatrix ZXing.Datamatrix.Internal.Detector::sampleGrid(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,System.Int32,System.Int32)
extern void Detector_sampleGrid_m5C9B882441FF7EB54BDF67E1B58D92A54EBDB0DC (void);
// 0x0000067F System.Int32 ZXing.Datamatrix.Internal.Detector::transitionsBetween(ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_transitionsBetween_mA85F407B07A571BF125ADB61388DE62163D88B08 (void);
// 0x00000680 System.Int32 ZXing.Common.BitArray::get_Size()
extern void BitArray_get_Size_m4DF38399DD2C5743CC831361C2BF1EADC62BF3C6 (void);
// 0x00000681 System.Int32 ZXing.Common.BitArray::get_SizeInBytes()
extern void BitArray_get_SizeInBytes_mA160D7ABAEB85F66A6394F3BFB69ABE9E032F258 (void);
// 0x00000682 System.Boolean ZXing.Common.BitArray::get_Item(System.Int32)
extern void BitArray_get_Item_mAFA7A42335FDD2BCF011CDA19BEE6A73B133E976 (void);
// 0x00000683 System.Void ZXing.Common.BitArray::set_Item(System.Int32,System.Boolean)
extern void BitArray_set_Item_mFC80B9E3B0D5F639ED4BA9C4D7A19E333F6B1B7F (void);
// 0x00000684 System.Void ZXing.Common.BitArray::.ctor()
extern void BitArray__ctor_m7A6A39E4287B6641CBF085414D8AEC6575CCD4EE (void);
// 0x00000685 System.Void ZXing.Common.BitArray::.ctor(System.Int32)
extern void BitArray__ctor_mEE09C1AAC6D553214FE658ADB254428A778FEA39 (void);
// 0x00000686 System.Void ZXing.Common.BitArray::.ctor(System.Int32[],System.Int32)
extern void BitArray__ctor_m6320AAA51371FED5B566D157E96F5151CA7C41B5 (void);
// 0x00000687 System.Void ZXing.Common.BitArray::ensureCapacity(System.Int32)
extern void BitArray_ensureCapacity_mFBF33B63B033066CDF2F419D096640D8F14BB1E2 (void);
// 0x00000688 System.Void ZXing.Common.BitArray::flip(System.Int32)
extern void BitArray_flip_m03814ED6F72AED6F3236024D7CC4BF6BD2EBB436 (void);
// 0x00000689 System.Int32 ZXing.Common.BitArray::numberOfTrailingZeros(System.Int32)
extern void BitArray_numberOfTrailingZeros_m094CE884719C78DD24055D8B77DE05E920EE64A7 (void);
// 0x0000068A System.Int32 ZXing.Common.BitArray::getNextSet(System.Int32)
extern void BitArray_getNextSet_m7D8FDFC67F413BABE26FD362EAAFC2646C4F76EB (void);
// 0x0000068B System.Int32 ZXing.Common.BitArray::getNextUnset(System.Int32)
extern void BitArray_getNextUnset_mBD48BDF5504BEB898B01393796FB216B6D64BD06 (void);
// 0x0000068C System.Void ZXing.Common.BitArray::setBulk(System.Int32,System.Int32)
extern void BitArray_setBulk_mB87C9BF176CD517704338DACEB077EB88524B396 (void);
// 0x0000068D System.Void ZXing.Common.BitArray::setRange(System.Int32,System.Int32)
extern void BitArray_setRange_m900B6D94995432AC75DA57D428FCBA29794E562C (void);
// 0x0000068E System.Void ZXing.Common.BitArray::clear()
extern void BitArray_clear_m971A2F552D290FA83246566705749F495EDD4307 (void);
// 0x0000068F System.Boolean ZXing.Common.BitArray::isRange(System.Int32,System.Int32,System.Boolean)
extern void BitArray_isRange_mFF511A8272B3F4CCDCAE42853CDB38C959549060 (void);
// 0x00000690 System.Void ZXing.Common.BitArray::appendBit(System.Boolean)
extern void BitArray_appendBit_mBBACCEDC6EE9337F6BC1D698C2CC882B45AA8807 (void);
// 0x00000691 System.Int32[] ZXing.Common.BitArray::get_Array()
extern void BitArray_get_Array_m1FEA291EBEB035CEBB7D2EF2F445DCDB7D8041D6 (void);
// 0x00000692 System.Void ZXing.Common.BitArray::appendBits(System.Int32,System.Int32)
extern void BitArray_appendBits_m85DD51F28D55A57CC604D35276425BF9BD06B19E (void);
// 0x00000693 System.Void ZXing.Common.BitArray::appendBitArray(ZXing.Common.BitArray)
extern void BitArray_appendBitArray_mE7864B47606471ECC538D9A084805FD34AE4767C (void);
// 0x00000694 System.Void ZXing.Common.BitArray::xor(ZXing.Common.BitArray)
extern void BitArray_xor_m522FC838D5251A6D8E8F4C4EE90F4DB39C1C3E4C (void);
// 0x00000695 System.Void ZXing.Common.BitArray::toBytes(System.Int32,System.Byte[],System.Int32,System.Int32)
extern void BitArray_toBytes_m003C974D86A84418ED4A3C2F21626F67F16B82D2 (void);
// 0x00000696 System.Void ZXing.Common.BitArray::reverse()
extern void BitArray_reverse_mD73A2F7769C812C5462CDFAF606045DA17FD8FFF (void);
// 0x00000697 System.Int32[] ZXing.Common.BitArray::makeArray(System.Int32)
extern void BitArray_makeArray_m0F95D55F98AD7C2989D406C3CEBF072AAB5A056B (void);
// 0x00000698 System.Boolean ZXing.Common.BitArray::Equals(System.Object)
extern void BitArray_Equals_m7BCB281AF1D0D5864A89C008D01C11E0C2B9AC5A (void);
// 0x00000699 System.Int32 ZXing.Common.BitArray::GetHashCode()
extern void BitArray_GetHashCode_m244214E8D90CEE4136FD8ED5FEBAF1CE4981E0C9 (void);
// 0x0000069A System.String ZXing.Common.BitArray::ToString()
extern void BitArray_ToString_m8820612AB7EF12D953BB24EF384BF773ABB75FA6 (void);
// 0x0000069B System.Object ZXing.Common.BitArray::Clone()
extern void BitArray_Clone_mF0D2E92181E2265376936D0D054570DEF55BF834 (void);
// 0x0000069C System.Void ZXing.Common.BitArray::.cctor()
extern void BitArray__cctor_m3CBBE968324F09DA08289066CFE2893F73B5B1DF (void);
// 0x0000069D System.Int32 ZXing.Common.BitMatrix::get_Width()
extern void BitMatrix_get_Width_m33298FBF710A8752CF28839B453B350B3E70E3DD (void);
// 0x0000069E System.Int32 ZXing.Common.BitMatrix::get_Height()
extern void BitMatrix_get_Height_mB0EEC75005372D759622F2C610ED7A9E4D635C07 (void);
// 0x0000069F System.Int32 ZXing.Common.BitMatrix::get_Dimension()
extern void BitMatrix_get_Dimension_m34E1EB9AA1F26C026AEFFDFA5BB2988CE0C80F0E (void);
// 0x000006A0 System.Int32 ZXing.Common.BitMatrix::get_RowSize()
extern void BitMatrix_get_RowSize_m0D4AAAB44EFEB30F24338076B9650C2C8043039B (void);
// 0x000006A1 System.Void ZXing.Common.BitMatrix::.ctor(System.Int32)
extern void BitMatrix__ctor_mE82BCBEF00A28E06132992A1119ED7373196FA9E (void);
// 0x000006A2 System.Void ZXing.Common.BitMatrix::.ctor(System.Int32,System.Int32)
extern void BitMatrix__ctor_m0288013DC4C9267ADD2FACB77C631BF9637B4B8B (void);
// 0x000006A3 System.Void ZXing.Common.BitMatrix::.ctor(System.Int32,System.Int32,System.Int32,System.Int32[])
extern void BitMatrix__ctor_m176EC1F1A4E880F672B4D101B48FFF58D9486EAA (void);
// 0x000006A4 System.Void ZXing.Common.BitMatrix::.ctor(System.Int32,System.Int32,System.Int32[])
extern void BitMatrix__ctor_mA46CCA4D3F98D5C32BF31861ACF9BD61B440FC15 (void);
// 0x000006A5 ZXing.Common.BitMatrix ZXing.Common.BitMatrix::parse(System.Boolean[][])
extern void BitMatrix_parse_m43CAD92BDC84A404F18E3578AE81B01D799C7343 (void);
// 0x000006A6 ZXing.Common.BitMatrix ZXing.Common.BitMatrix::parse(System.String,System.String,System.String)
extern void BitMatrix_parse_mEB9E4973546882F5F0168789ECF9B81AAA89D77A (void);
// 0x000006A7 System.Boolean ZXing.Common.BitMatrix::get_Item(System.Int32,System.Int32)
extern void BitMatrix_get_Item_mC54BF7E1E3134DFBDE2A4A86DBD7AC3A601B9DFD (void);
// 0x000006A8 System.Void ZXing.Common.BitMatrix::set_Item(System.Int32,System.Int32,System.Boolean)
extern void BitMatrix_set_Item_m7A55ACC841365AD83D5227278D0A05D1C5803694 (void);
// 0x000006A9 System.Void ZXing.Common.BitMatrix::flip(System.Int32,System.Int32)
extern void BitMatrix_flip_mDCF333F81AB50B0EF290A8DD3DDF3AF317824967 (void);
// 0x000006AA System.Void ZXing.Common.BitMatrix::flipWhen(System.Func`3<System.Int32,System.Int32,System.Boolean>)
extern void BitMatrix_flipWhen_m5C54FACCC2AF88DDE0DAF94CE43F3CE072E0246A (void);
// 0x000006AB System.Void ZXing.Common.BitMatrix::xor(ZXing.Common.BitMatrix)
extern void BitMatrix_xor_mB9C08716A8E3D0A0CC1DA27529C74F47E6F6C947 (void);
// 0x000006AC System.Void ZXing.Common.BitMatrix::clear()
extern void BitMatrix_clear_m6C9DB4600035ADD0C0947AB21FBF0E5B46116A65 (void);
// 0x000006AD System.Void ZXing.Common.BitMatrix::setRegion(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BitMatrix_setRegion_mD627055A82842183FFC48AE600D8825C81D7A7A2 (void);
// 0x000006AE ZXing.Common.BitArray ZXing.Common.BitMatrix::getRow(System.Int32,ZXing.Common.BitArray)
extern void BitMatrix_getRow_m477B04148EB01249C219ED23049B0E7F3D70461C (void);
// 0x000006AF System.Void ZXing.Common.BitMatrix::setRow(System.Int32,ZXing.Common.BitArray)
extern void BitMatrix_setRow_mD4AD62A436FE9F57EDC1C9834E52862C4B3F56EA (void);
// 0x000006B0 System.Void ZXing.Common.BitMatrix::rotate180()
extern void BitMatrix_rotate180_mEDA2AA7FE01B30477B1DE8FBA01EA8D7A7F09CF5 (void);
// 0x000006B1 System.Int32[] ZXing.Common.BitMatrix::getEnclosingRectangle()
extern void BitMatrix_getEnclosingRectangle_mE927073216698358821D7945E7F8A67AACE2919B (void);
// 0x000006B2 System.Int32[] ZXing.Common.BitMatrix::getTopLeftOnBit()
extern void BitMatrix_getTopLeftOnBit_mC5199987E1E4F7975DDABB20572DBCFAE81023CA (void);
// 0x000006B3 System.Int32[] ZXing.Common.BitMatrix::getBottomRightOnBit()
extern void BitMatrix_getBottomRightOnBit_m8A26B0A5AF83CDF3CEC80C62132E410804F19564 (void);
// 0x000006B4 System.Boolean ZXing.Common.BitMatrix::Equals(System.Object)
extern void BitMatrix_Equals_mE2C9839C9DD3EFA8AFB8EF3A4C9B3DE2B14E7D1C (void);
// 0x000006B5 System.Int32 ZXing.Common.BitMatrix::GetHashCode()
extern void BitMatrix_GetHashCode_m9402D3FABD681F2191BEEAFB707C775948BB4C87 (void);
// 0x000006B6 System.String ZXing.Common.BitMatrix::ToString()
extern void BitMatrix_ToString_m4DCC7B4E385264C5EBCF913006C78E6E52AE1A0A (void);
// 0x000006B7 System.String ZXing.Common.BitMatrix::ToString(System.String,System.String)
extern void BitMatrix_ToString_mB00E99EAD04BD7C6C500B6AFC2070E91067BFCF9 (void);
// 0x000006B8 System.String ZXing.Common.BitMatrix::ToString(System.String,System.String,System.String)
extern void BitMatrix_ToString_mFA86909D5E2D1D589E08B3A7580D5C524F4D0064 (void);
// 0x000006B9 System.String ZXing.Common.BitMatrix::buildToString(System.String,System.String,System.String)
extern void BitMatrix_buildToString_m2832ED0EF3570D346731BD0620198A0065DC060B (void);
// 0x000006BA System.Object ZXing.Common.BitMatrix::Clone()
extern void BitMatrix_Clone_mC33B8331DF830122404F2FCAE1020C4D062AB567 (void);
// 0x000006BB System.Void ZXing.Common.BitSource::.ctor(System.Byte[])
extern void BitSource__ctor_mC4D81F2535DE34C80545A5450D34F327B7067A69 (void);
// 0x000006BC System.Int32 ZXing.Common.BitSource::get_BitOffset()
extern void BitSource_get_BitOffset_mCF760D4152A853A99E525E4A36ADA79467100BD9 (void);
// 0x000006BD System.Int32 ZXing.Common.BitSource::get_ByteOffset()
extern void BitSource_get_ByteOffset_m8C2BE7D73D3B1820B2156E7D3371FCC4198D57A7 (void);
// 0x000006BE System.Int32 ZXing.Common.BitSource::readBits(System.Int32)
extern void BitSource_readBits_m7CEDDB2C5F2E399BBFADBCCF768939F4290C3492 (void);
// 0x000006BF System.Int32 ZXing.Common.BitSource::available()
extern void BitSource_available_mBBED7B769D334071B787E7B12F966A0E61683B8A (void);
// 0x000006C0 System.String ZXing.Common.CharacterSetECI::get_EncodingName()
extern void CharacterSetECI_get_EncodingName_m0132E1CCC822D69F0D5C78EC11C24B1F433BB997 (void);
// 0x000006C1 System.Void ZXing.Common.CharacterSetECI::.cctor()
extern void CharacterSetECI__cctor_m7DD580F00444462FA171012D61151D6C41BF6B89 (void);
// 0x000006C2 System.Void ZXing.Common.CharacterSetECI::.ctor(System.Int32,System.String)
extern void CharacterSetECI__ctor_m0896C7F553981F80059C1D11239DF7C7D901D9E0 (void);
// 0x000006C3 System.Void ZXing.Common.CharacterSetECI::addCharacterSet(System.Int32,System.String)
extern void CharacterSetECI_addCharacterSet_m725162161BB2EE5A68736CD39C6AAA19279E1B28 (void);
// 0x000006C4 System.Void ZXing.Common.CharacterSetECI::addCharacterSet(System.Int32,System.String[])
extern void CharacterSetECI_addCharacterSet_m0B4AA31F942C2D9C8F5C8A5DCBFEEF9DA979135C (void);
// 0x000006C5 ZXing.Common.CharacterSetECI ZXing.Common.CharacterSetECI::getCharacterSetECIByValue(System.Int32)
extern void CharacterSetECI_getCharacterSetECIByValue_m597E8F5C7F38A2F0D1BE871F13B026B4293AFDA0 (void);
// 0x000006C6 ZXing.Common.CharacterSetECI ZXing.Common.CharacterSetECI::getCharacterSetECIByName(System.String)
extern void CharacterSetECI_getCharacterSetECIByName_m6D13B4390078B1EEADB047E2571B95CAA2333711 (void);
// 0x000006C7 System.Byte[] ZXing.Common.DecoderResult::get_RawBytes()
extern void DecoderResult_get_RawBytes_m10433C77563D755542DC53B22FF569CB40671ED6 (void);
// 0x000006C8 System.Void ZXing.Common.DecoderResult::set_RawBytes(System.Byte[])
extern void DecoderResult_set_RawBytes_m76DA0DECBD0F1F6858DC5147AAE4F01D1D6ED11B (void);
// 0x000006C9 System.Int32 ZXing.Common.DecoderResult::get_NumBits()
extern void DecoderResult_get_NumBits_m1CC1FAC264D01170B4EE5ED3940C516F7015BFEF (void);
// 0x000006CA System.Void ZXing.Common.DecoderResult::set_NumBits(System.Int32)
extern void DecoderResult_set_NumBits_m07D1DB88C8CE2BA014207A87A82E97A74F1D8373 (void);
// 0x000006CB System.String ZXing.Common.DecoderResult::get_Text()
extern void DecoderResult_get_Text_mA346F7E285B56E86ECD455EAED9FA37C8E3B0978 (void);
// 0x000006CC System.Void ZXing.Common.DecoderResult::set_Text(System.String)
extern void DecoderResult_set_Text_m3D3E09DEB0639CED8199F58A4E1D37D711886B16 (void);
// 0x000006CD System.Collections.Generic.IList`1<System.Byte[]> ZXing.Common.DecoderResult::get_ByteSegments()
extern void DecoderResult_get_ByteSegments_m5731694C4E0AC645E2414F8D88B887EC75E50993 (void);
// 0x000006CE System.Void ZXing.Common.DecoderResult::set_ByteSegments(System.Collections.Generic.IList`1<System.Byte[]>)
extern void DecoderResult_set_ByteSegments_m472023F889663A57E28FFD7CEF98091C8F53E592 (void);
// 0x000006CF System.String ZXing.Common.DecoderResult::get_ECLevel()
extern void DecoderResult_get_ECLevel_m370D1148416EEF16E1403BDC9622BDA362DEF356 (void);
// 0x000006D0 System.Void ZXing.Common.DecoderResult::set_ECLevel(System.String)
extern void DecoderResult_set_ECLevel_m9602C70D943006A26044894E6F544944FA094639 (void);
// 0x000006D1 System.Boolean ZXing.Common.DecoderResult::get_StructuredAppend()
extern void DecoderResult_get_StructuredAppend_m980A5ECD49EE01C1A43B64031B5EE3983E157BD6 (void);
// 0x000006D2 System.Int32 ZXing.Common.DecoderResult::get_ErrorsCorrected()
extern void DecoderResult_get_ErrorsCorrected_m4816939A62187FB442762CD8FBEEE8BA753AC1C5 (void);
// 0x000006D3 System.Void ZXing.Common.DecoderResult::set_ErrorsCorrected(System.Int32)
extern void DecoderResult_set_ErrorsCorrected_m5DDAC6D51B8B1133C6F6A284C4549C614DEE7AC8 (void);
// 0x000006D4 System.Int32 ZXing.Common.DecoderResult::get_StructuredAppendSequenceNumber()
extern void DecoderResult_get_StructuredAppendSequenceNumber_m100E55D100CD9B70F0653CB9D916F1894AA25FDB (void);
// 0x000006D5 System.Void ZXing.Common.DecoderResult::set_StructuredAppendSequenceNumber(System.Int32)
extern void DecoderResult_set_StructuredAppendSequenceNumber_m42A590E8788A70C45463B186C93BC87EE4E682AE (void);
// 0x000006D6 System.Int32 ZXing.Common.DecoderResult::get_Erasures()
extern void DecoderResult_get_Erasures_m506D8B484E02C20310202DBB004F9690A454E78C (void);
// 0x000006D7 System.Void ZXing.Common.DecoderResult::set_Erasures(System.Int32)
extern void DecoderResult_set_Erasures_m9CA0CA95144177355CCA47FB800D0B49856B28D8 (void);
// 0x000006D8 System.Int32 ZXing.Common.DecoderResult::get_StructuredAppendParity()
extern void DecoderResult_get_StructuredAppendParity_m5D95FA2B02F68E689D8F78BCC513322568AF1892 (void);
// 0x000006D9 System.Void ZXing.Common.DecoderResult::set_StructuredAppendParity(System.Int32)
extern void DecoderResult_set_StructuredAppendParity_mFA66847C80576E7625B25DCE686B60654BA05FC9 (void);
// 0x000006DA System.Object ZXing.Common.DecoderResult::get_Other()
extern void DecoderResult_get_Other_m14F9227981860B2FA3687AAAF981FC759A6EF700 (void);
// 0x000006DB System.Void ZXing.Common.DecoderResult::set_Other(System.Object)
extern void DecoderResult_set_Other_mB9FE940C7118EC5E6D40B472CC32F776D449F7B2 (void);
// 0x000006DC System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String)
extern void DecoderResult__ctor_m7D04445A090ED460BE771B3B2DF57AAC8E6FAFF1 (void);
// 0x000006DD System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String,System.Int32,System.Int32)
extern void DecoderResult__ctor_m3741DA117665F12217EF92794E17C003378E6874 (void);
// 0x000006DE System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.Int32,System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String)
extern void DecoderResult__ctor_m122EF86C35C27FA4C3A40323849B20763476A2BD (void);
// 0x000006DF System.Void ZXing.Common.DecoderResult::.ctor(System.Byte[],System.Int32,System.String,System.Collections.Generic.IList`1<System.Byte[]>,System.String,System.Int32,System.Int32)
extern void DecoderResult__ctor_m0A7001D58008F9EC69184D5158AC3A2D6F2532C8 (void);
// 0x000006E0 System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object> ZXing.Common.DecodingOptions::get_Hints()
extern void DecodingOptions_get_Hints_mD35FD59AE533C6C03F6648681A5A993DF5B0954A (void);
// 0x000006E1 System.Void ZXing.Common.DecodingOptions::set_Hints(System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void DecodingOptions_set_Hints_m3539F092C766912ACE9CDA44CB6C685F01162176 (void);
// 0x000006E2 System.Void ZXing.Common.DecodingOptions::add_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern void DecodingOptions_add_ValueChanged_m2F4CA1ABCC8189BCDF994656A56EAAC54F96EBA7 (void);
// 0x000006E3 System.Void ZXing.Common.DecodingOptions::remove_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
extern void DecodingOptions_remove_ValueChanged_mC3015BD59DA7A19CA6B07EAA8F07EF399F1B32FA (void);
// 0x000006E4 System.Boolean ZXing.Common.DecodingOptions::get_TryHarder()
extern void DecodingOptions_get_TryHarder_mF05236C4627280C70D0DAF75519D6562412359F3 (void);
// 0x000006E5 System.Void ZXing.Common.DecodingOptions::set_TryHarder(System.Boolean)
extern void DecodingOptions_set_TryHarder_m849F6A91E1A0E8F0F5326176BFDF531A4F309C94 (void);
// 0x000006E6 System.Boolean ZXing.Common.DecodingOptions::get_PureBarcode()
extern void DecodingOptions_get_PureBarcode_m3CB262ECF641DE6648269A7B92E2FF37864566F8 (void);
// 0x000006E7 System.Void ZXing.Common.DecodingOptions::set_PureBarcode(System.Boolean)
extern void DecodingOptions_set_PureBarcode_m10E181A565532CDA02C63B676C2016422C810E66 (void);
// 0x000006E8 System.String ZXing.Common.DecodingOptions::get_CharacterSet()
extern void DecodingOptions_get_CharacterSet_m6C4F774AF64BADC83571FBBEFED6A7ED67DCFCDB (void);
// 0x000006E9 System.Void ZXing.Common.DecodingOptions::set_CharacterSet(System.String)
extern void DecodingOptions_set_CharacterSet_m22EBA73B1620CC7AD0B2D18CF04E0465503CF589 (void);
// 0x000006EA System.Collections.Generic.IList`1<ZXing.BarcodeFormat> ZXing.Common.DecodingOptions::get_PossibleFormats()
extern void DecodingOptions_get_PossibleFormats_m2ECFE51204A6C16B3A36C5E03D9B647BC7D9D5E1 (void);
// 0x000006EB System.Void ZXing.Common.DecodingOptions::set_PossibleFormats(System.Collections.Generic.IList`1<ZXing.BarcodeFormat>)
extern void DecodingOptions_set_PossibleFormats_m9F198E94B374D373F6D11921CCA221359035EA2F (void);
// 0x000006EC System.Boolean ZXing.Common.DecodingOptions::get_UseCode39ExtendedMode()
extern void DecodingOptions_get_UseCode39ExtendedMode_m242C71799419D1652FFCE73D0D362F0F396138B2 (void);
// 0x000006ED System.Void ZXing.Common.DecodingOptions::set_UseCode39ExtendedMode(System.Boolean)
extern void DecodingOptions_set_UseCode39ExtendedMode_mD782AFA600E7DD206DC1D319133AAB5214840A52 (void);
// 0x000006EE System.Boolean ZXing.Common.DecodingOptions::get_UseCode39RelaxedExtendedMode()
extern void DecodingOptions_get_UseCode39RelaxedExtendedMode_m65D53CABF4C6C62ED640C0D1599375C43746F10B (void);
// 0x000006EF System.Void ZXing.Common.DecodingOptions::set_UseCode39RelaxedExtendedMode(System.Boolean)
extern void DecodingOptions_set_UseCode39RelaxedExtendedMode_mEB7DE4B3512DC9026B6742DE73C99470FDBA0924 (void);
// 0x000006F0 System.Boolean ZXing.Common.DecodingOptions::get_AssumeCode39CheckDigit()
extern void DecodingOptions_get_AssumeCode39CheckDigit_m38B4D7ADDEB3B98966587848B217DE29D6F61833 (void);
// 0x000006F1 System.Void ZXing.Common.DecodingOptions::set_AssumeCode39CheckDigit(System.Boolean)
extern void DecodingOptions_set_AssumeCode39CheckDigit_m544BDFF56ED9C62F4BA687970ACF54F6304AAB3E (void);
// 0x000006F2 System.Boolean ZXing.Common.DecodingOptions::get_ReturnCodabarStartEnd()
extern void DecodingOptions_get_ReturnCodabarStartEnd_m9640663FCC4E045D29B3096A8B7BBE2630DC358E (void);
// 0x000006F3 System.Void ZXing.Common.DecodingOptions::set_ReturnCodabarStartEnd(System.Boolean)
extern void DecodingOptions_set_ReturnCodabarStartEnd_m5B7D25550D778CA717621A13B972CB52939F6E4E (void);
// 0x000006F4 System.Boolean ZXing.Common.DecodingOptions::get_AssumeGS1()
extern void DecodingOptions_get_AssumeGS1_m59AE07E37C73A3BA22B04083E29D0B8109CF3627 (void);
// 0x000006F5 System.Void ZXing.Common.DecodingOptions::set_AssumeGS1(System.Boolean)
extern void DecodingOptions_set_AssumeGS1_mC1786B7F39DE001B88F6BA9FAF1563E986892785 (void);
// 0x000006F6 System.Boolean ZXing.Common.DecodingOptions::get_AssumeMSICheckDigit()
extern void DecodingOptions_get_AssumeMSICheckDigit_m877BCAF2C0EA3C9DB1DAA7D8DE82EC6FC9554FF2 (void);
// 0x000006F7 System.Void ZXing.Common.DecodingOptions::set_AssumeMSICheckDigit(System.Boolean)
extern void DecodingOptions_set_AssumeMSICheckDigit_m117BB641C2BF3CDDC4216F92B3A9B0536E832D41 (void);
// 0x000006F8 System.Int32[] ZXing.Common.DecodingOptions::get_AllowedLengths()
extern void DecodingOptions_get_AllowedLengths_m500140D14F0D95DA13CEAFCC75DFC016391F0415 (void);
// 0x000006F9 System.Void ZXing.Common.DecodingOptions::set_AllowedLengths(System.Int32[])
extern void DecodingOptions_set_AllowedLengths_mCA9D22EFAF34635DD56BFA0A6A4BB9E9E8EC36F2 (void);
// 0x000006FA System.Int32[] ZXing.Common.DecodingOptions::get_AllowedEANExtensions()
extern void DecodingOptions_get_AllowedEANExtensions_mEE96CBB011CEC4886B17E85402E5E1FDF6350EAC (void);
// 0x000006FB System.Void ZXing.Common.DecodingOptions::set_AllowedEANExtensions(System.Int32[])
extern void DecodingOptions_set_AllowedEANExtensions_m18A0F591399ECE1B24CA1DCA39CE5EEA53D36C7F (void);
// 0x000006FC System.Void ZXing.Common.DecodingOptions::.ctor()
extern void DecodingOptions__ctor_mC1A403C24C25E1B2CA7A9E0F5D468BD76CFFC3D6 (void);
// 0x000006FD System.Void ZXing.Common.DecodingOptions::<.ctor>b__43_0(System.Object,System.EventArgs)
extern void DecodingOptions_U3C_ctorU3Eb__43_0_m713C1345D194F86F61799E4C0E2207F835B0FF89 (void);
// 0x000006FE System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::add_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
// 0x000006FF System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::remove_ValueChanged(System.Action`2<System.Object,System.EventArgs>)
// 0x00000700 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::.ctor()
// 0x00000701 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::OnValueChanged()
// 0x00000702 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Add(TKey,TValue)
// 0x00000703 System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::ContainsKey(TKey)
// 0x00000704 System.Collections.Generic.ICollection`1<TKey> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::get_Keys()
// 0x00000705 System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Remove(TKey)
// 0x00000706 System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::TryGetValue(TKey,TValue&)
// 0x00000707 System.Collections.Generic.ICollection`1<TValue> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::get_Values()
// 0x00000708 TValue ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::get_Item(TKey)
// 0x00000709 System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::set_Item(TKey,TValue)
// 0x0000070A System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000070B System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Clear()
// 0x0000070C System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000070D System.Void ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x0000070E System.Int32 ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::get_Count()
// 0x0000070F System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::get_IsReadOnly()
// 0x00000710 System.Boolean ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000711 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::GetEnumerator()
// 0x00000712 System.Collections.IEnumerator ZXing.Common.DecodingOptions/ChangeNotifyDictionary`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000713 ZXing.Common.BitMatrix ZXing.Common.DefaultGridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void DefaultGridSampler_sampleGrid_m969E4845DFA1E1648CFDC932ABBFF189527D70DF (void);
// 0x00000714 ZXing.Common.BitMatrix ZXing.Common.DefaultGridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,ZXing.Common.PerspectiveTransform)
extern void DefaultGridSampler_sampleGrid_m191F7B1B6113F490707B0B9F8C37CE81A8DC7A44 (void);
// 0x00000715 System.Void ZXing.Common.DefaultGridSampler::.ctor()
extern void DefaultGridSampler__ctor_m62D48782766E1F1A6C90D614E5654EB705CFE253 (void);
// 0x00000716 ZXing.Common.BitMatrix ZXing.Common.DetectorResult::get_Bits()
extern void DetectorResult_get_Bits_m8B97D1D1F2C614E247099F140CF5E33C144DC85A (void);
// 0x00000717 System.Void ZXing.Common.DetectorResult::set_Bits(ZXing.Common.BitMatrix)
extern void DetectorResult_set_Bits_mF0FFE6D538FEC3F4F159183B70F726DB8BBFCF22 (void);
// 0x00000718 ZXing.ResultPoint[] ZXing.Common.DetectorResult::get_Points()
extern void DetectorResult_get_Points_m463FC2087C0A15AD97E3B48993ABD05271E19A51 (void);
// 0x00000719 System.Void ZXing.Common.DetectorResult::set_Points(ZXing.ResultPoint[])
extern void DetectorResult_set_Points_mD6979B901F45B2A8903F34942FAE72342262D847 (void);
// 0x0000071A System.Void ZXing.Common.DetectorResult::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPoint[])
extern void DetectorResult__ctor_mBC6584792C5B7DA3F28012FE70D28B90FDE97AFE (void);
// 0x0000071B System.Int32 ZXing.Common.ECI::get_Value()
extern void ECI_get_Value_m4E525866285ADF1212BCE2B522176CE9A56515FD (void);
// 0x0000071C System.Void ZXing.Common.ECI::set_Value(System.Int32)
extern void ECI_set_Value_m39C291A3860B64138A64ECACF50410233420AE1E (void);
// 0x0000071D System.Void ZXing.Common.ECI::.ctor(System.Int32)
extern void ECI__ctor_m5AB03E8B5083B1E2350EAE59FAFE723024B984A9 (void);
// 0x0000071E ZXing.Common.ECI ZXing.Common.ECI::getECIByValue(System.Int32)
extern void ECI_getECIByValue_mF4CEDB3D718FD7235D161A0F122558A4DB70FB44 (void);
// 0x0000071F System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object> ZXing.Common.EncodingOptions::get_Hints()
extern void EncodingOptions_get_Hints_m35FF278C5959F8063174ED36516E87938E8CB019 (void);
// 0x00000720 System.Void ZXing.Common.EncodingOptions::set_Hints(System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void EncodingOptions_set_Hints_m172BC294493BC43F416BCAB600CA8FC23855E72A (void);
// 0x00000721 System.Int32 ZXing.Common.EncodingOptions::get_Height()
extern void EncodingOptions_get_Height_mE00290CDC21181E77087A6113B2658C589CFDB24 (void);
// 0x00000722 System.Void ZXing.Common.EncodingOptions::set_Height(System.Int32)
extern void EncodingOptions_set_Height_mE7AF0C5A6CE6741DB387574DBF7CD23E993BD80C (void);
// 0x00000723 System.Int32 ZXing.Common.EncodingOptions::get_Width()
extern void EncodingOptions_get_Width_mCC91F07FB4C7728246CECEC9A3E6932F36A31F16 (void);
// 0x00000724 System.Void ZXing.Common.EncodingOptions::set_Width(System.Int32)
extern void EncodingOptions_set_Width_mEA75DD346B1FA465B32DE799B9180E5F4A27E17B (void);
// 0x00000725 System.Boolean ZXing.Common.EncodingOptions::get_PureBarcode()
extern void EncodingOptions_get_PureBarcode_m95DE99A92E77F4BADE6D2BB51EBFA4EF0C0195E0 (void);
// 0x00000726 System.Void ZXing.Common.EncodingOptions::set_PureBarcode(System.Boolean)
extern void EncodingOptions_set_PureBarcode_m6875B158B2E45DBC960360CDB4EF07806B087BB9 (void);
// 0x00000727 System.Int32 ZXing.Common.EncodingOptions::get_Margin()
extern void EncodingOptions_get_Margin_mE8C52BAEC33258F73862415461681F07DBDC9C95 (void);
// 0x00000728 System.Void ZXing.Common.EncodingOptions::set_Margin(System.Int32)
extern void EncodingOptions_set_Margin_mB4F92033170F1504F3A15F64C56CF064FC77DD00 (void);
// 0x00000729 System.Boolean ZXing.Common.EncodingOptions::get_GS1Format()
extern void EncodingOptions_get_GS1Format_mF6DC0533B3F7133F42C4F9BEDE66D68F79F42A88 (void);
// 0x0000072A System.Void ZXing.Common.EncodingOptions::set_GS1Format(System.Boolean)
extern void EncodingOptions_set_GS1Format_m644F015756C60899A9BBD17119F260F76FCCF732 (void);
// 0x0000072B System.Void ZXing.Common.EncodingOptions::.ctor()
extern void EncodingOptions__ctor_mE1BC05F9615DDD0B3334EEE4E5E86C95F45108CF (void);
// 0x0000072C System.Void ZXing.Common.GlobalHistogramBinarizer::.ctor(ZXing.LuminanceSource)
extern void GlobalHistogramBinarizer__ctor_m9A8D2F3FE439047307D774950A763C8F4976D2C9 (void);
// 0x0000072D ZXing.Common.BitArray ZXing.Common.GlobalHistogramBinarizer::getBlackRow(System.Int32,ZXing.Common.BitArray)
extern void GlobalHistogramBinarizer_getBlackRow_m555A993F29D76B0FA0E5639A63E035FFC3F6166C (void);
// 0x0000072E ZXing.Common.BitMatrix ZXing.Common.GlobalHistogramBinarizer::get_BlackMatrix()
extern void GlobalHistogramBinarizer_get_BlackMatrix_m334B20D4DB054CE3CFC0816CADA4779BA5089D8C (void);
// 0x0000072F ZXing.Binarizer ZXing.Common.GlobalHistogramBinarizer::createBinarizer(ZXing.LuminanceSource)
extern void GlobalHistogramBinarizer_createBinarizer_mC6CF4F5C22E46942FC32AC98AA1AB780B131D17F (void);
// 0x00000730 System.Void ZXing.Common.GlobalHistogramBinarizer::initArrays(System.Int32)
extern void GlobalHistogramBinarizer_initArrays_m3E1BA663F13DDBD203419D8908BE83008BA172A9 (void);
// 0x00000731 System.Boolean ZXing.Common.GlobalHistogramBinarizer::estimateBlackPoint(System.Int32[],System.Int32&)
extern void GlobalHistogramBinarizer_estimateBlackPoint_m6355EB05C2DA98D775B979469DEB519C2802FB3B (void);
// 0x00000732 System.Void ZXing.Common.GlobalHistogramBinarizer::.cctor()
extern void GlobalHistogramBinarizer__cctor_mB1ED007AE1A57EAB76E7AACB0ACC51E4CD39FB42 (void);
// 0x00000733 ZXing.Common.GridSampler ZXing.Common.GridSampler::get_Instance()
extern void GridSampler_get_Instance_mEB420067749ABD696ADD56BA7A1479E6A8A65FCB (void);
// 0x00000734 System.Void ZXing.Common.GridSampler::setGridSampler(ZXing.Common.GridSampler)
extern void GridSampler_setGridSampler_mF1433D38996AF1A613DC1DCEE5E12E1E44A116C0 (void);
// 0x00000735 ZXing.Common.BitMatrix ZXing.Common.GridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
// 0x00000736 ZXing.Common.BitMatrix ZXing.Common.GridSampler::sampleGrid(ZXing.Common.BitMatrix,System.Int32,System.Int32,ZXing.Common.PerspectiveTransform)
extern void GridSampler_sampleGrid_mBAB1ADFF0655CBEE833C365BDB7EA85B95AAEED5 (void);
// 0x00000737 System.Boolean ZXing.Common.GridSampler::checkAndNudgePoints(ZXing.Common.BitMatrix,System.Single[])
extern void GridSampler_checkAndNudgePoints_m465A6B9F18DB964A403FF69CC005F7743D1A335E (void);
// 0x00000738 System.Void ZXing.Common.GridSampler::.ctor()
extern void GridSampler__ctor_mA67246E10FA276F048E6123C0B0B26A2E9B5231C (void);
// 0x00000739 System.Void ZXing.Common.GridSampler::.cctor()
extern void GridSampler__cctor_mE6D09255811C7FF1F6C7DC807FD5677A874020BF (void);
// 0x0000073A ZXing.Common.BitMatrix ZXing.Common.HybridBinarizer::get_BlackMatrix()
extern void HybridBinarizer_get_BlackMatrix_mD5F3AAA45194601E6BCF7DB4A50D27A744F8FA46 (void);
// 0x0000073B System.Void ZXing.Common.HybridBinarizer::.ctor(ZXing.LuminanceSource)
extern void HybridBinarizer__ctor_mFC8614CC8388B1A2D6AA5F88AA487FCC6ACCB011 (void);
// 0x0000073C ZXing.Binarizer ZXing.Common.HybridBinarizer::createBinarizer(ZXing.LuminanceSource)
extern void HybridBinarizer_createBinarizer_m23F731ABB20842192517E18CF9DCE3EA4B2DA3C0 (void);
// 0x0000073D System.Void ZXing.Common.HybridBinarizer::binarizeEntireImage()
extern void HybridBinarizer_binarizeEntireImage_m170DE7C5E90720B9E05AEA0A1B2DAC325FCAACB8 (void);
// 0x0000073E System.Void ZXing.Common.HybridBinarizer::calculateThresholdForBlock(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[][],ZXing.Common.BitMatrix)
extern void HybridBinarizer_calculateThresholdForBlock_m8B93886707F28E8F22EE72D3CCC68C6051D1028E (void);
// 0x0000073F System.Int32 ZXing.Common.HybridBinarizer::cap(System.Int32,System.Int32)
extern void HybridBinarizer_cap_m1D50B0872EE4BC9751320D73A381BFBC5CE32501 (void);
// 0x00000740 System.Void ZXing.Common.HybridBinarizer::thresholdBlock(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,ZXing.Common.BitMatrix)
extern void HybridBinarizer_thresholdBlock_m29FD2B4F92D29EC403254644AF713C43DB2ABD32 (void);
// 0x00000741 System.Int32[][] ZXing.Common.HybridBinarizer::calculateBlackPoints(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32)
extern void HybridBinarizer_calculateBlackPoints_m910615BC91FB4F70E33395648B060F30ED473768 (void);
// 0x00000742 System.Void ZXing.Common.PerspectiveTransform::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PerspectiveTransform__ctor_mB2C4EC15BAC6290D31086F85AED848B6D2B47857 (void);
// 0x00000743 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::quadrilateralToQuadrilateral(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PerspectiveTransform_quadrilateralToQuadrilateral_m62B7E82DDA2E54EE34F13DF6E45BABBB0649562A (void);
// 0x00000744 System.Void ZXing.Common.PerspectiveTransform::transformPoints(System.Single[])
extern void PerspectiveTransform_transformPoints_m752C1C67455A8D475D5BCF4F70849511A0CAF6EA (void);
// 0x00000745 System.Void ZXing.Common.PerspectiveTransform::transformPoints(System.Single[],System.Single[])
extern void PerspectiveTransform_transformPoints_mE8A1EE80012A50C8BDDB4B955C6535FAEE8F0ED6 (void);
// 0x00000746 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::squareToQuadrilateral(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PerspectiveTransform_squareToQuadrilateral_m929F8E5CAA1C9240B9007F7977C9E86A414416C2 (void);
// 0x00000747 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::quadrilateralToSquare(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PerspectiveTransform_quadrilateralToSquare_mCA85694C6234C0E46B05897F9FA47DFBB583BBB5 (void);
// 0x00000748 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::buildAdjoint()
extern void PerspectiveTransform_buildAdjoint_mA27B5E94D9A16CBF5537F196D7378B8E33B603CB (void);
// 0x00000749 ZXing.Common.PerspectiveTransform ZXing.Common.PerspectiveTransform::times(ZXing.Common.PerspectiveTransform)
extern void PerspectiveTransform_times_mCE43CB3821B712FC5C9DDA3E30A97884DE41960B (void);
// 0x0000074A System.String ZXing.Common.StringUtils::guessEncoding(System.Byte[],System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void StringUtils_guessEncoding_m766316A11ED814C0D8BB5E9E45FEB5A368DC1414 (void);
// 0x0000074B System.Void ZXing.Common.StringUtils::.cctor()
extern void StringUtils__cctor_m76A7B9E41678470F57C4F650B1F816F1D6BE76A6 (void);
// 0x0000074C System.Void ZXing.Common.ReedSolomon.GenericGF::.ctor(System.Int32,System.Int32,System.Int32)
extern void GenericGF__ctor_mD3E4CD23A1C9C302109D1EDD304614EA84FA717D (void);
// 0x0000074D ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::get_Zero()
extern void GenericGF_get_Zero_mCE585FD5D2A1534464BBA94A91EA64D113023FB8 (void);
// 0x0000074E ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::get_One()
extern void GenericGF_get_One_m2A25FC3E94C95C247813FCB79C0E17B673D9625C (void);
// 0x0000074F ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::buildMonomial(System.Int32,System.Int32)
extern void GenericGF_buildMonomial_m396672161FFCDFD398A7FE6E3F65731DFE20C5FF (void);
// 0x00000750 System.Int32 ZXing.Common.ReedSolomon.GenericGF::addOrSubtract(System.Int32,System.Int32)
extern void GenericGF_addOrSubtract_m7C803D901338526E280B1DCC8AE9BBA476846875 (void);
// 0x00000751 System.Int32 ZXing.Common.ReedSolomon.GenericGF::exp(System.Int32)
extern void GenericGF_exp_m93827D3BD1DDD4E717D19760D4DE4BC2CA9BFDE0 (void);
// 0x00000752 System.Int32 ZXing.Common.ReedSolomon.GenericGF::log(System.Int32)
extern void GenericGF_log_mBFB8FD8437ECF680CBCC1C6215A8ADDB3D2CD18E (void);
// 0x00000753 System.Int32 ZXing.Common.ReedSolomon.GenericGF::inverse(System.Int32)
extern void GenericGF_inverse_mBD77A499BB8921D8363FCB1705C4100BCEE48B09 (void);
// 0x00000754 System.Int32 ZXing.Common.ReedSolomon.GenericGF::multiply(System.Int32,System.Int32)
extern void GenericGF_multiply_mC443DC9AAC9D6FB96017A23C776F7A8A8ED2E3E1 (void);
// 0x00000755 System.Int32 ZXing.Common.ReedSolomon.GenericGF::get_Size()
extern void GenericGF_get_Size_m61A3B191C2565515444B14062956301987B85B7A (void);
// 0x00000756 System.Int32 ZXing.Common.ReedSolomon.GenericGF::get_GeneratorBase()
extern void GenericGF_get_GeneratorBase_m9734B2A7A3A31D4FD63C675DCDC14A7C5FBC6825 (void);
// 0x00000757 System.String ZXing.Common.ReedSolomon.GenericGF::ToString()
extern void GenericGF_ToString_m85D8BB5B904E27307B2BF09A4BAE227CBCE270B5 (void);
// 0x00000758 System.Void ZXing.Common.ReedSolomon.GenericGF::.cctor()
extern void GenericGF__cctor_mC7B5115DA0A0DA6C182741594935ECFB4431EAE7 (void);
// 0x00000759 System.Void ZXing.Common.ReedSolomon.GenericGFPoly::.ctor(ZXing.Common.ReedSolomon.GenericGF,System.Int32[])
extern void GenericGFPoly__ctor_m79E682C103DBF2478D05F006BB590AFDDF16C16E (void);
// 0x0000075A System.Int32[] ZXing.Common.ReedSolomon.GenericGFPoly::get_Coefficients()
extern void GenericGFPoly_get_Coefficients_m082F23EA5ED52CA9D387DA6800BC2550AE7E5E6F (void);
// 0x0000075B System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::get_Degree()
extern void GenericGFPoly_get_Degree_mECFE8E6344BF3DD39952AC4C8B3B7C67F60D9D0F (void);
// 0x0000075C System.Boolean ZXing.Common.ReedSolomon.GenericGFPoly::get_isZero()
extern void GenericGFPoly_get_isZero_mAAE9292A0204D7BE3CD0D21C26929AFFF90954C6 (void);
// 0x0000075D System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::getCoefficient(System.Int32)
extern void GenericGFPoly_getCoefficient_m9DC2657BB6EF4223D70A16EF16BA9B1282AFA920 (void);
// 0x0000075E System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::evaluateAt(System.Int32)
extern void GenericGFPoly_evaluateAt_m37AC93BABB2DD80894E0BFA51B37229BED3A600B (void);
// 0x0000075F ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::addOrSubtract(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void GenericGFPoly_addOrSubtract_m581D696BC0F812BA27B668B97A73525684840033 (void);
// 0x00000760 ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiply(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void GenericGFPoly_multiply_mD942ADD7D4E33149290503F453BF43F6A951372E (void);
// 0x00000761 ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiply(System.Int32)
extern void GenericGFPoly_multiply_mA2809ED733851F0E3BE0DC458E2737B4C3631704 (void);
// 0x00000762 ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiplyByMonomial(System.Int32,System.Int32)
extern void GenericGFPoly_multiplyByMonomial_m7BD9C994C5B80F25FBCDFAA472490307E2100958 (void);
// 0x00000763 ZXing.Common.ReedSolomon.GenericGFPoly[] ZXing.Common.ReedSolomon.GenericGFPoly::divide(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void GenericGFPoly_divide_m7C6B52093059408F36DF17CF80E0F1B8C2E36B68 (void);
// 0x00000764 System.String ZXing.Common.ReedSolomon.GenericGFPoly::ToString()
extern void GenericGFPoly_ToString_m8EFF0478D4E4A2C939B6FE6EFC5647D406D844E8 (void);
// 0x00000765 System.Void ZXing.Common.ReedSolomon.ReedSolomonDecoder::.ctor(ZXing.Common.ReedSolomon.GenericGF)
extern void ReedSolomonDecoder__ctor_m7BAF11D94B5C993B1DC0600D8A78C68348870433 (void);
// 0x00000766 System.Boolean ZXing.Common.ReedSolomon.ReedSolomonDecoder::decode(System.Int32[],System.Int32)
extern void ReedSolomonDecoder_decode_mBAD15FA812707B569B0DEDB766951D40A382A65A (void);
// 0x00000767 ZXing.Common.ReedSolomon.GenericGFPoly[] ZXing.Common.ReedSolomon.ReedSolomonDecoder::runEuclideanAlgorithm(ZXing.Common.ReedSolomon.GenericGFPoly,ZXing.Common.ReedSolomon.GenericGFPoly,System.Int32)
extern void ReedSolomonDecoder_runEuclideanAlgorithm_m0135B24670400776A0582EBC7E3A818C17610956 (void);
// 0x00000768 System.Int32[] ZXing.Common.ReedSolomon.ReedSolomonDecoder::findErrorLocations(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void ReedSolomonDecoder_findErrorLocations_m2291E846229FCAC76CB44BA2E9F5FF374D80B87F (void);
// 0x00000769 System.Int32[] ZXing.Common.ReedSolomon.ReedSolomonDecoder::findErrorMagnitudes(ZXing.Common.ReedSolomon.GenericGFPoly,System.Int32[])
extern void ReedSolomonDecoder_findErrorMagnitudes_m71B235E6755FA10E373310F4468A6649D5ACFDF3 (void);
// 0x0000076A System.Void ZXing.Common.ReedSolomon.ReedSolomonEncoder::.ctor(ZXing.Common.ReedSolomon.GenericGF)
extern void ReedSolomonEncoder__ctor_m2F3F022E6C669F6F2402E609EB3C5DE18DBD7582 (void);
// 0x0000076B ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.ReedSolomonEncoder::buildGenerator(System.Int32)
extern void ReedSolomonEncoder_buildGenerator_m8889A063DE3192F8CDB7CFED427B54CDA692F2D3 (void);
// 0x0000076C System.Void ZXing.Common.ReedSolomon.ReedSolomonEncoder::encode(System.Int32[],System.Int32)
extern void ReedSolomonEncoder_encode_m765C7D0363DEB8BEB6AA672FE0600F8A50EE8852 (void);
// 0x0000076D System.Int32 ZXing.Common.Detector.MathUtils::round(System.Single)
extern void MathUtils_round_mF9539156B6FA9A42206E49C9938D6C4976573F1A (void);
// 0x0000076E System.Single ZXing.Common.Detector.MathUtils::distance(System.Single,System.Single,System.Single,System.Single)
extern void MathUtils_distance_mD86E30697246AA12854809879CD0AC035E2AD02D (void);
// 0x0000076F System.Single ZXing.Common.Detector.MathUtils::distance(System.Int32,System.Int32,System.Int32,System.Int32)
extern void MathUtils_distance_mE7B9D4D03EFEC226CB48E670729DDCE0926ED5AA (void);
// 0x00000770 System.Int32 ZXing.Common.Detector.MathUtils::sum(System.Int32[])
extern void MathUtils_sum_mBD16039D17C48335BCB27D1D1CF5354D8879364E (void);
// 0x00000771 System.Void ZXing.Common.Detector.MonochromeRectangleDetector::.ctor(ZXing.Common.BitMatrix)
extern void MonochromeRectangleDetector__ctor_mA078B24C27A890B01542B2710D48E5FEBB4E4566 (void);
// 0x00000772 ZXing.ResultPoint[] ZXing.Common.Detector.MonochromeRectangleDetector::detect()
extern void MonochromeRectangleDetector_detect_mE9E655BB2FB91FD24665CC86E359EA90935C42A7 (void);
// 0x00000773 ZXing.ResultPoint ZXing.Common.Detector.MonochromeRectangleDetector::findCornerFromCenter(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void MonochromeRectangleDetector_findCornerFromCenter_m533C8D1CD41DCDAC3721A0F78E14D5FE891C4785 (void);
// 0x00000774 System.Int32[] ZXing.Common.Detector.MonochromeRectangleDetector::blackWhiteRange(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void MonochromeRectangleDetector_blackWhiteRange_m60BF2FA07D50112D7D92FC926A5583BF8B17C5F5 (void);
// 0x00000775 ZXing.Common.Detector.WhiteRectangleDetector ZXing.Common.Detector.WhiteRectangleDetector::Create(ZXing.Common.BitMatrix)
extern void WhiteRectangleDetector_Create_mA3E35CB328C5BAE11746D1CCD33FE063EEF237F0 (void);
// 0x00000776 ZXing.Common.Detector.WhiteRectangleDetector ZXing.Common.Detector.WhiteRectangleDetector::Create(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32)
extern void WhiteRectangleDetector_Create_m23DE7013F54BBA4C38F2F028B45C7CA8ABBC63BD (void);
// 0x00000777 System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix)
extern void WhiteRectangleDetector__ctor_m3ABBF42CAE3A59A562E035976ADEBB0975BCC2DA (void);
// 0x00000778 System.Void ZXing.Common.Detector.WhiteRectangleDetector::.ctor(ZXing.Common.BitMatrix,System.Int32,System.Int32,System.Int32)
extern void WhiteRectangleDetector__ctor_m6BEE0E4B3444DFD7A97FE6AC5771D82447221AA4 (void);
// 0x00000779 ZXing.ResultPoint[] ZXing.Common.Detector.WhiteRectangleDetector::detect()
extern void WhiteRectangleDetector_detect_m409A65D2F4DE020008ED8CBDD9E719CC33A6A7A6 (void);
// 0x0000077A ZXing.ResultPoint ZXing.Common.Detector.WhiteRectangleDetector::getBlackPointOnSegment(System.Single,System.Single,System.Single,System.Single)
extern void WhiteRectangleDetector_getBlackPointOnSegment_m6D5E116824A0CA70809415079343B86BD6D7F20A (void);
// 0x0000077B ZXing.ResultPoint[] ZXing.Common.Detector.WhiteRectangleDetector::centerEdges(ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void WhiteRectangleDetector_centerEdges_mFE5EF712912B6842997801BB678930E5B66E719E (void);
// 0x0000077C System.Boolean ZXing.Common.Detector.WhiteRectangleDetector::containsBlackPoint(System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WhiteRectangleDetector_containsBlackPoint_m3C29ED210660F606344DD768EE10D71B15C80F08 (void);
// 0x0000077D System.String[] ZXing.Client.Result.AbstractDoCoMoResultParser::matchDoCoMoPrefixedField(System.String,System.String,System.Boolean)
extern void AbstractDoCoMoResultParser_matchDoCoMoPrefixedField_mE62D6848687F1C4E5C9384818DB37158125C60F4 (void);
// 0x0000077E System.String ZXing.Client.Result.AbstractDoCoMoResultParser::matchSingleDoCoMoPrefixedField(System.String,System.String,System.Boolean)
extern void AbstractDoCoMoResultParser_matchSingleDoCoMoPrefixedField_mFA607848CDF0512BF4EDC52DFC69B4F88DA0C9EF (void);
// 0x0000077F System.Void ZXing.Client.Result.AbstractDoCoMoResultParser::.ctor()
extern void AbstractDoCoMoResultParser__ctor_m65B75A088D033B07539E18A3524A387B73F0FCBC (void);
// 0x00000780 ZXing.Client.Result.ParsedResult ZXing.Client.Result.AddressBookAUResultParser::parse(ZXing.Result)
extern void AddressBookAUResultParser_parse_m1A65A7E970A7E880899FA7FB11535F71BF3DCE8B (void);
// 0x00000781 System.String[] ZXing.Client.Result.AddressBookAUResultParser::matchMultipleValuePrefix(System.String,System.Int32,System.String,System.Boolean)
extern void AddressBookAUResultParser_matchMultipleValuePrefix_m36857180A3FD295C634A65D28094E463F83E1B87 (void);
// 0x00000782 System.Void ZXing.Client.Result.AddressBookAUResultParser::.ctor()
extern void AddressBookAUResultParser__ctor_m8668040C5DB3FA82F6E1B65484F8C19C1224F8EF (void);
// 0x00000783 ZXing.Client.Result.ParsedResult ZXing.Client.Result.AddressBookDoCoMoResultParser::parse(ZXing.Result)
extern void AddressBookDoCoMoResultParser_parse_m362FE67C1A4BEC04BAD6D88F9C14DEBF79811A14 (void);
// 0x00000784 System.String ZXing.Client.Result.AddressBookDoCoMoResultParser::parseName(System.String)
extern void AddressBookDoCoMoResultParser_parseName_m3C3546E640D5493FFE461CAE7F734C596E32D157 (void);
// 0x00000785 System.Void ZXing.Client.Result.AddressBookDoCoMoResultParser::.ctor()
extern void AddressBookDoCoMoResultParser__ctor_mC43F74C0AAD1DDE652DD1CDE04CA3C1A8B9D7BD5 (void);
// 0x00000786 System.Void ZXing.Client.Result.AddressBookParsedResult::.ctor(System.String[],System.String[],System.String[],System.String[],System.String[],System.String[],System.String[])
extern void AddressBookParsedResult__ctor_m645D999644ABF19CC3985FB2CED1885FAEF9E97B (void);
// 0x00000787 System.Void ZXing.Client.Result.AddressBookParsedResult::.ctor(System.String[],System.String[],System.String,System.String[],System.String[],System.String[],System.String[],System.String,System.String,System.String[],System.String[],System.String,System.String,System.String,System.String[],System.String[])
extern void AddressBookParsedResult__ctor_m513912DF4A0C3B23C8E76FF63C1997F90FEE72B6 (void);
// 0x00000788 System.String[] ZXing.Client.Result.AddressBookParsedResult::get_Names()
extern void AddressBookParsedResult_get_Names_m8790DF343459D55B964A4D5F352574904C36A960 (void);
// 0x00000789 System.String[] ZXing.Client.Result.AddressBookParsedResult::get_Nicknames()
extern void AddressBookParsedResult_get_Nicknames_mE9713EC68BC7BF150F7ED4173BE122D9A0001597 (void);
// 0x0000078A System.String ZXing.Client.Result.AddressBookParsedResult::get_Pronunciation()
extern void AddressBookParsedResult_get_Pronunciation_m707FBD15223C302B0786C5F23E5111A6CECF991F (void);
// 0x0000078B System.String[] ZXing.Client.Result.AddressBookParsedResult::get_PhoneNumbers()
extern void AddressBookParsedResult_get_PhoneNumbers_m6516BA79BD5BAC88300A2C7C778E2C609F396D57 (void);
// 0x0000078C System.String[] ZXing.Client.Result.AddressBookParsedResult::get_PhoneTypes()
extern void AddressBookParsedResult_get_PhoneTypes_mD1B234C956B49822347D482D98C8DDE87593A980 (void);
// 0x0000078D System.String[] ZXing.Client.Result.AddressBookParsedResult::get_Emails()
extern void AddressBookParsedResult_get_Emails_mBB68E0D6624F54B490B31102AA7151AC78223DC4 (void);
// 0x0000078E System.String[] ZXing.Client.Result.AddressBookParsedResult::get_EmailTypes()
extern void AddressBookParsedResult_get_EmailTypes_m490E814E80971DA6C71F6184E23FED2D54CE94A6 (void);
// 0x0000078F System.String ZXing.Client.Result.AddressBookParsedResult::get_InstantMessenger()
extern void AddressBookParsedResult_get_InstantMessenger_mACE2E2C09625A676579FA5519B6002CEC7F10C55 (void);
// 0x00000790 System.String ZXing.Client.Result.AddressBookParsedResult::get_Note()
extern void AddressBookParsedResult_get_Note_m1991E8E463F239C94C7B4B94454392A4C4E00007 (void);
// 0x00000791 System.String[] ZXing.Client.Result.AddressBookParsedResult::get_Addresses()
extern void AddressBookParsedResult_get_Addresses_m8B90BBA5FB46B563780D63B42FC3BB4A77BBC84F (void);
// 0x00000792 System.String[] ZXing.Client.Result.AddressBookParsedResult::get_AddressTypes()
extern void AddressBookParsedResult_get_AddressTypes_mEE93C1DF2679774841F710819C3D0D62006A5562 (void);
// 0x00000793 System.String ZXing.Client.Result.AddressBookParsedResult::get_Title()
extern void AddressBookParsedResult_get_Title_m3814EDE0DF44A851A38B2D3DF50588B8A007C66F (void);
// 0x00000794 System.String ZXing.Client.Result.AddressBookParsedResult::get_Org()
extern void AddressBookParsedResult_get_Org_m4B508C78DF4196BF5B8DFB105F175860B5B0E79D (void);
// 0x00000795 System.String[] ZXing.Client.Result.AddressBookParsedResult::get_URLs()
extern void AddressBookParsedResult_get_URLs_m171AE0EBC7704B1E923E4014C349F044BD55BC80 (void);
// 0x00000796 System.String ZXing.Client.Result.AddressBookParsedResult::get_Birthday()
extern void AddressBookParsedResult_get_Birthday_m484C471D3FF2EBD27283A2315EAD460B1DDD7542 (void);
// 0x00000797 System.String[] ZXing.Client.Result.AddressBookParsedResult::get_Geo()
extern void AddressBookParsedResult_get_Geo_m303DA2BEA7991CFC7D6F3984C91D98A0687C816C (void);
// 0x00000798 System.String ZXing.Client.Result.AddressBookParsedResult::getDisplayResult()
extern void AddressBookParsedResult_getDisplayResult_m67FC4288AF644CDC3EEA5A5840FAF9F5EBF14738 (void);
// 0x00000799 ZXing.Client.Result.ParsedResult ZXing.Client.Result.BizcardResultParser::parse(ZXing.Result)
extern void BizcardResultParser_parse_m989C6689B619721D41107A36526D7049F144B251 (void);
// 0x0000079A System.String[] ZXing.Client.Result.BizcardResultParser::buildPhoneNumbers(System.String,System.String,System.String)
extern void BizcardResultParser_buildPhoneNumbers_mAD841386FF776BEA9C72DAEC26541A66989E8C46 (void);
// 0x0000079B System.String ZXing.Client.Result.BizcardResultParser::buildName(System.String,System.String)
extern void BizcardResultParser_buildName_m7F274526D16DEBA8D8ACE3FEFF742407723AAD6F (void);
// 0x0000079C System.Void ZXing.Client.Result.BizcardResultParser::.ctor()
extern void BizcardResultParser__ctor_m0F294DB3EC674372B75984A7F425C3563E29B777 (void);
// 0x0000079D ZXing.Client.Result.ParsedResult ZXing.Client.Result.BookmarkDoCoMoResultParser::parse(ZXing.Result)
extern void BookmarkDoCoMoResultParser_parse_m60138F9A198FD796DC75E2751AD4AECEB4213705 (void);
// 0x0000079E System.Void ZXing.Client.Result.BookmarkDoCoMoResultParser::.ctor()
extern void BookmarkDoCoMoResultParser__ctor_m68FD7D21E400ED3C7595E46E424202834C4D4920 (void);
// 0x0000079F System.Void ZXing.Client.Result.CalendarParsedResult::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,System.String[],System.String,System.Double,System.Double)
extern void CalendarParsedResult__ctor_mDB126E0CAA8EB2AC3205806FB7D6B3D47BEB81AD (void);
// 0x000007A0 System.String ZXing.Client.Result.CalendarParsedResult::get_Summary()
extern void CalendarParsedResult_get_Summary_m094CE1CD413587ABF473A268B7A60C7D5AED96FD (void);
// 0x000007A1 System.DateTime ZXing.Client.Result.CalendarParsedResult::get_Start()
extern void CalendarParsedResult_get_Start_m97964C2BF52E1E762F60E72F04653DDF5E1F5F87 (void);
// 0x000007A2 System.Boolean ZXing.Client.Result.CalendarParsedResult::isStartAllDay()
extern void CalendarParsedResult_isStartAllDay_m346AB816C83BC410107895486206E9498D95EA2E (void);
// 0x000007A3 System.Nullable`1<System.DateTime> ZXing.Client.Result.CalendarParsedResult::get_End()
extern void CalendarParsedResult_get_End_mE134C97E4E53C933FB133C77A3B8503197654853 (void);
// 0x000007A4 System.Boolean ZXing.Client.Result.CalendarParsedResult::get_isEndAllDay()
extern void CalendarParsedResult_get_isEndAllDay_mB291BF7E232FAC3D1C1DC07213BF712949E83AFF (void);
// 0x000007A5 System.String ZXing.Client.Result.CalendarParsedResult::get_Location()
extern void CalendarParsedResult_get_Location_m805275F221FBDE63E0040865AD6D07602EABF22A (void);
// 0x000007A6 System.String ZXing.Client.Result.CalendarParsedResult::get_Organizer()
extern void CalendarParsedResult_get_Organizer_m4F558E024ACBD131B73DE2BF9F869AC9FC62F57C (void);
// 0x000007A7 System.String[] ZXing.Client.Result.CalendarParsedResult::get_Attendees()
extern void CalendarParsedResult_get_Attendees_m4E3FB2E7175ED01F80CEDF166CF9AA56BF0BB448 (void);
// 0x000007A8 System.String ZXing.Client.Result.CalendarParsedResult::get_Description()
extern void CalendarParsedResult_get_Description_m31E421B948D2C444830307BD1D2CDBEA3D6A7067 (void);
// 0x000007A9 System.Double ZXing.Client.Result.CalendarParsedResult::get_Latitude()
extern void CalendarParsedResult_get_Latitude_m26C477E08D1EE6F13D595627908A4FF3906F25FA (void);
// 0x000007AA System.Double ZXing.Client.Result.CalendarParsedResult::get_Longitude()
extern void CalendarParsedResult_get_Longitude_mF4B017429640FC08D5DD1F92DA55D1261A17947D (void);
// 0x000007AB System.DateTime ZXing.Client.Result.CalendarParsedResult::parseDate(System.String)
extern void CalendarParsedResult_parseDate_m238A0F3CF0DF8B773D26EB3D3ECBD493BE696B7C (void);
// 0x000007AC System.String ZXing.Client.Result.CalendarParsedResult::format(System.Boolean,System.Nullable`1<System.DateTime>)
extern void CalendarParsedResult_format_m675A42784459EEFDAC52EC4AB3F8C191D4188E1B (void);
// 0x000007AD System.Int64 ZXing.Client.Result.CalendarParsedResult::parseDurationMS(System.String)
extern void CalendarParsedResult_parseDurationMS_m269BAD70F68BC48FB6F9B61AC3383175E2D1D67A (void);
// 0x000007AE System.DateTime ZXing.Client.Result.CalendarParsedResult::parseDateTimeString(System.String)
extern void CalendarParsedResult_parseDateTimeString_mCE8C2F1AC4523D64D4D4B4BBEFFEB75AE58FD7EE (void);
// 0x000007AF System.Void ZXing.Client.Result.CalendarParsedResult::.cctor()
extern void CalendarParsedResult__cctor_m860EB43A75847DE3C27266EB558CFFCDB824DC6E (void);
// 0x000007B0 System.String ZXing.Client.Result.EmailAddressParsedResult::get_EmailAddress()
extern void EmailAddressParsedResult_get_EmailAddress_m1F62B1425E4C0E3DAF8FCC688CBB28DEC63B2859 (void);
// 0x000007B1 System.String[] ZXing.Client.Result.EmailAddressParsedResult::get_Tos()
extern void EmailAddressParsedResult_get_Tos_m5262F8BEC47F5785247335E1243C18BFBFE8A91D (void);
// 0x000007B2 System.Void ZXing.Client.Result.EmailAddressParsedResult::set_Tos(System.String[])
extern void EmailAddressParsedResult_set_Tos_mA224C50D93B8F2205AEF14391F665C6F9A1E3688 (void);
// 0x000007B3 System.String[] ZXing.Client.Result.EmailAddressParsedResult::get_CCs()
extern void EmailAddressParsedResult_get_CCs_mE772E3F26FB8E7642A650F2197EA55CB44D49291 (void);
// 0x000007B4 System.Void ZXing.Client.Result.EmailAddressParsedResult::set_CCs(System.String[])
extern void EmailAddressParsedResult_set_CCs_m4ED7B1380F359125C6F22A064EF8A78EEC014475 (void);
// 0x000007B5 System.String[] ZXing.Client.Result.EmailAddressParsedResult::get_BCCs()
extern void EmailAddressParsedResult_get_BCCs_m53C5499EBF18AE80C82F624C76E1F74808DB4698 (void);
// 0x000007B6 System.Void ZXing.Client.Result.EmailAddressParsedResult::set_BCCs(System.String[])
extern void EmailAddressParsedResult_set_BCCs_m7594188B0C956BCB006B5B9EB3932EF87C641C6D (void);
// 0x000007B7 System.String ZXing.Client.Result.EmailAddressParsedResult::get_Subject()
extern void EmailAddressParsedResult_get_Subject_m8450A71D85418A097DB6BB9668F6F1B5EDC11F7D (void);
// 0x000007B8 System.Void ZXing.Client.Result.EmailAddressParsedResult::set_Subject(System.String)
extern void EmailAddressParsedResult_set_Subject_m503A52107868F7178237B528748071CB32994370 (void);
// 0x000007B9 System.String ZXing.Client.Result.EmailAddressParsedResult::get_Body()
extern void EmailAddressParsedResult_get_Body_mA5BF39F58B67A78998A7DB4E0851879F85587A8E (void);
// 0x000007BA System.Void ZXing.Client.Result.EmailAddressParsedResult::set_Body(System.String)
extern void EmailAddressParsedResult_set_Body_m283F3B03716B7B27042259BFCCACD196E151A732 (void);
// 0x000007BB System.String ZXing.Client.Result.EmailAddressParsedResult::get_MailtoURI()
extern void EmailAddressParsedResult_get_MailtoURI_mD2F6163EF82DB1DE3601EE39B227C2977E7CB712 (void);
// 0x000007BC System.Void ZXing.Client.Result.EmailAddressParsedResult::.ctor(System.String)
extern void EmailAddressParsedResult__ctor_mCDBEE300A70E18F3AEDB2CCACB264F027F2DBECE (void);
// 0x000007BD System.Void ZXing.Client.Result.EmailAddressParsedResult::.ctor(System.String[],System.String[],System.String[],System.String,System.String)
extern void EmailAddressParsedResult__ctor_m1DAD8D1F2E18FC154EC2677D2130830203586350 (void);
// 0x000007BE ZXing.Client.Result.ParsedResult ZXing.Client.Result.EmailAddressResultParser::parse(ZXing.Result)
extern void EmailAddressResultParser_parse_m26B97BD6C28167CB5DD9C81ECCEF153B319A7142 (void);
// 0x000007BF System.Void ZXing.Client.Result.EmailAddressResultParser::.ctor()
extern void EmailAddressResultParser__ctor_m584C60D98B414C2D5F3674B310790043ADA339B2 (void);
// 0x000007C0 System.Void ZXing.Client.Result.EmailAddressResultParser::.cctor()
extern void EmailAddressResultParser__cctor_mE3471498258B8335F1B3536838B0DCA922B87A1C (void);
// 0x000007C1 ZXing.Client.Result.ParsedResult ZXing.Client.Result.EmailDoCoMoResultParser::parse(ZXing.Result)
extern void EmailDoCoMoResultParser_parse_m0001ABCFD5E02B6484AFA6D282165A2F6F9540B0 (void);
// 0x000007C2 System.Boolean ZXing.Client.Result.EmailDoCoMoResultParser::isBasicallyValidEmailAddress(System.String)
extern void EmailDoCoMoResultParser_isBasicallyValidEmailAddress_mE173C29F47820D06C353CA3576B50542B48A7DCE (void);
// 0x000007C3 System.Void ZXing.Client.Result.EmailDoCoMoResultParser::.ctor()
extern void EmailDoCoMoResultParser__ctor_m774C59D652E7B8D7B6DBFEE2BDECF163E76B464A (void);
// 0x000007C4 System.Void ZXing.Client.Result.EmailDoCoMoResultParser::.cctor()
extern void EmailDoCoMoResultParser__cctor_mFBBAD6C25DDDE79DAB25E6B991A9A643D574E2B8 (void);
// 0x000007C5 System.Void ZXing.Client.Result.ExpandedProductParsedResult::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void ExpandedProductParsedResult__ctor_m6442D6F11B8736457A3D97EACE30CC3197B08F94 (void);
// 0x000007C6 System.Boolean ZXing.Client.Result.ExpandedProductParsedResult::Equals(System.Object)
extern void ExpandedProductParsedResult_Equals_m5A1C1052ED38F7E03A72B4A66C37F8D107CBE821 (void);
// 0x000007C7 System.Boolean ZXing.Client.Result.ExpandedProductParsedResult::equalsOrNull(System.Object,System.Object)
extern void ExpandedProductParsedResult_equalsOrNull_mEF0434FE785B6701A6FF7016AC6F1EDBBF7E9C2E (void);
// 0x000007C8 System.Boolean ZXing.Client.Result.ExpandedProductParsedResult::equalsOrNull(System.Collections.Generic.IDictionary`2<System.String,System.String>,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void ExpandedProductParsedResult_equalsOrNull_m2732855F203674DE83C86AB444407E291D21549D (void);
// 0x000007C9 System.Int32 ZXing.Client.Result.ExpandedProductParsedResult::GetHashCode()
extern void ExpandedProductParsedResult_GetHashCode_m17A6420AFD666E1FAE73301F6D73D0B630E7B5FF (void);
// 0x000007CA System.Int32 ZXing.Client.Result.ExpandedProductParsedResult::hashNotNull(System.Object)
extern void ExpandedProductParsedResult_hashNotNull_mAA8B9F5D1F3178D599096AA54C8614A0B9488494 (void);
// 0x000007CB System.String ZXing.Client.Result.ExpandedProductParsedResult::get_RawText()
extern void ExpandedProductParsedResult_get_RawText_mB3D9D73EC7E273C5DE1293530CD70EEB59EF7DE6 (void);
// 0x000007CC System.String ZXing.Client.Result.ExpandedProductParsedResult::get_ProductID()
extern void ExpandedProductParsedResult_get_ProductID_m6AB2DF86E14625133A939100C3CF7A0A840AA482 (void);
// 0x000007CD System.String ZXing.Client.Result.ExpandedProductParsedResult::get_Sscc()
extern void ExpandedProductParsedResult_get_Sscc_m2CB96A1D49ECB4D4997B910B61E1979C4B19B1A5 (void);
// 0x000007CE System.String ZXing.Client.Result.ExpandedProductParsedResult::get_LotNumber()
extern void ExpandedProductParsedResult_get_LotNumber_m1D44B20BC585E0169F98F6F29F221EF554660A6B (void);
// 0x000007CF System.String ZXing.Client.Result.ExpandedProductParsedResult::get_ProductionDate()
extern void ExpandedProductParsedResult_get_ProductionDate_m7E36BC42C52BCCAB1F89C2AE8466600C8347424C (void);
// 0x000007D0 System.String ZXing.Client.Result.ExpandedProductParsedResult::get_PackagingDate()
extern void ExpandedProductParsedResult_get_PackagingDate_mB86E9C82DA5D4BD1E415CA90470B648B1BBB9501 (void);
// 0x000007D1 System.String ZXing.Client.Result.ExpandedProductParsedResult::get_BestBeforeDate()
extern void ExpandedProductParsedResult_get_BestBeforeDate_m6C3915046DE915D759608E1B5DB70207D7CCC589 (void);
// 0x000007D2 System.String ZXing.Client.Result.ExpandedProductParsedResult::get_ExpirationDate()
extern void ExpandedProductParsedResult_get_ExpirationDate_mF472B70C9CF3D9B985B1EB97F642E8906EBA16E5 (void);
// 0x000007D3 System.String ZXing.Client.Result.ExpandedProductParsedResult::get_Weight()
extern void ExpandedProductParsedResult_get_Weight_m76CC46E2E5568B9DD7234904B09AA3376D161A9E (void);
// 0x000007D4 System.String ZXing.Client.Result.ExpandedProductParsedResult::get_WeightType()
extern void ExpandedProductParsedResult_get_WeightType_m7BA2834649B1076B1EA252CFCDA5A788296D62C3 (void);
// 0x000007D5 System.String ZXing.Client.Result.ExpandedProductParsedResult::get_WeightIncrement()
extern void ExpandedProductParsedResult_get_WeightIncrement_m6F5D641A433D5717B7DB15B0C964FAFFFB0495B7 (void);
// 0x000007D6 System.String ZXing.Client.Result.ExpandedProductParsedResult::get_Price()
extern void ExpandedProductParsedResult_get_Price_m85BF9E6722601B81809C6A2831383023931F2EA6 (void);
// 0x000007D7 System.String ZXing.Client.Result.ExpandedProductParsedResult::get_PriceIncrement()
extern void ExpandedProductParsedResult_get_PriceIncrement_m034F7CFB8CCDA3BE7F318D90DB4536D88DAE8410 (void);
// 0x000007D8 System.String ZXing.Client.Result.ExpandedProductParsedResult::get_PriceCurrency()
extern void ExpandedProductParsedResult_get_PriceCurrency_m7651B36F3A0480B1D11997A769CC689066871CC5 (void);
// 0x000007D9 System.Collections.Generic.IDictionary`2<System.String,System.String> ZXing.Client.Result.ExpandedProductParsedResult::get_UncommonAIs()
extern void ExpandedProductParsedResult_get_UncommonAIs_m00690C11B1ADD8D0D99E5F6AAE00C778F5030498 (void);
// 0x000007DA System.String ZXing.Client.Result.ExpandedProductParsedResult::get_DisplayResult()
extern void ExpandedProductParsedResult_get_DisplayResult_mD7AB1B9980FD2BA3B42E9736F6AA7C2F7BA0112C (void);
// 0x000007DB System.Void ZXing.Client.Result.ExpandedProductParsedResult::.cctor()
extern void ExpandedProductParsedResult__cctor_m768642FB41A6C40A92AB399BCC1C4B2A1E188958 (void);
// 0x000007DC ZXing.Client.Result.ParsedResult ZXing.Client.Result.ExpandedProductResultParser::parse(ZXing.Result)
extern void ExpandedProductResultParser_parse_m5F8034D20A60D37B4EAEB35E1BE2378A03D64523 (void);
// 0x000007DD System.String ZXing.Client.Result.ExpandedProductResultParser::findAIvalue(System.Int32,System.String)
extern void ExpandedProductResultParser_findAIvalue_m1DABFB52C43FD6DBE1045EF38E7A2762648FEEEF (void);
// 0x000007DE System.String ZXing.Client.Result.ExpandedProductResultParser::findValue(System.Int32,System.String)
extern void ExpandedProductResultParser_findValue_m7367AB1E7A914DBFDB8531F36ABF026FA81F8736 (void);
// 0x000007DF System.Void ZXing.Client.Result.ExpandedProductResultParser::.ctor()
extern void ExpandedProductResultParser__ctor_m28F2FF656EDEC03598FBE15123EAADCD5D2B0CBE (void);
// 0x000007E0 System.Void ZXing.Client.Result.GeoParsedResult::.ctor(System.Double,System.Double,System.Double,System.String)
extern void GeoParsedResult__ctor_m9D105C79347C1D6D804B375ED4950B8976FBF69A (void);
// 0x000007E1 System.Double ZXing.Client.Result.GeoParsedResult::get_Latitude()
extern void GeoParsedResult_get_Latitude_m7B9B2915F01DE9B4C2A912BC96283B936E58DC9C (void);
// 0x000007E2 System.Void ZXing.Client.Result.GeoParsedResult::set_Latitude(System.Double)
extern void GeoParsedResult_set_Latitude_m675F8CBA3B3A78543D5E1C576F6CC917D619F8FB (void);
// 0x000007E3 System.Double ZXing.Client.Result.GeoParsedResult::get_Longitude()
extern void GeoParsedResult_get_Longitude_mB8BDBF3C9B1A1A8DEB3DC48646EF88A4AD4F026C (void);
// 0x000007E4 System.Void ZXing.Client.Result.GeoParsedResult::set_Longitude(System.Double)
extern void GeoParsedResult_set_Longitude_mD9ADC4E96B2CC1DAB9DC6AB35D1AF4AA9D0BDD8B (void);
// 0x000007E5 System.Double ZXing.Client.Result.GeoParsedResult::get_Altitude()
extern void GeoParsedResult_get_Altitude_m292CDA9ACA12ECB4B82C214D1B2CD17D424BFC74 (void);
// 0x000007E6 System.Void ZXing.Client.Result.GeoParsedResult::set_Altitude(System.Double)
extern void GeoParsedResult_set_Altitude_m7F707069DC9BB065F935A9A8582D368B31E42B79 (void);
// 0x000007E7 System.String ZXing.Client.Result.GeoParsedResult::get_Query()
extern void GeoParsedResult_get_Query_m32F18B61AFD98AFE3D99E53392A38F2F16B7B490 (void);
// 0x000007E8 System.Void ZXing.Client.Result.GeoParsedResult::set_Query(System.String)
extern void GeoParsedResult_set_Query_m49DFFC434F4BED2E289A8083D52A7547B9F83284 (void);
// 0x000007E9 System.String ZXing.Client.Result.GeoParsedResult::get_GeoURI()
extern void GeoParsedResult_get_GeoURI_mBD5CCFCFFA1F99B2799AA86A4591AA37118CA7A2 (void);
// 0x000007EA System.Void ZXing.Client.Result.GeoParsedResult::set_GeoURI(System.String)
extern void GeoParsedResult_set_GeoURI_m9246CC416D4C8C5D013AAA1C4EEBB0F96415D6E4 (void);
// 0x000007EB System.String ZXing.Client.Result.GeoParsedResult::get_GoogleMapsURI()
extern void GeoParsedResult_get_GoogleMapsURI_m0E05A9C356C0E88CA99BF159F0C871AD0E74612A (void);
// 0x000007EC System.Void ZXing.Client.Result.GeoParsedResult::set_GoogleMapsURI(System.String)
extern void GeoParsedResult_set_GoogleMapsURI_m237FCA8185D0EE6E98958FEBECED96FA6E6F50C6 (void);
// 0x000007ED System.String ZXing.Client.Result.GeoParsedResult::getDisplayResult()
extern void GeoParsedResult_getDisplayResult_mA77D2BDFC868E792948D0E1174049D821641BE63 (void);
// 0x000007EE System.String ZXing.Client.Result.GeoParsedResult::getGeoURI()
extern void GeoParsedResult_getGeoURI_mEC094D9626B90A75B56CF5A32553A1F9CE48B228 (void);
// 0x000007EF System.String ZXing.Client.Result.GeoParsedResult::getGoogleMapsURI()
extern void GeoParsedResult_getGoogleMapsURI_m7CF941D3D75A1AE3D5F047C1D7ED908ABA741DBD (void);
// 0x000007F0 ZXing.Client.Result.ParsedResult ZXing.Client.Result.GeoResultParser::parse(ZXing.Result)
extern void GeoResultParser_parse_m03111D066833C5EBEDFB3D4D915CDCCEE24EB321 (void);
// 0x000007F1 System.Void ZXing.Client.Result.GeoResultParser::.ctor()
extern void GeoResultParser__ctor_m0B74049A8284F987A16843F51414E8D584222D44 (void);
// 0x000007F2 System.Void ZXing.Client.Result.GeoResultParser::.cctor()
extern void GeoResultParser__cctor_mDA8D55C291A66FB685B6CA861A9CFE79CA6CA4D4 (void);
// 0x000007F3 System.Void ZXing.Client.Result.ISBNParsedResult::.ctor(System.String)
extern void ISBNParsedResult__ctor_mE29C7A8F6D11B95E4DB0166538C3132BB6672E34 (void);
// 0x000007F4 System.String ZXing.Client.Result.ISBNParsedResult::get_ISBN()
extern void ISBNParsedResult_get_ISBN_mCE3525CA3D196F7F8A6B645DA6818C496690BF74 (void);
// 0x000007F5 System.Void ZXing.Client.Result.ISBNParsedResult::set_ISBN(System.String)
extern void ISBNParsedResult_set_ISBN_m7D3A60E083E39FD9612184CF030C9ECB5F876D14 (void);
// 0x000007F6 ZXing.Client.Result.ParsedResult ZXing.Client.Result.ISBNResultParser::parse(ZXing.Result)
extern void ISBNResultParser_parse_m7F76E6C9B735AED47E003D256576F5C72E749B17 (void);
// 0x000007F7 System.Void ZXing.Client.Result.ISBNResultParser::.ctor()
extern void ISBNResultParser__ctor_m2097B01719774B1909D3201A8CE59929B3FDB100 (void);
// 0x000007F8 ZXing.Client.Result.ParsedResultType ZXing.Client.Result.ParsedResult::get_Type()
extern void ParsedResult_get_Type_m007B22367016D85A7940C86326F007F904EF26CE (void);
// 0x000007F9 System.Void ZXing.Client.Result.ParsedResult::set_Type(ZXing.Client.Result.ParsedResultType)
extern void ParsedResult_set_Type_mE7E6AAFC820F15369937F749A84F9FCD012D5D50 (void);
// 0x000007FA System.String ZXing.Client.Result.ParsedResult::get_DisplayResult()
extern void ParsedResult_get_DisplayResult_m1C4BEA2A33E11461C9198055F82E622F9878638C (void);
// 0x000007FB System.Void ZXing.Client.Result.ParsedResult::.ctor(ZXing.Client.Result.ParsedResultType)
extern void ParsedResult__ctor_mA217F44067BFA4341B82A6B83FCB2F84BC2F451C (void);
// 0x000007FC System.String ZXing.Client.Result.ParsedResult::ToString()
extern void ParsedResult_ToString_mFAB0A87357B6500E4334359E6C1213B4BADCE7CB (void);
// 0x000007FD System.Boolean ZXing.Client.Result.ParsedResult::Equals(System.Object)
extern void ParsedResult_Equals_m8AA95620AC9884981BEC97ECEC841A4DAB5F5E5F (void);
// 0x000007FE System.Int32 ZXing.Client.Result.ParsedResult::GetHashCode()
extern void ParsedResult_GetHashCode_m2425649AFCC66281DADEDD23646ED1E7E4C6AB1D (void);
// 0x000007FF System.Void ZXing.Client.Result.ParsedResult::maybeAppend(System.String,System.Text.StringBuilder)
extern void ParsedResult_maybeAppend_mDDC7404AD0C029DAB72E78F4715C6BA6A611927F (void);
// 0x00000800 System.Void ZXing.Client.Result.ParsedResult::maybeAppend(System.String[],System.Text.StringBuilder)
extern void ParsedResult_maybeAppend_mC758D17AD4A0974CA4BF9F9BFC1592CE64F9D825 (void);
// 0x00000801 System.Void ZXing.Client.Result.ProductParsedResult::.ctor(System.String)
extern void ProductParsedResult__ctor_m89EF437A50ECF22E1152ABC03C8175BED1731B18 (void);
// 0x00000802 System.Void ZXing.Client.Result.ProductParsedResult::.ctor(System.String,System.String)
extern void ProductParsedResult__ctor_m913FC763960F2718497835D0556354DBB11B561F (void);
// 0x00000803 System.String ZXing.Client.Result.ProductParsedResult::get_ProductID()
extern void ProductParsedResult_get_ProductID_m335C898DDC69E86630D3F16BD76A023AE55C6E7E (void);
// 0x00000804 System.Void ZXing.Client.Result.ProductParsedResult::set_ProductID(System.String)
extern void ProductParsedResult_set_ProductID_mE930C204472161A877073C436094D48D08DA2091 (void);
// 0x00000805 System.String ZXing.Client.Result.ProductParsedResult::get_NormalizedProductID()
extern void ProductParsedResult_get_NormalizedProductID_mF6418B1564B9561EE26EDD14ACA2240AE5BC2A44 (void);
// 0x00000806 System.Void ZXing.Client.Result.ProductParsedResult::set_NormalizedProductID(System.String)
extern void ProductParsedResult_set_NormalizedProductID_m520415415B0B49F2B80D8BA0910DD96B834EB38A (void);
// 0x00000807 ZXing.Client.Result.ParsedResult ZXing.Client.Result.ProductResultParser::parse(ZXing.Result)
extern void ProductResultParser_parse_mBC3185C24439FD66D3DABDA1DA84B58B45F925F8 (void);
// 0x00000808 System.Void ZXing.Client.Result.ProductResultParser::.ctor()
extern void ProductResultParser__ctor_m7FD5B6CB0436D20EB8C598F3E0C9DF8777440C06 (void);
// 0x00000809 ZXing.Client.Result.ParsedResult ZXing.Client.Result.ResultParser::parse(ZXing.Result)
// 0x0000080A ZXing.Client.Result.ParsedResult ZXing.Client.Result.ResultParser::parseResult(ZXing.Result)
extern void ResultParser_parseResult_m6059D8436C188C262D0E6EA2021D6CFABA7DE7A2 (void);
// 0x0000080B System.Void ZXing.Client.Result.ResultParser::maybeAppend(System.String,System.Text.StringBuilder)
extern void ResultParser_maybeAppend_mE572AB168D59D1CDE52DBEB7196D740EACBB7719 (void);
// 0x0000080C System.Void ZXing.Client.Result.ResultParser::maybeAppend(System.String[],System.Text.StringBuilder)
extern void ResultParser_maybeAppend_mC887A33C6B67FBCAB05331B162C625FE6B3B7B2F (void);
// 0x0000080D System.String[] ZXing.Client.Result.ResultParser::maybeWrap(System.String)
extern void ResultParser_maybeWrap_mECEC0410EC62B5761ED9118F035ED81822BA40E8 (void);
// 0x0000080E System.String ZXing.Client.Result.ResultParser::unescapeBackslash(System.String)
extern void ResultParser_unescapeBackslash_m4E624E0D1CAE213B6D6DC9AC15DA9343A4880E5C (void);
// 0x0000080F System.Int32 ZXing.Client.Result.ResultParser::parseHexDigit(System.Char)
extern void ResultParser_parseHexDigit_m05F2631B189D1D43139998FED0FEE91F2CE989CB (void);
// 0x00000810 System.Boolean ZXing.Client.Result.ResultParser::isStringOfDigits(System.String,System.Int32)
extern void ResultParser_isStringOfDigits_mD5DC11477DB3E573B9F9875A68159285A1A1C972 (void);
// 0x00000811 System.Boolean ZXing.Client.Result.ResultParser::isSubstringOfDigits(System.String,System.Int32,System.Int32)
extern void ResultParser_isSubstringOfDigits_mAEF3B4A19F8591F3E91BEF420ECA99D44E83C337 (void);
// 0x00000812 System.Collections.Generic.IDictionary`2<System.String,System.String> ZXing.Client.Result.ResultParser::parseNameValuePairs(System.String)
extern void ResultParser_parseNameValuePairs_mD246E0BC146446D2F81ECAA4FF15A9F903BBFA4B (void);
// 0x00000813 System.Void ZXing.Client.Result.ResultParser::appendKeyValue(System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void ResultParser_appendKeyValue_mBB2D1047D6AA51F8672DBEDE4CED61159F52245D (void);
// 0x00000814 System.String[] ZXing.Client.Result.ResultParser::matchPrefixedField(System.String,System.String,System.Char,System.Boolean)
extern void ResultParser_matchPrefixedField_mF6931EC774EF2FDD764676FF908859FE6489458C (void);
// 0x00000815 System.Int32 ZXing.Client.Result.ResultParser::countPrecedingBackslashes(System.String,System.Int32)
extern void ResultParser_countPrecedingBackslashes_m3C0AB519AB434035B602E8735C41C47C27050D19 (void);
// 0x00000816 System.String ZXing.Client.Result.ResultParser::matchSinglePrefixedField(System.String,System.String,System.Char,System.Boolean)
extern void ResultParser_matchSinglePrefixedField_m4748AC7105E9DBBFF4153C5EFAB795FAAD163A74 (void);
// 0x00000817 System.String ZXing.Client.Result.ResultParser::urlDecode(System.String)
extern void ResultParser_urlDecode_m5F78783082E778F27AAC4ED3914281566FB8021C (void);
// 0x00000818 System.Int32 ZXing.Client.Result.ResultParser::findFirstEscape(System.Char[])
extern void ResultParser_findFirstEscape_m18695018BA1835A2C1DC46A89B42DB5149F4F317 (void);
// 0x00000819 System.Void ZXing.Client.Result.ResultParser::.ctor()
extern void ResultParser__ctor_mFF8520ECF7CFFB9C850F062BF8D024F413156C97 (void);
// 0x0000081A System.Void ZXing.Client.Result.ResultParser::.cctor()
extern void ResultParser__cctor_mD06E1D4102C897C00DD5C8420C35BE06A3B3BD0C (void);
// 0x0000081B ZXing.Client.Result.ParsedResult ZXing.Client.Result.SMSMMSResultParser::parse(ZXing.Result)
extern void SMSMMSResultParser_parse_m741A88F2B66CB07A95A1CC6AA8FBE99C24B5B3B9 (void);
// 0x0000081C System.Void ZXing.Client.Result.SMSMMSResultParser::addNumberVia(System.Collections.Generic.ICollection`1<System.String>,System.Collections.Generic.ICollection`1<System.String>,System.String)
extern void SMSMMSResultParser_addNumberVia_m44A9C182FF04DCAC5E9BB6F8856157E8864A78A0 (void);
// 0x0000081D System.Void ZXing.Client.Result.SMSMMSResultParser::.ctor()
extern void SMSMMSResultParser__ctor_m3E03B3201C5F0C1B823B05907C90DD6A06A41A72 (void);
// 0x0000081E System.Void ZXing.Client.Result.SMSParsedResult::.ctor(System.String,System.String,System.String,System.String)
extern void SMSParsedResult__ctor_mD371CE3809C424A63F6872F3BF1430BCBCE8481C (void);
// 0x0000081F System.Void ZXing.Client.Result.SMSParsedResult::.ctor(System.String[],System.String[],System.String,System.String)
extern void SMSParsedResult__ctor_mF5E34C1410A9298B1BC8A489976ABE4C0A1AA636 (void);
// 0x00000820 System.String ZXing.Client.Result.SMSParsedResult::getSMSURI()
extern void SMSParsedResult_getSMSURI_mC2469228D29C3F89E329ECBC0DFB987D2C74B61F (void);
// 0x00000821 System.String[] ZXing.Client.Result.SMSParsedResult::get_Numbers()
extern void SMSParsedResult_get_Numbers_mB86CF2CFCBFAE38C557B10E4DAAE41413B967FEE (void);
// 0x00000822 System.Void ZXing.Client.Result.SMSParsedResult::set_Numbers(System.String[])
extern void SMSParsedResult_set_Numbers_m2A2EEFFCB5D4AE742E95BAF06FE5021AA4C0921F (void);
// 0x00000823 System.String[] ZXing.Client.Result.SMSParsedResult::get_Vias()
extern void SMSParsedResult_get_Vias_m3E3C2FCF1DA281D05DD4989BF4DE79FED4776787 (void);
// 0x00000824 System.Void ZXing.Client.Result.SMSParsedResult::set_Vias(System.String[])
extern void SMSParsedResult_set_Vias_mF67B884BA13267C4F81546126FE211D2B40D3FCD (void);
// 0x00000825 System.String ZXing.Client.Result.SMSParsedResult::get_Subject()
extern void SMSParsedResult_get_Subject_mEFAD3E4A258242B0EECE0A2A4D6F899F1B9B3D00 (void);
// 0x00000826 System.Void ZXing.Client.Result.SMSParsedResult::set_Subject(System.String)
extern void SMSParsedResult_set_Subject_m5D0267FE9447F729C6E9B0B9EA6CBC60CBD569FC (void);
// 0x00000827 System.String ZXing.Client.Result.SMSParsedResult::get_Body()
extern void SMSParsedResult_get_Body_m5C7563D6B69E93D74B53C07E1673744E0A61A6B6 (void);
// 0x00000828 System.Void ZXing.Client.Result.SMSParsedResult::set_Body(System.String)
extern void SMSParsedResult_set_Body_m2B9E87B2A500C0E533FEBE6CBBEC42D1311C1586 (void);
// 0x00000829 System.String ZXing.Client.Result.SMSParsedResult::get_SMSURI()
extern void SMSParsedResult_get_SMSURI_m14F5B9843CDD70587066A60074E78576558C67BB (void);
// 0x0000082A System.Void ZXing.Client.Result.SMSParsedResult::set_SMSURI(System.String)
extern void SMSParsedResult_set_SMSURI_mD360A4A3DAEF13C6FC8E27155C1B921AD079B93B (void);
// 0x0000082B ZXing.Client.Result.ParsedResult ZXing.Client.Result.SMSTOMMSTOResultParser::parse(ZXing.Result)
extern void SMSTOMMSTOResultParser_parse_mB1FD412CE3D8CEE5DC45E7FD469BD487793D273C (void);
// 0x0000082C System.Void ZXing.Client.Result.SMSTOMMSTOResultParser::.ctor()
extern void SMSTOMMSTOResultParser__ctor_mE9F6BCDDD5CF0D9AF014A06B3E353C4CAD680FE0 (void);
// 0x0000082D ZXing.Client.Result.ParsedResult ZXing.Client.Result.SMTPResultParser::parse(ZXing.Result)
extern void SMTPResultParser_parse_mF844E81F420BAAA66B6D7423E8791E9E469A0639 (void);
// 0x0000082E System.Void ZXing.Client.Result.SMTPResultParser::.ctor()
extern void SMTPResultParser__ctor_m8576234AA818896AA659024C653C8333AB812C6D (void);
// 0x0000082F System.Void ZXing.Client.Result.TelParsedResult::.ctor(System.String,System.String,System.String)
extern void TelParsedResult__ctor_m239B4043D0C7A65A2D67B323382181AA84E56E2D (void);
// 0x00000830 System.String ZXing.Client.Result.TelParsedResult::get_Number()
extern void TelParsedResult_get_Number_mEFE1F6FF62DBE65DB1F32BDD6574B91BA2A42E01 (void);
// 0x00000831 System.Void ZXing.Client.Result.TelParsedResult::set_Number(System.String)
extern void TelParsedResult_set_Number_mF37925DBE19616345DFDD8333C90468C441C0672 (void);
// 0x00000832 System.String ZXing.Client.Result.TelParsedResult::get_TelURI()
extern void TelParsedResult_get_TelURI_mD4B27C0480AD24E5D953C61D214CB2AF21EE310C (void);
// 0x00000833 System.Void ZXing.Client.Result.TelParsedResult::set_TelURI(System.String)
extern void TelParsedResult_set_TelURI_m67FB009C0E623636B72793AD23615A42D9CC803A (void);
// 0x00000834 System.String ZXing.Client.Result.TelParsedResult::get_Title()
extern void TelParsedResult_get_Title_m328ECF59686B97A9F5E8D2D3A0E08E306B6D8B0A (void);
// 0x00000835 System.Void ZXing.Client.Result.TelParsedResult::set_Title(System.String)
extern void TelParsedResult_set_Title_m6C8934456E30B34380758BD59C72C6A6FAB71B2E (void);
// 0x00000836 ZXing.Client.Result.ParsedResult ZXing.Client.Result.TelResultParser::parse(ZXing.Result)
extern void TelResultParser_parse_mC5AB74B031C32ACE2375A4631D7993E5973D029E (void);
// 0x00000837 System.Void ZXing.Client.Result.TelResultParser::.ctor()
extern void TelResultParser__ctor_mCC9AF7508499739783ED50689099B7225E25F5D1 (void);
// 0x00000838 System.Void ZXing.Client.Result.TextParsedResult::.ctor(System.String,System.String)
extern void TextParsedResult__ctor_m4060479DFF604CA66B3A48125C56F6FA9AE1521B (void);
// 0x00000839 System.String ZXing.Client.Result.TextParsedResult::get_Text()
extern void TextParsedResult_get_Text_m4D8FB51DF4AC46A7012507D02D8824305CF4D304 (void);
// 0x0000083A System.Void ZXing.Client.Result.TextParsedResult::set_Text(System.String)
extern void TextParsedResult_set_Text_m55DB302AB055373DF6D2F046918BAAA36160415F (void);
// 0x0000083B System.String ZXing.Client.Result.TextParsedResult::get_Language()
extern void TextParsedResult_get_Language_m15BA33CC9293321A0A9C591F7829C1394FFE91EF (void);
// 0x0000083C System.Void ZXing.Client.Result.TextParsedResult::set_Language(System.String)
extern void TextParsedResult_set_Language_m285738534DC6883FCB9169D34B5745CC7866B46F (void);
// 0x0000083D System.String ZXing.Client.Result.URIParsedResult::get_URI()
extern void URIParsedResult_get_URI_m8BF02FAFCA4049BF8A75481E1556A785824438E7 (void);
// 0x0000083E System.Void ZXing.Client.Result.URIParsedResult::set_URI(System.String)
extern void URIParsedResult_set_URI_mB69DB9230A3F0E57F065079B7026CD9C541403D5 (void);
// 0x0000083F System.String ZXing.Client.Result.URIParsedResult::get_Title()
extern void URIParsedResult_get_Title_m5CF62FA8D702E5AEA450D5DFB92A1DB89F8D8515 (void);
// 0x00000840 System.Void ZXing.Client.Result.URIParsedResult::set_Title(System.String)
extern void URIParsedResult_set_Title_mE526803CD2157435EB9A41E83418E93DC8F51A47 (void);
// 0x00000841 System.Boolean ZXing.Client.Result.URIParsedResult::get_PossiblyMaliciousURI()
extern void URIParsedResult_get_PossiblyMaliciousURI_mF95E1663D1BC45A5C7E3A2F8FFE4FE5E95026119 (void);
// 0x00000842 System.Void ZXing.Client.Result.URIParsedResult::set_PossiblyMaliciousURI(System.Boolean)
extern void URIParsedResult_set_PossiblyMaliciousURI_m5AFE5376279DC6B94C3452530087FE77D64894AE (void);
// 0x00000843 System.Void ZXing.Client.Result.URIParsedResult::.ctor(System.String,System.String)
extern void URIParsedResult__ctor_m9E1D382DE7CF3BD5C4586E53EE2B9CD66A1AF4E4 (void);
// 0x00000844 System.String ZXing.Client.Result.URIParsedResult::massageURI(System.String)
extern void URIParsedResult_massageURI_m62A5E5F09F0989D228308BC8F14CB55BEA95C495 (void);
// 0x00000845 System.Boolean ZXing.Client.Result.URIParsedResult::isColonFollowedByPortNumber(System.String,System.Int32)
extern void URIParsedResult_isColonFollowedByPortNumber_mBE7387722EA3C5DE5568D039BBA57EDC39ECAE43 (void);
// 0x00000846 ZXing.Client.Result.ParsedResult ZXing.Client.Result.URIResultParser::parse(ZXing.Result)
extern void URIResultParser_parse_m060B537712DE980B549F854311444663564B62DC (void);
// 0x00000847 System.Boolean ZXing.Client.Result.URIResultParser::isPossiblyMaliciousURI(System.String)
extern void URIResultParser_isPossiblyMaliciousURI_m30EE507A7F4C751DAFA45AB3E8FA1E647CEC15B0 (void);
// 0x00000848 System.Boolean ZXing.Client.Result.URIResultParser::isBasicallyValidURI(System.String)
extern void URIResultParser_isBasicallyValidURI_m90A406DABF0827B371D886FED2F36DA57B1C45C3 (void);
// 0x00000849 System.Void ZXing.Client.Result.URIResultParser::.ctor()
extern void URIResultParser__ctor_m87EE5EEBFACB69982E3E5913FE6C4C6366978FD7 (void);
// 0x0000084A System.Void ZXing.Client.Result.URIResultParser::.cctor()
extern void URIResultParser__cctor_mE12B5D1B0095DC9C1D69CE132581F576F6CAC584 (void);
// 0x0000084B ZXing.Client.Result.ParsedResult ZXing.Client.Result.URLTOResultParser::parse(ZXing.Result)
extern void URLTOResultParser_parse_m96D91B0A3212DB577146E0866A3AF4B6DE8F0FD9 (void);
// 0x0000084C System.Void ZXing.Client.Result.URLTOResultParser::.ctor()
extern void URLTOResultParser__ctor_m95B2F99D73D8C88DE9A07E705724A6340351B5EE (void);
// 0x0000084D ZXing.Client.Result.ParsedResult ZXing.Client.Result.VCardResultParser::parse(ZXing.Result)
extern void VCardResultParser_parse_m4E72F0F7358A4B64DAEA4ABADD2E31AE997B4A2F (void);
// 0x0000084E System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>> ZXing.Client.Result.VCardResultParser::matchVCardPrefixedField(System.String,System.String,System.Boolean,System.Boolean)
extern void VCardResultParser_matchVCardPrefixedField_m0D0884C3FC144493CE00BAA0738F99A332062621 (void);
// 0x0000084F System.String ZXing.Client.Result.VCardResultParser::decodeQuotedPrintable(System.String,System.String)
extern void VCardResultParser_decodeQuotedPrintable_mECCD87C23C073967DFA8EB469FD137B862BC52A6 (void);
// 0x00000850 System.Void ZXing.Client.Result.VCardResultParser::maybeAppendFragment(System.IO.MemoryStream,System.String,System.Text.StringBuilder)
extern void VCardResultParser_maybeAppendFragment_mD1303DB6EEBEE87C5E72EE1CA33EF42EB9BADE1D (void);
// 0x00000851 System.Collections.Generic.List`1<System.String> ZXing.Client.Result.VCardResultParser::matchSingleVCardPrefixedField(System.String,System.String,System.Boolean,System.Boolean)
extern void VCardResultParser_matchSingleVCardPrefixedField_m630524C526820E7DF15468EE8D778024063BAD3B (void);
// 0x00000852 System.String ZXing.Client.Result.VCardResultParser::toPrimaryValue(System.Collections.Generic.List`1<System.String>)
extern void VCardResultParser_toPrimaryValue_m5F4B81EF21D8468157FF575D5394E5A1733876F5 (void);
// 0x00000853 System.String[] ZXing.Client.Result.VCardResultParser::toPrimaryValues(System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<System.String>>)
extern void VCardResultParser_toPrimaryValues_m30BBA956D0D4D30A3E0B92A106999B06F116F360 (void);
// 0x00000854 System.String[] ZXing.Client.Result.VCardResultParser::toTypes(System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<System.String>>)
extern void VCardResultParser_toTypes_mAD086426CB06E6742E092B9B954576C4AFBA161F (void);
// 0x00000855 System.Boolean ZXing.Client.Result.VCardResultParser::isLikeVCardDate(System.String)
extern void VCardResultParser_isLikeVCardDate_m6EBEF930FFB9336179CCAD86E0B00B06E235133E (void);
// 0x00000856 System.Void ZXing.Client.Result.VCardResultParser::formatNames(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<System.String>>)
extern void VCardResultParser_formatNames_m12FDDC63A2B27CFE4294F2FC2B3C14FEAD0E5696 (void);
// 0x00000857 System.Void ZXing.Client.Result.VCardResultParser::maybeAppendComponent(System.String[],System.Int32,System.Text.StringBuilder)
extern void VCardResultParser_maybeAppendComponent_mC5311DE8E883C25607A38D94B1E4B9B07B5D9AA0 (void);
// 0x00000858 System.Void ZXing.Client.Result.VCardResultParser::.ctor()
extern void VCardResultParser__ctor_mF5780CF4F2C39DC7301E391A903261927C74E3BD (void);
// 0x00000859 System.Void ZXing.Client.Result.VCardResultParser::.cctor()
extern void VCardResultParser__cctor_mA9F1184378825B6C4D566C6FBB2CBC063FF5F460 (void);
// 0x0000085A ZXing.Client.Result.ParsedResult ZXing.Client.Result.VEventResultParser::parse(ZXing.Result)
extern void VEventResultParser_parse_m7590BDA4AC58059BAF7332662E1DCEBE5B0BC35C (void);
// 0x0000085B System.String ZXing.Client.Result.VEventResultParser::matchSingleVCardPrefixedField(System.String,System.String,System.Boolean)
extern void VEventResultParser_matchSingleVCardPrefixedField_mD2B1C17958723C5DC9A4036C92D88B19D524734B (void);
// 0x0000085C System.String[] ZXing.Client.Result.VEventResultParser::matchVCardPrefixedField(System.String,System.String,System.Boolean)
extern void VEventResultParser_matchVCardPrefixedField_m0C17ACFB8B5F0E533513E6E2EC39D3932B2FDFF2 (void);
// 0x0000085D System.String ZXing.Client.Result.VEventResultParser::stripMailto(System.String)
extern void VEventResultParser_stripMailto_m4F906B2791E6B7BF53BFB6B7964112621A05F4AC (void);
// 0x0000085E System.Void ZXing.Client.Result.VEventResultParser::.ctor()
extern void VEventResultParser__ctor_m86CE311D55064CEA0AB17DEAF60521E604BA7255 (void);
// 0x0000085F System.String ZXing.Client.Result.VINParsedResult::get_VIN()
extern void VINParsedResult_get_VIN_m41F5BAC17AB7753200A6F18F0197AF606BDE1E18 (void);
// 0x00000860 System.Void ZXing.Client.Result.VINParsedResult::set_VIN(System.String)
extern void VINParsedResult_set_VIN_mFFFEA31D8D6C6B394DF4FEB36C35B117A404DCFB (void);
// 0x00000861 System.String ZXing.Client.Result.VINParsedResult::get_WorldManufacturerID()
extern void VINParsedResult_get_WorldManufacturerID_m1B8FAD07E569B8681396280E048FFA593923CC2A (void);
// 0x00000862 System.Void ZXing.Client.Result.VINParsedResult::set_WorldManufacturerID(System.String)
extern void VINParsedResult_set_WorldManufacturerID_mC3112936A63BD6D17367D8A409A92ECAF8D9A238 (void);
// 0x00000863 System.String ZXing.Client.Result.VINParsedResult::get_VehicleDescriptorSection()
extern void VINParsedResult_get_VehicleDescriptorSection_mCBAB61B23D53015AAA9F655B811BC2EAD496B907 (void);
// 0x00000864 System.Void ZXing.Client.Result.VINParsedResult::set_VehicleDescriptorSection(System.String)
extern void VINParsedResult_set_VehicleDescriptorSection_m524B5F3C3BA5D6D3C5B806462E566F57BA9B95D0 (void);
// 0x00000865 System.String ZXing.Client.Result.VINParsedResult::get_VehicleIdentifierSection()
extern void VINParsedResult_get_VehicleIdentifierSection_mD7CF178AE57F0345B9539C1D369FD2FC40F5F687 (void);
// 0x00000866 System.Void ZXing.Client.Result.VINParsedResult::set_VehicleIdentifierSection(System.String)
extern void VINParsedResult_set_VehicleIdentifierSection_mBC62477EA62B3333B0233FC0AD0D03AEF7AFED1B (void);
// 0x00000867 System.String ZXing.Client.Result.VINParsedResult::get_CountryCode()
extern void VINParsedResult_get_CountryCode_m002A3FA7149E497245D2A61D9D3338FF81DB8F94 (void);
// 0x00000868 System.Void ZXing.Client.Result.VINParsedResult::set_CountryCode(System.String)
extern void VINParsedResult_set_CountryCode_m6D6DD637D7961181457220CB34EE4E5741CFF8FB (void);
// 0x00000869 System.String ZXing.Client.Result.VINParsedResult::get_VehicleAttributes()
extern void VINParsedResult_get_VehicleAttributes_mC7A54649319E624039BA425CF41E5557572E066E (void);
// 0x0000086A System.Void ZXing.Client.Result.VINParsedResult::set_VehicleAttributes(System.String)
extern void VINParsedResult_set_VehicleAttributes_m828E0785130DEFFE165AE464AED964E47CC6B24D (void);
// 0x0000086B System.Int32 ZXing.Client.Result.VINParsedResult::get_ModelYear()
extern void VINParsedResult_get_ModelYear_mDD02BA3A8457299E28D9C4A846863E4EBCE0DA25 (void);
// 0x0000086C System.Void ZXing.Client.Result.VINParsedResult::set_ModelYear(System.Int32)
extern void VINParsedResult_set_ModelYear_m998E0DB2451308CC490834F31ED06E56BCD06693 (void);
// 0x0000086D System.Char ZXing.Client.Result.VINParsedResult::get_PlantCode()
extern void VINParsedResult_get_PlantCode_m4391EFF085A612934090E87618A5BBEA95676162 (void);
// 0x0000086E System.Void ZXing.Client.Result.VINParsedResult::set_PlantCode(System.Char)
extern void VINParsedResult_set_PlantCode_m06DD134C31BDB420F62EF621BBE23E4063D8FCCF (void);
// 0x0000086F System.String ZXing.Client.Result.VINParsedResult::get_SequentialNumber()
extern void VINParsedResult_get_SequentialNumber_mFC092DF0BB0DCBD5FD5504A4218907F0F04649C5 (void);
// 0x00000870 System.Void ZXing.Client.Result.VINParsedResult::set_SequentialNumber(System.String)
extern void VINParsedResult_set_SequentialNumber_m8A9FD10FCCC5FF364AA4B6D4DC0DC90F6C57D808 (void);
// 0x00000871 System.Void ZXing.Client.Result.VINParsedResult::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.Char,System.String)
extern void VINParsedResult__ctor_m3CC20FD1D7F288861E5B6E09E5CFAF65AE1313D9 (void);
// 0x00000872 System.String ZXing.Client.Result.VINParsedResult::get_DisplayResult()
extern void VINParsedResult_get_DisplayResult_m20B331A7934F851B65174F2A1F613E896AFCB2CF (void);
// 0x00000873 ZXing.Client.Result.ParsedResult ZXing.Client.Result.VINResultParser::parse(ZXing.Result)
extern void VINResultParser_parse_mD87B2B98AB06699A29F761AFE70980A521480C22 (void);
// 0x00000874 System.Boolean ZXing.Client.Result.VINResultParser::checkChecksum(System.String)
extern void VINResultParser_checkChecksum_m35DC0B6670AADE79C500F1CBB421B54B0ECFBB6F (void);
// 0x00000875 System.Int32 ZXing.Client.Result.VINResultParser::vinCharValue(System.Char)
extern void VINResultParser_vinCharValue_mBA8F8431CF5AD539B254F3D98D121D332F7C4D0F (void);
// 0x00000876 System.Int32 ZXing.Client.Result.VINResultParser::vinPositionWeight(System.Int32)
extern void VINResultParser_vinPositionWeight_m13CF472F72A17D6ECD760F23815D23C83B36BF17 (void);
// 0x00000877 System.Char ZXing.Client.Result.VINResultParser::checkChar(System.Int32)
extern void VINResultParser_checkChar_m86A4B8034BF3C8C58A0F17F47F3CD3E98B0618BE (void);
// 0x00000878 System.Int32 ZXing.Client.Result.VINResultParser::modelYear(System.Char)
extern void VINResultParser_modelYear_mF78E7D90FC3DB3BE3673CDFC3B6118C9C64DB724 (void);
// 0x00000879 System.String ZXing.Client.Result.VINResultParser::countryCode(System.String)
extern void VINResultParser_countryCode_m6A007B25D7AC6D63DE850765E322C1BC9585207D (void);
// 0x0000087A System.Void ZXing.Client.Result.VINResultParser::.ctor()
extern void VINResultParser__ctor_mB81B3BDC1A306748AA0648FF083A1BD07948DDF4 (void);
// 0x0000087B System.Void ZXing.Client.Result.VINResultParser::.cctor()
extern void VINResultParser__cctor_m4319C65A57D809CCEC61A4FBB9ADB2A2A269990D (void);
// 0x0000087C System.Void ZXing.Client.Result.WifiParsedResult::.ctor(System.String,System.String,System.String)
extern void WifiParsedResult__ctor_m40C7EB8A0D4B6EF866D1E28309CC8AF4BBBFBDB6 (void);
// 0x0000087D System.Void ZXing.Client.Result.WifiParsedResult::.ctor(System.String,System.String,System.String,System.Boolean)
extern void WifiParsedResult__ctor_m954592F31B3FA1E0A8C0DCE143D7BBABA7B2E67E (void);
// 0x0000087E System.Void ZXing.Client.Result.WifiParsedResult::.ctor(System.String,System.String,System.String,System.Boolean,System.String,System.String,System.String,System.String)
extern void WifiParsedResult__ctor_m059C124746F638C61B5CCB6BEAEF6349BBA3DEC1 (void);
// 0x0000087F System.String ZXing.Client.Result.WifiParsedResult::get_Ssid()
extern void WifiParsedResult_get_Ssid_mC8AC5841158BD170AE08100521C0BC4D57C023CF (void);
// 0x00000880 System.Void ZXing.Client.Result.WifiParsedResult::set_Ssid(System.String)
extern void WifiParsedResult_set_Ssid_mE1C3E409891203AA532943003E1BEB3C90EC5CE0 (void);
// 0x00000881 System.String ZXing.Client.Result.WifiParsedResult::get_NetworkEncryption()
extern void WifiParsedResult_get_NetworkEncryption_mB2AAEB4C26D32C7CFE681DEF03630980FB10CCD6 (void);
// 0x00000882 System.Void ZXing.Client.Result.WifiParsedResult::set_NetworkEncryption(System.String)
extern void WifiParsedResult_set_NetworkEncryption_m02244F133BCA36D09D642F99A3D4A935E1233659 (void);
// 0x00000883 System.String ZXing.Client.Result.WifiParsedResult::get_Password()
extern void WifiParsedResult_get_Password_m5196A12772B0539ACA77DE5A71537C02DF3E98AB (void);
// 0x00000884 System.Void ZXing.Client.Result.WifiParsedResult::set_Password(System.String)
extern void WifiParsedResult_set_Password_m3813C62B08C1E3672442009BDD0A0852EF1E3EBB (void);
// 0x00000885 System.Boolean ZXing.Client.Result.WifiParsedResult::get_Hidden()
extern void WifiParsedResult_get_Hidden_m69DCB213A8ED95C02B5B330DE3D178208E2C86F5 (void);
// 0x00000886 System.Void ZXing.Client.Result.WifiParsedResult::set_Hidden(System.Boolean)
extern void WifiParsedResult_set_Hidden_m981BE4A1433755608740B7E3273893DFB8C3B90D (void);
// 0x00000887 System.String ZXing.Client.Result.WifiParsedResult::get_Identity()
extern void WifiParsedResult_get_Identity_mF36367387849F243A3DB7DE1DF920A7A7A21DA81 (void);
// 0x00000888 System.Void ZXing.Client.Result.WifiParsedResult::set_Identity(System.String)
extern void WifiParsedResult_set_Identity_m71E970DA4CD37933EEA7156DDD09ECF43E1EC8C9 (void);
// 0x00000889 System.String ZXing.Client.Result.WifiParsedResult::get_AnonymousIdentity()
extern void WifiParsedResult_get_AnonymousIdentity_mC2377B68942538C4E0DE388D3B26C82CA1EBA272 (void);
// 0x0000088A System.Void ZXing.Client.Result.WifiParsedResult::set_AnonymousIdentity(System.String)
extern void WifiParsedResult_set_AnonymousIdentity_mA9C05FEC275ED9E377FFE08D23C256605C11F0FF (void);
// 0x0000088B System.String ZXing.Client.Result.WifiParsedResult::get_EapMethod()
extern void WifiParsedResult_get_EapMethod_m7814586144D62123E480453ED06BBA8E2FE51A2F (void);
// 0x0000088C System.Void ZXing.Client.Result.WifiParsedResult::set_EapMethod(System.String)
extern void WifiParsedResult_set_EapMethod_mDCAB124D3053177756A153287A85F9636285F743 (void);
// 0x0000088D System.String ZXing.Client.Result.WifiParsedResult::get_Phase2Method()
extern void WifiParsedResult_get_Phase2Method_mE6A39DE9017277C759E0BA9F74201947C23AD6A3 (void);
// 0x0000088E System.Void ZXing.Client.Result.WifiParsedResult::set_Phase2Method(System.String)
extern void WifiParsedResult_set_Phase2Method_mA648682E24C365DFF5280041D16E244F5F9BB77E (void);
// 0x0000088F ZXing.Client.Result.ParsedResult ZXing.Client.Result.WifiResultParser::parse(ZXing.Result)
extern void WifiResultParser_parse_mC23A5D51E97B5E904E85F91BC9E583722EB06801 (void);
// 0x00000890 System.Void ZXing.Client.Result.WifiResultParser::.ctor()
extern void WifiResultParser__ctor_m490D7F7021C75D1BDE2BD7303EA07BFA8AAC9C93 (void);
// 0x00000891 ZXing.Result ZXing.Aztec.AztecReader::decode(ZXing.BinaryBitmap)
extern void AztecReader_decode_m6B7033967224885BD515DAB4628A9F22560E6E6F (void);
// 0x00000892 ZXing.Result ZXing.Aztec.AztecReader::decode(ZXing.BinaryBitmap,System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>)
extern void AztecReader_decode_m0F6C80BD09854EDA44F70B846BA6F21CFDC49E91 (void);
// 0x00000893 System.Void ZXing.Aztec.AztecReader::reset()
extern void AztecReader_reset_m1E07466CE8C17AD13F51DCEBCF9A11308FA159A8 (void);
// 0x00000894 System.Void ZXing.Aztec.AztecReader::.ctor()
extern void AztecReader__ctor_mCBC609EFA18813AEA11C4A6DF087E68DCD6F3336 (void);
// 0x00000895 System.Boolean ZXing.Aztec.AztecResultMetadata::get_Compact()
extern void AztecResultMetadata_get_Compact_m1430C7DAF96061FAE1A2105C5DFC091F379B8FA5 (void);
// 0x00000896 System.Void ZXing.Aztec.AztecResultMetadata::set_Compact(System.Boolean)
extern void AztecResultMetadata_set_Compact_m73367652825CD830773A5336F6F9134F52A1E65D (void);
// 0x00000897 System.Int32 ZXing.Aztec.AztecResultMetadata::get_Datablocks()
extern void AztecResultMetadata_get_Datablocks_mBF8F2055196EF44753C598D6C6FA1C4B2C4710F1 (void);
// 0x00000898 System.Void ZXing.Aztec.AztecResultMetadata::set_Datablocks(System.Int32)
extern void AztecResultMetadata_set_Datablocks_m28360A13E8ED3C88C89C8F88EAD5306C344DB950 (void);
// 0x00000899 System.Int32 ZXing.Aztec.AztecResultMetadata::get_Layers()
extern void AztecResultMetadata_get_Layers_mDBE1500AF7ADD1379D9A302CE9A4893B0D8CA226 (void);
// 0x0000089A System.Void ZXing.Aztec.AztecResultMetadata::set_Layers(System.Int32)
extern void AztecResultMetadata_set_Layers_m7FDA4D04DA26425519C9AADECC381BA26E65E81C (void);
// 0x0000089B System.Void ZXing.Aztec.AztecResultMetadata::.ctor(System.Boolean,System.Int32,System.Int32)
extern void AztecResultMetadata__ctor_m14D8BE64ADF2D77D4D8B027E8BC7AF833982662C (void);
// 0x0000089C System.Void ZXing.Aztec.AztecWriter::.cctor()
extern void AztecWriter__cctor_m0951B4B729701E5718F417C3C9F79DCF970A77AF (void);
// 0x0000089D ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32)
extern void AztecWriter_encode_mB4866D82FE01EAC3029F0E9F5B0183EC4C9E0DAA (void);
// 0x0000089E ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void AztecWriter_encode_m1A2E9C250748E195A9141447133D9A4AB0443C04 (void);
// 0x0000089F ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Text.Encoding,System.Int32,System.Int32)
extern void AztecWriter_encode_m8C7A47FDED5D17D2BF449074274B8307F829B99D (void);
// 0x000008A0 ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::renderResult(ZXing.Aztec.Internal.AztecCode,System.Int32,System.Int32)
extern void AztecWriter_renderResult_mE032EBA6309C764259921AB7AD4E53CC71D2E662 (void);
// 0x000008A1 System.Void ZXing.Aztec.AztecWriter::.ctor()
extern void AztecWriter__ctor_mA2737566BBF2E9BF6D5991CF6407E769741B9A91 (void);
// 0x000008A2 System.Nullable`1<System.Int32> ZXing.Aztec.AztecEncodingOptions::get_ErrorCorrection()
extern void AztecEncodingOptions_get_ErrorCorrection_m40E71A05C5F5B8FD7F07AA24A987E1FAAF6C3F76 (void);
// 0x000008A3 System.Void ZXing.Aztec.AztecEncodingOptions::set_ErrorCorrection(System.Nullable`1<System.Int32>)
extern void AztecEncodingOptions_set_ErrorCorrection_m85F38A60CC52418315573A1F74C52F567917289A (void);
// 0x000008A4 System.Nullable`1<System.Int32> ZXing.Aztec.AztecEncodingOptions::get_Layers()
extern void AztecEncodingOptions_get_Layers_m81A9CE0E23D4FE008D890A590D85556D50CE8669 (void);
// 0x000008A5 System.Void ZXing.Aztec.AztecEncodingOptions::set_Layers(System.Nullable`1<System.Int32>)
extern void AztecEncodingOptions_set_Layers_m48119005B3E1BF4A0FF88E03D1134F606FA7FFA1 (void);
// 0x000008A6 System.Void ZXing.Aztec.AztecEncodingOptions::.ctor()
extern void AztecEncodingOptions__ctor_mA4F317A242D4302CEB43D1B54A1A7E2B0289E393 (void);
// 0x000008A7 System.Boolean ZXing.Aztec.Internal.AztecDetectorResult::get_Compact()
extern void AztecDetectorResult_get_Compact_mFD9801AB8A2D778D5E09086526ADF6EF68C95575 (void);
// 0x000008A8 System.Void ZXing.Aztec.Internal.AztecDetectorResult::set_Compact(System.Boolean)
extern void AztecDetectorResult_set_Compact_m7E681FFE309327450CEC845730DA9164A4914EC6 (void);
// 0x000008A9 System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::get_NbDatablocks()
extern void AztecDetectorResult_get_NbDatablocks_m9FAE052F42EADB9D4B795316AEFE9BF71E90B790 (void);
// 0x000008AA System.Void ZXing.Aztec.Internal.AztecDetectorResult::set_NbDatablocks(System.Int32)
extern void AztecDetectorResult_set_NbDatablocks_m7EB372EE4FF562F799ADD79BB38FE19F188CDEB0 (void);
// 0x000008AB System.Int32 ZXing.Aztec.Internal.AztecDetectorResult::get_NbLayers()
extern void AztecDetectorResult_get_NbLayers_m6F22385A1607D79215D935D69424CEEF5C8BFBEF (void);
// 0x000008AC System.Void ZXing.Aztec.Internal.AztecDetectorResult::set_NbLayers(System.Int32)
extern void AztecDetectorResult_set_NbLayers_m7C0D2A6CB1D069E7CB405911D328901FA4371600 (void);
// 0x000008AD System.Void ZXing.Aztec.Internal.AztecDetectorResult::.ctor(ZXing.Common.BitMatrix,ZXing.ResultPoint[],System.Boolean,System.Int32,System.Int32)
extern void AztecDetectorResult__ctor_m042F3E01061A420A9A8AF8D1A069B2766A616730 (void);
// 0x000008AE ZXing.Common.DecoderResult ZXing.Aztec.Internal.Decoder::decode(ZXing.Aztec.Internal.AztecDetectorResult)
extern void Decoder_decode_m975B500E099D647B4AE51438DA9EAADD9F12F793 (void);
// 0x000008AF System.String ZXing.Aztec.Internal.Decoder::highLevelDecode(System.Boolean[])
extern void Decoder_highLevelDecode_mCD4D1203342ABB0232594F8A3AAFC991ED52697E (void);
// 0x000008B0 System.String ZXing.Aztec.Internal.Decoder::getEncodedData(System.Boolean[])
extern void Decoder_getEncodedData_m54DFB32C65271082FF08BB1FEF8F626EE72D1B2A (void);
// 0x000008B1 ZXing.Aztec.Internal.Decoder/Table ZXing.Aztec.Internal.Decoder::getTable(System.Char)
extern void Decoder_getTable_m4613C50AF912D5113D08AE42E426F4012969A18C (void);
// 0x000008B2 System.String ZXing.Aztec.Internal.Decoder::getCharacter(System.String[],System.Int32)
extern void Decoder_getCharacter_m20A140DA5F2CB6BDFD69620AC16AA5BE53D63083 (void);
// 0x000008B3 System.Boolean[] ZXing.Aztec.Internal.Decoder::correctBits(System.Boolean[])
extern void Decoder_correctBits_m159CA039058A11C0475AEDC47358AF7D76269F42 (void);
// 0x000008B4 System.Boolean[] ZXing.Aztec.Internal.Decoder::extractBits(ZXing.Common.BitMatrix)
extern void Decoder_extractBits_m237BC440228E149198E6841338704EA972B2D5C8 (void);
// 0x000008B5 System.Int32 ZXing.Aztec.Internal.Decoder::readCode(System.Boolean[],System.Int32,System.Int32)
extern void Decoder_readCode_mC3FA4A261AB6717D89E59D40C849653D21C9A494 (void);
// 0x000008B6 System.Byte ZXing.Aztec.Internal.Decoder::readByte(System.Boolean[],System.Int32)
extern void Decoder_readByte_m1ED04549537119AFB95DF7EC21F3D38D57E8BF76 (void);
// 0x000008B7 System.Byte[] ZXing.Aztec.Internal.Decoder::convertBoolArrayToByteArray(System.Boolean[])
extern void Decoder_convertBoolArrayToByteArray_m0106370E0B021B1F6CB9256618A5162A258CA17B (void);
// 0x000008B8 System.Int32 ZXing.Aztec.Internal.Decoder::totalBitsInLayer(System.Int32,System.Boolean)
extern void Decoder_totalBitsInLayer_m805B43C4EAB23AD9A2508A710E3BA661046BC06B (void);
// 0x000008B9 System.Void ZXing.Aztec.Internal.Decoder::.ctor()
extern void Decoder__ctor_m5CFC2A1F4328BD3443EEE8ABCFAD885AE44A8426 (void);
// 0x000008BA System.Void ZXing.Aztec.Internal.Decoder::.cctor()
extern void Decoder__cctor_mC5B1E6BAC27224BEC5E2280B2FE40949B076A3D5 (void);
// 0x000008BB System.Void ZXing.Aztec.Internal.Detector::.ctor(ZXing.Common.BitMatrix)
extern void Detector__ctor_mA3F037185930CEB5584A79637E33D3A60AB8C00D (void);
// 0x000008BC ZXing.Aztec.Internal.AztecDetectorResult ZXing.Aztec.Internal.Detector::detect()
extern void Detector_detect_mC36868ADC2112A2752DB231C145B94AD611DE6D1 (void);
// 0x000008BD ZXing.Aztec.Internal.AztecDetectorResult ZXing.Aztec.Internal.Detector::detect(System.Boolean)
extern void Detector_detect_m5EE6D067CB4AFF977CE646AC2F5DAA9CE4C0D14F (void);
// 0x000008BE System.Boolean ZXing.Aztec.Internal.Detector::extractParameters(ZXing.ResultPoint[])
extern void Detector_extractParameters_m1DDB158B193FD0F6F0C8E99579663BD7ACCDA099 (void);
// 0x000008BF System.Int32 ZXing.Aztec.Internal.Detector::getRotation(System.Int32[],System.Int32)
extern void Detector_getRotation_m46AAD30137FD8BC570F70C8261A2A2BF0FED35D1 (void);
// 0x000008C0 System.Int32 ZXing.Aztec.Internal.Detector::getCorrectedParameterData(System.Int64,System.Boolean)
extern void Detector_getCorrectedParameterData_m44C6D073EBC2D2C615F6B96B7F4AE0D0EF9E9EFB (void);
// 0x000008C1 ZXing.ResultPoint[] ZXing.Aztec.Internal.Detector::getBullsEyeCorners(ZXing.Aztec.Internal.Detector/Point)
extern void Detector_getBullsEyeCorners_mB0610453F689A1B2754BDFC63B9720F8B16149BE (void);
// 0x000008C2 ZXing.Aztec.Internal.Detector/Point ZXing.Aztec.Internal.Detector::getMatrixCenter()
extern void Detector_getMatrixCenter_m87D2C691DC365CD59DD1C2461F7CA17B4B1825BE (void);
// 0x000008C3 ZXing.ResultPoint[] ZXing.Aztec.Internal.Detector::getMatrixCornerPoints(ZXing.ResultPoint[])
extern void Detector_getMatrixCornerPoints_m1046C92DEB0B3624ECC45436BCBF17DD41A0AEC1 (void);
// 0x000008C4 ZXing.Common.BitMatrix ZXing.Aztec.Internal.Detector::sampleGrid(ZXing.Common.BitMatrix,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_sampleGrid_mA656EA2FCFE420E8C1E427013E1F9BC4360B442A (void);
// 0x000008C5 System.Int32 ZXing.Aztec.Internal.Detector::sampleLine(ZXing.ResultPoint,ZXing.ResultPoint,System.Int32)
extern void Detector_sampleLine_m3F2092A40080AEC8ECC8600EA9DF671D8A3E0E15 (void);
// 0x000008C6 System.Boolean ZXing.Aztec.Internal.Detector::isWhiteOrBlackRectangle(ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point)
extern void Detector_isWhiteOrBlackRectangle_m2D50371158046253F0D435A619133E56FD366469 (void);
// 0x000008C7 System.Int32 ZXing.Aztec.Internal.Detector::getColor(ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point)
extern void Detector_getColor_m1B9F53F7F6714BFB534C4404B04E8C4AA56C8B0D (void);
// 0x000008C8 ZXing.Aztec.Internal.Detector/Point ZXing.Aztec.Internal.Detector::getFirstDifferent(ZXing.Aztec.Internal.Detector/Point,System.Boolean,System.Int32,System.Int32)
extern void Detector_getFirstDifferent_m5FD38C7C9C0AE7767C9A46DCE5CBABFC9703AA39 (void);
// 0x000008C9 ZXing.ResultPoint[] ZXing.Aztec.Internal.Detector::expandSquare(ZXing.ResultPoint[],System.Int32,System.Int32)
extern void Detector_expandSquare_mF45ACEA75AECDE87BFB25149590F27B4C0945A0E (void);
// 0x000008CA System.Boolean ZXing.Aztec.Internal.Detector::isValid(System.Int32,System.Int32)
extern void Detector_isValid_m9F77938FA17D7CBFF401F7924327BDD815CCC684 (void);
// 0x000008CB System.Boolean ZXing.Aztec.Internal.Detector::isValid(ZXing.ResultPoint)
extern void Detector_isValid_m1FD2700BCED185A1AFE373539312E3B4E2AC2BB8 (void);
// 0x000008CC System.Single ZXing.Aztec.Internal.Detector::distance(ZXing.Aztec.Internal.Detector/Point,ZXing.Aztec.Internal.Detector/Point)
extern void Detector_distance_m765C3C333C4376746142B21C25B6694CE9F91498 (void);
// 0x000008CD System.Single ZXing.Aztec.Internal.Detector::distance(ZXing.ResultPoint,ZXing.ResultPoint)
extern void Detector_distance_mEB134F0DB2455BA849226532AD86E28380A6D342 (void);
// 0x000008CE System.Int32 ZXing.Aztec.Internal.Detector::getDimension()
extern void Detector_getDimension_m7F0A2EF9EA5F7828785438F49AC2D1E582BDEA45 (void);
// 0x000008CF System.Void ZXing.Aztec.Internal.Detector::.cctor()
extern void Detector__cctor_m421D861C5A6505AC6E6A58797BA1ECE0FAACC3B2 (void);
// 0x000008D0 System.Int32 ZXing.Aztec.Internal.Detector/Point::get_X()
extern void Point_get_X_mEE048E5CFDD4522F3F1AD9DD798A0ED6AED64B0D (void);
// 0x000008D1 System.Void ZXing.Aztec.Internal.Detector/Point::set_X(System.Int32)
extern void Point_set_X_m36F39BFA4A702392864A8516504ED08CAEF0B912 (void);
// 0x000008D2 System.Int32 ZXing.Aztec.Internal.Detector/Point::get_Y()
extern void Point_get_Y_mD6F4B36F9C34D3115ED81D8AC1D266DB7BC8CBE0 (void);
// 0x000008D3 System.Void ZXing.Aztec.Internal.Detector/Point::set_Y(System.Int32)
extern void Point_set_Y_m97FB36260991E183F0D4DE988EFA16C20A6AF771 (void);
// 0x000008D4 ZXing.ResultPoint ZXing.Aztec.Internal.Detector/Point::toResultPoint()
extern void Point_toResultPoint_m498027A9A371D0E922F705E05323C98FDE1E8DB9 (void);
// 0x000008D5 System.Void ZXing.Aztec.Internal.Detector/Point::.ctor(System.Int32,System.Int32)
extern void Point__ctor_m06CF8AB3BAA79ABFB5A66FC834AD48989ED049C2 (void);
// 0x000008D6 System.String ZXing.Aztec.Internal.Detector/Point::ToString()
extern void Point_ToString_m31F0012C02609FAF2FB6B667C8F97227783BE857 (void);
// 0x000008D7 System.Boolean ZXing.Aztec.Internal.AztecCode::get_isCompact()
extern void AztecCode_get_isCompact_m69FD2B81D44068FBBA6BF287FE7FBCA948EFC5B5 (void);
// 0x000008D8 System.Void ZXing.Aztec.Internal.AztecCode::set_isCompact(System.Boolean)
extern void AztecCode_set_isCompact_m4D938F46C5A2A2084B664F665BD82A5D55220A8E (void);
// 0x000008D9 System.Int32 ZXing.Aztec.Internal.AztecCode::get_Size()
extern void AztecCode_get_Size_m4BAC964B53FB59213876BCD77716959747604A42 (void);
// 0x000008DA System.Void ZXing.Aztec.Internal.AztecCode::set_Size(System.Int32)
extern void AztecCode_set_Size_m574136E98A858CFD70D39E7537846A5705F863CA (void);
// 0x000008DB System.Int32 ZXing.Aztec.Internal.AztecCode::get_Layers()
extern void AztecCode_get_Layers_m33D6AC4FD1CB92FADDAB5AFE1722E5E69F3D0C09 (void);
// 0x000008DC System.Void ZXing.Aztec.Internal.AztecCode::set_Layers(System.Int32)
extern void AztecCode_set_Layers_m1DE8F88B27C1394B796C63A3906C3E2CEDFFD42E (void);
// 0x000008DD System.Int32 ZXing.Aztec.Internal.AztecCode::get_CodeWords()
extern void AztecCode_get_CodeWords_m3DA2B05FF16A0F6CDD222DD78058DDE505C5F0DA (void);
// 0x000008DE System.Void ZXing.Aztec.Internal.AztecCode::set_CodeWords(System.Int32)
extern void AztecCode_set_CodeWords_mAF07A49B2A93C94876008227578B100FF1E9E649 (void);
// 0x000008DF ZXing.Common.BitMatrix ZXing.Aztec.Internal.AztecCode::get_Matrix()
extern void AztecCode_get_Matrix_mFAB881CE73E5A8FFA739CF57B1A09386EE08DA9E (void);
// 0x000008E0 System.Void ZXing.Aztec.Internal.AztecCode::set_Matrix(ZXing.Common.BitMatrix)
extern void AztecCode_set_Matrix_mDB6CA59D1FF5B2CC1364B2059088E7D294F8E6EE (void);
// 0x000008E1 System.Void ZXing.Aztec.Internal.AztecCode::.ctor()
extern void AztecCode__ctor_mB8C4EE157F5B3ECB35691135621C3A4C0A5BD791 (void);
// 0x000008E2 System.Void ZXing.Aztec.Internal.BinaryShiftToken::.ctor(ZXing.Aztec.Internal.Token,System.Int32,System.Int32)
extern void BinaryShiftToken__ctor_mD645FFE62473F3EE8A531760DA6B3177E7B29475 (void);
// 0x000008E3 System.Void ZXing.Aztec.Internal.BinaryShiftToken::appendTo(ZXing.Common.BitArray,System.Byte[])
extern void BinaryShiftToken_appendTo_m74811009F22D1B5EA7DA9F6BEDEE32F18DF6EF12 (void);
// 0x000008E4 System.String ZXing.Aztec.Internal.BinaryShiftToken::ToString()
extern void BinaryShiftToken_ToString_m6193FA8A97AF92FACA576EF68AA20964BDC86BD3 (void);
// 0x000008E5 ZXing.Aztec.Internal.AztecCode ZXing.Aztec.Internal.Encoder::encode(System.Byte[])
extern void Encoder_encode_mE24F3E069F3CD0F6F12952FA0A717A953B237247 (void);
// 0x000008E6 ZXing.Aztec.Internal.AztecCode ZXing.Aztec.Internal.Encoder::encode(System.Byte[],System.Int32,System.Int32)
extern void Encoder_encode_m8AE979BFC084695CBCC0CCE3F215C45B541A316A (void);
// 0x000008E7 System.Void ZXing.Aztec.Internal.Encoder::drawBullsEye(ZXing.Common.BitMatrix,System.Int32,System.Int32)
extern void Encoder_drawBullsEye_m58F92CE923B122619B5DCCB88A06C2CD0EF5CDB4 (void);
// 0x000008E8 ZXing.Common.BitArray ZXing.Aztec.Internal.Encoder::generateModeMessage(System.Boolean,System.Int32,System.Int32)
extern void Encoder_generateModeMessage_mA86608FB3DF26579C7FE27BDC096859E459909F9 (void);
// 0x000008E9 System.Void ZXing.Aztec.Internal.Encoder::drawModeMessage(ZXing.Common.BitMatrix,System.Boolean,System.Int32,ZXing.Common.BitArray)
extern void Encoder_drawModeMessage_m02AEC5F2FB33A9E9BFBC3D56416E5E0E5CAC5EC0 (void);
// 0x000008EA ZXing.Common.BitArray ZXing.Aztec.Internal.Encoder::generateCheckWords(ZXing.Common.BitArray,System.Int32,System.Int32)
extern void Encoder_generateCheckWords_m533D2727891AEDEED6E24D1611EDF0DF1ADC44CC (void);
// 0x000008EB System.Int32[] ZXing.Aztec.Internal.Encoder::bitsToWords(ZXing.Common.BitArray,System.Int32,System.Int32)
extern void Encoder_bitsToWords_mE8FABF720703844E657F5D787FFE93A59C7A1FC0 (void);
// 0x000008EC ZXing.Common.ReedSolomon.GenericGF ZXing.Aztec.Internal.Encoder::getGF(System.Int32)
extern void Encoder_getGF_m32F01B5B38672AF87DA19151DF2A169DDAC9C617 (void);
// 0x000008ED ZXing.Common.BitArray ZXing.Aztec.Internal.Encoder::stuffBits(ZXing.Common.BitArray,System.Int32)
extern void Encoder_stuffBits_m5E129EF2C2E5DDBECC13D4BE8EC1E9B3478237CC (void);
// 0x000008EE System.Int32 ZXing.Aztec.Internal.Encoder::TotalBitsInLayer(System.Int32,System.Boolean)
extern void Encoder_TotalBitsInLayer_m24841485CD1A8CEA70BA50F384023CEDDD478232 (void);
// 0x000008EF System.Void ZXing.Aztec.Internal.Encoder::.cctor()
extern void Encoder__cctor_mE4DE1D58427CF7C33125F6126C50E0A332837EE0 (void);
// 0x000008F0 System.Void ZXing.Aztec.Internal.HighLevelEncoder::.cctor()
extern void HighLevelEncoder__cctor_mC0F6D45211693A42DBCD68E86EFC26E395D5C7C1 (void);
// 0x000008F1 System.Void ZXing.Aztec.Internal.HighLevelEncoder::.ctor(System.Byte[])
extern void HighLevelEncoder__ctor_m6183D593CF192430A3505E7CC9FE9AB150124C14 (void);
// 0x000008F2 ZXing.Common.BitArray ZXing.Aztec.Internal.HighLevelEncoder::encode()
extern void HighLevelEncoder_encode_m0DBC61E51351B0B00223122A5821912A5E063B0E (void);
// 0x000008F3 System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State> ZXing.Aztec.Internal.HighLevelEncoder::updateStateListForChar(System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>,System.Int32)
extern void HighLevelEncoder_updateStateListForChar_m07101764BC528E73586360441081B487700AA6BB (void);
// 0x000008F4 System.Void ZXing.Aztec.Internal.HighLevelEncoder::updateStateForChar(ZXing.Aztec.Internal.State,System.Int32,System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State>)
extern void HighLevelEncoder_updateStateForChar_m16A30AE4AC2EE7D0C04DD29D8ED6F1786161CE42 (void);
// 0x000008F5 System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State> ZXing.Aztec.Internal.HighLevelEncoder::updateStateListForPair(System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>,System.Int32,System.Int32)
extern void HighLevelEncoder_updateStateListForPair_mC4FF70F78F74C1B67FAD58B9CA23618DA3FB273C (void);
// 0x000008F6 System.Void ZXing.Aztec.Internal.HighLevelEncoder::updateStateForPair(ZXing.Aztec.Internal.State,System.Int32,System.Int32,System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State>)
extern void HighLevelEncoder_updateStateForPair_m31DA2C18596BE8F153DFC924656653A436A4E11C (void);
// 0x000008F7 System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State> ZXing.Aztec.Internal.HighLevelEncoder::simplifyStates(System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>)
extern void HighLevelEncoder_simplifyStates_mA19C902477CC1EB73B87F2B5A87557F7268F0269 (void);
// 0x000008F8 System.Void ZXing.Aztec.Internal.SimpleToken::.ctor(ZXing.Aztec.Internal.Token,System.Int32,System.Int32)
extern void SimpleToken__ctor_mB048A551BBF58C0407ADBEEFB2F7A1F4DF03D0B2 (void);
// 0x000008F9 System.Void ZXing.Aztec.Internal.SimpleToken::appendTo(ZXing.Common.BitArray,System.Byte[])
extern void SimpleToken_appendTo_m4C8937DF31970AAF5772895EBDF090CA64701020 (void);
// 0x000008FA System.String ZXing.Aztec.Internal.SimpleToken::ToString()
extern void SimpleToken_ToString_m79770FE8DD4B006E567848DD0980F56C25FAD113 (void);
// 0x000008FB System.Void ZXing.Aztec.Internal.State::.ctor(ZXing.Aztec.Internal.Token,System.Int32,System.Int32,System.Int32)
extern void State__ctor_m6447D0311278C561A8EFBBD7E2B2E43397DC1CF2 (void);
// 0x000008FC System.Int32 ZXing.Aztec.Internal.State::get_Mode()
extern void State_get_Mode_m4436AA6CB58AD14939CB183C331AA418F8FF7216 (void);
// 0x000008FD ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.State::get_Token()
extern void State_get_Token_m88AC53BB8F5B296F24960AD16B74E7C1CD25C29D (void);
// 0x000008FE System.Int32 ZXing.Aztec.Internal.State::get_BinaryShiftByteCount()
extern void State_get_BinaryShiftByteCount_m271DAD3BB98C84DFA9E5DFB0F4ED3CBD7F0A6B1F (void);
// 0x000008FF System.Int32 ZXing.Aztec.Internal.State::get_BitCount()
extern void State_get_BitCount_m52B9A84CF4483AEB7042331BA881F9CD44B88321 (void);
// 0x00000900 ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::latchAndAppend(System.Int32,System.Int32)
extern void State_latchAndAppend_m5CA1DB20EC7F4F0F09A312297F5ED27A850C1946 (void);
// 0x00000901 ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::shiftAndAppend(System.Int32,System.Int32)
extern void State_shiftAndAppend_mEED73AEFB1E24794FD91A0A12CC294EE76552375 (void);
// 0x00000902 ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::addBinaryShiftChar(System.Int32)
extern void State_addBinaryShiftChar_m065C2AA0B61A61A21E6F719881BFED020F5E74C3 (void);
// 0x00000903 ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::endBinaryShift(System.Int32)
extern void State_endBinaryShift_m678C7C2FC95810C512799F280C87B27E36A4F08E (void);
// 0x00000904 System.Boolean ZXing.Aztec.Internal.State::isBetterThanOrEqualTo(ZXing.Aztec.Internal.State)
extern void State_isBetterThanOrEqualTo_mF4BA2E9B5B429AD0281CCBDA33C39662297A3D71 (void);
// 0x00000905 ZXing.Common.BitArray ZXing.Aztec.Internal.State::toBitArray(System.Byte[])
extern void State_toBitArray_mEC4C2D3D7D5AE27BC6A6E0EE2396AF0E161FF726 (void);
// 0x00000906 System.String ZXing.Aztec.Internal.State::ToString()
extern void State_ToString_m782270C7BD6F9E9516DDA1538AE44125B320A978 (void);
// 0x00000907 System.Int32 ZXing.Aztec.Internal.State::calculateBinaryShiftCost(ZXing.Aztec.Internal.State)
extern void State_calculateBinaryShiftCost_mB0FBF9E51F383611AEFB8FDFB52712E245CE3840 (void);
// 0x00000908 System.Void ZXing.Aztec.Internal.State::.cctor()
extern void State__cctor_m26AD9C470177A5366FAB2631B0A4F1789AA8E832 (void);
// 0x00000909 System.Void ZXing.Aztec.Internal.Token::.ctor(ZXing.Aztec.Internal.Token)
extern void Token__ctor_m307BB304EC6BC8BF180260E0F368B1A4BA3555B5 (void);
// 0x0000090A ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::get_Previous()
extern void Token_get_Previous_mF68DF3B7A53C17306BF3DD71CE7D9ECFDF7943A7 (void);
// 0x0000090B ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::add(System.Int32,System.Int32)
extern void Token_add_mF294D4F03C0ED6CB2621366E3464A0D194D6CF0C (void);
// 0x0000090C ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::addBinaryShift(System.Int32,System.Int32)
extern void Token_addBinaryShift_m9B06AF501CDCA405CC5D2FDCD11790E6D92BCB93 (void);
// 0x0000090D System.Void ZXing.Aztec.Internal.Token::appendTo(ZXing.Common.BitArray,System.Byte[])
// 0x0000090E System.Void ZXing.Aztec.Internal.Token::.cctor()
extern void Token__cctor_mCDC3233BCF4A0470E634B745D329A32E758C3485 (void);
static Il2CppMethodPointer s_methodPointers[2318] = 
{
	Base10BigInteger_set_NumberSign_m3CBEEB960CA88981C33703FBB58BF37D455B9112,
	Base10BigInteger__ctor_m2273BDB906101307D44FCCE9F52F4CE93698065B,
	Base10BigInteger__ctor_m1E816033C5D0510D1A8C59F50FA9F29049B57486,
	Base10BigInteger__ctor_m29460F91765C229C67A0ADCA792E11B169962E65,
	Base10BigInteger_Equals_mC3ADDEDC54C6F21ED5C83D80CE693A49BF13D817,
	Base10BigInteger_Equals_m1D61683BB526AE54AE5BAC5E6B230DB8D0A2242D,
	Base10BigInteger_GetHashCode_mCEA6B96D8A87B2F01BF230EF4CECE2358CB54B98,
	Base10BigInteger_ToString_m546FE1B2DCB8DC94B32B26611ECF4A0C21962469,
	Base10BigInteger_Opposite_m9627239D3B2B420630A87CB0086D1B3644FB5C03,
	Base10BigInteger_Greater_m991E62E0384D05EAA0F4720873CC0F776C3463BD,
	Base10BigInteger_GreaterOrEqual_mFAF54BF8FE925C04AC08362E2326231702202162,
	Base10BigInteger_Smaller_mDEE792A5B1CDB479264345D0BC74A96590745ABF,
	Base10BigInteger_SmallerOrEqual_m02A3F782241E20BBD27F16A3F9E6C28FC98E49C3,
	Base10BigInteger_Abs_mDAE66DB7F5058341955697D88026326A18FAA4CE,
	Base10BigInteger_Addition_mB911FF3FE09C408E7C0CEC109B9E833722DEC87A,
	Base10BigInteger_Subtraction_m9ADCFEB634BC865E5ECAE9785CF64D0327D8A8D6,
	Base10BigInteger_Multiplication_mB76DAC17641C7456EE6B1F9A72206178A2389326,
	Base10BigInteger_op_Implicit_m0685F5AC655D7C13DF0A1FE975EEF1CAF25A00D7,
	Base10BigInteger_op_Equality_m13F7E4701245AAB73FCC8CE8AA59DB5C0A04B381,
	Base10BigInteger_op_Inequality_m6F8661DF01213A719228ECD03782A5125AE89BA0,
	Base10BigInteger_op_GreaterThan_mE75DE386018D276F75E49EF0F2A150125B43FAF9,
	Base10BigInteger_op_LessThan_m96BB5D23C35202C986D40212555A807AE47582C1,
	Base10BigInteger_op_GreaterThanOrEqual_mD7D739D19B279A8B7C6B4C02071BF325B393E35E,
	Base10BigInteger_op_LessThanOrEqual_m6DCB27A0960D64EB89A2F821BEB4901A96ED47C1,
	Base10BigInteger_op_UnaryNegation_mCF591899D7E4535C8D4939FEB7200BDFAE92B486,
	Base10BigInteger_op_Addition_m1BC10FBF7D3701ABC3F4ACA195C13210C187682A,
	Base10BigInteger_op_Subtraction_mDA18AB789365062AB84967A3A1C187B24D60BC39,
	Base10BigInteger_op_Multiply_m1222CC74F9C22E5E362DCC19F879B41F527F7585,
	Base10BigInteger_op_Increment_m2822BBD11A7DE88A9C433B70C9E27C1299FC4F16,
	Base10BigInteger_op_Decrement_mD8FF66EF3FBAB883813C156F2C51F67999BBFA2A,
	Base10BigInteger_Add_m1E7A26B05EFAD56D157BF8B97A2A3AC4C1FEB6E5,
	Base10BigInteger_Subtract_m9D67EBA5C58A646EAAF3DCEC844DAD1D97B6AFB7,
	Base10BigInteger_Multiply_mE06CD7BCE17363F991E9F9690F528E27151CDEFE,
	Base10BigInteger__cctor_mD76CAC1761B36FD8B409DA0FD94D1125B5D38A4E,
	DigitContainer__ctor_mE96E0E4FD9ADD5F5504976750E8E93C09D212A05,
	DigitContainer_get_Item_m2002FDF39EE1E5CA730DC3A5D99CFA60C2681D11,
	DigitContainer_set_Item_m7BD6A5D93F25D8C7939C957D7122AD90B9E10D3C,
	BigInteger__ctor_mF7BC073C14A4A5BF155D50E331C36BA3A79E6FDA,
	BigInteger__ctor_m3A4B616C14071AFD89475CAEF3469D2F9DB29780,
	BigInteger__ctor_m4922D6DC6EFE1906B6EEC64E1025E100AF76B17C,
	BigInteger__ctor_m0F1DA20ED66B2C4C0909E39F21728D5D5A6A2F71,
	BigInteger__ctor_mCC52D4B4631B1485836B76CF4CA284477409CDC2,
	BigInteger_Equals_m593E60E9D0A83A0494C9DC3E367242EF1FBDCD76,
	BigInteger_Equals_mCABABD5A521E597E5A43AD0230463FA3E743FA5B,
	BigInteger_GetHashCode_mCB7D4CE28031AC0090F1666B13437756D81B1F8B,
	BigInteger_ToString_mC96A57AF89EF8E4D88CE5BB235FD887C44354C01,
	BigInteger_Parse_m0A604796C78F95B8C6971D89FF61DB8E2D1DF5FD,
	BigInteger_CompareTo_m3F8D479A0DDAED3839208EF468C8847234EF15D7,
	BigInteger_CompareTo_mA0D23C7174780E7A3501DE1118E778E1F46AEAC2,
	BigInteger_SizeInBinaryDigits_m6A34C73BA5239B1DEBB82FFD83E2FEB54DC42584,
	BigInteger_Opposite_mC3D6369A4C36F62603D9C77A7E9796542BE6DB46,
	BigInteger_Greater_m2631B31035E3E95B344027E7B2D37ADB79702726,
	BigInteger_GreaterOrEqual_m040D3711542E6F723D9389182AAEAB8D04F3A7B6,
	BigInteger_Smaller_m94B9E580C5825DBF94DB1699B20D90E03815A6FD,
	BigInteger_SmallerOrEqual_mFCC06F698E308A2ADCFEF55D68716D7D48E1669A,
	BigInteger_Abs_m0A83710115BA607534160F5B9578325AAEBEE626,
	BigInteger_Addition_m5BA4AF480D8A852A7E0E9F54A064F38601B78EFE,
	BigInteger_Subtraction_m70F5F79695B71209BD4F7474380DD2AFDF38EB0C,
	BigInteger_Multiplication_m304F2E0FF33954FB8E426CC17CE3CE7E4ECD1309,
	BigInteger_Division_m58107FB745BED586C69CA97BD205D61181282D0A,
	BigInteger_Modulo_m44E945B7593ED7D1EACA32BDCCFD002D77C30471,
	BigInteger_Power_mF525705FBD5DFEF89D8840E763041FA6B6CB8834,
	BigInteger_IntegerSqrt_mF4565B60C64FB6AE57A55A52B2C1BFA216B0B841,
	BigInteger_Gcd_m75759765A38289F3174DBE922113703D87A9CD3B,
	BigInteger_ExtendedEuclidGcd_mA1898B2F872FD74D51C12AC377CDF57C814FC5B6,
	BigInteger_ModularInverse_mD966998BDA1E35139D271A4DDB13579FAB3A7FC7,
	BigInteger_ModularExponentiation_m324AAB8BD416479D2920D6AA3D77D39D01166F99,
	BigInteger_op_Implicit_m92DFC46C439922D1C2169726D7D01E9937A63EE7,
	BigInteger_op_Implicit_mEE89EC87D3631AFA44476A240B5916EE9047E37B,
	BigInteger_op_Explicit_mDB4A52576F2E40D6A396EF06603547D359389884,
	BigInteger_op_Explicit_m918C4F76430F57F0589E122D1C000D7C0879F37F,
	BigInteger_op_Equality_mC64F6BC20A3586F38CDB2E5719E3CAB922DAF060,
	BigInteger_op_Inequality_mF07EE7023BB167AA89B9C5CB40C1578E33B0B3FC,
	BigInteger_op_GreaterThan_m414F57022733FC732A74EF035D2A0C7E147F808F,
	BigInteger_op_LessThan_m61607ED5FB0FA6C6BF7ADDA54151414B9F0AC719,
	BigInteger_op_GreaterThanOrEqual_m43827EA51AC5682E7377862A8F622275F35A2C9B,
	BigInteger_op_LessThanOrEqual_m5DFFECAC95C64FDF3171FF270ACFD62CA7ACBDA8,
	BigInteger_op_UnaryNegation_m6DC2B6390DED160704AC4B32ABC0BF45E6E48961,
	BigInteger_op_Addition_mF91A9CF94AF1349C601942CD1135FB7810D62DA6,
	BigInteger_op_Subtraction_m04B0E1F0677A56A57767B6ECDBD29A64DEB4788C,
	BigInteger_op_Multiply_mB31F216B74A4442DF3E7E55C96B103EA2FF66A73,
	BigInteger_op_Division_m397F1F0F1B51A04E6E79A149BF6F8928A3DB5319,
	BigInteger_op_Modulus_mE06DD76AF860B482C94EFF95173AA8FE00C734BC,
	BigInteger_op_Increment_mD47A731032CD6572BDC6A585E3AED71A610EE1AF,
	BigInteger_op_Decrement_m471BB568AB209CC3B9DC59C0CDDE0BB7B00D6BE8,
	BigInteger_Add_m488ABB1B939F38012AF9A6E085B706EB512DA538,
	BigInteger_Subtract_m570C3F53425F8673CF1A8FAF32E7054A895F7FB1,
	BigInteger_Multiply_m9F382102DC23B1B7499543BAEF29E6027530D5B0,
	BigInteger_DivideByOneDigitNumber_m6C7A70A338F34FF5F5739A4179488C868F240D50,
	BigInteger_DivideByBigNumber_m6111F0FAD547C58B23880A9F0C44E068457DDE5E,
	BigInteger_DivideByBigNumberSmaller_m1D55B72BE40B8E24E80F56FE38834D4E51E61434,
	BigInteger_Difference_m207942CDDDCC2FE903B33185A49AEEC6A10F71E1,
	BigInteger_Trial_m832DF33375318AC2C976D1B237E5DFEF41067D2D,
	BigInteger__cctor_m5C2306EC90F556C8B01A65A991CD735EFDD4AA35,
	DigitContainer__ctor_m4E1C715EFB266E166E880E9D25D7D44E1C239D49,
	DigitContainer_get_Item_m19B7181151F0D6F301C3DA5F3045D95C86C256E2,
	DigitContainer_set_Item_m7493A35A896C467B7F46E8F722E90626A11FFCED,
	BigIntegerException__ctor_mF132DC3F3DF515B8363B0EEFFBA61C6F630EEF3B,
	BarcodeReader__ctor_m3BFF43D19C843D8342FEBB9FBD933AFDC7ACAFE3,
	BarcodeReader__ctor_mEA1B103E92743F4810E2D5EBF2FEFDB4844D6FD6,
	BarcodeReader__ctor_m875C63EBB5CBE0E29CDBFC7A2CA1F621B1EC2856,
	BarcodeReader_get_CreateLuminanceSource_mEC53C67706472E1B070BBC96C9E1CE46973C9316,
	BarcodeReader_Decode_m95E2A7827061249D702871705584449403395CBA,
	BarcodeReader_DecodeMultiple_mABC648C145A9C4A70634AC36CF0DA1C7858B1DA5,
	BarcodeReader__cctor_m84BEA0972D0D5EB0B0156C6ED9915672642A86DD,
	U3CU3Ec__cctor_m6812D4B1A3BD71E61074C4B021B576D21FEB178D,
	U3CU3Ec__ctor_m44A8C0A014F3CB23B1145F46A2ADDE46E8A766DB,
	U3CU3Ec_U3C_cctorU3Eb__9_0_mB20CDC75F0A38586D237D370E782FE764E9510B3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BarcodeReaderGeneric_get_Options_mD9BF02898ABAA6D47217DF1A0E0573A9191242B2,
	BarcodeReaderGeneric_set_Options_mC29AB244E4C57779A8036F7C3B075E34603B8BC6,
	BarcodeReaderGeneric_get_Reader_m2D3499BA7F4EE99F3BBEB1ABD527CA194D308808,
	BarcodeReaderGeneric_add_ResultPointFound_m19E2B2F32FA793343AF6D8FB0E3011BAB03ECB90,
	BarcodeReaderGeneric_remove_ResultPointFound_mAB0FFAE72E90AF8CF01F52A90428FCCA3156841B,
	BarcodeReaderGeneric_add_explicitResultPointFound_m4FEC1DF4002E66FC305A16838741F61893610E55,
	BarcodeReaderGeneric_remove_explicitResultPointFound_m44C172C9EB6BE860EDA26B3CA82263E32E66705F,
	BarcodeReaderGeneric_add_ResultFound_m75CD43C0E19066A35E412B6F1AE99D312C796FE4,
	BarcodeReaderGeneric_remove_ResultFound_mA33EAA5B673FCAB3B568891AB81AF0C8F213B9C6,
	BarcodeReaderGeneric_get_AutoRotate_mE2849C96B5915AC6DE1924217E4F7E4B5A12975D,
	BarcodeReaderGeneric_set_AutoRotate_mA1394EB1D47270602573A2D3AB4C0E5CE3864413,
	BarcodeReaderGeneric_get_TryInverted_m1AC18A6A6B1E1F5EDF3C0F3675847D3A80C56F4D,
	BarcodeReaderGeneric_set_TryInverted_mED17767A8D1F3CA261F320EFA1D6B6F1DA99F26E,
	BarcodeReaderGeneric_get_CreateBinarizer_m96EE5FBB3EFBADE7BA7C2094C7307D6DFC63A1BA,
	BarcodeReaderGeneric__ctor_m9055324AD56FEE4AAEE0B58BA0D9E16FB79DA2E7,
	BarcodeReaderGeneric__ctor_mA06E10FDA65C709B91935303F5B64C0B5C6AC84A,
	BarcodeReaderGeneric_Decode_m67D479F5AB9CEBDC506698862C45F0901FC47EE6,
	BarcodeReaderGeneric_DecodeMultiple_mA6AFDC8A9E8FD6964F6B9661284EEB5CB453AFB6,
	BarcodeReaderGeneric_OnResultsFound_m0BD8C3CE10D8E42B3DC01EF9EE0F2AABC1A26DE8,
	BarcodeReaderGeneric_OnResultFound_mC7F86B8BD1DD9ED684A2A3DF498B3157200AFF1C,
	BarcodeReaderGeneric_OnResultPointFound_m8574FB86B3F8AE5F0CBE4B2557E8D87FE0C52DD7,
	BarcodeReaderGeneric_Decode_m9776ADD1C954A9BD09218116E2DAE2B911D7B152,
	BarcodeReaderGeneric_DecodeMultiple_m6554BBB3EBC00EED1ECAEC3ACF0F87744ACEE26C,
	BarcodeReaderGeneric__cctor_mB364ED63233DEA3B4C7D244D763D0442787B4987,
	BarcodeReaderGeneric_U3Cget_OptionsU3Eb__8_0_m421EC6A145647F46D16CD02C2C06B0A9DBFB6059,
	BarcodeReaderGeneric_U3Cset_OptionsU3Eb__9_0_m8F0B1A58EB5E97529342B0E2F575C5D69CF104CB,
	U3CU3Ec__cctor_m92C164218948DD06E8ABFE45468D4DA62C37075C,
	U3CU3Ec__ctor_mD3C7F7A78B1B43765D003098FA36FCC144986E7D,
	U3CU3Ec_U3C_cctorU3Eb__40_0_m61C0FD1FB8759C43E9D3B6F06F2E9E74F94D1AA1,
	U3CU3Ec_U3C_cctorU3Eb__40_1_m8C6C73B7D34623F2C975EB64EEA2335FFD6EBCE3,
	BarcodeWriter__ctor_m7E7FD1037C1CABBAF1908C061EE2721A76873721,
	BarcodeWriterPixelData__ctor_mF897BD7CBB8C08B4A6BADB0F9956E753B4A31C48,
	BarcodeWriterSvg__ctor_m733D1E0185F0C03964F17BA577B7AD742AB4FD17,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BarcodeWriterGeneric_get_Format_mF32FF0494D7F5F5030CEF380D88248ED3253BDF4,
	BarcodeWriterGeneric_set_Format_m69EE4AE9F27C4512C0191D782E1E8061076B16CF,
	BarcodeWriterGeneric_get_Options_m715D94C872B467A3E9F758B235694228A9D9887D,
	BarcodeWriterGeneric_set_Options_mAC402EE1F1CBDF8253E0170BED87AEDE7726F811,
	BarcodeWriterGeneric_get_Encoder_m9C226073013475A81FC45D47A1769800BA2D1FB1,
	BarcodeWriterGeneric_set_Encoder_mDAD625A99A1DDCC6D8528D398A2663B28117CE39,
	BarcodeWriterGeneric__ctor_m686D39585FD5CADE9A80E24B8AB4160ECCA0A755,
	BarcodeWriterGeneric__ctor_m489D4D870CBB5F2E8E61C22C903F1BBD5E6DAA19,
	BarcodeWriterGeneric_Encode_m57EB69FFF0BD8EE06699C7098546E28B5AF107C4,
	BaseLuminanceSource__ctor_m4948B3C2224D1EF73B788862AD932C11C70E3B5F,
	BaseLuminanceSource__ctor_m940CF5BFD91AA908F9449071E948DCD6FEA3436C,
	BaseLuminanceSource_getRow_mF57A4D1FF3101438BC125979DAA5DB45A57C7AA7,
	BaseLuminanceSource_get_Matrix_m0D2AD194D5474301A772C816122F029000357D81,
	BaseLuminanceSource_rotateCounterClockwise_m1FB02A981589690E07EF47C4ED3122735353373D,
	BaseLuminanceSource_rotateCounterClockwise45_mB9F94F58D5BA0569A1F693EEFC46641C3458F988,
	BaseLuminanceSource_get_RotateSupported_mBB4BFDBFDD974DA13AA2628093BDEE6889CB68F3,
	BaseLuminanceSource_crop_m2D9C767F95A74FA98B4A2DC07CC556BB270E79AB,
	BaseLuminanceSource_get_CropSupported_mB4D710560FE33F82D427A1C48D32EF8DCA8664BE,
	BaseLuminanceSource_get_InversionSupported_m6CE99DEA2CAB24D95A38F865DD78DCDE5AFA7E1D,
	BaseLuminanceSource_invert_m5FEA3F7FD45BFA682FF21D1704152033A373A6B6,
	NULL,
	Binarizer__ctor_m79D01156F2A73236AFFBA6F6198956CB3AFCC986,
	Binarizer_get_LuminanceSource_m675C074D5408ED1A04E5A251AF33DDF76C6F513B,
	NULL,
	NULL,
	NULL,
	Binarizer_get_Width_mEAC5749410E1726F30AA13CE052CE5B47B4C6A3B,
	Binarizer_get_Height_m72ACB476B8E8899C4CD01C6CEF809AC859D3DE40,
	BinaryBitmap__ctor_m24A978B33C375EEFE711F4178FCD1C0A2BBAEA24,
	BinaryBitmap__ctor_mC5E9C48ECADC45B110A07A91763CD2F5E3147FE0,
	BinaryBitmap_get_Width_mFA0AF5931276B5C64B36CEBA33786621B4C1DA84,
	BinaryBitmap_get_Height_m6632FFEE5D997D628C1D469DDD366466BAE457F0,
	BinaryBitmap_getBlackRow_mD0EFD03C83D0606EFDB3B4D8F0A3177A5C905728,
	BinaryBitmap_get_BlackMatrix_mA911AEC810AA216B50A2E796883ACF47DCA60893,
	BinaryBitmap_get_CropSupported_mA67B3824321364DBBC7E5837EA4B4337AC84014B,
	BinaryBitmap_crop_m9F755850C6482C5F353FEF39A202CE951181DAEB,
	BinaryBitmap_get_RotateSupported_m3645A172D5F6CB687725AB91DCBFA83034BDF6E4,
	BinaryBitmap_rotateCounterClockwise_m35EA73A7E7DF3C1BD87763AEAAC3C05EA0E64D38,
	BinaryBitmap_rotateCounterClockwise45_mBA30EC468FAC62DD0C1D312854E4F58340A0FAFB,
	BinaryBitmap_ToString_m5D282826FEFD8D4452C667497E9E816A7E5CFFDF,
	Dimension__ctor_m1B90F4D393D0982951AC28A9B43D4986830C4F94,
	Dimension_get_Width_m2CDBF4CC5D74D2B0CF89FF8B38A39CC93721EB20,
	Dimension_get_Height_mD28B2DE83EE6E872951D1E4A3B5D4F528E143334,
	Dimension_Equals_m28F79A32D4E174544AB9A8D5A7F56EC2A95CEAC7,
	Dimension_GetHashCode_m995F6C69F4036D066E207BA9FA859378E6FA3A71,
	Dimension_ToString_mAE6154A4BA4DCA8BA8DE152E3C8A1BEA002891B5,
	FormatException__ctor_m2299A3FD33ADF78B372EF9C915E48FF07EF46941,
	FormatException__ctor_m12BB44AE2A5E1A41727C33A562FADC9D9ABF0967,
	FormatException__ctor_m70C9CA46F36F4CF8A04A0DE3B2E9720B1EF68249,
	FormatException__ctor_m3CD02C31D612567306005AA1C017843CE94CCACA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InvertedLuminanceSource__ctor_mDEDE56B3C11E9C2EA492B554865252E16021D27D,
	InvertedLuminanceSource_getRow_mA26BFD34ED8E7F9DE79B768217F5F140852BEBCF,
	InvertedLuminanceSource_get_Matrix_mE7DADC33FA3A789BF10F063E243AFEDFE2D3A979,
	InvertedLuminanceSource_get_CropSupported_m6F3A6DD3DA05673F8DB75C67BA9B173B1DBAFB94,
	InvertedLuminanceSource_crop_mD64707A2BB43E2F475566E3093FE31B653BB1876,
	InvertedLuminanceSource_get_RotateSupported_m6D26479C4527FE26D8996A391BE844BDA5C9BFA6,
	InvertedLuminanceSource_invert_m99EB3B276DECB2A12C991A35DCCF07243D29E6D5,
	InvertedLuminanceSource_rotateCounterClockwise_m936CFE848798F29038902D49978547E21B6509EC,
	InvertedLuminanceSource_rotateCounterClockwise45_m6AA8D1089AB1591261D3F019063170982DFDD664,
	LuminanceSource__ctor_mC5B34877E69DB2093EA96E77FAB2FF17E8BA5259,
	NULL,
	NULL,
	LuminanceSource_get_Width_m84327B26782881AAEC0E6EE26BB6CBC021345A5F,
	LuminanceSource_set_Width_m062CCF31850F16C3863C756F1C2AF967ADA5336A,
	LuminanceSource_get_Height_m33DCC17920FE073B5C0B6DD08C6BEBE457032919,
	LuminanceSource_set_Height_m6B0F847CC5930324115DBB4EAD0909F5B0A9B260,
	LuminanceSource_get_CropSupported_mF8590DDAAC672FAD5C5199C86D272A3D01ADBED7,
	LuminanceSource_crop_m7A1A029A200FD45E1276F358D314192DE08934D4,
	LuminanceSource_get_RotateSupported_mA5160C3B5C063204BB3C7A3D8B44379BDE6248D6,
	LuminanceSource_rotateCounterClockwise_m31059FB64855B6BCC469B9EABE981469A7CA302D,
	LuminanceSource_rotateCounterClockwise45_m760493DC91121972B324BCAA83413EC102E38E7B,
	LuminanceSource_get_InversionSupported_mA6F920BBEC71351252B95C20A5464B9F7702C486,
	LuminanceSource_invert_m37B2CC69CE06BA09BC6851C47CF21060BB0DC955,
	LuminanceSource_ToString_mF93D29F0CFF0A30F8D58EEA2D6DEFB0875D5B8B7,
	MultiFormatReader_decode_m9B66B16BBF5D169EA2C9BE66B034A83B246CE4E7,
	MultiFormatReader_decode_m990A108ABCA2F463E7F37587B4957D64ED6F2DEE,
	MultiFormatReader_decodeWithState_m20C2C3CD5BAF03460B6A42C946CACFFD0A0B2110,
	MultiFormatReader_set_Hints_m281E68BE04E40DB50CF511F2CB10B405D3F5E133,
	MultiFormatReader_reset_mF573C78C8A05F98F3C6337DC4997E8EA7C807FD2,
	MultiFormatReader_decodeInternal_mF6D3DF95DAC8D9166579017CF6EE3677C4B0B66C,
	MultiFormatReader__ctor_m22C7456990B33553DE6937E091E153945A9524B5,
	MultiFormatWriter__cctor_m0697C5683DB8545839AD96C1F003DA4D18444D93,
	MultiFormatWriter_get_SupportedWriters_m9672B8364CE64634CB72014105F06305FA415A80,
	MultiFormatWriter_encode_m8C3B2D89242BC893C60C930D3E886423D257014C,
	MultiFormatWriter_encode_m9A0374384417F935C41E5373CA72490AF8936B34,
	MultiFormatWriter__ctor_m9E53BAE0AA700CD2EB27D857D834B9C9C612EBA6,
	U3CU3Ec__cctor_m515C3844F2D295BE3A4D6DD14A364788A6C27C2B,
	U3CU3Ec__ctor_m1113E1EAD43E52CE329EF60AF3B315DD0080D956,
	U3CU3Ec_U3C_cctorU3Eb__1_0_m5F62DA180AA9D5C7140F63F8EF64CCE5030FC461,
	U3CU3Ec_U3C_cctorU3Eb__1_1_mB7123D61F4F943F7BC6BFE6D9BA74D33E786AC3C,
	U3CU3Ec_U3C_cctorU3Eb__1_2_mBBA1CEC393E4D452E1B29BD35D563765F23499A9,
	U3CU3Ec_U3C_cctorU3Eb__1_3_m6FAED573CA72608F6BB66FFC5F6F9A8170D6877A,
	U3CU3Ec_U3C_cctorU3Eb__1_4_mA4DBE2D702DDDC34B365418E4A0B5AD160C6EAEB,
	U3CU3Ec_U3C_cctorU3Eb__1_5_m06C377090CB76FBCFF6D88B9A768C4A6343C88E8,
	U3CU3Ec_U3C_cctorU3Eb__1_6_mFE356E54E936FCD2F5ED92385FA8E5180BD16BBE,
	U3CU3Ec_U3C_cctorU3Eb__1_7_m3D48593CB730558EA8D89BE4A62AF56DFE75BEBC,
	U3CU3Ec_U3C_cctorU3Eb__1_8_mBA430395DF4E02A5255F5819BB58A1DF31918039,
	U3CU3Ec_U3C_cctorU3Eb__1_9_mA0B728176F45510D7610771AFAB44696523F726D,
	U3CU3Ec_U3C_cctorU3Eb__1_10_m82B89598C6D1236CF09943726EDE770F57508EAC,
	U3CU3Ec_U3C_cctorU3Eb__1_11_m74A8CA6CC5DF3C8CE0249607F5022E9DE6537CC3,
	U3CU3Ec_U3C_cctorU3Eb__1_12_mD1FD14DD31D34B4FC7D32B6A6F9934D64EF060F6,
	U3CU3Ec_U3C_cctorU3Eb__1_13_mF6431123BA8681AA8F02D0345BC73B5244611C24,
	U3CU3Ec_U3C_cctorU3Eb__1_14_m4798768D38463B839ED1F294E8A6AD41D6311CB2,
	PlanarYUVLuminanceSource__ctor_m370700CA786C78CD59237D28BC9C4C45DE97D768,
	PlanarYUVLuminanceSource__ctor_mB258DD3B0DB01184F19D395836B3D7D6B8257378,
	PlanarYUVLuminanceSource_getRow_mFCFC814FC46D7161699208F0B537AFE53731CBB6,
	PlanarYUVLuminanceSource_get_Matrix_m7F3557B5CA3E4254BCED3A8598D4B52472AB0062,
	PlanarYUVLuminanceSource_get_CropSupported_mBF9F3E3AB801BFE2BCA42E03A9EF754AB151A0C7,
	PlanarYUVLuminanceSource_crop_mB661406A7B13ED56501696FA0FE0D33954C5E991,
	PlanarYUVLuminanceSource_renderThumbnail_mAE867636A187F9F0E36E7D2D6C332CE328D8EBD6,
	PlanarYUVLuminanceSource_get_ThumbnailWidth_m48AD72588131430A5514BC8D0A84DCD2C8FF8A75,
	PlanarYUVLuminanceSource_get_ThumbnailHeight_m7C995F5DEFF9C40EE9794970AE86B879D8321C9E,
	PlanarYUVLuminanceSource_reverseHorizontal_mCD165AF8C02720DB0935FDC943D1EFF52FD75ADB,
	PlanarYUVLuminanceSource_CreateLuminanceSource_mE2D061D9E85D8D00A0A534844714A85936617523,
	NULL,
	NULL,
	NULL,
	ReaderException__ctor_mE42867457E0429A7A295C947AF882E4F2EEABEA4,
	ReaderException__ctor_mBBF97FA1D52C173F000C225F7A3519469E0CE56F,
	ReaderException__ctor_m996848175E1DBF82EFC27D5ED8EE65669798DDF3,
	ReaderException__ctor_m7F3F950E443CF1B903CF9A325B8B9EB61F2C78AF,
	Result_get_Text_mEAE1D660EEF3788AA89F312D7D7AD0E94BB70D66,
	Result_set_Text_m57B698D4925803C2DE47487571BBECE9708A645F,
	Result_get_RawBytes_m3AA4BB3EB5D378E63BE07AA1C8ADAE508079CEE7,
	Result_set_RawBytes_mAEC8114897C03331968B1445B0EA60B91947A41F,
	Result_get_ResultPoints_mA678D102FD830C51E332C355DA5004D4072DE8C4,
	Result_set_ResultPoints_mBB21E4578841276156438C4765BED52DCB0ED648,
	Result_get_BarcodeFormat_m69C929547E7704672F93EA848162EA432BB4CD08,
	Result_set_BarcodeFormat_mCC1690B0810F0233A339DE2FC05BC3119B4F0223,
	Result_get_ResultMetadata_m0D09D838B9025B8BA001433A6035EB9D5D717A4F,
	Result_set_ResultMetadata_m14E1B6A9768815184132D2EA521BDD8670BD0333,
	Result_get_Timestamp_m796F9523169BBE842086F9D67119B717AAAA6F48,
	Result_set_Timestamp_m998601E38F9A53D803D0247099A66B7ADDFCDE1E,
	Result_get_NumBits_mFB09449F2BF146ADF0EED87DE0339289AEBAEF85,
	Result_set_NumBits_mCA75BDE5F42932D0FCF842C1BD2C20F2B6BB2CFE,
	Result__ctor_m1E14F77F240EC3A52D1AEC392FB262EE148AC711,
	Result__ctor_m7E17E9F04AA01FD00884E74FB0E57C42B5D24025,
	Result__ctor_mC9223509BE8427A7DC2F4AFA391470FEBF8F36F2,
	Result__ctor_mF7A88874108B5DB5DBF378CCB139D1AB4ED828B7,
	Result_putMetadata_mB20B623DCEEC41116D938EC71C16DECF4B599B0D,
	Result_putAllMetadata_m02E5A99E31170EBEE1319DD5536577CD81E0A582,
	Result_addResultPoints_mA8A8E3DC1BDA2F05C4B5DB5A13B51B9AA01CF132,
	Result_ToString_mCB80B9FE72A5F1F1299238BDAD9F79B4D5EF586D,
	ResultPoint__ctor_m79F46362C8325C7AE4D7F123CD3A0A4CAF79330D,
	ResultPoint__ctor_mA455E826856C2C0705F54F02FAF007E9459DAE79,
	ResultPoint_get_X_m11301DCBBA25E7B6BC968B991988AE9A1C33FAD4,
	ResultPoint_get_Y_m1628B0C2874884F4981174FC694348C1C1785243,
	ResultPoint_Equals_m6D30D902F0D3720FF78334908DDA6B0598FC5E5C,
	ResultPoint_GetHashCode_m6C964DF85CE64D06EF4D74E4728CAF117AA61F52,
	ResultPoint_ToString_m047E6F1C7CAD358AD67F4D429073B14038FFFFD7,
	ResultPoint_orderBestPatterns_m4FD0B61781E4A2FD3FA7B832F34641EA85D08373,
	ResultPoint_distance_mACAD8E6DD21BD6059EC7D836B68B8273DE9BE22C,
	ResultPoint_crossProductZ_m1A1C09FE1521F24A29DE80B4EF4257243CF33840,
	ResultPointCallback__ctor_mAE7B4A4417E2D1CB6D91BB05B5ECC863017BB1C5,
	ResultPointCallback_Invoke_m7DAE0E818EB77473A75096BD148F63A218CA04A3,
	ResultPointCallback_BeginInvoke_m25BF2A89E01347344DC9CFFE8652E182B23EA2B3,
	ResultPointCallback_EndInvoke_m00BB9E46B22BE030A86EE45703C17759705C2E43,
	RGBLuminanceSource__ctor_m51CA2D10F091552757973B45E36F13590BDF0BC9,
	RGBLuminanceSource__ctor_m92099E26A903D8C8F574A1E3E24F8D44BE316F8A,
	RGBLuminanceSource__ctor_m3BEE113C47BAB2C5F432D656EE8235EA65A0C8C1,
	RGBLuminanceSource__ctor_mC49758F8C48164277A9F2C3E2F5633F0FC18B556,
	RGBLuminanceSource_CreateLuminanceSource_m5B1EFEBDB768B542A80269B11DCEE69412CB2132,
	RGBLuminanceSource_DetermineBitmapFormat_m2F09D25A7DE54E59227B813A1F9B46D49B2469AF,
	RGBLuminanceSource_CalculateLuminance_mC58CD7E48A8275962F37D3B6F0597A150A26891A,
	RGBLuminanceSource_CalculateLuminanceRGB565_mE4787D83B47CBE19EABC7789C3BF9E9BBC03C38A,
	RGBLuminanceSource_CalculateLuminanceRGB24_mDAC50668CB229AACD5DA5336CF5839FDAD79C019,
	RGBLuminanceSource_CalculateLuminanceBGR24_mBB4424D4580B9E0A087A57AF7A06167D7C7CEB11,
	RGBLuminanceSource_CalculateLuminanceRGB32_m5FDC80197850993728E43D106CA497A4A6151C06,
	RGBLuminanceSource_CalculateLuminanceBGR32_m545AC2ABC4ADFE5BFEA926F5F33A9D0C302BF82D,
	RGBLuminanceSource_CalculateLuminanceBGRA32_m7C1E312BFF6ABB3391B2B5B4DF5DC5E64FFC5A48,
	RGBLuminanceSource_CalculateLuminanceRGBA32_mE7ACE0463E9204B4A2BF96BBA8C8FFC8550982A6,
	RGBLuminanceSource_CalculateLuminanceARGB32_m8DC3EC9E02E41506476F0592691E565F69B3D2D5,
	RGBLuminanceSource_CalculateLuminanceUYVY_m4D7971B512BC85BF862A56F52CF7A24839F97444,
	RGBLuminanceSource_CalculateLuminanceYUYV_m2D1C404C1AE1E6223B148BF52C52201BD1A2B35E,
	RGBLuminanceSource_CalculateLuminanceGray16_m17BF7897E7F9F2654C696E2E4449F813BFB3FBD4,
	SupportClass_GetCharsFromString_m426BDB3F99FE11711DB087125D43E86566FFCBC0,
	NULL,
	SupportClass_toStringArray_m08D2FB8EEE0D5F1ED85245282AA33A07E3BCDDFA,
	NULL,
	NULL,
	NULL,
	SupportClass_ToBinaryString_mCB5BBF13C75F4ACAEB0506A2E7D69DA42F9A20C8,
	SupportClass_bitCount_m21473D5D1DA67A40A5F24674791A233A63788E8A,
	NULL,
	Color32LuminanceSource__ctor_m97737C90E0BE9EC855EB71BC8B5DBEBA04B01288,
	Color32LuminanceSource__ctor_mEC35DFEB9A578281FE68158040DB4B31F7C40A24,
	Color32LuminanceSource_SetPixels_mA89C21BBC7D2815DEDCDAB0E600187B4D3184229,
	Color32LuminanceSource_CreateLuminanceSource_m65C7C5A82776457193130328F2685061E01F4C40,
	Color32Renderer_get_Foreground_m74E1B20041CCCD76F715171DFEC058BF45531150,
	Color32Renderer_set_Foreground_m9225E2481B5DF15FBDABFD3A5F2B8D7FA0BA2C36,
	Color32Renderer_get_Background_mAB232887582459E31BDD62429EFB69CD960FDDDA,
	Color32Renderer_set_Background_m21CB6390A36918D1F3025B1EED887EEAE004807F,
	Color32Renderer__ctor_mC5A13A842C980C6B82C98E46A03B99193474DED3,
	Color32Renderer_Render_m351C5D62D12FCC130C012CA52F64E7DA04B82094,
	Color32Renderer_Render_mD07481C8167DE11C1FDF6F1942B81F29E4C50E43,
	NULL,
	NULL,
	WriterException__ctor_mFC6879418452293BFBE57C7AA829AC46365E7611,
	WriterException__ctor_mD352B904167FB22C64B85ACF91DF020B8267EF30,
	WriterException__ctor_mEC7412EB2A678F72EB5CEB68B484006807A62C03,
	NULL,
	NULL,
	PixelData_ToColor32_mCB6EF3582563538106EAD34C85351F07D0E03D25,
	PixelData__ctor_m698C4C322EA5FE08465693CBDBE97AF20865453E,
	PixelData_get_Pixels_m6C6C44024CE81793733E7C73BD28FDB075CFA1FD,
	PixelData_set_Pixels_m64F136781F0B4786BA25DC8C8215D6CDA69CB68C,
	PixelData_get_Width_mE0FC5755D330A797E7BCE338D7A2C0EB243353DF,
	PixelData_set_Width_m24A7249700CA98A02482A4040E2C6DDBB840A288,
	PixelData_get_Height_mDF58650306B5EF68901AD2EAE1ABBF3A67C52B4B,
	PixelData_set_Height_mC73A24A10B1DCFFE2C3943BED3E893C8D80819DF,
	PixelDataRenderer_get_Foreground_mB2D6865AB8A1AB7508DA28DE0E9EA0357369F1C5,
	PixelDataRenderer_set_Foreground_mF7894FD354D7A4C99A19DBC6C9CA7843E20EAE14,
	PixelDataRenderer_get_Background_mBF55401F10C4DAA22A585FB25569F32B2915B74F,
	PixelDataRenderer_set_Background_mF702170EBCA864367767EB0891D6B1752EAFDC89,
	PixelDataRenderer__ctor_m8C1845EC562DD8A5F1DEC899B3B7E0FC4A53A69B,
	PixelDataRenderer_Render_m9CC2948427EAFDBDF578FE78905937CC104BE8D9,
	PixelDataRenderer_Render_mD61F5FB224B8D818C8CB60B829A51F8E7D8B12E1,
	SvgRenderer_get_Foreground_m312A2472FE39F6044EAC40DC3B6326FC0917219E,
	SvgRenderer_set_Foreground_m024CE081587D1B60F003EC990256D06B38B97B1F,
	SvgRenderer_get_Background_m2D66426AD1C9FC6C654C617661EB24CE93ED7D77,
	SvgRenderer_set_Background_mEE254344A04EF967BD67F388E684D269B975E664,
	SvgRenderer_get_FontName_m677A2FEADB0F12C8B52BF98B1991723A6A136CD8,
	SvgRenderer_set_FontName_m4A434249869E31E2807CBA42A2193B9DD8CA864A,
	SvgRenderer_get_FontSize_mB38E7688F85B9AFBCBEDE3BEFC44D27D08EF3DDB,
	SvgRenderer_set_FontSize_mB1197775E856C8A63B7C5938B5E1F84632DB2EB6,
	SvgRenderer__ctor_mAAD5A231E7D7487B3EC3B74182D1508EFCE1ACAF,
	SvgRenderer_Render_m877711F5255B0A28F77F34FBAA5D94057632F8C9,
	SvgRenderer_Render_m8A425E3542A0A3D853C15812BDD07C2CDC9C54AC,
	SvgRenderer_Create_mC593A01CA3EE86F1B878681E721E7018509D2C9E,
	SvgRenderer_ModifyContentDependingOnBarcodeFormat_m7B6DD3974BA4817D83F3B444B252687766C81665,
	SvgRenderer_AppendDarkCell_mA93AD3C3F42F338D1A1B207E9EAA99CFF8C2A789,
	SvgRenderer_FindMaximumRectangle_m7199F76561F59CCB28F3A7BB6BECD294392E4FAE,
	SvgImage_get_Content_mEBAF6604E3C9E899D96750356C36FF9A0D012FB9,
	SvgImage_set_Content_m37C27169A98EC442269FE296F2A66D4747946288,
	SvgImage_get_Height_m4A4C1EFE7FB4225C26E9E790DBC9B30E29775008,
	SvgImage_set_Height_m917C5B6E223C02A68FFBB3E31A12981D226DBCB8,
	SvgImage_get_Width_m89DA3BCF8ABB02CB5E9E0ACB26A6BDE7634AF15B,
	SvgImage_set_Width_m62DA718D5E8F51D5ACC6924E3D5BAF506E03A122,
	SvgImage__ctor_m607C867740BBB03520D59A9AD9AE52724A7DBD19,
	SvgImage__ctor_mA1F5BBA10752AE10EA738E1E3CAA75EC4A03C466,
	SvgImage__ctor_m9373E58CE93221AB26051B448FF64EAA3C900B06,
	SvgImage_ToString_m3E153761F4573411F389B46FCF7A35877846DF24,
	SvgImage_AddHeader_mD39A343469014FDCC5E0A82AF0697F7FD499BADD,
	SvgImage_AddEnd_m7638A2A8011A6E30E2FE8991C306B450DA0274CA,
	SvgImage_AddTag_mD330B99C47734C5501990D2A58BADAC5955E9238,
	SvgImage_AddText_m2F7E6C50BB674D20325CD52749A728B17C95725D,
	SvgImage_AddRec_mA7FF92AFAF64CCF404A7BA5AEC058F1E4EA6FFA8,
	SvgImage_ConvertAlpha_mB1AFDD4295A8A95C45BA550311E54A62B146CCB5,
	SvgImage_GetBackgroundStyle_mA52CFF48E56A77F72045F7B4D73BC43A65D3C69B,
	SvgImage_GetColorRgb_mFFB8E32730DDC3A6E34838D054A4B53C9C895716,
	SvgImage_GetColorRgba_m8F7A7CCC4325FEBF66F48800C747D23E6E27336E,
	QrCodeEncodingOptions_get_ErrorCorrection_mEF81A8B3681653749276CC482737E557EFC74CD0,
	QrCodeEncodingOptions_set_ErrorCorrection_m6B92CA1620CCEEE5242720D102F33BF3232D620A,
	QrCodeEncodingOptions_get_CharacterSet_m90CD3FC6325B7C5B1E01C0DC1E344F1DD33E1BA3,
	QrCodeEncodingOptions_set_CharacterSet_m1970C5095AE0B2235A2790910F286BAC6F701B99,
	QrCodeEncodingOptions_get_DisableECI_m6D1B561F53C2883207C70FC029D0983CFBE302FD,
	QrCodeEncodingOptions_set_DisableECI_m1FA72A2F6BB60211C6EB1C776AD173C38DE6A854,
	QrCodeEncodingOptions_get_QrVersion_m5F9631B670EACDF6F34C16C05260CE4AFD424CC7,
	QrCodeEncodingOptions_set_QrVersion_m09FBBB137AD5684E6C76AD3DC9743B64E0B7BB7C,
	QrCodeEncodingOptions__ctor_mC5EF7BCD9427BE7EFAECF143D51D72A6D487A199,
	QRCodeReader_getDecoder_mFE6BFE5ADACFF7573EC009995E5915519ABBD049,
	QRCodeReader_decode_m24DEA81B154867B51E2E0CF0714DC01E44D6835D,
	QRCodeReader_decode_m282FC933773D428FB2AD20E41DA218EEA7456914,
	QRCodeReader_reset_mCC520C963D9370429051F693F1BE92A1765E3098,
	QRCodeReader_extractPureBits_mFDDDACD1F15811EF4AA40FC96B151FA5BACAE44A,
	QRCodeReader_moduleSize_m07CA733B2B9832022773BDF944358FB294EE0F61,
	QRCodeReader__ctor_m1EC9E46F30B90F1EF2FC21A74CE8520DA13433E3,
	QRCodeReader__cctor_mF81281A4A5216AC0CC4D4AC1A5CDBEE8E1B7CF31,
	QRCodeWriter_encode_mAD1D48443F04C98087A57085AE06312BDC50810D,
	QRCodeWriter_encode_m1BFFB91DC50E617B19BA3E5043C3E0E185EDFCCD,
	QRCodeWriter_renderResult_mB0748A5E248F44C3687E4E735A9C2D6140E3C186,
	QRCodeWriter__ctor_mD3B4389B0968B1E371BFA71E1C175D9CB01C677E,
	BitMatrixParser_createBitMatrixParser_m0C35E4E2699B59EF14FD4B6CDDA39596E22932FD,
	BitMatrixParser__ctor_mD44AE281FD8928634F8A8902216F43D6C7C9A166,
	BitMatrixParser_readFormatInformation_m2FBD0758585E8AB776DE313A265265B4D67C7C1E,
	BitMatrixParser_readVersion_mFE6EBB447E4E73ED7A33BB0B3AAE0E07ACB10C66,
	BitMatrixParser_copyBit_mE5B3281BA1E762825467ADDD4F9935EAA4573DDD,
	BitMatrixParser_readCodewords_m10235A7384448A064F57D8B085D1367B1B9667DB,
	BitMatrixParser_remask_m02ECE3CAE197FF831DAA57B8BE93D62B788984F7,
	BitMatrixParser_setMirror_m5A572BEF2347709247EBD6D4C20FA1093FEEB1FB,
	BitMatrixParser_mirror_m2627CE903118AD1D50668EE09E44ED7EA65E8BE5,
	DataBlock__ctor_m417EBE9EAF769C833FD469357B2B990DF06F038B,
	DataBlock_getDataBlocks_m09C2CB0E9E517D14A75A4018280276A8A4B635F5,
	DataBlock_get_NumDataCodewords_mDA2B9378F5878724714B5FDB555797255A61EE54,
	DataBlock_get_Codewords_m893B7358A589634CBF04810E94DF42BBC2ABA6FA,
	DataMask_unmaskBitMatrix_m6BDE78A99AB905FD832A0E4A226C23D401C0AE00,
	DataMask__cctor_mE44E06C960D185CE302E72CE983FC6A3689A6C5A,
	U3CU3Ec__cctor_mA0B6321A2B6A36815A86724DE41665FEAA1ABABD,
	U3CU3Ec__ctor_m556702C7E95C4F77530429D5BA643B8802C72D36,
	U3CU3Ec_U3C_cctorU3Eb__2_0_mFBCF966E297BA04C73BA8C517CA6926121786CEC,
	U3CU3Ec_U3C_cctorU3Eb__2_1_m4AB5E7BE0BDE8C54A771F326D7C00277241C0F04,
	U3CU3Ec_U3C_cctorU3Eb__2_2_m19FE764F48328392BE3BE9EAA653F1EC84C9BD60,
	U3CU3Ec_U3C_cctorU3Eb__2_3_mB37B2A94BDE52A330E0778A96B671B464A7DB6B1,
	U3CU3Ec_U3C_cctorU3Eb__2_4_m7813575439FDF5AAC2BF3AA4F25834506B326038,
	U3CU3Ec_U3C_cctorU3Eb__2_5_m9E7BDD970B53ED12F4FDC26C09A119815FC3CBBA,
	U3CU3Ec_U3C_cctorU3Eb__2_6_m7A92D2F9CCD2E141967ABD8E652D8BC78E0548EA,
	U3CU3Ec_U3C_cctorU3Eb__2_7_mDE1A7A2D9550E4A755FBC7B6B5B186A6283DFD74,
	DecodedBitStreamParser_decode_m7466DA93B0B4DA2BF16811DB96DC10D887F13E0B,
	DecodedBitStreamParser_decodeHanziSegment_mBBF7C0A4BBF1080F52B87297BE2E1162EDA05B83,
	DecodedBitStreamParser_decodeKanjiSegment_mCCBB64E1AC00744605EEFB90D7071A65B146540E,
	DecodedBitStreamParser_decodeByteSegment_m18FECF2FC312D5AE766D5940C8C4307708B4F8DD,
	DecodedBitStreamParser_toAlphaNumericChar_mCC688581B9FDE61C9C24216BAFF687A0878A4695,
	DecodedBitStreamParser_decodeAlphanumericSegment_m5BCF1F3FFE802DAE2552B833104F6D5083FBD61C,
	DecodedBitStreamParser_decodeNumericSegment_mB6A7AD95007BE58FFC3F487A810C2685703A85CB,
	DecodedBitStreamParser_parseECIValue_mF160C40D8B3FD522E2FAEAF60D058B4D3C9EE184,
	DecodedBitStreamParser__cctor_mACDF5C4B85C83F30294824652F16BEF436CFBC67,
	Decoder__ctor_m16ADDFAE3DDE51F59CC33BD4BAC8B093B59A8D3C,
	Decoder_decode_m81B7FE4F44CCBD41F83412B490CB84CDAB56A9C1,
	Decoder_decode_mD7898B72ADCA02F9706EEE378187EB899AC7ADAE,
	Decoder_decode_m3616EE0B80C3B5C2BD2DAD4D3863EAB51C75949D,
	Decoder_correctErrors_m380591C25A6BD9296CC9BFCFA3E1C43BD72FB3EB,
	ErrorCorrectionLevel__ctor_m71B8348BA6486EE615792F076F53F8DAF501D2F6,
	ErrorCorrectionLevel_get_Bits_m7765C510F012BE59471A5958DD5FDF978E1045A2,
	ErrorCorrectionLevel_get_Name_m6FD65C9C806F63D8295F9A575264D270FA646812,
	ErrorCorrectionLevel_ordinal_m340F3DD63E0FB50C6482A072868F5D4BA2D01003,
	ErrorCorrectionLevel_ToString_m77247B26B89A369067873DD008E7B6E951894D79,
	ErrorCorrectionLevel_forBits_m190CF75EDFAAD1DC6CA65F5A57984FC56CC0FD13,
	ErrorCorrectionLevel__cctor_m9BD213C961105B66D7E70697753295465042CD27,
	FormatInformation__ctor_m705630BBE9CD33F82EC54B6C82B5C41EAE4BCEC1,
	FormatInformation_numBitsDiffering_m03E1BAAF42556E828A2D610B28D6197C9BE066AC,
	FormatInformation_decodeFormatInformation_m0776EB2551C6A1A0FFE8CAAD3C4BB1393F6629DF,
	FormatInformation_doDecodeFormatInformation_m79CE9CADF3D3D0AB56932CFAE9020519FDA95F2A,
	FormatInformation_get_ErrorCorrectionLevel_m2C82275BE86395DED1EEF3CFD724BC170DA93B36,
	FormatInformation_get_DataMask_mF4640F81E0039269CAC251D7064E3CB13EDEA0F4,
	FormatInformation_GetHashCode_m064DB3EBA0C409DCEF4A245863879D16CC020D0B,
	FormatInformation_Equals_m2462A5E3B9F6FE5EEEABFF52B77EB38867CECD7A,
	FormatInformation__cctor_m7A04DB3D7D75E1914D26CBCBDEE7842CB55FE21F,
	Mode_get_Name_m8589A71CD40010EB376CF548C5E2A68B81ADE220,
	Mode_set_Name_mA322F9B2A69E018DA2C0C00728951DAD9C1B0AA5,
	Mode__ctor_mD5D384B0689300761D261562B95FDE1E9AE05F9B,
	Mode_forBits_m5733DD8E754E6E625B86A864D22C465A66A023C9,
	Mode_getCharacterCountBits_m0CF7362524C53F2B1030643EFFFDEC312D91E586,
	Mode_get_Bits_m7D132DB5475D33B0684FF74556DED9ED217078E8,
	Mode_set_Bits_m2D38555F80B0E2B3DCD0850233B3E3F549620942,
	Mode_ToString_m5F213C2613769D061B85CF780ED6E653E0944BD5,
	Mode__cctor_mE5C404377B748A0E467F7EEFDB572CF6A7C8C414,
	QRCodeDecoderMetaData__ctor_mFE0CD9D642985F21F08437774A0442973A4B0B78,
	QRCodeDecoderMetaData_get_IsMirrored_m7A00F5F719BF1D172FAFB00277C1DB3B49AA4A84,
	QRCodeDecoderMetaData_applyMirroredCorrection_m41BC45AAD2B641CDC73D998CAA5741ACE6B5B348,
	Version__ctor_m01721E975A63B7874D29F6E7AD23733118FD1200,
	Version_get_VersionNumber_mB6347F35E8C2A8F49282ABBB8D43A233A8D8C8FD,
	Version_get_AlignmentPatternCenters_m2BB7540E510C292C8ECC115CB3082763D9A2946C,
	Version_get_TotalCodewords_m0F5246C6C80C4274A970BB18F39C8C8DEA3877C6,
	Version_get_DimensionForVersion_m737D1DAB2ECADCF500A4A840912554F320DFC319,
	Version_getECBlocksForLevel_m1377C6C059A2F33E6055DCAE5FBF35D5FAE0BFD4,
	Version_getProvisionalVersionForDimension_m2A81BC5D0CF18D93D2518DAB6FC2270D2AE7FAB3,
	Version_getVersionForNumber_m09318039C3DF1A3108442E66639DA7871ED34D4A,
	Version_decodeVersionInformation_mFCF49CCEE179EADEC292876CD7FAF9A437A3A0FC,
	Version_buildFunctionPattern_m4201B1377FD8F4AF63AF2C60BADBC0854BB6E2BD,
	Version_ToString_m034F18F2F0993A74ADC1C9C8E9673FF68806DE26,
	Version_buildVersions_m88A9C36BF7ACC745F662B4E07913D34168EBFDF5,
	Version__cctor_m4DD86CFA355EC2ED3663016E34576E12C7667CA4,
	ECBlocks__ctor_m8A1F8D46EBC500F090667507F9C128CC84CEA84F,
	ECBlocks_get_ECCodewordsPerBlock_m40304ECA66F4757EB3C8CD9876C1BB81186816A5,
	ECBlocks_get_NumBlocks_m08D47F57DE75E8A8571809E3843EB5837F27D110,
	ECBlocks_get_TotalECCodewords_m9D1B9D8AE087E26CAD4945547757B61F97858326,
	ECBlocks_getECBlocks_mE3D4CB94FE7220D415BCE709BDA30D783BBAABC4,
	ECB__ctor_mA6F6B9D5ED3E489AEE67B99C06273667CFCD35A9,
	ECB_get_Count_mD1AFECFCF9690ED0A538935236B3F40650628746,
	ECB_get_DataCodewords_mC411D76846C30483ED5244B759AD405DF6CDDA02,
	AlignmentPattern__ctor_m7CE66A97BBA514C435318F43E8E15C5219152C7C,
	AlignmentPattern_aboutEquals_m729A633AEBF05B27CB4ECABBCABD1CD064E859C3,
	AlignmentPattern_combineEstimate_m93E40979572ACEB6EDE6E9C91D482ED21B5E9788,
	AlignmentPatternFinder__ctor_m8CD8E694DBBD61D6BBD9E40DF10644BEA4B70741,
	AlignmentPatternFinder_find_mC8C35A13562E9C40DB2367DBC2C3EF2B353B0FFC,
	AlignmentPatternFinder_centerFromEnd_mAB3FB3393A67FDCEAE1DFE0FFBDA5034236366DB,
	AlignmentPatternFinder_foundPatternCross_m0628F63F67F5FFE83A62D81DA0C69796E71E48F7,
	AlignmentPatternFinder_crossCheckVertical_mA29FA61B6FC8C2238BBAB3B5F65560889F73256B,
	AlignmentPatternFinder_handlePossibleCenter_m182FFCEC7642FDAED0135C9DC414AD23A5BB8FC6,
	Detector__ctor_m8E2D1BEAA7A021F5AC78FE188CA0A7072CDE1972,
	Detector_get_Image_mAEB5ED2E17DC8BCFF962A37FB3E93344A8025E6C,
	Detector_get_ResultPointCallback_mDEB29B0A5D00F575C8E9DF6B2AED795744E85CE1,
	Detector_detect_m48373A9D7AEBA18012FEC37EE755F9216BD8FE9B,
	Detector_detect_mF4B2B8E970BA235B2984116F56960F2A35E03214,
	Detector_processFinderPatternInfo_mC2780BE3A2CE5A3514F31BBF780151252D07F9F0,
	Detector_createTransform_m98985C87F283E6B9B8C529420BA2131348795277,
	Detector_sampleGrid_m65F3282647A51158C6F1B430D0DDA1478B78A1B6,
	Detector_computeDimension_mFBECCCC18E55FDB4B648176F29F8CE9F2BD5BBE8,
	Detector_calculateModuleSize_mCD9395157D1216CF81071C0AF137B7C056EDD522,
	Detector_calculateModuleSizeOneWay_mAB8A5292B6D2F6BE02C3CFF67CDA73732B955905,
	Detector_sizeOfBlackWhiteBlackRunBothWays_m2018D14F79014C77B491D1C649C9AB523B87CE95,
	Detector_sizeOfBlackWhiteBlackRun_m8393D64FBFA03DDA266FE28D6786DA81F46A5120,
	Detector_findAlignmentInRegion_m43D0A6C3B53CDB8FCAB8D19014CD54D224CB2156,
	FinderPattern__ctor_m1CC6D872C8006BF44F64AC1BE2350849109CC2C2,
	FinderPattern__ctor_m772EFBC6648743232AA622178CE9E4EF5757DA38,
	FinderPattern_get_EstimatedModuleSize_m1B0E82FB5A544C487124C7FA0EE786C312F98296,
	FinderPattern_get_Count_m3CACE523C00E436C3B064F4B93968FE059E631C4,
	FinderPattern_aboutEquals_mDF31E9A495CCCFF59AD738EC081863442077CC38,
	FinderPattern_combineEstimate_m43F2BBD287E591DAE94643E74C36D3B182C57809,
	FinderPatternFinder__ctor_m527030167D37D090043073D26C1FE7D07A9E0CF7,
	FinderPatternFinder__ctor_m5DD3CF355405AA15A0A247D7CBF7477E018C9FFF,
	FinderPatternFinder_get_Image_mD42C2619D374FA00B93178930BCF7038E4B7FD74,
	FinderPatternFinder_get_PossibleCenters_m966A7A1B015D250A168B101543ED66B54BF728B5,
	FinderPatternFinder_find_mE06B229CC5034B374536A100FDB13B03C044595A,
	FinderPatternFinder_centerFromEnd_mDA32205C1A160D4A35801ADAE541FC342285A08B,
	FinderPatternFinder_foundPatternCross_mB3A8133B9E721CFA7A338ECE0D8C0152EFD6D537,
	FinderPatternFinder_foundPatternDiagonal_m27AC1C6DD1655BB29D1DFAFA085EB1C52C488D5A,
	FinderPatternFinder_get_CrossCheckStateCount_mB873931F7B0F45CDE0FFB328FC0C5447918B2DB8,
	FinderPatternFinder_clearCounts_m3AFC88F8CFE32C39CE9F29AACA67E5DB40CF9EDC,
	FinderPatternFinder_shiftCounts2_mD3FBE67518360AE63932A0A5DD0ADE30F784D078,
	FinderPatternFinder_crossCheckDiagonal_m3DA4A839D4D19B6640BFBCE2DB4855D8FF35F5A6,
	FinderPatternFinder_crossCheckVertical_mE14E0C18420DC270342C824845A17CB55D77E7E4,
	FinderPatternFinder_crossCheckHorizontal_mE8A73C3349AFEAD0FE46B0D56B0E7100CCD17157,
	FinderPatternFinder_handlePossibleCenter_m0AFF4756351D688FFFBC1E9F9E121D235DC4D313,
	FinderPatternFinder_handlePossibleCenter_mFB68686FC8F78FF30E7860004B05925B5C422172,
	FinderPatternFinder_findRowSkip_m1DF9BDB418139E949BC8F00E5541F900A0B2616A,
	FinderPatternFinder_haveMultiplyConfirmedCenters_m553C3186A4248066A58A46D4F3F44E3082BF4325,
	FinderPatternFinder_squaredDistance_m3C8EA70986326F3477E40CAAB604AB41A13F0957,
	FinderPatternFinder_selectBestPatterns_m5089DD3AB6B63A94B0B33AABC3B656134A5BB54C,
	FinderPatternFinder__cctor_m278B05EE253660E72B1036B7613C3F4AD9D9A85D,
	EstimatedModuleComparator_Compare_m88757B47B409641AB1B40044699EC0B954EDF610,
	EstimatedModuleComparator__ctor_m937FB610C02C84DAE4727ED5C9BEDBFDE7762BE9,
	FinderPatternInfo__ctor_mBA50C87571AD4D6F9E2C710C96D80F91D4EAF7E8,
	FinderPatternInfo_get_BottomLeft_mCEBF02A4BBD4834A6BC9D36B6E93AFCF23186833,
	FinderPatternInfo_get_TopLeft_mD5214011F1E4736720CE9C4F1D1DAA79E3239B2B,
	FinderPatternInfo_get_TopRight_m62A70525270F592E71086352F83903C3EE7F8D64,
	BlockPair__ctor_m93FF79F4EDFA331594D3412886A6F2B334438999,
	BlockPair_get_DataBytes_mD344E280745399520B450B2BBD3F57FEDA15FBDB,
	BlockPair_get_ErrorCorrectionBytes_m7DC12B63785AACA63D6FC92FF85E21B685EFA9E9,
	ByteMatrix__ctor_m5BF5D0D2EF8A1CFE1F8D2CDF0B5027450DFBDA86,
	ByteMatrix_get_Height_m0BBB2D4EE0A0E8AA14FBF6B96BC56DB623A99B87,
	ByteMatrix_get_Width_m3A2778DEC4FC41F240C99517D777011D30C829D2,
	ByteMatrix_get_Item_m52DDC8DFBA6DF2647F55C8F0A9454C4D8914C5D0,
	ByteMatrix_set_Item_m0E0BCEAC4814389F016510426A994611EFD21E86,
	ByteMatrix_get_Array_m4E75E3F844C818CD59B197D7D437C2A13764226A,
	ByteMatrix_set_mA012E64ADBD2CABF526DD51EA03AB265B5180320,
	ByteMatrix_set_m325F76586AC96AC087720F79435805CB950DF95E,
	ByteMatrix_clear_m3CA557DCE9875F9831794BB83A0306E6BFD4CA3A,
	ByteMatrix_ToString_mAF14DC1D72F44ABE6426066E837F596E069D654E,
	Encoder_calculateMaskPenalty_m58C32A9DA15698F26C62B50D8BCA663C3F05AD61,
	Encoder_encode_m16DCE8022FD5465578A8FC9CC5AA4A5DA7E28806,
	Encoder_encode_m2EDC052563393CA571307B755ED422FAE19458F9,
	Encoder_recommendVersion_m65FD0A8C726F31CC55606174B9D832826C90AF79,
	Encoder_calculateBitsNeeded_m8DE3C9254970D62C185CCF804318DBD8362910D4,
	Encoder_getAlphanumericCode_m29EEF61669B24D1B35B0F218792BB8E77EDF0471,
	Encoder_chooseMode_mC5F801AB2E2C001A5AA994BC180432A699747830,
	Encoder_chooseMode_m91157097AE47F60FB5E3B29251EF661AAC46216F,
	Encoder_isOnlyDoubleByteKanji_mD93E6DC8D3450410B178B955887E83FCC32BDA3B,
	Encoder_chooseMaskPattern_m6CA486445C0C8302F4A08B006C10C72F893C8870,
	Encoder_chooseVersion_m93EE361D43E1B381DAE74A28BECE13856CBA9ECD,
	Encoder_willFit_m0DC3977B915D3DD1756BD77294A98CD3EA2E7F9A,
	Encoder_terminateBits_m1C287F51019811A61E12F472C6DD197B03C5FB44,
	Encoder_getNumDataBytesAndNumECBytesForBlockID_m9FDEE191A5220B5BAD61F7A8BA773C830218CBBD,
	Encoder_interleaveWithECBytes_mEC4BCEE18D1297AE876773A1A618B03105ED0D30,
	Encoder_generateECBytes_mE87BE80F78CAB53859068946BBFE636653AA1E4C,
	Encoder_appendModeInfo_m94BA3417A7294041D8D86C49D23E4132C15A7418,
	Encoder_appendLengthInfo_mEF6D1EAD7D6E485CEAA493B9524A48B395BF3690,
	Encoder_appendBytes_mDF8D49360EB44961B534720C246E103C96C9C56A,
	Encoder_appendNumericBytes_m15BDE8CBE3332A462ACB50E63477748CB250F7B3,
	Encoder_appendAlphanumericBytes_m2DD5F75AEF17F0439915B31B9C632C237D310DAE,
	Encoder_append8BitBytes_mA8F7621BA16AABEF89F9EC649C3719851E8734A0,
	Encoder_appendKanjiBytes_m21F62C25EEAC689F69484A1AB50A7325D7205BD8,
	Encoder_appendECI_m703055417EF7905A3C2330D0756DFE141AFAB1F2,
	Encoder__cctor_m34AA7E628D44AE462CD56E872CF91B9E7521A685,
	MaskUtil_applyMaskPenaltyRule1_m118D7DEFE94A221A27CC0A1A59021E7C8FA957BE,
	MaskUtil_applyMaskPenaltyRule2_m54CCDA0A12F79111CA3F583D44F3A661D2F7CB25,
	MaskUtil_applyMaskPenaltyRule3_m729EBC01D28C8E3C4F0764D4847775B6AFD2ADD0,
	MaskUtil_isWhiteHorizontal_m1D2DD3BEB589D6FFDD8C90876A3B636A822BB0E1,
	MaskUtil_isWhiteVertical_mD6DCCE743C307E52BEBA598A27F03272AC5C7F11,
	MaskUtil_applyMaskPenaltyRule4_mCB592A6461315D9BC4E767D5233DC7148DB7C7D4,
	MaskUtil_getDataMaskBit_m4308305BE0795ECBD5250F1EF95341CA79DC8DA7,
	MaskUtil_applyMaskPenaltyRule1Internal_m5B1782066035E7CD2C533034AA9D5C307B320691,
	MatrixUtil_clearMatrix_m76203C366F022DA0E9D483F6AE16D1DE11694326,
	MatrixUtil_buildMatrix_m230915868BF31910745EC7ED79C0D719B349639F,
	MatrixUtil_embedBasicPatterns_m1C5195D5DB505BE01BF58CEE8BDFDCFE5CD3464D,
	MatrixUtil_embedTypeInfo_mE7455DA7ED8C599AD7EF65F3AAFE0065824E7AB0,
	MatrixUtil_maybeEmbedVersionInfo_mE95AF355A6514EA4685461DBBC0AD96AC3909833,
	MatrixUtil_embedDataBits_m9945A435818BB5DB8636063CF48001E413E32FA3,
	MatrixUtil_findMSBSet_mFAC1CC728C031E91247A4D99B3EE9E1C281F60E6,
	MatrixUtil_calculateBCHCode_mB950ADB64DF8D3C251E27637107D9786A5340501,
	MatrixUtil_makeTypeInfoBits_m39D1F82B61A659DBDD88B0396C01F06FDC52A37B,
	MatrixUtil_makeVersionInfoBits_m5D50719A867B255C04F1567E51D6822C87908F52,
	MatrixUtil_isEmpty_m792DE59DE23F8F1DFDAA023424FA6E16D1714024,
	MatrixUtil_embedTimingPatterns_m931C0712DC8B3DB50EED4CF99D058A830F67F493,
	MatrixUtil_embedDarkDotAtLeftBottomCorner_m8FE6D90E7284544A23599D6895F2F6A0538E9279,
	MatrixUtil_embedHorizontalSeparationPattern_m3EF28A03F5048E44A6FDC47A7C3CC1C2A4B59C5D,
	MatrixUtil_embedVerticalSeparationPattern_mD04D28A9463F4B93B798A277509766476514D971,
	MatrixUtil_embedPositionAdjustmentPattern_m363FD8D4D54E2717EE23A71AE775797AA4D73B3B,
	MatrixUtil_embedPositionDetectionPattern_m65488C11A6D2955CB8E9946212D409E8574FB773,
	MatrixUtil_embedPositionDetectionPatternsAndSeparators_m293B0210618A820E038D6ED52D62DED5F4E9A580,
	MatrixUtil_maybeEmbedPositionAdjustmentPatterns_mAC137467DDFB07FAC342C7FA85FC8E64600E2FB1,
	MatrixUtil__cctor_m581EDA972D692161E5AFEAE9EDB8A1F91C896885,
	QRCode__ctor_mFD9833E91FDC7E5124792F72EFD522284FCDDA7E,
	QRCode_get_Mode_mDFD69D6029CB9FBDD24B2317856B72748262D2D4,
	QRCode_set_Mode_m13F333C6F87BAC2B46499A5445A2E7B2B99AD33F,
	QRCode_get_ECLevel_m4275719EB90C16F5AAC16AABDEDD703C1CD12808,
	QRCode_set_ECLevel_mE437CC64DFDF14A07449558EDC6475323B675DA1,
	QRCode_get_Version_m0657005C1B9D0A0EA2031B7EEDA53F6A00881613,
	QRCode_set_Version_m8A7BCE20A6E637FD7BBA585224A7610F7785436B,
	QRCode_get_MaskPattern_m7BEB1378FAC83BB501BEAF3395164171E912BE0F,
	QRCode_set_MaskPattern_mFDA72D45D1A371AE16AC14079C920A176DFABA41,
	QRCode_get_Matrix_mAC1C4484FC449938B1B7565350BAD8CBB7D659E8,
	QRCode_set_Matrix_m713783A882672E34A41040C552C82CEA73835D03,
	QRCode_ToString_m3889DA28EDDC6B5B70A05B99CE69280127146D51,
	QRCode_isValidMaskPattern_m16439B31E554F87DE9CC1B4AB3FA11B734A2218E,
	QRCode__cctor_mA0DF61B3743A4B5F3A1FF26BF318C2B727E74B68,
	PDF417EncodingOptions_get_Compact_m3E1CE4A5842692CC0FA25775032D5CF18A5CD03C,
	PDF417EncodingOptions_set_Compact_m555FB76EED76DEE43CC740D0BF76610AA173F597,
	PDF417EncodingOptions_get_Compaction_mE34020C07FE7AD9F7EE3A1D87137E387AE392207,
	PDF417EncodingOptions_set_Compaction_mABA2A2ACEC9E9F14979A2A02A99E38164E521D0C,
	PDF417EncodingOptions_get_Dimensions_m306F453AC0CF7706B6C7C776AC349CF7D738B7B0,
	PDF417EncodingOptions_set_Dimensions_m92B936D730940D4AF2AF0D43667FA1B9F01C4EB5,
	PDF417EncodingOptions_get_ErrorCorrection_m9AD796E82C172D7939B1284CE269C08BF83DCDAB,
	PDF417EncodingOptions_set_ErrorCorrection_m3745A9A6B8F51703BFEBB13D56D9C92817CAFCBD,
	PDF417EncodingOptions_get_AspectRatio_m1066D23D9D817921D3F4DDAD9B2AFFB5972B6512,
	PDF417EncodingOptions_set_AspectRatio_m2776D36E056F64A76AE3E0538B11E44F21308AC7,
	PDF417EncodingOptions_get_ImageAspectRatio_mAE08E3B79D3BD95BBE7B99A4F62D1FF450B14405,
	PDF417EncodingOptions_set_ImageAspectRatio_m461E475218DDDE3185674546675B85FA2396BBB1,
	PDF417EncodingOptions_get_CharacterSet_mAFE31CE2C2A40A295678CB143CBDE3403F3CFA1E,
	PDF417EncodingOptions_set_CharacterSet_m2EBD90AD6D1EA5C6F448DEFA56DA3FD032AE7587,
	PDF417EncodingOptions_get_DisableECI_m478ED4020C6B05AF8B2CE700256CAEF900820623,
	PDF417EncodingOptions_set_DisableECI_mCD4184BA964A45651D478CA9F4B9975D1122E89B,
	PDF417EncodingOptions__ctor_m1B361DD4C051CBB4D181116098B87F07DBB530FC,
	PDF417Common_getBitCountSum_m952005D503D7704115E58E3258562D8F4844406C,
	PDF417Common_toIntArray_m3A1F6C8BBB2FC0013A35B4347C06A9FBEF25655E,
	PDF417Common_getCodeword_m26B9A4512313D82703ECC8F6563079350EABCB5E,
	PDF417Common__cctor_mAF815F45BDC5B572DF7CCF20777AADA427910299,
	PDF417Reader_decode_m990942AB905A37F97D27134FA85CB9DDFF7AE5C6,
	PDF417Reader_decode_m2778DD0C14B24179B28A8C1B97E200447FB0FA58,
	PDF417Reader_decodeMultiple_m034FBB434D29F6D1D022845A846DC191F4F4AF97,
	PDF417Reader_decodeMultiple_m1E2259E50B8AEEB57EC8CC399E7CD43410B1554C,
	PDF417Reader_decode_m94F3916D2005FD7F6C57494E3E16260C0129EDC3,
	PDF417Reader_getMaxWidth_mD09F058726B3626BF8434E518AAF8588F82A629C,
	PDF417Reader_getMinWidth_m2F2D24938ECAC197E24B35DB6BBB39394B25CC43,
	PDF417Reader_getMaxCodewordWidth_mA8F3D4A2ED2E02DB5663CE2EFD39D2A63D7C8A6E,
	PDF417Reader_getMinCodewordWidth_m764C5F80539CE6984597C17A94F60DA913A86989,
	PDF417Reader_reset_mEEC77569470E6B3BE824CEAC172C993038FEA2BE,
	PDF417Reader__ctor_m596E6F0533DA264F1B6B577A3F80800DC581C16D,
	PDF417ResultMetadata_get_SegmentIndex_m06EA835E15417B33292F13B373E8A95E7C10A8BA,
	PDF417ResultMetadata_set_SegmentIndex_m5A6032E9D639F0976FDFAE8048440D82ED4F876E,
	PDF417ResultMetadata_get_FileId_m15E585971DD4CE7DA67463E817D755611903930A,
	PDF417ResultMetadata_set_FileId_mBB175B15EBF9886CF49BC6B1E52DB79B962BD695,
	PDF417ResultMetadata_get_OptionalData_mBC406EF8FF0FED08E3044C4584D83EBEBD8B14BA,
	PDF417ResultMetadata_set_OptionalData_mB0277715FBB425B74EB74230BB7C835FDB26386A,
	PDF417ResultMetadata_get_IsLastSegment_mE769D207C480F95469DE0A82A0B7567F60676141,
	PDF417ResultMetadata_set_IsLastSegment_m3F58AF03654B2D319784BE686D32D84ECDEFFF79,
	PDF417ResultMetadata_get_SegmentCount_mA66CFCA0A8ED5DBF8F0C28A97D15431392F9F4E7,
	PDF417ResultMetadata_set_SegmentCount_m78FA914FA3FFA5E4EBB20BAF3A96F537D232EB00,
	PDF417ResultMetadata_get_Sender_mF97741B8219A424A95C974FA89D51C998A2FBE46,
	PDF417ResultMetadata_set_Sender_mC09700E8431758B0E9E7DD4D4FE6EEC763B18FE3,
	PDF417ResultMetadata_get_Addressee_m122257853479B599ADE3E9558FD167235D1590B7,
	PDF417ResultMetadata_set_Addressee_mED0799341CAB6E149D624117AA37C0FB3508F2D6,
	PDF417ResultMetadata_get_FileName_m7FB66E88FFF62DA92B989B96016EEA1CBEC43A22,
	PDF417ResultMetadata_set_FileName_mB33ECE8A929AA992D61F7F3C9F5CE8E44A1F1DE2,
	PDF417ResultMetadata_get_FileSize_m6C605A98FF3AC2D87B3AC43D739622A95F903957,
	PDF417ResultMetadata_set_FileSize_mE1E1B13785E6CFF3833F8BF84083DFB32048336D,
	PDF417ResultMetadata_get_Checksum_mAD269218DE02E7B2A10BD86E33D13C611BC1AF31,
	PDF417ResultMetadata_set_Checksum_m1BEA43FBA191166F432634B5D50BF307315E9337,
	PDF417ResultMetadata_get_Timestamp_m39CB03064C51F18EE47AFF53FC1B231D3BFDDEF8,
	PDF417ResultMetadata_set_Timestamp_m3A4859104DB07DD217E73D985584DBD9493962CC,
	PDF417ResultMetadata__ctor_mE13E5C0F4A88D5EA98D198FBE3A45CDD9A46188A,
	PDF417Writer_encode_m01FD5727BFE7D2B1773CEF35DB7250FCF68412DB,
	PDF417Writer_encode_m0525C38078AFDAD9CED97AB704CD8C38268C5FDB,
	PDF417Writer_bitMatrixFromEncoder_m1BBF4BE26C84A080E30676751DDDB3362654E106,
	PDF417Writer_bitMatrixFromBitArray_m13261225DE128B354B9C74371DCA8826A06F7B4E,
	PDF417Writer_rotateArray_mF309A9FDD0873C3BA76E2DF148AB601EBFEE58BE,
	PDF417Writer__ctor_mB4C34696EDEE36E58E1E1318CD1115B70888CF91,
	BarcodeMetadata_get_ColumnCount_m495DD133C9AC8FF93A6F9E783AE1DD0F2CEBBEBB,
	BarcodeMetadata_set_ColumnCount_mD288D76895F245577A3E5E1118B7F5E30925E9F7,
	BarcodeMetadata_get_ErrorCorrectionLevel_m053AF2ADB6E7AF297637A82C358565CC5EE68926,
	BarcodeMetadata_set_ErrorCorrectionLevel_m7E14F433CA257C46D64EC5EC2006CBF1C2C6DEE8,
	BarcodeMetadata_get_RowCountUpper_m46E6E91B328FE48C850011A42E3082A0C1BF0212,
	BarcodeMetadata_set_RowCountUpper_m4D34A5D2DEEAB2C70FDCCB4DDD745D2F5EDB77CF,
	BarcodeMetadata_get_RowCountLower_mA8D9AF30B653BD86D4EF352D4C18F4A39B7AB97E,
	BarcodeMetadata_set_RowCountLower_m105DFED692DA7CA9AC33E9F12B738385FEBEDF83,
	BarcodeMetadata_get_RowCount_m59CB0D2FD39454348D476973BF6BCA4BE95B1A91,
	BarcodeMetadata_set_RowCount_m81E4F7B1256A34C6C21B58F71F39DAEE99F18268,
	BarcodeMetadata__ctor_m1167102FDE574005D14927FADDA35C11CD9C4FC6,
	BarcodeValue_setValue_mB3018B60837C4B804A096A5331AC71CDED2EAA4F,
	BarcodeValue_getValue_mC84B99ED8375B69C4380CFA82ECB75CB4AB85198,
	BarcodeValue_getConfidence_m99163A780C379C698822C39643DE59616F1CD569,
	BarcodeValue__ctor_m53812DF904735195921B590140AB002A4B81AB47,
	BoundingBox_get_TopLeft_m809A799D2D1817F6F1207C942E0E5EB0E8DD829F,
	BoundingBox_set_TopLeft_m4D4F353BE27281800269EAC9177562F780A0E7B7,
	BoundingBox_get_TopRight_mAFF6562680BC771AD1549197076470B3C455562B,
	BoundingBox_set_TopRight_mE3E435F55B76E39889FFC9B1A727CC1ADC846536,
	BoundingBox_get_BottomLeft_m414DD57EB4143B38B7656AE71C15F747AB3B3461,
	BoundingBox_set_BottomLeft_mCA0A10257D4EB7FFB89466168CE807F4CF705531,
	BoundingBox_get_BottomRight_m143B41C9B2CE26F54311527CE9068DD1A49906AA,
	BoundingBox_set_BottomRight_mDD7B1B526B4F0C55D4A9FB6D36F923E07912A445,
	BoundingBox_get_MinX_m1E0CF6C6E0F24A7052C3FED772E0E468898A67FC,
	BoundingBox_set_MinX_m47EC61F03B8F44AD57581D33BA25BC778CAAD6FF,
	BoundingBox_get_MaxX_mB7E6B98110BE682F8EFDB280E873C2C3D0B76530,
	BoundingBox_set_MaxX_m92FA78242CCA06564C2D2F6FA6FC734E8EB9B9DB,
	BoundingBox_get_MinY_mBF249C1D47764636FC69ADE98E83E0DA20091373,
	BoundingBox_set_MinY_mA97675214D9A2864A9C14471C6A857C9E30919F9,
	BoundingBox_get_MaxY_mFE45D2C1249C4AED90E11F6EDC0D7AAA0CF10034,
	BoundingBox_set_MaxY_mB9314DB37878A9D8994BA861166983357E892548,
	BoundingBox_Create_m95BA0F393F3DC1136BCCF8C36B0F30E13142B984,
	BoundingBox_Create_m07FF6A30C11FAC700A5AD6D52702D435946C638C,
	BoundingBox__ctor_m16EBD238EDEE9F2E0C68D36AD04DC008A08E1929,
	BoundingBox__ctor_mCED9D07198DFD85E2B842F2185AF839CFD1D3C70,
	BoundingBox_merge_m546EA21DEB263E7D6A8BD0AE022B6DBBC08CB3CA,
	BoundingBox_addMissingRows_mDD24DBF58916375029FDC43FFE2254D7D04719A8,
	Codeword_get_StartX_m1D0114919F24F5C6004531BF6306D92820AFD1A2,
	Codeword_set_StartX_mD20972ABC4A9D72AEFD38095A5329EC3936E5111,
	Codeword_get_EndX_m7DEDE2045D8192CCB527417E2E1F2910C319FDD4,
	Codeword_set_EndX_m45ED6E4F856E61BD0FCABDBA5D12A3D176F0F7B8,
	Codeword_get_Bucket_mC3EAAE2CF85CC24CE3EF1B4B05FA8C74642CBE18,
	Codeword_set_Bucket_mB1D82B0CB6BADCB796101189FD2B4751A7F2A067,
	Codeword_get_Value_m26AF7BAB4CDC25EDCF6622CD2485D6FBAC4165E5,
	Codeword_set_Value_m514E6D3BFE3730E1EF61D57EDB02F475A9B6A0B9,
	Codeword_get_RowNumber_mFF2FB6F1667309924D0A5FFF1EE976AE7D1F999F,
	Codeword_set_RowNumber_m0F59B62A19ECAEDCDE96BFAD662C3D78C9FEEE09,
	Codeword__ctor_m5A8BCFF187983FB2872EF8ECC7FE077959EE55A0,
	Codeword_get_Width_m74648550B4B42E891AB00DCE50913A7E57D12FF1,
	Codeword_get_HasValidRowNumber_m43D09D2258856407C7054D82AB161093D69AD28F,
	Codeword_IsValidRowNumber_m66435A557794FDF3A43E64A8B8EFCC00C261F894,
	Codeword_setRowNumberAsRowIndicatorColumn_mD7DB556D68985B744BC6B68E8839818A3F9162A2,
	Codeword_ToString_mF8BCC87A8D9E1824EE1040C67F2FA0C42A81611D,
	Codeword__cctor_mA05017E5A686CC5E97DC8E116FAC4B600175CF47,
	DecodedBitStreamParser__cctor_mD955CB3A083DA0072081DA3563EF9D1A02ABBB12,
	DecodedBitStreamParser_decode_mB87D4F07F351AE372FA629EA32EC2BBE0787F230,
	DecodedBitStreamParser_getEncoding_m260D2ADBD2482A32672BD2BEB35F2D2DD782AE15,
	DecodedBitStreamParser_decodeMacroBlock_m47DA022C962C6D8A25E066CF5062DE88B04A68C7,
	DecodedBitStreamParser_textCompaction_m450831274EA30B192E02BAFFFE01594E4FD37D98,
	DecodedBitStreamParser_decodeTextCompaction_m063EDA5987DAA7B8E4CF8EAA6486F4771F869820,
	DecodedBitStreamParser_byteCompaction_m07C768D9425A42F74E57C41736CB1BDD22C27EF4,
	DecodedBitStreamParser_numericCompaction_m4BB71B3FA378F91BAEBA2DE3DBD78A9CFA889CD2,
	DecodedBitStreamParser_decodeBase900toBase10_mDF6453F799AE86ECEA1E9F0E02073CDC1CE7693B,
	DetectionResult_get_Metadata_mCAC06316DF7AA38C4138D8C19B3D415BB3787F97,
	DetectionResult_set_Metadata_mFDC1EB9D7EE04E3ECC1015FC757FC181DD1F339A,
	DetectionResult_get_DetectionResultColumns_mDEF605B743BE0044505226C69D8098B2BC94F34E,
	DetectionResult_set_DetectionResultColumns_mEDA4568663EDC7AF66DEB44FA7E4ED0974E1820E,
	DetectionResult_get_Box_m45A7970A731A686CF5849071D5EB10ECDFA294A3,
	DetectionResult_set_Box_mEC9C5C08D9A50C0F986A3EC2A9E5FE048653019B,
	DetectionResult_get_ColumnCount_m79C72C6B91A03AE326628092E51430ED6F3E41BE,
	DetectionResult_set_ColumnCount_m8EB8FBAB718F027FF7653F35618759684883C46E,
	DetectionResult_get_RowCount_mF55E2A93BD3626245C4D9AD013B21F6A71CBB176,
	DetectionResult_get_ErrorCorrectionLevel_mF0DC649E4C8286CB20BD5960E7BD7A71008ABA0B,
	DetectionResult__ctor_m53CF2376F4F5885A7B6340B5383632729BC1EC1B,
	DetectionResult_getDetectionResultColumns_mA26963D25AFD334969B7D0D761BB7709E80490B8,
	DetectionResult_adjustIndicatorColumnRowNumbers_m6327AD854FECCE295423F3D512518CA5990EF546,
	DetectionResult_adjustRowNumbers_mAEC24ECB95C463D4E7690B64A6FDE3D7595BBAC6,
	DetectionResult_adjustRowNumbersByRow_mCA34679CBB8CA72B411AB880633CCF1744192F5B,
	DetectionResult_adjustRowNumbersFromBothRI_m7FE2C96EA378B5577C26713219CB17342C283962,
	DetectionResult_adjustRowNumbersFromRRI_m199CFB51CF20984F15F38C29F41A195817CC5069,
	DetectionResult_adjustRowNumbersFromLRI_m17AB24E049CB2E44CF6D9337A9DB61712CD279CA,
	DetectionResult_adjustRowNumberIfValid_m45CC5FB7A7DCB9548AF83278C8227EC48A803745,
	DetectionResult_adjustRowNumbers_m764DA9A1F56555D706DFE13F19C085208691B9B2,
	DetectionResult_adjustRowNumber_mD74ECBE2E9E19C78F455904E45BAD2CBBADC1416,
	DetectionResult_ToString_m4E3966F79B6C225345028DEC67C0C0BAC1477665,
	DetectionResultColumn_get_Box_m495172EFF137B324BCC742A3C033B1B41DD2FC7E,
	DetectionResultColumn_set_Box_mD828F00C972EE065B1AD151B2BA591E120AD6ECC,
	DetectionResultColumn_get_Codewords_mB3B530850F25B8A51BF07F28F5871D47B60BDB14,
	DetectionResultColumn_set_Codewords_mADE7E294F5BD95EB764D1C5B87F63D39D18B562A,
	DetectionResultColumn__ctor_mDC6656F6204664325067BDD4C276FCB57C79CF65,
	DetectionResultColumn_IndexForRow_m7B10BA3BC25D6DDE8C0BEC67E6D80F7502314EBB,
	DetectionResultColumn_RowForIndex_mA64382A3F30FAB7B45DDE63C6673EEFA1C19FF81,
	DetectionResultColumn_getCodeword_m065483E9E288F3BAED18758AC9EC72E77C7AFDA0,
	DetectionResultColumn_getCodewordNearby_mCC0A95DCC92A3FC15AD54FAEF20160ACD906730C,
	DetectionResultColumn_imageRowToCodewordIndex_m0E54C5321AF45E191F115C519B8A875FBE1610F6,
	DetectionResultColumn_setCodeword_mB2EE392DA7025ADEEBAFEAEE027B5B4476CDB9D3,
	DetectionResultColumn_ToString_mDCFDC4D16B67678A79E2D512310A160F705A7CE6,
	DetectionResultRowIndicatorColumn_get_IsLeft_mCB7F3ABA649CCDBE5B8C92C75B20424485D52939,
	DetectionResultRowIndicatorColumn_set_IsLeft_mD0CB7FAF258463DC6DA6DA3627FC60BF28D9BABC,
	DetectionResultRowIndicatorColumn__ctor_m2DAECFE168232DEC20C8FBADA84BD59A26B2D532,
	DetectionResultRowIndicatorColumn_setRowNumbers_m1BD12E880A6EB3BAE5C89872D61CFCD6F3C91BEE,
	DetectionResultRowIndicatorColumn_adjustCompleteIndicatorColumnRowNumbers_mFAA83340B35261DBB8C83C711F123EB9F512460A,
	DetectionResultRowIndicatorColumn_getRowHeights_m54525EC66F6CFA69132DF96DDB9384101140D2FC,
	DetectionResultRowIndicatorColumn_adjustIncompleteIndicatorColumnRowNumbers_mF75561B54EF8CF4956CB445ECD4AD5684741650F,
	DetectionResultRowIndicatorColumn_getBarcodeMetadata_m651F23D4821E2748E5D042D2057EA8BFE22247F4,
	DetectionResultRowIndicatorColumn_removeIncorrectCodewords_m9EC6C89FF130BE3544A5BA7CA17F5997E7565EC2,
	DetectionResultRowIndicatorColumn_ToString_m2323834957F6B2D1E8D8956E1141716DF5330C5B,
	PDF417CodewordDecoder__cctor_m18A6F45D8DEF81ED8AC15F484789A58825712A90,
	PDF417CodewordDecoder_getDecodedValue_mA279CC7655E2AF8563734E2BA7E9DE4798B8E354,
	PDF417CodewordDecoder_sampleBitCounts_m4C2CB28247AFED5F494F9E35EF174C10ED79473B,
	PDF417CodewordDecoder_getDecodedCodewordValue_m1FEE14F3B48E4D6D7CDA92939EEF39E557A2D244,
	PDF417CodewordDecoder_getBitValue_m70328F64C085CBA68C92C9A35B9BBBF0CE835746,
	PDF417CodewordDecoder_getClosestDecodedValue_mAF9B5E81599BC2C18748691F8E4F1A76F8D98D38,
	PDF417ScanningDecoder_decode_mB9C5C2C952F6469289964B3504D4DC9F088AA9DB,
	PDF417ScanningDecoder_merge_m287BAB2103F12EECD23533EE439C5FF3FD1F6B53,
	PDF417ScanningDecoder_adjustBoundingBox_mE101105DCC3BB8776CCA562845E7FD56FCF09A78,
	PDF417ScanningDecoder_getMax_m7325291BFC46BBC8846D3A9E8460081ED78AD4B0,
	PDF417ScanningDecoder_getBarcodeMetadata_m93005D71624DBF8A046808651DB2699B37ED02CB,
	PDF417ScanningDecoder_getRowIndicatorColumn_mFA3BB1C3DD905D9E097746CD37FFF3ACE112CFE6,
	PDF417ScanningDecoder_adjustCodewordCount_m462AEFBA30BFF693B0757F094020C904C75B126B,
	PDF417ScanningDecoder_createDecoderResult_mF68EF4D433A753A48228AA3E2247800BD50286A7,
	PDF417ScanningDecoder_createDecoderResultFromAmbiguousValues_mBB37E47398BE55F38392709DC61D68AFFBCD557B,
	PDF417ScanningDecoder_createBarcodeMatrix_m0031B1F9A607D8D180525F0321B9528712A90B92,
	PDF417ScanningDecoder_isValidBarcodeColumn_m3EA1D87F26C127A67571238C9F852B36826970FE,
	PDF417ScanningDecoder_getStartColumn_m7083F7E9FB84D6F2B8550D15B98B5B1EE0E5F79F,
	PDF417ScanningDecoder_detectCodeword_m417D253D5DD32B64884944214C49D8E78283AC7B,
	PDF417ScanningDecoder_getModuleBitCount_m7302588876C803EDE03E04D5E704FB1B4A3F232C,
	PDF417ScanningDecoder_getNumberOfECCodeWords_mD5215D60C9BA8BC8980956441832DFE52161DBDA,
	PDF417ScanningDecoder_adjustCodewordStartColumn_m08D180DF936D2F9A23A6D88A869542531F3E980D,
	PDF417ScanningDecoder_checkCodewordSkew_mEA0E382D9F3F2683E2489DBBCB8B21D93CDA53FD,
	PDF417ScanningDecoder_decodeCodewords_mEF38AE2872478FB1E8CB7736A268175067AD49FB,
	PDF417ScanningDecoder_correctErrors_mB45F03F4B2344AE589CFFE3CCD332270E62D72E1,
	PDF417ScanningDecoder_verifyCodewordCount_mB0537CC59012D4870A7DAD3E4B26BF91D7D9BE55,
	PDF417ScanningDecoder_getBitCountForCodeword_mE3D5E08553D1A210AC2502627DB5D57815C0E16D,
	PDF417ScanningDecoder_getCodewordBucketNumber_mA20A663455305059C14FC445CAFF40E2EB949BC6,
	PDF417ScanningDecoder_getCodewordBucketNumber_mD8D55368016ACEBFD805D791900F4DC659011962,
	PDF417ScanningDecoder_ToString_m5F200BFBA87F568B82ADAC2192EB41618C61B192,
	PDF417ScanningDecoder__cctor_m12203AF6128BD4141EC5D463CE8A839915A2DAB8,
	Detector_detect_mD5B08CB51DDB6A110B43C8C9C1D3BD929DD9A8B2,
	Detector_detect_m320C25A464C25B6DF88310915B525B16B378AD7E,
	Detector_findVertices_m1BD74BB15BCF96EC821CBB3900DEDD69B8548203,
	Detector_copyToResult_m23C82D6732944677F0A9891BE09C3ED2774E3E59,
	Detector_findRowsWithPattern_mC55194C1182052DCC63516B3179E9CE18BD44ACF,
	Detector_findGuardPattern_m8F090EE16E8A5948D47C9B5A20131CAA9D4ED3B6,
	Detector_patternMatchVariance_mA9965D7434FF7B607A76E184DB0907D50E94535C,
	Detector__ctor_m6E13EAD989DE10A2828A18CFFF658C2E18C23D79,
	Detector__cctor_mE873B9BAA0D9E694A2E9C00958867807ACD980F8,
	PDF417DetectorResult_get_Bits_m9BFEB179EAA8DA65BEEAD6A75C5F79FE676F7357,
	PDF417DetectorResult_set_Bits_m05D78DC749C240F4868352AF37376EA0C511EF34,
	PDF417DetectorResult_get_Points_mCC688776DBC06D17D300BC006C8F708B8127CB3F,
	PDF417DetectorResult_set_Points_m6882E23DA70AAF83E193C61315C6CEF642F6BDF1,
	PDF417DetectorResult__ctor_m57D6DD5548CB8072FD8DFCB29CB277E92AEEAE38,
	BarcodeMatrix__ctor_m3FE79388A37B82EAC2C6E1ECF074E91BE2CE4886,
	BarcodeMatrix_set_mA057BECB8D1937B0702630A613278331821B6480,
	BarcodeMatrix_startRow_m055DE6EB3D7DDB8A1F930BB4CB17EB7D024D83BE,
	BarcodeMatrix_getCurrentRow_m2C51A115A9DCFF92897C4E174BEE9A15B0EF5B1B,
	BarcodeMatrix_getMatrix_m0F3658D17C54BB1C81DAF0132A4DC9C0704C22E9,
	BarcodeMatrix_getScaledMatrix_m357C195E4D1E888FF4CC2A222A008E7C740F5A2F,
	BarcodeRow__ctor_m37C2E3D8DE46503396AD0A305C3CB1E90F273891,
	BarcodeRow_get_Item_m7892C96D2F02D4D440EF7345FF27B0C6699F8842,
	BarcodeRow_set_Item_mBB489746AFC98B730045C87FA8C0451884F9AFF8,
	BarcodeRow_set_mE5E497A54AF4C7B8DE244D5124F6333DE4E20BC9,
	BarcodeRow_addBar_m051B25AE3638032E21DF84D5BAD84872C1CB0457,
	BarcodeRow_getScaledRow_mFD8E8EC92989D81E3B1FA6DB8E31509DDF918801,
	Dimensions__ctor_m1113F7DDF01CC6FE92EB2DC5629AFEF6E42AC562,
	Dimensions_get_MinCols_m580F2409A2C9C791F8708FEC52E5318C5354E6FE,
	Dimensions_get_MaxCols_m2C50370953C5C0984C807D0A5CA0CA563BDCD017,
	Dimensions_get_MinRows_m09446D25C3BE695966CE2D456F9BD1F53EE5BD4D,
	Dimensions_get_MaxRows_mA2D5B26AFECDA435728FC4574DE05FC0120FE91A,
	PDF417__ctor_m2D1F66BE06CA44ACC1C50DC97360EDD8BD162EF0,
	PDF417__ctor_m8F53FE0E1610185CDBDB1C1E3EB68D92C89133F5,
	PDF417_get_BarcodeMatrix_mD2BA9DD09F45938AE401A391D7CC7BEB79FE8192,
	PDF417_calculateNumberOfRows_mD5C1F20CB7803632BA72ED7716899043F106F378,
	PDF417_getNumberOfPadCodewords_m4D5B543E6E8BFE5BB92A620B7F63D6BD8304F461,
	PDF417_encodeChar_m33598EE9026E5B47775357D7D470BA7B29FED36F,
	PDF417_encodeLowLevel_m7A71FD34665B4F40BABC635AC4FD81F31DD6A32F,
	PDF417_generateBarcodeLogic_m52A0D6F360E3843691AAC8CAEF021995543E60E6,
	PDF417_determineDimensions_mCFD1E7B2E4E9FCFCFDE96F10162BB34021B9C61C,
	PDF417_setDesiredAspectRatio_m2EEEDD9842C7493404D54A4D9576D4648AFE26F0,
	PDF417_setDimensions_m81D43D0E0478FB6FE887F90CEC269C3468953FC0,
	PDF417_setCompaction_m27EA88717186013B204CDB010804AB9E5E2660F0,
	PDF417_setCompact_m8D5966900B8A1872E749E40E9C2BC8B44F4DC290,
	PDF417_setEncoding_m5E1C981C3EF502DF48B6CE934C743A79BC21739F,
	PDF417_setDisableEci_m66B50298509B1B219B15E6975B1E6CAC9CAC0C08,
	PDF417__cctor_mDBFBD00A09B42B1EAF3BCF728BDB7C9BD3363437,
	PDF417ErrorCorrection_getErrorCorrectionCodewordCount_m182CF1B790FEB40433456CB47BE7501054545DA7,
	PDF417ErrorCorrection_getErrorCorrectionLevel_m950BFF7FFB747B18F787EDA404DF68300B107653,
	PDF417ErrorCorrection_getRecommendedMinimumErrorCorrectionLevel_m12EA19EB49A91FB39BE7D40062757F68EBA74117,
	PDF417ErrorCorrection_generateErrorCorrection_mEB7203E725BCAF4CAE5DA38A5EBEF5D876BF4763,
	PDF417ErrorCorrection__cctor_m2793F40DD450E7539A948BA9B77377E43857DB21,
	PDF417HighLevelEncoder__cctor_mED45AC858EB6DD672448508DA46631D951C76E52,
	PDF417HighLevelEncoder_encodeHighLevel_m482E99741FBE267C49824536D83C1230A3AAB524,
	PDF417HighLevelEncoder_getEncoder_m49689C8000DF02FD41759F54B5CE197C1430BF9F,
	PDF417HighLevelEncoder_toBytes_m8677D2A91FA1618C6FD780271F7AE77787FF28EE,
	PDF417HighLevelEncoder_toBytes_m035D38B1DF6BD4B01F24AAFC4E60B36CE354FE44,
	PDF417HighLevelEncoder_encodeText_m41AA2DE93569779052EEACA6A16689C482503050,
	PDF417HighLevelEncoder_encodeBinary_mA3E1609B86A393FA5FC8320DFC13AB111F0BEA08,
	PDF417HighLevelEncoder_encodeNumeric_mB134F709D9925E93F123462A0CB3CD8A63CA9087,
	PDF417HighLevelEncoder_isDigit_m8B31340B29F9A5BED9CA5CC8B0638C66FFD89B0F,
	PDF417HighLevelEncoder_isAlphaUpper_m8F4BD9C63823AF6E4D73B75ED2A883E069299196,
	PDF417HighLevelEncoder_isAlphaLower_mB7AD7CF1A6C0E194D1B16E741DCBFAF28251D0F3,
	PDF417HighLevelEncoder_isMixed_m07240A3B54A4A96713B0C7CA5D12733EE6425036,
	PDF417HighLevelEncoder_isPunctuation_m059ED04416EF6482131B1A9D5370A2EC08120252,
	PDF417HighLevelEncoder_isText_m839743B68A0B60AEC486E15689C75765A37A9FD4,
	PDF417HighLevelEncoder_determineConsecutiveDigitCount_mE36926C8E31E349FE4BD96DDE7609EE9E57622A7,
	PDF417HighLevelEncoder_determineConsecutiveTextCount_mCF597FC8C9D8585C4558018ADC5A1867CDBB3C39,
	PDF417HighLevelEncoder_determineConsecutiveBinaryCount_m35F18E91E07C1BB4036A0172DD8EAFBE1F247F10,
	PDF417HighLevelEncoder_encodingECI_m93E596FC29CC8DAA55DD3584B918A9D3576AA55A,
	ErrorCorrection__ctor_mF0AC86ACCFA509DDE4A856601B35B3456231505D,
	ErrorCorrection_decode_m1089E3BCEF64CCB99A4E7DE4D488BEA41A008026,
	ErrorCorrection_runEuclideanAlgorithm_m3EFF5753705A530D077A28B1C313CF5C6BAC1489,
	ErrorCorrection_findErrorLocations_mEACA2A4EB73B8E75B85859A4AA437ABEBAD8A3C8,
	ErrorCorrection_findErrorMagnitudes_mE0294AEBE952B7924D0FE86F6EC5BF89F6D6A305,
	ModulusGF_get_Zero_m863E0E297D8952146A1033987BAAAF3B20362D6A,
	ModulusGF_set_Zero_m7771EABE3F3AA76773960AF47478F4A578E353F1,
	ModulusGF_get_One_m0855E3521E13DAACB705044BB0FB2AA3C2BF3D49,
	ModulusGF_set_One_m58E56880F2B41E5CA3F0F80E9D5334FA592A9A16,
	ModulusGF__ctor_m5747FD9A82E764BD6886CC2D6AB4BD9B8CC1C73D,
	ModulusGF_buildMonomial_m7B6967B736DCDAC0711EAF70E0FFD69E30DFFFB7,
	ModulusGF_add_m99B1CD80199ED5607F3C7D0F62D03B429009B0E8,
	ModulusGF_subtract_mBAC1890BA40C2B104C6F726E2E2F1C705761333E,
	ModulusGF_exp_mA2A69B87DE69A0CC46676C5A830227FC4A95D6D8,
	ModulusGF_log_m8C3CD9E481DA8DE8E0C6BA9C4FD04EFE262B3682,
	ModulusGF_inverse_m9047A6009C43D57BF3612EB8CC2B729DEEC8C210,
	ModulusGF_multiply_m425B2400D37567ACD2138FE13A58D591FE71F962,
	ModulusGF_get_Size_mB39AB719BCC1423CD2F979552536CBBD18C80492,
	ModulusGF__cctor_mEF358A95DBB5D70851CFA53476089BFEC259AD4B,
	ModulusPoly__ctor_m25FE3FC1B5202FD32E10B82800D117962B2D2093,
	ModulusPoly_get_Coefficients_mCCF5E99E9A6B2217DBA075BB68AC8DF3507BDC5A,
	ModulusPoly_get_Degree_m67D79A67D205CB53D827116D2567FBB8679F2624,
	ModulusPoly_get_isZero_m8E5409ECC7C51F38DB75C053851A9AC0B868DA69,
	ModulusPoly_getCoefficient_m6C022ECC4B92A51C89DB39927E188809C60EAC7A,
	ModulusPoly_evaluateAt_m8FD3AB342A478A924B9A1F3EDAC0FC397AD5F16F,
	ModulusPoly_add_mC7938D09C20B77C6732F3AEC5D92E22BA6B1029B,
	ModulusPoly_subtract_mED658EEB9DEA2CB830DD7C1529308A933A9787DF,
	ModulusPoly_multiply_m3F3662B4812C05A0830796A3142DAE6C16D06B70,
	ModulusPoly_getNegative_m636F7115F47A3DC1FD2FD9DAF6EBB73FE7C11675,
	ModulusPoly_multiply_m63B561E5A1A1789F8AB99499B30E5F352859F87B,
	ModulusPoly_multiplyByMonomial_m8E0E2453137AABEB75E234F5EE6451C27DF0868E,
	ModulusPoly_ToString_mB713218C8814B1FA88292F9EB2324A2B35934FC5,
	CodaBarReader__ctor_m3BEA06B4996710156CA5D513E5664112A25FE2B8,
	CodaBarReader_decodeRow_m94BC56D21EB2729468619AB7C4ECA0A122865868,
	CodaBarReader_validatePattern_mD24E9887BB98FECFE00F0E03E50C398410D242A2,
	CodaBarReader_setCounters_m1056F8DC042F325B7CE66C74487F83CC8CAEF0A0,
	CodaBarReader_counterAppend_m84A441A4DC31F86F75EE5CECC3B3CDB6AEA48C72,
	CodaBarReader_findStartPattern_m235D5451C9C2D12C0DB780FDC9E074F35BBBFDBA,
	CodaBarReader_arrayContains_m05D85B2AC1DB1431392C43D052100A74CA02819A,
	CodaBarReader_toNarrowWidePattern_m246B2E1A57E2534EA41509E8A878815B26C9FB59,
	CodaBarReader__cctor_mCF60F4D0B8386A047014F56931815F249F434B2D,
	CodaBarWriter_get_SupportedWriteFormats_mFB097B05365959C1CF08E693364D0EE5194DED58,
	CodaBarWriter_encode_mE886A588978F1218B5F1F544113F8C4CB8F4B425,
	CodaBarWriter__ctor_m51A18492D97F3238D6F929A99395C376A56996CD,
	CodaBarWriter__cctor_mD4880D7425E3A4294EB6AC639090DCAD73B265BB,
	Code128EncodingOptions_get_ForceCodesetB_mC56272CBFC9BCAA855CEF2314298F7106C1B80E0,
	Code128EncodingOptions_set_ForceCodesetB_m5B0299C63DEA04770F92F3B94B00326F5AE91D1A,
	Code128EncodingOptions__ctor_m028F25714DB73AEA6AC5F78A4E1FC4F85EB60EDC,
	Code128Reader_findStartPattern_m8BE7E46342579879675FAA1C6B28A210AA2BF337,
	Code128Reader_decodeCode_mD5329CE5D63BBF7BD26B0C3169FD1A4071125387,
	Code128Reader_decodeRow_mB671902EEDA2484C8AD8A60CE6577877CCFBD01B,
	Code128Reader__ctor_m0B01C09151CD3ADBFDAD0385FC892025E05D6D24,
	Code128Reader__cctor_m63B729960536D600D1053A3F54109A56470161D1,
	Code128Writer_get_SupportedWriteFormats_m4E94A0424E71611B6EBBDCE376370E624218E882,
	Code128Writer_encode_m51A63FAF506ECE8FBF6278FDEE1F9DDCD27AC1E8,
	Code128Writer_encode_m946DF90812EE9004BA6C77FBF32730D80C80CF25,
	Code128Writer_findCType_m73D3F8C2576514CA23981EB6079849F6251DC345,
	Code128Writer_chooseCode_mDD7C044CFCA7E998A34B9E86AD333EF8D4194304,
	Code128Writer__ctor_m03C0D410B67030AD59FD2078C89EE6D2B8C32B02,
	Code128Writer__cctor_m85F977AA7EDDB7E1271D94943301022718B49531,
	Code39Reader_get_Alphabet_m4F82DDD17D865C85A796B4174B372D07AA16476E,
	Code39Reader__ctor_mE2A4C036D0071280910B2CACE7F10C7A539A1DC1,
	Code39Reader__ctor_m79C4AC6791A1D5104B1AE99F78F0BB3AC17F158C,
	Code39Reader__ctor_mF5EB3BA77062694C2FB251A2698074AECE818EF3,
	Code39Reader_decodeRow_mFB6F83D4DD66A04458EEC597B984547042FBCCEE,
	Code39Reader_findAsteriskPattern_mD1E73F37DB210DC50C996E5B7CC2EF7BF9A021C8,
	Code39Reader_toNarrowWidePattern_m189754A5668BB134015580C44BB6792F42EA45E4,
	Code39Reader_patternToChar_m780CFCE8C7B9B6FC0F50BF61FE35DAD3A95C2E47,
	Code39Reader_decodeExtended_m9CE4355C87FF553ED4936DB2A552E9182BADBE49,
	Code39Reader__cctor_m683DA26FE3240119861C4BC42B6D3FC013F459DA,
	Code39Writer_get_SupportedWriteFormats_mA2BC915438E6962D292413F57B2FCA5F2DA5914A,
	Code39Writer_encode_mC6C64C5C4D547DE2926C3C14F77ADFF0A63022DA,
	Code39Writer_toIntArray_mDC5FDD25AD2FB3A03AB5A7040691890B867E1FFC,
	Code39Writer_tryToConvertToExtendedMode_m37099563E131D7D560A225AF9308114791755FCB,
	Code39Writer__ctor_m32AB26CD681085CFEB302F065055F32B05202D13,
	Code39Writer__cctor_mEEEBFFB404983081171A016FFDDB4D570D017D06,
	Code93Reader__ctor_mD104B27A7B99357604ED3266F4E9B14F7288D7F9,
	Code93Reader_decodeRow_m12943EEC5E6DE93A01C2BE48E36DB7729FDF3B81,
	Code93Reader_findAsteriskPattern_m723184C62F219710E38BCC5395FB2A4A510B9735,
	Code93Reader_toPattern_mC8FBC15F8644382A00420116075C5DC92150024F,
	Code93Reader_patternToChar_mE0D4B7D07AAE60772BA9E1A719CC49668AC8330C,
	Code93Reader_decodeExtended_m28B866D7E254463A346B9D0B676A4BF4D9ACE05F,
	Code93Reader_checkChecksums_m040416DE7B98ECE9C777A2EDB7CFD6418DE7B683,
	Code93Reader_checkOneChecksum_mDF2707754E965BCA0BDB275A7E3FB40973AA7B8A,
	Code93Reader__cctor_m113446777B0810F256D93B3CA9AF82D4CFB30E8D,
	Code93Writer_get_SupportedWriteFormats_m98FF58A6366F453175D2C987425CF403ABF43C9D,
	Code93Writer_encode_m6FC094DE1E0B87B8EE7663F460F7E102557B46F7,
	Code93Writer_appendPattern_mF631EE31B3857808711A9D1B75BFFCFCD2373DE7,
	Code93Writer_appendPattern_m0A28A95F2265F2FC54C262F5CEAA5F49AC899CDE,
	Code93Writer_computeChecksumIndex_m7AB33F564C3841EB67D4292A0092CC4D25C7EBE5,
	Code93Writer_convertToExtended_mC97764F2A89FB1A5BB8FAE1BA8E7304FF6EBFBAE,
	Code93Writer__ctor_mDAC91EC164DDCE2D295F57384EE110471CF971AE,
	Code93Writer__cctor_mA44AC9D3211484785CC5AB02CCC077FF87ABEF4E,
	EAN13Reader__ctor_m22D6E926FA4F232637A64ADB918C8C68CB1A1435,
	EAN13Reader_decodeMiddle_mE80E5BD6784144251320D86694F19F865B7D7E18,
	EAN13Reader_get_BarcodeFormat_m6E31601906454EDCE34E277224C206ED17C06282,
	EAN13Reader_determineFirstDigit_mB926FCB753C9E158BD4BE779891764A94A76C85E,
	EAN13Reader__cctor_mCC2C0034CE113625E5529941C6AE21E3A8C51639,
	EAN13Writer_get_SupportedWriteFormats_mF86E0477490E7DE8254C50FAED51EAA12CDFA113,
	EAN13Writer_encode_m0A0284D6588CD40EDFB87080C6DC05870E2B4116,
	EAN13Writer__ctor_m9C4D4AED6A2EEE42E66EC9D40D993DAACF6FCC39,
	EAN13Writer__cctor_m6926480610FD72797D09269A6D667C2C9F719E19,
	EAN8Reader__ctor_m6E3B814D52B0A5ED2E9ABE664362C8668915EB96,
	EAN8Reader_decodeMiddle_mEF5AE8CDB21E07AD5FA0D2ED1CF7E2F906919030,
	EAN8Reader_get_BarcodeFormat_mF28C56A8411A4DCA74A565A7EF1586527A9FC1A3,
	EAN8Writer_get_SupportedWriteFormats_mB4F1BF6257A7E850EADB7B2A38BEA35A9D17EF7E,
	EAN8Writer_encode_m43B7560E5C695A6EF5AEEA40AE2A65819E538A38,
	EAN8Writer__ctor_m030610A728505DF891C32E0F5C997BFD5046B643,
	EAN8Writer__cctor_m21AF1FB5C8D8AFFC3650EB8A2D981C1B92E0FDD9,
	EANManufacturerOrgSupport_lookupCountryIdentifier_m47B81C7F764371EDFC05F46F9E2D1BF813C56008,
	EANManufacturerOrgSupport_add_m969371E77A295DCE664C4376816605E45954F1D6,
	EANManufacturerOrgSupport_initIfNeeded_mD83A5BA9701C07D6312FAA1A73537F7A9FF59179,
	EANManufacturerOrgSupport__ctor_m3F9E01BC9025941B93E9F70DDA97AB5804C50A0B,
	ITFReader_decodeRow_m22E033F6D0433161615F4553AD6D83FBD088680A,
	ITFReader_decodeMiddle_mECC4A1B9438B016100648650EAD9C048F6445343,
	ITFReader_decodeStart_m6AE1F290E6456DAAE138E03ACA290BC8243F74B5,
	ITFReader_validateQuietZone_m31442A2E97AD81270AAD0CA301FDF8645C1E6377,
	ITFReader_skipWhiteSpace_mFF7E082544C33FC9256EAA3AA6AED862317DF4DB,
	ITFReader_decodeEnd_m90406A2EA906E16EECBA65DABE4ABBBDD780CC69,
	ITFReader_findGuardPattern_m9CB621D4D8F5229E0A46CC0FA65940B20AA18A2A,
	ITFReader_decodeDigit_mE56B7EE512CB25E6A1CE92ABD658705C6FE76E23,
	ITFReader__ctor_mFD51E19DBD8BC4E9D0269EFC658AAB9FE0A7D472,
	ITFReader__cctor_mCD1E2778CF9172F7F81225CE8D9E16076C0087FC,
	ITFWriter_get_SupportedWriteFormats_m7C33CF8AA79E6E8C12A986D579081DCFC6184277,
	ITFWriter_encode_mFCC08A914DA9C560B107F84782432432E880ABBD,
	ITFWriter__ctor_m609B819419AF1081E7E5666AADE5E5676FBBDB7E,
	ITFWriter__cctor_mC163DDC775F5AFB7E978CF7A9FD961903560D26F,
	MSIReader__ctor_m8E09C6EC273D2D5A52DCDBE0936251B26644ACE9,
	MSIReader__ctor_mF6202DFD5B5572595FD81DFF374FFA4B171EBE0A,
	MSIReader_decodeRow_m2C22FB1AFF62E312556F2D92DBABF03E1217B0E8,
	MSIReader_findStartPattern_m57189F8044E2EDFDEF0EC8203B5C2E642E536B41,
	MSIReader_findEndPattern_m9C07665F4FDDA340895959B15CD03883C54C0472,
	MSIReader_calculateAverageCounterWidth_m1783EB674921CDABF86385772B87D25B16CE5EC7,
	MSIReader_toPattern_mCF6D7F8F8C5567C8584BD46ACEA28EBDB3C64D90,
	MSIReader_patternToChar_mC0363ECBD351636D44E9116F95B1D1CED7ED8BBB,
	MSIReader_CalculateChecksumLuhn_mBB8FBB9E7CA179870E6D7021704DC19BFB51F7C2,
	MSIReader__cctor_mC0C84786D1B8F65D243C3F1D733E272A90BA5480,
	MSIWriter_get_SupportedWriteFormats_m86D821A50C6B332101D87F103821DE1E50DF5AC8,
	MSIWriter_encode_mD0BF92D8A76112216305FE8371578955C4162232,
	MSIWriter__ctor_mBEC018413E5CF3A2A852A609AD45FA917E6DC6E1,
	MSIWriter__cctor_m9828D66C74D5CBD16107995DA76165C202C5CC1E,
	MultiFormatOneDReader__ctor_mA7FB46F719134F412591B0F837E59EF6275284CF,
	MultiFormatOneDReader_decodeRow_m142432FB5BC25CB5705FE21BD103B0B27D786FBB,
	MultiFormatOneDReader_reset_m210731053461A4410FF44486C7D9B5190D28EAD2,
	MultiFormatUPCEANReader__ctor_m804B8AAC5D80FAD4F6F9CFF44B834318EC0FC6ED,
	MultiFormatUPCEANReader_decodeRow_m1F2B5088C1FA7C726B5C0A6D9F3A492244E5DE7C,
	MultiFormatUPCEANReader_reset_mB67FD3C8C06EFEC7B9EC305F10FE6656DC3FBB65,
	NULL,
	OneDimensionalCodeWriter_encode_mD71076189BD3C9F57C8C0B9AD38325591C70CA38,
	OneDimensionalCodeWriter_encode_m362F7E046D1F23863719F9789C0013D5E34E4A78,
	OneDimensionalCodeWriter_renderResult_m6E22D5CD5A7C52F4FEC963D9DE1423E2AC4F5EC8,
	OneDimensionalCodeWriter_checkNumeric_m25DA6B8DF0D43D0697EA7398C8B51EE3040A4958,
	OneDimensionalCodeWriter_appendPattern_mA9B29292D329CFD3608783AE81DDF03E545FBAF1,
	OneDimensionalCodeWriter_get_DefaultMargin_mCC735551FA33496906924C3439AAFDABEDD8A6AF,
	NULL,
	OneDimensionalCodeWriter_CalculateChecksumDigitModulo10_m53712C80DD6DDE42B2BB32FA36CF6568A0B4474E,
	OneDimensionalCodeWriter__ctor_m67DD81464EEC4332E8A5641591441EE922327B81,
	OneDimensionalCodeWriter__cctor_m31C8768C1A0F673F848F57B53CEF5AB075A5CE7A,
	OneDReader_decode_m0D7756159D80880AFFC4F88CBAE45F91795E73CB,
	OneDReader_decode_m914C584C3CDF37B1C5339FA196CF318102FE512D,
	OneDReader_reset_m512511FE6DF0807BA98EAC80D2AE44E1B3CFDB26,
	OneDReader_doDecode_m8F0D3580006D9D4E3974BE0FFF085FEDD63ACD86,
	OneDReader_recordPattern_m36231F2E6967A5F110AC144CDC7F66821E881F15,
	OneDReader_recordPattern_m3261EDD976AF08CC3D5EBCF5AD0D6AF70C9BD722,
	OneDReader_recordPatternInReverse_mB51AFCA11C01A7A0362E776C7EC97763AD2D030A,
	OneDReader_patternMatchVariance_m8D5A7E8EDD59EACDF5F6F6C11B6478148B023DDC,
	NULL,
	OneDReader__ctor_mDCBD4E0D37075143C0AB48A5297BDC70E8D694AF,
	OneDReader__cctor_mB688BF74A4957B7A1C647D13F9566BF44D33243A,
	PharmaCodeReader_mean_m2B7BFB78DE54F8787CA2011C36D7779BF97C64D2,
	PharmaCodeReader_decodeRow_m716B6B8DF47E51841A2DB3BC3ABC7EAE351C67CC,
	PharmaCodeReader_finalProcessing_mAF72DD4B1D3D97E7FBBFFD43C0FAC1E35D437108,
	PharmaCodeReader__ctor_m15CA9D2962AE0D136E61FF8BA6DEF2E6842C75F1,
	PharmaCodeReader__cctor_m0DACF042E0928CD4727BF1EA9D16120DFD82ACCB,
	PixelInterval__ctor_m3ED84D1259A738FC06CE08339A886CCAB16B8C9C,
	PixelInterval_get_Color_m439B9190309D6A4251CC8805CB27FEC320C363C6,
	PixelInterval_set_Color_mC220877A724E4FCBB78D8E52A1BA00306EEE430A,
	PixelInterval_get_Length_m00B95F4A6BC347B50E9989B79F514373B8E31B37,
	PixelInterval_set_Length_m1370FA226169CCC71DAE6731DAA83B3F6F82EE1C,
	PixelInterval_get_Similar_m524C9D1ABBE9D611ED9F4398631AC092E9D5E850,
	PixelInterval_set_Similar_mEF2DE4A008093D8736D4FB2D7669ECEC3C4F0D6A,
	PixelInterval_get_Small_mD9E36F141851F9B14E652D208AB58EF7D7D9E686,
	PixelInterval_set_Small_m4BBED5287169F46B3E144E0BCF9CACCD9A3AA70A,
	PixelInterval_get_Large_m0E4A27AC439AD47B106F5E83D9FE63B91E321283,
	PixelInterval_set_Large_m856F3990583C8D8F877EFDA6CD2BB6703B6AD3C0,
	PixelInterval_incSimilar_m043A80ED43CF8AE588F35D6EE1A7F0F5E6EB5886,
	PixelInterval_incSmall_mC1067C2683E372B3D04745406C8CC3C701CBDA0E,
	PixelInterval_incLarge_m9EF5E32389201B6FD51E1F4FA4FA8E35F96FD4F1,
	PlesseyWriter_get_SupportedWriteFormats_mCDD5C60088F578EAF7C3061665EBCD53B70DE655,
	PlesseyWriter_encode_m11E3DEB39B1788C5B332BAF64AB251D340A69696,
	PlesseyWriter__ctor_mBAD22A528173A07A6E886EC74E9143E9902FE35B,
	PlesseyWriter__cctor_mDD383227C496495F0B77F3704C005036F7122CDC,
	UPCAReader_decodeRow_m55870FFE11E12BDCA62D33F89581D35E23403D6C,
	UPCAReader_decodeRow_m49F8AC4BBB564C255F16D879A923F1F44C9B20DA,
	UPCAReader_decode_mDFB9D147DD664E3F84007F8BFE2DF3BFB7289E30,
	UPCAReader_get_BarcodeFormat_mAEF19838FC214383B1CDB43B72A018FB1FA47DB7,
	UPCAReader_decodeMiddle_mF5402D33F26BF7A6EAC1C330B3CF8E085A895DE9,
	UPCAReader_maybeReturnResult_mAF92CDB04BDB0241309DD2198A5BCEC4D5EA16E0,
	UPCAReader__ctor_m55697E4C701AB2874295E9F31B6423B2130D3F60,
	UPCAWriter_encode_m5F15128CF0A86DD8DFC2844962F9D40CF9A8AC1F,
	UPCAWriter_encode_m85F0F50EEEF4B8D3A814E8DC4EE728E5FAD006C3,
	UPCAWriter__ctor_m642BF9362116F576C52D7A9965AC5DFE3F8F7CE9,
	UPCEANExtension2Support_decodeRow_m5D69D7FB6B9883012B8C006D4E7119E05C928195,
	UPCEANExtension2Support_decodeMiddle_m7D23683EE8B9011A050E7FCB2912A3C2C1D7E698,
	UPCEANExtension2Support_parseExtensionString_mC0E4F98644464C43CF000C741AB29ADB94496E8A,
	UPCEANExtension2Support__ctor_m2D8E8B3F30FCF468C481D71BAF74115263ABBD5A,
	UPCEANExtension5Support_decodeRow_m5D3A775468AB9B4AF402EDBEA7B90584A089E03D,
	UPCEANExtension5Support_decodeMiddle_mD37FA70ABC47AB5E788C94B1BE775BF13DB5A8F3,
	UPCEANExtension5Support_extensionChecksum_m63C8AE815828EE71C4AA70F6F2D2A9B48938E636,
	UPCEANExtension5Support_determineCheckDigit_mE77F6A1636FAE27D8D18196C84C51DFC090B9BE5,
	UPCEANExtension5Support_parseExtensionString_mD01076D53D961F327592ED3B74471B0C38ED5994,
	UPCEANExtension5Support_parseExtension5String_mA8EDCB54FF5D50DA075711BE15A8E009A67EDD35,
	UPCEANExtension5Support__ctor_mFFA2277F4C2B698BD430C8BCFE5418762E18F284,
	UPCEANExtension5Support__cctor_mEA65DB2E0F731553F10B07EBEE858CCD6650ACEC,
	UPCEANExtensionSupport_decodeRow_mD76AAFBF1D3431876412E4D6EA2CB070AD5FA245,
	UPCEANExtensionSupport__ctor_mFF7D8E10EDB00D8C17A7868C9A12AC06E44A5BE8,
	UPCEANExtensionSupport__cctor_m8CC363D21EFCA9628EA08E8840690491704C4D4A,
	UPCEANReader__cctor_mFEAE8E8813716E1A78570B6922E65CA4D5D2C3F9,
	UPCEANReader__ctor_m86D233D6C25C83F8DB3A78546E743A3706DD9FC3,
	UPCEANReader_findStartGuardPattern_mE3184D96AD82B8ABFC6185409F7859A2AC98073F,
	UPCEANReader_decodeRow_m124E7FB0E2BFAF0DB4DB9C48F629FF52FC93E6B5,
	UPCEANReader_decodeRow_mA74368C50FABECA1FFFE523FBBAB638ECEE6FA9A,
	UPCEANReader_checkChecksum_m5FCEB77BEECD0363704ACED4D4B56FF8F3E75170,
	UPCEANReader_checkStandardUPCEANChecksum_mB1503E08FC1ED7546DB111E71C7AE73B1363F707,
	UPCEANReader_getStandardUPCEANChecksum_m0B533161CA0B4D028BA64D79E2464A5E3F24E616,
	UPCEANReader_decodeEnd_mAA1710C2E0F8CDCC2C4D98337D84D4D7190CDA62,
	UPCEANReader_findGuardPattern_mABE86B29333FA6346CB06628E6BF6AD935160EA6,
	UPCEANReader_findGuardPattern_mDF210EB3D057E8E18B51113785014C9F5B714585,
	UPCEANReader_decodeDigit_m9F15C9A22192A991097A3C97FF4628A94E1DC059,
	NULL,
	NULL,
	UPCEANWriter_get_DefaultMargin_m500ECB2FE865C042C681922EC23AE920F2A182F6,
	UPCEANWriter__ctor_m31782810F11C6446DD274987BB205326A02539CA,
	UPCEReader__ctor_mFE8B3EB6C62ACD63962C700D79D425F499EED7EA,
	UPCEReader_decodeMiddle_m0DDAEA20247913FEC214F538F78DC70D1996C8B6,
	UPCEReader_decodeEnd_m25C57AE0513F6733446597C522E6E74236CF2328,
	UPCEReader_checkChecksum_m3C02FC19CF5CE90A3FBA0BF639F58E5EEF8BC6A3,
	UPCEReader_determineNumSysAndCheckDigit_mF1339E97B0C9B8300C6339DEAC605D46165B8C97,
	UPCEReader_get_BarcodeFormat_m0422536F16EC2884579534C2E1DCD9C3027BDE98,
	UPCEReader_convertUPCEtoUPCA_m6EEB800C4533CFF024F8B24F36F3A0C6BD062036,
	UPCEReader__cctor_m7363D4EFDE62630F5089E48C742EB901B699AE06,
	UPCEWriter_get_SupportedWriteFormats_m53768EC43F0858FDA3A8346630322874B6DEFF8D,
	UPCEWriter_encode_m6BDA755FECB6D152CA1743DCE76D5C0B3A16E5E3,
	UPCEWriter__ctor_m4CC21F3B200BDB1C7A868AB6C3F6C7D71F5A1D96,
	UPCEWriter__cctor_mF3FF2F97642F2F8139D1E3343E4CB204410429C8,
	AbstractRSSReader__ctor_m7442DD5A2EA646170C03F234483531030CA099FF,
	AbstractRSSReader_getDecodeFinderCounters_m79C0A2B24352B0F799407F3F6CF0F632E534ECAF,
	AbstractRSSReader_getDataCharacterCounters_mB5E2B8E45336DD163FA037B03D78EBC68D278762,
	AbstractRSSReader_getOddRoundingErrors_mB589C919393D950C449F88ECE9F815B0E144BA41,
	AbstractRSSReader_getEvenRoundingErrors_mD6ABB8DC653BF9805335A4BE503C511FF03C2A97,
	AbstractRSSReader_getOddCounts_mFB6556C24F5DCAFCCCAD1E29FF4884305080CF5F,
	AbstractRSSReader_getEvenCounts_m5A7CDD6AD88262C44FAB2BA8688DB40602AB28FC,
	AbstractRSSReader_parseFinderValue_m65508BBD4CEE1D9B225709CA03970922E44B3942,
	AbstractRSSReader_count_mDF59E3135522721189C039A5D39EC6B8A15C20D6,
	AbstractRSSReader_increment_m0F1DD2A2719AF31638A067B91BC6893021104C3B,
	AbstractRSSReader_decrement_mDAE74644C0AFA0603B984998FBAF3E1F86448868,
	AbstractRSSReader_isFinderPattern_m37F7BE21135C38BEEF9B079FAEC694AF9BF9EB5B,
	AbstractRSSReader__cctor_mA30B8A36CEAE1BD3D4D3D4D35E669261A4FD53C8,
	DataCharacter_get_Value_m60DBB849017F13B1D771C24E907849C00497D709,
	DataCharacter_set_Value_m2B073FE2A86BE0365F25333A84DD35B993E8A5CA,
	DataCharacter_get_ChecksumPortion_mD840CD43FD8ABDC679D01D19A698755EAA30D4CD,
	DataCharacter_set_ChecksumPortion_m6A5DA959D12F0A52405946B457B0298BA6314490,
	DataCharacter__ctor_m39FAE9A09CE6A41019B6C5563F94A60D21DDDC8B,
	DataCharacter_ToString_mC7BCB0F3FFCAE11838CC42A9332DE8319CE31B89,
	DataCharacter_Equals_m62E3086C2E2402D246AE720247F4A4624E5B191E,
	DataCharacter_GetHashCode_mF1D0EAEAAC95D459E6BE9D564BF9B254183D864B,
	FinderPattern_get_Value_m3A0645BC0EBC8C72D990FD4F2E454E6BEA262C1E,
	FinderPattern_set_Value_m3C1C7073A3AE8B490A856DB9687D70075E00FB73,
	FinderPattern_get_StartEnd_mF89D5ABA5B62368CE7B8B2180AB8927307B76356,
	FinderPattern_set_StartEnd_mB6505DB19671BF37C4DE88CF0E9466EB589403E3,
	FinderPattern_get_ResultPoints_m27A97739CB2E560692FE8D24834304231AD90217,
	FinderPattern_set_ResultPoints_mA31D84D4C7B9364531D2520D39B9C6138A480E7B,
	FinderPattern__ctor_m18CE10A307FE0B805C1E829538F6C58C4571D4BB,
	FinderPattern_Equals_m9B923E01E26F281612C3A1C972E08FBFC1ADFC59,
	FinderPattern_GetHashCode_m784A81D9A39F03EAB9C33978423A244A149331EC,
	Pair_get_FinderPattern_m4C6E68F603523C6B38FA1DAE2DC7B9BDA3557F1C,
	Pair_set_FinderPattern_m00BFF71B29FA5FB88B85626E1C7579497451FB69,
	Pair_get_Count_m73CA5A5EF831E1078558DCC37ADD134F0D2AD79A,
	Pair_set_Count_m87E259D87A7D1356B5F6B9CBE171FD594836F8A1,
	Pair__ctor_m5DED42D78363EA925B1DD8338F21195734868463,
	Pair_incrementCount_m4089C852B621FEC665A6B312D1992FF633199251,
	RSS14Reader__ctor_mDD7F0495B29828CAB1B0D6BF5F06E729F6D546E1,
	RSS14Reader_decodeRow_mC0ABFE466E9666A88D4B87550A049C7FC72E505C,
	RSS14Reader_addOrTally_m13D69117825796401A745AD7C27F0A6D673E3860,
	RSS14Reader_reset_mA389232DE62C1BB9F05535189528093860BC8E35,
	RSS14Reader_constructResult_m07F22DB5C3163A6DDD72D6A7178DC131515FC99B,
	RSS14Reader_checkChecksum_mE71657476A58F451533CEB56FCD62F7CC848A42B,
	RSS14Reader_decodePair_m055785C6AD3E4CC823E58EB95C219456B378AE11,
	RSS14Reader_decodeDataCharacter_m2E7C1DDBE209BD932D6A35A4395A86528FDB4D29,
	RSS14Reader_findFinderPattern_m7C6232348ABB28DB3C92F21CB6497FDA31D4C2AE,
	RSS14Reader_parseFoundFinderPattern_mC62D6CA489B9662306A5D0DD9A943BAB7CFC5A2F,
	RSS14Reader_adjustOddEvenCounts_m580669B07EE4470AC2AFFE4C9CB5E722E48C4CE5,
	RSS14Reader__cctor_m3F54AC9DB55AD8EECE0101E3540E45805D56EEE1,
	RSSUtils_getRSSvalue_m954F809E4F6ABB6B1F59EB52A79D39D8770E3543,
	RSSUtils_combins_m856C8BC3DD9CEE5CB91FE185A98867F58941C908,
	BitArrayBuilder_buildBitArray_m75CDADD7CD1A3D516E1FDF483D53AC6909EFF981,
	ExpandedPair_get_MayBeLast_mDEEC63E98670E3EAD9EAD0D81CCBA82F676F9528,
	ExpandedPair_set_MayBeLast_mD1345D00041D749799128E122E2496C419DEEEA1,
	ExpandedPair_get_LeftChar_m2A34B6E652E541B2E944EEB939B7830207374532,
	ExpandedPair_set_LeftChar_m4B9BB8060792611F7C799153DF391402112FEEDF,
	ExpandedPair_get_RightChar_mC504EBE9DB513B577DFB9868AA4AED67C8711EFA,
	ExpandedPair_set_RightChar_m09256C170B4B33144A81F724C23BA9425DBAB8D7,
	ExpandedPair_get_FinderPattern_mD465D0901090E9FD91A3C4F1D57662139614F4E9,
	ExpandedPair_set_FinderPattern_m41DEB17AF11FE89854A25582A55E032A426DD7B9,
	ExpandedPair__ctor_mAA2B89881E05C1587AA5CD90078FB8C1A395EE98,
	ExpandedPair_get_MustBeLast_m4F63330084A3B4A61C6EDE172F94C39C7ED2A5DE,
	ExpandedPair_ToString_m6BCF55F1944B6352F4E4BD5BBD5D7B79BFFD1AFE,
	ExpandedPair_Equals_m1A1DCFC7B34A083FB641447539C6A259814B2D32,
	ExpandedPair_EqualsOrNull_m24E584E00CE2508B34BE621670E8B9F5BEEF3B4C,
	ExpandedPair_GetHashCode_mFD91B216166AFDF22AA75F1BD379EA0BDE8026B0,
	ExpandedPair_hashNotNull_mCD9324E7358320B6C1F9D6CC515F5514F8F8F101,
	ExpandedRow__ctor_mD7D483A05A80B41E98727B16A00EA5AFBC3F6D35,
	ExpandedRow_get_Pairs_m773AD1334B5029D4246571C0197B59E3CC5973FD,
	ExpandedRow_set_Pairs_m111B020A54781C87F28CEA41F81F42E98F438AFC,
	ExpandedRow_get_RowNumber_m6E5CA96945FD6C80B5CF3BB5ED7FB9C51FF43837,
	ExpandedRow_set_RowNumber_m8FD41BF6861C044AD174F95E602F5FE67DF04018,
	ExpandedRow_IsEquivalent_mA7F8A895B8DEC50582B2EE21941BA843B53A449B,
	ExpandedRow_ToString_mFF6A9D2B9B272ABA260306B4059A33145E398645,
	ExpandedRow_Equals_m7418228BB6EF6528EB5696AB81818026D903F02D,
	ExpandedRow_GetHashCode_m3F438835732ADC7DD53454D7E03AEDAB518DCECF,
	RSSExpandedReader_get_Pairs_mC8DF1D8DC65C63EC10BD89F46B522F8F54E548AC,
	RSSExpandedReader_decodeRow_mEED65BC509A20E80753B59D47BCE1673D4ED6091,
	RSSExpandedReader_reset_m40774DB7711C71C753DC8E3E4809EB7AFDA9EB0B,
	RSSExpandedReader_decodeRow2pairs_m32FBF346869303961B237BFA200F10D12CE36D98,
	RSSExpandedReader_checkRows_m5197D7FA2D2CD303AE56D86A83BEF92672B06351,
	RSSExpandedReader_checkRows_mCC057C063CFDA09896BD4AA62DAA9608B7FD3516,
	RSSExpandedReader_isValidSequence_mC8DE2CF465238E90F785A9C85A3DF466187B9042,
	RSSExpandedReader_storeRow_mF70DAE9080CBB444CA1EC32593B98C873CC0EBE9,
	RSSExpandedReader_removePartialRows_mF6A5277FA8983147CF04BA721CB2C51C978585AD,
	RSSExpandedReader_isPartialRow_m5BCD933001D553396D5FC4F99B46B00858F4F120,
	RSSExpandedReader_get_Rows_mA24D47E18B1D176279224E87EF865C1EAA9921F6,
	RSSExpandedReader_constructResult_mCD2C146B8F64E3CAE15AEBE18F5D317FBE19496A,
	RSSExpandedReader_checkChecksum_mDB81344E8E2F137E8D461667D0DE44580AA6BC76,
	RSSExpandedReader_getNextSecondBar_m366EBB26FC87D1F590765D9CE7B8A3692E820B46,
	RSSExpandedReader_retrieveNextPair_m74D13489DF4090508B9F8F1E13EEFAD69CE49976,
	RSSExpandedReader_findNextPair_mD42022C11DC0B6C3F8C65E2122908A2617AA1ED9,
	RSSExpandedReader_reverseCounters_mA91F8A2834A1B636F7BCA46024A7F108E740D1D2,
	RSSExpandedReader_parseFoundFinderPattern_m282307979515A4224F7DF00D7A11E62AA7AAC0D2,
	RSSExpandedReader_decodeDataCharacter_m305668EB123EA575CE3765DAD9682B314E7CC45B,
	RSSExpandedReader_isNotA1left_mA2E3477E9B8B61D7AF94EF9C5E0FA1F733ED36F1,
	RSSExpandedReader_adjustOddEvenCounts_m000B4A540D7ED89B024DCA731E97993B25FF1D34,
	RSSExpandedReader__ctor_mB70A81ABAD6834435A906B891A424AC0C984FCF1,
	RSSExpandedReader__cctor_m32BDCCD1DB72A2E8DDD3EF491EA9E460FAFCE32F,
	AbstractExpandedDecoder__ctor_mBB1B3E222CEA936145C0E893557A3526AB26E432,
	AbstractExpandedDecoder_getInformation_mF824E4822198393A925050EC92465441AA8EFC3A,
	AbstractExpandedDecoder_getGeneralDecoder_m7D36E37B49F12AF10B6B87486562E72C32F7505B,
	NULL,
	AbstractExpandedDecoder_createDecoder_mC63D4488C616AB3BB0FC4A5D4E5A63741BD7EB61,
	AI013103decoder__ctor_mB4D0A5A6EE5E88AC5EEEF5BDBFE763072875A5BF,
	AI013103decoder_addWeightCode_mD57C994E081D1BB1DCC92F3937D530FAD0E86797,
	AI013103decoder_checkWeight_m6E3D79E07F48CBC37B2F6299E24A4202663E66D7,
	AI01320xDecoder__ctor_m4AD46F3BFE12FF4EDF19CBABC87976A5E33B0AA1,
	AI01320xDecoder_addWeightCode_m78652EAAECBB4E85E587FA865511A3977FE38B67,
	AI01320xDecoder_checkWeight_m21130C4617763518BB2A76CBBD103629E74DB255,
	AI01392xDecoder__ctor_m263ACBCBDFF9206A8E41EE60613769BD4324B03E,
	AI01392xDecoder_parseInformation_mCF373CB672B6DA156096895A25FE10283FDDD70B,
	AI01393xDecoder__ctor_m2D9E860B37866C11902A9BA47A84747D2D22E6B8,
	AI01393xDecoder_parseInformation_m7616FD3924449023D1B771B632F7105A429CC31E,
	AI01393xDecoder__cctor_m3694D6D50646FE982669689DED98377EED699D21,
	AI013x0x1xDecoder__ctor_mEFD9461A3724C0CC178D40B9D85EF96F4297B2EC,
	AI013x0x1xDecoder_parseInformation_m39DF0A42AD24E960A4C00A257C2FDC30EC777620,
	AI013x0x1xDecoder_encodeCompressedDate_mDEE0EC9C29E801B9100A0B970F28BE795F9B3B94,
	AI013x0x1xDecoder_addWeightCode_m7693C9C9EA1B5EFB12E983DBEB8B08C0BECA5589,
	AI013x0x1xDecoder_checkWeight_m88CDE391946929FAA54DFC25E87D1BB1A7C14F7C,
	AI013x0x1xDecoder__cctor_m2FC0EE28ED5A97DB53C631C2D80E8F15D0BFAF38,
	AI013x0xDecoder__ctor_m3E5D9577944AB56127AED5CAFFACDDC048BC9957,
	AI013x0xDecoder_parseInformation_mC72E61AA9A1C92C89C220FEDE8D7B0252A9EF74F,
	AI013x0xDecoder__cctor_mE53DF1837E302E14F14D45FCD89E3FDF7B5C76DA,
	AI01AndOtherAIs__ctor_mB6F2CAF30F2486CC244CA5C4B80C4A38E3799785,
	AI01AndOtherAIs_parseInformation_mCAC698F33C5BCC697E3E63481C412BC79C632256,
	AI01AndOtherAIs__cctor_m9A4F14FAA74413314C92C1BD52A8E639FF81D830,
	AI01decoder__ctor_m4350FF8DCE7EFAC08F06E25513A79C476A38C249,
	AI01decoder_encodeCompressedGtin_m3CE89DCC6CCF6D08B735F6BF7DA8D53E604633EB,
	AI01decoder_encodeCompressedGtinWithoutAI_mCBDF766FE45277B453B21A90A2286CF652D581F4,
	AI01decoder_appendCheckDigit_m41CE3FB19E4A200E731E19EBC59E32EAD5CAA76E,
	AI01decoder__cctor_m5C1722D1E0BDD9F53A0F249106FE06D4B9C25B55,
	AI01weightDecoder__ctor_mF32AB02F3115046F6FBF702A384D2C45C16CFC08,
	AI01weightDecoder_encodeCompressedWeight_m7501C476349F6259F4882B7C6EAE62217CF2147B,
	NULL,
	NULL,
	AnyAIDecoder__ctor_mB22F1D487F9F185EB4D67C7ACF88CA67FDBAFBFD,
	AnyAIDecoder_parseInformation_m0887B37E12AF783107DE46C47543E7EBF43197D2,
	AnyAIDecoder__cctor_mF0F937FDBBA18F040CAD63A6A393DFCC2A19DDE2,
	BlockParsedResult__ctor_m7008583A7874816E7AB2406B71CF1E563C199493,
	BlockParsedResult__ctor_m1616FC6AAF1B6E1C4C8ED5A79F83AD4D2D7B144F,
	BlockParsedResult_getDecodedInformation_mEDC5DD26A8504DE8870BD356D4C84F5059C02623,
	BlockParsedResult_isFinished_mE4E102B61FBE11FAB8D1915FD85DB93809334848,
	CurrentParsingState__ctor_m0D654924041BE81BC993EB44E458B338BF4436BA,
	CurrentParsingState_getPosition_mE905D11AC452ECF19B2B3C7281DC937299284DB5,
	CurrentParsingState_setPosition_mDD68F687E15D99F731A1F83E2724DD3C2EB9C142,
	CurrentParsingState_incrementPosition_m46BE6180A4757A84CE868CC17CDE83681AC58F2E,
	CurrentParsingState_isAlpha_mE9616B2B2030644E02FAF5711C2503CC35C0AEC4,
	CurrentParsingState_isNumeric_mA7D0650985648E1AB7813763F786B56BA2B3E86F,
	CurrentParsingState_isIsoIec646_m38A0BFCA5BD9F39E1BFE189BE703B1F88DCC1C54,
	CurrentParsingState_setNumeric_mB4890E2837DBCB8A59C03A0F953BF4E077F21DBF,
	CurrentParsingState_setAlpha_m3A114FF6C9F6D1A3A5F4E17F793CDF77423DAA40,
	CurrentParsingState_setIsoIec646_m0CA1B7A33D5AE9E5FC675298BF47015EDEFD15DA,
	DecodedChar__ctor_m4ACD230659EDDE1D263ED37F351E900B5543AB9C,
	DecodedChar_getValue_mDC614458975467F1EC662E04B8806C56BA576CB9,
	DecodedChar_isFNC1_m55BC10B8535F425F8EDD328AA33E70A4C5ECF76E,
	DecodedChar__cctor_m14D32C03D7F0DBEB91372D926226B555104495A8,
	DecodedInformation__ctor_m14F0A67C61F1CFA1AE8DFD3B326408B4DA0D96A2,
	DecodedInformation__ctor_m45AD0C128F665B4FD45A8AA8B2DEFDE68185414D,
	DecodedInformation_getNewString_m2111407862B5AC807879C717A87BB9DC1749A615,
	DecodedInformation_isRemaining_m92D8CC45E2DAD69EC38802581AE355D786D05BB9,
	DecodedInformation_getRemainingValue_mB1655DFFCDC8F569E13430F009C5C93EFE0FADB4,
	DecodedNumeric__ctor_m11E0096502B96C1AB40D91789A8C6073F3093AED,
	DecodedNumeric_getFirstDigit_mD8B952C89C43DAD9EBFB383353BAF71276526670,
	DecodedNumeric_getSecondDigit_mDC760D5BFE6D2250BCC2BE38F199AB2BC88B65AE,
	DecodedNumeric_getValue_m49EA4EF5A10BC57CD906D80E6B29C24A9B037DC9,
	DecodedNumeric_isFirstDigitFNC1_mD7EC5CAB758F5ED4704E391195B6B72298394CDC,
	DecodedNumeric_isSecondDigitFNC1_mE6D32C215B87E0F45EFF1FDFE684F846F83D7098,
	DecodedNumeric__cctor_m3D3F764A854DEB245DBCD15E8D78C96CD6D4B506,
	DecodedObject_get_NewPosition_m6661B582A9AA428F4A1928FEE5697EA0DED4E9AE,
	DecodedObject_set_NewPosition_mD5F05B001E48031279B3FBF785E585C881005708,
	DecodedObject__ctor_mD0F5992038C972F0882F0686D1FCC17CCE69CB70,
	FieldParser__cctor_m52563E3F5E39EFFAF9F666353F46B4FAE474B24C,
	FieldParser_parseFieldsInGeneralPurpose_mA6AD30303EAEA45EA7C677AC1359FE175B9E3264,
	FieldParser_processFixedAI_m4DF47E04DFDAA5D48E0DBF3DA7CBA10C379DEB25,
	FieldParser_processVariableAI_m2FA71FC7883104D5225EEE1966F52883A3454840,
	GeneralAppIdDecoder__ctor_m2AF7B7306EA26CEBAB2EC73695160A07D55F0246,
	GeneralAppIdDecoder_decodeAllCodes_m25AD982DAC4682BB19FD67B3E3569330D82F7B39,
	GeneralAppIdDecoder_isStillNumeric_m3E7449B9F87C18D4A3DF9243620C9FA5CBD97820,
	GeneralAppIdDecoder_decodeNumeric_mE25F62A6236DCF826663DE57E79B49DB874983E3,
	GeneralAppIdDecoder_extractNumericValueFromBitArray_m01B60D593D7168ADA36EC09C3CFB1D1E2E03066C,
	GeneralAppIdDecoder_extractNumericValueFromBitArray_mBE16ECC0479492D03F407539CD0126213D21E15D,
	GeneralAppIdDecoder_decodeGeneralPurposeField_mA5A24B57F3A223F26CCAB22F654A92B51AEEB638,
	GeneralAppIdDecoder_parseBlocks_m86E51BEACAB43FF27C9553BC4A0F6FD8FA130EA0,
	GeneralAppIdDecoder_parseNumericBlock_mD6DFAE70980A0DFFA33C65ADFE572CE3E03E30B5,
	GeneralAppIdDecoder_parseIsoIec646Block_m0E0D3A5D2D2C04B54881847F1DD92E8A3B35D59E,
	GeneralAppIdDecoder_parseAlphaBlock_mBA72BDA3D6215106164942B923A7C338932BD21C,
	GeneralAppIdDecoder_isStillIsoIec646_m752106E72C6B97FB651A89945750A1C8E5D8C214,
	GeneralAppIdDecoder_decodeIsoIec646_m76F366C18B4B94519A67A9DFE0FEBF64FF63AC50,
	GeneralAppIdDecoder_isStillAlpha_m2600D102EF86CA0C40C87F2474CA36CC62C233A2,
	GeneralAppIdDecoder_decodeAlphanumeric_m35282F7E1E1C42587809E03C3DE8457757CAD122,
	GeneralAppIdDecoder_isAlphaTo646ToAlphaLatch_m10A4E09629D26B9A23EB987BE45CD326A6974903,
	GeneralAppIdDecoder_isAlphaOr646ToNumericLatch_mF9F98BA1D0F74FCA7929F0352E60C3F1C599F97B,
	GeneralAppIdDecoder_isNumericToAlphaNumericLatch_m1696C53567BD18DA707815455484F3576ACFD7C7,
	ByQuadrantReader__ctor_m587BCE0E8852D76BB3895B70316883D7C3C6331B,
	ByQuadrantReader_decode_m2075834B22CBEE68A98ABC6F83AB3ED636A7510F,
	ByQuadrantReader_decode_mBB8324ED103B78BF0767E309C480C8469D8F8CD1,
	ByQuadrantReader_reset_mEDCBC22EDA0A588E10F700E287D34231031645EA,
	ByQuadrantReader_makeAbsolute_mB90847985F30BE15697F647FC8BB81988114029B,
	GenericMultipleBarcodeReader__ctor_mA7EC203EACCB76AABC0A70DA30DEF4A0418BDBCD,
	GenericMultipleBarcodeReader_decodeMultiple_m171BA20B09F8CF8E6AB18615643107170BD08B3E,
	GenericMultipleBarcodeReader_decodeMultiple_mA71B236451E4B1323A9A6F4EFDC6AB2B5AEA0A89,
	GenericMultipleBarcodeReader_doDecodeMultiple_m56A5C9EE86AE478E87FA2855AB9B623F288382D0,
	GenericMultipleBarcodeReader_translateResultPoints_mBDE02279A4290B78D5C3D4418A818DE7E0B88467,
	GenericMultipleBarcodeReader_decode_mB3C53F5AD41AA4C5FDA24CAE7BE4F4B469B2191C,
	GenericMultipleBarcodeReader_decode_m688C9CD4F0A7CEDE6900210F844FA5C2FBD0E16C,
	GenericMultipleBarcodeReader_reset_m0BE56AD34166F48A3B7B4031597EE2417C0106DD,
	NULL,
	NULL,
	QRCodeMultiReader_decodeMultiple_mABBCE57DB00B74DBFC27F09501D176F51B005444,
	QRCodeMultiReader_decodeMultiple_mF06B4A7196D5E6159173697D9DECEEFA992C1892,
	QRCodeMultiReader_ProcessStructuredAppend_m0763E6EDDA2F6C568418F690749083FECF418AA7,
	QRCodeMultiReader_SaSequenceSort_m7E8230B1C12AEE2EB0FB967C406D69A7DB3A0816,
	QRCodeMultiReader__ctor_mA645C1D5C68109C7B83EEE198FA8FF5ECE0FFDFD,
	QRCodeMultiReader__cctor_m7940B87F9491A93EF2CBBFCB303B69E837376B45,
	MultiDetector__ctor_m87C735618A0D0E15E1280B212E5949A8EEEFFAE5,
	MultiDetector_detectMulti_m6E750ED533AC8C71F4DDF4752A68A746905392B7,
	MultiDetector__cctor_mA49F47DE36402C6D92D48D8B3D945CEE31068285,
	MultiFinderPatternFinder__ctor_m582E8448778E37F0AE910C3D231DFC83ED17FCE3,
	MultiFinderPatternFinder_selectMultipleBestPatterns_m1EF946A27BDB5140FB17F33B599392B751112EB3,
	MultiFinderPatternFinder_findMulti_m51A729C27260587D8195B1C64FB530BF1D5B839A,
	MultiFinderPatternFinder__cctor_m0763043CBBCCD9FBBF7AE6C069A643C74970F0D0,
	ModuleSizeComparator_Compare_m6AC37A4356EA4CD59D90B81CE64CDDB17C7DF57E,
	ModuleSizeComparator__ctor_m3AF6535F92E114F29CD5E9ABBE64CC9C99676A40,
	MaxiCodeReader_decode_mD71AF3C834F5AA114A4445272B6E7C8518D85715,
	MaxiCodeReader_decode_m730A0FAB41B5EC323A4D45C7B1E7089A5547C6D7,
	MaxiCodeReader_reset_m98C2685C959A696973CF4B2DE01A6AC9FDC32442,
	MaxiCodeReader_extractPureBits_mE878FC5711EA47220B6923D197CA4016A9803DEF,
	MaxiCodeReader__ctor_m3309C54636F333D09FD215CF6C9B6479667EF4B5,
	MaxiCodeReader__cctor_mAFC8509B7C0BC9027C40B1BE0F47CDB80C11868B,
	BitMatrixParser__ctor_mE20181816ABDA5877569B8977AC0E4E04F4C23F2,
	BitMatrixParser_readCodewords_m789CE1C02BCB632366F8B80EDF85D76085D73723,
	BitMatrixParser__cctor_m1AB5E78EE0175BAC16B59F86D4AEA75BCBB1E046,
	DecodedBitStreamParser_decode_m87384708552DB6C158866A09FE9D6327B1D20887,
	DecodedBitStreamParser_getBit_m5A13627BF7B9771AEBAAAA854F3AEB58A4740836,
	DecodedBitStreamParser_getInt_m0AB19A2556B318D5EB825F8C4FEBDE25A3E99EC4,
	DecodedBitStreamParser_getCountry_m27288041061FFDF7D757C6AD7D2BFBD2A9419885,
	DecodedBitStreamParser_getServiceClass_m23DB86FA7CE9155AE5077747642345DC3B20B59D,
	DecodedBitStreamParser_getPostCode2Length_mC74C6ABCC0C90A7B7952E67E0805D77D60EFC3CE,
	DecodedBitStreamParser_getPostCode2_m2D42E8C8248B7D1F26B7097FE6FF071F87F3460F,
	DecodedBitStreamParser_getPostCode3_mDEF6245C182FBFAD251F8994D387693199AAEA70,
	DecodedBitStreamParser_getMessage_m3B5C534BB3F77ABE34432042FA6FB7FCC25F85AC,
	DecodedBitStreamParser__cctor_m150A7ACC8803C679534DE48B4208DC9FE874A62F,
	Decoder__ctor_m67D9A559880C0DD66930FCE08070230E550C3988,
	Decoder_decode_mB723528CE96D3B7F7E2A427E73AC8AD8E951309A,
	Decoder_decode_mEA1DEBF419A79FBDAFE8EB3CA0749471FD2AA63B,
	Decoder_correctErrors_mCF27E615A343FCC7FCFE46956DB45407F1DDFF18,
	IMBReader__cctor_m154E808405E34843AB0D5AB06A6C461D32C2FF80,
	IMBReader_doDecode_m2F0E3A95E2A683F49EB38759E58831A14122D56D,
	IMBReader_reset_m57C18978BEAE3CFADAC18BD37E258968DC76DB02,
	IMBReader_binaryStringToDec_m1E6B4FA27D8011E4DF97DA27F31EFDFD19AEB1A6,
	IMBReader_invertedBinaryString_m97B3AA2EE5E9AE0AC4DE259CBE0244E20E33358E,
	IMBReader_getCodeWords_m5213F8081647393BDAEBEA7FE614A807B755C8FD,
	IMBReader_getTrackingNumber_mDC9BFA36B8F6F02FF22820CD265B85CF8CEF8768,
	IMBReader_fillLists_m61C87F31D568329B66FD27C47BFDE1A47AB86B08,
	IMBReader_isIMB_mED7DF4B013F2CD489A065E815141075089AF4408,
	IMBReader_getNumberBars_mB0086A92D383931429C6D93683BEC5F50342D3E8,
	IMBReader_decodeRow_m4E715BD69F16F15EFFDEA6808D92408803F4F73F,
	IMBReader__ctor_m9F38E54F9F7E044BBE28650B49A2763EE85C1189,
	DataMatrixReader_decode_m680149836C044D3E055979BBFF61FDE59EEF58E3,
	DataMatrixReader_decode_m47350828549BDAF16CF2E7963F4769AE2E02F593,
	DataMatrixReader_reset_m4B2F552B5BEF16C14F74EFF67614DEAE5A61C660,
	DataMatrixReader_extractPureBits_m9E0BA73AAD73E5092B8289E9273BECDD8F1F1A19,
	DataMatrixReader_moduleSize_m8A385B2D5644A0BFE9DE90B2A7B4D225AE9672E1,
	DataMatrixReader__ctor_m4B01836FA21E00C62ADA2A653CB2B569E617A185,
	DataMatrixReader__cctor_mEA43C442BF5C99A5A0855D32B2789071A8384748,
	DataMatrixWriter_encode_m89AC74733EEEB39C54704D641F31DBA669983178,
	DataMatrixWriter_encode_mE8BEC0BCEC42AB7CB8EF1F191C305E88AB449EF8,
	DataMatrixWriter_encodeLowLevel_m06DFA4AE7541587A40B649EEC1F3AB22323AD03F,
	DataMatrixWriter_convertByteMatrixToBitMatrix_m856357FB7B0F295AECC589AFA67F9A460FE1237E,
	DataMatrixWriter__ctor_mDF8F61AD5D30A55A36FC17A0A9A8313B1388BBB3,
	DatamatrixEncodingOptions_get_SymbolShape_m914646B1E4B47D4AFA15143570919B495DD11DDF,
	DatamatrixEncodingOptions_set_SymbolShape_mA583FE99C76D1678F294A62F3C46A04673F01976,
	DatamatrixEncodingOptions_get_MinSize_mADC06B94795A51E631CF3CD45DCD0B6D863677A5,
	DatamatrixEncodingOptions_set_MinSize_m3A1871549778C96894FB85A078AC422A09BBD67D,
	DatamatrixEncodingOptions_get_MaxSize_m21522EF87A9998E595119EADB40B28DF6B7CFC7C,
	DatamatrixEncodingOptions_set_MaxSize_m01E7D4676DA9E00607288BDECB9BC4DF14165170,
	DatamatrixEncodingOptions_get_DefaultEncodation_mD60FAD9806CC0BF8FCB7F06F617A7AF3E60E45D1,
	DatamatrixEncodingOptions_set_DefaultEncodation_m13F82D8391B5785E13074B1063F0976F6DF16C14,
	DatamatrixEncodingOptions__ctor_m2F8A24CD4BEF623D33894F88F6B32FBF54CCEFBF,
	ASCIIEncoder_get_EncodingMode_mB3FD8167FA8BC7CCF6B7E828D21A7640E7B3BBD5,
	ASCIIEncoder_encode_mF9905BF3C9CEB5FFA039A69D4F2A366E8FBFAED3,
	ASCIIEncoder_encodeASCIIDigits_m12E8E0DB272078CACDE4C6ADEE65A6B84584A380,
	ASCIIEncoder__ctor_mE4317540C046ACC7281EDC032AB3727F526CD8EA,
	Base256Encoder_get_EncodingMode_m615F040653359B2506ECA6DB9F565C2CF71D252F,
	Base256Encoder_encode_m2F8061D6B0CBB7B736D186583692F24B84BFAAF2,
	Base256Encoder_randomize255State_m7AB5A958F77625BDC1436969867585F83E8127E9,
	Base256Encoder__ctor_m12BFEF901AEFF0FEA185CCF4A23FD660C931A212,
	C40Encoder_get_EncodingMode_m721D9EB23989D30921E4BF1FB09A4DB3119675AF,
	C40Encoder_encode_mA374CE97501CA1647E0563075FC1666CD1CD2D87,
	C40Encoder_backtrackOneCharacter_m7F7B5FAB798E99C5C5A0B8D23F15D0BBF9C89236,
	C40Encoder_writeNextTriplet_m2FB9D1BD13C372A7DB92A233F923E8F04A76FEB1,
	C40Encoder_handleEOD_mB68864C35FBB992B231C86EBBFD7E22C706C0098,
	C40Encoder_encodeChar_mC7946AFB8A04C547D3E84F52A7B75C9BD54F8C35,
	C40Encoder_encodeToCodewords_m7CE295FB46B3139D3301B0AFD92532C1C674B84F,
	C40Encoder__ctor_mDCE4C825212972EC2EEB9A34C0ABF3D36E27E3B7,
	DataMatrixSymbolInfo144__ctor_mC437363A4E22A5A6595CE073C0D51FA89A114540,
	DataMatrixSymbolInfo144_getInterleavedBlockCount_mBE710D72C18676F2E9A293EA6F552F1F769AA647,
	DataMatrixSymbolInfo144_getDataLengthForInterleavedBlock_m8A8303EBCA4680FBBA92D66D642D1007865D25A2,
	DefaultPlacement__ctor_m297E56E3796FD82FF30D955D074EB2DE0EBF90E4,
	DefaultPlacement_get_Numrows_m7CC74A53D4805FC3AA832C003AFFC6995A815BBC,
	DefaultPlacement_get_Numcols_mEB9F9D953AFF1BBF52B89DE78BB6CD3207D841D8,
	DefaultPlacement_get_Bits_mDC6B617E55C0CB0C324588F0026F4AB422D4A190,
	DefaultPlacement_getBit_m635974B2179B729BC03FE95A5D5C2F8279EDED56,
	DefaultPlacement_setBit_m9DAEA022C6231A85945A9F2DB56F4943D013A177,
	DefaultPlacement_noBit_m5B0102E87A04D9819690EAFF06FAFFE604E6DB3B,
	DefaultPlacement_place_m8F16677902BF1478B3BA834873639D9FF5B8F926,
	DefaultPlacement_module_m898D35F0109EACCABCFC8CC0E679A18DE5AAC6B5,
	DefaultPlacement_utah_mEE89A156D30BEB3AD4211D6B7AF73E9D8C6C7235,
	DefaultPlacement_corner1_mAE1C394FF59F8D177FA92DD1789919937432B725,
	DefaultPlacement_corner2_m880CD8D061466AACAEA3BD2DE8763FAB87E74AAA,
	DefaultPlacement_corner3_m4DC378F924CB5DC31F42C33F29B5172F28E4954E,
	DefaultPlacement_corner4_m099DB9D3A75EF29D142E75403E9D63331FC2ED22,
	EdifactEncoder_get_EncodingMode_m09A4625E2D3A8AC14F1C5DBA4949B9C6CF10075F,
	EdifactEncoder_encode_mD15CE61AE174EA1FABAF882CE3045B1CBAC98A9A,
	EdifactEncoder_handleEOD_mBE811765A0A556D58435AAB569D7BDD7955AF5B1,
	EdifactEncoder_encodeChar_m73DE86DCB647341C91FCA211A17F6302C05A2A4C,
	EdifactEncoder_encodeToCodewords_m413A7062BFE181ECD6B17FAA2898FD17686693FA,
	EdifactEncoder__ctor_m4601FE839B37AD8A9E531DCCFCF95A11C531A416,
	Encodation__ctor_mAE4959052BB100A1FE3A31728DA4A881EB5AB6C2,
	NULL,
	NULL,
	EncoderContext__cctor_mE8F317B0A62F6CB72F2D3C2E6875A1D251F7CF9E,
	EncoderContext__ctor_m3A9D1904BBC631385A86D6ECE5A9B42CE14CF040,
	EncoderContext_setSymbolShape_m10DC861EC36EDCC4FC1D770258DF8F9B4EECE63C,
	EncoderContext_setSizeConstraints_mBEEA9308933DFE9B57E1CA4B5F696AECED3D751E,
	EncoderContext_setSkipAtEnd_m50BE20756B70956262BEA812C6C2305688594DDF,
	EncoderContext_get_CurrentChar_mE84D6717C4E049CC3423F528A03F68EA10056E3A,
	EncoderContext_get_Current_m46D5FC6A2DD2155F83F74B8BC0A1E275FD7E8A33,
	EncoderContext_writeCodewords_mF04897B2BE6D9A0F130A9C01126FB97A2939F18F,
	EncoderContext_writeCodeword_mDF4D6B7C08E2922A8C9E8B56A3C8930AA42C6FCC,
	EncoderContext_get_CodewordCount_mD0CA58B49F7199DC3DE45721DDE2F753D0427830,
	EncoderContext_signalEncoderChange_mBD24176BD2C05E133A92C7BD4D708E5C66F8D9B2,
	EncoderContext_resetEncoderSignal_m7FB4A82E70B338FCDF2601B9EA36ED84B79A1117,
	EncoderContext_get_HasMoreCharacters_m1AAF96EF23CDBEE32B44D08A31B0DAC397C43C72,
	EncoderContext_get_TotalMessageCharCount_m85C3032DBAB9020207FAF99E4DCDA6D2D5E00D6A,
	EncoderContext_get_RemainingCharacters_mEAC8FAC64FF422FE0D8ED1675DE41874BEB57819,
	EncoderContext_updateSymbolInfo_m5F70C79A1E8DEB6A061F7E467C784C365C885DC2,
	EncoderContext_updateSymbolInfo_mD234191E900456A14B9BD7B164BF58CB33D3F39C,
	EncoderContext_resetSymbolInfo_m288D569C1F238C80B6861222018A953B0EF27629,
	EncoderContext_get_Pos_m190B315DEEB38598F4D38E008CBB3AABF0020150,
	EncoderContext_set_Pos_mBEEA215DF89CCCAA273268F5A398760963ACC672,
	EncoderContext_get_Codewords_m5F219B3DAA2082F1F029D4F09A578C16E8E4965E,
	EncoderContext_get_SymbolInfo_mFB0DAB05E41AE895D9C07B997009860E00DB7AD9,
	EncoderContext_get_NewEncoding_mB7D2C10FB5FCB774EF8E1701504F2E0791312010,
	EncoderContext_get_Message_m1E332A566A2D3792A23598E4A7A38F6BF2E628D6,
	EncoderContext_get_Fnc1CodewordIsWritten_m0A3B6778EADB1F88543B1C57244A9DC4690FE6FB,
	EncoderContext_set_Fnc1CodewordIsWritten_m64AAE62F4D46BBCAC6CCD3CA6E1D371B426E1FCB,
	ErrorCorrection__cctor_m07DBD8901D6818C9B85677B73A0102BAD0722BC3,
	ErrorCorrection_encodeECC200_m09F6C2FD040036E2218F24DBE887C66045AF16B4,
	ErrorCorrection_createECCBlock_m1D24779EDC2CD8491C87395A0DE337FEF82F0F07,
	ErrorCorrection_createECCBlock_m53D97940002E0C53F4BEE3AF9162F26E73B8B815,
	HighLevelEncoder_randomize253State_mDE1FFC45043EF18FD4475C45828FF0720207CB70,
	HighLevelEncoder_encodeHighLevel_m64AE463C282BD80D2ECA1950A25BC2CC85FD261E,
	HighLevelEncoder_encodeHighLevel_m1D35F724AB1F61FD937D94801557E55EEF202CE3,
	HighLevelEncoder_lookAheadTest_m06B7BD49ECBE30A5625FD6EC6F6FA880A38328E5,
	HighLevelEncoder_findMinimums_mE7B8B2850A7D2C968EE47D7A452868DACA3562D1,
	HighLevelEncoder_getMinimumCount_m67DF4D33CB29C56C5212C8FFABFF9C22D14B7A4D,
	HighLevelEncoder_isDigit_m2752D8C2999FE044D4D27EF1AB17382BAD5D7B11,
	HighLevelEncoder_isExtendedASCII_m0B78F51A4669782CA0B90886DEC7534B1E1B8178,
	HighLevelEncoder_isNativeC40_m604E3300097651DF0B2A6E9111BDAEF5780B7060,
	HighLevelEncoder_isNativeText_mCDC2A8A6537209929A38607972000CC9C08103A0,
	HighLevelEncoder_isNativeX12_m812EF23F11D13ECDD0B3E9C8773F169A7294DC45,
	HighLevelEncoder_isX12TermSep_m0524DDE5DA3F738AB79E4BD263E6161A1ADA7FB1,
	HighLevelEncoder_isNativeEDIFACT_mBD0CA9AAC8E38AE302B262F7C0CA5642F6AF184B,
	HighLevelEncoder_isSpecialB256_m2D32F9D933B7438A8A5421346A8B98F96668FE9A,
	HighLevelEncoder_determineConsecutiveDigitCount_m596276F6B096456459117767D870C7839EA7D105,
	HighLevelEncoder_illegalCharacter_m03D2BC640651D7B2E2C6BF3B419BB7465192E3D6,
	SymbolInfo_overrideSymbolSet_mCBE5737F02EA2C9EA0E8F8F84F13486D993522FC,
	SymbolInfo__ctor_mB40B5354F4C9CB4C844A24F108407DF6EFB67BCB,
	SymbolInfo__ctor_m3AFEB74FEF954B550F225B52EB044F9D5DC7E62E,
	SymbolInfo_lookup_mE06B4674441FA22A97D600272BCE450F20200E7C,
	SymbolInfo_lookup_mB1F91F2A206456DDDB2D50CFFC018F5163799FBC,
	SymbolInfo_lookup_m9FF5D72C3E3E9FCBF697034361F3EE2E453F4FFD,
	SymbolInfo_lookup_mB3693FD6A3DACE99F749415B0A5EFD6160F00414,
	SymbolInfo_lookup_m0BB75A135073754A1719F292339C5C71AAC18FC4,
	SymbolInfo_getHorizontalDataRegions_m0067F9B1B5E1D1A900067411DA8855FF07875DCC,
	SymbolInfo_getVerticalDataRegions_mD6AFF12B03A3EEA5AD9F02BBD8D79CBFE7272864,
	SymbolInfo_getSymbolDataWidth_mE996BA35548C931627CFB869C4768B4AE5DC26D6,
	SymbolInfo_getSymbolDataHeight_mC022AE234B64A1F5467982D879CB168ADA0AE0B8,
	SymbolInfo_getSymbolWidth_m14AFC048263EA0E994A8F0CE82E9A9BA5EA8B8B9,
	SymbolInfo_getSymbolHeight_mDD67722D36075F0A2CEC3C22687C1C47A976946A,
	SymbolInfo_getCodewordCount_m2FEDFC18126BBDEE809884E38C27C89F88837F7B,
	SymbolInfo_getInterleavedBlockCount_mF95A015E0832907159367AA9FC7F3ED6DD0C8F5A,
	SymbolInfo_getDataLengthForInterleavedBlock_mAF876E5947FD821A8DDCADD7E419763721369E21,
	SymbolInfo_getErrorLengthForInterleavedBlock_m91A53BFEEDD75A4A303B7B786973DC49651E6D74,
	SymbolInfo_ToString_mD35C8CACC8C9E5ABFF73FCF70752A73283E51E09,
	SymbolInfo__cctor_m43D08D9425D79B104F8AD2E583CA7F884EDED931,
	TextEncoder_get_EncodingMode_m86CE43681A201DDDEAFE054D5951384BC5D28610,
	TextEncoder_encodeChar_m60A6BD53E34F4514AD33ED8EDE9F88E331A509F3,
	TextEncoder__ctor_m5C7FABC1619AEAB813C7982AFB3251E4871BD293,
	X12Encoder_get_EncodingMode_mB92BD65DF7759C0CE0D46FF6674A816CF4EFA880,
	X12Encoder_encode_m932A8D147591F3BF8E98F33EA1A2123EE11F7052,
	X12Encoder_encodeChar_m47C3B6BFF7826194E2AA71747AE7A84021B54E05,
	X12Encoder_handleEOD_m806DE8C5FCF97FB7405AACEDAC8C7801C757ABF9,
	X12Encoder__ctor_mAFCDC0F3E3A4A18A5C96B987C63A1E412C368804,
	BitMatrixParser__ctor_m8952E0E88DC362C362CE0B2BD6C7AC8EC4E91D33,
	BitMatrixParser_get_Version_m8F32D9720384DE258671764AAE1D7613C66203E6,
	BitMatrixParser_readVersion_m5630022D1A1AD702C636EFE2014C776A62B3640B,
	BitMatrixParser_readCodewords_m69EA88D99E046E1437765E8A89F441FCDC01B425,
	BitMatrixParser_readModule_mDB212F050A4C3D72161466D4A82218863C020561,
	BitMatrixParser_readUtah_m0072D25025BF0BADA850204CE3E8C6F62C35D4FE,
	BitMatrixParser_readCorner1_mD87683297F236F957DBF1DA738A6150798C1A69D,
	BitMatrixParser_readCorner2_m4E54E03558FE070DC680DE15E207C6D67CACB32C,
	BitMatrixParser_readCorner3_m127DB08776F3CEEA642B16028031B62FA017965B,
	BitMatrixParser_readCorner4_mC73041C15CF445EEB5035B958BD5AB35899E9DC6,
	BitMatrixParser_extractDataRegion_m1E33D355D38F5E3DBBC5630DE6B12EDC2A85322D,
	DataBlock__ctor_mA5BFE004E88059E0C0F1B7FB6786DDC9A34AD6E2,
	DataBlock_getDataBlocks_mC36A098321916BAFCE360F900D4F8236DD863D00,
	DataBlock_get_NumDataCodewords_m9F64EFEA06811087B55167D4183F0E834DD50243,
	DataBlock_get_Codewords_m2AB2987D09216947E94CBE3C758AD8AA43647542,
	DecodedBitStreamParser_decode_mC42A1A597D6F83C3D2EA2DD56044515B3D1D3228,
	DecodedBitStreamParser_decodeAsciiSegment_mF9FC8418831991136F70F5534E67D82981B44396,
	DecodedBitStreamParser_decodeC40Segment_mB07959B45DD2A80738ADFAB482A02C11300FC9F2,
	DecodedBitStreamParser_decodeTextSegment_mAD8EE51BB4BA37355C4CC88D3734B08B4AB805DD,
	DecodedBitStreamParser_decodeAnsiX12Segment_m8A5FA0295AB280FEA001DEDA7181647F3FF03DAB,
	DecodedBitStreamParser_parseTwoBytes_m4D22E0393C04E7D4444B0E255B4CAE85DBFF62A2,
	DecodedBitStreamParser_decodeEdifactSegment_m6A7E257C5A130E3BD4686292BCC6680A337CD50F,
	DecodedBitStreamParser_decodeBase256Segment_m08BD48C08FC2EB3C964BE078E6F9FFC230C9A5EF,
	DecodedBitStreamParser_unrandomize255State_m2134D435230458DBCD53E1DA81C19C682E0082BB,
	DecodedBitStreamParser__cctor_mC3982E18509A8B8AD8BF7D737FDB990D72B1CA4A,
	Decoder__ctor_mCE27E19EBA3EC6D417FA001B6F8F39EF33B277FF,
	Decoder_decode_mE3A200FA5A94E67A034B6D26DA8D0937DA9301F1,
	Decoder_decode_m8C1552577C4DFD72959E5DDB5BB0DD0BEEB36A48,
	Decoder_correctErrors_m29FB1BFA4667F72A28FF099CDA99D02D87F555DA,
	Version__ctor_m65718E372B88E852EAA44C01115EEFE076C9E13B,
	Version_getVersionNumber_m26FEA40A5844657C39A421008E71CCF743EF6F6E,
	Version_getSymbolSizeRows_m23844A5AE8E3506918165AAE3DDA4DB0520242A6,
	Version_getSymbolSizeColumns_m0B558F3EDDC8913D604DB11F95B496DA575211AB,
	Version_getDataRegionSizeRows_m14AC2D3F28AAB5A7C6A59FCD3451FF245C5B970C,
	Version_getDataRegionSizeColumns_m246F25ED57270649AD405B2D88B5D6258D7848FB,
	Version_getTotalCodewords_m781492734B917EB3CE3E3405E70362E10C8C8A64,
	Version_getECBlocks_mCEF65FD7DF21697AD864E85488CF2982B6B03B3A,
	Version_getVersionForDimensions_mCE55D833283B1B93E1A4B853EF66C0A81A3BD377,
	Version_ToString_m5A61BBB35CEFB2637C1FB35814EC51175E1A1574,
	Version_buildVersions_m8D0E5EC42DD8E037EF541416C873E327C9CFCB0A,
	Version__cctor_m2C9DE7C5868569CF8F3792D09F4AB926446BED38,
	ECBlocks__ctor_m72399DDDDCC7680722E07BEF4882FF89146788D0,
	ECBlocks__ctor_mD3FF3BC5DD32245AC797BCAE9556AAC6014C9046,
	ECBlocks_get_ECCodewords_mDE5C53421D5EB90BE2BD02348140ED446A03F6A8,
	ECBlocks_get_ECBlocksValue_m4FFC95413150D9AF6922B3B610979655D16B6819,
	ECB__ctor_m3D0EF7A0EDD041B2074894B095618C52262B5366,
	ECB_get_Count_m500674EE11B169A8F76F80EEC5964E590141E595,
	ECB_get_DataCodewords_m422E72F5DB34EC2057E9880BC624E13B018C1F5B,
	Detector__ctor_m8F0920DEB6E3BDE35A96ADED7D1E7C917F638B3F,
	Detector_detect_m7ADA5EF4E17BA7D0D5E5712ADB4A69B22BC88B22,
	Detector_shiftPoint_mFF4D54A50A84008FE3E3BD51448D04F67D0FAAC3,
	Detector_moveAway_m2CF1B41044B7946D63C173230875D5920B688618,
	Detector_detectSolid1_m058793E2CBB758EB58FA3F19AF40D391D8E17A94,
	Detector_detectSolid2_m55943E6660061417B230F44C26DD592AB5EE8726,
	Detector_correctTopRight_m4AFBAF12BAF3197213A1E4383B02645061489DCB,
	Detector_shiftToModuleCenter_m97187AAD9E4AC894F5D0272D57B1570FC0A76BD7,
	Detector_isValid_m4EC00E0C6FCCE52B8AE95E6837478D17EFE5206C,
	Detector_sampleGrid_m5C9B882441FF7EB54BDF67E1B58D92A54EBDB0DC,
	Detector_transitionsBetween_mA85F407B07A571BF125ADB61388DE62163D88B08,
	BitArray_get_Size_m4DF38399DD2C5743CC831361C2BF1EADC62BF3C6,
	BitArray_get_SizeInBytes_mA160D7ABAEB85F66A6394F3BFB69ABE9E032F258,
	BitArray_get_Item_mAFA7A42335FDD2BCF011CDA19BEE6A73B133E976,
	BitArray_set_Item_mFC80B9E3B0D5F639ED4BA9C4D7A19E333F6B1B7F,
	BitArray__ctor_m7A6A39E4287B6641CBF085414D8AEC6575CCD4EE,
	BitArray__ctor_mEE09C1AAC6D553214FE658ADB254428A778FEA39,
	BitArray__ctor_m6320AAA51371FED5B566D157E96F5151CA7C41B5,
	BitArray_ensureCapacity_mFBF33B63B033066CDF2F419D096640D8F14BB1E2,
	BitArray_flip_m03814ED6F72AED6F3236024D7CC4BF6BD2EBB436,
	BitArray_numberOfTrailingZeros_m094CE884719C78DD24055D8B77DE05E920EE64A7,
	BitArray_getNextSet_m7D8FDFC67F413BABE26FD362EAAFC2646C4F76EB,
	BitArray_getNextUnset_mBD48BDF5504BEB898B01393796FB216B6D64BD06,
	BitArray_setBulk_mB87C9BF176CD517704338DACEB077EB88524B396,
	BitArray_setRange_m900B6D94995432AC75DA57D428FCBA29794E562C,
	BitArray_clear_m971A2F552D290FA83246566705749F495EDD4307,
	BitArray_isRange_mFF511A8272B3F4CCDCAE42853CDB38C959549060,
	BitArray_appendBit_mBBACCEDC6EE9337F6BC1D698C2CC882B45AA8807,
	BitArray_get_Array_m1FEA291EBEB035CEBB7D2EF2F445DCDB7D8041D6,
	BitArray_appendBits_m85DD51F28D55A57CC604D35276425BF9BD06B19E,
	BitArray_appendBitArray_mE7864B47606471ECC538D9A084805FD34AE4767C,
	BitArray_xor_m522FC838D5251A6D8E8F4C4EE90F4DB39C1C3E4C,
	BitArray_toBytes_m003C974D86A84418ED4A3C2F21626F67F16B82D2,
	BitArray_reverse_mD73A2F7769C812C5462CDFAF606045DA17FD8FFF,
	BitArray_makeArray_m0F95D55F98AD7C2989D406C3CEBF072AAB5A056B,
	BitArray_Equals_m7BCB281AF1D0D5864A89C008D01C11E0C2B9AC5A,
	BitArray_GetHashCode_m244214E8D90CEE4136FD8ED5FEBAF1CE4981E0C9,
	BitArray_ToString_m8820612AB7EF12D953BB24EF384BF773ABB75FA6,
	BitArray_Clone_mF0D2E92181E2265376936D0D054570DEF55BF834,
	BitArray__cctor_m3CBBE968324F09DA08289066CFE2893F73B5B1DF,
	BitMatrix_get_Width_m33298FBF710A8752CF28839B453B350B3E70E3DD,
	BitMatrix_get_Height_mB0EEC75005372D759622F2C610ED7A9E4D635C07,
	BitMatrix_get_Dimension_m34E1EB9AA1F26C026AEFFDFA5BB2988CE0C80F0E,
	BitMatrix_get_RowSize_m0D4AAAB44EFEB30F24338076B9650C2C8043039B,
	BitMatrix__ctor_mE82BCBEF00A28E06132992A1119ED7373196FA9E,
	BitMatrix__ctor_m0288013DC4C9267ADD2FACB77C631BF9637B4B8B,
	BitMatrix__ctor_m176EC1F1A4E880F672B4D101B48FFF58D9486EAA,
	BitMatrix__ctor_mA46CCA4D3F98D5C32BF31861ACF9BD61B440FC15,
	BitMatrix_parse_m43CAD92BDC84A404F18E3578AE81B01D799C7343,
	BitMatrix_parse_mEB9E4973546882F5F0168789ECF9B81AAA89D77A,
	BitMatrix_get_Item_mC54BF7E1E3134DFBDE2A4A86DBD7AC3A601B9DFD,
	BitMatrix_set_Item_m7A55ACC841365AD83D5227278D0A05D1C5803694,
	BitMatrix_flip_mDCF333F81AB50B0EF290A8DD3DDF3AF317824967,
	BitMatrix_flipWhen_m5C54FACCC2AF88DDE0DAF94CE43F3CE072E0246A,
	BitMatrix_xor_mB9C08716A8E3D0A0CC1DA27529C74F47E6F6C947,
	BitMatrix_clear_m6C9DB4600035ADD0C0947AB21FBF0E5B46116A65,
	BitMatrix_setRegion_mD627055A82842183FFC48AE600D8825C81D7A7A2,
	BitMatrix_getRow_m477B04148EB01249C219ED23049B0E7F3D70461C,
	BitMatrix_setRow_mD4AD62A436FE9F57EDC1C9834E52862C4B3F56EA,
	BitMatrix_rotate180_mEDA2AA7FE01B30477B1DE8FBA01EA8D7A7F09CF5,
	BitMatrix_getEnclosingRectangle_mE927073216698358821D7945E7F8A67AACE2919B,
	BitMatrix_getTopLeftOnBit_mC5199987E1E4F7975DDABB20572DBCFAE81023CA,
	BitMatrix_getBottomRightOnBit_m8A26B0A5AF83CDF3CEC80C62132E410804F19564,
	BitMatrix_Equals_mE2C9839C9DD3EFA8AFB8EF3A4C9B3DE2B14E7D1C,
	BitMatrix_GetHashCode_m9402D3FABD681F2191BEEAFB707C775948BB4C87,
	BitMatrix_ToString_m4DCC7B4E385264C5EBCF913006C78E6E52AE1A0A,
	BitMatrix_ToString_mB00E99EAD04BD7C6C500B6AFC2070E91067BFCF9,
	BitMatrix_ToString_mFA86909D5E2D1D589E08B3A7580D5C524F4D0064,
	BitMatrix_buildToString_m2832ED0EF3570D346731BD0620198A0065DC060B,
	BitMatrix_Clone_mC33B8331DF830122404F2FCAE1020C4D062AB567,
	BitSource__ctor_mC4D81F2535DE34C80545A5450D34F327B7067A69,
	BitSource_get_BitOffset_mCF760D4152A853A99E525E4A36ADA79467100BD9,
	BitSource_get_ByteOffset_m8C2BE7D73D3B1820B2156E7D3371FCC4198D57A7,
	BitSource_readBits_m7CEDDB2C5F2E399BBFADBCCF768939F4290C3492,
	BitSource_available_mBBED7B769D334071B787E7B12F966A0E61683B8A,
	CharacterSetECI_get_EncodingName_m0132E1CCC822D69F0D5C78EC11C24B1F433BB997,
	CharacterSetECI__cctor_m7DD580F00444462FA171012D61151D6C41BF6B89,
	CharacterSetECI__ctor_m0896C7F553981F80059C1D11239DF7C7D901D9E0,
	CharacterSetECI_addCharacterSet_m725162161BB2EE5A68736CD39C6AAA19279E1B28,
	CharacterSetECI_addCharacterSet_m0B4AA31F942C2D9C8F5C8A5DCBFEEF9DA979135C,
	CharacterSetECI_getCharacterSetECIByValue_m597E8F5C7F38A2F0D1BE871F13B026B4293AFDA0,
	CharacterSetECI_getCharacterSetECIByName_m6D13B4390078B1EEADB047E2571B95CAA2333711,
	DecoderResult_get_RawBytes_m10433C77563D755542DC53B22FF569CB40671ED6,
	DecoderResult_set_RawBytes_m76DA0DECBD0F1F6858DC5147AAE4F01D1D6ED11B,
	DecoderResult_get_NumBits_m1CC1FAC264D01170B4EE5ED3940C516F7015BFEF,
	DecoderResult_set_NumBits_m07D1DB88C8CE2BA014207A87A82E97A74F1D8373,
	DecoderResult_get_Text_mA346F7E285B56E86ECD455EAED9FA37C8E3B0978,
	DecoderResult_set_Text_m3D3E09DEB0639CED8199F58A4E1D37D711886B16,
	DecoderResult_get_ByteSegments_m5731694C4E0AC645E2414F8D88B887EC75E50993,
	DecoderResult_set_ByteSegments_m472023F889663A57E28FFD7CEF98091C8F53E592,
	DecoderResult_get_ECLevel_m370D1148416EEF16E1403BDC9622BDA362DEF356,
	DecoderResult_set_ECLevel_m9602C70D943006A26044894E6F544944FA094639,
	DecoderResult_get_StructuredAppend_m980A5ECD49EE01C1A43B64031B5EE3983E157BD6,
	DecoderResult_get_ErrorsCorrected_m4816939A62187FB442762CD8FBEEE8BA753AC1C5,
	DecoderResult_set_ErrorsCorrected_m5DDAC6D51B8B1133C6F6A284C4549C614DEE7AC8,
	DecoderResult_get_StructuredAppendSequenceNumber_m100E55D100CD9B70F0653CB9D916F1894AA25FDB,
	DecoderResult_set_StructuredAppendSequenceNumber_m42A590E8788A70C45463B186C93BC87EE4E682AE,
	DecoderResult_get_Erasures_m506D8B484E02C20310202DBB004F9690A454E78C,
	DecoderResult_set_Erasures_m9CA0CA95144177355CCA47FB800D0B49856B28D8,
	DecoderResult_get_StructuredAppendParity_m5D95FA2B02F68E689D8F78BCC513322568AF1892,
	DecoderResult_set_StructuredAppendParity_mFA66847C80576E7625B25DCE686B60654BA05FC9,
	DecoderResult_get_Other_m14F9227981860B2FA3687AAAF981FC759A6EF700,
	DecoderResult_set_Other_mB9FE940C7118EC5E6D40B472CC32F776D449F7B2,
	DecoderResult__ctor_m7D04445A090ED460BE771B3B2DF57AAC8E6FAFF1,
	DecoderResult__ctor_m3741DA117665F12217EF92794E17C003378E6874,
	DecoderResult__ctor_m122EF86C35C27FA4C3A40323849B20763476A2BD,
	DecoderResult__ctor_m0A7001D58008F9EC69184D5158AC3A2D6F2532C8,
	DecodingOptions_get_Hints_mD35FD59AE533C6C03F6648681A5A993DF5B0954A,
	DecodingOptions_set_Hints_m3539F092C766912ACE9CDA44CB6C685F01162176,
	DecodingOptions_add_ValueChanged_m2F4CA1ABCC8189BCDF994656A56EAAC54F96EBA7,
	DecodingOptions_remove_ValueChanged_mC3015BD59DA7A19CA6B07EAA8F07EF399F1B32FA,
	DecodingOptions_get_TryHarder_mF05236C4627280C70D0DAF75519D6562412359F3,
	DecodingOptions_set_TryHarder_m849F6A91E1A0E8F0F5326176BFDF531A4F309C94,
	DecodingOptions_get_PureBarcode_m3CB262ECF641DE6648269A7B92E2FF37864566F8,
	DecodingOptions_set_PureBarcode_m10E181A565532CDA02C63B676C2016422C810E66,
	DecodingOptions_get_CharacterSet_m6C4F774AF64BADC83571FBBEFED6A7ED67DCFCDB,
	DecodingOptions_set_CharacterSet_m22EBA73B1620CC7AD0B2D18CF04E0465503CF589,
	DecodingOptions_get_PossibleFormats_m2ECFE51204A6C16B3A36C5E03D9B647BC7D9D5E1,
	DecodingOptions_set_PossibleFormats_m9F198E94B374D373F6D11921CCA221359035EA2F,
	DecodingOptions_get_UseCode39ExtendedMode_m242C71799419D1652FFCE73D0D362F0F396138B2,
	DecodingOptions_set_UseCode39ExtendedMode_mD782AFA600E7DD206DC1D319133AAB5214840A52,
	DecodingOptions_get_UseCode39RelaxedExtendedMode_m65D53CABF4C6C62ED640C0D1599375C43746F10B,
	DecodingOptions_set_UseCode39RelaxedExtendedMode_mEB7DE4B3512DC9026B6742DE73C99470FDBA0924,
	DecodingOptions_get_AssumeCode39CheckDigit_m38B4D7ADDEB3B98966587848B217DE29D6F61833,
	DecodingOptions_set_AssumeCode39CheckDigit_m544BDFF56ED9C62F4BA687970ACF54F6304AAB3E,
	DecodingOptions_get_ReturnCodabarStartEnd_m9640663FCC4E045D29B3096A8B7BBE2630DC358E,
	DecodingOptions_set_ReturnCodabarStartEnd_m5B7D25550D778CA717621A13B972CB52939F6E4E,
	DecodingOptions_get_AssumeGS1_m59AE07E37C73A3BA22B04083E29D0B8109CF3627,
	DecodingOptions_set_AssumeGS1_mC1786B7F39DE001B88F6BA9FAF1563E986892785,
	DecodingOptions_get_AssumeMSICheckDigit_m877BCAF2C0EA3C9DB1DAA7D8DE82EC6FC9554FF2,
	DecodingOptions_set_AssumeMSICheckDigit_m117BB641C2BF3CDDC4216F92B3A9B0536E832D41,
	DecodingOptions_get_AllowedLengths_m500140D14F0D95DA13CEAFCC75DFC016391F0415,
	DecodingOptions_set_AllowedLengths_mCA9D22EFAF34635DD56BFA0A6A4BB9E9E8EC36F2,
	DecodingOptions_get_AllowedEANExtensions_mEE96CBB011CEC4886B17E85402E5E1FDF6350EAC,
	DecodingOptions_set_AllowedEANExtensions_m18A0F591399ECE1B24CA1DCA39CE5EEA53D36C7F,
	DecodingOptions__ctor_mC1A403C24C25E1B2CA7A9E0F5D468BD76CFFC3D6,
	DecodingOptions_U3C_ctorU3Eb__43_0_m713C1345D194F86F61799E4C0E2207F835B0FF89,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DefaultGridSampler_sampleGrid_m969E4845DFA1E1648CFDC932ABBFF189527D70DF,
	DefaultGridSampler_sampleGrid_m191F7B1B6113F490707B0B9F8C37CE81A8DC7A44,
	DefaultGridSampler__ctor_m62D48782766E1F1A6C90D614E5654EB705CFE253,
	DetectorResult_get_Bits_m8B97D1D1F2C614E247099F140CF5E33C144DC85A,
	DetectorResult_set_Bits_mF0FFE6D538FEC3F4F159183B70F726DB8BBFCF22,
	DetectorResult_get_Points_m463FC2087C0A15AD97E3B48993ABD05271E19A51,
	DetectorResult_set_Points_mD6979B901F45B2A8903F34942FAE72342262D847,
	DetectorResult__ctor_mBC6584792C5B7DA3F28012FE70D28B90FDE97AFE,
	ECI_get_Value_m4E525866285ADF1212BCE2B522176CE9A56515FD,
	ECI_set_Value_m39C291A3860B64138A64ECACF50410233420AE1E,
	ECI__ctor_m5AB03E8B5083B1E2350EAE59FAFE723024B984A9,
	ECI_getECIByValue_mF4CEDB3D718FD7235D161A0F122558A4DB70FB44,
	EncodingOptions_get_Hints_m35FF278C5959F8063174ED36516E87938E8CB019,
	EncodingOptions_set_Hints_m172BC294493BC43F416BCAB600CA8FC23855E72A,
	EncodingOptions_get_Height_mE00290CDC21181E77087A6113B2658C589CFDB24,
	EncodingOptions_set_Height_mE7AF0C5A6CE6741DB387574DBF7CD23E993BD80C,
	EncodingOptions_get_Width_mCC91F07FB4C7728246CECEC9A3E6932F36A31F16,
	EncodingOptions_set_Width_mEA75DD346B1FA465B32DE799B9180E5F4A27E17B,
	EncodingOptions_get_PureBarcode_m95DE99A92E77F4BADE6D2BB51EBFA4EF0C0195E0,
	EncodingOptions_set_PureBarcode_m6875B158B2E45DBC960360CDB4EF07806B087BB9,
	EncodingOptions_get_Margin_mE8C52BAEC33258F73862415461681F07DBDC9C95,
	EncodingOptions_set_Margin_mB4F92033170F1504F3A15F64C56CF064FC77DD00,
	EncodingOptions_get_GS1Format_mF6DC0533B3F7133F42C4F9BEDE66D68F79F42A88,
	EncodingOptions_set_GS1Format_m644F015756C60899A9BBD17119F260F76FCCF732,
	EncodingOptions__ctor_mE1BC05F9615DDD0B3334EEE4E5E86C95F45108CF,
	GlobalHistogramBinarizer__ctor_m9A8D2F3FE439047307D774950A763C8F4976D2C9,
	GlobalHistogramBinarizer_getBlackRow_m555A993F29D76B0FA0E5639A63E035FFC3F6166C,
	GlobalHistogramBinarizer_get_BlackMatrix_m334B20D4DB054CE3CFC0816CADA4779BA5089D8C,
	GlobalHistogramBinarizer_createBinarizer_mC6CF4F5C22E46942FC32AC98AA1AB780B131D17F,
	GlobalHistogramBinarizer_initArrays_m3E1BA663F13DDBD203419D8908BE83008BA172A9,
	GlobalHistogramBinarizer_estimateBlackPoint_m6355EB05C2DA98D775B979469DEB519C2802FB3B,
	GlobalHistogramBinarizer__cctor_mB1ED007AE1A57EAB76E7AACB0ACC51E4CD39FB42,
	GridSampler_get_Instance_mEB420067749ABD696ADD56BA7A1479E6A8A65FCB,
	GridSampler_setGridSampler_mF1433D38996AF1A613DC1DCEE5E12E1E44A116C0,
	NULL,
	GridSampler_sampleGrid_mBAB1ADFF0655CBEE833C365BDB7EA85B95AAEED5,
	GridSampler_checkAndNudgePoints_m465A6B9F18DB964A403FF69CC005F7743D1A335E,
	GridSampler__ctor_mA67246E10FA276F048E6123C0B0B26A2E9B5231C,
	GridSampler__cctor_mE6D09255811C7FF1F6C7DC807FD5677A874020BF,
	HybridBinarizer_get_BlackMatrix_mD5F3AAA45194601E6BCF7DB4A50D27A744F8FA46,
	HybridBinarizer__ctor_mFC8614CC8388B1A2D6AA5F88AA487FCC6ACCB011,
	HybridBinarizer_createBinarizer_m23F731ABB20842192517E18CF9DCE3EA4B2DA3C0,
	HybridBinarizer_binarizeEntireImage_m170DE7C5E90720B9E05AEA0A1B2DAC325FCAACB8,
	HybridBinarizer_calculateThresholdForBlock_m8B93886707F28E8F22EE72D3CCC68C6051D1028E,
	HybridBinarizer_cap_m1D50B0872EE4BC9751320D73A381BFBC5CE32501,
	HybridBinarizer_thresholdBlock_m29FD2B4F92D29EC403254644AF713C43DB2ABD32,
	HybridBinarizer_calculateBlackPoints_m910615BC91FB4F70E33395648B060F30ED473768,
	PerspectiveTransform__ctor_mB2C4EC15BAC6290D31086F85AED848B6D2B47857,
	PerspectiveTransform_quadrilateralToQuadrilateral_m62B7E82DDA2E54EE34F13DF6E45BABBB0649562A,
	PerspectiveTransform_transformPoints_m752C1C67455A8D475D5BCF4F70849511A0CAF6EA,
	PerspectiveTransform_transformPoints_mE8A1EE80012A50C8BDDB4B955C6535FAEE8F0ED6,
	PerspectiveTransform_squareToQuadrilateral_m929F8E5CAA1C9240B9007F7977C9E86A414416C2,
	PerspectiveTransform_quadrilateralToSquare_mCA85694C6234C0E46B05897F9FA47DFBB583BBB5,
	PerspectiveTransform_buildAdjoint_mA27B5E94D9A16CBF5537F196D7378B8E33B603CB,
	PerspectiveTransform_times_mCE43CB3821B712FC5C9DDA3E30A97884DE41960B,
	StringUtils_guessEncoding_m766316A11ED814C0D8BB5E9E45FEB5A368DC1414,
	StringUtils__cctor_m76A7B9E41678470F57C4F650B1F816F1D6BE76A6,
	GenericGF__ctor_mD3E4CD23A1C9C302109D1EDD304614EA84FA717D,
	GenericGF_get_Zero_mCE585FD5D2A1534464BBA94A91EA64D113023FB8,
	GenericGF_get_One_m2A25FC3E94C95C247813FCB79C0E17B673D9625C,
	GenericGF_buildMonomial_m396672161FFCDFD398A7FE6E3F65731DFE20C5FF,
	GenericGF_addOrSubtract_m7C803D901338526E280B1DCC8AE9BBA476846875,
	GenericGF_exp_m93827D3BD1DDD4E717D19760D4DE4BC2CA9BFDE0,
	GenericGF_log_mBFB8FD8437ECF680CBCC1C6215A8ADDB3D2CD18E,
	GenericGF_inverse_mBD77A499BB8921D8363FCB1705C4100BCEE48B09,
	GenericGF_multiply_mC443DC9AAC9D6FB96017A23C776F7A8A8ED2E3E1,
	GenericGF_get_Size_m61A3B191C2565515444B14062956301987B85B7A,
	GenericGF_get_GeneratorBase_m9734B2A7A3A31D4FD63C675DCDC14A7C5FBC6825,
	GenericGF_ToString_m85D8BB5B904E27307B2BF09A4BAE227CBCE270B5,
	GenericGF__cctor_mC7B5115DA0A0DA6C182741594935ECFB4431EAE7,
	GenericGFPoly__ctor_m79E682C103DBF2478D05F006BB590AFDDF16C16E,
	GenericGFPoly_get_Coefficients_m082F23EA5ED52CA9D387DA6800BC2550AE7E5E6F,
	GenericGFPoly_get_Degree_mECFE8E6344BF3DD39952AC4C8B3B7C67F60D9D0F,
	GenericGFPoly_get_isZero_mAAE9292A0204D7BE3CD0D21C26929AFFF90954C6,
	GenericGFPoly_getCoefficient_m9DC2657BB6EF4223D70A16EF16BA9B1282AFA920,
	GenericGFPoly_evaluateAt_m37AC93BABB2DD80894E0BFA51B37229BED3A600B,
	GenericGFPoly_addOrSubtract_m581D696BC0F812BA27B668B97A73525684840033,
	GenericGFPoly_multiply_mD942ADD7D4E33149290503F453BF43F6A951372E,
	GenericGFPoly_multiply_mA2809ED733851F0E3BE0DC458E2737B4C3631704,
	GenericGFPoly_multiplyByMonomial_m7BD9C994C5B80F25FBCDFAA472490307E2100958,
	GenericGFPoly_divide_m7C6B52093059408F36DF17CF80E0F1B8C2E36B68,
	GenericGFPoly_ToString_m8EFF0478D4E4A2C939B6FE6EFC5647D406D844E8,
	ReedSolomonDecoder__ctor_m7BAF11D94B5C993B1DC0600D8A78C68348870433,
	ReedSolomonDecoder_decode_mBAD15FA812707B569B0DEDB766951D40A382A65A,
	ReedSolomonDecoder_runEuclideanAlgorithm_m0135B24670400776A0582EBC7E3A818C17610956,
	ReedSolomonDecoder_findErrorLocations_m2291E846229FCAC76CB44BA2E9F5FF374D80B87F,
	ReedSolomonDecoder_findErrorMagnitudes_m71B235E6755FA10E373310F4468A6649D5ACFDF3,
	ReedSolomonEncoder__ctor_m2F3F022E6C669F6F2402E609EB3C5DE18DBD7582,
	ReedSolomonEncoder_buildGenerator_m8889A063DE3192F8CDB7CFED427B54CDA692F2D3,
	ReedSolomonEncoder_encode_m765C7D0363DEB8BEB6AA672FE0600F8A50EE8852,
	MathUtils_round_mF9539156B6FA9A42206E49C9938D6C4976573F1A,
	MathUtils_distance_mD86E30697246AA12854809879CD0AC035E2AD02D,
	MathUtils_distance_mE7B9D4D03EFEC226CB48E670729DDCE0926ED5AA,
	MathUtils_sum_mBD16039D17C48335BCB27D1D1CF5354D8879364E,
	MonochromeRectangleDetector__ctor_mA078B24C27A890B01542B2710D48E5FEBB4E4566,
	MonochromeRectangleDetector_detect_mE9E655BB2FB91FD24665CC86E359EA90935C42A7,
	MonochromeRectangleDetector_findCornerFromCenter_m533C8D1CD41DCDAC3721A0F78E14D5FE891C4785,
	MonochromeRectangleDetector_blackWhiteRange_m60BF2FA07D50112D7D92FC926A5583BF8B17C5F5,
	WhiteRectangleDetector_Create_mA3E35CB328C5BAE11746D1CCD33FE063EEF237F0,
	WhiteRectangleDetector_Create_m23DE7013F54BBA4C38F2F028B45C7CA8ABBC63BD,
	WhiteRectangleDetector__ctor_m3ABBF42CAE3A59A562E035976ADEBB0975BCC2DA,
	WhiteRectangleDetector__ctor_m6BEE0E4B3444DFD7A97FE6AC5771D82447221AA4,
	WhiteRectangleDetector_detect_m409A65D2F4DE020008ED8CBDD9E719CC33A6A7A6,
	WhiteRectangleDetector_getBlackPointOnSegment_m6D5E116824A0CA70809415079343B86BD6D7F20A,
	WhiteRectangleDetector_centerEdges_mFE5EF712912B6842997801BB678930E5B66E719E,
	WhiteRectangleDetector_containsBlackPoint_m3C29ED210660F606344DD768EE10D71B15C80F08,
	AbstractDoCoMoResultParser_matchDoCoMoPrefixedField_mE62D6848687F1C4E5C9384818DB37158125C60F4,
	AbstractDoCoMoResultParser_matchSingleDoCoMoPrefixedField_mFA607848CDF0512BF4EDC52DFC69B4F88DA0C9EF,
	AbstractDoCoMoResultParser__ctor_m65B75A088D033B07539E18A3524A387B73F0FCBC,
	AddressBookAUResultParser_parse_m1A65A7E970A7E880899FA7FB11535F71BF3DCE8B,
	AddressBookAUResultParser_matchMultipleValuePrefix_m36857180A3FD295C634A65D28094E463F83E1B87,
	AddressBookAUResultParser__ctor_m8668040C5DB3FA82F6E1B65484F8C19C1224F8EF,
	AddressBookDoCoMoResultParser_parse_m362FE67C1A4BEC04BAD6D88F9C14DEBF79811A14,
	AddressBookDoCoMoResultParser_parseName_m3C3546E640D5493FFE461CAE7F734C596E32D157,
	AddressBookDoCoMoResultParser__ctor_mC43F74C0AAD1DDE652DD1CDE04CA3C1A8B9D7BD5,
	AddressBookParsedResult__ctor_m645D999644ABF19CC3985FB2CED1885FAEF9E97B,
	AddressBookParsedResult__ctor_m513912DF4A0C3B23C8E76FF63C1997F90FEE72B6,
	AddressBookParsedResult_get_Names_m8790DF343459D55B964A4D5F352574904C36A960,
	AddressBookParsedResult_get_Nicknames_mE9713EC68BC7BF150F7ED4173BE122D9A0001597,
	AddressBookParsedResult_get_Pronunciation_m707FBD15223C302B0786C5F23E5111A6CECF991F,
	AddressBookParsedResult_get_PhoneNumbers_m6516BA79BD5BAC88300A2C7C778E2C609F396D57,
	AddressBookParsedResult_get_PhoneTypes_mD1B234C956B49822347D482D98C8DDE87593A980,
	AddressBookParsedResult_get_Emails_mBB68E0D6624F54B490B31102AA7151AC78223DC4,
	AddressBookParsedResult_get_EmailTypes_m490E814E80971DA6C71F6184E23FED2D54CE94A6,
	AddressBookParsedResult_get_InstantMessenger_mACE2E2C09625A676579FA5519B6002CEC7F10C55,
	AddressBookParsedResult_get_Note_m1991E8E463F239C94C7B4B94454392A4C4E00007,
	AddressBookParsedResult_get_Addresses_m8B90BBA5FB46B563780D63B42FC3BB4A77BBC84F,
	AddressBookParsedResult_get_AddressTypes_mEE93C1DF2679774841F710819C3D0D62006A5562,
	AddressBookParsedResult_get_Title_m3814EDE0DF44A851A38B2D3DF50588B8A007C66F,
	AddressBookParsedResult_get_Org_m4B508C78DF4196BF5B8DFB105F175860B5B0E79D,
	AddressBookParsedResult_get_URLs_m171AE0EBC7704B1E923E4014C349F044BD55BC80,
	AddressBookParsedResult_get_Birthday_m484C471D3FF2EBD27283A2315EAD460B1DDD7542,
	AddressBookParsedResult_get_Geo_m303DA2BEA7991CFC7D6F3984C91D98A0687C816C,
	AddressBookParsedResult_getDisplayResult_m67FC4288AF644CDC3EEA5A5840FAF9F5EBF14738,
	BizcardResultParser_parse_m989C6689B619721D41107A36526D7049F144B251,
	BizcardResultParser_buildPhoneNumbers_mAD841386FF776BEA9C72DAEC26541A66989E8C46,
	BizcardResultParser_buildName_m7F274526D16DEBA8D8ACE3FEFF742407723AAD6F,
	BizcardResultParser__ctor_m0F294DB3EC674372B75984A7F425C3563E29B777,
	BookmarkDoCoMoResultParser_parse_m60138F9A198FD796DC75E2751AD4AECEB4213705,
	BookmarkDoCoMoResultParser__ctor_m68FD7D21E400ED3C7595E46E424202834C4D4920,
	CalendarParsedResult__ctor_mDB126E0CAA8EB2AC3205806FB7D6B3D47BEB81AD,
	CalendarParsedResult_get_Summary_m094CE1CD413587ABF473A268B7A60C7D5AED96FD,
	CalendarParsedResult_get_Start_m97964C2BF52E1E762F60E72F04653DDF5E1F5F87,
	CalendarParsedResult_isStartAllDay_m346AB816C83BC410107895486206E9498D95EA2E,
	CalendarParsedResult_get_End_mE134C97E4E53C933FB133C77A3B8503197654853,
	CalendarParsedResult_get_isEndAllDay_mB291BF7E232FAC3D1C1DC07213BF712949E83AFF,
	CalendarParsedResult_get_Location_m805275F221FBDE63E0040865AD6D07602EABF22A,
	CalendarParsedResult_get_Organizer_m4F558E024ACBD131B73DE2BF9F869AC9FC62F57C,
	CalendarParsedResult_get_Attendees_m4E3FB2E7175ED01F80CEDF166CF9AA56BF0BB448,
	CalendarParsedResult_get_Description_m31E421B948D2C444830307BD1D2CDBEA3D6A7067,
	CalendarParsedResult_get_Latitude_m26C477E08D1EE6F13D595627908A4FF3906F25FA,
	CalendarParsedResult_get_Longitude_mF4B017429640FC08D5DD1F92DA55D1261A17947D,
	CalendarParsedResult_parseDate_m238A0F3CF0DF8B773D26EB3D3ECBD493BE696B7C,
	CalendarParsedResult_format_m675A42784459EEFDAC52EC4AB3F8C191D4188E1B,
	CalendarParsedResult_parseDurationMS_m269BAD70F68BC48FB6F9B61AC3383175E2D1D67A,
	CalendarParsedResult_parseDateTimeString_mCE8C2F1AC4523D64D4D4B4BBEFFEB75AE58FD7EE,
	CalendarParsedResult__cctor_m860EB43A75847DE3C27266EB558CFFCDB824DC6E,
	EmailAddressParsedResult_get_EmailAddress_m1F62B1425E4C0E3DAF8FCC688CBB28DEC63B2859,
	EmailAddressParsedResult_get_Tos_m5262F8BEC47F5785247335E1243C18BFBFE8A91D,
	EmailAddressParsedResult_set_Tos_mA224C50D93B8F2205AEF14391F665C6F9A1E3688,
	EmailAddressParsedResult_get_CCs_mE772E3F26FB8E7642A650F2197EA55CB44D49291,
	EmailAddressParsedResult_set_CCs_m4ED7B1380F359125C6F22A064EF8A78EEC014475,
	EmailAddressParsedResult_get_BCCs_m53C5499EBF18AE80C82F624C76E1F74808DB4698,
	EmailAddressParsedResult_set_BCCs_m7594188B0C956BCB006B5B9EB3932EF87C641C6D,
	EmailAddressParsedResult_get_Subject_m8450A71D85418A097DB6BB9668F6F1B5EDC11F7D,
	EmailAddressParsedResult_set_Subject_m503A52107868F7178237B528748071CB32994370,
	EmailAddressParsedResult_get_Body_mA5BF39F58B67A78998A7DB4E0851879F85587A8E,
	EmailAddressParsedResult_set_Body_m283F3B03716B7B27042259BFCCACD196E151A732,
	EmailAddressParsedResult_get_MailtoURI_mD2F6163EF82DB1DE3601EE39B227C2977E7CB712,
	EmailAddressParsedResult__ctor_mCDBEE300A70E18F3AEDB2CCACB264F027F2DBECE,
	EmailAddressParsedResult__ctor_m1DAD8D1F2E18FC154EC2677D2130830203586350,
	EmailAddressResultParser_parse_m26B97BD6C28167CB5DD9C81ECCEF153B319A7142,
	EmailAddressResultParser__ctor_m584C60D98B414C2D5F3674B310790043ADA339B2,
	EmailAddressResultParser__cctor_mE3471498258B8335F1B3536838B0DCA922B87A1C,
	EmailDoCoMoResultParser_parse_m0001ABCFD5E02B6484AFA6D282165A2F6F9540B0,
	EmailDoCoMoResultParser_isBasicallyValidEmailAddress_mE173C29F47820D06C353CA3576B50542B48A7DCE,
	EmailDoCoMoResultParser__ctor_m774C59D652E7B8D7B6DBFEE2BDECF163E76B464A,
	EmailDoCoMoResultParser__cctor_mFBBAD6C25DDDE79DAB25E6B991A9A643D574E2B8,
	ExpandedProductParsedResult__ctor_m6442D6F11B8736457A3D97EACE30CC3197B08F94,
	ExpandedProductParsedResult_Equals_m5A1C1052ED38F7E03A72B4A66C37F8D107CBE821,
	ExpandedProductParsedResult_equalsOrNull_mEF0434FE785B6701A6FF7016AC6F1EDBBF7E9C2E,
	ExpandedProductParsedResult_equalsOrNull_m2732855F203674DE83C86AB444407E291D21549D,
	ExpandedProductParsedResult_GetHashCode_m17A6420AFD666E1FAE73301F6D73D0B630E7B5FF,
	ExpandedProductParsedResult_hashNotNull_mAA8B9F5D1F3178D599096AA54C8614A0B9488494,
	ExpandedProductParsedResult_get_RawText_mB3D9D73EC7E273C5DE1293530CD70EEB59EF7DE6,
	ExpandedProductParsedResult_get_ProductID_m6AB2DF86E14625133A939100C3CF7A0A840AA482,
	ExpandedProductParsedResult_get_Sscc_m2CB96A1D49ECB4D4997B910B61E1979C4B19B1A5,
	ExpandedProductParsedResult_get_LotNumber_m1D44B20BC585E0169F98F6F29F221EF554660A6B,
	ExpandedProductParsedResult_get_ProductionDate_m7E36BC42C52BCCAB1F89C2AE8466600C8347424C,
	ExpandedProductParsedResult_get_PackagingDate_mB86E9C82DA5D4BD1E415CA90470B648B1BBB9501,
	ExpandedProductParsedResult_get_BestBeforeDate_m6C3915046DE915D759608E1B5DB70207D7CCC589,
	ExpandedProductParsedResult_get_ExpirationDate_mF472B70C9CF3D9B985B1EB97F642E8906EBA16E5,
	ExpandedProductParsedResult_get_Weight_m76CC46E2E5568B9DD7234904B09AA3376D161A9E,
	ExpandedProductParsedResult_get_WeightType_m7BA2834649B1076B1EA252CFCDA5A788296D62C3,
	ExpandedProductParsedResult_get_WeightIncrement_m6F5D641A433D5717B7DB15B0C964FAFFFB0495B7,
	ExpandedProductParsedResult_get_Price_m85BF9E6722601B81809C6A2831383023931F2EA6,
	ExpandedProductParsedResult_get_PriceIncrement_m034F7CFB8CCDA3BE7F318D90DB4536D88DAE8410,
	ExpandedProductParsedResult_get_PriceCurrency_m7651B36F3A0480B1D11997A769CC689066871CC5,
	ExpandedProductParsedResult_get_UncommonAIs_m00690C11B1ADD8D0D99E5F6AAE00C778F5030498,
	ExpandedProductParsedResult_get_DisplayResult_mD7AB1B9980FD2BA3B42E9736F6AA7C2F7BA0112C,
	ExpandedProductParsedResult__cctor_m768642FB41A6C40A92AB399BCC1C4B2A1E188958,
	ExpandedProductResultParser_parse_m5F8034D20A60D37B4EAEB35E1BE2378A03D64523,
	ExpandedProductResultParser_findAIvalue_m1DABFB52C43FD6DBE1045EF38E7A2762648FEEEF,
	ExpandedProductResultParser_findValue_m7367AB1E7A914DBFDB8531F36ABF026FA81F8736,
	ExpandedProductResultParser__ctor_m28F2FF656EDEC03598FBE15123EAADCD5D2B0CBE,
	GeoParsedResult__ctor_m9D105C79347C1D6D804B375ED4950B8976FBF69A,
	GeoParsedResult_get_Latitude_m7B9B2915F01DE9B4C2A912BC96283B936E58DC9C,
	GeoParsedResult_set_Latitude_m675F8CBA3B3A78543D5E1C576F6CC917D619F8FB,
	GeoParsedResult_get_Longitude_mB8BDBF3C9B1A1A8DEB3DC48646EF88A4AD4F026C,
	GeoParsedResult_set_Longitude_mD9ADC4E96B2CC1DAB9DC6AB35D1AF4AA9D0BDD8B,
	GeoParsedResult_get_Altitude_m292CDA9ACA12ECB4B82C214D1B2CD17D424BFC74,
	GeoParsedResult_set_Altitude_m7F707069DC9BB065F935A9A8582D368B31E42B79,
	GeoParsedResult_get_Query_m32F18B61AFD98AFE3D99E53392A38F2F16B7B490,
	GeoParsedResult_set_Query_m49DFFC434F4BED2E289A8083D52A7547B9F83284,
	GeoParsedResult_get_GeoURI_mBD5CCFCFFA1F99B2799AA86A4591AA37118CA7A2,
	GeoParsedResult_set_GeoURI_m9246CC416D4C8C5D013AAA1C4EEBB0F96415D6E4,
	GeoParsedResult_get_GoogleMapsURI_m0E05A9C356C0E88CA99BF159F0C871AD0E74612A,
	GeoParsedResult_set_GoogleMapsURI_m237FCA8185D0EE6E98958FEBECED96FA6E6F50C6,
	GeoParsedResult_getDisplayResult_mA77D2BDFC868E792948D0E1174049D821641BE63,
	GeoParsedResult_getGeoURI_mEC094D9626B90A75B56CF5A32553A1F9CE48B228,
	GeoParsedResult_getGoogleMapsURI_m7CF941D3D75A1AE3D5F047C1D7ED908ABA741DBD,
	GeoResultParser_parse_m03111D066833C5EBEDFB3D4D915CDCCEE24EB321,
	GeoResultParser__ctor_m0B74049A8284F987A16843F51414E8D584222D44,
	GeoResultParser__cctor_mDA8D55C291A66FB685B6CA861A9CFE79CA6CA4D4,
	ISBNParsedResult__ctor_mE29C7A8F6D11B95E4DB0166538C3132BB6672E34,
	ISBNParsedResult_get_ISBN_mCE3525CA3D196F7F8A6B645DA6818C496690BF74,
	ISBNParsedResult_set_ISBN_m7D3A60E083E39FD9612184CF030C9ECB5F876D14,
	ISBNResultParser_parse_m7F76E6C9B735AED47E003D256576F5C72E749B17,
	ISBNResultParser__ctor_m2097B01719774B1909D3201A8CE59929B3FDB100,
	ParsedResult_get_Type_m007B22367016D85A7940C86326F007F904EF26CE,
	ParsedResult_set_Type_mE7E6AAFC820F15369937F749A84F9FCD012D5D50,
	ParsedResult_get_DisplayResult_m1C4BEA2A33E11461C9198055F82E622F9878638C,
	ParsedResult__ctor_mA217F44067BFA4341B82A6B83FCB2F84BC2F451C,
	ParsedResult_ToString_mFAB0A87357B6500E4334359E6C1213B4BADCE7CB,
	ParsedResult_Equals_m8AA95620AC9884981BEC97ECEC841A4DAB5F5E5F,
	ParsedResult_GetHashCode_m2425649AFCC66281DADEDD23646ED1E7E4C6AB1D,
	ParsedResult_maybeAppend_mDDC7404AD0C029DAB72E78F4715C6BA6A611927F,
	ParsedResult_maybeAppend_mC758D17AD4A0974CA4BF9F9BFC1592CE64F9D825,
	ProductParsedResult__ctor_m89EF437A50ECF22E1152ABC03C8175BED1731B18,
	ProductParsedResult__ctor_m913FC763960F2718497835D0556354DBB11B561F,
	ProductParsedResult_get_ProductID_m335C898DDC69E86630D3F16BD76A023AE55C6E7E,
	ProductParsedResult_set_ProductID_mE930C204472161A877073C436094D48D08DA2091,
	ProductParsedResult_get_NormalizedProductID_mF6418B1564B9561EE26EDD14ACA2240AE5BC2A44,
	ProductParsedResult_set_NormalizedProductID_m520415415B0B49F2B80D8BA0910DD96B834EB38A,
	ProductResultParser_parse_mBC3185C24439FD66D3DABDA1DA84B58B45F925F8,
	ProductResultParser__ctor_m7FD5B6CB0436D20EB8C598F3E0C9DF8777440C06,
	NULL,
	ResultParser_parseResult_m6059D8436C188C262D0E6EA2021D6CFABA7DE7A2,
	ResultParser_maybeAppend_mE572AB168D59D1CDE52DBEB7196D740EACBB7719,
	ResultParser_maybeAppend_mC887A33C6B67FBCAB05331B162C625FE6B3B7B2F,
	ResultParser_maybeWrap_mECEC0410EC62B5761ED9118F035ED81822BA40E8,
	ResultParser_unescapeBackslash_m4E624E0D1CAE213B6D6DC9AC15DA9343A4880E5C,
	ResultParser_parseHexDigit_m05F2631B189D1D43139998FED0FEE91F2CE989CB,
	ResultParser_isStringOfDigits_mD5DC11477DB3E573B9F9875A68159285A1A1C972,
	ResultParser_isSubstringOfDigits_mAEF3B4A19F8591F3E91BEF420ECA99D44E83C337,
	ResultParser_parseNameValuePairs_mD246E0BC146446D2F81ECAA4FF15A9F903BBFA4B,
	ResultParser_appendKeyValue_mBB2D1047D6AA51F8672DBEDE4CED61159F52245D,
	ResultParser_matchPrefixedField_mF6931EC774EF2FDD764676FF908859FE6489458C,
	ResultParser_countPrecedingBackslashes_m3C0AB519AB434035B602E8735C41C47C27050D19,
	ResultParser_matchSinglePrefixedField_m4748AC7105E9DBBFF4153C5EFAB795FAAD163A74,
	ResultParser_urlDecode_m5F78783082E778F27AAC4ED3914281566FB8021C,
	ResultParser_findFirstEscape_m18695018BA1835A2C1DC46A89B42DB5149F4F317,
	ResultParser__ctor_mFF8520ECF7CFFB9C850F062BF8D024F413156C97,
	ResultParser__cctor_mD06E1D4102C897C00DD5C8420C35BE06A3B3BD0C,
	SMSMMSResultParser_parse_m741A88F2B66CB07A95A1CC6AA8FBE99C24B5B3B9,
	SMSMMSResultParser_addNumberVia_m44A9C182FF04DCAC5E9BB6F8856157E8864A78A0,
	SMSMMSResultParser__ctor_m3E03B3201C5F0C1B823B05907C90DD6A06A41A72,
	SMSParsedResult__ctor_mD371CE3809C424A63F6872F3BF1430BCBCE8481C,
	SMSParsedResult__ctor_mF5E34C1410A9298B1BC8A489976ABE4C0A1AA636,
	SMSParsedResult_getSMSURI_mC2469228D29C3F89E329ECBC0DFB987D2C74B61F,
	SMSParsedResult_get_Numbers_mB86CF2CFCBFAE38C557B10E4DAAE41413B967FEE,
	SMSParsedResult_set_Numbers_m2A2EEFFCB5D4AE742E95BAF06FE5021AA4C0921F,
	SMSParsedResult_get_Vias_m3E3C2FCF1DA281D05DD4989BF4DE79FED4776787,
	SMSParsedResult_set_Vias_mF67B884BA13267C4F81546126FE211D2B40D3FCD,
	SMSParsedResult_get_Subject_mEFAD3E4A258242B0EECE0A2A4D6F899F1B9B3D00,
	SMSParsedResult_set_Subject_m5D0267FE9447F729C6E9B0B9EA6CBC60CBD569FC,
	SMSParsedResult_get_Body_m5C7563D6B69E93D74B53C07E1673744E0A61A6B6,
	SMSParsedResult_set_Body_m2B9E87B2A500C0E533FEBE6CBBEC42D1311C1586,
	SMSParsedResult_get_SMSURI_m14F5B9843CDD70587066A60074E78576558C67BB,
	SMSParsedResult_set_SMSURI_mD360A4A3DAEF13C6FC8E27155C1B921AD079B93B,
	SMSTOMMSTOResultParser_parse_mB1FD412CE3D8CEE5DC45E7FD469BD487793D273C,
	SMSTOMMSTOResultParser__ctor_mE9F6BCDDD5CF0D9AF014A06B3E353C4CAD680FE0,
	SMTPResultParser_parse_mF844E81F420BAAA66B6D7423E8791E9E469A0639,
	SMTPResultParser__ctor_m8576234AA818896AA659024C653C8333AB812C6D,
	TelParsedResult__ctor_m239B4043D0C7A65A2D67B323382181AA84E56E2D,
	TelParsedResult_get_Number_mEFE1F6FF62DBE65DB1F32BDD6574B91BA2A42E01,
	TelParsedResult_set_Number_mF37925DBE19616345DFDD8333C90468C441C0672,
	TelParsedResult_get_TelURI_mD4B27C0480AD24E5D953C61D214CB2AF21EE310C,
	TelParsedResult_set_TelURI_m67FB009C0E623636B72793AD23615A42D9CC803A,
	TelParsedResult_get_Title_m328ECF59686B97A9F5E8D2D3A0E08E306B6D8B0A,
	TelParsedResult_set_Title_m6C8934456E30B34380758BD59C72C6A6FAB71B2E,
	TelResultParser_parse_mC5AB74B031C32ACE2375A4631D7993E5973D029E,
	TelResultParser__ctor_mCC9AF7508499739783ED50689099B7225E25F5D1,
	TextParsedResult__ctor_m4060479DFF604CA66B3A48125C56F6FA9AE1521B,
	TextParsedResult_get_Text_m4D8FB51DF4AC46A7012507D02D8824305CF4D304,
	TextParsedResult_set_Text_m55DB302AB055373DF6D2F046918BAAA36160415F,
	TextParsedResult_get_Language_m15BA33CC9293321A0A9C591F7829C1394FFE91EF,
	TextParsedResult_set_Language_m285738534DC6883FCB9169D34B5745CC7866B46F,
	URIParsedResult_get_URI_m8BF02FAFCA4049BF8A75481E1556A785824438E7,
	URIParsedResult_set_URI_mB69DB9230A3F0E57F065079B7026CD9C541403D5,
	URIParsedResult_get_Title_m5CF62FA8D702E5AEA450D5DFB92A1DB89F8D8515,
	URIParsedResult_set_Title_mE526803CD2157435EB9A41E83418E93DC8F51A47,
	URIParsedResult_get_PossiblyMaliciousURI_mF95E1663D1BC45A5C7E3A2F8FFE4FE5E95026119,
	URIParsedResult_set_PossiblyMaliciousURI_m5AFE5376279DC6B94C3452530087FE77D64894AE,
	URIParsedResult__ctor_m9E1D382DE7CF3BD5C4586E53EE2B9CD66A1AF4E4,
	URIParsedResult_massageURI_m62A5E5F09F0989D228308BC8F14CB55BEA95C495,
	URIParsedResult_isColonFollowedByPortNumber_mBE7387722EA3C5DE5568D039BBA57EDC39ECAE43,
	URIResultParser_parse_m060B537712DE980B549F854311444663564B62DC,
	URIResultParser_isPossiblyMaliciousURI_m30EE507A7F4C751DAFA45AB3E8FA1E647CEC15B0,
	URIResultParser_isBasicallyValidURI_m90A406DABF0827B371D886FED2F36DA57B1C45C3,
	URIResultParser__ctor_m87EE5EEBFACB69982E3E5913FE6C4C6366978FD7,
	URIResultParser__cctor_mE12B5D1B0095DC9C1D69CE132581F576F6CAC584,
	URLTOResultParser_parse_m96D91B0A3212DB577146E0866A3AF4B6DE8F0FD9,
	URLTOResultParser__ctor_m95B2F99D73D8C88DE9A07E705724A6340351B5EE,
	VCardResultParser_parse_m4E72F0F7358A4B64DAEA4ABADD2E31AE997B4A2F,
	VCardResultParser_matchVCardPrefixedField_m0D0884C3FC144493CE00BAA0738F99A332062621,
	VCardResultParser_decodeQuotedPrintable_mECCD87C23C073967DFA8EB469FD137B862BC52A6,
	VCardResultParser_maybeAppendFragment_mD1303DB6EEBEE87C5E72EE1CA33EF42EB9BADE1D,
	VCardResultParser_matchSingleVCardPrefixedField_m630524C526820E7DF15468EE8D778024063BAD3B,
	VCardResultParser_toPrimaryValue_m5F4B81EF21D8468157FF575D5394E5A1733876F5,
	VCardResultParser_toPrimaryValues_m30BBA956D0D4D30A3E0B92A106999B06F116F360,
	VCardResultParser_toTypes_mAD086426CB06E6742E092B9B954576C4AFBA161F,
	VCardResultParser_isLikeVCardDate_m6EBEF930FFB9336179CCAD86E0B00B06E235133E,
	VCardResultParser_formatNames_m12FDDC63A2B27CFE4294F2FC2B3C14FEAD0E5696,
	VCardResultParser_maybeAppendComponent_mC5311DE8E883C25607A38D94B1E4B9B07B5D9AA0,
	VCardResultParser__ctor_mF5780CF4F2C39DC7301E391A903261927C74E3BD,
	VCardResultParser__cctor_mA9F1184378825B6C4D566C6FBB2CBC063FF5F460,
	VEventResultParser_parse_m7590BDA4AC58059BAF7332662E1DCEBE5B0BC35C,
	VEventResultParser_matchSingleVCardPrefixedField_mD2B1C17958723C5DC9A4036C92D88B19D524734B,
	VEventResultParser_matchVCardPrefixedField_m0C17ACFB8B5F0E533513E6E2EC39D3932B2FDFF2,
	VEventResultParser_stripMailto_m4F906B2791E6B7BF53BFB6B7964112621A05F4AC,
	VEventResultParser__ctor_m86CE311D55064CEA0AB17DEAF60521E604BA7255,
	VINParsedResult_get_VIN_m41F5BAC17AB7753200A6F18F0197AF606BDE1E18,
	VINParsedResult_set_VIN_mFFFEA31D8D6C6B394DF4FEB36C35B117A404DCFB,
	VINParsedResult_get_WorldManufacturerID_m1B8FAD07E569B8681396280E048FFA593923CC2A,
	VINParsedResult_set_WorldManufacturerID_mC3112936A63BD6D17367D8A409A92ECAF8D9A238,
	VINParsedResult_get_VehicleDescriptorSection_mCBAB61B23D53015AAA9F655B811BC2EAD496B907,
	VINParsedResult_set_VehicleDescriptorSection_m524B5F3C3BA5D6D3C5B806462E566F57BA9B95D0,
	VINParsedResult_get_VehicleIdentifierSection_mD7CF178AE57F0345B9539C1D369FD2FC40F5F687,
	VINParsedResult_set_VehicleIdentifierSection_mBC62477EA62B3333B0233FC0AD0D03AEF7AFED1B,
	VINParsedResult_get_CountryCode_m002A3FA7149E497245D2A61D9D3338FF81DB8F94,
	VINParsedResult_set_CountryCode_m6D6DD637D7961181457220CB34EE4E5741CFF8FB,
	VINParsedResult_get_VehicleAttributes_mC7A54649319E624039BA425CF41E5557572E066E,
	VINParsedResult_set_VehicleAttributes_m828E0785130DEFFE165AE464AED964E47CC6B24D,
	VINParsedResult_get_ModelYear_mDD02BA3A8457299E28D9C4A846863E4EBCE0DA25,
	VINParsedResult_set_ModelYear_m998E0DB2451308CC490834F31ED06E56BCD06693,
	VINParsedResult_get_PlantCode_m4391EFF085A612934090E87618A5BBEA95676162,
	VINParsedResult_set_PlantCode_m06DD134C31BDB420F62EF621BBE23E4063D8FCCF,
	VINParsedResult_get_SequentialNumber_mFC092DF0BB0DCBD5FD5504A4218907F0F04649C5,
	VINParsedResult_set_SequentialNumber_m8A9FD10FCCC5FF364AA4B6D4DC0DC90F6C57D808,
	VINParsedResult__ctor_m3CC20FD1D7F288861E5B6E09E5CFAF65AE1313D9,
	VINParsedResult_get_DisplayResult_m20B331A7934F851B65174F2A1F613E896AFCB2CF,
	VINResultParser_parse_mD87B2B98AB06699A29F761AFE70980A521480C22,
	VINResultParser_checkChecksum_m35DC0B6670AADE79C500F1CBB421B54B0ECFBB6F,
	VINResultParser_vinCharValue_mBA8F8431CF5AD539B254F3D98D121D332F7C4D0F,
	VINResultParser_vinPositionWeight_m13CF472F72A17D6ECD760F23815D23C83B36BF17,
	VINResultParser_checkChar_m86A4B8034BF3C8C58A0F17F47F3CD3E98B0618BE,
	VINResultParser_modelYear_mF78E7D90FC3DB3BE3673CDFC3B6118C9C64DB724,
	VINResultParser_countryCode_m6A007B25D7AC6D63DE850765E322C1BC9585207D,
	VINResultParser__ctor_mB81B3BDC1A306748AA0648FF083A1BD07948DDF4,
	VINResultParser__cctor_m4319C65A57D809CCEC61A4FBB9ADB2A2A269990D,
	WifiParsedResult__ctor_m40C7EB8A0D4B6EF866D1E28309CC8AF4BBBFBDB6,
	WifiParsedResult__ctor_m954592F31B3FA1E0A8C0DCE143D7BBABA7B2E67E,
	WifiParsedResult__ctor_m059C124746F638C61B5CCB6BEAEF6349BBA3DEC1,
	WifiParsedResult_get_Ssid_mC8AC5841158BD170AE08100521C0BC4D57C023CF,
	WifiParsedResult_set_Ssid_mE1C3E409891203AA532943003E1BEB3C90EC5CE0,
	WifiParsedResult_get_NetworkEncryption_mB2AAEB4C26D32C7CFE681DEF03630980FB10CCD6,
	WifiParsedResult_set_NetworkEncryption_m02244F133BCA36D09D642F99A3D4A935E1233659,
	WifiParsedResult_get_Password_m5196A12772B0539ACA77DE5A71537C02DF3E98AB,
	WifiParsedResult_set_Password_m3813C62B08C1E3672442009BDD0A0852EF1E3EBB,
	WifiParsedResult_get_Hidden_m69DCB213A8ED95C02B5B330DE3D178208E2C86F5,
	WifiParsedResult_set_Hidden_m981BE4A1433755608740B7E3273893DFB8C3B90D,
	WifiParsedResult_get_Identity_mF36367387849F243A3DB7DE1DF920A7A7A21DA81,
	WifiParsedResult_set_Identity_m71E970DA4CD37933EEA7156DDD09ECF43E1EC8C9,
	WifiParsedResult_get_AnonymousIdentity_mC2377B68942538C4E0DE388D3B26C82CA1EBA272,
	WifiParsedResult_set_AnonymousIdentity_mA9C05FEC275ED9E377FFE08D23C256605C11F0FF,
	WifiParsedResult_get_EapMethod_m7814586144D62123E480453ED06BBA8E2FE51A2F,
	WifiParsedResult_set_EapMethod_mDCAB124D3053177756A153287A85F9636285F743,
	WifiParsedResult_get_Phase2Method_mE6A39DE9017277C759E0BA9F74201947C23AD6A3,
	WifiParsedResult_set_Phase2Method_mA648682E24C365DFF5280041D16E244F5F9BB77E,
	WifiResultParser_parse_mC23A5D51E97B5E904E85F91BC9E583722EB06801,
	WifiResultParser__ctor_m490D7F7021C75D1BDE2BD7303EA07BFA8AAC9C93,
	AztecReader_decode_m6B7033967224885BD515DAB4628A9F22560E6E6F,
	AztecReader_decode_m0F6C80BD09854EDA44F70B846BA6F21CFDC49E91,
	AztecReader_reset_m1E07466CE8C17AD13F51DCEBCF9A11308FA159A8,
	AztecReader__ctor_mCBC609EFA18813AEA11C4A6DF087E68DCD6F3336,
	AztecResultMetadata_get_Compact_m1430C7DAF96061FAE1A2105C5DFC091F379B8FA5,
	AztecResultMetadata_set_Compact_m73367652825CD830773A5336F6F9134F52A1E65D,
	AztecResultMetadata_get_Datablocks_mBF8F2055196EF44753C598D6C6FA1C4B2C4710F1,
	AztecResultMetadata_set_Datablocks_m28360A13E8ED3C88C89C8F88EAD5306C344DB950,
	AztecResultMetadata_get_Layers_mDBE1500AF7ADD1379D9A302CE9A4893B0D8CA226,
	AztecResultMetadata_set_Layers_m7FDA4D04DA26425519C9AADECC381BA26E65E81C,
	AztecResultMetadata__ctor_m14D8BE64ADF2D77D4D8B027E8BC7AF833982662C,
	AztecWriter__cctor_m0951B4B729701E5718F417C3C9F79DCF970A77AF,
	AztecWriter_encode_mB4866D82FE01EAC3029F0E9F5B0183EC4C9E0DAA,
	AztecWriter_encode_m1A2E9C250748E195A9141447133D9A4AB0443C04,
	AztecWriter_encode_m8C7A47FDED5D17D2BF449074274B8307F829B99D,
	AztecWriter_renderResult_mE032EBA6309C764259921AB7AD4E53CC71D2E662,
	AztecWriter__ctor_mA2737566BBF2E9BF6D5991CF6407E769741B9A91,
	AztecEncodingOptions_get_ErrorCorrection_m40E71A05C5F5B8FD7F07AA24A987E1FAAF6C3F76,
	AztecEncodingOptions_set_ErrorCorrection_m85F38A60CC52418315573A1F74C52F567917289A,
	AztecEncodingOptions_get_Layers_m81A9CE0E23D4FE008D890A590D85556D50CE8669,
	AztecEncodingOptions_set_Layers_m48119005B3E1BF4A0FF88E03D1134F606FA7FFA1,
	AztecEncodingOptions__ctor_mA4F317A242D4302CEB43D1B54A1A7E2B0289E393,
	AztecDetectorResult_get_Compact_mFD9801AB8A2D778D5E09086526ADF6EF68C95575,
	AztecDetectorResult_set_Compact_m7E681FFE309327450CEC845730DA9164A4914EC6,
	AztecDetectorResult_get_NbDatablocks_m9FAE052F42EADB9D4B795316AEFE9BF71E90B790,
	AztecDetectorResult_set_NbDatablocks_m7EB372EE4FF562F799ADD79BB38FE19F188CDEB0,
	AztecDetectorResult_get_NbLayers_m6F22385A1607D79215D935D69424CEEF5C8BFBEF,
	AztecDetectorResult_set_NbLayers_m7C0D2A6CB1D069E7CB405911D328901FA4371600,
	AztecDetectorResult__ctor_m042F3E01061A420A9A8AF8D1A069B2766A616730,
	Decoder_decode_m975B500E099D647B4AE51438DA9EAADD9F12F793,
	Decoder_highLevelDecode_mCD4D1203342ABB0232594F8A3AAFC991ED52697E,
	Decoder_getEncodedData_m54DFB32C65271082FF08BB1FEF8F626EE72D1B2A,
	Decoder_getTable_m4613C50AF912D5113D08AE42E426F4012969A18C,
	Decoder_getCharacter_m20A140DA5F2CB6BDFD69620AC16AA5BE53D63083,
	Decoder_correctBits_m159CA039058A11C0475AEDC47358AF7D76269F42,
	Decoder_extractBits_m237BC440228E149198E6841338704EA972B2D5C8,
	Decoder_readCode_mC3FA4A261AB6717D89E59D40C849653D21C9A494,
	Decoder_readByte_m1ED04549537119AFB95DF7EC21F3D38D57E8BF76,
	Decoder_convertBoolArrayToByteArray_m0106370E0B021B1F6CB9256618A5162A258CA17B,
	Decoder_totalBitsInLayer_m805B43C4EAB23AD9A2508A710E3BA661046BC06B,
	Decoder__ctor_m5CFC2A1F4328BD3443EEE8ABCFAD885AE44A8426,
	Decoder__cctor_mC5B1E6BAC27224BEC5E2280B2FE40949B076A3D5,
	Detector__ctor_mA3F037185930CEB5584A79637E33D3A60AB8C00D,
	Detector_detect_mC36868ADC2112A2752DB231C145B94AD611DE6D1,
	Detector_detect_m5EE6D067CB4AFF977CE646AC2F5DAA9CE4C0D14F,
	Detector_extractParameters_m1DDB158B193FD0F6F0C8E99579663BD7ACCDA099,
	Detector_getRotation_m46AAD30137FD8BC570F70C8261A2A2BF0FED35D1,
	Detector_getCorrectedParameterData_m44C6D073EBC2D2C615F6B96B7F4AE0D0EF9E9EFB,
	Detector_getBullsEyeCorners_mB0610453F689A1B2754BDFC63B9720F8B16149BE,
	Detector_getMatrixCenter_m87D2C691DC365CD59DD1C2461F7CA17B4B1825BE,
	Detector_getMatrixCornerPoints_m1046C92DEB0B3624ECC45436BCBF17DD41A0AEC1,
	Detector_sampleGrid_mA656EA2FCFE420E8C1E427013E1F9BC4360B442A,
	Detector_sampleLine_m3F2092A40080AEC8ECC8600EA9DF671D8A3E0E15,
	Detector_isWhiteOrBlackRectangle_m2D50371158046253F0D435A619133E56FD366469,
	Detector_getColor_m1B9F53F7F6714BFB534C4404B04E8C4AA56C8B0D,
	Detector_getFirstDifferent_m5FD38C7C9C0AE7767C9A46DCE5CBABFC9703AA39,
	Detector_expandSquare_mF45ACEA75AECDE87BFB25149590F27B4C0945A0E,
	Detector_isValid_m9F77938FA17D7CBFF401F7924327BDD815CCC684,
	Detector_isValid_m1FD2700BCED185A1AFE373539312E3B4E2AC2BB8,
	Detector_distance_m765C3C333C4376746142B21C25B6694CE9F91498,
	Detector_distance_mEB134F0DB2455BA849226532AD86E28380A6D342,
	Detector_getDimension_m7F0A2EF9EA5F7828785438F49AC2D1E582BDEA45,
	Detector__cctor_m421D861C5A6505AC6E6A58797BA1ECE0FAACC3B2,
	Point_get_X_mEE048E5CFDD4522F3F1AD9DD798A0ED6AED64B0D,
	Point_set_X_m36F39BFA4A702392864A8516504ED08CAEF0B912,
	Point_get_Y_mD6F4B36F9C34D3115ED81D8AC1D266DB7BC8CBE0,
	Point_set_Y_m97FB36260991E183F0D4DE988EFA16C20A6AF771,
	Point_toResultPoint_m498027A9A371D0E922F705E05323C98FDE1E8DB9,
	Point__ctor_m06CF8AB3BAA79ABFB5A66FC834AD48989ED049C2,
	Point_ToString_m31F0012C02609FAF2FB6B667C8F97227783BE857,
	AztecCode_get_isCompact_m69FD2B81D44068FBBA6BF287FE7FBCA948EFC5B5,
	AztecCode_set_isCompact_m4D938F46C5A2A2084B664F665BD82A5D55220A8E,
	AztecCode_get_Size_m4BAC964B53FB59213876BCD77716959747604A42,
	AztecCode_set_Size_m574136E98A858CFD70D39E7537846A5705F863CA,
	AztecCode_get_Layers_m33D6AC4FD1CB92FADDAB5AFE1722E5E69F3D0C09,
	AztecCode_set_Layers_m1DE8F88B27C1394B796C63A3906C3E2CEDFFD42E,
	AztecCode_get_CodeWords_m3DA2B05FF16A0F6CDD222DD78058DDE505C5F0DA,
	AztecCode_set_CodeWords_mAF07A49B2A93C94876008227578B100FF1E9E649,
	AztecCode_get_Matrix_mFAB881CE73E5A8FFA739CF57B1A09386EE08DA9E,
	AztecCode_set_Matrix_mDB6CA59D1FF5B2CC1364B2059088E7D294F8E6EE,
	AztecCode__ctor_mB8C4EE157F5B3ECB35691135621C3A4C0A5BD791,
	BinaryShiftToken__ctor_mD645FFE62473F3EE8A531760DA6B3177E7B29475,
	BinaryShiftToken_appendTo_m74811009F22D1B5EA7DA9F6BEDEE32F18DF6EF12,
	BinaryShiftToken_ToString_m6193FA8A97AF92FACA576EF68AA20964BDC86BD3,
	Encoder_encode_mE24F3E069F3CD0F6F12952FA0A717A953B237247,
	Encoder_encode_m8AE979BFC084695CBCC0CCE3F215C45B541A316A,
	Encoder_drawBullsEye_m58F92CE923B122619B5DCCB88A06C2CD0EF5CDB4,
	Encoder_generateModeMessage_mA86608FB3DF26579C7FE27BDC096859E459909F9,
	Encoder_drawModeMessage_m02AEC5F2FB33A9E9BFBC3D56416E5E0E5CAC5EC0,
	Encoder_generateCheckWords_m533D2727891AEDEED6E24D1611EDF0DF1ADC44CC,
	Encoder_bitsToWords_mE8FABF720703844E657F5D787FFE93A59C7A1FC0,
	Encoder_getGF_m32F01B5B38672AF87DA19151DF2A169DDAC9C617,
	Encoder_stuffBits_m5E129EF2C2E5DDBECC13D4BE8EC1E9B3478237CC,
	Encoder_TotalBitsInLayer_m24841485CD1A8CEA70BA50F384023CEDDD478232,
	Encoder__cctor_mE4DE1D58427CF7C33125F6126C50E0A332837EE0,
	HighLevelEncoder__cctor_mC0F6D45211693A42DBCD68E86EFC26E395D5C7C1,
	HighLevelEncoder__ctor_m6183D593CF192430A3505E7CC9FE9AB150124C14,
	HighLevelEncoder_encode_m0DBC61E51351B0B00223122A5821912A5E063B0E,
	HighLevelEncoder_updateStateListForChar_m07101764BC528E73586360441081B487700AA6BB,
	HighLevelEncoder_updateStateForChar_m16A30AE4AC2EE7D0C04DD29D8ED6F1786161CE42,
	HighLevelEncoder_updateStateListForPair_mC4FF70F78F74C1B67FAD58B9CA23618DA3FB273C,
	HighLevelEncoder_updateStateForPair_m31DA2C18596BE8F153DFC924656653A436A4E11C,
	HighLevelEncoder_simplifyStates_mA19C902477CC1EB73B87F2B5A87557F7268F0269,
	SimpleToken__ctor_mB048A551BBF58C0407ADBEEFB2F7A1F4DF03D0B2,
	SimpleToken_appendTo_m4C8937DF31970AAF5772895EBDF090CA64701020,
	SimpleToken_ToString_m79770FE8DD4B006E567848DD0980F56C25FAD113,
	State__ctor_m6447D0311278C561A8EFBBD7E2B2E43397DC1CF2,
	State_get_Mode_m4436AA6CB58AD14939CB183C331AA418F8FF7216,
	State_get_Token_m88AC53BB8F5B296F24960AD16B74E7C1CD25C29D,
	State_get_BinaryShiftByteCount_m271DAD3BB98C84DFA9E5DFB0F4ED3CBD7F0A6B1F,
	State_get_BitCount_m52B9A84CF4483AEB7042331BA881F9CD44B88321,
	State_latchAndAppend_m5CA1DB20EC7F4F0F09A312297F5ED27A850C1946,
	State_shiftAndAppend_mEED73AEFB1E24794FD91A0A12CC294EE76552375,
	State_addBinaryShiftChar_m065C2AA0B61A61A21E6F719881BFED020F5E74C3,
	State_endBinaryShift_m678C7C2FC95810C512799F280C87B27E36A4F08E,
	State_isBetterThanOrEqualTo_mF4BA2E9B5B429AD0281CCBDA33C39662297A3D71,
	State_toBitArray_mEC4C2D3D7D5AE27BC6A6E0EE2396AF0E161FF726,
	State_ToString_m782270C7BD6F9E9516DDA1538AE44125B320A978,
	State_calculateBinaryShiftCost_mB0FBF9E51F383611AEFB8FDFB52712E245CE3840,
	State__cctor_m26AD9C470177A5366FAB2631B0A4F1789AA8E832,
	Token__ctor_m307BB304EC6BC8BF180260E0F368B1A4BA3555B5,
	Token_get_Previous_mF68DF3B7A53C17306BF3DD71CE7D9ECFDF7943A7,
	Token_add_mF294D4F03C0ED6CB2621366E3464A0D194D6CF0C,
	Token_addBinaryShift_m9B06AF501CDCA405CC5D2FDCD11790E6D92BCB93,
	NULL,
	Token__cctor_mCDC3233BCF4A0470E634B745D329A32E758C3485,
};
static const int32_t s_InvokerIndices[2318] = 
{
	7557,
	9442,
	7558,
	7597,
	5478,
	5478,
	9247,
	9289,
	13922,
	12478,
	12478,
	12478,
	12478,
	13922,
	12869,
	12869,
	12869,
	13915,
	12478,
	12478,
	12478,
	12478,
	12478,
	12478,
	13922,
	12869,
	12869,
	12869,
	13922,
	13922,
	12869,
	12869,
	12869,
	14502,
	9442,
	6622,
	4003,
	9442,
	7558,
	7597,
	7597,
	7597,
	5478,
	5478,
	9247,
	9289,
	13922,
	6353,
	6353,
	13753,
	13922,
	12478,
	12478,
	12478,
	12478,
	13922,
	12869,
	12869,
	12869,
	12869,
	12869,
	12862,
	13922,
	12869,
	11145,
	12869,
	11811,
	13915,
	13914,
	13753,
	14189,
	12478,
	12478,
	12478,
	12478,
	12478,
	12478,
	13922,
	12869,
	12869,
	12869,
	12869,
	12869,
	13922,
	13922,
	12869,
	12869,
	12869,
	12863,
	12869,
	10764,
	11345,
	11067,
	14502,
	9442,
	6622,
	4003,
	4368,
	9442,
	2423,
	1717,
	9289,
	2083,
	2083,
	14502,
	14502,
	9442,
	2083,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9289,
	7597,
	9289,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	9161,
	7469,
	9161,
	7469,
	9289,
	9442,
	2423,
	6745,
	6745,
	7597,
	7597,
	7597,
	1438,
	1438,
	14502,
	4368,
	4368,
	14502,
	9442,
	6745,
	1438,
	9442,
	9442,
	9442,
	0,
	0,
	0,
	0,
	0,
	9247,
	7557,
	9289,
	7597,
	9289,
	7597,
	9442,
	7597,
	6745,
	4002,
	2387,
	3441,
	9289,
	9289,
	9289,
	9161,
	1407,
	9161,
	9161,
	9289,
	0,
	7597,
	9289,
	0,
	0,
	0,
	9247,
	9247,
	7597,
	7597,
	9247,
	9247,
	3441,
	9289,
	9161,
	1407,
	9161,
	9289,
	9289,
	9289,
	4002,
	9247,
	9247,
	5478,
	9247,
	9289,
	9442,
	7597,
	7597,
	4368,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	7597,
	3441,
	9289,
	9161,
	1407,
	9161,
	9289,
	9289,
	9289,
	4002,
	0,
	0,
	9247,
	7557,
	9247,
	7557,
	9161,
	1407,
	9161,
	9289,
	9289,
	9161,
	9289,
	9289,
	6745,
	3462,
	6745,
	7597,
	9442,
	6745,
	9442,
	14502,
	14458,
	1438,
	693,
	9442,
	14502,
	9442,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	146,
	2387,
	3441,
	9289,
	9161,
	1407,
	9289,
	9247,
	9247,
	4002,
	2083,
	0,
	0,
	0,
	9442,
	7597,
	7597,
	4368,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9247,
	7557,
	9289,
	7597,
	9248,
	7558,
	9247,
	7557,
	1715,
	851,
	859,
	397,
	4037,
	7597,
	7597,
	9289,
	9442,
	4437,
	9343,
	9343,
	5478,
	9247,
	9289,
	14256,
	12951,
	11873,
	4362,
	7597,
	2094,
	7597,
	4002,
	2387,
	1673,
	1675,
	2083,
	11670,
	4359,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	10623,
	0,
	13922,
	0,
	0,
	0,
	13914,
	13749,
	0,
	4002,
	2387,
	7597,
	2083,
	9165,
	7474,
	9165,
	7474,
	9442,
	2084,
	1444,
	0,
	0,
	9442,
	7597,
	4368,
	0,
	0,
	9289,
	2307,
	9289,
	7597,
	9247,
	7557,
	9247,
	7557,
	9165,
	7474,
	9165,
	7474,
	9442,
	2084,
	1444,
	9165,
	7474,
	9165,
	7474,
	9289,
	7597,
	9247,
	7557,
	9442,
	2084,
	1444,
	852,
	3441,
	11345,
	10031,
	9289,
	7597,
	9247,
	7557,
	9247,
	7557,
	9442,
	4002,
	7597,
	9289,
	9442,
	9442,
	353,
	2419,
	1579,
	13665,
	13901,
	13901,
	13901,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	8996,
	7312,
	9442,
	9289,
	6745,
	3462,
	9442,
	13922,
	11544,
	9442,
	14502,
	1438,
	693,
	11138,
	9442,
	13922,
	7597,
	9289,
	9289,
	1968,
	9289,
	9442,
	7469,
	9442,
	4037,
	11811,
	9247,
	9289,
	12135,
	14502,
	14502,
	9442,
	2701,
	2701,
	2701,
	2701,
	2701,
	2701,
	2701,
	2701,
	11157,
	11546,
	11546,
	9928,
	14150,
	10763,
	11546,
	13753,
	14502,
	9442,
	3462,
	3462,
	3462,
	2759,
	2307,
	9247,
	9289,
	9247,
	9289,
	13914,
	14502,
	7557,
	12714,
	12841,
	12841,
	9289,
	9161,
	9247,
	5478,
	14502,
	9247,
	7557,
	2387,
	13914,
	6353,
	9247,
	7557,
	9289,
	14502,
	7469,
	9161,
	7597,
	2319,
	9247,
	9289,
	9247,
	9247,
	6745,
	13914,
	13914,
	13914,
	9289,
	9289,
	14458,
	14502,
	4037,
	9247,
	9247,
	9247,
	9289,
	4002,
	9247,
	9247,
	2464,
	1904,
	2107,
	226,
	9289,
	12264,
	5478,
	892,
	2083,
	7597,
	9289,
	9289,
	9289,
	6745,
	6745,
	10408,
	11810,
	10130,
	2144,
	3514,
	1486,
	1486,
	1473,
	2464,
	1744,
	9343,
	9247,
	1904,
	2107,
	7597,
	4368,
	9289,
	9289,
	6745,
	12264,
	13600,
	13600,
	9289,
	7597,
	7597,
	2701,
	892,
	892,
	942,
	1869,
	9247,
	9161,
	12646,
	9289,
	14502,
	3164,
	9442,
	7597,
	9289,
	9289,
	9289,
	4368,
	9289,
	9289,
	4002,
	9247,
	9247,
	3124,
	2304,
	9289,
	2299,
	2299,
	7469,
	9289,
	13753,
	12869,
	11811,
	11157,
	10948,
	13749,
	13922,
	12869,
	13600,
	10948,
	12843,
	11515,
	13183,
	10023,
	11138,
	12862,
	13266,
	11296,
	11353,
	13266,
	13266,
	12197,
	13266,
	13266,
	14502,
	13753,
	13753,
	13753,
	11538,
	10753,
	13753,
	11511,
	12725,
	14256,
	10646,
	13266,
	12184,
	13266,
	12184,
	13749,
	12714,
	12184,
	13266,
	13595,
	14256,
	14256,
	12132,
	12132,
	12132,
	12132,
	14256,
	13266,
	14502,
	9442,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9247,
	7557,
	9289,
	7597,
	9289,
	13595,
	14502,
	9161,
	7469,
	9247,
	7557,
	9289,
	7597,
	9247,
	7557,
	9247,
	7557,
	9343,
	7646,
	9289,
	7597,
	9161,
	7469,
	9442,
	13753,
	13922,
	13750,
	14502,
	6745,
	3462,
	6745,
	3462,
	11808,
	12729,
	12729,
	13753,
	13753,
	9442,
	9442,
	9247,
	7557,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9247,
	7557,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9248,
	7558,
	9247,
	7557,
	9248,
	7558,
	9442,
	693,
	1438,
	9851,
	12862,
	13922,
	9442,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	1579,
	7557,
	9289,
	6319,
	9442,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	10409,
	13922,
	866,
	7597,
	12869,
	2056,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	1579,
	9247,
	9161,
	5439,
	9442,
	9289,
	14502,
	14502,
	12869,
	13922,
	11672,
	11672,
	11346,
	10169,
	11672,
	12862,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9247,
	7557,
	9247,
	9247,
	4368,
	9289,
	7597,
	9247,
	9247,
	9442,
	9247,
	9247,
	11649,
	2307,
	12478,
	9289,
	9289,
	7597,
	9289,
	7597,
	7597,
	6319,
	6319,
	6739,
	6739,
	6319,
	4037,
	9289,
	9161,
	7469,
	4345,
	9442,
	7597,
	9289,
	7597,
	9289,
	4368,
	9289,
	14502,
	13753,
	13922,
	13753,
	13753,
	13753,
	9859,
	12869,
	13922,
	13753,
	12869,
	9978,
	12478,
	13922,
	10369,
	13922,
	12476,
	10918,
	9769,
	9968,
	13749,
	9945,
	11511,
	11803,
	11679,
	12476,
	13914,
	13749,
	13753,
	13922,
	14502,
	11808,
	12825,
	11802,
	12197,
	9971,
	9845,
	11679,
	9442,
	14502,
	9289,
	7597,
	9289,
	7597,
	4368,
	2299,
	2308,
	9442,
	9289,
	9289,
	3440,
	7557,
	6847,
	4064,
	3947,
	3662,
	6739,
	1579,
	9247,
	9247,
	9247,
	9247,
	9442,
	7469,
	9289,
	11648,
	10852,
	12132,
	815,
	813,
	675,
	7646,
	1579,
	7557,
	7469,
	7597,
	7469,
	14502,
	13749,
	12714,
	13749,
	12862,
	14502,
	14502,
	11141,
	13922,
	12869,
	12894,
	10242,
	10622,
	11329,
	13611,
	13611,
	13611,
	13611,
	13611,
	13611,
	12727,
	12727,
	10341,
	13183,
	9442,
	945,
	2091,
	6745,
	2094,
	9289,
	7597,
	9289,
	7597,
	4002,
	3440,
	3124,
	3124,
	6319,
	6319,
	6319,
	3124,
	9247,
	14502,
	4368,
	9289,
	9247,
	9161,
	6319,
	6319,
	6745,
	6745,
	6745,
	9289,
	6739,
	3440,
	9289,
	9442,
	2064,
	5439,
	5478,
	7557,
	9247,
	12480,
	6319,
	14502,
	9289,
	6745,
	9442,
	14502,
	9161,
	7469,
	9442,
	13922,
	10762,
	2064,
	9442,
	14502,
	9289,
	693,
	6745,
	12727,
	1983,
	9442,
	14502,
	14458,
	9442,
	7469,
	3659,
	2064,
	12869,
	13753,
	12449,
	13922,
	14502,
	9289,
	6745,
	13183,
	13922,
	9442,
	14502,
	9442,
	2064,
	6745,
	13753,
	12449,
	13922,
	13600,
	11538,
	14502,
	9289,
	6745,
	10921,
	11670,
	12727,
	13922,
	9442,
	14502,
	9442,
	1988,
	9247,
	12476,
	14502,
	9289,
	6745,
	9442,
	14502,
	9442,
	1988,
	9247,
	9289,
	6745,
	9442,
	14502,
	6745,
	4368,
	9442,
	9442,
	2064,
	10754,
	6745,
	2759,
	13753,
	6745,
	11803,
	12473,
	9442,
	14502,
	9289,
	6745,
	9442,
	14502,
	9442,
	7469,
	2064,
	3462,
	2084,
	4359,
	3163,
	12449,
	13753,
	14502,
	9289,
	6745,
	9442,
	14502,
	7597,
	2064,
	9442,
	7597,
	2064,
	9442,
	0,
	1438,
	693,
	11138,
	14256,
	10921,
	9247,
	0,
	13922,
	9442,
	14502,
	6745,
	3462,
	9442,
	3462,
	11539,
	10756,
	11539,
	11679,
	0,
	9442,
	14502,
	13675,
	2064,
	4785,
	9442,
	14502,
	3662,
	9161,
	7469,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9442,
	9442,
	9442,
	9289,
	6745,
	9442,
	14502,
	1413,
	2064,
	3462,
	9247,
	1988,
	13922,
	9442,
	1438,
	693,
	9442,
	2064,
	1988,
	13922,
	9442,
	2064,
	1988,
	13753,
	12449,
	13922,
	13922,
	9442,
	14502,
	2063,
	9442,
	14502,
	14502,
	9442,
	13922,
	2064,
	1413,
	5478,
	13600,
	13401,
	3459,
	11134,
	10381,
	10124,
	0,
	0,
	9247,
	9442,
	9442,
	1988,
	3459,
	5478,
	12476,
	9247,
	13922,
	14502,
	9289,
	6745,
	9442,
	14502,
	9442,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	11544,
	13753,
	13266,
	13266,
	13600,
	14502,
	9247,
	7557,
	9247,
	7557,
	4002,
	9289,
	5478,
	9247,
	9247,
	7557,
	9289,
	7597,
	9289,
	7597,
	781,
	5478,
	9247,
	9289,
	7597,
	9247,
	7557,
	2307,
	9442,
	9442,
	2064,
	13266,
	9442,
	12869,
	12478,
	1426,
	2088,
	3456,
	1434,
	2645,
	14502,
	11668,
	12714,
	13922,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	2423,
	9161,
	9289,
	5478,
	12478,
	9247,
	13753,
	2380,
	9289,
	7597,
	9247,
	7557,
	5478,
	9289,
	5478,
	9247,
	9289,
	2064,
	9442,
	2705,
	6723,
	3459,
	13600,
	3947,
	13266,
	12478,
	9289,
	13922,
	9161,
	12727,
	2091,
	1873,
	14256,
	2081,
	1451,
	11533,
	5439,
	9442,
	14502,
	7597,
	9289,
	9289,
	0,
	13922,
	7597,
	4359,
	6319,
	7597,
	4359,
	6319,
	7597,
	9289,
	7597,
	9289,
	14502,
	2423,
	9289,
	4359,
	4359,
	6319,
	14502,
	7597,
	9289,
	14502,
	7597,
	9289,
	14502,
	7597,
	4359,
	2387,
	13259,
	14502,
	7597,
	2387,
	0,
	0,
	7597,
	9289,
	14502,
	7469,
	4345,
	9289,
	9161,
	9442,
	9247,
	7557,
	7557,
	9161,
	9161,
	9161,
	9442,
	9442,
	9442,
	4123,
	9421,
	9161,
	14502,
	4037,
	2318,
	9289,
	9161,
	9247,
	2304,
	9247,
	9247,
	9247,
	9161,
	9161,
	14502,
	9247,
	7557,
	7557,
	14502,
	13922,
	11776,
	11776,
	7597,
	3459,
	5439,
	6739,
	3124,
	11670,
	3441,
	9289,
	9289,
	9289,
	9289,
	5439,
	6739,
	5439,
	6739,
	5439,
	5439,
	5439,
	7597,
	6745,
	3462,
	9442,
	12183,
	7597,
	6745,
	3462,
	403,
	11802,
	6745,
	3462,
	9442,
	0,
	0,
	6745,
	3462,
	13922,
	12729,
	9442,
	14502,
	7597,
	6745,
	14502,
	4368,
	9289,
	6745,
	14502,
	3164,
	9442,
	6745,
	3462,
	9442,
	13922,
	9442,
	14502,
	7597,
	9289,
	14502,
	12862,
	12715,
	12729,
	13753,
	13753,
	13753,
	13753,
	13922,
	11802,
	14502,
	9442,
	6745,
	3462,
	456,
	14502,
	3462,
	9442,
	6981,
	6745,
	258,
	6745,
	151,
	1168,
	1218,
	2064,
	9442,
	6745,
	3462,
	9442,
	13922,
	11544,
	9442,
	14502,
	1438,
	693,
	11151,
	11802,
	9442,
	9008,
	7326,
	9289,
	7597,
	9289,
	7597,
	8996,
	7312,
	9442,
	9247,
	7597,
	13013,
	9442,
	9247,
	7597,
	13011,
	9442,
	9247,
	7597,
	1242,
	13266,
	4368,
	3235,
	12862,
	9442,
	9442,
	9247,
	6319,
	2387,
	9247,
	9247,
	9289,
	2701,
	2299,
	2701,
	9442,
	1579,
	2304,
	7557,
	7557,
	7557,
	7557,
	9247,
	7597,
	13266,
	13293,
	12862,
	9442,
	9442,
	0,
	0,
	14502,
	7597,
	7557,
	4368,
	7557,
	9421,
	9421,
	7597,
	7716,
	9247,
	7557,
	9442,
	9161,
	9247,
	9247,
	9442,
	7557,
	9442,
	9247,
	7557,
	9289,
	9289,
	9247,
	9289,
	9161,
	7469,
	14502,
	12869,
	12862,
	11138,
	13011,
	13922,
	10387,
	11670,
	10945,
	13753,
	13611,
	13611,
	13611,
	13611,
	13611,
	13611,
	13611,
	13611,
	12727,
	14266,
	14256,
	342,
	128,
	13914,
	12841,
	11773,
	11774,
	10366,
	9247,
	9247,
	9247,
	9247,
	9247,
	9247,
	9247,
	9247,
	6319,
	6319,
	9289,
	14502,
	9247,
	3235,
	9442,
	9247,
	7597,
	3235,
	4368,
	9442,
	7597,
	9289,
	13922,
	9289,
	916,
	1038,
	3124,
	3124,
	3124,
	3124,
	6745,
	4037,
	12869,
	9247,
	9289,
	13922,
	10766,
	12478,
	12478,
	12478,
	12132,
	12478,
	11547,
	12714,
	14502,
	9442,
	6745,
	6745,
	2759,
	357,
	9247,
	9247,
	9247,
	9247,
	9247,
	9247,
	9289,
	12841,
	9289,
	14458,
	14502,
	4037,
	2319,
	9247,
	9289,
	4002,
	9247,
	9247,
	7597,
	9289,
	11810,
	11817,
	6745,
	6745,
	6745,
	6745,
	5478,
	9859,
	3164,
	9247,
	9247,
	5439,
	3947,
	9442,
	7557,
	4359,
	7557,
	7557,
	13749,
	6319,
	6319,
	4002,
	4002,
	9442,
	1819,
	7469,
	9289,
	4002,
	7597,
	7597,
	1594,
	9442,
	13914,
	5478,
	9247,
	9289,
	9289,
	14502,
	9247,
	9247,
	9247,
	9247,
	7557,
	4002,
	1581,
	2307,
	13922,
	11811,
	2701,
	2299,
	4002,
	7597,
	7597,
	9442,
	1579,
	3441,
	4037,
	9442,
	9289,
	9289,
	9289,
	5478,
	9247,
	9289,
	3462,
	2094,
	2094,
	9289,
	7597,
	9247,
	9247,
	6319,
	9247,
	9289,
	14502,
	4037,
	13183,
	13183,
	13914,
	13922,
	9289,
	7597,
	9247,
	7557,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9161,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9289,
	7597,
	1717,
	412,
	825,
	228,
	9289,
	7597,
	7597,
	7597,
	9161,
	7469,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9442,
	4368,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4,
	1439,
	9442,
	9289,
	7597,
	9289,
	7597,
	4368,
	9247,
	7557,
	7557,
	13914,
	9289,
	7597,
	9247,
	7557,
	9247,
	7557,
	9161,
	7469,
	9247,
	7557,
	9161,
	7469,
	9442,
	7597,
	3441,
	9289,
	6745,
	7557,
	12473,
	14502,
	14458,
	14256,
	0,
	1439,
	12478,
	9442,
	14502,
	9289,
	7597,
	6745,
	9442,
	9887,
	12714,
	10028,
	10382,
	84,
	9654,
	7597,
	4368,
	9782,
	9782,
	9289,
	6745,
	12869,
	14502,
	2304,
	9289,
	9289,
	3440,
	12714,
	6319,
	6319,
	6319,
	3124,
	9247,
	9247,
	9289,
	14502,
	4368,
	9289,
	9247,
	9161,
	6319,
	6319,
	6745,
	6745,
	6739,
	3440,
	6745,
	9289,
	7597,
	2759,
	2091,
	6745,
	3462,
	7597,
	6739,
	4359,
	13754,
	11197,
	11193,
	13753,
	7597,
	9289,
	48,
	676,
	13922,
	11138,
	7597,
	1675,
	9289,
	1477,
	1462,
	915,
	11808,
	11808,
	9442,
	6745,
	11141,
	9442,
	6745,
	13922,
	9442,
	246,
	7,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	6745,
	11811,
	12869,
	9442,
	6745,
	9442,
	41,
	9289,
	9184,
	9161,
	8988,
	9161,
	9289,
	9289,
	9289,
	9289,
	9189,
	9189,
	13640,
	12822,
	13773,
	13640,
	14502,
	9289,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	866,
	6745,
	9442,
	14502,
	6745,
	13600,
	9442,
	14502,
	10,
	5478,
	12478,
	12478,
	9247,
	13753,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	14502,
	6745,
	12843,
	12843,
	9442,
	1563,
	9189,
	7501,
	9189,
	7501,
	9189,
	7501,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	9289,
	9289,
	6745,
	9442,
	14502,
	7597,
	9289,
	7597,
	6745,
	9442,
	9247,
	7557,
	9289,
	7557,
	9289,
	5478,
	9247,
	13266,
	13266,
	7597,
	4368,
	9289,
	7597,
	9289,
	7597,
	6745,
	9442,
	0,
	13922,
	13266,
	13266,
	13922,
	13922,
	13756,
	12476,
	11538,
	13922,
	13266,
	11159,
	12727,
	11159,
	13922,
	13753,
	9442,
	14502,
	6745,
	12197,
	9442,
	1717,
	1717,
	9289,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	6745,
	9442,
	6745,
	9442,
	2423,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	6745,
	9442,
	4368,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	4368,
	13922,
	12476,
	6745,
	13600,
	13600,
	9442,
	14502,
	6745,
	9442,
	6745,
	11146,
	12869,
	12197,
	11146,
	13922,
	13922,
	13922,
	13600,
	14256,
	12184,
	9442,
	14502,
	6745,
	11808,
	11808,
	13922,
	9442,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9247,
	7557,
	9421,
	7716,
	9289,
	7597,
	78,
	9289,
	6745,
	13600,
	13756,
	13749,
	14150,
	13756,
	13922,
	9442,
	14502,
	2423,
	1714,
	152,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	6745,
	9442,
	6745,
	3462,
	9442,
	9442,
	9161,
	7469,
	9247,
	7557,
	9247,
	7557,
	2248,
	14502,
	1438,
	693,
	9848,
	11802,
	9442,
	8996,
	7312,
	8996,
	7312,
	9442,
	9161,
	7469,
	9247,
	7557,
	9247,
	7557,
	839,
	6745,
	13922,
	13922,
	13756,
	12862,
	6745,
	6745,
	11670,
	12476,
	13922,
	12713,
	9442,
	14502,
	7597,
	9289,
	6723,
	5478,
	12727,
	12718,
	6745,
	9289,
	6745,
	716,
	1987,
	957,
	3164,
	1425,
	11802,
	2701,
	5478,
	12951,
	12951,
	9247,
	14502,
	9247,
	7557,
	9247,
	7557,
	9289,
	4002,
	9289,
	9161,
	7469,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9289,
	7597,
	9442,
	2387,
	4368,
	9289,
	13922,
	11802,
	12183,
	11761,
	11320,
	11802,
	11802,
	13914,
	12862,
	12713,
	14502,
	14502,
	7597,
	9289,
	3459,
	2390,
	11802,
	11329,
	13922,
	2387,
	4368,
	9289,
	1675,
	9247,
	9289,
	9247,
	9247,
	3440,
	3440,
	6739,
	6739,
	5478,
	6745,
	9289,
	13753,
	14502,
	7597,
	9289,
	3440,
	3440,
	0,
	14502,
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x0200000B, { 0, 6 } },
	{ 0x02000011, { 6, 3 } },
	{ 0x020000E4, { 21, 22 } },
	{ 0x06000178, { 9, 6 } },
	{ 0x0600017A, { 15, 5 } },
	{ 0x0600017F, { 20, 1 } },
};
extern const uint32_t g_rgctx_BarcodeReader_1__ctor_m800CCA602EB565109B48FDBB898987C886DD9348;
extern const uint32_t g_rgctx_BarcodeReader_1__ctor_m440050981E2EED770048B238B98463C843949063;
extern const uint32_t g_rgctx_BarcodeReader_1_get_CreateLuminanceSource_m3FE83F2D4BF4F3D53E3FE649854EAFD24AF08062;
extern const uint32_t g_rgctx_T_t8E85D83C0396DAE68BB7A259FCDCA4B87ABA80F0;
extern const uint32_t g_rgctx_Func_2_tFE0D5008EF0AB0530228EFA9CD85C33604186AC3;
extern const uint32_t g_rgctx_Func_2_Invoke_m5CC3D0079FFBE58CEF25E5D2B488F4EED78DAE73;
extern const uint32_t g_rgctx_BarcodeWriter_1_get_Renderer_m4D8424D13666C3D20B5797BFFE725F9BBB15F39C;
extern const uint32_t g_rgctx_IBarcodeRenderer_1_t1F95CEFE08C61BBA9C498F00C9A3A7AF73994442;
extern const uint32_t g_rgctx_IBarcodeRenderer_1_Render_mCBBFC9DA0B7D77FD2ED8E84581337B620B366A7C;
extern const uint32_t g_rgctx_Activator_CreateInstance_TisT_t5CCF354FDC1D5740E2FC1D7545657475F9CA3B42_m4538F5ECCE6DDA70689EB0783E08A08E83C6AB85;
extern const uint32_t g_rgctx_ICollection_1_tB320E870E9FF0DA25B8CA22B52D860D394313FF4;
extern const uint32_t g_rgctx_ICollection_1_Add_mFF693A2AD7CE1F34FB9413A5563845AC15157A49;
extern const uint32_t g_rgctx_ICollection_1_get_Count_mD668FCACB126611BE7ACAB2B12E3D025F7FF6DCB;
extern const uint32_t g_rgctx_IList_1_tEF9C2E99F3542574E5C61DCCC166B4449048087B;
extern const uint32_t g_rgctx_IList_1_RemoveAt_mD0EA75F8BAC8CBBEC6E8E2FFF1866365E6C99687;
extern const uint32_t g_rgctx_IEnumerable_1_tE832CC09667121E26B92CDC0FA667A9FC127B0E7;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m493439C2F1A69043AF800B1124EB6713AEE24604;
extern const uint32_t g_rgctx_IEnumerator_1_tFB25C13022A01B4E2ED0E4353A87092EF081342B;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_mA935F1D5970AC93B806C9B91A8ED925422E5A763;
extern const uint32_t g_rgctx_T_t03BC2A6622A0465E74038D1B5F2D238C26D20590;
extern const uint32_t g_rgctx_T_t4AF0D9DB9B042FA46700E4CCE94FAFB7804CE21A;
extern const uint32_t g_rgctx_Dictionary_2_tEBD7A2928EA8A1CFA67A0C4B7A3C10A1E494C844;
extern const uint32_t g_rgctx_Dictionary_2__ctor_m3682F0F9DD40ABAAB9579E036D0AB97A54D3B871;
extern const uint32_t g_rgctx_IDictionary_2_tF5A1A820A216A7EF96EED1729CC469D809B25DF4;
extern const uint32_t g_rgctx_IDictionary_2_Add_m1B816156128B7807021B1736540B43F4DF64BC19;
extern const uint32_t g_rgctx_ChangeNotifyDictionary_2_OnValueChanged_m343D233C3E308B0AEBE33860E157E63D1A375833;
extern const uint32_t g_rgctx_IDictionary_2_ContainsKey_m6A8BEADC5FB3C2FAA90A441058F7DB45C1C7B448;
extern const uint32_t g_rgctx_IDictionary_2_get_Keys_mE5C7D4F238610C2AFC89234353D43F1DC809CD87;
extern const uint32_t g_rgctx_IDictionary_2_Remove_mBD4AB63E982DDC13ADBC7B94F35DE1AEC35CACD4;
extern const uint32_t g_rgctx_IDictionary_2_TryGetValue_m76FDED594A4F1966ADB82DAB4ADEBCBB623BB52E;
extern const uint32_t g_rgctx_IDictionary_2_get_Values_mB6F7416A8ACC30712BBE5433B1E1353EB089C7DF;
extern const uint32_t g_rgctx_IDictionary_2_get_Item_mB0DA7EEE8D575D3E882345FBCE03BE37A3907025;
extern const uint32_t g_rgctx_IDictionary_2_set_Item_m9CD15010A75F4203D958077AD2ADF17E0D961875;
extern const uint32_t g_rgctx_ICollection_1_t87A6721779A278149B17B31616F5D6E15CE4137A;
extern const uint32_t g_rgctx_ICollection_1_Add_m45017FBF3DF48E5617D5D7050720B318759C3104;
extern const uint32_t g_rgctx_ICollection_1_Clear_mC21215A2D5E83DCD65B7C72FDAA5269FF23E4ACC;
extern const uint32_t g_rgctx_ICollection_1_Contains_m61EF7D91D247B0D899341E9ADB2335DA44F9875E;
extern const uint32_t g_rgctx_ICollection_1_CopyTo_m3361A01DD1C6F5ACFB711A2AAB7DFCFDA6B09CD9;
extern const uint32_t g_rgctx_ICollection_1_get_Count_m2E6EEDC7A4532B0DBCF3F87C873B6851A703BE2A;
extern const uint32_t g_rgctx_ICollection_1_get_IsReadOnly_mE8E2C155EF9AC26C15273BEA931245291DAF1E2A;
extern const uint32_t g_rgctx_ICollection_1_Remove_m94332BEB2BD8708FBDF06D43C7DE7A39AE222183;
extern const uint32_t g_rgctx_IEnumerable_1_t058DD37489B7AF0903BAA2AA2C9317C41D942588;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_mEF699FB65B923D29C4679C47A269EABF93752A58;
static const Il2CppRGCTXDefinition s_rgctxValues[43] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BarcodeReader_1__ctor_m800CCA602EB565109B48FDBB898987C886DD9348 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BarcodeReader_1__ctor_m440050981E2EED770048B238B98463C843949063 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BarcodeReader_1_get_CreateLuminanceSource_m3FE83F2D4BF4F3D53E3FE649854EAFD24AF08062 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t8E85D83C0396DAE68BB7A259FCDCA4B87ABA80F0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tFE0D5008EF0AB0530228EFA9CD85C33604186AC3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m5CC3D0079FFBE58CEF25E5D2B488F4EED78DAE73 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_BarcodeWriter_1_get_Renderer_m4D8424D13666C3D20B5797BFFE725F9BBB15F39C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IBarcodeRenderer_1_t1F95CEFE08C61BBA9C498F00C9A3A7AF73994442 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IBarcodeRenderer_1_Render_mCBBFC9DA0B7D77FD2ED8E84581337B620B366A7C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Activator_CreateInstance_TisT_t5CCF354FDC1D5740E2FC1D7545657475F9CA3B42_m4538F5ECCE6DDA70689EB0783E08A08E83C6AB85 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_tB320E870E9FF0DA25B8CA22B52D860D394313FF4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Add_mFF693A2AD7CE1F34FB9413A5563845AC15157A49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_get_Count_mD668FCACB126611BE7ACAB2B12E3D025F7FF6DCB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IList_1_tEF9C2E99F3542574E5C61DCCC166B4449048087B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IList_1_RemoveAt_mD0EA75F8BAC8CBBEC6E8E2FFF1866365E6C99687 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_tE832CC09667121E26B92CDC0FA667A9FC127B0E7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_m493439C2F1A69043AF800B1124EB6713AEE24604 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tFB25C13022A01B4E2ED0E4353A87092EF081342B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_mA935F1D5970AC93B806C9B91A8ED925422E5A763 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t03BC2A6622A0465E74038D1B5F2D238C26D20590 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t4AF0D9DB9B042FA46700E4CCE94FAFB7804CE21A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_tEBD7A2928EA8A1CFA67A0C4B7A3C10A1E494C844 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_m3682F0F9DD40ABAAB9579E036D0AB97A54D3B871 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IDictionary_2_tF5A1A820A216A7EF96EED1729CC469D809B25DF4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_Add_m1B816156128B7807021B1736540B43F4DF64BC19 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ChangeNotifyDictionary_2_OnValueChanged_m343D233C3E308B0AEBE33860E157E63D1A375833 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_ContainsKey_m6A8BEADC5FB3C2FAA90A441058F7DB45C1C7B448 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_get_Keys_mE5C7D4F238610C2AFC89234353D43F1DC809CD87 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_Remove_mBD4AB63E982DDC13ADBC7B94F35DE1AEC35CACD4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_TryGetValue_m76FDED594A4F1966ADB82DAB4ADEBCBB623BB52E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_get_Values_mB6F7416A8ACC30712BBE5433B1E1353EB089C7DF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_get_Item_mB0DA7EEE8D575D3E882345FBCE03BE37A3907025 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_set_Item_m9CD15010A75F4203D958077AD2ADF17E0D961875 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_t87A6721779A278149B17B31616F5D6E15CE4137A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Add_m45017FBF3DF48E5617D5D7050720B318759C3104 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Clear_mC21215A2D5E83DCD65B7C72FDAA5269FF23E4ACC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Contains_m61EF7D91D247B0D899341E9ADB2335DA44F9875E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_CopyTo_m3361A01DD1C6F5ACFB711A2AAB7DFCFDA6B09CD9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_get_Count_m2E6EEDC7A4532B0DBCF3F87C873B6851A703BE2A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_get_IsReadOnly_mE8E2C155EF9AC26C15273BEA931245291DAF1E2A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Remove_m94332BEB2BD8708FBDF06D43C7DE7A39AE222183 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t058DD37489B7AF0903BAA2AA2C9317C41D942588 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_mEF699FB65B923D29C4679C47A269EABF93752A58 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_QRCode_CodeGenModule;
const Il2CppCodeGenModule g_QRCode_CodeGenModule = 
{
	"QRCode.dll",
	2318,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	6,
	s_rgctxIndices,
	43,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
