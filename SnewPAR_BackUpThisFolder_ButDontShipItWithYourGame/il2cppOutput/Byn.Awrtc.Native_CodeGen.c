﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor()
extern void CloseEventArgs__ctor_mF2B76761766FC1EEA686E575C79F58E2BEC7A7F7 (void);
// 0x00000002 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(System.UInt16)
extern void CloseEventArgs__ctor_m7C2BD8A3B003C330CBBBE406A069B6B022672137 (void);
// 0x00000003 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(WebSocketSharpUnityMod.CloseStatusCode)
extern void CloseEventArgs__ctor_m84541D1F2E9B92B522BC184A580F04F225B122B9 (void);
// 0x00000004 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(WebSocketSharpUnityMod.PayloadData)
extern void CloseEventArgs__ctor_m989196EBA933B49BC1ECEB619AD20440A3D2A99A (void);
// 0x00000005 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(System.UInt16,System.String)
extern void CloseEventArgs__ctor_m5734B11540984613D124180934A61F32FA375832 (void);
// 0x00000006 System.Void WebSocketSharpUnityMod.CloseEventArgs::.ctor(WebSocketSharpUnityMod.CloseStatusCode,System.String)
extern void CloseEventArgs__ctor_m0C2BB8F628399643774DE73C6E451107719DC222 (void);
// 0x00000007 WebSocketSharpUnityMod.PayloadData WebSocketSharpUnityMod.CloseEventArgs::get_PayloadData()
extern void CloseEventArgs_get_PayloadData_m61FC405897BEF0E2A94802816160A5ABD4DE96AA (void);
// 0x00000008 System.UInt16 WebSocketSharpUnityMod.CloseEventArgs::get_Code()
extern void CloseEventArgs_get_Code_mED2A982C2C23D28CE77A1AF0ED506028705225A8 (void);
// 0x00000009 System.String WebSocketSharpUnityMod.CloseEventArgs::get_Reason()
extern void CloseEventArgs_get_Reason_mA400E4FE20C8816A0AF9650BF1F28FA1E820C935 (void);
// 0x0000000A System.Boolean WebSocketSharpUnityMod.CloseEventArgs::get_WasClean()
extern void CloseEventArgs_get_WasClean_mB25C8670B696CA41CF8FBE3079D8486DAA809BA2 (void);
// 0x0000000B System.Void WebSocketSharpUnityMod.CloseEventArgs::set_WasClean(System.Boolean)
extern void CloseEventArgs_set_WasClean_m6253D7A3075BDA7D7E70259C89E6AA60E42C42A4 (void);
// 0x0000000C System.Void WebSocketSharpUnityMod.ErrorEventArgs::.ctor(System.String)
extern void ErrorEventArgs__ctor_m6978EE4718336C163E973EC0BAE3C5CCA4CF125C (void);
// 0x0000000D System.Void WebSocketSharpUnityMod.ErrorEventArgs::.ctor(System.String,System.Exception)
extern void ErrorEventArgs__ctor_mA7711C2DB8ACCBD4614D8BE72A6C4D8C4F5E58EF (void);
// 0x0000000E System.Exception WebSocketSharpUnityMod.ErrorEventArgs::get_Exception()
extern void ErrorEventArgs_get_Exception_m65B0EAC3CCDA1564F2B3D04F9F81C9B90891967D (void);
// 0x0000000F System.String WebSocketSharpUnityMod.ErrorEventArgs::get_Message()
extern void ErrorEventArgs_get_Message_m2BE0962EDCEF2699CD6C8E662FFF65392A1F4B11 (void);
// 0x00000010 System.Byte[] WebSocketSharpUnityMod.Ext::compress(System.Byte[])
extern void Ext_compress_m485158E0BAC622C16ED36691F377D3C6E922CAD3 (void);
// 0x00000011 System.IO.MemoryStream WebSocketSharpUnityMod.Ext::compress(System.IO.Stream)
extern void Ext_compress_m34B1A8F2434EEF3BFEA0F0A7CDE14E46FCA7F133 (void);
// 0x00000012 System.Byte[] WebSocketSharpUnityMod.Ext::compressToArray(System.IO.Stream)
extern void Ext_compressToArray_mA0172DBC07B15EA5FB0DF45170FC861F3FC85923 (void);
// 0x00000013 System.Byte[] WebSocketSharpUnityMod.Ext::decompress(System.Byte[])
extern void Ext_decompress_mE99DFE5D5A1A579814540472474E8F43715F1384 (void);
// 0x00000014 System.IO.MemoryStream WebSocketSharpUnityMod.Ext::decompress(System.IO.Stream)
extern void Ext_decompress_m3508D81FFAD52F9C385B5A196DB1E07F4842DBA1 (void);
// 0x00000015 System.Byte[] WebSocketSharpUnityMod.Ext::decompressToArray(System.IO.Stream)
extern void Ext_decompressToArray_mFAC58DE341C493FA38D7342FEE50A30829BB526B (void);
// 0x00000016 System.Void WebSocketSharpUnityMod.Ext::times(System.UInt64,System.Action)
extern void Ext_times_m4760483BAEB311A2EDAB6A090BE5EE30A6242912 (void);
// 0x00000017 System.Byte[] WebSocketSharpUnityMod.Ext::Append(System.UInt16,System.String)
extern void Ext_Append_m52F69117520A2828940E23E0D90865E9AF06C517 (void);
// 0x00000018 System.String WebSocketSharpUnityMod.Ext::CheckIfAvailable(WebSocketSharpUnityMod.Server.ServerState,System.Boolean,System.Boolean,System.Boolean)
extern void Ext_CheckIfAvailable_mB6EC7D74388477AC0A518CBBD8ABF07E0629F40B (void);
// 0x00000019 System.String WebSocketSharpUnityMod.Ext::CheckIfAvailable(WebSocketSharpUnityMod.WebSocketState,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void Ext_CheckIfAvailable_m0DE519558AE633FAC2DEB088DB34E69235A785ED (void);
// 0x0000001A System.String WebSocketSharpUnityMod.Ext::CheckIfValidProtocols(System.String[])
extern void Ext_CheckIfValidProtocols_mEF3B763702F5C2A34F7B2E06FB49B93F32D32FF1 (void);
// 0x0000001B System.String WebSocketSharpUnityMod.Ext::CheckIfValidServicePath(System.String)
extern void Ext_CheckIfValidServicePath_m108F73486130B30A43F1452EE0387AC5779B20BF (void);
// 0x0000001C System.String WebSocketSharpUnityMod.Ext::CheckIfValidSessionID(System.String)
extern void Ext_CheckIfValidSessionID_mFCA33D9F2F166EBE107E5DB7C08947D4B0FAB640 (void);
// 0x0000001D System.String WebSocketSharpUnityMod.Ext::CheckIfValidWaitTime(System.TimeSpan)
extern void Ext_CheckIfValidWaitTime_m666389616B8717F1E5D0061B0A25F389C703A433 (void);
// 0x0000001E System.Boolean WebSocketSharpUnityMod.Ext::CheckWaitTime(System.TimeSpan,System.String&)
extern void Ext_CheckWaitTime_m8292A62B2623D507EEFCC27F9038FCCB0134C0BC (void);
// 0x0000001F System.Void WebSocketSharpUnityMod.Ext::Close(WebSocketSharpUnityMod.Net.HttpListenerResponse,WebSocketSharpUnityMod.Net.HttpStatusCode)
extern void Ext_Close_mB443764F003B2CF77CBFD5F56A738949D66063EC (void);
// 0x00000020 System.Void WebSocketSharpUnityMod.Ext::CloseWithAuthChallenge(WebSocketSharpUnityMod.Net.HttpListenerResponse,System.String)
extern void Ext_CloseWithAuthChallenge_m528D45A1D4CD907629B7255B8C4F6B9CB31A7879 (void);
// 0x00000021 System.Byte[] WebSocketSharpUnityMod.Ext::Compress(System.Byte[],WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_Compress_m108FD47E3EB0423AD5AAC45FC776AAAEB5375A68 (void);
// 0x00000022 System.IO.Stream WebSocketSharpUnityMod.Ext::Compress(System.IO.Stream,WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_Compress_m6C3332A2970C16969C76755A62D1C5DDA27BF246 (void);
// 0x00000023 System.Byte[] WebSocketSharpUnityMod.Ext::CompressToArray(System.IO.Stream,WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_CompressToArray_m9D0542478AE96CC59B8431F763AB1DA1067414B9 (void);
// 0x00000024 System.Boolean WebSocketSharpUnityMod.Ext::Contains(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<T,System.Boolean>)
// 0x00000025 System.Boolean WebSocketSharpUnityMod.Ext::ContainsTwice(System.String[])
extern void Ext_ContainsTwice_m68140E6F054F3391145DFD370795CEA26A5C9038 (void);
// 0x00000026 T[] WebSocketSharpUnityMod.Ext::Copy(T[],System.Int64)
// 0x00000027 System.Void WebSocketSharpUnityMod.Ext::CopyTo(System.IO.Stream,System.IO.Stream,System.Int32)
extern void Ext_CopyTo_m1ACB528FCC743B159F5269F46C59D823BFA20517 (void);
// 0x00000028 System.Void WebSocketSharpUnityMod.Ext::CopyToAsync(System.IO.Stream,System.IO.Stream,System.Int32,System.Action,System.Action`1<System.Exception>)
extern void Ext_CopyToAsync_mD5A93F57BE84D666153A16DACC2071FF251D68BD (void);
// 0x00000029 System.Byte[] WebSocketSharpUnityMod.Ext::Decompress(System.Byte[],WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_Decompress_mEB4E5DEE4DD2B99ECFECEA851412505073359A03 (void);
// 0x0000002A System.IO.Stream WebSocketSharpUnityMod.Ext::Decompress(System.IO.Stream,WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_Decompress_m5CB1F37EDCC6B7B11A2F917609978359C8928749 (void);
// 0x0000002B System.Byte[] WebSocketSharpUnityMod.Ext::DecompressToArray(System.IO.Stream,WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_DecompressToArray_m9B8B2875219F705CF8C2D14C7D7C2083ADACC421 (void);
// 0x0000002C System.Boolean WebSocketSharpUnityMod.Ext::EqualsWith(System.Int32,System.Char,System.Action`1<System.Int32>)
extern void Ext_EqualsWith_mBEE764F89FFA97EB05789C49139F944DB145AEB1 (void);
// 0x0000002D System.String WebSocketSharpUnityMod.Ext::GetAbsolutePath(System.Uri)
extern void Ext_GetAbsolutePath_mE06626FB7BEE7B3A54324D91DDFF63A5047E2983 (void);
// 0x0000002E System.String WebSocketSharpUnityMod.Ext::GetMessage(WebSocketSharpUnityMod.CloseStatusCode)
extern void Ext_GetMessage_mF64524CF56DE72157B99D3148366BA691C716543 (void);
// 0x0000002F System.String WebSocketSharpUnityMod.Ext::GetName(System.String,System.Char)
extern void Ext_GetName_mD8DD130D0C27607285A48A013413DA2D106D0650 (void);
// 0x00000030 System.String WebSocketSharpUnityMod.Ext::GetValue(System.String,System.Char)
extern void Ext_GetValue_m150D80CB6793F60F86064091D4EAE8E6F94A7D20 (void);
// 0x00000031 System.String WebSocketSharpUnityMod.Ext::GetValue(System.String,System.Char,System.Boolean)
extern void Ext_GetValue_m801AA9845996C6A649DA1F538BFD371B0C5268BE (void);
// 0x00000032 WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext WebSocketSharpUnityMod.Ext::GetWebSocketContext(System.Net.Sockets.TcpClient,System.String,System.Boolean,WebSocketSharpUnityMod.Net.ServerSslConfiguration,WebSocketSharpUnityMod.Logger)
extern void Ext_GetWebSocketContext_mDF86F38D91B781FF4E554810271BD0F1CCB62B72 (void);
// 0x00000033 System.Byte[] WebSocketSharpUnityMod.Ext::InternalToByteArray(System.UInt16,WebSocketSharpUnityMod.ByteOrder)
extern void Ext_InternalToByteArray_m91C3F342EF7F1DB6D3549FDD70E1701E40A9DAF2 (void);
// 0x00000034 System.Byte[] WebSocketSharpUnityMod.Ext::InternalToByteArray(System.UInt64,WebSocketSharpUnityMod.ByteOrder)
extern void Ext_InternalToByteArray_m50B23BD10801FF6C0FB8204B46F7EDDFEF30DDD2 (void);
// 0x00000035 System.Boolean WebSocketSharpUnityMod.Ext::IsCompressionExtension(System.String,WebSocketSharpUnityMod.CompressionMethod)
extern void Ext_IsCompressionExtension_mFFD1A76367C07F03E9AE59FC096F6A54EF83414C (void);
// 0x00000036 System.Boolean WebSocketSharpUnityMod.Ext::IsControl(System.Byte)
extern void Ext_IsControl_mFA1456BACC196E275312AC010A40517A9201921A (void);
// 0x00000037 System.Boolean WebSocketSharpUnityMod.Ext::IsControl(WebSocketSharpUnityMod.Opcode)
extern void Ext_IsControl_m423B3FF9B0E1D3998FBA5E00BB1CBFBF05D7BF69 (void);
// 0x00000038 System.Boolean WebSocketSharpUnityMod.Ext::IsData(System.Byte)
extern void Ext_IsData_m092786329B98B9D7A5A2D4EED7D5D7E496115FF7 (void);
// 0x00000039 System.Boolean WebSocketSharpUnityMod.Ext::IsData(WebSocketSharpUnityMod.Opcode)
extern void Ext_IsData_m951013EFCE345E54C629AADB8D9516AF5B586462 (void);
// 0x0000003A System.Boolean WebSocketSharpUnityMod.Ext::IsPortNumber(System.Int32)
extern void Ext_IsPortNumber_m040DA9233A1A61E1DC52A0931ED7A9B573A04A53 (void);
// 0x0000003B System.Boolean WebSocketSharpUnityMod.Ext::IsReserved(System.UInt16)
extern void Ext_IsReserved_mD6A7A606663A5D2142D9E7CD8AC443D0809C117D (void);
// 0x0000003C System.Boolean WebSocketSharpUnityMod.Ext::IsReserved(WebSocketSharpUnityMod.CloseStatusCode)
extern void Ext_IsReserved_m7B07270058ECC4EE5406D3858450825AE1A8FAF6 (void);
// 0x0000003D System.Boolean WebSocketSharpUnityMod.Ext::IsSupported(System.Byte)
extern void Ext_IsSupported_m4886F8A154117D4B97C9B40065619BC7F574469C (void);
// 0x0000003E System.Boolean WebSocketSharpUnityMod.Ext::IsText(System.String)
extern void Ext_IsText_mFF7A23F47E36B5957A0BEF7F8F7D71CE635EED7F (void);
// 0x0000003F System.Boolean WebSocketSharpUnityMod.Ext::IsToken(System.String)
extern void Ext_IsToken_m2BFAF5BFE7DCDE7172F2AB5E11E0E1B95356EAB3 (void);
// 0x00000040 System.String WebSocketSharpUnityMod.Ext::Quote(System.String)
extern void Ext_Quote_m75E0AB845C879E63A1603E6197F020A9AE2D8FB4 (void);
// 0x00000041 System.Byte[] WebSocketSharpUnityMod.Ext::ReadBytes(System.IO.Stream,System.Int32)
extern void Ext_ReadBytes_m581BDE8DBD808338EE3D9E0C8DDF94FEE930AA12 (void);
// 0x00000042 System.Byte[] WebSocketSharpUnityMod.Ext::ReadBytes(System.IO.Stream,System.Int64,System.Int32)
extern void Ext_ReadBytes_m48DBAF2CDF9F539014D14D1407762C79DD248D6C (void);
// 0x00000043 System.Void WebSocketSharpUnityMod.Ext::ReadBytesAsync(System.IO.Stream,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_mF916EB46545EAD8D72ABDEEEDE09C8BCA27AA04B (void);
// 0x00000044 System.Void WebSocketSharpUnityMod.Ext::ReadBytesAsync(System.IO.Stream,System.Int64,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_m2D7B3B848FCCA9AFA15DC2BF7B3A46788D135382 (void);
// 0x00000045 System.String WebSocketSharpUnityMod.Ext::RemovePrefix(System.String,System.String[])
extern void Ext_RemovePrefix_mAC46C41EC405D62BBA0E7BD3668151528A47B78C (void);
// 0x00000046 T[] WebSocketSharpUnityMod.Ext::Reverse(T[])
// 0x00000047 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharpUnityMod.Ext::SplitHeaderValue(System.String,System.Char[])
extern void Ext_SplitHeaderValue_mBFA61BC57519A89B664BB90CAD02A4062B48C5B4 (void);
// 0x00000048 System.Byte[] WebSocketSharpUnityMod.Ext::ToByteArray(System.IO.Stream)
extern void Ext_ToByteArray_mB715EA201A0347E4E4575B1D970200B394D9940B (void);
// 0x00000049 WebSocketSharpUnityMod.CompressionMethod WebSocketSharpUnityMod.Ext::ToCompressionMethod(System.String)
extern void Ext_ToCompressionMethod_m4756514838729031E8D615AC4BABAE0F717BDDD4 (void);
// 0x0000004A System.String WebSocketSharpUnityMod.Ext::ToExtensionString(WebSocketSharpUnityMod.CompressionMethod,System.String[])
extern void Ext_ToExtensionString_m5C2BB01900C06FDFCEE4AF2D3A0E572C886FBE47 (void);
// 0x0000004B System.Net.IPAddress WebSocketSharpUnityMod.Ext::ToIPAddress(System.String)
extern void Ext_ToIPAddress_m151DBF434D73374CCE6CFBF63FA574CE29961E83 (void);
// 0x0000004C System.Collections.Generic.List`1<TSource> WebSocketSharpUnityMod.Ext::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004D System.UInt16 WebSocketSharpUnityMod.Ext::ToUInt16(System.Byte[],WebSocketSharpUnityMod.ByteOrder)
extern void Ext_ToUInt16_m06DB102E5C75F8C94B93DB229B719A62F7EAE79D (void);
// 0x0000004E System.UInt64 WebSocketSharpUnityMod.Ext::ToUInt64(System.Byte[],WebSocketSharpUnityMod.ByteOrder)
extern void Ext_ToUInt64_mD87D7CC89DE0A7386B8B6C2E420603500930687A (void);
// 0x0000004F System.String WebSocketSharpUnityMod.Ext::TrimEndSlash(System.String)
extern void Ext_TrimEndSlash_m1C5C26EC1304CF822D143243C77E6DD628454B9E (void);
// 0x00000050 System.Boolean WebSocketSharpUnityMod.Ext::TryCreateWebSocketUri(System.String,System.Uri&,System.String&)
extern void Ext_TryCreateWebSocketUri_m6ECDEB2A7F57781B66B5E6C5342931460E30C82E (void);
// 0x00000051 System.String WebSocketSharpUnityMod.Ext::Unquote(System.String)
extern void Ext_Unquote_m0EDDE0A9584C643A0AD5560B4515CE4AEFAB04D7 (void);
// 0x00000052 System.String WebSocketSharpUnityMod.Ext::UTF8Decode(System.Byte[])
extern void Ext_UTF8Decode_m2E520D6039D0A076440B5E6248DB500A2574E0E9 (void);
// 0x00000053 System.Byte[] WebSocketSharpUnityMod.Ext::UTF8Encode(System.String)
extern void Ext_UTF8Encode_mAAF3200AF93F0D779DDBD35A6A4C7237DD83D447 (void);
// 0x00000054 System.Void WebSocketSharpUnityMod.Ext::WriteBytes(System.IO.Stream,System.Byte[],System.Int32)
extern void Ext_WriteBytes_m3904B44AF581F2C833DE020AB5D40D979E231AE7 (void);
// 0x00000055 System.Void WebSocketSharpUnityMod.Ext::WriteBytesAsync(System.IO.Stream,System.Byte[],System.Int32,System.Action,System.Action`1<System.Exception>)
extern void Ext_WriteBytesAsync_m136D35DE8352379FCF68B15E471EC3D8B5B8E247 (void);
// 0x00000056 System.Boolean WebSocketSharpUnityMod.Ext::Contains(System.String,System.Char[])
extern void Ext_Contains_m57CAB022534ADF189526AF14BAC4B9268600A73D (void);
// 0x00000057 System.Boolean WebSocketSharpUnityMod.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String)
extern void Ext_Contains_m95619F286FE8CD1385A2D96762AB4466ED2D0A56 (void);
// 0x00000058 System.Boolean WebSocketSharpUnityMod.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String,System.String)
extern void Ext_Contains_mC11B223DEF28DAAD738FB4200496C33807EBCB3E (void);
// 0x00000059 System.Void WebSocketSharpUnityMod.Ext::Emit(System.EventHandler,System.Object,System.EventArgs)
extern void Ext_Emit_mA500E38C534CC0B9B09969CBDF598EA256B576C3 (void);
// 0x0000005A System.Void WebSocketSharpUnityMod.Ext::Emit(System.EventHandler`1<TEventArgs>,System.Object,TEventArgs)
// 0x0000005B WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Ext::GetCookies(System.Collections.Specialized.NameValueCollection,System.Boolean)
extern void Ext_GetCookies_mFE982227ECA6E14B9C8E8C38334593E12B6B4D49 (void);
// 0x0000005C System.String WebSocketSharpUnityMod.Ext::GetDescription(WebSocketSharpUnityMod.Net.HttpStatusCode)
extern void Ext_GetDescription_m78EF8904EC42BFA2E9B44FEB7F5234B9799144B0 (void);
// 0x0000005D System.String WebSocketSharpUnityMod.Ext::GetStatusDescription(System.Int32)
extern void Ext_GetStatusDescription_m6ABF3F8A47DBA46D0A2BC4DB027A1433D1329DBE (void);
// 0x0000005E System.Boolean WebSocketSharpUnityMod.Ext::IsCloseStatusCode(System.UInt16)
extern void Ext_IsCloseStatusCode_m2362BEEC3FC6A136CD8CC116F0E6592C207A37CD (void);
// 0x0000005F System.Boolean WebSocketSharpUnityMod.Ext::IsEnclosedIn(System.String,System.Char)
extern void Ext_IsEnclosedIn_m2E1BF90B8CDC75226704D8D47C42699C64BF2180 (void);
// 0x00000060 System.Boolean WebSocketSharpUnityMod.Ext::IsHostOrder(WebSocketSharpUnityMod.ByteOrder)
extern void Ext_IsHostOrder_mCA66CE42F50BD9D0C3DEA95851B31854E413219F (void);
// 0x00000061 System.Boolean WebSocketSharpUnityMod.Ext::IsLocal(System.Net.IPAddress)
extern void Ext_IsLocal_m86D0AA69F5ABE6EE8BA0268579AB083D2DEBB9B6 (void);
// 0x00000062 System.Boolean WebSocketSharpUnityMod.Ext::IsNullOrEmpty(System.String)
extern void Ext_IsNullOrEmpty_m6D5613A66F684263FF6484CC129ADF9C0476B8FA (void);
// 0x00000063 System.Boolean WebSocketSharpUnityMod.Ext::IsPredefinedScheme(System.String)
extern void Ext_IsPredefinedScheme_mD5AC83D673512CDA373D179742CA62A23892CBA5 (void);
// 0x00000064 System.Boolean WebSocketSharpUnityMod.Ext::IsUpgradeTo(WebSocketSharpUnityMod.Net.HttpListenerRequest,System.String)
extern void Ext_IsUpgradeTo_mA74827D1DDEE41ED7FB8396B024F00098BB9F0E5 (void);
// 0x00000065 System.Boolean WebSocketSharpUnityMod.Ext::MaybeUri(System.String)
extern void Ext_MaybeUri_mFEEA728C8A0A036B1524D85143146BC6A59B4580 (void);
// 0x00000066 T[] WebSocketSharpUnityMod.Ext::SubArray(T[],System.Int32,System.Int32)
// 0x00000067 T[] WebSocketSharpUnityMod.Ext::SubArray(T[],System.Int64,System.Int64)
// 0x00000068 System.Void WebSocketSharpUnityMod.Ext::Times(System.Int32,System.Action)
extern void Ext_Times_m60C27A4B0B4A411F61438B60F639DCADCA6A6175 (void);
// 0x00000069 System.Void WebSocketSharpUnityMod.Ext::Times(System.Int64,System.Action)
extern void Ext_Times_m92238BB482A078B92803E2C4C6BF37B774F36A85 (void);
// 0x0000006A System.Void WebSocketSharpUnityMod.Ext::Times(System.UInt32,System.Action)
extern void Ext_Times_mF4A874778E951D2837C9146FEF35D2531EA50483 (void);
// 0x0000006B System.Void WebSocketSharpUnityMod.Ext::Times(System.UInt64,System.Action)
extern void Ext_Times_mFE49D2EE8FD5DEB52F8D1A942AD2350B2C8B2A29 (void);
// 0x0000006C System.Void WebSocketSharpUnityMod.Ext::Times(System.Int32,System.Action`1<System.Int32>)
extern void Ext_Times_m129856A98AC3C4D5AF7ABAA4BC8912F4C347FFE8 (void);
// 0x0000006D System.Void WebSocketSharpUnityMod.Ext::Times(System.Int64,System.Action`1<System.Int64>)
extern void Ext_Times_m5EA97FB7203C06CFEBF7CAC3DA31D15887E5159B (void);
// 0x0000006E System.Void WebSocketSharpUnityMod.Ext::Times(System.UInt32,System.Action`1<System.UInt32>)
extern void Ext_Times_mAD15787D9E880EDDF62D16187D6F9F80E98A59FC (void);
// 0x0000006F System.Void WebSocketSharpUnityMod.Ext::Times(System.UInt64,System.Action`1<System.UInt64>)
extern void Ext_Times_m0B6FA309352DEA3933E8555CBDD0BF22488507E0 (void);
// 0x00000070 T WebSocketSharpUnityMod.Ext::To(System.Byte[],WebSocketSharpUnityMod.ByteOrder)
// 0x00000071 System.Byte[] WebSocketSharpUnityMod.Ext::ToByteArray(T,WebSocketSharpUnityMod.ByteOrder)
// 0x00000072 System.Byte[] WebSocketSharpUnityMod.Ext::ToHostOrder(System.Byte[],WebSocketSharpUnityMod.ByteOrder)
extern void Ext_ToHostOrder_mB56E8A9DA8E906C2578E7CD6B80913AFCE5A90D0 (void);
// 0x00000073 System.String WebSocketSharpUnityMod.Ext::ToString(T[],System.String)
// 0x00000074 System.Uri WebSocketSharpUnityMod.Ext::ToUri(System.String)
extern void Ext_ToUri_m501AF00613B3D9BD573941AF3DED7AE23400C089 (void);
// 0x00000075 System.String WebSocketSharpUnityMod.Ext::UrlDecode(System.String)
extern void Ext_UrlDecode_mE9286881DE7990E99F61DC78F4C9B952875864F8 (void);
// 0x00000076 System.String WebSocketSharpUnityMod.Ext::UrlEncode(System.String)
extern void Ext_UrlEncode_mD47864D8A9341036B5E7AEF469059B81BE87C941 (void);
// 0x00000077 System.Void WebSocketSharpUnityMod.Ext::WriteContent(WebSocketSharpUnityMod.Net.HttpListenerResponse,System.Byte[])
extern void Ext_WriteContent_m3D23C0B37B21A0CBA768BDDB99BA931F81822101 (void);
// 0x00000078 System.Void WebSocketSharpUnityMod.Ext::.cctor()
extern void Ext__cctor_m4EB4089287BC1F43021E0E92ACF2B36BA184AB92 (void);
// 0x00000079 System.Void WebSocketSharpUnityMod.Ext/<>c::.cctor()
extern void U3CU3Ec__cctor_mE1E176D0D6AFF75CD02AAB37CF5086961D5902A2 (void);
// 0x0000007A System.Void WebSocketSharpUnityMod.Ext/<>c::.ctor()
extern void U3CU3Ec__ctor_m03B14D44F4B933F669C8121DCC4CAB99E1D074CA (void);
// 0x0000007B System.Boolean WebSocketSharpUnityMod.Ext/<>c::<CheckIfValidProtocols>b__12_0(System.String)
extern void U3CU3Ec_U3CCheckIfValidProtocolsU3Eb__12_0_m91F5C1A4EB94A8F320A1F147A5E6B3DB23EFC0EB (void);
// 0x0000007C System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m01D2D7954FF11090FD99A99C7A57F60C5DC2C5EF (void);
// 0x0000007D System.Boolean WebSocketSharpUnityMod.Ext/<>c__DisplayClass23_0::<ContainsTwice>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass23_0_U3CContainsTwiceU3Eb__0_mE24E10F8B8933EED271D40C85AE75BA85AA01443 (void);
// 0x0000007E System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m34A9D8ECA2EC4206504C98E98C260178652017A4 (void);
// 0x0000007F System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass26_0::<CopyToAsync>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass26_0_U3CCopyToAsyncU3Eb__0_m2E192460B8D361CF496844C102213812A1A32F91 (void);
// 0x00000080 System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m363DDB10DEF49F563231D6EDE7CAD5A3781DE7BA (void);
// 0x00000081 System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass53_0::<ReadBytesAsync>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass53_0_U3CReadBytesAsyncU3Eb__0_m7BE7A9C46400DF6BC6ED061D9C79C334B118DECC (void);
// 0x00000082 System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass54_0::.ctor()
extern void U3CU3Ec__DisplayClass54_0__ctor_m524241D6EA0AA7774DC4A974F7F8FA699BD94A61 (void);
// 0x00000083 System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass54_0::<ReadBytesAsync>b__0(System.Int64)
extern void U3CU3Ec__DisplayClass54_0_U3CReadBytesAsyncU3Eb__0_m174A38F629E57DC9195D1B96BD29F9C965126F38 (void);
// 0x00000084 System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass54_1::.ctor()
extern void U3CU3Ec__DisplayClass54_1__ctor_m5E774EF7E67BC058302A0D23A0DD2105BF2E8ECF (void);
// 0x00000085 System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass54_1::<ReadBytesAsync>b__1(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass54_1_U3CReadBytesAsyncU3Eb__1_m5C19DE34B98021C970E5460BAAF8C2AF6491BC80 (void);
// 0x00000086 System.Void WebSocketSharpUnityMod.Ext/<SplitHeaderValue>d__57::.ctor(System.Int32)
extern void U3CSplitHeaderValueU3Ed__57__ctor_mF955B13C74AE0D37EF845F32BE9E8E3762E90A94 (void);
// 0x00000087 System.Void WebSocketSharpUnityMod.Ext/<SplitHeaderValue>d__57::System.IDisposable.Dispose()
extern void U3CSplitHeaderValueU3Ed__57_System_IDisposable_Dispose_mBCCB828DB6B5584F09E58A5FD906885F70BCF697 (void);
// 0x00000088 System.Boolean WebSocketSharpUnityMod.Ext/<SplitHeaderValue>d__57::MoveNext()
extern void U3CSplitHeaderValueU3Ed__57_MoveNext_m9E701C894410E560C75E256C23BB49FE6859A0AA (void);
// 0x00000089 System.String WebSocketSharpUnityMod.Ext/<SplitHeaderValue>d__57::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3CSplitHeaderValueU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mE9BC365BCEC7F5E3C2CE6293256ECB9CA43E3ED5 (void);
// 0x0000008A System.Void WebSocketSharpUnityMod.Ext/<SplitHeaderValue>d__57::System.Collections.IEnumerator.Reset()
extern void U3CSplitHeaderValueU3Ed__57_System_Collections_IEnumerator_Reset_m9699B98756BF307837D22B0FB1FA7413701D3705 (void);
// 0x0000008B System.Object WebSocketSharpUnityMod.Ext/<SplitHeaderValue>d__57::System.Collections.IEnumerator.get_Current()
extern void U3CSplitHeaderValueU3Ed__57_System_Collections_IEnumerator_get_Current_m80F74DE2FC76E75F5B52F5DDE6B8966AE8889803 (void);
// 0x0000008C System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharpUnityMod.Ext/<SplitHeaderValue>d__57::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__57_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m2E07293CA7EC06DCCA6E3E7F674E0F7AF515B54A (void);
// 0x0000008D System.Collections.IEnumerator WebSocketSharpUnityMod.Ext/<SplitHeaderValue>d__57::System.Collections.IEnumerable.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__57_System_Collections_IEnumerable_GetEnumerator_m84581C512C3B1239D2EDF9B63289E1B5167BB7AA (void);
// 0x0000008E System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass71_0::.ctor()
extern void U3CU3Ec__DisplayClass71_0__ctor_mC793B7E00DAC9672F6966C3D5CB425DB835D3083 (void);
// 0x0000008F System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass71_0::<WriteBytesAsync>b__0()
extern void U3CU3Ec__DisplayClass71_0_U3CWriteBytesAsyncU3Eb__0_mDC5894DC73F861A3AE9DD3C13CF88854EE4EC9F9 (void);
// 0x00000090 System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass71_0::<WriteBytesAsync>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass71_0_U3CWriteBytesAsyncU3Eb__1_m14DA387C049EB560EE112AD66EEAF93C6AE526CC (void);
// 0x00000091 System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass101_0`1::.ctor()
// 0x00000092 System.Void WebSocketSharpUnityMod.Ext/<>c__DisplayClass101_0`1::<ToString>b__0(System.Int32)
// 0x00000093 System.Void WebSocketSharpUnityMod.HttpBase::.ctor(System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpBase__ctor_m8931225CB535D6B6C9B7907B6413043B8C33EDA5 (void);
// 0x00000094 System.String WebSocketSharpUnityMod.HttpBase::get_EntityBody()
extern void HttpBase_get_EntityBody_m3BD76E1B557F1E25E95E47D04364C75AF3C6A365 (void);
// 0x00000095 System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.HttpBase::get_Headers()
extern void HttpBase_get_Headers_m205F9346FC52003B503465FE556465FB5E0F5A47 (void);
// 0x00000096 System.Version WebSocketSharpUnityMod.HttpBase::get_ProtocolVersion()
extern void HttpBase_get_ProtocolVersion_m22ADA88C17D3C1B111B40E1CACBD853D8D989096 (void);
// 0x00000097 System.Byte[] WebSocketSharpUnityMod.HttpBase::readEntityBody(System.IO.Stream,System.String)
extern void HttpBase_readEntityBody_m8C4461063B6DFD4B38440E1E0272426FCDA3116E (void);
// 0x00000098 System.String[] WebSocketSharpUnityMod.HttpBase::readHeaders(System.IO.Stream,System.Int32)
extern void HttpBase_readHeaders_mABE74433D17B5C4439E998C25B423F46F30DC404 (void);
// 0x00000099 T WebSocketSharpUnityMod.HttpBase::Read(System.IO.Stream,System.Func`2<System.String[],T>,System.Int32)
// 0x0000009A System.Byte[] WebSocketSharpUnityMod.HttpBase::ToByteArray()
extern void HttpBase_ToByteArray_mAC4BA4E41DDDA30E8ADBA25B864ED0A60B5220DD (void);
// 0x0000009B System.Void WebSocketSharpUnityMod.HttpBase/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m52F7066D6D08EDAB5EA2CB4862637CF2ECA0544E (void);
// 0x0000009C System.Void WebSocketSharpUnityMod.HttpBase/<>c__DisplayClass13_0::<readHeaders>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass13_0_U3CreadHeadersU3Eb__0_m25B4AF347E9762B3AC08C5F740772AF9307C41A1 (void);
// 0x0000009D System.Void WebSocketSharpUnityMod.HttpBase/<>c__DisplayClass14_0`1::.ctor()
// 0x0000009E System.Void WebSocketSharpUnityMod.HttpBase/<>c__DisplayClass14_0`1::<Read>b__0(System.Object)
// 0x0000009F System.Void WebSocketSharpUnityMod.HttpRequest::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpRequest__ctor_mD2E0AE0407A69DF24D1FA2DF20E99CC7458B3FF5 (void);
// 0x000000A0 System.Void WebSocketSharpUnityMod.HttpRequest::.ctor(System.String,System.String)
extern void HttpRequest__ctor_mB00BE77082590567BBBD70118C2B92B0C0B28683 (void);
// 0x000000A1 WebSocketSharpUnityMod.Net.AuthenticationResponse WebSocketSharpUnityMod.HttpRequest::get_AuthenticationResponse()
extern void HttpRequest_get_AuthenticationResponse_m30A034D9F4328DA4D20BAEA671E09C8DBA9CF4A6 (void);
// 0x000000A2 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.HttpRequest::get_Cookies()
extern void HttpRequest_get_Cookies_m8ECBD9E856BC13BF3211594FD9E89AA224B4B07E (void);
// 0x000000A3 System.String WebSocketSharpUnityMod.HttpRequest::get_HttpMethod()
extern void HttpRequest_get_HttpMethod_mA0E139150B1F0B9C89C3F2E4E46276C6B3D60DCC (void);
// 0x000000A4 System.Boolean WebSocketSharpUnityMod.HttpRequest::get_IsWebSocketRequest()
extern void HttpRequest_get_IsWebSocketRequest_mD9FBDD78CAAF386815C7F3D406E547213384B5F9 (void);
// 0x000000A5 System.String WebSocketSharpUnityMod.HttpRequest::get_RequestUri()
extern void HttpRequest_get_RequestUri_mFB146DBFE64A1DF6DAB2470E522FB9EF7CF5E6F9 (void);
// 0x000000A6 WebSocketSharpUnityMod.HttpRequest WebSocketSharpUnityMod.HttpRequest::CreateConnectRequest(System.Uri)
extern void HttpRequest_CreateConnectRequest_m1F5DF3ED2A5840C1FF2B08582F66727542E33F5C (void);
// 0x000000A7 WebSocketSharpUnityMod.HttpRequest WebSocketSharpUnityMod.HttpRequest::CreateWebSocketRequest(System.Uri)
extern void HttpRequest_CreateWebSocketRequest_m591149F120844F4FA59CF9776B2C2B00049D30E4 (void);
// 0x000000A8 WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.HttpRequest::GetResponse(System.IO.Stream,System.Int32)
extern void HttpRequest_GetResponse_m7DC56D8E2B5EC05E6F126BE31BADDFC6905EEA0A (void);
// 0x000000A9 WebSocketSharpUnityMod.HttpRequest WebSocketSharpUnityMod.HttpRequest::Parse(System.String[])
extern void HttpRequest_Parse_mEB96F9888D2DC4B149E2A5872C643C51BAE2C85C (void);
// 0x000000AA WebSocketSharpUnityMod.HttpRequest WebSocketSharpUnityMod.HttpRequest::Read(System.IO.Stream,System.Int32)
extern void HttpRequest_Read_m6D73546440BD5524749D641FB19FF7E04A2F2095 (void);
// 0x000000AB System.Void WebSocketSharpUnityMod.HttpRequest::SetCookies(WebSocketSharpUnityMod.Net.CookieCollection)
extern void HttpRequest_SetCookies_mBE54CE60C71FADB7EB110A2267EEE85686EA6531 (void);
// 0x000000AC System.String WebSocketSharpUnityMod.HttpRequest::ToString()
extern void HttpRequest_ToString_mE06EC6EDB8986DCFB841B7EE6FD1C3F0F49426EF (void);
// 0x000000AD System.Void WebSocketSharpUnityMod.HttpResponse::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpResponse__ctor_m90EF3E0CE36099FD0B64B93EDBF173940C216994 (void);
// 0x000000AE System.Void WebSocketSharpUnityMod.HttpResponse::.ctor(WebSocketSharpUnityMod.Net.HttpStatusCode)
extern void HttpResponse__ctor_mF291727C7CAC0C66217123979D0EF58462B4E769 (void);
// 0x000000AF System.Void WebSocketSharpUnityMod.HttpResponse::.ctor(WebSocketSharpUnityMod.Net.HttpStatusCode,System.String)
extern void HttpResponse__ctor_m0D2BD62616E17FAD95B8A80FB561A88AE391BF8D (void);
// 0x000000B0 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.HttpResponse::get_Cookies()
extern void HttpResponse_get_Cookies_mE45EB17AFA95622CEF15C38EA06B9774FA85D8EB (void);
// 0x000000B1 System.Boolean WebSocketSharpUnityMod.HttpResponse::get_HasConnectionClose()
extern void HttpResponse_get_HasConnectionClose_m96AE03E8AD8AE473D632031FA053520C637EBA1C (void);
// 0x000000B2 System.Boolean WebSocketSharpUnityMod.HttpResponse::get_IsProxyAuthenticationRequired()
extern void HttpResponse_get_IsProxyAuthenticationRequired_m64BFC146D098E053061F92FB4C8DF1A8F7A413DB (void);
// 0x000000B3 System.Boolean WebSocketSharpUnityMod.HttpResponse::get_IsRedirect()
extern void HttpResponse_get_IsRedirect_m1D43EAE8DF102196726FA29162B1A3BA2419E2B6 (void);
// 0x000000B4 System.Boolean WebSocketSharpUnityMod.HttpResponse::get_IsUnauthorized()
extern void HttpResponse_get_IsUnauthorized_m04C7F3392EBC5F4B63071A2ABD954C58A8B3A099 (void);
// 0x000000B5 System.Boolean WebSocketSharpUnityMod.HttpResponse::get_IsWebSocketResponse()
extern void HttpResponse_get_IsWebSocketResponse_mE581E887C5F6B5717B799F0FAB2FF0E19F981344 (void);
// 0x000000B6 System.String WebSocketSharpUnityMod.HttpResponse::get_Reason()
extern void HttpResponse_get_Reason_m11479AE5D1B4718DFAA76338089851BE7938DB99 (void);
// 0x000000B7 System.String WebSocketSharpUnityMod.HttpResponse::get_StatusCode()
extern void HttpResponse_get_StatusCode_mAB47C354D4BE22BE676540F5A397F2742C38F7EB (void);
// 0x000000B8 WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.HttpResponse::CreateCloseResponse(WebSocketSharpUnityMod.Net.HttpStatusCode)
extern void HttpResponse_CreateCloseResponse_m5E5D527C1E3F3836B46A596E10BC1D01CA400CC5 (void);
// 0x000000B9 WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.HttpResponse::CreateUnauthorizedResponse(System.String)
extern void HttpResponse_CreateUnauthorizedResponse_mA21EDFC66AC8CA88ADFDD16239E246917CED29DB (void);
// 0x000000BA WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.HttpResponse::CreateWebSocketResponse()
extern void HttpResponse_CreateWebSocketResponse_m4B5A9A5943BBBEE294D00DA16B43320FA16DB2F5 (void);
// 0x000000BB WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.HttpResponse::Parse(System.String[])
extern void HttpResponse_Parse_m2EA92660B00CC2F35640E6574B47D5B778AA4E1F (void);
// 0x000000BC WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.HttpResponse::Read(System.IO.Stream,System.Int32)
extern void HttpResponse_Read_m356B89AE7741B0286A3E2648ECF137402ABE4971 (void);
// 0x000000BD System.Void WebSocketSharpUnityMod.HttpResponse::SetCookies(WebSocketSharpUnityMod.Net.CookieCollection)
extern void HttpResponse_SetCookies_m3126CD1EECC1A310E23B08065575779A07BEF5AF (void);
// 0x000000BE System.String WebSocketSharpUnityMod.HttpResponse::ToString()
extern void HttpResponse_ToString_mD3C2D7B7465E063F84E479F092E39684D481519B (void);
// 0x000000BF System.Void WebSocketSharpUnityMod.LogData::.ctor(WebSocketSharpUnityMod.LogLevel,System.Diagnostics.StackFrame,System.String)
extern void LogData__ctor_mFAB6B90C12B4858753B74C542A6B42A2D9BEE497 (void);
// 0x000000C0 System.Diagnostics.StackFrame WebSocketSharpUnityMod.LogData::get_Caller()
extern void LogData_get_Caller_mCD87FF0D2601C31C0F8DFCCDA09ADC6E27792AD2 (void);
// 0x000000C1 System.DateTime WebSocketSharpUnityMod.LogData::get_Date()
extern void LogData_get_Date_m3CCAF3E485F1F623EF22763D110F3EF864ED06ED (void);
// 0x000000C2 WebSocketSharpUnityMod.LogLevel WebSocketSharpUnityMod.LogData::get_Level()
extern void LogData_get_Level_m7C3274DF34D0B9D866E7DC7D0EB97E52F0858717 (void);
// 0x000000C3 System.String WebSocketSharpUnityMod.LogData::get_Message()
extern void LogData_get_Message_mE30E26D8CB0D0F588D0E227958E43418A20B0E40 (void);
// 0x000000C4 System.String WebSocketSharpUnityMod.LogData::ToString()
extern void LogData_ToString_mC78D2ABBB3F8BD3CE248DDD38B9D0DE931FAEA49 (void);
// 0x000000C5 System.Void WebSocketSharpUnityMod.Logger::.ctor()
extern void Logger__ctor_m6A8685F78240B853EFA5FFBF35BF443DF9E3E340 (void);
// 0x000000C6 System.Void WebSocketSharpUnityMod.Logger::.ctor(WebSocketSharpUnityMod.LogLevel)
extern void Logger__ctor_m05BB5B96E953213D7FD1E621C5104B2A0147D4DB (void);
// 0x000000C7 System.Void WebSocketSharpUnityMod.Logger::.ctor(WebSocketSharpUnityMod.LogLevel,System.String,System.Action`2<WebSocketSharpUnityMod.LogData,System.String>)
extern void Logger__ctor_mE75FD905ECBB63AD3EEE25E0ED91746C8B09A94D (void);
// 0x000000C8 System.String WebSocketSharpUnityMod.Logger::get_File()
extern void Logger_get_File_m65542F8D0156F74AF5BDF5FED76609736316D757 (void);
// 0x000000C9 System.Void WebSocketSharpUnityMod.Logger::set_File(System.String)
extern void Logger_set_File_m518F4BB93B864B2A55408D50936239C0B1932A7B (void);
// 0x000000CA WebSocketSharpUnityMod.LogLevel WebSocketSharpUnityMod.Logger::get_Level()
extern void Logger_get_Level_m3996D95040AA6A27F65F69108A3A2995F45625C1 (void);
// 0x000000CB System.Void WebSocketSharpUnityMod.Logger::set_Level(WebSocketSharpUnityMod.LogLevel)
extern void Logger_set_Level_m244B320CE2C1E32091F73CCBDF46862DD4D8C2F2 (void);
// 0x000000CC System.Action`2<WebSocketSharpUnityMod.LogData,System.String> WebSocketSharpUnityMod.Logger::get_Output()
extern void Logger_get_Output_m413241C28CD8D18794567D366DC3BF01B69579CB (void);
// 0x000000CD System.Void WebSocketSharpUnityMod.Logger::set_Output(System.Action`2<WebSocketSharpUnityMod.LogData,System.String>)
extern void Logger_set_Output_mFBF70E8D11A0FFD1B4B37F8DAB7A515C217A0AFB (void);
// 0x000000CE System.Void WebSocketSharpUnityMod.Logger::defaultOutput(WebSocketSharpUnityMod.LogData,System.String)
extern void Logger_defaultOutput_m9CCF5119E52B1522440117733E6F6B2FD49067E4 (void);
// 0x000000CF System.Void WebSocketSharpUnityMod.Logger::output(System.String,WebSocketSharpUnityMod.LogLevel)
extern void Logger_output_mEF2F966920E1EDB22E17FAE04C5B889964826FC3 (void);
// 0x000000D0 System.Void WebSocketSharpUnityMod.Logger::writeToFile(System.String,System.String)
extern void Logger_writeToFile_m12DA0AC08650CB6CF06CB02B6CA89A26C0EA4B30 (void);
// 0x000000D1 System.Void WebSocketSharpUnityMod.Logger::Debug(System.String)
extern void Logger_Debug_mAA19017C63E5405FE991BC999BDD393A66A162B0 (void);
// 0x000000D2 System.Void WebSocketSharpUnityMod.Logger::Error(System.String)
extern void Logger_Error_m01C374BBF237EB193B388AB977ED2893787DFAF5 (void);
// 0x000000D3 System.Void WebSocketSharpUnityMod.Logger::Fatal(System.String)
extern void Logger_Fatal_m079607EDDB12421CC011BF2EB9AA6941D822D653 (void);
// 0x000000D4 System.Void WebSocketSharpUnityMod.Logger::Info(System.String)
extern void Logger_Info_m375065466F160565B0AC1662527EEAA5CD238A84 (void);
// 0x000000D5 System.Void WebSocketSharpUnityMod.Logger::Trace(System.String)
extern void Logger_Trace_m0F6D60D683F5D2294C946EC94567E680E9F0D4C0 (void);
// 0x000000D6 System.Void WebSocketSharpUnityMod.Logger::Warn(System.String)
extern void Logger_Warn_mF68555389DEEA88701E37540ACAA0710CFE9A5E2 (void);
// 0x000000D7 System.Void WebSocketSharpUnityMod.MessageEventArgs::.ctor(WebSocketSharpUnityMod.WebSocketFrame)
extern void MessageEventArgs__ctor_mE8249469EC7B3F71D92D3D3AAF559A6D25F664CE (void);
// 0x000000D8 System.Void WebSocketSharpUnityMod.MessageEventArgs::.ctor(WebSocketSharpUnityMod.Opcode,System.Byte[])
extern void MessageEventArgs__ctor_m3C33595A944E468C528A9A23D2FEF7B863DFB243 (void);
// 0x000000D9 System.String WebSocketSharpUnityMod.MessageEventArgs::get_Data()
extern void MessageEventArgs_get_Data_m87B02FD9A7760BC0874D0E3AB4CA0757B34ECBDA (void);
// 0x000000DA System.Boolean WebSocketSharpUnityMod.MessageEventArgs::get_IsBinary()
extern void MessageEventArgs_get_IsBinary_m7CD6BDC296C53C4B846735A28C3B5AE7118DC57F (void);
// 0x000000DB System.Boolean WebSocketSharpUnityMod.MessageEventArgs::get_IsPing()
extern void MessageEventArgs_get_IsPing_m01DD45E176A409A1F8C0B4E8796AA5185AB91002 (void);
// 0x000000DC System.Boolean WebSocketSharpUnityMod.MessageEventArgs::get_IsText()
extern void MessageEventArgs_get_IsText_m4C2517C9944A7FD99286DAD253DA762487B8219C (void);
// 0x000000DD System.Byte[] WebSocketSharpUnityMod.MessageEventArgs::get_RawData()
extern void MessageEventArgs_get_RawData_mCB83537E45EC73ACCFB7404336D4AFFFD08B1EBE (void);
// 0x000000DE WebSocketSharpUnityMod.Opcode WebSocketSharpUnityMod.MessageEventArgs::get_Type()
extern void MessageEventArgs_get_Type_m2B46EF5C22988F36EC2591340D356C6A87F4A147 (void);
// 0x000000DF System.Void WebSocketSharpUnityMod.PayloadData::.cctor()
extern void PayloadData__cctor_m745B1D00398C2BE6EF8BA9DCF0461ACF5CD9CE3A (void);
// 0x000000E0 System.Void WebSocketSharpUnityMod.PayloadData::.ctor()
extern void PayloadData__ctor_m0A5867C71E4EC9D8B236D64F4C0BD1FF326BFE13 (void);
// 0x000000E1 System.Void WebSocketSharpUnityMod.PayloadData::.ctor(System.Byte[])
extern void PayloadData__ctor_mE276A01210F764E9FD4533F571CC38569EFEBB39 (void);
// 0x000000E2 System.Void WebSocketSharpUnityMod.PayloadData::.ctor(System.Byte[],System.Int64)
extern void PayloadData__ctor_mC8492ECA5991786CBF706C9A148AC89FD14AE9F7 (void);
// 0x000000E3 System.Int64 WebSocketSharpUnityMod.PayloadData::get_ExtensionDataLength()
extern void PayloadData_get_ExtensionDataLength_m108491D6C8B10A0933E4122738363A9A0B3B9489 (void);
// 0x000000E4 System.Void WebSocketSharpUnityMod.PayloadData::set_ExtensionDataLength(System.Int64)
extern void PayloadData_set_ExtensionDataLength_mB8698897DFD6D0AAD7554682F3B778ADA25F3F2F (void);
// 0x000000E5 System.Boolean WebSocketSharpUnityMod.PayloadData::get_IncludesReservedCloseStatusCode()
extern void PayloadData_get_IncludesReservedCloseStatusCode_mCB2EE5B3CFF047136A50A7883E0053DC63E38F7E (void);
// 0x000000E6 System.Byte[] WebSocketSharpUnityMod.PayloadData::get_ApplicationData()
extern void PayloadData_get_ApplicationData_m0944FA1CA9154D36D9DDD22C389EB2CF335EE6A1 (void);
// 0x000000E7 System.Byte[] WebSocketSharpUnityMod.PayloadData::get_ExtensionData()
extern void PayloadData_get_ExtensionData_m05F6F1B0AD3B014B99AB74D21FDDAFCC861E99A4 (void);
// 0x000000E8 System.UInt64 WebSocketSharpUnityMod.PayloadData::get_Length()
extern void PayloadData_get_Length_m4523B0010FDC114DD0224F6CEF1F8572FCFAC2C3 (void);
// 0x000000E9 System.Void WebSocketSharpUnityMod.PayloadData::Mask(System.Byte[])
extern void PayloadData_Mask_mE53818229057F7FAD25A8FD17A7278DAD21CB168 (void);
// 0x000000EA System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharpUnityMod.PayloadData::GetEnumerator()
extern void PayloadData_GetEnumerator_m47201F6D6E55A37ED705656585911B8A4E69DD1A (void);
// 0x000000EB System.Byte[] WebSocketSharpUnityMod.PayloadData::ToArray()
extern void PayloadData_ToArray_m159A892BF129C643EDACB6B6AC4A5C594A480B14 (void);
// 0x000000EC System.String WebSocketSharpUnityMod.PayloadData::ToString()
extern void PayloadData_ToString_m4ED9CDFF8B40A579AF7F6DAD7EFB03D949AB766F (void);
// 0x000000ED System.Collections.IEnumerator WebSocketSharpUnityMod.PayloadData::System.Collections.IEnumerable.GetEnumerator()
extern void PayloadData_System_Collections_IEnumerable_GetEnumerator_mCC8CC4814763D02EF32FA08B1C495F9E8E95F03E (void);
// 0x000000EE System.Void WebSocketSharpUnityMod.PayloadData/<GetEnumerator>d__21::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__21__ctor_mAE2E6399EE6E9360ED599C0A7727F6892D6F89F2 (void);
// 0x000000EF System.Void WebSocketSharpUnityMod.PayloadData/<GetEnumerator>d__21::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__21_System_IDisposable_Dispose_m3ED8AED0B547BB7C7EA032144F7F4475DBDA4223 (void);
// 0x000000F0 System.Boolean WebSocketSharpUnityMod.PayloadData/<GetEnumerator>d__21::MoveNext()
extern void U3CGetEnumeratorU3Ed__21_MoveNext_mFEF779ECF951DBB50781A08566D6ED0ACE0C4FE1 (void);
// 0x000000F1 System.Byte WebSocketSharpUnityMod.PayloadData/<GetEnumerator>d__21::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mBD381491005684B4A461F4DB2988F50759CA4341 (void);
// 0x000000F2 System.Void WebSocketSharpUnityMod.PayloadData/<GetEnumerator>d__21::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__21_System_Collections_IEnumerator_Reset_m6FAE01735A2D41A989B3580049A701293A1B5EC8 (void);
// 0x000000F3 System.Object WebSocketSharpUnityMod.PayloadData/<GetEnumerator>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__21_System_Collections_IEnumerator_get_Current_m1C06D1B885B472BA6EA791A25308459B39EE0706 (void);
// 0x000000F4 System.Void WebSocketSharpUnityMod.WebSocket::.cctor()
extern void WebSocket__cctor_m1621B89CCD07A6EF5C68CCBFFBFE2F77DAF31166 (void);
// 0x000000F5 System.Void WebSocketSharpUnityMod.WebSocket::.ctor(WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext,System.String)
extern void WebSocket__ctor_m3B00E2F9E1092B98725C30482433DE5DE7343284 (void);
// 0x000000F6 System.Void WebSocketSharpUnityMod.WebSocket::.ctor(WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext,System.String)
extern void WebSocket__ctor_m88F26D04167D968209E3D602A101399D0141F4A6 (void);
// 0x000000F7 System.Void WebSocketSharpUnityMod.WebSocket::.ctor(System.String,System.String[])
extern void WebSocket__ctor_m3113A80584F6A176AFEAAF9A9831A8086AC23BD3 (void);
// 0x000000F8 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.WebSocket::get_CookieCollection()
extern void WebSocket_get_CookieCollection_mD457FD1BECF09C0C9EBC56AE3EAC6C6744C1D089 (void);
// 0x000000F9 System.Func`2<WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext,System.String> WebSocketSharpUnityMod.WebSocket::get_CustomHandshakeRequestChecker()
extern void WebSocket_get_CustomHandshakeRequestChecker_mA982EFE77768DEDB804CF718205CA0443FF1A09E (void);
// 0x000000FA System.Void WebSocketSharpUnityMod.WebSocket::set_CustomHandshakeRequestChecker(System.Func`2<WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext,System.String>)
extern void WebSocket_set_CustomHandshakeRequestChecker_m91F74C4820298EAB914DFBAD9CF11E4E53A469BB (void);
// 0x000000FB System.Boolean WebSocketSharpUnityMod.WebSocket::get_HasMessage()
extern void WebSocket_get_HasMessage_m7985370086F1A03A99F5EA58508CE413239F1240 (void);
// 0x000000FC System.Boolean WebSocketSharpUnityMod.WebSocket::get_IgnoreExtensions()
extern void WebSocket_get_IgnoreExtensions_mBEF78CA4D16DFEB4DB808B45C2BDED334C8432A7 (void);
// 0x000000FD System.Void WebSocketSharpUnityMod.WebSocket::set_IgnoreExtensions(System.Boolean)
extern void WebSocket_set_IgnoreExtensions_m29201A16F32C52024891A3F325835573CECAC5DD (void);
// 0x000000FE System.Boolean WebSocketSharpUnityMod.WebSocket::get_IsConnected()
extern void WebSocket_get_IsConnected_m92FBE7A173EA5C44C4F90FB8C9F832F9CAF3C18C (void);
// 0x000000FF WebSocketSharpUnityMod.CompressionMethod WebSocketSharpUnityMod.WebSocket::get_Compression()
extern void WebSocket_get_Compression_m159ACF48A333642A9CF318FF7A0D3AA6674C201F (void);
// 0x00000100 System.Void WebSocketSharpUnityMod.WebSocket::set_Compression(WebSocketSharpUnityMod.CompressionMethod)
extern void WebSocket_set_Compression_mA5ABD83F4DAE8B28FD211A2A2F7B9DABBF758BAB (void);
// 0x00000101 System.Collections.Generic.IEnumerable`1<WebSocketSharpUnityMod.Net.Cookie> WebSocketSharpUnityMod.WebSocket::get_Cookies()
extern void WebSocket_get_Cookies_m7C2238B44BD33D3FC00E3F0CCCCAF648D40F7A96 (void);
// 0x00000102 WebSocketSharpUnityMod.Net.NetworkCredential WebSocketSharpUnityMod.WebSocket::get_Credentials()
extern void WebSocket_get_Credentials_mF3A32B582B4C4474996D309DE9C3264EA8584BA5 (void);
// 0x00000103 System.Boolean WebSocketSharpUnityMod.WebSocket::get_EmitOnPing()
extern void WebSocket_get_EmitOnPing_mA3816204AAE08B83001B14B49D247D9CB60F2769 (void);
// 0x00000104 System.Void WebSocketSharpUnityMod.WebSocket::set_EmitOnPing(System.Boolean)
extern void WebSocket_set_EmitOnPing_mF79EB62A8016ECEAF30E4C3188D9219D15F08F49 (void);
// 0x00000105 System.Boolean WebSocketSharpUnityMod.WebSocket::get_EnableRedirection()
extern void WebSocket_get_EnableRedirection_mF5D0308770B0F34394F80871F42587D3C64EB97F (void);
// 0x00000106 System.Void WebSocketSharpUnityMod.WebSocket::set_EnableRedirection(System.Boolean)
extern void WebSocket_set_EnableRedirection_m88F23E8C8465BE7E4223CF250F6C62B64BF31D23 (void);
// 0x00000107 System.String WebSocketSharpUnityMod.WebSocket::get_Extensions()
extern void WebSocket_get_Extensions_m0A13AA3AF175C3D29EF6E233FF5133C7BD8234F9 (void);
// 0x00000108 System.Boolean WebSocketSharpUnityMod.WebSocket::get_IsAlive()
extern void WebSocket_get_IsAlive_m98002C68FCC3D5ACB87FB926617C86D41B9DCEBE (void);
// 0x00000109 System.Boolean WebSocketSharpUnityMod.WebSocket::get_IsSecure()
extern void WebSocket_get_IsSecure_m6BD6D607830069377195D8494C64666BEA07AFF2 (void);
// 0x0000010A WebSocketSharpUnityMod.Logger WebSocketSharpUnityMod.WebSocket::get_Log()
extern void WebSocket_get_Log_m9E090D5AB9E455144819B631896397FCB6ABEDAE (void);
// 0x0000010B System.Void WebSocketSharpUnityMod.WebSocket::set_Log(WebSocketSharpUnityMod.Logger)
extern void WebSocket_set_Log_m7DB64A11564925FD8F641C861E6E80CBF8D91940 (void);
// 0x0000010C System.String WebSocketSharpUnityMod.WebSocket::get_Origin()
extern void WebSocket_get_Origin_m38B9FAB346BEEB864F65D51ADCE0DD0ABEE9E9D6 (void);
// 0x0000010D System.Void WebSocketSharpUnityMod.WebSocket::set_Origin(System.String)
extern void WebSocket_set_Origin_m9E94B0806EBA320A1D539EF4FA5547F8BD759285 (void);
// 0x0000010E System.String WebSocketSharpUnityMod.WebSocket::get_Protocol()
extern void WebSocket_get_Protocol_m54A3B3032B49C0450345EFFB0484254ED495E2FD (void);
// 0x0000010F System.Void WebSocketSharpUnityMod.WebSocket::set_Protocol(System.String)
extern void WebSocket_set_Protocol_m715DBC96EC0EF90B67962998BE216AEFA84C7CBF (void);
// 0x00000110 WebSocketSharpUnityMod.WebSocketState WebSocketSharpUnityMod.WebSocket::get_ReadyState()
extern void WebSocket_get_ReadyState_mCE3BB14B47B93D609BD23883FE3D0842BD22ADDF (void);
// 0x00000111 WebSocketSharpUnityMod.Net.ClientSslConfiguration WebSocketSharpUnityMod.WebSocket::get_SslConfiguration()
extern void WebSocket_get_SslConfiguration_m02154604CA8E2344709140ED9A3019A99C594449 (void);
// 0x00000112 System.Void WebSocketSharpUnityMod.WebSocket::set_SslConfiguration(WebSocketSharpUnityMod.Net.ClientSslConfiguration)
extern void WebSocket_set_SslConfiguration_m23C6FBD64760519641A28C8131688124943F1A9E (void);
// 0x00000113 System.Uri WebSocketSharpUnityMod.WebSocket::get_Url()
extern void WebSocket_get_Url_m950920B3F521186470AA63919CA4C8D054673D92 (void);
// 0x00000114 System.TimeSpan WebSocketSharpUnityMod.WebSocket::get_WaitTime()
extern void WebSocket_get_WaitTime_mF31AC1BAE02AF378DC91835A1AE5C190F0C8F5F5 (void);
// 0x00000115 System.Void WebSocketSharpUnityMod.WebSocket::set_WaitTime(System.TimeSpan)
extern void WebSocket_set_WaitTime_m2EF55C6AE3992D02C8DB52E26D964A746D350DC8 (void);
// 0x00000116 System.Void WebSocketSharpUnityMod.WebSocket::add_OnClose(System.EventHandler`1<WebSocketSharpUnityMod.CloseEventArgs>)
extern void WebSocket_add_OnClose_m61B5A75D1BD6449F82DF10B04ADA81671973973B (void);
// 0x00000117 System.Void WebSocketSharpUnityMod.WebSocket::remove_OnClose(System.EventHandler`1<WebSocketSharpUnityMod.CloseEventArgs>)
extern void WebSocket_remove_OnClose_m243CC74F7038A97DF19D67C4A0C546E28671EF33 (void);
// 0x00000118 System.Void WebSocketSharpUnityMod.WebSocket::add_OnError(System.EventHandler`1<WebSocketSharpUnityMod.ErrorEventArgs>)
extern void WebSocket_add_OnError_m3CBF661D2FC3AC3565AAC0910DC49494C965EDF8 (void);
// 0x00000119 System.Void WebSocketSharpUnityMod.WebSocket::remove_OnError(System.EventHandler`1<WebSocketSharpUnityMod.ErrorEventArgs>)
extern void WebSocket_remove_OnError_m983FA9CC52960D91D37E159443C0812A4ACC424D (void);
// 0x0000011A System.Void WebSocketSharpUnityMod.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharpUnityMod.MessageEventArgs>)
extern void WebSocket_add_OnMessage_m2C9ECD30437B12D0936EB5457B853F693ECE2295 (void);
// 0x0000011B System.Void WebSocketSharpUnityMod.WebSocket::remove_OnMessage(System.EventHandler`1<WebSocketSharpUnityMod.MessageEventArgs>)
extern void WebSocket_remove_OnMessage_m8054EDBEC1DCD087ACAEF25551A883FD52110CBF (void);
// 0x0000011C System.Void WebSocketSharpUnityMod.WebSocket::add_OnOpen(System.EventHandler)
extern void WebSocket_add_OnOpen_m3D14BE0E2AA0AE6541FA4EAECB65097952F41E6B (void);
// 0x0000011D System.Void WebSocketSharpUnityMod.WebSocket::remove_OnOpen(System.EventHandler)
extern void WebSocket_remove_OnOpen_mCF9B71DA18274FC1D460FEE811AF78C25858D52F (void);
// 0x0000011E System.Boolean WebSocketSharpUnityMod.WebSocket::accept()
extern void WebSocket_accept_m697448DF0BCA0CF5737B123752181C05FDEBBFB2 (void);
// 0x0000011F System.Boolean WebSocketSharpUnityMod.WebSocket::acceptHandshake()
extern void WebSocket_acceptHandshake_m73C69E3FAE88F1971098D4B42E3177451419B0C8 (void);
// 0x00000120 System.Boolean WebSocketSharpUnityMod.WebSocket::checkHandshakeRequest(WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext,System.String&)
extern void WebSocket_checkHandshakeRequest_mC04005AB305318BF534932C689902B4138144F9A (void);
// 0x00000121 System.Boolean WebSocketSharpUnityMod.WebSocket::checkHandshakeResponse(WebSocketSharpUnityMod.HttpResponse,System.String&)
extern void WebSocket_checkHandshakeResponse_m7A83947080F9B878642772FDE3BFF6902F427318 (void);
// 0x00000122 System.Boolean WebSocketSharpUnityMod.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern void WebSocket_checkIfAvailable_m11A5DF0BF7FF28F748607CEF55A019F16AA2E282 (void);
// 0x00000123 System.Boolean WebSocketSharpUnityMod.WebSocket::checkIfAvailable(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String&)
extern void WebSocket_checkIfAvailable_mB34105EE87DE969ECB0665299A597B046FBD4E61 (void);
// 0x00000124 System.Boolean WebSocketSharpUnityMod.WebSocket::checkReceivedFrame(WebSocketSharpUnityMod.WebSocketFrame,System.String&)
extern void WebSocket_checkReceivedFrame_m5AC31E961B09369349AB507DC98A78B092C7B7C5 (void);
// 0x00000125 System.Void WebSocketSharpUnityMod.WebSocket::close(WebSocketSharpUnityMod.CloseEventArgs,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_close_m75E2333541140BFEAFD9BB7AC92FBD01FA17175D (void);
// 0x00000126 System.Void WebSocketSharpUnityMod.WebSocket::closeAsync(WebSocketSharpUnityMod.CloseEventArgs,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_closeAsync_mB5084C60E9F4051605DEBF3B1B1C4FBD34176713 (void);
// 0x00000127 System.Boolean WebSocketSharpUnityMod.WebSocket::closeHandshake(System.Byte[],System.Boolean,System.Boolean)
extern void WebSocket_closeHandshake_mDB84B6304C26685A32A57E9511F863C989050D3E (void);
// 0x00000128 System.Boolean WebSocketSharpUnityMod.WebSocket::connect()
extern void WebSocket_connect_mD803C50EB116A5E249A62BF6D2BAA3444D31DB77 (void);
// 0x00000129 System.String WebSocketSharpUnityMod.WebSocket::createExtensions()
extern void WebSocket_createExtensions_m17B67429C0C90A30D9DA5CC9B2FEDE71205EEE47 (void);
// 0x0000012A WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.WebSocket::createHandshakeFailureResponse(WebSocketSharpUnityMod.Net.HttpStatusCode)
extern void WebSocket_createHandshakeFailureResponse_mAAD433D8D5A514D3200C2C2B4704D6DC1D731BA7 (void);
// 0x0000012B WebSocketSharpUnityMod.HttpRequest WebSocketSharpUnityMod.WebSocket::createHandshakeRequest()
extern void WebSocket_createHandshakeRequest_mAF8411FF33D72360754A71358B5C399932053691 (void);
// 0x0000012C WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.WebSocket::createHandshakeResponse()
extern void WebSocket_createHandshakeResponse_m5BD4825C8F5182EA4E844ECDACA48FA5F5F60E7E (void);
// 0x0000012D System.Boolean WebSocketSharpUnityMod.WebSocket::customCheckHandshakeRequest(WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext,System.String&)
extern void WebSocket_customCheckHandshakeRequest_mCFD5C1C0D9E62B7A65C6EA4F94989EFA61464ECB (void);
// 0x0000012E WebSocketSharpUnityMod.MessageEventArgs WebSocketSharpUnityMod.WebSocket::dequeueFromMessageEventQueue()
extern void WebSocket_dequeueFromMessageEventQueue_m750A85233B56C2C1F135D725BC6A8F2F904386EB (void);
// 0x0000012F System.Boolean WebSocketSharpUnityMod.WebSocket::doHandshake()
extern void WebSocket_doHandshake_mBE35DE03A899D74070C5D81E4032B0DE62427730 (void);
// 0x00000130 System.Void WebSocketSharpUnityMod.WebSocket::enqueueToMessageEventQueue(WebSocketSharpUnityMod.MessageEventArgs)
extern void WebSocket_enqueueToMessageEventQueue_mBBB11637467DB5D416DAC7DB3A1D9838D94B9D51 (void);
// 0x00000131 System.Void WebSocketSharpUnityMod.WebSocket::error(System.String,System.Exception)
extern void WebSocket_error_m851827605CE244482FDC13B105D9C189FEEA4C79 (void);
// 0x00000132 System.Void WebSocketSharpUnityMod.WebSocket::fatal(System.String,System.Exception)
extern void WebSocket_fatal_m2F2AEF1F47CD08B37A7500DFFF715E8F616C0832 (void);
// 0x00000133 System.Void WebSocketSharpUnityMod.WebSocket::fatal(System.String,WebSocketSharpUnityMod.CloseStatusCode)
extern void WebSocket_fatal_m0E50A2991B9C844EB4CF2C8FD80C73B263E8BEEE (void);
// 0x00000134 System.Void WebSocketSharpUnityMod.WebSocket::init()
extern void WebSocket_init_mB932C1D03011A46C11B344FEC7A8CFB1034639A0 (void);
// 0x00000135 System.Void WebSocketSharpUnityMod.WebSocket::message()
extern void WebSocket_message_m81F70EC7AF969AEF39900D149918DF2548342559 (void);
// 0x00000136 System.Void WebSocketSharpUnityMod.WebSocket::messagec(WebSocketSharpUnityMod.MessageEventArgs)
extern void WebSocket_messagec_m699DB7DF9EC7FC9BE4CB49D10CAAA1697AB8B432 (void);
// 0x00000137 System.Void WebSocketSharpUnityMod.WebSocket::messages(WebSocketSharpUnityMod.MessageEventArgs)
extern void WebSocket_messages_m79F157A00E17E0298871A9560045C400F3C8AAF7 (void);
// 0x00000138 System.Void WebSocketSharpUnityMod.WebSocket::open()
extern void WebSocket_open_m589826A86F87DF7858D9FCD54B28AD5EC89AF5D2 (void);
// 0x00000139 System.Boolean WebSocketSharpUnityMod.WebSocket::processCloseFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processCloseFrame_m8B698607360D2BBB0A71042516DE047152A268E1 (void);
// 0x0000013A System.Void WebSocketSharpUnityMod.WebSocket::processCookies(WebSocketSharpUnityMod.Net.CookieCollection)
extern void WebSocket_processCookies_m34A3DC74D1EF2288C287C7FE20EE64E570977936 (void);
// 0x0000013B System.Boolean WebSocketSharpUnityMod.WebSocket::processDataFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processDataFrame_mF9531E7600B6E23BB4565128CFDC4BA1541B8C26 (void);
// 0x0000013C System.Boolean WebSocketSharpUnityMod.WebSocket::processFragmentFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processFragmentFrame_m902B531D18313004A571477669B5202C7F2CE0E2 (void);
// 0x0000013D System.Boolean WebSocketSharpUnityMod.WebSocket::processPingFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processPingFrame_mADAF9E172323A41AFB871D79E69D431D91E72049 (void);
// 0x0000013E System.Boolean WebSocketSharpUnityMod.WebSocket::processPongFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processPongFrame_m67485968DF8BA862348244E2A29C1ED073286D9E (void);
// 0x0000013F System.Boolean WebSocketSharpUnityMod.WebSocket::processReceivedFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processReceivedFrame_m32DB1CB48BE005AD0B82B6E78120483418465BE5 (void);
// 0x00000140 System.Void WebSocketSharpUnityMod.WebSocket::processSecWebSocketExtensionsClientHeader(System.String)
extern void WebSocket_processSecWebSocketExtensionsClientHeader_mB4BD5A73BAA57A8A2C4827BBCA211A3D757D4821 (void);
// 0x00000141 System.Void WebSocketSharpUnityMod.WebSocket::processSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_processSecWebSocketExtensionsServerHeader_m729A672C97794F5064F96FA46F58C1900FC4920A (void);
// 0x00000142 System.Void WebSocketSharpUnityMod.WebSocket::processSecWebSocketProtocolHeader(System.Collections.Generic.IEnumerable`1<System.String>)
extern void WebSocket_processSecWebSocketProtocolHeader_mB36F8AABACC411CF16364952A2F0EC17D079F5D6 (void);
// 0x00000143 System.Boolean WebSocketSharpUnityMod.WebSocket::processUnsupportedFrame(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocket_processUnsupportedFrame_mCCA7EA3371D286EB406F757488CB9A64E0E0ED9B (void);
// 0x00000144 System.Void WebSocketSharpUnityMod.WebSocket::releaseClientResources()
extern void WebSocket_releaseClientResources_mF7BDEA8CEA82A696C521737A77D5A78396509EA4 (void);
// 0x00000145 System.Void WebSocketSharpUnityMod.WebSocket::releaseCommonResources()
extern void WebSocket_releaseCommonResources_m16DA2B4DFED0D51BE08BE7FDA92C8DAFFFF2B4FB (void);
// 0x00000146 System.Void WebSocketSharpUnityMod.WebSocket::releaseResources()
extern void WebSocket_releaseResources_mC98420FB0C8F370D763CE7D3A2672045966905E1 (void);
// 0x00000147 System.Void WebSocketSharpUnityMod.WebSocket::releaseServerResources()
extern void WebSocket_releaseServerResources_mD154B640F2D9825EE568B1CA4888C085A9721C9C (void);
// 0x00000148 System.Boolean WebSocketSharpUnityMod.WebSocket::send(System.Byte[])
extern void WebSocket_send_mD4A3B874B4413392230C3A59903ACD95FA3C5740 (void);
// 0x00000149 System.Boolean WebSocketSharpUnityMod.WebSocket::send(WebSocketSharpUnityMod.Opcode,System.IO.Stream)
extern void WebSocket_send_m3449CCDE2282E2759204FD1EBC85797837958B87 (void);
// 0x0000014A System.Boolean WebSocketSharpUnityMod.WebSocket::send(WebSocketSharpUnityMod.Opcode,System.IO.Stream,System.Boolean)
extern void WebSocket_send_mAC5E98BE0CF29922C0D992E04600ACE5D85260EC (void);
// 0x0000014B System.Boolean WebSocketSharpUnityMod.WebSocket::send(WebSocketSharpUnityMod.Fin,WebSocketSharpUnityMod.Opcode,System.Byte[],System.Boolean)
extern void WebSocket_send_m3CB93B62ABB36C86952E21CE6EF239FD004808AA (void);
// 0x0000014C System.Void WebSocketSharpUnityMod.WebSocket::sendAsync(WebSocketSharpUnityMod.Opcode,System.IO.Stream,System.Action`1<System.Boolean>)
extern void WebSocket_sendAsync_m2FC169BCC46E9D79D478CF54CC21F7BAF13F2EA2 (void);
// 0x0000014D System.Boolean WebSocketSharpUnityMod.WebSocket::sendBytes(System.Byte[])
extern void WebSocket_sendBytes_m9B25CCC90133A2AFF9363BFB028C134CBCC909DE (void);
// 0x0000014E WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.WebSocket::sendHandshakeRequest()
extern void WebSocket_sendHandshakeRequest_m256C62FFF89D52590E7191711193FB8DA72C63AE (void);
// 0x0000014F WebSocketSharpUnityMod.HttpResponse WebSocketSharpUnityMod.WebSocket::sendHttpRequest(WebSocketSharpUnityMod.HttpRequest,System.Int32)
extern void WebSocket_sendHttpRequest_m15FA5B9AC082FE119B6D534315A00CE17E73A034 (void);
// 0x00000150 System.Boolean WebSocketSharpUnityMod.WebSocket::sendHttpResponse(WebSocketSharpUnityMod.HttpResponse)
extern void WebSocket_sendHttpResponse_m6C0E74225E8D4344C34D101020C1F0DDFC415A77 (void);
// 0x00000151 System.Void WebSocketSharpUnityMod.WebSocket::sendProxyConnectRequest()
extern void WebSocket_sendProxyConnectRequest_m57C03EC1F9FD0AC95BFB7489DFE9B0EB978133A4 (void);
// 0x00000152 System.Void WebSocketSharpUnityMod.WebSocket::setClientStream()
extern void WebSocket_setClientStream_m7314C7A289D5BC4E90F3753BCE998DFADF8EF106 (void);
// 0x00000153 System.Void WebSocketSharpUnityMod.WebSocket::setClientStreamOriginal()
extern void WebSocket_setClientStreamOriginal_m82BA292E86483174DE1FC63E45FF9B5633506BF8 (void);
// 0x00000154 System.Boolean WebSocketSharpUnityMod.WebSocket::TrySetClientStream(System.Int32)
extern void WebSocket_TrySetClientStream_mEC08D07CF96334479832E6264F105965DF12B38E (void);
// 0x00000155 System.Void WebSocketSharpUnityMod.WebSocket::startReceiving()
extern void WebSocket_startReceiving_mB6FA9F774E4A507F51E3441E0765D4A3E03391D2 (void);
// 0x00000156 System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketAcceptHeader(System.String)
extern void WebSocket_validateSecWebSocketAcceptHeader_mCCC7BC0B0883A88A448703D85C4DB08AA80EC6A6 (void);
// 0x00000157 System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketExtensionsClientHeader(System.String)
extern void WebSocket_validateSecWebSocketExtensionsClientHeader_m67980345FF623A6CE531AC5AC515029264429883 (void);
// 0x00000158 System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_validateSecWebSocketExtensionsServerHeader_m5D0557397DC970AD34539A857FBDDC1F557DBAA6 (void);
// 0x00000159 System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketKeyHeader(System.String)
extern void WebSocket_validateSecWebSocketKeyHeader_m7D9A371203127958BA6001DCF603386223FD7E55 (void);
// 0x0000015A System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketProtocolClientHeader(System.String)
extern void WebSocket_validateSecWebSocketProtocolClientHeader_m52652F67B73834A9FFF348EFDAD6E15A4008C200 (void);
// 0x0000015B System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketProtocolServerHeader(System.String)
extern void WebSocket_validateSecWebSocketProtocolServerHeader_m368BCDE95BBD863109D1875912997CC1A23E6356 (void);
// 0x0000015C System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketVersionClientHeader(System.String)
extern void WebSocket_validateSecWebSocketVersionClientHeader_m5787288B62F674073C9DEB10EB046CB5649A1018 (void);
// 0x0000015D System.Boolean WebSocketSharpUnityMod.WebSocket::validateSecWebSocketVersionServerHeader(System.String)
extern void WebSocket_validateSecWebSocketVersionServerHeader_mFF1B314E4F105E4BEAB00CF21A8B64C1FC06825B (void);
// 0x0000015E System.String WebSocketSharpUnityMod.WebSocket::CheckCloseParameters(System.UInt16,System.String,System.Boolean)
extern void WebSocket_CheckCloseParameters_mABC996DB8E6C1F21590CFE6DC0494EADCAE539F2 (void);
// 0x0000015F System.String WebSocketSharpUnityMod.WebSocket::CheckCloseParameters(WebSocketSharpUnityMod.CloseStatusCode,System.String,System.Boolean)
extern void WebSocket_CheckCloseParameters_m68CF9B623D19D8B800BCD17197A146F16801E5C4 (void);
// 0x00000160 System.String WebSocketSharpUnityMod.WebSocket::CheckPingParameter(System.String,System.Byte[]&)
extern void WebSocket_CheckPingParameter_m42DD83060467F52153C03D8E37CD91029AE2C164 (void);
// 0x00000161 System.String WebSocketSharpUnityMod.WebSocket::CheckSendParameter(System.Byte[])
extern void WebSocket_CheckSendParameter_mC12718C0FD3BA0AC67A5874FAF9D6F9654E71F71 (void);
// 0x00000162 System.String WebSocketSharpUnityMod.WebSocket::CheckSendParameter(System.IO.FileInfo)
extern void WebSocket_CheckSendParameter_m2DC38DD68457607A050BD8D73BD21B47FC1D270A (void);
// 0x00000163 System.String WebSocketSharpUnityMod.WebSocket::CheckSendParameter(System.String)
extern void WebSocket_CheckSendParameter_mCA4DDB25557AC78ADA103D8DB6EE1AF9612BBFAB (void);
// 0x00000164 System.String WebSocketSharpUnityMod.WebSocket::CheckSendParameters(System.IO.Stream,System.Int32)
extern void WebSocket_CheckSendParameters_mF7894EF507B47ACB07044F8A8A46F6245246CEFA (void);
// 0x00000165 System.Void WebSocketSharpUnityMod.WebSocket::Close(WebSocketSharpUnityMod.HttpResponse)
extern void WebSocket_Close_mA38C53008CEDA5EAA3B67BEC3D2A23DDC4EDAB36 (void);
// 0x00000166 System.Void WebSocketSharpUnityMod.WebSocket::Close(WebSocketSharpUnityMod.Net.HttpStatusCode)
extern void WebSocket_Close_m215B38800A7BC8151C3522DF9D2B8E030702AE71 (void);
// 0x00000167 System.Void WebSocketSharpUnityMod.WebSocket::Close(WebSocketSharpUnityMod.CloseEventArgs,System.Byte[],System.Boolean)
extern void WebSocket_Close_mF976981830E95656BFE13471D2F61EAE67CD69E8 (void);
// 0x00000168 System.String WebSocketSharpUnityMod.WebSocket::CreateBase64Key()
extern void WebSocket_CreateBase64Key_m6703E8338082186E13606F0F2E982FE5E1BFBB26 (void);
// 0x00000169 System.String WebSocketSharpUnityMod.WebSocket::CreateResponseKey(System.String)
extern void WebSocket_CreateResponseKey_m4C85F7941191B46A2A8124A2C815DEF55C55F3E4 (void);
// 0x0000016A System.Void WebSocketSharpUnityMod.WebSocket::InternalAccept()
extern void WebSocket_InternalAccept_m3BC602691FD85E1D1694BFD2B8402B2BB346E61A (void);
// 0x0000016B System.Boolean WebSocketSharpUnityMod.WebSocket::Ping(System.Byte[],System.TimeSpan)
extern void WebSocket_Ping_mD062DFF5A38BCFEE037AB6F86A63DD8F15E753FE (void);
// 0x0000016C System.Void WebSocketSharpUnityMod.WebSocket::Send(WebSocketSharpUnityMod.Opcode,System.Byte[],System.Collections.Generic.Dictionary`2<WebSocketSharpUnityMod.CompressionMethod,System.Byte[]>)
extern void WebSocket_Send_mA345E498AA572644D98E73EC3A92E2BD1315FD40 (void);
// 0x0000016D System.Void WebSocketSharpUnityMod.WebSocket::Send(WebSocketSharpUnityMod.Opcode,System.IO.Stream,System.Collections.Generic.Dictionary`2<WebSocketSharpUnityMod.CompressionMethod,System.IO.Stream>)
extern void WebSocket_Send_mF6DB0EAC501163009FB45913B36537467B21222C (void);
// 0x0000016E System.Void WebSocketSharpUnityMod.WebSocket::Accept()
extern void WebSocket_Accept_mCEAC67EAD50C9D969D2A8EABBCC684655C8A1871 (void);
// 0x0000016F System.Void WebSocketSharpUnityMod.WebSocket::AcceptAsync()
extern void WebSocket_AcceptAsync_m54996157EDC9846359B1D2EF4E650234B11E044A (void);
// 0x00000170 System.Void WebSocketSharpUnityMod.WebSocket::Close()
extern void WebSocket_Close_m21143A7A9234BCCA0496FF2A765F899734A0D0A5 (void);
// 0x00000171 System.Void WebSocketSharpUnityMod.WebSocket::Close(System.UInt16)
extern void WebSocket_Close_mBA7C615786C6FA34C038E0812457B023F10AB4A4 (void);
// 0x00000172 System.Void WebSocketSharpUnityMod.WebSocket::Close(WebSocketSharpUnityMod.CloseStatusCode)
extern void WebSocket_Close_m87B89C4CFFA7AF0F82C5C4B1040B2A5E37991A18 (void);
// 0x00000173 System.Void WebSocketSharpUnityMod.WebSocket::Close(System.UInt16,System.String)
extern void WebSocket_Close_m3EBF84F346CC022EB2535A3342DC2D079625A2A4 (void);
// 0x00000174 System.Void WebSocketSharpUnityMod.WebSocket::Close(WebSocketSharpUnityMod.CloseStatusCode,System.String)
extern void WebSocket_Close_m9EEB56FC521BFEEF27FC379DD09276A84B6F53C0 (void);
// 0x00000175 System.Void WebSocketSharpUnityMod.WebSocket::CloseAsync()
extern void WebSocket_CloseAsync_m41986F2722D2D612B33C254A18CD3C4A0D90EB79 (void);
// 0x00000176 System.Void WebSocketSharpUnityMod.WebSocket::CloseAsync(System.UInt16)
extern void WebSocket_CloseAsync_mE55F91EF91A581064CEB4B4D88F1D238745465FC (void);
// 0x00000177 System.Void WebSocketSharpUnityMod.WebSocket::CloseAsync(WebSocketSharpUnityMod.CloseStatusCode)
extern void WebSocket_CloseAsync_m9BAFDF6538AE6EB87068A6F4296980A0A4BB6651 (void);
// 0x00000178 System.Void WebSocketSharpUnityMod.WebSocket::CloseAsync(System.UInt16,System.String)
extern void WebSocket_CloseAsync_mA175695B7D497C5B5626666DFA09B4517FEA8E0B (void);
// 0x00000179 System.Void WebSocketSharpUnityMod.WebSocket::CloseAsync(WebSocketSharpUnityMod.CloseStatusCode,System.String)
extern void WebSocket_CloseAsync_m35C92D5F4267DDBC8E2324C9E6E941DF9FD85829 (void);
// 0x0000017A System.Void WebSocketSharpUnityMod.WebSocket::Connect()
extern void WebSocket_Connect_mD0F171D175A667AD6B6B551DB92AA4E550636E8F (void);
// 0x0000017B System.Void WebSocketSharpUnityMod.WebSocket::ConnectAsync()
extern void WebSocket_ConnectAsync_mB58FAD1540B0C2DD96379997F2604B3A2B6655E1 (void);
// 0x0000017C System.Boolean WebSocketSharpUnityMod.WebSocket::Ping()
extern void WebSocket_Ping_m28693025CC15C745A836BA9B78A9258308D1D064 (void);
// 0x0000017D System.Boolean WebSocketSharpUnityMod.WebSocket::Ping(System.String)
extern void WebSocket_Ping_m7B06B066EFAC73CA5C1D67A658F05660E584828E (void);
// 0x0000017E System.Void WebSocketSharpUnityMod.WebSocket::Send(System.Byte[])
extern void WebSocket_Send_m820637BD0508765A60DAF71A702BCAD99B8C8CCA (void);
// 0x0000017F System.Void WebSocketSharpUnityMod.WebSocket::Send(System.IO.FileInfo)
extern void WebSocket_Send_mF82393046DF4A1832EF424B326313803F05C3DB9 (void);
// 0x00000180 System.Void WebSocketSharpUnityMod.WebSocket::Send(System.String)
extern void WebSocket_Send_m22D6D6E76DB2F8AA9D9C8EC350B607176852BC90 (void);
// 0x00000181 System.Void WebSocketSharpUnityMod.WebSocket::SendAsync(System.Byte[],System.Action`1<System.Boolean>)
extern void WebSocket_SendAsync_m11CC853F4E91EA25F53BD3612B1AE0C4C4C05CBB (void);
// 0x00000182 System.Void WebSocketSharpUnityMod.WebSocket::SendAsync(System.IO.FileInfo,System.Action`1<System.Boolean>)
extern void WebSocket_SendAsync_mCF36B00A6C6C13E6EFE6E2BAE7C1CED43A8315E8 (void);
// 0x00000183 System.Void WebSocketSharpUnityMod.WebSocket::SendAsync(System.String,System.Action`1<System.Boolean>)
extern void WebSocket_SendAsync_m7212F00F9DCC53617CC32BDE6AAD2B7E69415DD3 (void);
// 0x00000184 System.Void WebSocketSharpUnityMod.WebSocket::SendAsync(System.IO.Stream,System.Int32,System.Action`1<System.Boolean>)
extern void WebSocket_SendAsync_m8130CC33A6AB1BB6A2C7DC67EF39DF936777ED48 (void);
// 0x00000185 System.Void WebSocketSharpUnityMod.WebSocket::SetCookie(WebSocketSharpUnityMod.Net.Cookie)
extern void WebSocket_SetCookie_m9EA39295E3E5B29AA34BB86DF44FAE35A4D07047 (void);
// 0x00000186 System.Void WebSocketSharpUnityMod.WebSocket::SetCredentials(System.String,System.String,System.Boolean)
extern void WebSocket_SetCredentials_m213FB163E427A632F8530ABCF998197986B14374 (void);
// 0x00000187 System.Void WebSocketSharpUnityMod.WebSocket::SetProxy(System.String,System.String,System.String)
extern void WebSocket_SetProxy_mD825B512A6BC7B3EE4FAFCE76F96B40507627482 (void);
// 0x00000188 System.Void WebSocketSharpUnityMod.WebSocket::System.IDisposable.Dispose()
extern void WebSocket_System_IDisposable_Dispose_m17688B3E5596E7127711913DCAFA2B90E9F21350 (void);
// 0x00000189 System.Void WebSocketSharpUnityMod.WebSocket::<open>b__139_0(System.IAsyncResult)
extern void WebSocket_U3CopenU3Eb__139_0_mEAAB03D1976E0F661C7A971942699BFF1337C889 (void);
// 0x0000018A System.Boolean WebSocketSharpUnityMod.WebSocket::<processSecWebSocketProtocolHeader>b__149_0(System.String)
extern void WebSocket_U3CprocessSecWebSocketProtocolHeaderU3Eb__149_0_mCD1C6EBCE8132B9AF7270BBDD6E159DB1EB757C5 (void);
// 0x0000018B System.Void WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::.ctor(System.Int32)
extern void U3Cget_CookiesU3Ed__67__ctor_mA33DC5CCBB1C430496CB7D2CB3725E00535E9871 (void);
// 0x0000018C System.Void WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::System.IDisposable.Dispose()
extern void U3Cget_CookiesU3Ed__67_System_IDisposable_Dispose_m120F53BE27792169AA77EE32031232EC1DD48368 (void);
// 0x0000018D System.Boolean WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::MoveNext()
extern void U3Cget_CookiesU3Ed__67_MoveNext_m80C5E04FD1A93DD25967817386BD08A1F57E6AC4 (void);
// 0x0000018E System.Void WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::<>m__Finally1()
extern void U3Cget_CookiesU3Ed__67_U3CU3Em__Finally1_m27AA296EE1D243C3453AC4DB6E154B0F90D2D11E (void);
// 0x0000018F System.Void WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::<>m__Finally2()
extern void U3Cget_CookiesU3Ed__67_U3CU3Em__Finally2_mF4FF9F7F82E1749F68898F20ADD077703B0CA117 (void);
// 0x00000190 WebSocketSharpUnityMod.Net.Cookie WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::System.Collections.Generic.IEnumerator<WebSocketSharpUnityMod.Net.Cookie>.get_Current()
extern void U3Cget_CookiesU3Ed__67_System_Collections_Generic_IEnumeratorU3CWebSocketSharpUnityMod_Net_CookieU3E_get_Current_m27F768172C5DBDFE9EC38E7FDFD8F4FB51AD6B31 (void);
// 0x00000191 System.Void WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::System.Collections.IEnumerator.Reset()
extern void U3Cget_CookiesU3Ed__67_System_Collections_IEnumerator_Reset_m2F9764C52EF17FC594538AC778D4806B154C80DC (void);
// 0x00000192 System.Object WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::System.Collections.IEnumerator.get_Current()
extern void U3Cget_CookiesU3Ed__67_System_Collections_IEnumerator_get_Current_m5C6D8CFAC8FDDD6E0E2D18D0295EA418F6A18568 (void);
// 0x00000193 System.Collections.Generic.IEnumerator`1<WebSocketSharpUnityMod.Net.Cookie> WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::System.Collections.Generic.IEnumerable<WebSocketSharpUnityMod.Net.Cookie>.GetEnumerator()
extern void U3Cget_CookiesU3Ed__67_System_Collections_Generic_IEnumerableU3CWebSocketSharpUnityMod_Net_CookieU3E_GetEnumerator_m830E275A5931F3F18C0BFB751A21EB4AC2E14D5B (void);
// 0x00000194 System.Collections.IEnumerator WebSocketSharpUnityMod.WebSocket/<get_Cookies>d__67::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_CookiesU3Ed__67_System_Collections_IEnumerable_GetEnumerator_m21BC0942BE82FF00608E1ED2B709C3CAA3945B9C (void);
// 0x00000195 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass121_0::.ctor()
extern void U3CU3Ec__DisplayClass121_0__ctor_m547E7639FE6F7C1D1EF62EF9FBE20007DBFD8E7F (void);
// 0x00000196 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass121_0::<closeAsync>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass121_0_U3CcloseAsyncU3Eb__0_mB294DAC159F60F65C389D4A594980DE88EFE32F5 (void);
// 0x00000197 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass138_0::.ctor()
extern void U3CU3Ec__DisplayClass138_0__ctor_m830FAA3C5DBA1FEEAB65976B4DAA3EFB102ADFCC (void);
// 0x00000198 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass138_0::<messages>b__0(System.Object)
extern void U3CU3Ec__DisplayClass138_0_U3CmessagesU3Eb__0_mF67875B6693DB2905536C36427F2701E585E3946 (void);
// 0x00000199 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass159_0::.ctor()
extern void U3CU3Ec__DisplayClass159_0__ctor_m6D695657E53F0063C744AD9DB73C50395B96ED72 (void);
// 0x0000019A System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass159_0::<sendAsync>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass159_0_U3CsendAsyncU3Eb__0_m73A5F394CD664634D8CDA55ACEE9F5E3992E6AC2 (void);
// 0x0000019B System.Void WebSocketSharpUnityMod.WebSocket/<>c::.cctor()
extern void U3CU3Ec__cctor_mF12A52F702DD83A0C0150EE002251D906E9D6FA1 (void);
// 0x0000019C System.Void WebSocketSharpUnityMod.WebSocket/<>c::.ctor()
extern void U3CU3Ec__ctor_m4C55FD0206B73878AF2D0C1E1C03C39C37EAF059 (void);
// 0x0000019D System.Void WebSocketSharpUnityMod.WebSocket/<>c::<TrySetClientStream>b__167_0(System.IAsyncResult)
extern void U3CU3Ec_U3CTrySetClientStreamU3Eb__167_0_mFE9365C6ABF98269FB1231B2B183AE9E0EADD092 (void);
// 0x0000019E System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass168_0::.ctor()
extern void U3CU3Ec__DisplayClass168_0__ctor_m400AB6AB21A9C03EA1881537415B189EF43B9034 (void);
// 0x0000019F System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass168_0::<startReceiving>b__0()
extern void U3CU3Ec__DisplayClass168_0_U3CstartReceivingU3Eb__0_m5579419F28117FBFCD5C4A709B7A623300CDC581 (void);
// 0x000001A0 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass168_0::<startReceiving>b__1(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass168_0_U3CstartReceivingU3Eb__1_m593314F2F89370EECEA4C7E216141B0B7C74467D (void);
// 0x000001A1 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass168_0::<startReceiving>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass168_0_U3CstartReceivingU3Eb__2_m9036DFB0BE0143AC8A6FB0E14C1078F6212FCE9F (void);
// 0x000001A2 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass171_0::.ctor()
extern void U3CU3Ec__DisplayClass171_0__ctor_mF04550C79381B6F0B8787DBE507D2008931AA1DF (void);
// 0x000001A3 System.Boolean WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass171_0::<validateSecWebSocketExtensionsServerHeader>b__0(System.String)
extern void U3CU3Ec__DisplayClass171_0_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__0_m22C61D5EA139E431F1D048C7645ACB3BA4AECA9E (void);
// 0x000001A4 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass174_0::.ctor()
extern void U3CU3Ec__DisplayClass174_0__ctor_mC932F430FCE6E7117A1A3A76F036A1E582CF86BA (void);
// 0x000001A5 System.Boolean WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass174_0::<validateSecWebSocketProtocolServerHeader>b__0(System.String)
extern void U3CU3Ec__DisplayClass174_0_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__0_mF035C86F330210AE2D3EE130676C8AE85235F50B (void);
// 0x000001A6 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass194_0::.ctor()
extern void U3CU3Ec__DisplayClass194_0__ctor_m0C7EF572D78FB065892FEF04DE01B12825021A77 (void);
// 0x000001A7 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass194_0::<AcceptAsync>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass194_0_U3CAcceptAsyncU3Eb__0_m225D4A59A6A9FAD522DE5BAD241016F6C4EA7FEF (void);
// 0x000001A8 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass206_0::.ctor()
extern void U3CU3Ec__DisplayClass206_0__ctor_m2C25264FFE6F15C049E0A50180EF7755E9DE614C (void);
// 0x000001A9 System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass206_0::<ConnectAsync>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass206_0_U3CConnectAsyncU3Eb__0_mB2D12F810A2D42CA085E1CA6F6A95A62C4455823 (void);
// 0x000001AA System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass215_0::.ctor()
extern void U3CU3Ec__DisplayClass215_0__ctor_m8670031BC15C89EF26B0A99963B12B6B6EC9B834 (void);
// 0x000001AB System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass215_0::<SendAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass215_0_U3CSendAsyncU3Eb__0_mEF09AAE407D92AC5D98C739C639DDEEED597F1CC (void);
// 0x000001AC System.Void WebSocketSharpUnityMod.WebSocket/<>c__DisplayClass215_0::<SendAsync>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass215_0_U3CSendAsyncU3Eb__1_m1BC498D98724B2E038C19493B2B2CCAD78281F4C (void);
// 0x000001AD System.Void WebSocketSharpUnityMod.WebSocketException::.ctor()
extern void WebSocketException__ctor_m6CF8B8F9FC1E642EBD5D7BBAA132A324DC398271 (void);
// 0x000001AE System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(System.Exception)
extern void WebSocketException__ctor_mEE5C74C868EAA9061B1654E99610F10488FB8505 (void);
// 0x000001AF System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(System.String)
extern void WebSocketException__ctor_mEB0B76AC7E8439BC184B642EBDC0BCC57FC4B51B (void);
// 0x000001B0 System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(WebSocketSharpUnityMod.CloseStatusCode)
extern void WebSocketException__ctor_mEC397E314EEA9683DFEE43493E45AA5AF72EA8F0 (void);
// 0x000001B1 System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(System.String,System.Exception)
extern void WebSocketException__ctor_mD01D65C75DE8D7BEA64C99B9AF66B33FBD85D7FF (void);
// 0x000001B2 System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(WebSocketSharpUnityMod.CloseStatusCode,System.Exception)
extern void WebSocketException__ctor_m1E3E2C4323667CAE32EAFAF5A0A09E505D2AB7E1 (void);
// 0x000001B3 System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(WebSocketSharpUnityMod.CloseStatusCode,System.String)
extern void WebSocketException__ctor_m2DAE24EF1B6BF1AAEB9CBD336D85C1A12971426A (void);
// 0x000001B4 System.Void WebSocketSharpUnityMod.WebSocketException::.ctor(WebSocketSharpUnityMod.CloseStatusCode,System.String,System.Exception)
extern void WebSocketException__ctor_mB5B94664D00CF8AFD9B62D29654C2DE22A08F1EC (void);
// 0x000001B5 WebSocketSharpUnityMod.CloseStatusCode WebSocketSharpUnityMod.WebSocketException::get_Code()
extern void WebSocketException_get_Code_mC6D5F6744FFC138CCCEFF32B1E397873B97C4E60 (void);
// 0x000001B6 System.Void WebSocketSharpUnityMod.WebSocketFrame::.cctor()
extern void WebSocketFrame__cctor_m306E895FE0050355F640C8BFAA418D9BFAA639C3 (void);
// 0x000001B7 System.Void WebSocketSharpUnityMod.WebSocketFrame::.ctor()
extern void WebSocketFrame__ctor_m3F03D51743E865CA024A154DBB076C495DD8A8A0 (void);
// 0x000001B8 System.Void WebSocketSharpUnityMod.WebSocketFrame::.ctor(WebSocketSharpUnityMod.Opcode,WebSocketSharpUnityMod.PayloadData,System.Boolean)
extern void WebSocketFrame__ctor_mE8D0C4FF6160480D33DB99244CEF72726BFB0CCA (void);
// 0x000001B9 System.Void WebSocketSharpUnityMod.WebSocketFrame::.ctor(WebSocketSharpUnityMod.Fin,WebSocketSharpUnityMod.Opcode,System.Byte[],System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_m887512DA86D1B4D0E36EE0B1A8EFC01F997D76B5 (void);
// 0x000001BA System.Void WebSocketSharpUnityMod.WebSocketFrame::.ctor(WebSocketSharpUnityMod.Fin,WebSocketSharpUnityMod.Opcode,WebSocketSharpUnityMod.PayloadData,System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_mFAB822ED235720AA0AA8BD844236D95DDE2E6FAE (void);
// 0x000001BB System.Int32 WebSocketSharpUnityMod.WebSocketFrame::get_ExtendedPayloadLengthCount()
extern void WebSocketFrame_get_ExtendedPayloadLengthCount_m2B0C5F06A5206B19F268BB73A8C04529BF47F99E (void);
// 0x000001BC System.UInt64 WebSocketSharpUnityMod.WebSocketFrame::get_FullPayloadLength()
extern void WebSocketFrame_get_FullPayloadLength_mD2BA70BE17059347350BA73C9CF033361F81D035 (void);
// 0x000001BD System.Byte[] WebSocketSharpUnityMod.WebSocketFrame::get_ExtendedPayloadLength()
extern void WebSocketFrame_get_ExtendedPayloadLength_mC5536F16FC0BF0DE8AE6E1D468DF32866A5109A7 (void);
// 0x000001BE WebSocketSharpUnityMod.Fin WebSocketSharpUnityMod.WebSocketFrame::get_Fin()
extern void WebSocketFrame_get_Fin_m6194D4FCECA2C34922495CAAD7CE6DABD720C6A7 (void);
// 0x000001BF System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsBinary()
extern void WebSocketFrame_get_IsBinary_m2133AA6E22584B6C6F62C3C385279E53E3328DB6 (void);
// 0x000001C0 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsClose()
extern void WebSocketFrame_get_IsClose_m9B114E30FDEDD6CC5F2CBADB8DAD90F0C3427643 (void);
// 0x000001C1 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsCompressed()
extern void WebSocketFrame_get_IsCompressed_m974859F6DC198592695B82869419424E0445114C (void);
// 0x000001C2 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsContinuation()
extern void WebSocketFrame_get_IsContinuation_mACA6E0BFD323E8393E3C07878E5F5EDA68EC2693 (void);
// 0x000001C3 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsControl()
extern void WebSocketFrame_get_IsControl_m4575F8C0DBC05A511D3778D32C67FFF001F49BDA (void);
// 0x000001C4 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsData()
extern void WebSocketFrame_get_IsData_m945ED3349595C9F5C5A76C369DD8B3AF2F5F617A (void);
// 0x000001C5 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsFinal()
extern void WebSocketFrame_get_IsFinal_m4161D41E732D85056FA7566C76E35E2DC0F3DBF0 (void);
// 0x000001C6 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsFragment()
extern void WebSocketFrame_get_IsFragment_m2CB41DEB930EFED16B2CA01C0361E0DD07C6EF71 (void);
// 0x000001C7 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsMasked()
extern void WebSocketFrame_get_IsMasked_m7C3DB62A3A9D8771B09B4A8F7F7328FBDE9B3824 (void);
// 0x000001C8 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsPing()
extern void WebSocketFrame_get_IsPing_m41842E30E7536A0229DBB12880FB523461B8F22F (void);
// 0x000001C9 System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsPong()
extern void WebSocketFrame_get_IsPong_mDA60DAC471ADD50D4174D6AEF0725CFEA6083A35 (void);
// 0x000001CA System.Boolean WebSocketSharpUnityMod.WebSocketFrame::get_IsText()
extern void WebSocketFrame_get_IsText_mD91BB86C74A951BE1B0E4610B84B4D23514560F6 (void);
// 0x000001CB System.UInt64 WebSocketSharpUnityMod.WebSocketFrame::get_Length()
extern void WebSocketFrame_get_Length_mEA6DA6BDEFB7591A094DF94C888E69B596F80D3D (void);
// 0x000001CC WebSocketSharpUnityMod.Mask WebSocketSharpUnityMod.WebSocketFrame::get_Mask()
extern void WebSocketFrame_get_Mask_mD9A66CE1F3F7D1BC1169913F73A4406500F7C61B (void);
// 0x000001CD System.Byte[] WebSocketSharpUnityMod.WebSocketFrame::get_MaskingKey()
extern void WebSocketFrame_get_MaskingKey_mDD927C3B606FDEBD8A63B778A52CADB60D822268 (void);
// 0x000001CE WebSocketSharpUnityMod.Opcode WebSocketSharpUnityMod.WebSocketFrame::get_Opcode()
extern void WebSocketFrame_get_Opcode_m8269669F172338E8157B332AE82757D336ACF1CD (void);
// 0x000001CF WebSocketSharpUnityMod.PayloadData WebSocketSharpUnityMod.WebSocketFrame::get_PayloadData()
extern void WebSocketFrame_get_PayloadData_mA0795CD18EE80F7600F7A89C22F395D0A2D098F1 (void);
// 0x000001D0 System.Byte WebSocketSharpUnityMod.WebSocketFrame::get_PayloadLength()
extern void WebSocketFrame_get_PayloadLength_m5D417915B7CE7086273302366DA9D86C1C7F6103 (void);
// 0x000001D1 WebSocketSharpUnityMod.Rsv WebSocketSharpUnityMod.WebSocketFrame::get_Rsv1()
extern void WebSocketFrame_get_Rsv1_mCAADEEC7F978EE57B1B6EB0CAF9535D6352F059D (void);
// 0x000001D2 WebSocketSharpUnityMod.Rsv WebSocketSharpUnityMod.WebSocketFrame::get_Rsv2()
extern void WebSocketFrame_get_Rsv2_mFB76F4F96807CC0C4BD01EB4E6EDD5688CC1425B (void);
// 0x000001D3 WebSocketSharpUnityMod.Rsv WebSocketSharpUnityMod.WebSocketFrame::get_Rsv3()
extern void WebSocketFrame_get_Rsv3_mC45F590421DACC0716F026A739E96691F690CB75 (void);
// 0x000001D4 System.Byte[] WebSocketSharpUnityMod.WebSocketFrame::createMaskingKey()
extern void WebSocketFrame_createMaskingKey_m8DDE3770B5097F09AFA78EE05235672DCF52C1AB (void);
// 0x000001D5 System.String WebSocketSharpUnityMod.WebSocketFrame::dump(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocketFrame_dump_mF57C01EFF82A53E0EC25E134124A2505FDB848FF (void);
// 0x000001D6 System.String WebSocketSharpUnityMod.WebSocketFrame::print(WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocketFrame_print_m0847EF6DF29B0CA0BC84CAAA0811B9435D60C5FD (void);
// 0x000001D7 WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::processHeader(System.Byte[])
extern void WebSocketFrame_processHeader_m06D22200932D70BDB3067F6EE0614551B4B227AB (void);
// 0x000001D8 WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::readExtendedPayloadLength(System.IO.Stream,WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocketFrame_readExtendedPayloadLength_mC91AA425B2B2B469DAF67D00473F68A54CE6FE94 (void);
// 0x000001D9 System.Void WebSocketSharpUnityMod.WebSocketFrame::readExtendedPayloadLengthAsync(System.IO.Stream,WebSocketSharpUnityMod.WebSocketFrame,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readExtendedPayloadLengthAsync_m5693FDEE040A36CB6DC5F403846B417DF55D2A65 (void);
// 0x000001DA WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::readHeader(System.IO.Stream)
extern void WebSocketFrame_readHeader_m6294E5E3A4F2D07E74F01F0DF73A61D798CF077E (void);
// 0x000001DB System.Void WebSocketSharpUnityMod.WebSocketFrame::readHeaderAsync(System.IO.Stream,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readHeaderAsync_m99D44CA06C8709E7BDBC915A5CDA18F6BEB16FC0 (void);
// 0x000001DC WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::readMaskingKey(System.IO.Stream,WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocketFrame_readMaskingKey_m0DD971CB47C6DDF8C477FE4ED344D04E6231C399 (void);
// 0x000001DD System.Void WebSocketSharpUnityMod.WebSocketFrame::readMaskingKeyAsync(System.IO.Stream,WebSocketSharpUnityMod.WebSocketFrame,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readMaskingKeyAsync_m8D78333B7D08CEA12A923C2C90957302AF451602 (void);
// 0x000001DE WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::readPayloadData(System.IO.Stream,WebSocketSharpUnityMod.WebSocketFrame)
extern void WebSocketFrame_readPayloadData_mE6BA47846B2FB639890653E9EAA49C18F91E3948 (void);
// 0x000001DF System.Void WebSocketSharpUnityMod.WebSocketFrame::readPayloadDataAsync(System.IO.Stream,WebSocketSharpUnityMod.WebSocketFrame,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readPayloadDataAsync_m59F0E48D63D636A686AE43F1F8A1C78DFEFBC162 (void);
// 0x000001E0 WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::CreateCloseFrame(WebSocketSharpUnityMod.PayloadData,System.Boolean)
extern void WebSocketFrame_CreateCloseFrame_mD675C61B1F9ACB7064C9013A73BCF6114CDBC7B7 (void);
// 0x000001E1 WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::CreatePingFrame(System.Boolean)
extern void WebSocketFrame_CreatePingFrame_m0F86473DCBDDC18016D98403C26A8C70402C3FEA (void);
// 0x000001E2 WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::CreatePingFrame(System.Byte[],System.Boolean)
extern void WebSocketFrame_CreatePingFrame_mAC9AC1658B687E512076F19DA0366D7C8A09F41A (void);
// 0x000001E3 WebSocketSharpUnityMod.WebSocketFrame WebSocketSharpUnityMod.WebSocketFrame::ReadFrame(System.IO.Stream,System.Boolean)
extern void WebSocketFrame_ReadFrame_m747A50D544B6A8B5B17A69D8427C8E982C7F0C78 (void);
// 0x000001E4 System.Void WebSocketSharpUnityMod.WebSocketFrame::ReadFrameAsync(System.IO.Stream,System.Boolean,System.Action`1<WebSocketSharpUnityMod.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_ReadFrameAsync_m95E4BFE723179248816DC4075F6C7CE4FC00455C (void);
// 0x000001E5 System.Void WebSocketSharpUnityMod.WebSocketFrame::Unmask()
extern void WebSocketFrame_Unmask_mE942269D91488F3AA15337C21BA67AC71446D05A (void);
// 0x000001E6 System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharpUnityMod.WebSocketFrame::GetEnumerator()
extern void WebSocketFrame_GetEnumerator_m8E680EF0D7044772DF92A1787524AB229C211171 (void);
// 0x000001E7 System.Void WebSocketSharpUnityMod.WebSocketFrame::Print(System.Boolean)
extern void WebSocketFrame_Print_m800D8C5C8290A0D69F5E7B728F5A5668AA422A98 (void);
// 0x000001E8 System.String WebSocketSharpUnityMod.WebSocketFrame::PrintToString(System.Boolean)
extern void WebSocketFrame_PrintToString_m6907610E78C2EBC00CEC34A5EAC884921ECFFC5C (void);
// 0x000001E9 System.Byte[] WebSocketSharpUnityMod.WebSocketFrame::ToArray()
extern void WebSocketFrame_ToArray_m2E2031DCD593D785EB83F28C23B64FB6AAD1D61A (void);
// 0x000001EA System.String WebSocketSharpUnityMod.WebSocketFrame::ToString()
extern void WebSocketFrame_ToString_m6488C2433A975E2DE05F24B5ACE715D75F47965D (void);
// 0x000001EB System.Collections.IEnumerator WebSocketSharpUnityMod.WebSocketFrame::System.Collections.IEnumerable.GetEnumerator()
extern void WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_mBF47583EC2A918BB95EA58C5245FB89D2877167D (void);
// 0x000001EC System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass67_0::.ctor()
extern void U3CU3Ec__DisplayClass67_0__ctor_mF8C02948947A91EF20640D4C03895B15493A75E9 (void);
// 0x000001ED System.Action`4<System.String,System.String,System.String,System.String> WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass67_0::<dump>b__0()
extern void U3CU3Ec__DisplayClass67_0_U3CdumpU3Eb__0_m0E2FF95B4F256730D183AC924AC612332428EEAF (void);
// 0x000001EE System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass67_1::.ctor()
extern void U3CU3Ec__DisplayClass67_1__ctor_m38F304A5E077CAC4607E80BB0F8FA596EF0482D1 (void);
// 0x000001EF System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass67_1::<dump>b__1(System.String,System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass67_1_U3CdumpU3Eb__1_m5A6CCBFD6FCBFAA683EEAA2CF3D4283BB8027C21 (void);
// 0x000001F0 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass71_0::.ctor()
extern void U3CU3Ec__DisplayClass71_0__ctor_m7A8DA592C71590119A2993D9D0129801D1444F7A (void);
// 0x000001F1 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass71_0::<readExtendedPayloadLengthAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass71_0_U3CreadExtendedPayloadLengthAsyncU3Eb__0_m41E5D0DB494B6E4937836DA4D9CA8DA9E5237C52 (void);
// 0x000001F2 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass73_0::.ctor()
extern void U3CU3Ec__DisplayClass73_0__ctor_m4D28650F70961B2D119DE949A1E8081838B445CA (void);
// 0x000001F3 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass73_0::<readHeaderAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass73_0_U3CreadHeaderAsyncU3Eb__0_m02B9CFE8F4A2BA29D75BC45200923DC0C72DEFF8 (void);
// 0x000001F4 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass75_0::.ctor()
extern void U3CU3Ec__DisplayClass75_0__ctor_m748B1B593B58E4358C90E2CB753F3624028DBEE2 (void);
// 0x000001F5 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass75_0::<readMaskingKeyAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass75_0_U3CreadMaskingKeyAsyncU3Eb__0_mEB3B5BD88B3B84929559B7E877C57C5BBCFD2CD0 (void);
// 0x000001F6 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass77_0::.ctor()
extern void U3CU3Ec__DisplayClass77_0__ctor_m1F01DC275A7EDCB7CFE3C60D01BE54B454EE6B01 (void);
// 0x000001F7 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass77_0::<readPayloadDataAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass77_0_U3CreadPayloadDataAsyncU3Eb__0_mE68262BEC24352220A3D34FD0615908269CEF94C (void);
// 0x000001F8 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass82_0::.ctor()
extern void U3CU3Ec__DisplayClass82_0__ctor_mE6ABAC7FA8339553CB8E3EE96305DB53DBC09BB2 (void);
// 0x000001F9 System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass82_0::<ReadFrameAsync>b__0(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass82_0_U3CReadFrameAsyncU3Eb__0_m8BC28AFE7C56836F91E0D3C08D1F21DE17F3DE18 (void);
// 0x000001FA System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass82_0::<ReadFrameAsync>b__1(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass82_0_U3CReadFrameAsyncU3Eb__1_m12C5828FD3A8819F82888F321A02EA937F1D7DCE (void);
// 0x000001FB System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass82_0::<ReadFrameAsync>b__2(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass82_0_U3CReadFrameAsyncU3Eb__2_m128FAE7299E07EE2DC330A5F8FE97D0423FA9D70 (void);
// 0x000001FC System.Void WebSocketSharpUnityMod.WebSocketFrame/<>c__DisplayClass82_0::<ReadFrameAsync>b__3(WebSocketSharpUnityMod.WebSocketFrame)
extern void U3CU3Ec__DisplayClass82_0_U3CReadFrameAsyncU3Eb__3_mB5D3BE45056FF03E9FC72E9BBF9D27F9F0C773C8 (void);
// 0x000001FD System.Void WebSocketSharpUnityMod.WebSocketFrame/<GetEnumerator>d__84::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__84__ctor_m61807D2EA695A4A5005433E74F396A53CD92E073 (void);
// 0x000001FE System.Void WebSocketSharpUnityMod.WebSocketFrame/<GetEnumerator>d__84::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__84_System_IDisposable_Dispose_m70E795E27D6B4ACD82D221ADBD0E213104A24343 (void);
// 0x000001FF System.Boolean WebSocketSharpUnityMod.WebSocketFrame/<GetEnumerator>d__84::MoveNext()
extern void U3CGetEnumeratorU3Ed__84_MoveNext_m605B746C044D216AB3D5F9DA4D05B3FFEC760B16 (void);
// 0x00000200 System.Byte WebSocketSharpUnityMod.WebSocketFrame/<GetEnumerator>d__84::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m3C777653C42EBA2B5339751391137F14C1DB252C (void);
// 0x00000201 System.Void WebSocketSharpUnityMod.WebSocketFrame/<GetEnumerator>d__84::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__84_System_Collections_IEnumerator_Reset_m6E22B0CD5FD3BF05685631AE24061B97A0201343 (void);
// 0x00000202 System.Object WebSocketSharpUnityMod.WebSocketFrame/<GetEnumerator>d__84::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__84_System_Collections_IEnumerator_get_Current_m1B418000A17166EBC22478C28B78DA70D30BD10C (void);
// 0x00000203 System.Void WebSocketSharpUnityMod.Server.HttpRequestEventArgs::.ctor(WebSocketSharpUnityMod.Net.HttpListenerContext)
extern void HttpRequestEventArgs__ctor_mE0B7F5E9C736F36B413E09A9DA01650A1F16432E (void);
// 0x00000204 WebSocketSharpUnityMod.Net.HttpListenerRequest WebSocketSharpUnityMod.Server.HttpRequestEventArgs::get_Request()
extern void HttpRequestEventArgs_get_Request_m7188D6F345DEF2FA8D64025BF8B20AB6AF1CBEDC (void);
// 0x00000205 WebSocketSharpUnityMod.Net.HttpListenerResponse WebSocketSharpUnityMod.Server.HttpRequestEventArgs::get_Response()
extern void HttpRequestEventArgs_get_Response_m811886A92305AD435F2200973BF49338945CDA0E (void);
// 0x00000206 System.Void WebSocketSharpUnityMod.Server.HttpServer::.ctor()
extern void HttpServer__ctor_m3969441ABEB298622CB66F0FBC0FD67F5FF257BB (void);
// 0x00000207 System.Void WebSocketSharpUnityMod.Server.HttpServer::.ctor(System.Int32)
extern void HttpServer__ctor_m084A6C5FFD6ECF610B00BFF852DB855ADEA3DC22 (void);
// 0x00000208 System.Void WebSocketSharpUnityMod.Server.HttpServer::.ctor(System.String)
extern void HttpServer__ctor_m23B4F82081A43677745D3C3B4351C7D24112EF14 (void);
// 0x00000209 System.Void WebSocketSharpUnityMod.Server.HttpServer::.ctor(System.Int32,System.Boolean)
extern void HttpServer__ctor_mA23FCEDEA55160C386E074AFD918AAF58D1147B7 (void);
// 0x0000020A System.Void WebSocketSharpUnityMod.Server.HttpServer::.ctor(System.Net.IPAddress,System.Int32)
extern void HttpServer__ctor_m8B3783538D19B929F66167E1FB73F00BAA647F82 (void);
// 0x0000020B System.Void WebSocketSharpUnityMod.Server.HttpServer::.ctor(System.Net.IPAddress,System.Int32,System.Boolean)
extern void HttpServer__ctor_m9BD0356C662F3C664054DD3849922E119AEDF7DF (void);
// 0x0000020C System.Net.IPAddress WebSocketSharpUnityMod.Server.HttpServer::get_Address()
extern void HttpServer_get_Address_m2576FB0B49DEABABF7DFE8904CB5961EA69AACA4 (void);
// 0x0000020D WebSocketSharpUnityMod.Net.AuthenticationSchemes WebSocketSharpUnityMod.Server.HttpServer::get_AuthenticationSchemes()
extern void HttpServer_get_AuthenticationSchemes_m23BA33F6476A95A6C17BA4532DE83E86A90DAD56 (void);
// 0x0000020E System.Void WebSocketSharpUnityMod.Server.HttpServer::set_AuthenticationSchemes(WebSocketSharpUnityMod.Net.AuthenticationSchemes)
extern void HttpServer_set_AuthenticationSchemes_mBD4933EED3269102C93B056085CDA3BB6B5F0F08 (void);
// 0x0000020F System.Boolean WebSocketSharpUnityMod.Server.HttpServer::get_IsListening()
extern void HttpServer_get_IsListening_m8E2400CFCB208D3C557EB37CA47A31E8E366A461 (void);
// 0x00000210 System.Boolean WebSocketSharpUnityMod.Server.HttpServer::get_IsSecure()
extern void HttpServer_get_IsSecure_mE5834DB1C6ADDE074FFE05F680C1349F716C9CC5 (void);
// 0x00000211 System.Boolean WebSocketSharpUnityMod.Server.HttpServer::get_KeepClean()
extern void HttpServer_get_KeepClean_m76E8276230E931FC525D8374164F53F1A88B8EB7 (void);
// 0x00000212 System.Void WebSocketSharpUnityMod.Server.HttpServer::set_KeepClean(System.Boolean)
extern void HttpServer_set_KeepClean_m9362782483BB16088728D89358D8308338E1A7AC (void);
// 0x00000213 WebSocketSharpUnityMod.Logger WebSocketSharpUnityMod.Server.HttpServer::get_Log()
extern void HttpServer_get_Log_m7781C03E8C460B9241E49A70C553566EC2A87481 (void);
// 0x00000214 System.Int32 WebSocketSharpUnityMod.Server.HttpServer::get_Port()
extern void HttpServer_get_Port_mB6D435AF5494EA5A290420543A94DE1C704B55AB (void);
// 0x00000215 System.String WebSocketSharpUnityMod.Server.HttpServer::get_Realm()
extern void HttpServer_get_Realm_m6DD8D5A61EC61B5AFBB0502B99AF92675668C1AF (void);
// 0x00000216 System.Void WebSocketSharpUnityMod.Server.HttpServer::set_Realm(System.String)
extern void HttpServer_set_Realm_m827DE665E3D95979EDB43F4D8C14C865D644AA06 (void);
// 0x00000217 System.Boolean WebSocketSharpUnityMod.Server.HttpServer::get_ReuseAddress()
extern void HttpServer_get_ReuseAddress_mF4895B41C4533F8AE8822E584620476D699F143C (void);
// 0x00000218 System.Void WebSocketSharpUnityMod.Server.HttpServer::set_ReuseAddress(System.Boolean)
extern void HttpServer_set_ReuseAddress_m750B0097D7968055ACC3B9E2AB4ABE26B40122D0 (void);
// 0x00000219 System.String WebSocketSharpUnityMod.Server.HttpServer::get_RootPath()
extern void HttpServer_get_RootPath_m98229F6AA86F7D2F6C51513E087C6F5BE8D117C7 (void);
// 0x0000021A System.Void WebSocketSharpUnityMod.Server.HttpServer::set_RootPath(System.String)
extern void HttpServer_set_RootPath_m4022A901A9E0EA86E6637F1B4B8AF4D63929509F (void);
// 0x0000021B WebSocketSharpUnityMod.Net.ServerSslConfiguration WebSocketSharpUnityMod.Server.HttpServer::get_SslConfiguration()
extern void HttpServer_get_SslConfiguration_mB0B0B4279C596134523767B55174CDEB1A73484E (void);
// 0x0000021C System.Void WebSocketSharpUnityMod.Server.HttpServer::set_SslConfiguration(WebSocketSharpUnityMod.Net.ServerSslConfiguration)
extern void HttpServer_set_SslConfiguration_m60278C094D4F6B81CE32BDF1F6CB4E9E59EB88CC (void);
// 0x0000021D System.Func`2<System.Security.Principal.IIdentity,WebSocketSharpUnityMod.Net.NetworkCredential> WebSocketSharpUnityMod.Server.HttpServer::get_UserCredentialsFinder()
extern void HttpServer_get_UserCredentialsFinder_mB68D3398F1F94B1E1BA582E36526CA7F375DA20B (void);
// 0x0000021E System.Void WebSocketSharpUnityMod.Server.HttpServer::set_UserCredentialsFinder(System.Func`2<System.Security.Principal.IIdentity,WebSocketSharpUnityMod.Net.NetworkCredential>)
extern void HttpServer_set_UserCredentialsFinder_m1FBE3917A2FBDDF448954F730744C503287CCCF1 (void);
// 0x0000021F System.TimeSpan WebSocketSharpUnityMod.Server.HttpServer::get_WaitTime()
extern void HttpServer_get_WaitTime_mD35FCA8F0624B49D68C51F2BF1AB8EF1B33A0DB5 (void);
// 0x00000220 System.Void WebSocketSharpUnityMod.Server.HttpServer::set_WaitTime(System.TimeSpan)
extern void HttpServer_set_WaitTime_mBD0CF7B6BD6C2722BCF7E5F73CB3E64800429879 (void);
// 0x00000221 WebSocketSharpUnityMod.Server.WebSocketServiceManager WebSocketSharpUnityMod.Server.HttpServer::get_WebSocketServices()
extern void HttpServer_get_WebSocketServices_m1343E0F3ABCF66E7E31DBD6348C9BA697BAAB89C (void);
// 0x00000222 System.Void WebSocketSharpUnityMod.Server.HttpServer::add_OnConnect(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_add_OnConnect_m253A2D2EC681364297E4AE5C648C0891B92286D2 (void);
// 0x00000223 System.Void WebSocketSharpUnityMod.Server.HttpServer::remove_OnConnect(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_remove_OnConnect_mD5510A7CE2CECF16F47A8D25D5678DEE876D323A (void);
// 0x00000224 System.Void WebSocketSharpUnityMod.Server.HttpServer::add_OnDelete(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_add_OnDelete_m44C77148C1E5005E491A0F3741D32181ABE7B416 (void);
// 0x00000225 System.Void WebSocketSharpUnityMod.Server.HttpServer::remove_OnDelete(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_remove_OnDelete_m4B5DEB36E8B4128B65179E3B98346F4C02FD0711 (void);
// 0x00000226 System.Void WebSocketSharpUnityMod.Server.HttpServer::add_OnGet(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_add_OnGet_mCFE88CC3C6A162B711295EA4236F59F69093390D (void);
// 0x00000227 System.Void WebSocketSharpUnityMod.Server.HttpServer::remove_OnGet(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_remove_OnGet_mF697034A5E8F7B199BB93338B2323E3705F02DF5 (void);
// 0x00000228 System.Void WebSocketSharpUnityMod.Server.HttpServer::add_OnHead(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_add_OnHead_m7C50D1BFC64B7C324A31ED04ACDF18A3E14EBE96 (void);
// 0x00000229 System.Void WebSocketSharpUnityMod.Server.HttpServer::remove_OnHead(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_remove_OnHead_m7B040610C3EA55400834D6B66D4A9715BE575199 (void);
// 0x0000022A System.Void WebSocketSharpUnityMod.Server.HttpServer::add_OnOptions(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_add_OnOptions_m772E05CA32374497B14D44670FE5638EA97C1781 (void);
// 0x0000022B System.Void WebSocketSharpUnityMod.Server.HttpServer::remove_OnOptions(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_remove_OnOptions_mCF127FA31C9F9AA2B528451EF43F31C4D6FA2629 (void);
// 0x0000022C System.Void WebSocketSharpUnityMod.Server.HttpServer::add_OnPatch(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_add_OnPatch_mB1F3937DB8491C80060EE4B205D19C3400340C8E (void);
// 0x0000022D System.Void WebSocketSharpUnityMod.Server.HttpServer::remove_OnPatch(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_remove_OnPatch_mED2513606534E53ADF3FCEBF5B1C7E859F4D3BC1 (void);
// 0x0000022E System.Void WebSocketSharpUnityMod.Server.HttpServer::add_OnPost(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_add_OnPost_mDA23DCBFD1CF0FA8B8B2D6BD5536442E4E2F9619 (void);
// 0x0000022F System.Void WebSocketSharpUnityMod.Server.HttpServer::remove_OnPost(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_remove_OnPost_mE78E6BB87EB113F26ED15CF2792D5367E21903F0 (void);
// 0x00000230 System.Void WebSocketSharpUnityMod.Server.HttpServer::add_OnPut(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_add_OnPut_mC1FF6C7F2E80C5F09830EA4CE08B3A6F599D40D9 (void);
// 0x00000231 System.Void WebSocketSharpUnityMod.Server.HttpServer::remove_OnPut(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_remove_OnPut_m3D3853D50B365C9EA2FDA71F77FA6A44C7DB615F (void);
// 0x00000232 System.Void WebSocketSharpUnityMod.Server.HttpServer::add_OnTrace(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_add_OnTrace_m45C2DAE7138F07025428E6F9A9F6EC56585E3776 (void);
// 0x00000233 System.Void WebSocketSharpUnityMod.Server.HttpServer::remove_OnTrace(System.EventHandler`1<WebSocketSharpUnityMod.Server.HttpRequestEventArgs>)
extern void HttpServer_remove_OnTrace_m4B85129150909AB589ECFDC5E52295CC24E41A4E (void);
// 0x00000234 System.Void WebSocketSharpUnityMod.Server.HttpServer::abort()
extern void HttpServer_abort_mA38C6AFE2219413FA16E16EBF1C79F470235B0F4 (void);
// 0x00000235 System.String WebSocketSharpUnityMod.Server.HttpServer::checkIfCertificateExists()
extern void HttpServer_checkIfCertificateExists_mFFF3628014257D3056E43C0C6938FC4E450BBA74 (void);
// 0x00000236 System.Void WebSocketSharpUnityMod.Server.HttpServer::init(System.String,System.Net.IPAddress,System.Int32,System.Boolean)
extern void HttpServer_init_mF554AE2F1AA8FC5BE77762E3BE289B9A5CEDCC7F (void);
// 0x00000237 System.Void WebSocketSharpUnityMod.Server.HttpServer::processRequest(WebSocketSharpUnityMod.Net.HttpListenerContext)
extern void HttpServer_processRequest_m7359FA062C7A54E7389EBF4FD985CD33DABBE812 (void);
// 0x00000238 System.Void WebSocketSharpUnityMod.Server.HttpServer::processRequest(WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext)
extern void HttpServer_processRequest_m83F4FDC994D2E78B77C41A14D514B1E00C024FAA (void);
// 0x00000239 System.Void WebSocketSharpUnityMod.Server.HttpServer::receiveRequest()
extern void HttpServer_receiveRequest_m2A10820A4997C5A836EEA89A5F103C2430CE11E1 (void);
// 0x0000023A System.Void WebSocketSharpUnityMod.Server.HttpServer::startReceiving()
extern void HttpServer_startReceiving_m4E127CB9FBF4C7E503F08D094E947152E1BBD32F (void);
// 0x0000023B System.Void WebSocketSharpUnityMod.Server.HttpServer::stopReceiving(System.Int32)
extern void HttpServer_stopReceiving_mD602C3E2AD3EB0587F9374CE177320BC0885E948 (void);
// 0x0000023C System.Boolean WebSocketSharpUnityMod.Server.HttpServer::tryCreateUri(System.String,System.Uri&,System.String&)
extern void HttpServer_tryCreateUri_m08D8FC25F19D7C85371F27CC2049984244291F09 (void);
// 0x0000023D System.Void WebSocketSharpUnityMod.Server.HttpServer::AddWebSocketService(System.String,System.Func`1<TBehavior>)
// 0x0000023E System.Void WebSocketSharpUnityMod.Server.HttpServer::AddWebSocketService(System.String)
// 0x0000023F System.Byte[] WebSocketSharpUnityMod.Server.HttpServer::GetFile(System.String)
extern void HttpServer_GetFile_mBFDFE78AC79FCD09D9255A9C1C83722A5AC9FEE0 (void);
// 0x00000240 System.Boolean WebSocketSharpUnityMod.Server.HttpServer::RemoveWebSocketService(System.String)
extern void HttpServer_RemoveWebSocketService_m109334F46E479884A7AA7C7F261B1A5946EA2EF7 (void);
// 0x00000241 System.Void WebSocketSharpUnityMod.Server.HttpServer::Start()
extern void HttpServer_Start_m7EF6EF4C7D863D1D7FF5294023314663940E1A51 (void);
// 0x00000242 System.Void WebSocketSharpUnityMod.Server.HttpServer::Stop()
extern void HttpServer_Stop_m49B2E145294ABA04B1CC2D7FDF6619DC46B7AE73 (void);
// 0x00000243 System.Void WebSocketSharpUnityMod.Server.HttpServer::Stop(System.UInt16,System.String)
extern void HttpServer_Stop_mCAF538BE77B39C4F558C12B4E17E79F4BE4D757F (void);
// 0x00000244 System.Void WebSocketSharpUnityMod.Server.HttpServer::Stop(WebSocketSharpUnityMod.CloseStatusCode,System.String)
extern void HttpServer_Stop_m12C6180B3521F2778B616C3EC59EA2CC2733789C (void);
// 0x00000245 System.Void WebSocketSharpUnityMod.Server.HttpServer/<>c__DisplayClass86_0::.ctor()
extern void U3CU3Ec__DisplayClass86_0__ctor_mCC0E5F7F0855451C61A3A1F61375C49FDFB3169F (void);
// 0x00000246 System.Void WebSocketSharpUnityMod.Server.HttpServer/<>c__DisplayClass86_0::<receiveRequest>b__0(System.Object)
extern void U3CU3Ec__DisplayClass86_0_U3CreceiveRequestU3Eb__0_mACF2AD9A11635ED2B62A3F62B2011D53EA8D1AB3 (void);
// 0x00000247 System.Void WebSocketSharpUnityMod.Server.HttpServer/<>c__91`1::.cctor()
// 0x00000248 System.Void WebSocketSharpUnityMod.Server.HttpServer/<>c__91`1::.ctor()
// 0x00000249 TBehaviorWithNew WebSocketSharpUnityMod.Server.HttpServer/<>c__91`1::<AddWebSocketService>b__91_0()
// 0x0000024A WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext WebSocketSharpUnityMod.Server.IWebSocketSession::get_Context()
// 0x0000024B System.String WebSocketSharpUnityMod.Server.IWebSocketSession::get_ID()
// 0x0000024C System.String WebSocketSharpUnityMod.Server.IWebSocketSession::get_Protocol()
// 0x0000024D System.DateTime WebSocketSharpUnityMod.Server.IWebSocketSession::get_StartTime()
// 0x0000024E WebSocketSharpUnityMod.WebSocketState WebSocketSharpUnityMod.Server.IWebSocketSession::get_State()
// 0x0000024F System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::.ctor()
extern void WebSocketBehavior__ctor_mB69CD0F8BE079E631AF0425828422167FB129D18 (void);
// 0x00000250 WebSocketSharpUnityMod.Logger WebSocketSharpUnityMod.Server.WebSocketBehavior::get_Log()
extern void WebSocketBehavior_get_Log_m7989008183C959C2D20EEC95D593C609204709B6 (void);
// 0x00000251 WebSocketSharpUnityMod.Server.WebSocketSessionManager WebSocketSharpUnityMod.Server.WebSocketBehavior::get_Sessions()
extern void WebSocketBehavior_get_Sessions_m7F6B6AB299DADD590D5503713CA23A98DC6F2909 (void);
// 0x00000252 WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext WebSocketSharpUnityMod.Server.WebSocketBehavior::get_Context()
extern void WebSocketBehavior_get_Context_mD9BB71416FBF52E80975BBDFCE3AC3413E3C1AD4 (void);
// 0x00000253 System.Func`3<WebSocketSharpUnityMod.Net.CookieCollection,WebSocketSharpUnityMod.Net.CookieCollection,System.Boolean> WebSocketSharpUnityMod.Server.WebSocketBehavior::get_CookiesValidator()
extern void WebSocketBehavior_get_CookiesValidator_m62D859A743F514AF889BEA2D672FF1B60875A993 (void);
// 0x00000254 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::set_CookiesValidator(System.Func`3<WebSocketSharpUnityMod.Net.CookieCollection,WebSocketSharpUnityMod.Net.CookieCollection,System.Boolean>)
extern void WebSocketBehavior_set_CookiesValidator_mFD88608DF2480C5967C512C0AD8D27C0EC71D14B (void);
// 0x00000255 System.Boolean WebSocketSharpUnityMod.Server.WebSocketBehavior::get_EmitOnPing()
extern void WebSocketBehavior_get_EmitOnPing_m8B77A4E461D7E5CF1B64E41D09A458F0CFA29BD4 (void);
// 0x00000256 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::set_EmitOnPing(System.Boolean)
extern void WebSocketBehavior_set_EmitOnPing_mDB569002E3F3E5E223FCD08FBA8BF2DE5F82A13B (void);
// 0x00000257 System.String WebSocketSharpUnityMod.Server.WebSocketBehavior::get_ID()
extern void WebSocketBehavior_get_ID_m82FC940BB8335EDFC98BD2BBF0F2351AE15B20DB (void);
// 0x00000258 System.Boolean WebSocketSharpUnityMod.Server.WebSocketBehavior::get_IgnoreExtensions()
extern void WebSocketBehavior_get_IgnoreExtensions_m2DB944A7016C790243C545C89137F0BCAD6F0373 (void);
// 0x00000259 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::set_IgnoreExtensions(System.Boolean)
extern void WebSocketBehavior_set_IgnoreExtensions_mE87692D54808E6DB0178C40E4CBDE58BF46BD99F (void);
// 0x0000025A System.Func`2<System.String,System.Boolean> WebSocketSharpUnityMod.Server.WebSocketBehavior::get_OriginValidator()
extern void WebSocketBehavior_get_OriginValidator_m587F2A0B36B278CD8CB941BE4013DC0C328E947B (void);
// 0x0000025B System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::set_OriginValidator(System.Func`2<System.String,System.Boolean>)
extern void WebSocketBehavior_set_OriginValidator_m33F0210C2E1969E07B7CC689024E980136C64F68 (void);
// 0x0000025C System.String WebSocketSharpUnityMod.Server.WebSocketBehavior::get_Protocol()
extern void WebSocketBehavior_get_Protocol_mCCCFCA255F83B003B5E8B1419A9453D2D640C52B (void);
// 0x0000025D System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::set_Protocol(System.String)
extern void WebSocketBehavior_set_Protocol_mCABE8B7F086BF0D5816C0DA89885DDE32ACA2310 (void);
// 0x0000025E System.DateTime WebSocketSharpUnityMod.Server.WebSocketBehavior::get_StartTime()
extern void WebSocketBehavior_get_StartTime_m6E415E633C0EFD47EE9D5740781EC051018F89BC (void);
// 0x0000025F WebSocketSharpUnityMod.WebSocketState WebSocketSharpUnityMod.Server.WebSocketBehavior::get_State()
extern void WebSocketBehavior_get_State_mDB2AF6D0504D9519CB6B58C463F615849AB7B768 (void);
// 0x00000260 System.String WebSocketSharpUnityMod.Server.WebSocketBehavior::checkHandshakeRequest(WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext)
extern void WebSocketBehavior_checkHandshakeRequest_m6BB3061938DE6E18861822C870E36A86C5BFBF63 (void);
// 0x00000261 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::onClose(System.Object,WebSocketSharpUnityMod.CloseEventArgs)
extern void WebSocketBehavior_onClose_m18DDD35ACA597100CCC7E51B694232F5DEA49E4D (void);
// 0x00000262 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::onError(System.Object,WebSocketSharpUnityMod.ErrorEventArgs)
extern void WebSocketBehavior_onError_mEF0AF28694877B39491D642E2C35DB1C85F65DD3 (void);
// 0x00000263 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::onMessage(System.Object,WebSocketSharpUnityMod.MessageEventArgs)
extern void WebSocketBehavior_onMessage_m12C120F35939E2C366B131ACB89310F59801332A (void);
// 0x00000264 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::onOpen(System.Object,System.EventArgs)
extern void WebSocketBehavior_onOpen_m3DCF2E02A3E3429190F1612514442A55213CE79D (void);
// 0x00000265 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::Start(WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext,WebSocketSharpUnityMod.Server.WebSocketSessionManager)
extern void WebSocketBehavior_Start_mCB1750F34AD3277A423A5B1E0A64DF322D1D763C (void);
// 0x00000266 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::Error(System.String,System.Exception)
extern void WebSocketBehavior_Error_m3A2E53BF08D3B77F74A87EE7791A8B29F7A50DB4 (void);
// 0x00000267 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::OnClose(WebSocketSharpUnityMod.CloseEventArgs)
extern void WebSocketBehavior_OnClose_mBAE33B8F48CD6F519FBEDE7922344BE9D97BE9F2 (void);
// 0x00000268 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::OnError(WebSocketSharpUnityMod.ErrorEventArgs)
extern void WebSocketBehavior_OnError_m9FD926C4E48244487D876A752DE014761E99B7A3 (void);
// 0x00000269 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::OnMessage(WebSocketSharpUnityMod.MessageEventArgs)
extern void WebSocketBehavior_OnMessage_m9F31D9FF320C3ADC9C402690894AA3E696D9CDA3 (void);
// 0x0000026A System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::OnOpen()
extern void WebSocketBehavior_OnOpen_mB911D85CA1721BFE1E31708E61609DE5959B8A14 (void);
// 0x0000026B System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::Send(System.Byte[])
extern void WebSocketBehavior_Send_mBD106C2E60AD33F3C3A9B3DEEFB06BFD14BFB587 (void);
// 0x0000026C System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::Send(System.IO.FileInfo)
extern void WebSocketBehavior_Send_m1A19F4767E85EB01AC26CCD66E6EBB2D7091324E (void);
// 0x0000026D System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::Send(System.String)
extern void WebSocketBehavior_Send_mC481D466582024F86CA96B5037B6A9EC5928600C (void);
// 0x0000026E System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::SendAsync(System.Byte[],System.Action`1<System.Boolean>)
extern void WebSocketBehavior_SendAsync_m14B725F78AAC6A9235B227DAEC5CEDDEF085868F (void);
// 0x0000026F System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::SendAsync(System.IO.FileInfo,System.Action`1<System.Boolean>)
extern void WebSocketBehavior_SendAsync_mE6ECD5B800114C61E7754FDF9EEC3D63EE98AC81 (void);
// 0x00000270 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::SendAsync(System.String,System.Action`1<System.Boolean>)
extern void WebSocketBehavior_SendAsync_mDB1C430F6EAFEDDA169E166EDFD5A32A0491C78C (void);
// 0x00000271 System.Void WebSocketSharpUnityMod.Server.WebSocketBehavior::SendAsync(System.IO.Stream,System.Int32,System.Action`1<System.Boolean>)
extern void WebSocketBehavior_SendAsync_mA0FF0DF16EF4493E9573EADF35573BC8DC72E489 (void);
// 0x00000272 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::.cctor()
extern void WebSocketServer__cctor_m72649DF133AEC9F2EF23F0370E96A5C97E2D1A41 (void);
// 0x00000273 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::.ctor()
extern void WebSocketServer__ctor_m8172E84A867095DC3059A2B5CD929025B39CF486 (void);
// 0x00000274 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::.ctor(System.Int32)
extern void WebSocketServer__ctor_mFB2C38442C2E6CC649521BD06EAB2BC6C2A7B968 (void);
// 0x00000275 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::.ctor(System.String)
extern void WebSocketServer__ctor_mD7809553404466B2FF023D876F31216B8BF0F21A (void);
// 0x00000276 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::.ctor(System.Int32,System.Boolean)
extern void WebSocketServer__ctor_mA03A10790B620EEA506B4A292611FF985F6CA28C (void);
// 0x00000277 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::.ctor(System.Net.IPAddress,System.Int32)
extern void WebSocketServer__ctor_m1AA10A592E68E82E359F38189FC3D02D094659FB (void);
// 0x00000278 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::.ctor(System.Net.IPAddress,System.Int32,System.Boolean)
extern void WebSocketServer__ctor_m95EB71747A91626A796383551B55D491BC4E06A7 (void);
// 0x00000279 System.Net.IPAddress WebSocketSharpUnityMod.Server.WebSocketServer::get_Address()
extern void WebSocketServer_get_Address_mD48CCAD7685114CE68C8FE4CDD2906B6D569330A (void);
// 0x0000027A WebSocketSharpUnityMod.Net.AuthenticationSchemes WebSocketSharpUnityMod.Server.WebSocketServer::get_AuthenticationSchemes()
extern void WebSocketServer_get_AuthenticationSchemes_m10C0F0A8AFA89540338A49907BA43C42FBAE2220 (void);
// 0x0000027B System.Void WebSocketSharpUnityMod.Server.WebSocketServer::set_AuthenticationSchemes(WebSocketSharpUnityMod.Net.AuthenticationSchemes)
extern void WebSocketServer_set_AuthenticationSchemes_mBA40F1856975767D01B03AA8373FA7F816C9945D (void);
// 0x0000027C System.Boolean WebSocketSharpUnityMod.Server.WebSocketServer::get_IsListening()
extern void WebSocketServer_get_IsListening_m1B94CA092B6151AC62F86DEB460C93063A6C2E0E (void);
// 0x0000027D System.Boolean WebSocketSharpUnityMod.Server.WebSocketServer::get_IsSecure()
extern void WebSocketServer_get_IsSecure_m9E6F5A71B04AA05AD129B89442C5D8F67CE4CF58 (void);
// 0x0000027E System.Boolean WebSocketSharpUnityMod.Server.WebSocketServer::get_KeepClean()
extern void WebSocketServer_get_KeepClean_mFD6F732AD1D3692D1DBEB919F030E37C084BA8FD (void);
// 0x0000027F System.Void WebSocketSharpUnityMod.Server.WebSocketServer::set_KeepClean(System.Boolean)
extern void WebSocketServer_set_KeepClean_m1BC48920A4FA76EFEDA01DC606EE075471525029 (void);
// 0x00000280 WebSocketSharpUnityMod.Logger WebSocketSharpUnityMod.Server.WebSocketServer::get_Log()
extern void WebSocketServer_get_Log_mF1FE98412ED085FFAB59E9CDBA867622182C6A62 (void);
// 0x00000281 System.Int32 WebSocketSharpUnityMod.Server.WebSocketServer::get_Port()
extern void WebSocketServer_get_Port_m7B932D59EFF54AC92F4F7AE56F03B4F679C8E667 (void);
// 0x00000282 System.String WebSocketSharpUnityMod.Server.WebSocketServer::get_Realm()
extern void WebSocketServer_get_Realm_m511AC86786D84DAFFF98A7D908F32BE385671DB0 (void);
// 0x00000283 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::set_Realm(System.String)
extern void WebSocketServer_set_Realm_mBFDF3FD3CA1B4C183709938E3220F4FBCCF09598 (void);
// 0x00000284 System.Boolean WebSocketSharpUnityMod.Server.WebSocketServer::get_ReuseAddress()
extern void WebSocketServer_get_ReuseAddress_m3D6C2C83BD9F34225662309DA2357970A08A48E6 (void);
// 0x00000285 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::set_ReuseAddress(System.Boolean)
extern void WebSocketServer_set_ReuseAddress_m05AD4870B04E824926F6E7778F27536C959EED3F (void);
// 0x00000286 WebSocketSharpUnityMod.Net.ServerSslConfiguration WebSocketSharpUnityMod.Server.WebSocketServer::get_SslConfiguration()
extern void WebSocketServer_get_SslConfiguration_m8E3D94D3EC0CD107B6C9146B230D82EC7BB270A9 (void);
// 0x00000287 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::set_SslConfiguration(WebSocketSharpUnityMod.Net.ServerSslConfiguration)
extern void WebSocketServer_set_SslConfiguration_m16CED19833B58FE4D8EBDBFD30DE8948967A1575 (void);
// 0x00000288 System.Func`2<System.Security.Principal.IIdentity,WebSocketSharpUnityMod.Net.NetworkCredential> WebSocketSharpUnityMod.Server.WebSocketServer::get_UserCredentialsFinder()
extern void WebSocketServer_get_UserCredentialsFinder_mCF29E4D3B4BF27E6E6CE5BBB3D36EC5F436EC02F (void);
// 0x00000289 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::set_UserCredentialsFinder(System.Func`2<System.Security.Principal.IIdentity,WebSocketSharpUnityMod.Net.NetworkCredential>)
extern void WebSocketServer_set_UserCredentialsFinder_mFF89368995A1E13CBFE54F34B7985CEFC6576937 (void);
// 0x0000028A System.TimeSpan WebSocketSharpUnityMod.Server.WebSocketServer::get_WaitTime()
extern void WebSocketServer_get_WaitTime_m1EE6F8045FF57FE4C0AAA93FD59B2AD5666DE3A9 (void);
// 0x0000028B System.Void WebSocketSharpUnityMod.Server.WebSocketServer::set_WaitTime(System.TimeSpan)
extern void WebSocketServer_set_WaitTime_m150D71A7AA8B4B01B3CCB897F8FEAE81EF6CFB92 (void);
// 0x0000028C WebSocketSharpUnityMod.Server.WebSocketServiceManager WebSocketSharpUnityMod.Server.WebSocketServer::get_WebSocketServices()
extern void WebSocketServer_get_WebSocketServices_m1CFA935B60F4C7D881699CAF4C62A287652D4F9A (void);
// 0x0000028D System.Void WebSocketSharpUnityMod.Server.WebSocketServer::abort()
extern void WebSocketServer_abort_mB6645B62DF787AABA98444DA99A0FA0FA60C7997 (void);
// 0x0000028E System.String WebSocketSharpUnityMod.Server.WebSocketServer::checkIfCertificateExists()
extern void WebSocketServer_checkIfCertificateExists_m03F573435B7B9FCD69267ECE2C82EA8002D29A4A (void);
// 0x0000028F System.String WebSocketSharpUnityMod.Server.WebSocketServer::getRealm()
extern void WebSocketServer_getRealm_mC98FD0370B937B7C7B07E7AE3C1AC4CB5386D51A (void);
// 0x00000290 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::init(System.String,System.Net.IPAddress,System.Int32,System.Boolean)
extern void WebSocketServer_init_m1122C9D953F43BF2A78CD321165A3E73B839CE98 (void);
// 0x00000291 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::processRequest(WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext)
extern void WebSocketServer_processRequest_m4E5E807CB9C78064903BBB3A0065BAE81E1F9EBD (void);
// 0x00000292 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::receiveRequest()
extern void WebSocketServer_receiveRequest_mA0DA0A6FA24F454215A923B0DE6C9ABC7DEF6970 (void);
// 0x00000293 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::startReceiving()
extern void WebSocketServer_startReceiving_m8D00A595C395160E85AAA689865DF5D494101639 (void);
// 0x00000294 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::stopReceiving(System.Int32)
extern void WebSocketServer_stopReceiving_m196D234F665B7D0E7FC13A3E737AF22F08BB1327 (void);
// 0x00000295 System.Boolean WebSocketSharpUnityMod.Server.WebSocketServer::tryCreateUri(System.String,System.Uri&,System.String&)
extern void WebSocketServer_tryCreateUri_m9ACB258906C0FA984027E811A58AEBD205F708CC (void);
// 0x00000296 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::AddWebSocketService(System.String,System.Func`1<TBehavior>)
// 0x00000297 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::AddWebSocketService(System.String)
// 0x00000298 System.Boolean WebSocketSharpUnityMod.Server.WebSocketServer::RemoveWebSocketService(System.String)
extern void WebSocketServer_RemoveWebSocketService_m025ADC8F965745C3E2DE2B0F8A24E74B198C6F38 (void);
// 0x00000299 System.Void WebSocketSharpUnityMod.Server.WebSocketServer::Start()
extern void WebSocketServer_Start_m4166A74AEB499EC4936AC02A4BC3B0F6F74A394E (void);
// 0x0000029A System.Void WebSocketSharpUnityMod.Server.WebSocketServer::Stop()
extern void WebSocketServer_Stop_mBEAD1110D6BFCFA962387A255E43AC7755E357DD (void);
// 0x0000029B System.Void WebSocketSharpUnityMod.Server.WebSocketServer::Stop(System.UInt16,System.String)
extern void WebSocketServer_Stop_mBCD6AC7CE488F32636FBF2546C1D21E1E0C9B279 (void);
// 0x0000029C System.Void WebSocketSharpUnityMod.Server.WebSocketServer::Stop(WebSocketSharpUnityMod.CloseStatusCode,System.String)
extern void WebSocketServer_Stop_mF3D73B556324C40603F4B47E0297A4D47CACAC39 (void);
// 0x0000029D System.Void WebSocketSharpUnityMod.Server.WebSocketServer/<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_m693AF89D78F7D599A78EC51A996145904F91A44B (void);
// 0x0000029E System.Void WebSocketSharpUnityMod.Server.WebSocketServer/<>c__DisplayClass62_0::<receiveRequest>b__0(System.Object)
extern void U3CU3Ec__DisplayClass62_0_U3CreceiveRequestU3Eb__0_m9ECAF23C2B920ED73D051B796FEF6FC07C7AECE8 (void);
// 0x0000029F System.Void WebSocketSharpUnityMod.Server.WebSocketServer/<>c__67`1::.cctor()
// 0x000002A0 System.Void WebSocketSharpUnityMod.Server.WebSocketServer/<>c__67`1::.ctor()
// 0x000002A1 TBehaviorWithNew WebSocketSharpUnityMod.Server.WebSocketServer/<>c__67`1::<AddWebSocketService>b__67_0()
// 0x000002A2 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceHost::.ctor()
extern void WebSocketServiceHost__ctor_mD116561F533248F8F7B4B9E1305D41DBCF1CDFE9 (void);
// 0x000002A3 WebSocketSharpUnityMod.Server.ServerState WebSocketSharpUnityMod.Server.WebSocketServiceHost::get_State()
extern void WebSocketServiceHost_get_State_m2A2F4166E8246F7B9D78738EED701E68C0DF5E70 (void);
// 0x000002A4 System.Boolean WebSocketSharpUnityMod.Server.WebSocketServiceHost::get_KeepClean()
// 0x000002A5 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceHost::set_KeepClean(System.Boolean)
// 0x000002A6 System.String WebSocketSharpUnityMod.Server.WebSocketServiceHost::get_Path()
// 0x000002A7 WebSocketSharpUnityMod.Server.WebSocketSessionManager WebSocketSharpUnityMod.Server.WebSocketServiceHost::get_Sessions()
// 0x000002A8 System.Type WebSocketSharpUnityMod.Server.WebSocketServiceHost::get_Type()
// 0x000002A9 System.TimeSpan WebSocketSharpUnityMod.Server.WebSocketServiceHost::get_WaitTime()
// 0x000002AA System.Void WebSocketSharpUnityMod.Server.WebSocketServiceHost::set_WaitTime(System.TimeSpan)
// 0x000002AB System.Void WebSocketSharpUnityMod.Server.WebSocketServiceHost::Start()
extern void WebSocketServiceHost_Start_mC5CCDF205BD529D2229354D711331C8C754DCEC9 (void);
// 0x000002AC System.Void WebSocketSharpUnityMod.Server.WebSocketServiceHost::StartSession(WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext)
extern void WebSocketServiceHost_StartSession_m52F57EB4D8ACF4D9B98829048D918788864AAC08 (void);
// 0x000002AD System.Void WebSocketSharpUnityMod.Server.WebSocketServiceHost::Stop(System.UInt16,System.String)
extern void WebSocketServiceHost_Stop_m3F1CACDC7861191EEAD602A606BC3FC4AECB68A1 (void);
// 0x000002AE WebSocketSharpUnityMod.Server.WebSocketBehavior WebSocketSharpUnityMod.Server.WebSocketServiceHost::CreateSession()
// 0x000002AF System.Void WebSocketSharpUnityMod.Server.WebSocketServiceHost`1::.ctor(System.String,System.Func`1<TBehavior>,WebSocketSharpUnityMod.Logger)
// 0x000002B0 System.Boolean WebSocketSharpUnityMod.Server.WebSocketServiceHost`1::get_KeepClean()
// 0x000002B1 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceHost`1::set_KeepClean(System.Boolean)
// 0x000002B2 System.String WebSocketSharpUnityMod.Server.WebSocketServiceHost`1::get_Path()
// 0x000002B3 WebSocketSharpUnityMod.Server.WebSocketSessionManager WebSocketSharpUnityMod.Server.WebSocketServiceHost`1::get_Sessions()
// 0x000002B4 System.Type WebSocketSharpUnityMod.Server.WebSocketServiceHost`1::get_Type()
// 0x000002B5 System.TimeSpan WebSocketSharpUnityMod.Server.WebSocketServiceHost`1::get_WaitTime()
// 0x000002B6 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceHost`1::set_WaitTime(System.TimeSpan)
// 0x000002B7 WebSocketSharpUnityMod.Server.WebSocketBehavior WebSocketSharpUnityMod.Server.WebSocketServiceHost`1::CreateSession()
// 0x000002B8 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::.ctor()
extern void WebSocketServiceManager__ctor_m09F432932F99C4C442AA774656F873A99B6FD08B (void);
// 0x000002B9 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::.ctor(WebSocketSharpUnityMod.Logger)
extern void WebSocketServiceManager__ctor_mC95391F343A22D22B57341D1EE19497F4413885F (void);
// 0x000002BA System.Int32 WebSocketSharpUnityMod.Server.WebSocketServiceManager::get_Count()
extern void WebSocketServiceManager_get_Count_m6F5B479E90D06B89806FF76A7A770601B57D8A35 (void);
// 0x000002BB System.Collections.Generic.IEnumerable`1<WebSocketSharpUnityMod.Server.WebSocketServiceHost> WebSocketSharpUnityMod.Server.WebSocketServiceManager::get_Hosts()
extern void WebSocketServiceManager_get_Hosts_m9F5A21549711201CE552AC5F12268BADB682938B (void);
// 0x000002BC WebSocketSharpUnityMod.Server.WebSocketServiceHost WebSocketSharpUnityMod.Server.WebSocketServiceManager::get_Item(System.String)
extern void WebSocketServiceManager_get_Item_mA7149399742C2F1106DAFF68B804A9E4B4BC2003 (void);
// 0x000002BD System.Boolean WebSocketSharpUnityMod.Server.WebSocketServiceManager::get_KeepClean()
extern void WebSocketServiceManager_get_KeepClean_m93E8C12AFEBB254A00BFE0D312B3D25A9E655EC5 (void);
// 0x000002BE System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::set_KeepClean(System.Boolean)
extern void WebSocketServiceManager_set_KeepClean_m2412638ED5B723D2DEED01C5C5388FD97F4A462C (void);
// 0x000002BF System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharpUnityMod.Server.WebSocketServiceManager::get_Paths()
extern void WebSocketServiceManager_get_Paths_mE3D8418DF9C1DA598EB6A7867373A6EC7E68F019 (void);
// 0x000002C0 System.Int32 WebSocketSharpUnityMod.Server.WebSocketServiceManager::get_SessionCount()
extern void WebSocketServiceManager_get_SessionCount_m520286E2C0F82330C8DBCC4935D38F3BF52264AF (void);
// 0x000002C1 System.TimeSpan WebSocketSharpUnityMod.Server.WebSocketServiceManager::get_WaitTime()
extern void WebSocketServiceManager_get_WaitTime_m569AC21587EAA1D71ECD933DA896BFE56824BBEB (void);
// 0x000002C2 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::set_WaitTime(System.TimeSpan)
extern void WebSocketServiceManager_set_WaitTime_m2908164E9F33D135FB2CE980DCF687068E15ED29 (void);
// 0x000002C3 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::broadcast(WebSocketSharpUnityMod.Opcode,System.Byte[],System.Action)
extern void WebSocketServiceManager_broadcast_m99E69DD7D8825528FF9A99CB1A8958B5B9AA37C1 (void);
// 0x000002C4 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::broadcast(WebSocketSharpUnityMod.Opcode,System.IO.Stream,System.Action)
extern void WebSocketServiceManager_broadcast_mA690614B15595047D64B8FEAF574664B468232DF (void);
// 0x000002C5 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::broadcastAsync(WebSocketSharpUnityMod.Opcode,System.Byte[],System.Action)
extern void WebSocketServiceManager_broadcastAsync_m743C98B66C0F6D40CD3398CF1843DB161B769741 (void);
// 0x000002C6 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::broadcastAsync(WebSocketSharpUnityMod.Opcode,System.IO.Stream,System.Action)
extern void WebSocketServiceManager_broadcastAsync_m6E9253CE1B06050FBCD5D94D026D7135EFB2F1D3 (void);
// 0x000002C7 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Boolean>> WebSocketSharpUnityMod.Server.WebSocketServiceManager::broadping(System.Byte[],System.TimeSpan)
extern void WebSocketServiceManager_broadping_m86E74256212C28DCC271A3AF0B836013A3659153 (void);
// 0x000002C8 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::Add(System.String,System.Func`1<TBehavior>)
// 0x000002C9 System.Boolean WebSocketSharpUnityMod.Server.WebSocketServiceManager::InternalTryGetServiceHost(System.String,WebSocketSharpUnityMod.Server.WebSocketServiceHost&)
extern void WebSocketServiceManager_InternalTryGetServiceHost_m3C017BD2FE5A536AAB1D0E47A24A58C876B4DA50 (void);
// 0x000002CA System.Boolean WebSocketSharpUnityMod.Server.WebSocketServiceManager::Remove(System.String)
extern void WebSocketServiceManager_Remove_m3A90AF612AC710838FA4BC2C10FEC0A39C7D7DFD (void);
// 0x000002CB System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::Start()
extern void WebSocketServiceManager_Start_m47638B5FEC7A09AAEF6F7EF1E668FC6D4A8656C0 (void);
// 0x000002CC System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::Stop(WebSocketSharpUnityMod.CloseEventArgs,System.Boolean,System.Boolean)
extern void WebSocketServiceManager_Stop_mB3FAEDE44385078FFF435E86D52D30AD9A79BF4A (void);
// 0x000002CD System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::Broadcast(System.Byte[])
extern void WebSocketServiceManager_Broadcast_m8A13771105D003BC2AB1DC011665F2C0B11D3F45 (void);
// 0x000002CE System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::Broadcast(System.String)
extern void WebSocketServiceManager_Broadcast_m3A9E0DB049F342BF7FDD7FE1B88C24C395D45293 (void);
// 0x000002CF System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::BroadcastAsync(System.Byte[],System.Action)
extern void WebSocketServiceManager_BroadcastAsync_mC5BEE11884B1DB0A3B56B57E92F1EB7FAE5C52E8 (void);
// 0x000002D0 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::BroadcastAsync(System.String,System.Action)
extern void WebSocketServiceManager_BroadcastAsync_m93428AF99D48D232DA5152B658DC5A9BC18E1A56 (void);
// 0x000002D1 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager::BroadcastAsync(System.IO.Stream,System.Int32,System.Action)
extern void WebSocketServiceManager_BroadcastAsync_m05C6C128479892A201C9AF50A7B7C725FCF1F6DF (void);
// 0x000002D2 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Boolean>> WebSocketSharpUnityMod.Server.WebSocketServiceManager::Broadping()
extern void WebSocketServiceManager_Broadping_m7B9C6760A1F3DA1AC76491F96741F3ECBEE5F000 (void);
// 0x000002D3 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Boolean>> WebSocketSharpUnityMod.Server.WebSocketServiceManager::Broadping(System.String)
extern void WebSocketServiceManager_Broadping_m3BA617CA437C4A440D374E9E3053F2BFFD2EBECE (void);
// 0x000002D4 System.Boolean WebSocketSharpUnityMod.Server.WebSocketServiceManager::TryGetServiceHost(System.String,WebSocketSharpUnityMod.Server.WebSocketServiceHost&)
extern void WebSocketServiceManager_TryGetServiceHost_m87CD93EB962B01F5CCFB629EAFA80CDF9427D84F (void);
// 0x000002D5 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m18B85F1BC450905DACD20CA087188ED47E6A76C5 (void);
// 0x000002D6 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager/<>c__DisplayClass26_0::<broadcastAsync>b__0(System.Object)
extern void U3CU3Ec__DisplayClass26_0_U3CbroadcastAsyncU3Eb__0_m9C3E1FF8BB55F081B3A4D5C511F343677A80086E (void);
// 0x000002D7 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_mD141A9C2AC564F39314BBF5966BDE2881F947CF2 (void);
// 0x000002D8 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager/<>c__DisplayClass27_0::<broadcastAsync>b__0(System.Object)
extern void U3CU3Ec__DisplayClass27_0_U3CbroadcastAsyncU3Eb__0_m913A1C316BB927CD1B1B8E87D96CB51930FC7F9F (void);
// 0x000002D9 System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mD30A536C8E63D2C8EF8EED0AC615CD03168A053E (void);
// 0x000002DA System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager/<>c__DisplayClass38_0::<BroadcastAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass38_0_U3CBroadcastAsyncU3Eb__0_mA0C8BABA31245B64EFA1A7E9E3941B4255155DBF (void);
// 0x000002DB System.Void WebSocketSharpUnityMod.Server.WebSocketServiceManager/<>c__DisplayClass38_0::<BroadcastAsync>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass38_0_U3CBroadcastAsyncU3Eb__1_mC6AA706384A1C9F1A3133697D0438291C0B27C91 (void);
// 0x000002DC System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::.ctor()
extern void WebSocketSessionManager__ctor_m25768072902FC30847D656939AE41D5CBADCA0AC (void);
// 0x000002DD System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::.ctor(WebSocketSharpUnityMod.Logger)
extern void WebSocketSessionManager__ctor_m32EA57F8957E7660203F448446DEE3DCF7624C3D (void);
// 0x000002DE WebSocketSharpUnityMod.Server.ServerState WebSocketSharpUnityMod.Server.WebSocketSessionManager::get_State()
extern void WebSocketSessionManager_get_State_m0ED89683BB2A3AEA12F12AD7A3D37EE38627D688 (void);
// 0x000002DF System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharpUnityMod.Server.WebSocketSessionManager::get_ActiveIDs()
extern void WebSocketSessionManager_get_ActiveIDs_m1F5990BD33FF4364A49341CCCEBEFD9F53AD37C8 (void);
// 0x000002E0 System.Int32 WebSocketSharpUnityMod.Server.WebSocketSessionManager::get_Count()
extern void WebSocketSessionManager_get_Count_m21319BABAE8AA310EA0329ADBA12A2590803BDFF (void);
// 0x000002E1 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharpUnityMod.Server.WebSocketSessionManager::get_IDs()
extern void WebSocketSessionManager_get_IDs_mF4D23E72A231C5AF92B23745D4F5915089429646 (void);
// 0x000002E2 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharpUnityMod.Server.WebSocketSessionManager::get_InactiveIDs()
extern void WebSocketSessionManager_get_InactiveIDs_m1FDB4A898C1CFBB9ABA3190661386F0DC09CDD65 (void);
// 0x000002E3 WebSocketSharpUnityMod.Server.IWebSocketSession WebSocketSharpUnityMod.Server.WebSocketSessionManager::get_Item(System.String)
extern void WebSocketSessionManager_get_Item_mD42A05520E9C67A2C9C1B7F3E7D1A045D1CE6459 (void);
// 0x000002E4 System.Boolean WebSocketSharpUnityMod.Server.WebSocketSessionManager::get_KeepClean()
extern void WebSocketSessionManager_get_KeepClean_m053333A96F311042CAE8531E5F39CD6CD79282CC (void);
// 0x000002E5 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::set_KeepClean(System.Boolean)
extern void WebSocketSessionManager_set_KeepClean_m2C8F6B62229C2D917D1F257BF6058C49E93FACB4 (void);
// 0x000002E6 System.Collections.Generic.IEnumerable`1<WebSocketSharpUnityMod.Server.IWebSocketSession> WebSocketSharpUnityMod.Server.WebSocketSessionManager::get_Sessions()
extern void WebSocketSessionManager_get_Sessions_mCD7C1ADBF162416653A781CF1C198EF27479C0AA (void);
// 0x000002E7 System.TimeSpan WebSocketSharpUnityMod.Server.WebSocketSessionManager::get_WaitTime()
extern void WebSocketSessionManager_get_WaitTime_mB50DE7AC810937F307D508E0AF7C3C591349BFEB (void);
// 0x000002E8 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::set_WaitTime(System.TimeSpan)
extern void WebSocketSessionManager_set_WaitTime_m5195D503FA4324CA708DB05BB69020EB5C830F5E (void);
// 0x000002E9 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::broadcast(WebSocketSharpUnityMod.Opcode,System.Byte[],System.Action)
extern void WebSocketSessionManager_broadcast_m7F671FA9856609A4B7381571F138756DA1C324A9 (void);
// 0x000002EA System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::broadcast(WebSocketSharpUnityMod.Opcode,System.IO.Stream,System.Action)
extern void WebSocketSessionManager_broadcast_m681636F712603C575E09667B5B156F8D4EAA08E5 (void);
// 0x000002EB System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::broadcastAsync(WebSocketSharpUnityMod.Opcode,System.Byte[],System.Action)
extern void WebSocketSessionManager_broadcastAsync_m471A1A5BE638CF6936D5548D53B34D0EE15F8620 (void);
// 0x000002EC System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::broadcastAsync(WebSocketSharpUnityMod.Opcode,System.IO.Stream,System.Action)
extern void WebSocketSessionManager_broadcastAsync_m2735643DBB2FB059F46FB04E323EC985EE61A093 (void);
// 0x000002ED System.String WebSocketSharpUnityMod.Server.WebSocketSessionManager::createID()
extern void WebSocketSessionManager_createID_mDAF9E8FC14219F1A54786D002A76449E289F43FF (void);
// 0x000002EE System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::setSweepTimer(System.Double)
extern void WebSocketSessionManager_setSweepTimer_m9DE40755D5EA9B91D146314222D2CD7790758CDC (void);
// 0x000002EF System.Boolean WebSocketSharpUnityMod.Server.WebSocketSessionManager::tryGetSession(System.String,WebSocketSharpUnityMod.Server.IWebSocketSession&)
extern void WebSocketSessionManager_tryGetSession_m6DCB52B7032EBD4112370651F85D37C2CECC687B (void);
// 0x000002F0 System.String WebSocketSharpUnityMod.Server.WebSocketSessionManager::Add(WebSocketSharpUnityMod.Server.IWebSocketSession)
extern void WebSocketSessionManager_Add_m6B3923C8F7C4B3C6800472521EA45225AD3EEE1D (void);
// 0x000002F1 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::Broadcast(WebSocketSharpUnityMod.Opcode,System.Byte[],System.Collections.Generic.Dictionary`2<WebSocketSharpUnityMod.CompressionMethod,System.Byte[]>)
extern void WebSocketSessionManager_Broadcast_mD8D91F66484D595FA395C30B6C9A9D381859D192 (void);
// 0x000002F2 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::Broadcast(WebSocketSharpUnityMod.Opcode,System.IO.Stream,System.Collections.Generic.Dictionary`2<WebSocketSharpUnityMod.CompressionMethod,System.IO.Stream>)
extern void WebSocketSessionManager_Broadcast_m3A96C906D013AEF90661F84CCC5A573AC061D883 (void);
// 0x000002F3 System.Collections.Generic.Dictionary`2<System.String,System.Boolean> WebSocketSharpUnityMod.Server.WebSocketSessionManager::Broadping(System.Byte[],System.TimeSpan)
extern void WebSocketSessionManager_Broadping_m991AC37F0D609BB46C1E86747D4D9EF1A8A348F2 (void);
// 0x000002F4 System.Boolean WebSocketSharpUnityMod.Server.WebSocketSessionManager::Remove(System.String)
extern void WebSocketSessionManager_Remove_mE7BB3D33ACE2DC677ADD7C5DFE7EA73FFF94DEDA (void);
// 0x000002F5 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::Start()
extern void WebSocketSessionManager_Start_mCF432CB3DB5A958027D14A4F9119656ED14A0B0E (void);
// 0x000002F6 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::Stop(WebSocketSharpUnityMod.CloseEventArgs,System.Byte[],System.Boolean)
extern void WebSocketSessionManager_Stop_mB5E1F1E059202093CCEB948819342577A48D4B19 (void);
// 0x000002F7 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::Broadcast(System.Byte[])
extern void WebSocketSessionManager_Broadcast_m400B4014269C101873CF4E848BDCA3A9989C088B (void);
// 0x000002F8 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::Broadcast(System.String)
extern void WebSocketSessionManager_Broadcast_m40452D4BD59939863152DD2998D040C0497CA70A (void);
// 0x000002F9 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::BroadcastAsync(System.Byte[],System.Action)
extern void WebSocketSessionManager_BroadcastAsync_mB56D32EC3D7E905E8734B52B4DFC855A36219A35 (void);
// 0x000002FA System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::BroadcastAsync(System.String,System.Action)
extern void WebSocketSessionManager_BroadcastAsync_m5BDA76C32B355900B2F2AD7A8CB7417F68BE902B (void);
// 0x000002FB System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::BroadcastAsync(System.IO.Stream,System.Int32,System.Action)
extern void WebSocketSessionManager_BroadcastAsync_mFF6FFB8A5158036E35A168F4E06145724A1D9287 (void);
// 0x000002FC System.Collections.Generic.Dictionary`2<System.String,System.Boolean> WebSocketSharpUnityMod.Server.WebSocketSessionManager::Broadping()
extern void WebSocketSessionManager_Broadping_m0485E20FDA7FFBD78325768D302CAB52F8E1C119 (void);
// 0x000002FD System.Collections.Generic.Dictionary`2<System.String,System.Boolean> WebSocketSharpUnityMod.Server.WebSocketSessionManager::Broadping(System.String)
extern void WebSocketSessionManager_Broadping_mC6CD0A064BA771A54D940A02FC6B672C733BA72C (void);
// 0x000002FE System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::CloseSession(System.String)
extern void WebSocketSessionManager_CloseSession_m7D6F1210508F2248C4C4519D39F4607E8E33F4AE (void);
// 0x000002FF System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::CloseSession(System.String,System.UInt16,System.String)
extern void WebSocketSessionManager_CloseSession_m4FE99ED53C796A628594CF4BDF3B3F8853E35FEE (void);
// 0x00000300 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::CloseSession(System.String,WebSocketSharpUnityMod.CloseStatusCode,System.String)
extern void WebSocketSessionManager_CloseSession_m41C39F768CEC8A58EB1E77B89CC79BF9A9EF4ECD (void);
// 0x00000301 System.Boolean WebSocketSharpUnityMod.Server.WebSocketSessionManager::PingTo(System.String)
extern void WebSocketSessionManager_PingTo_mF5BCEB41BD64DBB52B2F6A7FE1F516D53E526845 (void);
// 0x00000302 System.Boolean WebSocketSharpUnityMod.Server.WebSocketSessionManager::PingTo(System.String,System.String)
extern void WebSocketSessionManager_PingTo_mD46C8FA8E99DFC66B2CF874FD79C7AD90343D063 (void);
// 0x00000303 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::SendTo(System.Byte[],System.String)
extern void WebSocketSessionManager_SendTo_mA0828B37A86153F5B0F205CD6EE8E943B6ACC1F8 (void);
// 0x00000304 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::SendTo(System.String,System.String)
extern void WebSocketSessionManager_SendTo_mF898F94C7C0685CDC47271DF263FABC9B6DA68B6 (void);
// 0x00000305 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::SendToAsync(System.Byte[],System.String,System.Action`1<System.Boolean>)
extern void WebSocketSessionManager_SendToAsync_mDE62CA5269E313F07EDB9EEEE826EB3F022A55ED (void);
// 0x00000306 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::SendToAsync(System.String,System.String,System.Action`1<System.Boolean>)
extern void WebSocketSessionManager_SendToAsync_m1B866F38590B70BF1BB6AF65BC1DAC56FF62AF92 (void);
// 0x00000307 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::SendToAsync(System.IO.Stream,System.Int32,System.String,System.Action`1<System.Boolean>)
extern void WebSocketSessionManager_SendToAsync_m266D7E691657543CF6DE6220B395A8A3D1E30F97 (void);
// 0x00000308 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::Sweep()
extern void WebSocketSessionManager_Sweep_mED81F3B6998A8227A11A83AA3791A5D7900E8844 (void);
// 0x00000309 System.Boolean WebSocketSharpUnityMod.Server.WebSocketSessionManager::TryGetSession(System.String,WebSocketSharpUnityMod.Server.IWebSocketSession&)
extern void WebSocketSessionManager_TryGetSession_m0B32975452F2DC998E31627B1FFA15FDCBDBA37F (void);
// 0x0000030A System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager::<setSweepTimer>b__36_0(System.Object,System.Timers.ElapsedEventArgs)
extern void WebSocketSessionManager_U3CsetSweepTimerU3Eb__36_0_m36E23225D660A9F5675CB286D11459767626CE3B (void);
// 0x0000030B System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_ActiveIDs>d__14::.ctor(System.Int32)
extern void U3Cget_ActiveIDsU3Ed__14__ctor_m71A1D26CC23B9A74B72852F8C243B50E88D4EA9A (void);
// 0x0000030C System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_ActiveIDs>d__14::System.IDisposable.Dispose()
extern void U3Cget_ActiveIDsU3Ed__14_System_IDisposable_Dispose_mE2DD0639D42CD88B106730AF528C3FFCD30B88DD (void);
// 0x0000030D System.Boolean WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_ActiveIDs>d__14::MoveNext()
extern void U3Cget_ActiveIDsU3Ed__14_MoveNext_mAED55DC5C264F65E3D81536F0961E88577721595 (void);
// 0x0000030E System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_ActiveIDs>d__14::<>m__Finally1()
extern void U3Cget_ActiveIDsU3Ed__14_U3CU3Em__Finally1_mBA460B49A0A5CC9645DA4B07242FE5D3E0877EAF (void);
// 0x0000030F System.String WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_ActiveIDs>d__14::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3Cget_ActiveIDsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mDCF0F98E67F62D0A9A107C2682CBAE3F478EE410 (void);
// 0x00000310 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_ActiveIDs>d__14::System.Collections.IEnumerator.Reset()
extern void U3Cget_ActiveIDsU3Ed__14_System_Collections_IEnumerator_Reset_m71DE20ACE7C248A0AF728D65E7930CA8EB703EBE (void);
// 0x00000311 System.Object WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_ActiveIDs>d__14::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ActiveIDsU3Ed__14_System_Collections_IEnumerator_get_Current_mB84D4061A1B43CAD9203C40A5FBD96655BF7CF31 (void);
// 0x00000312 System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_ActiveIDs>d__14::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3Cget_ActiveIDsU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mB57543128E608ABFEA2CBF32CB45B17D917A8421 (void);
// 0x00000313 System.Collections.IEnumerator WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_ActiveIDs>d__14::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ActiveIDsU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m566759125BAE1DEA020747B5D031659268B06E2F (void);
// 0x00000314 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_InactiveIDs>d__20::.ctor(System.Int32)
extern void U3Cget_InactiveIDsU3Ed__20__ctor_mBF11FB11062B60B1D8183E1830DDF36F6AF12966 (void);
// 0x00000315 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_InactiveIDs>d__20::System.IDisposable.Dispose()
extern void U3Cget_InactiveIDsU3Ed__20_System_IDisposable_Dispose_mFED8DB88C4FE58CFE1E4656216CE48504A210C46 (void);
// 0x00000316 System.Boolean WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_InactiveIDs>d__20::MoveNext()
extern void U3Cget_InactiveIDsU3Ed__20_MoveNext_mFC308BF77E1F9236DB135B9C8FA4E707F797711B (void);
// 0x00000317 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_InactiveIDs>d__20::<>m__Finally1()
extern void U3Cget_InactiveIDsU3Ed__20_U3CU3Em__Finally1_m6655D3A9B0FDB0FC53FED7F19367E0B359BF70D5 (void);
// 0x00000318 System.String WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_InactiveIDs>d__20::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3Cget_InactiveIDsU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mCD617A347D13DAC605EA9AB9D1EDD5CD0EA62737 (void);
// 0x00000319 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_InactiveIDs>d__20::System.Collections.IEnumerator.Reset()
extern void U3Cget_InactiveIDsU3Ed__20_System_Collections_IEnumerator_Reset_m5568B7F0AD45A3F8BC4DFADA53F89E8883EB7DC4 (void);
// 0x0000031A System.Object WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_InactiveIDs>d__20::System.Collections.IEnumerator.get_Current()
extern void U3Cget_InactiveIDsU3Ed__20_System_Collections_IEnumerator_get_Current_m777CE54F9CCC0360D73965C468034786F90E98FA (void);
// 0x0000031B System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_InactiveIDs>d__20::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3Cget_InactiveIDsU3Ed__20_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m7F650158FCE88C529E8AE22DDA34659E85B7768C (void);
// 0x0000031C System.Collections.IEnumerator WebSocketSharpUnityMod.Server.WebSocketSessionManager/<get_InactiveIDs>d__20::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_InactiveIDsU3Ed__20_System_Collections_IEnumerable_GetEnumerator_m83600AC75155D76E33FF2D0A1719D4F47512A563 (void);
// 0x0000031D System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m54840A31A193E591125ABF917E09F9CE87C963F5 (void);
// 0x0000031E System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<>c__DisplayClass33_0::<broadcastAsync>b__0(System.Object)
extern void U3CU3Ec__DisplayClass33_0_U3CbroadcastAsyncU3Eb__0_m6BB47473743B37FEA0B725680E8BB03E10B55299 (void);
// 0x0000031F System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m41EA5222E9446B595CB48FDBBA756B1A3FF722CD (void);
// 0x00000320 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<>c__DisplayClass34_0::<broadcastAsync>b__0(System.Object)
extern void U3CU3Ec__DisplayClass34_0_U3CbroadcastAsyncU3Eb__0_mECEB1D124FE4FDF4F5F965DD222B2711137D8B95 (void);
// 0x00000321 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<>c__DisplayClass49_0::.ctor()
extern void U3CU3Ec__DisplayClass49_0__ctor_mEF5C8C0CC5FDA327BD1448BFB3250E16F410EBDF (void);
// 0x00000322 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<>c__DisplayClass49_0::<BroadcastAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass49_0_U3CBroadcastAsyncU3Eb__0_m12DF611FB9D093F75F4E4448E2640872F2A2129A (void);
// 0x00000323 System.Void WebSocketSharpUnityMod.Server.WebSocketSessionManager/<>c__DisplayClass49_0::<BroadcastAsync>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass49_0_U3CBroadcastAsyncU3Eb__1_m26707B8214BF59609B776413331B25C3A326426E (void);
// 0x00000324 System.Void WebSocketSharpUnityMod.Net.AuthenticationBase::.ctor(WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationBase__ctor_mCC17DCF2330D0A07482C6959B0B5CAB149E2B54D (void);
// 0x00000325 System.String WebSocketSharpUnityMod.Net.AuthenticationBase::get_Algorithm()
extern void AuthenticationBase_get_Algorithm_mC6876C655FBB774C61D393D94BA1A169EE19D4E9 (void);
// 0x00000326 System.String WebSocketSharpUnityMod.Net.AuthenticationBase::get_Nonce()
extern void AuthenticationBase_get_Nonce_mF7FF68B003DF16E25E5C77A188FB455FD3C3F498 (void);
// 0x00000327 System.String WebSocketSharpUnityMod.Net.AuthenticationBase::get_Opaque()
extern void AuthenticationBase_get_Opaque_mC8AE16C9A9EB66EE4CD7F3228E79C1786B2B22C1 (void);
// 0x00000328 System.String WebSocketSharpUnityMod.Net.AuthenticationBase::get_Qop()
extern void AuthenticationBase_get_Qop_m3F5B34297DDAAA98F8E3043BC5FCB662B529BCFF (void);
// 0x00000329 System.String WebSocketSharpUnityMod.Net.AuthenticationBase::get_Realm()
extern void AuthenticationBase_get_Realm_mD7829F7E6DC2C4DFF645CB521AB90D3734A1FEE5 (void);
// 0x0000032A WebSocketSharpUnityMod.Net.AuthenticationSchemes WebSocketSharpUnityMod.Net.AuthenticationBase::get_Scheme()
extern void AuthenticationBase_get_Scheme_m1689D3F10B62EBA0BAE138C1C1188DEFE114E782 (void);
// 0x0000032B System.String WebSocketSharpUnityMod.Net.AuthenticationBase::CreateNonceValue()
extern void AuthenticationBase_CreateNonceValue_m4BC9D67561C4180035764A5CBE8F3D19C54465DF (void);
// 0x0000032C System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.AuthenticationBase::ParseParameters(System.String)
extern void AuthenticationBase_ParseParameters_m79398071F670AD3CA383FF817C9BA2CD5E620BA5 (void);
// 0x0000032D System.String WebSocketSharpUnityMod.Net.AuthenticationBase::ToBasicString()
// 0x0000032E System.String WebSocketSharpUnityMod.Net.AuthenticationBase::ToDigestString()
// 0x0000032F System.String WebSocketSharpUnityMod.Net.AuthenticationBase::ToString()
extern void AuthenticationBase_ToString_m0958A662EFBEC1EFFA1822FE2FCD546BFD69E502 (void);
// 0x00000330 System.Void WebSocketSharpUnityMod.Net.AuthenticationChallenge::.ctor(WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationChallenge__ctor_m7C1DC26E8D61978A9E6E636406DE698F236D0CB1 (void);
// 0x00000331 System.Void WebSocketSharpUnityMod.Net.AuthenticationChallenge::.ctor(WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.String)
extern void AuthenticationChallenge__ctor_m564A64DE7723B251A44CD2D711093FFC7337BA79 (void);
// 0x00000332 System.String WebSocketSharpUnityMod.Net.AuthenticationChallenge::get_Domain()
extern void AuthenticationChallenge_get_Domain_mF3577716CD18FF5367EAEC53B84A58A4B61A066A (void);
// 0x00000333 System.String WebSocketSharpUnityMod.Net.AuthenticationChallenge::get_Stale()
extern void AuthenticationChallenge_get_Stale_m39B62C3738FFAC4E8B96FC25CD543F8EE56482F5 (void);
// 0x00000334 WebSocketSharpUnityMod.Net.AuthenticationChallenge WebSocketSharpUnityMod.Net.AuthenticationChallenge::CreateBasicChallenge(System.String)
extern void AuthenticationChallenge_CreateBasicChallenge_mE266121701AA39B52DF9FA608929148F49993ED2 (void);
// 0x00000335 WebSocketSharpUnityMod.Net.AuthenticationChallenge WebSocketSharpUnityMod.Net.AuthenticationChallenge::CreateDigestChallenge(System.String)
extern void AuthenticationChallenge_CreateDigestChallenge_mD6EA07881965C6EDA6744B25C861F32B33882712 (void);
// 0x00000336 WebSocketSharpUnityMod.Net.AuthenticationChallenge WebSocketSharpUnityMod.Net.AuthenticationChallenge::Parse(System.String)
extern void AuthenticationChallenge_Parse_mD71FF96E69FE4EBEADBECDB58663273D62F28C04 (void);
// 0x00000337 System.String WebSocketSharpUnityMod.Net.AuthenticationChallenge::ToBasicString()
extern void AuthenticationChallenge_ToBasicString_m557BC02F8000F216197E8A552BE1864CB948E58F (void);
// 0x00000338 System.String WebSocketSharpUnityMod.Net.AuthenticationChallenge::ToDigestString()
extern void AuthenticationChallenge_ToDigestString_m9EE9ECB4CDAEC4A40D3888D6A8C76ABEED38E514 (void);
// 0x00000339 System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse::.ctor(WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationResponse__ctor_m2AC63DBCEC3D747348AC25D029A7A8A538175A8D (void);
// 0x0000033A System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse::.ctor(WebSocketSharpUnityMod.Net.NetworkCredential)
extern void AuthenticationResponse__ctor_m501C0D5DC897D230EDE4396AA2975527CC015C86 (void);
// 0x0000033B System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse::.ctor(WebSocketSharpUnityMod.Net.AuthenticationChallenge,WebSocketSharpUnityMod.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_mACB2B78FFC1DB43ABD7C8521B0E91233717DF795 (void);
// 0x0000033C System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse::.ctor(WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection,WebSocketSharpUnityMod.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_m1C7B470FD21D62980ECCA906FB380248C815E86E (void);
// 0x0000033D System.UInt32 WebSocketSharpUnityMod.Net.AuthenticationResponse::get_NonceCount()
extern void AuthenticationResponse_get_NonceCount_mCC8C6768527C73648C29B205409CD358DB4ECF78 (void);
// 0x0000033E System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::get_Cnonce()
extern void AuthenticationResponse_get_Cnonce_mF03A011E3D7572E5EFE20684992A7B17A1606CE5 (void);
// 0x0000033F System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::get_Nc()
extern void AuthenticationResponse_get_Nc_mD28446249D6D0EBC989AD31C6A15F100D2D9BCC8 (void);
// 0x00000340 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::get_Password()
extern void AuthenticationResponse_get_Password_m19676AA2C70F57FEF8082DC4D8878D959F6076C1 (void);
// 0x00000341 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::get_Response()
extern void AuthenticationResponse_get_Response_mB3907DD6F7C1BE8CFFA09E8900CFC3DDB6EA0BDB (void);
// 0x00000342 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::get_Uri()
extern void AuthenticationResponse_get_Uri_mC8A054282B7C9276D561277CBC3F6970D1D6B25A (void);
// 0x00000343 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::get_UserName()
extern void AuthenticationResponse_get_UserName_mA7AE765B25EA04C67784A830498C78F8F03ECFE4 (void);
// 0x00000344 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::createA1(System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_mAF222A0FF8126876F6CEEC0C9EC6CA25EB111815 (void);
// 0x00000345 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::createA1(System.String,System.String,System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_m60A8831FAB8EDC6EB8C4478EA9E539F414FEBA3D (void);
// 0x00000346 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::createA2(System.String,System.String)
extern void AuthenticationResponse_createA2_m1398FC52F3A58552A1ABF51D22708F5FAF350A3D (void);
// 0x00000347 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::createA2(System.String,System.String,System.String)
extern void AuthenticationResponse_createA2_mBC9AD136B11EC200901BA25F73597E3CE7BFCCE8 (void);
// 0x00000348 System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::hash(System.String)
extern void AuthenticationResponse_hash_m4CA59ECFF76BD3CE112060D503A1193361FCA0AB (void);
// 0x00000349 System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse::initAsDigest()
extern void AuthenticationResponse_initAsDigest_m20782FE5DCE735D38C5B0670A8A5A49850FE102E (void);
// 0x0000034A System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::CreateRequestDigest(System.Collections.Specialized.NameValueCollection)
extern void AuthenticationResponse_CreateRequestDigest_mDD1E74E90D9D84D39D3D1025E5F5F610D185C4E8 (void);
// 0x0000034B WebSocketSharpUnityMod.Net.AuthenticationResponse WebSocketSharpUnityMod.Net.AuthenticationResponse::Parse(System.String)
extern void AuthenticationResponse_Parse_mF8C0525BC1ADCF128EA6475AC857078A8EB10747 (void);
// 0x0000034C System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.AuthenticationResponse::ParseBasicCredentials(System.String)
extern void AuthenticationResponse_ParseBasicCredentials_m59A623E08482745A5A31B16F3F874C86314551AF (void);
// 0x0000034D System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::ToBasicString()
extern void AuthenticationResponse_ToBasicString_m58519C18C73E0F0E75350E8BF395FB28FA5798CA (void);
// 0x0000034E System.String WebSocketSharpUnityMod.Net.AuthenticationResponse::ToDigestString()
extern void AuthenticationResponse_ToDigestString_m4F6A53EB9A440DDA02CC17408989D8276B5AC9AC (void);
// 0x0000034F System.Security.Principal.IIdentity WebSocketSharpUnityMod.Net.AuthenticationResponse::ToIdentity()
extern void AuthenticationResponse_ToIdentity_m25C1885E53C91AC6025105F431EF0AF08B3F517F (void);
// 0x00000350 System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse/<>c::.cctor()
extern void U3CU3Ec__cctor_mCF27718999A80EF862E4204DF166E44EF8FE89DE (void);
// 0x00000351 System.Void WebSocketSharpUnityMod.Net.AuthenticationResponse/<>c::.ctor()
extern void U3CU3Ec__ctor_m5A149873B490D222B5F0A03301C191B7E0540948 (void);
// 0x00000352 System.Boolean WebSocketSharpUnityMod.Net.AuthenticationResponse/<>c::<initAsDigest>b__24_0(System.String)
extern void U3CU3Ec_U3CinitAsDigestU3Eb__24_0_m1515862841DFEF5E20DD628F148C14C157B2BE2B (void);
// 0x00000353 System.Void WebSocketSharpUnityMod.Net.Chunk::.ctor(System.Byte[])
extern void Chunk__ctor_m35FA5CB3798FADA79257B58B52A6C5A4A12D4CC1 (void);
// 0x00000354 System.Int32 WebSocketSharpUnityMod.Net.Chunk::get_ReadLeft()
extern void Chunk_get_ReadLeft_m2C267307045C0ADB83C2FDD6AAD639DE6B8A44D6 (void);
// 0x00000355 System.Int32 WebSocketSharpUnityMod.Net.Chunk::Read(System.Byte[],System.Int32,System.Int32)
extern void Chunk_Read_m9C93FE5623A3C89EBC16C4DEE83BDC7C349AE387 (void);
// 0x00000356 System.Void WebSocketSharpUnityMod.Net.ChunkedRequestStream::.ctor(System.IO.Stream,System.Byte[],System.Int32,System.Int32,WebSocketSharpUnityMod.Net.HttpListenerContext)
extern void ChunkedRequestStream__ctor_mE6417B53C4E36CDFDC4CB1EA6A5C24E1FEF26FCD (void);
// 0x00000357 WebSocketSharpUnityMod.Net.ChunkStream WebSocketSharpUnityMod.Net.ChunkedRequestStream::get_Decoder()
extern void ChunkedRequestStream_get_Decoder_mAA16D7CDB260D3BC79FDCBD341A6C596F073A459 (void);
// 0x00000358 System.Void WebSocketSharpUnityMod.Net.ChunkedRequestStream::set_Decoder(WebSocketSharpUnityMod.Net.ChunkStream)
extern void ChunkedRequestStream_set_Decoder_m411B29C7AEF3524E480788AFF9A378384CAC3232 (void);
// 0x00000359 System.Void WebSocketSharpUnityMod.Net.ChunkedRequestStream::onRead(System.IAsyncResult)
extern void ChunkedRequestStream_onRead_mB5D6EB7178A5E77259B5CDC239FE370D2A001F08 (void);
// 0x0000035A System.IAsyncResult WebSocketSharpUnityMod.Net.ChunkedRequestStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void ChunkedRequestStream_BeginRead_m141FF45E6EB09B4A6F6768D165764340F367DD51 (void);
// 0x0000035B System.Void WebSocketSharpUnityMod.Net.ChunkedRequestStream::Close()
extern void ChunkedRequestStream_Close_m11F7D14EE4BBCDF74FEE0B95713817AE5D0ACB5C (void);
// 0x0000035C System.Int32 WebSocketSharpUnityMod.Net.ChunkedRequestStream::EndRead(System.IAsyncResult)
extern void ChunkedRequestStream_EndRead_m0B9E1ABCEE3145FFC13072AA109A2D11659667C9 (void);
// 0x0000035D System.Int32 WebSocketSharpUnityMod.Net.ChunkedRequestStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ChunkedRequestStream_Read_mD305267D55969EF68AF29F354C0F374C01A81FF4 (void);
// 0x0000035E System.Void WebSocketSharpUnityMod.Net.ChunkStream::.ctor(WebSocketSharpUnityMod.Net.WebHeaderCollection)
extern void ChunkStream__ctor_m03E7DDB3013B3A9B0B1ADD7A809A763D5357BD21 (void);
// 0x0000035F System.Void WebSocketSharpUnityMod.Net.ChunkStream::.ctor(System.Byte[],System.Int32,System.Int32,WebSocketSharpUnityMod.Net.WebHeaderCollection)
extern void ChunkStream__ctor_m5C508A4E45A2D4C82A78AEE87949118FB178444C (void);
// 0x00000360 WebSocketSharpUnityMod.Net.WebHeaderCollection WebSocketSharpUnityMod.Net.ChunkStream::get_Headers()
extern void ChunkStream_get_Headers_mA71B2DF40AD18045FC19F11468F7A1D6790FE646 (void);
// 0x00000361 System.Int32 WebSocketSharpUnityMod.Net.ChunkStream::get_ChunkLeft()
extern void ChunkStream_get_ChunkLeft_m2DA695C8961ABEBC648BB79F7BEFB1A434F8B584 (void);
// 0x00000362 System.Boolean WebSocketSharpUnityMod.Net.ChunkStream::get_WantMore()
extern void ChunkStream_get_WantMore_mA4CE15B3605E88562976B86760DDADEAE434B4DF (void);
// 0x00000363 System.Int32 WebSocketSharpUnityMod.Net.ChunkStream::read(System.Byte[],System.Int32,System.Int32)
extern void ChunkStream_read_mBE104F4E81474AE5483D35733BAF77DB83FDBDDE (void);
// 0x00000364 System.String WebSocketSharpUnityMod.Net.ChunkStream::removeChunkExtension(System.String)
extern void ChunkStream_removeChunkExtension_mA8897A2F15DA590D249D84DB782A3F33A1694AAD (void);
// 0x00000365 WebSocketSharpUnityMod.Net.InputChunkState WebSocketSharpUnityMod.Net.ChunkStream::seekCrLf(System.Byte[],System.Int32&,System.Int32)
extern void ChunkStream_seekCrLf_m8EF4A84701657FD6696FE7094B953277069785F6 (void);
// 0x00000366 WebSocketSharpUnityMod.Net.InputChunkState WebSocketSharpUnityMod.Net.ChunkStream::setChunkSize(System.Byte[],System.Int32&,System.Int32)
extern void ChunkStream_setChunkSize_mA7AC39ADF9EB9C6274BF359907E3B7236A835EFC (void);
// 0x00000367 WebSocketSharpUnityMod.Net.InputChunkState WebSocketSharpUnityMod.Net.ChunkStream::setTrailer(System.Byte[],System.Int32&,System.Int32)
extern void ChunkStream_setTrailer_m706ACF6F3CCE96D7DF4BE1040114E9C4A9E11DEE (void);
// 0x00000368 System.Void WebSocketSharpUnityMod.Net.ChunkStream::throwProtocolViolation(System.String)
extern void ChunkStream_throwProtocolViolation_m4A3CED4BE98FDBF32BB434F8055993AA55D192DD (void);
// 0x00000369 System.Void WebSocketSharpUnityMod.Net.ChunkStream::write(System.Byte[],System.Int32&,System.Int32)
extern void ChunkStream_write_mB02D418DFD86CA164D999C5B3DF338303310EF0C (void);
// 0x0000036A WebSocketSharpUnityMod.Net.InputChunkState WebSocketSharpUnityMod.Net.ChunkStream::writeData(System.Byte[],System.Int32&,System.Int32)
extern void ChunkStream_writeData_m0D7D11CE44AC61F11B488EAA13217217BDFFB0F6 (void);
// 0x0000036B System.Void WebSocketSharpUnityMod.Net.ChunkStream::ResetBuffer()
extern void ChunkStream_ResetBuffer_m872822F34770962F506A86C60ED7A82D3C93BD11 (void);
// 0x0000036C System.Int32 WebSocketSharpUnityMod.Net.ChunkStream::WriteAndReadBack(System.Byte[],System.Int32,System.Int32,System.Int32)
extern void ChunkStream_WriteAndReadBack_m9C398A99686597D051430229062F7C05981AD88E (void);
// 0x0000036D System.Int32 WebSocketSharpUnityMod.Net.ChunkStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ChunkStream_Read_mD62D112650E2A44A983446E2568FD193F99C99B0 (void);
// 0x0000036E System.Void WebSocketSharpUnityMod.Net.ChunkStream::Write(System.Byte[],System.Int32,System.Int32)
extern void ChunkStream_Write_mC05F797E6726BABBF283006D246BA4A3A10D2C39 (void);
// 0x0000036F System.Void WebSocketSharpUnityMod.Net.ClientSslConfiguration::.ctor(System.String)
extern void ClientSslConfiguration__ctor_m0DF6D3AC193EA0CF8012D62CF8B63C3827F9EFD1 (void);
// 0x00000370 System.Void WebSocketSharpUnityMod.Net.ClientSslConfiguration::.ctor(System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Authentication.SslProtocols,System.Boolean)
extern void ClientSslConfiguration__ctor_mD020D7F29B628FCCD6EC8EDAD0C6DCD555D34DDA (void);
// 0x00000371 System.Security.Cryptography.X509Certificates.X509CertificateCollection WebSocketSharpUnityMod.Net.ClientSslConfiguration::get_ClientCertificates()
extern void ClientSslConfiguration_get_ClientCertificates_m71DD6B7FA341AF7CB50E872774BE1173407150C6 (void);
// 0x00000372 System.Void WebSocketSharpUnityMod.Net.ClientSslConfiguration::set_ClientCertificates(System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern void ClientSslConfiguration_set_ClientCertificates_m1753A54F9D70581B422DB50D588D2626019DCC98 (void);
// 0x00000373 System.Net.Security.LocalCertificateSelectionCallback WebSocketSharpUnityMod.Net.ClientSslConfiguration::get_ClientCertificateSelectionCallback()
extern void ClientSslConfiguration_get_ClientCertificateSelectionCallback_m9896F1C9540AD42CC546FE25D4AC32EB3842C4F8 (void);
// 0x00000374 System.Void WebSocketSharpUnityMod.Net.ClientSslConfiguration::set_ClientCertificateSelectionCallback(System.Net.Security.LocalCertificateSelectionCallback)
extern void ClientSslConfiguration_set_ClientCertificateSelectionCallback_m16B455B03377ED342D31131B47C91037E7939129 (void);
// 0x00000375 System.Net.Security.RemoteCertificateValidationCallback WebSocketSharpUnityMod.Net.ClientSslConfiguration::get_ServerCertificateValidationCallback()
extern void ClientSslConfiguration_get_ServerCertificateValidationCallback_m4178571D83653E57C436F81EDC79D51BC003EF35 (void);
// 0x00000376 System.Void WebSocketSharpUnityMod.Net.ClientSslConfiguration::set_ServerCertificateValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern void ClientSslConfiguration_set_ServerCertificateValidationCallback_m529973888D6BF43CC449D92A931B8F1AB5983DDC (void);
// 0x00000377 System.String WebSocketSharpUnityMod.Net.ClientSslConfiguration::get_TargetHost()
extern void ClientSslConfiguration_get_TargetHost_m0B2B6381849B532E145F2313CF0B76BCB2F93447 (void);
// 0x00000378 System.Void WebSocketSharpUnityMod.Net.ClientSslConfiguration::set_TargetHost(System.String)
extern void ClientSslConfiguration_set_TargetHost_mA1077E861D29F26D2F77E694B917A9518EB9BE64 (void);
// 0x00000379 System.Void WebSocketSharpUnityMod.Net.Cookie::.cctor()
extern void Cookie__cctor_m4F91054F0A01EFA60B05372A0A7A47C8C96C4921 (void);
// 0x0000037A System.Void WebSocketSharpUnityMod.Net.Cookie::.ctor()
extern void Cookie__ctor_m53ADB863562A530F6A84258F7F4F877F2C14FF36 (void);
// 0x0000037B System.Void WebSocketSharpUnityMod.Net.Cookie::.ctor(System.String,System.String)
extern void Cookie__ctor_m0DFAE89D7F1A7B1093556AB2C7A8564ED7DB625F (void);
// 0x0000037C System.Void WebSocketSharpUnityMod.Net.Cookie::.ctor(System.String,System.String,System.String)
extern void Cookie__ctor_m40E586F5352605E220E4E056A2F8CC8C407CBFB9 (void);
// 0x0000037D System.Void WebSocketSharpUnityMod.Net.Cookie::.ctor(System.String,System.String,System.String,System.String)
extern void Cookie__ctor_m33B6B507421A6DBB25769E15B3C29EFED9EB678B (void);
// 0x0000037E System.Boolean WebSocketSharpUnityMod.Net.Cookie::get_ExactDomain()
extern void Cookie_get_ExactDomain_mA4F885A2C0629E82311646A09C51CC963276C437 (void);
// 0x0000037F System.Void WebSocketSharpUnityMod.Net.Cookie::set_ExactDomain(System.Boolean)
extern void Cookie_set_ExactDomain_mAE3A5C261C6A716EA70FA5DD6C84AEECF8092141 (void);
// 0x00000380 System.Int32 WebSocketSharpUnityMod.Net.Cookie::get_MaxAge()
extern void Cookie_get_MaxAge_m329D54507884FD8A7EE855AB2ED6FA867A819B89 (void);
// 0x00000381 System.Int32[] WebSocketSharpUnityMod.Net.Cookie::get_Ports()
extern void Cookie_get_Ports_m49697E38253F1F3ABBC6AAA626CFE22601BA6207 (void);
// 0x00000382 System.String WebSocketSharpUnityMod.Net.Cookie::get_Comment()
extern void Cookie_get_Comment_m7902891140C2DD0270E1D7E855C1FEDD4592E30E (void);
// 0x00000383 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Comment(System.String)
extern void Cookie_set_Comment_mD30377A67C3476C947418FBFC579A0C62DD73E8B (void);
// 0x00000384 System.Uri WebSocketSharpUnityMod.Net.Cookie::get_CommentUri()
extern void Cookie_get_CommentUri_m2B7A3B6AF9E30CA372DA7D02F75A1EE2DAD7C19F (void);
// 0x00000385 System.Void WebSocketSharpUnityMod.Net.Cookie::set_CommentUri(System.Uri)
extern void Cookie_set_CommentUri_mB2BDEAFFFC642D4FD801F768C494C21AE47B3408 (void);
// 0x00000386 System.Boolean WebSocketSharpUnityMod.Net.Cookie::get_Discard()
extern void Cookie_get_Discard_m9A9C0FCE9A631CCF3584C80B0B46E0E50DD6C572 (void);
// 0x00000387 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Discard(System.Boolean)
extern void Cookie_set_Discard_mF288E7F150DB28E8481CF4FA414BC84C8A992E3B (void);
// 0x00000388 System.String WebSocketSharpUnityMod.Net.Cookie::get_Domain()
extern void Cookie_get_Domain_mF31006B082D041079E94388A896D1688BA4C8DAE (void);
// 0x00000389 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Domain(System.String)
extern void Cookie_set_Domain_mF83D794807D2DF2BD393E46F2307A47CC636E4EC (void);
// 0x0000038A System.Boolean WebSocketSharpUnityMod.Net.Cookie::get_Expired()
extern void Cookie_get_Expired_mBEA6B34053183D841EC0ED8C22695F73E847DA7F (void);
// 0x0000038B System.Void WebSocketSharpUnityMod.Net.Cookie::set_Expired(System.Boolean)
extern void Cookie_set_Expired_m267F7D9C458207E3BFE8DB7E3D894E596AA9FFF5 (void);
// 0x0000038C System.DateTime WebSocketSharpUnityMod.Net.Cookie::get_Expires()
extern void Cookie_get_Expires_m3A3F6A4C9FA2E691C9DAFCD74A4FA8AFD81DA188 (void);
// 0x0000038D System.Void WebSocketSharpUnityMod.Net.Cookie::set_Expires(System.DateTime)
extern void Cookie_set_Expires_m319B977C71E51F829476593D1D3A8FDF0164B3D4 (void);
// 0x0000038E System.Boolean WebSocketSharpUnityMod.Net.Cookie::get_HttpOnly()
extern void Cookie_get_HttpOnly_m732D27AFCF2C823565DB232F0634004A1418174F (void);
// 0x0000038F System.Void WebSocketSharpUnityMod.Net.Cookie::set_HttpOnly(System.Boolean)
extern void Cookie_set_HttpOnly_mD780A033DF95092D6EA798F8B8615B25E1419B39 (void);
// 0x00000390 System.String WebSocketSharpUnityMod.Net.Cookie::get_Name()
extern void Cookie_get_Name_mB43D79B55813FEC7888511E224CA9418A0C3E91F (void);
// 0x00000391 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Name(System.String)
extern void Cookie_set_Name_mF6582E5F1199D35B1FC8811133EBB7B1C3C892BD (void);
// 0x00000392 System.String WebSocketSharpUnityMod.Net.Cookie::get_Path()
extern void Cookie_get_Path_m756D99D6F41FA053C2D3CEAA2A3B32B99D241CF2 (void);
// 0x00000393 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Path(System.String)
extern void Cookie_set_Path_m5CE1F08D38DBD8A42820F20C986E8328C94E1EB4 (void);
// 0x00000394 System.String WebSocketSharpUnityMod.Net.Cookie::get_Port()
extern void Cookie_get_Port_mA8D05E5A09642C0AC56FA2ED74EE0D61FC7D9DD4 (void);
// 0x00000395 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Port(System.String)
extern void Cookie_set_Port_mA49EEA0CEB0E5C4ECD8DF5D7DA3F1D14B8E1B63E (void);
// 0x00000396 System.Boolean WebSocketSharpUnityMod.Net.Cookie::get_Secure()
extern void Cookie_get_Secure_m0524EEBBA3FD8ED25FBC560CC9220649211D45F4 (void);
// 0x00000397 System.Void WebSocketSharpUnityMod.Net.Cookie::set_Secure(System.Boolean)
extern void Cookie_set_Secure_mA858981CC5FE105932D6FCFECEF7977868B666C6 (void);
// 0x00000398 System.DateTime WebSocketSharpUnityMod.Net.Cookie::get_TimeStamp()
extern void Cookie_get_TimeStamp_m6766D0222C55690923C3730B277CD8D5DAA5E1F8 (void);
// 0x00000399 System.String WebSocketSharpUnityMod.Net.Cookie::get_Value()
extern void Cookie_get_Value_m9AC44F88B1FABDE5B96269DFD99AEECC9D7283AD (void);
// 0x0000039A System.Void WebSocketSharpUnityMod.Net.Cookie::set_Value(System.String)
extern void Cookie_set_Value_m817C5C9B6138CB096EF3A8557A7C576F12C48D09 (void);
// 0x0000039B System.Int32 WebSocketSharpUnityMod.Net.Cookie::get_Version()
extern void Cookie_get_Version_mFA4DCD7A61B6F69FF503221F6F3546F7D302A227 (void);
// 0x0000039C System.Void WebSocketSharpUnityMod.Net.Cookie::set_Version(System.Int32)
extern void Cookie_set_Version_mBBAE5EDB9EF3D2BB8F8A404678CC514997AFCF8D (void);
// 0x0000039D System.Boolean WebSocketSharpUnityMod.Net.Cookie::canSetName(System.String,System.String&)
extern void Cookie_canSetName_mF13051F50831B0E42DD1F21C03B42A1089FE2DA9 (void);
// 0x0000039E System.Boolean WebSocketSharpUnityMod.Net.Cookie::canSetValue(System.String,System.String&)
extern void Cookie_canSetValue_m14150EC3F22B886CF2E174C7B676F626F8DCC82A (void);
// 0x0000039F System.Int32 WebSocketSharpUnityMod.Net.Cookie::hash(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Cookie_hash_mBA8EF104BE7E1B7BC93C3BF5B48333D8E9C4E688 (void);
// 0x000003A0 System.String WebSocketSharpUnityMod.Net.Cookie::toResponseStringVersion0()
extern void Cookie_toResponseStringVersion0_m48EA8C7B5FD4869C26A6C1CB9239FCCEEFE614E6 (void);
// 0x000003A1 System.String WebSocketSharpUnityMod.Net.Cookie::toResponseStringVersion1()
extern void Cookie_toResponseStringVersion1_m777890D5AC07791822B4CBD9FBD018F6ADC35534 (void);
// 0x000003A2 System.Boolean WebSocketSharpUnityMod.Net.Cookie::tryCreatePorts(System.String,System.Int32[]&,System.String&)
extern void Cookie_tryCreatePorts_m5113CF0988929389158E5D1CB8CDA3FBC391E5A0 (void);
// 0x000003A3 System.String WebSocketSharpUnityMod.Net.Cookie::ToRequestString(System.Uri)
extern void Cookie_ToRequestString_m388BA2E520C312D46118FABF62F439A455B5C0ED (void);
// 0x000003A4 System.String WebSocketSharpUnityMod.Net.Cookie::ToResponseString()
extern void Cookie_ToResponseString_m824C988C4F4EAA7636749AA6E723BC8F40D4C733 (void);
// 0x000003A5 System.Boolean WebSocketSharpUnityMod.Net.Cookie::Equals(System.Object)
extern void Cookie_Equals_mB22B6B4C0C13FE3AB01846FE98E095D32B7D7BE2 (void);
// 0x000003A6 System.Int32 WebSocketSharpUnityMod.Net.Cookie::GetHashCode()
extern void Cookie_GetHashCode_m7B2995EA1A3FEEC428B28E5A7FAB65DAC687262E (void);
// 0x000003A7 System.String WebSocketSharpUnityMod.Net.Cookie::ToString()
extern void Cookie_ToString_m7CA62A33F15252D8A44D3ECE7A1BABA3408704DA (void);
// 0x000003A8 System.Void WebSocketSharpUnityMod.Net.CookieCollection::.ctor()
extern void CookieCollection__ctor_m14A8C1360F2CE04E4BE5CBEF9AF2336586100BB6 (void);
// 0x000003A9 System.Collections.Generic.IList`1<WebSocketSharpUnityMod.Net.Cookie> WebSocketSharpUnityMod.Net.CookieCollection::get_List()
extern void CookieCollection_get_List_m785F26EF8F90881453923AE4BBBA025E20AB9095 (void);
// 0x000003AA System.Collections.Generic.IEnumerable`1<WebSocketSharpUnityMod.Net.Cookie> WebSocketSharpUnityMod.Net.CookieCollection::get_Sorted()
extern void CookieCollection_get_Sorted_m2E360A96D56EFBAA642C5CA7806CB28242D4193C (void);
// 0x000003AB System.Int32 WebSocketSharpUnityMod.Net.CookieCollection::get_Count()
extern void CookieCollection_get_Count_m25F4295075CF5F29B7C31588506037809637D148 (void);
// 0x000003AC System.Boolean WebSocketSharpUnityMod.Net.CookieCollection::get_IsReadOnly()
extern void CookieCollection_get_IsReadOnly_m3BF8D280E436F43227A33A36E6C0B75CD5FCC644 (void);
// 0x000003AD System.Boolean WebSocketSharpUnityMod.Net.CookieCollection::get_IsSynchronized()
extern void CookieCollection_get_IsSynchronized_m6B08A651906E17BE2962D952916CF9A20E38FCB4 (void);
// 0x000003AE WebSocketSharpUnityMod.Net.Cookie WebSocketSharpUnityMod.Net.CookieCollection::get_Item(System.Int32)
extern void CookieCollection_get_Item_m3FB28B94FB8ADE10502FE4B25052A02E32023209 (void);
// 0x000003AF WebSocketSharpUnityMod.Net.Cookie WebSocketSharpUnityMod.Net.CookieCollection::get_Item(System.String)
extern void CookieCollection_get_Item_m4FC55C7137165506659F20D2071E61CE4FA1C7DC (void);
// 0x000003B0 System.Object WebSocketSharpUnityMod.Net.CookieCollection::get_SyncRoot()
extern void CookieCollection_get_SyncRoot_m02157C22EE5F15273DDC8886863276F588131F44 (void);
// 0x000003B1 System.Int32 WebSocketSharpUnityMod.Net.CookieCollection::compareCookieWithinSort(WebSocketSharpUnityMod.Net.Cookie,WebSocketSharpUnityMod.Net.Cookie)
extern void CookieCollection_compareCookieWithinSort_m54CE61F46A6B299F07EAD6A7E0D3A19651EA7723 (void);
// 0x000003B2 System.Int32 WebSocketSharpUnityMod.Net.CookieCollection::compareCookieWithinSorted(WebSocketSharpUnityMod.Net.Cookie,WebSocketSharpUnityMod.Net.Cookie)
extern void CookieCollection_compareCookieWithinSorted_mFE7616F7435C3E6E9E46FDA25E0AF06DD08F9695 (void);
// 0x000003B3 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.CookieCollection::parseRequest(System.String)
extern void CookieCollection_parseRequest_m2B2E4A0F22D9B91C1D9737CF2A57983D32BA9A09 (void);
// 0x000003B4 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.CookieCollection::parseResponse(System.String)
extern void CookieCollection_parseResponse_mAB3AE65D43C18B6D489ED605F79C404D5F850D3E (void);
// 0x000003B5 System.Int32 WebSocketSharpUnityMod.Net.CookieCollection::searchCookie(WebSocketSharpUnityMod.Net.Cookie)
extern void CookieCollection_searchCookie_m2DE7141B639697E13CA16B9443C39E5EECBF728A (void);
// 0x000003B6 System.String[] WebSocketSharpUnityMod.Net.CookieCollection::splitCookieHeaderValue(System.String)
extern void CookieCollection_splitCookieHeaderValue_m0F177D416CFE4A6942D4CE50ED7D464D02CC0CD2 (void);
// 0x000003B7 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.CookieCollection::Parse(System.String,System.Boolean)
extern void CookieCollection_Parse_m1DA38A2F8517E947075414A81EFA6CE979862EA1 (void);
// 0x000003B8 System.Void WebSocketSharpUnityMod.Net.CookieCollection::SetOrRemove(WebSocketSharpUnityMod.Net.Cookie)
extern void CookieCollection_SetOrRemove_mCD57F106953634BC3DD4B0D24B680E1754955701 (void);
// 0x000003B9 System.Void WebSocketSharpUnityMod.Net.CookieCollection::SetOrRemove(WebSocketSharpUnityMod.Net.CookieCollection)
extern void CookieCollection_SetOrRemove_m208060BAB8FD1D301BB8D2813C22D382232E129F (void);
// 0x000003BA System.Void WebSocketSharpUnityMod.Net.CookieCollection::Sort()
extern void CookieCollection_Sort_m6BFA3943F67D709CE8E85F3E0A6CDD62CB98AA46 (void);
// 0x000003BB System.Void WebSocketSharpUnityMod.Net.CookieCollection::Add(WebSocketSharpUnityMod.Net.Cookie)
extern void CookieCollection_Add_mC83C8B507817EA1F9362AB5C86B61C9D027E015F (void);
// 0x000003BC System.Void WebSocketSharpUnityMod.Net.CookieCollection::Add(WebSocketSharpUnityMod.Net.CookieCollection)
extern void CookieCollection_Add_m6F3B890A2148C8760BB6F943D304E76F49A86665 (void);
// 0x000003BD System.Void WebSocketSharpUnityMod.Net.CookieCollection::CopyTo(System.Array,System.Int32)
extern void CookieCollection_CopyTo_m450870DA9111239A9C767A05A6EE3DF69D8A5D01 (void);
// 0x000003BE System.Void WebSocketSharpUnityMod.Net.CookieCollection::CopyTo(WebSocketSharpUnityMod.Net.Cookie[],System.Int32)
extern void CookieCollection_CopyTo_m58B0FDCD4EE74CA863B6A3B61EF13F7A2A57D6C0 (void);
// 0x000003BF System.Collections.IEnumerator WebSocketSharpUnityMod.Net.CookieCollection::GetEnumerator()
extern void CookieCollection_GetEnumerator_m63D38C707641C1D36CFC55B53DF0BA2D1F9768AD (void);
// 0x000003C0 System.Void WebSocketSharpUnityMod.Net.CookieException::.ctor(System.String)
extern void CookieException__ctor_mD5782CCF8E18D015592CB0F684A78FC450739578 (void);
// 0x000003C1 System.Void WebSocketSharpUnityMod.Net.CookieException::.ctor(System.String,System.Exception)
extern void CookieException__ctor_mF5EBA66B5D5C3AE99B3F48A72C98BEB35C0BBBD3 (void);
// 0x000003C2 System.Void WebSocketSharpUnityMod.Net.CookieException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException__ctor_mDCF9C1D8149AF9A7B93E3F4900DD979C89B14E55 (void);
// 0x000003C3 System.Void WebSocketSharpUnityMod.Net.CookieException::.ctor()
extern void CookieException__ctor_mACA6A3117DA4F3989526B87F82A40E17579AC747 (void);
// 0x000003C4 System.Void WebSocketSharpUnityMod.Net.CookieException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_GetObjectData_mAE891B42C0A92043294B2D4DDE06FFB904FB6B85 (void);
// 0x000003C5 System.Void WebSocketSharpUnityMod.Net.CookieException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m94A34D680E17B10C3EBC5ADBDE084DF611739C10 (void);
// 0x000003C6 System.Void WebSocketSharpUnityMod.Net.EndPointListener::.cctor()
extern void EndPointListener__cctor_mFF10D80D62E34DF360C83BD58023E3DA8F39695F (void);
// 0x000003C7 System.Void WebSocketSharpUnityMod.Net.EndPointListener::.ctor(System.Net.IPAddress,System.Int32,System.Boolean,System.Boolean,System.String,WebSocketSharpUnityMod.Net.ServerSslConfiguration)
extern void EndPointListener__ctor_mB4002CF8590264D2C7721CEEE597F818D09350B1 (void);
// 0x000003C8 System.Net.IPAddress WebSocketSharpUnityMod.Net.EndPointListener::get_Address()
extern void EndPointListener_get_Address_m4C5ED63522C1688DFA7D68188CA4E142522A98EC (void);
// 0x000003C9 System.Boolean WebSocketSharpUnityMod.Net.EndPointListener::get_IsSecure()
extern void EndPointListener_get_IsSecure_m66881F91519548FCD53A32A84D5B1A24CFE4E08E (void);
// 0x000003CA System.Int32 WebSocketSharpUnityMod.Net.EndPointListener::get_Port()
extern void EndPointListener_get_Port_m0C8605A6DE8D43DA3F020F80451F3257ACFB9C74 (void);
// 0x000003CB WebSocketSharpUnityMod.Net.ServerSslConfiguration WebSocketSharpUnityMod.Net.EndPointListener::get_SslConfiguration()
extern void EndPointListener_get_SslConfiguration_mA91A2BCD28E09C058DFA9EFF091F3A71648B509B (void);
// 0x000003CC System.Void WebSocketSharpUnityMod.Net.EndPointListener::addSpecial(System.Collections.Generic.List`1<WebSocketSharpUnityMod.Net.HttpListenerPrefix>,WebSocketSharpUnityMod.Net.HttpListenerPrefix)
extern void EndPointListener_addSpecial_m31E283677BADCE5B0EFC5E7C5D16EDE4D3782049 (void);
// 0x000003CD System.Void WebSocketSharpUnityMod.Net.EndPointListener::checkIfRemove()
extern void EndPointListener_checkIfRemove_m067B9E20F53C085950137170892D0AE0892D25F6 (void);
// 0x000003CE System.Security.Cryptography.RSACryptoServiceProvider WebSocketSharpUnityMod.Net.EndPointListener::createRSAFromFile(System.String)
extern void EndPointListener_createRSAFromFile_m3A9F97D2C1E0C7616E9258BF69968CB576AE0376 (void);
// 0x000003CF System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharpUnityMod.Net.EndPointListener::getCertificate(System.Int32,System.String,System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void EndPointListener_getCertificate_m3A153196FCC636E3648C23A57DB294EC19D40BBD (void);
// 0x000003D0 WebSocketSharpUnityMod.Net.HttpListener WebSocketSharpUnityMod.Net.EndPointListener::matchFromList(System.String,System.String,System.Collections.Generic.List`1<WebSocketSharpUnityMod.Net.HttpListenerPrefix>,WebSocketSharpUnityMod.Net.HttpListenerPrefix&)
extern void EndPointListener_matchFromList_mE2255D7952F41091BF33FD97C49425DC3235373B (void);
// 0x000003D1 System.Void WebSocketSharpUnityMod.Net.EndPointListener::onAccept(System.IAsyncResult)
extern void EndPointListener_onAccept_m1D72BE5ADBD0BD295854CDA292FCB164763B4235 (void);
// 0x000003D2 System.Void WebSocketSharpUnityMod.Net.EndPointListener::processAccepted(System.Net.Sockets.Socket,WebSocketSharpUnityMod.Net.EndPointListener)
extern void EndPointListener_processAccepted_mBD05790FC7B44012C541664FFDA21096E8045208 (void);
// 0x000003D3 System.Boolean WebSocketSharpUnityMod.Net.EndPointListener::removeSpecial(System.Collections.Generic.List`1<WebSocketSharpUnityMod.Net.HttpListenerPrefix>,WebSocketSharpUnityMod.Net.HttpListenerPrefix)
extern void EndPointListener_removeSpecial_m7CB0C1308751269ED07137A5597745F667A0B336 (void);
// 0x000003D4 WebSocketSharpUnityMod.Net.HttpListener WebSocketSharpUnityMod.Net.EndPointListener::searchListener(System.Uri,WebSocketSharpUnityMod.Net.HttpListenerPrefix&)
extern void EndPointListener_searchListener_mAEEF7BF037DE00AB46C8BB85B124F357DBDE8AB1 (void);
// 0x000003D5 System.Boolean WebSocketSharpUnityMod.Net.EndPointListener::CertificateExists(System.Int32,System.String)
extern void EndPointListener_CertificateExists_mBD01DA95AFCE3EB76509F2E7327FA08CB7E01ED8 (void);
// 0x000003D6 System.Void WebSocketSharpUnityMod.Net.EndPointListener::RemoveConnection(WebSocketSharpUnityMod.Net.HttpConnection)
extern void EndPointListener_RemoveConnection_m587594B990A6F552DDF6B56AF3FAA40A2F21DD05 (void);
// 0x000003D7 System.Void WebSocketSharpUnityMod.Net.EndPointListener::AddPrefix(WebSocketSharpUnityMod.Net.HttpListenerPrefix,WebSocketSharpUnityMod.Net.HttpListener)
extern void EndPointListener_AddPrefix_mDC204A1C39257F3C7A48E8E35DAC6B334B393ACA (void);
// 0x000003D8 System.Boolean WebSocketSharpUnityMod.Net.EndPointListener::BindContext(WebSocketSharpUnityMod.Net.HttpListenerContext)
extern void EndPointListener_BindContext_mD188A24DCB3D8B1A29F024D5223D4211787ED5A0 (void);
// 0x000003D9 System.Void WebSocketSharpUnityMod.Net.EndPointListener::Close()
extern void EndPointListener_Close_m197C1311FAB53A1A9D765A0314E3DFCC12D526A2 (void);
// 0x000003DA System.Void WebSocketSharpUnityMod.Net.EndPointListener::RemovePrefix(WebSocketSharpUnityMod.Net.HttpListenerPrefix,WebSocketSharpUnityMod.Net.HttpListener)
extern void EndPointListener_RemovePrefix_m42F8E9530ED340FE981EC9ACECAA2B8FCA925806 (void);
// 0x000003DB System.Void WebSocketSharpUnityMod.Net.EndPointManager::.cctor()
extern void EndPointManager__cctor_mB7666DE72A44091F467B8B2AFED7664D1EF1B3D7 (void);
// 0x000003DC System.Void WebSocketSharpUnityMod.Net.EndPointManager::.ctor()
extern void EndPointManager__ctor_mB6EE976DD846FB5341BC8C002A02EA5B2264BB6D (void);
// 0x000003DD System.Void WebSocketSharpUnityMod.Net.EndPointManager::addPrefix(System.String,WebSocketSharpUnityMod.Net.HttpListener)
extern void EndPointManager_addPrefix_m5A77FE5C5EB41A326F2F2B28DEFFC308924E1207 (void);
// 0x000003DE System.Net.IPAddress WebSocketSharpUnityMod.Net.EndPointManager::convertToIPAddress(System.String)
extern void EndPointManager_convertToIPAddress_m7C6CD2AFA5B41017C60A774B0800D107D6169DBE (void);
// 0x000003DF WebSocketSharpUnityMod.Net.EndPointListener WebSocketSharpUnityMod.Net.EndPointManager::getEndPointListener(WebSocketSharpUnityMod.Net.HttpListenerPrefix,WebSocketSharpUnityMod.Net.HttpListener)
extern void EndPointManager_getEndPointListener_mF71FB3757EF627EEDFFB3263C744C46219D5B5E6 (void);
// 0x000003E0 System.Void WebSocketSharpUnityMod.Net.EndPointManager::removePrefix(System.String,WebSocketSharpUnityMod.Net.HttpListener)
extern void EndPointManager_removePrefix_mC63A0660716687C373AD56C7952B2E86019BC22E (void);
// 0x000003E1 System.Void WebSocketSharpUnityMod.Net.EndPointManager::RemoveEndPoint(WebSocketSharpUnityMod.Net.EndPointListener)
extern void EndPointManager_RemoveEndPoint_m5CC4C9275D5CB0BFD722BE15C89766C0F9D1ED56 (void);
// 0x000003E2 System.Void WebSocketSharpUnityMod.Net.EndPointManager::AddListener(WebSocketSharpUnityMod.Net.HttpListener)
extern void EndPointManager_AddListener_m5DC387AC014FCA8139BB49337D22098FC41B8CBA (void);
// 0x000003E3 System.Void WebSocketSharpUnityMod.Net.EndPointManager::AddPrefix(System.String,WebSocketSharpUnityMod.Net.HttpListener)
extern void EndPointManager_AddPrefix_mC06E704DAF29914A65567BC4E084FDBEBA900910 (void);
// 0x000003E4 System.Void WebSocketSharpUnityMod.Net.EndPointManager::RemoveListener(WebSocketSharpUnityMod.Net.HttpListener)
extern void EndPointManager_RemoveListener_mED85FF1F83D96C95DD52E5F06A826E9FC3142FD7 (void);
// 0x000003E5 System.Void WebSocketSharpUnityMod.Net.EndPointManager::RemovePrefix(System.String,WebSocketSharpUnityMod.Net.HttpListener)
extern void EndPointManager_RemovePrefix_m8F03DA2525B7053384C66DFBB28EDFB1FB64BDBB (void);
// 0x000003E6 System.Void WebSocketSharpUnityMod.Net.HttpBasicIdentity::.ctor(System.String,System.String)
extern void HttpBasicIdentity__ctor_m7AAE469F3A1FE3B1AAB8DA8C2A47441EA1FF2B13 (void);
// 0x000003E7 System.String WebSocketSharpUnityMod.Net.HttpBasicIdentity::get_Password()
extern void HttpBasicIdentity_get_Password_m514E59976E658EBDC2E772AF6426AD245E29B1D3 (void);
// 0x000003E8 System.Void WebSocketSharpUnityMod.Net.HttpConnection::.ctor(System.Net.Sockets.Socket,WebSocketSharpUnityMod.Net.EndPointListener)
extern void HttpConnection__ctor_mB5029014F3B42BE1F9692098D022A4BF5A790238 (void);
// 0x000003E9 System.Boolean WebSocketSharpUnityMod.Net.HttpConnection::get_IsClosed()
extern void HttpConnection_get_IsClosed_m08F01D2C593CC64FA421FF1BA4B45861841777A8 (void);
// 0x000003EA System.Boolean WebSocketSharpUnityMod.Net.HttpConnection::get_IsSecure()
extern void HttpConnection_get_IsSecure_mEB48A91A06CADC4230A708C53CFB2DF86F50EA94 (void);
// 0x000003EB System.Net.IPEndPoint WebSocketSharpUnityMod.Net.HttpConnection::get_LocalEndPoint()
extern void HttpConnection_get_LocalEndPoint_m0A54225672308E472183517DC5CC25B4B70BF440 (void);
// 0x000003EC WebSocketSharpUnityMod.Net.HttpListenerPrefix WebSocketSharpUnityMod.Net.HttpConnection::get_Prefix()
extern void HttpConnection_get_Prefix_m740F270E5E26CBF22B2BB909F55DCC3F05F9F181 (void);
// 0x000003ED System.Void WebSocketSharpUnityMod.Net.HttpConnection::set_Prefix(WebSocketSharpUnityMod.Net.HttpListenerPrefix)
extern void HttpConnection_set_Prefix_m774F9CA86A72880134E703008342562F0149E1F7 (void);
// 0x000003EE System.Net.IPEndPoint WebSocketSharpUnityMod.Net.HttpConnection::get_RemoteEndPoint()
extern void HttpConnection_get_RemoteEndPoint_m1BBB751BBB2A9B843A26B13D76BA77AD8700C6F9 (void);
// 0x000003EF System.Int32 WebSocketSharpUnityMod.Net.HttpConnection::get_Reuses()
extern void HttpConnection_get_Reuses_mDF819A7D9B624F9A68194A42ECB1798A6692CC40 (void);
// 0x000003F0 System.IO.Stream WebSocketSharpUnityMod.Net.HttpConnection::get_Stream()
extern void HttpConnection_get_Stream_mB40AC16C57E670FB48F42BF5CF17B715349780A3 (void);
// 0x000003F1 System.Void WebSocketSharpUnityMod.Net.HttpConnection::close()
extern void HttpConnection_close_m64953DCEAF4DCE64A22DEEE9C5A3877D3BF3C5D9 (void);
// 0x000003F2 System.Void WebSocketSharpUnityMod.Net.HttpConnection::closeSocket()
extern void HttpConnection_closeSocket_m42154AC9F4BBED88492B3B5C9CDC718000627158 (void);
// 0x000003F3 System.Void WebSocketSharpUnityMod.Net.HttpConnection::disposeRequestBuffer()
extern void HttpConnection_disposeRequestBuffer_m06F28BA3EC21997592439B67F4D45516E6C76027 (void);
// 0x000003F4 System.Void WebSocketSharpUnityMod.Net.HttpConnection::disposeStream()
extern void HttpConnection_disposeStream_m197B427D1B89FF65246A813470D33DA614645AE2 (void);
// 0x000003F5 System.Void WebSocketSharpUnityMod.Net.HttpConnection::disposeTimer()
extern void HttpConnection_disposeTimer_mA668FDC3819E1BF8051AC92DDC3B82E2212858AE (void);
// 0x000003F6 System.Void WebSocketSharpUnityMod.Net.HttpConnection::init()
extern void HttpConnection_init_mC2117D11B8A0F01FC9E20641EFD9783AA6556F86 (void);
// 0x000003F7 System.Void WebSocketSharpUnityMod.Net.HttpConnection::onRead(System.IAsyncResult)
extern void HttpConnection_onRead_m4A005CFA09074010F7D0240FDDF9D0876FBE91B9 (void);
// 0x000003F8 System.Void WebSocketSharpUnityMod.Net.HttpConnection::onTimeout(System.Object)
extern void HttpConnection_onTimeout_m53E17324F81068B84DC1A6F7512EB31919E5F085 (void);
// 0x000003F9 System.Boolean WebSocketSharpUnityMod.Net.HttpConnection::processInput(System.Byte[],System.Int32)
extern void HttpConnection_processInput_mFBA0BA04D0544CCFBE9BC2BED55D68726B854D75 (void);
// 0x000003FA System.String WebSocketSharpUnityMod.Net.HttpConnection::readLineFrom(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern void HttpConnection_readLineFrom_m929E8CD5A0E782D745E37EFA6C8B5058467ADF15 (void);
// 0x000003FB System.Void WebSocketSharpUnityMod.Net.HttpConnection::removeConnection()
extern void HttpConnection_removeConnection_m38C881B7F1B84992C7490973A2C457256E205E23 (void);
// 0x000003FC System.Void WebSocketSharpUnityMod.Net.HttpConnection::unregisterContext()
extern void HttpConnection_unregisterContext_mB3A9B63DFCE53142E66EFFC4EDAB9AD62CEB1F34 (void);
// 0x000003FD System.Void WebSocketSharpUnityMod.Net.HttpConnection::Close(System.Boolean)
extern void HttpConnection_Close_m1C921595B72AF843272D1340459DC534D3EE07DE (void);
// 0x000003FE System.Void WebSocketSharpUnityMod.Net.HttpConnection::BeginReadRequest()
extern void HttpConnection_BeginReadRequest_mC71301E5E92B9C6D6C8F8E865320F0C2C7928B93 (void);
// 0x000003FF System.Void WebSocketSharpUnityMod.Net.HttpConnection::Close()
extern void HttpConnection_Close_m6BC15027F22928466E2F12EFB445EFC2F15C84AA (void);
// 0x00000400 WebSocketSharpUnityMod.Net.RequestStream WebSocketSharpUnityMod.Net.HttpConnection::GetRequestStream(System.Int64,System.Boolean)
extern void HttpConnection_GetRequestStream_m767C6A5919321A5B98C7608A4CED561DE8E0B2ED (void);
// 0x00000401 WebSocketSharpUnityMod.Net.ResponseStream WebSocketSharpUnityMod.Net.HttpConnection::GetResponseStream()
extern void HttpConnection_GetResponseStream_m62FAE850DCF408BE831746A6E144C997C1C67575 (void);
// 0x00000402 System.Void WebSocketSharpUnityMod.Net.HttpConnection::SendError()
extern void HttpConnection_SendError_mFF9C72E97CF88337A6225F611F12103F172210CD (void);
// 0x00000403 System.Void WebSocketSharpUnityMod.Net.HttpConnection::SendError(System.String,System.Int32)
extern void HttpConnection_SendError_mDC1251521508CF9FB7B5B6098C28FB7DF7BB1BAC (void);
// 0x00000404 System.Void WebSocketSharpUnityMod.Net.HttpDigestIdentity::.ctor(System.Collections.Specialized.NameValueCollection)
extern void HttpDigestIdentity__ctor_m4C2917B8F82352E687CE71C41C5FE658D26BCD48 (void);
// 0x00000405 System.String WebSocketSharpUnityMod.Net.HttpDigestIdentity::get_Algorithm()
extern void HttpDigestIdentity_get_Algorithm_m49DD24F85E444DE18C9A4AC8E9EDB48FBD181F5B (void);
// 0x00000406 System.String WebSocketSharpUnityMod.Net.HttpDigestIdentity::get_Cnonce()
extern void HttpDigestIdentity_get_Cnonce_m93A952AECD10DDEA84A08A1CD44AA105E66F12D6 (void);
// 0x00000407 System.String WebSocketSharpUnityMod.Net.HttpDigestIdentity::get_Nc()
extern void HttpDigestIdentity_get_Nc_mD6B4C06253728EAC3EA30ED9CBDC48EA4F4E5BAA (void);
// 0x00000408 System.String WebSocketSharpUnityMod.Net.HttpDigestIdentity::get_Nonce()
extern void HttpDigestIdentity_get_Nonce_mF5491DC694C659083510779BB879273C40013CE0 (void);
// 0x00000409 System.String WebSocketSharpUnityMod.Net.HttpDigestIdentity::get_Opaque()
extern void HttpDigestIdentity_get_Opaque_m5C1907DD3BFFBD6E5FACA3A46237427967B8A7A2 (void);
// 0x0000040A System.String WebSocketSharpUnityMod.Net.HttpDigestIdentity::get_Qop()
extern void HttpDigestIdentity_get_Qop_m51BCDBF56C73AAFCE8DC9E300A8772D58EDFC579 (void);
// 0x0000040B System.String WebSocketSharpUnityMod.Net.HttpDigestIdentity::get_Realm()
extern void HttpDigestIdentity_get_Realm_m0B92C0A2C7D2BD021F00A6EAC97A15F46A13525E (void);
// 0x0000040C System.String WebSocketSharpUnityMod.Net.HttpDigestIdentity::get_Response()
extern void HttpDigestIdentity_get_Response_mE75320F44CD2C79B18587E0A2CE36D4742C1CC32 (void);
// 0x0000040D System.String WebSocketSharpUnityMod.Net.HttpDigestIdentity::get_Uri()
extern void HttpDigestIdentity_get_Uri_m61CD600009BA55001AAC66BF7E64028D33410235 (void);
// 0x0000040E System.Boolean WebSocketSharpUnityMod.Net.HttpDigestIdentity::IsValid(System.String,System.String,System.String,System.String)
extern void HttpDigestIdentity_IsValid_m27105B2D570A9403472AE1276494B0CA89CB2414 (void);
// 0x0000040F System.Void WebSocketSharpUnityMod.Net.HttpHeaderInfo::.ctor(System.String,WebSocketSharpUnityMod.Net.HttpHeaderType)
extern void HttpHeaderInfo__ctor_m0BE40726E48CD3EAB4EBA6B3ECC3EAFBEB2FC54D (void);
// 0x00000410 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_IsMultiValueInRequest()
extern void HttpHeaderInfo_get_IsMultiValueInRequest_m55B6A7EABC5C9234BE8CB7918DE51691A829A15C (void);
// 0x00000411 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_IsMultiValueInResponse()
extern void HttpHeaderInfo_get_IsMultiValueInResponse_m7CE771D0FAAFA7FAF615FE0EAE4762F0669568D4 (void);
// 0x00000412 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_IsRequest()
extern void HttpHeaderInfo_get_IsRequest_m5964E73CC17680C198AAAA3C2AEAE24CC0B466A5 (void);
// 0x00000413 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_IsResponse()
extern void HttpHeaderInfo_get_IsResponse_mC67A9009FA1C242600EEB37DD78403CAE0AC68ED (void);
// 0x00000414 System.String WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_Name()
extern void HttpHeaderInfo_get_Name_m733335DEA747ABAB9986F9D9F352788668AFAFF8 (void);
// 0x00000415 WebSocketSharpUnityMod.Net.HttpHeaderType WebSocketSharpUnityMod.Net.HttpHeaderInfo::get_Type()
extern void HttpHeaderInfo_get_Type_mBD7B549513E510EF3FE697F9C91DEDC608543C10 (void);
// 0x00000416 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::IsMultiValue(System.Boolean)
extern void HttpHeaderInfo_IsMultiValue_m3125D6DB0559ACD005313F56ADEBA32F3917128E (void);
// 0x00000417 System.Boolean WebSocketSharpUnityMod.Net.HttpHeaderInfo::IsRestricted(System.Boolean)
extern void HttpHeaderInfo_IsRestricted_m9F890F6948F74153ECC0E29EB8ED7327490CFC2F (void);
// 0x00000418 System.Void WebSocketSharpUnityMod.Net.HttpListener::.cctor()
extern void HttpListener__cctor_m6DB7671E16189BBE4556C301A5E43575FF6C27C0 (void);
// 0x00000419 System.Void WebSocketSharpUnityMod.Net.HttpListener::.ctor()
extern void HttpListener__ctor_mFBFCF7EAA857293DABA606005E98DF971423179C (void);
// 0x0000041A System.Boolean WebSocketSharpUnityMod.Net.HttpListener::get_IsDisposed()
extern void HttpListener_get_IsDisposed_m7EC67B89FBB08F53BB3BB1A8841EB4775E391227 (void);
// 0x0000041B System.Boolean WebSocketSharpUnityMod.Net.HttpListener::get_ReuseAddress()
extern void HttpListener_get_ReuseAddress_m924451A5B555095A428BA650BD514D63F4C5FDBF (void);
// 0x0000041C System.Void WebSocketSharpUnityMod.Net.HttpListener::set_ReuseAddress(System.Boolean)
extern void HttpListener_set_ReuseAddress_m3788F5F175B71462DEC3B6776D8A52FFE12E7599 (void);
// 0x0000041D WebSocketSharpUnityMod.Net.AuthenticationSchemes WebSocketSharpUnityMod.Net.HttpListener::get_AuthenticationSchemes()
extern void HttpListener_get_AuthenticationSchemes_mAD0479A5FBE2FA5BE32FD2599773B680437846CF (void);
// 0x0000041E System.Void WebSocketSharpUnityMod.Net.HttpListener::set_AuthenticationSchemes(WebSocketSharpUnityMod.Net.AuthenticationSchemes)
extern void HttpListener_set_AuthenticationSchemes_m260E324033728C270E9030FBE1B66AAB60894E18 (void);
// 0x0000041F System.Func`2<WebSocketSharpUnityMod.Net.HttpListenerRequest,WebSocketSharpUnityMod.Net.AuthenticationSchemes> WebSocketSharpUnityMod.Net.HttpListener::get_AuthenticationSchemeSelector()
extern void HttpListener_get_AuthenticationSchemeSelector_m1A303DC011F89C3B3B2F0C44B263F19B37775D93 (void);
// 0x00000420 System.Void WebSocketSharpUnityMod.Net.HttpListener::set_AuthenticationSchemeSelector(System.Func`2<WebSocketSharpUnityMod.Net.HttpListenerRequest,WebSocketSharpUnityMod.Net.AuthenticationSchemes>)
extern void HttpListener_set_AuthenticationSchemeSelector_mDA83556F975E557BEA658D3E017CEAEB80A0C5A5 (void);
// 0x00000421 System.String WebSocketSharpUnityMod.Net.HttpListener::get_CertificateFolderPath()
extern void HttpListener_get_CertificateFolderPath_m3696DE6EC23C1FE48D2E6494EF268EAF019CB2B6 (void);
// 0x00000422 System.Void WebSocketSharpUnityMod.Net.HttpListener::set_CertificateFolderPath(System.String)
extern void HttpListener_set_CertificateFolderPath_mB2354315751DE129FF09883F093E58096F77967D (void);
// 0x00000423 System.Boolean WebSocketSharpUnityMod.Net.HttpListener::get_IgnoreWriteExceptions()
extern void HttpListener_get_IgnoreWriteExceptions_mB728A9C5A445CD6C524D9FBF9FA23BDAEF7111B0 (void);
// 0x00000424 System.Void WebSocketSharpUnityMod.Net.HttpListener::set_IgnoreWriteExceptions(System.Boolean)
extern void HttpListener_set_IgnoreWriteExceptions_mE93F590774CAA5B18B709D1C88D9B5D2463F87DF (void);
// 0x00000425 System.Boolean WebSocketSharpUnityMod.Net.HttpListener::get_IsListening()
extern void HttpListener_get_IsListening_mC18B065D0BADCCE9CBC8601235C75DCBBE246C4B (void);
// 0x00000426 System.Boolean WebSocketSharpUnityMod.Net.HttpListener::get_IsSupported()
extern void HttpListener_get_IsSupported_m3CFE3F345395246E7CB1FB482B99E084978E4CE8 (void);
// 0x00000427 WebSocketSharpUnityMod.Logger WebSocketSharpUnityMod.Net.HttpListener::get_Log()
extern void HttpListener_get_Log_m6EACCFC086164C7E34694B01E7F927F8C5B49899 (void);
// 0x00000428 WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection WebSocketSharpUnityMod.Net.HttpListener::get_Prefixes()
extern void HttpListener_get_Prefixes_mC9B0A2B1230301CD0CD02F4D269AD63980795ACB (void);
// 0x00000429 System.String WebSocketSharpUnityMod.Net.HttpListener::get_Realm()
extern void HttpListener_get_Realm_m89D2668FD8AF8CEEFE1BF73209F03733ADC1AE95 (void);
// 0x0000042A System.Void WebSocketSharpUnityMod.Net.HttpListener::set_Realm(System.String)
extern void HttpListener_set_Realm_mD93E32F8229D5BB946ACBF910056A4C9779E5482 (void);
// 0x0000042B WebSocketSharpUnityMod.Net.ServerSslConfiguration WebSocketSharpUnityMod.Net.HttpListener::get_SslConfiguration()
extern void HttpListener_get_SslConfiguration_mEC965E5F17F7C89E405CA805639973637E83FFCC (void);
// 0x0000042C System.Void WebSocketSharpUnityMod.Net.HttpListener::set_SslConfiguration(WebSocketSharpUnityMod.Net.ServerSslConfiguration)
extern void HttpListener_set_SslConfiguration_m0488010781BD9D20602710B41DC9E5BD512318DD (void);
// 0x0000042D System.Boolean WebSocketSharpUnityMod.Net.HttpListener::get_UnsafeConnectionNtlmAuthentication()
extern void HttpListener_get_UnsafeConnectionNtlmAuthentication_mB3F63E879CAF4EBBDC0DC902104D47F470DA6BA8 (void);
// 0x0000042E System.Void WebSocketSharpUnityMod.Net.HttpListener::set_UnsafeConnectionNtlmAuthentication(System.Boolean)
extern void HttpListener_set_UnsafeConnectionNtlmAuthentication_mA922230EA1DED62D9576E4ABB3E7CD2E152EA0A3 (void);
// 0x0000042F System.Func`2<System.Security.Principal.IIdentity,WebSocketSharpUnityMod.Net.NetworkCredential> WebSocketSharpUnityMod.Net.HttpListener::get_UserCredentialsFinder()
extern void HttpListener_get_UserCredentialsFinder_m6A0C507B82FF390A97E890B0E0A842FEA10A0262 (void);
// 0x00000430 System.Void WebSocketSharpUnityMod.Net.HttpListener::set_UserCredentialsFinder(System.Func`2<System.Security.Principal.IIdentity,WebSocketSharpUnityMod.Net.NetworkCredential>)
extern void HttpListener_set_UserCredentialsFinder_mBA4A9695EE741095A10087BB485B2CD15C1BFB84 (void);
// 0x00000431 System.Void WebSocketSharpUnityMod.Net.HttpListener::cleanupConnections()
extern void HttpListener_cleanupConnections_m255FB326473ECF746440F2767897807DA768BF4A (void);
// 0x00000432 System.Void WebSocketSharpUnityMod.Net.HttpListener::cleanupContextQueue(System.Boolean)
extern void HttpListener_cleanupContextQueue_mF56CA42A3FEBB881E0674051EAF0A92FEE2ED4C6 (void);
// 0x00000433 System.Void WebSocketSharpUnityMod.Net.HttpListener::cleanupContextRegistry()
extern void HttpListener_cleanupContextRegistry_mA570B19EF6E2651D6BCE0FCE41174976CBADC0DE (void);
// 0x00000434 System.Void WebSocketSharpUnityMod.Net.HttpListener::cleanupWaitQueue(System.Exception)
extern void HttpListener_cleanupWaitQueue_m465C49E9DBF01F918BB93F1D4EC29F3FBC42D0FA (void);
// 0x00000435 System.Void WebSocketSharpUnityMod.Net.HttpListener::close(System.Boolean)
extern void HttpListener_close_m2E280513239AC23077B613E4C2B52A404979167E (void);
// 0x00000436 WebSocketSharpUnityMod.Net.HttpListenerAsyncResult WebSocketSharpUnityMod.Net.HttpListener::getAsyncResultFromQueue()
extern void HttpListener_getAsyncResultFromQueue_m019DEAE6B5A6DD6D77134F8CA8F5F9F639CE0841 (void);
// 0x00000437 WebSocketSharpUnityMod.Net.HttpListenerContext WebSocketSharpUnityMod.Net.HttpListener::getContextFromQueue()
extern void HttpListener_getContextFromQueue_mD26F93E62E37FB50E4CB9619F6B0ADE7C3CC4515 (void);
// 0x00000438 System.Boolean WebSocketSharpUnityMod.Net.HttpListener::AddConnection(WebSocketSharpUnityMod.Net.HttpConnection)
extern void HttpListener_AddConnection_m461511299FBE4C94C4B494246349ECF0AD10B2B7 (void);
// 0x00000439 WebSocketSharpUnityMod.Net.HttpListenerAsyncResult WebSocketSharpUnityMod.Net.HttpListener::BeginGetContext(WebSocketSharpUnityMod.Net.HttpListenerAsyncResult)
extern void HttpListener_BeginGetContext_m0396F8C56C360BB09AD25C49DC29B19ACBBE8EEE (void);
// 0x0000043A System.Void WebSocketSharpUnityMod.Net.HttpListener::CheckDisposed()
extern void HttpListener_CheckDisposed_m8D76315D44940F15CEA99A19CCE6436853B4CC37 (void);
// 0x0000043B System.String WebSocketSharpUnityMod.Net.HttpListener::GetRealm()
extern void HttpListener_GetRealm_m411A0D5F68FBDBF2CDD0D9F82ADDBAF6A636D3F6 (void);
// 0x0000043C System.Func`2<System.Security.Principal.IIdentity,WebSocketSharpUnityMod.Net.NetworkCredential> WebSocketSharpUnityMod.Net.HttpListener::GetUserCredentialsFinder()
extern void HttpListener_GetUserCredentialsFinder_mC6FE79818867A5105E13B0A842AF6ED74540222B (void);
// 0x0000043D System.Boolean WebSocketSharpUnityMod.Net.HttpListener::RegisterContext(WebSocketSharpUnityMod.Net.HttpListenerContext)
extern void HttpListener_RegisterContext_m5FDF336978875E34D3FC9345AEBB3BB19B2D9833 (void);
// 0x0000043E System.Void WebSocketSharpUnityMod.Net.HttpListener::RemoveConnection(WebSocketSharpUnityMod.Net.HttpConnection)
extern void HttpListener_RemoveConnection_mCCAC9A600A729258A50632CC0C100B85210A25D7 (void);
// 0x0000043F WebSocketSharpUnityMod.Net.AuthenticationSchemes WebSocketSharpUnityMod.Net.HttpListener::SelectAuthenticationScheme(WebSocketSharpUnityMod.Net.HttpListenerRequest)
extern void HttpListener_SelectAuthenticationScheme_mAF7099B38608BF22D0ECCB8B1F6284940801DD58 (void);
// 0x00000440 System.Void WebSocketSharpUnityMod.Net.HttpListener::UnregisterContext(WebSocketSharpUnityMod.Net.HttpListenerContext)
extern void HttpListener_UnregisterContext_m04E9D562FDC557E2B44F2DE0549F5BDBD3D8FFB4 (void);
// 0x00000441 System.Void WebSocketSharpUnityMod.Net.HttpListener::Abort()
extern void HttpListener_Abort_m80CF56EB184A9F212EA5F34F7F31CE169EAFDCB8 (void);
// 0x00000442 System.IAsyncResult WebSocketSharpUnityMod.Net.HttpListener::BeginGetContext(System.AsyncCallback,System.Object)
extern void HttpListener_BeginGetContext_m70627EA6463075FD37B205DE58FD052ECC0A533B (void);
// 0x00000443 System.Void WebSocketSharpUnityMod.Net.HttpListener::Close()
extern void HttpListener_Close_m3B869874BF045358AFDBC3B60DEA33A2A67967CA (void);
// 0x00000444 WebSocketSharpUnityMod.Net.HttpListenerContext WebSocketSharpUnityMod.Net.HttpListener::EndGetContext(System.IAsyncResult)
extern void HttpListener_EndGetContext_m775C727CA935F2DDACA2630BC896A7047221BDDF (void);
// 0x00000445 WebSocketSharpUnityMod.Net.HttpListenerContext WebSocketSharpUnityMod.Net.HttpListener::GetContext()
extern void HttpListener_GetContext_m1DF66E8D457A86B23DED4236FAC6697DFE2B6468 (void);
// 0x00000446 System.Void WebSocketSharpUnityMod.Net.HttpListener::Start()
extern void HttpListener_Start_m9F61EA1F92808C3611A1C00804F53DF833C67CB7 (void);
// 0x00000447 System.Void WebSocketSharpUnityMod.Net.HttpListener::Stop()
extern void HttpListener_Stop_m81D3605DE53FC16804A95377ED9ACAD0C83C218F (void);
// 0x00000448 System.Void WebSocketSharpUnityMod.Net.HttpListener::System.IDisposable.Dispose()
extern void HttpListener_System_IDisposable_Dispose_m877C69D6A2F9DA88A0AE5241AEC7FAFA84F863BB (void);
// 0x00000449 System.Void WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::.ctor(System.AsyncCallback,System.Object)
extern void HttpListenerAsyncResult__ctor_mDFE90FC87EB0D8FF53FE4BC9B8B580AB74B22B57 (void);
// 0x0000044A System.Boolean WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::get_EndCalled()
extern void HttpListenerAsyncResult_get_EndCalled_m1E16AAD336095C198EF1DAB1F15F1A4DBEFDF3CC (void);
// 0x0000044B System.Void WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::set_EndCalled(System.Boolean)
extern void HttpListenerAsyncResult_set_EndCalled_m421B28DE70BE083A785F63F04D278BF352A4987C (void);
// 0x0000044C System.Boolean WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::get_InGet()
extern void HttpListenerAsyncResult_get_InGet_mF99BA3CA5C7DEB8AA4442DE97C276322B54FFB21 (void);
// 0x0000044D System.Void WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::set_InGet(System.Boolean)
extern void HttpListenerAsyncResult_set_InGet_m1E9F270F0896007F38E308BF210CA831BDC704E2 (void);
// 0x0000044E System.Object WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::get_AsyncState()
extern void HttpListenerAsyncResult_get_AsyncState_m515FEB15AFFAA8B649800CE079B8585B2E3DEB3D (void);
// 0x0000044F System.Threading.WaitHandle WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::get_AsyncWaitHandle()
extern void HttpListenerAsyncResult_get_AsyncWaitHandle_mBF886006E806892775E22CA9D88B7E78A362658D (void);
// 0x00000450 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::get_CompletedSynchronously()
extern void HttpListenerAsyncResult_get_CompletedSynchronously_m454705B78FF65727546DF543D20FEDA7D9089998 (void);
// 0x00000451 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::get_IsCompleted()
extern void HttpListenerAsyncResult_get_IsCompleted_m8F4B041D3A89C0B2DADBE7C52D5D49D8D8EFB39D (void);
// 0x00000452 System.Void WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::complete(WebSocketSharpUnityMod.Net.HttpListenerAsyncResult)
extern void HttpListenerAsyncResult_complete_mD904E5E919816B563D476AB0B0EAE3A2C461B630 (void);
// 0x00000453 System.Void WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::Complete(System.Exception)
extern void HttpListenerAsyncResult_Complete_m7F3B28A9A81ABB892C3AD870E90D99600085C5A9 (void);
// 0x00000454 System.Void WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::Complete(WebSocketSharpUnityMod.Net.HttpListenerContext)
extern void HttpListenerAsyncResult_Complete_m84099D5C394117D0C9DC99E86B9D93666332A967 (void);
// 0x00000455 System.Void WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::Complete(WebSocketSharpUnityMod.Net.HttpListenerContext,System.Boolean)
extern void HttpListenerAsyncResult_Complete_m9FCC747787E480BA2BD5086F2B838C6F2FFEE900 (void);
// 0x00000456 WebSocketSharpUnityMod.Net.HttpListenerContext WebSocketSharpUnityMod.Net.HttpListenerAsyncResult::GetContext()
extern void HttpListenerAsyncResult_GetContext_mCC12C2BDDF06E001D3188680D7FA4A1896CF4EAF (void);
// 0x00000457 System.Void WebSocketSharpUnityMod.Net.HttpListenerAsyncResult/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m32E221A89D77D3F93C72170A4776B5B30766B993 (void);
// 0x00000458 System.Void WebSocketSharpUnityMod.Net.HttpListenerAsyncResult/<>c__DisplayClass25_0::<complete>b__0(System.Object)
extern void U3CU3Ec__DisplayClass25_0_U3CcompleteU3Eb__0_m9DA9AD85378DC26575A9B173543399474028C428 (void);
// 0x00000459 System.Void WebSocketSharpUnityMod.Net.HttpListenerContext::.ctor(WebSocketSharpUnityMod.Net.HttpConnection)
extern void HttpListenerContext__ctor_mEC3E975A38CD16AFA31BEF8520DC3E355215EACE (void);
// 0x0000045A WebSocketSharpUnityMod.Net.HttpConnection WebSocketSharpUnityMod.Net.HttpListenerContext::get_Connection()
extern void HttpListenerContext_get_Connection_mC4F7B876830F5F3C60CEA63B40B7EF5863B3F789 (void);
// 0x0000045B System.String WebSocketSharpUnityMod.Net.HttpListenerContext::get_ErrorMessage()
extern void HttpListenerContext_get_ErrorMessage_m3BE91A1B1688A07BED20C32B26219939F3649AE8 (void);
// 0x0000045C System.Void WebSocketSharpUnityMod.Net.HttpListenerContext::set_ErrorMessage(System.String)
extern void HttpListenerContext_set_ErrorMessage_m1CCA813E55636BF7CBB6F628E649069A3742AFB4 (void);
// 0x0000045D System.Int32 WebSocketSharpUnityMod.Net.HttpListenerContext::get_ErrorStatus()
extern void HttpListenerContext_get_ErrorStatus_m943D1E92BF78E5B6A2EFFC81F2408E34C60084B5 (void);
// 0x0000045E System.Void WebSocketSharpUnityMod.Net.HttpListenerContext::set_ErrorStatus(System.Int32)
extern void HttpListenerContext_set_ErrorStatus_m8690256B921C64D08DAEAFC3761AC7FAA57A2C74 (void);
// 0x0000045F System.Boolean WebSocketSharpUnityMod.Net.HttpListenerContext::get_HasError()
extern void HttpListenerContext_get_HasError_mE15CA0879C3BB54F5BA541F22EA07E15357C3259 (void);
// 0x00000460 WebSocketSharpUnityMod.Net.HttpListener WebSocketSharpUnityMod.Net.HttpListenerContext::get_Listener()
extern void HttpListenerContext_get_Listener_mCA83CF74E76C9CCB9B7211E8D8D3FA2FD3EC5065 (void);
// 0x00000461 System.Void WebSocketSharpUnityMod.Net.HttpListenerContext::set_Listener(WebSocketSharpUnityMod.Net.HttpListener)
extern void HttpListenerContext_set_Listener_m8B1342FD32CD4E5DC243FD8E45F36D7B92819FFD (void);
// 0x00000462 WebSocketSharpUnityMod.Net.HttpListenerRequest WebSocketSharpUnityMod.Net.HttpListenerContext::get_Request()
extern void HttpListenerContext_get_Request_m24974B2B52774205FB7972EEBECFED2F261BB344 (void);
// 0x00000463 WebSocketSharpUnityMod.Net.HttpListenerResponse WebSocketSharpUnityMod.Net.HttpListenerContext::get_Response()
extern void HttpListenerContext_get_Response_m99796CD0302E981122DB731B3257553E1B81578D (void);
// 0x00000464 System.Security.Principal.IPrincipal WebSocketSharpUnityMod.Net.HttpListenerContext::get_User()
extern void HttpListenerContext_get_User_m786286044AFBB90D197ADB6BF95F3FD2301BE43B (void);
// 0x00000465 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerContext::Authenticate()
extern void HttpListenerContext_Authenticate_mE0531DF1F21A904E9E2D23AF737FCDA0DE381EFC (void);
// 0x00000466 System.Void WebSocketSharpUnityMod.Net.HttpListenerContext::Unregister()
extern void HttpListenerContext_Unregister_mE3AF8FB9936C548DD32A716A480C767908130894 (void);
// 0x00000467 WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext WebSocketSharpUnityMod.Net.HttpListenerContext::AcceptWebSocket(System.String)
extern void HttpListenerContext_AcceptWebSocket_m329ED388A7A7FCA57CD5B3F697A6F3B79E9F626E (void);
// 0x00000468 System.Void WebSocketSharpUnityMod.Net.HttpListenerException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void HttpListenerException__ctor_m140E7F8D237F079E65DB2DC169E22A2ED29DD372 (void);
// 0x00000469 System.Void WebSocketSharpUnityMod.Net.HttpListenerException::.ctor()
extern void HttpListenerException__ctor_m6B6290F1220D8449F137B81D575028BBEE0FE6E9 (void);
// 0x0000046A System.Void WebSocketSharpUnityMod.Net.HttpListenerException::.ctor(System.Int32)
extern void HttpListenerException__ctor_mD3699129B859D0B3E9BF1D3AB957A310005E0BC4 (void);
// 0x0000046B System.Void WebSocketSharpUnityMod.Net.HttpListenerException::.ctor(System.Int32,System.String)
extern void HttpListenerException__ctor_m3582E8A7B6B6D65205A9323926836B0ADE4A5271 (void);
// 0x0000046C System.Int32 WebSocketSharpUnityMod.Net.HttpListenerException::get_ErrorCode()
extern void HttpListenerException_get_ErrorCode_m273A7F192104C12B023F5FCEC5DC3E5285B1FD42 (void);
// 0x0000046D System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefix::.ctor(System.String)
extern void HttpListenerPrefix__ctor_m9471F9C54726E8B6BB9A5F596ABC4EDCD3DD2BC1 (void);
// 0x0000046E System.Net.IPAddress[] WebSocketSharpUnityMod.Net.HttpListenerPrefix::get_Addresses()
extern void HttpListenerPrefix_get_Addresses_m9D898AA243841CA80ED3C91B368F48C2BA9A4E8F (void);
// 0x0000046F System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefix::set_Addresses(System.Net.IPAddress[])
extern void HttpListenerPrefix_set_Addresses_m511945F7C3CCD2BCEA0413872C3441BFCA578083 (void);
// 0x00000470 System.String WebSocketSharpUnityMod.Net.HttpListenerPrefix::get_Host()
extern void HttpListenerPrefix_get_Host_m783076509C0A08605C45273F698C274337116BC7 (void);
// 0x00000471 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerPrefix::get_IsSecure()
extern void HttpListenerPrefix_get_IsSecure_m5D54225852FD061FC5C75092BAE49FACFB53C104 (void);
// 0x00000472 WebSocketSharpUnityMod.Net.HttpListener WebSocketSharpUnityMod.Net.HttpListenerPrefix::get_Listener()
extern void HttpListenerPrefix_get_Listener_mDAECC43C3CF2583FA7653C6FC975675333AB6322 (void);
// 0x00000473 System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefix::set_Listener(WebSocketSharpUnityMod.Net.HttpListener)
extern void HttpListenerPrefix_set_Listener_mC46618D99B0C192ACB5590E0CEA85F6AE1E2205E (void);
// 0x00000474 System.String WebSocketSharpUnityMod.Net.HttpListenerPrefix::get_Path()
extern void HttpListenerPrefix_get_Path_m10771CA8A45375B4C9DFED95E7F5BF6F711D6274 (void);
// 0x00000475 System.Int32 WebSocketSharpUnityMod.Net.HttpListenerPrefix::get_Port()
extern void HttpListenerPrefix_get_Port_m832B2097F488695083FF75F86A74DE9C7953B4FF (void);
// 0x00000476 System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefix::parse(System.String)
extern void HttpListenerPrefix_parse_m89C95236AB93B0D47C3B98903CA1A9B17FAA2575 (void);
// 0x00000477 System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefix::CheckPrefix(System.String)
extern void HttpListenerPrefix_CheckPrefix_mC855904E5FDA2BAA5D60099D70EB52ED64A7DE7F (void);
// 0x00000478 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerPrefix::Equals(System.Object)
extern void HttpListenerPrefix_Equals_mB5C98D8967CA57AE18EEEE7BF766F6D0F534CAB2 (void);
// 0x00000479 System.Int32 WebSocketSharpUnityMod.Net.HttpListenerPrefix::GetHashCode()
extern void HttpListenerPrefix_GetHashCode_mBAB7258A7E5DFCD43E58D823680F0D07EE2E65B9 (void);
// 0x0000047A System.String WebSocketSharpUnityMod.Net.HttpListenerPrefix::ToString()
extern void HttpListenerPrefix_ToString_m6F58A3AE018DD19708A4FBE1EBE8F085B4754ED9 (void);
// 0x0000047B System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::.ctor(WebSocketSharpUnityMod.Net.HttpListener)
extern void HttpListenerPrefixCollection__ctor_mC23B93B1D0027950B807CCC74A14B59F575ACD95 (void);
// 0x0000047C System.Int32 WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::get_Count()
extern void HttpListenerPrefixCollection_get_Count_m0C786C2DDFCD24339BF7FE8C40F3E081E16E4367 (void);
// 0x0000047D System.Boolean WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::get_IsReadOnly()
extern void HttpListenerPrefixCollection_get_IsReadOnly_m750B2A31F9E66A09F5C00F5DAEDC0C6033EE3DF3 (void);
// 0x0000047E System.Boolean WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::get_IsSynchronized()
extern void HttpListenerPrefixCollection_get_IsSynchronized_m1910465F7D5E7F1A3FB0F8C593C5DF4CCCC0951E (void);
// 0x0000047F System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::Add(System.String)
extern void HttpListenerPrefixCollection_Add_mFA985ECF86E60620CB71F2325A1D540F4E81B456 (void);
// 0x00000480 System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::Clear()
extern void HttpListenerPrefixCollection_Clear_m6AF1A36740EF66AEB3C666FCDFA909DC1BD14E8E (void);
// 0x00000481 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::Contains(System.String)
extern void HttpListenerPrefixCollection_Contains_m13DD6563A540C233C51746636E7F921869BF360A (void);
// 0x00000482 System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::CopyTo(System.Array,System.Int32)
extern void HttpListenerPrefixCollection_CopyTo_m0186A1EFACAEF3F53DE11757026A38787A26A93C (void);
// 0x00000483 System.Void WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::CopyTo(System.String[],System.Int32)
extern void HttpListenerPrefixCollection_CopyTo_m7D7A030835662800DBE8C0B2E398447D554C0CC6 (void);
// 0x00000484 System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::GetEnumerator()
extern void HttpListenerPrefixCollection_GetEnumerator_mE470E08D97942B167E6BA903290DB889792D86E0 (void);
// 0x00000485 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::Remove(System.String)
extern void HttpListenerPrefixCollection_Remove_m63DA07AE4F0519675940C024FB1B57B8BAC47228 (void);
// 0x00000486 System.Collections.IEnumerator WebSocketSharpUnityMod.Net.HttpListenerPrefixCollection::System.Collections.IEnumerable.GetEnumerator()
extern void HttpListenerPrefixCollection_System_Collections_IEnumerable_GetEnumerator_mF6DC16639A8D811CB4B11778CE74FD11D92E5B0E (void);
// 0x00000487 System.Void WebSocketSharpUnityMod.Net.HttpListenerRequest::.cctor()
extern void HttpListenerRequest__cctor_m4588214FD422C13F9248002AA994FEED9A15C50C (void);
// 0x00000488 System.Void WebSocketSharpUnityMod.Net.HttpListenerRequest::.ctor(WebSocketSharpUnityMod.Net.HttpListenerContext)
extern void HttpListenerRequest__ctor_m5B86C8FAFA005EA20CEE0A7897A3F47F9BF4524E (void);
// 0x00000489 System.String[] WebSocketSharpUnityMod.Net.HttpListenerRequest::get_AcceptTypes()
extern void HttpListenerRequest_get_AcceptTypes_m602E67A500D996A4712FF5FE4BEBAACE24134CAB (void);
// 0x0000048A System.Int32 WebSocketSharpUnityMod.Net.HttpListenerRequest::get_ClientCertificateError()
extern void HttpListenerRequest_get_ClientCertificateError_m3959169BF9F484C40114BCA995530B53EBE2DD66 (void);
// 0x0000048B System.Text.Encoding WebSocketSharpUnityMod.Net.HttpListenerRequest::get_ContentEncoding()
extern void HttpListenerRequest_get_ContentEncoding_m6E2C9DAAFB0DCC965837FBB6151927BC4446F704 (void);
// 0x0000048C System.Int64 WebSocketSharpUnityMod.Net.HttpListenerRequest::get_ContentLength64()
extern void HttpListenerRequest_get_ContentLength64_m285203447B5D6EA9405F156821463503924FAA13 (void);
// 0x0000048D System.String WebSocketSharpUnityMod.Net.HttpListenerRequest::get_ContentType()
extern void HttpListenerRequest_get_ContentType_mF1D61FA461C611CB045B4B6C78305A2A743E23AC (void);
// 0x0000048E WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.HttpListenerRequest::get_Cookies()
extern void HttpListenerRequest_get_Cookies_m0DABBC7927B532FA60F7784E6B5FA541CE30ECFE (void);
// 0x0000048F System.Boolean WebSocketSharpUnityMod.Net.HttpListenerRequest::get_HasEntityBody()
extern void HttpListenerRequest_get_HasEntityBody_m54937AFADCF1CA3156B2573294C1015F265B54E5 (void);
// 0x00000490 System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.HttpListenerRequest::get_Headers()
extern void HttpListenerRequest_get_Headers_m1D67466C679CFB22D3936A19BC6E998EF218812F (void);
// 0x00000491 System.String WebSocketSharpUnityMod.Net.HttpListenerRequest::get_HttpMethod()
extern void HttpListenerRequest_get_HttpMethod_mA4556510681007979ADEBCF013FFECA8A1233E6A (void);
// 0x00000492 System.IO.Stream WebSocketSharpUnityMod.Net.HttpListenerRequest::get_InputStream()
extern void HttpListenerRequest_get_InputStream_m89DA3623E70DE0C87DE3F388DD7CD4CE0346182E (void);
// 0x00000493 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerRequest::get_IsAuthenticated()
extern void HttpListenerRequest_get_IsAuthenticated_m6C529C9247853E86198109D562644CF3294ECD90 (void);
// 0x00000494 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerRequest::get_IsLocal()
extern void HttpListenerRequest_get_IsLocal_m3AA66F38458E54D48997E9928D91C41956F1A890 (void);
// 0x00000495 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerRequest::get_IsSecureConnection()
extern void HttpListenerRequest_get_IsSecureConnection_mB34F36C0029F88C249CC9A22FF738BB314E99863 (void);
// 0x00000496 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerRequest::get_IsWebSocketRequest()
extern void HttpListenerRequest_get_IsWebSocketRequest_mD8989F090E6C76736E2FA7E2024BF70C14779EFE (void);
// 0x00000497 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerRequest::get_KeepAlive()
extern void HttpListenerRequest_get_KeepAlive_m67BE6D6B08BEF0A710C6AB16DC1A535C45F8404A (void);
// 0x00000498 System.Net.IPEndPoint WebSocketSharpUnityMod.Net.HttpListenerRequest::get_LocalEndPoint()
extern void HttpListenerRequest_get_LocalEndPoint_m091629B464A47422B597E8D39A7ABCEF50A319E5 (void);
// 0x00000499 System.Version WebSocketSharpUnityMod.Net.HttpListenerRequest::get_ProtocolVersion()
extern void HttpListenerRequest_get_ProtocolVersion_m1BCAEC81C2A07EC2268C05CC01A77A0499BA411A (void);
// 0x0000049A System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.HttpListenerRequest::get_QueryString()
extern void HttpListenerRequest_get_QueryString_m94801886E1A2A398ECA163CA31807D594E9E936D (void);
// 0x0000049B System.String WebSocketSharpUnityMod.Net.HttpListenerRequest::get_RawUrl()
extern void HttpListenerRequest_get_RawUrl_mFA504322F210C80A04B5A04284035BDEA48B6865 (void);
// 0x0000049C System.Net.IPEndPoint WebSocketSharpUnityMod.Net.HttpListenerRequest::get_RemoteEndPoint()
extern void HttpListenerRequest_get_RemoteEndPoint_mEABBF656AE82DEE3DE9176C512FED612144BCE65 (void);
// 0x0000049D System.Guid WebSocketSharpUnityMod.Net.HttpListenerRequest::get_RequestTraceIdentifier()
extern void HttpListenerRequest_get_RequestTraceIdentifier_mC5C07FCB74AE3876E2ABA2F21DB21D47BDB24F72 (void);
// 0x0000049E System.Uri WebSocketSharpUnityMod.Net.HttpListenerRequest::get_Url()
extern void HttpListenerRequest_get_Url_mC24F6486FB47DC1EE7155102E6CE8222BE0EEA8C (void);
// 0x0000049F System.Uri WebSocketSharpUnityMod.Net.HttpListenerRequest::get_UrlReferrer()
extern void HttpListenerRequest_get_UrlReferrer_m120327F7DEBD583F525445F180D1423507A557F5 (void);
// 0x000004A0 System.String WebSocketSharpUnityMod.Net.HttpListenerRequest::get_UserAgent()
extern void HttpListenerRequest_get_UserAgent_m2F6AC550D3C0F5A28AD34C7A4837B588C8620F5A (void);
// 0x000004A1 System.String WebSocketSharpUnityMod.Net.HttpListenerRequest::get_UserHostAddress()
extern void HttpListenerRequest_get_UserHostAddress_m2FE2B2CB8A71DF20C393CEE7AD7CCFA54960D29B (void);
// 0x000004A2 System.String WebSocketSharpUnityMod.Net.HttpListenerRequest::get_UserHostName()
extern void HttpListenerRequest_get_UserHostName_mC4DC1B67C87F5FF89200ECB415BF60264BFB2E48 (void);
// 0x000004A3 System.String[] WebSocketSharpUnityMod.Net.HttpListenerRequest::get_UserLanguages()
extern void HttpListenerRequest_get_UserLanguages_mF89752D22E46BB5AE87C95E97E42ECD70DF0ADF9 (void);
// 0x000004A4 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerRequest::tryCreateVersion(System.String,System.Version&)
extern void HttpListenerRequest_tryCreateVersion_mBCB0B197E20221088E98C35EC9040118A141105C (void);
// 0x000004A5 System.Void WebSocketSharpUnityMod.Net.HttpListenerRequest::AddHeader(System.String)
extern void HttpListenerRequest_AddHeader_mA0986EDA8BB18D6AD2E5489A25A0A2B0E0DA2E51 (void);
// 0x000004A6 System.Void WebSocketSharpUnityMod.Net.HttpListenerRequest::FinishInitialization()
extern void HttpListenerRequest_FinishInitialization_m542353B91FF62352E79CCAAD2BA793F7A47F0FF2 (void);
// 0x000004A7 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerRequest::FlushInput()
extern void HttpListenerRequest_FlushInput_m6E501639996008731D48241D9C0C0F62804AE233 (void);
// 0x000004A8 System.Void WebSocketSharpUnityMod.Net.HttpListenerRequest::SetRequestLine(System.String)
extern void HttpListenerRequest_SetRequestLine_m0E97F29945C71B1A9177DC9D3C0FCE8605707697 (void);
// 0x000004A9 System.IAsyncResult WebSocketSharpUnityMod.Net.HttpListenerRequest::BeginGetClientCertificate(System.AsyncCallback,System.Object)
extern void HttpListenerRequest_BeginGetClientCertificate_m72607FAF39637FD69E782DFC51643565C77DA6B9 (void);
// 0x000004AA System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharpUnityMod.Net.HttpListenerRequest::EndGetClientCertificate(System.IAsyncResult)
extern void HttpListenerRequest_EndGetClientCertificate_m11F1A47BA087EAA3EBB6EEFDACB15A225011E9B9 (void);
// 0x000004AB System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharpUnityMod.Net.HttpListenerRequest::GetClientCertificate()
extern void HttpListenerRequest_GetClientCertificate_mCC7D169967F086ADCA5BD5E44B4F5AB63547507C (void);
// 0x000004AC System.String WebSocketSharpUnityMod.Net.HttpListenerRequest::ToString()
extern void HttpListenerRequest_ToString_m210D02619389F43DEC83127FE5E74F00E97AC7E3 (void);
// 0x000004AD System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::.ctor(WebSocketSharpUnityMod.Net.HttpListenerContext)
extern void HttpListenerResponse__ctor_m51DEAF9B754DF9718709C63C85B64A38313EA3C5 (void);
// 0x000004AE System.Boolean WebSocketSharpUnityMod.Net.HttpListenerResponse::get_CloseConnection()
extern void HttpListenerResponse_get_CloseConnection_mF16F6A96086009E76E506338157D8D807C33E9B3 (void);
// 0x000004AF System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_CloseConnection(System.Boolean)
extern void HttpListenerResponse_set_CloseConnection_m45ECAD23BD3B6A83C17638F771A90F81F16DF458 (void);
// 0x000004B0 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerResponse::get_HeadersSent()
extern void HttpListenerResponse_get_HeadersSent_m4E70A2D819C64D629A6D0123A085FC801D326359 (void);
// 0x000004B1 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_HeadersSent(System.Boolean)
extern void HttpListenerResponse_set_HeadersSent_m35A1E66F56D95D44C2DD3E58F739F8EDDA9FC243 (void);
// 0x000004B2 System.Text.Encoding WebSocketSharpUnityMod.Net.HttpListenerResponse::get_ContentEncoding()
extern void HttpListenerResponse_get_ContentEncoding_m94F135C884D6021D79B736D5E445607C16A1BF2F (void);
// 0x000004B3 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_ContentEncoding(System.Text.Encoding)
extern void HttpListenerResponse_set_ContentEncoding_m374AAE8C21E73089F0B940628EF3F5A17F9820D9 (void);
// 0x000004B4 System.Int64 WebSocketSharpUnityMod.Net.HttpListenerResponse::get_ContentLength64()
extern void HttpListenerResponse_get_ContentLength64_m281467961A422506EFA125D438E6865284B5C3AE (void);
// 0x000004B5 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_ContentLength64(System.Int64)
extern void HttpListenerResponse_set_ContentLength64_m4971B5FF337F08EF62867B9BD78E794FACAC93A2 (void);
// 0x000004B6 System.String WebSocketSharpUnityMod.Net.HttpListenerResponse::get_ContentType()
extern void HttpListenerResponse_get_ContentType_mD49F13F0462A82440F42C7D6306F7CD52A9BF1F2 (void);
// 0x000004B7 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_ContentType(System.String)
extern void HttpListenerResponse_set_ContentType_mE7CD25DF1B2304ED889AA8739FEBA01B74D22B34 (void);
// 0x000004B8 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.HttpListenerResponse::get_Cookies()
extern void HttpListenerResponse_get_Cookies_m6177F1AC3B42BC5B6C79CD5B3E22EB7E810C5AD3 (void);
// 0x000004B9 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_Cookies(WebSocketSharpUnityMod.Net.CookieCollection)
extern void HttpListenerResponse_set_Cookies_mFCBAD89F755926ACC31810735420E98E3B3F41C2 (void);
// 0x000004BA WebSocketSharpUnityMod.Net.WebHeaderCollection WebSocketSharpUnityMod.Net.HttpListenerResponse::get_Headers()
extern void HttpListenerResponse_get_Headers_mB674B9C921F3CCDF0C631D194E96C01305283402 (void);
// 0x000004BB System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_Headers(WebSocketSharpUnityMod.Net.WebHeaderCollection)
extern void HttpListenerResponse_set_Headers_m9973A9B5350D0C6B223D605D2B03505AA4E06EFC (void);
// 0x000004BC System.Boolean WebSocketSharpUnityMod.Net.HttpListenerResponse::get_KeepAlive()
extern void HttpListenerResponse_get_KeepAlive_m164AA94968E1429CA81FF5AF4EE86E2D562A191C (void);
// 0x000004BD System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_KeepAlive(System.Boolean)
extern void HttpListenerResponse_set_KeepAlive_m5D321C535F7666E014A249238919DCAC10936B6C (void);
// 0x000004BE System.IO.Stream WebSocketSharpUnityMod.Net.HttpListenerResponse::get_OutputStream()
extern void HttpListenerResponse_get_OutputStream_mCEA179A379AD1BC3812F2C93EECB461BAB20F506 (void);
// 0x000004BF System.Version WebSocketSharpUnityMod.Net.HttpListenerResponse::get_ProtocolVersion()
extern void HttpListenerResponse_get_ProtocolVersion_m7AE0FE1E4995F05BA91DBEDA73662705D6D68A65 (void);
// 0x000004C0 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_ProtocolVersion(System.Version)
extern void HttpListenerResponse_set_ProtocolVersion_mE4991307CCD873BF096B3B39BF823EDC1A322091 (void);
// 0x000004C1 System.String WebSocketSharpUnityMod.Net.HttpListenerResponse::get_RedirectLocation()
extern void HttpListenerResponse_get_RedirectLocation_mBE5530EC02F1B2694EB30B9AD3FDC7DE84EF5272 (void);
// 0x000004C2 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_RedirectLocation(System.String)
extern void HttpListenerResponse_set_RedirectLocation_mE981DEB87B3F0E5246E06CA811291CFBFABE9E10 (void);
// 0x000004C3 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerResponse::get_SendChunked()
extern void HttpListenerResponse_get_SendChunked_mAB66A1FA96BA4FA3F037FF9DC4933D54429D45E8 (void);
// 0x000004C4 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_SendChunked(System.Boolean)
extern void HttpListenerResponse_set_SendChunked_mAA7D3CAEE544EC21DAAAAB7BD26FEDF131C25A09 (void);
// 0x000004C5 System.Int32 WebSocketSharpUnityMod.Net.HttpListenerResponse::get_StatusCode()
extern void HttpListenerResponse_get_StatusCode_m5098E0674B169BF37719F4AA4535FE5E882F5B0B (void);
// 0x000004C6 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_StatusCode(System.Int32)
extern void HttpListenerResponse_set_StatusCode_mEECB72E6E0478CEA400056A150C924140F92BCFD (void);
// 0x000004C7 System.String WebSocketSharpUnityMod.Net.HttpListenerResponse::get_StatusDescription()
extern void HttpListenerResponse_get_StatusDescription_m2AC1D537324FF1FD0967CF93371AD4D827DE8531 (void);
// 0x000004C8 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::set_StatusDescription(System.String)
extern void HttpListenerResponse_set_StatusDescription_mFBF9C58EF31D591443C8F791D51966EAB84DF079 (void);
// 0x000004C9 System.Boolean WebSocketSharpUnityMod.Net.HttpListenerResponse::canAddOrUpdate(WebSocketSharpUnityMod.Net.Cookie)
extern void HttpListenerResponse_canAddOrUpdate_m8B465468321E43F67E08CE893EAFEFA46F04DD60 (void);
// 0x000004CA System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::checkDisposed()
extern void HttpListenerResponse_checkDisposed_m13B9D79D23D872991215FB8AC8A2EDE4F3B8B3E2 (void);
// 0x000004CB System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::checkDisposedOrHeadersSent()
extern void HttpListenerResponse_checkDisposedOrHeadersSent_m9E82829926121E7EF7AFF4556F343A5A865D0D6B (void);
// 0x000004CC System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::close(System.Boolean)
extern void HttpListenerResponse_close_mBE90E8A70885962B2BAD3972E8ED129D835ADA68 (void);
// 0x000004CD System.Collections.Generic.IEnumerable`1<WebSocketSharpUnityMod.Net.Cookie> WebSocketSharpUnityMod.Net.HttpListenerResponse::findCookie(WebSocketSharpUnityMod.Net.Cookie)
extern void HttpListenerResponse_findCookie_m1B3D82E3E8451793D2694E36B04795E3DD84FF8D (void);
// 0x000004CE WebSocketSharpUnityMod.Net.WebHeaderCollection WebSocketSharpUnityMod.Net.HttpListenerResponse::WriteHeadersTo(System.IO.MemoryStream)
extern void HttpListenerResponse_WriteHeadersTo_m18E71505856C1964A4F83196E1A2FC9C025766E5 (void);
// 0x000004CF System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::Abort()
extern void HttpListenerResponse_Abort_m2E5A4B236A33F84CD1ECD45889B32E25402C098A (void);
// 0x000004D0 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::AddHeader(System.String,System.String)
extern void HttpListenerResponse_AddHeader_mE8CD2E42D8F85D049537A37FB16744C8FB8A751E (void);
// 0x000004D1 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::AppendCookie(WebSocketSharpUnityMod.Net.Cookie)
extern void HttpListenerResponse_AppendCookie_m2E2D010E79FCA14DE60B6138758BC6F88FE14F17 (void);
// 0x000004D2 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::AppendHeader(System.String,System.String)
extern void HttpListenerResponse_AppendHeader_m2B96FDFDA0FD182CB2C343DA46FEACDDE2924DF5 (void);
// 0x000004D3 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::Close()
extern void HttpListenerResponse_Close_m5DC68D529409BBC0E5F639C40F9291E12C847355 (void);
// 0x000004D4 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::Close(System.Byte[],System.Boolean)
extern void HttpListenerResponse_Close_m71175099B665CC31424CE74041DBDE114611EA97 (void);
// 0x000004D5 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::CopyFrom(WebSocketSharpUnityMod.Net.HttpListenerResponse)
extern void HttpListenerResponse_CopyFrom_mF115294EF054C17306439064D4A61F7AF21BB2C1 (void);
// 0x000004D6 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::Redirect(System.String)
extern void HttpListenerResponse_Redirect_mD0F05DDC5C5F08F14C5F73AB9A3EA18D96BE902E (void);
// 0x000004D7 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::SetCookie(WebSocketSharpUnityMod.Net.Cookie)
extern void HttpListenerResponse_SetCookie_m83161ECA1CF34C3DF4E2D19A3B5F3C3CF636EAD1 (void);
// 0x000004D8 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse::System.IDisposable.Dispose()
extern void HttpListenerResponse_System_IDisposable_Dispose_m0714F8AB66A93EBA9AA2D41C17E10D97938DD0E6 (void);
// 0x000004D9 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse/<findCookie>d__62::.ctor(System.Int32)
extern void U3CfindCookieU3Ed__62__ctor_m864668186C1C9059A9DC53C6B23CF6E8AA84E797 (void);
// 0x000004DA System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse/<findCookie>d__62::System.IDisposable.Dispose()
extern void U3CfindCookieU3Ed__62_System_IDisposable_Dispose_m78B075B93855822EE59FBE7D4DD5D6BB2784EDE2 (void);
// 0x000004DB System.Boolean WebSocketSharpUnityMod.Net.HttpListenerResponse/<findCookie>d__62::MoveNext()
extern void U3CfindCookieU3Ed__62_MoveNext_m8ADA93366AF79A21256DC5B3098C7D8EF4A95106 (void);
// 0x000004DC System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse/<findCookie>d__62::<>m__Finally1()
extern void U3CfindCookieU3Ed__62_U3CU3Em__Finally1_m21C257A670F19BC258C805DB05734F9240501B8E (void);
// 0x000004DD WebSocketSharpUnityMod.Net.Cookie WebSocketSharpUnityMod.Net.HttpListenerResponse/<findCookie>d__62::System.Collections.Generic.IEnumerator<WebSocketSharpUnityMod.Net.Cookie>.get_Current()
extern void U3CfindCookieU3Ed__62_System_Collections_Generic_IEnumeratorU3CWebSocketSharpUnityMod_Net_CookieU3E_get_Current_m07D0FD4B852A2695B804A37C87FDC7387BBD4FDD (void);
// 0x000004DE System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse/<findCookie>d__62::System.Collections.IEnumerator.Reset()
extern void U3CfindCookieU3Ed__62_System_Collections_IEnumerator_Reset_m73D562801308883F4A4E69DF6C14435BB1FFC5F7 (void);
// 0x000004DF System.Object WebSocketSharpUnityMod.Net.HttpListenerResponse/<findCookie>d__62::System.Collections.IEnumerator.get_Current()
extern void U3CfindCookieU3Ed__62_System_Collections_IEnumerator_get_Current_mE47F57ED433706BBBE7D96A9425B1DBD836C9F45 (void);
// 0x000004E0 System.Collections.Generic.IEnumerator`1<WebSocketSharpUnityMod.Net.Cookie> WebSocketSharpUnityMod.Net.HttpListenerResponse/<findCookie>d__62::System.Collections.Generic.IEnumerable<WebSocketSharpUnityMod.Net.Cookie>.GetEnumerator()
extern void U3CfindCookieU3Ed__62_System_Collections_Generic_IEnumerableU3CWebSocketSharpUnityMod_Net_CookieU3E_GetEnumerator_m71E5056FAE4848EC5512E7ECD86D66EB32A15EC0 (void);
// 0x000004E1 System.Collections.IEnumerator WebSocketSharpUnityMod.Net.HttpListenerResponse/<findCookie>d__62::System.Collections.IEnumerable.GetEnumerator()
extern void U3CfindCookieU3Ed__62_System_Collections_IEnumerable_GetEnumerator_mBEE18F7F9D8764EA76E11AF54F29D80BF013941B (void);
// 0x000004E2 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse/<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_mC03A9702424A8E446928EAAE64C1F44DF623CAF2 (void);
// 0x000004E3 System.Void WebSocketSharpUnityMod.Net.HttpListenerResponse/<>c__DisplayClass69_0::<Close>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass69_0_U3CCloseU3Eb__0_mB95CE1A6D7A51AFA0C4D1747E56277AFE915F06B (void);
// 0x000004E4 System.Void WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::.ctor(System.AsyncCallback,System.Object)
extern void HttpStreamAsyncResult__ctor_mC9C296D4CE0D895260A62717A195D3BEC3C1A129 (void);
// 0x000004E5 System.Byte[] WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_Buffer()
extern void HttpStreamAsyncResult_get_Buffer_m48B4F985F2C35FEA081FFD64C041C4D3166EC9E4 (void);
// 0x000004E6 System.Void WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::set_Buffer(System.Byte[])
extern void HttpStreamAsyncResult_set_Buffer_m67B161E3660F7E9FB0A457033A3CFBBC54E0EB46 (void);
// 0x000004E7 System.Int32 WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_Count()
extern void HttpStreamAsyncResult_get_Count_mE13189DED845A1B36C3B5B90F8AD0F0AA3C1887B (void);
// 0x000004E8 System.Void WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::set_Count(System.Int32)
extern void HttpStreamAsyncResult_set_Count_mF30E41D15EB1D2BF022265FFB72D7F0C97F49846 (void);
// 0x000004E9 System.Exception WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_Exception()
extern void HttpStreamAsyncResult_get_Exception_mE14DAF1619CBCA7FDF3D5FD93B6E27BFDE550410 (void);
// 0x000004EA System.Boolean WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_HasException()
extern void HttpStreamAsyncResult_get_HasException_mD7BBB7259E986D5FED980149CF5775078058FC21 (void);
// 0x000004EB System.Int32 WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_Offset()
extern void HttpStreamAsyncResult_get_Offset_mE8AD886F547F6189A80658A747889066497B5BCE (void);
// 0x000004EC System.Void WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::set_Offset(System.Int32)
extern void HttpStreamAsyncResult_set_Offset_m41F9C3CB1C068633C32D7D8C2D3E321CA1723EF9 (void);
// 0x000004ED System.Int32 WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_SyncRead()
extern void HttpStreamAsyncResult_get_SyncRead_m1CF0914E07C9B9CA5FE0FBDA9AAC0B7D16E4F6FC (void);
// 0x000004EE System.Void WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::set_SyncRead(System.Int32)
extern void HttpStreamAsyncResult_set_SyncRead_m083B1EFEF780B921E96668A3B3D9F18BB591E11E (void);
// 0x000004EF System.Object WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_AsyncState()
extern void HttpStreamAsyncResult_get_AsyncState_mB8EA61F82E78DD9F2AE899A38BE145DBC8650646 (void);
// 0x000004F0 System.Threading.WaitHandle WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_AsyncWaitHandle()
extern void HttpStreamAsyncResult_get_AsyncWaitHandle_m38C3B3AD53ACDACBBF5A7AAC285AD6D6B44BC72C (void);
// 0x000004F1 System.Boolean WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_CompletedSynchronously()
extern void HttpStreamAsyncResult_get_CompletedSynchronously_m0D37D46F2175F9BECF8BAB65A1360113DCF71B30 (void);
// 0x000004F2 System.Boolean WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::get_IsCompleted()
extern void HttpStreamAsyncResult_get_IsCompleted_mE3CD055E1DF4137078914AD08EAFC718CF548A31 (void);
// 0x000004F3 System.Void WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::Complete()
extern void HttpStreamAsyncResult_Complete_mC7308B4772E293F6161CB158BB8EAF4298F38F4E (void);
// 0x000004F4 System.Void WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::Complete(System.Exception)
extern void HttpStreamAsyncResult_Complete_mE701DDCC355CADA85AB3A64AF8898E77372AFD0E (void);
// 0x000004F5 System.Void WebSocketSharpUnityMod.Net.HttpStreamAsyncResult::<Complete>b__35_0(System.IAsyncResult)
extern void HttpStreamAsyncResult_U3CCompleteU3Eb__35_0_m46B4F9E4D4F6B2E67FD87AA033469FE6BDE18B71 (void);
// 0x000004F6 System.Int32 WebSocketSharpUnityMod.Net.HttpUtility::getChar(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_getChar_m6C1F450078077C2A1AC357074158F91BDF48E980 (void);
// 0x000004F7 System.Int32 WebSocketSharpUnityMod.Net.HttpUtility::getChar(System.String,System.Int32,System.Int32)
extern void HttpUtility_getChar_mCB07049DB6A9EF8A3D3C0ED463BCE2AD1F138F4F (void);
// 0x000004F8 System.Char[] WebSocketSharpUnityMod.Net.HttpUtility::getChars(System.IO.MemoryStream,System.Text.Encoding)
extern void HttpUtility_getChars_mD589FDCD414874C32186E5D043FDD50AE1419BB2 (void);
// 0x000004F9 System.Collections.Generic.Dictionary`2<System.String,System.Char> WebSocketSharpUnityMod.Net.HttpUtility::getEntities()
extern void HttpUtility_getEntities_mFF2B3D0849E663A3227E60F8822607CECFA8F2C9 (void);
// 0x000004FA System.Int32 WebSocketSharpUnityMod.Net.HttpUtility::getInt(System.Byte)
extern void HttpUtility_getInt_m69065B0089DFCA44403C4F1EFED32631E82FDB47 (void);
// 0x000004FB System.Void WebSocketSharpUnityMod.Net.HttpUtility::initEntities()
extern void HttpUtility_initEntities_mF1A2B4890D84F9151065A3D22A837CCF535B29D9 (void);
// 0x000004FC System.Boolean WebSocketSharpUnityMod.Net.HttpUtility::notEncoded(System.Char)
extern void HttpUtility_notEncoded_mFF861798529A3F04235A68227E9746BF6A957557 (void);
// 0x000004FD System.Void WebSocketSharpUnityMod.Net.HttpUtility::urlEncode(System.Char,System.IO.Stream,System.Boolean)
extern void HttpUtility_urlEncode_m6E123DBA1F26D94B27FEAFCC28F2C5AE55EA7E46 (void);
// 0x000004FE System.Void WebSocketSharpUnityMod.Net.HttpUtility::urlPathEncode(System.Char,System.IO.Stream)
extern void HttpUtility_urlPathEncode_m2ADA293327D6F322F458DE7799296B49381BDE87 (void);
// 0x000004FF System.Void WebSocketSharpUnityMod.Net.HttpUtility::writeCharBytes(System.Char,System.Collections.IList,System.Text.Encoding)
extern void HttpUtility_writeCharBytes_m1F0610D1F7C722BC5546469F7F6787C5D11DFAF8 (void);
// 0x00000500 System.Uri WebSocketSharpUnityMod.Net.HttpUtility::CreateRequestUrl(System.String,System.String,System.Boolean,System.Boolean)
extern void HttpUtility_CreateRequestUrl_m0696971E0EADE4FAA192AF6897235CE1870B43FF (void);
// 0x00000501 System.Security.Principal.IPrincipal WebSocketSharpUnityMod.Net.HttpUtility::CreateUser(System.String,WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.String,System.String,System.Func`2<System.Security.Principal.IIdentity,WebSocketSharpUnityMod.Net.NetworkCredential>)
extern void HttpUtility_CreateUser_mB03B07375B80D0B3DBD3343144EB0AD3E2D2951F (void);
// 0x00000502 System.Text.Encoding WebSocketSharpUnityMod.Net.HttpUtility::GetEncoding(System.String)
extern void HttpUtility_GetEncoding_mF4637143ED19E0E987D87BD1C01E5A0C2DBB797A (void);
// 0x00000503 System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.HttpUtility::InternalParseQueryString(System.String,System.Text.Encoding)
extern void HttpUtility_InternalParseQueryString_mB71618C3A2BFC60106A4B068986B994362027C8D (void);
// 0x00000504 System.String WebSocketSharpUnityMod.Net.HttpUtility::InternalUrlDecode(System.Byte[],System.Int32,System.Int32,System.Text.Encoding)
extern void HttpUtility_InternalUrlDecode_mE949D49D06C3D05B18AB298F6FC6AAB424353D4E (void);
// 0x00000505 System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::InternalUrlDecodeToBytes(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_InternalUrlDecodeToBytes_m48C3A8B27F8A9DFF49ED2FDDB47A7C7E7AD705CD (void);
// 0x00000506 System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::InternalUrlEncodeToBytes(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_InternalUrlEncodeToBytes_m1746DA854EB1727D2DAAE84D82C2DC48A51DC7D0 (void);
// 0x00000507 System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::InternalUrlEncodeUnicodeToBytes(System.String)
extern void HttpUtility_InternalUrlEncodeUnicodeToBytes_mD566124911E1A93829621D9645482B8776AFE590 (void);
// 0x00000508 System.String WebSocketSharpUnityMod.Net.HttpUtility::HtmlAttributeEncode(System.String)
extern void HttpUtility_HtmlAttributeEncode_m1D0C8591D221208542CE68B6CC788AC05CF62EA4 (void);
// 0x00000509 System.Void WebSocketSharpUnityMod.Net.HttpUtility::HtmlAttributeEncode(System.String,System.IO.TextWriter)
extern void HttpUtility_HtmlAttributeEncode_mCCFAE88DA85127681D79FEB9355CE818A00E913E (void);
// 0x0000050A System.String WebSocketSharpUnityMod.Net.HttpUtility::HtmlDecode(System.String)
extern void HttpUtility_HtmlDecode_m003A03CB7E06EB81D28E684C6EEDAF16DA171FA8 (void);
// 0x0000050B System.Void WebSocketSharpUnityMod.Net.HttpUtility::HtmlDecode(System.String,System.IO.TextWriter)
extern void HttpUtility_HtmlDecode_m618EB62412AD2525ACC1C34F0703297F58EDAEEB (void);
// 0x0000050C System.String WebSocketSharpUnityMod.Net.HttpUtility::HtmlEncode(System.String)
extern void HttpUtility_HtmlEncode_mD0F087D5066224014110072CBF5DC8EC718503B9 (void);
// 0x0000050D System.Void WebSocketSharpUnityMod.Net.HttpUtility::HtmlEncode(System.String,System.IO.TextWriter)
extern void HttpUtility_HtmlEncode_m7591E13D672E18905FB3D77949DF8B394B637F33 (void);
// 0x0000050E System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.HttpUtility::ParseQueryString(System.String)
extern void HttpUtility_ParseQueryString_m13C434438542D7EDF8443A169426407BA4768045 (void);
// 0x0000050F System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.HttpUtility::ParseQueryString(System.String,System.Text.Encoding)
extern void HttpUtility_ParseQueryString_m8582D6FE6C742276DCE0231FFF21881CA7560940 (void);
// 0x00000510 System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlDecode(System.String)
extern void HttpUtility_UrlDecode_mFCD9F973A776CF0A19D3A71B29FADA93317ABA5D (void);
// 0x00000511 System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern void HttpUtility_UrlDecode_m5C81E873E9D0CBE0A175C73DC839EC6D9AC08FD0 (void);
// 0x00000512 System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlDecode(System.Byte[],System.Text.Encoding)
extern void HttpUtility_UrlDecode_mD64246F8FDA37815035618DC4869761D7F573E44 (void);
// 0x00000513 System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlDecode(System.Byte[],System.Int32,System.Int32,System.Text.Encoding)
extern void HttpUtility_UrlDecode_m2FB4B58E70269D234521D38525762C5951758396 (void);
// 0x00000514 System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::UrlDecodeToBytes(System.Byte[])
extern void HttpUtility_UrlDecodeToBytes_mB7BF5C4F54A7001650361DAD1D4DBC77C6682652 (void);
// 0x00000515 System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::UrlDecodeToBytes(System.String)
extern void HttpUtility_UrlDecodeToBytes_m59859D70F33ACA2ED5AB2D39AE382B8B885A0B4A (void);
// 0x00000516 System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::UrlDecodeToBytes(System.String,System.Text.Encoding)
extern void HttpUtility_UrlDecodeToBytes_mAC1FF176183DC6586039738ABA8565FE3534C40E (void);
// 0x00000517 System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::UrlDecodeToBytes(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_UrlDecodeToBytes_m8854169F3EDE2E04A9D9843C8CE57F5F01859D95 (void);
// 0x00000518 System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlEncode(System.Byte[])
extern void HttpUtility_UrlEncode_mCF5B38E86D59790F6C9359D9F9C18966121F3963 (void);
// 0x00000519 System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlEncode(System.String)
extern void HttpUtility_UrlEncode_m8A7960372B4391C2F43152AE0B61ACCE59A82145 (void);
// 0x0000051A System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlEncode(System.String,System.Text.Encoding)
extern void HttpUtility_UrlEncode_m27CBD80CA21463625C7A6AC7431D2319A72B8CDB (void);
// 0x0000051B System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlEncode(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_UrlEncode_m5FA3E059BBE19F72299960865CF2B93F48A673F4 (void);
// 0x0000051C System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::UrlEncodeToBytes(System.Byte[])
extern void HttpUtility_UrlEncodeToBytes_m4831725AECC75224D30A460CD8A3BAC425BCFD23 (void);
// 0x0000051D System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::UrlEncodeToBytes(System.String)
extern void HttpUtility_UrlEncodeToBytes_m247179BBF31BCC5E6FE780B51C3A50D37D1F11CB (void);
// 0x0000051E System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::UrlEncodeToBytes(System.String,System.Text.Encoding)
extern void HttpUtility_UrlEncodeToBytes_mBB8EFC170CA14F44133882880737D71B16DA5319 (void);
// 0x0000051F System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::UrlEncodeToBytes(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_UrlEncodeToBytes_m169A57085B0215DCC13D865F78D7A8E15853E530 (void);
// 0x00000520 System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlEncodeUnicode(System.String)
extern void HttpUtility_UrlEncodeUnicode_m37BAB8B9C607D5D9257B4AAE6D7646A113AFE738 (void);
// 0x00000521 System.Byte[] WebSocketSharpUnityMod.Net.HttpUtility::UrlEncodeUnicodeToBytes(System.String)
extern void HttpUtility_UrlEncodeUnicodeToBytes_m4703342B501B1412480F5636F5E75680387BB992 (void);
// 0x00000522 System.String WebSocketSharpUnityMod.Net.HttpUtility::UrlPathEncode(System.String)
extern void HttpUtility_UrlPathEncode_mA78BE952B206D445DE109480A9447828D4FF2A32 (void);
// 0x00000523 System.Void WebSocketSharpUnityMod.Net.HttpUtility::.ctor()
extern void HttpUtility__ctor_m85F72F1BB5242727149A2C350706336CDC849B79 (void);
// 0x00000524 System.Void WebSocketSharpUnityMod.Net.HttpUtility::.cctor()
extern void HttpUtility__cctor_m3BE265BBF9EA3E3594A1A6BA73BBB49E662F74E7 (void);
// 0x00000525 System.Void WebSocketSharpUnityMod.Net.HttpVersion::.ctor()
extern void HttpVersion__ctor_m915D7D7E452A3408B29605BC0CD16D0E90960B92 (void);
// 0x00000526 System.Void WebSocketSharpUnityMod.Net.HttpVersion::.cctor()
extern void HttpVersion__cctor_m38BCDF68B4DF8873B9AA05C75B63A95B8F395252 (void);
// 0x00000527 System.Void WebSocketSharpUnityMod.Net.NetworkCredential::.ctor(System.String,System.String)
extern void NetworkCredential__ctor_mBAD56DDCC46A90B0D7ABBF761B72900350AC141E (void);
// 0x00000528 System.Void WebSocketSharpUnityMod.Net.NetworkCredential::.ctor(System.String,System.String,System.String,System.String[])
extern void NetworkCredential__ctor_m45FE77FCF6A8FC07F670757287DFBCB5E777C889 (void);
// 0x00000529 System.String WebSocketSharpUnityMod.Net.NetworkCredential::get_Domain()
extern void NetworkCredential_get_Domain_mB454CBCBBDDF9C5397DAA88C856CD425473D04DA (void);
// 0x0000052A System.Void WebSocketSharpUnityMod.Net.NetworkCredential::set_Domain(System.String)
extern void NetworkCredential_set_Domain_m3C30116A3E255D782AC2319597F9BCBD065BC5DA (void);
// 0x0000052B System.String WebSocketSharpUnityMod.Net.NetworkCredential::get_Password()
extern void NetworkCredential_get_Password_mFC0A02C22ED8BE886BE31377B2847F60777E08F8 (void);
// 0x0000052C System.Void WebSocketSharpUnityMod.Net.NetworkCredential::set_Password(System.String)
extern void NetworkCredential_set_Password_m7F88E6C32C9E942096AA6253EE1BF091F4771FB6 (void);
// 0x0000052D System.String[] WebSocketSharpUnityMod.Net.NetworkCredential::get_Roles()
extern void NetworkCredential_get_Roles_mACD1CBA559713AF93F5F06C2972E08F71B481223 (void);
// 0x0000052E System.Void WebSocketSharpUnityMod.Net.NetworkCredential::set_Roles(System.String[])
extern void NetworkCredential_set_Roles_mE843750819387AFE9362E10BB3EF124C5035D774 (void);
// 0x0000052F System.String WebSocketSharpUnityMod.Net.NetworkCredential::get_UserName()
extern void NetworkCredential_get_UserName_mA1F0D33106141A6069181FECD978FD4B67B1A660 (void);
// 0x00000530 System.Void WebSocketSharpUnityMod.Net.NetworkCredential::set_UserName(System.String)
extern void NetworkCredential_set_UserName_mA223359925DA7968BE188A24DDDF1AC1FC3BA17B (void);
// 0x00000531 System.String WebSocketSharpUnityMod.Net.QueryStringCollection::ToString()
extern void QueryStringCollection_ToString_m623FABAAE439F550F8E4D8AA5A13969C6C5F3951 (void);
// 0x00000532 System.Void WebSocketSharpUnityMod.Net.QueryStringCollection::.ctor()
extern void QueryStringCollection__ctor_mFF3F9A1B60AB3144D7CC36784E2FCB52FA6C0F46 (void);
// 0x00000533 System.Void WebSocketSharpUnityMod.Net.ReadBufferState::.ctor(System.Byte[],System.Int32,System.Int32,WebSocketSharpUnityMod.Net.HttpStreamAsyncResult)
extern void ReadBufferState__ctor_m4B2D1A019702369C0AE90582F5F8B1211EC2D3F7 (void);
// 0x00000534 WebSocketSharpUnityMod.Net.HttpStreamAsyncResult WebSocketSharpUnityMod.Net.ReadBufferState::get_AsyncResult()
extern void ReadBufferState_get_AsyncResult_mBEFE4F6B0328495DC143F37D4848DF447FC8DDF6 (void);
// 0x00000535 System.Void WebSocketSharpUnityMod.Net.ReadBufferState::set_AsyncResult(WebSocketSharpUnityMod.Net.HttpStreamAsyncResult)
extern void ReadBufferState_set_AsyncResult_m47FDF655BBF5BA79C53596BF90850CFB4A696157 (void);
// 0x00000536 System.Byte[] WebSocketSharpUnityMod.Net.ReadBufferState::get_Buffer()
extern void ReadBufferState_get_Buffer_m313E73A63ACA8F18030C4668AF97C662649AADCD (void);
// 0x00000537 System.Void WebSocketSharpUnityMod.Net.ReadBufferState::set_Buffer(System.Byte[])
extern void ReadBufferState_set_Buffer_m300A3EA65E7D83E5C74E78463DAE453CAC861E9B (void);
// 0x00000538 System.Int32 WebSocketSharpUnityMod.Net.ReadBufferState::get_Count()
extern void ReadBufferState_get_Count_mED9BBB669198829E85F21978EEE252FB7A3F6AB2 (void);
// 0x00000539 System.Void WebSocketSharpUnityMod.Net.ReadBufferState::set_Count(System.Int32)
extern void ReadBufferState_set_Count_mE2A780E079A22737E0DD77BD69AE99F97D3E26BA (void);
// 0x0000053A System.Int32 WebSocketSharpUnityMod.Net.ReadBufferState::get_InitialCount()
extern void ReadBufferState_get_InitialCount_m1C6367FAF29E4A46FD65551355DAFDD93CB22FE2 (void);
// 0x0000053B System.Void WebSocketSharpUnityMod.Net.ReadBufferState::set_InitialCount(System.Int32)
extern void ReadBufferState_set_InitialCount_m1334EC06B209E213FF273B7D782A479676ED1F46 (void);
// 0x0000053C System.Int32 WebSocketSharpUnityMod.Net.ReadBufferState::get_Offset()
extern void ReadBufferState_get_Offset_m08557FD9E87466BCCA08B32C8BCC2F3585CBC18C (void);
// 0x0000053D System.Void WebSocketSharpUnityMod.Net.ReadBufferState::set_Offset(System.Int32)
extern void ReadBufferState_set_Offset_mB420BCA57E1F286E4C64E8997F8028FB2C2E2E22 (void);
// 0x0000053E System.Void WebSocketSharpUnityMod.Net.RequestStream::.ctor(System.IO.Stream,System.Byte[],System.Int32,System.Int32)
extern void RequestStream__ctor_mF6D789B645AD51616AD87D28BABBA0E9F899F37B (void);
// 0x0000053F System.Void WebSocketSharpUnityMod.Net.RequestStream::.ctor(System.IO.Stream,System.Byte[],System.Int32,System.Int32,System.Int64)
extern void RequestStream__ctor_m80BF17AD78E5CF36506CBA959215AB32CD16CBFD (void);
// 0x00000540 System.Boolean WebSocketSharpUnityMod.Net.RequestStream::get_CanRead()
extern void RequestStream_get_CanRead_m6576DD6B00DD1824DF11DCAD722A0E1433A23248 (void);
// 0x00000541 System.Boolean WebSocketSharpUnityMod.Net.RequestStream::get_CanSeek()
extern void RequestStream_get_CanSeek_m97D2695D61A0F04811477A9D6430332CB3E87C0E (void);
// 0x00000542 System.Boolean WebSocketSharpUnityMod.Net.RequestStream::get_CanWrite()
extern void RequestStream_get_CanWrite_m43F31DF825CC83E6F02C9FCB0D36ED75825F5DB5 (void);
// 0x00000543 System.Int64 WebSocketSharpUnityMod.Net.RequestStream::get_Length()
extern void RequestStream_get_Length_m422B2BB2F324610CE07D2E7C02B45B4911298D95 (void);
// 0x00000544 System.Int64 WebSocketSharpUnityMod.Net.RequestStream::get_Position()
extern void RequestStream_get_Position_mCA0D6359514812233C1A085E9ACF064A31311ACE (void);
// 0x00000545 System.Void WebSocketSharpUnityMod.Net.RequestStream::set_Position(System.Int64)
extern void RequestStream_set_Position_mE864D33F943C6F17699A60E1C550160EA4408E30 (void);
// 0x00000546 System.Int32 WebSocketSharpUnityMod.Net.RequestStream::fillFromBuffer(System.Byte[],System.Int32,System.Int32)
extern void RequestStream_fillFromBuffer_m1AD69F5B427A74A626A13EF484EE946B728CA9FB (void);
// 0x00000547 System.IAsyncResult WebSocketSharpUnityMod.Net.RequestStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void RequestStream_BeginRead_m0FB85EE696F9A2B5E4FAFC3FF33AA709FB86CA4B (void);
// 0x00000548 System.IAsyncResult WebSocketSharpUnityMod.Net.RequestStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void RequestStream_BeginWrite_m335FA78EB81C3CF504B124F3F1F7EF2C688E4378 (void);
// 0x00000549 System.Void WebSocketSharpUnityMod.Net.RequestStream::Close()
extern void RequestStream_Close_m2A803CFF93665F0CE4D8345B9DA6D2B0ADF45EF2 (void);
// 0x0000054A System.Int32 WebSocketSharpUnityMod.Net.RequestStream::EndRead(System.IAsyncResult)
extern void RequestStream_EndRead_mD3BECB27918E0CFD67CFE832D8FFD1767AF7B6A5 (void);
// 0x0000054B System.Void WebSocketSharpUnityMod.Net.RequestStream::EndWrite(System.IAsyncResult)
extern void RequestStream_EndWrite_mB17293DED56E7387457B4E0B44AA36DE1CE58395 (void);
// 0x0000054C System.Void WebSocketSharpUnityMod.Net.RequestStream::Flush()
extern void RequestStream_Flush_m2574453BEAAB9606303717779A0B391E2B79F07B (void);
// 0x0000054D System.Int32 WebSocketSharpUnityMod.Net.RequestStream::Read(System.Byte[],System.Int32,System.Int32)
extern void RequestStream_Read_m507CC4E360E9E3FEC1838DBAF946F48359178141 (void);
// 0x0000054E System.Int64 WebSocketSharpUnityMod.Net.RequestStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void RequestStream_Seek_m1CBC519F56F0564E658B280117088D154EB21F87 (void);
// 0x0000054F System.Void WebSocketSharpUnityMod.Net.RequestStream::SetLength(System.Int64)
extern void RequestStream_SetLength_mA3A582176C092F3EE0D9C18E497DF029333FEAFC (void);
// 0x00000550 System.Void WebSocketSharpUnityMod.Net.RequestStream::Write(System.Byte[],System.Int32,System.Int32)
extern void RequestStream_Write_m99AC8FCC832B109652BE8F851D72CF1B5C41AFF8 (void);
// 0x00000551 System.Void WebSocketSharpUnityMod.Net.ResponseStream::.ctor(System.IO.Stream,WebSocketSharpUnityMod.Net.HttpListenerResponse,System.Boolean)
extern void ResponseStream__ctor_m93A046FE999ABBCE64972CB7FF66FE6685B8A38B (void);
// 0x00000552 System.Boolean WebSocketSharpUnityMod.Net.ResponseStream::get_CanRead()
extern void ResponseStream_get_CanRead_mB7F0C320D93D669B631458CC73A32D41EFD751AA (void);
// 0x00000553 System.Boolean WebSocketSharpUnityMod.Net.ResponseStream::get_CanSeek()
extern void ResponseStream_get_CanSeek_mC175250738B684F11EE0397AC04AA3CEB3A4887C (void);
// 0x00000554 System.Boolean WebSocketSharpUnityMod.Net.ResponseStream::get_CanWrite()
extern void ResponseStream_get_CanWrite_m5165E8C05453F59416FEC3422CE980EDBEEAF53C (void);
// 0x00000555 System.Int64 WebSocketSharpUnityMod.Net.ResponseStream::get_Length()
extern void ResponseStream_get_Length_mDA4C6562EFA38477ECDF62D4F34BC39C80565147 (void);
// 0x00000556 System.Int64 WebSocketSharpUnityMod.Net.ResponseStream::get_Position()
extern void ResponseStream_get_Position_mBE3ADD9236FF7F1FD6F667C005B53ACFD2E2FC81 (void);
// 0x00000557 System.Void WebSocketSharpUnityMod.Net.ResponseStream::set_Position(System.Int64)
extern void ResponseStream_set_Position_m0E2CA58302B2DF9A622544480CAD0060AE9F2120 (void);
// 0x00000558 System.Boolean WebSocketSharpUnityMod.Net.ResponseStream::flush(System.Boolean)
extern void ResponseStream_flush_m96E1722D529C8D5F8ED1047BC7FA6EC7610BF939 (void);
// 0x00000559 System.Void WebSocketSharpUnityMod.Net.ResponseStream::flushBody(System.Boolean)
extern void ResponseStream_flushBody_m9877333AB4633565F070880A965C4E232F364249 (void);
// 0x0000055A System.Boolean WebSocketSharpUnityMod.Net.ResponseStream::flushHeaders(System.Boolean)
extern void ResponseStream_flushHeaders_mFABF70B224F42569F0CE3672D926602D85EBCEE0 (void);
// 0x0000055B System.Byte[] WebSocketSharpUnityMod.Net.ResponseStream::getChunkSizeBytes(System.Int32,System.Boolean)
extern void ResponseStream_getChunkSizeBytes_m5303DBB70E5C1372D5AC49C6EBC072CEE0F29C05 (void);
// 0x0000055C System.Void WebSocketSharpUnityMod.Net.ResponseStream::writeChunked(System.Byte[],System.Int32,System.Int32)
extern void ResponseStream_writeChunked_mFEB8F316A00AF568533B7CAFE3F754B52228220F (void);
// 0x0000055D System.Void WebSocketSharpUnityMod.Net.ResponseStream::writeChunkedWithoutThrowingException(System.Byte[],System.Int32,System.Int32)
extern void ResponseStream_writeChunkedWithoutThrowingException_m656E9D98B65BB35602F8E17B2C2586240BB7415B (void);
// 0x0000055E System.Void WebSocketSharpUnityMod.Net.ResponseStream::writeWithoutThrowingException(System.Byte[],System.Int32,System.Int32)
extern void ResponseStream_writeWithoutThrowingException_m52F430378C40957620B9736AAD6D2384594FC9B0 (void);
// 0x0000055F System.Void WebSocketSharpUnityMod.Net.ResponseStream::Close(System.Boolean)
extern void ResponseStream_Close_m53EA47FCE2B5D9D39FC1120B8823E0EDBA86A3D9 (void);
// 0x00000560 System.Void WebSocketSharpUnityMod.Net.ResponseStream::InternalWrite(System.Byte[],System.Int32,System.Int32)
extern void ResponseStream_InternalWrite_m88328EF0A96B0D4FF3F80CF582B6B9AF17037E04 (void);
// 0x00000561 System.IAsyncResult WebSocketSharpUnityMod.Net.ResponseStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void ResponseStream_BeginRead_m2DBEDEB03C0A2ADC21B82EF8141C7A0B0CA80A2A (void);
// 0x00000562 System.IAsyncResult WebSocketSharpUnityMod.Net.ResponseStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void ResponseStream_BeginWrite_mBB0CFE68509838C1B70389610859DC761EADB07B (void);
// 0x00000563 System.Void WebSocketSharpUnityMod.Net.ResponseStream::Close()
extern void ResponseStream_Close_m37A82381D603B7DF0AA97348C23373B700295CFD (void);
// 0x00000564 System.Void WebSocketSharpUnityMod.Net.ResponseStream::Dispose(System.Boolean)
extern void ResponseStream_Dispose_m5F447D61CB6E45B52570C0549DDE960FF2786D1A (void);
// 0x00000565 System.Int32 WebSocketSharpUnityMod.Net.ResponseStream::EndRead(System.IAsyncResult)
extern void ResponseStream_EndRead_mA38EBB6F247F9E0ACA2601E23E20E10A5E4E2666 (void);
// 0x00000566 System.Void WebSocketSharpUnityMod.Net.ResponseStream::EndWrite(System.IAsyncResult)
extern void ResponseStream_EndWrite_m6B2870845E73D7368984B4D4880AABE2DD115987 (void);
// 0x00000567 System.Void WebSocketSharpUnityMod.Net.ResponseStream::Flush()
extern void ResponseStream_Flush_m0E859964556D8A10CED69B3D230C6C866F969425 (void);
// 0x00000568 System.Int32 WebSocketSharpUnityMod.Net.ResponseStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ResponseStream_Read_m43EC16F56B6052A3479C170B5E9674C48E105207 (void);
// 0x00000569 System.Int64 WebSocketSharpUnityMod.Net.ResponseStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void ResponseStream_Seek_m9AAC33C6B4DA4F66A2C028E2CEE329235A1D9310 (void);
// 0x0000056A System.Void WebSocketSharpUnityMod.Net.ResponseStream::SetLength(System.Int64)
extern void ResponseStream_SetLength_m86CCDA3BDC5FFF61FBCDC42F12077E4C59DB7FDC (void);
// 0x0000056B System.Void WebSocketSharpUnityMod.Net.ResponseStream::Write(System.Byte[],System.Int32,System.Int32)
extern void ResponseStream_Write_mD8E7183E1529BE89EB8E698FDF3F8658A8597AF7 (void);
// 0x0000056C System.Void WebSocketSharpUnityMod.Net.ResponseStream::.cctor()
extern void ResponseStream__cctor_m221265C9458B97B3411CE550435CC56651A24CDE (void);
// 0x0000056D System.Void WebSocketSharpUnityMod.Net.ServerSslConfiguration::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void ServerSslConfiguration__ctor_m871324D0A1424CA83FA86DB70DCE865A764BEB23 (void);
// 0x0000056E System.Void WebSocketSharpUnityMod.Net.ServerSslConfiguration::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2,System.Boolean,System.Security.Authentication.SslProtocols,System.Boolean)
extern void ServerSslConfiguration__ctor_mD1FEA2C0AF70774D23B44E0D6B21E95C4CDB4AB1 (void);
// 0x0000056F System.Boolean WebSocketSharpUnityMod.Net.ServerSslConfiguration::get_ClientCertificateRequired()
extern void ServerSslConfiguration_get_ClientCertificateRequired_m59D8EAC864FB731D8B76ED0E4C472206EC7E1D2C (void);
// 0x00000570 System.Void WebSocketSharpUnityMod.Net.ServerSslConfiguration::set_ClientCertificateRequired(System.Boolean)
extern void ServerSslConfiguration_set_ClientCertificateRequired_m6229EB6DBE666866DC4A4ABAE74FD7945238A6BD (void);
// 0x00000571 System.Net.Security.RemoteCertificateValidationCallback WebSocketSharpUnityMod.Net.ServerSslConfiguration::get_ClientCertificateValidationCallback()
extern void ServerSslConfiguration_get_ClientCertificateValidationCallback_mC6454A5DC8110975E6E487186719771BD941DA22 (void);
// 0x00000572 System.Void WebSocketSharpUnityMod.Net.ServerSslConfiguration::set_ClientCertificateValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern void ServerSslConfiguration_set_ClientCertificateValidationCallback_m2E17E047ADAACA554EA7A1604EE7AE57A6F372DE (void);
// 0x00000573 System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharpUnityMod.Net.ServerSslConfiguration::get_ServerCertificate()
extern void ServerSslConfiguration_get_ServerCertificate_m54C0BD84F5AB98EEC45E6173A6523E2D7251E6C0 (void);
// 0x00000574 System.Void WebSocketSharpUnityMod.Net.ServerSslConfiguration::set_ServerCertificate(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern void ServerSslConfiguration_set_ServerCertificate_m1F938426C8C096B3D4C90E1DE6CE7CC37B56D41A (void);
// 0x00000575 System.Void WebSocketSharpUnityMod.Net.SslConfiguration::.ctor(System.Security.Authentication.SslProtocols,System.Boolean)
extern void SslConfiguration__ctor_m7E742C9826B0C507D8B8B7DF84FC8A7B447C6DDD (void);
// 0x00000576 System.Net.Security.LocalCertificateSelectionCallback WebSocketSharpUnityMod.Net.SslConfiguration::get_CertificateSelectionCallback()
extern void SslConfiguration_get_CertificateSelectionCallback_mCB32C0D015192718A52C395A0B743E0F61431903 (void);
// 0x00000577 System.Void WebSocketSharpUnityMod.Net.SslConfiguration::set_CertificateSelectionCallback(System.Net.Security.LocalCertificateSelectionCallback)
extern void SslConfiguration_set_CertificateSelectionCallback_m5C532365546E145F3766A91F171FCBB62ABB1FA3 (void);
// 0x00000578 System.Net.Security.RemoteCertificateValidationCallback WebSocketSharpUnityMod.Net.SslConfiguration::get_CertificateValidationCallback()
extern void SslConfiguration_get_CertificateValidationCallback_m5C5ACA8D0CE6B5F4968F15F8D7F40F0604C03BE3 (void);
// 0x00000579 System.Void WebSocketSharpUnityMod.Net.SslConfiguration::set_CertificateValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern void SslConfiguration_set_CertificateValidationCallback_mF959EA0A69CEC3A77DA0C382363C359A2218902C (void);
// 0x0000057A System.Boolean WebSocketSharpUnityMod.Net.SslConfiguration::get_CheckCertificateRevocation()
extern void SslConfiguration_get_CheckCertificateRevocation_m1346C308FDAC41137BF8560E33C1BC252BE4E9AA (void);
// 0x0000057B System.Void WebSocketSharpUnityMod.Net.SslConfiguration::set_CheckCertificateRevocation(System.Boolean)
extern void SslConfiguration_set_CheckCertificateRevocation_m166FF037EEB0EFF445AB45DDCEE1B2747BCC5B67 (void);
// 0x0000057C System.Security.Authentication.SslProtocols WebSocketSharpUnityMod.Net.SslConfiguration::get_EnabledSslProtocols()
extern void SslConfiguration_get_EnabledSslProtocols_m607F9EF3B993FEA14CD07F19814E0D76FDAEA40B (void);
// 0x0000057D System.Void WebSocketSharpUnityMod.Net.SslConfiguration::set_EnabledSslProtocols(System.Security.Authentication.SslProtocols)
extern void SslConfiguration_set_EnabledSslProtocols_m9EBBB04ACF3449F739F232BBD0ED40A57B81D53C (void);
// 0x0000057E System.Void WebSocketSharpUnityMod.Net.SslConfiguration/<>c::.cctor()
extern void U3CU3Ec__cctor_m8A5D84E010CCAF5B7BEC0E9306EA4978FAEA9EB5 (void);
// 0x0000057F System.Void WebSocketSharpUnityMod.Net.SslConfiguration/<>c::.ctor()
extern void U3CU3Ec__ctor_mAB01352673FA235C44BA1C5E2F5B9C41532F0AAA (void);
// 0x00000580 System.Security.Cryptography.X509Certificates.X509Certificate WebSocketSharpUnityMod.Net.SslConfiguration/<>c::<get_CertificateSelectionCallback>b__6_0(System.Object,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void U3CU3Ec_U3Cget_CertificateSelectionCallbackU3Eb__6_0_mD12EFABE59EF2DA13637E5C47754847769BBDC50 (void);
// 0x00000581 System.Boolean WebSocketSharpUnityMod.Net.SslConfiguration/<>c::<get_CertificateValidationCallback>b__9_0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void U3CU3Ec_U3Cget_CertificateValidationCallbackU3Eb__9_0_m485AC7F61C0B75C52DCBAA75BE38F21781932410 (void);
// 0x00000582 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::.cctor()
extern void WebHeaderCollection__cctor_m649A94667979754A9759D0C611345A3079765B4D (void);
// 0x00000583 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::.ctor(WebSocketSharpUnityMod.Net.HttpHeaderType,System.Boolean)
extern void WebHeaderCollection__ctor_mE931371B497688B4364E74ABBD28B8173BFC896E (void);
// 0x00000584 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection__ctor_mFE9E6882157002C92630B7B68854466EB7E0C7A4 (void);
// 0x00000585 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::.ctor()
extern void WebHeaderCollection__ctor_m27D3710730CFC952C4108056CF8978C056B6E580 (void);
// 0x00000586 WebSocketSharpUnityMod.Net.HttpHeaderType WebSocketSharpUnityMod.Net.WebHeaderCollection::get_State()
extern void WebHeaderCollection_get_State_mAC4BA8818B0238899AE8788B5CA5FDCF0B67E610 (void);
// 0x00000587 System.String[] WebSocketSharpUnityMod.Net.WebHeaderCollection::get_AllKeys()
extern void WebHeaderCollection_get_AllKeys_m813214E0117C771F6A6597402CD1D1A66663B93A (void);
// 0x00000588 System.Int32 WebSocketSharpUnityMod.Net.WebHeaderCollection::get_Count()
extern void WebHeaderCollection_get_Count_m811124F7A68BD0C2B150AC635C0BE93713FD7C6B (void);
// 0x00000589 System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::get_Item(WebSocketSharpUnityMod.Net.HttpRequestHeader)
extern void WebHeaderCollection_get_Item_m2AABCFFAE5E9E1A937C90275BEB85171FAD13D50 (void);
// 0x0000058A System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::set_Item(WebSocketSharpUnityMod.Net.HttpRequestHeader,System.String)
extern void WebHeaderCollection_set_Item_mB9F76090F01AB6364850722684C5AF282AFEB8D1 (void);
// 0x0000058B System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::get_Item(WebSocketSharpUnityMod.Net.HttpResponseHeader)
extern void WebHeaderCollection_get_Item_m58ECA637FBA09F32A24C7C1C242483589F01F9A4 (void);
// 0x0000058C System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::set_Item(WebSocketSharpUnityMod.Net.HttpResponseHeader,System.String)
extern void WebHeaderCollection_set_Item_m204A34CED0FFE9B8D085827D96E145DF2F08100C (void);
// 0x0000058D System.Collections.Specialized.NameObjectCollectionBase/KeysCollection WebSocketSharpUnityMod.Net.WebHeaderCollection::get_Keys()
extern void WebHeaderCollection_get_Keys_mF1F0667A23DF6089DE32FCEDB51C8D2C2F764AE0 (void);
// 0x0000058E System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::add(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_add_mA18BEDAF0773EFC55073CC4718F21B5C24CC43C2 (void);
// 0x0000058F System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::addWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingName_mABA029B67161DF2509E0C874A3F6F25D70A9AEFD (void);
// 0x00000590 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::addWithoutCheckingNameAndRestricted(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingNameAndRestricted_m1B2B245B45F7FE49BADF58EBC06C3CF7CF8B7101 (void);
// 0x00000591 System.Int32 WebSocketSharpUnityMod.Net.WebHeaderCollection::checkColonSeparated(System.String)
extern void WebHeaderCollection_checkColonSeparated_mC5BBDFFC466608E4D883819A8AB33B9E3D02B4F3 (void);
// 0x00000592 WebSocketSharpUnityMod.Net.HttpHeaderType WebSocketSharpUnityMod.Net.WebHeaderCollection::checkHeaderType(System.String)
extern void WebHeaderCollection_checkHeaderType_mED3DEFFB088E69C4D68A3D4465CDB67BA678F80B (void);
// 0x00000593 System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::checkName(System.String)
extern void WebHeaderCollection_checkName_mF8F1426D4242A53EE03B9CA5DBAE48E43D3E9FD4 (void);
// 0x00000594 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::checkRestricted(System.String)
extern void WebHeaderCollection_checkRestricted_m2B0FF5CEEECFB90771ACF15971B8AA0A1DD5254F (void);
// 0x00000595 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::checkState(System.Boolean)
extern void WebHeaderCollection_checkState_m11F272D6C9A6680B1F240C19AC163D09372DB589 (void);
// 0x00000596 System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::checkValue(System.String)
extern void WebHeaderCollection_checkValue_mD96CF1A4287DA2D1DCB33ABBE7A085B779F6775A (void);
// 0x00000597 System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::convert(System.String)
extern void WebHeaderCollection_convert_m598CBFDB00113BE2BF1CBBBBD8CCB925926D72CE (void);
// 0x00000598 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_mFD0F6D28F81C2AEB7F41628A5105F0A6EE5CB7F0 (void);
// 0x00000599 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_m96DF3A9E0DD5BAD62A8E0AFAF9964EA2388B7480 (void);
// 0x0000059A System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::doWithoutCheckingName(System.Action`2<System.String,System.String>,System.String,System.String)
extern void WebHeaderCollection_doWithoutCheckingName_m5FD9B4F2ED7AA65499C3530966EB8B150BF2D413 (void);
// 0x0000059B WebSocketSharpUnityMod.Net.HttpHeaderInfo WebSocketSharpUnityMod.Net.WebHeaderCollection::getHeaderInfo(System.String)
extern void WebHeaderCollection_getHeaderInfo_m6836D5FD0763F5C9869F7904A49B39F641619C85 (void);
// 0x0000059C System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::isRestricted(System.String,System.Boolean)
extern void WebHeaderCollection_isRestricted_m7A78936CF437B958699CE85238B813074AD6B643 (void);
// 0x0000059D System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::removeWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_removeWithoutCheckingName_mB88FCDA4ED5A8AA87841D5E3973E80A8F1BA90AD (void);
// 0x0000059E System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::setWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_setWithoutCheckingName_m705FDBAFE127FF3E1131870F5DA4E600093AB06B (void);
// 0x0000059F System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::Convert(WebSocketSharpUnityMod.Net.HttpRequestHeader)
extern void WebHeaderCollection_Convert_m0625AA81995B8C99A6B54AB95B324F74282C3375 (void);
// 0x000005A0 System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::Convert(WebSocketSharpUnityMod.Net.HttpResponseHeader)
extern void WebHeaderCollection_Convert_mE3C62D75B561F96816D04C4F197A65EE89C6DF2C (void);
// 0x000005A1 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::InternalRemove(System.String)
extern void WebHeaderCollection_InternalRemove_mA2B7F6584275E823940011889FF94808834E0BF8 (void);
// 0x000005A2 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::InternalSet(System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m7E3360DD00BB4615DF1043BC4A9B461475AD12AD (void);
// 0x000005A3 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::InternalSet(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m9B02D9B5DF0E2F8DC4BBF275DE95E2BD71CD26CD (void);
// 0x000005A4 System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::IsHeaderName(System.String)
extern void WebHeaderCollection_IsHeaderName_m9F6296159955C4FA47B1DFB5F3CAE9271DAD3624 (void);
// 0x000005A5 System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::IsHeaderValue(System.String)
extern void WebHeaderCollection_IsHeaderValue_m2F452605E6E6581659E09CB86AD4D37ACC6D6EAD (void);
// 0x000005A6 System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::IsMultiValue(System.String,System.Boolean)
extern void WebHeaderCollection_IsMultiValue_m692F484792EE2EA7B9D4C69647BE5B2117D714B1 (void);
// 0x000005A7 System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::ToStringMultiValue(System.Boolean)
extern void WebHeaderCollection_ToStringMultiValue_m2E48F26A1F843D0EF20D659D4DF9D7E713604666 (void);
// 0x000005A8 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::AddWithoutValidate(System.String,System.String)
extern void WebHeaderCollection_AddWithoutValidate_m230085464EE0CED91C1283D50C1D6B3E8619A6C5 (void);
// 0x000005A9 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Add(System.String)
extern void WebHeaderCollection_Add_mF5A95C1CF443989ED7841038C2DD1609D6B6114D (void);
// 0x000005AA System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Add(WebSocketSharpUnityMod.Net.HttpRequestHeader,System.String)
extern void WebHeaderCollection_Add_m8159ADD8D69C59281B5453D8EAA02D8F1A8C3E5F (void);
// 0x000005AB System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Add(WebSocketSharpUnityMod.Net.HttpResponseHeader,System.String)
extern void WebHeaderCollection_Add_mA8FA8DF2D855A72C760C12A847F56ED5C8833585 (void);
// 0x000005AC System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Add(System.String,System.String)
extern void WebHeaderCollection_Add_m3B1FBF1454344B58CF6365243A1AC54EDEA61119 (void);
// 0x000005AD System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Clear()
extern void WebHeaderCollection_Clear_m6689B340B69057D427CA66CB2B11379EF08A56CF (void);
// 0x000005AE System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::Get(System.Int32)
extern void WebHeaderCollection_Get_m74DAED1A5966632EB6E235171193E2CE64F5D431 (void);
// 0x000005AF System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::Get(System.String)
extern void WebHeaderCollection_Get_m4EBDC30E316252603EC3E552B6CCD01642C8CE11 (void);
// 0x000005B0 System.Collections.IEnumerator WebSocketSharpUnityMod.Net.WebHeaderCollection::GetEnumerator()
extern void WebHeaderCollection_GetEnumerator_m89003D0942C681E32112384913FA585D5EC0CBCE (void);
// 0x000005B1 System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::GetKey(System.Int32)
extern void WebHeaderCollection_GetKey_mBE31634017E9650FEFD826DA59C4718ACDDA4774 (void);
// 0x000005B2 System.String[] WebSocketSharpUnityMod.Net.WebHeaderCollection::GetValues(System.Int32)
extern void WebHeaderCollection_GetValues_mEC849F7AE5A395B22A98F9429C8E742473DBD65A (void);
// 0x000005B3 System.String[] WebSocketSharpUnityMod.Net.WebHeaderCollection::GetValues(System.String)
extern void WebHeaderCollection_GetValues_m002548CFB27172001B416D7EEDB6F0EB647F7CEF (void);
// 0x000005B4 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_GetObjectData_m757636963D78F387D3B709C3D9A852DB51344845 (void);
// 0x000005B5 System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::IsRestricted(System.String)
extern void WebHeaderCollection_IsRestricted_m41C322A06233D04789FB3F45DB33E93872C636A9 (void);
// 0x000005B6 System.Boolean WebSocketSharpUnityMod.Net.WebHeaderCollection::IsRestricted(System.String,System.Boolean)
extern void WebHeaderCollection_IsRestricted_mFF8895DB7DDA2790A6C9A5D545A683CFEFA12B76 (void);
// 0x000005B7 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::OnDeserialization(System.Object)
extern void WebHeaderCollection_OnDeserialization_mFCF18F78CC55D019CDDC1DAF5C9A481706C80FD3 (void);
// 0x000005B8 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Remove(WebSocketSharpUnityMod.Net.HttpRequestHeader)
extern void WebHeaderCollection_Remove_mAE67BAFC234EF48F1811C13DDCEFDF2EA01C66D6 (void);
// 0x000005B9 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Remove(WebSocketSharpUnityMod.Net.HttpResponseHeader)
extern void WebHeaderCollection_Remove_m26BE115ACEBDE5FBE12ED78638829A969A752468 (void);
// 0x000005BA System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Remove(System.String)
extern void WebHeaderCollection_Remove_mF61A70469EEC887B5AB14A093BF018DD52599110 (void);
// 0x000005BB System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Set(WebSocketSharpUnityMod.Net.HttpRequestHeader,System.String)
extern void WebHeaderCollection_Set_m8B5D88EDC48984F173F01578CCB4ABB8AB192581 (void);
// 0x000005BC System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Set(WebSocketSharpUnityMod.Net.HttpResponseHeader,System.String)
extern void WebHeaderCollection_Set_mE726AE0FA9A6ECD6640C2B94156571D4AC4ACAC3 (void);
// 0x000005BD System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::Set(System.String,System.String)
extern void WebHeaderCollection_Set_m152417993DA03F0DE1C8AEAE394992D960A71C59 (void);
// 0x000005BE System.Byte[] WebSocketSharpUnityMod.Net.WebHeaderCollection::ToByteArray()
extern void WebHeaderCollection_ToByteArray_m3F06646093A569682236424CA038A01967D4D5D9 (void);
// 0x000005BF System.String WebSocketSharpUnityMod.Net.WebHeaderCollection::ToString()
extern void WebHeaderCollection_ToString_mF7F5E7EA442EBE443DDE302376E9F6F7DE41AFEE (void);
// 0x000005C0 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m522BD95208406D143C58721C43197C1F026BA653 (void);
// 0x000005C1 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_m1463B408740B589BCBFD4AF08C4E740B845193DE (void);
// 0x000005C2 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection/<>c__DisplayClass46_0::<ToStringMultiValue>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass46_0_U3CToStringMultiValueU3Eb__0_mDC437E9B28B9DA6D5A173BF7F6D71416AC18DAB3 (void);
// 0x000005C3 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_mD2F9BD52E27030223F67992F8F89AD13247FB4D4 (void);
// 0x000005C4 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection/<>c__DisplayClass59_0::<GetObjectData>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass59_0_U3CGetObjectDataU3Eb__0_mE70B43504D2DADDF374DD21616E28A879E933E78 (void);
// 0x000005C5 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_m7DB28D035AD376368D0F6CE909E5F7F1ABE416A1 (void);
// 0x000005C6 System.Void WebSocketSharpUnityMod.Net.WebHeaderCollection/<>c__DisplayClass70_0::<ToString>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass70_0_U3CToStringU3Eb__0_mBFDEF858CDE42592A03FF94706E492AB3264BAA0 (void);
// 0x000005C7 System.Void WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::.ctor(WebSocketSharpUnityMod.Net.HttpListenerContext,System.String)
extern void HttpListenerWebSocketContext__ctor_m976B3728277B2E976BF2D55E7D2A71D75E687B39 (void);
// 0x000005C8 WebSocketSharpUnityMod.Logger WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_Log()
extern void HttpListenerWebSocketContext_get_Log_mAFA8B7E4E6381E3448011C5C660C4FCA93637060 (void);
// 0x000005C9 System.IO.Stream WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_Stream()
extern void HttpListenerWebSocketContext_get_Stream_m2394C53F18518C119968CA6775807A51FF10D819 (void);
// 0x000005CA WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_CookieCollection()
extern void HttpListenerWebSocketContext_get_CookieCollection_m08E8A674833FAA94F05D1B49C7FFEDFD0CF2A3D1 (void);
// 0x000005CB System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_Headers()
extern void HttpListenerWebSocketContext_get_Headers_m0FDA29F17EC98E00E86028ABEF3A1D831E5CCF3A (void);
// 0x000005CC System.String WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_Host()
extern void HttpListenerWebSocketContext_get_Host_m2937E828609C7A44E8D043FE29BDABF3A9E63FCA (void);
// 0x000005CD System.Boolean WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_IsAuthenticated()
extern void HttpListenerWebSocketContext_get_IsAuthenticated_m1A96456C2B472DC14D064C113B75DAA0E243FE75 (void);
// 0x000005CE System.Boolean WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_IsLocal()
extern void HttpListenerWebSocketContext_get_IsLocal_m1DADF3F55009C8C43ED080BF3CFA867A25439727 (void);
// 0x000005CF System.Boolean WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_IsSecureConnection()
extern void HttpListenerWebSocketContext_get_IsSecureConnection_m0A82A2064ADC96BA3B58EFBE4758CB64695E44B3 (void);
// 0x000005D0 System.Boolean WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_IsWebSocketRequest()
extern void HttpListenerWebSocketContext_get_IsWebSocketRequest_m77006BD20C5D70AC4F8920F5BE979A79D877072D (void);
// 0x000005D1 System.String WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_Origin()
extern void HttpListenerWebSocketContext_get_Origin_mA42B609329D0F5C13BE69CACD57C1969CD06100C (void);
// 0x000005D2 System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_QueryString()
extern void HttpListenerWebSocketContext_get_QueryString_m4BDD673EFD0E82D7E5A1512E7A35253996F974D7 (void);
// 0x000005D3 System.Uri WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_RequestUri()
extern void HttpListenerWebSocketContext_get_RequestUri_mE7BF2A920B919D1FF590ACB7C9CE8ADEA75D30D1 (void);
// 0x000005D4 System.String WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_SecWebSocketKey()
extern void HttpListenerWebSocketContext_get_SecWebSocketKey_m93A6CA90785510238066A1320BF760BE36A85B6D (void);
// 0x000005D5 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_SecWebSocketProtocols()
extern void HttpListenerWebSocketContext_get_SecWebSocketProtocols_m40F443FA2ABDD4F1C47BCFA3845D6254A4D7569F (void);
// 0x000005D6 System.String WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_SecWebSocketVersion()
extern void HttpListenerWebSocketContext_get_SecWebSocketVersion_mAD79169E810DDE27DF184653F51A851310D93970 (void);
// 0x000005D7 System.Net.IPEndPoint WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_ServerEndPoint()
extern void HttpListenerWebSocketContext_get_ServerEndPoint_m3E9342BA2B36832B3C2F9032E2255663F5186DAD (void);
// 0x000005D8 System.Security.Principal.IPrincipal WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_User()
extern void HttpListenerWebSocketContext_get_User_m596480E807419DC74D5B37171C9CA2613220001D (void);
// 0x000005D9 System.Net.IPEndPoint WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_UserEndPoint()
extern void HttpListenerWebSocketContext_get_UserEndPoint_mBEC306B1C7BD629DD187CAB74A626AC7EF38E6FB (void);
// 0x000005DA WebSocketSharpUnityMod.WebSocket WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::get_WebSocket()
extern void HttpListenerWebSocketContext_get_WebSocket_m21D5F1FC52F608150240E12B1A3B86CA74FD0C8B (void);
// 0x000005DB System.Void WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::Close()
extern void HttpListenerWebSocketContext_Close_m76493BE0E623488EDB63CC4ACE9B885129911795 (void);
// 0x000005DC System.Void WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::Close(WebSocketSharpUnityMod.Net.HttpStatusCode)
extern void HttpListenerWebSocketContext_Close_m3EAE58BDB1A54BF29568BCF34B05D8F60D4F8557 (void);
// 0x000005DD System.String WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext::ToString()
extern void HttpListenerWebSocketContext_ToString_mED17C27709F2DA175298430C7E1FC718B53397C9 (void);
// 0x000005DE System.Void WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext/<get_SecWebSocketProtocols>d__30::.ctor(System.Int32)
extern void U3Cget_SecWebSocketProtocolsU3Ed__30__ctor_mE4F9E1CEEC73589F17DC8E03EE0A93F83393A917 (void);
// 0x000005DF System.Void WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext/<get_SecWebSocketProtocols>d__30::System.IDisposable.Dispose()
extern void U3Cget_SecWebSocketProtocolsU3Ed__30_System_IDisposable_Dispose_m1138720B320B2BF20D042113176BE73804F27A50 (void);
// 0x000005E0 System.Boolean WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext/<get_SecWebSocketProtocols>d__30::MoveNext()
extern void U3Cget_SecWebSocketProtocolsU3Ed__30_MoveNext_mB600C047EEEE9729A43DE4D26E0D7EB9D5FBFEE9 (void);
// 0x000005E1 System.String WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext/<get_SecWebSocketProtocols>d__30::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m637959AC8B2CE9AA5C9669A4C8CD59199BCC26AC (void);
// 0x000005E2 System.Void WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext/<get_SecWebSocketProtocols>d__30::System.Collections.IEnumerator.Reset()
extern void U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_IEnumerator_Reset_mF16574E98614515C07FFB82275CE1E8FC2AFD4D1 (void);
// 0x000005E3 System.Object WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext/<get_SecWebSocketProtocols>d__30::System.Collections.IEnumerator.get_Current()
extern void U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_IEnumerator_get_Current_m46D1F81DBFCAF73D2CCCEE7D39120583B2E9C460 (void);
// 0x000005E4 System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext/<get_SecWebSocketProtocols>d__30::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mBF7E12CA5972C6AFA90382002906C689500F8A7A (void);
// 0x000005E5 System.Collections.IEnumerator WebSocketSharpUnityMod.Net.WebSockets.HttpListenerWebSocketContext/<get_SecWebSocketProtocols>d__30::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_IEnumerable_GetEnumerator_m1535AABF39710E7A1B416238BD0CF80294E5CBE9 (void);
// 0x000005E6 System.Void WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::.ctor(System.Net.Sockets.TcpClient,System.String,System.Boolean,WebSocketSharpUnityMod.Net.ServerSslConfiguration,WebSocketSharpUnityMod.Logger)
extern void TcpListenerWebSocketContext__ctor_m4E5CAB6165756B39D5017BB4B914147838B057A9 (void);
// 0x000005E7 WebSocketSharpUnityMod.Logger WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_Log()
extern void TcpListenerWebSocketContext_get_Log_m83133CAFCD83CA825A460E01A359273AF309111A (void);
// 0x000005E8 System.IO.Stream WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_Stream()
extern void TcpListenerWebSocketContext_get_Stream_m5BE41F20F6BF386224F8D919469C7E5D5EE76AC6 (void);
// 0x000005E9 WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_CookieCollection()
extern void TcpListenerWebSocketContext_get_CookieCollection_m25829DBF077031487BF16533D225168A0EBDF194 (void);
// 0x000005EA System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_Headers()
extern void TcpListenerWebSocketContext_get_Headers_m37D098FCBD95F3A0B8A482E02DD5F7BEF044F51B (void);
// 0x000005EB System.String WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_Host()
extern void TcpListenerWebSocketContext_get_Host_mCAE31D6EEF83DBBA35511D5A46ED0811A9A6488F (void);
// 0x000005EC System.Boolean WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_IsAuthenticated()
extern void TcpListenerWebSocketContext_get_IsAuthenticated_mF91BB26FDFC5C666C72CC4DFFF3313D8940E3A59 (void);
// 0x000005ED System.Boolean WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_IsLocal()
extern void TcpListenerWebSocketContext_get_IsLocal_m96C5D64008B10C31B341EDA3C6EC98032A808662 (void);
// 0x000005EE System.Boolean WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_IsSecureConnection()
extern void TcpListenerWebSocketContext_get_IsSecureConnection_m9356E1668C7DB4532EAAD863BC138FF17CDDBBD1 (void);
// 0x000005EF System.Boolean WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_IsWebSocketRequest()
extern void TcpListenerWebSocketContext_get_IsWebSocketRequest_mA2B72A9B003FD23ED473A60152D40B05469A135E (void);
// 0x000005F0 System.String WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_Origin()
extern void TcpListenerWebSocketContext_get_Origin_mF6D7EE059D43CABFF03AD80B3119803B556F1E11 (void);
// 0x000005F1 System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_QueryString()
extern void TcpListenerWebSocketContext_get_QueryString_m40C0A98C125DFC16F8F9D5F5277968C9A6DED8F7 (void);
// 0x000005F2 System.Uri WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_RequestUri()
extern void TcpListenerWebSocketContext_get_RequestUri_mF0FAEEF1DBA0C9653505327EC27A35B7C53F88F1 (void);
// 0x000005F3 System.String WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_SecWebSocketKey()
extern void TcpListenerWebSocketContext_get_SecWebSocketKey_m80AFCDCA89F6D6B769748CF5892361592560AF6F (void);
// 0x000005F4 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_SecWebSocketProtocols()
extern void TcpListenerWebSocketContext_get_SecWebSocketProtocols_m89369E3BC10EED63E8C6F8BBC0FD5CD5F38AC1CA (void);
// 0x000005F5 System.String WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_SecWebSocketVersion()
extern void TcpListenerWebSocketContext_get_SecWebSocketVersion_m9C0BF44A4E07CC87CEF218C5192AE791E25D6FA9 (void);
// 0x000005F6 System.Net.IPEndPoint WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_ServerEndPoint()
extern void TcpListenerWebSocketContext_get_ServerEndPoint_m73BC116BEEA4F1B37301808AA19941067330C7E0 (void);
// 0x000005F7 System.Security.Principal.IPrincipal WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_User()
extern void TcpListenerWebSocketContext_get_User_mDD633590374B719BDD7EB547BF00792D2479B1F9 (void);
// 0x000005F8 System.Net.IPEndPoint WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_UserEndPoint()
extern void TcpListenerWebSocketContext_get_UserEndPoint_m468711EBC5C2265E9A1BD8DADD8AE1462D38F571 (void);
// 0x000005F9 WebSocketSharpUnityMod.WebSocket WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::get_WebSocket()
extern void TcpListenerWebSocketContext_get_WebSocket_m85DFCFD2E88AD1E9BEAE96CD35D8E39F394DE91A (void);
// 0x000005FA System.Boolean WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::Authenticate(WebSocketSharpUnityMod.Net.AuthenticationSchemes,System.String,System.Func`2<System.Security.Principal.IIdentity,WebSocketSharpUnityMod.Net.NetworkCredential>)
extern void TcpListenerWebSocketContext_Authenticate_mE37C79CD5E61A3E6F745C5DCC193B266AFAE094C (void);
// 0x000005FB System.Void WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::Close()
extern void TcpListenerWebSocketContext_Close_m3B26EEB001479D687827EC649EDA9260FBD3740A (void);
// 0x000005FC System.Void WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::Close(WebSocketSharpUnityMod.Net.HttpStatusCode)
extern void TcpListenerWebSocketContext_Close_m51BC1F700942DD2F3CA6C3E9B3CE77E2DCD2C5D2 (void);
// 0x000005FD System.Void WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::SendAuthenticationChallenge(System.String)
extern void TcpListenerWebSocketContext_SendAuthenticationChallenge_m0C0D26F76F947A188E5A53EB69A3737705BEA91E (void);
// 0x000005FE System.String WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext::ToString()
extern void TcpListenerWebSocketContext_ToString_m786F77423F6D864D69C8894951B7304A2B2E8C9F (void);
// 0x000005FF System.Void WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<get_SecWebSocketProtocols>d__38::.ctor(System.Int32)
extern void U3Cget_SecWebSocketProtocolsU3Ed__38__ctor_mACFAE394CAA570D5687A43312F14E15B4633FC88 (void);
// 0x00000600 System.Void WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<get_SecWebSocketProtocols>d__38::System.IDisposable.Dispose()
extern void U3Cget_SecWebSocketProtocolsU3Ed__38_System_IDisposable_Dispose_m6227922B3FECD81F3CE806AB696C49D317138E1B (void);
// 0x00000601 System.Boolean WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<get_SecWebSocketProtocols>d__38::MoveNext()
extern void U3Cget_SecWebSocketProtocolsU3Ed__38_MoveNext_m935CBD050087B1A6BD601EF35C5989CA50930069 (void);
// 0x00000602 System.String WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<get_SecWebSocketProtocols>d__38::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mF1DA314254C0D92D7583A53293CCFD695B8F49F0 (void);
// 0x00000603 System.Void WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<get_SecWebSocketProtocols>d__38::System.Collections.IEnumerator.Reset()
extern void U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_IEnumerator_Reset_mB1A336EC6C311FCF818463A4CD9039D33AC46CEC (void);
// 0x00000604 System.Object WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<get_SecWebSocketProtocols>d__38::System.Collections.IEnumerator.get_Current()
extern void U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_IEnumerator_get_Current_mCF745DFB18C99509AAF064E6140CF46519A16B6C (void);
// 0x00000605 System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<get_SecWebSocketProtocols>d__38::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m28C9B2E6469AA5DC0B6C78F54B8EC12FAAC1BF85 (void);
// 0x00000606 System.Collections.IEnumerator WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<get_SecWebSocketProtocols>d__38::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_IEnumerable_GetEnumerator_m70F790F886216C71F7F6409F36436E5C590801E4 (void);
// 0x00000607 System.Void WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<>c__DisplayClass49_0::.ctor()
extern void U3CU3Ec__DisplayClass49_0__ctor_mDFC465C527EBFBC61910E39996222339520C59F3 (void);
// 0x00000608 System.Boolean WebSocketSharpUnityMod.Net.WebSockets.TcpListenerWebSocketContext/<>c__DisplayClass49_0::<Authenticate>b__0()
extern void U3CU3Ec__DisplayClass49_0_U3CAuthenticateU3Eb__0_mDAD8EA7DE6BA39591009FCA534892B990D6BDBF4 (void);
// 0x00000609 System.Void WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::.ctor()
extern void WebSocketContext__ctor_mC2B6467971A86E79172511E451FC574C86A1C956 (void);
// 0x0000060A WebSocketSharpUnityMod.Net.CookieCollection WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_CookieCollection()
// 0x0000060B System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_Headers()
// 0x0000060C System.String WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_Host()
// 0x0000060D System.Boolean WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_IsAuthenticated()
// 0x0000060E System.Boolean WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_IsLocal()
// 0x0000060F System.Boolean WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_IsSecureConnection()
// 0x00000610 System.Boolean WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_IsWebSocketRequest()
// 0x00000611 System.String WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_Origin()
// 0x00000612 System.Collections.Specialized.NameValueCollection WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_QueryString()
// 0x00000613 System.Uri WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_RequestUri()
// 0x00000614 System.String WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_SecWebSocketKey()
// 0x00000615 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_SecWebSocketProtocols()
// 0x00000616 System.String WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_SecWebSocketVersion()
// 0x00000617 System.Net.IPEndPoint WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_ServerEndPoint()
// 0x00000618 System.Security.Principal.IPrincipal WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_User()
// 0x00000619 System.Net.IPEndPoint WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_UserEndPoint()
// 0x0000061A WebSocketSharpUnityMod.WebSocket WebSocketSharpUnityMod.Net.WebSockets.WebSocketContext::get_WebSocket()
// 0x0000061B System.Void Byn.Dwrtc.ICsPeer::add_EventTriggered(System.Action`1<Byn.Dwrtc.CsPeerEventArgs>)
// 0x0000061C System.Void Byn.Dwrtc.ICsPeer::remove_EventTriggered(System.Action`1<Byn.Dwrtc.CsPeerEventArgs>)
// 0x0000061D System.Void Byn.Dwrtc.ICsPeer::AddLocalStream(Byn.Dwrtc.ICsMediaStream)
// 0x0000061E System.Void Byn.Dwrtc.ICsPeer::AddIceCandidate(System.String)
// 0x0000061F System.Void Byn.Dwrtc.ICsPeer::Close()
// 0x00000620 System.Void Byn.Dwrtc.ICsPeer::CreateAnswer()
// 0x00000621 System.Void Byn.Dwrtc.ICsPeer::CreateOffer()
// 0x00000622 System.Void Byn.Dwrtc.ICsPeer::Flush()
// 0x00000623 System.Void Byn.Dwrtc.ICsPeer::SetLocalDescription(System.String)
// 0x00000624 System.String Byn.Dwrtc.ICsPeer::GetLocalDescription()
// 0x00000625 System.Void Byn.Dwrtc.ICsPeer::SetRemoteDescription(System.String)
// 0x00000626 System.Void Byn.Dwrtc.ICsPeer::Update()
// 0x00000627 System.Void Byn.Dwrtc.ICsMediaStream::Dispose()
// 0x00000628 System.Void Byn.Dwrtc.ICsMediaStream::SetVolume(System.Double)
// 0x00000629 Byn.Awrtc.IFrame Byn.Dwrtc.ICsMediaStream::TryGetFrame()
// 0x0000062A Byn.Dwrtc.CsPeerEventType Byn.Dwrtc.CsPeerEventArgs::get_EventType()
extern void CsPeerEventArgs_get_EventType_m85634115F16B45AEBAA2EDEC95539208D90E2147 (void);
// 0x0000062B System.Void Byn.Dwrtc.CsPeerEventArgs::.ctor(Byn.Dwrtc.CsPeerEventType)
extern void CsPeerEventArgs__ctor_mEC497F173378DF9F38C389E69FBF17CC6682FFA6 (void);
// 0x0000062C System.String Byn.Dwrtc.CsPeerEventStringArgs::get_Content()
extern void CsPeerEventStringArgs_get_Content_m1BAA06C784FCCAE30C39427EFEEE275E6EB13300 (void);
// 0x0000062D System.Void Byn.Dwrtc.CsPeerEventStringArgs::.ctor(Byn.Dwrtc.CsPeerEventType,System.String)
extern void CsPeerEventStringArgs__ctor_m7E260877D664FD175027A5C8C87BCC7073D8F2DD (void);
// 0x0000062E Byn.Dwrtc.ICsMediaStream Byn.Dwrtc.CsPeerEventMediaStreamArgs::get_Stream()
extern void CsPeerEventMediaStreamArgs_get_Stream_m8B18E1BEA60BA27665642E4DEB079637A2F2630D (void);
// 0x0000062F System.Void Byn.Dwrtc.CsPeerEventMediaStreamArgs::.ctor(Byn.Dwrtc.CsPeerEventType,Byn.Dwrtc.ICsMediaStream)
extern void CsPeerEventMediaStreamArgs__ctor_m0BB7F654D35E0B495E96B5ED2DE90BFA9973370B (void);
// 0x00000630 System.Object Byn.Dwrtc.CsPeerEventDataChannelArgs::get_DataChannel()
extern void CsPeerEventDataChannelArgs_get_DataChannel_m40E6B85265DF8775C0035E01C25D55C5A96B6200 (void);
// 0x00000631 System.Void Byn.Dwrtc.CsPeerEventDataChannelArgs::.ctor(Byn.Dwrtc.CsPeerEventType,System.Object)
extern void CsPeerEventDataChannelArgs__ctor_m36EA62650CE43DFE00912886B66CC183CDE197AB (void);
// 0x00000632 WebRtcCSharp.PollingMediaStreamRef Byn.Dwrtc.Native.NativeCsMediaStream::GetInternalStream()
extern void NativeCsMediaStream_GetInternalStream_m79A78C8BDF67B96A2327B5CE66EF5D8EAA732103 (void);
// 0x00000633 System.Void Byn.Dwrtc.Native.NativeCsMediaStream::.ctor()
extern void NativeCsMediaStream__ctor_m4B19997E60E1C846E784F4FDFEE395F19A056F79 (void);
// 0x00000634 System.Void Byn.Dwrtc.Native.NativeCsMediaStream::.ctor(WebRtcCSharp.PollingMediaStreamRef)
extern void NativeCsMediaStream__ctor_mBB13033363FED15AF8E3DF8EB2489B240ACDBA42 (void);
// 0x00000635 System.Boolean Byn.Dwrtc.Native.NativeCsMediaStream::Create(Byn.Awrtc.Native.NativeWebRtcNetworkFactory,Byn.Awrtc.MediaConfig)
extern void NativeCsMediaStream_Create_m7470906EE9289D24EA5BB2BFD3E98C5A7462E1F2 (void);
// 0x00000636 System.Void Byn.Dwrtc.Native.NativeCsMediaStream::Dispose()
extern void NativeCsMediaStream_Dispose_m67F09C74D9EBCD6FAC56790E0E3AFA681CCCAE0D (void);
// 0x00000637 System.Void Byn.Dwrtc.Native.NativeCsMediaStream::SetVolume(System.Double)
extern void NativeCsMediaStream_SetVolume_m331254826A93F9CAFE2A084318B5D84DCFC2AD64 (void);
// 0x00000638 Byn.Awrtc.IFrame Byn.Dwrtc.Native.NativeCsMediaStream::TryGetFrame()
extern void NativeCsMediaStream_TryGetFrame_mC52B7DE279EC9B5B63B0752640C4689C0C5B575D (void);
// 0x00000639 WebRtcCSharp.AsyncPeerRef Byn.Dwrtc.Native.NativeCsPeer::get_Internal()
extern void NativeCsPeer_get_Internal_m9D390F11ED9D82D8E82FA25DCB6F78DCB2F1F63D (void);
// 0x0000063A System.Void Byn.Dwrtc.Native.NativeCsPeer::add_EventTriggered(System.Action`1<Byn.Dwrtc.CsPeerEventArgs>)
extern void NativeCsPeer_add_EventTriggered_m3EBC96AE2E811C45DF1F892D48AE5F6FAB551E81 (void);
// 0x0000063B System.Void Byn.Dwrtc.Native.NativeCsPeer::remove_EventTriggered(System.Action`1<Byn.Dwrtc.CsPeerEventArgs>)
extern void NativeCsPeer_remove_EventTriggered_m738F407BDDF6A83D182EE516B8E6F7C1B4E00B0F (void);
// 0x0000063C System.Void Byn.Dwrtc.Native.NativeCsPeer::.ctor(WebRtcCSharp.AsyncPeerRef)
extern void NativeCsPeer__ctor_m66684B02BE95A8FEBA0D6EA926F913B2FC8EB243 (void);
// 0x0000063D System.Void Byn.Dwrtc.Native.NativeCsPeer::CreateOffer()
extern void NativeCsPeer_CreateOffer_m3E677839E10974AAB901D5D250C43C1E12485EB2 (void);
// 0x0000063E System.Void Byn.Dwrtc.Native.NativeCsPeer::CreateAnswer()
extern void NativeCsPeer_CreateAnswer_m81BC9E7E48097587F7E0E9618A944AF592089BC1 (void);
// 0x0000063F System.Void Byn.Dwrtc.Native.NativeCsPeer::SetLocalDescription(System.String)
extern void NativeCsPeer_SetLocalDescription_m6910310A7EA230B3AE099EF5ACCCCD0945EF57B5 (void);
// 0x00000640 System.Void Byn.Dwrtc.Native.NativeCsPeer::SetRemoteDescription(System.String)
extern void NativeCsPeer_SetRemoteDescription_m6C8D50ACC9BA1E48C9A2E6E063F2124307EFEA30 (void);
// 0x00000641 System.Void Byn.Dwrtc.Native.NativeCsPeer::AddIceCandidate(System.String)
extern void NativeCsPeer_AddIceCandidate_m5B7084D0EDE74C694430AA7F34F65F8FB28D940D (void);
// 0x00000642 System.Void Byn.Dwrtc.Native.NativeCsPeer::Update()
extern void NativeCsPeer_Update_m92968C32A345A4FC5645CDDC114D31BD39F43C9D (void);
// 0x00000643 System.Void Byn.Dwrtc.Native.NativeCsPeer::Flush()
extern void NativeCsPeer_Flush_m2966AB79A66B456D6BAE1AB8190DEA5323CA9A90 (void);
// 0x00000644 System.Void Byn.Dwrtc.Native.NativeCsPeer::Close()
extern void NativeCsPeer_Close_mDE2BC1BBC2B497938F2130570B2D1513C6FF1225 (void);
// 0x00000645 System.Void Byn.Dwrtc.Native.NativeCsPeer::AddLocalStream(Byn.Dwrtc.ICsMediaStream)
extern void NativeCsPeer_AddLocalStream_m9ABD05E0703F45CDA4EC353A76B2C2E407ACC30B (void);
// 0x00000646 System.String Byn.Dwrtc.Native.NativeCsPeer::GetLocalDescription()
extern void NativeCsPeer_GetLocalDescription_m8F614671AAEEAEFEC0133F84787450AE03B5560E (void);
// 0x00000647 System.Void Byn.Dwrtc.Native.NativeCsPeer::Dispose(System.Boolean)
extern void NativeCsPeer_Dispose_m6CEE2308BFF7AF83E6875F8B642F09996DC89AC3 (void);
// 0x00000648 System.Void Byn.Dwrtc.Native.NativeCsPeer::Dispose()
extern void NativeCsPeer_Dispose_mECCB488796B5055E87310E9E42CA52B06AB521B5 (void);
// 0x00000649 System.Int32 Byn.Awrtc.Native.AudioFrames::get_BitsPerSample()
extern void AudioFrames_get_BitsPerSample_m06694B90DF355FDD27491C11D4637A2F808B7C6C (void);
// 0x0000064A System.Int32 Byn.Awrtc.Native.AudioFrames::get_SampleRate()
extern void AudioFrames_get_SampleRate_m727E7A6EB67AA9D104919DA8AAAA339EBE850E52 (void);
// 0x0000064B System.UInt32 Byn.Awrtc.Native.AudioFrames::get_NumberOfChannels()
extern void AudioFrames_get_NumberOfChannels_m0363D939ECC9FBEC7A695F1C5C286E0B721A5488 (void);
// 0x0000064C System.UInt32 Byn.Awrtc.Native.AudioFrames::get_NumberOfFrames()
extern void AudioFrames_get_NumberOfFrames_m3CE5B08CD4F653573605D5862E69B882497B1287 (void);
// 0x0000064D System.Byte[] Byn.Awrtc.Native.AudioFrames::get_Buffer()
extern void AudioFrames_get_Buffer_m3C52F35D9D6501DDB761437E7CC5B65936ED11D8 (void);
// 0x0000064E System.Void Byn.Awrtc.Native.AudioFrames::.ctor(System.Byte[],System.Int32,System.Int32,System.UInt32,System.UInt32)
extern void AudioFrames__ctor_m081801470DB9730058CAA12D650E0E5E9F9665D2 (void);
// 0x0000064F System.Void Byn.Awrtc.Native.AudioFrames::Dispose(System.Boolean)
extern void AudioFrames_Dispose_m5EC88C82686FCDCAC3A6269D74F4838480C140E4 (void);
// 0x00000650 System.Void Byn.Awrtc.Native.AudioFrames::Dispose()
extern void AudioFrames_Dispose_m2592DD1B8D305905D2FC3FF35CA69A524CC376C5 (void);
// 0x00000651 System.Byte[] Byn.Awrtc.Native.DirectMemoryFrame::get_Buffer()
extern void DirectMemoryFrame_get_Buffer_mEBD0653C49802FA92F4EB82B78AB84689649E570 (void);
// 0x00000652 System.Boolean Byn.Awrtc.Native.DirectMemoryFrame::get_Buffered()
extern void DirectMemoryFrame_get_Buffered_mCB9BA652D798A54A6D81C5CBA788E9360586ED13 (void);
// 0x00000653 Byn.Awrtc.FramePixelFormat Byn.Awrtc.Native.DirectMemoryFrame::get_Format()
extern void DirectMemoryFrame_get_Format_mC1A7380FB377464B959E0E9B61A34E1ADCBE866B (void);
// 0x00000654 System.Int32 Byn.Awrtc.Native.DirectMemoryFrame::get_Height()
extern void DirectMemoryFrame_get_Height_mB579BB0ED4427585E5CF9D0C669576942F2B6D21 (void);
// 0x00000655 System.Int32 Byn.Awrtc.Native.DirectMemoryFrame::get_Width()
extern void DirectMemoryFrame_get_Width_m9ADE89E4C2BB5CBCE6B75B15D74629E5C6737C8A (void);
// 0x00000656 System.Boolean Byn.Awrtc.Native.DirectMemoryFrame::get_IsTopRowFirst()
extern void DirectMemoryFrame_get_IsTopRowFirst_m1B4D1840079EF37104A9CAE2EC11F536BF0BE7D4 (void);
// 0x00000657 System.Int32 Byn.Awrtc.Native.DirectMemoryFrame::get_Rotation()
extern void DirectMemoryFrame_get_Rotation_m680A6595A5F4C53EC7EF4C0DBC3199A8C204C39A (void);
// 0x00000658 System.Void Byn.Awrtc.Native.DirectMemoryFrame::.ctor(WebRtcCSharp.PollingMediaStreamRef)
extern void DirectMemoryFrame__ctor_mDA836D7B1ECDB1A91AE7A3CEC999AE800D3DBD4E (void);
// 0x00000659 System.IntPtr Byn.Awrtc.Native.DirectMemoryFrame::GetIntPtr()
extern void DirectMemoryFrame_GetIntPtr_m72BDEC37288DF869B61CE646D5837FD2F213AFE9 (void);
// 0x0000065A System.Int32 Byn.Awrtc.Native.DirectMemoryFrame::GetSize()
extern void DirectMemoryFrame_GetSize_mD0C86AD7A6FDDB9AA3DBB66DD2C9870E7B928F50 (void);
// 0x0000065B System.Void Byn.Awrtc.Native.DirectMemoryFrame::Dispose()
extern void DirectMemoryFrame_Dispose_m24586F5732667C18FFF427B184A8D5FD5D8DBA26 (void);
// 0x0000065C System.Boolean Byn.Awrtc.Native.InternalDataPeer::get_UseUnifiedPlan()
extern void InternalDataPeer_get_UseUnifiedPlan_mB184F94600810033E5CFF2635ED10A8EB10B5EF7 (void);
// 0x0000065D System.Void Byn.Awrtc.Native.InternalDataPeer::set_UseUnifiedPlan(System.Boolean)
extern void InternalDataPeer_set_UseUnifiedPlan_m7E05874FEAB4D90780351B28E4C5CAC58D906E52 (void);
// 0x0000065E Byn.Awrtc.Native.NativeAwrtcFactory Byn.Awrtc.Native.InternalDataPeer::get_Factory()
extern void InternalDataPeer_get_Factory_mFE5E92CEB8C6539ACB834004B0C8D17C9502E747 (void);
// 0x0000065F System.Void Byn.Awrtc.Native.InternalDataPeer::.ctor(Byn.Awrtc.ConnectionId,Byn.Awrtc.Native.NativeAwrtcFactory)
extern void InternalDataPeer__ctor_m1E5C1B18A3A196E815389B7B07E345201DEC7C51 (void);
// 0x00000660 System.Boolean Byn.Awrtc.Native.InternalDataPeer::ForwardSetupPeer(Byn.Awrtc.IceServer[])
extern void InternalDataPeer_ForwardSetupPeer_mA75A5A711F36017BD2E06F9C5EB502B69CDDF693 (void);
// 0x00000661 System.Void Byn.Awrtc.Native.InternalDataPeer::ForwardSignalingToPeer(System.String)
extern void InternalDataPeer_ForwardSignalingToPeer_mBE72B04B41B990F6040A87CC52B77A48A6BF3B1E (void);
// 0x00000662 System.Void Byn.Awrtc.Native.InternalDataPeer::ForwardStartSignaling()
extern void InternalDataPeer_ForwardStartSignaling_m5B2C427792EA25994892F843D4915D5072EADEF4 (void);
// 0x00000663 System.Void Byn.Awrtc.Native.InternalDataPeer::ForwardOnDispose()
extern void InternalDataPeer_ForwardOnDispose_m08DE19431EFD2248ECEC860E824590D2C9D8CBF5 (void);
// 0x00000664 System.Void Byn.Awrtc.Native.InternalDataPeer::Update()
extern void InternalDataPeer_Update_mADDFD5CC5FA5EA595AC87F855D8754B07E1C47D3 (void);
// 0x00000665 Byn.Awrtc.MessageDataBuffer Byn.Awrtc.Native.InternalDataPeer::ToMessageDataBuffer(WebRtcCSharp.DataBuffer)
extern void InternalDataPeer_ToMessageDataBuffer_mE5CF0FA0FDFF7E66F02230A81935CCE8A3ACB57F (void);
// 0x00000666 System.Boolean Byn.Awrtc.Native.InternalDataPeer::SendData(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void InternalDataPeer_SendData_m4FBB9D74B6829FBE1DE0A9D4C9506D345480A697 (void);
// 0x00000667 System.Int32 Byn.Awrtc.Native.InternalDataPeer::GetBufferedAmount(System.Boolean)
extern void InternalDataPeer_GetBufferedAmount_m33607EF316D707C36ADCF681CF2232F6DFFE5F99 (void);
// 0x00000668 System.Void Byn.Awrtc.Native.InternalDataPeer::.cctor()
extern void InternalDataPeer__cctor_mEE8932C37130E9D2F34AD56BD7B363E8A2990681 (void);
// 0x00000669 Byn.Awrtc.Base.IInternalMediaStream Byn.Awrtc.Native.InternalMediaPeer::get_RemoteStream()
extern void InternalMediaPeer_get_RemoteStream_mB43832E3B3690D97C96163F00BB501964BA5E3E4 (void);
// 0x0000066A System.Void Byn.Awrtc.Native.InternalMediaPeer::.ctor(Byn.Awrtc.ConnectionId,Byn.Awrtc.Native.NativeAwrtcFactory,Byn.Awrtc.MediaConfig)
extern void InternalMediaPeer__ctor_mA21693030A6A64FEE00171DEAA7B48E771D769F9 (void);
// 0x0000066B System.Void Byn.Awrtc.Native.InternalMediaPeer::AddLocalStream(Byn.Awrtc.Base.IInternalMediaStream)
extern void InternalMediaPeer_AddLocalStream_mB882F853D1731BF5F4A49815B3B26D749DD0CB19 (void);
// 0x0000066C System.Void Byn.Awrtc.Native.InternalMediaPeer::RemoveLocalStream(Byn.Awrtc.Base.IInternalMediaStream)
extern void InternalMediaPeer_RemoveLocalStream_m34F0440C600ADD0BF6BF44E59D0C992F1328C5DD (void);
// 0x0000066D Byn.Awrtc.Native.AudioFrames Byn.Awrtc.Native.InternalMediaPeer::DequeueRemoteAudioFrames()
extern void InternalMediaPeer_DequeueRemoteAudioFrames_m802098A14E65CFFAD1A705721590A3FE52A31097 (void);
// 0x0000066E System.Boolean Byn.Awrtc.Native.InternalMediaPeer::HasAudioTrack()
extern void InternalMediaPeer_HasAudioTrack_m561116393A7AC3C0838DDB7C97D632AB09C19DD9 (void);
// 0x0000066F System.Boolean Byn.Awrtc.Native.InternalMediaPeer::HasVideoTrack()
extern void InternalMediaPeer_HasVideoTrack_mCFF24C7DB638FEF2C0E2E59172D7BA2862DD4CD8 (void);
// 0x00000670 System.Void Byn.Awrtc.Native.InternalMediaPeer::SetVolume(System.Double)
extern void InternalMediaPeer_SetVolume_m96793890E606DBD931F3D0D70F664CFC9D0FB0B1 (void);
// 0x00000671 Byn.Awrtc.IFrame Byn.Awrtc.Native.InternalMediaPeer::TryGetFrame()
extern void InternalMediaPeer_TryGetFrame_mB4412C843E1EA39EDEC6173D364DB6B58588718C (void);
// 0x00000672 Byn.Awrtc.IFrame Byn.Awrtc.Native.InternalMediaPeer::FrameToi420Buffer(WebRtcCSharp.PollingMediaStreamRef)
extern void InternalMediaPeer_FrameToi420Buffer_m8697F6704E6DC8E45F6A8B0B68885108752E9A76 (void);
// 0x00000673 System.Void Byn.Awrtc.Native.InternalMediaPeer::FrameToBuffer(WebRtcCSharp.PollingMediaStreamRef,Byn.Awrtc.BufferedFrame&)
extern void InternalMediaPeer_FrameToBuffer_m5B1F8D8F26836FDE1B894EADD23DD3525F2C5BCA (void);
// 0x00000674 WebRtcCSharp.PollingMediaStreamRef Byn.Awrtc.Native.InternalMediaStream::GetInternalStream()
extern void InternalMediaStream_GetInternalStream_m77133FB747F5138A2ECE45F5EE3521C10B2444B4 (void);
// 0x00000675 System.Void Byn.Awrtc.Native.InternalMediaStream::.ctor(Byn.Awrtc.Native.NativeAwrtcFactory,Byn.Awrtc.MediaConfig)
extern void InternalMediaStream__ctor_m5B2E2149313544A5DAC2C95D8EF2288C2C37A926 (void);
// 0x00000676 System.Void Byn.Awrtc.Native.InternalMediaStream::.ctor(Byn.Awrtc.Native.NativeAwrtcFactory,Byn.Awrtc.MediaConfig,WebRtcCSharp.PollingMediaStreamRef)
extern void InternalMediaStream__ctor_m13D130206AF3B28CDE088E26CFE4D43D9617F6BD (void);
// 0x00000677 System.Boolean Byn.Awrtc.Native.InternalMediaStream::Setup(System.String&)
extern void InternalMediaStream_Setup_m293F599D5AAC2A15C88827C1913DB89A6049A34B (void);
// 0x00000678 System.Void Byn.Awrtc.Native.InternalMediaStream::Dispose()
extern void InternalMediaStream_Dispose_mC43341344DFC04AB81131BE8926177988AE8EC57 (void);
// 0x00000679 System.Boolean Byn.Awrtc.Native.InternalMediaStream::IsMute()
extern void InternalMediaStream_IsMute_mECDA5DED58157BD6081461A414208DC909972B53 (void);
// 0x0000067A System.Void Byn.Awrtc.Native.InternalMediaStream::SetMute(System.Boolean)
extern void InternalMediaStream_SetMute_m2E95856FFC501117B919256D22EBBC9B3F0825CC (void);
// 0x0000067B System.Void Byn.Awrtc.Native.InternalMediaStream::SetVolume(System.Double)
extern void InternalMediaStream_SetVolume_mC6BED91E8F4C31ABEE93EFA0EBFA4EB3F9C041BE (void);
// 0x0000067C Byn.Awrtc.IFrame Byn.Awrtc.Native.InternalMediaStream::TryGetFrame()
extern void InternalMediaStream_TryGetFrame_mF364EBE44537A38572544F243FB05321FFC35C85 (void);
// 0x0000067D System.Boolean Byn.Awrtc.Native.InternalMediaStream::HasAudioTrack()
extern void InternalMediaStream_HasAudioTrack_mC7DAAF0856164657B61F993C5BDA1F0BFC505E78 (void);
// 0x0000067E System.Boolean Byn.Awrtc.Native.InternalMediaStream::HasVideoTrack()
extern void InternalMediaStream_HasVideoTrack_m588FFF938008546B29FB3CDC31EB444D0DCEB416 (void);
// 0x0000067F Byn.Awrtc.Native.AudioFrames Byn.Awrtc.Native.AudioFramesUpdateEventArgs::get_Frame()
extern void AudioFramesUpdateEventArgs_get_Frame_m5C7ACE499F1E141BE4DD4E364C24391F5E4FD989 (void);
// 0x00000680 Byn.Awrtc.ConnectionId Byn.Awrtc.Native.AudioFramesUpdateEventArgs::get_ConnectionId()
extern void AudioFramesUpdateEventArgs_get_ConnectionId_mFC1000BD52FDAA9167F6C369CC9248E6A524B4F4 (void);
// 0x00000681 System.Int32 Byn.Awrtc.Native.AudioFramesUpdateEventArgs::get_TrackId()
extern void AudioFramesUpdateEventArgs_get_TrackId_m087D84CAEC8CBD49DF389EFB858EA5033B03D447 (void);
// 0x00000682 System.Boolean Byn.Awrtc.Native.AudioFramesUpdateEventArgs::get_IsRemote()
extern void AudioFramesUpdateEventArgs_get_IsRemote_m009D30BA9963E32E7A3A49A2C784692D5F105550 (void);
// 0x00000683 System.Void Byn.Awrtc.Native.AudioFramesUpdateEventArgs::.ctor(Byn.Awrtc.ConnectionId,Byn.Awrtc.Native.AudioFrames)
extern void AudioFramesUpdateEventArgs__ctor_mDD831305263703944899498BC6B4ECFF92DF472C (void);
// 0x00000684 Byn.Awrtc.Native.NativeAudioOptions Byn.Awrtc.Native.NativeMediaConfig::get_AudioOptions()
extern void NativeMediaConfig_get_AudioOptions_mD41F74A6FF594BF6FAA13A936DE5BA6CAE4B5E93 (void);
// 0x00000685 System.Void Byn.Awrtc.Native.NativeMediaConfig::set_AudioOptions(Byn.Awrtc.Native.NativeAudioOptions)
extern void NativeMediaConfig_set_AudioOptions_m010ACC14B13590A25DF2DD20D7382B0D9225B2DC (void);
// 0x00000686 System.Void Byn.Awrtc.Native.NativeMediaConfig::.ctor()
extern void NativeMediaConfig__ctor_m224DF6CB43453EC7638723BAB89B5C1B750CF9A3 (void);
// 0x00000687 System.Void Byn.Awrtc.Native.NativeMediaConfig::.ctor(Byn.Awrtc.Native.NativeMediaConfig)
extern void NativeMediaConfig__ctor_m5B85A4341EB62D77E7C7BA5D0B84B21ED70891B0 (void);
// 0x00000688 Byn.Awrtc.MediaConfig Byn.Awrtc.Native.NativeMediaConfig::DeepClone()
extern void NativeMediaConfig_DeepClone_m0A518B9C461A6F1F62F89302E1EC9E53003DDD81 (void);
// 0x00000689 System.String Byn.Awrtc.Native.NativeMediaConfig::ToString()
extern void NativeMediaConfig_ToString_mF8E6CF6197F824492B366C4F22822FDE95270B9C (void);
// 0x0000068A System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_echo_cancellation()
extern void NativeAudioOptions_get_echo_cancellation_m9E470CD57F3762AAE0125C24D76D431CACB78024 (void);
// 0x0000068B System.Void Byn.Awrtc.Native.NativeAudioOptions::set_echo_cancellation(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_echo_cancellation_m7E497F153821B2F2E14055F27452AE07B6A2B2B7 (void);
// 0x0000068C System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_extended_filter_aec()
extern void NativeAudioOptions_get_extended_filter_aec_mE3AC7DDF2409B738788793796C4A5DEC8477601E (void);
// 0x0000068D System.Void Byn.Awrtc.Native.NativeAudioOptions::set_extended_filter_aec(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_extended_filter_aec_mCEFC504064FBFB1AE0B80F6687D514B220AA11B2 (void);
// 0x0000068E System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_delay_agnostic_aec()
extern void NativeAudioOptions_get_delay_agnostic_aec_mBF07F2854628EAE68AF532C4293BE75356609D3A (void);
// 0x0000068F System.Void Byn.Awrtc.Native.NativeAudioOptions::set_delay_agnostic_aec(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_delay_agnostic_aec_m992922006A37CEFA18C527E6CD048651B1B67C27 (void);
// 0x00000690 System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_noise_suppression()
extern void NativeAudioOptions_get_noise_suppression_m7AEA2ABAA3872C0CC7A7838CA6FB93CE5867C475 (void);
// 0x00000691 System.Void Byn.Awrtc.Native.NativeAudioOptions::set_noise_suppression(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_noise_suppression_mE5E7466541E3DE460B85857B166A4DDF4865AB9D (void);
// 0x00000692 System.Nullable`1<System.Boolean> Byn.Awrtc.Native.NativeAudioOptions::get_auto_gain_control()
extern void NativeAudioOptions_get_auto_gain_control_m73BFFF9F9172C79BE5198EDB26DBC30350816AA7 (void);
// 0x00000693 System.Void Byn.Awrtc.Native.NativeAudioOptions::set_auto_gain_control(System.Nullable`1<System.Boolean>)
extern void NativeAudioOptions_set_auto_gain_control_m9070DE089F540AEA3C8503E907F32492A352EFE2 (void);
// 0x00000694 System.Void Byn.Awrtc.Native.NativeAudioOptions::.ctor()
extern void NativeAudioOptions__ctor_mD9995B0B2878DAF2A0EFD5A941BDE277468EAFE4 (void);
// 0x00000695 System.Void Byn.Awrtc.Native.NativeAudioOptions::.ctor(Byn.Awrtc.Native.NativeAudioOptions)
extern void NativeAudioOptions__ctor_m95AE9469B55D9324C5820546A703461C573326DC (void);
// 0x00000696 Byn.Awrtc.Native.NativeAudioOptions Byn.Awrtc.Native.NativeAudioOptions::DeepClone()
extern void NativeAudioOptions_DeepClone_mC974B2FD19E96692A10FA06EA23A5C3FC6E37DD3 (void);
// 0x00000697 System.String Byn.Awrtc.Native.NativeAudioOptions::ToString()
extern void NativeAudioOptions_ToString_m9DA1C89E7BD62C9128DED3FE7DD9A402B7F6A503 (void);
// 0x00000698 System.Void Byn.Awrtc.Native.NativeMediaNetwork::.ctor(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[],Byn.Awrtc.Native.NativeAwrtcFactory)
extern void NativeMediaNetwork__ctor_mAC698CDFC1D3B7B3C3C77880EBAEBD89C34A1FFC (void);
// 0x00000699 Byn.Awrtc.Base.IInternalMediaStream Byn.Awrtc.Native.NativeMediaNetwork::CreateLocalStream(Byn.Awrtc.MediaConfig)
extern void NativeMediaNetwork_CreateLocalStream_m57AA1A42117E4773AF72A785507725507CEAF290 (void);
// 0x0000069A Byn.Awrtc.Base.IMediaPeer Byn.Awrtc.Native.NativeMediaNetwork::CreateMediaPeer(Byn.Awrtc.ConnectionId)
extern void NativeMediaNetwork_CreateMediaPeer_mD7D31036824832F0056437B5F61F78C063C6F48A (void);
// 0x0000069B System.Void Byn.Awrtc.Native.NativeMediaNetwork::Dispose(System.Boolean)
extern void NativeMediaNetwork_Dispose_mEE74A270A91995EF6481C1DD3E47A7711D5C4B51 (void);
// 0x0000069C System.Boolean Byn.Awrtc.Native.NativeMediaNetwork::IsMute()
extern void NativeMediaNetwork_IsMute_mEA50DC7D1710AF5BBA0030A29240327F992D20BF (void);
// 0x0000069D System.Void Byn.Awrtc.Native.NativeMediaNetwork::SetMute(System.Boolean)
extern void NativeMediaNetwork_SetMute_m70285B65136E9419630AAEA2F531689B5E933CED (void);
// 0x0000069E System.Void Byn.Awrtc.Native.NativeVideoInput::.ctor(WebRtcCSharp.VideoInputRef)
extern void NativeVideoInput__ctor_mC306DBDF7284880707A267278558885308079B6A (void);
// 0x0000069F System.Void Byn.Awrtc.Native.NativeVideoInput::AddDevice(System.String,System.Int32,System.Int32,System.Int32)
extern void NativeVideoInput_AddDevice_m22BB86D16CE3D74822F56668F58F2E352A7941C2 (void);
// 0x000006A0 System.Void Byn.Awrtc.Native.NativeVideoInput::RemoveDevice(System.String)
extern void NativeVideoInput_RemoveDevice_m066C258D2BDE4DED57A6DECC0C99BB2C8C404D83 (void);
// 0x000006A1 System.Boolean Byn.Awrtc.Native.NativeVideoInput::UpdateFrame(System.String,System.Byte[],System.Int32,System.Int32,WebRtcCSharp.VideoType,System.Int32,System.Boolean)
extern void NativeVideoInput_UpdateFrame_mD6BC1855ABC07BF19401071A5483920BD355F18F (void);
// 0x000006A2 System.Boolean Byn.Awrtc.Native.NativeVideoInput::UpdateFrame(System.String,System.IntPtr,System.Int32,System.Int32,System.Int32,WebRtcCSharp.VideoType,System.Int32,System.Boolean)
extern void NativeVideoInput_UpdateFrame_mAE48E07F938D57DB9E17F578B92BB2D3AF9E4DD2 (void);
// 0x000006A3 System.Void Byn.Awrtc.Native.NativeVideoInput::Dispose()
extern void NativeVideoInput_Dispose_mF71BD96D39667DF64FC5DE0F0332F639ADF9ABD4 (void);
// 0x000006A4 System.Void Byn.Awrtc.Native.NativeWebRtcCall::.ctor(Byn.Awrtc.Native.NativeAwrtcFactory,Byn.Awrtc.NetworkConfig,Byn.Awrtc.IBasicNetwork)
extern void NativeWebRtcCall__ctor_m6A0D5B34D59737A268FB65CB3D4A4538F20F0300 (void);
// 0x000006A5 Byn.Awrtc.IMediaNetwork Byn.Awrtc.Native.NativeWebRtcCall::CreateNetwork(Byn.Awrtc.IBasicNetwork)
extern void NativeWebRtcCall_CreateNetwork_mB7FFD769DDDADBA98DE5952C4632DB5E01274791 (void);
// 0x000006A6 System.Void Byn.Awrtc.Native.NativeWebRtcCall::Dispose(System.Boolean)
extern void NativeWebRtcCall_Dispose_m60B46E538027792C0F28F5057650315F2985D25D (void);
// 0x000006A7 System.Boolean Byn.Awrtc.Native.NativeAwrtcFactory::get_IsInitialized()
extern void NativeAwrtcFactory_get_IsInitialized_mCC4DE63D0B7C2C44083ED20909F46F5A88605BC9 (void);
// 0x000006A8 WebRtcCSharp.RTCPeerConnectionFactoryRef Byn.Awrtc.Native.NativeAwrtcFactory::get_NativeFactory()
extern void NativeAwrtcFactory_get_NativeFactory_m02F4717D097D433CCF8ECB58908C4CE46C76E02E (void);
// 0x000006A9 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::add_LastCallDisposed(System.Action)
extern void NativeAwrtcFactory_add_LastCallDisposed_mC5736E1422112E83EA3C151F543754DC7C122FA5 (void);
// 0x000006AA System.Void Byn.Awrtc.Native.NativeAwrtcFactory::remove_LastCallDisposed(System.Action)
extern void NativeAwrtcFactory_remove_LastCallDisposed_mF10A6AAE36C257A0735933474304958EC79FAD00 (void);
// 0x000006AB Byn.Awrtc.Native.NativeVideoInput Byn.Awrtc.Native.NativeAwrtcFactory::get_VideoInput()
extern void NativeAwrtcFactory_get_VideoInput_m765970E2E772420F225F97A4F5077739E5F9D2BF (void);
// 0x000006AC System.Void Byn.Awrtc.Native.NativeAwrtcFactory::.ctor()
extern void NativeAwrtcFactory__ctor_m0A8E24DBD64DE3913976C7E12160B43B2039FD62 (void);
// 0x000006AD System.Void Byn.Awrtc.Native.NativeAwrtcFactory::Initialize()
extern void NativeAwrtcFactory_Initialize_mF82645E5FB32638A7A81AE27AFE71739EB19B5D9 (void);
// 0x000006AE System.Void Byn.Awrtc.Native.NativeAwrtcFactory::TryStaticInit()
extern void NativeAwrtcFactory_TryStaticInit_m922B8A8CD830EA1C7DEE8EE3BEDB54532793A6B8 (void);
// 0x000006AF System.Void Byn.Awrtc.Native.NativeAwrtcFactory::OnNativeLog(System.String)
extern void NativeAwrtcFactory_OnNativeLog_mC4EA544FC288667D0739863152F3C2FFFE6325D8 (void);
// 0x000006B0 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::SetNativeLogToSLog(WebRtcCSharp.LoggingSeverity)
extern void NativeAwrtcFactory_SetNativeLogToSLog_m1B8F12E0DABFCCC78CFE83F33A79CB2C80B5866C (void);
// 0x000006B1 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::SetNativeLogLevel(WebRtcCSharp.LoggingSeverity)
extern void NativeAwrtcFactory_SetNativeLogLevel_m5BC71422EBB8B2CA251A3B7D81695359D5BD3821 (void);
// 0x000006B2 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::SetNativeLogLevelInternal(WebRtcCSharp.LoggingSeverity)
extern void NativeAwrtcFactory_SetNativeLogLevelInternal_mB9BF494730D3936BCE5EEAE9AD564E8297D6E9A6 (void);
// 0x000006B3 System.String[] Byn.Awrtc.Native.NativeAwrtcFactory::GetVideoDevices()
extern void NativeAwrtcFactory_GetVideoDevices_mBA1A2545BAF6675ED4F672107D861C7E2204A849 (void);
// 0x000006B4 Byn.Awrtc.ICall Byn.Awrtc.Native.NativeAwrtcFactory::CreateCall(Byn.Awrtc.NetworkConfig)
extern void NativeAwrtcFactory_CreateCall_m6EFAB450F5FA00219A7FD59963A00393092AC4C8 (void);
// 0x000006B5 Byn.Awrtc.ICall Byn.Awrtc.Native.NativeAwrtcFactory::CreateCall(Byn.Awrtc.NetworkConfig,Byn.Awrtc.IBasicNetwork)
extern void NativeAwrtcFactory_CreateCall_m57F3DDD3AE997CAF3B023495C17C6548FA58A9B9 (void);
// 0x000006B6 Byn.Awrtc.IMediaNetwork Byn.Awrtc.Native.NativeAwrtcFactory::CreateMediaNetwork(Byn.Awrtc.NetworkConfig)
extern void NativeAwrtcFactory_CreateMediaNetwork_m9664DD530CA03C9C6171E6049AC8A8E8B44BFA7F (void);
// 0x000006B7 Byn.Awrtc.IWebRtcNetwork Byn.Awrtc.Native.NativeAwrtcFactory::CreateBasicNetwork(System.String,Byn.Awrtc.IceServer[])
extern void NativeAwrtcFactory_CreateBasicNetwork_m7917BC319757ED71D398FA408A2FF001CB2ED148 (void);
// 0x000006B8 Byn.Awrtc.IWebRtcNetwork Byn.Awrtc.Native.NativeAwrtcFactory::CreateBasicNetwork(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[])
extern void NativeAwrtcFactory_CreateBasicNetwork_mD68D75ADF4A5F256C44C475D387E589BCFD5DA00 (void);
// 0x000006B9 Byn.Awrtc.IMediaNetwork Byn.Awrtc.Native.NativeAwrtcFactory::CreateMediaNetwork(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[])
extern void NativeAwrtcFactory_CreateMediaNetwork_m2DF5F9091264946D33A832F13B318B15513563DC (void);
// 0x000006BA Byn.Dwrtc.ICsPeer Byn.Awrtc.Native.NativeAwrtcFactory::CreatePeer(WebRtcCSharp.PeerConnectionInterface/RTCConfiguration)
extern void NativeAwrtcFactory_CreatePeer_mAFB668E743B38F25452941968765CA5983BF0F07 (void);
// 0x000006BB Byn.Dwrtc.ICsMediaStream Byn.Awrtc.Native.NativeAwrtcFactory::CreateStream(WebRtcCSharp.MediaConstraints)
extern void NativeAwrtcFactory_CreateStream_m43ACA3EC59C0254B14DC33DDA032AEB923A0B610 (void);
// 0x000006BC System.Void Byn.Awrtc.Native.NativeAwrtcFactory::OnCallDisposed(Byn.Awrtc.Native.NativeWebRtcCall)
extern void NativeAwrtcFactory_OnCallDisposed_mD3FF24C7651358DD989A41B831FFCB8E091B44E0 (void);
// 0x000006BD System.Void Byn.Awrtc.Native.NativeAwrtcFactory::OnNetworkDisposed(Byn.Awrtc.Native.NativeMediaNetwork)
extern void NativeAwrtcFactory_OnNetworkDisposed_mCD6A6B6FAC2D337F6D546FE0E5991DD66AFEE1F9 (void);
// 0x000006BE System.Void Byn.Awrtc.Native.NativeAwrtcFactory::OnNetworkDestroyed(Byn.Awrtc.Native.NativeWebRtcNetwork)
extern void NativeAwrtcFactory_OnNetworkDestroyed_m5E4A6F5ACAF109A406AA71BA6B3CE2B4B828E4E8 (void);
// 0x000006BF System.Void Byn.Awrtc.Native.NativeAwrtcFactory::Dispose(System.Boolean)
extern void NativeAwrtcFactory_Dispose_m5B7FE0B7726CE047797BE156FDE4A6B180B44FDF (void);
// 0x000006C0 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::Dispose()
extern void NativeAwrtcFactory_Dispose_mFD278D7BA01BF4E2A4DC816106FFE9BC94CAE527 (void);
// 0x000006C1 System.Boolean Byn.Awrtc.Native.NativeAwrtcFactory::CanSelectVideoDevice()
extern void NativeAwrtcFactory_CanSelectVideoDevice_mA05CDFC7FEC1F3E6F598C3D7CCF9393705BCF69D (void);
// 0x000006C2 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::SetGlobalSpeakerMute(System.Boolean)
extern void NativeAwrtcFactory_SetGlobalSpeakerMute_mC712CA3638FAFB0A8CEFA2B4D9B4B341A6A901AA (void);
// 0x000006C3 System.Boolean Byn.Awrtc.Native.NativeAwrtcFactory::GetGlobalSpeakerMute()
extern void NativeAwrtcFactory_GetGlobalSpeakerMute_m813FD92F9AA518C87E7C25C27F8F7D98CB301CD1 (void);
// 0x000006C4 System.Boolean Byn.Awrtc.Native.NativeAwrtcFactory::HasGlobalSpeakerMute()
extern void NativeAwrtcFactory_HasGlobalSpeakerMute_mA22789FCB5422367DE2CAFB62D6C40BCD4807998 (void);
// 0x000006C5 System.String Byn.Awrtc.Native.NativeAwrtcFactory::GetVersion()
extern void NativeAwrtcFactory_GetVersion_m06B9809CBADF5AAA909E292F4668C7FC4BCD659B (void);
// 0x000006C6 System.String Byn.Awrtc.Native.NativeAwrtcFactory::GetWrapperVersion()
extern void NativeAwrtcFactory_GetWrapperVersion_mC174E74A4DE01C7AE409306574BBE96FF7E08A85 (void);
// 0x000006C7 System.String Byn.Awrtc.Native.NativeAwrtcFactory::GetWebRtcVersion()
extern void NativeAwrtcFactory_GetWebRtcVersion_m31AEBCC7C507045BB35454DD26C9B15747D6D337 (void);
// 0x000006C8 System.Void Byn.Awrtc.Native.NativeAwrtcFactory::.cctor()
extern void NativeAwrtcFactory__cctor_m918E9117E8C10BF5AD5EE8E06FAE975EEC9A1837 (void);
// 0x000006C9 System.Void Byn.Awrtc.Native.NativeWebRtcCallFactory::.ctor()
extern void NativeWebRtcCallFactory__ctor_m491560E86C777C93191B1A131AEBCA980EF96331 (void);
// 0x000006CA Byn.Awrtc.Native.NativeAwrtcFactory Byn.Awrtc.Native.NativeWebRtcNetwork::get_Factory()
extern void NativeWebRtcNetwork_get_Factory_m1DFE2DD1000AB837D3CA9AFCD2BC0EF8145FD67B (void);
// 0x000006CB System.Void Byn.Awrtc.Native.NativeWebRtcNetwork::.ctor(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[],Byn.Awrtc.Native.NativeAwrtcFactory)
extern void NativeWebRtcNetwork__ctor_m6569408556F18611155C0944CFA03E838ABF8538 (void);
// 0x000006CC System.Void Byn.Awrtc.Native.NativeWebRtcNetwork::TestValidity()
extern void NativeWebRtcNetwork_TestValidity_m4F298D3DD78F50CA4658215931B5CAB7F5E11956 (void);
// 0x000006CD Byn.Awrtc.Base.IDataPeer Byn.Awrtc.Native.NativeWebRtcNetwork::CreatePeer(Byn.Awrtc.ConnectionId,Byn.Awrtc.IceServer[])
extern void NativeWebRtcNetwork_CreatePeer_mC3F9429598142D73635CEED1B16478BEF51AB576 (void);
// 0x000006CE System.Void Byn.Awrtc.Native.NativeWebRtcNetwork::Dispose(System.Boolean)
extern void NativeWebRtcNetwork_Dispose_mE5494993BCB0CEA74540EE60CC2689C0D1B0F0F8 (void);
// 0x000006CF System.Void Byn.Awrtc.Native.NativeWebRtcNetworkFactory::.ctor()
extern void NativeWebRtcNetworkFactory__ctor_mEB56C87CDB1441E4E5E7566D0838638F1212DC10 (void);
// 0x000006D0 WebRtcCSharp.MediaConstraints Byn.Awrtc.Native.WrapperConverter::ConfigToConstraints(Byn.Awrtc.MediaConfig)
extern void WrapperConverter_ConfigToConstraints_m4C9239BDD0EFFFED055E3979C37B290B6D8DFD29 (void);
// 0x000006D1 WebRtcCSharp.AudioOptions Byn.Awrtc.Native.WrapperConverter::ConfigToAudioOptions(Byn.Awrtc.MediaConfig)
extern void WrapperConverter_ConfigToAudioOptions_mB32D4EE3C87450063BC1D3506D0D71BB6BE76CAF (void);
// 0x000006D2 Byn.Awrtc.Base.WebsocketReadyState Byn.Awrtc.Native.WebsocketSharpClient::get_ReadyState()
extern void WebsocketSharpClient_get_ReadyState_m1C42EBA5B4C7305D6822EB1E4514E0576F68A91D (void);
// 0x000006D3 System.Net.Security.RemoteCertificateValidationCallback Byn.Awrtc.Native.WebsocketSharpClient::get_CertCallback()
extern void WebsocketSharpClient_get_CertCallback_m3A72A88D8C98CC01DC4A3337E746E230D2F5EEA5 (void);
// 0x000006D4 System.Void Byn.Awrtc.Native.WebsocketSharpClient::set_CertCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern void WebsocketSharpClient_set_CertCallback_m6B44F914675BFF0C01904BE82842E9460180301F (void);
// 0x000006D5 System.Void Byn.Awrtc.Native.WebsocketSharpClient::add_OnOpen(Byn.Awrtc.Base.OnOpenCallback)
extern void WebsocketSharpClient_add_OnOpen_m67DBA342592F3E628B94E31CD61168822D5063FF (void);
// 0x000006D6 System.Void Byn.Awrtc.Native.WebsocketSharpClient::remove_OnOpen(Byn.Awrtc.Base.OnOpenCallback)
extern void WebsocketSharpClient_remove_OnOpen_m7EAF580080F0C84804CDD5361C9C4E2F76168759 (void);
// 0x000006D7 System.Void Byn.Awrtc.Native.WebsocketSharpClient::add_OnClose(Byn.Awrtc.Base.OnCloseCallback)
extern void WebsocketSharpClient_add_OnClose_m2463E7D42D6EBFADE7E5B24F3FCB43853E0B6920 (void);
// 0x000006D8 System.Void Byn.Awrtc.Native.WebsocketSharpClient::remove_OnClose(Byn.Awrtc.Base.OnCloseCallback)
extern void WebsocketSharpClient_remove_OnClose_mCA1DA66B29FE023FF5585E9ACA105BD72429C0E9 (void);
// 0x000006D9 System.Void Byn.Awrtc.Native.WebsocketSharpClient::add_OnError(Byn.Awrtc.Base.OnErrorCallback)
extern void WebsocketSharpClient_add_OnError_m970F166055A8162B45AAA7DB3C776D30C1410647 (void);
// 0x000006DA System.Void Byn.Awrtc.Native.WebsocketSharpClient::remove_OnError(Byn.Awrtc.Base.OnErrorCallback)
extern void WebsocketSharpClient_remove_OnError_mF240C59B8A75D8C8940E00CFE931E805A34DF23E (void);
// 0x000006DB System.Void Byn.Awrtc.Native.WebsocketSharpClient::add_OnMessage(Byn.Awrtc.Base.OnMessageCallback)
extern void WebsocketSharpClient_add_OnMessage_m2B8CEB9151BEE21F5C349D7241984D01668591C0 (void);
// 0x000006DC System.Void Byn.Awrtc.Native.WebsocketSharpClient::remove_OnMessage(Byn.Awrtc.Base.OnMessageCallback)
extern void WebsocketSharpClient_remove_OnMessage_m47C7CB357A93433029157B2B780C9928FE148A56 (void);
// 0x000006DD System.Void Byn.Awrtc.Native.WebsocketSharpClient::add_OnTextMessage(Byn.Awrtc.Base.OnTextMessageCallback)
extern void WebsocketSharpClient_add_OnTextMessage_mDAE6A59098E8C68BF7213FB56F8C543220F1F67F (void);
// 0x000006DE System.Void Byn.Awrtc.Native.WebsocketSharpClient::remove_OnTextMessage(Byn.Awrtc.Base.OnTextMessageCallback)
extern void WebsocketSharpClient_remove_OnTextMessage_m83FA0E4EC25C577BE7C7C50AC1A866BE0C1A2DBF (void);
// 0x000006DF System.Void Byn.Awrtc.Native.WebsocketSharpClient::.ctor()
extern void WebsocketSharpClient__ctor_m4C7A402886B32175DF7D219CACA261942BAE65C8 (void);
// 0x000006E0 System.Void Byn.Awrtc.Native.WebsocketSharpClient::Connect(System.String)
extern void WebsocketSharpClient_Connect_m3241467D054C9F5E1F8013D0D5BEFD9230DCD125 (void);
// 0x000006E1 System.Boolean Byn.Awrtc.Native.WebsocketSharpClient::IsAlive()
extern void WebsocketSharpClient_IsAlive_m6003C15A6D14E3C1822181AA5376D26F47E21F8F (void);
// 0x000006E2 System.Void Byn.Awrtc.Native.WebsocketSharpClient::Send(System.Byte[])
extern void WebsocketSharpClient_Send_m07E8F072ECBD3C8EC3FAF7B78CDEF0FAD441A7A7 (void);
// 0x000006E3 System.Void Byn.Awrtc.Native.WebsocketSharpClient::Send(System.String)
extern void WebsocketSharpClient_Send_m611ECA2FF77A93980F778463217727CC1A2CF57E (void);
// 0x000006E4 System.Void Byn.Awrtc.Native.WebsocketSharpClient::OnWebsocketOnOpen(System.Object,System.EventArgs)
extern void WebsocketSharpClient_OnWebsocketOnOpen_m21529C17F81EFFBDFF412D78441295A486673A28 (void);
// 0x000006E5 System.Void Byn.Awrtc.Native.WebsocketSharpClient::OnWebsocketOnClose(System.Object,WebSocketSharpUnityMod.CloseEventArgs)
extern void WebsocketSharpClient_OnWebsocketOnClose_mF12A7D749D647C071B9F1F7C5C3856C0095634E6 (void);
// 0x000006E6 System.Void Byn.Awrtc.Native.WebsocketSharpClient::OnWebsocketOnMessage(System.Object,WebSocketSharpUnityMod.MessageEventArgs)
extern void WebsocketSharpClient_OnWebsocketOnMessage_mD0CC276AC7BD43FA961F5E34E7BF3334C6656FB9 (void);
// 0x000006E7 System.Void Byn.Awrtc.Native.WebsocketSharpClient::OnWebsocketOnError(System.Object,WebSocketSharpUnityMod.ErrorEventArgs)
extern void WebsocketSharpClient_OnWebsocketOnError_m91964AF587F12AC27B7C0524E2264C29B65061B6 (void);
// 0x000006E8 System.Void Byn.Awrtc.Native.WebsocketSharpClient::Dispose(System.Boolean)
extern void WebsocketSharpClient_Dispose_m3B14EB27F6A18E9BF32656D0664B2C92F71FB926 (void);
// 0x000006E9 System.Void Byn.Awrtc.Native.WebsocketSharpClient::Dispose()
extern void WebsocketSharpClient_Dispose_m00B2C2E56FC147C17F7CEF691E804231282DC890 (void);
// 0x000006EA System.Net.Security.RemoteCertificateValidationCallback Byn.Awrtc.Native.WebsocketSharpFactory::get_CertCallbackOverride()
extern void WebsocketSharpFactory_get_CertCallbackOverride_mCA4669F27C8230EB8D543E33286FC4F16B54BA51 (void);
// 0x000006EB System.Void Byn.Awrtc.Native.WebsocketSharpFactory::set_CertCallbackOverride(System.Net.Security.RemoteCertificateValidationCallback)
extern void WebsocketSharpFactory_set_CertCallbackOverride_m8BC5F1AF0AD74BCEE8B7D341FB82F37DCFF75DEF (void);
// 0x000006EC System.Void Byn.Awrtc.Native.WebsocketSharpFactory::.ctor()
extern void WebsocketSharpFactory__ctor_m17EC9CE2605BE0B42F532DD168AFB9A94116FA5E (void);
// 0x000006ED System.Boolean Byn.Awrtc.Native.WebsocketSharpFactory::DefaultCertCallback(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void WebsocketSharpFactory_DefaultCertCallback_m896A4C674BEFB565BC6D3BA0121AA6189E09C51B (void);
// 0x000006EE Byn.Awrtc.Base.IWebsocketClient Byn.Awrtc.Native.WebsocketSharpFactory::Create()
extern void WebsocketSharpFactory_Create_m27E349E36C1EE6EF166FE2E807A70BE8EA0E0027 (void);
// 0x000006EF System.Void Byn.Awrtc.Native.WebsocketSharpFactory::.cctor()
extern void WebsocketSharpFactory__cctor_m6F55E1E68E9D9A93EE6D70C6EEBF0E2BFBA4FE92 (void);
// 0x000006F0 System.Void Byn.Awrtc.Base.AMediaNetwork::.ctor(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[])
extern void AMediaNetwork__ctor_mE3DB6701E59C1664EC9EF1072787A36D51575D75 (void);
// 0x000006F1 Byn.Awrtc.Base.IInternalMediaStream Byn.Awrtc.Base.AMediaNetwork::CreateLocalStream(Byn.Awrtc.MediaConfig)
// 0x000006F2 System.Void Byn.Awrtc.Base.AMediaNetwork::Configure(Byn.Awrtc.MediaConfig)
extern void AMediaNetwork_Configure_m4F5A5FF5A871C96C82FF58E4D01D76B29BB4258E (void);
// 0x000006F3 Byn.Awrtc.Base.IMediaPeer Byn.Awrtc.Base.AMediaNetwork::GetConnectedPeer(Byn.Awrtc.ConnectionId)
extern void AMediaNetwork_GetConnectedPeer_m021DB4A0F024B64C11E340F717A50C45EE3B35F9 (void);
// 0x000006F4 System.Void Byn.Awrtc.Base.AMediaNetwork::OnLocalStreamAdded()
extern void AMediaNetwork_OnLocalStreamAdded_m0FA573AF57F769832AA7BC5DD2ABE0DF953A54E4 (void);
// 0x000006F5 System.Void Byn.Awrtc.Base.AMediaNetwork::AddLocalStreamTo(Byn.Awrtc.Base.IMediaPeer)
extern void AMediaNetwork_AddLocalStreamTo_mD170706D2E9705F230E90203DB573A8A01097345 (void);
// 0x000006F6 System.Void Byn.Awrtc.Base.AMediaNetwork::OnLocalStreamRemoval()
extern void AMediaNetwork_OnLocalStreamRemoval_m28371EFBAC94CAAF8CF89EF4F97D77BEF46EDC05 (void);
// 0x000006F7 Byn.Awrtc.MediaConfigurationState Byn.Awrtc.Base.AMediaNetwork::GetConfigurationState()
extern void AMediaNetwork_GetConfigurationState_mE50BA0F8C0541E47BFDB74380B6C9679DB31FB59 (void);
// 0x000006F8 System.String Byn.Awrtc.Base.AMediaNetwork::GetConfigurationError()
extern void AMediaNetwork_GetConfigurationError_m3D3991C43E3558A9C01A856FEBAE0DF9AF5A174B (void);
// 0x000006F9 System.Void Byn.Awrtc.Base.AMediaNetwork::ResetConfiguration()
extern void AMediaNetwork_ResetConfiguration_mDD6AB011A63501A886850A979EE9087A0AC4EBB6 (void);
// 0x000006FA Byn.Awrtc.Base.IDataPeer Byn.Awrtc.Base.AMediaNetwork::CreatePeer(Byn.Awrtc.ConnectionId,Byn.Awrtc.IceServer[])
extern void AMediaNetwork_CreatePeer_m32B4A257502C8EDFFFC223C74622977D0AC0921D (void);
// 0x000006FB Byn.Awrtc.Base.IMediaPeer Byn.Awrtc.Base.AMediaNetwork::CreateMediaPeer(Byn.Awrtc.ConnectionId)
// 0x000006FC Byn.Awrtc.IFrame Byn.Awrtc.Base.AMediaNetwork::TryGetFrame(Byn.Awrtc.ConnectionId)
extern void AMediaNetwork_TryGetFrame_mB05650155E20A3D4441320C800EC8CC83075BFD1 (void);
// 0x000006FD System.Void Byn.Awrtc.Base.AMediaNetwork::SetVolume(System.Double,Byn.Awrtc.ConnectionId)
extern void AMediaNetwork_SetVolume_m4F07DFFAF7ECF5DF6E2A08C57A7A42CE3CDBCAA9 (void);
// 0x000006FE System.Void Byn.Awrtc.Base.AMediaNetwork::Dispose(System.Boolean)
extern void AMediaNetwork_Dispose_mA57660044DE2EE0A8FADA8C961BF722C7E2A22A5 (void);
// 0x000006FF System.Void Byn.Awrtc.Base.AMediaNetwork::DisposeLocalStream()
extern void AMediaNetwork_DisposeLocalStream_mB210696276C4C3F37EADBCF8190CC59307B02D8B (void);
// 0x00000700 System.Boolean Byn.Awrtc.Base.AMediaNetwork::HasAudioTrack(Byn.Awrtc.ConnectionId)
extern void AMediaNetwork_HasAudioTrack_mFCC947A7BCFE6212A34BBB66AC3D95474B5A014F (void);
// 0x00000701 System.Boolean Byn.Awrtc.Base.AMediaNetwork::HasVideoTrack(Byn.Awrtc.ConnectionId)
extern void AMediaNetwork_HasVideoTrack_m7D6B421BFE9F8AAD89E395D7A2CF7175690D18F6 (void);
// 0x00000702 System.Boolean Byn.Awrtc.Base.AMediaNetwork::IsMute()
// 0x00000703 System.Void Byn.Awrtc.Base.AMediaNetwork::SetMute(System.Boolean)
// 0x00000704 System.Collections.Generic.Dictionary`2<Byn.Awrtc.ConnectionId,Byn.Awrtc.Base.IDataPeer> Byn.Awrtc.Base.AWebRtcNetwork::get_IdToConnection()
extern void AWebRtcNetwork_get_IdToConnection_m0F4B8C74AF6BB0FFF2E515B96A50CE8F51D8215E (void);
// 0x00000705 System.Void Byn.Awrtc.Base.AWebRtcNetwork::.ctor(Byn.Awrtc.Base.SignalingConfig,Byn.Awrtc.IceServer[])
extern void AWebRtcNetwork__ctor_mFDD3128A9E69602969B47520073DC3DDA082971E (void);
// 0x00000706 Byn.Awrtc.Base.IDataPeer Byn.Awrtc.Base.AWebRtcNetwork::CreatePeer(Byn.Awrtc.ConnectionId,Byn.Awrtc.IceServer[])
// 0x00000707 System.Void Byn.Awrtc.Base.AWebRtcNetwork::StartServer(System.String)
extern void AWebRtcNetwork_StartServer_m8AAD130FE1030F44631689329656FA5AAEB0C743 (void);
// 0x00000708 System.Void Byn.Awrtc.Base.AWebRtcNetwork::StopServer()
extern void AWebRtcNetwork_StopServer_mCA13E334EE4ED4FCB859AF25F311CFB422B74231 (void);
// 0x00000709 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcNetwork::Connect(System.String)
extern void AWebRtcNetwork_Connect_mCA1BE2627E7682820B0CB227ABE0E229EF284D90 (void);
// 0x0000070A System.Boolean Byn.Awrtc.Base.AWebRtcNetwork::Dequeue(Byn.Awrtc.NetworkEvent&)
extern void AWebRtcNetwork_Dequeue_mF5A7C7F7BEFB6A728E67EB3141B5EA1F2FD18697 (void);
// 0x0000070B System.Boolean Byn.Awrtc.Base.AWebRtcNetwork::Peek(Byn.Awrtc.NetworkEvent&)
extern void AWebRtcNetwork_Peek_m2F6026F57EA84BDFA0D7242CB2DF3AD285C380DD (void);
// 0x0000070C System.Void Byn.Awrtc.Base.AWebRtcNetwork::Flush()
extern void AWebRtcNetwork_Flush_mA9C005AC91E39542A297BE4C2F1BED6B1D7F88E4 (void);
// 0x0000070D System.Boolean Byn.Awrtc.Base.AWebRtcNetwork::SendData(Byn.Awrtc.ConnectionId,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void AWebRtcNetwork_SendData_m91FD5312B0B78194EAFAB4B0AB9A6462ABC26FEB (void);
// 0x0000070E System.Int32 Byn.Awrtc.Base.AWebRtcNetwork::GetBufferedAmount(Byn.Awrtc.ConnectionId,System.Boolean)
extern void AWebRtcNetwork_GetBufferedAmount_m3941687E84D2CCB801D82D82FF79298A1E9DE8DB (void);
// 0x0000070F System.Void Byn.Awrtc.Base.AWebRtcNetwork::Disconnect(Byn.Awrtc.ConnectionId)
extern void AWebRtcNetwork_Disconnect_m69A70320C9F651CBED9C6C1D5026294CEEC2FE98 (void);
// 0x00000710 System.Void Byn.Awrtc.Base.AWebRtcNetwork::Update()
extern void AWebRtcNetwork_Update_m81B7277F82C2DC1A8B4069945F1506E23D9C2230 (void);
// 0x00000711 System.Void Byn.Awrtc.Base.AWebRtcNetwork::Shutdown()
extern void AWebRtcNetwork_Shutdown_m369134B6672D6C6333787CD62FC0A93DD6A00AAE (void);
// 0x00000712 System.Void Byn.Awrtc.Base.AWebRtcNetwork::CheckSignalingState()
extern void AWebRtcNetwork_CheckSignalingState_m5DDDADE115AD76A3EF2F033FDFEC0D75B756F312 (void);
// 0x00000713 System.Void Byn.Awrtc.Base.AWebRtcNetwork::UpdateSignalingNetwork()
extern void AWebRtcNetwork_UpdateSignalingNetwork_m7FEC3CE094968ACE061C0C8904332BFE004CEB51 (void);
// 0x00000714 System.Void Byn.Awrtc.Base.AWebRtcNetwork::UpdatePeers()
extern void AWebRtcNetwork_UpdatePeers_m2AF129713B3B9515260F8FDB19771D1724F6A789 (void);
// 0x00000715 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcNetwork::AddOutgoingConnection(System.String)
extern void AWebRtcNetwork_AddOutgoingConnection_m125E5B7821D55E756BEBEC18E791DF8F7B18996C (void);
// 0x00000716 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcNetwork::AddIncomingConnection(Byn.Awrtc.ConnectionId)
extern void AWebRtcNetwork_AddIncomingConnection_m4EEF391C2D527F58527589DE6E3B0554A04B2CE3 (void);
// 0x00000717 System.Void Byn.Awrtc.Base.AWebRtcNetwork::ConnectionEtablished(Byn.Awrtc.ConnectionId)
extern void AWebRtcNetwork_ConnectionEtablished_m772CF38C7E3F382E90E0CD342AD116A08D95F2F2 (void);
// 0x00000718 System.Void Byn.Awrtc.Base.AWebRtcNetwork::InitialSignalingFailed(Byn.Awrtc.ConnectionId,Byn.Awrtc.ErrorInfo)
extern void AWebRtcNetwork_InitialSignalingFailed_m39E0F4F83F6B992E0CE63BC39065D90052D4E12E (void);
// 0x00000719 System.Void Byn.Awrtc.Base.AWebRtcNetwork::HandleDisconnect(Byn.Awrtc.ConnectionId)
extern void AWebRtcNetwork_HandleDisconnect_m0CC74DD6CB55D88D2AEEA9390348AF99B535858B (void);
// 0x0000071A Byn.Awrtc.MessageDataBuffer Byn.Awrtc.Base.AWebRtcNetwork::StringToBuffer(System.String)
extern void AWebRtcNetwork_StringToBuffer_m44D5233C0DACB5A5538A1495585F0E8AFBC6B24A (void);
// 0x0000071B System.String Byn.Awrtc.Base.AWebRtcNetwork::BufferToString(Byn.Awrtc.MessageDataBuffer)
extern void AWebRtcNetwork_BufferToString_m6E136EB6627B2F33971F2FCC939F6B5065AFC43E (void);
// 0x0000071C Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcNetwork::NextConnectionId()
extern void AWebRtcNetwork_NextConnectionId_m4734DE346AB2D3D4DCF02F5337860459DD40AEDF (void);
// 0x0000071D System.Void Byn.Awrtc.Base.AWebRtcNetwork::Dispose(System.Boolean)
extern void AWebRtcNetwork_Dispose_m6A7B89521EAA47B197DFB0DCF3FC1566B27E14FC (void);
// 0x0000071E System.Void Byn.Awrtc.Base.AWebRtcNetwork::Dispose()
extern void AWebRtcNetwork_Dispose_m2C45A5FA264C30F006DDF95E4D56CF3DAF9D7D8F (void);
// 0x0000071F System.Void Byn.Awrtc.Base.AWebRtcNetwork::.cctor()
extern void AWebRtcNetwork__cctor_mB428E57E09617F6172AE68523D3EE90248A33B62 (void);
// 0x00000720 System.Int32 Byn.Awrtc.Base.AWebRtcPeer::NextLocalId()
extern void AWebRtcPeer_NextLocalId_m322FE37335D513C739CB877E33A75343F17CBBAE (void);
// 0x00000721 System.Int32 Byn.Awrtc.Base.AWebRtcPeer::get_LocalId()
extern void AWebRtcPeer_get_LocalId_m104E28E10F6675B95B2BAC6C3CC6B8ADB66C65A6 (void);
// 0x00000722 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.AWebRtcPeer::get_ConnectionId()
extern void AWebRtcPeer_get_ConnectionId_m8210DCE237989466AC07CBCCC59AE566D4E0E89C (void);
// 0x00000723 Byn.Awrtc.Base.SignalingInfo Byn.Awrtc.Base.AWebRtcPeer::get_SignalingInfo()
extern void AWebRtcPeer_get_SignalingInfo_mA4C0FEB40D5E506DC5473A66D7D4B61385FD5D3F (void);
// 0x00000724 System.Void Byn.Awrtc.Base.AWebRtcPeer::set_SignalingInfo(Byn.Awrtc.Base.SignalingInfo)
extern void AWebRtcPeer_set_SignalingInfo_m0EA934D87B954729EA0CCF2A9FFD2FCA47E45196 (void);
// 0x00000725 Byn.Awrtc.Base.PeerConnectionState Byn.Awrtc.Base.AWebRtcPeer::get_ConnectionState()
extern void AWebRtcPeer_get_ConnectionState_m98868F73EE70FF87F3820FB3D95AFFBED75E3E3F (void);
// 0x00000726 Byn.Awrtc.Base.PeerSignalingState Byn.Awrtc.Base.AWebRtcPeer::get_SignalingState()
extern void AWebRtcPeer_get_SignalingState_m3C1D14B5A54150659ECD60BACEC953603F1866CC (void);
// 0x00000727 System.Boolean Byn.Awrtc.Base.AWebRtcPeer::get_WasRenegotiationRequested()
extern void AWebRtcPeer_get_WasRenegotiationRequested_mB76F42CE91F2B6A50F75E8F43F7525649B2BE19C (void);
// 0x00000728 System.Void Byn.Awrtc.Base.AWebRtcPeer::NegotiateSignaling()
extern void AWebRtcPeer_NegotiateSignaling_mC3853F146C19CE2C18FFA350E0A2CADAA51198F2 (void);
// 0x00000729 System.Void Byn.Awrtc.Base.AWebRtcPeer::EnqueueOutgoingSignaling(System.String)
extern void AWebRtcPeer_EnqueueOutgoingSignaling_m7B9A8766C695E026B48E88B52EE32EE181287D87 (void);
// 0x0000072A System.Void Byn.Awrtc.Base.AWebRtcPeer::ForwardOnDispose()
// 0x0000072B System.Void Byn.Awrtc.Base.AWebRtcPeer::UpdateConnectionState(Byn.Awrtc.Base.PeerConnectionState)
extern void AWebRtcPeer_UpdateConnectionState_m2BE880DE2E992813841599FBC52E4FC9A523A6AE (void);
// 0x0000072C System.Void Byn.Awrtc.Base.AWebRtcPeer::UpdateSignalingState(Byn.Awrtc.Base.PeerSignalingState)
extern void AWebRtcPeer_UpdateSignalingState_mD0EEF1F42A8B3DAE659633293E5F946B644E7657 (void);
// 0x0000072D System.Boolean Byn.Awrtc.Base.AWebRtcPeer::IsNumber(System.String)
extern void AWebRtcPeer_IsNumber_mC64282E9A1142BC8612257E339B32E56E25929F1 (void);
// 0x0000072E System.Void Byn.Awrtc.Base.AWebRtcPeer::HandleSignalingNegotiation(System.String)
extern void AWebRtcPeer_HandleSignalingNegotiation_m480BC576E36D5F09FEB49873BF0F3BB3CA0DBD23 (void);
// 0x0000072F System.Void Byn.Awrtc.Base.AWebRtcPeer::.ctor(Byn.Awrtc.ConnectionId)
extern void AWebRtcPeer__ctor_mF15523F56098A03F061F20F5688A88CCA95CFA10 (void);
// 0x00000730 System.Boolean Byn.Awrtc.Base.AWebRtcPeer::SetupPeer(Byn.Awrtc.IceServer[])
extern void AWebRtcPeer_SetupPeer_m423502843F06A7810E8F2C0D58ACF84CAAC4821B (void);
// 0x00000731 System.Boolean Byn.Awrtc.Base.AWebRtcPeer::ForwardSetupPeer(Byn.Awrtc.IceServer[])
// 0x00000732 System.Void Byn.Awrtc.Base.AWebRtcPeer::AddSignalingMessage(System.String)
extern void AWebRtcPeer_AddSignalingMessage_m1E2CF5E6D2C5839F95B3D41F0612DC2E75D16AE6 (void);
// 0x00000733 System.Void Byn.Awrtc.Base.AWebRtcPeer::ForwardSignalingToPeer(System.String)
// 0x00000734 System.Boolean Byn.Awrtc.Base.AWebRtcPeer::DequeueSignalingMessage(System.String&)
extern void AWebRtcPeer_DequeueSignalingMessage_m5CCD21B214196CF917EC59569ACDA471ADF5EA38 (void);
// 0x00000735 System.Boolean Byn.Awrtc.Base.AWebRtcPeer::DequeueEvent(Byn.Awrtc.NetworkEvent&)
extern void AWebRtcPeer_DequeueEvent_m512EB58CB7B447132AF4D5FB877749A50D2D5CC3 (void);
// 0x00000736 System.Void Byn.Awrtc.Base.AWebRtcPeer::Update()
// 0x00000737 System.Void Byn.Awrtc.Base.AWebRtcPeer::EnqueueOutgoingEvent(Byn.Awrtc.NetworkEvent)
extern void AWebRtcPeer_EnqueueOutgoingEvent_m7CDD57FC6F72E9CD62A666054A4DD332907E342D (void);
// 0x00000738 System.Void Byn.Awrtc.Base.AWebRtcPeer::StartSignaling()
extern void AWebRtcPeer_StartSignaling_m11918234476E21A24155D5C6FE576106A737D418 (void);
// 0x00000739 System.Void Byn.Awrtc.Base.AWebRtcPeer::ForwardStartSignaling()
// 0x0000073A System.Void Byn.Awrtc.Base.AWebRtcPeer::L(System.String)
extern void AWebRtcPeer_L_m617158CFA53586F62B399D6180078483C3A534EC (void);
// 0x0000073B System.Void Byn.Awrtc.Base.AWebRtcPeer::LW(System.String)
extern void AWebRtcPeer_LW_m9458EF8EEC312E1F8C8CF3674857F96DFC43CA4D (void);
// 0x0000073C System.Void Byn.Awrtc.Base.AWebRtcPeer::LE(System.String)
extern void AWebRtcPeer_LE_mA30ED2AFB54F131BCEABC7DB0A74BC374B76E518 (void);
// 0x0000073D System.Boolean Byn.Awrtc.Base.AWebRtcPeer::get_IsDisposed()
extern void AWebRtcPeer_get_IsDisposed_m329B97F59A0590AB58825ED34379F518D9D0D591 (void);
// 0x0000073E System.Void Byn.Awrtc.Base.AWebRtcPeer::Dispose()
extern void AWebRtcPeer_Dispose_m5ADD326B9A1FC944C29E1DDC77A82E02139337CF (void);
// 0x0000073F System.Void Byn.Awrtc.Base.AWebRtcPeer::.cctor()
extern void AWebRtcPeer__cctor_mAC6B072AA0109742C99C287EEBB697262D5A27C9 (void);
// 0x00000740 System.Void Byn.Awrtc.Base.WebsocketCloseStatus::.ctor()
extern void WebsocketCloseStatus__ctor_m989E8708B7A205B5C5642B182CC7FD9116820F64 (void);
// 0x00000741 System.Void Byn.Awrtc.Base.WebsocketCloseStatus::.cctor()
extern void WebsocketCloseStatus__cctor_mD394A4F3EA4B8EA454FE750BA235C8AB19FDCB84 (void);
// 0x00000742 System.Void Byn.Awrtc.Base.AWebsocketFactory::SetDefault(Byn.Awrtc.Base.AWebsocketFactory)
extern void AWebsocketFactory_SetDefault_m0415E34A5CD240727748863E30B4EA9F26FE8FE7 (void);
// 0x00000743 Byn.Awrtc.Base.AWebsocketFactory Byn.Awrtc.Base.AWebsocketFactory::get_DefaultFactory()
extern void AWebsocketFactory_get_DefaultFactory_m92D8AB3F471C649972DD30B8CCCFECA984945161 (void);
// 0x00000744 Byn.Awrtc.Base.IWebsocketClient Byn.Awrtc.Base.AWebsocketFactory::CreateDefault()
extern void AWebsocketFactory_CreateDefault_m704CE68BF9F70F446EB2DF32BAC7C37178286272 (void);
// 0x00000745 Byn.Awrtc.Base.IWebsocketClient Byn.Awrtc.Base.AWebsocketFactory::Create()
// 0x00000746 System.Void Byn.Awrtc.Base.AWebsocketFactory::Dispose(System.Boolean)
extern void AWebsocketFactory_Dispose_m71FC40E6903249B93FE521BE023B05EB537DC17A (void);
// 0x00000747 System.Void Byn.Awrtc.Base.AWebsocketFactory::Dispose()
extern void AWebsocketFactory_Dispose_m3EC5E2A0EF4A9F39FA56D649681AB8EE12407461 (void);
// 0x00000748 System.Void Byn.Awrtc.Base.AWebsocketFactory::.ctor()
extern void AWebsocketFactory__ctor_m457213DA9AEC799C397E551C9BBCD85BA8C6DA5C (void);
// 0x00000749 System.Boolean Byn.Awrtc.Base.IInternalMediaStream::Setup(System.String&)
// 0x0000074A System.Boolean Byn.Awrtc.Base.IInternalMediaStream::HasAudioTrack()
// 0x0000074B System.Boolean Byn.Awrtc.Base.IInternalMediaStream::HasVideoTrack()
// 0x0000074C System.Void Byn.Awrtc.Base.IInternalMediaStream::SetVolume(System.Double)
// 0x0000074D System.Boolean Byn.Awrtc.Base.IInternalMediaStream::IsMute()
// 0x0000074E System.Void Byn.Awrtc.Base.IInternalMediaStream::SetMute(System.Boolean)
// 0x0000074F Byn.Awrtc.IFrame Byn.Awrtc.Base.IInternalMediaStream::TryGetFrame()
// 0x00000750 Byn.Awrtc.Base.PeerConnectionState Byn.Awrtc.Base.IInternalPeer::get_ConnectionState()
// 0x00000751 Byn.Awrtc.Base.PeerSignalingState Byn.Awrtc.Base.IInternalPeer::get_SignalingState()
// 0x00000752 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.IInternalPeer::get_ConnectionId()
// 0x00000753 Byn.Awrtc.Base.SignalingInfo Byn.Awrtc.Base.IInternalPeer::get_SignalingInfo()
// 0x00000754 System.Void Byn.Awrtc.Base.IInternalPeer::set_SignalingInfo(Byn.Awrtc.Base.SignalingInfo)
// 0x00000755 System.Boolean Byn.Awrtc.Base.IInternalPeer::SetupPeer(Byn.Awrtc.IceServer[])
// 0x00000756 System.Int32 Byn.Awrtc.Base.IInternalPeer::get_LocalId()
// 0x00000757 System.Boolean Byn.Awrtc.Base.IInternalPeer::get_WasRenegotiationRequested()
// 0x00000758 System.Void Byn.Awrtc.Base.IInternalPeer::NegotiateSignaling()
// 0x00000759 System.Void Byn.Awrtc.Base.IInternalPeer::AddSignalingMessage(System.String)
// 0x0000075A System.Boolean Byn.Awrtc.Base.IInternalPeer::DequeueSignalingMessage(System.String&)
// 0x0000075B System.Void Byn.Awrtc.Base.IInternalPeer::Dispose()
// 0x0000075C System.Void Byn.Awrtc.Base.IInternalPeer::StartSignaling()
// 0x0000075D System.Void Byn.Awrtc.Base.IInternalPeer::Update()
// 0x0000075E System.Boolean Byn.Awrtc.Base.IInternalPeer::DequeueEvent(Byn.Awrtc.NetworkEvent&)
// 0x0000075F System.Boolean Byn.Awrtc.Base.IDataPeer::SendData(System.Byte[],System.Int32,System.Int32,System.Boolean)
// 0x00000760 System.Int32 Byn.Awrtc.Base.IDataPeer::GetBufferedAmount(System.Boolean)
// 0x00000761 Byn.Awrtc.Base.IInternalMediaStream Byn.Awrtc.Base.IMediaPeer::get_RemoteStream()
// 0x00000762 System.Void Byn.Awrtc.Base.IMediaPeer::AddLocalStream(Byn.Awrtc.Base.IInternalMediaStream)
// 0x00000763 System.Void Byn.Awrtc.Base.IMediaPeer::RemoveLocalStream(Byn.Awrtc.Base.IInternalMediaStream)
// 0x00000764 System.Void Byn.Awrtc.Base.OnErrorCallback::.ctor(System.Object,System.IntPtr)
extern void OnErrorCallback__ctor_m721B76328F8C6B3428C7B72E96F2E1E7E7FDAF76 (void);
// 0x00000765 System.Void Byn.Awrtc.Base.OnErrorCallback::Invoke(System.Object,System.String)
extern void OnErrorCallback_Invoke_m17E6585C7FA1B54FD6CE391662B876CE3F225A3B (void);
// 0x00000766 System.IAsyncResult Byn.Awrtc.Base.OnErrorCallback::BeginInvoke(System.Object,System.String,System.AsyncCallback,System.Object)
extern void OnErrorCallback_BeginInvoke_mD2D4D9E3D74A0D5F12509F02529225F1221F85BF (void);
// 0x00000767 System.Void Byn.Awrtc.Base.OnErrorCallback::EndInvoke(System.IAsyncResult)
extern void OnErrorCallback_EndInvoke_m668D63C857AAA2914DB027188A7B29A0D3D69BE4 (void);
// 0x00000768 System.Void Byn.Awrtc.Base.OnMessageCallback::.ctor(System.Object,System.IntPtr)
extern void OnMessageCallback__ctor_m61D86E85C46EF79031010E44F3CA23A2B9BF9CBB (void);
// 0x00000769 System.Void Byn.Awrtc.Base.OnMessageCallback::Invoke(System.Object,System.Byte[])
extern void OnMessageCallback_Invoke_m1E6773FB0F0D522913A4B916099ADCA67E12CCAD (void);
// 0x0000076A System.IAsyncResult Byn.Awrtc.Base.OnMessageCallback::BeginInvoke(System.Object,System.Byte[],System.AsyncCallback,System.Object)
extern void OnMessageCallback_BeginInvoke_mAD8702E81C00832DFF0C9873C2E9968CE4EB6F89 (void);
// 0x0000076B System.Void Byn.Awrtc.Base.OnMessageCallback::EndInvoke(System.IAsyncResult)
extern void OnMessageCallback_EndInvoke_m8D80B3734BD34C1A30C041ABEF3EC765F3353370 (void);
// 0x0000076C System.Void Byn.Awrtc.Base.OnTextMessageCallback::.ctor(System.Object,System.IntPtr)
extern void OnTextMessageCallback__ctor_m97D4EC2A7B040AFF9FFBE77129F9391D2370AF8D (void);
// 0x0000076D System.Void Byn.Awrtc.Base.OnTextMessageCallback::Invoke(System.Object,System.String)
extern void OnTextMessageCallback_Invoke_mC5CD2BD5C1AA8176E078EDBE6B04465E1D0D0674 (void);
// 0x0000076E System.IAsyncResult Byn.Awrtc.Base.OnTextMessageCallback::BeginInvoke(System.Object,System.String,System.AsyncCallback,System.Object)
extern void OnTextMessageCallback_BeginInvoke_mF84C5761C1433EFDA549CE5C726F06B24D7BD70A (void);
// 0x0000076F System.Void Byn.Awrtc.Base.OnTextMessageCallback::EndInvoke(System.IAsyncResult)
extern void OnTextMessageCallback_EndInvoke_mAFB2626BF467C4462A06636E3F32CE65361D3C0A (void);
// 0x00000770 System.Void Byn.Awrtc.Base.OnOpenCallback::.ctor(System.Object,System.IntPtr)
extern void OnOpenCallback__ctor_mC7C8398782F1F74DA195C20B5FAA45C37E5B4288 (void);
// 0x00000771 System.Void Byn.Awrtc.Base.OnOpenCallback::Invoke(System.Object)
extern void OnOpenCallback_Invoke_mAE7B0889E5CBD76DDDDD9C1D6697D43664557177 (void);
// 0x00000772 System.IAsyncResult Byn.Awrtc.Base.OnOpenCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void OnOpenCallback_BeginInvoke_m3BE351D86140977DD0EA577D6965BAE018688FBF (void);
// 0x00000773 System.Void Byn.Awrtc.Base.OnOpenCallback::EndInvoke(System.IAsyncResult)
extern void OnOpenCallback_EndInvoke_mE6A598A2D56A422E51761C26EE07F901EC4AAEA9 (void);
// 0x00000774 System.Void Byn.Awrtc.Base.OnCloseCallback::.ctor(System.Object,System.IntPtr)
extern void OnCloseCallback__ctor_m07C64DB7B7EF3891395666A0EB52B67C24F6CAD4 (void);
// 0x00000775 System.Void Byn.Awrtc.Base.OnCloseCallback::Invoke(System.Object,System.Int32,System.String)
extern void OnCloseCallback_Invoke_m62CB0E9CA8F7289A87A8485087C851BCEF298F45 (void);
// 0x00000776 System.IAsyncResult Byn.Awrtc.Base.OnCloseCallback::BeginInvoke(System.Object,System.Int32,System.String,System.AsyncCallback,System.Object)
extern void OnCloseCallback_BeginInvoke_m3F590762D7800B8ABE9DE7FA7AB722B692287DD3 (void);
// 0x00000777 System.Void Byn.Awrtc.Base.OnCloseCallback::EndInvoke(System.IAsyncResult)
extern void OnCloseCallback_EndInvoke_m830A38C9B326A4056CBE18DA25524803FDB5DDC6 (void);
// 0x00000778 Byn.Awrtc.Base.WebsocketReadyState Byn.Awrtc.Base.IWebsocketClient::get_ReadyState()
// 0x00000779 System.Void Byn.Awrtc.Base.IWebsocketClient::add_OnOpen(Byn.Awrtc.Base.OnOpenCallback)
// 0x0000077A System.Void Byn.Awrtc.Base.IWebsocketClient::remove_OnOpen(Byn.Awrtc.Base.OnOpenCallback)
// 0x0000077B System.Void Byn.Awrtc.Base.IWebsocketClient::add_OnClose(Byn.Awrtc.Base.OnCloseCallback)
// 0x0000077C System.Void Byn.Awrtc.Base.IWebsocketClient::remove_OnClose(Byn.Awrtc.Base.OnCloseCallback)
// 0x0000077D System.Void Byn.Awrtc.Base.IWebsocketClient::add_OnError(Byn.Awrtc.Base.OnErrorCallback)
// 0x0000077E System.Void Byn.Awrtc.Base.IWebsocketClient::remove_OnError(Byn.Awrtc.Base.OnErrorCallback)
// 0x0000077F System.Void Byn.Awrtc.Base.IWebsocketClient::add_OnMessage(Byn.Awrtc.Base.OnMessageCallback)
// 0x00000780 System.Void Byn.Awrtc.Base.IWebsocketClient::remove_OnMessage(Byn.Awrtc.Base.OnMessageCallback)
// 0x00000781 System.Void Byn.Awrtc.Base.IWebsocketClient::add_OnTextMessage(Byn.Awrtc.Base.OnTextMessageCallback)
// 0x00000782 System.Void Byn.Awrtc.Base.IWebsocketClient::remove_OnTextMessage(Byn.Awrtc.Base.OnTextMessageCallback)
// 0x00000783 System.Void Byn.Awrtc.Base.IWebsocketClient::Connect(System.String)
// 0x00000784 System.Void Byn.Awrtc.Base.IWebsocketClient::Send(System.Byte[])
// 0x00000785 System.Void Byn.Awrtc.Base.IWebsocketClient::Send(System.String)
// 0x00000786 System.Void Byn.Awrtc.Base.SignalingConfig::.ctor(Byn.Awrtc.IBasicNetwork)
extern void SignalingConfig__ctor_mD54AE46BAFEC4FA608A9201919154014DAFE0ECD (void);
// 0x00000787 Byn.Awrtc.IBasicNetwork Byn.Awrtc.Base.SignalingConfig::get_Network()
extern void SignalingConfig_get_Network_m6FDA04DE134B03B946A4A6B11B41ED68A713D1CD (void);
// 0x00000788 System.Boolean Byn.Awrtc.Base.SignalingConfig::get_KeepSignalingAlive()
extern void SignalingConfig_get_KeepSignalingAlive_mBD8CABE2BEA5F9771DDE3BD7C440B808C283F441 (void);
// 0x00000789 System.Void Byn.Awrtc.Base.SignalingConfig::set_KeepSignalingAlive(System.Boolean)
extern void SignalingConfig_set_KeepSignalingAlive_m7D9EBB0FFC2DBABC42535A5FB17F393982167ED6 (void);
// 0x0000078A System.Int32 Byn.Awrtc.Base.SignalingConfig::get_SignalingTimeout()
extern void SignalingConfig_get_SignalingTimeout_m5E512ED6DF4DA42027F8D2914FEB435048F40D21 (void);
// 0x0000078B System.Void Byn.Awrtc.Base.SignalingConfig::set_SignalingTimeout(System.Int32)
extern void SignalingConfig_set_SignalingTimeout_mEB9680E4EF910A83B9BE721041F4F3D366CF4560 (void);
// 0x0000078C System.Void Byn.Awrtc.Base.SignalingConfig::Lock()
extern void SignalingConfig_Lock_mA5FD586A380F056C44A569ADEF8E7EFA81D0DF7B (void);
// 0x0000078D System.Boolean Byn.Awrtc.Base.SignalingInfo::get_IsSignalingConnected()
extern void SignalingInfo_get_IsSignalingConnected_m2B22DBBAF55BFBEF557EB7D5BDFA7A564128FC66 (void);
// 0x0000078E Byn.Awrtc.ConnectionId Byn.Awrtc.Base.SignalingInfo::get_ConnectionId()
extern void SignalingInfo_get_ConnectionId_mB826E196B26655A8A8166D6FD30BDB5DF2854AB4 (void);
// 0x0000078F System.Boolean Byn.Awrtc.Base.SignalingInfo::get_IsIncoming()
extern void SignalingInfo_get_IsIncoming_m55535798EF8E4393229CBDE59BD30EC06780CC0B (void);
// 0x00000790 System.Int32 Byn.Awrtc.Base.SignalingInfo::get_SignalingDuration()
extern void SignalingInfo_get_SignalingDuration_m10A9D07ADED8BEC9B396511FB1A8EAC6F29864BD (void);
// 0x00000791 System.Void Byn.Awrtc.Base.SignalingInfo::.ctor(Byn.Awrtc.ConnectionId,System.Boolean,System.DateTime)
extern void SignalingInfo__ctor_m838555F15B35A377A43FD0B69F5D3384898BD20D (void);
// 0x00000792 System.Void Byn.Awrtc.Base.SignalingInfo::SetSignalingStart(System.DateTime)
extern void SignalingInfo_SetSignalingStart_m299A21A02DFBCA75F27A91193AD945C3D1A36470 (void);
// 0x00000793 System.Void Byn.Awrtc.Base.SignalingInfo::SignalingDisconnected()
extern void SignalingInfo_SignalingDisconnected_m33FBFDD5D8FF83A74E3FE151DBBA40F546886E69 (void);
// 0x00000794 Byn.Awrtc.Base.WebsocketConnectionStatus Byn.Awrtc.Base.WebsocketNetwork::get_Status()
extern void WebsocketNetwork_get_Status_m10CDA64CF347494FF78B91373BEFDA79BAA137BD (void);
// 0x00000795 System.Void Byn.Awrtc.Base.WebsocketNetwork::.ctor(System.String,Byn.Awrtc.Base.Configuration)
extern void WebsocketNetwork__ctor_m92F50B981A17117766315EA18362F494A1444E82 (void);
// 0x00000796 System.Void Byn.Awrtc.Base.WebsocketNetwork::WebsocketConnect()
extern void WebsocketNetwork_WebsocketConnect_m27F1958923FD2B55B49032C51BA0159EA4CA3B14 (void);
// 0x00000797 System.Void Byn.Awrtc.Base.WebsocketNetwork::WebsocketCleanup()
extern void WebsocketNetwork_WebsocketCleanup_m4F2F9E56529FB1B44C8B4BC5BEEB4FDAD4E612EC (void);
// 0x00000798 System.Void Byn.Awrtc.Base.WebsocketNetwork::EnsureServerConnection()
extern void WebsocketNetwork_EnsureServerConnection_mE9B7283AC789019CECAC8BBA2966D8A290A08B53 (void);
// 0x00000799 System.Void Byn.Awrtc.Base.WebsocketNetwork::UpdateHeartbeat()
extern void WebsocketNetwork_UpdateHeartbeat_m113A62CB24B97E08663DEF1B89ADE4BD989AA077 (void);
// 0x0000079A System.Void Byn.Awrtc.Base.WebsocketNetwork::TriggerHeartbeatTimeout()
extern void WebsocketNetwork_TriggerHeartbeatTimeout_m54536FF5AA647FA38F3CF59BD831BFF3133E7F68 (void);
// 0x0000079B System.Void Byn.Awrtc.Base.WebsocketNetwork::CheckSleep()
extern void WebsocketNetwork_CheckSleep_m36F58F4AE704930EC77468E24E8760701FDE8F33 (void);
// 0x0000079C System.Void Byn.Awrtc.Base.WebsocketNetwork::OnWebsocketOnOpen(System.Object)
extern void WebsocketNetwork_OnWebsocketOnOpen_m771D1D7988D738F389B7D9BDEF82861B9D922AD2 (void);
// 0x0000079D System.Void Byn.Awrtc.Base.WebsocketNetwork::OnWebsocketOnClose(System.Object,System.Int32,System.String)
extern void WebsocketNetwork_OnWebsocketOnClose_m756A66448C681E76840AB5B05FF6D526D38EC538 (void);
// 0x0000079E System.Void Byn.Awrtc.Base.WebsocketNetwork::OnWebsocketOnError(System.Object,System.String)
extern void WebsocketNetwork_OnWebsocketOnError_m416D364C7C37A5DC085606BD2101622DB38FA72D (void);
// 0x0000079F System.Void Byn.Awrtc.Base.WebsocketNetwork::OnWebsocketOnMessage(System.Object,System.Byte[])
extern void WebsocketNetwork_OnWebsocketOnMessage_m2BF7C325F9C75537E699DDDE002907467D16FE91 (void);
// 0x000007A0 System.Void Byn.Awrtc.Base.WebsocketNetwork::Cleanup(System.String)
extern void WebsocketNetwork_Cleanup_m85BAF7B3BAC38CE69A71DBD8D7660CBEC55742C0 (void);
// 0x000007A1 System.Void Byn.Awrtc.Base.WebsocketNetwork::EnqueueOutgoing(Byn.Awrtc.NetworkEvent)
extern void WebsocketNetwork_EnqueueOutgoing_m574F63627CBCACE91AC478F46DE77E2AB3EB9994 (void);
// 0x000007A2 System.Void Byn.Awrtc.Base.WebsocketNetwork::EnqueueIncoming(Byn.Awrtc.NetworkEvent)
extern void WebsocketNetwork_EnqueueIncoming_mC890897DEAEC574FD0DF956504D33608AAA3AAF7 (void);
// 0x000007A3 System.Void Byn.Awrtc.Base.WebsocketNetwork::TryRemoveConnecting(Byn.Awrtc.ConnectionId)
extern void WebsocketNetwork_TryRemoveConnecting_m0CDF5E4BD1B6658F7F4C471E48F723AB67CD7A47 (void);
// 0x000007A4 System.Void Byn.Awrtc.Base.WebsocketNetwork::TryRemoveConnection(Byn.Awrtc.ConnectionId)
extern void WebsocketNetwork_TryRemoveConnection_m02F5DF133FFF607845A9FA37BA59B0EC144FC939 (void);
// 0x000007A5 System.Void Byn.Awrtc.Base.WebsocketNetwork::ParseMessage(System.Byte[])
extern void WebsocketNetwork_ParseMessage_mF566EB12FF2422B4B12D690BAD7AF84C00FD2491 (void);
// 0x000007A6 System.String Byn.Awrtc.Base.WebsocketNetwork::MessageToLog(System.Byte[])
extern void WebsocketNetwork_MessageToLog_m8E4065ABA559C51CAEB041E7B25F0FE804ACDD1F (void);
// 0x000007A7 System.Void Byn.Awrtc.Base.WebsocketNetwork::HandleIncomingEvent(Byn.Awrtc.NetworkEvent)
extern void WebsocketNetwork_HandleIncomingEvent_m412DA3E0A5AEC66E019C2706918E8F7E44082EF2 (void);
// 0x000007A8 System.Void Byn.Awrtc.Base.WebsocketNetwork::HandleOutgoingEvents()
extern void WebsocketNetwork_HandleOutgoingEvents_mF35318BBDE24FF8858C66264CD5F0A724CAC7CF0 (void);
// 0x000007A9 System.Void Byn.Awrtc.Base.WebsocketNetwork::SendHeartbeat()
extern void WebsocketNetwork_SendHeartbeat_mB3E19DFF0950C020FC8C2E96507F233DD28769C3 (void);
// 0x000007AA System.Void Byn.Awrtc.Base.WebsocketNetwork::SendVersion()
extern void WebsocketNetwork_SendVersion_mAD873D1B59027DB2EE1511614141F528289E8D66 (void);
// 0x000007AB System.Void Byn.Awrtc.Base.WebsocketNetwork::SendNetworkEvent(Byn.Awrtc.NetworkEvent)
extern void WebsocketNetwork_SendNetworkEvent_m7D4B74D3047F53B26202F4BF369012444F090745 (void);
// 0x000007AC System.Void Byn.Awrtc.Base.WebsocketNetwork::InternalSend(System.Byte[])
extern void WebsocketNetwork_InternalSend_m2307DE3CCF6D59057A8EF4E47CB781146352883F (void);
// 0x000007AD Byn.Awrtc.ConnectionId Byn.Awrtc.Base.WebsocketNetwork::NextConnectionId()
extern void WebsocketNetwork_NextConnectionId_m241C86F746F560FC0F43C7331E6B6AD6BF240F70 (void);
// 0x000007AE System.String Byn.Awrtc.Base.WebsocketNetwork::GetRandomKey()
extern void WebsocketNetwork_GetRandomKey_m5FDEAB10DB1A26229F979C06FE4AD7FD83DC2EBA (void);
// 0x000007AF System.Boolean Byn.Awrtc.Base.WebsocketNetwork::Dequeue(Byn.Awrtc.NetworkEvent&)
extern void WebsocketNetwork_Dequeue_m8A175860F5BBF5E23E581DE01E7ED83D62C307A4 (void);
// 0x000007B0 System.Boolean Byn.Awrtc.Base.WebsocketNetwork::Peek(Byn.Awrtc.NetworkEvent&)
extern void WebsocketNetwork_Peek_mFF555DD7A156E83D4B58DAF0BF430C7F7B40C700 (void);
// 0x000007B1 System.Void Byn.Awrtc.Base.WebsocketNetwork::Update()
extern void WebsocketNetwork_Update_m259BCAFBA8A03F5EBDB14E16F795A20653848100 (void);
// 0x000007B2 System.Void Byn.Awrtc.Base.WebsocketNetwork::Flush()
extern void WebsocketNetwork_Flush_m1875231AB0CFFACDA1AF6EBBDD9886A8E4ACFBAF (void);
// 0x000007B3 System.Boolean Byn.Awrtc.Base.WebsocketNetwork::SendData(Byn.Awrtc.ConnectionId,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void WebsocketNetwork_SendData_mE04B2B1082EED96828492D7925E7DFA8F870A45B (void);
// 0x000007B4 System.Void Byn.Awrtc.Base.WebsocketNetwork::Disconnect(Byn.Awrtc.ConnectionId)
extern void WebsocketNetwork_Disconnect_m3CC073A01F3980DEA308EBAB2EDD42263FFAC60B (void);
// 0x000007B5 System.Void Byn.Awrtc.Base.WebsocketNetwork::Shutdown()
extern void WebsocketNetwork_Shutdown_mC2681353AB00DB9E6FECAF8C7B30AD3EE8F67767 (void);
// 0x000007B6 System.Void Byn.Awrtc.Base.WebsocketNetwork::StartServer(System.String)
extern void WebsocketNetwork_StartServer_m2A759D83DD9AF600E0470419720C2C298A3152EB (void);
// 0x000007B7 System.Void Byn.Awrtc.Base.WebsocketNetwork::StopServer()
extern void WebsocketNetwork_StopServer_m7B9F364CAD9382DD3AB2A14FB19C031C7559A7B9 (void);
// 0x000007B8 Byn.Awrtc.ConnectionId Byn.Awrtc.Base.WebsocketNetwork::Connect(System.String)
extern void WebsocketNetwork_Connect_mB8780B7952D6AA57EDE7FC99CA2FD5B5BE501E14 (void);
// 0x000007B9 System.Void Byn.Awrtc.Base.WebsocketNetwork::Dispose(System.Boolean)
extern void WebsocketNetwork_Dispose_m83752BFE72224F6F92E455C4BF75CCD3301DBC85 (void);
// 0x000007BA System.Void Byn.Awrtc.Base.WebsocketNetwork::Dispose()
extern void WebsocketNetwork_Dispose_m893D76FA94494FE708972D5396A29146E1F6E3E5 (void);
// 0x000007BB System.Void Byn.Awrtc.Base.WebsocketNetwork::.cctor()
extern void WebsocketNetwork__cctor_m6A8A676032C70BAC73E204E06B6E3C624BF9F70F (void);
// 0x000007BC System.Int32 Byn.Awrtc.Base.Configuration::get_Heartbeat()
extern void Configuration_get_Heartbeat_mC79FD1FFF4AFE9DD17B588AB554814CC91FB4A03 (void);
// 0x000007BD System.Void Byn.Awrtc.Base.Configuration::set_Heartbeat(System.Int32)
extern void Configuration_set_Heartbeat_m7B7737F797DEA602FDD13305FA167DEE8C17293B (void);
// 0x000007BE System.Void Byn.Awrtc.Base.Configuration::Lock()
extern void Configuration_Lock_m9193632A1D6D6F90A6B71CAC3B70D00E911B2CE3 (void);
// 0x000007BF System.Void Byn.Awrtc.Base.Configuration::.ctor()
extern void Configuration__ctor_m78870C2FCE611EDCE80DD946202F948F0FE2C38D (void);
static Il2CppMethodPointer s_methodPointers[1983] = 
{
	CloseEventArgs__ctor_mF2B76761766FC1EEA686E575C79F58E2BEC7A7F7,
	CloseEventArgs__ctor_m7C2BD8A3B003C330CBBBE406A069B6B022672137,
	CloseEventArgs__ctor_m84541D1F2E9B92B522BC184A580F04F225B122B9,
	CloseEventArgs__ctor_m989196EBA933B49BC1ECEB619AD20440A3D2A99A,
	CloseEventArgs__ctor_m5734B11540984613D124180934A61F32FA375832,
	CloseEventArgs__ctor_m0C2BB8F628399643774DE73C6E451107719DC222,
	CloseEventArgs_get_PayloadData_m61FC405897BEF0E2A94802816160A5ABD4DE96AA,
	CloseEventArgs_get_Code_mED2A982C2C23D28CE77A1AF0ED506028705225A8,
	CloseEventArgs_get_Reason_mA400E4FE20C8816A0AF9650BF1F28FA1E820C935,
	CloseEventArgs_get_WasClean_mB25C8670B696CA41CF8FBE3079D8486DAA809BA2,
	CloseEventArgs_set_WasClean_m6253D7A3075BDA7D7E70259C89E6AA60E42C42A4,
	ErrorEventArgs__ctor_m6978EE4718336C163E973EC0BAE3C5CCA4CF125C,
	ErrorEventArgs__ctor_mA7711C2DB8ACCBD4614D8BE72A6C4D8C4F5E58EF,
	ErrorEventArgs_get_Exception_m65B0EAC3CCDA1564F2B3D04F9F81C9B90891967D,
	ErrorEventArgs_get_Message_m2BE0962EDCEF2699CD6C8E662FFF65392A1F4B11,
	Ext_compress_m485158E0BAC622C16ED36691F377D3C6E922CAD3,
	Ext_compress_m34B1A8F2434EEF3BFEA0F0A7CDE14E46FCA7F133,
	Ext_compressToArray_mA0172DBC07B15EA5FB0DF45170FC861F3FC85923,
	Ext_decompress_mE99DFE5D5A1A579814540472474E8F43715F1384,
	Ext_decompress_m3508D81FFAD52F9C385B5A196DB1E07F4842DBA1,
	Ext_decompressToArray_mFAC58DE341C493FA38D7342FEE50A30829BB526B,
	Ext_times_m4760483BAEB311A2EDAB6A090BE5EE30A6242912,
	Ext_Append_m52F69117520A2828940E23E0D90865E9AF06C517,
	Ext_CheckIfAvailable_mB6EC7D74388477AC0A518CBBD8ABF07E0629F40B,
	Ext_CheckIfAvailable_m0DE519558AE633FAC2DEB088DB34E69235A785ED,
	Ext_CheckIfValidProtocols_mEF3B763702F5C2A34F7B2E06FB49B93F32D32FF1,
	Ext_CheckIfValidServicePath_m108F73486130B30A43F1452EE0387AC5779B20BF,
	Ext_CheckIfValidSessionID_mFCA33D9F2F166EBE107E5DB7C08947D4B0FAB640,
	Ext_CheckIfValidWaitTime_m666389616B8717F1E5D0061B0A25F389C703A433,
	Ext_CheckWaitTime_m8292A62B2623D507EEFCC27F9038FCCB0134C0BC,
	Ext_Close_mB443764F003B2CF77CBFD5F56A738949D66063EC,
	Ext_CloseWithAuthChallenge_m528D45A1D4CD907629B7255B8C4F6B9CB31A7879,
	Ext_Compress_m108FD47E3EB0423AD5AAC45FC776AAAEB5375A68,
	Ext_Compress_m6C3332A2970C16969C76755A62D1C5DDA27BF246,
	Ext_CompressToArray_m9D0542478AE96CC59B8431F763AB1DA1067414B9,
	NULL,
	Ext_ContainsTwice_m68140E6F054F3391145DFD370795CEA26A5C9038,
	NULL,
	Ext_CopyTo_m1ACB528FCC743B159F5269F46C59D823BFA20517,
	Ext_CopyToAsync_mD5A93F57BE84D666153A16DACC2071FF251D68BD,
	Ext_Decompress_mEB4E5DEE4DD2B99ECFECEA851412505073359A03,
	Ext_Decompress_m5CB1F37EDCC6B7B11A2F917609978359C8928749,
	Ext_DecompressToArray_m9B8B2875219F705CF8C2D14C7D7C2083ADACC421,
	Ext_EqualsWith_mBEE764F89FFA97EB05789C49139F944DB145AEB1,
	Ext_GetAbsolutePath_mE06626FB7BEE7B3A54324D91DDFF63A5047E2983,
	Ext_GetMessage_mF64524CF56DE72157B99D3148366BA691C716543,
	Ext_GetName_mD8DD130D0C27607285A48A013413DA2D106D0650,
	Ext_GetValue_m150D80CB6793F60F86064091D4EAE8E6F94A7D20,
	Ext_GetValue_m801AA9845996C6A649DA1F538BFD371B0C5268BE,
	Ext_GetWebSocketContext_mDF86F38D91B781FF4E554810271BD0F1CCB62B72,
	Ext_InternalToByteArray_m91C3F342EF7F1DB6D3549FDD70E1701E40A9DAF2,
	Ext_InternalToByteArray_m50B23BD10801FF6C0FB8204B46F7EDDFEF30DDD2,
	Ext_IsCompressionExtension_mFFD1A76367C07F03E9AE59FC096F6A54EF83414C,
	Ext_IsControl_mFA1456BACC196E275312AC010A40517A9201921A,
	Ext_IsControl_m423B3FF9B0E1D3998FBA5E00BB1CBFBF05D7BF69,
	Ext_IsData_m092786329B98B9D7A5A2D4EED7D5D7E496115FF7,
	Ext_IsData_m951013EFCE345E54C629AADB8D9516AF5B586462,
	Ext_IsPortNumber_m040DA9233A1A61E1DC52A0931ED7A9B573A04A53,
	Ext_IsReserved_mD6A7A606663A5D2142D9E7CD8AC443D0809C117D,
	Ext_IsReserved_m7B07270058ECC4EE5406D3858450825AE1A8FAF6,
	Ext_IsSupported_m4886F8A154117D4B97C9B40065619BC7F574469C,
	Ext_IsText_mFF7A23F47E36B5957A0BEF7F8F7D71CE635EED7F,
	Ext_IsToken_m2BFAF5BFE7DCDE7172F2AB5E11E0E1B95356EAB3,
	Ext_Quote_m75E0AB845C879E63A1603E6197F020A9AE2D8FB4,
	Ext_ReadBytes_m581BDE8DBD808338EE3D9E0C8DDF94FEE930AA12,
	Ext_ReadBytes_m48DBAF2CDF9F539014D14D1407762C79DD248D6C,
	Ext_ReadBytesAsync_mF916EB46545EAD8D72ABDEEEDE09C8BCA27AA04B,
	Ext_ReadBytesAsync_m2D7B3B848FCCA9AFA15DC2BF7B3A46788D135382,
	Ext_RemovePrefix_mAC46C41EC405D62BBA0E7BD3668151528A47B78C,
	NULL,
	Ext_SplitHeaderValue_mBFA61BC57519A89B664BB90CAD02A4062B48C5B4,
	Ext_ToByteArray_mB715EA201A0347E4E4575B1D970200B394D9940B,
	Ext_ToCompressionMethod_m4756514838729031E8D615AC4BABAE0F717BDDD4,
	Ext_ToExtensionString_m5C2BB01900C06FDFCEE4AF2D3A0E572C886FBE47,
	Ext_ToIPAddress_m151DBF434D73374CCE6CFBF63FA574CE29961E83,
	NULL,
	Ext_ToUInt16_m06DB102E5C75F8C94B93DB229B719A62F7EAE79D,
	Ext_ToUInt64_mD87D7CC89DE0A7386B8B6C2E420603500930687A,
	Ext_TrimEndSlash_m1C5C26EC1304CF822D143243C77E6DD628454B9E,
	Ext_TryCreateWebSocketUri_m6ECDEB2A7F57781B66B5E6C5342931460E30C82E,
	Ext_Unquote_m0EDDE0A9584C643A0AD5560B4515CE4AEFAB04D7,
	Ext_UTF8Decode_m2E520D6039D0A076440B5E6248DB500A2574E0E9,
	Ext_UTF8Encode_mAAF3200AF93F0D779DDBD35A6A4C7237DD83D447,
	Ext_WriteBytes_m3904B44AF581F2C833DE020AB5D40D979E231AE7,
	Ext_WriteBytesAsync_m136D35DE8352379FCF68B15E471EC3D8B5B8E247,
	Ext_Contains_m57CAB022534ADF189526AF14BAC4B9268600A73D,
	Ext_Contains_m95619F286FE8CD1385A2D96762AB4466ED2D0A56,
	Ext_Contains_mC11B223DEF28DAAD738FB4200496C33807EBCB3E,
	Ext_Emit_mA500E38C534CC0B9B09969CBDF598EA256B576C3,
	NULL,
	Ext_GetCookies_mFE982227ECA6E14B9C8E8C38334593E12B6B4D49,
	Ext_GetDescription_m78EF8904EC42BFA2E9B44FEB7F5234B9799144B0,
	Ext_GetStatusDescription_m6ABF3F8A47DBA46D0A2BC4DB027A1433D1329DBE,
	Ext_IsCloseStatusCode_m2362BEEC3FC6A136CD8CC116F0E6592C207A37CD,
	Ext_IsEnclosedIn_m2E1BF90B8CDC75226704D8D47C42699C64BF2180,
	Ext_IsHostOrder_mCA66CE42F50BD9D0C3DEA95851B31854E413219F,
	Ext_IsLocal_m86D0AA69F5ABE6EE8BA0268579AB083D2DEBB9B6,
	Ext_IsNullOrEmpty_m6D5613A66F684263FF6484CC129ADF9C0476B8FA,
	Ext_IsPredefinedScheme_mD5AC83D673512CDA373D179742CA62A23892CBA5,
	Ext_IsUpgradeTo_mA74827D1DDEE41ED7FB8396B024F00098BB9F0E5,
	Ext_MaybeUri_mFEEA728C8A0A036B1524D85143146BC6A59B4580,
	NULL,
	NULL,
	Ext_Times_m60C27A4B0B4A411F61438B60F639DCADCA6A6175,
	Ext_Times_m92238BB482A078B92803E2C4C6BF37B774F36A85,
	Ext_Times_mF4A874778E951D2837C9146FEF35D2531EA50483,
	Ext_Times_mFE49D2EE8FD5DEB52F8D1A942AD2350B2C8B2A29,
	Ext_Times_m129856A98AC3C4D5AF7ABAA4BC8912F4C347FFE8,
	Ext_Times_m5EA97FB7203C06CFEBF7CAC3DA31D15887E5159B,
	Ext_Times_mAD15787D9E880EDDF62D16187D6F9F80E98A59FC,
	Ext_Times_m0B6FA309352DEA3933E8555CBDD0BF22488507E0,
	NULL,
	NULL,
	Ext_ToHostOrder_mB56E8A9DA8E906C2578E7CD6B80913AFCE5A90D0,
	NULL,
	Ext_ToUri_m501AF00613B3D9BD573941AF3DED7AE23400C089,
	Ext_UrlDecode_mE9286881DE7990E99F61DC78F4C9B952875864F8,
	Ext_UrlEncode_mD47864D8A9341036B5E7AEF469059B81BE87C941,
	Ext_WriteContent_m3D23C0B37B21A0CBA768BDDB99BA931F81822101,
	Ext__cctor_m4EB4089287BC1F43021E0E92ACF2B36BA184AB92,
	U3CU3Ec__cctor_mE1E176D0D6AFF75CD02AAB37CF5086961D5902A2,
	U3CU3Ec__ctor_m03B14D44F4B933F669C8121DCC4CAB99E1D074CA,
	U3CU3Ec_U3CCheckIfValidProtocolsU3Eb__12_0_m91F5C1A4EB94A8F320A1F147A5E6B3DB23EFC0EB,
	U3CU3Ec__DisplayClass23_0__ctor_m01D2D7954FF11090FD99A99C7A57F60C5DC2C5EF,
	U3CU3Ec__DisplayClass23_0_U3CContainsTwiceU3Eb__0_mE24E10F8B8933EED271D40C85AE75BA85AA01443,
	U3CU3Ec__DisplayClass26_0__ctor_m34A9D8ECA2EC4206504C98E98C260178652017A4,
	U3CU3Ec__DisplayClass26_0_U3CCopyToAsyncU3Eb__0_m2E192460B8D361CF496844C102213812A1A32F91,
	U3CU3Ec__DisplayClass53_0__ctor_m363DDB10DEF49F563231D6EDE7CAD5A3781DE7BA,
	U3CU3Ec__DisplayClass53_0_U3CReadBytesAsyncU3Eb__0_m7BE7A9C46400DF6BC6ED061D9C79C334B118DECC,
	U3CU3Ec__DisplayClass54_0__ctor_m524241D6EA0AA7774DC4A974F7F8FA699BD94A61,
	U3CU3Ec__DisplayClass54_0_U3CReadBytesAsyncU3Eb__0_m174A38F629E57DC9195D1B96BD29F9C965126F38,
	U3CU3Ec__DisplayClass54_1__ctor_m5E774EF7E67BC058302A0D23A0DD2105BF2E8ECF,
	U3CU3Ec__DisplayClass54_1_U3CReadBytesAsyncU3Eb__1_m5C19DE34B98021C970E5460BAAF8C2AF6491BC80,
	U3CSplitHeaderValueU3Ed__57__ctor_mF955B13C74AE0D37EF845F32BE9E8E3762E90A94,
	U3CSplitHeaderValueU3Ed__57_System_IDisposable_Dispose_mBCCB828DB6B5584F09E58A5FD906885F70BCF697,
	U3CSplitHeaderValueU3Ed__57_MoveNext_m9E701C894410E560C75E256C23BB49FE6859A0AA,
	U3CSplitHeaderValueU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mE9BC365BCEC7F5E3C2CE6293256ECB9CA43E3ED5,
	U3CSplitHeaderValueU3Ed__57_System_Collections_IEnumerator_Reset_m9699B98756BF307837D22B0FB1FA7413701D3705,
	U3CSplitHeaderValueU3Ed__57_System_Collections_IEnumerator_get_Current_m80F74DE2FC76E75F5B52F5DDE6B8966AE8889803,
	U3CSplitHeaderValueU3Ed__57_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m2E07293CA7EC06DCCA6E3E7F674E0F7AF515B54A,
	U3CSplitHeaderValueU3Ed__57_System_Collections_IEnumerable_GetEnumerator_m84581C512C3B1239D2EDF9B63289E1B5167BB7AA,
	U3CU3Ec__DisplayClass71_0__ctor_mC793B7E00DAC9672F6966C3D5CB425DB835D3083,
	U3CU3Ec__DisplayClass71_0_U3CWriteBytesAsyncU3Eb__0_mDC5894DC73F861A3AE9DD3C13CF88854EE4EC9F9,
	U3CU3Ec__DisplayClass71_0_U3CWriteBytesAsyncU3Eb__1_m14DA387C049EB560EE112AD66EEAF93C6AE526CC,
	NULL,
	NULL,
	HttpBase__ctor_m8931225CB535D6B6C9B7907B6413043B8C33EDA5,
	HttpBase_get_EntityBody_m3BD76E1B557F1E25E95E47D04364C75AF3C6A365,
	HttpBase_get_Headers_m205F9346FC52003B503465FE556465FB5E0F5A47,
	HttpBase_get_ProtocolVersion_m22ADA88C17D3C1B111B40E1CACBD853D8D989096,
	HttpBase_readEntityBody_m8C4461063B6DFD4B38440E1E0272426FCDA3116E,
	HttpBase_readHeaders_mABE74433D17B5C4439E998C25B423F46F30DC404,
	NULL,
	HttpBase_ToByteArray_mAC4BA4E41DDDA30E8ADBA25B864ED0A60B5220DD,
	U3CU3Ec__DisplayClass13_0__ctor_m52F7066D6D08EDAB5EA2CB4862637CF2ECA0544E,
	U3CU3Ec__DisplayClass13_0_U3CreadHeadersU3Eb__0_m25B4AF347E9762B3AC08C5F740772AF9307C41A1,
	NULL,
	NULL,
	HttpRequest__ctor_mD2E0AE0407A69DF24D1FA2DF20E99CC7458B3FF5,
	HttpRequest__ctor_mB00BE77082590567BBBD70118C2B92B0C0B28683,
	HttpRequest_get_AuthenticationResponse_m30A034D9F4328DA4D20BAEA671E09C8DBA9CF4A6,
	HttpRequest_get_Cookies_m8ECBD9E856BC13BF3211594FD9E89AA224B4B07E,
	HttpRequest_get_HttpMethod_mA0E139150B1F0B9C89C3F2E4E46276C6B3D60DCC,
	HttpRequest_get_IsWebSocketRequest_mD9FBDD78CAAF386815C7F3D406E547213384B5F9,
	HttpRequest_get_RequestUri_mFB146DBFE64A1DF6DAB2470E522FB9EF7CF5E6F9,
	HttpRequest_CreateConnectRequest_m1F5DF3ED2A5840C1FF2B08582F66727542E33F5C,
	HttpRequest_CreateWebSocketRequest_m591149F120844F4FA59CF9776B2C2B00049D30E4,
	HttpRequest_GetResponse_m7DC56D8E2B5EC05E6F126BE31BADDFC6905EEA0A,
	HttpRequest_Parse_mEB96F9888D2DC4B149E2A5872C643C51BAE2C85C,
	HttpRequest_Read_m6D73546440BD5524749D641FB19FF7E04A2F2095,
	HttpRequest_SetCookies_mBE54CE60C71FADB7EB110A2267EEE85686EA6531,
	HttpRequest_ToString_mE06EC6EDB8986DCFB841B7EE6FD1C3F0F49426EF,
	HttpResponse__ctor_m90EF3E0CE36099FD0B64B93EDBF173940C216994,
	HttpResponse__ctor_mF291727C7CAC0C66217123979D0EF58462B4E769,
	HttpResponse__ctor_m0D2BD62616E17FAD95B8A80FB561A88AE391BF8D,
	HttpResponse_get_Cookies_mE45EB17AFA95622CEF15C38EA06B9774FA85D8EB,
	HttpResponse_get_HasConnectionClose_m96AE03E8AD8AE473D632031FA053520C637EBA1C,
	HttpResponse_get_IsProxyAuthenticationRequired_m64BFC146D098E053061F92FB4C8DF1A8F7A413DB,
	HttpResponse_get_IsRedirect_m1D43EAE8DF102196726FA29162B1A3BA2419E2B6,
	HttpResponse_get_IsUnauthorized_m04C7F3392EBC5F4B63071A2ABD954C58A8B3A099,
	HttpResponse_get_IsWebSocketResponse_mE581E887C5F6B5717B799F0FAB2FF0E19F981344,
	HttpResponse_get_Reason_m11479AE5D1B4718DFAA76338089851BE7938DB99,
	HttpResponse_get_StatusCode_mAB47C354D4BE22BE676540F5A397F2742C38F7EB,
	HttpResponse_CreateCloseResponse_m5E5D527C1E3F3836B46A596E10BC1D01CA400CC5,
	HttpResponse_CreateUnauthorizedResponse_mA21EDFC66AC8CA88ADFDD16239E246917CED29DB,
	HttpResponse_CreateWebSocketResponse_m4B5A9A5943BBBEE294D00DA16B43320FA16DB2F5,
	HttpResponse_Parse_m2EA92660B00CC2F35640E6574B47D5B778AA4E1F,
	HttpResponse_Read_m356B89AE7741B0286A3E2648ECF137402ABE4971,
	HttpResponse_SetCookies_m3126CD1EECC1A310E23B08065575779A07BEF5AF,
	HttpResponse_ToString_mD3C2D7B7465E063F84E479F092E39684D481519B,
	LogData__ctor_mFAB6B90C12B4858753B74C542A6B42A2D9BEE497,
	LogData_get_Caller_mCD87FF0D2601C31C0F8DFCCDA09ADC6E27792AD2,
	LogData_get_Date_m3CCAF3E485F1F623EF22763D110F3EF864ED06ED,
	LogData_get_Level_m7C3274DF34D0B9D866E7DC7D0EB97E52F0858717,
	LogData_get_Message_mE30E26D8CB0D0F588D0E227958E43418A20B0E40,
	LogData_ToString_mC78D2ABBB3F8BD3CE248DDD38B9D0DE931FAEA49,
	Logger__ctor_m6A8685F78240B853EFA5FFBF35BF443DF9E3E340,
	Logger__ctor_m05BB5B96E953213D7FD1E621C5104B2A0147D4DB,
	Logger__ctor_mE75FD905ECBB63AD3EEE25E0ED91746C8B09A94D,
	Logger_get_File_m65542F8D0156F74AF5BDF5FED76609736316D757,
	Logger_set_File_m518F4BB93B864B2A55408D50936239C0B1932A7B,
	Logger_get_Level_m3996D95040AA6A27F65F69108A3A2995F45625C1,
	Logger_set_Level_m244B320CE2C1E32091F73CCBDF46862DD4D8C2F2,
	Logger_get_Output_m413241C28CD8D18794567D366DC3BF01B69579CB,
	Logger_set_Output_mFBF70E8D11A0FFD1B4B37F8DAB7A515C217A0AFB,
	Logger_defaultOutput_m9CCF5119E52B1522440117733E6F6B2FD49067E4,
	Logger_output_mEF2F966920E1EDB22E17FAE04C5B889964826FC3,
	Logger_writeToFile_m12DA0AC08650CB6CF06CB02B6CA89A26C0EA4B30,
	Logger_Debug_mAA19017C63E5405FE991BC999BDD393A66A162B0,
	Logger_Error_m01C374BBF237EB193B388AB977ED2893787DFAF5,
	Logger_Fatal_m079607EDDB12421CC011BF2EB9AA6941D822D653,
	Logger_Info_m375065466F160565B0AC1662527EEAA5CD238A84,
	Logger_Trace_m0F6D60D683F5D2294C946EC94567E680E9F0D4C0,
	Logger_Warn_mF68555389DEEA88701E37540ACAA0710CFE9A5E2,
	MessageEventArgs__ctor_mE8249469EC7B3F71D92D3D3AAF559A6D25F664CE,
	MessageEventArgs__ctor_m3C33595A944E468C528A9A23D2FEF7B863DFB243,
	MessageEventArgs_get_Data_m87B02FD9A7760BC0874D0E3AB4CA0757B34ECBDA,
	MessageEventArgs_get_IsBinary_m7CD6BDC296C53C4B846735A28C3B5AE7118DC57F,
	MessageEventArgs_get_IsPing_m01DD45E176A409A1F8C0B4E8796AA5185AB91002,
	MessageEventArgs_get_IsText_m4C2517C9944A7FD99286DAD253DA762487B8219C,
	MessageEventArgs_get_RawData_mCB83537E45EC73ACCFB7404336D4AFFFD08B1EBE,
	MessageEventArgs_get_Type_m2B46EF5C22988F36EC2591340D356C6A87F4A147,
	PayloadData__cctor_m745B1D00398C2BE6EF8BA9DCF0461ACF5CD9CE3A,
	PayloadData__ctor_m0A5867C71E4EC9D8B236D64F4C0BD1FF326BFE13,
	PayloadData__ctor_mE276A01210F764E9FD4533F571CC38569EFEBB39,
	PayloadData__ctor_mC8492ECA5991786CBF706C9A148AC89FD14AE9F7,
	PayloadData_get_ExtensionDataLength_m108491D6C8B10A0933E4122738363A9A0B3B9489,
	PayloadData_set_ExtensionDataLength_mB8698897DFD6D0AAD7554682F3B778ADA25F3F2F,
	PayloadData_get_IncludesReservedCloseStatusCode_mCB2EE5B3CFF047136A50A7883E0053DC63E38F7E,
	PayloadData_get_ApplicationData_m0944FA1CA9154D36D9DDD22C389EB2CF335EE6A1,
	PayloadData_get_ExtensionData_m05F6F1B0AD3B014B99AB74D21FDDAFCC861E99A4,
	PayloadData_get_Length_m4523B0010FDC114DD0224F6CEF1F8572FCFAC2C3,
	PayloadData_Mask_mE53818229057F7FAD25A8FD17A7278DAD21CB168,
	PayloadData_GetEnumerator_m47201F6D6E55A37ED705656585911B8A4E69DD1A,
	PayloadData_ToArray_m159A892BF129C643EDACB6B6AC4A5C594A480B14,
	PayloadData_ToString_m4ED9CDFF8B40A579AF7F6DAD7EFB03D949AB766F,
	PayloadData_System_Collections_IEnumerable_GetEnumerator_mCC8CC4814763D02EF32FA08B1C495F9E8E95F03E,
	U3CGetEnumeratorU3Ed__21__ctor_mAE2E6399EE6E9360ED599C0A7727F6892D6F89F2,
	U3CGetEnumeratorU3Ed__21_System_IDisposable_Dispose_m3ED8AED0B547BB7C7EA032144F7F4475DBDA4223,
	U3CGetEnumeratorU3Ed__21_MoveNext_mFEF779ECF951DBB50781A08566D6ED0ACE0C4FE1,
	U3CGetEnumeratorU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mBD381491005684B4A461F4DB2988F50759CA4341,
	U3CGetEnumeratorU3Ed__21_System_Collections_IEnumerator_Reset_m6FAE01735A2D41A989B3580049A701293A1B5EC8,
	U3CGetEnumeratorU3Ed__21_System_Collections_IEnumerator_get_Current_m1C06D1B885B472BA6EA791A25308459B39EE0706,
	WebSocket__cctor_m1621B89CCD07A6EF5C68CCBFFBFE2F77DAF31166,
	WebSocket__ctor_m3B00E2F9E1092B98725C30482433DE5DE7343284,
	WebSocket__ctor_m88F26D04167D968209E3D602A101399D0141F4A6,
	WebSocket__ctor_m3113A80584F6A176AFEAAF9A9831A8086AC23BD3,
	WebSocket_get_CookieCollection_mD457FD1BECF09C0C9EBC56AE3EAC6C6744C1D089,
	WebSocket_get_CustomHandshakeRequestChecker_mA982EFE77768DEDB804CF718205CA0443FF1A09E,
	WebSocket_set_CustomHandshakeRequestChecker_m91F74C4820298EAB914DFBAD9CF11E4E53A469BB,
	WebSocket_get_HasMessage_m7985370086F1A03A99F5EA58508CE413239F1240,
	WebSocket_get_IgnoreExtensions_mBEF78CA4D16DFEB4DB808B45C2BDED334C8432A7,
	WebSocket_set_IgnoreExtensions_m29201A16F32C52024891A3F325835573CECAC5DD,
	WebSocket_get_IsConnected_m92FBE7A173EA5C44C4F90FB8C9F832F9CAF3C18C,
	WebSocket_get_Compression_m159ACF48A333642A9CF318FF7A0D3AA6674C201F,
	WebSocket_set_Compression_mA5ABD83F4DAE8B28FD211A2A2F7B9DABBF758BAB,
	WebSocket_get_Cookies_m7C2238B44BD33D3FC00E3F0CCCCAF648D40F7A96,
	WebSocket_get_Credentials_mF3A32B582B4C4474996D309DE9C3264EA8584BA5,
	WebSocket_get_EmitOnPing_mA3816204AAE08B83001B14B49D247D9CB60F2769,
	WebSocket_set_EmitOnPing_mF79EB62A8016ECEAF30E4C3188D9219D15F08F49,
	WebSocket_get_EnableRedirection_mF5D0308770B0F34394F80871F42587D3C64EB97F,
	WebSocket_set_EnableRedirection_m88F23E8C8465BE7E4223CF250F6C62B64BF31D23,
	WebSocket_get_Extensions_m0A13AA3AF175C3D29EF6E233FF5133C7BD8234F9,
	WebSocket_get_IsAlive_m98002C68FCC3D5ACB87FB926617C86D41B9DCEBE,
	WebSocket_get_IsSecure_m6BD6D607830069377195D8494C64666BEA07AFF2,
	WebSocket_get_Log_m9E090D5AB9E455144819B631896397FCB6ABEDAE,
	WebSocket_set_Log_m7DB64A11564925FD8F641C861E6E80CBF8D91940,
	WebSocket_get_Origin_m38B9FAB346BEEB864F65D51ADCE0DD0ABEE9E9D6,
	WebSocket_set_Origin_m9E94B0806EBA320A1D539EF4FA5547F8BD759285,
	WebSocket_get_Protocol_m54A3B3032B49C0450345EFFB0484254ED495E2FD,
	WebSocket_set_Protocol_m715DBC96EC0EF90B67962998BE216AEFA84C7CBF,
	WebSocket_get_ReadyState_mCE3BB14B47B93D609BD23883FE3D0842BD22ADDF,
	WebSocket_get_SslConfiguration_m02154604CA8E2344709140ED9A3019A99C594449,
	WebSocket_set_SslConfiguration_m23C6FBD64760519641A28C8131688124943F1A9E,
	WebSocket_get_Url_m950920B3F521186470AA63919CA4C8D054673D92,
	WebSocket_get_WaitTime_mF31AC1BAE02AF378DC91835A1AE5C190F0C8F5F5,
	WebSocket_set_WaitTime_m2EF55C6AE3992D02C8DB52E26D964A746D350DC8,
	WebSocket_add_OnClose_m61B5A75D1BD6449F82DF10B04ADA81671973973B,
	WebSocket_remove_OnClose_m243CC74F7038A97DF19D67C4A0C546E28671EF33,
	WebSocket_add_OnError_m3CBF661D2FC3AC3565AAC0910DC49494C965EDF8,
	WebSocket_remove_OnError_m983FA9CC52960D91D37E159443C0812A4ACC424D,
	WebSocket_add_OnMessage_m2C9ECD30437B12D0936EB5457B853F693ECE2295,
	WebSocket_remove_OnMessage_m8054EDBEC1DCD087ACAEF25551A883FD52110CBF,
	WebSocket_add_OnOpen_m3D14BE0E2AA0AE6541FA4EAECB65097952F41E6B,
	WebSocket_remove_OnOpen_mCF9B71DA18274FC1D460FEE811AF78C25858D52F,
	WebSocket_accept_m697448DF0BCA0CF5737B123752181C05FDEBBFB2,
	WebSocket_acceptHandshake_m73C69E3FAE88F1971098D4B42E3177451419B0C8,
	WebSocket_checkHandshakeRequest_mC04005AB305318BF534932C689902B4138144F9A,
	WebSocket_checkHandshakeResponse_m7A83947080F9B878642772FDE3BFF6902F427318,
	WebSocket_checkIfAvailable_m11A5DF0BF7FF28F748607CEF55A019F16AA2E282,
	WebSocket_checkIfAvailable_mB34105EE87DE969ECB0665299A597B046FBD4E61,
	WebSocket_checkReceivedFrame_m5AC31E961B09369349AB507DC98A78B092C7B7C5,
	WebSocket_close_m75E2333541140BFEAFD9BB7AC92FBD01FA17175D,
	WebSocket_closeAsync_mB5084C60E9F4051605DEBF3B1B1C4FBD34176713,
	WebSocket_closeHandshake_mDB84B6304C26685A32A57E9511F863C989050D3E,
	WebSocket_connect_mD803C50EB116A5E249A62BF6D2BAA3444D31DB77,
	WebSocket_createExtensions_m17B67429C0C90A30D9DA5CC9B2FEDE71205EEE47,
	WebSocket_createHandshakeFailureResponse_mAAD433D8D5A514D3200C2C2B4704D6DC1D731BA7,
	WebSocket_createHandshakeRequest_mAF8411FF33D72360754A71358B5C399932053691,
	WebSocket_createHandshakeResponse_m5BD4825C8F5182EA4E844ECDACA48FA5F5F60E7E,
	WebSocket_customCheckHandshakeRequest_mCFD5C1C0D9E62B7A65C6EA4F94989EFA61464ECB,
	WebSocket_dequeueFromMessageEventQueue_m750A85233B56C2C1F135D725BC6A8F2F904386EB,
	WebSocket_doHandshake_mBE35DE03A899D74070C5D81E4032B0DE62427730,
	WebSocket_enqueueToMessageEventQueue_mBBB11637467DB5D416DAC7DB3A1D9838D94B9D51,
	WebSocket_error_m851827605CE244482FDC13B105D9C189FEEA4C79,
	WebSocket_fatal_m2F2AEF1F47CD08B37A7500DFFF715E8F616C0832,
	WebSocket_fatal_m0E50A2991B9C844EB4CF2C8FD80C73B263E8BEEE,
	WebSocket_init_mB932C1D03011A46C11B344FEC7A8CFB1034639A0,
	WebSocket_message_m81F70EC7AF969AEF39900D149918DF2548342559,
	WebSocket_messagec_m699DB7DF9EC7FC9BE4CB49D10CAAA1697AB8B432,
	WebSocket_messages_m79F157A00E17E0298871A9560045C400F3C8AAF7,
	WebSocket_open_m589826A86F87DF7858D9FCD54B28AD5EC89AF5D2,
	WebSocket_processCloseFrame_m8B698607360D2BBB0A71042516DE047152A268E1,
	WebSocket_processCookies_m34A3DC74D1EF2288C287C7FE20EE64E570977936,
	WebSocket_processDataFrame_mF9531E7600B6E23BB4565128CFDC4BA1541B8C26,
	WebSocket_processFragmentFrame_m902B531D18313004A571477669B5202C7F2CE0E2,
	WebSocket_processPingFrame_mADAF9E172323A41AFB871D79E69D431D91E72049,
	WebSocket_processPongFrame_m67485968DF8BA862348244E2A29C1ED073286D9E,
	WebSocket_processReceivedFrame_m32DB1CB48BE005AD0B82B6E78120483418465BE5,
	WebSocket_processSecWebSocketExtensionsClientHeader_mB4BD5A73BAA57A8A2C4827BBCA211A3D757D4821,
	WebSocket_processSecWebSocketExtensionsServerHeader_m729A672C97794F5064F96FA46F58C1900FC4920A,
	WebSocket_processSecWebSocketProtocolHeader_mB36F8AABACC411CF16364952A2F0EC17D079F5D6,
	WebSocket_processUnsupportedFrame_mCCA7EA3371D286EB406F757488CB9A64E0E0ED9B,
	WebSocket_releaseClientResources_mF7BDEA8CEA82A696C521737A77D5A78396509EA4,
	WebSocket_releaseCommonResources_m16DA2B4DFED0D51BE08BE7FDA92C8DAFFFF2B4FB,
	WebSocket_releaseResources_mC98420FB0C8F370D763CE7D3A2672045966905E1,
	WebSocket_releaseServerResources_mD154B640F2D9825EE568B1CA4888C085A9721C9C,
	WebSocket_send_mD4A3B874B4413392230C3A59903ACD95FA3C5740,
	WebSocket_send_m3449CCDE2282E2759204FD1EBC85797837958B87,
	WebSocket_send_mAC5E98BE0CF29922C0D992E04600ACE5D85260EC,
	WebSocket_send_m3CB93B62ABB36C86952E21CE6EF239FD004808AA,
	WebSocket_sendAsync_m2FC169BCC46E9D79D478CF54CC21F7BAF13F2EA2,
	WebSocket_sendBytes_m9B25CCC90133A2AFF9363BFB028C134CBCC909DE,
	WebSocket_sendHandshakeRequest_m256C62FFF89D52590E7191711193FB8DA72C63AE,
	WebSocket_sendHttpRequest_m15FA5B9AC082FE119B6D534315A00CE17E73A034,
	WebSocket_sendHttpResponse_m6C0E74225E8D4344C34D101020C1F0DDFC415A77,
	WebSocket_sendProxyConnectRequest_m57C03EC1F9FD0AC95BFB7489DFE9B0EB978133A4,
	WebSocket_setClientStream_m7314C7A289D5BC4E90F3753BCE998DFADF8EF106,
	WebSocket_setClientStreamOriginal_m82BA292E86483174DE1FC63E45FF9B5633506BF8,
	WebSocket_TrySetClientStream_mEC08D07CF96334479832E6264F105965DF12B38E,
	WebSocket_startReceiving_mB6FA9F774E4A507F51E3441E0765D4A3E03391D2,
	WebSocket_validateSecWebSocketAcceptHeader_mCCC7BC0B0883A88A448703D85C4DB08AA80EC6A6,
	WebSocket_validateSecWebSocketExtensionsClientHeader_m67980345FF623A6CE531AC5AC515029264429883,
	WebSocket_validateSecWebSocketExtensionsServerHeader_m5D0557397DC970AD34539A857FBDDC1F557DBAA6,
	WebSocket_validateSecWebSocketKeyHeader_m7D9A371203127958BA6001DCF603386223FD7E55,
	WebSocket_validateSecWebSocketProtocolClientHeader_m52652F67B73834A9FFF348EFDAD6E15A4008C200,
	WebSocket_validateSecWebSocketProtocolServerHeader_m368BCDE95BBD863109D1875912997CC1A23E6356,
	WebSocket_validateSecWebSocketVersionClientHeader_m5787288B62F674073C9DEB10EB046CB5649A1018,
	WebSocket_validateSecWebSocketVersionServerHeader_mFF1B314E4F105E4BEAB00CF21A8B64C1FC06825B,
	WebSocket_CheckCloseParameters_mABC996DB8E6C1F21590CFE6DC0494EADCAE539F2,
	WebSocket_CheckCloseParameters_m68CF9B623D19D8B800BCD17197A146F16801E5C4,
	WebSocket_CheckPingParameter_m42DD83060467F52153C03D8E37CD91029AE2C164,
	WebSocket_CheckSendParameter_mC12718C0FD3BA0AC67A5874FAF9D6F9654E71F71,
	WebSocket_CheckSendParameter_m2DC38DD68457607A050BD8D73BD21B47FC1D270A,
	WebSocket_CheckSendParameter_mCA4DDB25557AC78ADA103D8DB6EE1AF9612BBFAB,
	WebSocket_CheckSendParameters_mF7894EF507B47ACB07044F8A8A46F6245246CEFA,
	WebSocket_Close_mA38C53008CEDA5EAA3B67BEC3D2A23DDC4EDAB36,
	WebSocket_Close_m215B38800A7BC8151C3522DF9D2B8E030702AE71,
	WebSocket_Close_mF976981830E95656BFE13471D2F61EAE67CD69E8,
	WebSocket_CreateBase64Key_m6703E8338082186E13606F0F2E982FE5E1BFBB26,
	WebSocket_CreateResponseKey_m4C85F7941191B46A2A8124A2C815DEF55C55F3E4,
	WebSocket_InternalAccept_m3BC602691FD85E1D1694BFD2B8402B2BB346E61A,
	WebSocket_Ping_mD062DFF5A38BCFEE037AB6F86A63DD8F15E753FE,
	WebSocket_Send_mA345E498AA572644D98E73EC3A92E2BD1315FD40,
	WebSocket_Send_mF6DB0EAC501163009FB45913B36537467B21222C,
	WebSocket_Accept_mCEAC67EAD50C9D969D2A8EABBCC684655C8A1871,
	WebSocket_AcceptAsync_m54996157EDC9846359B1D2EF4E650234B11E044A,
	WebSocket_Close_m21143A7A9234BCCA0496FF2A765F899734A0D0A5,
	WebSocket_Close_mBA7C615786C6FA34C038E0812457B023F10AB4A4,
	WebSocket_Close_m87B89C4CFFA7AF0F82C5C4B1040B2A5E37991A18,
	WebSocket_Close_m3EBF84F346CC022EB2535A3342DC2D079625A2A4,
	WebSocket_Close_m9EEB56FC521BFEEF27FC379DD09276A84B6F53C0,
	WebSocket_CloseAsync_m41986F2722D2D612B33C254A18CD3C4A0D90EB79,
	WebSocket_CloseAsync_mE55F91EF91A581064CEB4B4D88F1D238745465FC,
	WebSocket_CloseAsync_m9BAFDF6538AE6EB87068A6F4296980A0A4BB6651,
	WebSocket_CloseAsync_mA175695B7D497C5B5626666DFA09B4517FEA8E0B,
	WebSocket_CloseAsync_m35C92D5F4267DDBC8E2324C9E6E941DF9FD85829,
	WebSocket_Connect_mD0F171D175A667AD6B6B551DB92AA4E550636E8F,
	WebSocket_ConnectAsync_mB58FAD1540B0C2DD96379997F2604B3A2B6655E1,
	WebSocket_Ping_m28693025CC15C745A836BA9B78A9258308D1D064,
	WebSocket_Ping_m7B06B066EFAC73CA5C1D67A658F05660E584828E,
	WebSocket_Send_m820637BD0508765A60DAF71A702BCAD99B8C8CCA,
	WebSocket_Send_mF82393046DF4A1832EF424B326313803F05C3DB9,
	WebSocket_Send_m22D6D6E76DB2F8AA9D9C8EC350B607176852BC90,
	WebSocket_SendAsync_m11CC853F4E91EA25F53BD3612B1AE0C4C4C05CBB,
	WebSocket_SendAsync_mCF36B00A6C6C13E6EFE6E2BAE7C1CED43A8315E8,
	WebSocket_SendAsync_m7212F00F9DCC53617CC32BDE6AAD2B7E69415DD3,
	WebSocket_SendAsync_m8130CC33A6AB1BB6A2C7DC67EF39DF936777ED48,
	WebSocket_SetCookie_m9EA39295E3E5B29AA34BB86DF44FAE35A4D07047,
	WebSocket_SetCredentials_m213FB163E427A632F8530ABCF998197986B14374,
	WebSocket_SetProxy_mD825B512A6BC7B3EE4FAFCE76F96B40507627482,
	WebSocket_System_IDisposable_Dispose_m17688B3E5596E7127711913DCAFA2B90E9F21350,
	WebSocket_U3CopenU3Eb__139_0_mEAAB03D1976E0F661C7A971942699BFF1337C889,
	WebSocket_U3CprocessSecWebSocketProtocolHeaderU3Eb__149_0_mCD1C6EBCE8132B9AF7270BBDD6E159DB1EB757C5,
	U3Cget_CookiesU3Ed__67__ctor_mA33DC5CCBB1C430496CB7D2CB3725E00535E9871,
	U3Cget_CookiesU3Ed__67_System_IDisposable_Dispose_m120F53BE27792169AA77EE32031232EC1DD48368,
	U3Cget_CookiesU3Ed__67_MoveNext_m80C5E04FD1A93DD25967817386BD08A1F57E6AC4,
	U3Cget_CookiesU3Ed__67_U3CU3Em__Finally1_m27AA296EE1D243C3453AC4DB6E154B0F90D2D11E,
	U3Cget_CookiesU3Ed__67_U3CU3Em__Finally2_mF4FF9F7F82E1749F68898F20ADD077703B0CA117,
	U3Cget_CookiesU3Ed__67_System_Collections_Generic_IEnumeratorU3CWebSocketSharpUnityMod_Net_CookieU3E_get_Current_m27F768172C5DBDFE9EC38E7FDFD8F4FB51AD6B31,
	U3Cget_CookiesU3Ed__67_System_Collections_IEnumerator_Reset_m2F9764C52EF17FC594538AC778D4806B154C80DC,
	U3Cget_CookiesU3Ed__67_System_Collections_IEnumerator_get_Current_m5C6D8CFAC8FDDD6E0E2D18D0295EA418F6A18568,
	U3Cget_CookiesU3Ed__67_System_Collections_Generic_IEnumerableU3CWebSocketSharpUnityMod_Net_CookieU3E_GetEnumerator_m830E275A5931F3F18C0BFB751A21EB4AC2E14D5B,
	U3Cget_CookiesU3Ed__67_System_Collections_IEnumerable_GetEnumerator_m21BC0942BE82FF00608E1ED2B709C3CAA3945B9C,
	U3CU3Ec__DisplayClass121_0__ctor_m547E7639FE6F7C1D1EF62EF9FBE20007DBFD8E7F,
	U3CU3Ec__DisplayClass121_0_U3CcloseAsyncU3Eb__0_mB294DAC159F60F65C389D4A594980DE88EFE32F5,
	U3CU3Ec__DisplayClass138_0__ctor_m830FAA3C5DBA1FEEAB65976B4DAA3EFB102ADFCC,
	U3CU3Ec__DisplayClass138_0_U3CmessagesU3Eb__0_mF67875B6693DB2905536C36427F2701E585E3946,
	U3CU3Ec__DisplayClass159_0__ctor_m6D695657E53F0063C744AD9DB73C50395B96ED72,
	U3CU3Ec__DisplayClass159_0_U3CsendAsyncU3Eb__0_m73A5F394CD664634D8CDA55ACEE9F5E3992E6AC2,
	U3CU3Ec__cctor_mF12A52F702DD83A0C0150EE002251D906E9D6FA1,
	U3CU3Ec__ctor_m4C55FD0206B73878AF2D0C1E1C03C39C37EAF059,
	U3CU3Ec_U3CTrySetClientStreamU3Eb__167_0_mFE9365C6ABF98269FB1231B2B183AE9E0EADD092,
	U3CU3Ec__DisplayClass168_0__ctor_m400AB6AB21A9C03EA1881537415B189EF43B9034,
	U3CU3Ec__DisplayClass168_0_U3CstartReceivingU3Eb__0_m5579419F28117FBFCD5C4A709B7A623300CDC581,
	U3CU3Ec__DisplayClass168_0_U3CstartReceivingU3Eb__1_m593314F2F89370EECEA4C7E216141B0B7C74467D,
	U3CU3Ec__DisplayClass168_0_U3CstartReceivingU3Eb__2_m9036DFB0BE0143AC8A6FB0E14C1078F6212FCE9F,
	U3CU3Ec__DisplayClass171_0__ctor_mF04550C79381B6F0B8787DBE507D2008931AA1DF,
	U3CU3Ec__DisplayClass171_0_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__0_m22C61D5EA139E431F1D048C7645ACB3BA4AECA9E,
	U3CU3Ec__DisplayClass174_0__ctor_mC932F430FCE6E7117A1A3A76F036A1E582CF86BA,
	U3CU3Ec__DisplayClass174_0_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__0_mF035C86F330210AE2D3EE130676C8AE85235F50B,
	U3CU3Ec__DisplayClass194_0__ctor_m0C7EF572D78FB065892FEF04DE01B12825021A77,
	U3CU3Ec__DisplayClass194_0_U3CAcceptAsyncU3Eb__0_m225D4A59A6A9FAD522DE5BAD241016F6C4EA7FEF,
	U3CU3Ec__DisplayClass206_0__ctor_m2C25264FFE6F15C049E0A50180EF7755E9DE614C,
	U3CU3Ec__DisplayClass206_0_U3CConnectAsyncU3Eb__0_mB2D12F810A2D42CA085E1CA6F6A95A62C4455823,
	U3CU3Ec__DisplayClass215_0__ctor_m8670031BC15C89EF26B0A99963B12B6B6EC9B834,
	U3CU3Ec__DisplayClass215_0_U3CSendAsyncU3Eb__0_mEF09AAE407D92AC5D98C739C639DDEEED597F1CC,
	U3CU3Ec__DisplayClass215_0_U3CSendAsyncU3Eb__1_m1BC498D98724B2E038C19493B2B2CCAD78281F4C,
	WebSocketException__ctor_m6CF8B8F9FC1E642EBD5D7BBAA132A324DC398271,
	WebSocketException__ctor_mEE5C74C868EAA9061B1654E99610F10488FB8505,
	WebSocketException__ctor_mEB0B76AC7E8439BC184B642EBDC0BCC57FC4B51B,
	WebSocketException__ctor_mEC397E314EEA9683DFEE43493E45AA5AF72EA8F0,
	WebSocketException__ctor_mD01D65C75DE8D7BEA64C99B9AF66B33FBD85D7FF,
	WebSocketException__ctor_m1E3E2C4323667CAE32EAFAF5A0A09E505D2AB7E1,
	WebSocketException__ctor_m2DAE24EF1B6BF1AAEB9CBD336D85C1A12971426A,
	WebSocketException__ctor_mB5B94664D00CF8AFD9B62D29654C2DE22A08F1EC,
	WebSocketException_get_Code_mC6D5F6744FFC138CCCEFF32B1E397873B97C4E60,
	WebSocketFrame__cctor_m306E895FE0050355F640C8BFAA418D9BFAA639C3,
	WebSocketFrame__ctor_m3F03D51743E865CA024A154DBB076C495DD8A8A0,
	WebSocketFrame__ctor_mE8D0C4FF6160480D33DB99244CEF72726BFB0CCA,
	WebSocketFrame__ctor_m887512DA86D1B4D0E36EE0B1A8EFC01F997D76B5,
	WebSocketFrame__ctor_mFAB822ED235720AA0AA8BD844236D95DDE2E6FAE,
	WebSocketFrame_get_ExtendedPayloadLengthCount_m2B0C5F06A5206B19F268BB73A8C04529BF47F99E,
	WebSocketFrame_get_FullPayloadLength_mD2BA70BE17059347350BA73C9CF033361F81D035,
	WebSocketFrame_get_ExtendedPayloadLength_mC5536F16FC0BF0DE8AE6E1D468DF32866A5109A7,
	WebSocketFrame_get_Fin_m6194D4FCECA2C34922495CAAD7CE6DABD720C6A7,
	WebSocketFrame_get_IsBinary_m2133AA6E22584B6C6F62C3C385279E53E3328DB6,
	WebSocketFrame_get_IsClose_m9B114E30FDEDD6CC5F2CBADB8DAD90F0C3427643,
	WebSocketFrame_get_IsCompressed_m974859F6DC198592695B82869419424E0445114C,
	WebSocketFrame_get_IsContinuation_mACA6E0BFD323E8393E3C07878E5F5EDA68EC2693,
	WebSocketFrame_get_IsControl_m4575F8C0DBC05A511D3778D32C67FFF001F49BDA,
	WebSocketFrame_get_IsData_m945ED3349595C9F5C5A76C369DD8B3AF2F5F617A,
	WebSocketFrame_get_IsFinal_m4161D41E732D85056FA7566C76E35E2DC0F3DBF0,
	WebSocketFrame_get_IsFragment_m2CB41DEB930EFED16B2CA01C0361E0DD07C6EF71,
	WebSocketFrame_get_IsMasked_m7C3DB62A3A9D8771B09B4A8F7F7328FBDE9B3824,
	WebSocketFrame_get_IsPing_m41842E30E7536A0229DBB12880FB523461B8F22F,
	WebSocketFrame_get_IsPong_mDA60DAC471ADD50D4174D6AEF0725CFEA6083A35,
	WebSocketFrame_get_IsText_mD91BB86C74A951BE1B0E4610B84B4D23514560F6,
	WebSocketFrame_get_Length_mEA6DA6BDEFB7591A094DF94C888E69B596F80D3D,
	WebSocketFrame_get_Mask_mD9A66CE1F3F7D1BC1169913F73A4406500F7C61B,
	WebSocketFrame_get_MaskingKey_mDD927C3B606FDEBD8A63B778A52CADB60D822268,
	WebSocketFrame_get_Opcode_m8269669F172338E8157B332AE82757D336ACF1CD,
	WebSocketFrame_get_PayloadData_mA0795CD18EE80F7600F7A89C22F395D0A2D098F1,
	WebSocketFrame_get_PayloadLength_m5D417915B7CE7086273302366DA9D86C1C7F6103,
	WebSocketFrame_get_Rsv1_mCAADEEC7F978EE57B1B6EB0CAF9535D6352F059D,
	WebSocketFrame_get_Rsv2_mFB76F4F96807CC0C4BD01EB4E6EDD5688CC1425B,
	WebSocketFrame_get_Rsv3_mC45F590421DACC0716F026A739E96691F690CB75,
	WebSocketFrame_createMaskingKey_m8DDE3770B5097F09AFA78EE05235672DCF52C1AB,
	WebSocketFrame_dump_mF57C01EFF82A53E0EC25E134124A2505FDB848FF,
	WebSocketFrame_print_m0847EF6DF29B0CA0BC84CAAA0811B9435D60C5FD,
	WebSocketFrame_processHeader_m06D22200932D70BDB3067F6EE0614551B4B227AB,
	WebSocketFrame_readExtendedPayloadLength_mC91AA425B2B2B469DAF67D00473F68A54CE6FE94,
	WebSocketFrame_readExtendedPayloadLengthAsync_m5693FDEE040A36CB6DC5F403846B417DF55D2A65,
	WebSocketFrame_readHeader_m6294E5E3A4F2D07E74F01F0DF73A61D798CF077E,
	WebSocketFrame_readHeaderAsync_m99D44CA06C8709E7BDBC915A5CDA18F6BEB16FC0,
	WebSocketFrame_readMaskingKey_m0DD971CB47C6DDF8C477FE4ED344D04E6231C399,
	WebSocketFrame_readMaskingKeyAsync_m8D78333B7D08CEA12A923C2C90957302AF451602,
	WebSocketFrame_readPayloadData_mE6BA47846B2FB639890653E9EAA49C18F91E3948,
	WebSocketFrame_readPayloadDataAsync_m59F0E48D63D636A686AE43F1F8A1C78DFEFBC162,
	WebSocketFrame_CreateCloseFrame_mD675C61B1F9ACB7064C9013A73BCF6114CDBC7B7,
	WebSocketFrame_CreatePingFrame_m0F86473DCBDDC18016D98403C26A8C70402C3FEA,
	WebSocketFrame_CreatePingFrame_mAC9AC1658B687E512076F19DA0366D7C8A09F41A,
	WebSocketFrame_ReadFrame_m747A50D544B6A8B5B17A69D8427C8E982C7F0C78,
	WebSocketFrame_ReadFrameAsync_m95E4BFE723179248816DC4075F6C7CE4FC00455C,
	WebSocketFrame_Unmask_mE942269D91488F3AA15337C21BA67AC71446D05A,
	WebSocketFrame_GetEnumerator_m8E680EF0D7044772DF92A1787524AB229C211171,
	WebSocketFrame_Print_m800D8C5C8290A0D69F5E7B728F5A5668AA422A98,
	WebSocketFrame_PrintToString_m6907610E78C2EBC00CEC34A5EAC884921ECFFC5C,
	WebSocketFrame_ToArray_m2E2031DCD593D785EB83F28C23B64FB6AAD1D61A,
	WebSocketFrame_ToString_m6488C2433A975E2DE05F24B5ACE715D75F47965D,
	WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_mBF47583EC2A918BB95EA58C5245FB89D2877167D,
	U3CU3Ec__DisplayClass67_0__ctor_mF8C02948947A91EF20640D4C03895B15493A75E9,
	U3CU3Ec__DisplayClass67_0_U3CdumpU3Eb__0_m0E2FF95B4F256730D183AC924AC612332428EEAF,
	U3CU3Ec__DisplayClass67_1__ctor_m38F304A5E077CAC4607E80BB0F8FA596EF0482D1,
	U3CU3Ec__DisplayClass67_1_U3CdumpU3Eb__1_m5A6CCBFD6FCBFAA683EEAA2CF3D4283BB8027C21,
	U3CU3Ec__DisplayClass71_0__ctor_m7A8DA592C71590119A2993D9D0129801D1444F7A,
	U3CU3Ec__DisplayClass71_0_U3CreadExtendedPayloadLengthAsyncU3Eb__0_m41E5D0DB494B6E4937836DA4D9CA8DA9E5237C52,
	U3CU3Ec__DisplayClass73_0__ctor_m4D28650F70961B2D119DE949A1E8081838B445CA,
	U3CU3Ec__DisplayClass73_0_U3CreadHeaderAsyncU3Eb__0_m02B9CFE8F4A2BA29D75BC45200923DC0C72DEFF8,
	U3CU3Ec__DisplayClass75_0__ctor_m748B1B593B58E4358C90E2CB753F3624028DBEE2,
	U3CU3Ec__DisplayClass75_0_U3CreadMaskingKeyAsyncU3Eb__0_mEB3B5BD88B3B84929559B7E877C57C5BBCFD2CD0,
	U3CU3Ec__DisplayClass77_0__ctor_m1F01DC275A7EDCB7CFE3C60D01BE54B454EE6B01,
	U3CU3Ec__DisplayClass77_0_U3CreadPayloadDataAsyncU3Eb__0_mE68262BEC24352220A3D34FD0615908269CEF94C,
	U3CU3Ec__DisplayClass82_0__ctor_mE6ABAC7FA8339553CB8E3EE96305DB53DBC09BB2,
	U3CU3Ec__DisplayClass82_0_U3CReadFrameAsyncU3Eb__0_m8BC28AFE7C56836F91E0D3C08D1F21DE17F3DE18,
	U3CU3Ec__DisplayClass82_0_U3CReadFrameAsyncU3Eb__1_m12C5828FD3A8819F82888F321A02EA937F1D7DCE,
	U3CU3Ec__DisplayClass82_0_U3CReadFrameAsyncU3Eb__2_m128FAE7299E07EE2DC330A5F8FE97D0423FA9D70,
	U3CU3Ec__DisplayClass82_0_U3CReadFrameAsyncU3Eb__3_mB5D3BE45056FF03E9FC72E9BBF9D27F9F0C773C8,
	U3CGetEnumeratorU3Ed__84__ctor_m61807D2EA695A4A5005433E74F396A53CD92E073,
	U3CGetEnumeratorU3Ed__84_System_IDisposable_Dispose_m70E795E27D6B4ACD82D221ADBD0E213104A24343,
	U3CGetEnumeratorU3Ed__84_MoveNext_m605B746C044D216AB3D5F9DA4D05B3FFEC760B16,
	U3CGetEnumeratorU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m3C777653C42EBA2B5339751391137F14C1DB252C,
	U3CGetEnumeratorU3Ed__84_System_Collections_IEnumerator_Reset_m6E22B0CD5FD3BF05685631AE24061B97A0201343,
	U3CGetEnumeratorU3Ed__84_System_Collections_IEnumerator_get_Current_m1B418000A17166EBC22478C28B78DA70D30BD10C,
	HttpRequestEventArgs__ctor_mE0B7F5E9C736F36B413E09A9DA01650A1F16432E,
	HttpRequestEventArgs_get_Request_m7188D6F345DEF2FA8D64025BF8B20AB6AF1CBEDC,
	HttpRequestEventArgs_get_Response_m811886A92305AD435F2200973BF49338945CDA0E,
	HttpServer__ctor_m3969441ABEB298622CB66F0FBC0FD67F5FF257BB,
	HttpServer__ctor_m084A6C5FFD6ECF610B00BFF852DB855ADEA3DC22,
	HttpServer__ctor_m23B4F82081A43677745D3C3B4351C7D24112EF14,
	HttpServer__ctor_mA23FCEDEA55160C386E074AFD918AAF58D1147B7,
	HttpServer__ctor_m8B3783538D19B929F66167E1FB73F00BAA647F82,
	HttpServer__ctor_m9BD0356C662F3C664054DD3849922E119AEDF7DF,
	HttpServer_get_Address_m2576FB0B49DEABABF7DFE8904CB5961EA69AACA4,
	HttpServer_get_AuthenticationSchemes_m23BA33F6476A95A6C17BA4532DE83E86A90DAD56,
	HttpServer_set_AuthenticationSchemes_mBD4933EED3269102C93B056085CDA3BB6B5F0F08,
	HttpServer_get_IsListening_m8E2400CFCB208D3C557EB37CA47A31E8E366A461,
	HttpServer_get_IsSecure_mE5834DB1C6ADDE074FFE05F680C1349F716C9CC5,
	HttpServer_get_KeepClean_m76E8276230E931FC525D8374164F53F1A88B8EB7,
	HttpServer_set_KeepClean_m9362782483BB16088728D89358D8308338E1A7AC,
	HttpServer_get_Log_m7781C03E8C460B9241E49A70C553566EC2A87481,
	HttpServer_get_Port_mB6D435AF5494EA5A290420543A94DE1C704B55AB,
	HttpServer_get_Realm_m6DD8D5A61EC61B5AFBB0502B99AF92675668C1AF,
	HttpServer_set_Realm_m827DE665E3D95979EDB43F4D8C14C865D644AA06,
	HttpServer_get_ReuseAddress_mF4895B41C4533F8AE8822E584620476D699F143C,
	HttpServer_set_ReuseAddress_m750B0097D7968055ACC3B9E2AB4ABE26B40122D0,
	HttpServer_get_RootPath_m98229F6AA86F7D2F6C51513E087C6F5BE8D117C7,
	HttpServer_set_RootPath_m4022A901A9E0EA86E6637F1B4B8AF4D63929509F,
	HttpServer_get_SslConfiguration_mB0B0B4279C596134523767B55174CDEB1A73484E,
	HttpServer_set_SslConfiguration_m60278C094D4F6B81CE32BDF1F6CB4E9E59EB88CC,
	HttpServer_get_UserCredentialsFinder_mB68D3398F1F94B1E1BA582E36526CA7F375DA20B,
	HttpServer_set_UserCredentialsFinder_m1FBE3917A2FBDDF448954F730744C503287CCCF1,
	HttpServer_get_WaitTime_mD35FCA8F0624B49D68C51F2BF1AB8EF1B33A0DB5,
	HttpServer_set_WaitTime_mBD0CF7B6BD6C2722BCF7E5F73CB3E64800429879,
	HttpServer_get_WebSocketServices_m1343E0F3ABCF66E7E31DBD6348C9BA697BAAB89C,
	HttpServer_add_OnConnect_m253A2D2EC681364297E4AE5C648C0891B92286D2,
	HttpServer_remove_OnConnect_mD5510A7CE2CECF16F47A8D25D5678DEE876D323A,
	HttpServer_add_OnDelete_m44C77148C1E5005E491A0F3741D32181ABE7B416,
	HttpServer_remove_OnDelete_m4B5DEB36E8B4128B65179E3B98346F4C02FD0711,
	HttpServer_add_OnGet_mCFE88CC3C6A162B711295EA4236F59F69093390D,
	HttpServer_remove_OnGet_mF697034A5E8F7B199BB93338B2323E3705F02DF5,
	HttpServer_add_OnHead_m7C50D1BFC64B7C324A31ED04ACDF18A3E14EBE96,
	HttpServer_remove_OnHead_m7B040610C3EA55400834D6B66D4A9715BE575199,
	HttpServer_add_OnOptions_m772E05CA32374497B14D44670FE5638EA97C1781,
	HttpServer_remove_OnOptions_mCF127FA31C9F9AA2B528451EF43F31C4D6FA2629,
	HttpServer_add_OnPatch_mB1F3937DB8491C80060EE4B205D19C3400340C8E,
	HttpServer_remove_OnPatch_mED2513606534E53ADF3FCEBF5B1C7E859F4D3BC1,
	HttpServer_add_OnPost_mDA23DCBFD1CF0FA8B8B2D6BD5536442E4E2F9619,
	HttpServer_remove_OnPost_mE78E6BB87EB113F26ED15CF2792D5367E21903F0,
	HttpServer_add_OnPut_mC1FF6C7F2E80C5F09830EA4CE08B3A6F599D40D9,
	HttpServer_remove_OnPut_m3D3853D50B365C9EA2FDA71F77FA6A44C7DB615F,
	HttpServer_add_OnTrace_m45C2DAE7138F07025428E6F9A9F6EC56585E3776,
	HttpServer_remove_OnTrace_m4B85129150909AB589ECFDC5E52295CC24E41A4E,
	HttpServer_abort_mA38C6AFE2219413FA16E16EBF1C79F470235B0F4,
	HttpServer_checkIfCertificateExists_mFFF3628014257D3056E43C0C6938FC4E450BBA74,
	HttpServer_init_mF554AE2F1AA8FC5BE77762E3BE289B9A5CEDCC7F,
	HttpServer_processRequest_m7359FA062C7A54E7389EBF4FD985CD33DABBE812,
	HttpServer_processRequest_m83F4FDC994D2E78B77C41A14D514B1E00C024FAA,
	HttpServer_receiveRequest_m2A10820A4997C5A836EEA89A5F103C2430CE11E1,
	HttpServer_startReceiving_m4E127CB9FBF4C7E503F08D094E947152E1BBD32F,
	HttpServer_stopReceiving_mD602C3E2AD3EB0587F9374CE177320BC0885E948,
	HttpServer_tryCreateUri_m08D8FC25F19D7C85371F27CC2049984244291F09,
	NULL,
	NULL,
	HttpServer_GetFile_mBFDFE78AC79FCD09D9255A9C1C83722A5AC9FEE0,
	HttpServer_RemoveWebSocketService_m109334F46E479884A7AA7C7F261B1A5946EA2EF7,
	HttpServer_Start_m7EF6EF4C7D863D1D7FF5294023314663940E1A51,
	HttpServer_Stop_m49B2E145294ABA04B1CC2D7FDF6619DC46B7AE73,
	HttpServer_Stop_mCAF538BE77B39C4F558C12B4E17E79F4BE4D757F,
	HttpServer_Stop_m12C6180B3521F2778B616C3EC59EA2CC2733789C,
	U3CU3Ec__DisplayClass86_0__ctor_mCC0E5F7F0855451C61A3A1F61375C49FDFB3169F,
	U3CU3Ec__DisplayClass86_0_U3CreceiveRequestU3Eb__0_mACF2AD9A11635ED2B62A3F62B2011D53EA8D1AB3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WebSocketBehavior__ctor_mB69CD0F8BE079E631AF0425828422167FB129D18,
	WebSocketBehavior_get_Log_m7989008183C959C2D20EEC95D593C609204709B6,
	WebSocketBehavior_get_Sessions_m7F6B6AB299DADD590D5503713CA23A98DC6F2909,
	WebSocketBehavior_get_Context_mD9BB71416FBF52E80975BBDFCE3AC3413E3C1AD4,
	WebSocketBehavior_get_CookiesValidator_m62D859A743F514AF889BEA2D672FF1B60875A993,
	WebSocketBehavior_set_CookiesValidator_mFD88608DF2480C5967C512C0AD8D27C0EC71D14B,
	WebSocketBehavior_get_EmitOnPing_m8B77A4E461D7E5CF1B64E41D09A458F0CFA29BD4,
	WebSocketBehavior_set_EmitOnPing_mDB569002E3F3E5E223FCD08FBA8BF2DE5F82A13B,
	WebSocketBehavior_get_ID_m82FC940BB8335EDFC98BD2BBF0F2351AE15B20DB,
	WebSocketBehavior_get_IgnoreExtensions_m2DB944A7016C790243C545C89137F0BCAD6F0373,
	WebSocketBehavior_set_IgnoreExtensions_mE87692D54808E6DB0178C40E4CBDE58BF46BD99F,
	WebSocketBehavior_get_OriginValidator_m587F2A0B36B278CD8CB941BE4013DC0C328E947B,
	WebSocketBehavior_set_OriginValidator_m33F0210C2E1969E07B7CC689024E980136C64F68,
	WebSocketBehavior_get_Protocol_mCCCFCA255F83B003B5E8B1419A9453D2D640C52B,
	WebSocketBehavior_set_Protocol_mCABE8B7F086BF0D5816C0DA89885DDE32ACA2310,
	WebSocketBehavior_get_StartTime_m6E415E633C0EFD47EE9D5740781EC051018F89BC,
	WebSocketBehavior_get_State_mDB2AF6D0504D9519CB6B58C463F615849AB7B768,
	WebSocketBehavior_checkHandshakeRequest_m6BB3061938DE6E18861822C870E36A86C5BFBF63,
	WebSocketBehavior_onClose_m18DDD35ACA597100CCC7E51B694232F5DEA49E4D,
	WebSocketBehavior_onError_mEF0AF28694877B39491D642E2C35DB1C85F65DD3,
	WebSocketBehavior_onMessage_m12C120F35939E2C366B131ACB89310F59801332A,
	WebSocketBehavior_onOpen_m3DCF2E02A3E3429190F1612514442A55213CE79D,
	WebSocketBehavior_Start_mCB1750F34AD3277A423A5B1E0A64DF322D1D763C,
	WebSocketBehavior_Error_m3A2E53BF08D3B77F74A87EE7791A8B29F7A50DB4,
	WebSocketBehavior_OnClose_mBAE33B8F48CD6F519FBEDE7922344BE9D97BE9F2,
	WebSocketBehavior_OnError_m9FD926C4E48244487D876A752DE014761E99B7A3,
	WebSocketBehavior_OnMessage_m9F31D9FF320C3ADC9C402690894AA3E696D9CDA3,
	WebSocketBehavior_OnOpen_mB911D85CA1721BFE1E31708E61609DE5959B8A14,
	WebSocketBehavior_Send_mBD106C2E60AD33F3C3A9B3DEEFB06BFD14BFB587,
	WebSocketBehavior_Send_m1A19F4767E85EB01AC26CCD66E6EBB2D7091324E,
	WebSocketBehavior_Send_mC481D466582024F86CA96B5037B6A9EC5928600C,
	WebSocketBehavior_SendAsync_m14B725F78AAC6A9235B227DAEC5CEDDEF085868F,
	WebSocketBehavior_SendAsync_mE6ECD5B800114C61E7754FDF9EEC3D63EE98AC81,
	WebSocketBehavior_SendAsync_mDB1C430F6EAFEDDA169E166EDFD5A32A0491C78C,
	WebSocketBehavior_SendAsync_mA0FF0DF16EF4493E9573EADF35573BC8DC72E489,
	WebSocketServer__cctor_m72649DF133AEC9F2EF23F0370E96A5C97E2D1A41,
	WebSocketServer__ctor_m8172E84A867095DC3059A2B5CD929025B39CF486,
	WebSocketServer__ctor_mFB2C38442C2E6CC649521BD06EAB2BC6C2A7B968,
	WebSocketServer__ctor_mD7809553404466B2FF023D876F31216B8BF0F21A,
	WebSocketServer__ctor_mA03A10790B620EEA506B4A292611FF985F6CA28C,
	WebSocketServer__ctor_m1AA10A592E68E82E359F38189FC3D02D094659FB,
	WebSocketServer__ctor_m95EB71747A91626A796383551B55D491BC4E06A7,
	WebSocketServer_get_Address_mD48CCAD7685114CE68C8FE4CDD2906B6D569330A,
	WebSocketServer_get_AuthenticationSchemes_m10C0F0A8AFA89540338A49907BA43C42FBAE2220,
	WebSocketServer_set_AuthenticationSchemes_mBA40F1856975767D01B03AA8373FA7F816C9945D,
	WebSocketServer_get_IsListening_m1B94CA092B6151AC62F86DEB460C93063A6C2E0E,
	WebSocketServer_get_IsSecure_m9E6F5A71B04AA05AD129B89442C5D8F67CE4CF58,
	WebSocketServer_get_KeepClean_mFD6F732AD1D3692D1DBEB919F030E37C084BA8FD,
	WebSocketServer_set_KeepClean_m1BC48920A4FA76EFEDA01DC606EE075471525029,
	WebSocketServer_get_Log_mF1FE98412ED085FFAB59E9CDBA867622182C6A62,
	WebSocketServer_get_Port_m7B932D59EFF54AC92F4F7AE56F03B4F679C8E667,
	WebSocketServer_get_Realm_m511AC86786D84DAFFF98A7D908F32BE385671DB0,
	WebSocketServer_set_Realm_mBFDF3FD3CA1B4C183709938E3220F4FBCCF09598,
	WebSocketServer_get_ReuseAddress_m3D6C2C83BD9F34225662309DA2357970A08A48E6,
	WebSocketServer_set_ReuseAddress_m05AD4870B04E824926F6E7778F27536C959EED3F,
	WebSocketServer_get_SslConfiguration_m8E3D94D3EC0CD107B6C9146B230D82EC7BB270A9,
	WebSocketServer_set_SslConfiguration_m16CED19833B58FE4D8EBDBFD30DE8948967A1575,
	WebSocketServer_get_UserCredentialsFinder_mCF29E4D3B4BF27E6E6CE5BBB3D36EC5F436EC02F,
	WebSocketServer_set_UserCredentialsFinder_mFF89368995A1E13CBFE54F34B7985CEFC6576937,
	WebSocketServer_get_WaitTime_m1EE6F8045FF57FE4C0AAA93FD59B2AD5666DE3A9,
	WebSocketServer_set_WaitTime_m150D71A7AA8B4B01B3CCB897F8FEAE81EF6CFB92,
	WebSocketServer_get_WebSocketServices_m1CFA935B60F4C7D881699CAF4C62A287652D4F9A,
	WebSocketServer_abort_mB6645B62DF787AABA98444DA99A0FA0FA60C7997,
	WebSocketServer_checkIfCertificateExists_m03F573435B7B9FCD69267ECE2C82EA8002D29A4A,
	WebSocketServer_getRealm_mC98FD0370B937B7C7B07E7AE3C1AC4CB5386D51A,
	WebSocketServer_init_m1122C9D953F43BF2A78CD321165A3E73B839CE98,
	WebSocketServer_processRequest_m4E5E807CB9C78064903BBB3A0065BAE81E1F9EBD,
	WebSocketServer_receiveRequest_mA0DA0A6FA24F454215A923B0DE6C9ABC7DEF6970,
	WebSocketServer_startReceiving_m8D00A595C395160E85AAA689865DF5D494101639,
	WebSocketServer_stopReceiving_m196D234F665B7D0E7FC13A3E737AF22F08BB1327,
	WebSocketServer_tryCreateUri_m9ACB258906C0FA984027E811A58AEBD205F708CC,
	NULL,
	NULL,
	WebSocketServer_RemoveWebSocketService_m025ADC8F965745C3E2DE2B0F8A24E74B198C6F38,
	WebSocketServer_Start_m4166A74AEB499EC4936AC02A4BC3B0F6F74A394E,
	WebSocketServer_Stop_mBEAD1110D6BFCFA962387A255E43AC7755E357DD,
	WebSocketServer_Stop_mBCD6AC7CE488F32636FBF2546C1D21E1E0C9B279,
	WebSocketServer_Stop_mF3D73B556324C40603F4B47E0297A4D47CACAC39,
	U3CU3Ec__DisplayClass62_0__ctor_m693AF89D78F7D599A78EC51A996145904F91A44B,
	U3CU3Ec__DisplayClass62_0_U3CreceiveRequestU3Eb__0_m9ECAF23C2B920ED73D051B796FEF6FC07C7AECE8,
	NULL,
	NULL,
	NULL,
	WebSocketServiceHost__ctor_mD116561F533248F8F7B4B9E1305D41DBCF1CDFE9,
	WebSocketServiceHost_get_State_m2A2F4166E8246F7B9D78738EED701E68C0DF5E70,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WebSocketServiceHost_Start_mC5CCDF205BD529D2229354D711331C8C754DCEC9,
	WebSocketServiceHost_StartSession_m52F57EB4D8ACF4D9B98829048D918788864AAC08,
	WebSocketServiceHost_Stop_m3F1CACDC7861191EEAD602A606BC3FC4AECB68A1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WebSocketServiceManager__ctor_m09F432932F99C4C442AA774656F873A99B6FD08B,
	WebSocketServiceManager__ctor_mC95391F343A22D22B57341D1EE19497F4413885F,
	WebSocketServiceManager_get_Count_m6F5B479E90D06B89806FF76A7A770601B57D8A35,
	WebSocketServiceManager_get_Hosts_m9F5A21549711201CE552AC5F12268BADB682938B,
	WebSocketServiceManager_get_Item_mA7149399742C2F1106DAFF68B804A9E4B4BC2003,
	WebSocketServiceManager_get_KeepClean_m93E8C12AFEBB254A00BFE0D312B3D25A9E655EC5,
	WebSocketServiceManager_set_KeepClean_m2412638ED5B723D2DEED01C5C5388FD97F4A462C,
	WebSocketServiceManager_get_Paths_mE3D8418DF9C1DA598EB6A7867373A6EC7E68F019,
	WebSocketServiceManager_get_SessionCount_m520286E2C0F82330C8DBCC4935D38F3BF52264AF,
	WebSocketServiceManager_get_WaitTime_m569AC21587EAA1D71ECD933DA896BFE56824BBEB,
	WebSocketServiceManager_set_WaitTime_m2908164E9F33D135FB2CE980DCF687068E15ED29,
	WebSocketServiceManager_broadcast_m99E69DD7D8825528FF9A99CB1A8958B5B9AA37C1,
	WebSocketServiceManager_broadcast_mA690614B15595047D64B8FEAF574664B468232DF,
	WebSocketServiceManager_broadcastAsync_m743C98B66C0F6D40CD3398CF1843DB161B769741,
	WebSocketServiceManager_broadcastAsync_m6E9253CE1B06050FBCD5D94D026D7135EFB2F1D3,
	WebSocketServiceManager_broadping_m86E74256212C28DCC271A3AF0B836013A3659153,
	NULL,
	WebSocketServiceManager_InternalTryGetServiceHost_m3C017BD2FE5A536AAB1D0E47A24A58C876B4DA50,
	WebSocketServiceManager_Remove_m3A90AF612AC710838FA4BC2C10FEC0A39C7D7DFD,
	WebSocketServiceManager_Start_m47638B5FEC7A09AAEF6F7EF1E668FC6D4A8656C0,
	WebSocketServiceManager_Stop_mB3FAEDE44385078FFF435E86D52D30AD9A79BF4A,
	WebSocketServiceManager_Broadcast_m8A13771105D003BC2AB1DC011665F2C0B11D3F45,
	WebSocketServiceManager_Broadcast_m3A9E0DB049F342BF7FDD7FE1B88C24C395D45293,
	WebSocketServiceManager_BroadcastAsync_mC5BEE11884B1DB0A3B56B57E92F1EB7FAE5C52E8,
	WebSocketServiceManager_BroadcastAsync_m93428AF99D48D232DA5152B658DC5A9BC18E1A56,
	WebSocketServiceManager_BroadcastAsync_m05C6C128479892A201C9AF50A7B7C725FCF1F6DF,
	WebSocketServiceManager_Broadping_m7B9C6760A1F3DA1AC76491F96741F3ECBEE5F000,
	WebSocketServiceManager_Broadping_m3BA617CA437C4A440D374E9E3053F2BFFD2EBECE,
	WebSocketServiceManager_TryGetServiceHost_m87CD93EB962B01F5CCFB629EAFA80CDF9427D84F,
	U3CU3Ec__DisplayClass26_0__ctor_m18B85F1BC450905DACD20CA087188ED47E6A76C5,
	U3CU3Ec__DisplayClass26_0_U3CbroadcastAsyncU3Eb__0_m9C3E1FF8BB55F081B3A4D5C511F343677A80086E,
	U3CU3Ec__DisplayClass27_0__ctor_mD141A9C2AC564F39314BBF5966BDE2881F947CF2,
	U3CU3Ec__DisplayClass27_0_U3CbroadcastAsyncU3Eb__0_m913A1C316BB927CD1B1B8E87D96CB51930FC7F9F,
	U3CU3Ec__DisplayClass38_0__ctor_mD30A536C8E63D2C8EF8EED0AC615CD03168A053E,
	U3CU3Ec__DisplayClass38_0_U3CBroadcastAsyncU3Eb__0_mA0C8BABA31245B64EFA1A7E9E3941B4255155DBF,
	U3CU3Ec__DisplayClass38_0_U3CBroadcastAsyncU3Eb__1_mC6AA706384A1C9F1A3133697D0438291C0B27C91,
	WebSocketSessionManager__ctor_m25768072902FC30847D656939AE41D5CBADCA0AC,
	WebSocketSessionManager__ctor_m32EA57F8957E7660203F448446DEE3DCF7624C3D,
	WebSocketSessionManager_get_State_m0ED89683BB2A3AEA12F12AD7A3D37EE38627D688,
	WebSocketSessionManager_get_ActiveIDs_m1F5990BD33FF4364A49341CCCEBEFD9F53AD37C8,
	WebSocketSessionManager_get_Count_m21319BABAE8AA310EA0329ADBA12A2590803BDFF,
	WebSocketSessionManager_get_IDs_mF4D23E72A231C5AF92B23745D4F5915089429646,
	WebSocketSessionManager_get_InactiveIDs_m1FDB4A898C1CFBB9ABA3190661386F0DC09CDD65,
	WebSocketSessionManager_get_Item_mD42A05520E9C67A2C9C1B7F3E7D1A045D1CE6459,
	WebSocketSessionManager_get_KeepClean_m053333A96F311042CAE8531E5F39CD6CD79282CC,
	WebSocketSessionManager_set_KeepClean_m2C8F6B62229C2D917D1F257BF6058C49E93FACB4,
	WebSocketSessionManager_get_Sessions_mCD7C1ADBF162416653A781CF1C198EF27479C0AA,
	WebSocketSessionManager_get_WaitTime_mB50DE7AC810937F307D508E0AF7C3C591349BFEB,
	WebSocketSessionManager_set_WaitTime_m5195D503FA4324CA708DB05BB69020EB5C830F5E,
	WebSocketSessionManager_broadcast_m7F671FA9856609A4B7381571F138756DA1C324A9,
	WebSocketSessionManager_broadcast_m681636F712603C575E09667B5B156F8D4EAA08E5,
	WebSocketSessionManager_broadcastAsync_m471A1A5BE638CF6936D5548D53B34D0EE15F8620,
	WebSocketSessionManager_broadcastAsync_m2735643DBB2FB059F46FB04E323EC985EE61A093,
	WebSocketSessionManager_createID_mDAF9E8FC14219F1A54786D002A76449E289F43FF,
	WebSocketSessionManager_setSweepTimer_m9DE40755D5EA9B91D146314222D2CD7790758CDC,
	WebSocketSessionManager_tryGetSession_m6DCB52B7032EBD4112370651F85D37C2CECC687B,
	WebSocketSessionManager_Add_m6B3923C8F7C4B3C6800472521EA45225AD3EEE1D,
	WebSocketSessionManager_Broadcast_mD8D91F66484D595FA395C30B6C9A9D381859D192,
	WebSocketSessionManager_Broadcast_m3A96C906D013AEF90661F84CCC5A573AC061D883,
	WebSocketSessionManager_Broadping_m991AC37F0D609BB46C1E86747D4D9EF1A8A348F2,
	WebSocketSessionManager_Remove_mE7BB3D33ACE2DC677ADD7C5DFE7EA73FFF94DEDA,
	WebSocketSessionManager_Start_mCF432CB3DB5A958027D14A4F9119656ED14A0B0E,
	WebSocketSessionManager_Stop_mB5E1F1E059202093CCEB948819342577A48D4B19,
	WebSocketSessionManager_Broadcast_m400B4014269C101873CF4E848BDCA3A9989C088B,
	WebSocketSessionManager_Broadcast_m40452D4BD59939863152DD2998D040C0497CA70A,
	WebSocketSessionManager_BroadcastAsync_mB56D32EC3D7E905E8734B52B4DFC855A36219A35,
	WebSocketSessionManager_BroadcastAsync_m5BDA76C32B355900B2F2AD7A8CB7417F68BE902B,
	WebSocketSessionManager_BroadcastAsync_mFF6FFB8A5158036E35A168F4E06145724A1D9287,
	WebSocketSessionManager_Broadping_m0485E20FDA7FFBD78325768D302CAB52F8E1C119,
	WebSocketSessionManager_Broadping_mC6CD0A064BA771A54D940A02FC6B672C733BA72C,
	WebSocketSessionManager_CloseSession_m7D6F1210508F2248C4C4519D39F4607E8E33F4AE,
	WebSocketSessionManager_CloseSession_m4FE99ED53C796A628594CF4BDF3B3F8853E35FEE,
	WebSocketSessionManager_CloseSession_m41C39F768CEC8A58EB1E77B89CC79BF9A9EF4ECD,
	WebSocketSessionManager_PingTo_mF5BCEB41BD64DBB52B2F6A7FE1F516D53E526845,
	WebSocketSessionManager_PingTo_mD46C8FA8E99DFC66B2CF874FD79C7AD90343D063,
	WebSocketSessionManager_SendTo_mA0828B37A86153F5B0F205CD6EE8E943B6ACC1F8,
	WebSocketSessionManager_SendTo_mF898F94C7C0685CDC47271DF263FABC9B6DA68B6,
	WebSocketSessionManager_SendToAsync_mDE62CA5269E313F07EDB9EEEE826EB3F022A55ED,
	WebSocketSessionManager_SendToAsync_m1B866F38590B70BF1BB6AF65BC1DAC56FF62AF92,
	WebSocketSessionManager_SendToAsync_m266D7E691657543CF6DE6220B395A8A3D1E30F97,
	WebSocketSessionManager_Sweep_mED81F3B6998A8227A11A83AA3791A5D7900E8844,
	WebSocketSessionManager_TryGetSession_m0B32975452F2DC998E31627B1FFA15FDCBDBA37F,
	WebSocketSessionManager_U3CsetSweepTimerU3Eb__36_0_m36E23225D660A9F5675CB286D11459767626CE3B,
	U3Cget_ActiveIDsU3Ed__14__ctor_m71A1D26CC23B9A74B72852F8C243B50E88D4EA9A,
	U3Cget_ActiveIDsU3Ed__14_System_IDisposable_Dispose_mE2DD0639D42CD88B106730AF528C3FFCD30B88DD,
	U3Cget_ActiveIDsU3Ed__14_MoveNext_mAED55DC5C264F65E3D81536F0961E88577721595,
	U3Cget_ActiveIDsU3Ed__14_U3CU3Em__Finally1_mBA460B49A0A5CC9645DA4B07242FE5D3E0877EAF,
	U3Cget_ActiveIDsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mDCF0F98E67F62D0A9A107C2682CBAE3F478EE410,
	U3Cget_ActiveIDsU3Ed__14_System_Collections_IEnumerator_Reset_m71DE20ACE7C248A0AF728D65E7930CA8EB703EBE,
	U3Cget_ActiveIDsU3Ed__14_System_Collections_IEnumerator_get_Current_mB84D4061A1B43CAD9203C40A5FBD96655BF7CF31,
	U3Cget_ActiveIDsU3Ed__14_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mB57543128E608ABFEA2CBF32CB45B17D917A8421,
	U3Cget_ActiveIDsU3Ed__14_System_Collections_IEnumerable_GetEnumerator_m566759125BAE1DEA020747B5D031659268B06E2F,
	U3Cget_InactiveIDsU3Ed__20__ctor_mBF11FB11062B60B1D8183E1830DDF36F6AF12966,
	U3Cget_InactiveIDsU3Ed__20_System_IDisposable_Dispose_mFED8DB88C4FE58CFE1E4656216CE48504A210C46,
	U3Cget_InactiveIDsU3Ed__20_MoveNext_mFC308BF77E1F9236DB135B9C8FA4E707F797711B,
	U3Cget_InactiveIDsU3Ed__20_U3CU3Em__Finally1_m6655D3A9B0FDB0FC53FED7F19367E0B359BF70D5,
	U3Cget_InactiveIDsU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mCD617A347D13DAC605EA9AB9D1EDD5CD0EA62737,
	U3Cget_InactiveIDsU3Ed__20_System_Collections_IEnumerator_Reset_m5568B7F0AD45A3F8BC4DFADA53F89E8883EB7DC4,
	U3Cget_InactiveIDsU3Ed__20_System_Collections_IEnumerator_get_Current_m777CE54F9CCC0360D73965C468034786F90E98FA,
	U3Cget_InactiveIDsU3Ed__20_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m7F650158FCE88C529E8AE22DDA34659E85B7768C,
	U3Cget_InactiveIDsU3Ed__20_System_Collections_IEnumerable_GetEnumerator_m83600AC75155D76E33FF2D0A1719D4F47512A563,
	U3CU3Ec__DisplayClass33_0__ctor_m54840A31A193E591125ABF917E09F9CE87C963F5,
	U3CU3Ec__DisplayClass33_0_U3CbroadcastAsyncU3Eb__0_m6BB47473743B37FEA0B725680E8BB03E10B55299,
	U3CU3Ec__DisplayClass34_0__ctor_m41EA5222E9446B595CB48FDBBA756B1A3FF722CD,
	U3CU3Ec__DisplayClass34_0_U3CbroadcastAsyncU3Eb__0_mECEB1D124FE4FDF4F5F965DD222B2711137D8B95,
	U3CU3Ec__DisplayClass49_0__ctor_mEF5C8C0CC5FDA327BD1448BFB3250E16F410EBDF,
	U3CU3Ec__DisplayClass49_0_U3CBroadcastAsyncU3Eb__0_m12DF611FB9D093F75F4E4448E2640872F2A2129A,
	U3CU3Ec__DisplayClass49_0_U3CBroadcastAsyncU3Eb__1_m26707B8214BF59609B776413331B25C3A326426E,
	AuthenticationBase__ctor_mCC17DCF2330D0A07482C6959B0B5CAB149E2B54D,
	AuthenticationBase_get_Algorithm_mC6876C655FBB774C61D393D94BA1A169EE19D4E9,
	AuthenticationBase_get_Nonce_mF7FF68B003DF16E25E5C77A188FB455FD3C3F498,
	AuthenticationBase_get_Opaque_mC8AE16C9A9EB66EE4CD7F3228E79C1786B2B22C1,
	AuthenticationBase_get_Qop_m3F5B34297DDAAA98F8E3043BC5FCB662B529BCFF,
	AuthenticationBase_get_Realm_mD7829F7E6DC2C4DFF645CB521AB90D3734A1FEE5,
	AuthenticationBase_get_Scheme_m1689D3F10B62EBA0BAE138C1C1188DEFE114E782,
	AuthenticationBase_CreateNonceValue_m4BC9D67561C4180035764A5CBE8F3D19C54465DF,
	AuthenticationBase_ParseParameters_m79398071F670AD3CA383FF817C9BA2CD5E620BA5,
	NULL,
	NULL,
	AuthenticationBase_ToString_m0958A662EFBEC1EFFA1822FE2FCD546BFD69E502,
	AuthenticationChallenge__ctor_m7C1DC26E8D61978A9E6E636406DE698F236D0CB1,
	AuthenticationChallenge__ctor_m564A64DE7723B251A44CD2D711093FFC7337BA79,
	AuthenticationChallenge_get_Domain_mF3577716CD18FF5367EAEC53B84A58A4B61A066A,
	AuthenticationChallenge_get_Stale_m39B62C3738FFAC4E8B96FC25CD543F8EE56482F5,
	AuthenticationChallenge_CreateBasicChallenge_mE266121701AA39B52DF9FA608929148F49993ED2,
	AuthenticationChallenge_CreateDigestChallenge_mD6EA07881965C6EDA6744B25C861F32B33882712,
	AuthenticationChallenge_Parse_mD71FF96E69FE4EBEADBECDB58663273D62F28C04,
	AuthenticationChallenge_ToBasicString_m557BC02F8000F216197E8A552BE1864CB948E58F,
	AuthenticationChallenge_ToDigestString_m9EE9ECB4CDAEC4A40D3888D6A8C76ABEED38E514,
	AuthenticationResponse__ctor_m2AC63DBCEC3D747348AC25D029A7A8A538175A8D,
	AuthenticationResponse__ctor_m501C0D5DC897D230EDE4396AA2975527CC015C86,
	AuthenticationResponse__ctor_mACB2B78FFC1DB43ABD7C8521B0E91233717DF795,
	AuthenticationResponse__ctor_m1C7B470FD21D62980ECCA906FB380248C815E86E,
	AuthenticationResponse_get_NonceCount_mCC8C6768527C73648C29B205409CD358DB4ECF78,
	AuthenticationResponse_get_Cnonce_mF03A011E3D7572E5EFE20684992A7B17A1606CE5,
	AuthenticationResponse_get_Nc_mD28446249D6D0EBC989AD31C6A15F100D2D9BCC8,
	AuthenticationResponse_get_Password_m19676AA2C70F57FEF8082DC4D8878D959F6076C1,
	AuthenticationResponse_get_Response_mB3907DD6F7C1BE8CFFA09E8900CFC3DDB6EA0BDB,
	AuthenticationResponse_get_Uri_mC8A054282B7C9276D561277CBC3F6970D1D6B25A,
	AuthenticationResponse_get_UserName_mA7AE765B25EA04C67784A830498C78F8F03ECFE4,
	AuthenticationResponse_createA1_mAF222A0FF8126876F6CEEC0C9EC6CA25EB111815,
	AuthenticationResponse_createA1_m60A8831FAB8EDC6EB8C4478EA9E539F414FEBA3D,
	AuthenticationResponse_createA2_m1398FC52F3A58552A1ABF51D22708F5FAF350A3D,
	AuthenticationResponse_createA2_mBC9AD136B11EC200901BA25F73597E3CE7BFCCE8,
	AuthenticationResponse_hash_m4CA59ECFF76BD3CE112060D503A1193361FCA0AB,
	AuthenticationResponse_initAsDigest_m20782FE5DCE735D38C5B0670A8A5A49850FE102E,
	AuthenticationResponse_CreateRequestDigest_mDD1E74E90D9D84D39D3D1025E5F5F610D185C4E8,
	AuthenticationResponse_Parse_mF8C0525BC1ADCF128EA6475AC857078A8EB10747,
	AuthenticationResponse_ParseBasicCredentials_m59A623E08482745A5A31B16F3F874C86314551AF,
	AuthenticationResponse_ToBasicString_m58519C18C73E0F0E75350E8BF395FB28FA5798CA,
	AuthenticationResponse_ToDigestString_m4F6A53EB9A440DDA02CC17408989D8276B5AC9AC,
	AuthenticationResponse_ToIdentity_m25C1885E53C91AC6025105F431EF0AF08B3F517F,
	U3CU3Ec__cctor_mCF27718999A80EF862E4204DF166E44EF8FE89DE,
	U3CU3Ec__ctor_m5A149873B490D222B5F0A03301C191B7E0540948,
	U3CU3Ec_U3CinitAsDigestU3Eb__24_0_m1515862841DFEF5E20DD628F148C14C157B2BE2B,
	Chunk__ctor_m35FA5CB3798FADA79257B58B52A6C5A4A12D4CC1,
	Chunk_get_ReadLeft_m2C267307045C0ADB83C2FDD6AAD639DE6B8A44D6,
	Chunk_Read_m9C93FE5623A3C89EBC16C4DEE83BDC7C349AE387,
	ChunkedRequestStream__ctor_mE6417B53C4E36CDFDC4CB1EA6A5C24E1FEF26FCD,
	ChunkedRequestStream_get_Decoder_mAA16D7CDB260D3BC79FDCBD341A6C596F073A459,
	ChunkedRequestStream_set_Decoder_m411B29C7AEF3524E480788AFF9A378384CAC3232,
	ChunkedRequestStream_onRead_mB5D6EB7178A5E77259B5CDC239FE370D2A001F08,
	ChunkedRequestStream_BeginRead_m141FF45E6EB09B4A6F6768D165764340F367DD51,
	ChunkedRequestStream_Close_m11F7D14EE4BBCDF74FEE0B95713817AE5D0ACB5C,
	ChunkedRequestStream_EndRead_m0B9E1ABCEE3145FFC13072AA109A2D11659667C9,
	ChunkedRequestStream_Read_mD305267D55969EF68AF29F354C0F374C01A81FF4,
	ChunkStream__ctor_m03E7DDB3013B3A9B0B1ADD7A809A763D5357BD21,
	ChunkStream__ctor_m5C508A4E45A2D4C82A78AEE87949118FB178444C,
	ChunkStream_get_Headers_mA71B2DF40AD18045FC19F11468F7A1D6790FE646,
	ChunkStream_get_ChunkLeft_m2DA695C8961ABEBC648BB79F7BEFB1A434F8B584,
	ChunkStream_get_WantMore_mA4CE15B3605E88562976B86760DDADEAE434B4DF,
	ChunkStream_read_mBE104F4E81474AE5483D35733BAF77DB83FDBDDE,
	ChunkStream_removeChunkExtension_mA8897A2F15DA590D249D84DB782A3F33A1694AAD,
	ChunkStream_seekCrLf_m8EF4A84701657FD6696FE7094B953277069785F6,
	ChunkStream_setChunkSize_mA7AC39ADF9EB9C6274BF359907E3B7236A835EFC,
	ChunkStream_setTrailer_m706ACF6F3CCE96D7DF4BE1040114E9C4A9E11DEE,
	ChunkStream_throwProtocolViolation_m4A3CED4BE98FDBF32BB434F8055993AA55D192DD,
	ChunkStream_write_mB02D418DFD86CA164D999C5B3DF338303310EF0C,
	ChunkStream_writeData_m0D7D11CE44AC61F11B488EAA13217217BDFFB0F6,
	ChunkStream_ResetBuffer_m872822F34770962F506A86C60ED7A82D3C93BD11,
	ChunkStream_WriteAndReadBack_m9C398A99686597D051430229062F7C05981AD88E,
	ChunkStream_Read_mD62D112650E2A44A983446E2568FD193F99C99B0,
	ChunkStream_Write_mC05F797E6726BABBF283006D246BA4A3A10D2C39,
	ClientSslConfiguration__ctor_m0DF6D3AC193EA0CF8012D62CF8B63C3827F9EFD1,
	ClientSslConfiguration__ctor_mD020D7F29B628FCCD6EC8EDAD0C6DCD555D34DDA,
	ClientSslConfiguration_get_ClientCertificates_m71DD6B7FA341AF7CB50E872774BE1173407150C6,
	ClientSslConfiguration_set_ClientCertificates_m1753A54F9D70581B422DB50D588D2626019DCC98,
	ClientSslConfiguration_get_ClientCertificateSelectionCallback_m9896F1C9540AD42CC546FE25D4AC32EB3842C4F8,
	ClientSslConfiguration_set_ClientCertificateSelectionCallback_m16B455B03377ED342D31131B47C91037E7939129,
	ClientSslConfiguration_get_ServerCertificateValidationCallback_m4178571D83653E57C436F81EDC79D51BC003EF35,
	ClientSslConfiguration_set_ServerCertificateValidationCallback_m529973888D6BF43CC449D92A931B8F1AB5983DDC,
	ClientSslConfiguration_get_TargetHost_m0B2B6381849B532E145F2313CF0B76BCB2F93447,
	ClientSslConfiguration_set_TargetHost_mA1077E861D29F26D2F77E694B917A9518EB9BE64,
	Cookie__cctor_m4F91054F0A01EFA60B05372A0A7A47C8C96C4921,
	Cookie__ctor_m53ADB863562A530F6A84258F7F4F877F2C14FF36,
	Cookie__ctor_m0DFAE89D7F1A7B1093556AB2C7A8564ED7DB625F,
	Cookie__ctor_m40E586F5352605E220E4E056A2F8CC8C407CBFB9,
	Cookie__ctor_m33B6B507421A6DBB25769E15B3C29EFED9EB678B,
	Cookie_get_ExactDomain_mA4F885A2C0629E82311646A09C51CC963276C437,
	Cookie_set_ExactDomain_mAE3A5C261C6A716EA70FA5DD6C84AEECF8092141,
	Cookie_get_MaxAge_m329D54507884FD8A7EE855AB2ED6FA867A819B89,
	Cookie_get_Ports_m49697E38253F1F3ABBC6AAA626CFE22601BA6207,
	Cookie_get_Comment_m7902891140C2DD0270E1D7E855C1FEDD4592E30E,
	Cookie_set_Comment_mD30377A67C3476C947418FBFC579A0C62DD73E8B,
	Cookie_get_CommentUri_m2B7A3B6AF9E30CA372DA7D02F75A1EE2DAD7C19F,
	Cookie_set_CommentUri_mB2BDEAFFFC642D4FD801F768C494C21AE47B3408,
	Cookie_get_Discard_m9A9C0FCE9A631CCF3584C80B0B46E0E50DD6C572,
	Cookie_set_Discard_mF288E7F150DB28E8481CF4FA414BC84C8A992E3B,
	Cookie_get_Domain_mF31006B082D041079E94388A896D1688BA4C8DAE,
	Cookie_set_Domain_mF83D794807D2DF2BD393E46F2307A47CC636E4EC,
	Cookie_get_Expired_mBEA6B34053183D841EC0ED8C22695F73E847DA7F,
	Cookie_set_Expired_m267F7D9C458207E3BFE8DB7E3D894E596AA9FFF5,
	Cookie_get_Expires_m3A3F6A4C9FA2E691C9DAFCD74A4FA8AFD81DA188,
	Cookie_set_Expires_m319B977C71E51F829476593D1D3A8FDF0164B3D4,
	Cookie_get_HttpOnly_m732D27AFCF2C823565DB232F0634004A1418174F,
	Cookie_set_HttpOnly_mD780A033DF95092D6EA798F8B8615B25E1419B39,
	Cookie_get_Name_mB43D79B55813FEC7888511E224CA9418A0C3E91F,
	Cookie_set_Name_mF6582E5F1199D35B1FC8811133EBB7B1C3C892BD,
	Cookie_get_Path_m756D99D6F41FA053C2D3CEAA2A3B32B99D241CF2,
	Cookie_set_Path_m5CE1F08D38DBD8A42820F20C986E8328C94E1EB4,
	Cookie_get_Port_mA8D05E5A09642C0AC56FA2ED74EE0D61FC7D9DD4,
	Cookie_set_Port_mA49EEA0CEB0E5C4ECD8DF5D7DA3F1D14B8E1B63E,
	Cookie_get_Secure_m0524EEBBA3FD8ED25FBC560CC9220649211D45F4,
	Cookie_set_Secure_mA858981CC5FE105932D6FCFECEF7977868B666C6,
	Cookie_get_TimeStamp_m6766D0222C55690923C3730B277CD8D5DAA5E1F8,
	Cookie_get_Value_m9AC44F88B1FABDE5B96269DFD99AEECC9D7283AD,
	Cookie_set_Value_m817C5C9B6138CB096EF3A8557A7C576F12C48D09,
	Cookie_get_Version_mFA4DCD7A61B6F69FF503221F6F3546F7D302A227,
	Cookie_set_Version_mBBAE5EDB9EF3D2BB8F8A404678CC514997AFCF8D,
	Cookie_canSetName_mF13051F50831B0E42DD1F21C03B42A1089FE2DA9,
	Cookie_canSetValue_m14150EC3F22B886CF2E174C7B676F626F8DCC82A,
	Cookie_hash_mBA8EF104BE7E1B7BC93C3BF5B48333D8E9C4E688,
	Cookie_toResponseStringVersion0_m48EA8C7B5FD4869C26A6C1CB9239FCCEEFE614E6,
	Cookie_toResponseStringVersion1_m777890D5AC07791822B4CBD9FBD018F6ADC35534,
	Cookie_tryCreatePorts_m5113CF0988929389158E5D1CB8CDA3FBC391E5A0,
	Cookie_ToRequestString_m388BA2E520C312D46118FABF62F439A455B5C0ED,
	Cookie_ToResponseString_m824C988C4F4EAA7636749AA6E723BC8F40D4C733,
	Cookie_Equals_mB22B6B4C0C13FE3AB01846FE98E095D32B7D7BE2,
	Cookie_GetHashCode_m7B2995EA1A3FEEC428B28E5A7FAB65DAC687262E,
	Cookie_ToString_m7CA62A33F15252D8A44D3ECE7A1BABA3408704DA,
	CookieCollection__ctor_m14A8C1360F2CE04E4BE5CBEF9AF2336586100BB6,
	CookieCollection_get_List_m785F26EF8F90881453923AE4BBBA025E20AB9095,
	CookieCollection_get_Sorted_m2E360A96D56EFBAA642C5CA7806CB28242D4193C,
	CookieCollection_get_Count_m25F4295075CF5F29B7C31588506037809637D148,
	CookieCollection_get_IsReadOnly_m3BF8D280E436F43227A33A36E6C0B75CD5FCC644,
	CookieCollection_get_IsSynchronized_m6B08A651906E17BE2962D952916CF9A20E38FCB4,
	CookieCollection_get_Item_m3FB28B94FB8ADE10502FE4B25052A02E32023209,
	CookieCollection_get_Item_m4FC55C7137165506659F20D2071E61CE4FA1C7DC,
	CookieCollection_get_SyncRoot_m02157C22EE5F15273DDC8886863276F588131F44,
	CookieCollection_compareCookieWithinSort_m54CE61F46A6B299F07EAD6A7E0D3A19651EA7723,
	CookieCollection_compareCookieWithinSorted_mFE7616F7435C3E6E9E46FDA25E0AF06DD08F9695,
	CookieCollection_parseRequest_m2B2E4A0F22D9B91C1D9737CF2A57983D32BA9A09,
	CookieCollection_parseResponse_mAB3AE65D43C18B6D489ED605F79C404D5F850D3E,
	CookieCollection_searchCookie_m2DE7141B639697E13CA16B9443C39E5EECBF728A,
	CookieCollection_splitCookieHeaderValue_m0F177D416CFE4A6942D4CE50ED7D464D02CC0CD2,
	CookieCollection_Parse_m1DA38A2F8517E947075414A81EFA6CE979862EA1,
	CookieCollection_SetOrRemove_mCD57F106953634BC3DD4B0D24B680E1754955701,
	CookieCollection_SetOrRemove_m208060BAB8FD1D301BB8D2813C22D382232E129F,
	CookieCollection_Sort_m6BFA3943F67D709CE8E85F3E0A6CDD62CB98AA46,
	CookieCollection_Add_mC83C8B507817EA1F9362AB5C86B61C9D027E015F,
	CookieCollection_Add_m6F3B890A2148C8760BB6F943D304E76F49A86665,
	CookieCollection_CopyTo_m450870DA9111239A9C767A05A6EE3DF69D8A5D01,
	CookieCollection_CopyTo_m58B0FDCD4EE74CA863B6A3B61EF13F7A2A57D6C0,
	CookieCollection_GetEnumerator_m63D38C707641C1D36CFC55B53DF0BA2D1F9768AD,
	CookieException__ctor_mD5782CCF8E18D015592CB0F684A78FC450739578,
	CookieException__ctor_mF5EBA66B5D5C3AE99B3F48A72C98BEB35C0BBBD3,
	CookieException__ctor_mDCF9C1D8149AF9A7B93E3F4900DD979C89B14E55,
	CookieException__ctor_mACA6A3117DA4F3989526B87F82A40E17579AC747,
	CookieException_GetObjectData_mAE891B42C0A92043294B2D4DDE06FFB904FB6B85,
	CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m94A34D680E17B10C3EBC5ADBDE084DF611739C10,
	EndPointListener__cctor_mFF10D80D62E34DF360C83BD58023E3DA8F39695F,
	EndPointListener__ctor_mB4002CF8590264D2C7721CEEE597F818D09350B1,
	EndPointListener_get_Address_m4C5ED63522C1688DFA7D68188CA4E142522A98EC,
	EndPointListener_get_IsSecure_m66881F91519548FCD53A32A84D5B1A24CFE4E08E,
	EndPointListener_get_Port_m0C8605A6DE8D43DA3F020F80451F3257ACFB9C74,
	EndPointListener_get_SslConfiguration_mA91A2BCD28E09C058DFA9EFF091F3A71648B509B,
	EndPointListener_addSpecial_m31E283677BADCE5B0EFC5E7C5D16EDE4D3782049,
	EndPointListener_checkIfRemove_m067B9E20F53C085950137170892D0AE0892D25F6,
	EndPointListener_createRSAFromFile_m3A9F97D2C1E0C7616E9258BF69968CB576AE0376,
	EndPointListener_getCertificate_m3A153196FCC636E3648C23A57DB294EC19D40BBD,
	EndPointListener_matchFromList_mE2255D7952F41091BF33FD97C49425DC3235373B,
	EndPointListener_onAccept_m1D72BE5ADBD0BD295854CDA292FCB164763B4235,
	EndPointListener_processAccepted_mBD05790FC7B44012C541664FFDA21096E8045208,
	EndPointListener_removeSpecial_m7CB0C1308751269ED07137A5597745F667A0B336,
	EndPointListener_searchListener_mAEEF7BF037DE00AB46C8BB85B124F357DBDE8AB1,
	EndPointListener_CertificateExists_mBD01DA95AFCE3EB76509F2E7327FA08CB7E01ED8,
	EndPointListener_RemoveConnection_m587594B990A6F552DDF6B56AF3FAA40A2F21DD05,
	EndPointListener_AddPrefix_mDC204A1C39257F3C7A48E8E35DAC6B334B393ACA,
	EndPointListener_BindContext_mD188A24DCB3D8B1A29F024D5223D4211787ED5A0,
	EndPointListener_Close_m197C1311FAB53A1A9D765A0314E3DFCC12D526A2,
	EndPointListener_RemovePrefix_m42F8E9530ED340FE981EC9ACECAA2B8FCA925806,
	EndPointManager__cctor_mB7666DE72A44091F467B8B2AFED7664D1EF1B3D7,
	EndPointManager__ctor_mB6EE976DD846FB5341BC8C002A02EA5B2264BB6D,
	EndPointManager_addPrefix_m5A77FE5C5EB41A326F2F2B28DEFFC308924E1207,
	EndPointManager_convertToIPAddress_m7C6CD2AFA5B41017C60A774B0800D107D6169DBE,
	EndPointManager_getEndPointListener_mF71FB3757EF627EEDFFB3263C744C46219D5B5E6,
	EndPointManager_removePrefix_mC63A0660716687C373AD56C7952B2E86019BC22E,
	EndPointManager_RemoveEndPoint_m5CC4C9275D5CB0BFD722BE15C89766C0F9D1ED56,
	EndPointManager_AddListener_m5DC387AC014FCA8139BB49337D22098FC41B8CBA,
	EndPointManager_AddPrefix_mC06E704DAF29914A65567BC4E084FDBEBA900910,
	EndPointManager_RemoveListener_mED85FF1F83D96C95DD52E5F06A826E9FC3142FD7,
	EndPointManager_RemovePrefix_m8F03DA2525B7053384C66DFBB28EDFB1FB64BDBB,
	HttpBasicIdentity__ctor_m7AAE469F3A1FE3B1AAB8DA8C2A47441EA1FF2B13,
	HttpBasicIdentity_get_Password_m514E59976E658EBDC2E772AF6426AD245E29B1D3,
	HttpConnection__ctor_mB5029014F3B42BE1F9692098D022A4BF5A790238,
	HttpConnection_get_IsClosed_m08F01D2C593CC64FA421FF1BA4B45861841777A8,
	HttpConnection_get_IsSecure_mEB48A91A06CADC4230A708C53CFB2DF86F50EA94,
	HttpConnection_get_LocalEndPoint_m0A54225672308E472183517DC5CC25B4B70BF440,
	HttpConnection_get_Prefix_m740F270E5E26CBF22B2BB909F55DCC3F05F9F181,
	HttpConnection_set_Prefix_m774F9CA86A72880134E703008342562F0149E1F7,
	HttpConnection_get_RemoteEndPoint_m1BBB751BBB2A9B843A26B13D76BA77AD8700C6F9,
	HttpConnection_get_Reuses_mDF819A7D9B624F9A68194A42ECB1798A6692CC40,
	HttpConnection_get_Stream_mB40AC16C57E670FB48F42BF5CF17B715349780A3,
	HttpConnection_close_m64953DCEAF4DCE64A22DEEE9C5A3877D3BF3C5D9,
	HttpConnection_closeSocket_m42154AC9F4BBED88492B3B5C9CDC718000627158,
	HttpConnection_disposeRequestBuffer_m06F28BA3EC21997592439B67F4D45516E6C76027,
	HttpConnection_disposeStream_m197B427D1B89FF65246A813470D33DA614645AE2,
	HttpConnection_disposeTimer_mA668FDC3819E1BF8051AC92DDC3B82E2212858AE,
	HttpConnection_init_mC2117D11B8A0F01FC9E20641EFD9783AA6556F86,
	HttpConnection_onRead_m4A005CFA09074010F7D0240FDDF9D0876FBE91B9,
	HttpConnection_onTimeout_m53E17324F81068B84DC1A6F7512EB31919E5F085,
	HttpConnection_processInput_mFBA0BA04D0544CCFBE9BC2BED55D68726B854D75,
	HttpConnection_readLineFrom_m929E8CD5A0E782D745E37EFA6C8B5058467ADF15,
	HttpConnection_removeConnection_m38C881B7F1B84992C7490973A2C457256E205E23,
	HttpConnection_unregisterContext_mB3A9B63DFCE53142E66EFFC4EDAB9AD62CEB1F34,
	HttpConnection_Close_m1C921595B72AF843272D1340459DC534D3EE07DE,
	HttpConnection_BeginReadRequest_mC71301E5E92B9C6D6C8F8E865320F0C2C7928B93,
	HttpConnection_Close_m6BC15027F22928466E2F12EFB445EFC2F15C84AA,
	HttpConnection_GetRequestStream_m767C6A5919321A5B98C7608A4CED561DE8E0B2ED,
	HttpConnection_GetResponseStream_m62FAE850DCF408BE831746A6E144C997C1C67575,
	HttpConnection_SendError_mFF9C72E97CF88337A6225F611F12103F172210CD,
	HttpConnection_SendError_mDC1251521508CF9FB7B5B6098C28FB7DF7BB1BAC,
	HttpDigestIdentity__ctor_m4C2917B8F82352E687CE71C41C5FE658D26BCD48,
	HttpDigestIdentity_get_Algorithm_m49DD24F85E444DE18C9A4AC8E9EDB48FBD181F5B,
	HttpDigestIdentity_get_Cnonce_m93A952AECD10DDEA84A08A1CD44AA105E66F12D6,
	HttpDigestIdentity_get_Nc_mD6B4C06253728EAC3EA30ED9CBDC48EA4F4E5BAA,
	HttpDigestIdentity_get_Nonce_mF5491DC694C659083510779BB879273C40013CE0,
	HttpDigestIdentity_get_Opaque_m5C1907DD3BFFBD6E5FACA3A46237427967B8A7A2,
	HttpDigestIdentity_get_Qop_m51BCDBF56C73AAFCE8DC9E300A8772D58EDFC579,
	HttpDigestIdentity_get_Realm_m0B92C0A2C7D2BD021F00A6EAC97A15F46A13525E,
	HttpDigestIdentity_get_Response_mE75320F44CD2C79B18587E0A2CE36D4742C1CC32,
	HttpDigestIdentity_get_Uri_m61CD600009BA55001AAC66BF7E64028D33410235,
	HttpDigestIdentity_IsValid_m27105B2D570A9403472AE1276494B0CA89CB2414,
	HttpHeaderInfo__ctor_m0BE40726E48CD3EAB4EBA6B3ECC3EAFBEB2FC54D,
	HttpHeaderInfo_get_IsMultiValueInRequest_m55B6A7EABC5C9234BE8CB7918DE51691A829A15C,
	HttpHeaderInfo_get_IsMultiValueInResponse_m7CE771D0FAAFA7FAF615FE0EAE4762F0669568D4,
	HttpHeaderInfo_get_IsRequest_m5964E73CC17680C198AAAA3C2AEAE24CC0B466A5,
	HttpHeaderInfo_get_IsResponse_mC67A9009FA1C242600EEB37DD78403CAE0AC68ED,
	HttpHeaderInfo_get_Name_m733335DEA747ABAB9986F9D9F352788668AFAFF8,
	HttpHeaderInfo_get_Type_mBD7B549513E510EF3FE697F9C91DEDC608543C10,
	HttpHeaderInfo_IsMultiValue_m3125D6DB0559ACD005313F56ADEBA32F3917128E,
	HttpHeaderInfo_IsRestricted_m9F890F6948F74153ECC0E29EB8ED7327490CFC2F,
	HttpListener__cctor_m6DB7671E16189BBE4556C301A5E43575FF6C27C0,
	HttpListener__ctor_mFBFCF7EAA857293DABA606005E98DF971423179C,
	HttpListener_get_IsDisposed_m7EC67B89FBB08F53BB3BB1A8841EB4775E391227,
	HttpListener_get_ReuseAddress_m924451A5B555095A428BA650BD514D63F4C5FDBF,
	HttpListener_set_ReuseAddress_m3788F5F175B71462DEC3B6776D8A52FFE12E7599,
	HttpListener_get_AuthenticationSchemes_mAD0479A5FBE2FA5BE32FD2599773B680437846CF,
	HttpListener_set_AuthenticationSchemes_m260E324033728C270E9030FBE1B66AAB60894E18,
	HttpListener_get_AuthenticationSchemeSelector_m1A303DC011F89C3B3B2F0C44B263F19B37775D93,
	HttpListener_set_AuthenticationSchemeSelector_mDA83556F975E557BEA658D3E017CEAEB80A0C5A5,
	HttpListener_get_CertificateFolderPath_m3696DE6EC23C1FE48D2E6494EF268EAF019CB2B6,
	HttpListener_set_CertificateFolderPath_mB2354315751DE129FF09883F093E58096F77967D,
	HttpListener_get_IgnoreWriteExceptions_mB728A9C5A445CD6C524D9FBF9FA23BDAEF7111B0,
	HttpListener_set_IgnoreWriteExceptions_mE93F590774CAA5B18B709D1C88D9B5D2463F87DF,
	HttpListener_get_IsListening_mC18B065D0BADCCE9CBC8601235C75DCBBE246C4B,
	HttpListener_get_IsSupported_m3CFE3F345395246E7CB1FB482B99E084978E4CE8,
	HttpListener_get_Log_m6EACCFC086164C7E34694B01E7F927F8C5B49899,
	HttpListener_get_Prefixes_mC9B0A2B1230301CD0CD02F4D269AD63980795ACB,
	HttpListener_get_Realm_m89D2668FD8AF8CEEFE1BF73209F03733ADC1AE95,
	HttpListener_set_Realm_mD93E32F8229D5BB946ACBF910056A4C9779E5482,
	HttpListener_get_SslConfiguration_mEC965E5F17F7C89E405CA805639973637E83FFCC,
	HttpListener_set_SslConfiguration_m0488010781BD9D20602710B41DC9E5BD512318DD,
	HttpListener_get_UnsafeConnectionNtlmAuthentication_mB3F63E879CAF4EBBDC0DC902104D47F470DA6BA8,
	HttpListener_set_UnsafeConnectionNtlmAuthentication_mA922230EA1DED62D9576E4ABB3E7CD2E152EA0A3,
	HttpListener_get_UserCredentialsFinder_m6A0C507B82FF390A97E890B0E0A842FEA10A0262,
	HttpListener_set_UserCredentialsFinder_mBA4A9695EE741095A10087BB485B2CD15C1BFB84,
	HttpListener_cleanupConnections_m255FB326473ECF746440F2767897807DA768BF4A,
	HttpListener_cleanupContextQueue_mF56CA42A3FEBB881E0674051EAF0A92FEE2ED4C6,
	HttpListener_cleanupContextRegistry_mA570B19EF6E2651D6BCE0FCE41174976CBADC0DE,
	HttpListener_cleanupWaitQueue_m465C49E9DBF01F918BB93F1D4EC29F3FBC42D0FA,
	HttpListener_close_m2E280513239AC23077B613E4C2B52A404979167E,
	HttpListener_getAsyncResultFromQueue_m019DEAE6B5A6DD6D77134F8CA8F5F9F639CE0841,
	HttpListener_getContextFromQueue_mD26F93E62E37FB50E4CB9619F6B0ADE7C3CC4515,
	HttpListener_AddConnection_m461511299FBE4C94C4B494246349ECF0AD10B2B7,
	HttpListener_BeginGetContext_m0396F8C56C360BB09AD25C49DC29B19ACBBE8EEE,
	HttpListener_CheckDisposed_m8D76315D44940F15CEA99A19CCE6436853B4CC37,
	HttpListener_GetRealm_m411A0D5F68FBDBF2CDD0D9F82ADDBAF6A636D3F6,
	HttpListener_GetUserCredentialsFinder_mC6FE79818867A5105E13B0A842AF6ED74540222B,
	HttpListener_RegisterContext_m5FDF336978875E34D3FC9345AEBB3BB19B2D9833,
	HttpListener_RemoveConnection_mCCAC9A600A729258A50632CC0C100B85210A25D7,
	HttpListener_SelectAuthenticationScheme_mAF7099B38608BF22D0ECCB8B1F6284940801DD58,
	HttpListener_UnregisterContext_m04E9D562FDC557E2B44F2DE0549F5BDBD3D8FFB4,
	HttpListener_Abort_m80CF56EB184A9F212EA5F34F7F31CE169EAFDCB8,
	HttpListener_BeginGetContext_m70627EA6463075FD37B205DE58FD052ECC0A533B,
	HttpListener_Close_m3B869874BF045358AFDBC3B60DEA33A2A67967CA,
	HttpListener_EndGetContext_m775C727CA935F2DDACA2630BC896A7047221BDDF,
	HttpListener_GetContext_m1DF66E8D457A86B23DED4236FAC6697DFE2B6468,
	HttpListener_Start_m9F61EA1F92808C3611A1C00804F53DF833C67CB7,
	HttpListener_Stop_m81D3605DE53FC16804A95377ED9ACAD0C83C218F,
	HttpListener_System_IDisposable_Dispose_m877C69D6A2F9DA88A0AE5241AEC7FAFA84F863BB,
	HttpListenerAsyncResult__ctor_mDFE90FC87EB0D8FF53FE4BC9B8B580AB74B22B57,
	HttpListenerAsyncResult_get_EndCalled_m1E16AAD336095C198EF1DAB1F15F1A4DBEFDF3CC,
	HttpListenerAsyncResult_set_EndCalled_m421B28DE70BE083A785F63F04D278BF352A4987C,
	HttpListenerAsyncResult_get_InGet_mF99BA3CA5C7DEB8AA4442DE97C276322B54FFB21,
	HttpListenerAsyncResult_set_InGet_m1E9F270F0896007F38E308BF210CA831BDC704E2,
	HttpListenerAsyncResult_get_AsyncState_m515FEB15AFFAA8B649800CE079B8585B2E3DEB3D,
	HttpListenerAsyncResult_get_AsyncWaitHandle_mBF886006E806892775E22CA9D88B7E78A362658D,
	HttpListenerAsyncResult_get_CompletedSynchronously_m454705B78FF65727546DF543D20FEDA7D9089998,
	HttpListenerAsyncResult_get_IsCompleted_m8F4B041D3A89C0B2DADBE7C52D5D49D8D8EFB39D,
	HttpListenerAsyncResult_complete_mD904E5E919816B563D476AB0B0EAE3A2C461B630,
	HttpListenerAsyncResult_Complete_m7F3B28A9A81ABB892C3AD870E90D99600085C5A9,
	HttpListenerAsyncResult_Complete_m84099D5C394117D0C9DC99E86B9D93666332A967,
	HttpListenerAsyncResult_Complete_m9FCC747787E480BA2BD5086F2B838C6F2FFEE900,
	HttpListenerAsyncResult_GetContext_mCC12C2BDDF06E001D3188680D7FA4A1896CF4EAF,
	U3CU3Ec__DisplayClass25_0__ctor_m32E221A89D77D3F93C72170A4776B5B30766B993,
	U3CU3Ec__DisplayClass25_0_U3CcompleteU3Eb__0_m9DA9AD85378DC26575A9B173543399474028C428,
	HttpListenerContext__ctor_mEC3E975A38CD16AFA31BEF8520DC3E355215EACE,
	HttpListenerContext_get_Connection_mC4F7B876830F5F3C60CEA63B40B7EF5863B3F789,
	HttpListenerContext_get_ErrorMessage_m3BE91A1B1688A07BED20C32B26219939F3649AE8,
	HttpListenerContext_set_ErrorMessage_m1CCA813E55636BF7CBB6F628E649069A3742AFB4,
	HttpListenerContext_get_ErrorStatus_m943D1E92BF78E5B6A2EFFC81F2408E34C60084B5,
	HttpListenerContext_set_ErrorStatus_m8690256B921C64D08DAEAFC3761AC7FAA57A2C74,
	HttpListenerContext_get_HasError_mE15CA0879C3BB54F5BA541F22EA07E15357C3259,
	HttpListenerContext_get_Listener_mCA83CF74E76C9CCB9B7211E8D8D3FA2FD3EC5065,
	HttpListenerContext_set_Listener_m8B1342FD32CD4E5DC243FD8E45F36D7B92819FFD,
	HttpListenerContext_get_Request_m24974B2B52774205FB7972EEBECFED2F261BB344,
	HttpListenerContext_get_Response_m99796CD0302E981122DB731B3257553E1B81578D,
	HttpListenerContext_get_User_m786286044AFBB90D197ADB6BF95F3FD2301BE43B,
	HttpListenerContext_Authenticate_mE0531DF1F21A904E9E2D23AF737FCDA0DE381EFC,
	HttpListenerContext_Unregister_mE3AF8FB9936C548DD32A716A480C767908130894,
	HttpListenerContext_AcceptWebSocket_m329ED388A7A7FCA57CD5B3F697A6F3B79E9F626E,
	HttpListenerException__ctor_m140E7F8D237F079E65DB2DC169E22A2ED29DD372,
	HttpListenerException__ctor_m6B6290F1220D8449F137B81D575028BBEE0FE6E9,
	HttpListenerException__ctor_mD3699129B859D0B3E9BF1D3AB957A310005E0BC4,
	HttpListenerException__ctor_m3582E8A7B6B6D65205A9323926836B0ADE4A5271,
	HttpListenerException_get_ErrorCode_m273A7F192104C12B023F5FCEC5DC3E5285B1FD42,
	HttpListenerPrefix__ctor_m9471F9C54726E8B6BB9A5F596ABC4EDCD3DD2BC1,
	HttpListenerPrefix_get_Addresses_m9D898AA243841CA80ED3C91B368F48C2BA9A4E8F,
	HttpListenerPrefix_set_Addresses_m511945F7C3CCD2BCEA0413872C3441BFCA578083,
	HttpListenerPrefix_get_Host_m783076509C0A08605C45273F698C274337116BC7,
	HttpListenerPrefix_get_IsSecure_m5D54225852FD061FC5C75092BAE49FACFB53C104,
	HttpListenerPrefix_get_Listener_mDAECC43C3CF2583FA7653C6FC975675333AB6322,
	HttpListenerPrefix_set_Listener_mC46618D99B0C192ACB5590E0CEA85F6AE1E2205E,
	HttpListenerPrefix_get_Path_m10771CA8A45375B4C9DFED95E7F5BF6F711D6274,
	HttpListenerPrefix_get_Port_m832B2097F488695083FF75F86A74DE9C7953B4FF,
	HttpListenerPrefix_parse_m89C95236AB93B0D47C3B98903CA1A9B17FAA2575,
	HttpListenerPrefix_CheckPrefix_mC855904E5FDA2BAA5D60099D70EB52ED64A7DE7F,
	HttpListenerPrefix_Equals_mB5C98D8967CA57AE18EEEE7BF766F6D0F534CAB2,
	HttpListenerPrefix_GetHashCode_mBAB7258A7E5DFCD43E58D823680F0D07EE2E65B9,
	HttpListenerPrefix_ToString_m6F58A3AE018DD19708A4FBE1EBE8F085B4754ED9,
	HttpListenerPrefixCollection__ctor_mC23B93B1D0027950B807CCC74A14B59F575ACD95,
	HttpListenerPrefixCollection_get_Count_m0C786C2DDFCD24339BF7FE8C40F3E081E16E4367,
	HttpListenerPrefixCollection_get_IsReadOnly_m750B2A31F9E66A09F5C00F5DAEDC0C6033EE3DF3,
	HttpListenerPrefixCollection_get_IsSynchronized_m1910465F7D5E7F1A3FB0F8C593C5DF4CCCC0951E,
	HttpListenerPrefixCollection_Add_mFA985ECF86E60620CB71F2325A1D540F4E81B456,
	HttpListenerPrefixCollection_Clear_m6AF1A36740EF66AEB3C666FCDFA909DC1BD14E8E,
	HttpListenerPrefixCollection_Contains_m13DD6563A540C233C51746636E7F921869BF360A,
	HttpListenerPrefixCollection_CopyTo_m0186A1EFACAEF3F53DE11757026A38787A26A93C,
	HttpListenerPrefixCollection_CopyTo_m7D7A030835662800DBE8C0B2E398447D554C0CC6,
	HttpListenerPrefixCollection_GetEnumerator_mE470E08D97942B167E6BA903290DB889792D86E0,
	HttpListenerPrefixCollection_Remove_m63DA07AE4F0519675940C024FB1B57B8BAC47228,
	HttpListenerPrefixCollection_System_Collections_IEnumerable_GetEnumerator_mF6DC16639A8D811CB4B11778CE74FD11D92E5B0E,
	HttpListenerRequest__cctor_m4588214FD422C13F9248002AA994FEED9A15C50C,
	HttpListenerRequest__ctor_m5B86C8FAFA005EA20CEE0A7897A3F47F9BF4524E,
	HttpListenerRequest_get_AcceptTypes_m602E67A500D996A4712FF5FE4BEBAACE24134CAB,
	HttpListenerRequest_get_ClientCertificateError_m3959169BF9F484C40114BCA995530B53EBE2DD66,
	HttpListenerRequest_get_ContentEncoding_m6E2C9DAAFB0DCC965837FBB6151927BC4446F704,
	HttpListenerRequest_get_ContentLength64_m285203447B5D6EA9405F156821463503924FAA13,
	HttpListenerRequest_get_ContentType_mF1D61FA461C611CB045B4B6C78305A2A743E23AC,
	HttpListenerRequest_get_Cookies_m0DABBC7927B532FA60F7784E6B5FA541CE30ECFE,
	HttpListenerRequest_get_HasEntityBody_m54937AFADCF1CA3156B2573294C1015F265B54E5,
	HttpListenerRequest_get_Headers_m1D67466C679CFB22D3936A19BC6E998EF218812F,
	HttpListenerRequest_get_HttpMethod_mA4556510681007979ADEBCF013FFECA8A1233E6A,
	HttpListenerRequest_get_InputStream_m89DA3623E70DE0C87DE3F388DD7CD4CE0346182E,
	HttpListenerRequest_get_IsAuthenticated_m6C529C9247853E86198109D562644CF3294ECD90,
	HttpListenerRequest_get_IsLocal_m3AA66F38458E54D48997E9928D91C41956F1A890,
	HttpListenerRequest_get_IsSecureConnection_mB34F36C0029F88C249CC9A22FF738BB314E99863,
	HttpListenerRequest_get_IsWebSocketRequest_mD8989F090E6C76736E2FA7E2024BF70C14779EFE,
	HttpListenerRequest_get_KeepAlive_m67BE6D6B08BEF0A710C6AB16DC1A535C45F8404A,
	HttpListenerRequest_get_LocalEndPoint_m091629B464A47422B597E8D39A7ABCEF50A319E5,
	HttpListenerRequest_get_ProtocolVersion_m1BCAEC81C2A07EC2268C05CC01A77A0499BA411A,
	HttpListenerRequest_get_QueryString_m94801886E1A2A398ECA163CA31807D594E9E936D,
	HttpListenerRequest_get_RawUrl_mFA504322F210C80A04B5A04284035BDEA48B6865,
	HttpListenerRequest_get_RemoteEndPoint_mEABBF656AE82DEE3DE9176C512FED612144BCE65,
	HttpListenerRequest_get_RequestTraceIdentifier_mC5C07FCB74AE3876E2ABA2F21DB21D47BDB24F72,
	HttpListenerRequest_get_Url_mC24F6486FB47DC1EE7155102E6CE8222BE0EEA8C,
	HttpListenerRequest_get_UrlReferrer_m120327F7DEBD583F525445F180D1423507A557F5,
	HttpListenerRequest_get_UserAgent_m2F6AC550D3C0F5A28AD34C7A4837B588C8620F5A,
	HttpListenerRequest_get_UserHostAddress_m2FE2B2CB8A71DF20C393CEE7AD7CCFA54960D29B,
	HttpListenerRequest_get_UserHostName_mC4DC1B67C87F5FF89200ECB415BF60264BFB2E48,
	HttpListenerRequest_get_UserLanguages_mF89752D22E46BB5AE87C95E97E42ECD70DF0ADF9,
	HttpListenerRequest_tryCreateVersion_mBCB0B197E20221088E98C35EC9040118A141105C,
	HttpListenerRequest_AddHeader_mA0986EDA8BB18D6AD2E5489A25A0A2B0E0DA2E51,
	HttpListenerRequest_FinishInitialization_m542353B91FF62352E79CCAAD2BA793F7A47F0FF2,
	HttpListenerRequest_FlushInput_m6E501639996008731D48241D9C0C0F62804AE233,
	HttpListenerRequest_SetRequestLine_m0E97F29945C71B1A9177DC9D3C0FCE8605707697,
	HttpListenerRequest_BeginGetClientCertificate_m72607FAF39637FD69E782DFC51643565C77DA6B9,
	HttpListenerRequest_EndGetClientCertificate_m11F1A47BA087EAA3EBB6EEFDACB15A225011E9B9,
	HttpListenerRequest_GetClientCertificate_mCC7D169967F086ADCA5BD5E44B4F5AB63547507C,
	HttpListenerRequest_ToString_m210D02619389F43DEC83127FE5E74F00E97AC7E3,
	HttpListenerResponse__ctor_m51DEAF9B754DF9718709C63C85B64A38313EA3C5,
	HttpListenerResponse_get_CloseConnection_mF16F6A96086009E76E506338157D8D807C33E9B3,
	HttpListenerResponse_set_CloseConnection_m45ECAD23BD3B6A83C17638F771A90F81F16DF458,
	HttpListenerResponse_get_HeadersSent_m4E70A2D819C64D629A6D0123A085FC801D326359,
	HttpListenerResponse_set_HeadersSent_m35A1E66F56D95D44C2DD3E58F739F8EDDA9FC243,
	HttpListenerResponse_get_ContentEncoding_m94F135C884D6021D79B736D5E445607C16A1BF2F,
	HttpListenerResponse_set_ContentEncoding_m374AAE8C21E73089F0B940628EF3F5A17F9820D9,
	HttpListenerResponse_get_ContentLength64_m281467961A422506EFA125D438E6865284B5C3AE,
	HttpListenerResponse_set_ContentLength64_m4971B5FF337F08EF62867B9BD78E794FACAC93A2,
	HttpListenerResponse_get_ContentType_mD49F13F0462A82440F42C7D6306F7CD52A9BF1F2,
	HttpListenerResponse_set_ContentType_mE7CD25DF1B2304ED889AA8739FEBA01B74D22B34,
	HttpListenerResponse_get_Cookies_m6177F1AC3B42BC5B6C79CD5B3E22EB7E810C5AD3,
	HttpListenerResponse_set_Cookies_mFCBAD89F755926ACC31810735420E98E3B3F41C2,
	HttpListenerResponse_get_Headers_mB674B9C921F3CCDF0C631D194E96C01305283402,
	HttpListenerResponse_set_Headers_m9973A9B5350D0C6B223D605D2B03505AA4E06EFC,
	HttpListenerResponse_get_KeepAlive_m164AA94968E1429CA81FF5AF4EE86E2D562A191C,
	HttpListenerResponse_set_KeepAlive_m5D321C535F7666E014A249238919DCAC10936B6C,
	HttpListenerResponse_get_OutputStream_mCEA179A379AD1BC3812F2C93EECB461BAB20F506,
	HttpListenerResponse_get_ProtocolVersion_m7AE0FE1E4995F05BA91DBEDA73662705D6D68A65,
	HttpListenerResponse_set_ProtocolVersion_mE4991307CCD873BF096B3B39BF823EDC1A322091,
	HttpListenerResponse_get_RedirectLocation_mBE5530EC02F1B2694EB30B9AD3FDC7DE84EF5272,
	HttpListenerResponse_set_RedirectLocation_mE981DEB87B3F0E5246E06CA811291CFBFABE9E10,
	HttpListenerResponse_get_SendChunked_mAB66A1FA96BA4FA3F037FF9DC4933D54429D45E8,
	HttpListenerResponse_set_SendChunked_mAA7D3CAEE544EC21DAAAAB7BD26FEDF131C25A09,
	HttpListenerResponse_get_StatusCode_m5098E0674B169BF37719F4AA4535FE5E882F5B0B,
	HttpListenerResponse_set_StatusCode_mEECB72E6E0478CEA400056A150C924140F92BCFD,
	HttpListenerResponse_get_StatusDescription_m2AC1D537324FF1FD0967CF93371AD4D827DE8531,
	HttpListenerResponse_set_StatusDescription_mFBF9C58EF31D591443C8F791D51966EAB84DF079,
	HttpListenerResponse_canAddOrUpdate_m8B465468321E43F67E08CE893EAFEFA46F04DD60,
	HttpListenerResponse_checkDisposed_m13B9D79D23D872991215FB8AC8A2EDE4F3B8B3E2,
	HttpListenerResponse_checkDisposedOrHeadersSent_m9E82829926121E7EF7AFF4556F343A5A865D0D6B,
	HttpListenerResponse_close_mBE90E8A70885962B2BAD3972E8ED129D835ADA68,
	HttpListenerResponse_findCookie_m1B3D82E3E8451793D2694E36B04795E3DD84FF8D,
	HttpListenerResponse_WriteHeadersTo_m18E71505856C1964A4F83196E1A2FC9C025766E5,
	HttpListenerResponse_Abort_m2E5A4B236A33F84CD1ECD45889B32E25402C098A,
	HttpListenerResponse_AddHeader_mE8CD2E42D8F85D049537A37FB16744C8FB8A751E,
	HttpListenerResponse_AppendCookie_m2E2D010E79FCA14DE60B6138758BC6F88FE14F17,
	HttpListenerResponse_AppendHeader_m2B96FDFDA0FD182CB2C343DA46FEACDDE2924DF5,
	HttpListenerResponse_Close_m5DC68D529409BBC0E5F639C40F9291E12C847355,
	HttpListenerResponse_Close_m71175099B665CC31424CE74041DBDE114611EA97,
	HttpListenerResponse_CopyFrom_mF115294EF054C17306439064D4A61F7AF21BB2C1,
	HttpListenerResponse_Redirect_mD0F05DDC5C5F08F14C5F73AB9A3EA18D96BE902E,
	HttpListenerResponse_SetCookie_m83161ECA1CF34C3DF4E2D19A3B5F3C3CF636EAD1,
	HttpListenerResponse_System_IDisposable_Dispose_m0714F8AB66A93EBA9AA2D41C17E10D97938DD0E6,
	U3CfindCookieU3Ed__62__ctor_m864668186C1C9059A9DC53C6B23CF6E8AA84E797,
	U3CfindCookieU3Ed__62_System_IDisposable_Dispose_m78B075B93855822EE59FBE7D4DD5D6BB2784EDE2,
	U3CfindCookieU3Ed__62_MoveNext_m8ADA93366AF79A21256DC5B3098C7D8EF4A95106,
	U3CfindCookieU3Ed__62_U3CU3Em__Finally1_m21C257A670F19BC258C805DB05734F9240501B8E,
	U3CfindCookieU3Ed__62_System_Collections_Generic_IEnumeratorU3CWebSocketSharpUnityMod_Net_CookieU3E_get_Current_m07D0FD4B852A2695B804A37C87FDC7387BBD4FDD,
	U3CfindCookieU3Ed__62_System_Collections_IEnumerator_Reset_m73D562801308883F4A4E69DF6C14435BB1FFC5F7,
	U3CfindCookieU3Ed__62_System_Collections_IEnumerator_get_Current_mE47F57ED433706BBBE7D96A9425B1DBD836C9F45,
	U3CfindCookieU3Ed__62_System_Collections_Generic_IEnumerableU3CWebSocketSharpUnityMod_Net_CookieU3E_GetEnumerator_m71E5056FAE4848EC5512E7ECD86D66EB32A15EC0,
	U3CfindCookieU3Ed__62_System_Collections_IEnumerable_GetEnumerator_mBEE18F7F9D8764EA76E11AF54F29D80BF013941B,
	U3CU3Ec__DisplayClass69_0__ctor_mC03A9702424A8E446928EAAE64C1F44DF623CAF2,
	U3CU3Ec__DisplayClass69_0_U3CCloseU3Eb__0_mB95CE1A6D7A51AFA0C4D1747E56277AFE915F06B,
	HttpStreamAsyncResult__ctor_mC9C296D4CE0D895260A62717A195D3BEC3C1A129,
	HttpStreamAsyncResult_get_Buffer_m48B4F985F2C35FEA081FFD64C041C4D3166EC9E4,
	HttpStreamAsyncResult_set_Buffer_m67B161E3660F7E9FB0A457033A3CFBBC54E0EB46,
	HttpStreamAsyncResult_get_Count_mE13189DED845A1B36C3B5B90F8AD0F0AA3C1887B,
	HttpStreamAsyncResult_set_Count_mF30E41D15EB1D2BF022265FFB72D7F0C97F49846,
	HttpStreamAsyncResult_get_Exception_mE14DAF1619CBCA7FDF3D5FD93B6E27BFDE550410,
	HttpStreamAsyncResult_get_HasException_mD7BBB7259E986D5FED980149CF5775078058FC21,
	HttpStreamAsyncResult_get_Offset_mE8AD886F547F6189A80658A747889066497B5BCE,
	HttpStreamAsyncResult_set_Offset_m41F9C3CB1C068633C32D7D8C2D3E321CA1723EF9,
	HttpStreamAsyncResult_get_SyncRead_m1CF0914E07C9B9CA5FE0FBDA9AAC0B7D16E4F6FC,
	HttpStreamAsyncResult_set_SyncRead_m083B1EFEF780B921E96668A3B3D9F18BB591E11E,
	HttpStreamAsyncResult_get_AsyncState_mB8EA61F82E78DD9F2AE899A38BE145DBC8650646,
	HttpStreamAsyncResult_get_AsyncWaitHandle_m38C3B3AD53ACDACBBF5A7AAC285AD6D6B44BC72C,
	HttpStreamAsyncResult_get_CompletedSynchronously_m0D37D46F2175F9BECF8BAB65A1360113DCF71B30,
	HttpStreamAsyncResult_get_IsCompleted_mE3CD055E1DF4137078914AD08EAFC718CF548A31,
	HttpStreamAsyncResult_Complete_mC7308B4772E293F6161CB158BB8EAF4298F38F4E,
	HttpStreamAsyncResult_Complete_mE701DDCC355CADA85AB3A64AF8898E77372AFD0E,
	HttpStreamAsyncResult_U3CCompleteU3Eb__35_0_m46B4F9E4D4F6B2E67FD87AA033469FE6BDE18B71,
	HttpUtility_getChar_m6C1F450078077C2A1AC357074158F91BDF48E980,
	HttpUtility_getChar_mCB07049DB6A9EF8A3D3C0ED463BCE2AD1F138F4F,
	HttpUtility_getChars_mD589FDCD414874C32186E5D043FDD50AE1419BB2,
	HttpUtility_getEntities_mFF2B3D0849E663A3227E60F8822607CECFA8F2C9,
	HttpUtility_getInt_m69065B0089DFCA44403C4F1EFED32631E82FDB47,
	HttpUtility_initEntities_mF1A2B4890D84F9151065A3D22A837CCF535B29D9,
	HttpUtility_notEncoded_mFF861798529A3F04235A68227E9746BF6A957557,
	HttpUtility_urlEncode_m6E123DBA1F26D94B27FEAFCC28F2C5AE55EA7E46,
	HttpUtility_urlPathEncode_m2ADA293327D6F322F458DE7799296B49381BDE87,
	HttpUtility_writeCharBytes_m1F0610D1F7C722BC5546469F7F6787C5D11DFAF8,
	HttpUtility_CreateRequestUrl_m0696971E0EADE4FAA192AF6897235CE1870B43FF,
	HttpUtility_CreateUser_mB03B07375B80D0B3DBD3343144EB0AD3E2D2951F,
	HttpUtility_GetEncoding_mF4637143ED19E0E987D87BD1C01E5A0C2DBB797A,
	HttpUtility_InternalParseQueryString_mB71618C3A2BFC60106A4B068986B994362027C8D,
	HttpUtility_InternalUrlDecode_mE949D49D06C3D05B18AB298F6FC6AAB424353D4E,
	HttpUtility_InternalUrlDecodeToBytes_m48C3A8B27F8A9DFF49ED2FDDB47A7C7E7AD705CD,
	HttpUtility_InternalUrlEncodeToBytes_m1746DA854EB1727D2DAAE84D82C2DC48A51DC7D0,
	HttpUtility_InternalUrlEncodeUnicodeToBytes_mD566124911E1A93829621D9645482B8776AFE590,
	HttpUtility_HtmlAttributeEncode_m1D0C8591D221208542CE68B6CC788AC05CF62EA4,
	HttpUtility_HtmlAttributeEncode_mCCFAE88DA85127681D79FEB9355CE818A00E913E,
	HttpUtility_HtmlDecode_m003A03CB7E06EB81D28E684C6EEDAF16DA171FA8,
	HttpUtility_HtmlDecode_m618EB62412AD2525ACC1C34F0703297F58EDAEEB,
	HttpUtility_HtmlEncode_mD0F087D5066224014110072CBF5DC8EC718503B9,
	HttpUtility_HtmlEncode_m7591E13D672E18905FB3D77949DF8B394B637F33,
	HttpUtility_ParseQueryString_m13C434438542D7EDF8443A169426407BA4768045,
	HttpUtility_ParseQueryString_m8582D6FE6C742276DCE0231FFF21881CA7560940,
	HttpUtility_UrlDecode_mFCD9F973A776CF0A19D3A71B29FADA93317ABA5D,
	HttpUtility_UrlDecode_m5C81E873E9D0CBE0A175C73DC839EC6D9AC08FD0,
	HttpUtility_UrlDecode_mD64246F8FDA37815035618DC4869761D7F573E44,
	HttpUtility_UrlDecode_m2FB4B58E70269D234521D38525762C5951758396,
	HttpUtility_UrlDecodeToBytes_mB7BF5C4F54A7001650361DAD1D4DBC77C6682652,
	HttpUtility_UrlDecodeToBytes_m59859D70F33ACA2ED5AB2D39AE382B8B885A0B4A,
	HttpUtility_UrlDecodeToBytes_mAC1FF176183DC6586039738ABA8565FE3534C40E,
	HttpUtility_UrlDecodeToBytes_m8854169F3EDE2E04A9D9843C8CE57F5F01859D95,
	HttpUtility_UrlEncode_mCF5B38E86D59790F6C9359D9F9C18966121F3963,
	HttpUtility_UrlEncode_m8A7960372B4391C2F43152AE0B61ACCE59A82145,
	HttpUtility_UrlEncode_m27CBD80CA21463625C7A6AC7431D2319A72B8CDB,
	HttpUtility_UrlEncode_m5FA3E059BBE19F72299960865CF2B93F48A673F4,
	HttpUtility_UrlEncodeToBytes_m4831725AECC75224D30A460CD8A3BAC425BCFD23,
	HttpUtility_UrlEncodeToBytes_m247179BBF31BCC5E6FE780B51C3A50D37D1F11CB,
	HttpUtility_UrlEncodeToBytes_mBB8EFC170CA14F44133882880737D71B16DA5319,
	HttpUtility_UrlEncodeToBytes_m169A57085B0215DCC13D865F78D7A8E15853E530,
	HttpUtility_UrlEncodeUnicode_m37BAB8B9C607D5D9257B4AAE6D7646A113AFE738,
	HttpUtility_UrlEncodeUnicodeToBytes_m4703342B501B1412480F5636F5E75680387BB992,
	HttpUtility_UrlPathEncode_mA78BE952B206D445DE109480A9447828D4FF2A32,
	HttpUtility__ctor_m85F72F1BB5242727149A2C350706336CDC849B79,
	HttpUtility__cctor_m3BE265BBF9EA3E3594A1A6BA73BBB49E662F74E7,
	HttpVersion__ctor_m915D7D7E452A3408B29605BC0CD16D0E90960B92,
	HttpVersion__cctor_m38BCDF68B4DF8873B9AA05C75B63A95B8F395252,
	NetworkCredential__ctor_mBAD56DDCC46A90B0D7ABBF761B72900350AC141E,
	NetworkCredential__ctor_m45FE77FCF6A8FC07F670757287DFBCB5E777C889,
	NetworkCredential_get_Domain_mB454CBCBBDDF9C5397DAA88C856CD425473D04DA,
	NetworkCredential_set_Domain_m3C30116A3E255D782AC2319597F9BCBD065BC5DA,
	NetworkCredential_get_Password_mFC0A02C22ED8BE886BE31377B2847F60777E08F8,
	NetworkCredential_set_Password_m7F88E6C32C9E942096AA6253EE1BF091F4771FB6,
	NetworkCredential_get_Roles_mACD1CBA559713AF93F5F06C2972E08F71B481223,
	NetworkCredential_set_Roles_mE843750819387AFE9362E10BB3EF124C5035D774,
	NetworkCredential_get_UserName_mA1F0D33106141A6069181FECD978FD4B67B1A660,
	NetworkCredential_set_UserName_mA223359925DA7968BE188A24DDDF1AC1FC3BA17B,
	QueryStringCollection_ToString_m623FABAAE439F550F8E4D8AA5A13969C6C5F3951,
	QueryStringCollection__ctor_mFF3F9A1B60AB3144D7CC36784E2FCB52FA6C0F46,
	ReadBufferState__ctor_m4B2D1A019702369C0AE90582F5F8B1211EC2D3F7,
	ReadBufferState_get_AsyncResult_mBEFE4F6B0328495DC143F37D4848DF447FC8DDF6,
	ReadBufferState_set_AsyncResult_m47FDF655BBF5BA79C53596BF90850CFB4A696157,
	ReadBufferState_get_Buffer_m313E73A63ACA8F18030C4668AF97C662649AADCD,
	ReadBufferState_set_Buffer_m300A3EA65E7D83E5C74E78463DAE453CAC861E9B,
	ReadBufferState_get_Count_mED9BBB669198829E85F21978EEE252FB7A3F6AB2,
	ReadBufferState_set_Count_mE2A780E079A22737E0DD77BD69AE99F97D3E26BA,
	ReadBufferState_get_InitialCount_m1C6367FAF29E4A46FD65551355DAFDD93CB22FE2,
	ReadBufferState_set_InitialCount_m1334EC06B209E213FF273B7D782A479676ED1F46,
	ReadBufferState_get_Offset_m08557FD9E87466BCCA08B32C8BCC2F3585CBC18C,
	ReadBufferState_set_Offset_mB420BCA57E1F286E4C64E8997F8028FB2C2E2E22,
	RequestStream__ctor_mF6D789B645AD51616AD87D28BABBA0E9F899F37B,
	RequestStream__ctor_m80BF17AD78E5CF36506CBA959215AB32CD16CBFD,
	RequestStream_get_CanRead_m6576DD6B00DD1824DF11DCAD722A0E1433A23248,
	RequestStream_get_CanSeek_m97D2695D61A0F04811477A9D6430332CB3E87C0E,
	RequestStream_get_CanWrite_m43F31DF825CC83E6F02C9FCB0D36ED75825F5DB5,
	RequestStream_get_Length_m422B2BB2F324610CE07D2E7C02B45B4911298D95,
	RequestStream_get_Position_mCA0D6359514812233C1A085E9ACF064A31311ACE,
	RequestStream_set_Position_mE864D33F943C6F17699A60E1C550160EA4408E30,
	RequestStream_fillFromBuffer_m1AD69F5B427A74A626A13EF484EE946B728CA9FB,
	RequestStream_BeginRead_m0FB85EE696F9A2B5E4FAFC3FF33AA709FB86CA4B,
	RequestStream_BeginWrite_m335FA78EB81C3CF504B124F3F1F7EF2C688E4378,
	RequestStream_Close_m2A803CFF93665F0CE4D8345B9DA6D2B0ADF45EF2,
	RequestStream_EndRead_mD3BECB27918E0CFD67CFE832D8FFD1767AF7B6A5,
	RequestStream_EndWrite_mB17293DED56E7387457B4E0B44AA36DE1CE58395,
	RequestStream_Flush_m2574453BEAAB9606303717779A0B391E2B79F07B,
	RequestStream_Read_m507CC4E360E9E3FEC1838DBAF946F48359178141,
	RequestStream_Seek_m1CBC519F56F0564E658B280117088D154EB21F87,
	RequestStream_SetLength_mA3A582176C092F3EE0D9C18E497DF029333FEAFC,
	RequestStream_Write_m99AC8FCC832B109652BE8F851D72CF1B5C41AFF8,
	ResponseStream__ctor_m93A046FE999ABBCE64972CB7FF66FE6685B8A38B,
	ResponseStream_get_CanRead_mB7F0C320D93D669B631458CC73A32D41EFD751AA,
	ResponseStream_get_CanSeek_mC175250738B684F11EE0397AC04AA3CEB3A4887C,
	ResponseStream_get_CanWrite_m5165E8C05453F59416FEC3422CE980EDBEEAF53C,
	ResponseStream_get_Length_mDA4C6562EFA38477ECDF62D4F34BC39C80565147,
	ResponseStream_get_Position_mBE3ADD9236FF7F1FD6F667C005B53ACFD2E2FC81,
	ResponseStream_set_Position_m0E2CA58302B2DF9A622544480CAD0060AE9F2120,
	ResponseStream_flush_m96E1722D529C8D5F8ED1047BC7FA6EC7610BF939,
	ResponseStream_flushBody_m9877333AB4633565F070880A965C4E232F364249,
	ResponseStream_flushHeaders_mFABF70B224F42569F0CE3672D926602D85EBCEE0,
	ResponseStream_getChunkSizeBytes_m5303DBB70E5C1372D5AC49C6EBC072CEE0F29C05,
	ResponseStream_writeChunked_mFEB8F316A00AF568533B7CAFE3F754B52228220F,
	ResponseStream_writeChunkedWithoutThrowingException_m656E9D98B65BB35602F8E17B2C2586240BB7415B,
	ResponseStream_writeWithoutThrowingException_m52F430378C40957620B9736AAD6D2384594FC9B0,
	ResponseStream_Close_m53EA47FCE2B5D9D39FC1120B8823E0EDBA86A3D9,
	ResponseStream_InternalWrite_m88328EF0A96B0D4FF3F80CF582B6B9AF17037E04,
	ResponseStream_BeginRead_m2DBEDEB03C0A2ADC21B82EF8141C7A0B0CA80A2A,
	ResponseStream_BeginWrite_mBB0CFE68509838C1B70389610859DC761EADB07B,
	ResponseStream_Close_m37A82381D603B7DF0AA97348C23373B700295CFD,
	ResponseStream_Dispose_m5F447D61CB6E45B52570C0549DDE960FF2786D1A,
	ResponseStream_EndRead_mA38EBB6F247F9E0ACA2601E23E20E10A5E4E2666,
	ResponseStream_EndWrite_m6B2870845E73D7368984B4D4880AABE2DD115987,
	ResponseStream_Flush_m0E859964556D8A10CED69B3D230C6C866F969425,
	ResponseStream_Read_m43EC16F56B6052A3479C170B5E9674C48E105207,
	ResponseStream_Seek_m9AAC33C6B4DA4F66A2C028E2CEE329235A1D9310,
	ResponseStream_SetLength_m86CCDA3BDC5FFF61FBCDC42F12077E4C59DB7FDC,
	ResponseStream_Write_mD8E7183E1529BE89EB8E698FDF3F8658A8597AF7,
	ResponseStream__cctor_m221265C9458B97B3411CE550435CC56651A24CDE,
	ServerSslConfiguration__ctor_m871324D0A1424CA83FA86DB70DCE865A764BEB23,
	ServerSslConfiguration__ctor_mD1FEA2C0AF70774D23B44E0D6B21E95C4CDB4AB1,
	ServerSslConfiguration_get_ClientCertificateRequired_m59D8EAC864FB731D8B76ED0E4C472206EC7E1D2C,
	ServerSslConfiguration_set_ClientCertificateRequired_m6229EB6DBE666866DC4A4ABAE74FD7945238A6BD,
	ServerSslConfiguration_get_ClientCertificateValidationCallback_mC6454A5DC8110975E6E487186719771BD941DA22,
	ServerSslConfiguration_set_ClientCertificateValidationCallback_m2E17E047ADAACA554EA7A1604EE7AE57A6F372DE,
	ServerSslConfiguration_get_ServerCertificate_m54C0BD84F5AB98EEC45E6173A6523E2D7251E6C0,
	ServerSslConfiguration_set_ServerCertificate_m1F938426C8C096B3D4C90E1DE6CE7CC37B56D41A,
	SslConfiguration__ctor_m7E742C9826B0C507D8B8B7DF84FC8A7B447C6DDD,
	SslConfiguration_get_CertificateSelectionCallback_mCB32C0D015192718A52C395A0B743E0F61431903,
	SslConfiguration_set_CertificateSelectionCallback_m5C532365546E145F3766A91F171FCBB62ABB1FA3,
	SslConfiguration_get_CertificateValidationCallback_m5C5ACA8D0CE6B5F4968F15F8D7F40F0604C03BE3,
	SslConfiguration_set_CertificateValidationCallback_mF959EA0A69CEC3A77DA0C382363C359A2218902C,
	SslConfiguration_get_CheckCertificateRevocation_m1346C308FDAC41137BF8560E33C1BC252BE4E9AA,
	SslConfiguration_set_CheckCertificateRevocation_m166FF037EEB0EFF445AB45DDCEE1B2747BCC5B67,
	SslConfiguration_get_EnabledSslProtocols_m607F9EF3B993FEA14CD07F19814E0D76FDAEA40B,
	SslConfiguration_set_EnabledSslProtocols_m9EBBB04ACF3449F739F232BBD0ED40A57B81D53C,
	U3CU3Ec__cctor_m8A5D84E010CCAF5B7BEC0E9306EA4978FAEA9EB5,
	U3CU3Ec__ctor_mAB01352673FA235C44BA1C5E2F5B9C41532F0AAA,
	U3CU3Ec_U3Cget_CertificateSelectionCallbackU3Eb__6_0_mD12EFABE59EF2DA13637E5C47754847769BBDC50,
	U3CU3Ec_U3Cget_CertificateValidationCallbackU3Eb__9_0_m485AC7F61C0B75C52DCBAA75BE38F21781932410,
	WebHeaderCollection__cctor_m649A94667979754A9759D0C611345A3079765B4D,
	WebHeaderCollection__ctor_mE931371B497688B4364E74ABBD28B8173BFC896E,
	WebHeaderCollection__ctor_mFE9E6882157002C92630B7B68854466EB7E0C7A4,
	WebHeaderCollection__ctor_m27D3710730CFC952C4108056CF8978C056B6E580,
	WebHeaderCollection_get_State_mAC4BA8818B0238899AE8788B5CA5FDCF0B67E610,
	WebHeaderCollection_get_AllKeys_m813214E0117C771F6A6597402CD1D1A66663B93A,
	WebHeaderCollection_get_Count_m811124F7A68BD0C2B150AC635C0BE93713FD7C6B,
	WebHeaderCollection_get_Item_m2AABCFFAE5E9E1A937C90275BEB85171FAD13D50,
	WebHeaderCollection_set_Item_mB9F76090F01AB6364850722684C5AF282AFEB8D1,
	WebHeaderCollection_get_Item_m58ECA637FBA09F32A24C7C1C242483589F01F9A4,
	WebHeaderCollection_set_Item_m204A34CED0FFE9B8D085827D96E145DF2F08100C,
	WebHeaderCollection_get_Keys_mF1F0667A23DF6089DE32FCEDB51C8D2C2F764AE0,
	WebHeaderCollection_add_mA18BEDAF0773EFC55073CC4718F21B5C24CC43C2,
	WebHeaderCollection_addWithoutCheckingName_mABA029B67161DF2509E0C874A3F6F25D70A9AEFD,
	WebHeaderCollection_addWithoutCheckingNameAndRestricted_m1B2B245B45F7FE49BADF58EBC06C3CF7CF8B7101,
	WebHeaderCollection_checkColonSeparated_mC5BBDFFC466608E4D883819A8AB33B9E3D02B4F3,
	WebHeaderCollection_checkHeaderType_mED3DEFFB088E69C4D68A3D4465CDB67BA678F80B,
	WebHeaderCollection_checkName_mF8F1426D4242A53EE03B9CA5DBAE48E43D3E9FD4,
	WebHeaderCollection_checkRestricted_m2B0FF5CEEECFB90771ACF15971B8AA0A1DD5254F,
	WebHeaderCollection_checkState_m11F272D6C9A6680B1F240C19AC163D09372DB589,
	WebHeaderCollection_checkValue_mD96CF1A4287DA2D1DCB33ABBE7A085B779F6775A,
	WebHeaderCollection_convert_m598CBFDB00113BE2BF1CBBBBD8CCB925926D72CE,
	WebHeaderCollection_doWithCheckingState_mFD0F6D28F81C2AEB7F41628A5105F0A6EE5CB7F0,
	WebHeaderCollection_doWithCheckingState_m96DF3A9E0DD5BAD62A8E0AFAF9964EA2388B7480,
	WebHeaderCollection_doWithoutCheckingName_m5FD9B4F2ED7AA65499C3530966EB8B150BF2D413,
	WebHeaderCollection_getHeaderInfo_m6836D5FD0763F5C9869F7904A49B39F641619C85,
	WebHeaderCollection_isRestricted_m7A78936CF437B958699CE85238B813074AD6B643,
	WebHeaderCollection_removeWithoutCheckingName_mB88FCDA4ED5A8AA87841D5E3973E80A8F1BA90AD,
	WebHeaderCollection_setWithoutCheckingName_m705FDBAFE127FF3E1131870F5DA4E600093AB06B,
	WebHeaderCollection_Convert_m0625AA81995B8C99A6B54AB95B324F74282C3375,
	WebHeaderCollection_Convert_mE3C62D75B561F96816D04C4F197A65EE89C6DF2C,
	WebHeaderCollection_InternalRemove_mA2B7F6584275E823940011889FF94808834E0BF8,
	WebHeaderCollection_InternalSet_m7E3360DD00BB4615DF1043BC4A9B461475AD12AD,
	WebHeaderCollection_InternalSet_m9B02D9B5DF0E2F8DC4BBF275DE95E2BD71CD26CD,
	WebHeaderCollection_IsHeaderName_m9F6296159955C4FA47B1DFB5F3CAE9271DAD3624,
	WebHeaderCollection_IsHeaderValue_m2F452605E6E6581659E09CB86AD4D37ACC6D6EAD,
	WebHeaderCollection_IsMultiValue_m692F484792EE2EA7B9D4C69647BE5B2117D714B1,
	WebHeaderCollection_ToStringMultiValue_m2E48F26A1F843D0EF20D659D4DF9D7E713604666,
	WebHeaderCollection_AddWithoutValidate_m230085464EE0CED91C1283D50C1D6B3E8619A6C5,
	WebHeaderCollection_Add_mF5A95C1CF443989ED7841038C2DD1609D6B6114D,
	WebHeaderCollection_Add_m8159ADD8D69C59281B5453D8EAA02D8F1A8C3E5F,
	WebHeaderCollection_Add_mA8FA8DF2D855A72C760C12A847F56ED5C8833585,
	WebHeaderCollection_Add_m3B1FBF1454344B58CF6365243A1AC54EDEA61119,
	WebHeaderCollection_Clear_m6689B340B69057D427CA66CB2B11379EF08A56CF,
	WebHeaderCollection_Get_m74DAED1A5966632EB6E235171193E2CE64F5D431,
	WebHeaderCollection_Get_m4EBDC30E316252603EC3E552B6CCD01642C8CE11,
	WebHeaderCollection_GetEnumerator_m89003D0942C681E32112384913FA585D5EC0CBCE,
	WebHeaderCollection_GetKey_mBE31634017E9650FEFD826DA59C4718ACDDA4774,
	WebHeaderCollection_GetValues_mEC849F7AE5A395B22A98F9429C8E742473DBD65A,
	WebHeaderCollection_GetValues_m002548CFB27172001B416D7EEDB6F0EB647F7CEF,
	WebHeaderCollection_GetObjectData_m757636963D78F387D3B709C3D9A852DB51344845,
	WebHeaderCollection_IsRestricted_m41C322A06233D04789FB3F45DB33E93872C636A9,
	WebHeaderCollection_IsRestricted_mFF8895DB7DDA2790A6C9A5D545A683CFEFA12B76,
	WebHeaderCollection_OnDeserialization_mFCF18F78CC55D019CDDC1DAF5C9A481706C80FD3,
	WebHeaderCollection_Remove_mAE67BAFC234EF48F1811C13DDCEFDF2EA01C66D6,
	WebHeaderCollection_Remove_m26BE115ACEBDE5FBE12ED78638829A969A752468,
	WebHeaderCollection_Remove_mF61A70469EEC887B5AB14A093BF018DD52599110,
	WebHeaderCollection_Set_m8B5D88EDC48984F173F01578CCB4ABB8AB192581,
	WebHeaderCollection_Set_mE726AE0FA9A6ECD6640C2B94156571D4AC4ACAC3,
	WebHeaderCollection_Set_m152417993DA03F0DE1C8AEAE394992D960A71C59,
	WebHeaderCollection_ToByteArray_m3F06646093A569682236424CA038A01967D4D5D9,
	WebHeaderCollection_ToString_mF7F5E7EA442EBE443DDE302376E9F6F7DE41AFEE,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m522BD95208406D143C58721C43197C1F026BA653,
	U3CU3Ec__DisplayClass46_0__ctor_m1463B408740B589BCBFD4AF08C4E740B845193DE,
	U3CU3Ec__DisplayClass46_0_U3CToStringMultiValueU3Eb__0_mDC437E9B28B9DA6D5A173BF7F6D71416AC18DAB3,
	U3CU3Ec__DisplayClass59_0__ctor_mD2F9BD52E27030223F67992F8F89AD13247FB4D4,
	U3CU3Ec__DisplayClass59_0_U3CGetObjectDataU3Eb__0_mE70B43504D2DADDF374DD21616E28A879E933E78,
	U3CU3Ec__DisplayClass70_0__ctor_m7DB28D035AD376368D0F6CE909E5F7F1ABE416A1,
	U3CU3Ec__DisplayClass70_0_U3CToStringU3Eb__0_mBFDEF858CDE42592A03FF94706E492AB3264BAA0,
	HttpListenerWebSocketContext__ctor_m976B3728277B2E976BF2D55E7D2A71D75E687B39,
	HttpListenerWebSocketContext_get_Log_mAFA8B7E4E6381E3448011C5C660C4FCA93637060,
	HttpListenerWebSocketContext_get_Stream_m2394C53F18518C119968CA6775807A51FF10D819,
	HttpListenerWebSocketContext_get_CookieCollection_m08E8A674833FAA94F05D1B49C7FFEDFD0CF2A3D1,
	HttpListenerWebSocketContext_get_Headers_m0FDA29F17EC98E00E86028ABEF3A1D831E5CCF3A,
	HttpListenerWebSocketContext_get_Host_m2937E828609C7A44E8D043FE29BDABF3A9E63FCA,
	HttpListenerWebSocketContext_get_IsAuthenticated_m1A96456C2B472DC14D064C113B75DAA0E243FE75,
	HttpListenerWebSocketContext_get_IsLocal_m1DADF3F55009C8C43ED080BF3CFA867A25439727,
	HttpListenerWebSocketContext_get_IsSecureConnection_m0A82A2064ADC96BA3B58EFBE4758CB64695E44B3,
	HttpListenerWebSocketContext_get_IsWebSocketRequest_m77006BD20C5D70AC4F8920F5BE979A79D877072D,
	HttpListenerWebSocketContext_get_Origin_mA42B609329D0F5C13BE69CACD57C1969CD06100C,
	HttpListenerWebSocketContext_get_QueryString_m4BDD673EFD0E82D7E5A1512E7A35253996F974D7,
	HttpListenerWebSocketContext_get_RequestUri_mE7BF2A920B919D1FF590ACB7C9CE8ADEA75D30D1,
	HttpListenerWebSocketContext_get_SecWebSocketKey_m93A6CA90785510238066A1320BF760BE36A85B6D,
	HttpListenerWebSocketContext_get_SecWebSocketProtocols_m40F443FA2ABDD4F1C47BCFA3845D6254A4D7569F,
	HttpListenerWebSocketContext_get_SecWebSocketVersion_mAD79169E810DDE27DF184653F51A851310D93970,
	HttpListenerWebSocketContext_get_ServerEndPoint_m3E9342BA2B36832B3C2F9032E2255663F5186DAD,
	HttpListenerWebSocketContext_get_User_m596480E807419DC74D5B37171C9CA2613220001D,
	HttpListenerWebSocketContext_get_UserEndPoint_mBEC306B1C7BD629DD187CAB74A626AC7EF38E6FB,
	HttpListenerWebSocketContext_get_WebSocket_m21D5F1FC52F608150240E12B1A3B86CA74FD0C8B,
	HttpListenerWebSocketContext_Close_m76493BE0E623488EDB63CC4ACE9B885129911795,
	HttpListenerWebSocketContext_Close_m3EAE58BDB1A54BF29568BCF34B05D8F60D4F8557,
	HttpListenerWebSocketContext_ToString_mED17C27709F2DA175298430C7E1FC718B53397C9,
	U3Cget_SecWebSocketProtocolsU3Ed__30__ctor_mE4F9E1CEEC73589F17DC8E03EE0A93F83393A917,
	U3Cget_SecWebSocketProtocolsU3Ed__30_System_IDisposable_Dispose_m1138720B320B2BF20D042113176BE73804F27A50,
	U3Cget_SecWebSocketProtocolsU3Ed__30_MoveNext_mB600C047EEEE9729A43DE4D26E0D7EB9D5FBFEE9,
	U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m637959AC8B2CE9AA5C9669A4C8CD59199BCC26AC,
	U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_IEnumerator_Reset_mF16574E98614515C07FFB82275CE1E8FC2AFD4D1,
	U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_IEnumerator_get_Current_m46D1F81DBFCAF73D2CCCEE7D39120583B2E9C460,
	U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mBF7E12CA5972C6AFA90382002906C689500F8A7A,
	U3Cget_SecWebSocketProtocolsU3Ed__30_System_Collections_IEnumerable_GetEnumerator_m1535AABF39710E7A1B416238BD0CF80294E5CBE9,
	TcpListenerWebSocketContext__ctor_m4E5CAB6165756B39D5017BB4B914147838B057A9,
	TcpListenerWebSocketContext_get_Log_m83133CAFCD83CA825A460E01A359273AF309111A,
	TcpListenerWebSocketContext_get_Stream_m5BE41F20F6BF386224F8D919469C7E5D5EE76AC6,
	TcpListenerWebSocketContext_get_CookieCollection_m25829DBF077031487BF16533D225168A0EBDF194,
	TcpListenerWebSocketContext_get_Headers_m37D098FCBD95F3A0B8A482E02DD5F7BEF044F51B,
	TcpListenerWebSocketContext_get_Host_mCAE31D6EEF83DBBA35511D5A46ED0811A9A6488F,
	TcpListenerWebSocketContext_get_IsAuthenticated_mF91BB26FDFC5C666C72CC4DFFF3313D8940E3A59,
	TcpListenerWebSocketContext_get_IsLocal_m96C5D64008B10C31B341EDA3C6EC98032A808662,
	TcpListenerWebSocketContext_get_IsSecureConnection_m9356E1668C7DB4532EAAD863BC138FF17CDDBBD1,
	TcpListenerWebSocketContext_get_IsWebSocketRequest_mA2B72A9B003FD23ED473A60152D40B05469A135E,
	TcpListenerWebSocketContext_get_Origin_mF6D7EE059D43CABFF03AD80B3119803B556F1E11,
	TcpListenerWebSocketContext_get_QueryString_m40C0A98C125DFC16F8F9D5F5277968C9A6DED8F7,
	TcpListenerWebSocketContext_get_RequestUri_mF0FAEEF1DBA0C9653505327EC27A35B7C53F88F1,
	TcpListenerWebSocketContext_get_SecWebSocketKey_m80AFCDCA89F6D6B769748CF5892361592560AF6F,
	TcpListenerWebSocketContext_get_SecWebSocketProtocols_m89369E3BC10EED63E8C6F8BBC0FD5CD5F38AC1CA,
	TcpListenerWebSocketContext_get_SecWebSocketVersion_m9C0BF44A4E07CC87CEF218C5192AE791E25D6FA9,
	TcpListenerWebSocketContext_get_ServerEndPoint_m73BC116BEEA4F1B37301808AA19941067330C7E0,
	TcpListenerWebSocketContext_get_User_mDD633590374B719BDD7EB547BF00792D2479B1F9,
	TcpListenerWebSocketContext_get_UserEndPoint_m468711EBC5C2265E9A1BD8DADD8AE1462D38F571,
	TcpListenerWebSocketContext_get_WebSocket_m85DFCFD2E88AD1E9BEAE96CD35D8E39F394DE91A,
	TcpListenerWebSocketContext_Authenticate_mE37C79CD5E61A3E6F745C5DCC193B266AFAE094C,
	TcpListenerWebSocketContext_Close_m3B26EEB001479D687827EC649EDA9260FBD3740A,
	TcpListenerWebSocketContext_Close_m51BC1F700942DD2F3CA6C3E9B3CE77E2DCD2C5D2,
	TcpListenerWebSocketContext_SendAuthenticationChallenge_m0C0D26F76F947A188E5A53EB69A3737705BEA91E,
	TcpListenerWebSocketContext_ToString_m786F77423F6D864D69C8894951B7304A2B2E8C9F,
	U3Cget_SecWebSocketProtocolsU3Ed__38__ctor_mACFAE394CAA570D5687A43312F14E15B4633FC88,
	U3Cget_SecWebSocketProtocolsU3Ed__38_System_IDisposable_Dispose_m6227922B3FECD81F3CE806AB696C49D317138E1B,
	U3Cget_SecWebSocketProtocolsU3Ed__38_MoveNext_m935CBD050087B1A6BD601EF35C5989CA50930069,
	U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mF1DA314254C0D92D7583A53293CCFD695B8F49F0,
	U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_IEnumerator_Reset_mB1A336EC6C311FCF818463A4CD9039D33AC46CEC,
	U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_IEnumerator_get_Current_mCF745DFB18C99509AAF064E6140CF46519A16B6C,
	U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m28C9B2E6469AA5DC0B6C78F54B8EC12FAAC1BF85,
	U3Cget_SecWebSocketProtocolsU3Ed__38_System_Collections_IEnumerable_GetEnumerator_m70F790F886216C71F7F6409F36436E5C590801E4,
	U3CU3Ec__DisplayClass49_0__ctor_mDFC465C527EBFBC61910E39996222339520C59F3,
	U3CU3Ec__DisplayClass49_0_U3CAuthenticateU3Eb__0_mDAD8EA7DE6BA39591009FCA534892B990D6BDBF4,
	WebSocketContext__ctor_mC2B6467971A86E79172511E451FC574C86A1C956,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CsPeerEventArgs_get_EventType_m85634115F16B45AEBAA2EDEC95539208D90E2147,
	CsPeerEventArgs__ctor_mEC497F173378DF9F38C389E69FBF17CC6682FFA6,
	CsPeerEventStringArgs_get_Content_m1BAA06C784FCCAE30C39427EFEEE275E6EB13300,
	CsPeerEventStringArgs__ctor_m7E260877D664FD175027A5C8C87BCC7073D8F2DD,
	CsPeerEventMediaStreamArgs_get_Stream_m8B18E1BEA60BA27665642E4DEB079637A2F2630D,
	CsPeerEventMediaStreamArgs__ctor_m0BB7F654D35E0B495E96B5ED2DE90BFA9973370B,
	CsPeerEventDataChannelArgs_get_DataChannel_m40E6B85265DF8775C0035E01C25D55C5A96B6200,
	CsPeerEventDataChannelArgs__ctor_m36EA62650CE43DFE00912886B66CC183CDE197AB,
	NativeCsMediaStream_GetInternalStream_m79A78C8BDF67B96A2327B5CE66EF5D8EAA732103,
	NativeCsMediaStream__ctor_m4B19997E60E1C846E784F4FDFEE395F19A056F79,
	NativeCsMediaStream__ctor_mBB13033363FED15AF8E3DF8EB2489B240ACDBA42,
	NativeCsMediaStream_Create_m7470906EE9289D24EA5BB2BFD3E98C5A7462E1F2,
	NativeCsMediaStream_Dispose_m67F09C74D9EBCD6FAC56790E0E3AFA681CCCAE0D,
	NativeCsMediaStream_SetVolume_m331254826A93F9CAFE2A084318B5D84DCFC2AD64,
	NativeCsMediaStream_TryGetFrame_mC52B7DE279EC9B5B63B0752640C4689C0C5B575D,
	NativeCsPeer_get_Internal_m9D390F11ED9D82D8E82FA25DCB6F78DCB2F1F63D,
	NativeCsPeer_add_EventTriggered_m3EBC96AE2E811C45DF1F892D48AE5F6FAB551E81,
	NativeCsPeer_remove_EventTriggered_m738F407BDDF6A83D182EE516B8E6F7C1B4E00B0F,
	NativeCsPeer__ctor_m66684B02BE95A8FEBA0D6EA926F913B2FC8EB243,
	NativeCsPeer_CreateOffer_m3E677839E10974AAB901D5D250C43C1E12485EB2,
	NativeCsPeer_CreateAnswer_m81BC9E7E48097587F7E0E9618A944AF592089BC1,
	NativeCsPeer_SetLocalDescription_m6910310A7EA230B3AE099EF5ACCCCD0945EF57B5,
	NativeCsPeer_SetRemoteDescription_m6C8D50ACC9BA1E48C9A2E6E063F2124307EFEA30,
	NativeCsPeer_AddIceCandidate_m5B7084D0EDE74C694430AA7F34F65F8FB28D940D,
	NativeCsPeer_Update_m92968C32A345A4FC5645CDDC114D31BD39F43C9D,
	NativeCsPeer_Flush_m2966AB79A66B456D6BAE1AB8190DEA5323CA9A90,
	NativeCsPeer_Close_mDE2BC1BBC2B497938F2130570B2D1513C6FF1225,
	NativeCsPeer_AddLocalStream_m9ABD05E0703F45CDA4EC353A76B2C2E407ACC30B,
	NativeCsPeer_GetLocalDescription_m8F614671AAEEAEFEC0133F84787450AE03B5560E,
	NativeCsPeer_Dispose_m6CEE2308BFF7AF83E6875F8B642F09996DC89AC3,
	NativeCsPeer_Dispose_mECCB488796B5055E87310E9E42CA52B06AB521B5,
	AudioFrames_get_BitsPerSample_m06694B90DF355FDD27491C11D4637A2F808B7C6C,
	AudioFrames_get_SampleRate_m727E7A6EB67AA9D104919DA8AAAA339EBE850E52,
	AudioFrames_get_NumberOfChannels_m0363D939ECC9FBEC7A695F1C5C286E0B721A5488,
	AudioFrames_get_NumberOfFrames_m3CE5B08CD4F653573605D5862E69B882497B1287,
	AudioFrames_get_Buffer_m3C52F35D9D6501DDB761437E7CC5B65936ED11D8,
	AudioFrames__ctor_m081801470DB9730058CAA12D650E0E5E9F9665D2,
	AudioFrames_Dispose_m5EC88C82686FCDCAC3A6269D74F4838480C140E4,
	AudioFrames_Dispose_m2592DD1B8D305905D2FC3FF35CA69A524CC376C5,
	DirectMemoryFrame_get_Buffer_mEBD0653C49802FA92F4EB82B78AB84689649E570,
	DirectMemoryFrame_get_Buffered_mCB9BA652D798A54A6D81C5CBA788E9360586ED13,
	DirectMemoryFrame_get_Format_mC1A7380FB377464B959E0E9B61A34E1ADCBE866B,
	DirectMemoryFrame_get_Height_mB579BB0ED4427585E5CF9D0C669576942F2B6D21,
	DirectMemoryFrame_get_Width_m9ADE89E4C2BB5CBCE6B75B15D74629E5C6737C8A,
	DirectMemoryFrame_get_IsTopRowFirst_m1B4D1840079EF37104A9CAE2EC11F536BF0BE7D4,
	DirectMemoryFrame_get_Rotation_m680A6595A5F4C53EC7EF4C0DBC3199A8C204C39A,
	DirectMemoryFrame__ctor_mDA836D7B1ECDB1A91AE7A3CEC999AE800D3DBD4E,
	DirectMemoryFrame_GetIntPtr_m72BDEC37288DF869B61CE646D5837FD2F213AFE9,
	DirectMemoryFrame_GetSize_mD0C86AD7A6FDDB9AA3DBB66DD2C9870E7B928F50,
	DirectMemoryFrame_Dispose_m24586F5732667C18FFF427B184A8D5FD5D8DBA26,
	InternalDataPeer_get_UseUnifiedPlan_mB184F94600810033E5CFF2635ED10A8EB10B5EF7,
	InternalDataPeer_set_UseUnifiedPlan_m7E05874FEAB4D90780351B28E4C5CAC58D906E52,
	InternalDataPeer_get_Factory_mFE5E92CEB8C6539ACB834004B0C8D17C9502E747,
	InternalDataPeer__ctor_m1E5C1B18A3A196E815389B7B07E345201DEC7C51,
	InternalDataPeer_ForwardSetupPeer_mA75A5A711F36017BD2E06F9C5EB502B69CDDF693,
	InternalDataPeer_ForwardSignalingToPeer_mBE72B04B41B990F6040A87CC52B77A48A6BF3B1E,
	InternalDataPeer_ForwardStartSignaling_m5B2C427792EA25994892F843D4915D5072EADEF4,
	InternalDataPeer_ForwardOnDispose_m08DE19431EFD2248ECEC860E824590D2C9D8CBF5,
	InternalDataPeer_Update_mADDFD5CC5FA5EA595AC87F855D8754B07E1C47D3,
	InternalDataPeer_ToMessageDataBuffer_mE5CF0FA0FDFF7E66F02230A81935CCE8A3ACB57F,
	InternalDataPeer_SendData_m4FBB9D74B6829FBE1DE0A9D4C9506D345480A697,
	InternalDataPeer_GetBufferedAmount_m33607EF316D707C36ADCF681CF2232F6DFFE5F99,
	InternalDataPeer__cctor_mEE8932C37130E9D2F34AD56BD7B363E8A2990681,
	InternalMediaPeer_get_RemoteStream_mB43832E3B3690D97C96163F00BB501964BA5E3E4,
	InternalMediaPeer__ctor_mA21693030A6A64FEE00171DEAA7B48E771D769F9,
	InternalMediaPeer_AddLocalStream_mB882F853D1731BF5F4A49815B3B26D749DD0CB19,
	InternalMediaPeer_RemoveLocalStream_m34F0440C600ADD0BF6BF44E59D0C992F1328C5DD,
	InternalMediaPeer_DequeueRemoteAudioFrames_m802098A14E65CFFAD1A705721590A3FE52A31097,
	InternalMediaPeer_HasAudioTrack_m561116393A7AC3C0838DDB7C97D632AB09C19DD9,
	InternalMediaPeer_HasVideoTrack_mCFF24C7DB638FEF2C0E2E59172D7BA2862DD4CD8,
	InternalMediaPeer_SetVolume_m96793890E606DBD931F3D0D70F664CFC9D0FB0B1,
	InternalMediaPeer_TryGetFrame_mB4412C843E1EA39EDEC6173D364DB6B58588718C,
	InternalMediaPeer_FrameToi420Buffer_m8697F6704E6DC8E45F6A8B0B68885108752E9A76,
	InternalMediaPeer_FrameToBuffer_m5B1F8D8F26836FDE1B894EADD23DD3525F2C5BCA,
	InternalMediaStream_GetInternalStream_m77133FB747F5138A2ECE45F5EE3521C10B2444B4,
	InternalMediaStream__ctor_m5B2E2149313544A5DAC2C95D8EF2288C2C37A926,
	InternalMediaStream__ctor_m13D130206AF3B28CDE088E26CFE4D43D9617F6BD,
	InternalMediaStream_Setup_m293F599D5AAC2A15C88827C1913DB89A6049A34B,
	InternalMediaStream_Dispose_mC43341344DFC04AB81131BE8926177988AE8EC57,
	InternalMediaStream_IsMute_mECDA5DED58157BD6081461A414208DC909972B53,
	InternalMediaStream_SetMute_m2E95856FFC501117B919256D22EBBC9B3F0825CC,
	InternalMediaStream_SetVolume_mC6BED91E8F4C31ABEE93EFA0EBFA4EB3F9C041BE,
	InternalMediaStream_TryGetFrame_mF364EBE44537A38572544F243FB05321FFC35C85,
	InternalMediaStream_HasAudioTrack_mC7DAAF0856164657B61F993C5BDA1F0BFC505E78,
	InternalMediaStream_HasVideoTrack_m588FFF938008546B29FB3CDC31EB444D0DCEB416,
	AudioFramesUpdateEventArgs_get_Frame_m5C7ACE499F1E141BE4DD4E364C24391F5E4FD989,
	AudioFramesUpdateEventArgs_get_ConnectionId_mFC1000BD52FDAA9167F6C369CC9248E6A524B4F4,
	AudioFramesUpdateEventArgs_get_TrackId_m087D84CAEC8CBD49DF389EFB858EA5033B03D447,
	AudioFramesUpdateEventArgs_get_IsRemote_m009D30BA9963E32E7A3A49A2C784692D5F105550,
	AudioFramesUpdateEventArgs__ctor_mDD831305263703944899498BC6B4ECFF92DF472C,
	NativeMediaConfig_get_AudioOptions_mD41F74A6FF594BF6FAA13A936DE5BA6CAE4B5E93,
	NativeMediaConfig_set_AudioOptions_m010ACC14B13590A25DF2DD20D7382B0D9225B2DC,
	NativeMediaConfig__ctor_m224DF6CB43453EC7638723BAB89B5C1B750CF9A3,
	NativeMediaConfig__ctor_m5B85A4341EB62D77E7C7BA5D0B84B21ED70891B0,
	NativeMediaConfig_DeepClone_m0A518B9C461A6F1F62F89302E1EC9E53003DDD81,
	NativeMediaConfig_ToString_mF8E6CF6197F824492B366C4F22822FDE95270B9C,
	NativeAudioOptions_get_echo_cancellation_m9E470CD57F3762AAE0125C24D76D431CACB78024,
	NativeAudioOptions_set_echo_cancellation_m7E497F153821B2F2E14055F27452AE07B6A2B2B7,
	NativeAudioOptions_get_extended_filter_aec_mE3AC7DDF2409B738788793796C4A5DEC8477601E,
	NativeAudioOptions_set_extended_filter_aec_mCEFC504064FBFB1AE0B80F6687D514B220AA11B2,
	NativeAudioOptions_get_delay_agnostic_aec_mBF07F2854628EAE68AF532C4293BE75356609D3A,
	NativeAudioOptions_set_delay_agnostic_aec_m992922006A37CEFA18C527E6CD048651B1B67C27,
	NativeAudioOptions_get_noise_suppression_m7AEA2ABAA3872C0CC7A7838CA6FB93CE5867C475,
	NativeAudioOptions_set_noise_suppression_mE5E7466541E3DE460B85857B166A4DDF4865AB9D,
	NativeAudioOptions_get_auto_gain_control_m73BFFF9F9172C79BE5198EDB26DBC30350816AA7,
	NativeAudioOptions_set_auto_gain_control_m9070DE089F540AEA3C8503E907F32492A352EFE2,
	NativeAudioOptions__ctor_mD9995B0B2878DAF2A0EFD5A941BDE277468EAFE4,
	NativeAudioOptions__ctor_m95AE9469B55D9324C5820546A703461C573326DC,
	NativeAudioOptions_DeepClone_mC974B2FD19E96692A10FA06EA23A5C3FC6E37DD3,
	NativeAudioOptions_ToString_m9DA1C89E7BD62C9128DED3FE7DD9A402B7F6A503,
	NativeMediaNetwork__ctor_mAC698CDFC1D3B7B3C3C77880EBAEBD89C34A1FFC,
	NativeMediaNetwork_CreateLocalStream_m57AA1A42117E4773AF72A785507725507CEAF290,
	NativeMediaNetwork_CreateMediaPeer_mD7D31036824832F0056437B5F61F78C063C6F48A,
	NativeMediaNetwork_Dispose_mEE74A270A91995EF6481C1DD3E47A7711D5C4B51,
	NativeMediaNetwork_IsMute_mEA50DC7D1710AF5BBA0030A29240327F992D20BF,
	NativeMediaNetwork_SetMute_m70285B65136E9419630AAEA2F531689B5E933CED,
	NativeVideoInput__ctor_mC306DBDF7284880707A267278558885308079B6A,
	NativeVideoInput_AddDevice_m22BB86D16CE3D74822F56668F58F2E352A7941C2,
	NativeVideoInput_RemoveDevice_m066C258D2BDE4DED57A6DECC0C99BB2C8C404D83,
	NativeVideoInput_UpdateFrame_mD6BC1855ABC07BF19401071A5483920BD355F18F,
	NativeVideoInput_UpdateFrame_mAE48E07F938D57DB9E17F578B92BB2D3AF9E4DD2,
	NativeVideoInput_Dispose_mF71BD96D39667DF64FC5DE0F0332F639ADF9ABD4,
	NativeWebRtcCall__ctor_m6A0D5B34D59737A268FB65CB3D4A4538F20F0300,
	NativeWebRtcCall_CreateNetwork_mB7FFD769DDDADBA98DE5952C4632DB5E01274791,
	NativeWebRtcCall_Dispose_m60B46E538027792C0F28F5057650315F2985D25D,
	NativeAwrtcFactory_get_IsInitialized_mCC4DE63D0B7C2C44083ED20909F46F5A88605BC9,
	NativeAwrtcFactory_get_NativeFactory_m02F4717D097D433CCF8ECB58908C4CE46C76E02E,
	NativeAwrtcFactory_add_LastCallDisposed_mC5736E1422112E83EA3C151F543754DC7C122FA5,
	NativeAwrtcFactory_remove_LastCallDisposed_mF10A6AAE36C257A0735933474304958EC79FAD00,
	NativeAwrtcFactory_get_VideoInput_m765970E2E772420F225F97A4F5077739E5F9D2BF,
	NativeAwrtcFactory__ctor_m0A8E24DBD64DE3913976C7E12160B43B2039FD62,
	NativeAwrtcFactory_Initialize_mF82645E5FB32638A7A81AE27AFE71739EB19B5D9,
	NativeAwrtcFactory_TryStaticInit_m922B8A8CD830EA1C7DEE8EE3BEDB54532793A6B8,
	NativeAwrtcFactory_OnNativeLog_mC4EA544FC288667D0739863152F3C2FFFE6325D8,
	NativeAwrtcFactory_SetNativeLogToSLog_m1B8F12E0DABFCCC78CFE83F33A79CB2C80B5866C,
	NativeAwrtcFactory_SetNativeLogLevel_m5BC71422EBB8B2CA251A3B7D81695359D5BD3821,
	NativeAwrtcFactory_SetNativeLogLevelInternal_mB9BF494730D3936BCE5EEAE9AD564E8297D6E9A6,
	NativeAwrtcFactory_GetVideoDevices_mBA1A2545BAF6675ED4F672107D861C7E2204A849,
	NativeAwrtcFactory_CreateCall_m6EFAB450F5FA00219A7FD59963A00393092AC4C8,
	NativeAwrtcFactory_CreateCall_m57F3DDD3AE997CAF3B023495C17C6548FA58A9B9,
	NativeAwrtcFactory_CreateMediaNetwork_m9664DD530CA03C9C6171E6049AC8A8E8B44BFA7F,
	NativeAwrtcFactory_CreateBasicNetwork_m7917BC319757ED71D398FA408A2FF001CB2ED148,
	NativeAwrtcFactory_CreateBasicNetwork_mD68D75ADF4A5F256C44C475D387E589BCFD5DA00,
	NativeAwrtcFactory_CreateMediaNetwork_m2DF5F9091264946D33A832F13B318B15513563DC,
	NativeAwrtcFactory_CreatePeer_mAFB668E743B38F25452941968765CA5983BF0F07,
	NativeAwrtcFactory_CreateStream_m43ACA3EC59C0254B14DC33DDA032AEB923A0B610,
	NativeAwrtcFactory_OnCallDisposed_mD3FF24C7651358DD989A41B831FFCB8E091B44E0,
	NativeAwrtcFactory_OnNetworkDisposed_mCD6A6B6FAC2D337F6D546FE0E5991DD66AFEE1F9,
	NativeAwrtcFactory_OnNetworkDestroyed_m5E4A6F5ACAF109A406AA71BA6B3CE2B4B828E4E8,
	NativeAwrtcFactory_Dispose_m5B7FE0B7726CE047797BE156FDE4A6B180B44FDF,
	NativeAwrtcFactory_Dispose_mFD278D7BA01BF4E2A4DC816106FFE9BC94CAE527,
	NativeAwrtcFactory_CanSelectVideoDevice_mA05CDFC7FEC1F3E6F598C3D7CCF9393705BCF69D,
	NativeAwrtcFactory_SetGlobalSpeakerMute_mC712CA3638FAFB0A8CEFA2B4D9B4B341A6A901AA,
	NativeAwrtcFactory_GetGlobalSpeakerMute_m813FD92F9AA518C87E7C25C27F8F7D98CB301CD1,
	NativeAwrtcFactory_HasGlobalSpeakerMute_mA22789FCB5422367DE2CAFB62D6C40BCD4807998,
	NativeAwrtcFactory_GetVersion_m06B9809CBADF5AAA909E292F4668C7FC4BCD659B,
	NativeAwrtcFactory_GetWrapperVersion_mC174E74A4DE01C7AE409306574BBE96FF7E08A85,
	NativeAwrtcFactory_GetWebRtcVersion_m31AEBCC7C507045BB35454DD26C9B15747D6D337,
	NativeAwrtcFactory__cctor_m918E9117E8C10BF5AD5EE8E06FAE975EEC9A1837,
	NativeWebRtcCallFactory__ctor_m491560E86C777C93191B1A131AEBCA980EF96331,
	NativeWebRtcNetwork_get_Factory_m1DFE2DD1000AB837D3CA9AFCD2BC0EF8145FD67B,
	NativeWebRtcNetwork__ctor_m6569408556F18611155C0944CFA03E838ABF8538,
	NativeWebRtcNetwork_TestValidity_m4F298D3DD78F50CA4658215931B5CAB7F5E11956,
	NativeWebRtcNetwork_CreatePeer_mC3F9429598142D73635CEED1B16478BEF51AB576,
	NativeWebRtcNetwork_Dispose_mE5494993BCB0CEA74540EE60CC2689C0D1B0F0F8,
	NativeWebRtcNetworkFactory__ctor_mEB56C87CDB1441E4E5E7566D0838638F1212DC10,
	WrapperConverter_ConfigToConstraints_m4C9239BDD0EFFFED055E3979C37B290B6D8DFD29,
	WrapperConverter_ConfigToAudioOptions_mB32D4EE3C87450063BC1D3506D0D71BB6BE76CAF,
	WebsocketSharpClient_get_ReadyState_m1C42EBA5B4C7305D6822EB1E4514E0576F68A91D,
	WebsocketSharpClient_get_CertCallback_m3A72A88D8C98CC01DC4A3337E746E230D2F5EEA5,
	WebsocketSharpClient_set_CertCallback_m6B44F914675BFF0C01904BE82842E9460180301F,
	WebsocketSharpClient_add_OnOpen_m67DBA342592F3E628B94E31CD61168822D5063FF,
	WebsocketSharpClient_remove_OnOpen_m7EAF580080F0C84804CDD5361C9C4E2F76168759,
	WebsocketSharpClient_add_OnClose_m2463E7D42D6EBFADE7E5B24F3FCB43853E0B6920,
	WebsocketSharpClient_remove_OnClose_mCA1DA66B29FE023FF5585E9ACA105BD72429C0E9,
	WebsocketSharpClient_add_OnError_m970F166055A8162B45AAA7DB3C776D30C1410647,
	WebsocketSharpClient_remove_OnError_mF240C59B8A75D8C8940E00CFE931E805A34DF23E,
	WebsocketSharpClient_add_OnMessage_m2B8CEB9151BEE21F5C349D7241984D01668591C0,
	WebsocketSharpClient_remove_OnMessage_m47C7CB357A93433029157B2B780C9928FE148A56,
	WebsocketSharpClient_add_OnTextMessage_mDAE6A59098E8C68BF7213FB56F8C543220F1F67F,
	WebsocketSharpClient_remove_OnTextMessage_m83FA0E4EC25C577BE7C7C50AC1A866BE0C1A2DBF,
	WebsocketSharpClient__ctor_m4C7A402886B32175DF7D219CACA261942BAE65C8,
	WebsocketSharpClient_Connect_m3241467D054C9F5E1F8013D0D5BEFD9230DCD125,
	WebsocketSharpClient_IsAlive_m6003C15A6D14E3C1822181AA5376D26F47E21F8F,
	WebsocketSharpClient_Send_m07E8F072ECBD3C8EC3FAF7B78CDEF0FAD441A7A7,
	WebsocketSharpClient_Send_m611ECA2FF77A93980F778463217727CC1A2CF57E,
	WebsocketSharpClient_OnWebsocketOnOpen_m21529C17F81EFFBDFF412D78441295A486673A28,
	WebsocketSharpClient_OnWebsocketOnClose_mF12A7D749D647C071B9F1F7C5C3856C0095634E6,
	WebsocketSharpClient_OnWebsocketOnMessage_mD0CC276AC7BD43FA961F5E34E7BF3334C6656FB9,
	WebsocketSharpClient_OnWebsocketOnError_m91964AF587F12AC27B7C0524E2264C29B65061B6,
	WebsocketSharpClient_Dispose_m3B14EB27F6A18E9BF32656D0664B2C92F71FB926,
	WebsocketSharpClient_Dispose_m00B2C2E56FC147C17F7CEF691E804231282DC890,
	WebsocketSharpFactory_get_CertCallbackOverride_mCA4669F27C8230EB8D543E33286FC4F16B54BA51,
	WebsocketSharpFactory_set_CertCallbackOverride_m8BC5F1AF0AD74BCEE8B7D341FB82F37DCFF75DEF,
	WebsocketSharpFactory__ctor_m17EC9CE2605BE0B42F532DD168AFB9A94116FA5E,
	WebsocketSharpFactory_DefaultCertCallback_m896A4C674BEFB565BC6D3BA0121AA6189E09C51B,
	WebsocketSharpFactory_Create_m27E349E36C1EE6EF166FE2E807A70BE8EA0E0027,
	WebsocketSharpFactory__cctor_m6F55E1E68E9D9A93EE6D70C6EEBF0E2BFBA4FE92,
	AMediaNetwork__ctor_mE3DB6701E59C1664EC9EF1072787A36D51575D75,
	NULL,
	AMediaNetwork_Configure_m4F5A5FF5A871C96C82FF58E4D01D76B29BB4258E,
	AMediaNetwork_GetConnectedPeer_m021DB4A0F024B64C11E340F717A50C45EE3B35F9,
	AMediaNetwork_OnLocalStreamAdded_m0FA573AF57F769832AA7BC5DD2ABE0DF953A54E4,
	AMediaNetwork_AddLocalStreamTo_mD170706D2E9705F230E90203DB573A8A01097345,
	AMediaNetwork_OnLocalStreamRemoval_m28371EFBAC94CAAF8CF89EF4F97D77BEF46EDC05,
	AMediaNetwork_GetConfigurationState_mE50BA0F8C0541E47BFDB74380B6C9679DB31FB59,
	AMediaNetwork_GetConfigurationError_m3D3991C43E3558A9C01A856FEBAE0DF9AF5A174B,
	AMediaNetwork_ResetConfiguration_mDD6AB011A63501A886850A979EE9087A0AC4EBB6,
	AMediaNetwork_CreatePeer_m32B4A257502C8EDFFFC223C74622977D0AC0921D,
	NULL,
	AMediaNetwork_TryGetFrame_mB05650155E20A3D4441320C800EC8CC83075BFD1,
	AMediaNetwork_SetVolume_m4F07DFFAF7ECF5DF6E2A08C57A7A42CE3CDBCAA9,
	AMediaNetwork_Dispose_mA57660044DE2EE0A8FADA8C961BF722C7E2A22A5,
	AMediaNetwork_DisposeLocalStream_mB210696276C4C3F37EADBCF8190CC59307B02D8B,
	AMediaNetwork_HasAudioTrack_mFCC947A7BCFE6212A34BBB66AC3D95474B5A014F,
	AMediaNetwork_HasVideoTrack_m7D6B421BFE9F8AAD89E395D7A2CF7175690D18F6,
	NULL,
	NULL,
	AWebRtcNetwork_get_IdToConnection_m0F4B8C74AF6BB0FFF2E515B96A50CE8F51D8215E,
	AWebRtcNetwork__ctor_mFDD3128A9E69602969B47520073DC3DDA082971E,
	NULL,
	AWebRtcNetwork_StartServer_m8AAD130FE1030F44631689329656FA5AAEB0C743,
	AWebRtcNetwork_StopServer_mCA13E334EE4ED4FCB859AF25F311CFB422B74231,
	AWebRtcNetwork_Connect_mCA1BE2627E7682820B0CB227ABE0E229EF284D90,
	AWebRtcNetwork_Dequeue_mF5A7C7F7BEFB6A728E67EB3141B5EA1F2FD18697,
	AWebRtcNetwork_Peek_m2F6026F57EA84BDFA0D7242CB2DF3AD285C380DD,
	AWebRtcNetwork_Flush_mA9C005AC91E39542A297BE4C2F1BED6B1D7F88E4,
	AWebRtcNetwork_SendData_m91FD5312B0B78194EAFAB4B0AB9A6462ABC26FEB,
	AWebRtcNetwork_GetBufferedAmount_m3941687E84D2CCB801D82D82FF79298A1E9DE8DB,
	AWebRtcNetwork_Disconnect_m69A70320C9F651CBED9C6C1D5026294CEEC2FE98,
	AWebRtcNetwork_Update_m81B7277F82C2DC1A8B4069945F1506E23D9C2230,
	AWebRtcNetwork_Shutdown_m369134B6672D6C6333787CD62FC0A93DD6A00AAE,
	AWebRtcNetwork_CheckSignalingState_m5DDDADE115AD76A3EF2F033FDFEC0D75B756F312,
	AWebRtcNetwork_UpdateSignalingNetwork_m7FEC3CE094968ACE061C0C8904332BFE004CEB51,
	AWebRtcNetwork_UpdatePeers_m2AF129713B3B9515260F8FDB19771D1724F6A789,
	AWebRtcNetwork_AddOutgoingConnection_m125E5B7821D55E756BEBEC18E791DF8F7B18996C,
	AWebRtcNetwork_AddIncomingConnection_m4EEF391C2D527F58527589DE6E3B0554A04B2CE3,
	AWebRtcNetwork_ConnectionEtablished_m772CF38C7E3F382E90E0CD342AD116A08D95F2F2,
	AWebRtcNetwork_InitialSignalingFailed_m39E0F4F83F6B992E0CE63BC39065D90052D4E12E,
	AWebRtcNetwork_HandleDisconnect_m0CC74DD6CB55D88D2AEEA9390348AF99B535858B,
	AWebRtcNetwork_StringToBuffer_m44D5233C0DACB5A5538A1495585F0E8AFBC6B24A,
	AWebRtcNetwork_BufferToString_m6E136EB6627B2F33971F2FCC939F6B5065AFC43E,
	AWebRtcNetwork_NextConnectionId_m4734DE346AB2D3D4DCF02F5337860459DD40AEDF,
	AWebRtcNetwork_Dispose_m6A7B89521EAA47B197DFB0DCF3FC1566B27E14FC,
	AWebRtcNetwork_Dispose_m2C45A5FA264C30F006DDF95E4D56CF3DAF9D7D8F,
	AWebRtcNetwork__cctor_mB428E57E09617F6172AE68523D3EE90248A33B62,
	AWebRtcPeer_NextLocalId_m322FE37335D513C739CB877E33A75343F17CBBAE,
	AWebRtcPeer_get_LocalId_m104E28E10F6675B95B2BAC6C3CC6B8ADB66C65A6,
	AWebRtcPeer_get_ConnectionId_m8210DCE237989466AC07CBCCC59AE566D4E0E89C,
	AWebRtcPeer_get_SignalingInfo_mA4C0FEB40D5E506DC5473A66D7D4B61385FD5D3F,
	AWebRtcPeer_set_SignalingInfo_m0EA934D87B954729EA0CCF2A9FFD2FCA47E45196,
	AWebRtcPeer_get_ConnectionState_m98868F73EE70FF87F3820FB3D95AFFBED75E3E3F,
	AWebRtcPeer_get_SignalingState_m3C1D14B5A54150659ECD60BACEC953603F1866CC,
	AWebRtcPeer_get_WasRenegotiationRequested_mB76F42CE91F2B6A50F75E8F43F7525649B2BE19C,
	AWebRtcPeer_NegotiateSignaling_mC3853F146C19CE2C18FFA350E0A2CADAA51198F2,
	AWebRtcPeer_EnqueueOutgoingSignaling_m7B9A8766C695E026B48E88B52EE32EE181287D87,
	NULL,
	AWebRtcPeer_UpdateConnectionState_m2BE880DE2E992813841599FBC52E4FC9A523A6AE,
	AWebRtcPeer_UpdateSignalingState_mD0EEF1F42A8B3DAE659633293E5F946B644E7657,
	AWebRtcPeer_IsNumber_mC64282E9A1142BC8612257E339B32E56E25929F1,
	AWebRtcPeer_HandleSignalingNegotiation_m480BC576E36D5F09FEB49873BF0F3BB3CA0DBD23,
	AWebRtcPeer__ctor_mF15523F56098A03F061F20F5688A88CCA95CFA10,
	AWebRtcPeer_SetupPeer_m423502843F06A7810E8F2C0D58ACF84CAAC4821B,
	NULL,
	AWebRtcPeer_AddSignalingMessage_m1E2CF5E6D2C5839F95B3D41F0612DC2E75D16AE6,
	NULL,
	AWebRtcPeer_DequeueSignalingMessage_m5CCD21B214196CF917EC59569ACDA471ADF5EA38,
	AWebRtcPeer_DequeueEvent_m512EB58CB7B447132AF4D5FB877749A50D2D5CC3,
	NULL,
	AWebRtcPeer_EnqueueOutgoingEvent_m7CDD57FC6F72E9CD62A666054A4DD332907E342D,
	AWebRtcPeer_StartSignaling_m11918234476E21A24155D5C6FE576106A737D418,
	NULL,
	AWebRtcPeer_L_m617158CFA53586F62B399D6180078483C3A534EC,
	AWebRtcPeer_LW_m9458EF8EEC312E1F8C8CF3674857F96DFC43CA4D,
	AWebRtcPeer_LE_mA30ED2AFB54F131BCEABC7DB0A74BC374B76E518,
	AWebRtcPeer_get_IsDisposed_m329B97F59A0590AB58825ED34379F518D9D0D591,
	AWebRtcPeer_Dispose_m5ADD326B9A1FC944C29E1DDC77A82E02139337CF,
	AWebRtcPeer__cctor_mAC6B072AA0109742C99C287EEBB697262D5A27C9,
	WebsocketCloseStatus__ctor_m989E8708B7A205B5C5642B182CC7FD9116820F64,
	WebsocketCloseStatus__cctor_mD394A4F3EA4B8EA454FE750BA235C8AB19FDCB84,
	AWebsocketFactory_SetDefault_m0415E34A5CD240727748863E30B4EA9F26FE8FE7,
	AWebsocketFactory_get_DefaultFactory_m92D8AB3F471C649972DD30B8CCCFECA984945161,
	AWebsocketFactory_CreateDefault_m704CE68BF9F70F446EB2DF32BAC7C37178286272,
	NULL,
	AWebsocketFactory_Dispose_m71FC40E6903249B93FE521BE023B05EB537DC17A,
	AWebsocketFactory_Dispose_m3EC5E2A0EF4A9F39FA56D649681AB8EE12407461,
	AWebsocketFactory__ctor_m457213DA9AEC799C397E551C9BBCD85BA8C6DA5C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OnErrorCallback__ctor_m721B76328F8C6B3428C7B72E96F2E1E7E7FDAF76,
	OnErrorCallback_Invoke_m17E6585C7FA1B54FD6CE391662B876CE3F225A3B,
	OnErrorCallback_BeginInvoke_mD2D4D9E3D74A0D5F12509F02529225F1221F85BF,
	OnErrorCallback_EndInvoke_m668D63C857AAA2914DB027188A7B29A0D3D69BE4,
	OnMessageCallback__ctor_m61D86E85C46EF79031010E44F3CA23A2B9BF9CBB,
	OnMessageCallback_Invoke_m1E6773FB0F0D522913A4B916099ADCA67E12CCAD,
	OnMessageCallback_BeginInvoke_mAD8702E81C00832DFF0C9873C2E9968CE4EB6F89,
	OnMessageCallback_EndInvoke_m8D80B3734BD34C1A30C041ABEF3EC765F3353370,
	OnTextMessageCallback__ctor_m97D4EC2A7B040AFF9FFBE77129F9391D2370AF8D,
	OnTextMessageCallback_Invoke_mC5CD2BD5C1AA8176E078EDBE6B04465E1D0D0674,
	OnTextMessageCallback_BeginInvoke_mF84C5761C1433EFDA549CE5C726F06B24D7BD70A,
	OnTextMessageCallback_EndInvoke_mAFB2626BF467C4462A06636E3F32CE65361D3C0A,
	OnOpenCallback__ctor_mC7C8398782F1F74DA195C20B5FAA45C37E5B4288,
	OnOpenCallback_Invoke_mAE7B0889E5CBD76DDDDD9C1D6697D43664557177,
	OnOpenCallback_BeginInvoke_m3BE351D86140977DD0EA577D6965BAE018688FBF,
	OnOpenCallback_EndInvoke_mE6A598A2D56A422E51761C26EE07F901EC4AAEA9,
	OnCloseCallback__ctor_m07C64DB7B7EF3891395666A0EB52B67C24F6CAD4,
	OnCloseCallback_Invoke_m62CB0E9CA8F7289A87A8485087C851BCEF298F45,
	OnCloseCallback_BeginInvoke_m3F590762D7800B8ABE9DE7FA7AB722B692287DD3,
	OnCloseCallback_EndInvoke_m830A38C9B326A4056CBE18DA25524803FDB5DDC6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SignalingConfig__ctor_mD54AE46BAFEC4FA608A9201919154014DAFE0ECD,
	SignalingConfig_get_Network_m6FDA04DE134B03B946A4A6B11B41ED68A713D1CD,
	SignalingConfig_get_KeepSignalingAlive_mBD8CABE2BEA5F9771DDE3BD7C440B808C283F441,
	SignalingConfig_set_KeepSignalingAlive_m7D9EBB0FFC2DBABC42535A5FB17F393982167ED6,
	SignalingConfig_get_SignalingTimeout_m5E512ED6DF4DA42027F8D2914FEB435048F40D21,
	SignalingConfig_set_SignalingTimeout_mEB9680E4EF910A83B9BE721041F4F3D366CF4560,
	SignalingConfig_Lock_mA5FD586A380F056C44A569ADEF8E7EFA81D0DF7B,
	SignalingInfo_get_IsSignalingConnected_m2B22DBBAF55BFBEF557EB7D5BDFA7A564128FC66,
	SignalingInfo_get_ConnectionId_mB826E196B26655A8A8166D6FD30BDB5DF2854AB4,
	SignalingInfo_get_IsIncoming_m55535798EF8E4393229CBDE59BD30EC06780CC0B,
	SignalingInfo_get_SignalingDuration_m10A9D07ADED8BEC9B396511FB1A8EAC6F29864BD,
	SignalingInfo__ctor_m838555F15B35A377A43FD0B69F5D3384898BD20D,
	SignalingInfo_SetSignalingStart_m299A21A02DFBCA75F27A91193AD945C3D1A36470,
	SignalingInfo_SignalingDisconnected_m33FBFDD5D8FF83A74E3FE151DBBA40F546886E69,
	WebsocketNetwork_get_Status_m10CDA64CF347494FF78B91373BEFDA79BAA137BD,
	WebsocketNetwork__ctor_m92F50B981A17117766315EA18362F494A1444E82,
	WebsocketNetwork_WebsocketConnect_m27F1958923FD2B55B49032C51BA0159EA4CA3B14,
	WebsocketNetwork_WebsocketCleanup_m4F2F9E56529FB1B44C8B4BC5BEEB4FDAD4E612EC,
	WebsocketNetwork_EnsureServerConnection_mE9B7283AC789019CECAC8BBA2966D8A290A08B53,
	WebsocketNetwork_UpdateHeartbeat_m113A62CB24B97E08663DEF1B89ADE4BD989AA077,
	WebsocketNetwork_TriggerHeartbeatTimeout_m54536FF5AA647FA38F3CF59BD831BFF3133E7F68,
	WebsocketNetwork_CheckSleep_m36F58F4AE704930EC77468E24E8760701FDE8F33,
	WebsocketNetwork_OnWebsocketOnOpen_m771D1D7988D738F389B7D9BDEF82861B9D922AD2,
	WebsocketNetwork_OnWebsocketOnClose_m756A66448C681E76840AB5B05FF6D526D38EC538,
	WebsocketNetwork_OnWebsocketOnError_m416D364C7C37A5DC085606BD2101622DB38FA72D,
	WebsocketNetwork_OnWebsocketOnMessage_m2BF7C325F9C75537E699DDDE002907467D16FE91,
	WebsocketNetwork_Cleanup_m85BAF7B3BAC38CE69A71DBD8D7660CBEC55742C0,
	WebsocketNetwork_EnqueueOutgoing_m574F63627CBCACE91AC478F46DE77E2AB3EB9994,
	WebsocketNetwork_EnqueueIncoming_mC890897DEAEC574FD0DF956504D33608AAA3AAF7,
	WebsocketNetwork_TryRemoveConnecting_m0CDF5E4BD1B6658F7F4C471E48F723AB67CD7A47,
	WebsocketNetwork_TryRemoveConnection_m02F5DF133FFF607845A9FA37BA59B0EC144FC939,
	WebsocketNetwork_ParseMessage_mF566EB12FF2422B4B12D690BAD7AF84C00FD2491,
	WebsocketNetwork_MessageToLog_m8E4065ABA559C51CAEB041E7B25F0FE804ACDD1F,
	WebsocketNetwork_HandleIncomingEvent_m412DA3E0A5AEC66E019C2706918E8F7E44082EF2,
	WebsocketNetwork_HandleOutgoingEvents_mF35318BBDE24FF8858C66264CD5F0A724CAC7CF0,
	WebsocketNetwork_SendHeartbeat_mB3E19DFF0950C020FC8C2E96507F233DD28769C3,
	WebsocketNetwork_SendVersion_mAD873D1B59027DB2EE1511614141F528289E8D66,
	WebsocketNetwork_SendNetworkEvent_m7D4B74D3047F53B26202F4BF369012444F090745,
	WebsocketNetwork_InternalSend_m2307DE3CCF6D59057A8EF4E47CB781146352883F,
	WebsocketNetwork_NextConnectionId_m241C86F746F560FC0F43C7331E6B6AD6BF240F70,
	WebsocketNetwork_GetRandomKey_m5FDEAB10DB1A26229F979C06FE4AD7FD83DC2EBA,
	WebsocketNetwork_Dequeue_m8A175860F5BBF5E23E581DE01E7ED83D62C307A4,
	WebsocketNetwork_Peek_mFF555DD7A156E83D4B58DAF0BF430C7F7B40C700,
	WebsocketNetwork_Update_m259BCAFBA8A03F5EBDB14E16F795A20653848100,
	WebsocketNetwork_Flush_m1875231AB0CFFACDA1AF6EBBDD9886A8E4ACFBAF,
	WebsocketNetwork_SendData_mE04B2B1082EED96828492D7925E7DFA8F870A45B,
	WebsocketNetwork_Disconnect_m3CC073A01F3980DEA308EBAB2EDD42263FFAC60B,
	WebsocketNetwork_Shutdown_mC2681353AB00DB9E6FECAF8C7B30AD3EE8F67767,
	WebsocketNetwork_StartServer_m2A759D83DD9AF600E0470419720C2C298A3152EB,
	WebsocketNetwork_StopServer_m7B9F364CAD9382DD3AB2A14FB19C031C7559A7B9,
	WebsocketNetwork_Connect_mB8780B7952D6AA57EDE7FC99CA2FD5B5BE501E14,
	WebsocketNetwork_Dispose_m83752BFE72224F6F92E455C4BF75CCD3301DBC85,
	WebsocketNetwork_Dispose_m893D76FA94494FE708972D5396A29146E1F6E3E5,
	WebsocketNetwork__cctor_m6A8A676032C70BAC73E204E06B6E3C624BF9F70F,
	Configuration_get_Heartbeat_mC79FD1FFF4AFE9DD17B588AB554814CC91FB4A03,
	Configuration_set_Heartbeat_m7B7737F797DEA602FDD13305FA167DEE8C17293B,
	Configuration_Lock_m9193632A1D6D6F90A6B71CAC3B70D00E911B2CE3,
	Configuration__ctor_m78870C2FCE611EDCE80DD946202F948F0FE2C38D,
};
static const int32_t s_InvokerIndices[1983] = 
{
	9442,
	7716,
	7716,
	7597,
	4456,
	4456,
	9289,
	9421,
	9289,
	9161,
	7469,
	7597,
	4368,
	9289,
	9289,
	13922,
	13922,
	13922,
	13922,
	13922,
	13922,
	13302,
	12894,
	11112,
	10423,
	13922,
	13922,
	13922,
	13935,
	12524,
	13259,
	13266,
	12854,
	12854,
	12854,
	0,
	13600,
	0,
	12195,
	10641,
	12854,
	12854,
	12854,
	11516,
	13922,
	13936,
	12874,
	12874,
	11818,
	10391,
	12893,
	12899,
	12474,
	13587,
	13587,
	13587,
	13587,
	13595,
	13611,
	13611,
	13587,
	13600,
	13600,
	13922,
	12862,
	11805,
	11332,
	10629,
	12869,
	0,
	12869,
	13922,
	13600,
	12825,
	13922,
	0,
	13009,
	13031,
	13922,
	11529,
	13922,
	13922,
	13922,
	12195,
	10641,
	12478,
	12478,
	11547,
	12197,
	0,
	12854,
	13914,
	13914,
	13611,
	12480,
	13595,
	13600,
	13600,
	13600,
	12478,
	13600,
	0,
	0,
	13183,
	13190,
	13296,
	13302,
	13183,
	13190,
	13296,
	13302,
	0,
	0,
	12862,
	0,
	13922,
	13922,
	13922,
	13266,
	14502,
	14502,
	9442,
	5478,
	9442,
	5439,
	9442,
	7597,
	9442,
	7597,
	9442,
	7558,
	9442,
	7597,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	9289,
	9289,
	9442,
	9442,
	7597,
	0,
	0,
	4368,
	9289,
	9289,
	9289,
	12869,
	12862,
	0,
	9289,
	9442,
	7557,
	0,
	0,
	1717,
	4368,
	9289,
	9289,
	9289,
	9161,
	9289,
	13922,
	13922,
	3459,
	13922,
	12862,
	7597,
	9289,
	1717,
	7557,
	4037,
	9289,
	9161,
	9161,
	9161,
	9161,
	9161,
	9289,
	9289,
	13914,
	13922,
	14458,
	13922,
	12862,
	7597,
	9289,
	2319,
	9289,
	9184,
	9247,
	9289,
	9289,
	9442,
	7557,
	2319,
	9289,
	7597,
	9247,
	7557,
	9289,
	7597,
	13266,
	4359,
	13266,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	3664,
	9289,
	9161,
	9161,
	9161,
	9289,
	9161,
	14502,
	9442,
	7597,
	4360,
	9248,
	7558,
	9161,
	9289,
	9289,
	9423,
	7597,
	9289,
	9289,
	9289,
	9289,
	7557,
	9442,
	9161,
	9161,
	9442,
	9289,
	14502,
	4368,
	4368,
	4368,
	9289,
	9289,
	7597,
	9161,
	9161,
	7469,
	9161,
	9161,
	7469,
	9289,
	9289,
	9161,
	7469,
	9161,
	7469,
	9289,
	9161,
	9161,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9421,
	9289,
	7597,
	9289,
	9404,
	7702,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	9161,
	9161,
	2752,
	2752,
	442,
	167,
	2752,
	1626,
	1626,
	1856,
	9161,
	9289,
	6739,
	9289,
	9289,
	2752,
	9289,
	9161,
	7597,
	4368,
	4368,
	4382,
	9442,
	9442,
	7597,
	7597,
	9442,
	5478,
	7597,
	5478,
	5478,
	5478,
	5478,
	5478,
	7597,
	7597,
	7597,
	5478,
	9442,
	9442,
	9442,
	9442,
	5478,
	2646,
	1800,
	905,
	2251,
	5478,
	9289,
	3459,
	5478,
	9442,
	9442,
	9442,
	5439,
	9442,
	5478,
	5478,
	5478,
	5478,
	5478,
	5478,
	5478,
	5478,
	11835,
	11835,
	12853,
	13922,
	13922,
	13922,
	12862,
	7597,
	7557,
	2414,
	14458,
	13922,
	9442,
	2768,
	2251,
	2251,
	9442,
	9442,
	9442,
	7716,
	7716,
	4456,
	4456,
	9442,
	7716,
	7716,
	4456,
	4456,
	9442,
	9442,
	9161,
	5478,
	7597,
	7597,
	7597,
	4368,
	4368,
	4368,
	2390,
	7597,
	2414,
	2423,
	9442,
	7597,
	5478,
	7557,
	9442,
	9161,
	9442,
	9442,
	9289,
	9442,
	9289,
	9289,
	9289,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	14502,
	9442,
	7597,
	9442,
	9442,
	7597,
	7597,
	9442,
	5478,
	9442,
	5478,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	7597,
	9442,
	7597,
	7597,
	7716,
	4368,
	4456,
	4456,
	2471,
	9421,
	14502,
	9442,
	2250,
	741,
	741,
	9247,
	9423,
	9289,
	9161,
	9161,
	9161,
	9161,
	9161,
	9161,
	9161,
	9161,
	9161,
	9161,
	9161,
	9161,
	9161,
	9423,
	9161,
	9289,
	9161,
	9289,
	9161,
	9161,
	9161,
	9161,
	14458,
	13922,
	13922,
	13922,
	12869,
	11353,
	13922,
	12197,
	12869,
	11353,
	12869,
	11353,
	12854,
	13898,
	12854,
	12854,
	11321,
	9442,
	9289,
	7469,
	6723,
	9289,
	9289,
	9289,
	9442,
	9289,
	9442,
	1717,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	7597,
	7597,
	7597,
	7557,
	9442,
	9161,
	9161,
	9442,
	9289,
	7597,
	9289,
	9289,
	9442,
	7557,
	7597,
	3947,
	4359,
	2380,
	9289,
	9247,
	7557,
	9161,
	9161,
	9161,
	7469,
	9289,
	9247,
	9289,
	7597,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9404,
	7702,
	9289,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	9442,
	9289,
	1705,
	7597,
	7597,
	9442,
	9442,
	7557,
	11529,
	0,
	0,
	6745,
	5478,
	9442,
	9442,
	4456,
	4456,
	9442,
	7597,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9442,
	9289,
	9289,
	9289,
	9289,
	7597,
	9161,
	7469,
	9289,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9184,
	9421,
	6745,
	4368,
	4368,
	4368,
	4368,
	4368,
	4368,
	7597,
	7597,
	7597,
	9442,
	7597,
	7597,
	7597,
	4368,
	4368,
	4368,
	2390,
	14502,
	9442,
	7557,
	7597,
	3947,
	4359,
	2380,
	9289,
	9247,
	7557,
	9161,
	9161,
	9161,
	7469,
	9289,
	9247,
	9289,
	7597,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9404,
	7702,
	9289,
	9442,
	9289,
	9289,
	1705,
	7597,
	9442,
	9442,
	7557,
	11529,
	0,
	0,
	5478,
	9442,
	9442,
	4456,
	4456,
	9442,
	7597,
	0,
	0,
	0,
	9442,
	9247,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9442,
	7597,
	4456,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9442,
	7597,
	9247,
	9289,
	6745,
	9161,
	7469,
	9289,
	9247,
	9404,
	7702,
	2251,
	2251,
	2251,
	2251,
	3465,
	0,
	2752,
	5478,
	9442,
	2355,
	7597,
	7597,
	4368,
	4368,
	2390,
	9289,
	6745,
	2752,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	7597,
	9442,
	7597,
	9247,
	9289,
	9247,
	9289,
	9289,
	6745,
	9161,
	7469,
	9289,
	9404,
	7702,
	2251,
	2251,
	2251,
	2251,
	14458,
	7501,
	2752,
	6745,
	2251,
	2251,
	3465,
	5478,
	9442,
	2414,
	7597,
	7597,
	4368,
	4368,
	2390,
	9289,
	6745,
	7597,
	2443,
	2443,
	5478,
	2761,
	4368,
	4368,
	2423,
	2423,
	1681,
	9442,
	2752,
	4368,
	7557,
	9442,
	9161,
	9442,
	9289,
	9442,
	9289,
	9289,
	9289,
	7557,
	9442,
	9161,
	9442,
	9289,
	9442,
	9289,
	9289,
	9289,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	7597,
	4037,
	9289,
	9289,
	9289,
	9289,
	9289,
	9247,
	14458,
	13922,
	0,
	0,
	9289,
	4037,
	4037,
	9289,
	9289,
	13922,
	13922,
	13922,
	9289,
	9289,
	4037,
	7597,
	2428,
	1600,
	9422,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	11811,
	10409,
	12869,
	11811,
	13922,
	9442,
	13922,
	13922,
	13922,
	9289,
	9289,
	9289,
	14502,
	9442,
	5478,
	7597,
	9247,
	1983,
	848,
	9289,
	7597,
	7597,
	695,
	9442,
	6353,
	1983,
	7597,
	1676,
	9289,
	9247,
	9161,
	1983,
	13922,
	1976,
	1976,
	1976,
	14256,
	2351,
	1976,
	9442,
	1218,
	1983,
	2387,
	7597,
	1705,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	14502,
	9442,
	4368,
	2423,
	1717,
	9161,
	7469,
	9247,
	9289,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9289,
	7597,
	9161,
	7469,
	9184,
	7496,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9184,
	9289,
	7597,
	9247,
	7557,
	12473,
	12473,
	10168,
	9289,
	9289,
	11529,
	6745,
	9289,
	5478,
	9247,
	9289,
	9442,
	9289,
	9289,
	9247,
	9161,
	9161,
	6739,
	6745,
	9289,
	12729,
	12729,
	13922,
	13922,
	6353,
	13922,
	12854,
	7597,
	7597,
	9442,
	7597,
	7597,
	4359,
	4359,
	9289,
	7597,
	4368,
	4375,
	9442,
	4375,
	4375,
	14502,
	373,
	9289,
	9161,
	9247,
	9289,
	13266,
	9442,
	13922,
	11779,
	11154,
	14256,
	13266,
	12478,
	3454,
	12453,
	7597,
	4368,
	5478,
	9442,
	4368,
	14502,
	9442,
	13266,
	13922,
	12869,
	13266,
	14256,
	14256,
	13266,
	14256,
	13266,
	4368,
	9289,
	4368,
	9161,
	9161,
	9289,
	9289,
	7597,
	9289,
	9247,
	9289,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	14256,
	14256,
	2759,
	1435,
	9442,
	9442,
	7469,
	9442,
	9442,
	3443,
	9289,
	9442,
	4359,
	7597,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	957,
	4359,
	9161,
	9161,
	9161,
	9161,
	9289,
	9247,
	5361,
	5361,
	14502,
	9442,
	9161,
	9161,
	7469,
	9247,
	7557,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9161,
	14423,
	9289,
	9289,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9289,
	7597,
	9442,
	7469,
	9442,
	7597,
	7469,
	9289,
	9289,
	5478,
	6745,
	9442,
	9289,
	9289,
	5478,
	7597,
	6353,
	7597,
	9442,
	3462,
	9442,
	6745,
	9289,
	9442,
	9442,
	9442,
	4368,
	9161,
	7469,
	9161,
	7469,
	9289,
	9289,
	9161,
	9161,
	14256,
	7597,
	7597,
	4345,
	9289,
	9442,
	7597,
	7597,
	9289,
	9289,
	7597,
	9247,
	7557,
	9161,
	9289,
	7597,
	9289,
	9289,
	9289,
	9161,
	9442,
	6745,
	4375,
	9442,
	7557,
	4037,
	9247,
	7597,
	9289,
	7597,
	9289,
	9161,
	9289,
	7597,
	9289,
	9247,
	7597,
	14256,
	5478,
	9247,
	9289,
	7597,
	9247,
	9161,
	9161,
	7597,
	9442,
	5478,
	4359,
	4359,
	9289,
	5478,
	9289,
	14502,
	7597,
	9289,
	9247,
	9289,
	9248,
	9289,
	9289,
	9161,
	9289,
	9289,
	9289,
	9161,
	9161,
	9161,
	9161,
	9161,
	9289,
	9289,
	9289,
	9289,
	9289,
	9221,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	12473,
	7597,
	9442,
	9161,
	7597,
	3462,
	6745,
	9289,
	9289,
	7597,
	9161,
	7469,
	9161,
	7469,
	9289,
	7597,
	9248,
	7558,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9289,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9247,
	7557,
	9289,
	7597,
	5478,
	9442,
	9442,
	7469,
	6745,
	6745,
	9442,
	4368,
	7597,
	4368,
	9442,
	4345,
	7597,
	7597,
	7597,
	9442,
	7557,
	9442,
	9161,
	9442,
	9289,
	9442,
	9289,
	9289,
	9289,
	9442,
	7597,
	4368,
	9289,
	7597,
	9247,
	7557,
	9289,
	9161,
	9247,
	7557,
	9247,
	7557,
	9289,
	9289,
	9161,
	9161,
	9442,
	7597,
	7597,
	11670,
	11670,
	12869,
	14458,
	13737,
	14502,
	13611,
	12225,
	13293,
	12227,
	11146,
	10388,
	13922,
	12869,
	11139,
	11802,
	11802,
	13922,
	13922,
	13266,
	13922,
	13266,
	13922,
	13266,
	13922,
	12869,
	13922,
	12869,
	12869,
	11139,
	13922,
	13922,
	12869,
	11802,
	13922,
	13922,
	12869,
	11802,
	13922,
	13922,
	12869,
	11802,
	13922,
	13922,
	13922,
	9442,
	14502,
	9442,
	14502,
	4368,
	1717,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	9442,
	1676,
	9289,
	7597,
	9289,
	7597,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	1707,
	847,
	9161,
	9161,
	9161,
	9248,
	9248,
	7558,
	1983,
	695,
	695,
	9442,
	6353,
	7597,
	9442,
	1983,
	3367,
	7558,
	2387,
	2414,
	9161,
	9161,
	9161,
	9248,
	9248,
	7558,
	5361,
	7469,
	5361,
	12839,
	2387,
	2387,
	2387,
	7469,
	2387,
	695,
	695,
	9442,
	7469,
	6353,
	7597,
	9442,
	1983,
	3367,
	7558,
	2387,
	14502,
	7597,
	1628,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	3947,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9247,
	7557,
	14502,
	9442,
	716,
	956,
	14502,
	3947,
	4375,
	9442,
	9247,
	9289,
	9247,
	6739,
	4037,
	6739,
	4037,
	9289,
	2414,
	4368,
	4368,
	13753,
	13753,
	13922,
	7597,
	7469,
	13922,
	13922,
	1714,
	855,
	2423,
	13922,
	12474,
	4368,
	4368,
	13914,
	13914,
	7597,
	4345,
	2414,
	13600,
	13600,
	12474,
	6723,
	4368,
	7597,
	4037,
	4037,
	4368,
	9442,
	6739,
	6745,
	9289,
	6739,
	6739,
	6745,
	4375,
	13600,
	12474,
	7597,
	7557,
	7557,
	7597,
	4037,
	4037,
	4368,
	9289,
	9289,
	4375,
	9442,
	7557,
	9442,
	7557,
	9442,
	7557,
	4368,
	9289,
	9289,
	9289,
	9289,
	9289,
	9161,
	9161,
	9161,
	9161,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9442,
	7557,
	9289,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	9289,
	9289,
	842,
	9289,
	9289,
	9289,
	9289,
	9289,
	9161,
	9161,
	9161,
	9161,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	1827,
	9442,
	7557,
	7597,
	9289,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	9289,
	9289,
	9442,
	9161,
	9442,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9247,
	7557,
	9289,
	4037,
	9289,
	4037,
	9289,
	4037,
	9289,
	9442,
	7597,
	2761,
	9442,
	7501,
	9289,
	9289,
	7597,
	7597,
	7597,
	9442,
	9442,
	7597,
	7597,
	7597,
	9442,
	9442,
	9442,
	7597,
	9289,
	7469,
	9442,
	9247,
	9247,
	9422,
	9422,
	9289,
	819,
	7469,
	9442,
	9289,
	9161,
	9247,
	9247,
	9247,
	9161,
	9247,
	7597,
	9250,
	9247,
	9442,
	14423,
	14242,
	9289,
	3674,
	5478,
	7597,
	9442,
	9442,
	9442,
	6745,
	942,
	6263,
	14502,
	9289,
	2257,
	7597,
	7597,
	9289,
	9161,
	9161,
	7501,
	9289,
	13922,
	13250,
	9289,
	4368,
	2423,
	5304,
	9442,
	9161,
	7469,
	7501,
	9289,
	9161,
	9161,
	9289,
	9173,
	9247,
	9161,
	3674,
	9289,
	7597,
	9442,
	7597,
	9289,
	9289,
	8985,
	7299,
	8985,
	7299,
	8985,
	7299,
	8985,
	7299,
	8985,
	7299,
	9442,
	7597,
	9289,
	9289,
	2423,
	6745,
	6726,
	7469,
	9161,
	7469,
	7597,
	1675,
	7597,
	183,
	111,
	9442,
	2423,
	6745,
	7469,
	9161,
	9289,
	7597,
	7597,
	9289,
	9442,
	9442,
	14502,
	14256,
	14252,
	14252,
	14252,
	9289,
	6745,
	3462,
	6745,
	3462,
	3462,
	3462,
	6745,
	6745,
	7597,
	7597,
	7597,
	7469,
	9442,
	9161,
	7469,
	9161,
	9161,
	14458,
	14458,
	14458,
	14502,
	9442,
	9289,
	2423,
	9442,
	3421,
	7469,
	9442,
	13922,
	13922,
	9247,
	9289,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	9442,
	7597,
	9161,
	7597,
	7597,
	4368,
	4368,
	4368,
	4368,
	7469,
	9442,
	14458,
	14256,
	9442,
	956,
	9289,
	14502,
	4368,
	0,
	7597,
	6726,
	9442,
	7597,
	9442,
	9161,
	9289,
	9442,
	3421,
	0,
	6726,
	3682,
	7469,
	9442,
	5375,
	5375,
	0,
	0,
	9289,
	4368,
	0,
	7597,
	9442,
	5860,
	5304,
	5304,
	9442,
	443,
	3074,
	7483,
	9442,
	9442,
	9442,
	9442,
	9442,
	5860,
	5858,
	7483,
	3674,
	7483,
	6745,
	6745,
	9173,
	7469,
	9442,
	14502,
	14447,
	9247,
	9173,
	9289,
	7597,
	9247,
	9247,
	9161,
	9442,
	7597,
	0,
	7557,
	7557,
	5478,
	7597,
	7483,
	5478,
	0,
	7597,
	0,
	5304,
	5304,
	0,
	7596,
	9442,
	0,
	7597,
	7597,
	7597,
	9161,
	9442,
	14502,
	9442,
	14502,
	14256,
	14458,
	14458,
	0,
	7469,
	9442,
	9442,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	4362,
	4368,
	1462,
	7597,
	4362,
	4368,
	1462,
	7597,
	4362,
	4368,
	1462,
	7597,
	4362,
	7597,
	2094,
	7597,
	4362,
	2390,
	698,
	7597,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	7597,
	9289,
	9161,
	7469,
	9247,
	7557,
	9442,
	9161,
	9173,
	9161,
	9247,
	2255,
	7496,
	9442,
	9247,
	4368,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	7597,
	2390,
	4368,
	4368,
	7597,
	7596,
	7596,
	7483,
	7483,
	7597,
	13922,
	7596,
	9442,
	9442,
	9442,
	7596,
	7597,
	9173,
	14458,
	5304,
	5304,
	9442,
	9442,
	443,
	7483,
	9442,
	7597,
	9442,
	5860,
	7469,
	9442,
	14502,
	9247,
	7557,
	9442,
	9442,
};
static const Il2CppTokenRangePair s_rgctxIndices[20] = 
{
	{ 0x02000010, { 23, 2 } },
	{ 0x0200003A, { 37, 4 } },
	{ 0x02000040, { 47, 4 } },
	{ 0x02000042, { 51, 4 } },
	{ 0x06000024, { 0, 6 } },
	{ 0x06000026, { 6, 1 } },
	{ 0x06000046, { 7, 1 } },
	{ 0x0600004C, { 8, 2 } },
	{ 0x0600005A, { 10, 2 } },
	{ 0x06000066, { 12, 1 } },
	{ 0x06000067, { 13, 1 } },
	{ 0x06000070, { 14, 2 } },
	{ 0x06000071, { 16, 2 } },
	{ 0x06000073, { 18, 5 } },
	{ 0x06000099, { 25, 6 } },
	{ 0x0600023D, { 31, 1 } },
	{ 0x0600023E, { 32, 5 } },
	{ 0x06000296, { 41, 1 } },
	{ 0x06000297, { 42, 5 } },
	{ 0x060002C8, { 55, 2 } },
};
extern const uint32_t g_rgctx_IEnumerable_1_tE02F3B1E9222BBA5004886277166A4E27F4CED81;
extern const uint32_t g_rgctx_IEnumerable_1_GetEnumerator_m106010215FE6D6EAD273209C98B05CF50D9BC4DE;
extern const uint32_t g_rgctx_IEnumerator_1_t3B6D60C608941FE4EB342A2C8358B9582248ECB9;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m8E042B0B0471942BAC2752A200AC316C0C18EFD1;
extern const uint32_t g_rgctx_Func_2_tF85CF39F3640E6E683EB3335F13D01B27672CF42;
extern const uint32_t g_rgctx_Func_2_Invoke_mCCC47DC84B5B1645EBD51EC1976A52BD5C6C6332;
extern const uint32_t g_rgctx_TU5BU5D_t7A8C429DDD719F2AD49A322A828579F2D5C9286F;
extern const uint32_t g_rgctx_TU5BU5D_tF8ECCA37707809AE32C1C425ABE382CA43AB964E;
extern const uint32_t g_rgctx_List_1_t2F56CF53D58EB57F3E1390BA9E240714355B3566;
extern const uint32_t g_rgctx_List_1__ctor_mDED3031686219160F506E378E4C48BD4364E54DB;
extern const uint32_t g_rgctx_EventHandler_1_tB85AD8B9CBE4EFCFDCFD2B2F81DD81895B204EC9;
extern const uint32_t g_rgctx_EventHandler_1_Invoke_mBF7025548294DEEF3E60DA2B7277A2289AC84777;
extern const uint32_t g_rgctx_TU5BU5D_t29BD4E1618BEA76EC063C08B291960DCAE8AD885;
extern const uint32_t g_rgctx_TU5BU5D_t86CF70D07DFA319E340E238AD6ACE0E6F32512C0;
extern const uint32_t g_rgctx_T_tCC94E3124C58973B4D690E4764731BB75E2AEB0D;
extern const uint32_t g_rgctx_T_tCC94E3124C58973B4D690E4764731BB75E2AEB0D;
extern const uint32_t g_rgctx_T_tD9ABF00E493EA33E48764D480C7E958F4C9B2A28;
extern const uint32_t g_rgctx_T_tD9ABF00E493EA33E48764D480C7E958F4C9B2A28;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass101_0_1_t9794EFF381C9E17EFF3B27A111D2179890E4D069;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass101_0_1__ctor_m53C1EF7473834D6E5A636215C3812A5DB6438C19;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass101_0_1_U3CToStringU3Eb__0_mB6B7CF1EDEA136283A1CDB4D18D1D8756AEA3786;
extern const uint32_t g_rgctx_T_t079F4F2FFD6B3E275C0827761A12E0067CB5ED65;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_t079F4F2FFD6B3E275C0827761A12E0067CB5ED65_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_T_t99E0FB416420CC0219661DB7D7B429C3BC428DD0;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_t99E0FB416420CC0219661DB7D7B429C3BC428DD0_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass14_0_1_t3B94FFF6F96FBE24952AD7A9C8A885D4DCFA79F4;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass14_0_1__ctor_m6C9959EB28FE50616F6F0EA94B8363897084F07C;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass14_0_1_U3CReadU3Eb__0_m6E9D58407376EE9DD9AFA433919B88C78C16E1EE;
extern const uint32_t g_rgctx_Func_2_tCC0E24FBE36FB5B442521B46B42885CFA7E6FED0;
extern const uint32_t g_rgctx_Func_2_Invoke_mB51FFA16B2C808CFF2FADFF73F2FEF4D2B42258A;
extern const uint32_t g_rgctx_T_t6E2593806A814D90E4E97A8321EFC57FAC495214;
extern const uint32_t g_rgctx_WebSocketServiceManager_Add_TisTBehavior_tB6555895AD27A3CE988CFB6B6CBB73865AB8E89B_m8E5BCDF08D5FAF63E9EE4C482F4C68FA1C729441;
extern const uint32_t g_rgctx_U3CU3Ec__91_1_tE6FB81F622478E5BE72D8CFD00688000307DD2B2;
extern const uint32_t g_rgctx_U3CU3Ec__91_1_U3CAddWebSocketServiceU3Eb__91_0_mC6DBA73A7B1239291F053BF37C408942D88A8CAB;
extern const uint32_t g_rgctx_Func_1_t0C5180D8E403ACB3518F9796B9367DBC8BA0B77D;
extern const uint32_t g_rgctx_Func_1__ctor_m9C797167939770D1EEE115D1B0E732EA26A09543;
extern const uint32_t g_rgctx_HttpServer_AddWebSocketService_TisTBehaviorWithNew_tAB1F94AF6DABE809D5540298A473DF1EEE19B521_mD7E24811301A343EAA4F4ECEE766CB28E2CC062B;
extern const uint32_t g_rgctx_U3CU3Ec__91_1_tDCE4E7184E95F709D4FC113E5B6D1D3BE04C3C26;
extern const uint32_t g_rgctx_U3CU3Ec__91_1__ctor_m28CB5C158F186ED0B204977F6DED42068446699F;
extern const uint32_t g_rgctx_U3CU3Ec__91_1_tDCE4E7184E95F709D4FC113E5B6D1D3BE04C3C26;
extern const uint32_t g_rgctx_Activator_CreateInstance_TisTBehaviorWithNew_t650764CDB4C7DEFE41B72C01A66BC87B1BC25477_m95C13793C1D302FB6239D5F57C02287FB68BBEFB;
extern const uint32_t g_rgctx_WebSocketServiceManager_Add_TisTBehavior_tBECD1F6640AD80D6C683A10E680C96B01890AADF_mDE1EFA6F9616704E122926A4811A984CE980CADC;
extern const uint32_t g_rgctx_U3CU3Ec__67_1_t44205A89A57CD778608E0D5E3E5D89BCD56C6877;
extern const uint32_t g_rgctx_U3CU3Ec__67_1_U3CAddWebSocketServiceU3Eb__67_0_m948FE73E86D1A2590E7ABF7A074F1775874FB6C8;
extern const uint32_t g_rgctx_Func_1_tF20076B82BFB391EFF489239115729FEAC60DD04;
extern const uint32_t g_rgctx_Func_1__ctor_mE1C405FF13A5DD8011CD91FEC131EE63023D92B6;
extern const uint32_t g_rgctx_WebSocketServer_AddWebSocketService_TisTBehaviorWithNew_t829777D372A96628711F12CE8FC4A39524DA7F97_mF507D69CDEFC97F0CC96D9A6ACEDB017E48DE521;
extern const uint32_t g_rgctx_U3CU3Ec__67_1_t0E2E239E02B99B0A82606BBF738805F1FD0437BB;
extern const uint32_t g_rgctx_U3CU3Ec__67_1__ctor_m7411DDC708633873DC4E99FF2511C3AFE0428AE4;
extern const uint32_t g_rgctx_U3CU3Ec__67_1_t0E2E239E02B99B0A82606BBF738805F1FD0437BB;
extern const uint32_t g_rgctx_Activator_CreateInstance_TisTBehaviorWithNew_tF89469BD953B0A875876F5A1F2380CBFEE9B9FA2_m5B4591DCE3802D6D92553DB18B05A92D912F19A0;
extern const uint32_t g_rgctx_TBehavior_t617DEE9EDEDDC40831024F756E77034E2320A8C6;
extern const uint32_t g_rgctx_Func_1_t176FA70F20B7A8878EE215E3C847FACABE106C97;
extern const uint32_t g_rgctx_Func_1_Invoke_m88003413D48546D453F5150E1609566ED7671589;
extern const uint32_t g_rgctx_TBehavior_t617DEE9EDEDDC40831024F756E77034E2320A8C6;
extern const uint32_t g_rgctx_WebSocketServiceHost_1_t5F76EA6AD9D824A890B381E8FEB0830172EBE3E4;
extern const uint32_t g_rgctx_WebSocketServiceHost_1__ctor_mB29D776EE3A6774D1A8CB66F3A6D35991786B59A;
static const Il2CppRGCTXDefinition s_rgctxValues[57] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_tE02F3B1E9222BBA5004886277166A4E27F4CED81 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerable_1_GetEnumerator_m106010215FE6D6EAD273209C98B05CF50D9BC4DE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t3B6D60C608941FE4EB342A2C8358B9582248ECB9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m8E042B0B0471942BAC2752A200AC316C0C18EFD1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tF85CF39F3640E6E683EB3335F13D01B27672CF42 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mCCC47DC84B5B1645EBD51EC1976A52BD5C6C6332 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_t7A8C429DDD719F2AD49A322A828579F2D5C9286F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_tF8ECCA37707809AE32C1C425ABE382CA43AB964E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t2F56CF53D58EB57F3E1390BA9E240714355B3566 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mDED3031686219160F506E378E4C48BD4364E54DB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EventHandler_1_tB85AD8B9CBE4EFCFDCFD2B2F81DD81895B204EC9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EventHandler_1_Invoke_mBF7025548294DEEF3E60DA2B7277A2289AC84777 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_t29BD4E1618BEA76EC063C08B291960DCAE8AD885 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_t86CF70D07DFA319E340E238AD6ACE0E6F32512C0 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tCC94E3124C58973B4D690E4764731BB75E2AEB0D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tCC94E3124C58973B4D690E4764731BB75E2AEB0D },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tD9ABF00E493EA33E48764D480C7E958F4C9B2A28 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tD9ABF00E493EA33E48764D480C7E958F4C9B2A28 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass101_0_1_t9794EFF381C9E17EFF3B27A111D2179890E4D069 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass101_0_1__ctor_m53C1EF7473834D6E5A636215C3812A5DB6438C19 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass101_0_1_U3CToStringU3Eb__0_mB6B7CF1EDEA136283A1CDB4D18D1D8756AEA3786 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t079F4F2FFD6B3E275C0827761A12E0067CB5ED65 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_t079F4F2FFD6B3E275C0827761A12E0067CB5ED65_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t99E0FB416420CC0219661DB7D7B429C3BC428DD0 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_t99E0FB416420CC0219661DB7D7B429C3BC428DD0_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass14_0_1_t3B94FFF6F96FBE24952AD7A9C8A885D4DCFA79F4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass14_0_1__ctor_m6C9959EB28FE50616F6F0EA94B8363897084F07C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass14_0_1_U3CReadU3Eb__0_m6E9D58407376EE9DD9AFA433919B88C78C16E1EE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tCC0E24FBE36FB5B442521B46B42885CFA7E6FED0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mB51FFA16B2C808CFF2FADFF73F2FEF4D2B42258A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t6E2593806A814D90E4E97A8321EFC57FAC495214 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_WebSocketServiceManager_Add_TisTBehavior_tB6555895AD27A3CE988CFB6B6CBB73865AB8E89B_m8E5BCDF08D5FAF63E9EE4C482F4C68FA1C729441 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__91_1_tE6FB81F622478E5BE72D8CFD00688000307DD2B2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__91_1_U3CAddWebSocketServiceU3Eb__91_0_mC6DBA73A7B1239291F053BF37C408942D88A8CAB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t0C5180D8E403ACB3518F9796B9367DBC8BA0B77D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1__ctor_m9C797167939770D1EEE115D1B0E732EA26A09543 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HttpServer_AddWebSocketService_TisTBehaviorWithNew_tAB1F94AF6DABE809D5540298A473DF1EEE19B521_mD7E24811301A343EAA4F4ECEE766CB28E2CC062B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__91_1_tDCE4E7184E95F709D4FC113E5B6D1D3BE04C3C26 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__91_1__ctor_m28CB5C158F186ED0B204977F6DED42068446699F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__91_1_tDCE4E7184E95F709D4FC113E5B6D1D3BE04C3C26 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Activator_CreateInstance_TisTBehaviorWithNew_t650764CDB4C7DEFE41B72C01A66BC87B1BC25477_m95C13793C1D302FB6239D5F57C02287FB68BBEFB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_WebSocketServiceManager_Add_TisTBehavior_tBECD1F6640AD80D6C683A10E680C96B01890AADF_mDE1EFA6F9616704E122926A4811A984CE980CADC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__67_1_t44205A89A57CD778608E0D5E3E5D89BCD56C6877 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__67_1_U3CAddWebSocketServiceU3Eb__67_0_m948FE73E86D1A2590E7ABF7A074F1775874FB6C8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_tF20076B82BFB391EFF489239115729FEAC60DD04 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1__ctor_mE1C405FF13A5DD8011CD91FEC131EE63023D92B6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_WebSocketServer_AddWebSocketService_TisTBehaviorWithNew_t829777D372A96628711F12CE8FC4A39524DA7F97_mF507D69CDEFC97F0CC96D9A6ACEDB017E48DE521 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__67_1_t0E2E239E02B99B0A82606BBF738805F1FD0437BB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__67_1__ctor_m7411DDC708633873DC4E99FF2511C3AFE0428AE4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__67_1_t0E2E239E02B99B0A82606BBF738805F1FD0437BB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Activator_CreateInstance_TisTBehaviorWithNew_tF89469BD953B0A875876F5A1F2380CBFEE9B9FA2_m5B4591DCE3802D6D92553DB18B05A92D912F19A0 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TBehavior_t617DEE9EDEDDC40831024F756E77034E2320A8C6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t176FA70F20B7A8878EE215E3C847FACABE106C97 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m88003413D48546D453F5150E1609566ED7671589 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TBehavior_t617DEE9EDEDDC40831024F756E77034E2320A8C6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_WebSocketServiceHost_1_t5F76EA6AD9D824A890B381E8FEB0830172EBE3E4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_WebSocketServiceHost_1__ctor_mB29D776EE3A6774D1A8CB66F3A6D35991786B59A },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Byn_Awrtc_Native_CodeGenModule;
const Il2CppCodeGenModule g_Byn_Awrtc_Native_CodeGenModule = 
{
	"Byn.Awrtc.Native.dll",
	1983,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	20,
	s_rgctxIndices,
	57,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
