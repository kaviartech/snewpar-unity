﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m288BA0BFA12E5E462CB60C5B3D8BE3677EFF93F2 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m522246D01124BDBCC7B05E716AF928006F31508D (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
extern void NullableAttribute__ctor_m74E75D3E2DC3E77488AB9C381D226214DF3CC2A0 (void);
// 0x00000004 System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
extern void NullableContextAttribute__ctor_m09AEB7692E9EE8247068756A53B49D700A2FBDF5 (void);
// 0x00000005 System.Void System.Runtime.CompilerServices.NullablePublicOnlyAttribute::.ctor(System.Boolean)
extern void NullablePublicOnlyAttribute__ctor_m1EB5F8C417EC2EC2A3046C71684DACCA5E15D15F (void);
// 0x00000006 System.Char System.HexConverter::ToCharUpper(System.Int32)
extern void HexConverter_ToCharUpper_mA514E6A22DBB36B2CE741A5026B23A45A10BC776 (void);
// 0x00000007 System.Void System.Diagnostics.CodeAnalysis.NotNullAttribute::.ctor()
extern void NotNullAttribute__ctor_m2F96337C648C3DF27411B9368A82219C8D6A630D (void);
// 0x00000008 System.UInt32 System.Text.UnicodeUtility::GetScalarFromUtf16SurrogatePair(System.UInt32,System.UInt32)
extern void UnicodeUtility_GetScalarFromUtf16SurrogatePair_m6BF54197FBBFCDBE7172B89440194AE653351DE6 (void);
// 0x00000009 System.Int32 System.Text.UnicodeUtility::GetUtf16SequenceLength(System.UInt32)
extern void UnicodeUtility_GetUtf16SequenceLength_m0FE2252F5535FFF306564627DEFE355C5DF2CD9A (void);
// 0x0000000A System.Boolean System.Text.UnicodeUtility::IsAsciiCodePoint(System.UInt32)
extern void UnicodeUtility_IsAsciiCodePoint_mDE38F64B792FCDC8C910B91B7A5C7019ECBADA44 (void);
// 0x0000000B System.Boolean System.Text.UnicodeUtility::IsHighSurrogateCodePoint(System.UInt32)
extern void UnicodeUtility_IsHighSurrogateCodePoint_mF5DAC97DE50D6EFF231B3D3FD2EECFBCE1E97100 (void);
// 0x0000000C System.Boolean System.Text.UnicodeUtility::IsInRangeInclusive(System.UInt32,System.UInt32,System.UInt32)
extern void UnicodeUtility_IsInRangeInclusive_m1FFA10EDA34D1F09E82721521DCA5E2A2B41C0E8 (void);
// 0x0000000D System.Boolean System.Text.UnicodeUtility::IsLowSurrogateCodePoint(System.UInt32)
extern void UnicodeUtility_IsLowSurrogateCodePoint_mCF3C7B96F7CACD1657A588B8DC85CE957C50264B (void);
// 0x0000000E System.Boolean System.Text.UnicodeUtility::IsSurrogateCodePoint(System.UInt32)
extern void UnicodeUtility_IsSurrogateCodePoint_mE6713FFE894D59D385CD071BB60A2618B68573C1 (void);
// 0x0000000F System.UInt32[] System.Text.Unicode.UnicodeHelpers::CreateDefinedCharacterBitmapMachineEndian()
extern void UnicodeHelpers_CreateDefinedCharacterBitmapMachineEndian_m23577DE0EFEA6B63D4EA8F7BB2732B2A8C87BD4F (void);
// 0x00000010 System.Buffers.OperationStatus System.Text.Unicode.UnicodeHelpers::DecodeScalarValueFromUtf8(System.ReadOnlySpan`1<System.Byte>,System.UInt32&,System.Int32&)
extern void UnicodeHelpers_DecodeScalarValueFromUtf8_mCE6926D9E56D292232835F9774218C999488202C (void);
// 0x00000011 System.ReadOnlySpan`1<System.UInt32> System.Text.Unicode.UnicodeHelpers::GetDefinedCharacterBitmap()
extern void UnicodeHelpers_GetDefinedCharacterBitmap_m33E763520F56351D9F8B26EECBE5078FC0ADD34F (void);
// 0x00000012 System.Void System.Text.Unicode.UnicodeHelpers::GetUtf16SurrogatePairFromAstralScalarValue(System.Int32,System.Char&,System.Char&)
extern void UnicodeHelpers_GetUtf16SurrogatePairFromAstralScalarValue_mC0A819940F8905BE73AD0B3826C508220B7D5D56 (void);
// 0x00000013 System.Boolean System.Text.Unicode.UnicodeHelpers::IsSupplementaryCodePoint(System.Int32)
extern void UnicodeHelpers_IsSupplementaryCodePoint_m5875C82A22F29496C03AB28B311E8321B8ED91F5 (void);
// 0x00000014 System.ReadOnlySpan`1<System.Byte> System.Text.Unicode.UnicodeHelpers::get_DefinedCharsBitmapSpan()
extern void UnicodeHelpers_get_DefinedCharsBitmapSpan_mBF980F8D332E76BC8CEBCFEDD1233C8A2A45ABEF (void);
// 0x00000015 System.Void System.Text.Unicode.UnicodeHelpers::.cctor()
extern void UnicodeHelpers__cctor_mED57E4DE51568523E847C79EBF145D6DB10410B7 (void);
// 0x00000016 System.Void System.Text.Unicode.UnicodeRange::.ctor(System.Int32,System.Int32)
extern void UnicodeRange__ctor_m55C49ADABCAD9160E480CD01439D639C24980AB5 (void);
// 0x00000017 System.Int32 System.Text.Unicode.UnicodeRange::get_FirstCodePoint()
extern void UnicodeRange_get_FirstCodePoint_mA023CB7BCEE4210B23D6FBFA1B421D4C53F31D35 (void);
// 0x00000018 System.Void System.Text.Unicode.UnicodeRange::set_FirstCodePoint(System.Int32)
extern void UnicodeRange_set_FirstCodePoint_m786BFD5158D01D1AD0073DFC0609231BAF4E5777 (void);
// 0x00000019 System.Int32 System.Text.Unicode.UnicodeRange::get_Length()
extern void UnicodeRange_get_Length_m3664F9267AB716A55BB92139410B57B47C1AD154 (void);
// 0x0000001A System.Void System.Text.Unicode.UnicodeRange::set_Length(System.Int32)
extern void UnicodeRange_set_Length_m7965F884FA95909701DC2125445C86B43B549720 (void);
// 0x0000001B System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRange::Create(System.Char,System.Char)
extern void UnicodeRange_Create_m45AF1389FADA69EF5C153C7FA1B09D9C77F469AD (void);
// 0x0000001C System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::CreateRange(System.Text.Unicode.UnicodeRange&,System.Char,System.Char)
extern void UnicodeRanges_CreateRange_mD0D98535E601E77326370417B89B42D5C0FCACC3 (void);
// 0x0000001D System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::get_BasicLatin()
extern void UnicodeRanges_get_BasicLatin_mAB30D6E10C1E286D899D7C56E1049A418E6C4698 (void);
// 0x0000001E System.Text.Internal.AllowedCharactersBitmap System.Text.Internal.AllowedCharactersBitmap::CreateNew()
extern void AllowedCharactersBitmap_CreateNew_m1068713B9D9E53E5A3D7D6056D70FE768A170484 (void);
// 0x0000001F System.Void System.Text.Internal.AllowedCharactersBitmap::.ctor(System.UInt32[])
extern void AllowedCharactersBitmap__ctor_m84D288D533707728EBC3AA9F4D92A2A425CD4355 (void);
// 0x00000020 System.Void System.Text.Internal.AllowedCharactersBitmap::AllowCharacter(System.Char)
extern void AllowedCharactersBitmap_AllowCharacter_mA62947598D488B8397D1B785EB8710A3C2502C24 (void);
// 0x00000021 System.Void System.Text.Internal.AllowedCharactersBitmap::ForbidCharacter(System.Char)
extern void AllowedCharactersBitmap_ForbidCharacter_mA96107773390A5E578FE51E57088924839152B65 (void);
// 0x00000022 System.Void System.Text.Internal.AllowedCharactersBitmap::ForbidUndefinedCharacters()
extern void AllowedCharactersBitmap_ForbidUndefinedCharacters_mD6CE16DBD95C181B9C36CF710079DFAC5AD7C639 (void);
// 0x00000023 System.Text.Internal.AllowedCharactersBitmap System.Text.Internal.AllowedCharactersBitmap::Clone()
extern void AllowedCharactersBitmap_Clone_m0B4354E106746EBAB8C251B846BC07C7F8EED079 (void);
// 0x00000024 System.Boolean System.Text.Internal.AllowedCharactersBitmap::IsCharacterAllowed(System.Char)
extern void AllowedCharactersBitmap_IsCharacterAllowed_mBAD9A317E8F0EB7672C2C1C4D0F277F1F112E2D2 (void);
// 0x00000025 System.Boolean System.Text.Internal.AllowedCharactersBitmap::IsUnicodeScalarAllowed(System.Int32)
extern void AllowedCharactersBitmap_IsUnicodeScalarAllowed_mCE82E28101DFD37C0376B042BFDBD5713EBB51A5 (void);
// 0x00000026 System.Int32 System.Text.Internal.AllowedCharactersBitmap::FindFirstCharacterToEncode(System.Char*,System.Int32)
extern void AllowedCharactersBitmap_FindFirstCharacterToEncode_m0E37005FCB6C9A200E34E8D7F82119933B12510C (void);
// 0x00000027 System.Void System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::.ctor()
extern void DefaultJavaScriptEncoderBasicLatin__ctor_mA4678374F0BAF83C52194B6E675AB72725377972 (void);
// 0x00000028 System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::WillEncode(System.Int32)
extern void DefaultJavaScriptEncoderBasicLatin_WillEncode_m4E53C5910E23CD0549705CF3B6316DBC39CABC1C (void);
// 0x00000029 System.Int32 System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::FindFirstCharacterToEncode(System.Char*,System.Int32)
extern void DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncode_mEE41382BC0EE3FBF57FCA8128B33CE14D0E60945 (void);
// 0x0000002A System.Int32 System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::FindFirstCharacterToEncodeUtf8(System.ReadOnlySpan`1<System.Byte>)
extern void DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncodeUtf8_mDA508AED3C4D8AE936BBAA2F484DB8A692607F45 (void);
// 0x0000002B System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void DefaultJavaScriptEncoderBasicLatin_TryEncodeUnicodeScalar_m2691077A7D3D766406F250A5F245DB9803CA041E (void);
// 0x0000002C System.ReadOnlySpan`1<System.Byte> System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::get_AllowList()
extern void DefaultJavaScriptEncoderBasicLatin_get_AllowList_mCD3190A2592B682101C144D458558CCCAD7FB1C0 (void);
// 0x0000002D System.Boolean System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::NeedsEscaping(System.Char)
extern void DefaultJavaScriptEncoderBasicLatin_NeedsEscaping_mD26E7F6A44E3E504E620A44D29A10578FE394486 (void);
// 0x0000002E System.Void System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::.cctor()
extern void DefaultJavaScriptEncoderBasicLatin__cctor_mB0E2A79C691082E6FE6A16A94D10CBAA29694FF4 (void);
// 0x0000002F System.Void System.Text.Encodings.Web.HtmlEncoder::.ctor()
extern void HtmlEncoder__ctor_mB04BFEDA89DD7987924E2B8A75CD985F6EE71D02 (void);
// 0x00000030 System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::.ctor(System.Text.Encodings.Web.TextEncoderSettings)
extern void DefaultHtmlEncoder__ctor_m0DF07C80DAB8C7B18B14176343114B086ECBBC0E (void);
// 0x00000031 System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::ForbidHtmlCharacters(System.Text.Internal.AllowedCharactersBitmap)
extern void DefaultHtmlEncoder_ForbidHtmlCharacters_m01992C50627493B9A7302D591887147D34AD8591 (void);
// 0x00000032 System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::WillEncode(System.Int32)
extern void DefaultHtmlEncoder_WillEncode_m714DB0F9F170F5E40C6635DC903C7F89C50DAD28 (void);
// 0x00000033 System.Int32 System.Text.Encodings.Web.DefaultHtmlEncoder::FindFirstCharacterToEncode(System.Char*,System.Int32)
extern void DefaultHtmlEncoder_FindFirstCharacterToEncode_m726646ED7E20FC2094AE1A133D31A6CE49BCC3F3 (void);
// 0x00000034 System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void DefaultHtmlEncoder_TryEncodeUnicodeScalar_m39A2DC1D1C3D9F58799A1A64B9CD83780BFE6BB3 (void);
// 0x00000035 System.Boolean System.Text.Encodings.Web.DefaultHtmlEncoder::TryWriteEncodedScalarAsNumericEntity(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void DefaultHtmlEncoder_TryWriteEncodedScalarAsNumericEntity_m2DD5A9DD47D14C87C0B3C36B8CFFA2CF8B0DCE27 (void);
// 0x00000036 System.Void System.Text.Encodings.Web.DefaultHtmlEncoder::.cctor()
extern void DefaultHtmlEncoder__cctor_m8D91E212CFE007F9CDD669ADD799B15F0147BAD1 (void);
// 0x00000037 System.Text.Encodings.Web.JavaScriptEncoder System.Text.Encodings.Web.JavaScriptEncoder::get_Default()
extern void JavaScriptEncoder_get_Default_mA1BD7ACB1459CE7B06A32D6CF329C522C567D4ED (void);
// 0x00000038 System.Void System.Text.Encodings.Web.JavaScriptEncoder::.ctor()
extern void JavaScriptEncoder__ctor_m59B9FB268C118BDFC532CC01F6E9798DE75386B6 (void);
// 0x00000039 System.Boolean System.Text.Encodings.Web.JavaScriptEncoderHelper::TryWriteEncodedScalarAsNumericEntity(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void JavaScriptEncoderHelper_TryWriteEncodedScalarAsNumericEntity_mCC4DB3060EB6E69D1D42E8C01D2D53F9E95C0021 (void);
// 0x0000003A System.Boolean System.Text.Encodings.Web.JavaScriptEncoderHelper::TryWriteEncodedSingleCharacter(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void JavaScriptEncoderHelper_TryWriteEncodedSingleCharacter_mE7A5A0EF4960169B3B6B05E6E7118E41DF0BBAE5 (void);
// 0x0000003B System.Boolean System.Text.Encodings.Web.TextEncoder::TryEncodeUnicodeScalar(System.Int32,System.Char*,System.Int32,System.Int32&)
// 0x0000003C System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncode(System.Char*,System.Int32)
// 0x0000003D System.Boolean System.Text.Encodings.Web.TextEncoder::WillEncode(System.Int32)
// 0x0000003E System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::EncodeUtf8(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32&,System.Int32&,System.Boolean)
extern void TextEncoder_EncodeUtf8_mBDED5D080EC52F201E62664593CECBC70FCCC200 (void);
// 0x0000003F System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::Encode(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Int32&,System.Int32&,System.Boolean)
extern void TextEncoder_Encode_mD4B2544A8B30FBA75511601A6672A95C262EB663 (void);
// 0x00000040 System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncode(System.ReadOnlySpan`1<System.Char>)
extern void TextEncoder_FindFirstCharacterToEncode_m055812C29995C1DFD2E2C899571736DBE5B28589 (void);
// 0x00000041 System.Int32 System.Text.Encodings.Web.TextEncoder::FindFirstCharacterToEncodeUtf8(System.ReadOnlySpan`1<System.Byte>)
extern void TextEncoder_FindFirstCharacterToEncodeUtf8_mAF4E9C94A751B36CE70A67B1324BDC2169D9FA9E (void);
// 0x00000042 System.Boolean System.Text.Encodings.Web.TextEncoder::TryCopyCharacters(System.Char[],System.Char*,System.Int32,System.Int32&)
extern void TextEncoder_TryCopyCharacters_mABB4D1DB6C503BBCDB5D1415084FD1A6CC35C782 (void);
// 0x00000043 System.Boolean System.Text.Encodings.Web.TextEncoder::TryWriteScalarAsChar(System.Int32,System.Char*,System.Int32,System.Int32&)
extern void TextEncoder_TryWriteScalarAsChar_mF2648F63D53C3FED74F34D3C49B3CE13D9F2C69C (void);
// 0x00000044 System.Byte[] System.Text.Encodings.Web.TextEncoder::GetAsciiEncoding(System.Byte)
extern void TextEncoder_GetAsciiEncoding_m4127218BE1A0242F780621DCF5A415826367C63C (void);
// 0x00000045 System.Void System.Text.Encodings.Web.TextEncoder::InitializeAsciiCache()
extern void TextEncoder_InitializeAsciiCache_m28C04EEA881CAF8616897BF1E3973C7FC768960B (void);
// 0x00000046 System.Boolean System.Text.Encodings.Web.TextEncoder::DoesAsciiNeedEncoding(System.UInt32)
extern void TextEncoder_DoesAsciiNeedEncoding_m75B40B8AADFFD6095825932C2599D47165B257D6 (void);
// 0x00000047 System.Void System.Text.Encodings.Web.TextEncoder::.ctor()
extern void TextEncoder__ctor_mAB973DBE5360A8CE416505D5F1487855B4A9B660 (void);
// 0x00000048 System.Void System.Text.Encodings.Web.TextEncoder::.cctor()
extern void TextEncoder__cctor_mC720029556F3DA6776FD769F91F97C585371CE17 (void);
// 0x00000049 System.Buffers.OperationStatus System.Text.Encodings.Web.TextEncoder::<Encode>g__EncodeCore|15_0(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Int32&,System.Int32&,System.Boolean)
extern void TextEncoder_U3CEncodeU3Eg__EncodeCoreU7C15_0_mEF12EEA0EC07785552E46509F3407967C95E9D26 (void);
// 0x0000004A System.Void System.Text.Encodings.Web.TextEncoderSettings::.ctor(System.Text.Unicode.UnicodeRange[])
extern void TextEncoderSettings__ctor_mF766697620B2A4E93EC4966FB24D3A90749695DB (void);
// 0x0000004B System.Void System.Text.Encodings.Web.TextEncoderSettings::AllowRange(System.Text.Unicode.UnicodeRange)
extern void TextEncoderSettings_AllowRange_mE18D9F922D41074A99B571237B152FA25E3CD466 (void);
// 0x0000004C System.Void System.Text.Encodings.Web.TextEncoderSettings::AllowRanges(System.Text.Unicode.UnicodeRange[])
extern void TextEncoderSettings_AllowRanges_m51D0C4BD65E72A1D7166C0CA74656AA1658D7580 (void);
// 0x0000004D System.Text.Internal.AllowedCharactersBitmap System.Text.Encodings.Web.TextEncoderSettings::GetAllowedCharacters()
extern void TextEncoderSettings_GetAllowedCharacters_mE45DB4EE7216198ED57CA3F0789011FE08851064 (void);
static Il2CppMethodPointer s_methodPointers[77] = 
{
	EmbeddedAttribute__ctor_m288BA0BFA12E5E462CB60C5B3D8BE3677EFF93F2,
	IsReadOnlyAttribute__ctor_m522246D01124BDBCC7B05E716AF928006F31508D,
	NullableAttribute__ctor_m74E75D3E2DC3E77488AB9C381D226214DF3CC2A0,
	NullableContextAttribute__ctor_m09AEB7692E9EE8247068756A53B49D700A2FBDF5,
	NullablePublicOnlyAttribute__ctor_m1EB5F8C417EC2EC2A3046C71684DACCA5E15D15F,
	HexConverter_ToCharUpper_mA514E6A22DBB36B2CE741A5026B23A45A10BC776,
	NotNullAttribute__ctor_m2F96337C648C3DF27411B9368A82219C8D6A630D,
	UnicodeUtility_GetScalarFromUtf16SurrogatePair_m6BF54197FBBFCDBE7172B89440194AE653351DE6,
	UnicodeUtility_GetUtf16SequenceLength_m0FE2252F5535FFF306564627DEFE355C5DF2CD9A,
	UnicodeUtility_IsAsciiCodePoint_mDE38F64B792FCDC8C910B91B7A5C7019ECBADA44,
	UnicodeUtility_IsHighSurrogateCodePoint_mF5DAC97DE50D6EFF231B3D3FD2EECFBCE1E97100,
	UnicodeUtility_IsInRangeInclusive_m1FFA10EDA34D1F09E82721521DCA5E2A2B41C0E8,
	UnicodeUtility_IsLowSurrogateCodePoint_mCF3C7B96F7CACD1657A588B8DC85CE957C50264B,
	UnicodeUtility_IsSurrogateCodePoint_mE6713FFE894D59D385CD071BB60A2618B68573C1,
	UnicodeHelpers_CreateDefinedCharacterBitmapMachineEndian_m23577DE0EFEA6B63D4EA8F7BB2732B2A8C87BD4F,
	UnicodeHelpers_DecodeScalarValueFromUtf8_mCE6926D9E56D292232835F9774218C999488202C,
	UnicodeHelpers_GetDefinedCharacterBitmap_m33E763520F56351D9F8B26EECBE5078FC0ADD34F,
	UnicodeHelpers_GetUtf16SurrogatePairFromAstralScalarValue_mC0A819940F8905BE73AD0B3826C508220B7D5D56,
	UnicodeHelpers_IsSupplementaryCodePoint_m5875C82A22F29496C03AB28B311E8321B8ED91F5,
	UnicodeHelpers_get_DefinedCharsBitmapSpan_mBF980F8D332E76BC8CEBCFEDD1233C8A2A45ABEF,
	UnicodeHelpers__cctor_mED57E4DE51568523E847C79EBF145D6DB10410B7,
	UnicodeRange__ctor_m55C49ADABCAD9160E480CD01439D639C24980AB5,
	UnicodeRange_get_FirstCodePoint_mA023CB7BCEE4210B23D6FBFA1B421D4C53F31D35,
	UnicodeRange_set_FirstCodePoint_m786BFD5158D01D1AD0073DFC0609231BAF4E5777,
	UnicodeRange_get_Length_m3664F9267AB716A55BB92139410B57B47C1AD154,
	UnicodeRange_set_Length_m7965F884FA95909701DC2125445C86B43B549720,
	UnicodeRange_Create_m45AF1389FADA69EF5C153C7FA1B09D9C77F469AD,
	UnicodeRanges_CreateRange_mD0D98535E601E77326370417B89B42D5C0FCACC3,
	UnicodeRanges_get_BasicLatin_mAB30D6E10C1E286D899D7C56E1049A418E6C4698,
	AllowedCharactersBitmap_CreateNew_m1068713B9D9E53E5A3D7D6056D70FE768A170484,
	AllowedCharactersBitmap__ctor_m84D288D533707728EBC3AA9F4D92A2A425CD4355,
	AllowedCharactersBitmap_AllowCharacter_mA62947598D488B8397D1B785EB8710A3C2502C24,
	AllowedCharactersBitmap_ForbidCharacter_mA96107773390A5E578FE51E57088924839152B65,
	AllowedCharactersBitmap_ForbidUndefinedCharacters_mD6CE16DBD95C181B9C36CF710079DFAC5AD7C639,
	AllowedCharactersBitmap_Clone_m0B4354E106746EBAB8C251B846BC07C7F8EED079,
	AllowedCharactersBitmap_IsCharacterAllowed_mBAD9A317E8F0EB7672C2C1C4D0F277F1F112E2D2,
	AllowedCharactersBitmap_IsUnicodeScalarAllowed_mCE82E28101DFD37C0376B042BFDBD5713EBB51A5,
	AllowedCharactersBitmap_FindFirstCharacterToEncode_m0E37005FCB6C9A200E34E8D7F82119933B12510C,
	DefaultJavaScriptEncoderBasicLatin__ctor_mA4678374F0BAF83C52194B6E675AB72725377972,
	DefaultJavaScriptEncoderBasicLatin_WillEncode_m4E53C5910E23CD0549705CF3B6316DBC39CABC1C,
	DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncode_mEE41382BC0EE3FBF57FCA8128B33CE14D0E60945,
	DefaultJavaScriptEncoderBasicLatin_FindFirstCharacterToEncodeUtf8_mDA508AED3C4D8AE936BBAA2F484DB8A692607F45,
	DefaultJavaScriptEncoderBasicLatin_TryEncodeUnicodeScalar_m2691077A7D3D766406F250A5F245DB9803CA041E,
	DefaultJavaScriptEncoderBasicLatin_get_AllowList_mCD3190A2592B682101C144D458558CCCAD7FB1C0,
	DefaultJavaScriptEncoderBasicLatin_NeedsEscaping_mD26E7F6A44E3E504E620A44D29A10578FE394486,
	DefaultJavaScriptEncoderBasicLatin__cctor_mB0E2A79C691082E6FE6A16A94D10CBAA29694FF4,
	HtmlEncoder__ctor_mB04BFEDA89DD7987924E2B8A75CD985F6EE71D02,
	DefaultHtmlEncoder__ctor_m0DF07C80DAB8C7B18B14176343114B086ECBBC0E,
	DefaultHtmlEncoder_ForbidHtmlCharacters_m01992C50627493B9A7302D591887147D34AD8591,
	DefaultHtmlEncoder_WillEncode_m714DB0F9F170F5E40C6635DC903C7F89C50DAD28,
	DefaultHtmlEncoder_FindFirstCharacterToEncode_m726646ED7E20FC2094AE1A133D31A6CE49BCC3F3,
	DefaultHtmlEncoder_TryEncodeUnicodeScalar_m39A2DC1D1C3D9F58799A1A64B9CD83780BFE6BB3,
	DefaultHtmlEncoder_TryWriteEncodedScalarAsNumericEntity_m2DD5A9DD47D14C87C0B3C36B8CFFA2CF8B0DCE27,
	DefaultHtmlEncoder__cctor_m8D91E212CFE007F9CDD669ADD799B15F0147BAD1,
	JavaScriptEncoder_get_Default_mA1BD7ACB1459CE7B06A32D6CF329C522C567D4ED,
	JavaScriptEncoder__ctor_m59B9FB268C118BDFC532CC01F6E9798DE75386B6,
	JavaScriptEncoderHelper_TryWriteEncodedScalarAsNumericEntity_mCC4DB3060EB6E69D1D42E8C01D2D53F9E95C0021,
	JavaScriptEncoderHelper_TryWriteEncodedSingleCharacter_mE7A5A0EF4960169B3B6B05E6E7118E41DF0BBAE5,
	NULL,
	NULL,
	NULL,
	TextEncoder_EncodeUtf8_mBDED5D080EC52F201E62664593CECBC70FCCC200,
	TextEncoder_Encode_mD4B2544A8B30FBA75511601A6672A95C262EB663,
	TextEncoder_FindFirstCharacterToEncode_m055812C29995C1DFD2E2C899571736DBE5B28589,
	TextEncoder_FindFirstCharacterToEncodeUtf8_mAF4E9C94A751B36CE70A67B1324BDC2169D9FA9E,
	TextEncoder_TryCopyCharacters_mABB4D1DB6C503BBCDB5D1415084FD1A6CC35C782,
	TextEncoder_TryWriteScalarAsChar_mF2648F63D53C3FED74F34D3C49B3CE13D9F2C69C,
	TextEncoder_GetAsciiEncoding_m4127218BE1A0242F780621DCF5A415826367C63C,
	TextEncoder_InitializeAsciiCache_m28C04EEA881CAF8616897BF1E3973C7FC768960B,
	TextEncoder_DoesAsciiNeedEncoding_m75B40B8AADFFD6095825932C2599D47165B257D6,
	TextEncoder__ctor_mAB973DBE5360A8CE416505D5F1487855B4A9B660,
	TextEncoder__cctor_mC720029556F3DA6776FD769F91F97C585371CE17,
	TextEncoder_U3CEncodeU3Eg__EncodeCoreU7C15_0_mEF12EEA0EC07785552E46509F3407967C95E9D26,
	TextEncoderSettings__ctor_mF766697620B2A4E93EC4966FB24D3A90749695DB,
	TextEncoderSettings_AllowRange_mE18D9F922D41074A99B571237B152FA25E3CD466,
	TextEncoderSettings_AllowRanges_m51D0C4BD65E72A1D7166C0CA74656AA1658D7580,
	TextEncoderSettings_GetAllowedCharacters_mE45DB4EE7216198ED57CA3F0789011FE08851064,
};
extern void AllowedCharactersBitmap__ctor_m84D288D533707728EBC3AA9F4D92A2A425CD4355_AdjustorThunk (void);
extern void AllowedCharactersBitmap_AllowCharacter_mA62947598D488B8397D1B785EB8710A3C2502C24_AdjustorThunk (void);
extern void AllowedCharactersBitmap_ForbidCharacter_mA96107773390A5E578FE51E57088924839152B65_AdjustorThunk (void);
extern void AllowedCharactersBitmap_ForbidUndefinedCharacters_mD6CE16DBD95C181B9C36CF710079DFAC5AD7C639_AdjustorThunk (void);
extern void AllowedCharactersBitmap_Clone_m0B4354E106746EBAB8C251B846BC07C7F8EED079_AdjustorThunk (void);
extern void AllowedCharactersBitmap_IsCharacterAllowed_mBAD9A317E8F0EB7672C2C1C4D0F277F1F112E2D2_AdjustorThunk (void);
extern void AllowedCharactersBitmap_IsUnicodeScalarAllowed_mCE82E28101DFD37C0376B042BFDBD5713EBB51A5_AdjustorThunk (void);
extern void AllowedCharactersBitmap_FindFirstCharacterToEncode_m0E37005FCB6C9A200E34E8D7F82119933B12510C_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x0600001F, AllowedCharactersBitmap__ctor_m84D288D533707728EBC3AA9F4D92A2A425CD4355_AdjustorThunk },
	{ 0x06000020, AllowedCharactersBitmap_AllowCharacter_mA62947598D488B8397D1B785EB8710A3C2502C24_AdjustorThunk },
	{ 0x06000021, AllowedCharactersBitmap_ForbidCharacter_mA96107773390A5E578FE51E57088924839152B65_AdjustorThunk },
	{ 0x06000022, AllowedCharactersBitmap_ForbidUndefinedCharacters_mD6CE16DBD95C181B9C36CF710079DFAC5AD7C639_AdjustorThunk },
	{ 0x06000023, AllowedCharactersBitmap_Clone_m0B4354E106746EBAB8C251B846BC07C7F8EED079_AdjustorThunk },
	{ 0x06000024, AllowedCharactersBitmap_IsCharacterAllowed_mBAD9A317E8F0EB7672C2C1C4D0F277F1F112E2D2_AdjustorThunk },
	{ 0x06000025, AllowedCharactersBitmap_IsUnicodeScalarAllowed_mCE82E28101DFD37C0376B042BFDBD5713EBB51A5_AdjustorThunk },
	{ 0x06000026, AllowedCharactersBitmap_FindFirstCharacterToEncode_m0E37005FCB6C9A200E34E8D7F82119933B12510C_AdjustorThunk },
};
static const int32_t s_InvokerIndices[77] = 
{
	9442,
	9442,
	7469,
	7469,
	7469,
	14150,
	9442,
	13028,
	13757,
	13612,
	13612,
	11573,
	13612,
	13612,
	14458,
	11623,
	14399,
	12127,
	13595,
	14394,
	14502,
	4002,
	9247,
	7557,
	9247,
	7557,
	12895,
	11756,
	14458,
	14411,
	7597,
	7716,
	7716,
	9442,
	9141,
	5601,
	5439,
	3044,
	9442,
	5439,
	3044,
	6173,
	911,
	14394,
	13611,
	14502,
	9442,
	7597,
	14236,
	5439,
	3044,
	911,
	10734,
	14502,
	14458,
	9442,
	10734,
	10734,
	0,
	0,
	0,
	476,
	477,
	6174,
	6173,
	10749,
	10734,
	6723,
	9442,
	5602,
	9442,
	14502,
	477,
	7597,
	7597,
	7597,
	9141,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Text_Encodings_Web_CodeGenModule;
const Il2CppCodeGenModule g_System_Text_Encodings_Web_CodeGenModule = 
{
	"System.Text.Encodings.Web.dll",
	77,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
