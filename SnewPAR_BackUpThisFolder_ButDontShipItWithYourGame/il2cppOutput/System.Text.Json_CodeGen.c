﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m57062416AA0B0C88D87FFE5636D49A61F7010DE4 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m8D50662C3DFF9E5EB44208928F00DAC3DE0D7D69 (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.IsByRefLikeAttribute::.ctor()
extern void IsByRefLikeAttribute__ctor_m03D16E2B157C52538B122D32A7008A2B8E74529B (void);
// 0x00000004 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
extern void NullableAttribute__ctor_m7287C1DF6EDAD6325A9A8B2581C1602640707D7B (void);
// 0x00000005 System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
extern void NullableContextAttribute__ctor_mDAA05787EC832A8F14D58A91E9DA3A7569B982D6 (void);
// 0x00000006 System.Void System.Runtime.CompilerServices.NullablePublicOnlyAttribute::.ctor(System.Boolean)
extern void NullablePublicOnlyAttribute__ctor_m3409379E5ED0F4B8D7DB99E48AC717C2D1677A55 (void);
// 0x00000007 System.Char System.HexConverter::ToCharUpper(System.Int32)
extern void HexConverter_ToCharUpper_m1EB698BB21EA438E644EF4A4ED2ABD5AB90C4929 (void);
// 0x00000008 System.Int32 System.HexConverter::FromChar(System.Int32)
extern void HexConverter_FromChar_m550EA11F67C43F1430F5AA8A3CD991D8062C9AD9 (void);
// 0x00000009 System.Boolean System.HexConverter::IsHexChar(System.Int32)
extern void HexConverter_IsHexChar_mC6DAD65E9943236A7C67BDD73A19B77D25CCD6FA (void);
// 0x0000000A System.ReadOnlySpan`1<System.Byte> System.HexConverter::get_CharToHexLookup()
extern void HexConverter_get_CharToHexLookup_m734AAFFA259562CE33DB2D34AED8E0B679145E5E (void);
// 0x0000000B System.Boolean System.SR::UsingResourceKeys()
extern void SR_UsingResourceKeys_mDC2DAF1D8F4990443E9DA23C5AA2441DCF0C5CD3 (void);
// 0x0000000C System.String System.SR::GetResourceString(System.String,System.String)
extern void SR_GetResourceString_m02D10C3BA46D92C0D4F70D01FC25DFD4F3F4E276 (void);
// 0x0000000D System.String System.SR::Format(System.String,System.Object)
extern void SR_Format_mD6D7C45289B8602CA7F2EA0692244E53FF55C8DF (void);
// 0x0000000E System.String System.SR::Format(System.String,System.Object,System.Object)
extern void SR_Format_m3477D4175CB8E27C4C1753CE70687768BBD2B60F (void);
// 0x0000000F System.String System.SR::Format(System.String,System.Object,System.Object,System.Object)
extern void SR_Format_mBA031A77F7C9E51B2BCDC8385FEE7AFD795C8474 (void);
// 0x00000010 System.String System.SR::Format(System.String,System.Object[])
extern void SR_Format_m4E2280EF70B69103437BCADA5DDAB7C825A3DA07 (void);
// 0x00000011 System.Resources.ResourceManager System.SR::get_ResourceManager()
extern void SR_get_ResourceManager_m583D2FBABEDB90189BE00E73DE8E9B1D132C9D10 (void);
// 0x00000012 System.String System.SR::get_ArrayDepthTooLarge()
extern void SR_get_ArrayDepthTooLarge_m42201D2763F3CCD2A8914674917E70355F639DFC (void);
// 0x00000013 System.String System.SR::get_CannotReadIncompleteUTF16()
extern void SR_get_CannotReadIncompleteUTF16_m944367C7ECB4D121DCDB34E023E29C253555B85D (void);
// 0x00000014 System.String System.SR::get_CannotReadInvalidUTF16()
extern void SR_get_CannotReadInvalidUTF16_mF95679BADED663876D59BCC0C8CA683D3C3ED6DB (void);
// 0x00000015 System.String System.SR::get_CannotStartObjectArrayAfterPrimitiveOrClose()
extern void SR_get_CannotStartObjectArrayAfterPrimitiveOrClose_m8777602CBE6CB87E09CF87E63C5ECABEC1E57433 (void);
// 0x00000016 System.String System.SR::get_CannotStartObjectArrayWithoutProperty()
extern void SR_get_CannotStartObjectArrayWithoutProperty_mFED3E92F24ACF5FF6D753EBA6E89559E64DDE44A (void);
// 0x00000017 System.String System.SR::get_CannotTranscodeInvalidUtf8()
extern void SR_get_CannotTranscodeInvalidUtf8_m35D92692E6BC08640662F9B9E8FE8209B3E84579 (void);
// 0x00000018 System.String System.SR::get_CannotDecodeInvalidBase64()
extern void SR_get_CannotDecodeInvalidBase64_mBAD093ED9F405A8684ACA1D593A12F592CC824EE (void);
// 0x00000019 System.String System.SR::get_CannotTranscodeInvalidUtf16()
extern void SR_get_CannotTranscodeInvalidUtf16_mE65FCC4D0FF86F063F074ED5AC4E0A8B890D9619 (void);
// 0x0000001A System.String System.SR::get_CannotEncodeInvalidUTF16()
extern void SR_get_CannotEncodeInvalidUTF16_m475504A3662BB4D3BADF25DE623556C3F1DB7A45 (void);
// 0x0000001B System.String System.SR::get_CannotEncodeInvalidUTF8()
extern void SR_get_CannotEncodeInvalidUTF8_m1F969CCE265A1A7886F4413856F63EDB7E08D8F7 (void);
// 0x0000001C System.String System.SR::get_CannotWritePropertyWithinArray()
extern void SR_get_CannotWritePropertyWithinArray_m98A0BFB0A4DE2AB2B60FEF2F2F6E12B2E0A3356C (void);
// 0x0000001D System.String System.SR::get_CannotWritePropertyAfterProperty()
extern void SR_get_CannotWritePropertyAfterProperty_m9496F5B2837CF6C844126AE533AECF3466393437 (void);
// 0x0000001E System.String System.SR::get_CannotWriteValueAfterPrimitiveOrClose()
extern void SR_get_CannotWriteValueAfterPrimitiveOrClose_mB63AADF2F8301C81C5B7E11C0EE43EA067708AEF (void);
// 0x0000001F System.String System.SR::get_CannotWriteValueWithinObject()
extern void SR_get_CannotWriteValueWithinObject_mA5FA81DA8CF33F1A208D619F7FA0EC0736B33F54 (void);
// 0x00000020 System.String System.SR::get_DepthTooLarge()
extern void SR_get_DepthTooLarge_mB0589B173A594380338004E7489B57DE41162AAC (void);
// 0x00000021 System.String System.SR::get_EndOfCommentNotFound()
extern void SR_get_EndOfCommentNotFound_m9B50FF177B87E1EE818EDEE2F8DFDDCC9D2BF9F8 (void);
// 0x00000022 System.String System.SR::get_EndOfStringNotFound()
extern void SR_get_EndOfStringNotFound_mF6AFA0E2F889B80281BC679767D03ABD159D0624 (void);
// 0x00000023 System.String System.SR::get_ExpectedEndAfterSingleJson()
extern void SR_get_ExpectedEndAfterSingleJson_mC8A25E8EAD9414C376832A43B8906C6EC84A434D (void);
// 0x00000024 System.String System.SR::get_ExpectedEndOfDigitNotFound()
extern void SR_get_ExpectedEndOfDigitNotFound_mFC63AB23ACA9E4C5126FABF43654FD62552F1BB1 (void);
// 0x00000025 System.String System.SR::get_ExpectedFalse()
extern void SR_get_ExpectedFalse_mAFD561299B6913F6BBBEF585B766D11AC1365E94 (void);
// 0x00000026 System.String System.SR::get_ExpectedJsonTokens()
extern void SR_get_ExpectedJsonTokens_mB32485B7F496984825183B6B0E337F72E5497959 (void);
// 0x00000027 System.String System.SR::get_ExpectedOneCompleteToken()
extern void SR_get_ExpectedOneCompleteToken_mB2B9B8F5110736B8CEDD7264740F842CD5B8684E (void);
// 0x00000028 System.String System.SR::get_ExpectedNextDigitEValueNotFound()
extern void SR_get_ExpectedNextDigitEValueNotFound_m3CD5E5989BC1D79D2D9C672BF5A0FA5366AF2B79 (void);
// 0x00000029 System.String System.SR::get_ExpectedNull()
extern void SR_get_ExpectedNull_m1F6D91F25CB3CB209FB0FD387C4ED2C6B5F13F24 (void);
// 0x0000002A System.String System.SR::get_ExpectedSeparatorAfterPropertyNameNotFound()
extern void SR_get_ExpectedSeparatorAfterPropertyNameNotFound_m094EC61DD8D2B78F8A324EE5167815927B5446F6 (void);
// 0x0000002B System.String System.SR::get_ExpectedStartOfPropertyNotFound()
extern void SR_get_ExpectedStartOfPropertyNotFound_mE619D505227886A17E58271844782E833C8FF32C (void);
// 0x0000002C System.String System.SR::get_ExpectedStartOfPropertyOrValueNotFound()
extern void SR_get_ExpectedStartOfPropertyOrValueNotFound_mD895BA183797E6136DC12277A96C345EE853988F (void);
// 0x0000002D System.String System.SR::get_ExpectedStartOfValueNotFound()
extern void SR_get_ExpectedStartOfValueNotFound_mAC2C01E3639D1E3CA30EF277EAD9B42BE8AAADC9 (void);
// 0x0000002E System.String System.SR::get_ExpectedTrue()
extern void SR_get_ExpectedTrue_m6606A88E12781E1E74F42766367AF157AFC1A45A (void);
// 0x0000002F System.String System.SR::get_ExpectedValueAfterPropertyNameNotFound()
extern void SR_get_ExpectedValueAfterPropertyNameNotFound_mA12BC622B845109BEDC4BA0709E5C07E77A960CA (void);
// 0x00000030 System.String System.SR::get_FailedToGetLargerSpan()
extern void SR_get_FailedToGetLargerSpan_mC6A78FD7DBF417802D87AB7CF4BFCE9D1262DEC1 (void);
// 0x00000031 System.String System.SR::get_FoundInvalidCharacter()
extern void SR_get_FoundInvalidCharacter_m684715313507A3908483C9701A966AE7F2020201 (void);
// 0x00000032 System.String System.SR::get_InvalidCast()
extern void SR_get_InvalidCast_m4765E42F23B957FD767339E6E82F985954E8049A (void);
// 0x00000033 System.String System.SR::get_InvalidCharacterAfterEscapeWithinString()
extern void SR_get_InvalidCharacterAfterEscapeWithinString_mB8E8F0BA4CDE2240D12ADB05739FC35B778061AC (void);
// 0x00000034 System.String System.SR::get_InvalidCharacterWithinString()
extern void SR_get_InvalidCharacterWithinString_m4A6DD434A1E5DFAC1D3A723D15F3125E63B6AE5D (void);
// 0x00000035 System.String System.SR::get_InvalidEndOfJsonNonPrimitive()
extern void SR_get_InvalidEndOfJsonNonPrimitive_m663B13FD3A43B1274D418CC8B9D6B9C529A2B2F2 (void);
// 0x00000036 System.String System.SR::get_InvalidHexCharacterWithinString()
extern void SR_get_InvalidHexCharacterWithinString_m5DCE6F8C8568A4492B5E24F404037A8686687987 (void);
// 0x00000037 System.String System.SR::get_JsonDocumentDoesNotSupportComments()
extern void SR_get_JsonDocumentDoesNotSupportComments_mA201632FDA65628C0896F111966E8ECC344274D7 (void);
// 0x00000038 System.String System.SR::get_JsonElementHasWrongType()
extern void SR_get_JsonElementHasWrongType_m1D09105C4084618BB38D6CB31CAB03BAF2FD01A2 (void);
// 0x00000039 System.String System.SR::get_MaxDepthMustBePositive()
extern void SR_get_MaxDepthMustBePositive_mEF176814E001949851FEDD2D9236DF6EA256FD86 (void);
// 0x0000003A System.String System.SR::get_CommentHandlingMustBeValid()
extern void SR_get_CommentHandlingMustBeValid_m054623C5A1C932ABC065112C9C7805A9C6622F9C (void);
// 0x0000003B System.String System.SR::get_MismatchedObjectArray()
extern void SR_get_MismatchedObjectArray_m46D72F2BA23CD73AE10B67EB45802D16FDA45F58 (void);
// 0x0000003C System.String System.SR::get_CannotWriteEndAfterProperty()
extern void SR_get_CannotWriteEndAfterProperty_mE0DBAD298E5211671FDCAE72140F59DE09286064 (void);
// 0x0000003D System.String System.SR::get_ObjectDepthTooLarge()
extern void SR_get_ObjectDepthTooLarge_m2E9BDB6520C02291742E6F7B3BF1B7D7127AC2FB (void);
// 0x0000003E System.String System.SR::get_PropertyNameTooLarge()
extern void SR_get_PropertyNameTooLarge_mAD27EA72AE6A6687579BC45EA2729F6A5145594A (void);
// 0x0000003F System.String System.SR::get_FormatDecimal()
extern void SR_get_FormatDecimal_mFE14407BA8C956175CD21C4C8FC78D506894A07B (void);
// 0x00000040 System.String System.SR::get_FormatDouble()
extern void SR_get_FormatDouble_m47646195A9A67DA6AAE37E57E33E52609C26E789 (void);
// 0x00000041 System.String System.SR::get_FormatInt32()
extern void SR_get_FormatInt32_m2D9194959286497E57C3D2FEBEC6ECEF606A0545 (void);
// 0x00000042 System.String System.SR::get_FormatInt64()
extern void SR_get_FormatInt64_m94CE8F1D75F5ACD76A91027BB9BA6DA1633076F3 (void);
// 0x00000043 System.String System.SR::get_FormatSingle()
extern void SR_get_FormatSingle_m2E5BAC80D30378742097778A48AE38BD379DC539 (void);
// 0x00000044 System.String System.SR::get_FormatUInt32()
extern void SR_get_FormatUInt32_m0A8D6D4FF312A518DE93DF736154B88E09300257 (void);
// 0x00000045 System.String System.SR::get_FormatUInt64()
extern void SR_get_FormatUInt64_m92955C632B6C395C1551C5D44E954BA92A534607 (void);
// 0x00000046 System.String System.SR::get_RequiredDigitNotFoundAfterDecimal()
extern void SR_get_RequiredDigitNotFoundAfterDecimal_mFE209CED59EA456CC2B949D7E841B13DBC8AB3E7 (void);
// 0x00000047 System.String System.SR::get_RequiredDigitNotFoundAfterSign()
extern void SR_get_RequiredDigitNotFoundAfterSign_mBD2EEFCE2750E5DDF34B0B865985888ADFD9E947 (void);
// 0x00000048 System.String System.SR::get_RequiredDigitNotFoundEndOfData()
extern void SR_get_RequiredDigitNotFoundEndOfData_m703BDAE0498CB850C64A5A49A87A314F03F02E37 (void);
// 0x00000049 System.String System.SR::get_SpecialNumberValuesNotSupported()
extern void SR_get_SpecialNumberValuesNotSupported_m6B42A74BCA4B396AA9BF6CD2E072C5D38B82E97A (void);
// 0x0000004A System.String System.SR::get_ValueTooLarge()
extern void SR_get_ValueTooLarge_m63BD834F730B5047341CF85B5B6433569B12FC56 (void);
// 0x0000004B System.String System.SR::get_ZeroDepthAtEnd()
extern void SR_get_ZeroDepthAtEnd_m84EF3B9084B657AD36F6F4DF561DEF9BC2C54361 (void);
// 0x0000004C System.String System.SR::get_DeserializeUnableToConvertValue()
extern void SR_get_DeserializeUnableToConvertValue_m0E0B9A3CA7AD02904B530584D637BDEAB96DB77E (void);
// 0x0000004D System.String System.SR::get_BufferWriterAdvancedTooFar()
extern void SR_get_BufferWriterAdvancedTooFar_m99BE0E80A2137618B1DB1C71DDD9E5C4B9C68C40 (void);
// 0x0000004E System.String System.SR::get_FormatDateTime()
extern void SR_get_FormatDateTime_m7EFB48E294A725881CB03632A563946061D649A7 (void);
// 0x0000004F System.String System.SR::get_FormatDateTimeOffset()
extern void SR_get_FormatDateTimeOffset_m9849F3417114F1E724BA31702FE8B2F153728155 (void);
// 0x00000050 System.String System.SR::get_FormatGuid()
extern void SR_get_FormatGuid_m7A1A536CA15EC5BBA8DC92C49CA44D7C38EC2863 (void);
// 0x00000051 System.String System.SR::get_ExpectedStartOfPropertyOrValueAfterComment()
extern void SR_get_ExpectedStartOfPropertyOrValueAfterComment_mED814A86D624CAA0430521B7D48534D6327DF923 (void);
// 0x00000052 System.String System.SR::get_TrailingCommaNotAllowedBeforeArrayEnd()
extern void SR_get_TrailingCommaNotAllowedBeforeArrayEnd_mB904FDB19B7E0F9BA59D359BF0C01C3B3F857A6B (void);
// 0x00000053 System.String System.SR::get_TrailingCommaNotAllowedBeforeObjectEnd()
extern void SR_get_TrailingCommaNotAllowedBeforeObjectEnd_mE3B14EFBD3D465B6553FCE68330AAE478938AA13 (void);
// 0x00000054 System.String System.SR::get_SerializerOptionsImmutable()
extern void SR_get_SerializerOptionsImmutable_mF1C8D5D0819B80B783A9DE9E0438360A1FCC200C (void);
// 0x00000055 System.String System.SR::get_SerializerPropertyNameConflict()
extern void SR_get_SerializerPropertyNameConflict_m311ABE143B44E390CBBD6FEF07D37DD1E1E97DA3 (void);
// 0x00000056 System.String System.SR::get_SerializerPropertyNameNull()
extern void SR_get_SerializerPropertyNameNull_m363B36EFBEDDBAF7970311F3D75FC9FF997D94A4 (void);
// 0x00000057 System.String System.SR::get_SerializationDataExtensionPropertyInvalid()
extern void SR_get_SerializationDataExtensionPropertyInvalid_mAA7EF613733E82BDCD57647BB7ABBAD13FBAD413 (void);
// 0x00000058 System.String System.SR::get_SerializationDuplicateTypeAttribute()
extern void SR_get_SerializationDuplicateTypeAttribute_m6C887253D35FA1258A842F35411881519909BE4A (void);
// 0x00000059 System.String System.SR::get_SerializationNotSupportedType()
extern void SR_get_SerializationNotSupportedType_m50366E9C21059F0A4A4A26E910B5FAEC3774CA85 (void);
// 0x0000005A System.String System.SR::get_InvalidCharacterAtStartOfComment()
extern void SR_get_InvalidCharacterAtStartOfComment_m3659D1047146A39D588F0971CD5A3962D6B81942 (void);
// 0x0000005B System.String System.SR::get_UnexpectedEndOfDataWhileReadingComment()
extern void SR_get_UnexpectedEndOfDataWhileReadingComment_m4F1285062C69545771CA99434210FAD9EA2C452E (void);
// 0x0000005C System.String System.SR::get_CannotSkip()
extern void SR_get_CannotSkip_mCB40E4DCC66433C0B5CCEF72F551567A5A56571E (void);
// 0x0000005D System.String System.SR::get_NotEnoughData()
extern void SR_get_NotEnoughData_mA27E56766957D9E955DAA6181E413FBCA89320FF (void);
// 0x0000005E System.String System.SR::get_UnexpectedEndOfLineSeparator()
extern void SR_get_UnexpectedEndOfLineSeparator_m8A21E48439B2859498B1D7027054B6B296C98DFE (void);
// 0x0000005F System.String System.SR::get_DeserializeNoConstructor()
extern void SR_get_DeserializeNoConstructor_m9BDE5BD3E9788C75F6AC924AFF86CC88DAD20C7E (void);
// 0x00000060 System.String System.SR::get_DeserializePolymorphicInterface()
extern void SR_get_DeserializePolymorphicInterface_mC7A869FEEF8FF96D7F2A022827DFBBF8E63C7F51 (void);
// 0x00000061 System.String System.SR::get_SerializationConverterOnAttributeNotCompatible()
extern void SR_get_SerializationConverterOnAttributeNotCompatible_m3F1415FA25600BE6C4A54093A4B8D4CE8B6D68EE (void);
// 0x00000062 System.String System.SR::get_SerializationConverterOnAttributeInvalid()
extern void SR_get_SerializationConverterOnAttributeInvalid_m299BA13F75F2A8CFC6B0A5D312630D911E1D2074 (void);
// 0x00000063 System.String System.SR::get_SerializationConverterRead()
extern void SR_get_SerializationConverterRead_mE5BF4C7022874FAB823D03F78710AFABFD074E75 (void);
// 0x00000064 System.String System.SR::get_SerializationConverterNotCompatible()
extern void SR_get_SerializationConverterNotCompatible_m137B152C460F8E6DE9C830EC8BBE8D901069A30B (void);
// 0x00000065 System.String System.SR::get_SerializationConverterWrite()
extern void SR_get_SerializationConverterWrite_m4E4ADAEB485B27F9A1E090A3EEB64FB0EE408ED0 (void);
// 0x00000066 System.String System.SR::get_NamingPolicyReturnNull()
extern void SR_get_NamingPolicyReturnNull_m117D80023B270D57599E1F14D1898B64DC3895EF (void);
// 0x00000067 System.String System.SR::get_SerializationDuplicateAttribute()
extern void SR_get_SerializationDuplicateAttribute_m12970C66F746F878F2C6C151CD3B6224C8A081A9 (void);
// 0x00000068 System.String System.SR::get_SerializeUnableToSerialize()
extern void SR_get_SerializeUnableToSerialize_m55B1E260F720800E11E001569DA2A06201F42C18 (void);
// 0x00000069 System.String System.SR::get_FormatByte()
extern void SR_get_FormatByte_m3D487820B74021CBEEBAEE3AECFCBBBB7E63E16D (void);
// 0x0000006A System.String System.SR::get_FormatInt16()
extern void SR_get_FormatInt16_m9841915E590428B04F883DA31ED8CAC7167FFF90 (void);
// 0x0000006B System.String System.SR::get_FormatSByte()
extern void SR_get_FormatSByte_mB243F9693025F63AE7D97F4C9E2B9DD285D1FDE3 (void);
// 0x0000006C System.String System.SR::get_FormatUInt16()
extern void SR_get_FormatUInt16_m3D2E561525B5C247DCCEC1C78C983A3550AB3E68 (void);
// 0x0000006D System.String System.SR::get_SerializerCycleDetected()
extern void SR_get_SerializerCycleDetected_m94EAC7F070FEB6C0A113F0104C119E5A49711CB2 (void);
// 0x0000006E System.String System.SR::get_InvalidLeadingZeroInNumber()
extern void SR_get_InvalidLeadingZeroInNumber_mAEE042FD33F97A117F9A97549FECDBA75B23A726 (void);
// 0x0000006F System.String System.SR::get_MetadataCannotParsePreservedObjectToImmutable()
extern void SR_get_MetadataCannotParsePreservedObjectToImmutable_m30326102F9C88F587E1F5B28F397FBFE48AA3167 (void);
// 0x00000070 System.String System.SR::get_MetadataDuplicateIdFound()
extern void SR_get_MetadataDuplicateIdFound_mBB6FC2B830F7E293EB1FF18DBBAD5C2D81AA8BDB (void);
// 0x00000071 System.String System.SR::get_MetadataIdIsNotFirstProperty()
extern void SR_get_MetadataIdIsNotFirstProperty_mC2DB7C2423B816C5D010FEA1F1EA3ECC7E8C6BE2 (void);
// 0x00000072 System.String System.SR::get_MetadataInvalidReferenceToValueType()
extern void SR_get_MetadataInvalidReferenceToValueType_m5A739ADDEBE9ECB04FAB1EE067D4CCF7DAE05F6F (void);
// 0x00000073 System.String System.SR::get_MetadataInvalidTokenAfterValues()
extern void SR_get_MetadataInvalidTokenAfterValues_m95B2159ECC07F378B9F9DA8E9992343DB109C67D (void);
// 0x00000074 System.String System.SR::get_MetadataPreservedArrayFailed()
extern void SR_get_MetadataPreservedArrayFailed_mD377318C7455ECBDAAAB61DBDCD0AAB974EFF207 (void);
// 0x00000075 System.String System.SR::get_MetadataPreservedArrayInvalidProperty()
extern void SR_get_MetadataPreservedArrayInvalidProperty_mBD3E16B2BAFD2B9883530B2F8660EAC0FA2CAD66 (void);
// 0x00000076 System.String System.SR::get_MetadataPreservedArrayPropertyNotFound()
extern void SR_get_MetadataPreservedArrayPropertyNotFound_m20136FD7E40494CC5BCC48FC6B15F2321A641616 (void);
// 0x00000077 System.String System.SR::get_MetadataReferenceCannotContainOtherProperties()
extern void SR_get_MetadataReferenceCannotContainOtherProperties_mFFCE2478326F28E2C3C6E18ECEF81E4E0F29DCAF (void);
// 0x00000078 System.String System.SR::get_MetadataReferenceNotFound()
extern void SR_get_MetadataReferenceNotFound_m0D072B743C9AD166D347B2F70FE0043EA5F1B6E5 (void);
// 0x00000079 System.String System.SR::get_MetadataValueWasNotString()
extern void SR_get_MetadataValueWasNotString_m8AFEEF170E2202B2D3BDEBC53DEDE33A2C706D02 (void);
// 0x0000007A System.String System.SR::get_MetadataInvalidPropertyWithLeadingDollarSign()
extern void SR_get_MetadataInvalidPropertyWithLeadingDollarSign_mADECBFF91824081CAE7961C3F64B5B5BE0FC2BFB (void);
// 0x0000007B System.String System.SR::get_MultipleMembersBindWithConstructorParameter()
extern void SR_get_MultipleMembersBindWithConstructorParameter_mAEC260022C23BF38ADCA1B767B60B53E8875D572 (void);
// 0x0000007C System.String System.SR::get_ConstructorParamIncompleteBinding()
extern void SR_get_ConstructorParamIncompleteBinding_mC581DBF82C64641E6BC0B40CC8DA5CB47E6D7A88 (void);
// 0x0000007D System.String System.SR::get_ConstructorMaxOf64Parameters()
extern void SR_get_ConstructorMaxOf64Parameters_mBB5AFF1FEDD827305F95B3117F73C460627249FD (void);
// 0x0000007E System.String System.SR::get_ObjectWithParameterizedCtorRefMetadataNotHonored()
extern void SR_get_ObjectWithParameterizedCtorRefMetadataNotHonored_m8996B39C9B5E96C4A95B624B0548B660F6391713 (void);
// 0x0000007F System.String System.SR::get_SerializerConverterFactoryReturnsNull()
extern void SR_get_SerializerConverterFactoryReturnsNull_m3118B7812A487C6358D67C3FCB52D9037459F63F (void);
// 0x00000080 System.String System.SR::get_SerializationNotSupportedParentType()
extern void SR_get_SerializationNotSupportedParentType_m07FAC518AD295B0A26B231A1BCA47442AB6B3D9A (void);
// 0x00000081 System.String System.SR::get_ExtensionDataCannotBindToCtorParam()
extern void SR_get_ExtensionDataCannotBindToCtorParam_m420FD3FD24F813F00880D64F68AECA269125BE47 (void);
// 0x00000082 System.String System.SR::get_BufferMaximumSizeExceeded()
extern void SR_get_BufferMaximumSizeExceeded_m431F865C69355E6C41A8E3F80FFFBF7AC3FA4EC8 (void);
// 0x00000083 System.String System.SR::get_CannotSerializeInvalidType()
extern void SR_get_CannotSerializeInvalidType_m7C952A313DC1993F9B3232F38D6C80D3D046678B (void);
// 0x00000084 System.String System.SR::get_SerializeTypeInstanceNotSupported()
extern void SR_get_SerializeTypeInstanceNotSupported_m2AB8B23DAF1CD7C6AEBDE5F2CCDE4285F1575ACE (void);
// 0x00000085 System.String System.SR::get_JsonIncludeOnNonPublicInvalid()
extern void SR_get_JsonIncludeOnNonPublicInvalid_mF62600692DFAB089F35D4F093D1E87A75BC25B0E (void);
// 0x00000086 System.String System.SR::get_CannotSerializeInvalidMember()
extern void SR_get_CannotSerializeInvalidMember_mBD7FBF18C19F170E88B5992329F0ECC8422BA434 (void);
// 0x00000087 System.String System.SR::get_CannotPopulateCollection()
extern void SR_get_CannotPopulateCollection_mB97D398A7585F7EB1436376EE5878C80D9DCB5FC (void);
// 0x00000088 System.String System.SR::get_FormatBoolean()
extern void SR_get_FormatBoolean_mE269628FC3DFBA7EDD02281BE138A1C3CE081DC1 (void);
// 0x00000089 System.String System.SR::get_DictionaryKeyTypeNotSupported()
extern void SR_get_DictionaryKeyTypeNotSupported_mDA7E2E872C601CC7EB461C2C484FEBCFE92556AB (void);
// 0x0000008A System.String System.SR::get_IgnoreConditionOnValueTypeInvalid()
extern void SR_get_IgnoreConditionOnValueTypeInvalid_m7419DD1B9527C4EE15F293E640157C4B1D4D6AF2 (void);
// 0x0000008B System.String System.SR::get_NumberHandlingConverterMustBeBuiltIn()
extern void SR_get_NumberHandlingConverterMustBeBuiltIn_m27905EE924069B6B7F7075BCAE99EDB79F97EF93 (void);
// 0x0000008C System.String System.SR::get_NumberHandlingOnPropertyTypeMustBeNumberOrCollection()
extern void SR_get_NumberHandlingOnPropertyTypeMustBeNumberOrCollection_m8A2EADDB6529F46977892B674E769ED6EC2A2C4A (void);
// 0x0000008D System.String System.SR::get_ConverterCanConvertNullableRedundant()
extern void SR_get_ConverterCanConvertNullableRedundant_mFB45D61F9A1E72FC84765E2221D49BA9245E027B (void);
// 0x0000008E System.String System.SR::get_MetadataReferenceOfTypeCannotBeAssignedToType()
extern void SR_get_MetadataReferenceOfTypeCannotBeAssignedToType_m7F1BF09FD2E7834FE2710A120298FCCDFA1C2CA4 (void);
// 0x0000008F System.String System.SR::get_DeserializeUnableToAssignValue()
extern void SR_get_DeserializeUnableToAssignValue_m8285893FC06745EF5DAF15239A53254EC2EF0642 (void);
// 0x00000090 System.String System.SR::get_DeserializeUnableToAssignNull()
extern void SR_get_DeserializeUnableToAssignNull_m255E8E315DEE64BA5151D37E7DA15F4EBE971CA0 (void);
// 0x00000091 System.Void System.SR::.cctor()
extern void SR__cctor_mE3BD13B00C21BC7B674A7F3CC07024B3553DC202 (void);
// 0x00000092 System.Void System.Collections.Generic.ReferenceEqualityComparer::.ctor()
extern void ReferenceEqualityComparer__ctor_m34B573993FD142FE3743042DE404EE5B460B87F7 (void);
// 0x00000093 System.Collections.Generic.ReferenceEqualityComparer System.Collections.Generic.ReferenceEqualityComparer::get_Instance()
extern void ReferenceEqualityComparer_get_Instance_mD073B163F8FCF8802F3DD0710ADE60E4AE9C70D1 (void);
// 0x00000094 System.Boolean System.Collections.Generic.ReferenceEqualityComparer::Equals(System.Object,System.Object)
extern void ReferenceEqualityComparer_Equals_mD317F0D5B41F522C68D89521F8B9BE893846886D (void);
// 0x00000095 System.Int32 System.Collections.Generic.ReferenceEqualityComparer::GetHashCode(System.Object)
extern void ReferenceEqualityComparer_GetHashCode_m1D4FDFFFD7E03A6556AB19067C3C22798B96FE1A (void);
// 0x00000096 System.Void System.Collections.Generic.ReferenceEqualityComparer::.cctor()
extern void ReferenceEqualityComparer__cctor_mF61AE9229CB4EE4FDB0A7F1D8F10062C66F837F9 (void);
// 0x00000097 System.Void System.Diagnostics.CodeAnalysis.DynamicallyAccessedMembersAttribute::.ctor(System.Diagnostics.CodeAnalysis.DynamicallyAccessedMemberTypes)
extern void DynamicallyAccessedMembersAttribute__ctor_m09C8E63DE16193F2BF50CA9B0013974A2BD3F117 (void);
// 0x00000098 System.Void System.Diagnostics.CodeAnalysis.DisallowNullAttribute::.ctor()
extern void DisallowNullAttribute__ctor_m219EE54312A960E5EF2A2EC11B8CE905B2E74AA5 (void);
// 0x00000099 System.Void System.Diagnostics.CodeAnalysis.MaybeNullWhenAttribute::.ctor(System.Boolean)
extern void MaybeNullWhenAttribute__ctor_mE1D907652B18CE48FAF3ED10DA1C5B2E4394C64A (void);
// 0x0000009A System.Void System.Diagnostics.CodeAnalysis.NotNullWhenAttribute::.ctor(System.Boolean)
extern void NotNullWhenAttribute__ctor_mBA27378DCF373BEC96DA61866A9B2ECA6E4A9963 (void);
// 0x0000009B System.Void System.Diagnostics.CodeAnalysis.NotNullIfNotNullAttribute::.ctor(System.String)
extern void NotNullIfNotNullAttribute__ctor_mBE4276E7861F6AFF93745199B2CA3E919DE0CB2E (void);
// 0x0000009C System.Void System.Diagnostics.CodeAnalysis.DoesNotReturnAttribute::.ctor()
extern void DoesNotReturnAttribute__ctor_m147B1C7E5EF5B98AB3161A70862E2F53A233F5B3 (void);
// 0x0000009D System.ReadOnlyMemory`1<T> System.Buffers.ArrayBufferWriter`1::get_WrittenMemory()
// 0x0000009E System.Int32 System.Buffers.ArrayBufferWriter`1::get_WrittenCount()
// 0x0000009F System.Int32 System.Buffers.ArrayBufferWriter`1::get_FreeCapacity()
// 0x000000A0 System.Void System.Buffers.ArrayBufferWriter`1::Clear()
// 0x000000A1 System.Void System.Buffers.ArrayBufferWriter`1::Advance(System.Int32)
// 0x000000A2 System.Memory`1<T> System.Buffers.ArrayBufferWriter`1::GetMemory(System.Int32)
// 0x000000A3 System.Void System.Buffers.ArrayBufferWriter`1::CheckAndResizeBuffer(System.Int32)
// 0x000000A4 System.Void System.Buffers.ArrayBufferWriter`1::ThrowInvalidOperationException_AdvancedTooFar(System.Int32)
// 0x000000A5 System.Void System.Buffers.ArrayBufferWriter`1::ThrowOutOfMemoryException(System.UInt32)
// 0x000000A6 System.Void System.Text.Json.PooledByteBufferWriter::.ctor(System.Int32)
extern void PooledByteBufferWriter__ctor_mA2CDADEA601F4B449CB29C59BC45CB30F460F895 (void);
// 0x000000A7 System.ReadOnlyMemory`1<System.Byte> System.Text.Json.PooledByteBufferWriter::get_WrittenMemory()
extern void PooledByteBufferWriter_get_WrittenMemory_m9EA1D7168A9612DF33F3DE0F679F3B8B7A0632A0 (void);
// 0x000000A8 System.Void System.Text.Json.PooledByteBufferWriter::ClearHelper()
extern void PooledByteBufferWriter_ClearHelper_mBD6BA55E55056AC390D8C66A4F237243D6877D61 (void);
// 0x000000A9 System.Void System.Text.Json.PooledByteBufferWriter::Dispose()
extern void PooledByteBufferWriter_Dispose_m8706065BB3DE3495BA0AF0900DC3E8B065851BDD (void);
// 0x000000AA System.Void System.Text.Json.PooledByteBufferWriter::Advance(System.Int32)
extern void PooledByteBufferWriter_Advance_m2D5F5B7394CC615CE4B53CC4BE579A093014586E (void);
// 0x000000AB System.Memory`1<System.Byte> System.Text.Json.PooledByteBufferWriter::GetMemory(System.Int32)
extern void PooledByteBufferWriter_GetMemory_m23D69042BE9652FBB2A39785F104CBF30F17D353 (void);
// 0x000000AC System.Void System.Text.Json.PooledByteBufferWriter::CheckAndResizeBuffer(System.Int32)
extern void PooledByteBufferWriter_CheckAndResizeBuffer_m4D69B85CDDB2E57BD02D2E2C10B29376B282B476 (void);
// 0x000000AD System.Void System.Text.Json.ThrowHelper::ThrowOutOfMemoryException_BufferMaximumSizeExceeded(System.UInt32)
extern void ThrowHelper_ThrowOutOfMemoryException_BufferMaximumSizeExceeded_m79696E7BFDA49E5D410358EA30E5A82D093BF59D (void);
// 0x000000AE System.ArgumentOutOfRangeException System.Text.Json.ThrowHelper::GetArgumentOutOfRangeException_MaxDepthMustBePositive(System.String)
extern void ThrowHelper_GetArgumentOutOfRangeException_MaxDepthMustBePositive_m750E289BFD3B7A66BD128EAEFEEB57B3373D55B0 (void);
// 0x000000AF System.ArgumentOutOfRangeException System.Text.Json.ThrowHelper::GetArgumentOutOfRangeException(System.String,System.String)
extern void ThrowHelper_GetArgumentOutOfRangeException_mEA1F2D0D8BB8DC705E57649C1F766FC5B218EEA1 (void);
// 0x000000B0 System.ArgumentOutOfRangeException System.Text.Json.ThrowHelper::GetArgumentOutOfRangeException_CommentEnumMustBeInRange(System.String)
extern void ThrowHelper_GetArgumentOutOfRangeException_CommentEnumMustBeInRange_mD4C28C947D91AD6C634C592231BD52C910C0802C (void);
// 0x000000B1 System.ArgumentException System.Text.Json.ThrowHelper::GetArgumentException(System.String)
extern void ThrowHelper_GetArgumentException_mACA6F87FA5A34AAAB64CF58A133D986308CD2FC5 (void);
// 0x000000B2 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_PropertyNameTooLarge(System.Int32)
extern void ThrowHelper_ThrowArgumentException_PropertyNameTooLarge_m591F53BAC4D2B10A55D1389C8A1A907157A0E63E (void);
// 0x000000B3 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_ValueTooLarge(System.Int32)
extern void ThrowHelper_ThrowArgumentException_ValueTooLarge_m98E94862DE95E1CD322A446B4B845268A124C055 (void);
// 0x000000B4 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_ValueNotSupported()
extern void ThrowHelper_ThrowArgumentException_ValueNotSupported_m579817B44127511B903F738EBBC7D02FDE054F53 (void);
// 0x000000B5 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_NeedLargerSpan()
extern void ThrowHelper_ThrowInvalidOperationException_NeedLargerSpan_mCCE8D5F2D1147F2E3BDB377D4916507B12561F28 (void);
// 0x000000B6 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException(System.Int32)
extern void ThrowHelper_ThrowInvalidOperationException_m7D22016093BF6ECF1EF1B98DD672E6D7FC51ED70 (void);
// 0x000000B7 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException(System.String)
extern void ThrowHelper_ThrowInvalidOperationException_m8DEC1F901C1321582AE466E5C280EFAB23511C4F (void);
// 0x000000B8 System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException(System.String)
extern void ThrowHelper_GetInvalidOperationException_mEEE0CBC4C08A945CD63041ACD80775F5056FD394 (void);
// 0x000000B9 System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ExpectedNumber(System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_ExpectedNumber_m5EEA4C741057DC0BF0CBAB746B63B1131955506B (void);
// 0x000000BA System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ExpectedBoolean(System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_ExpectedBoolean_mA8F271F7AAD4502C3E7FD563A6258A0B2AB93326 (void);
// 0x000000BB System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ExpectedString(System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_ExpectedString_mBD6F5B8CC77AC75D80B1FBEF9AC5B88B32C16317 (void);
// 0x000000BC System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_CannotSkipOnPartial()
extern void ThrowHelper_GetInvalidOperationException_CannotSkipOnPartial_m29D6936DDE65B674D1262C02F7FFCD168EFA6F0A (void);
// 0x000000BD System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException(System.String,System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_m5FE9606EDE8F607F2F4661CEF82080D5DEE35597 (void);
// 0x000000BE System.InvalidOperationException System.Text.Json.ThrowHelper::GetJsonElementWrongTypeException(System.Text.Json.JsonTokenType,System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetJsonElementWrongTypeException_m86CA67C05608A212950C235A8BBBA936B2B7A004 (void);
// 0x000000BF System.InvalidOperationException System.Text.Json.ThrowHelper::GetJsonElementWrongTypeException(System.Text.Json.JsonValueKind,System.Text.Json.JsonValueKind)
extern void ThrowHelper_GetJsonElementWrongTypeException_mCB63BC840CD43F6D8F13CCB3023ACD9287E21C1C (void);
// 0x000000C0 System.Void System.Text.Json.ThrowHelper::ThrowJsonReaderException(System.Text.Json.Utf8JsonReader&,System.Text.Json.ExceptionResource,System.Byte,System.ReadOnlySpan`1<System.Byte>)
extern void ThrowHelper_ThrowJsonReaderException_m86265B8897AA1A8AA169F05BA7421ECF0260EE82 (void);
// 0x000000C1 System.Text.Json.JsonException System.Text.Json.ThrowHelper::GetJsonReaderException(System.Text.Json.Utf8JsonReader&,System.Text.Json.ExceptionResource,System.Byte,System.ReadOnlySpan`1<System.Byte>)
extern void ThrowHelper_GetJsonReaderException_m64F661A9A7D85E2596AB156BAF319034CA4BDFF3 (void);
// 0x000000C2 System.Boolean System.Text.Json.ThrowHelper::IsPrintable(System.Byte)
extern void ThrowHelper_IsPrintable_m2E0D5128B77C215AE99AC6D18A162ACE1A71AC20 (void);
// 0x000000C3 System.String System.Text.Json.ThrowHelper::GetPrintableString(System.Byte)
extern void ThrowHelper_GetPrintableString_m40E31A72DA7DFAA32ED8B7A06A6609797EA0F551 (void);
// 0x000000C4 System.String System.Text.Json.ThrowHelper::GetResourceString(System.Text.Json.Utf8JsonReader&,System.Text.Json.ExceptionResource,System.Byte,System.String)
extern void ThrowHelper_GetResourceString_mE47C527A4103CFD253D1489FD20B7AF1121BC3FA (void);
// 0x000000C5 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException(System.Text.Json.ExceptionResource,System.Int32,System.Byte,System.Text.Json.JsonTokenType)
extern void ThrowHelper_ThrowInvalidOperationException_mE97A968AF5F73C0C37D09D7C9AAEB27EF4785516 (void);
// 0x000000C6 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_InvalidUTF8(System.ReadOnlySpan`1<System.Byte>)
extern void ThrowHelper_ThrowArgumentException_InvalidUTF8_m61926CFA2D24DCE662F6BC2B2D1A2E4B66B98498 (void);
// 0x000000C7 System.Void System.Text.Json.ThrowHelper::ThrowArgumentException_InvalidUTF16(System.Int32)
extern void ThrowHelper_ThrowArgumentException_InvalidUTF16_m063FC3F74E3C16AAC9D6C6F79AFC414D443EB4DE (void);
// 0x000000C8 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ReadInvalidUTF16(System.Int32)
extern void ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_m4A32F5D71AC0CCB638A23C7D369287B7FE350B25 (void);
// 0x000000C9 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ReadInvalidUTF16()
extern void ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_m891B6B9BC2208ED7FE09E655D0D6A9880E90D392 (void);
// 0x000000CA System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ReadInvalidUTF8(System.Text.DecoderFallbackException)
extern void ThrowHelper_GetInvalidOperationException_ReadInvalidUTF8_mE44B06A595A70AF59AF82E964F4234B189AD8CBA (void);
// 0x000000CB System.ArgumentException System.Text.Json.ThrowHelper::GetArgumentException_ReadInvalidUTF16(System.Text.EncoderFallbackException)
extern void ThrowHelper_GetArgumentException_ReadInvalidUTF16_m252AF38593FFB0FD4E807A9F0D2DCAD0656370E5 (void);
// 0x000000CC System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException(System.String,System.Exception)
extern void ThrowHelper_GetInvalidOperationException_m32A1F2595A56602FB6FFBB5241AABE3FBF959773 (void);
// 0x000000CD System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException(System.Text.Json.ExceptionResource,System.Int32,System.Byte,System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_m47C288150449D789996BE18DBD7DCF2D5DFD2393 (void);
// 0x000000CE System.String System.Text.Json.ThrowHelper::GetResourceString(System.Text.Json.ExceptionResource,System.Int32,System.Byte,System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetResourceString_mA42609E40D339ED9AABB16A1923814DC4841AB9D (void);
// 0x000000CF System.FormatException System.Text.Json.ThrowHelper::GetFormatException()
extern void ThrowHelper_GetFormatException_mFB5DB5DD18152932CDB9D3EC5229F1F4A659F88E (void);
// 0x000000D0 System.FormatException System.Text.Json.ThrowHelper::GetFormatException(System.Text.Json.NumericType)
extern void ThrowHelper_GetFormatException_m544F59063DA3B19472A42E104FA8DB8738F19C98 (void);
// 0x000000D1 System.FormatException System.Text.Json.ThrowHelper::GetFormatException(System.Text.Json.DataType)
extern void ThrowHelper_GetFormatException_m01DB51AA6723798546C757FBD716FB83AD457EFC (void);
// 0x000000D2 System.InvalidOperationException System.Text.Json.ThrowHelper::GetInvalidOperationException_ExpectedChar(System.Text.Json.JsonTokenType)
extern void ThrowHelper_GetInvalidOperationException_ExpectedChar_mA357290B52AE1CCBE21E4D44D81798306B15131A (void);
// 0x000000D3 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_SerializationNotSupported(System.Type)
extern void ThrowHelper_ThrowNotSupportedException_SerializationNotSupported_mC24229F331EB2F60F8B977CE5BB4BAFAEA326828 (void);
// 0x000000D4 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_ConstructorMaxOf64Parameters(System.Reflection.ConstructorInfo,System.Type)
extern void ThrowHelper_ThrowNotSupportedException_ConstructorMaxOf64Parameters_m4CF7BD9AAE68BE7A348E93A2FEB9E58E13BF9743 (void);
// 0x000000D5 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_DictionaryKeyTypeNotSupported(System.Type)
extern void ThrowHelper_ThrowNotSupportedException_DictionaryKeyTypeNotSupported_mF60A3FF91D6CD3C9FB8FC242A3584658A59ABCA0 (void);
// 0x000000D6 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_DeserializeUnableToConvertValue(System.Type)
extern void ThrowHelper_ThrowJsonException_DeserializeUnableToConvertValue_m45C862293B0F080C12A4EECB3084FBF0B8CDD83D (void);
// 0x000000D7 System.Void System.Text.Json.ThrowHelper::ThrowInvalidCastException_DeserializeUnableToAssignValue(System.Type,System.Type)
extern void ThrowHelper_ThrowInvalidCastException_DeserializeUnableToAssignValue_mEDC3F06B4CE394CA1F7FDC3FF2E9902B02173DC5 (void);
// 0x000000D8 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_DeserializeUnableToAssignNull(System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_DeserializeUnableToAssignNull_mEADD0C71C1896AB70526F02F16CCA9DF430A427B (void);
// 0x000000D9 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_SerializationConverterRead(System.Text.Json.Serialization.JsonConverter)
extern void ThrowHelper_ThrowJsonException_SerializationConverterRead_m661AF889A7B9F40C97180860B0CCA8117410C1C9 (void);
// 0x000000DA System.Void System.Text.Json.ThrowHelper::ThrowJsonException_SerializationConverterWrite(System.Text.Json.Serialization.JsonConverter)
extern void ThrowHelper_ThrowJsonException_SerializationConverterWrite_mD4203A8BB078A26D1F944BAAB79C87BBB5650179 (void);
// 0x000000DB System.Void System.Text.Json.ThrowHelper::ThrowJsonException_SerializerCycleDetected(System.Int32)
extern void ThrowHelper_ThrowJsonException_SerializerCycleDetected_mEA299568BAF8CA18E6A119E98ED7714D41FB0E38 (void);
// 0x000000DC System.Void System.Text.Json.ThrowHelper::ThrowJsonException(System.String)
extern void ThrowHelper_ThrowJsonException_m8A46CAA703623FA179B048050F106600A3722FE8 (void);
// 0x000000DD System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_CannotSerializeInvalidType(System.Type,System.Type,System.Reflection.MemberInfo)
extern void ThrowHelper_ThrowInvalidOperationException_CannotSerializeInvalidType_mBA5982C757C417BE964B629A43A9CEF2124BA660 (void);
// 0x000000DE System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationConverterNotCompatible(System.Type,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationConverterNotCompatible_m551F2D6AF89BE3277AB0C3E2D619D9A60B8C2187 (void);
// 0x000000DF System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationConverterOnAttributeInvalid(System.Type,System.Reflection.MemberInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeInvalid_mBC7789EBE47721D6E34286B5F42EBF8F490E5236 (void);
// 0x000000E0 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationConverterOnAttributeNotCompatible(System.Type,System.Reflection.MemberInfo,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeNotCompatible_m2DF8EE86D37F018A4CE6E02074C89CCAAC9D6E24 (void);
// 0x000000E1 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializerOptionsImmutable()
extern void ThrowHelper_ThrowInvalidOperationException_SerializerOptionsImmutable_mFC63B8BBB752B3F456704C8B4FA9A0369804D896 (void);
// 0x000000E2 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializerPropertyNameConflict(System.Type,System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameConflict_m468997F3DEF6CA979BB5F162D8BA858A51D02C2E (void);
// 0x000000E3 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializerPropertyNameNull(System.Type,System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameNull_m5CFD2A6DEA5E46DEDA5366C029D1FB7489A82E40 (void);
// 0x000000E4 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_NamingPolicyReturnNull(System.Text.Json.JsonNamingPolicy)
extern void ThrowHelper_ThrowInvalidOperationException_NamingPolicyReturnNull_mB518C297AE2F92A385AA8CE79EDA0C152A0FC046 (void);
// 0x000000E5 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializerConverterFactoryReturnsNull(System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_SerializerConverterFactoryReturnsNull_m3D01132E2B95AF5852EC9EBA9D692B23E7FF622A (void);
// 0x000000E6 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_MultiplePropertiesBindToConstructorParameters(System.Type,System.String,System.String,System.String,System.Reflection.ConstructorInfo)
extern void ThrowHelper_ThrowInvalidOperationException_MultiplePropertiesBindToConstructorParameters_m4DAE587048436F628F4628A61230CC083F397AB7 (void);
// 0x000000E7 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ConstructorParameterIncompleteBinding(System.Reflection.ConstructorInfo,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_ConstructorParameterIncompleteBinding_m04767FC2344112C92B1A6CD19126C73F16C70426 (void);
// 0x000000E8 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ExtensionDataCannotBindToCtorParam(System.Reflection.MemberInfo,System.Type,System.Reflection.ConstructorInfo)
extern void ThrowHelper_ThrowInvalidOperationException_ExtensionDataCannotBindToCtorParam_mF02345D3CC2FCA07275CB3081D6A081474A577B1 (void);
// 0x000000E9 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_JsonIncludeOnNonPublicInvalid(System.Reflection.MemberInfo,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_JsonIncludeOnNonPublicInvalid_mEBF3157BA9B05074D4C9EB8D24E89592FDB8E354 (void);
// 0x000000EA System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_IgnoreConditionOnValueTypeInvalid(System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_IgnoreConditionOnValueTypeInvalid_m22375B31A81C111155F76CD369373E22E9F5BC0A (void);
// 0x000000EB System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_NumberHandlingOnPropertyInvalid(System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_NumberHandlingOnPropertyInvalid_mDA9C9F343B33690C02EB21BA707294E8DF79684D (void);
// 0x000000EC System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_ConverterCanConvertNullableRedundant(System.Type,System.Text.Json.Serialization.JsonConverter)
extern void ThrowHelper_ThrowInvalidOperationException_ConverterCanConvertNullableRedundant_mD3EEE3DFDF279293BDC4F46C71609D731929032C (void);
// 0x000000ED System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_ObjectWithParameterizedCtorRefMetadataNotHonored(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowNotSupportedException_ObjectWithParameterizedCtorRefMetadataNotHonored_m4A50791211BA9F37D478B0488D6D6EFD102BA1CD (void);
// 0x000000EE System.Void System.Text.Json.ThrowHelper::ReThrowWithPath(System.Text.Json.ReadStack&,System.Text.Json.JsonReaderException)
extern void ThrowHelper_ReThrowWithPath_m78EE5E489A425E56E74E87DBCE71776EF7BA2808 (void);
// 0x000000EF System.Void System.Text.Json.ThrowHelper::ReThrowWithPath(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Exception)
extern void ThrowHelper_ReThrowWithPath_m878D9F148EB22BFBA260F8DD9F2D15A28F03F843 (void);
// 0x000000F0 System.Void System.Text.Json.ThrowHelper::AddJsonExceptionInformation(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonException)
extern void ThrowHelper_AddJsonExceptionInformation_m93DA5D5ECE14280C710EA2CE1960B7FA22ABAE03 (void);
// 0x000000F1 System.Void System.Text.Json.ThrowHelper::ReThrowWithPath(System.Text.Json.WriteStack&,System.Exception)
extern void ThrowHelper_ReThrowWithPath_m0807C880321B63D0277EFF7BE510A1F0FBD21A57 (void);
// 0x000000F2 System.Void System.Text.Json.ThrowHelper::AddJsonExceptionInformation(System.Text.Json.WriteStack&,System.Text.Json.JsonException)
extern void ThrowHelper_AddJsonExceptionInformation_m112F77F7E113AD4E3C1F5F86117CE950EFB1F934 (void);
// 0x000000F3 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationDuplicateAttribute(System.Type,System.Type,System.Reflection.MemberInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateAttribute_mD696CFD27EA6ABDC13DCD058D698C0E94B9E6E2E (void);
// 0x000000F4 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationDuplicateTypeAttribute(System.Type,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateTypeAttribute_m893BBAB25A9BCBB6161AC6D3C6CB43FD902D0216 (void);
// 0x000000F5 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationDuplicateTypeAttribute(System.Type)
// 0x000000F6 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_SerializationDataExtensionPropertyInvalid(System.Type,System.Text.Json.JsonPropertyInfo)
extern void ThrowHelper_ThrowInvalidOperationException_SerializationDataExtensionPropertyInvalid_m5EB0D9D7A1ED0FA5D31729E323FCB04D3DB23735 (void);
// 0x000000F7 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.NotSupportedException)
extern void ThrowHelper_ThrowNotSupportedException_mE938772C865354567463F8EB9643524AC591A67E (void);
// 0x000000F8 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException(System.Text.Json.WriteStack&,System.NotSupportedException)
extern void ThrowHelper_ThrowNotSupportedException_mF66B063BE62D70EF91197DEB39C5BF34D05DAB7D (void);
// 0x000000F9 System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_DeserializeNoConstructor(System.Type,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowNotSupportedException_DeserializeNoConstructor_m3B8416576117E4CF29B7466BBFFDE06F08BE0E5E (void);
// 0x000000FA System.Void System.Text.Json.ThrowHelper::ThrowNotSupportedException_CannotPopulateCollection(System.Type,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowNotSupportedException_CannotPopulateCollection_m545753EB67DF0A42857B280D6B3E35EC82CC7812 (void);
// 0x000000FB System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataValuesInvalidToken(System.Text.Json.JsonTokenType)
extern void ThrowHelper_ThrowJsonException_MetadataValuesInvalidToken_mC4B686B0F521BAD7FFF91AC86135B3461D048731 (void);
// 0x000000FC System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataReferenceNotFound(System.String)
extern void ThrowHelper_ThrowJsonException_MetadataReferenceNotFound_m8BE143B132A6882F7DF48C413E40C2845E2E9B5B (void);
// 0x000000FD System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataValueWasNotString(System.Text.Json.JsonTokenType)
extern void ThrowHelper_ThrowJsonException_MetadataValueWasNotString_m0256A7850A68C37BFCD0DC892D510785CA9C2882 (void);
// 0x000000FE System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataValueWasNotString(System.Text.Json.JsonValueKind)
extern void ThrowHelper_ThrowJsonException_MetadataValueWasNotString_mD81E092228AF69DAD1EDB6707B3AD1B81765735B (void);
// 0x000000FF System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mDD09A1CDEECB496920D33515903B54FB816CD6CE (void);
// 0x00000100 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties()
extern void ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_m1F2786A4970058AB222A15EBDC5A8B21733797E3 (void);
// 0x00000101 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataIdIsNotFirstProperty(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowJsonException_MetadataIdIsNotFirstProperty_m599DC6EE20D8CE9ED6FCE9F3C17C4FC7AA71FC52 (void);
// 0x00000102 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataMissingIdBeforeValues(System.Text.Json.ReadStack&,System.ReadOnlySpan`1<System.Byte>)
extern void ThrowHelper_ThrowJsonException_MetadataMissingIdBeforeValues_mD247FCE14C0D9E3BAB67B6614029B082F841B3B1 (void);
// 0x00000103 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&)
extern void ThrowHelper_ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign_mECFAEA7A557630248C78CB073637AC76CB6FF722 (void);
// 0x00000104 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataDuplicateIdFound(System.String)
extern void ThrowHelper_ThrowJsonException_MetadataDuplicateIdFound_m11947A1B06CBC9C4035D6DC610417AF31D6005FC (void);
// 0x00000105 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataInvalidReferenceToValueType(System.Type)
extern void ThrowHelper_ThrowJsonException_MetadataInvalidReferenceToValueType_m9D05E3528B59C04A67B2C9125CBB17C2FA144AFC (void);
// 0x00000106 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataPreservedArrayInvalidProperty(System.Text.Json.ReadStack&,System.Type,System.Text.Json.Utf8JsonReader&)
extern void ThrowHelper_ThrowJsonException_MetadataPreservedArrayInvalidProperty_m889853362A58AB715BB4FFB0C287A993D4A72FDD (void);
// 0x00000107 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataPreservedArrayValuesNotFound(System.Text.Json.ReadStack&,System.Type)
extern void ThrowHelper_ThrowJsonException_MetadataPreservedArrayValuesNotFound_m88AE8B99500E8629944E0F935D7EFA5560E31070 (void);
// 0x00000108 System.Void System.Text.Json.ThrowHelper::ThrowJsonException_MetadataCannotParsePreservedObjectIntoImmutable(System.Type)
extern void ThrowHelper_ThrowJsonException_MetadataCannotParsePreservedObjectIntoImmutable_m8781A043C3D02AD141FFFF8A70DE7C2DADA595F5 (void);
// 0x00000109 System.Void System.Text.Json.ThrowHelper::ThrowInvalidOperationException_MetadataReferenceOfTypeCannotBeAssignedToType(System.String,System.Type,System.Type)
extern void ThrowHelper_ThrowInvalidOperationException_MetadataReferenceOfTypeCannotBeAssignedToType_m0D5C9B04DBCFFFB1EEBF400B90519B14EA822EA7 (void);
// 0x0000010A System.Void System.Text.Json.ThrowHelper::ThrowUnexpectedMetadataException(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void ThrowHelper_ThrowUnexpectedMetadataException_m052DF778174E9595C074513E1256F4CF9B8ADDC9 (void);
// 0x0000010B System.Int32 System.Text.Json.BitStack::get_CurrentDepth()
extern void BitStack_get_CurrentDepth_m29C47DB4A4B69D9C76A93EBA1637E97A243FC869 (void);
// 0x0000010C System.Void System.Text.Json.BitStack::PushTrue()
extern void BitStack_PushTrue_mBEF1D3C195EA821E02716255C61AFB88258D5BAB (void);
// 0x0000010D System.Void System.Text.Json.BitStack::PushFalse()
extern void BitStack_PushFalse_m83CDC7E063685BF4FAD27FDCD7F51CF8D49EBAD3 (void);
// 0x0000010E System.Void System.Text.Json.BitStack::PushToArray(System.Boolean)
extern void BitStack_PushToArray_mE3486049BF012A03170FBC4C4BF1CE139F81F6C0 (void);
// 0x0000010F System.Boolean System.Text.Json.BitStack::Pop()
extern void BitStack_Pop_mEBB94B5C8DD6FACC49DCB2A2A7D365E5A75A4FD2 (void);
// 0x00000110 System.Boolean System.Text.Json.BitStack::PopFromArray()
extern void BitStack_PopFromArray_mC5F5242B631329764A700C94FC08270FE1803691 (void);
// 0x00000111 System.Void System.Text.Json.BitStack::DoubleArray(System.Int32)
extern void BitStack_DoubleArray_m46DB748394540255EF86FBD4970988C81DF3395F (void);
// 0x00000112 System.Void System.Text.Json.BitStack::SetFirstBit()
extern void BitStack_SetFirstBit_m9263D9E5C36F9375CDA674AC231BCF1FDF858472 (void);
// 0x00000113 System.Void System.Text.Json.BitStack::ResetFirstBit()
extern void BitStack_ResetFirstBit_m71CD0C8BD6150A422ABAABF746E6F0EC53FD8359 (void);
// 0x00000114 System.Int32 System.Text.Json.BitStack::Div32Rem(System.Int32,System.Int32&)
extern void BitStack_Div32Rem_m2EF455EE5BC45D67DFEF697EB03E0C7F53D9592F (void);
// 0x00000115 System.Boolean System.Text.Json.JsonDocument::get_IsDisposable()
extern void JsonDocument_get_IsDisposable_m212A389096307FEAD14943ACE082EE06653D22EA (void);
// 0x00000116 System.Text.Json.JsonElement System.Text.Json.JsonDocument::get_RootElement()
extern void JsonDocument_get_RootElement_m40BA0A219E870618E0E6DD88367CFB484F2C5195 (void);
// 0x00000117 System.Void System.Text.Json.JsonDocument::.ctor(System.ReadOnlyMemory`1<System.Byte>,System.Text.Json.JsonDocument/MetadataDb,System.Byte[],System.Boolean)
extern void JsonDocument__ctor_m84EA59E1DB006199769A7D58D844F7A86B08173F (void);
// 0x00000118 System.Void System.Text.Json.JsonDocument::Dispose()
extern void JsonDocument_Dispose_m2CEFC6A03073BF3504F21DEA2E09E19042383ED6 (void);
// 0x00000119 System.Void System.Text.Json.JsonDocument::WriteTo(System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WriteTo_m3E1525459DB06CB769488220F558516BA1E3DFDE (void);
// 0x0000011A System.Text.Json.JsonTokenType System.Text.Json.JsonDocument::GetJsonTokenType(System.Int32)
extern void JsonDocument_GetJsonTokenType_mB89F7B88FE549403D51C44AE479C23F511571BA9 (void);
// 0x0000011B System.Int32 System.Text.Json.JsonDocument::GetEndIndex(System.Int32,System.Boolean)
extern void JsonDocument_GetEndIndex_m7649DD254640741203DDD17105B1BD42F1ABA2BD (void);
// 0x0000011C System.ReadOnlyMemory`1<System.Byte> System.Text.Json.JsonDocument::GetRawValue(System.Int32,System.Boolean)
extern void JsonDocument_GetRawValue_m041DE8FDAF9BBF04627C80067AB84EF58A1C48F0 (void);
// 0x0000011D System.ReadOnlyMemory`1<System.Byte> System.Text.Json.JsonDocument::GetPropertyRawValue(System.Int32)
extern void JsonDocument_GetPropertyRawValue_m136F2D737AB4DEB3BDE96250B4F93C61CF0711B9 (void);
// 0x0000011E System.String System.Text.Json.JsonDocument::GetString(System.Int32,System.Text.Json.JsonTokenType)
extern void JsonDocument_GetString_m67D5DF0C839EF45BC9C1B800981786AB46EDDA27 (void);
// 0x0000011F System.Boolean System.Text.Json.JsonDocument::TextEquals(System.Int32,System.ReadOnlySpan`1<System.Byte>,System.Boolean,System.Boolean)
extern void JsonDocument_TextEquals_m7C64ABF5685A8004EA85D98366FDDED93463D775 (void);
// 0x00000120 System.Boolean System.Text.Json.JsonDocument::TryGetValue(System.Int32,System.Int32&)
extern void JsonDocument_TryGetValue_mA6324944BBAC3741B7CB48ADB8CF13EC53D14677 (void);
// 0x00000121 System.String System.Text.Json.JsonDocument::GetRawValueAsString(System.Int32)
extern void JsonDocument_GetRawValueAsString_m81007913ABD82375532603DFD76454D7CA784108 (void);
// 0x00000122 System.String System.Text.Json.JsonDocument::GetPropertyRawValueAsString(System.Int32)
extern void JsonDocument_GetPropertyRawValueAsString_mC5D5361947421D454D9EA948160BDD834F2A7D31 (void);
// 0x00000123 System.Text.Json.JsonElement System.Text.Json.JsonDocument::CloneElement(System.Int32)
extern void JsonDocument_CloneElement_mCA0507E07D125635120AE95AD28206686C61B712 (void);
// 0x00000124 System.Void System.Text.Json.JsonDocument::WriteElementTo(System.Int32,System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WriteElementTo_m6B43E581F2E379DF5F53E09AFF9E5992CF544C21 (void);
// 0x00000125 System.Void System.Text.Json.JsonDocument::WriteComplexElement(System.Int32,System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WriteComplexElement_mEEDFB2BD62CF0D2CB4AB76FFAA45459F493CBE42 (void);
// 0x00000126 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonDocument::UnescapeString(System.Text.Json.JsonDocument/DbRow&,System.ArraySegment`1<System.Byte>&)
extern void JsonDocument_UnescapeString_m3426A69B8BA315520180E6FBEE38905E213EFB79 (void);
// 0x00000127 System.Void System.Text.Json.JsonDocument::ClearAndReturn(System.ArraySegment`1<System.Byte>)
extern void JsonDocument_ClearAndReturn_mEA37B5007D8C568187F058DEAB1C3C4506D06CC8 (void);
// 0x00000128 System.Void System.Text.Json.JsonDocument::WritePropertyName(System.Text.Json.JsonDocument/DbRow&,System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WritePropertyName_m5377F1DD3708153B4E0841F81C1DCC39C01A0E1C (void);
// 0x00000129 System.Void System.Text.Json.JsonDocument::WriteString(System.Text.Json.JsonDocument/DbRow&,System.Text.Json.Utf8JsonWriter)
extern void JsonDocument_WriteString_mBFC04E9D7FEEE29A2EC49FD91CE60AD096BDA3D3 (void);
// 0x0000012A System.Void System.Text.Json.JsonDocument::Parse(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonReaderOptions,System.Text.Json.JsonDocument/MetadataDb&,System.Text.Json.JsonDocument/StackRowStack&)
extern void JsonDocument_Parse_m88DDF34A946810A62CE6560350C85D2B9E8F270E (void);
// 0x0000012B System.Void System.Text.Json.JsonDocument::CheckNotDisposed()
extern void JsonDocument_CheckNotDisposed_m406A04C85C33DFCF4FF505BEF37E88CE399583C0 (void);
// 0x0000012C System.Void System.Text.Json.JsonDocument::CheckExpectedType(System.Text.Json.JsonTokenType,System.Text.Json.JsonTokenType)
extern void JsonDocument_CheckExpectedType_m354D00E8EB7C96836FB54421A7F35A007A2FEB36 (void);
// 0x0000012D System.Void System.Text.Json.JsonDocument::CheckSupportedOptions(System.Text.Json.JsonReaderOptions,System.String)
extern void JsonDocument_CheckSupportedOptions_mB43D28A15D6478A0954926BC4BF12ED1D9FE457C (void);
// 0x0000012E System.Text.Json.JsonDocument System.Text.Json.JsonDocument::Parse(System.ReadOnlyMemory`1<System.Char>,System.Text.Json.JsonDocumentOptions)
extern void JsonDocument_Parse_m31EAD3B41D1A91C11B685C0133863F61628B65AC (void);
// 0x0000012F System.Text.Json.JsonDocument System.Text.Json.JsonDocument::Parse(System.String,System.Text.Json.JsonDocumentOptions)
extern void JsonDocument_Parse_m290785DE223664D929CAC7888BAEE7693B60DFC2 (void);
// 0x00000130 System.Text.Json.JsonDocument System.Text.Json.JsonDocument::ParseValue(System.Text.Json.Utf8JsonReader&)
extern void JsonDocument_ParseValue_m088FB4B1ACC350C2FED324556EC221C06C6CB786 (void);
// 0x00000131 System.Boolean System.Text.Json.JsonDocument::TryParseValue(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonDocument&,System.Boolean)
extern void JsonDocument_TryParseValue_mFAE1CD76E9A91AE946FF037CA0A3BEAA5C2A102A (void);
// 0x00000132 System.Text.Json.JsonDocument System.Text.Json.JsonDocument::Parse(System.ReadOnlyMemory`1<System.Byte>,System.Text.Json.JsonReaderOptions,System.Byte[])
extern void JsonDocument_Parse_m4DB8A56DDE364571F37E232634DDFEB6B991F271 (void);
// 0x00000133 System.Boolean System.Text.Json.JsonDocument::TryGetNamedPropertyValue(System.Int32,System.ReadOnlySpan`1<System.Char>,System.Text.Json.JsonElement&)
extern void JsonDocument_TryGetNamedPropertyValue_m19024E1EA661AC1CB7758B34270A236EEB7A00DD (void);
// 0x00000134 System.Boolean System.Text.Json.JsonDocument::TryGetNamedPropertyValue(System.Int32,System.Int32,System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonElement&)
extern void JsonDocument_TryGetNamedPropertyValue_m5942E503498F83A6D769CE0360EFA5DC1624200C (void);
// 0x00000135 System.Int32 System.Text.Json.JsonDocument/DbRow::get_Location()
extern void DbRow_get_Location_m26C010A9C88629FBF5B9BE4AB8DA90E22422CFAC (void);
// 0x00000136 System.Int32 System.Text.Json.JsonDocument/DbRow::get_SizeOrLength()
extern void DbRow_get_SizeOrLength_m84D8BF651739FC1923CCEA2500DF7D9929DD8ACF (void);
// 0x00000137 System.Boolean System.Text.Json.JsonDocument/DbRow::get_IsUnknownSize()
extern void DbRow_get_IsUnknownSize_m72AF174FA7DFE73898270932D83924CB0DA5C52A (void);
// 0x00000138 System.Boolean System.Text.Json.JsonDocument/DbRow::get_HasComplexChildren()
extern void DbRow_get_HasComplexChildren_m8900AC0494A74A8EE1BCA197C6223110CF35B997 (void);
// 0x00000139 System.Int32 System.Text.Json.JsonDocument/DbRow::get_NumberOfRows()
extern void DbRow_get_NumberOfRows_m1C82FF779E2D2653D42E81A38364AE4CC2B7DCAE (void);
// 0x0000013A System.Text.Json.JsonTokenType System.Text.Json.JsonDocument/DbRow::get_TokenType()
extern void DbRow_get_TokenType_mCCE39C67D2D66F0053D567BD45AD095D34977063 (void);
// 0x0000013B System.Void System.Text.Json.JsonDocument/DbRow::.ctor(System.Text.Json.JsonTokenType,System.Int32,System.Int32)
extern void DbRow__ctor_m5A251F6E7871A6B66B5CC2E73937621035F95178 (void);
// 0x0000013C System.Boolean System.Text.Json.JsonDocument/DbRow::get_IsSimpleValue()
extern void DbRow_get_IsSimpleValue_m2038B00EE08F425ABCD2BC3C24A151E7EDAE3927 (void);
// 0x0000013D System.Int32 System.Text.Json.JsonDocument/MetadataDb::get_Length()
extern void MetadataDb_get_Length_m22933560D56DEB73CDA8422130DFB5A6089E523C (void);
// 0x0000013E System.Void System.Text.Json.JsonDocument/MetadataDb::set_Length(System.Int32)
extern void MetadataDb_set_Length_m1E5804A10F184287716024455518919240C3E635 (void);
// 0x0000013F System.Void System.Text.Json.JsonDocument/MetadataDb::.ctor(System.Byte[])
extern void MetadataDb__ctor_m39EBC88D82771277ACD7B74A804CC43263D5B706 (void);
// 0x00000140 System.Void System.Text.Json.JsonDocument/MetadataDb::.ctor(System.Int32)
extern void MetadataDb__ctor_m032FBF24011241945608F14DD9D7A12DB7CB8535 (void);
// 0x00000141 System.Void System.Text.Json.JsonDocument/MetadataDb::Dispose()
extern void MetadataDb_Dispose_m77CE10F7F04101B702C044AC728B6B6E8E912569 (void);
// 0x00000142 System.Void System.Text.Json.JsonDocument/MetadataDb::TrimExcess()
extern void MetadataDb_TrimExcess_m990B41FACFAE4EC6756F75A83B7130A8FD6EF21C (void);
// 0x00000143 System.Void System.Text.Json.JsonDocument/MetadataDb::Append(System.Text.Json.JsonTokenType,System.Int32,System.Int32)
extern void MetadataDb_Append_m2537E212C18338E2810CC9B80A5DD82B897DAFE7 (void);
// 0x00000144 System.Void System.Text.Json.JsonDocument/MetadataDb::Enlarge()
extern void MetadataDb_Enlarge_mDE37991365E52CA27B9FAE0F2A8964AA102BDD5A (void);
// 0x00000145 System.Void System.Text.Json.JsonDocument/MetadataDb::SetLength(System.Int32,System.Int32)
extern void MetadataDb_SetLength_mFC66B57EF58895AFFE2DAD33297935F63C5B8F79 (void);
// 0x00000146 System.Void System.Text.Json.JsonDocument/MetadataDb::SetNumberOfRows(System.Int32,System.Int32)
extern void MetadataDb_SetNumberOfRows_m5D37816F2AD652AB316EFBE4B0A2643457B72C82 (void);
// 0x00000147 System.Void System.Text.Json.JsonDocument/MetadataDb::SetHasComplexChildren(System.Int32)
extern void MetadataDb_SetHasComplexChildren_mB746E8ED47FF55757A24BD397A0E4D91E18F8EAE (void);
// 0x00000148 System.Int32 System.Text.Json.JsonDocument/MetadataDb::FindIndexOfFirstUnsetSizeOrLength(System.Text.Json.JsonTokenType)
extern void MetadataDb_FindIndexOfFirstUnsetSizeOrLength_m9BB4939039E8059B31868E36E84F888B26BAE35E (void);
// 0x00000149 System.Int32 System.Text.Json.JsonDocument/MetadataDb::FindOpenElement(System.Text.Json.JsonTokenType)
extern void MetadataDb_FindOpenElement_m8CA68EDBB9605702DCA7BB048DC51689A38ADCB7 (void);
// 0x0000014A System.Text.Json.JsonDocument/DbRow System.Text.Json.JsonDocument/MetadataDb::Get(System.Int32)
extern void MetadataDb_Get_m2EE4C63C4D6B477C49D85DA90D6CFFC009A0B662 (void);
// 0x0000014B System.Text.Json.JsonTokenType System.Text.Json.JsonDocument/MetadataDb::GetJsonTokenType(System.Int32)
extern void MetadataDb_GetJsonTokenType_mF9DF07B766B9E488CD4A203A1F0BE3D09551A41E (void);
// 0x0000014C System.Text.Json.JsonDocument/MetadataDb System.Text.Json.JsonDocument/MetadataDb::CopySegment(System.Int32,System.Int32)
extern void MetadataDb_CopySegment_mF7BE8719D6FE1E244B55E9E3251855DD065A4B1E (void);
// 0x0000014D System.Void System.Text.Json.JsonDocument/StackRow::.ctor(System.Int32,System.Int32)
extern void StackRow__ctor_m2A8EFF70E90A96B442084512D9788BBA144AA25F (void);
// 0x0000014E System.Void System.Text.Json.JsonDocument/StackRowStack::.ctor(System.Int32)
extern void StackRowStack__ctor_mA8C386AAC2C88F726223EF8F9CED4C3E79948976 (void);
// 0x0000014F System.Void System.Text.Json.JsonDocument/StackRowStack::Dispose()
extern void StackRowStack_Dispose_m442EF445F037EC392B2F1494ACEFF8D4E6B2A052 (void);
// 0x00000150 System.Void System.Text.Json.JsonDocument/StackRowStack::Push(System.Text.Json.JsonDocument/StackRow)
extern void StackRowStack_Push_m4F9FD4AE22507CABB234B17F9D72F52F0BF9215A (void);
// 0x00000151 System.Text.Json.JsonDocument/StackRow System.Text.Json.JsonDocument/StackRowStack::Pop()
extern void StackRowStack_Pop_m59CEFB7385936EBFB490BCFEB5001B63F6E79B31 (void);
// 0x00000152 System.Void System.Text.Json.JsonDocument/StackRowStack::Enlarge()
extern void StackRowStack_Enlarge_mB7433A4A8E2C398B72C14512663278632CA70D89 (void);
// 0x00000153 System.Text.Json.JsonCommentHandling System.Text.Json.JsonDocumentOptions::get_CommentHandling()
extern void JsonDocumentOptions_get_CommentHandling_m9486C1460A2DC728E08C0E0FAED390C30503FAC7 (void);
// 0x00000154 System.Int32 System.Text.Json.JsonDocumentOptions::get_MaxDepth()
extern void JsonDocumentOptions_get_MaxDepth_mB728B90176B962CD67750F71D6AF5FCB816CBF15 (void);
// 0x00000155 System.Boolean System.Text.Json.JsonDocumentOptions::get_AllowTrailingCommas()
extern void JsonDocumentOptions_get_AllowTrailingCommas_m03274EEEEC6A5BC79CCCE3E89FCE91179F7DE313 (void);
// 0x00000156 System.Text.Json.JsonReaderOptions System.Text.Json.JsonDocumentOptions::GetReaderOptions()
extern void JsonDocumentOptions_GetReaderOptions_mBC8F60D77D1AE7A350DFF94D8869A6177015540A (void);
// 0x00000157 System.Void System.Text.Json.JsonElement::.ctor(System.Text.Json.JsonDocument,System.Int32)
extern void JsonElement__ctor_m8E42B93E64E1C54BD78FF8163CD5C482E0EAA44B (void);
// 0x00000158 System.Text.Json.JsonTokenType System.Text.Json.JsonElement::get_TokenType()
extern void JsonElement_get_TokenType_mD6DC1A601E15744E583F39CFEAED4C161168C456 (void);
// 0x00000159 System.Text.Json.JsonValueKind System.Text.Json.JsonElement::get_ValueKind()
extern void JsonElement_get_ValueKind_m14EFE30FAA112F5199CBD9C42DC9C3AEF6ADA1B5 (void);
// 0x0000015A System.Text.Json.JsonElement System.Text.Json.JsonElement::GetProperty(System.String)
extern void JsonElement_GetProperty_mB0BB5EA6AC89BB64A9F3BD39EE474AD2D26F72D7 (void);
// 0x0000015B System.Boolean System.Text.Json.JsonElement::TryGetProperty(System.String,System.Text.Json.JsonElement&)
extern void JsonElement_TryGetProperty_m11C90F70F1EE0DA94CD3E752FD0259C599687D46 (void);
// 0x0000015C System.Boolean System.Text.Json.JsonElement::TryGetProperty(System.ReadOnlySpan`1<System.Char>,System.Text.Json.JsonElement&)
extern void JsonElement_TryGetProperty_m7322A7C18BB189A5E607ED747340376EB5FC173E (void);
// 0x0000015D System.String System.Text.Json.JsonElement::GetString()
extern void JsonElement_GetString_m7AE007D2F1B4016AA1B53BF79B1A1DD1EA42EB94 (void);
// 0x0000015E System.Boolean System.Text.Json.JsonElement::TryGetInt32(System.Int32&)
extern void JsonElement_TryGetInt32_m5FCAA7B399C4469AFFE24400FDDBE78F8C60041E (void);
// 0x0000015F System.Int32 System.Text.Json.JsonElement::GetInt32()
extern void JsonElement_GetInt32_m21DEB1B177269FFB57C09E9B094DF8C719926A73 (void);
// 0x00000160 System.String System.Text.Json.JsonElement::GetRawText()
extern void JsonElement_GetRawText_m054A9C6C86FA951676F21FEB0ABAAEE0835A11EE (void);
// 0x00000161 System.String System.Text.Json.JsonElement::GetPropertyRawText()
extern void JsonElement_GetPropertyRawText_m575B8D8F5C386BE964EB43964706F19BC0E72233 (void);
// 0x00000162 System.Boolean System.Text.Json.JsonElement::TextEqualsHelper(System.ReadOnlySpan`1<System.Byte>,System.Boolean,System.Boolean)
extern void JsonElement_TextEqualsHelper_mE1DE405E05A250F42B364021CDE5BDD629E7C6F5 (void);
// 0x00000163 System.Void System.Text.Json.JsonElement::WriteTo(System.Text.Json.Utf8JsonWriter)
extern void JsonElement_WriteTo_m8802C6ABB5C038C5E111274B0FED5702BFB72B10 (void);
// 0x00000164 System.Text.Json.JsonElement/ArrayEnumerator System.Text.Json.JsonElement::EnumerateArray()
extern void JsonElement_EnumerateArray_m9B35AF71578078459E6DAF2D7416D07F327E6E38 (void);
// 0x00000165 System.Text.Json.JsonElement/ObjectEnumerator System.Text.Json.JsonElement::EnumerateObject()
extern void JsonElement_EnumerateObject_mD4E563B682ABDCB35E2D27A573BB903CE3EA7D76 (void);
// 0x00000166 System.String System.Text.Json.JsonElement::ToString()
extern void JsonElement_ToString_m9E0942575D303AD18247677C192C7E065AE83634 (void);
// 0x00000167 System.Text.Json.JsonElement System.Text.Json.JsonElement::Clone()
extern void JsonElement_Clone_mCD2B3D79FE552E8FA4A29C9244675D1926B7EC50 (void);
// 0x00000168 System.Void System.Text.Json.JsonElement::CheckValidInstance()
extern void JsonElement_CheckValidInstance_m175423F0C65D579DB09D5B1C9ABE2E643064DC8E (void);
// 0x00000169 System.Void System.Text.Json.JsonElement/ArrayEnumerator::.ctor(System.Text.Json.JsonElement)
extern void ArrayEnumerator__ctor_m1D7B7BD7496B0CD223EE9399B5A57725AEEF7381 (void);
// 0x0000016A System.Text.Json.JsonElement System.Text.Json.JsonElement/ArrayEnumerator::get_Current()
extern void ArrayEnumerator_get_Current_m5F5C30DD80B5F2331BF392DA21D4EB06E050F827 (void);
// 0x0000016B System.Text.Json.JsonElement/ArrayEnumerator System.Text.Json.JsonElement/ArrayEnumerator::GetEnumerator()
extern void ArrayEnumerator_GetEnumerator_m938DD30A37AEE868DC5935D0CDE254FA6CAFCF97 (void);
// 0x0000016C System.Collections.IEnumerator System.Text.Json.JsonElement/ArrayEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void ArrayEnumerator_System_Collections_IEnumerable_GetEnumerator_m2E636672376124E9810A741FED6ACE7BFD20939E (void);
// 0x0000016D System.Collections.Generic.IEnumerator`1<System.Text.Json.JsonElement> System.Text.Json.JsonElement/ArrayEnumerator::System.Collections.Generic.IEnumerable<System.Text.Json.JsonElement>.GetEnumerator()
extern void ArrayEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonElementU3E_GetEnumerator_m9ABEE9A87DD3D50D065C9738B683D691A143539B (void);
// 0x0000016E System.Void System.Text.Json.JsonElement/ArrayEnumerator::Dispose()
extern void ArrayEnumerator_Dispose_mD4E9E4516CF8CC2CEA173A59B8407A94271C34D2 (void);
// 0x0000016F System.Void System.Text.Json.JsonElement/ArrayEnumerator::Reset()
extern void ArrayEnumerator_Reset_m3DB230A1965DE1E4F9DBEC7BF58635D2AC1FA823 (void);
// 0x00000170 System.Object System.Text.Json.JsonElement/ArrayEnumerator::System.Collections.IEnumerator.get_Current()
extern void ArrayEnumerator_System_Collections_IEnumerator_get_Current_mE0067C8DCE7157F9C9695C3FF3FDD5CEED826422 (void);
// 0x00000171 System.Boolean System.Text.Json.JsonElement/ArrayEnumerator::MoveNext()
extern void ArrayEnumerator_MoveNext_mF1B0AFD404AA0995A6BE8106CE602684BBE47433 (void);
// 0x00000172 System.Void System.Text.Json.JsonElement/ObjectEnumerator::.ctor(System.Text.Json.JsonElement)
extern void ObjectEnumerator__ctor_m703D9D5B13C21AC9E29F92FAB758D52A102C8F01 (void);
// 0x00000173 System.Text.Json.JsonProperty System.Text.Json.JsonElement/ObjectEnumerator::get_Current()
extern void ObjectEnumerator_get_Current_mC1865163B29F8CBEAC81A2E5C4A26AF3B997C7BC (void);
// 0x00000174 System.Text.Json.JsonElement/ObjectEnumerator System.Text.Json.JsonElement/ObjectEnumerator::GetEnumerator()
extern void ObjectEnumerator_GetEnumerator_mEE9CACFE7559A6D381D802909FE21EDA46FC103C (void);
// 0x00000175 System.Collections.IEnumerator System.Text.Json.JsonElement/ObjectEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void ObjectEnumerator_System_Collections_IEnumerable_GetEnumerator_mA2F0FD6FAB9BEF8BA08389BE66483F44BAF90BC5 (void);
// 0x00000176 System.Collections.Generic.IEnumerator`1<System.Text.Json.JsonProperty> System.Text.Json.JsonElement/ObjectEnumerator::System.Collections.Generic.IEnumerable<System.Text.Json.JsonProperty>.GetEnumerator()
extern void ObjectEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonPropertyU3E_GetEnumerator_m2F6759C0BBDA3C73CB81373048E97DE7E10D0AB0 (void);
// 0x00000177 System.Void System.Text.Json.JsonElement/ObjectEnumerator::Dispose()
extern void ObjectEnumerator_Dispose_mE6013E4D463D4DFAE6D9A4458393358F84F4115A (void);
// 0x00000178 System.Void System.Text.Json.JsonElement/ObjectEnumerator::Reset()
extern void ObjectEnumerator_Reset_m29B204531B2E013AE91F1B6CD394EC9E32447E5C (void);
// 0x00000179 System.Object System.Text.Json.JsonElement/ObjectEnumerator::System.Collections.IEnumerator.get_Current()
extern void ObjectEnumerator_System_Collections_IEnumerator_get_Current_mC740D819F66BC7DB0738A495920A650C31444B56 (void);
// 0x0000017A System.Boolean System.Text.Json.JsonElement/ObjectEnumerator::MoveNext()
extern void ObjectEnumerator_MoveNext_m7D620190FB99A0075BE003767FC70105507DA629 (void);
// 0x0000017B System.Text.Json.JsonElement System.Text.Json.JsonProperty::get_Value()
extern void JsonProperty_get_Value_m6ED717E9179F9D4A00DF42D36A191A8E2BB8C402 (void);
// 0x0000017C System.Void System.Text.Json.JsonProperty::.ctor(System.Text.Json.JsonElement,System.String)
extern void JsonProperty__ctor_m3447EAB80FB5BA6813D3F9F18EBAD08B7FCFB312 (void);
// 0x0000017D System.Boolean System.Text.Json.JsonProperty::EscapedNameEquals(System.ReadOnlySpan`1<System.Byte>)
extern void JsonProperty_EscapedNameEquals_mF067EB713ECF2C089B42447941C7A34C50C02352 (void);
// 0x0000017E System.String System.Text.Json.JsonProperty::ToString()
extern void JsonProperty_ToString_m9F70631770655A6F3B6AEAA2C3717D17A1D0F573 (void);
// 0x0000017F System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_TrueValue()
extern void JsonConstants_get_TrueValue_m7875CFE6FDF00788A7122ABE0D7EE9734C4909C4 (void);
// 0x00000180 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_FalseValue()
extern void JsonConstants_get_FalseValue_mF6E16F3E048CAA53A90759098574839A72FB4381 (void);
// 0x00000181 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_NullValue()
extern void JsonConstants_get_NullValue_mA5886DDBB16FC136D274B471F912CEEC7A9847AF (void);
// 0x00000182 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_NaNValue()
extern void JsonConstants_get_NaNValue_mBD4A12A27DC7CAB18F7BD8E0B46268074B646B8E (void);
// 0x00000183 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_PositiveInfinityValue()
extern void JsonConstants_get_PositiveInfinityValue_m15BAD40B6D9D8BC432804DA52F246A8673007722 (void);
// 0x00000184 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_NegativeInfinityValue()
extern void JsonConstants_get_NegativeInfinityValue_m6118A86711FE3D1277570ED96C7D89007376ADDD (void);
// 0x00000185 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_Delimiters()
extern void JsonConstants_get_Delimiters_m2173CDD642F01C58C897DCB96B27183F35B1A1B7 (void);
// 0x00000186 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonConstants::get_EscapableChars()
extern void JsonConstants_get_EscapableChars_m54662DEE4FD6B912A08DA47422AD6A4544CDBC8D (void);
// 0x00000187 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonEncodedText::get_EncodedUtf8Bytes()
extern void JsonEncodedText_get_EncodedUtf8Bytes_m7D24410A44ADF371EDDBA9059403D34F98B463A1 (void);
// 0x00000188 System.Void System.Text.Json.JsonEncodedText::.ctor(System.Byte[])
extern void JsonEncodedText__ctor_m7F8DBA12F3C948E827627FF1300E95AA48E42CEA (void);
// 0x00000189 System.Text.Json.JsonEncodedText System.Text.Json.JsonEncodedText::Encode(System.String,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonEncodedText_Encode_m2861A2FED41292077761CCC009958C8D5371F988 (void);
// 0x0000018A System.Text.Json.JsonEncodedText System.Text.Json.JsonEncodedText::Encode(System.ReadOnlySpan`1<System.Char>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonEncodedText_Encode_m3F96860D13424084A29B9D1B6C9925F35FEC3046 (void);
// 0x0000018B System.Text.Json.JsonEncodedText System.Text.Json.JsonEncodedText::TranscodeAndEncode(System.ReadOnlySpan`1<System.Char>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonEncodedText_TranscodeAndEncode_mB7FDCDAC54BE481FDF2EFA0A5026D258383616DA (void);
// 0x0000018C System.Text.Json.JsonEncodedText System.Text.Json.JsonEncodedText::EncodeHelper(System.ReadOnlySpan`1<System.Byte>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonEncodedText_EncodeHelper_m4D3F6AACFAC1CCB0CBAE0B0B66A20964BF5DE98F (void);
// 0x0000018D System.Boolean System.Text.Json.JsonEncodedText::Equals(System.Text.Json.JsonEncodedText)
extern void JsonEncodedText_Equals_m26E3AB94F984E7BAE83FB67FD47A7C5A0BB116E4 (void);
// 0x0000018E System.Boolean System.Text.Json.JsonEncodedText::Equals(System.Object)
extern void JsonEncodedText_Equals_mE176157268A0949E8C234E0175B00F9FF800B6C9 (void);
// 0x0000018F System.String System.Text.Json.JsonEncodedText::ToString()
extern void JsonEncodedText_ToString_mA0D2C5B818AE0E302B31B5FB722ADBFA30F5BA8B (void);
// 0x00000190 System.Int32 System.Text.Json.JsonEncodedText::GetHashCode()
extern void JsonEncodedText_GetHashCode_mCFF493ABAB2853A884DE0BCDFBB1BC35BE1C04A6 (void);
// 0x00000191 System.Void System.Text.Json.JsonException::.ctor(System.String,System.String,System.Nullable`1<System.Int64>,System.Nullable`1<System.Int64>,System.Exception)
extern void JsonException__ctor_m1C432F1AB17DC969900631573632DB232CB33047 (void);
// 0x00000192 System.Void System.Text.Json.JsonException::.ctor(System.String,System.String,System.Nullable`1<System.Int64>,System.Nullable`1<System.Int64>)
extern void JsonException__ctor_m1E761A74D58F7F5B1CBB73BA03385A15D72B533F (void);
// 0x00000193 System.Void System.Text.Json.JsonException::.ctor(System.String,System.Exception)
extern void JsonException__ctor_m40BA7C406CA01A0E34788450DF7D370FF7532DE9 (void);
// 0x00000194 System.Void System.Text.Json.JsonException::.ctor(System.String)
extern void JsonException__ctor_m728B5CA35DCEB2546A6B4BCEB0149C2152EEFD49 (void);
// 0x00000195 System.Void System.Text.Json.JsonException::.ctor()
extern void JsonException__ctor_mDEC3E59EFA840EC27C3946B5B9E2075098A5625B (void);
// 0x00000196 System.Void System.Text.Json.JsonException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonException__ctor_m31777501B4846AC4AAC92FD7CA8BAA5BBC3EA1DF (void);
// 0x00000197 System.Boolean System.Text.Json.JsonException::get_AppendPathInformation()
extern void JsonException_get_AppendPathInformation_mA5B0D2C79D49996A94B3D1F91CD212B722B24582 (void);
// 0x00000198 System.Void System.Text.Json.JsonException::set_AppendPathInformation(System.Boolean)
extern void JsonException_set_AppendPathInformation_m3DDECA6D2488441D7922477ADF57A49DA1F90605 (void);
// 0x00000199 System.Void System.Text.Json.JsonException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonException_GetObjectData_m8E0016AB0548FE7A8BDF35FAEE8E8705EC13E390 (void);
// 0x0000019A System.Nullable`1<System.Int64> System.Text.Json.JsonException::get_LineNumber()
extern void JsonException_get_LineNumber_mDF9F960ED9C7280C7B11E844A5BAD60480DB4EDA (void);
// 0x0000019B System.Void System.Text.Json.JsonException::set_LineNumber(System.Nullable`1<System.Int64>)
extern void JsonException_set_LineNumber_m6C62627B0C62AD93C308E8EC768A92061394DC78 (void);
// 0x0000019C System.Nullable`1<System.Int64> System.Text.Json.JsonException::get_BytePositionInLine()
extern void JsonException_get_BytePositionInLine_mE44FEC698C6386B30A3BC61394AF59E9B5EA5B0C (void);
// 0x0000019D System.Void System.Text.Json.JsonException::set_BytePositionInLine(System.Nullable`1<System.Int64>)
extern void JsonException_set_BytePositionInLine_mC4D58939BBBC83722253ABA6BDE1A83C232397C4 (void);
// 0x0000019E System.String System.Text.Json.JsonException::get_Path()
extern void JsonException_get_Path_mEEBE83065DDE2080206A9FD4543C296CF08AEA3C (void);
// 0x0000019F System.Void System.Text.Json.JsonException::set_Path(System.String)
extern void JsonException_set_Path_mB5FD61A615FEF3A1CB865A643014E6D0AB55A32B (void);
// 0x000001A0 System.String System.Text.Json.JsonException::get_Message()
extern void JsonException_get_Message_mD662F1821A73A4F62CA214ED78E53995819682EA (void);
// 0x000001A1 System.Void System.Text.Json.JsonException::SetMessage(System.String)
extern void JsonException_SetMessage_m30E6A9D577ADCE49E7E6EC886351CFBD9684C540 (void);
// 0x000001A2 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonHelpers::GetSpan(System.Text.Json.Utf8JsonReader&)
extern void JsonHelpers_GetSpan_m094FA1F64075E4554D87D94CC6D1971C4961FB8B (void);
// 0x000001A3 System.Boolean System.Text.Json.JsonHelpers::IsInRangeInclusive(System.UInt32,System.UInt32,System.UInt32)
extern void JsonHelpers_IsInRangeInclusive_m5A57568AE3106614F199BB8CA2D86C636B709020 (void);
// 0x000001A4 System.Boolean System.Text.Json.JsonHelpers::IsInRangeInclusive(System.Int32,System.Int32,System.Int32)
extern void JsonHelpers_IsInRangeInclusive_mA3EBC817F38C1240438A438AF5D7F1AC0D1179D3 (void);
// 0x000001A5 System.Boolean System.Text.Json.JsonHelpers::IsInRangeInclusive(System.Int64,System.Int64,System.Int64)
extern void JsonHelpers_IsInRangeInclusive_m218E16DFBF192F48B74D08EBF9384E45FBADB02F (void);
// 0x000001A6 System.Boolean System.Text.Json.JsonHelpers::IsDigit(System.Byte)
extern void JsonHelpers_IsDigit_mEC357A2F49C864A81517680770AAD23F8EF84689 (void);
// 0x000001A7 System.Void System.Text.Json.JsonHelpers::ReadWithVerify(System.Text.Json.Utf8JsonReader&)
extern void JsonHelpers_ReadWithVerify_mC62AC4ED73712CFF23DB3E2A432E7D25EF71A86F (void);
// 0x000001A8 System.String System.Text.Json.JsonHelpers::Utf8GetString(System.ReadOnlySpan`1<System.Byte>)
extern void JsonHelpers_Utf8GetString_m76C5E13DB097957F1383ECF2C351ED4D6D10AC9A (void);
// 0x000001A9 System.Boolean System.Text.Json.JsonHelpers::TryAdd(System.Collections.Generic.Dictionary`2<TKey,TValue>,TKey&,TValue&)
// 0x000001AA System.Boolean System.Text.Json.JsonHelpers::IsFinite(System.Double)
extern void JsonHelpers_IsFinite_m7A314B8340B765C430A9D82C6BA0E299F8BBFF77 (void);
// 0x000001AB System.Boolean System.Text.Json.JsonHelpers::IsFinite(System.Single)
extern void JsonHelpers_IsFinite_mAE82735137E005E75F6535A32F4C0E1CF28B9D5D (void);
// 0x000001AC System.Boolean System.Text.Json.JsonHelpers::IsValidDateTimeOffsetParseLength(System.Int32)
extern void JsonHelpers_IsValidDateTimeOffsetParseLength_mF4C79BB778330902E5250C000092EE723722F5C4 (void);
// 0x000001AD System.Boolean System.Text.Json.JsonHelpers::IsValidDateTimeOffsetParseLength(System.Int64)
extern void JsonHelpers_IsValidDateTimeOffsetParseLength_m46C66CF4904AAC707A86D55F2D1A5E97835034C9 (void);
// 0x000001AE System.Boolean System.Text.Json.JsonHelpers::TryParseAsISO(System.ReadOnlySpan`1<System.Byte>,System.DateTime&)
extern void JsonHelpers_TryParseAsISO_m2507DBB2887E5A78083371D322098729F0EF1399 (void);
// 0x000001AF System.Boolean System.Text.Json.JsonHelpers::TryParseAsISO(System.ReadOnlySpan`1<System.Byte>,System.DateTimeOffset&)
extern void JsonHelpers_TryParseAsISO_mDAFD46B648F3C83E7D027043C48171F3753E592A (void);
// 0x000001B0 System.Boolean System.Text.Json.JsonHelpers::TryParseDateTimeOffset(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonHelpers/DateTimeParseData&)
extern void JsonHelpers_TryParseDateTimeOffset_m96EC4831B67C0669D200F3C8F75DEB2CD71D5635 (void);
// 0x000001B1 System.Boolean System.Text.Json.JsonHelpers::TryGetNextTwoDigits(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void JsonHelpers_TryGetNextTwoDigits_m6E3351EBB772DD1CD455B346FC59CC07C90E9F65 (void);
// 0x000001B2 System.Boolean System.Text.Json.JsonHelpers::TryCreateDateTimeOffset(System.DateTime,System.Text.Json.JsonHelpers/DateTimeParseData&,System.DateTimeOffset&)
extern void JsonHelpers_TryCreateDateTimeOffset_mA2C13B99E1B0AF9A0C8610DBE6C127562A5B3C86 (void);
// 0x000001B3 System.Boolean System.Text.Json.JsonHelpers::TryCreateDateTimeOffset(System.Text.Json.JsonHelpers/DateTimeParseData&,System.DateTimeOffset&)
extern void JsonHelpers_TryCreateDateTimeOffset_m8911B050AA61D053EF7DC6FD8A0394FB21F55CD3 (void);
// 0x000001B4 System.Boolean System.Text.Json.JsonHelpers::TryCreateDateTimeOffsetInterpretingDataAsLocalTime(System.Text.Json.JsonHelpers/DateTimeParseData,System.DateTimeOffset&)
extern void JsonHelpers_TryCreateDateTimeOffsetInterpretingDataAsLocalTime_m7432A7F6E64532D7B9E231C000CCCD7366013D3C (void);
// 0x000001B5 System.Boolean System.Text.Json.JsonHelpers::TryCreateDateTime(System.Text.Json.JsonHelpers/DateTimeParseData,System.DateTimeKind,System.DateTime&)
extern void JsonHelpers_TryCreateDateTime_mB28773F8B027544DACCFA3FC5724DAAEA152DDE5 (void);
// 0x000001B6 System.Byte[] System.Text.Json.JsonHelpers::GetEscapedPropertyNameSection(System.ReadOnlySpan`1<System.Byte>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonHelpers_GetEscapedPropertyNameSection_m783398E43BBF77DAED66330ABD8842AF57C0C007 (void);
// 0x000001B7 System.Byte[] System.Text.Json.JsonHelpers::EscapeValue(System.ReadOnlySpan`1<System.Byte>,System.Int32,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonHelpers_EscapeValue_m3E2E9D0720AC01CEEEB056913005AB731D072B18 (void);
// 0x000001B8 System.Byte[] System.Text.Json.JsonHelpers::GetEscapedPropertyNameSection(System.ReadOnlySpan`1<System.Byte>,System.Int32,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonHelpers_GetEscapedPropertyNameSection_m2CB915E336BB039CF3018EF219B3C5A18014349D (void);
// 0x000001B9 System.Byte[] System.Text.Json.JsonHelpers::GetPropertyNameSection(System.ReadOnlySpan`1<System.Byte>)
extern void JsonHelpers_GetPropertyNameSection_m65A8494205AA2FBC7557A05CECFE9017F8CBEF7D (void);
// 0x000001BA System.Void System.Text.Json.JsonHelpers::.cctor()
extern void JsonHelpers__cctor_m093F39C2E6018C5F89AF682FFDE27F0D8A0CF4B9 (void);
// 0x000001BB System.Boolean System.Text.Json.JsonHelpers::<TryParseDateTimeOffset>g__ParseOffset|21_0(System.Text.Json.JsonHelpers/DateTimeParseData&,System.ReadOnlySpan`1<System.Byte>)
extern void JsonHelpers_U3CTryParseDateTimeOffsetU3Eg__ParseOffsetU7C21_0_m6E9B55C841FFDF04FC7AE12075C9FB911D9F90A8 (void);
// 0x000001BC System.Boolean System.Text.Json.JsonHelpers/DateTimeParseData::get_OffsetNegative()
extern void DateTimeParseData_get_OffsetNegative_m6E163581952036148996FE989BA72F37B7178A3F (void);
// 0x000001BD System.Void System.Text.Json.JsonReaderException::.ctor(System.String,System.Int64,System.Int64)
extern void JsonReaderException__ctor_m14B8A4DFCA96A371826CF3FAD95FD304B599FE27 (void);
// 0x000001BE System.Void System.Text.Json.JsonReaderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonReaderException__ctor_m397CF90939F7B7BC02AA246A16C297E27429F122 (void);
// 0x000001BF System.ValueTuple`2<System.Int32,System.Int32> System.Text.Json.JsonReaderHelper::CountNewLines(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_CountNewLines_m9B18D4780FC63C9B7787C34FE01282BDBB46A69D (void);
// 0x000001C0 System.Text.Json.JsonValueKind System.Text.Json.JsonReaderHelper::ToValueKind(System.Text.Json.JsonTokenType)
extern void JsonReaderHelper_ToValueKind_m1F17C11CB61DCAEF1D12E3B9E5D3C179B9A4C927 (void);
// 0x000001C1 System.Boolean System.Text.Json.JsonReaderHelper::IsTokenTypePrimitive(System.Text.Json.JsonTokenType)
extern void JsonReaderHelper_IsTokenTypePrimitive_m3875B80E4B968F0B3AB821DD40B93DFCE5D94284 (void);
// 0x000001C2 System.Boolean System.Text.Json.JsonReaderHelper::IsHexDigit(System.Byte)
extern void JsonReaderHelper_IsHexDigit_m48B7245415C91527D78B34EE7488C0AAF632306F (void);
// 0x000001C3 System.Int32 System.Text.Json.JsonReaderHelper::IndexOfQuoteOrAnyControlOrBackSlash(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_IndexOfQuoteOrAnyControlOrBackSlash_m11D4FB3397E71F357AE1EF90148D6A48072B1795 (void);
// 0x000001C4 System.Int32 System.Text.Json.JsonReaderHelper::IndexOfOrLessThan(System.Byte&,System.Byte,System.Byte,System.Byte,System.Int32)
extern void JsonReaderHelper_IndexOfOrLessThan_m474697A56E76C9C5495A254F1FFE380EFF8F2A1B (void);
// 0x000001C5 System.Int32 System.Text.Json.JsonReaderHelper::LocateFirstFoundByte(System.Numerics.Vector`1<System.Byte>)
extern void JsonReaderHelper_LocateFirstFoundByte_m60AD06C7AE32AA2DCAC5C6BD7C549531370843F7 (void);
// 0x000001C6 System.Int32 System.Text.Json.JsonReaderHelper::LocateFirstFoundByte(System.UInt64)
extern void JsonReaderHelper_LocateFirstFoundByte_mEF86A92884308A164D027B38113917D8E115AC4B (void);
// 0x000001C7 System.Boolean System.Text.Json.JsonReaderHelper::TryGetEscapedDateTime(System.ReadOnlySpan`1<System.Byte>,System.DateTime&)
extern void JsonReaderHelper_TryGetEscapedDateTime_mB524532A1C43E4ED359E2BB16C0936CA160FBE3D (void);
// 0x000001C8 System.Boolean System.Text.Json.JsonReaderHelper::TryGetEscapedDateTimeOffset(System.ReadOnlySpan`1<System.Byte>,System.DateTimeOffset&)
extern void JsonReaderHelper_TryGetEscapedDateTimeOffset_m3C682D1CF044AC310FC84818273CB008FD8B3350 (void);
// 0x000001C9 System.Boolean System.Text.Json.JsonReaderHelper::TryGetEscapedGuid(System.ReadOnlySpan`1<System.Byte>,System.Guid&)
extern void JsonReaderHelper_TryGetEscapedGuid_mD1C277DFD799FAE37646D5FD767A51D8DA2DEB68 (void);
// 0x000001CA System.Char System.Text.Json.JsonReaderHelper::GetFloatingPointStandardParseFormat(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_GetFloatingPointStandardParseFormat_mB23C1B1EBBD4F4DAAC029F401D150785F6DB4AB5 (void);
// 0x000001CB System.Boolean System.Text.Json.JsonReaderHelper::TryGetFloatingPointConstant(System.ReadOnlySpan`1<System.Byte>,System.Single&)
extern void JsonReaderHelper_TryGetFloatingPointConstant_m078B4EA3A481A4E6FF1C72800C4225E87038EA3E (void);
// 0x000001CC System.Boolean System.Text.Json.JsonReaderHelper::TryGetFloatingPointConstant(System.ReadOnlySpan`1<System.Byte>,System.Double&)
extern void JsonReaderHelper_TryGetFloatingPointConstant_m767E23B30F8253F20ED0268D73D737BEFD0D510D (void);
// 0x000001CD System.Boolean System.Text.Json.JsonReaderHelper::TryGetUnescapedBase64Bytes(System.ReadOnlySpan`1<System.Byte>,System.Int32,System.Byte[]&)
extern void JsonReaderHelper_TryGetUnescapedBase64Bytes_m04BFB37655BB206FCDE2FB38D53F7A071621B244 (void);
// 0x000001CE System.String System.Text.Json.JsonReaderHelper::GetUnescapedString(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void JsonReaderHelper_GetUnescapedString_m02CC3AA07EA7E8683C2D0E929934D33B95665E40 (void);
// 0x000001CF System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonReaderHelper::GetUnescapedSpan(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void JsonReaderHelper_GetUnescapedSpan_m3A30EECA3C64BFC31103789E917A1B8C114F3D11 (void);
// 0x000001D0 System.Boolean System.Text.Json.JsonReaderHelper::UnescapeAndCompare(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_UnescapeAndCompare_m6E913E97842F4832A7CD91E4630431DCC0F8E85D (void);
// 0x000001D1 System.Boolean System.Text.Json.JsonReaderHelper::TryDecodeBase64InPlace(System.Span`1<System.Byte>,System.Byte[]&)
extern void JsonReaderHelper_TryDecodeBase64InPlace_mD30B156D52BEA1265DA38A811C5A0B2CAFF66F5C (void);
// 0x000001D2 System.Boolean System.Text.Json.JsonReaderHelper::TryDecodeBase64(System.ReadOnlySpan`1<System.Byte>,System.Byte[]&)
extern void JsonReaderHelper_TryDecodeBase64_mF72B51F191B3EA1A36C7A6E1EDA281C6794E4547 (void);
// 0x000001D3 System.String System.Text.Json.JsonReaderHelper::TranscodeHelper(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_TranscodeHelper_m7D2B2896AD860AE873EF6AA8F3C42DB2266D8F86 (void);
// 0x000001D4 System.Int32 System.Text.Json.JsonReaderHelper::GetUtf8ByteCount(System.ReadOnlySpan`1<System.Char>)
extern void JsonReaderHelper_GetUtf8ByteCount_m10368357E772BAD837731771AB26854EBBFB916A (void);
// 0x000001D5 System.Int32 System.Text.Json.JsonReaderHelper::GetUtf8FromText(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Byte>)
extern void JsonReaderHelper_GetUtf8FromText_m08B25D4555626637C50AD6639EFDAA1643E4DAFD (void);
// 0x000001D6 System.String System.Text.Json.JsonReaderHelper::GetTextFromUtf8(System.ReadOnlySpan`1<System.Byte>)
extern void JsonReaderHelper_GetTextFromUtf8_m8E608A928E234BA9AE70A69F77D7829BBCEDB9A8 (void);
// 0x000001D7 System.Void System.Text.Json.JsonReaderHelper::Unescape(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32,System.Int32&)
extern void JsonReaderHelper_Unescape_m78415A45DF58099511B97E969EF2BD51EEFB41AA (void);
// 0x000001D8 System.Void System.Text.Json.JsonReaderHelper::EncodeToUtf8Bytes(System.UInt32,System.Span`1<System.Byte>,System.Int32&)
extern void JsonReaderHelper_EncodeToUtf8Bytes_m6E588C811CA1B083C63540B60EF697E32C96FDC6 (void);
// 0x000001D9 System.Void System.Text.Json.JsonReaderHelper::.cctor()
extern void JsonReaderHelper__cctor_m090B44C9FF4641AF05F6203A860D178F63E7B591 (void);
// 0x000001DA System.Text.Json.JsonCommentHandling System.Text.Json.JsonReaderOptions::get_CommentHandling()
extern void JsonReaderOptions_get_CommentHandling_m785941C88C0D78EE452E47079133CA53EAA86918 (void);
// 0x000001DB System.Void System.Text.Json.JsonReaderOptions::set_CommentHandling(System.Text.Json.JsonCommentHandling)
extern void JsonReaderOptions_set_CommentHandling_m3A14D93E8354AB403D202AC5352A0355DEF47641 (void);
// 0x000001DC System.Int32 System.Text.Json.JsonReaderOptions::get_MaxDepth()
extern void JsonReaderOptions_get_MaxDepth_m2F0DA8E50E5D545945DB32DFFFA74E941849F3F6 (void);
// 0x000001DD System.Void System.Text.Json.JsonReaderOptions::set_MaxDepth(System.Int32)
extern void JsonReaderOptions_set_MaxDepth_m2AEBFDFBFEC808E1E4902FFF561FAC990FD8420F (void);
// 0x000001DE System.Boolean System.Text.Json.JsonReaderOptions::get_AllowTrailingCommas()
extern void JsonReaderOptions_get_AllowTrailingCommas_m8D5027DF6AD1F91F789AC6AF9A7BF550E81B256D (void);
// 0x000001DF System.Void System.Text.Json.JsonReaderOptions::set_AllowTrailingCommas(System.Boolean)
extern void JsonReaderOptions_set_AllowTrailingCommas_mB7C7B82225E31EFECAC172BC8CBF073C09A4036B (void);
// 0x000001E0 System.Void System.Text.Json.JsonReaderState::.ctor(System.Text.Json.JsonReaderOptions)
extern void JsonReaderState__ctor_mD1D4FD65E3D005FD3B169BE5F949DCBCB638D5B2 (void);
// 0x000001E1 System.Text.Json.JsonReaderOptions System.Text.Json.JsonReaderState::get_Options()
extern void JsonReaderState_get_Options_mC7B2AAB4CD55B90A1EB81F03B82BDEB9E8ED3894 (void);
// 0x000001E2 System.Boolean System.Text.Json.Utf8JsonReader::get_IsLastSpan()
extern void Utf8JsonReader_get_IsLastSpan_m0D510710CF414FB8B8403E4051E06289C14E5B17 (void);
// 0x000001E3 System.Buffers.ReadOnlySequence`1<System.Byte> System.Text.Json.Utf8JsonReader::get_OriginalSequence()
extern void Utf8JsonReader_get_OriginalSequence_mFC7BF57B6E80D112834FAF24855EF4FC82F928B0 (void);
// 0x000001E4 System.ReadOnlySpan`1<System.Byte> System.Text.Json.Utf8JsonReader::get_OriginalSpan()
extern void Utf8JsonReader_get_OriginalSpan_mCA0CC6DA6C7595E089AE833D3E8FAD70196ED6D3 (void);
// 0x000001E5 System.ReadOnlySpan`1<System.Byte> System.Text.Json.Utf8JsonReader::get_ValueSpan()
extern void Utf8JsonReader_get_ValueSpan_mD7C0AA41BFEC523AEF71D7AF4BFF784F84CC9AF8 (void);
// 0x000001E6 System.Void System.Text.Json.Utf8JsonReader::set_ValueSpan(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_set_ValueSpan_m37AB280FA0870B85790F31CC32FE61A145BB73DE (void);
// 0x000001E7 System.Int64 System.Text.Json.Utf8JsonReader::get_BytesConsumed()
extern void Utf8JsonReader_get_BytesConsumed_mEF9EB31417202F307C10482D117C0D1F1383746B (void);
// 0x000001E8 System.Int64 System.Text.Json.Utf8JsonReader::get_TokenStartIndex()
extern void Utf8JsonReader_get_TokenStartIndex_m616B80F3D48063970EF25BC002FE2B7512948D3E (void);
// 0x000001E9 System.Void System.Text.Json.Utf8JsonReader::set_TokenStartIndex(System.Int64)
extern void Utf8JsonReader_set_TokenStartIndex_m391AF219C657ED98FC368E82D7129EA13E4B744F (void);
// 0x000001EA System.Int32 System.Text.Json.Utf8JsonReader::get_CurrentDepth()
extern void Utf8JsonReader_get_CurrentDepth_m09EED2F622F6DF3BE234674E7532337C2728EBA9 (void);
// 0x000001EB System.Boolean System.Text.Json.Utf8JsonReader::get_IsInArray()
extern void Utf8JsonReader_get_IsInArray_m5633237E6C12D3D53E4F623953D307DC58C3E4EE (void);
// 0x000001EC System.Text.Json.JsonTokenType System.Text.Json.Utf8JsonReader::get_TokenType()
extern void Utf8JsonReader_get_TokenType_mCE6BF109ADE03F304F8C16D68AF519C5CFBA631A (void);
// 0x000001ED System.Boolean System.Text.Json.Utf8JsonReader::get_HasValueSequence()
extern void Utf8JsonReader_get_HasValueSequence_m3EA84B4D984814A502FD58E70EC154B6DE181A5F (void);
// 0x000001EE System.Void System.Text.Json.Utf8JsonReader::set_HasValueSequence(System.Boolean)
extern void Utf8JsonReader_set_HasValueSequence_m16BA0D508AFFA53D77D2924821C4BFB400EACBA4 (void);
// 0x000001EF System.Boolean System.Text.Json.Utf8JsonReader::get_IsFinalBlock()
extern void Utf8JsonReader_get_IsFinalBlock_m4FF1E669D83E57A86A0B60261A8027A3D2A9D8E5 (void);
// 0x000001F0 System.Buffers.ReadOnlySequence`1<System.Byte> System.Text.Json.Utf8JsonReader::get_ValueSequence()
extern void Utf8JsonReader_get_ValueSequence_m881899F712004242C5F89AE5899A105AEBBC295F (void);
// 0x000001F1 System.Void System.Text.Json.Utf8JsonReader::set_ValueSequence(System.Buffers.ReadOnlySequence`1<System.Byte>)
extern void Utf8JsonReader_set_ValueSequence_mCB1696984F939D6F8012728EA22DEC31D54F2B02 (void);
// 0x000001F2 System.Text.Json.JsonReaderState System.Text.Json.Utf8JsonReader::get_CurrentState()
extern void Utf8JsonReader_get_CurrentState_mA51B5336D4893034ED7344FED22DB0FFA6E7C796 (void);
// 0x000001F3 System.Void System.Text.Json.Utf8JsonReader::.ctor(System.ReadOnlySpan`1<System.Byte>,System.Boolean,System.Text.Json.JsonReaderState)
extern void Utf8JsonReader__ctor_mF8257CB8E2F367ADFA10F6A9B1B1E22A92039A75 (void);
// 0x000001F4 System.Boolean System.Text.Json.Utf8JsonReader::Read()
extern void Utf8JsonReader_Read_mD210940FACF194A87F83C11308A87397B77DCB07 (void);
// 0x000001F5 System.Void System.Text.Json.Utf8JsonReader::Skip()
extern void Utf8JsonReader_Skip_m4DA7165BB21473A73328812E2541A62476BFBA5D (void);
// 0x000001F6 System.Void System.Text.Json.Utf8JsonReader::SkipHelper()
extern void Utf8JsonReader_SkipHelper_mF4192AF21A1478DCD7D1313EC666F98462376E6D (void);
// 0x000001F7 System.Boolean System.Text.Json.Utf8JsonReader::TrySkip()
extern void Utf8JsonReader_TrySkip_mAA1EF79F9187B277465240CE3699015D57F85196 (void);
// 0x000001F8 System.Boolean System.Text.Json.Utf8JsonReader::TrySkipHelper()
extern void Utf8JsonReader_TrySkipHelper_m2DDAA3DB1C0BF9547970FCED4E3F7DE403489017 (void);
// 0x000001F9 System.Void System.Text.Json.Utf8JsonReader::StartObject()
extern void Utf8JsonReader_StartObject_mC83FBAF3E163D146444B2868D4AFCC959FA19F7C (void);
// 0x000001FA System.Void System.Text.Json.Utf8JsonReader::EndObject()
extern void Utf8JsonReader_EndObject_m5F1441EE29712895D6102136C79EF57D88B84A2B (void);
// 0x000001FB System.Void System.Text.Json.Utf8JsonReader::StartArray()
extern void Utf8JsonReader_StartArray_m6DA2C6FA69288410291C62C4A3976ABA06E9EC06 (void);
// 0x000001FC System.Void System.Text.Json.Utf8JsonReader::EndArray()
extern void Utf8JsonReader_EndArray_m7B69D7C38934493393321F3A27365A4B67FBB4C3 (void);
// 0x000001FD System.Void System.Text.Json.Utf8JsonReader::UpdateBitStackOnEndToken()
extern void Utf8JsonReader_UpdateBitStackOnEndToken_m384055041D18215523F5DCB0DC4F9BF8B564DA11 (void);
// 0x000001FE System.Boolean System.Text.Json.Utf8JsonReader::ReadSingleSegment()
extern void Utf8JsonReader_ReadSingleSegment_m947CD352F825A73A97962979917C1DE44CC2A548 (void);
// 0x000001FF System.Boolean System.Text.Json.Utf8JsonReader::HasMoreData()
extern void Utf8JsonReader_HasMoreData_mF3B6BF4E12A6153B8D0696EB6E4DEFC68FC40B23 (void);
// 0x00000200 System.Boolean System.Text.Json.Utf8JsonReader::HasMoreData(System.Text.Json.ExceptionResource)
extern void Utf8JsonReader_HasMoreData_m50F0F11A42D737D72C7BFA79D53F0133C69D1BC9 (void);
// 0x00000201 System.Boolean System.Text.Json.Utf8JsonReader::ReadFirstToken(System.Byte)
extern void Utf8JsonReader_ReadFirstToken_mD90BB812B023F17A3EBBD30A3983D9791639372E (void);
// 0x00000202 System.Void System.Text.Json.Utf8JsonReader::SkipWhiteSpace()
extern void Utf8JsonReader_SkipWhiteSpace_m04627FB93ABE8F3D19F2EBB6EEBAE3E1F40B5E68 (void);
// 0x00000203 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeValue(System.Byte)
extern void Utf8JsonReader_ConsumeValue_m846BBF871AFF435D23E18421514C2C3BF8CE156F (void);
// 0x00000204 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeLiteral(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonTokenType)
extern void Utf8JsonReader_ConsumeLiteral_m89BFF61B6087D786F73E77F1D812F611126CAB10 (void);
// 0x00000205 System.Boolean System.Text.Json.Utf8JsonReader::CheckLiteral(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_CheckLiteral_mC50D33BAC348FD003A97DA41F7BCCE3BB0DB9988 (void);
// 0x00000206 System.Void System.Text.Json.Utf8JsonReader::ThrowInvalidLiteral(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_ThrowInvalidLiteral_m3182F7C44BEF0C05A91ADFA8087F952FDFF2064A (void);
// 0x00000207 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeNumber()
extern void Utf8JsonReader_ConsumeNumber_m40F72939079F09EA3A3F8C2FCE2E05FE48C049AF (void);
// 0x00000208 System.Boolean System.Text.Json.Utf8JsonReader::ConsumePropertyName()
extern void Utf8JsonReader_ConsumePropertyName_m049CE9287BA5B4C7AF8F8C7D68EBA79C82A7451A (void);
// 0x00000209 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeString()
extern void Utf8JsonReader_ConsumeString_mF022344616A784A7765F0569118543E35C5F9498 (void);
// 0x0000020A System.Boolean System.Text.Json.Utf8JsonReader::ConsumeStringAndValidate(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ConsumeStringAndValidate_mAFEBEA2CDF74CC836345B04881A2903E05C1985B (void);
// 0x0000020B System.Boolean System.Text.Json.Utf8JsonReader::ValidateHexDigits(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ValidateHexDigits_m0309E92E7DC46970B49FCBAD399D28E39F10069A (void);
// 0x0000020C System.Boolean System.Text.Json.Utf8JsonReader::TryGetNumber(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_TryGetNumber_mDDD3CE6A95A382184215D8DB6A31D767AFECD6CF (void);
// 0x0000020D System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeNegativeSign(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeNegativeSign_m4C8B15B4D156BE3117809A55CD2C19B8725DABEC (void);
// 0x0000020E System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeZero(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeZero_mDAB03B486171BA7CBCCA27B47FA6BF43186E299C (void);
// 0x0000020F System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeIntegerDigits(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeIntegerDigits_m220F559BD087754C8B69A37459241981A0C82783 (void);
// 0x00000210 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeDecimalDigits(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeDecimalDigits_mFD41DF26D628251C7BBD82174BF64846A0C83B2E (void);
// 0x00000211 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeSign(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeSign_m0620EDBAA824B5FC333F926075A607FC985C476D (void);
// 0x00000212 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeNextTokenOrRollback(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenOrRollback_m417EB8BF1E5C468E57452D00D30806FA775542E4 (void);
// 0x00000213 System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextToken(System.Byte)
extern void Utf8JsonReader_ConsumeNextToken_m7D4010DE7EC28BF0391950239A30A423715CDF51 (void);
// 0x00000214 System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenFromLastNonCommentToken()
extern void Utf8JsonReader_ConsumeNextTokenFromLastNonCommentToken_mBA25C73B75A78924E0E71612292917FB405C8B5B (void);
// 0x00000215 System.Boolean System.Text.Json.Utf8JsonReader::SkipAllComments(System.Byte&)
extern void Utf8JsonReader_SkipAllComments_m2D586CC874AC235E5DAA1A76BCCECCEDF8C10F34 (void);
// 0x00000216 System.Boolean System.Text.Json.Utf8JsonReader::SkipAllComments(System.Byte&,System.Text.Json.ExceptionResource)
extern void Utf8JsonReader_SkipAllComments_m35A788BF6302E95161122D7993E69BC373C64F9F (void);
// 0x00000217 System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenUntilAfterAllCommentsAreSkipped(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkipped_m7E03615D1717381CA3DAC23B30AE4E009B96F2BC (void);
// 0x00000218 System.Boolean System.Text.Json.Utf8JsonReader::SkipComment()
extern void Utf8JsonReader_SkipComment_mB8CC711E5F3D6D24E5EE25198DBDB7F0B3B8B52B (void);
// 0x00000219 System.Boolean System.Text.Json.Utf8JsonReader::SkipSingleLineComment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_SkipSingleLineComment_mEFE4A7A109C1F3E01064F405520C52B76E793DC3 (void);
// 0x0000021A System.Int32 System.Text.Json.Utf8JsonReader::FindLineSeparator(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_FindLineSeparator_m9C0187692AE1FC17F7B40A7DDB0ACB7FBFEB340A (void);
// 0x0000021B System.Void System.Text.Json.Utf8JsonReader::ThrowOnDangerousLineSeparator(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_ThrowOnDangerousLineSeparator_mCA676223806C01CA53EC299BFC2C5BC48060EBD2 (void);
// 0x0000021C System.Boolean System.Text.Json.Utf8JsonReader::SkipMultiLineComment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_SkipMultiLineComment_m8C37CD84ABC6DA62709D69A8D23C779043D025F5 (void);
// 0x0000021D System.Boolean System.Text.Json.Utf8JsonReader::ConsumeComment()
extern void Utf8JsonReader_ConsumeComment_mF43CB1C22ECE1F624E0B562D93A099C01216B78E (void);
// 0x0000021E System.Boolean System.Text.Json.Utf8JsonReader::ConsumeSingleLineComment(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ConsumeSingleLineComment_m55604E2C6DEA3EA494E5663AC035A2131010D844 (void);
// 0x0000021F System.Boolean System.Text.Json.Utf8JsonReader::ConsumeMultiLineComment(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ConsumeMultiLineComment_m27C5FF33B76C1C46A721951202BE2983F4F0961B (void);
// 0x00000220 System.ReadOnlySpan`1<System.Byte> System.Text.Json.Utf8JsonReader::GetUnescapedSpan()
extern void Utf8JsonReader_GetUnescapedSpan_mFAEB005896BE9873061EC7843FD2CDC107B3DE3F (void);
// 0x00000221 System.Boolean System.Text.Json.Utf8JsonReader::ReadMultiSegment()
extern void Utf8JsonReader_ReadMultiSegment_m34ED95DFD5361E1AF24A36E81C9059A00853C644 (void);
// 0x00000222 System.Boolean System.Text.Json.Utf8JsonReader::ValidateStateAtEndOfData()
extern void Utf8JsonReader_ValidateStateAtEndOfData_mE1A59B817BD48A6F2809EEBC7F464A0E28196FF7 (void);
// 0x00000223 System.Boolean System.Text.Json.Utf8JsonReader::HasMoreDataMultiSegment()
extern void Utf8JsonReader_HasMoreDataMultiSegment_m31E6091999973DF6F5E877267CD93D1CCDE10AA9 (void);
// 0x00000224 System.Boolean System.Text.Json.Utf8JsonReader::HasMoreDataMultiSegment(System.Text.Json.ExceptionResource)
extern void Utf8JsonReader_HasMoreDataMultiSegment_mF1B6563CC059AB8F085A0D8A33A11756EDB0B22C (void);
// 0x00000225 System.Boolean System.Text.Json.Utf8JsonReader::GetNextSpan()
extern void Utf8JsonReader_GetNextSpan_m9C9704D9B74FE08A99E2D13820494D10BD775BF3 (void);
// 0x00000226 System.Boolean System.Text.Json.Utf8JsonReader::ReadFirstTokenMultiSegment(System.Byte)
extern void Utf8JsonReader_ReadFirstTokenMultiSegment_mE99805A003E45117C29863C73E136DF8622096CB (void);
// 0x00000227 System.Void System.Text.Json.Utf8JsonReader::SkipWhiteSpaceMultiSegment()
extern void Utf8JsonReader_SkipWhiteSpaceMultiSegment_m738D7B895E031A7F201D91EC074AAFEE27429A1E (void);
// 0x00000228 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeValueMultiSegment(System.Byte)
extern void Utf8JsonReader_ConsumeValueMultiSegment_mCE9C63707898C340F9731E600FEE7A2F138D0DBC (void);
// 0x00000229 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeLiteralMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.JsonTokenType)
extern void Utf8JsonReader_ConsumeLiteralMultiSegment_m8CE5FBA9EBF636905ABA1189DEA46B55877C255C (void);
// 0x0000022A System.Boolean System.Text.Json.Utf8JsonReader::CheckLiteralMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_CheckLiteralMultiSegment_m29DBBFB73EDCEDE193F51D9FDDE5CDE7327DF107 (void);
// 0x0000022B System.Int32 System.Text.Json.Utf8JsonReader::FindMismatch(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_FindMismatch_m3CFC046BA471A51916E4A98AF3C2B055513722FC (void);
// 0x0000022C System.Text.Json.JsonException System.Text.Json.Utf8JsonReader::GetInvalidLiteralMultiSegment(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_GetInvalidLiteralMultiSegment_m1D4FD07D765B89E82B186D3132EEDA513C420D5D (void);
// 0x0000022D System.Boolean System.Text.Json.Utf8JsonReader::ConsumeNumberMultiSegment()
extern void Utf8JsonReader_ConsumeNumberMultiSegment_mE9CA9359DADDC9A32A0B332B5CE97D5917A99C81 (void);
// 0x0000022E System.Boolean System.Text.Json.Utf8JsonReader::ConsumePropertyNameMultiSegment()
extern void Utf8JsonReader_ConsumePropertyNameMultiSegment_m3A62F15386C832D140978B9FB7C4533F22F19BDC (void);
// 0x0000022F System.Boolean System.Text.Json.Utf8JsonReader::ConsumeStringMultiSegment()
extern void Utf8JsonReader_ConsumeStringMultiSegment_m9AC39085975494E255DE8A0E3F6F319895CFDBA8 (void);
// 0x00000230 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeStringNextSegment()
extern void Utf8JsonReader_ConsumeStringNextSegment_m85FB7C7822B8165B3A01A6A5E7943EF09212AE1A (void);
// 0x00000231 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeStringAndValidateMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonReader_ConsumeStringAndValidateMultiSegment_mB975AD804E2DE7E80CCE8106DE68C961A6443BE3 (void);
// 0x00000232 System.Void System.Text.Json.Utf8JsonReader::RollBackState(System.Text.Json.Utf8JsonReader/PartialStateForRollback&,System.Boolean)
extern void Utf8JsonReader_RollBackState_m14899B240A51A2C5298DE5D9CC5C20B68337A8F5 (void);
// 0x00000233 System.Boolean System.Text.Json.Utf8JsonReader::TryGetNumberMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_TryGetNumberMultiSegment_mB941922D23D7625DA33A2411425322B95CB49758 (void);
// 0x00000234 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeNegativeSignMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&,System.Text.Json.Utf8JsonReader/PartialStateForRollback&)
extern void Utf8JsonReader_ConsumeNegativeSignMultiSegment_mDBEB1BD82E16D0A858BB9176369250A7BC16A7B0 (void);
// 0x00000235 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeZeroMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&,System.Text.Json.Utf8JsonReader/PartialStateForRollback&)
extern void Utf8JsonReader_ConsumeZeroMultiSegment_m2F501FEFAD08DB5672B8820A8C3739FACE5167B8 (void);
// 0x00000236 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeIntegerDigitsMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&)
extern void Utf8JsonReader_ConsumeIntegerDigitsMultiSegment_m5B05767D5DFC620AAAEDF25D188A1B2F7B1F3F61 (void);
// 0x00000237 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeDecimalDigitsMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&,System.Text.Json.Utf8JsonReader/PartialStateForRollback&)
extern void Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_m8B3F9AAC81565DC842D722B03F6EBF9E038EC2C6 (void);
// 0x00000238 System.Text.Json.ConsumeNumberResult System.Text.Json.Utf8JsonReader::ConsumeSignMultiSegment(System.ReadOnlySpan`1<System.Byte>&,System.Int32&,System.Text.Json.Utf8JsonReader/PartialStateForRollback&)
extern void Utf8JsonReader_ConsumeSignMultiSegment_m0BDC08DBB245EB51E464A0456FD1F9A08022A78A (void);
// 0x00000239 System.Boolean System.Text.Json.Utf8JsonReader::ConsumeNextTokenOrRollbackMultiSegment(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenOrRollbackMultiSegment_m1558F3E362E36E672D9370F677E80CF59027A03B (void);
// 0x0000023A System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenMultiSegment(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenMultiSegment_m768B8C95D7DBF55ED9EEE1A3A7E6C397A3FF2162 (void);
// 0x0000023B System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenFromLastNonCommentTokenMultiSegment()
extern void Utf8JsonReader_ConsumeNextTokenFromLastNonCommentTokenMultiSegment_mCBD2A8ECC218C80D1637EE6F4AFAD5DE7EC84E1E (void);
// 0x0000023C System.Boolean System.Text.Json.Utf8JsonReader::SkipAllCommentsMultiSegment(System.Byte&)
extern void Utf8JsonReader_SkipAllCommentsMultiSegment_m9F26B47E140957BFC485A5B1194A67FFA80AFF38 (void);
// 0x0000023D System.Boolean System.Text.Json.Utf8JsonReader::SkipAllCommentsMultiSegment(System.Byte&,System.Text.Json.ExceptionResource)
extern void Utf8JsonReader_SkipAllCommentsMultiSegment_m6BDCBE0CE1EA8656FE6ECF632FC8BFD7925039D5 (void);
// 0x0000023E System.Text.Json.ConsumeTokenResult System.Text.Json.Utf8JsonReader::ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment(System.Byte)
extern void Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment_m927DB030F5F92F4CB68854405095A7AC372E1CF9 (void);
// 0x0000023F System.Boolean System.Text.Json.Utf8JsonReader::SkipOrConsumeCommentMultiSegmentWithRollback()
extern void Utf8JsonReader_SkipOrConsumeCommentMultiSegmentWithRollback_m4A93250DEBD59CE9497CE8D857E1D1AFFAECE616 (void);
// 0x00000240 System.Boolean System.Text.Json.Utf8JsonReader::SkipCommentMultiSegment(System.Int32&)
extern void Utf8JsonReader_SkipCommentMultiSegment_m41581C771A4EAD318B08C195AE89705757923A12 (void);
// 0x00000241 System.Boolean System.Text.Json.Utf8JsonReader::SkipSingleLineCommentMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_SkipSingleLineCommentMultiSegment_m64E237F76BE411B9D1C000094FB64D3E3D921EDC (void);
// 0x00000242 System.Int32 System.Text.Json.Utf8JsonReader::FindLineSeparatorMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_FindLineSeparatorMultiSegment_m7409FC03149D983045F95B254E219425B2AF125F (void);
// 0x00000243 System.Void System.Text.Json.Utf8JsonReader::ThrowOnDangerousLineSeparatorMultiSegment(System.ReadOnlySpan`1<System.Byte>,System.Int32&)
extern void Utf8JsonReader_ThrowOnDangerousLineSeparatorMultiSegment_m71F825306CAE9334C56ACD50EA17AC32E5F53231 (void);
// 0x00000244 System.Boolean System.Text.Json.Utf8JsonReader::SkipMultiLineCommentMultiSegment(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_SkipMultiLineCommentMultiSegment_m927C47BED86E20BDB7136DE116E105CE8E52187E (void);
// 0x00000245 System.Text.Json.Utf8JsonReader/PartialStateForRollback System.Text.Json.Utf8JsonReader::CaptureState()
extern void Utf8JsonReader_CaptureState_mE854A6A21277EB2333CF3387664CDA0072756A30 (void);
// 0x00000246 System.String System.Text.Json.Utf8JsonReader::GetString()
extern void Utf8JsonReader_GetString_m3AA74B2BD8AE8211C2F45748764CA0F8D0772F11 (void);
// 0x00000247 System.Boolean System.Text.Json.Utf8JsonReader::GetBoolean()
extern void Utf8JsonReader_GetBoolean_m4A0CCA2B3FF77D11CDEF65077936B5CA951962C6 (void);
// 0x00000248 System.Byte[] System.Text.Json.Utf8JsonReader::GetBytesFromBase64()
extern void Utf8JsonReader_GetBytesFromBase64_mBD400780F573FB4057B04E9B6CE298AC931ACD5A (void);
// 0x00000249 System.Byte System.Text.Json.Utf8JsonReader::GetByte()
extern void Utf8JsonReader_GetByte_mEC280899A1AD56A59AABAF6F35E061C5A257A232 (void);
// 0x0000024A System.Byte System.Text.Json.Utf8JsonReader::GetByteWithQuotes()
extern void Utf8JsonReader_GetByteWithQuotes_mDAF5E947FAA492D8510E6BFA0AF8CC8BECA26C05 (void);
// 0x0000024B System.SByte System.Text.Json.Utf8JsonReader::GetSByte()
extern void Utf8JsonReader_GetSByte_m86C5D95B6393261601AB9C07BEABB0675C00846A (void);
// 0x0000024C System.SByte System.Text.Json.Utf8JsonReader::GetSByteWithQuotes()
extern void Utf8JsonReader_GetSByteWithQuotes_mCB8C7C905A515C7695ABC62CAEA61496AA408688 (void);
// 0x0000024D System.Int16 System.Text.Json.Utf8JsonReader::GetInt16()
extern void Utf8JsonReader_GetInt16_m486F6B00F180588F76CF037EA708079C921D5C2B (void);
// 0x0000024E System.Int16 System.Text.Json.Utf8JsonReader::GetInt16WithQuotes()
extern void Utf8JsonReader_GetInt16WithQuotes_mFBCA9FB631328EFD1F4A727A40E464784A59C4F3 (void);
// 0x0000024F System.Int32 System.Text.Json.Utf8JsonReader::GetInt32()
extern void Utf8JsonReader_GetInt32_m9994A31A6BBC68EC25BFEA942924684639E0A350 (void);
// 0x00000250 System.Int32 System.Text.Json.Utf8JsonReader::GetInt32WithQuotes()
extern void Utf8JsonReader_GetInt32WithQuotes_m26F4EF8D42082CD49AE7E2FE9683CCA264E9F746 (void);
// 0x00000251 System.Int64 System.Text.Json.Utf8JsonReader::GetInt64()
extern void Utf8JsonReader_GetInt64_m8F101B63B25099413DEC5D297BB5FFE578DE165A (void);
// 0x00000252 System.Int64 System.Text.Json.Utf8JsonReader::GetInt64WithQuotes()
extern void Utf8JsonReader_GetInt64WithQuotes_m3096847DAFC9F239BE4FAC096D247502AD492147 (void);
// 0x00000253 System.UInt16 System.Text.Json.Utf8JsonReader::GetUInt16()
extern void Utf8JsonReader_GetUInt16_mDADEC8D0157630B34BA4BED95BEC8FC0FB103F36 (void);
// 0x00000254 System.UInt16 System.Text.Json.Utf8JsonReader::GetUInt16WithQuotes()
extern void Utf8JsonReader_GetUInt16WithQuotes_m3AFF72BAC86A2C8D95E30D2CE927C58C01AD4CF5 (void);
// 0x00000255 System.UInt32 System.Text.Json.Utf8JsonReader::GetUInt32()
extern void Utf8JsonReader_GetUInt32_m727AE5A5ECBB3D29C4E592193706C81080C3AA75 (void);
// 0x00000256 System.UInt32 System.Text.Json.Utf8JsonReader::GetUInt32WithQuotes()
extern void Utf8JsonReader_GetUInt32WithQuotes_m4A9C9DF5E09E76CABB9355907A996C884EBFE5A7 (void);
// 0x00000257 System.UInt64 System.Text.Json.Utf8JsonReader::GetUInt64()
extern void Utf8JsonReader_GetUInt64_mD82438E5E150BE6DCB19E9AA8527032FE1073A7B (void);
// 0x00000258 System.UInt64 System.Text.Json.Utf8JsonReader::GetUInt64WithQuotes()
extern void Utf8JsonReader_GetUInt64WithQuotes_mDF51094C764240D18BE44B978CFFF09900AB4AE7 (void);
// 0x00000259 System.Single System.Text.Json.Utf8JsonReader::GetSingle()
extern void Utf8JsonReader_GetSingle_mE9EEE464C1E81A3F019BBA55FC21105525AEEE42 (void);
// 0x0000025A System.Single System.Text.Json.Utf8JsonReader::GetSingleWithQuotes()
extern void Utf8JsonReader_GetSingleWithQuotes_m8BDBB093E18B06F604D53E31CABDE152B79FCEFD (void);
// 0x0000025B System.Single System.Text.Json.Utf8JsonReader::GetSingleFloatingPointConstant()
extern void Utf8JsonReader_GetSingleFloatingPointConstant_m557E5950D17D08737B79AE2B41C281FC62BA1E4A (void);
// 0x0000025C System.Double System.Text.Json.Utf8JsonReader::GetDouble()
extern void Utf8JsonReader_GetDouble_m8B6B801EAD7C30EFEFCEE8CEF6D1522CA4778EE1 (void);
// 0x0000025D System.Double System.Text.Json.Utf8JsonReader::GetDoubleWithQuotes()
extern void Utf8JsonReader_GetDoubleWithQuotes_m3C6612DDA71C26099B2C314761AAF3F7CD733658 (void);
// 0x0000025E System.Double System.Text.Json.Utf8JsonReader::GetDoubleFloatingPointConstant()
extern void Utf8JsonReader_GetDoubleFloatingPointConstant_m7649E11BD1B5F7CFEABE646638133CBBF0F69976 (void);
// 0x0000025F System.Decimal System.Text.Json.Utf8JsonReader::GetDecimal()
extern void Utf8JsonReader_GetDecimal_m0F141F3F50CD249A6EA21E01188DE311EAE86F41 (void);
// 0x00000260 System.Decimal System.Text.Json.Utf8JsonReader::GetDecimalWithQuotes()
extern void Utf8JsonReader_GetDecimalWithQuotes_m8EBE26F40D4FACB3B92BD0C7F75C4AE8E5E509B3 (void);
// 0x00000261 System.DateTime System.Text.Json.Utf8JsonReader::GetDateTime()
extern void Utf8JsonReader_GetDateTime_mE98D9A548388D8526C4F0CC35899D2F14D43B22F (void);
// 0x00000262 System.DateTime System.Text.Json.Utf8JsonReader::GetDateTimeNoValidation()
extern void Utf8JsonReader_GetDateTimeNoValidation_mB25F994C72A93EA8103DD71FEE99A87D6634B104 (void);
// 0x00000263 System.DateTimeOffset System.Text.Json.Utf8JsonReader::GetDateTimeOffset()
extern void Utf8JsonReader_GetDateTimeOffset_m34029D75821CEB4740BF1D8B359747D6475C88B9 (void);
// 0x00000264 System.DateTimeOffset System.Text.Json.Utf8JsonReader::GetDateTimeOffsetNoValidation()
extern void Utf8JsonReader_GetDateTimeOffsetNoValidation_m32A0E97EF22CB407C923E5E795301AE87FE45309 (void);
// 0x00000265 System.Guid System.Text.Json.Utf8JsonReader::GetGuid()
extern void Utf8JsonReader_GetGuid_mF53227165B93F42BCC17F4212C98AE792D47699A (void);
// 0x00000266 System.Guid System.Text.Json.Utf8JsonReader::GetGuidNoValidation()
extern void Utf8JsonReader_GetGuidNoValidation_mF56895F5C26C43EAC16F16A589C002B6699C7771 (void);
// 0x00000267 System.Boolean System.Text.Json.Utf8JsonReader::TryGetBytesFromBase64(System.Byte[]&)
extern void Utf8JsonReader_TryGetBytesFromBase64_m7DEA918E54E58B402F89A126FD98D29D1D4F64A4 (void);
// 0x00000268 System.Boolean System.Text.Json.Utf8JsonReader::TryGetByte(System.Byte&)
extern void Utf8JsonReader_TryGetByte_m3A5B27FD2CA5F304075022FF7E1BC05D2441E4BB (void);
// 0x00000269 System.Boolean System.Text.Json.Utf8JsonReader::TryGetByteCore(System.Byte&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetByteCore_m37008192DDB31799D7864470CC49F8C8A0CBD778 (void);
// 0x0000026A System.Boolean System.Text.Json.Utf8JsonReader::TryGetSByte(System.SByte&)
extern void Utf8JsonReader_TryGetSByte_m59FC81E3CF3CC2BA6161271E603F4C8F96CD6329 (void);
// 0x0000026B System.Boolean System.Text.Json.Utf8JsonReader::TryGetSByteCore(System.SByte&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetSByteCore_m136CD23B2D669BC9E205AEED6B9CA0A2D79DAC43 (void);
// 0x0000026C System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt16(System.Int16&)
extern void Utf8JsonReader_TryGetInt16_mDE28715B75D564D7EB68209FCC19C1A6357DD372 (void);
// 0x0000026D System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt16Core(System.Int16&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetInt16Core_mCBDC2DF50B262CB386B03CD349C1D60176C53AFA (void);
// 0x0000026E System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt32(System.Int32&)
extern void Utf8JsonReader_TryGetInt32_m98C94CE92457BDD3B53CAED4A8701BF93EAC2617 (void);
// 0x0000026F System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt32Core(System.Int32&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetInt32Core_m088C7F21848C7409B390A4D9BCF66E1BFDFAAFD8 (void);
// 0x00000270 System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt64(System.Int64&)
extern void Utf8JsonReader_TryGetInt64_m2A2CEAA1791795E6DB1420BB381D71B6C593272F (void);
// 0x00000271 System.Boolean System.Text.Json.Utf8JsonReader::TryGetInt64Core(System.Int64&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetInt64Core_m3993E1A0CB78D318C835E868E467414537576332 (void);
// 0x00000272 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt16(System.UInt16&)
extern void Utf8JsonReader_TryGetUInt16_m22C896C87811291DFD23ED0457B38E7825D6D296 (void);
// 0x00000273 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt16Core(System.UInt16&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetUInt16Core_m9A0768CC1CF4BAC47EA82BAAD39CDF0E24C6664C (void);
// 0x00000274 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt32(System.UInt32&)
extern void Utf8JsonReader_TryGetUInt32_mA2AA2EEC55C6FEDBD9844AD8473698D2214636BA (void);
// 0x00000275 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt32Core(System.UInt32&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetUInt32Core_m40A9CCB041D399F181EDE2B459EA7D77EBE6FB63 (void);
// 0x00000276 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt64(System.UInt64&)
extern void Utf8JsonReader_TryGetUInt64_m9728DDCC0E176DC03C0FEEB2C3FFC4566B2D72B9 (void);
// 0x00000277 System.Boolean System.Text.Json.Utf8JsonReader::TryGetUInt64Core(System.UInt64&,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonReader_TryGetUInt64Core_m73F5FB752D9F173DDA97E4679DBF61BFB82C3C71 (void);
// 0x00000278 System.Boolean System.Text.Json.Utf8JsonReader::TryGetSingle(System.Single&)
extern void Utf8JsonReader_TryGetSingle_m94587873A494E75F4825C1CA091504E3BB8FEF2D (void);
// 0x00000279 System.Boolean System.Text.Json.Utf8JsonReader::TryGetDouble(System.Double&)
extern void Utf8JsonReader_TryGetDouble_m13353F863EF92179E6798D51791CDBD278180CEB (void);
// 0x0000027A System.Boolean System.Text.Json.Utf8JsonReader::TryGetDecimal(System.Decimal&)
extern void Utf8JsonReader_TryGetDecimal_m3B8E8434E5962329BBF1F17FD4FC6F4CF186D8F2 (void);
// 0x0000027B System.Boolean System.Text.Json.Utf8JsonReader::TryGetDateTime(System.DateTime&)
extern void Utf8JsonReader_TryGetDateTime_m07EA74E20E656F735523B9F5C04C0DFA45D8AFC5 (void);
// 0x0000027C System.Boolean System.Text.Json.Utf8JsonReader::TryGetDateTimeCore(System.DateTime&)
extern void Utf8JsonReader_TryGetDateTimeCore_mD25A6F6750D17FD2E4D61338DECD3F546E1B1931 (void);
// 0x0000027D System.Boolean System.Text.Json.Utf8JsonReader::TryGetDateTimeOffset(System.DateTimeOffset&)
extern void Utf8JsonReader_TryGetDateTimeOffset_mDBA3CEC29896D95E62041F3AB3C851BBF1B7D8AD (void);
// 0x0000027E System.Boolean System.Text.Json.Utf8JsonReader::TryGetDateTimeOffsetCore(System.DateTimeOffset&)
extern void Utf8JsonReader_TryGetDateTimeOffsetCore_mD9E6F3076962699F2C5876A47B83E133E9B33533 (void);
// 0x0000027F System.Boolean System.Text.Json.Utf8JsonReader::TryGetGuid(System.Guid&)
extern void Utf8JsonReader_TryGetGuid_mA3FD580BF871065767D82031AD0A499DB0EB8BD8 (void);
// 0x00000280 System.Boolean System.Text.Json.Utf8JsonReader::TryGetGuidCore(System.Guid&)
extern void Utf8JsonReader_TryGetGuidCore_m44AE06756804CEB5DBA738D40527CA2254094530 (void);
// 0x00000281 System.Void System.Text.Json.Utf8JsonReader/PartialStateForRollback::.ctor(System.Int64,System.Int64,System.Int32,System.SequencePosition)
extern void PartialStateForRollback__ctor_m0F515AA3F80A7125D3079909A6578BD37B6EB3BB (void);
// 0x00000282 System.SequencePosition System.Text.Json.Utf8JsonReader/PartialStateForRollback::GetStartPosition(System.Int32)
extern void PartialStateForRollback_GetStartPosition_m57397FA90C6F268F5711E7E5131D5237E52F41B0 (void);
// 0x00000283 System.Void System.Text.Json.Arguments`4::.ctor()
// 0x00000284 System.Void System.Text.Json.ArgumentState::.ctor()
extern void ArgumentState__ctor_m4F54AB0DAFC7A03AFC29261512C3AD0ED1853D05 (void);
// 0x00000285 System.String System.Text.Json.JsonCamelCaseNamingPolicy::ConvertName(System.String)
extern void JsonCamelCaseNamingPolicy_ConvertName_mFF8B31FCFD0ACD691C193A2072E7D1581A6DD3E9 (void);
// 0x00000286 System.Void System.Text.Json.JsonCamelCaseNamingPolicy::FixCasing(System.Span`1<System.Char>)
extern void JsonCamelCaseNamingPolicy_FixCasing_m269D622EE92ED2E000DEE250533E8C3206687BDF (void);
// 0x00000287 System.Void System.Text.Json.JsonCamelCaseNamingPolicy::.ctor()
extern void JsonCamelCaseNamingPolicy__ctor_m8854A22CDF025E31E6CB2C027B25AEED30C16312 (void);
// 0x00000288 System.Text.Json.JsonClassInfo/ConstructorDelegate System.Text.Json.JsonClassInfo::get_CreateObject()
extern void JsonClassInfo_get_CreateObject_m3EBF112EA9432FA43BFA1E1D879507F57856E3F2 (void);
// 0x00000289 System.Void System.Text.Json.JsonClassInfo::set_CreateObject(System.Text.Json.JsonClassInfo/ConstructorDelegate)
extern void JsonClassInfo_set_CreateObject_m34917503915617101E42AD20C1D74EFDEC9F2822 (void);
// 0x0000028A System.Object System.Text.Json.JsonClassInfo::get_CreateObjectWithArgs()
extern void JsonClassInfo_get_CreateObjectWithArgs_m6912350EF7B13944758447C444D4436B39C630F1 (void);
// 0x0000028B System.Void System.Text.Json.JsonClassInfo::set_CreateObjectWithArgs(System.Object)
extern void JsonClassInfo_set_CreateObjectWithArgs_m9E5EFAC1AF224E53D2183769AF6BADF6AD44BB40 (void);
// 0x0000028C System.Object System.Text.Json.JsonClassInfo::get_AddMethodDelegate()
extern void JsonClassInfo_get_AddMethodDelegate_m7468AC11B693425EFCF34C4887F0B15FF1F3824F (void);
// 0x0000028D System.Void System.Text.Json.JsonClassInfo::set_AddMethodDelegate(System.Object)
extern void JsonClassInfo_set_AddMethodDelegate_m28A0B9710051599AAB0D4505DA3B9070A89BA838 (void);
// 0x0000028E System.Text.Json.ClassType System.Text.Json.JsonClassInfo::get_ClassType()
extern void JsonClassInfo_get_ClassType_mB58B47E20ED61254BDFAFFA920187F81F5F57189 (void);
// 0x0000028F System.Void System.Text.Json.JsonClassInfo::set_ClassType(System.Text.Json.ClassType)
extern void JsonClassInfo_set_ClassType_m6E03618BC777E3BFD9B238A6FDA54735C76E69A3 (void);
// 0x00000290 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::get_DataExtensionProperty()
extern void JsonClassInfo_get_DataExtensionProperty_mBEF729DF2A3F4EB8B567B5B4A4C8CE9E77B52C97 (void);
// 0x00000291 System.Void System.Text.Json.JsonClassInfo::set_DataExtensionProperty(System.Text.Json.JsonPropertyInfo)
extern void JsonClassInfo_set_DataExtensionProperty_mF78B121F29FE973427984392D38467BA457B9231 (void);
// 0x00000292 System.Text.Json.JsonClassInfo System.Text.Json.JsonClassInfo::get_ElementClassInfo()
extern void JsonClassInfo_get_ElementClassInfo_mC307A666007CD376510C711088774003D490696E (void);
// 0x00000293 System.Type System.Text.Json.JsonClassInfo::get_ElementType()
extern void JsonClassInfo_get_ElementType_mFAE161CCB42C2611182480DA837FA59895C97EDD (void);
// 0x00000294 System.Void System.Text.Json.JsonClassInfo::set_ElementType(System.Type)
extern void JsonClassInfo_set_ElementType_m8AD53CEED0B02CE93E0F4F45E4097E68C638F743 (void);
// 0x00000295 System.Text.Json.JsonSerializerOptions System.Text.Json.JsonClassInfo::get_Options()
extern void JsonClassInfo_get_Options_m455CFEE519679BA34B37450E658EA2892CAC46E5 (void);
// 0x00000296 System.Void System.Text.Json.JsonClassInfo::set_Options(System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_set_Options_mB86EA19543A41CA89EFDB334170B32EC0341CD00 (void);
// 0x00000297 System.Type System.Text.Json.JsonClassInfo::get_Type()
extern void JsonClassInfo_get_Type_mADC140B544ABD66F415F4CC722A44401CADDA2DF (void);
// 0x00000298 System.Void System.Text.Json.JsonClassInfo::set_Type(System.Type)
extern void JsonClassInfo_set_Type_mC2132BC8095379C552F973C5F1207017D33838FC (void);
// 0x00000299 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::get_PropertyInfoForClassInfo()
extern void JsonClassInfo_get_PropertyInfoForClassInfo_mA5E941F592678AC615E03EAB08F7C4852EF1FBDF (void);
// 0x0000029A System.Void System.Text.Json.JsonClassInfo::set_PropertyInfoForClassInfo(System.Text.Json.JsonPropertyInfo)
extern void JsonClassInfo_set_PropertyInfoForClassInfo_m0F7EB242F99630CB150E15DC5059DF0B0C8C30B8 (void);
// 0x0000029B System.Void System.Text.Json.JsonClassInfo::.ctor(System.Type,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo__ctor_mB0FFE94BDFC6E5191DE7029A75A2B42EBE3660A2 (void);
// 0x0000029C System.Void System.Text.Json.JsonClassInfo::CacheMember(System.Type,System.Type,System.Reflection.MemberInfo,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Collections.Generic.Dictionary`2<System.String,System.Text.Json.JsonPropertyInfo>,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern void JsonClassInfo_CacheMember_mDE451A3FFF1932D024C5E87216349DD71A13A9EE (void);
// 0x0000029D System.Void System.Text.Json.JsonClassInfo::InitializeConstructorParameters(System.Reflection.ConstructorInfo)
extern void JsonClassInfo_InitializeConstructorParameters_m0D4D7729F0AC4CA827E230A5E676CEBC0FE6BEAE (void);
// 0x0000029E System.Boolean System.Text.Json.JsonClassInfo::PropertyIsOverridenAndIgnored(System.Reflection.MemberInfo,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>)
extern void JsonClassInfo_PropertyIsOverridenAndIgnored_m58CA0851B95234F54FD442D696002956E66F321F (void);
// 0x0000029F System.Boolean System.Text.Json.JsonClassInfo::PropertyIsVirtual(System.Reflection.PropertyInfo)
extern void JsonClassInfo_PropertyIsVirtual_m89E8D941D30BF148B3EB940A4FF97F15584133B2 (void);
// 0x000002A0 System.Boolean System.Text.Json.JsonClassInfo::DetermineExtensionDataProperty(System.Collections.Generic.Dictionary`2<System.String,System.Text.Json.JsonPropertyInfo>)
extern void JsonClassInfo_DetermineExtensionDataProperty_mED80FAB663CA5DC121EF5E563A40D70303855E9F (void);
// 0x000002A1 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::GetPropertyWithUniqueAttribute(System.Type,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Text.Json.JsonPropertyInfo>)
extern void JsonClassInfo_GetPropertyWithUniqueAttribute_m949E9EDBEABE05FC1C6E9A14B30D40E26DA85F8D (void);
// 0x000002A2 System.Text.Json.JsonParameterInfo System.Text.Json.JsonClassInfo::AddConstructorParameter(System.Reflection.ParameterInfo,System.Text.Json.JsonPropertyInfo,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_AddConstructorParameter_m95BD7C701497F9EA77D1F62A97CDE7351BA10973 (void);
// 0x000002A3 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonClassInfo::GetConverter(System.Type,System.Type,System.Reflection.MemberInfo,System.Type&,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_GetConverter_m4D8FCA3F0612E829323DD0A04F320F5BD9240EE0 (void);
// 0x000002A4 System.Void System.Text.Json.JsonClassInfo::ValidateType(System.Type,System.Type,System.Reflection.MemberInfo,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_ValidateType_mBAB0C4E7BE4A099E1636D940E6AE1368D7AC0E67 (void);
// 0x000002A5 System.Boolean System.Text.Json.JsonClassInfo::IsInvalidForSerialization(System.Type)
extern void JsonClassInfo_IsInvalidForSerialization_m8711EFCAE07559D74A27DBE78C7AD6B52FC9F92D (void);
// 0x000002A6 System.Boolean System.Text.Json.JsonClassInfo::IsByRefLike(System.Type)
extern void JsonClassInfo_IsByRefLike_m35555FEAF733150FC3A25A17EBB58F23211E08B6 (void);
// 0x000002A7 System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling> System.Text.Json.JsonClassInfo::GetNumberHandlingForType(System.Type)
extern void JsonClassInfo_GetNumberHandlingForType_mE28062630549D5A7FB1D308533CF0B2A559379E5 (void);
// 0x000002A8 System.Int32 System.Text.Json.JsonClassInfo::get_ParameterCount()
extern void JsonClassInfo_get_ParameterCount_m40342A48D50A73E4009D410636E99363D2D78050 (void);
// 0x000002A9 System.Void System.Text.Json.JsonClassInfo::set_ParameterCount(System.Int32)
extern void JsonClassInfo_set_ParameterCount_m724623350D2C95B221A10C84C0EFDA7872952176 (void);
// 0x000002AA System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::AddProperty(System.Reflection.MemberInfo,System.Type,System.Type,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_AddProperty_m1A38048CF13A18F1170C3EAF348EBA8BBBE6DFEE (void);
// 0x000002AB System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::CreateProperty(System.Type,System.Type,System.Reflection.MemberInfo,System.Type,System.Text.Json.Serialization.JsonConverter,System.Text.Json.JsonSerializerOptions,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>)
extern void JsonClassInfo_CreateProperty_m37DF3D2728C0A9C9A4C07183C36FCA4F5A46BA7C (void);
// 0x000002AC System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::CreatePropertyInfoForClassInfo(System.Type,System.Type,System.Text.Json.Serialization.JsonConverter,System.Text.Json.JsonSerializerOptions)
extern void JsonClassInfo_CreatePropertyInfoForClassInfo_mA733C3680077D46EC8880E83E72981DA56981A64 (void);
// 0x000002AD System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo::GetProperty(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStackFrame&,System.Byte[]&)
extern void JsonClassInfo_GetProperty_m4AD766C54EA3A1D37706F74AD38173F375B76506 (void);
// 0x000002AE System.Text.Json.JsonParameterInfo System.Text.Json.JsonClassInfo::GetParameter(System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStackFrame&,System.Byte[]&)
extern void JsonClassInfo_GetParameter_mD05AE1588E681D5092CC833222DB24E00614259D (void);
// 0x000002AF System.Boolean System.Text.Json.JsonClassInfo::IsPropertyRefEqual(System.Text.Json.PropertyRef&,System.ReadOnlySpan`1<System.Byte>,System.UInt64)
extern void JsonClassInfo_IsPropertyRefEqual_mF49B94C280F0A7C83B637E102015E60FCD4C4F1C (void);
// 0x000002B0 System.Boolean System.Text.Json.JsonClassInfo::IsParameterRefEqual(System.Text.Json.ParameterRef&,System.ReadOnlySpan`1<System.Byte>,System.UInt64)
extern void JsonClassInfo_IsParameterRefEqual_mF9F55586BE7F3213556DFED8BA3786A19AAA871B (void);
// 0x000002B1 System.UInt64 System.Text.Json.JsonClassInfo::GetKey(System.ReadOnlySpan`1<System.Byte>)
extern void JsonClassInfo_GetKey_mA405F4C97CF779AC1BB3AE849B0AB5F8A2522018 (void);
// 0x000002B2 System.Void System.Text.Json.JsonClassInfo::UpdateSortedPropertyCache(System.Text.Json.ReadStackFrame&)
extern void JsonClassInfo_UpdateSortedPropertyCache_m93D69873D48BA0E8E8D6DE3AE3C6AF21BDB168AB (void);
// 0x000002B3 System.Void System.Text.Json.JsonClassInfo::UpdateSortedParameterCache(System.Text.Json.ReadStackFrame&)
extern void JsonClassInfo_UpdateSortedParameterCache_mFB7B316FD7C2009DEBE2457F2648D00E0E3FD9CE (void);
// 0x000002B4 System.Void System.Text.Json.JsonClassInfo::.cctor()
extern void JsonClassInfo__cctor_m914422BBC28D3DE010CCD1AA3E7569D5DD1FB9A4 (void);
// 0x000002B5 System.Type System.Text.Json.JsonClassInfo::<InitializeConstructorParameters>g__GetMemberType|46_0(System.Reflection.MemberInfo)
extern void JsonClassInfo_U3CInitializeConstructorParametersU3Eg__GetMemberTypeU7C46_0_m4DB5544FEDE2BFFFCDBD0C71F871A97E52BEE698 (void);
// 0x000002B6 System.Void System.Text.Json.JsonClassInfo/ConstructorDelegate::.ctor(System.Object,System.IntPtr)
extern void ConstructorDelegate__ctor_mD390978F74D17842A5B524334220F182F0FFE5A2 (void);
// 0x000002B7 System.Object System.Text.Json.JsonClassInfo/ConstructorDelegate::Invoke()
extern void ConstructorDelegate_Invoke_m64AFA88C347ABBF8E71F60DF07F0D94D853C6EE9 (void);
// 0x000002B8 System.Void System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x000002B9 T System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`1::Invoke(System.Object[])
// 0x000002BA System.Void System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`5::.ctor(System.Object,System.IntPtr)
// 0x000002BB T System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`5::Invoke(TArg0,TArg1,TArg2,TArg3)
// 0x000002BC System.Void System.Text.Json.JsonClassInfo/ParameterLookupKey::.ctor(System.String,System.Type)
extern void ParameterLookupKey__ctor_m8DA6397018FAE0536BF32BE115B712CCA3D54A01 (void);
// 0x000002BD System.String System.Text.Json.JsonClassInfo/ParameterLookupKey::get_Name()
extern void ParameterLookupKey_get_Name_mFEBA84A3AABA84F665B6225A40910FCA4035DF9B (void);
// 0x000002BE System.Type System.Text.Json.JsonClassInfo/ParameterLookupKey::get_Type()
extern void ParameterLookupKey_get_Type_mBA2A95BA0E212EBA50A6A6BA1DD07F9418C35A90 (void);
// 0x000002BF System.Int32 System.Text.Json.JsonClassInfo/ParameterLookupKey::GetHashCode()
extern void ParameterLookupKey_GetHashCode_m941A3A9109B92C2949699D93BE49AC04835C392D (void);
// 0x000002C0 System.Boolean System.Text.Json.JsonClassInfo/ParameterLookupKey::Equals(System.Object)
extern void ParameterLookupKey_Equals_m03A9EC5CACB1EF489D151E7EDFB30161EBE327DD (void);
// 0x000002C1 System.Void System.Text.Json.JsonClassInfo/ParameterLookupValue::.ctor(System.Text.Json.JsonPropertyInfo)
extern void ParameterLookupValue__ctor_mA7337C487E75BB0463EA0E263A728EBB0F8EB7D6 (void);
// 0x000002C2 System.String System.Text.Json.JsonClassInfo/ParameterLookupValue::get_DuplicateName()
extern void ParameterLookupValue_get_DuplicateName_mEF39EDF507355719BE0C657CDAD196E4D862A9C6 (void);
// 0x000002C3 System.Void System.Text.Json.JsonClassInfo/ParameterLookupValue::set_DuplicateName(System.String)
extern void ParameterLookupValue_set_DuplicateName_m2E5925A1C4EB54A46A0D8027EC79094CE910A5B1 (void);
// 0x000002C4 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonClassInfo/ParameterLookupValue::get_JsonPropertyInfo()
extern void ParameterLookupValue_get_JsonPropertyInfo_m3B8BDAF0D81391028FEE43EBCFF523404F2600AB (void);
// 0x000002C5 System.String System.Text.Json.JsonDefaultNamingPolicy::ConvertName(System.String)
extern void JsonDefaultNamingPolicy_ConvertName_m42E74A92E3FE81040CD72A02792EBCA736527B3E (void);
// 0x000002C6 System.Void System.Text.Json.JsonDefaultNamingPolicy::.ctor()
extern void JsonDefaultNamingPolicy__ctor_m4435B81DC2DA68A9B1191F818BF265C550D8D819 (void);
// 0x000002C7 System.Void System.Text.Json.JsonNamingPolicy::.ctor()
extern void JsonNamingPolicy__ctor_mDA3AFFECBC22A7C0FB171A00AFC1E1F00BC79204 (void);
// 0x000002C8 System.String System.Text.Json.JsonNamingPolicy::ConvertName(System.String)
// 0x000002C9 System.Void System.Text.Json.JsonNamingPolicy::.cctor()
extern void JsonNamingPolicy__cctor_mC6F9988BFAEB4FC160002FE854DAF4C9EFD12FF6 (void);
// 0x000002CA System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonParameterInfo::get_ConverterBase()
extern void JsonParameterInfo_get_ConverterBase_mCC08551310814992064D684373C6615BFA1257D5 (void);
// 0x000002CB System.Void System.Text.Json.JsonParameterInfo::set_ConverterBase(System.Text.Json.Serialization.JsonConverter)
extern void JsonParameterInfo_set_ConverterBase_m93143D7C5A636B19EB4E2F6DCA2164C1B6221EDC (void);
// 0x000002CC System.Object System.Text.Json.JsonParameterInfo::get_DefaultValue()
extern void JsonParameterInfo_get_DefaultValue_mD3A36A05BE23345A76389D6D77930755D17E880A (void);
// 0x000002CD System.Void System.Text.Json.JsonParameterInfo::set_DefaultValue(System.Object)
extern void JsonParameterInfo_set_DefaultValue_mD9EC1B414F7C6B4635348B54D081285F8A2611A5 (void);
// 0x000002CE System.Boolean System.Text.Json.JsonParameterInfo::get_IgnoreDefaultValuesOnRead()
extern void JsonParameterInfo_get_IgnoreDefaultValuesOnRead_mA4ECA944598FE5BADC46249FDA9E3189FB546E0B (void);
// 0x000002CF System.Void System.Text.Json.JsonParameterInfo::set_IgnoreDefaultValuesOnRead(System.Boolean)
extern void JsonParameterInfo_set_IgnoreDefaultValuesOnRead_m2779C8F1629C24F418F5D6FEC30FEAD15362DD71 (void);
// 0x000002D0 System.Text.Json.JsonSerializerOptions System.Text.Json.JsonParameterInfo::get_Options()
extern void JsonParameterInfo_get_Options_m7F95CBCFF8FE693469ABC96F51164729A55A8EEF (void);
// 0x000002D1 System.Void System.Text.Json.JsonParameterInfo::set_Options(System.Text.Json.JsonSerializerOptions)
extern void JsonParameterInfo_set_Options_mBF96669910F25F29C17297FDC0F4BB0FE35099F1 (void);
// 0x000002D2 System.Byte[] System.Text.Json.JsonParameterInfo::get_NameAsUtf8Bytes()
extern void JsonParameterInfo_get_NameAsUtf8Bytes_m688D4F37A8BAA98F54FB8AB50EA226074556D91D (void);
// 0x000002D3 System.Void System.Text.Json.JsonParameterInfo::set_NameAsUtf8Bytes(System.Byte[])
extern void JsonParameterInfo_set_NameAsUtf8Bytes_m9A4C3C3E99D222691ECC2D75D22D196B6D966B55 (void);
// 0x000002D4 System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling> System.Text.Json.JsonParameterInfo::get_NumberHandling()
extern void JsonParameterInfo_get_NumberHandling_mC0C94DACE6C1990B7D6B232A27730FA513F6A5CF (void);
// 0x000002D5 System.Void System.Text.Json.JsonParameterInfo::set_NumberHandling(System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>)
extern void JsonParameterInfo_set_NumberHandling_mADA24929F092015A0F64BC2EDAEF302618BB20E0 (void);
// 0x000002D6 System.Int32 System.Text.Json.JsonParameterInfo::get_Position()
extern void JsonParameterInfo_get_Position_m568DD91DFCBC41C1AB61BE19D4AE03B11ECACF42 (void);
// 0x000002D7 System.Void System.Text.Json.JsonParameterInfo::set_Position(System.Int32)
extern void JsonParameterInfo_set_Position_m6E579FF1797800B3BAED9D35A646E9E2B32D41D3 (void);
// 0x000002D8 System.Text.Json.JsonClassInfo System.Text.Json.JsonParameterInfo::get_RuntimeClassInfo()
extern void JsonParameterInfo_get_RuntimeClassInfo_m9385118A9259F52F310B62874E90EF7BD0D83759 (void);
// 0x000002D9 System.Type System.Text.Json.JsonParameterInfo::get_RuntimePropertyType()
extern void JsonParameterInfo_get_RuntimePropertyType_mDC2CE8AA4F39F0447C0DCAA8B8050A93968465BC (void);
// 0x000002DA System.Void System.Text.Json.JsonParameterInfo::set_RuntimePropertyType(System.Type)
extern void JsonParameterInfo_set_RuntimePropertyType_mECB4C8ADF8951CE7563A0860825196EAC3C7E3BE (void);
// 0x000002DB System.Boolean System.Text.Json.JsonParameterInfo::get_ShouldDeserialize()
extern void JsonParameterInfo_get_ShouldDeserialize_m0805077B1A252FA8DA64409F2F077354D105D3CA (void);
// 0x000002DC System.Void System.Text.Json.JsonParameterInfo::set_ShouldDeserialize(System.Boolean)
extern void JsonParameterInfo_set_ShouldDeserialize_m3EFD6DB8AA2A9DA33B98BAD73394E7BFCAA01265 (void);
// 0x000002DD System.Void System.Text.Json.JsonParameterInfo::Initialize(System.Type,System.Reflection.ParameterInfo,System.Text.Json.JsonPropertyInfo,System.Text.Json.JsonSerializerOptions)
extern void JsonParameterInfo_Initialize_m8152E501631F2148C3E048D0259B6B0865FFBCD1 (void);
// 0x000002DE System.Text.Json.JsonParameterInfo System.Text.Json.JsonParameterInfo::CreateIgnoredParameterPlaceholder(System.Text.Json.JsonPropertyInfo)
extern void JsonParameterInfo_CreateIgnoredParameterPlaceholder_m7B3C05C55E8A8ADE239FC2659778096B52A559C2 (void);
// 0x000002DF System.Void System.Text.Json.JsonParameterInfo::.ctor()
extern void JsonParameterInfo__ctor_mEA4711A4328921BD302B5EF10375222C7B5308B2 (void);
// 0x000002E0 T System.Text.Json.JsonParameterInfo`1::get_TypedDefaultValue()
// 0x000002E1 System.Void System.Text.Json.JsonParameterInfo`1::set_TypedDefaultValue(T)
// 0x000002E2 System.Void System.Text.Json.JsonParameterInfo`1::Initialize(System.Type,System.Reflection.ParameterInfo,System.Text.Json.JsonPropertyInfo,System.Text.Json.JsonSerializerOptions)
// 0x000002E3 System.Void System.Text.Json.JsonParameterInfo`1::.ctor()
// 0x000002E4 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonPropertyInfo::get_ConverterBase()
// 0x000002E5 System.Void System.Text.Json.JsonPropertyInfo::set_ConverterBase(System.Text.Json.Serialization.JsonConverter)
// 0x000002E6 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonPropertyInfo::GetPropertyPlaceholder()
extern void JsonPropertyInfo_GetPropertyPlaceholder_m8DDE8771A9D400F54491B0E9131BC9864FA1A400 (void);
// 0x000002E7 System.Text.Json.JsonPropertyInfo System.Text.Json.JsonPropertyInfo::CreateIgnoredPropertyPlaceholder(System.Reflection.MemberInfo,System.Text.Json.JsonSerializerOptions)
extern void JsonPropertyInfo_CreateIgnoredPropertyPlaceholder_m48161A419A057A5A255DC902A0864AD45798DD77 (void);
// 0x000002E8 System.Type System.Text.Json.JsonPropertyInfo::get_DeclaredPropertyType()
extern void JsonPropertyInfo_get_DeclaredPropertyType_mA7E5DAEF07385CD52EA765681C042819778D4E23 (void);
// 0x000002E9 System.Void System.Text.Json.JsonPropertyInfo::set_DeclaredPropertyType(System.Type)
extern void JsonPropertyInfo_set_DeclaredPropertyType_m35C010F02946728DE042F6D6343DECD703D780D5 (void);
// 0x000002EA System.Void System.Text.Json.JsonPropertyInfo::GetPolicies(System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Boolean)
extern void JsonPropertyInfo_GetPolicies_m348B854F6CD69667E92E1A4FAAE41DCE26F7BDAD (void);
// 0x000002EB System.Void System.Text.Json.JsonPropertyInfo::DeterminePropertyName()
extern void JsonPropertyInfo_DeterminePropertyName_m7DB7DFC642DD42E791262DF3837035C6ECE925C0 (void);
// 0x000002EC System.Void System.Text.Json.JsonPropertyInfo::DetermineSerializationCapabilities(System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>)
extern void JsonPropertyInfo_DetermineSerializationCapabilities_mBA5E636AEEBBFD9B64B909E6C3E0260EA4545E8A (void);
// 0x000002ED System.Void System.Text.Json.JsonPropertyInfo::DetermineIgnoreCondition(System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>,System.Boolean)
extern void JsonPropertyInfo_DetermineIgnoreCondition_m1A9DA7C8F29E66CF3D7D75DCEA496CE09EECA422 (void);
// 0x000002EE System.Void System.Text.Json.JsonPropertyInfo::DetermineNumberHandling(System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>)
extern void JsonPropertyInfo_DetermineNumberHandling_mC32578B27199802D8790E888F303E2E1ABEE7DF8 (void);
// 0x000002EF TAttribute System.Text.Json.JsonPropertyInfo::GetAttribute(System.Reflection.MemberInfo)
// 0x000002F0 System.Boolean System.Text.Json.JsonPropertyInfo::GetMemberAndWriteJson(System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
// 0x000002F1 System.Boolean System.Text.Json.JsonPropertyInfo::GetMemberAndWriteJsonExtensionData(System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
// 0x000002F2 System.Object System.Text.Json.JsonPropertyInfo::GetValueAsObject(System.Object)
// 0x000002F3 System.Boolean System.Text.Json.JsonPropertyInfo::get_HasGetter()
extern void JsonPropertyInfo_get_HasGetter_m025A2B5681CD7CCEBBC7E3F57C04C6A339C980A7 (void);
// 0x000002F4 System.Void System.Text.Json.JsonPropertyInfo::set_HasGetter(System.Boolean)
extern void JsonPropertyInfo_set_HasGetter_m3A356164FCCD506528DCFB55D33E603FBDC10F29 (void);
// 0x000002F5 System.Boolean System.Text.Json.JsonPropertyInfo::get_HasSetter()
extern void JsonPropertyInfo_get_HasSetter_m5EEEE5CAD0764FDEA90B717276823A4E9460E13B (void);
// 0x000002F6 System.Void System.Text.Json.JsonPropertyInfo::set_HasSetter(System.Boolean)
extern void JsonPropertyInfo_set_HasSetter_mFEA57CBF1D0DC3C17C890C4199B9741C159D681E (void);
// 0x000002F7 System.Void System.Text.Json.JsonPropertyInfo::Initialize(System.Type,System.Type,System.Type,System.Text.Json.ClassType,System.Reflection.MemberInfo,System.Text.Json.Serialization.JsonConverter,System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Text.Json.JsonSerializerOptions)
extern void JsonPropertyInfo_Initialize_m452B07B8CBC1C066ED5A69933A6F731CB75379B4 (void);
// 0x000002F8 System.Boolean System.Text.Json.JsonPropertyInfo::get_IgnoreDefaultValuesOnRead()
extern void JsonPropertyInfo_get_IgnoreDefaultValuesOnRead_m728486838F41D007DA5C66D1E32132B5A977AC36 (void);
// 0x000002F9 System.Void System.Text.Json.JsonPropertyInfo::set_IgnoreDefaultValuesOnRead(System.Boolean)
extern void JsonPropertyInfo_set_IgnoreDefaultValuesOnRead_m1CB085CFB0CF60A74A7917881F912C0274110E3F (void);
// 0x000002FA System.Boolean System.Text.Json.JsonPropertyInfo::get_IgnoreDefaultValuesOnWrite()
extern void JsonPropertyInfo_get_IgnoreDefaultValuesOnWrite_m6014C570FD13D67F0DD65DF4C5DECB59459C02B1 (void);
// 0x000002FB System.Void System.Text.Json.JsonPropertyInfo::set_IgnoreDefaultValuesOnWrite(System.Boolean)
extern void JsonPropertyInfo_set_IgnoreDefaultValuesOnWrite_m37324953D9E5DD8939747E2B250562699A0C1AB0 (void);
// 0x000002FC System.Boolean System.Text.Json.JsonPropertyInfo::get_IsForClassInfo()
extern void JsonPropertyInfo_get_IsForClassInfo_mB3FBB996FFE12A31E528E5DCE807D9F689ED953B (void);
// 0x000002FD System.Void System.Text.Json.JsonPropertyInfo::set_IsForClassInfo(System.Boolean)
extern void JsonPropertyInfo_set_IsForClassInfo_mAF881BB94BFB8B2C9ACFA7F5678D748E4766E78D (void);
// 0x000002FE System.String System.Text.Json.JsonPropertyInfo::get_NameAsString()
extern void JsonPropertyInfo_get_NameAsString_m11191ABD723F638610ABCF00711196121E7E5B33 (void);
// 0x000002FF System.Void System.Text.Json.JsonPropertyInfo::set_NameAsString(System.String)
extern void JsonPropertyInfo_set_NameAsString_m60C685E0FDA27B1FA13BADB4C5EDC1AD21A75556 (void);
// 0x00000300 System.Text.Json.JsonSerializerOptions System.Text.Json.JsonPropertyInfo::get_Options()
extern void JsonPropertyInfo_get_Options_m6A4F9F50FB8D18A8D87E918C5257DF56CC171D92 (void);
// 0x00000301 System.Void System.Text.Json.JsonPropertyInfo::set_Options(System.Text.Json.JsonSerializerOptions)
extern void JsonPropertyInfo_set_Options_mEEF7A440A2F290AE89E5F74577EF042ECB193BAA (void);
// 0x00000302 System.Boolean System.Text.Json.JsonPropertyInfo::ReadJsonAndAddExtensionProperty(System.Object,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&)
extern void JsonPropertyInfo_ReadJsonAndAddExtensionProperty_mBAF58461BED720ED2D5E5CDAB4A1B6242B2DF3B9 (void);
// 0x00000303 System.Boolean System.Text.Json.JsonPropertyInfo::ReadJsonAndSetMember(System.Object,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&)
// 0x00000304 System.Boolean System.Text.Json.JsonPropertyInfo::ReadJsonAsObject(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Object&)
// 0x00000305 System.Boolean System.Text.Json.JsonPropertyInfo::ReadJsonExtensionDataValue(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Object&)
extern void JsonPropertyInfo_ReadJsonExtensionDataValue_m98174C8D99A68C441F14DDFDB3845DD95DB77F6D (void);
// 0x00000306 System.Type System.Text.Json.JsonPropertyInfo::get_ParentClassType()
extern void JsonPropertyInfo_get_ParentClassType_m81E11A124BF5BF4161D1E85664A29CC826EE5654 (void);
// 0x00000307 System.Void System.Text.Json.JsonPropertyInfo::set_ParentClassType(System.Type)
extern void JsonPropertyInfo_set_ParentClassType_m53899BF0AF2631BB72C321DD9E9A8E35332782C9 (void);
// 0x00000308 System.Reflection.MemberInfo System.Text.Json.JsonPropertyInfo::get_MemberInfo()
extern void JsonPropertyInfo_get_MemberInfo_mD28BFADF987F4D6B3040E01220E7ECEB2B892842 (void);
// 0x00000309 System.Void System.Text.Json.JsonPropertyInfo::set_MemberInfo(System.Reflection.MemberInfo)
extern void JsonPropertyInfo_set_MemberInfo_mCEBE51C89CE4B8A2AA463D598DD4C29A821A3A86 (void);
// 0x0000030A System.Text.Json.JsonClassInfo System.Text.Json.JsonPropertyInfo::get_RuntimeClassInfo()
extern void JsonPropertyInfo_get_RuntimeClassInfo_mF77D80FC7CCE7E7E8D4F56E4CB2A629713978EB3 (void);
// 0x0000030B System.Type System.Text.Json.JsonPropertyInfo::get_RuntimePropertyType()
extern void JsonPropertyInfo_get_RuntimePropertyType_mE9E72D4AB3EE30C9C20D661CE1127645DB143466 (void);
// 0x0000030C System.Void System.Text.Json.JsonPropertyInfo::set_RuntimePropertyType(System.Type)
extern void JsonPropertyInfo_set_RuntimePropertyType_mCBB9ECD02C7CC07549F4B7D2706140D769A6A348 (void);
// 0x0000030D System.Void System.Text.Json.JsonPropertyInfo::SetExtensionDictionaryAsObject(System.Object,System.Object)
// 0x0000030E System.Boolean System.Text.Json.JsonPropertyInfo::get_ShouldSerialize()
extern void JsonPropertyInfo_get_ShouldSerialize_mC0489A6FED6EC669A944D044A73D205F14FA21E2 (void);
// 0x0000030F System.Void System.Text.Json.JsonPropertyInfo::set_ShouldSerialize(System.Boolean)
extern void JsonPropertyInfo_set_ShouldSerialize_m10AB20E671B7E2C15BED04BCCC99E3FB79872E93 (void);
// 0x00000310 System.Boolean System.Text.Json.JsonPropertyInfo::get_ShouldDeserialize()
extern void JsonPropertyInfo_get_ShouldDeserialize_mC0F6FB0AC12102EADC4C22A84B009D7FA903108D (void);
// 0x00000311 System.Void System.Text.Json.JsonPropertyInfo::set_ShouldDeserialize(System.Boolean)
extern void JsonPropertyInfo_set_ShouldDeserialize_m9013BB660E0FA69F093B58C6EF325A88B07CA99F (void);
// 0x00000312 System.Boolean System.Text.Json.JsonPropertyInfo::get_IsIgnored()
extern void JsonPropertyInfo_get_IsIgnored_m7570E12D5A959200E91D1F1BFA6BFF9CD42FE78F (void);
// 0x00000313 System.Void System.Text.Json.JsonPropertyInfo::set_IsIgnored(System.Boolean)
extern void JsonPropertyInfo_set_IsIgnored_mBA15B57ADD5B0972DC91A6DF5F18D2E70A4073E7 (void);
// 0x00000314 System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling> System.Text.Json.JsonPropertyInfo::get_NumberHandling()
extern void JsonPropertyInfo_get_NumberHandling_m97868DFAB3D0971C720873E936E4ED5FE6222166 (void);
// 0x00000315 System.Void System.Text.Json.JsonPropertyInfo::set_NumberHandling(System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>)
extern void JsonPropertyInfo_set_NumberHandling_m183C90FA1664FD8250BDD4211AC8229640E290F2 (void);
// 0x00000316 System.Void System.Text.Json.JsonPropertyInfo::.ctor()
extern void JsonPropertyInfo__ctor_mF8AE5468031A60DA2D460931063E2C81702F0D3E (void);
// 0x00000317 System.Void System.Text.Json.JsonPropertyInfo::.cctor()
extern void JsonPropertyInfo__cctor_mC06E09EE5C8788EACD0A71D78A3729C647E04EE7 (void);
// 0x00000318 System.Func`2<System.Object,T> System.Text.Json.JsonPropertyInfo`1::get_Get()
// 0x00000319 System.Void System.Text.Json.JsonPropertyInfo`1::set_Get(System.Func`2<System.Object,T>)
// 0x0000031A System.Action`2<System.Object,T> System.Text.Json.JsonPropertyInfo`1::get_Set()
// 0x0000031B System.Void System.Text.Json.JsonPropertyInfo`1::set_Set(System.Action`2<System.Object,T>)
// 0x0000031C System.Text.Json.Serialization.JsonConverter`1<T> System.Text.Json.JsonPropertyInfo`1::get_Converter()
// 0x0000031D System.Void System.Text.Json.JsonPropertyInfo`1::set_Converter(System.Text.Json.Serialization.JsonConverter`1<T>)
// 0x0000031E System.Void System.Text.Json.JsonPropertyInfo`1::Initialize(System.Type,System.Type,System.Type,System.Text.Json.ClassType,System.Reflection.MemberInfo,System.Text.Json.Serialization.JsonConverter,System.Nullable`1<System.Text.Json.Serialization.JsonIgnoreCondition>,System.Nullable`1<System.Text.Json.Serialization.JsonNumberHandling>,System.Text.Json.JsonSerializerOptions)
// 0x0000031F System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonPropertyInfo`1::get_ConverterBase()
// 0x00000320 System.Void System.Text.Json.JsonPropertyInfo`1::set_ConverterBase(System.Text.Json.Serialization.JsonConverter)
// 0x00000321 System.Object System.Text.Json.JsonPropertyInfo`1::GetValueAsObject(System.Object)
// 0x00000322 System.Boolean System.Text.Json.JsonPropertyInfo`1::GetMemberAndWriteJson(System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
// 0x00000323 System.Boolean System.Text.Json.JsonPropertyInfo`1::GetMemberAndWriteJsonExtensionData(System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
// 0x00000324 System.Boolean System.Text.Json.JsonPropertyInfo`1::ReadJsonAndSetMember(System.Object,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&)
// 0x00000325 System.Boolean System.Text.Json.JsonPropertyInfo`1::ReadJsonAsObject(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Object&)
// 0x00000326 System.Void System.Text.Json.JsonPropertyInfo`1::SetExtensionDictionaryAsObject(System.Object,System.Object)
// 0x00000327 System.Void System.Text.Json.JsonPropertyInfo`1::.ctor()
// 0x00000328 System.Boolean System.Text.Json.JsonSerializer::ResolveMetadataForJsonObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000329 System.Boolean System.Text.Json.JsonSerializer::ResolveMetadataForJsonArray(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000032A System.Boolean System.Text.Json.JsonSerializer::TryReadAheadMetadataAndSetState(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.StackFrameObjectState)
extern void JsonSerializer_TryReadAheadMetadataAndSetState_m6B18228F63C23C3EBEAD05B281F7F210B3EAA364 (void);
// 0x0000032B System.Text.Json.MetadataPropertyName System.Text.Json.JsonSerializer::GetMetadataPropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void JsonSerializer_GetMetadataPropertyName_m04EA82F5813FCC311E79BF04990FF7C6E0C9B8EB (void);
// 0x0000032C System.Boolean System.Text.Json.JsonSerializer::TryGetReferenceFromJsonElement(System.Text.Json.ReadStack&,System.Text.Json.JsonElement,System.Object&)
extern void JsonSerializer_TryGetReferenceFromJsonElement_mB6DD6DDE9BD124B2FF3753C01945404B82E5A975 (void);
// 0x0000032D System.Void System.Text.Json.JsonSerializer::ValidateValueIsCorrectType(System.Object,System.String)
// 0x0000032E System.Text.Json.JsonPropertyInfo System.Text.Json.JsonSerializer::LookupProperty(System.Object,System.ReadOnlySpan`1<System.Byte>,System.Text.Json.ReadStack&,System.Boolean&,System.Boolean)
extern void JsonSerializer_LookupProperty_mD8AE09F693994C7A922EE7812796D6A07A231719 (void);
// 0x0000032F System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonSerializer::GetPropertyName(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions)
extern void JsonSerializer_GetPropertyName_mC1C0556CD85CCF682207683A331349038C9B9FDB (void);
// 0x00000330 System.Void System.Text.Json.JsonSerializer::CreateDataExtensionProperty(System.Object,System.Text.Json.JsonPropertyInfo)
extern void JsonSerializer_CreateDataExtensionProperty_m7E6DDAC1AEBBAEB9A4ED744C11CED6B446EF9A96 (void);
// 0x00000331 TValue System.Text.Json.JsonSerializer::ReadCore(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x00000332 TValue System.Text.Json.JsonSerializer::ReadCore(System.Text.Json.Serialization.JsonConverter,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x00000333 TValue System.Text.Json.JsonSerializer::Deserialize(System.String,System.Text.Json.JsonSerializerOptions)
// 0x00000334 TValue System.Text.Json.JsonSerializer::Deserialize(System.String,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x00000335 System.Text.Json.MetadataPropertyName System.Text.Json.JsonSerializer::WriteReferenceForObject(System.Text.Json.Serialization.JsonConverter,System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
extern void JsonSerializer_WriteReferenceForObject_m50C78FADF91B563E516BDB5E206D98F1DF8B94E8 (void);
// 0x00000336 System.Text.Json.MetadataPropertyName System.Text.Json.JsonSerializer::WriteReferenceForCollection(System.Text.Json.Serialization.JsonConverter,System.Object,System.Text.Json.WriteStack&,System.Text.Json.Utf8JsonWriter)
extern void JsonSerializer_WriteReferenceForCollection_m114018149E92E3C03D39390A1401D78CBAEB9BB6 (void);
// 0x00000337 System.Void System.Text.Json.JsonSerializer::WriteCore(System.Text.Json.Utf8JsonWriter,TValue&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x00000338 System.Boolean System.Text.Json.JsonSerializer::WriteCore(System.Text.Json.Serialization.JsonConverter,System.Text.Json.Utf8JsonWriter,TValue&,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000339 System.String System.Text.Json.JsonSerializer::Serialize(TValue,System.Text.Json.JsonSerializerOptions)
// 0x0000033A System.String System.Text.Json.JsonSerializer::Serialize(TValue&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x0000033B System.Void System.Text.Json.JsonSerializer::.cctor()
extern void JsonSerializer__cctor_m4E7DDB69F78AC9C224F51B9B5D45E8F9C92C1595 (void);
// 0x0000033C System.Collections.Generic.Dictionary`2<System.Type,System.Text.Json.Serialization.JsonConverter> System.Text.Json.JsonSerializerOptions::GetDefaultSimpleConverters()
extern void JsonSerializerOptions_GetDefaultSimpleConverters_m2EE0299A75B9DB1DD205CEDFF2CE2A30C763E1FE (void);
// 0x0000033D System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::GetDictionaryKeyConverter(System.Type)
extern void JsonSerializerOptions_GetDictionaryKeyConverter_mCF912E4307003471C53EEBD39A9207E44BF51221 (void);
// 0x0000033E System.Collections.Concurrent.ConcurrentDictionary`2<System.Type,System.Text.Json.Serialization.JsonConverter> System.Text.Json.JsonSerializerOptions::GetDictionaryKeyConverters()
extern void JsonSerializerOptions_GetDictionaryKeyConverters_mA2A9D36A4ECB24BE030C9B48CA38EB25E4900481 (void);
// 0x0000033F System.Collections.Generic.IList`1<System.Text.Json.Serialization.JsonConverter> System.Text.Json.JsonSerializerOptions::get_Converters()
extern void JsonSerializerOptions_get_Converters_m8E101768CD4E9529474EB92573A0E68E8FBA92A2 (void);
// 0x00000340 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::DetermineConverter(System.Type,System.Type,System.Reflection.MemberInfo)
extern void JsonSerializerOptions_DetermineConverter_mE313161067E843257F33620EE88955B63893C524 (void);
// 0x00000341 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::GetConverter(System.Type)
extern void JsonSerializerOptions_GetConverter_mA1E8ADC867E97387A72326D4FD6AE68D7FB1C1BE (void);
// 0x00000342 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::GetConverterFromAttribute(System.Text.Json.Serialization.JsonConverterAttribute,System.Type,System.Type,System.Reflection.MemberInfo)
extern void JsonSerializerOptions_GetConverterFromAttribute_mF069BED8CED47DE0931D861C2A09BF014AE47659 (void);
// 0x00000343 System.Attribute System.Text.Json.JsonSerializerOptions::GetAttributeThatCanHaveMultiple(System.Type,System.Type,System.Reflection.MemberInfo)
extern void JsonSerializerOptions_GetAttributeThatCanHaveMultiple_m5874EBF0FA09BF4B0BC8769905E8EEA11F27C290 (void);
// 0x00000344 System.Attribute System.Text.Json.JsonSerializerOptions::GetAttributeThatCanHaveMultiple(System.Type,System.Type)
extern void JsonSerializerOptions_GetAttributeThatCanHaveMultiple_m2A03FC9FB47A97A81C90254D67EC3176F65D3737 (void);
// 0x00000345 System.Attribute System.Text.Json.JsonSerializerOptions::GetAttributeThatCanHaveMultiple(System.Type,System.Type,System.Reflection.MemberInfo,System.Object[])
extern void JsonSerializerOptions_GetAttributeThatCanHaveMultiple_m81E89DACD7113BBF7C75498C8445E920F9BB00F9 (void);
// 0x00000346 System.Text.Json.JsonClassInfo System.Text.Json.JsonSerializerOptions::get__lastClass()
extern void JsonSerializerOptions_get__lastClass_mCDEFA43D861813355210E3618634C7AB6989CEAE (void);
// 0x00000347 System.Void System.Text.Json.JsonSerializerOptions::set__lastClass(System.Text.Json.JsonClassInfo)
extern void JsonSerializerOptions_set__lastClass_m075FD70E92ECCA58B0F36BFFC7383F87140BE846 (void);
// 0x00000348 System.Void System.Text.Json.JsonSerializerOptions::.ctor()
extern void JsonSerializerOptions__ctor_m9431C3AEE8FE5AF4D2ACAE80B23CA0CC18CC4281 (void);
// 0x00000349 System.Boolean System.Text.Json.JsonSerializerOptions::get_AllowTrailingCommas()
extern void JsonSerializerOptions_get_AllowTrailingCommas_m01F28C687E038924AB6160A6876EC268C61A5A08 (void);
// 0x0000034A System.Int32 System.Text.Json.JsonSerializerOptions::get_DefaultBufferSize()
extern void JsonSerializerOptions_get_DefaultBufferSize_mC38EE8C3CF396E251E76982164CCA47EABE0B24E (void);
// 0x0000034B System.Text.Encodings.Web.JavaScriptEncoder System.Text.Json.JsonSerializerOptions::get_Encoder()
extern void JsonSerializerOptions_get_Encoder_m259F8A917736DFDA61A9C95D33975208707B20C4 (void);
// 0x0000034C System.Text.Json.JsonNamingPolicy System.Text.Json.JsonSerializerOptions::get_DictionaryKeyPolicy()
extern void JsonSerializerOptions_get_DictionaryKeyPolicy_m33DEE0C33A09A92D55C9F111E9988410DBDCFE52 (void);
// 0x0000034D System.Boolean System.Text.Json.JsonSerializerOptions::get_IgnoreNullValues()
extern void JsonSerializerOptions_get_IgnoreNullValues_m6256070F160F5CBF7BB77B3A7F6033ADD1B2B750 (void);
// 0x0000034E System.Text.Json.Serialization.JsonIgnoreCondition System.Text.Json.JsonSerializerOptions::get_DefaultIgnoreCondition()
extern void JsonSerializerOptions_get_DefaultIgnoreCondition_mDC7106616086AFEAB7A75DD18AFDB9FF8ECC0F7F (void);
// 0x0000034F System.Text.Json.Serialization.JsonNumberHandling System.Text.Json.JsonSerializerOptions::get_NumberHandling()
extern void JsonSerializerOptions_get_NumberHandling_mDF95C8EE4D68BE61C604B68AB1F744960426C3BE (void);
// 0x00000350 System.Boolean System.Text.Json.JsonSerializerOptions::get_IgnoreReadOnlyProperties()
extern void JsonSerializerOptions_get_IgnoreReadOnlyProperties_mF9CE0A5C472896550773A8C88B2504A2DA21D422 (void);
// 0x00000351 System.Boolean System.Text.Json.JsonSerializerOptions::get_IgnoreReadOnlyFields()
extern void JsonSerializerOptions_get_IgnoreReadOnlyFields_m099F0E064483BA6C3D5097F1760E0F9FFF2FF6EB (void);
// 0x00000352 System.Boolean System.Text.Json.JsonSerializerOptions::get_IncludeFields()
extern void JsonSerializerOptions_get_IncludeFields_m9035956C431B293552FB195C6B740C3BF29DA6FF (void);
// 0x00000353 System.Int32 System.Text.Json.JsonSerializerOptions::get_MaxDepth()
extern void JsonSerializerOptions_get_MaxDepth_m406F3FAD7524C2F3EC3D0EF53BCD2E151FAAFBE1 (void);
// 0x00000354 System.Int32 System.Text.Json.JsonSerializerOptions::get_EffectiveMaxDepth()
extern void JsonSerializerOptions_get_EffectiveMaxDepth_mCC1A5D0D20B923ED0F846BEB13020E8119AC1DDA (void);
// 0x00000355 System.Text.Json.JsonNamingPolicy System.Text.Json.JsonSerializerOptions::get_PropertyNamingPolicy()
extern void JsonSerializerOptions_get_PropertyNamingPolicy_m799A8243B78ED65A708A57E838B88C902D5403FF (void);
// 0x00000356 System.Boolean System.Text.Json.JsonSerializerOptions::get_PropertyNameCaseInsensitive()
extern void JsonSerializerOptions_get_PropertyNameCaseInsensitive_m42B624BEFB6E510442F5486AF45A3F7E0B3C421B (void);
// 0x00000357 System.Text.Json.JsonCommentHandling System.Text.Json.JsonSerializerOptions::get_ReadCommentHandling()
extern void JsonSerializerOptions_get_ReadCommentHandling_m426DAA2198333E198D0C0BC03F82C3D60E6B8C11 (void);
// 0x00000358 System.Boolean System.Text.Json.JsonSerializerOptions::get_WriteIndented()
extern void JsonSerializerOptions_get_WriteIndented_m27E15D67417240BEAE4EBF64C2AF4E81098C99F8 (void);
// 0x00000359 System.Text.Json.Serialization.ReferenceHandler System.Text.Json.JsonSerializerOptions::get_ReferenceHandler()
extern void JsonSerializerOptions_get_ReferenceHandler_m103B32EEF3E853E416576B461142BC497342ECED (void);
// 0x0000035A System.Text.Json.MemberAccessor System.Text.Json.JsonSerializerOptions::get_MemberAccessorStrategy()
extern void JsonSerializerOptions_get_MemberAccessorStrategy_m4A5B819731E3EBA23BDB9DBBD7606148A4620B88 (void);
// 0x0000035B System.Text.Json.JsonClassInfo System.Text.Json.JsonSerializerOptions::GetOrAddClass(System.Type)
extern void JsonSerializerOptions_GetOrAddClass_m0843F531DE2D905BB015DBAE8C4FEF0C5E00D574 (void);
// 0x0000035C System.Text.Json.JsonClassInfo System.Text.Json.JsonSerializerOptions::GetOrAddClassForRootType(System.Type)
extern void JsonSerializerOptions_GetOrAddClassForRootType_m5FEDB77AB83AE1637D8DC6FD9295281D122CE4DE (void);
// 0x0000035D System.Boolean System.Text.Json.JsonSerializerOptions::TypeIsCached(System.Type)
extern void JsonSerializerOptions_TypeIsCached_m7494E4E476E801188093642E5E13763963D0B16F (void);
// 0x0000035E System.Text.Json.JsonReaderOptions System.Text.Json.JsonSerializerOptions::GetReaderOptions()
extern void JsonSerializerOptions_GetReaderOptions_m3DFC2A2CF3AB3DB94DE7CBEDBF717CEC878966DA (void);
// 0x0000035F System.Text.Json.JsonWriterOptions System.Text.Json.JsonSerializerOptions::GetWriterOptions()
extern void JsonSerializerOptions_GetWriterOptions_m2831442D630359B6FFF6FB69CD71A31ABF4F5033 (void);
// 0x00000360 System.Void System.Text.Json.JsonSerializerOptions::VerifyMutable()
extern void JsonSerializerOptions_VerifyMutable_m131F3518E80AE52B84A26FB14EA95FB154CCA324 (void);
// 0x00000361 System.Void System.Text.Json.JsonSerializerOptions::.cctor()
extern void JsonSerializerOptions__cctor_m2EF8DD15009E63DFB2440CF25EEB487C987FDBB1 (void);
// 0x00000362 System.Void System.Text.Json.JsonSerializerOptions::<GetDefaultSimpleConverters>g__Add|3_0(System.Text.Json.Serialization.JsonConverter,System.Text.Json.JsonSerializerOptions/<>c__DisplayClass3_0&)
extern void JsonSerializerOptions_U3CGetDefaultSimpleConvertersU3Eg__AddU7C3_0_mEA8BDB668D0A86C811CD04F3FF1239DA7B5022C8 (void);
// 0x00000363 System.Text.Json.Serialization.JsonConverter System.Text.Json.JsonSerializerOptions::<GetDictionaryKeyConverter>g__GetEnumConverter|4_0(System.Text.Json.JsonSerializerOptions/<>c__DisplayClass4_0&)
extern void JsonSerializerOptions_U3CGetDictionaryKeyConverterU3Eg__GetEnumConverterU7C4_0_m26851F02B29FB81551992789E5E7F304ABC8174C (void);
// 0x00000364 System.Void System.Text.Json.JsonSerializerOptions::<GetDictionaryKeyConverters>g__Add|6_0(System.Text.Json.Serialization.JsonConverter,System.Text.Json.JsonSerializerOptions/<>c__DisplayClass6_0&)
extern void JsonSerializerOptions_U3CGetDictionaryKeyConvertersU3Eg__AddU7C6_0_m871391C97F5C4D6D9CE6ECC72C4372DA176EAE6C (void);
// 0x00000365 System.Text.Json.JsonClassInfo/ConstructorDelegate System.Text.Json.MemberAccessor::CreateConstructor(System.Type)
// 0x00000366 System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`1<T> System.Text.Json.MemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo)
// 0x00000367 System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`5<T,TArg0,TArg1,TArg2,TArg3> System.Text.Json.MemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo)
// 0x00000368 System.Action`2<TCollection,System.Object> System.Text.Json.MemberAccessor::CreateAddMethodDelegate()
// 0x00000369 System.Func`2<System.Collections.Generic.IEnumerable`1<TElement>,TCollection> System.Text.Json.MemberAccessor::CreateImmutableEnumerableCreateRangeDelegate()
// 0x0000036A System.Func`2<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,TElement>>,TCollection> System.Text.Json.MemberAccessor::CreateImmutableDictionaryCreateRangeDelegate()
// 0x0000036B System.Func`2<System.Object,TProperty> System.Text.Json.MemberAccessor::CreatePropertyGetter(System.Reflection.PropertyInfo)
// 0x0000036C System.Action`2<System.Object,TProperty> System.Text.Json.MemberAccessor::CreatePropertySetter(System.Reflection.PropertyInfo)
// 0x0000036D System.Func`2<System.Object,TProperty> System.Text.Json.MemberAccessor::CreateFieldGetter(System.Reflection.FieldInfo)
// 0x0000036E System.Action`2<System.Object,TProperty> System.Text.Json.MemberAccessor::CreateFieldSetter(System.Reflection.FieldInfo)
// 0x0000036F System.Void System.Text.Json.MemberAccessor::.ctor()
extern void MemberAccessor__ctor_m16A4BAC1382B1E770361738C12D604B051948066 (void);
// 0x00000370 System.Void System.Text.Json.ParameterRef::.ctor(System.UInt64,System.Text.Json.JsonParameterInfo,System.Byte[])
extern void ParameterRef__ctor_mD6F9E352509A2D16FBBFB12B05A40A70FB04672A (void);
// 0x00000371 System.Void System.Text.Json.PropertyRef::.ctor(System.UInt64,System.Text.Json.JsonPropertyInfo,System.Byte[])
extern void PropertyRef__ctor_mBB574D64A64A57C51A58468AC6514DE568D533E5 (void);
// 0x00000372 System.Boolean System.Text.Json.ReadStack::get_IsContinuation()
extern void ReadStack_get_IsContinuation_mCEC1851A10A829FFFE42BBAD53711A0A990526F8 (void);
// 0x00000373 System.Void System.Text.Json.ReadStack::AddCurrent()
extern void ReadStack_AddCurrent_m3B0106DB1337C3B7F26B2F874E40B80FA7FCBD18 (void);
// 0x00000374 System.Void System.Text.Json.ReadStack::Initialize(System.Type,System.Text.Json.JsonSerializerOptions,System.Boolean)
extern void ReadStack_Initialize_m4CCFB4EF4B2C80815463CE81FE499FFF8528BB3B (void);
// 0x00000375 System.Void System.Text.Json.ReadStack::Push()
extern void ReadStack_Push_m1F2599D52633E428D94A9D90E77A833FE64BC0BC (void);
// 0x00000376 System.Void System.Text.Json.ReadStack::Pop(System.Boolean)
extern void ReadStack_Pop_mD20CAC0E80A32CAE52481E175241AFDE02D4C9DD (void);
// 0x00000377 System.String System.Text.Json.ReadStack::JsonPath()
extern void ReadStack_JsonPath_m4DF8F1F7BB216E3E6F9EFE4066E9B94D270E49C5 (void);
// 0x00000378 System.Void System.Text.Json.ReadStack::SetConstructorArgumentState()
extern void ReadStack_SetConstructorArgumentState_mCE9355370E6E636D813179995CFCB4B869A20033 (void);
// 0x00000379 System.Void System.Text.Json.ReadStack::.cctor()
extern void ReadStack__cctor_mC4E80F9C51420C1613550EEEDA9E5500E6533CB1 (void);
// 0x0000037A System.Void System.Text.Json.ReadStack::<JsonPath>g__AppendStackFrame|19_0(System.Text.StringBuilder,System.Text.Json.ReadStackFrame&)
extern void ReadStack_U3CJsonPathU3Eg__AppendStackFrameU7C19_0_m09DC1845C124681041777206875E96719B692E6A (void);
// 0x0000037B System.Int32 System.Text.Json.ReadStack::<JsonPath>g__GetCount|19_1(System.Collections.IEnumerable)
extern void ReadStack_U3CJsonPathU3Eg__GetCountU7C19_1_m3DFAE067EBF1971BBC7F7870F3F41DE01F59004F (void);
// 0x0000037C System.Void System.Text.Json.ReadStack::<JsonPath>g__AppendPropertyName|19_2(System.Text.StringBuilder,System.String)
extern void ReadStack_U3CJsonPathU3Eg__AppendPropertyNameU7C19_2_mA3A779D7C6AEC6E48320FC93C5E3B87657C7D295 (void);
// 0x0000037D System.String System.Text.Json.ReadStack::<JsonPath>g__GetPropertyName|19_3(System.Text.Json.ReadStackFrame&)
extern void ReadStack_U3CJsonPathU3Eg__GetPropertyNameU7C19_3_m50B37A112F20DD2EEE4684007F8D886D5A92EB88 (void);
// 0x0000037E System.Void System.Text.Json.ReadStackFrame::EndConstructorParameter()
extern void ReadStackFrame_EndConstructorParameter_m8D04A7A3CE2D19DA2C0CCFA8937859DEF1151A14 (void);
// 0x0000037F System.Void System.Text.Json.ReadStackFrame::EndProperty()
extern void ReadStackFrame_EndProperty_mBF44974D081F8BDCC44666395A7CC82DB3E557E4 (void);
// 0x00000380 System.Void System.Text.Json.ReadStackFrame::EndElement()
extern void ReadStackFrame_EndElement_mA9D0401AD7A3F30475D97C6E49C671AA98558979 (void);
// 0x00000381 System.Boolean System.Text.Json.ReadStackFrame::IsProcessingDictionary()
extern void ReadStackFrame_IsProcessingDictionary_mBFC8EA6ACB5BA357E8887719A4CD2F61F14DD2AE (void);
// 0x00000382 System.Boolean System.Text.Json.ReadStackFrame::IsProcessingEnumerable()
extern void ReadStackFrame_IsProcessingEnumerable_m3A7B122E538645493032819C697D4FD0D7BB40E3 (void);
// 0x00000383 System.Void System.Text.Json.ReadStackFrame::Reset()
extern void ReadStackFrame_Reset_m9998E100B9CF5B2FC93FDE5C50FDB77227903C9A (void);
// 0x00000384 System.Boolean System.Text.Json.WriteStack::get_IsContinuation()
extern void WriteStack_get_IsContinuation_mBDA5ACA7E19BAF628ED4BDCE702B8B46E6AA9BDC (void);
// 0x00000385 System.Void System.Text.Json.WriteStack::AddCurrent()
extern void WriteStack_AddCurrent_mCA9F10EA1D6E24E25E27295386208F3E71A3654E (void);
// 0x00000386 System.Text.Json.Serialization.JsonConverter System.Text.Json.WriteStack::Initialize(System.Type,System.Text.Json.JsonSerializerOptions,System.Boolean)
extern void WriteStack_Initialize_mD5B4E7AF1DE653A9D2E614A452799636C009F9CC (void);
// 0x00000387 System.Void System.Text.Json.WriteStack::Push()
extern void WriteStack_Push_m7609AF43966C67D97FE3A16525E55D53FA2569AA (void);
// 0x00000388 System.Void System.Text.Json.WriteStack::Pop(System.Boolean)
extern void WriteStack_Pop_mC6F6750427C0EEEEAC7B9ED1BF9ECC07677F7000 (void);
// 0x00000389 System.String System.Text.Json.WriteStack::PropertyPath()
extern void WriteStack_PropertyPath_mAE92BA9B54387C839164F69A7944CF0401165039 (void);
// 0x0000038A System.Void System.Text.Json.WriteStack::<PropertyPath>g__AppendStackFrame|13_0(System.Text.StringBuilder,System.Text.Json.WriteStackFrame&)
extern void WriteStack_U3CPropertyPathU3Eg__AppendStackFrameU7C13_0_m74ADF97F41495445B46D2A01592312F0C76412E6 (void);
// 0x0000038B System.Void System.Text.Json.WriteStack::<PropertyPath>g__AppendPropertyName|13_1(System.Text.StringBuilder,System.String)
extern void WriteStack_U3CPropertyPathU3Eg__AppendPropertyNameU7C13_1_mE397598417E5CB94719735BB1B4D60A0E99FFC95 (void);
// 0x0000038C System.Void System.Text.Json.WriteStackFrame::EndDictionaryElement()
extern void WriteStackFrame_EndDictionaryElement_m0E57022D7831F92A528248C5A25CE8B5E0A28F0C (void);
// 0x0000038D System.Void System.Text.Json.WriteStackFrame::EndProperty()
extern void WriteStackFrame_EndProperty_m713E4D5585A2354CD0CE2DF56B7DA3B9E7FD31C8 (void);
// 0x0000038E System.Text.Json.JsonPropertyInfo System.Text.Json.WriteStackFrame::GetPolymorphicJsonPropertyInfo()
extern void WriteStackFrame_GetPolymorphicJsonPropertyInfo_m8C493C533AC6BB1D6E45E7F27F0664F157CDD602 (void);
// 0x0000038F System.Text.Json.Serialization.JsonConverter System.Text.Json.WriteStackFrame::InitializeReEntry(System.Type,System.Text.Json.JsonSerializerOptions,System.String)
extern void WriteStackFrame_InitializeReEntry_mDA0CD4D8B09439188519F97297A2A2D85D3BD619 (void);
// 0x00000390 System.Void System.Text.Json.WriteStackFrame::Reset()
extern void WriteStackFrame_Reset_mC74400A4F12DD1688E4D4C1EC4C6DC305B96E56E (void);
// 0x00000391 System.Boolean System.Text.Json.TypeExtensions::IsNullableValueType(System.Type)
extern void TypeExtensions_IsNullableValueType_m4DB0F40A9B037168E72346EAC4F5E36CA3388916 (void);
// 0x00000392 System.Boolean System.Text.Json.TypeExtensions::IsNullableType(System.Type)
extern void TypeExtensions_IsNullableType_mAF8621F34981409231CA30DD00F28B6096765F68 (void);
// 0x00000393 System.Boolean System.Text.Json.TypeExtensions::IsAssignableFromInternal(System.Type,System.Type)
extern void TypeExtensions_IsAssignableFromInternal_m984159CD43E51B85C367651715071C56FE692379 (void);
// 0x00000394 System.Void System.Text.Json.JsonWriterHelper::WriteIndentation(System.Span`1<System.Byte>,System.Int32)
extern void JsonWriterHelper_WriteIndentation_m3FF1948E81F7765D1C4D8ED825664DD6CFF8537C (void);
// 0x00000395 System.Void System.Text.Json.JsonWriterHelper::ValidateProperty(System.ReadOnlySpan`1<System.Byte>)
extern void JsonWriterHelper_ValidateProperty_m1F34A8D5BFC3B8AEF6935B5B42C1A0045D07162B (void);
// 0x00000396 System.Void System.Text.Json.JsonWriterHelper::ValidateValue(System.ReadOnlySpan`1<System.Byte>)
extern void JsonWriterHelper_ValidateValue_mD8CCD8D9A6983372F1D9E36E37291A855324C3BA (void);
// 0x00000397 System.Void System.Text.Json.JsonWriterHelper::ValidateBytes(System.ReadOnlySpan`1<System.Byte>)
extern void JsonWriterHelper_ValidateBytes_m66574E5CBB54C01C8DD0CF770AB713231151ABDE (void);
// 0x00000398 System.Void System.Text.Json.JsonWriterHelper::ValidateDouble(System.Double)
extern void JsonWriterHelper_ValidateDouble_m273C268E501F4DE8A73411EC0E4F91913F5C7447 (void);
// 0x00000399 System.Void System.Text.Json.JsonWriterHelper::ValidateSingle(System.Single)
extern void JsonWriterHelper_ValidateSingle_mB5D28135F97269867F8DC0389E382DEA7317DF21 (void);
// 0x0000039A System.Void System.Text.Json.JsonWriterHelper::ValidateProperty(System.ReadOnlySpan`1<System.Char>)
extern void JsonWriterHelper_ValidateProperty_m870A88B693769B0670D79885D858B52192EE93E6 (void);
// 0x0000039B System.Void System.Text.Json.JsonWriterHelper::ValidateValue(System.ReadOnlySpan`1<System.Char>)
extern void JsonWriterHelper_ValidateValue_mFB60C5BD2132B5D147FAD6A9DE920C2F765DA2B0 (void);
// 0x0000039C System.Void System.Text.Json.JsonWriterHelper::ValidateNumber(System.ReadOnlySpan`1<System.Byte>)
extern void JsonWriterHelper_ValidateNumber_mD57FF09936333FB187C8E1FBBF58F1C8CECBFDA6 (void);
// 0x0000039D System.Void System.Text.Json.JsonWriterHelper::WriteDateTimeTrimmed(System.Span`1<System.Byte>,System.DateTime,System.Int32&)
extern void JsonWriterHelper_WriteDateTimeTrimmed_mEAF361B4FF5284DDDB39A5B0A0F1FF24A12FD65F (void);
// 0x0000039E System.Void System.Text.Json.JsonWriterHelper::WriteDateTimeOffsetTrimmed(System.Span`1<System.Byte>,System.DateTimeOffset,System.Int32&)
extern void JsonWriterHelper_WriteDateTimeOffsetTrimmed_m054EF2DB0488EF8100A719D92F5EB7DAABF47CA1 (void);
// 0x0000039F System.Void System.Text.Json.JsonWriterHelper::TrimDateTimeOffset(System.Span`1<System.Byte>,System.Int32&)
extern void JsonWriterHelper_TrimDateTimeOffset_mAC5CD5A22C382458B18E1A5316289454789490BC (void);
// 0x000003A0 System.UInt32 System.Text.Json.JsonWriterHelper::DivMod(System.UInt32,System.UInt32,System.UInt32&)
extern void JsonWriterHelper_DivMod_mD6FC02993BB66E34D1611A3CB0803C548EE1F3A5 (void);
// 0x000003A1 System.ReadOnlySpan`1<System.Byte> System.Text.Json.JsonWriterHelper::get_AllowList()
extern void JsonWriterHelper_get_AllowList_mA28609B6B9076FA68AC62A6E99A56F15278670D3 (void);
// 0x000003A2 System.Boolean System.Text.Json.JsonWriterHelper::NeedsEscaping(System.Byte)
extern void JsonWriterHelper_NeedsEscaping_mFE93A07F36E809CB539B11A56C653D48554C9395 (void);
// 0x000003A3 System.Boolean System.Text.Json.JsonWriterHelper::NeedsEscapingNoBoundsCheck(System.Char)
extern void JsonWriterHelper_NeedsEscapingNoBoundsCheck_mD12552627BD85E3DD84860062048DA6D9BF57878 (void);
// 0x000003A4 System.Int32 System.Text.Json.JsonWriterHelper::NeedsEscaping(System.ReadOnlySpan`1<System.Byte>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonWriterHelper_NeedsEscaping_m2C8304CA115F8E6DEF835C700499D77387F13CA8 (void);
// 0x000003A5 System.Int32 System.Text.Json.JsonWriterHelper::NeedsEscaping(System.ReadOnlySpan`1<System.Char>,System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonWriterHelper_NeedsEscaping_m1EE16B480DFB8212DF545CAE1531E7741B705C9E (void);
// 0x000003A6 System.Int32 System.Text.Json.JsonWriterHelper::GetMaxEscapedLength(System.Int32,System.Int32)
extern void JsonWriterHelper_GetMaxEscapedLength_mE4CCE091405F6401E43DAE665652621A358326C4 (void);
// 0x000003A7 System.Void System.Text.Json.JsonWriterHelper::EscapeString(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Text.Encodings.Web.JavaScriptEncoder,System.Int32&)
extern void JsonWriterHelper_EscapeString_m300A0A3F3C73F32440B227F5FA7CD90146D852C4 (void);
// 0x000003A8 System.Void System.Text.Json.JsonWriterHelper::EscapeString(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32,System.Text.Encodings.Web.JavaScriptEncoder,System.Int32&)
extern void JsonWriterHelper_EscapeString_m564702FF6E13FE62D005D7AFEC973C086288877D (void);
// 0x000003A9 System.Void System.Text.Json.JsonWriterHelper::EscapeNextBytes(System.Byte,System.Span`1<System.Byte>,System.Int32&)
extern void JsonWriterHelper_EscapeNextBytes_mA3984914396004727102D290DD1BD96A3E4DE830 (void);
// 0x000003AA System.Boolean System.Text.Json.JsonWriterHelper::IsAsciiValue(System.Byte)
extern void JsonWriterHelper_IsAsciiValue_mB771D4B77A06BDE98B39D52324FAD6A8E1F33E57 (void);
// 0x000003AB System.Boolean System.Text.Json.JsonWriterHelper::IsAsciiValue(System.Char)
extern void JsonWriterHelper_IsAsciiValue_m95272B549B92BDD457C999FBC9F41B51226673B6 (void);
// 0x000003AC System.Void System.Text.Json.JsonWriterHelper::EscapeString(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Text.Encodings.Web.JavaScriptEncoder,System.Int32&)
extern void JsonWriterHelper_EscapeString_m6A3C10DB5FDFA81CF6BEAF45725802FFE0CAA868 (void);
// 0x000003AD System.Void System.Text.Json.JsonWriterHelper::EscapeString(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Char>,System.Int32,System.Text.Encodings.Web.JavaScriptEncoder,System.Int32&)
extern void JsonWriterHelper_EscapeString_mD98ADCFA56B4C5896030D35AE577B6210588B383 (void);
// 0x000003AE System.Void System.Text.Json.JsonWriterHelper::EscapeNextChars(System.Char,System.Span`1<System.Char>,System.Int32&)
extern void JsonWriterHelper_EscapeNextChars_mCDD04B1D68EAFB21573130C3C2645DBE31307C39 (void);
// 0x000003AF System.Int32 System.Text.Json.JsonWriterHelper::WriteHex(System.Int32,System.Span`1<System.Char>,System.Int32)
extern void JsonWriterHelper_WriteHex_m7A120B39639623607153F51F1E3D78A8C43A47FE (void);
// 0x000003B0 System.Buffers.OperationStatus System.Text.Json.JsonWriterHelper::ToUtf8(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32&,System.Int32&)
extern void JsonWriterHelper_ToUtf8_m0EC452325B9038CDF4AFC9F74E55A7194674B4F6 (void);
// 0x000003B1 System.Int32 System.Text.Json.JsonWriterHelper::PtrDiff(System.Char*,System.Char*)
extern void JsonWriterHelper_PtrDiff_m38A892D52BF473A2345B38153C2F143007339D33 (void);
// 0x000003B2 System.Int32 System.Text.Json.JsonWriterHelper::PtrDiff(System.Byte*,System.Byte*)
extern void JsonWriterHelper_PtrDiff_mD1A028CB8E273EE4BA081972D46DBBF5E1152425 (void);
// 0x000003B3 System.Void System.Text.Json.JsonWriterHelper::.cctor()
extern void JsonWriterHelper__cctor_m3E983799FC90D8DEB824C22C20E0462A5FF2FABC (void);
// 0x000003B4 System.Text.Encodings.Web.JavaScriptEncoder System.Text.Json.JsonWriterOptions::get_Encoder()
extern void JsonWriterOptions_get_Encoder_mAE5C32DE2F9E54D87346B602EA2DB2C0676D7406 (void);
// 0x000003B5 System.Void System.Text.Json.JsonWriterOptions::set_Encoder(System.Text.Encodings.Web.JavaScriptEncoder)
extern void JsonWriterOptions_set_Encoder_m5E357AEFC0AB885F7858AE1D70B7060273AED306 (void);
// 0x000003B6 System.Boolean System.Text.Json.JsonWriterOptions::get_Indented()
extern void JsonWriterOptions_get_Indented_mDF7E8A47E47FF6E43BE1E577F434CA7525E4AC0A (void);
// 0x000003B7 System.Void System.Text.Json.JsonWriterOptions::set_Indented(System.Boolean)
extern void JsonWriterOptions_set_Indented_m5418A714E77EA6D9734E6A497FFD407D799AF8DB (void);
// 0x000003B8 System.Boolean System.Text.Json.JsonWriterOptions::get_SkipValidation()
extern void JsonWriterOptions_get_SkipValidation_m9113D9C54C02B4EBA452A57485A91F8A30E74DB0 (void);
// 0x000003B9 System.Void System.Text.Json.JsonWriterOptions::set_SkipValidation(System.Boolean)
extern void JsonWriterOptions_set_SkipValidation_m805F1EC553421C91697F04ED05567BB51981F43A (void);
// 0x000003BA System.Boolean System.Text.Json.JsonWriterOptions::get_IndentedOrNotSkipValidation()
extern void JsonWriterOptions_get_IndentedOrNotSkipValidation_mFC205FBEF9620AD791C2EAC0A7F776281E2D9900 (void);
// 0x000003BB System.Int32 System.Text.Json.Utf8JsonWriter::get_BytesPending()
extern void Utf8JsonWriter_get_BytesPending_m28D90DE67217F118AFA594E9F9D055183474E452 (void);
// 0x000003BC System.Void System.Text.Json.Utf8JsonWriter::set_BytesPending(System.Int32)
extern void Utf8JsonWriter_set_BytesPending_m74D2AA59465ED7058538164D4B1DFB19E58C2ACD (void);
// 0x000003BD System.Int64 System.Text.Json.Utf8JsonWriter::get_BytesCommitted()
extern void Utf8JsonWriter_get_BytesCommitted_mEAF5E6159D99CD34796639914CCA84473295AACF (void);
// 0x000003BE System.Void System.Text.Json.Utf8JsonWriter::set_BytesCommitted(System.Int64)
extern void Utf8JsonWriter_set_BytesCommitted_m0611AE9EF0E556F695ED2D1C71DA272DC0506B4C (void);
// 0x000003BF System.Int32 System.Text.Json.Utf8JsonWriter::get_Indentation()
extern void Utf8JsonWriter_get_Indentation_m829020AB9C3F388A3F1BD34EE21E9534AA6861CF (void);
// 0x000003C0 System.Int32 System.Text.Json.Utf8JsonWriter::get_CurrentDepth()
extern void Utf8JsonWriter_get_CurrentDepth_m8C0AF093B68AFDD3BC097AB985DB8FF76CD7BA47 (void);
// 0x000003C1 System.Void System.Text.Json.Utf8JsonWriter::.ctor(System.Buffers.IBufferWriter`1<System.Byte>,System.Text.Json.JsonWriterOptions)
extern void Utf8JsonWriter__ctor_m036DB06EA633C229C4147A6DE656934C77AB794F (void);
// 0x000003C2 System.Void System.Text.Json.Utf8JsonWriter::ResetHelper()
extern void Utf8JsonWriter_ResetHelper_m75A417EA64C4EBF6D4E2AF8C54D90B95770E49B3 (void);
// 0x000003C3 System.Void System.Text.Json.Utf8JsonWriter::CheckNotDisposed()
extern void Utf8JsonWriter_CheckNotDisposed_m3023C02F6F75AB548E99526BE817223286AC82EA (void);
// 0x000003C4 System.Void System.Text.Json.Utf8JsonWriter::Flush()
extern void Utf8JsonWriter_Flush_m71B82C161B4ECCB5D90DCBBF667D3647404A7799 (void);
// 0x000003C5 System.Void System.Text.Json.Utf8JsonWriter::Dispose()
extern void Utf8JsonWriter_Dispose_mBAFB87EB612CD2B6E911A32B5BDDC41308AB433F (void);
// 0x000003C6 System.Void System.Text.Json.Utf8JsonWriter::WriteStartArray()
extern void Utf8JsonWriter_WriteStartArray_mD418C2E501B88084EA5837B9BE9038EA75CA1337 (void);
// 0x000003C7 System.Void System.Text.Json.Utf8JsonWriter::WriteStartObject()
extern void Utf8JsonWriter_WriteStartObject_mA1EF1E8CCF738E49D16F99CDA5F1F9E6868D88B2 (void);
// 0x000003C8 System.Void System.Text.Json.Utf8JsonWriter::WriteStart(System.Byte)
extern void Utf8JsonWriter_WriteStart_mA8A609C59C9BC1B5BFB4D82BD18328952019FDA1 (void);
// 0x000003C9 System.Void System.Text.Json.Utf8JsonWriter::WriteStartMinimized(System.Byte)
extern void Utf8JsonWriter_WriteStartMinimized_mB1A7139369CD1119EDD231484C804B9DFBA883EC (void);
// 0x000003CA System.Void System.Text.Json.Utf8JsonWriter::WriteStartSlow(System.Byte)
extern void Utf8JsonWriter_WriteStartSlow_m0A135B5009203BA02F5341E152E27654552DC02C (void);
// 0x000003CB System.Void System.Text.Json.Utf8JsonWriter::ValidateStart()
extern void Utf8JsonWriter_ValidateStart_m6731F9FFACF4565D12DC27106FF4BF655650EEF8 (void);
// 0x000003CC System.Void System.Text.Json.Utf8JsonWriter::WriteStartIndented(System.Byte)
extern void Utf8JsonWriter_WriteStartIndented_m7BB700FADBAC557FE53D6A264734B0F0597FDB39 (void);
// 0x000003CD System.Void System.Text.Json.Utf8JsonWriter::WriteStartArray(System.Text.Json.JsonEncodedText)
extern void Utf8JsonWriter_WriteStartArray_m03E814CDF1C585AD239AC26E0B0EEBA6EF370F6F (void);
// 0x000003CE System.Void System.Text.Json.Utf8JsonWriter::WriteStartHelper(System.ReadOnlySpan`1<System.Byte>,System.Byte)
extern void Utf8JsonWriter_WriteStartHelper_m2A737E064AA3D2FDB19B629304F533A8FE583090 (void);
// 0x000003CF System.Void System.Text.Json.Utf8JsonWriter::WriteStartByOptions(System.ReadOnlySpan`1<System.Byte>,System.Byte)
extern void Utf8JsonWriter_WriteStartByOptions_mE98BD442E748E3950B2943CE3B1136B08DF92FAF (void);
// 0x000003D0 System.Void System.Text.Json.Utf8JsonWriter::WriteEndArray()
extern void Utf8JsonWriter_WriteEndArray_m5777DA78B4AD16920865AFBF6800D5A0AE7463EB (void);
// 0x000003D1 System.Void System.Text.Json.Utf8JsonWriter::WriteEndObject()
extern void Utf8JsonWriter_WriteEndObject_mCBB8ED094BB4B5196B0D6496B2C8EC2A40C71EFA (void);
// 0x000003D2 System.Void System.Text.Json.Utf8JsonWriter::WriteEnd(System.Byte)
extern void Utf8JsonWriter_WriteEnd_m77890C8C2A9C911157F9972E4FF9CBFA712D02AF (void);
// 0x000003D3 System.Void System.Text.Json.Utf8JsonWriter::WriteEndMinimized(System.Byte)
extern void Utf8JsonWriter_WriteEndMinimized_mA33335B7FDB7413EE731D3C1844B45026F25EB3A (void);
// 0x000003D4 System.Void System.Text.Json.Utf8JsonWriter::WriteEndSlow(System.Byte)
extern void Utf8JsonWriter_WriteEndSlow_mECA3F6EB97FB3CBE19FBDABA77010DC1EE8290D4 (void);
// 0x000003D5 System.Void System.Text.Json.Utf8JsonWriter::ValidateEnd(System.Byte)
extern void Utf8JsonWriter_ValidateEnd_m1F0FF13359265179652859C5FD0A4E3219F45B3E (void);
// 0x000003D6 System.Void System.Text.Json.Utf8JsonWriter::WriteEndIndented(System.Byte)
extern void Utf8JsonWriter_WriteEndIndented_m39BDB70B1110F40D7C17E13095CA802DCD0CD457 (void);
// 0x000003D7 System.Void System.Text.Json.Utf8JsonWriter::WriteNewLine(System.Span`1<System.Byte>)
extern void Utf8JsonWriter_WriteNewLine_mE43F1B48D99F3E69397722AD89C068FECF60DBF2 (void);
// 0x000003D8 System.Void System.Text.Json.Utf8JsonWriter::UpdateBitStackOnStart(System.Byte)
extern void Utf8JsonWriter_UpdateBitStackOnStart_m12969BF6585ED46DCD0106D02A9D0C9344495337 (void);
// 0x000003D9 System.Void System.Text.Json.Utf8JsonWriter::Grow(System.Int32)
extern void Utf8JsonWriter_Grow_m45CE325E04A63A8160A6DE6D507B0FBD324914EF (void);
// 0x000003DA System.Void System.Text.Json.Utf8JsonWriter::FirstCallToGetMemory(System.Int32)
extern void Utf8JsonWriter_FirstCallToGetMemory_m421AB335FA4A40AB99E7DBEC68C5C93B45BA300B (void);
// 0x000003DB System.Void System.Text.Json.Utf8JsonWriter::SetFlagToAddListSeparatorBeforeNextItem()
extern void Utf8JsonWriter_SetFlagToAddListSeparatorBeforeNextItem_mC49F581160BBC99B7CB7AEA4B9BD5D9C0C02A271 (void);
// 0x000003DC System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.DateTime)
extern void Utf8JsonWriter_WritePropertyName_mDAE1CD6CD231D4BDBD36ED2DEFA7A9AB1DBB27F0 (void);
// 0x000003DD System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.DateTimeOffset)
extern void Utf8JsonWriter_WritePropertyName_m6E24B02F2463E24E463A2FBACC3FABD86C712149 (void);
// 0x000003DE System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Decimal)
extern void Utf8JsonWriter_WritePropertyName_m48CEDC9718A9DC59E65A3BDB8E61FF58F0F43CA4 (void);
// 0x000003DF System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Double)
extern void Utf8JsonWriter_WritePropertyName_m9745E77C67CE1CE4A7FFEBFBD1A49B215616BA21 (void);
// 0x000003E0 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Single)
extern void Utf8JsonWriter_WritePropertyName_mF18FCE3386F464FCF73C67F457114F3AAB3174CE (void);
// 0x000003E1 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Guid)
extern void Utf8JsonWriter_WritePropertyName_m9E5769869B82691F435838443F4B1F32A9134D55 (void);
// 0x000003E2 System.Void System.Text.Json.Utf8JsonWriter::ValidateDepth()
extern void Utf8JsonWriter_ValidateDepth_mB823685607B5551DBB7711165324EEDAB31C5680 (void);
// 0x000003E3 System.Void System.Text.Json.Utf8JsonWriter::ValidateWritingProperty()
extern void Utf8JsonWriter_ValidateWritingProperty_mA774D19194EDA65A154C7188ACB9363727C22381 (void);
// 0x000003E4 System.Void System.Text.Json.Utf8JsonWriter::ValidateWritingProperty(System.Byte)
extern void Utf8JsonWriter_ValidateWritingProperty_mC581A96093E5B8C0F28A38802637D6BA4E6B831B (void);
// 0x000003E5 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameMinimized(System.ReadOnlySpan`1<System.Byte>,System.Byte)
extern void Utf8JsonWriter_WritePropertyNameMinimized_m54CBAC2EF2D453DCA1C9B748662092292F6F61A8 (void);
// 0x000003E6 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameIndented(System.ReadOnlySpan`1<System.Byte>,System.Byte)
extern void Utf8JsonWriter_WritePropertyNameIndented_mE93A9AF3608723700069C12733B8D67BBF56F81C (void);
// 0x000003E7 System.Void System.Text.Json.Utf8JsonWriter::TranscodeAndWrite(System.ReadOnlySpan`1<System.Char>,System.Span`1<System.Byte>)
extern void Utf8JsonWriter_TranscodeAndWrite_m02497951FB0FC80A851B31DC706BA4E315B5BEB2 (void);
// 0x000003E8 System.Void System.Text.Json.Utf8JsonWriter::WriteNull(System.Text.Json.JsonEncodedText)
extern void Utf8JsonWriter_WriteNull_m6DFE540D01400EA53C8981F2381BB9A808F19753 (void);
// 0x000003E9 System.Void System.Text.Json.Utf8JsonWriter::WriteNullSection(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNullSection_m64DDF170B1C5AA3DBFB28E42B70851C8C36B7BF0 (void);
// 0x000003EA System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralHelper(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralHelper_mB50BD28FAB2D9A9D8372DDA1587CDA618C575DAD (void);
// 0x000003EB System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralByOptions(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralByOptions_mF40749C21DDE2ECCD83A84C932B531870879B3DE (void);
// 0x000003EC System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralMinimized(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralMinimized_mCF37DA0347C180E374CD50A5B7699612CB89B07F (void);
// 0x000003ED System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralSection(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralSection_mED2F87320316A0959FA57AB10B79FC801F25E0F5 (void);
// 0x000003EE System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralIndented(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralIndented_mADA2844178A4819A5B286C59B67ED38A3B11F1A2 (void);
// 0x000003EF System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Boolean)
extern void Utf8JsonWriter_WritePropertyName_m18A003196843D1D3F241ABC7AB9E290B57C04DCD (void);
// 0x000003F0 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Int32)
extern void Utf8JsonWriter_WritePropertyName_m58C006A592B878D9777F4E5793E423027DD75FF6 (void);
// 0x000003F1 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Int64)
extern void Utf8JsonWriter_WritePropertyName_m04C61608930141086671CD87B191CCB42B7CAAD2 (void);
// 0x000003F2 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.Text.Json.JsonEncodedText)
extern void Utf8JsonWriter_WritePropertyName_m4EC2DAE071F9EF28604782A57F202CD14D9F5A3A (void);
// 0x000003F3 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameSection(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WritePropertyNameSection_mE4638818A05308F694887A41B259D807CFF0D885 (void);
// 0x000003F4 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameHelper(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WritePropertyNameHelper_m8A3CEF329C2682F61605A1C57EAF563196D58E55 (void);
// 0x000003F5 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.String)
extern void Utf8JsonWriter_WritePropertyName_m996942A298DED618DCBE28108F27E373B2E5D95F (void);
// 0x000003F6 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WritePropertyName_mD5C11D2F04DD4116BD2609292A3B2D05E1C357B2 (void);
// 0x000003F7 System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeProperty(System.ReadOnlySpan`1<System.Char>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeProperty_mCD08606811907E99503A17C244B37E19D66831C9 (void);
// 0x000003F8 System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptionsPropertyName(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringByOptionsPropertyName_mD1E3902B7A1A32B78FD5845C5756B954118412A4 (void);
// 0x000003F9 System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimizedPropertyName(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringMinimizedPropertyName_m235EA20291770CBE9C0EF62ACB96CE02E213236F (void);
// 0x000003FA System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndentedPropertyName(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringIndentedPropertyName_mCFD4644263D11B444714D883359C350CFFE46C3F (void);
// 0x000003FB System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WritePropertyName_m3FAD66D83E22D24D7263E1079BB6890810EFC2A5 (void);
// 0x000003FC System.Void System.Text.Json.Utf8JsonWriter::WritePropertyNameUnescaped(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WritePropertyNameUnescaped_m09979AE493AA8956D5205CE26CECC1F6B66D6AE2 (void);
// 0x000003FD System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeProperty(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeProperty_mD9E3F7403C744CFF3A069A9DFE3BCD459A768211 (void);
// 0x000003FE System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptionsPropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringByOptionsPropertyName_m39086B7083EDA5468602039C03E6F04B30D255FC (void);
// 0x000003FF System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimizedPropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringMinimizedPropertyName_m7629BDC4144A3DB500FEDA53A2FBF3EB536D4FE0 (void);
// 0x00000400 System.Void System.Text.Json.Utf8JsonWriter::WriteStringPropertyNameSection(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringPropertyNameSection_m44FD370CB27FCBB506A84E3C9358639F5E38217E (void);
// 0x00000401 System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndentedPropertyName(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringIndentedPropertyName_m18DEAB086D3C46BBD359C512045A1A77EB513141 (void);
// 0x00000402 System.Void System.Text.Json.Utf8JsonWriter::WriteString(System.Text.Json.JsonEncodedText,System.String)
extern void Utf8JsonWriter_WriteString_m384F27B26D0420B7FA6F228E6F703E4D6FA97374 (void);
// 0x00000403 System.Void System.Text.Json.Utf8JsonWriter::WriteString(System.Text.Json.JsonEncodedText,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteString_mEA5D1F20772424C9F65052E81FE89F0686254F29 (void);
// 0x00000404 System.Void System.Text.Json.Utf8JsonWriter::WriteStringHelperEscapeValue(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringHelperEscapeValue_m4DD21A58FA2D0B801070396213E1E4A5D58E2F06 (void);
// 0x00000405 System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeValueOnly(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeValueOnly_m7611554B512BFA5371D7478F894ACA4B7B207529 (void);
// 0x00000406 System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptions(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringByOptions_m89197627FBA2D52B62559D6F4C6B820F24BC5A16 (void);
// 0x00000407 System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimized(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringMinimized_m9AC498A1407ACE4CBC2B12A2E875A962E580AC26 (void);
// 0x00000408 System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndented(System.ReadOnlySpan`1<System.Byte>,System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringIndented_m35BE4CEEF434283CB28B295C89A14B0F4899BF60 (void);
// 0x00000409 System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.UInt32)
extern void Utf8JsonWriter_WritePropertyName_mAA130A04E9004C4A74F36C3F1396B4FE5868E921 (void);
// 0x0000040A System.Void System.Text.Json.Utf8JsonWriter::WritePropertyName(System.UInt64)
extern void Utf8JsonWriter_WritePropertyName_m670F9DE802D05679BF7EB5701E9ADB3027402C11 (void);
// 0x0000040B System.Void System.Text.Json.Utf8JsonWriter::WriteBase64StringValue(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteBase64StringValue_m4CC0DAD92DF2723492C5DF18B8B3028BA6009C00 (void);
// 0x0000040C System.Void System.Text.Json.Utf8JsonWriter::WriteBase64ByOptions(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteBase64ByOptions_m55F12E59982BAB3D259EB44ABCAE8A57CB157ECC (void);
// 0x0000040D System.Void System.Text.Json.Utf8JsonWriter::WriteBase64Minimized(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteBase64Minimized_m160BE7FFE69E00FD96281A193DA692A48E51672A (void);
// 0x0000040E System.Void System.Text.Json.Utf8JsonWriter::WriteBase64Indented(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteBase64Indented_m2A0919AFD3140A557347B134520E5FEDEFEF8A6E (void);
// 0x0000040F System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.DateTime)
extern void Utf8JsonWriter_WriteStringValue_mE130AF1B2A2C307C9A4BE3282CDCA46E708BDAC0 (void);
// 0x00000410 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueMinimized(System.DateTime)
extern void Utf8JsonWriter_WriteStringValueMinimized_mE92796D227AC277168F1B2DFBF30EE80C5877C9D (void);
// 0x00000411 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueIndented(System.DateTime)
extern void Utf8JsonWriter_WriteStringValueIndented_mACD09DBFF483579AB0421C839E4491979D549CEC (void);
// 0x00000412 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.DateTimeOffset)
extern void Utf8JsonWriter_WriteStringValue_mDB0C7D389DBCAA8408DB0FFD6BE056F4DB179732 (void);
// 0x00000413 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueMinimized(System.DateTimeOffset)
extern void Utf8JsonWriter_WriteStringValueMinimized_mC19B4EDA129797DCDDDAA706633FBEBC6F1C7C7B (void);
// 0x00000414 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueIndented(System.DateTimeOffset)
extern void Utf8JsonWriter_WriteStringValueIndented_mF97D6265D42C31C9CD5B61346DA24B279FF69EFB (void);
// 0x00000415 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Decimal)
extern void Utf8JsonWriter_WriteNumberValue_m95366FE40CF93FC6091A43A618B70867460869D6 (void);
// 0x00000416 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.Decimal)
extern void Utf8JsonWriter_WriteNumberValueMinimized_m4F0299106C4EAB1CF9373B92AD508A79B8E114ED (void);
// 0x00000417 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.Decimal)
extern void Utf8JsonWriter_WriteNumberValueIndented_m2E96C373C9B770E458A9F1CD50B2C603842F2CC5 (void);
// 0x00000418 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.Decimal)
extern void Utf8JsonWriter_WriteNumberValueAsString_m6838AE0510AFCF321853096B370CFA8FF08D6747 (void);
// 0x00000419 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Double)
extern void Utf8JsonWriter_WriteNumberValue_mF264282A28EF611A3174E363C56C810583DE103E (void);
// 0x0000041A System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.Double)
extern void Utf8JsonWriter_WriteNumberValueMinimized_mEB0F2A86C89610DEF4BA8247805357A8C6254474 (void);
// 0x0000041B System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.Double)
extern void Utf8JsonWriter_WriteNumberValueIndented_m977A5605F0FD013FA8D556B37A5903BCF10C2217 (void);
// 0x0000041C System.Boolean System.Text.Json.Utf8JsonWriter::TryFormatDouble(System.Double,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8JsonWriter_TryFormatDouble_m1E46A985C1868AFE99C22B1C4385D3E55409C631 (void);
// 0x0000041D System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.Double)
extern void Utf8JsonWriter_WriteNumberValueAsString_m7BE06043327957908883709EA7A8C5FF0F777E89 (void);
// 0x0000041E System.Void System.Text.Json.Utf8JsonWriter::WriteFloatingPointConstant(System.Double)
extern void Utf8JsonWriter_WriteFloatingPointConstant_m9B685089C517B3CED834A0EABC369AD77830292C (void);
// 0x0000041F System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Single)
extern void Utf8JsonWriter_WriteNumberValue_m94C41B349B7D55EB269FA36E4454661C72DED51D (void);
// 0x00000420 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.Single)
extern void Utf8JsonWriter_WriteNumberValueMinimized_m58D6CF2A5BB517EFB6EF5D090C42DCEC11C5F1B6 (void);
// 0x00000421 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.Single)
extern void Utf8JsonWriter_WriteNumberValueIndented_mE3555B7FFB2B32EFB3F1D37B4EF919F8A4D2DCE1 (void);
// 0x00000422 System.Boolean System.Text.Json.Utf8JsonWriter::TryFormatSingle(System.Single,System.Span`1<System.Byte>,System.Int32&)
extern void Utf8JsonWriter_TryFormatSingle_mDCEC63E655C729E9BB8ECF387BDBDA0804AFCE4E (void);
// 0x00000423 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.Single)
extern void Utf8JsonWriter_WriteNumberValueAsString_mAC5CE62CE5ADB88A61DC78AD17359B1311C61DC7 (void);
// 0x00000424 System.Void System.Text.Json.Utf8JsonWriter::WriteFloatingPointConstant(System.Single)
extern void Utf8JsonWriter_WriteFloatingPointConstant_mC67C37D7A585CBFA4C2C0A70254F5C8567FE28F9 (void);
// 0x00000425 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNumberValue_m60A639D1ADCDC797A1127B323B98F16CD8BAE893 (void);
// 0x00000426 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNumberValueMinimized_mEF778935C8C0B6CB9D0FC62459DC0F8E0F0F1416 (void);
// 0x00000427 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNumberValueIndented_m70FB4B419767F277A8FEF5E6147CECA4DED043C1 (void);
// 0x00000428 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.Guid)
extern void Utf8JsonWriter_WriteStringValue_m3797F39346137A2D7232677B84B2F690896C4E6A (void);
// 0x00000429 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueMinimized(System.Guid)
extern void Utf8JsonWriter_WriteStringValueMinimized_m3041C690044C0EED2D5E0B28C58EADC9F73DB5BB (void);
// 0x0000042A System.Void System.Text.Json.Utf8JsonWriter::WriteStringValueIndented(System.Guid)
extern void Utf8JsonWriter_WriteStringValueIndented_mA63D85F1B2CE38CED7A4CD8CA249914F6D305D74 (void);
// 0x0000042B System.Void System.Text.Json.Utf8JsonWriter::ValidateWritingValue()
extern void Utf8JsonWriter_ValidateWritingValue_m1EDE5D0CAC37BCB2E4805B0455348BE0832B78A9 (void);
// 0x0000042C System.Void System.Text.Json.Utf8JsonWriter::Base64EncodeAndWrite(System.ReadOnlySpan`1<System.Byte>,System.Span`1<System.Byte>,System.Int32)
extern void Utf8JsonWriter_Base64EncodeAndWrite_m4E354E57EB2F24CAB3C90CDA20487D73B6922E7B (void);
// 0x0000042D System.Void System.Text.Json.Utf8JsonWriter::WriteNullValue()
extern void Utf8JsonWriter_WriteNullValue_mD363DC1B38AAE17A2666646629B9A3640C898004 (void);
// 0x0000042E System.Void System.Text.Json.Utf8JsonWriter::WriteBooleanValue(System.Boolean)
extern void Utf8JsonWriter_WriteBooleanValue_mA04EC87538557C4D69DA626CE79295F85A722BFF (void);
// 0x0000042F System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralByOptions(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralByOptions_m3755A2C70936A990E52C40F30BD55AFA97F8725A (void);
// 0x00000430 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralMinimized(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralMinimized_m84ED52B356CB9F4101AE7F94A2D2B5A89FCD270B (void);
// 0x00000431 System.Void System.Text.Json.Utf8JsonWriter::WriteLiteralIndented(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteLiteralIndented_mFFA6AFBB368388771828D0F54CA09FEECAAB9220 (void);
// 0x00000432 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Int32)
extern void Utf8JsonWriter_WriteNumberValue_m8A2EB2DC02416A0F4B0CA97AFA70B873FBCC3B7D (void);
// 0x00000433 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.Int64)
extern void Utf8JsonWriter_WriteNumberValue_m04B3E8A97F3FFECFFCCD5F17B74488E285482FA1 (void);
// 0x00000434 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.Int64)
extern void Utf8JsonWriter_WriteNumberValueMinimized_mE627304636D30A948A8EE0DA7292EBA8A2F8C2E8 (void);
// 0x00000435 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.Int64)
extern void Utf8JsonWriter_WriteNumberValueIndented_m3621E4629BDFBC8D31107EACD21F4932575CF463 (void);
// 0x00000436 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.Int64)
extern void Utf8JsonWriter_WriteNumberValueAsString_m3BB71B7C2534669326AE6C75F57E6E4AD429F05D (void);
// 0x00000437 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.Text.Json.JsonEncodedText)
extern void Utf8JsonWriter_WriteStringValue_m09F57C202C9D190F1EEE5D6AF6C6F11B36ADAAB2 (void);
// 0x00000438 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.String)
extern void Utf8JsonWriter_WriteStringValue_m348BACAA9A8628253717C938871B084D8AB48D80 (void);
// 0x00000439 System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringValue_mE4B6D84C62301EE664A48706F72A91583690A6FC (void);
// 0x0000043A System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscape(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringEscape_m62161ADC6931F29D6D6477EC6EA0C33ED68E3065 (void);
// 0x0000043B System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptions(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringByOptions_mB9F334C1CDC0D38156C588C17ADF3499E52A8378 (void);
// 0x0000043C System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimized(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringMinimized_mCDE83318D5992A162E60B45316CC28EDB29DF31C (void);
// 0x0000043D System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndented(System.ReadOnlySpan`1<System.Char>)
extern void Utf8JsonWriter_WriteStringIndented_mC739CAADEF3039DDEE8567FD158DF0B9C609E1EA (void);
// 0x0000043E System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeValue(System.ReadOnlySpan`1<System.Char>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeValue_m5F92419D0847CAF6993B1EB667B83A5DCF936007 (void);
// 0x0000043F System.Void System.Text.Json.Utf8JsonWriter::WriteStringValue(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringValue_mAC062B9CFDB5DFD4F1E55081AF5E390D0671616A (void);
// 0x00000440 System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscape(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringEscape_mB59D9F02536516EA8EF1FD9096CE40A2EA996451 (void);
// 0x00000441 System.Void System.Text.Json.Utf8JsonWriter::WriteStringByOptions(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringByOptions_m6FFDC7E75661DC2BBA64B77C53B8E262814AFD57 (void);
// 0x00000442 System.Void System.Text.Json.Utf8JsonWriter::WriteStringMinimized(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringMinimized_mC68E8AD0C2D230CD87ADAB875F7CF843D37EDD5A (void);
// 0x00000443 System.Void System.Text.Json.Utf8JsonWriter::WriteStringIndented(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteStringIndented_mE4AB77B76E3FB99A9836D4A32D8442273D0E3B41 (void);
// 0x00000444 System.Void System.Text.Json.Utf8JsonWriter::WriteStringEscapeValue(System.ReadOnlySpan`1<System.Byte>,System.Int32)
extern void Utf8JsonWriter_WriteStringEscapeValue_m10974C00896F712FB5766F448315918EA56BFA91 (void);
// 0x00000445 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsStringUnescaped(System.ReadOnlySpan`1<System.Byte>)
extern void Utf8JsonWriter_WriteNumberValueAsStringUnescaped_m5F0697B41CEABFEAB9EE4CCF8202C31BCCE0CD13 (void);
// 0x00000446 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.UInt32)
extern void Utf8JsonWriter_WriteNumberValue_m89BBC1D792A2DD977776AA582F1DB31A7A76E7B0 (void);
// 0x00000447 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValue(System.UInt64)
extern void Utf8JsonWriter_WriteNumberValue_m3075EC515D272E426FE6A49DF02814388AECFBD0 (void);
// 0x00000448 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueMinimized(System.UInt64)
extern void Utf8JsonWriter_WriteNumberValueMinimized_mE7D6AF009796F6D436D5688CA8AD185D2F9C2D70 (void);
// 0x00000449 System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueIndented(System.UInt64)
extern void Utf8JsonWriter_WriteNumberValueIndented_m8D4E40F1D48E202EE3964E001154CF75F36A5542 (void);
// 0x0000044A System.Void System.Text.Json.Utf8JsonWriter::WriteNumberValueAsString(System.UInt64)
extern void Utf8JsonWriter_WriteNumberValueAsString_m65853705FCD729A943F6F47CFE0851D8B067011F (void);
// 0x0000044B System.Void System.Text.Json.Utf8JsonWriter::.cctor()
extern void Utf8JsonWriter__cctor_m65152A2DCCF94AD3B43EA4A0CAB34CD9160F75CE (void);
// 0x0000044C System.Type System.Text.Json.Serialization.JsonConverterAttribute::get_ConverterType()
extern void JsonConverterAttribute_get_ConverterType_m4F859C91463CB5C3485CF230A347929B0DFB6923 (void);
// 0x0000044D System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.JsonConverterAttribute::CreateConverter(System.Type)
extern void JsonConverterAttribute_CreateConverter_mA104EBFE9E69E3EA640082F5D61F12761FDDF1FF (void);
// 0x0000044E System.Text.Json.Serialization.JsonIgnoreCondition System.Text.Json.Serialization.JsonIgnoreAttribute::get_Condition()
extern void JsonIgnoreAttribute_get_Condition_m352D83EB9F4BF8EAA3C61F69259CBC3E2FEE5C88 (void);
// 0x0000044F System.Text.Json.Serialization.JsonNumberHandling System.Text.Json.Serialization.JsonNumberHandlingAttribute::get_Handling()
extern void JsonNumberHandlingAttribute_get_Handling_mEB49A88D50DD9959865987E2A067F73D8E8960BB (void);
// 0x00000450 System.String System.Text.Json.Serialization.JsonPropertyNameAttribute::get_Name()
extern void JsonPropertyNameAttribute_get_Name_mC37BF00D1224BB51B5165DA09B92E48A090FB0E2 (void);
// 0x00000451 System.Void System.Text.Json.Serialization.ConverterList::.ctor(System.Text.Json.JsonSerializerOptions)
extern void ConverterList__ctor_m4E3C5A32A90624E0ED5A0DF62DDF8BF134F695E0 (void);
// 0x00000452 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.ConverterList::get_Item(System.Int32)
extern void ConverterList_get_Item_m79FDDF8D8C41749EC27D74A578AA5D376C99FCB9 (void);
// 0x00000453 System.Void System.Text.Json.Serialization.ConverterList::set_Item(System.Int32,System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_set_Item_mB3A908ACE6F9BE728544B4AA53DD70BDA1356A5E (void);
// 0x00000454 System.Int32 System.Text.Json.Serialization.ConverterList::get_Count()
extern void ConverterList_get_Count_mD6915CE1800C4C21C5B5ED83934570F3509D5FDE (void);
// 0x00000455 System.Boolean System.Text.Json.Serialization.ConverterList::get_IsReadOnly()
extern void ConverterList_get_IsReadOnly_mDD798CA9C6464F9846CE1FA15FE4B68FF9F37650 (void);
// 0x00000456 System.Void System.Text.Json.Serialization.ConverterList::Add(System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_Add_m980F6953ECE005778CE39AF01E80BA737D24C016 (void);
// 0x00000457 System.Void System.Text.Json.Serialization.ConverterList::Clear()
extern void ConverterList_Clear_m52C2EE6E20254E31312DD99B935290AD641DD0E2 (void);
// 0x00000458 System.Boolean System.Text.Json.Serialization.ConverterList::Contains(System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_Contains_m5BBF3F2DEB647C13064D4958948F4574C1214C70 (void);
// 0x00000459 System.Void System.Text.Json.Serialization.ConverterList::CopyTo(System.Text.Json.Serialization.JsonConverter[],System.Int32)
extern void ConverterList_CopyTo_mE79D8B1CCA4BAF4CB1BCFEF1EEBD61E7C1F5EBFB (void);
// 0x0000045A System.Collections.Generic.IEnumerator`1<System.Text.Json.Serialization.JsonConverter> System.Text.Json.Serialization.ConverterList::GetEnumerator()
extern void ConverterList_GetEnumerator_m363F6BC3F8246EB73EE94B101D403425EE91B2C5 (void);
// 0x0000045B System.Int32 System.Text.Json.Serialization.ConverterList::IndexOf(System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_IndexOf_m00E33929E63DBF007A48057D66BE5E91C7E2FD7D (void);
// 0x0000045C System.Void System.Text.Json.Serialization.ConverterList::Insert(System.Int32,System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_Insert_mF9607457DB86C593C5D6791318F02B52FE13928F (void);
// 0x0000045D System.Boolean System.Text.Json.Serialization.ConverterList::Remove(System.Text.Json.Serialization.JsonConverter)
extern void ConverterList_Remove_mF1668BE4C4D66A63642E6BCA2F3DD65137BBF014 (void);
// 0x0000045E System.Void System.Text.Json.Serialization.ConverterList::RemoveAt(System.Int32)
extern void ConverterList_RemoveAt_m5B3158281E9A5D8FC06FB8AE6175585EB25032C7 (void);
// 0x0000045F System.Collections.IEnumerator System.Text.Json.Serialization.ConverterList::System.Collections.IEnumerable.GetEnumerator()
extern void ConverterList_System_Collections_IEnumerable_GetEnumerator_m6E6DD8CC8B66665EBA5560079712AB72F11A9A7F (void);
// 0x00000460 System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetCompatibleGenericBaseClass(System.Type,System.Type)
extern void IEnumerableConverterFactoryHelpers_GetCompatibleGenericBaseClass_m533A7F2E411C4988EC4D744EA5925FA05DB20F9A (void);
// 0x00000461 System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetCompatibleGenericInterface(System.Type,System.Type)
extern void IEnumerableConverterFactoryHelpers_GetCompatibleGenericInterface_m8E23E177924E5C6D83FA669354E9CE6C977ABC41 (void);
// 0x00000462 System.Boolean System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::IsImmutableDictionaryType(System.Type)
extern void IEnumerableConverterFactoryHelpers_IsImmutableDictionaryType_m08B2157F3DEFDA39B118F37FCF14353F8A463C53 (void);
// 0x00000463 System.Boolean System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::IsImmutableEnumerableType(System.Type)
extern void IEnumerableConverterFactoryHelpers_IsImmutableEnumerableType_mDB655BAA8E2BDF78B243B848CB865ED245ACE3DD (void);
// 0x00000464 System.Reflection.MethodInfo System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetImmutableEnumerableCreateRangeMethod(System.Type,System.Type)
extern void IEnumerableConverterFactoryHelpers_GetImmutableEnumerableCreateRangeMethod_m821F4E086E5DA87DF7CD1A4B12E723E236B1870D (void);
// 0x00000465 System.Reflection.MethodInfo System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetImmutableDictionaryCreateRangeMethod(System.Type,System.Type)
extern void IEnumerableConverterFactoryHelpers_GetImmutableDictionaryCreateRangeMethod_m433C377D25F9CC52A0E9B13E4E2405F34E7F3943 (void);
// 0x00000466 System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetImmutableEnumerableConstructingType(System.Type)
extern void IEnumerableConverterFactoryHelpers_GetImmutableEnumerableConstructingType_mC9082C942B14AFBD0426E03155444F22353BE1E1 (void);
// 0x00000467 System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetImmutableDictionaryConstructingType(System.Type)
extern void IEnumerableConverterFactoryHelpers_GetImmutableDictionaryConstructingType_m4A915E05D75EC8E6BB552AE5DA5ED8FBD81F08AE (void);
// 0x00000468 System.Boolean System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::IsNonGenericStackOrQueue(System.Type)
extern void IEnumerableConverterFactoryHelpers_IsNonGenericStackOrQueue_m724A1A43FDADC65DC1A640DD140A55616D2542F9 (void);
// 0x00000469 System.Type System.Text.Json.Serialization.IEnumerableConverterFactoryHelpers::GetTypeIfExists(System.String)
extern void IEnumerableConverterFactoryHelpers_GetTypeIfExists_m9605BB4E0549221D4373EC8E201788B2F4D88462 (void);
// 0x0000046A System.Text.Json.ClassType System.Text.Json.Serialization.JsonCollectionConverter`2::get_ClassType()
// 0x0000046B System.Type System.Text.Json.Serialization.JsonCollectionConverter`2::get_ElementType()
// 0x0000046C System.Void System.Text.Json.Serialization.JsonCollectionConverter`2::.ctor()
// 0x0000046D System.Text.Json.ClassType System.Text.Json.Serialization.JsonDictionaryConverter`1::get_ClassType()
// 0x0000046E System.Boolean System.Text.Json.Serialization.JsonDictionaryConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000046F System.Void System.Text.Json.Serialization.JsonDictionaryConverter`1::.ctor()
// 0x00000470 System.Text.Json.ClassType System.Text.Json.Serialization.JsonObjectConverter`1::get_ClassType()
// 0x00000471 System.Type System.Text.Json.Serialization.JsonObjectConverter`1::get_ElementType()
// 0x00000472 System.Void System.Text.Json.Serialization.JsonObjectConverter`1::.ctor()
// 0x00000473 System.Void System.Text.Json.Serialization.JsonConverter::.ctor()
extern void JsonConverter__ctor_m23EFDEA2C29481891A2DA60D4E39F996475129EF (void);
// 0x00000474 System.Boolean System.Text.Json.Serialization.JsonConverter::CanConvert(System.Type)
// 0x00000475 System.Text.Json.ClassType System.Text.Json.Serialization.JsonConverter::get_ClassType()
// 0x00000476 System.Boolean System.Text.Json.Serialization.JsonConverter::get_CanUseDirectReadOrWrite()
extern void JsonConverter_get_CanUseDirectReadOrWrite_mE1D5C56BEAC0A084D137B109347864E236355287 (void);
// 0x00000477 System.Void System.Text.Json.Serialization.JsonConverter::set_CanUseDirectReadOrWrite(System.Boolean)
extern void JsonConverter_set_CanUseDirectReadOrWrite_m0E51E0526E0BD47E24A839266D241496EC091261 (void);
// 0x00000478 System.Boolean System.Text.Json.Serialization.JsonConverter::get_CanHaveIdMetadata()
extern void JsonConverter_get_CanHaveIdMetadata_m27650670CDB655B2030CDEEBF8E469D2F4D1AFE8 (void);
// 0x00000479 System.Boolean System.Text.Json.Serialization.JsonConverter::get_CanBePolymorphic()
extern void JsonConverter_get_CanBePolymorphic_m70362671690C01E809BC9121656CF387E1BABF78 (void);
// 0x0000047A System.Void System.Text.Json.Serialization.JsonConverter::set_CanBePolymorphic(System.Boolean)
extern void JsonConverter_set_CanBePolymorphic_m64C5B8CEE959D5087C91F323CB33AA690D78B840 (void);
// 0x0000047B System.Text.Json.JsonPropertyInfo System.Text.Json.Serialization.JsonConverter::CreateJsonPropertyInfo()
// 0x0000047C System.Text.Json.JsonParameterInfo System.Text.Json.Serialization.JsonConverter::CreateJsonParameterInfo()
// 0x0000047D System.Type System.Text.Json.Serialization.JsonConverter::get_ElementType()
// 0x0000047E System.Boolean System.Text.Json.Serialization.JsonConverter::get_IsValueType()
extern void JsonConverter_get_IsValueType_mD0A2E2F1397C567881231F647B24B9301A6665F2 (void);
// 0x0000047F System.Void System.Text.Json.Serialization.JsonConverter::set_IsValueType(System.Boolean)
extern void JsonConverter_set_IsValueType_mFF06DEF51DA822C23B4DD2A4B6B6D25000E69F10 (void);
// 0x00000480 System.Boolean System.Text.Json.Serialization.JsonConverter::get_IsInternalConverter()
extern void JsonConverter_get_IsInternalConverter_m3F6D0C0722D8FD57533189C2645030FB07A96AEE (void);
// 0x00000481 System.Void System.Text.Json.Serialization.JsonConverter::set_IsInternalConverter(System.Boolean)
extern void JsonConverter_set_IsInternalConverter_mB054A28BB915D42B82F816CA0E11C9CD57EB0000 (void);
// 0x00000482 System.Object System.Text.Json.Serialization.JsonConverter::ReadCoreAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x00000483 System.Type System.Text.Json.Serialization.JsonConverter::get_RuntimeType()
extern void JsonConverter_get_RuntimeType_m5EEBFDDE9072D9B99D767DFE36D5DC819753C181 (void);
// 0x00000484 System.Boolean System.Text.Json.Serialization.JsonConverter::ShouldFlush(System.Text.Json.Utf8JsonWriter,System.Text.Json.WriteStack&)
extern void JsonConverter_ShouldFlush_m66E1D9591D7D86B244C7FC4280FA3E7214425FA1 (void);
// 0x00000485 System.Type System.Text.Json.Serialization.JsonConverter::get_TypeToConvert()
// 0x00000486 System.Boolean System.Text.Json.Serialization.JsonConverter::TryReadAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,System.Object&)
// 0x00000487 System.Boolean System.Text.Json.Serialization.JsonConverter::TryWriteAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000488 System.Boolean System.Text.Json.Serialization.JsonConverter::WriteCoreAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000489 System.Void System.Text.Json.Serialization.JsonConverter::WriteWithQuotesAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000048A System.Boolean System.Text.Json.Serialization.JsonConverter::get_ConstructorIsParameterized()
extern void JsonConverter_get_ConstructorIsParameterized_mB2021F0E53B080BCABDA8420269391C3661B7174 (void);
// 0x0000048B System.Reflection.ConstructorInfo System.Text.Json.Serialization.JsonConverter::get_ConstructorInfo()
extern void JsonConverter_get_ConstructorInfo_m025ED1FC090F35D33CDABDA3CDB651B0DB96EA9F (void);
// 0x0000048C System.Void System.Text.Json.Serialization.JsonConverter::set_ConstructorInfo(System.Reflection.ConstructorInfo)
extern void JsonConverter_set_ConstructorInfo_mFBBC3F48DAA76C204E110C8F905E7EC1FF9F621B (void);
// 0x0000048D System.Void System.Text.Json.Serialization.JsonConverter::Initialize(System.Text.Json.JsonSerializerOptions)
extern void JsonConverter_Initialize_mE4E6E9CAC22D9DF6CC40EDAAE657AFC8B61860C4 (void);
// 0x0000048E System.Void System.Text.Json.Serialization.JsonConverter::CreateInstanceForReferenceResolver(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
extern void JsonConverter_CreateInstanceForReferenceResolver_m9470CF210E5182867CFBB046E7A024905A473F98 (void);
// 0x0000048F System.Boolean System.Text.Json.Serialization.JsonConverter::SingleValueReadWithReadAhead(System.Text.Json.ClassType,System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void JsonConverter_SingleValueReadWithReadAhead_mBF2F841A38AD31B2C37E924F836E769C4B8C8EE9 (void);
// 0x00000490 System.Boolean System.Text.Json.Serialization.JsonConverter::DoSingleValueReadWithReadAhead(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
extern void JsonConverter_DoSingleValueReadWithReadAhead_mF6825E41E994CDC4001A8D711F5E80EAFBD5CA67 (void);
// 0x00000491 System.Void System.Text.Json.Serialization.JsonConverterFactory::.ctor()
extern void JsonConverterFactory__ctor_m411B07ED997EEDA8B37628F05D4F8DA392BB1956 (void);
// 0x00000492 System.Text.Json.ClassType System.Text.Json.Serialization.JsonConverterFactory::get_ClassType()
extern void JsonConverterFactory_get_ClassType_m4E2428FF151D03A6D7C4B0564F8A957B04275E68 (void);
// 0x00000493 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.JsonConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
// 0x00000494 System.Text.Json.JsonPropertyInfo System.Text.Json.Serialization.JsonConverterFactory::CreateJsonPropertyInfo()
extern void JsonConverterFactory_CreateJsonPropertyInfo_m4A768485F2154AD0D83B102D05C69EDC482B4703 (void);
// 0x00000495 System.Text.Json.JsonParameterInfo System.Text.Json.Serialization.JsonConverterFactory::CreateJsonParameterInfo()
extern void JsonConverterFactory_CreateJsonParameterInfo_mC8B045F4D21D063AA97C9492F7E0D71D70EC3C0D (void);
// 0x00000496 System.Type System.Text.Json.Serialization.JsonConverterFactory::get_ElementType()
extern void JsonConverterFactory_get_ElementType_m56332CF4413D09D8788C4713A8CE4CEC888C3249 (void);
// 0x00000497 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.JsonConverterFactory::GetConverterInternal(System.Type,System.Text.Json.JsonSerializerOptions)
extern void JsonConverterFactory_GetConverterInternal_m56BD0E54C66E7862785D9D2F92615DC179929EAA (void);
// 0x00000498 System.Object System.Text.Json.Serialization.JsonConverterFactory::ReadCoreAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
extern void JsonConverterFactory_ReadCoreAsObject_m19F79BFD1D59A655393D6423723DEF3ADA3E2E1E (void);
// 0x00000499 System.Boolean System.Text.Json.Serialization.JsonConverterFactory::TryReadAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,System.Object&)
extern void JsonConverterFactory_TryReadAsObject_mF2E8D9B0B3033B868331F6287F6FBCC8DEC04E8E (void);
// 0x0000049A System.Boolean System.Text.Json.Serialization.JsonConverterFactory::TryWriteAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void JsonConverterFactory_TryWriteAsObject_m7CD0F76414173623EBA5FBA8BE8426F53468AA90 (void);
// 0x0000049B System.Type System.Text.Json.Serialization.JsonConverterFactory::get_TypeToConvert()
extern void JsonConverterFactory_get_TypeToConvert_mEBC0D6524B9C810A51A935D00E6EBD4BC71320D0 (void);
// 0x0000049C System.Boolean System.Text.Json.Serialization.JsonConverterFactory::WriteCoreAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void JsonConverterFactory_WriteCoreAsObject_m7EBC5DED957FAE1C4AE026076BB956E2C17AB2EF (void);
// 0x0000049D System.Void System.Text.Json.Serialization.JsonConverterFactory::WriteWithQuotesAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void JsonConverterFactory_WriteWithQuotesAsObject_m4F587F5EDDF4E25CEFF97328025105FE7AE8CEBD (void);
// 0x0000049E System.Object System.Text.Json.Serialization.JsonConverter`1::ReadCoreAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x0000049F T System.Text.Json.Serialization.JsonConverter`1::ReadCore(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x000004A0 System.Boolean System.Text.Json.Serialization.JsonConverter`1::WriteCoreAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004A1 System.Boolean System.Text.Json.Serialization.JsonConverter`1::WriteCore(System.Text.Json.Utf8JsonWriter,T&,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004A2 System.Void System.Text.Json.Serialization.JsonConverter`1::.ctor()
// 0x000004A3 System.Boolean System.Text.Json.Serialization.JsonConverter`1::CanConvert(System.Type)
// 0x000004A4 System.Text.Json.ClassType System.Text.Json.Serialization.JsonConverter`1::get_ClassType()
// 0x000004A5 System.Text.Json.JsonPropertyInfo System.Text.Json.Serialization.JsonConverter`1::CreateJsonPropertyInfo()
// 0x000004A6 System.Text.Json.JsonParameterInfo System.Text.Json.Serialization.JsonConverter`1::CreateJsonParameterInfo()
// 0x000004A7 System.Type System.Text.Json.Serialization.JsonConverter`1::get_ElementType()
// 0x000004A8 System.Boolean System.Text.Json.Serialization.JsonConverter`1::get_HandleNull()
// 0x000004A9 System.Boolean System.Text.Json.Serialization.JsonConverter`1::get_HandleNullOnRead()
// 0x000004AA System.Void System.Text.Json.Serialization.JsonConverter`1::set_HandleNullOnRead(System.Boolean)
// 0x000004AB System.Boolean System.Text.Json.Serialization.JsonConverter`1::get_HandleNullOnWrite()
// 0x000004AC System.Void System.Text.Json.Serialization.JsonConverter`1::set_HandleNullOnWrite(System.Boolean)
// 0x000004AD System.Boolean System.Text.Json.Serialization.JsonConverter`1::get_CanBeNull()
// 0x000004AE System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryWriteAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004AF System.Boolean System.Text.Json.Serialization.JsonConverter`1::OnTryWrite(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004B0 System.Boolean System.Text.Json.Serialization.JsonConverter`1::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,T&)
// 0x000004B1 T System.Text.Json.Serialization.JsonConverter`1::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x000004B2 System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,T&)
// 0x000004B3 System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryReadAsObject(System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,System.Object&)
// 0x000004B4 System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryWrite(System.Text.Json.Utf8JsonWriter,T&,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004B5 System.Boolean System.Text.Json.Serialization.JsonConverter`1::TryWriteDataExtensionProperty(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004B6 System.Type System.Text.Json.Serialization.JsonConverter`1::get_TypeToConvert()
// 0x000004B7 System.Void System.Text.Json.Serialization.JsonConverter`1::VerifyRead(System.Text.Json.JsonTokenType,System.Int32,System.Int64,System.Boolean,System.Text.Json.Utf8JsonReader&)
// 0x000004B8 System.Void System.Text.Json.Serialization.JsonConverter`1::VerifyWrite(System.Int32,System.Text.Json.Utf8JsonWriter)
// 0x000004B9 System.Void System.Text.Json.Serialization.JsonConverter`1::Write(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions)
// 0x000004BA T System.Text.Json.Serialization.JsonConverter`1::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
// 0x000004BB System.Void System.Text.Json.Serialization.JsonConverter`1::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004BC System.Void System.Text.Json.Serialization.JsonConverter`1::WriteWithQuotesAsObject(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004BD T System.Text.Json.Serialization.JsonConverter`1::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
// 0x000004BE System.Void System.Text.Json.Serialization.JsonConverter`1::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.Serialization.JsonNumberHandling)
// 0x000004BF T System.Text.Json.Serialization.JsonResumableConverter`1::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x000004C0 System.Void System.Text.Json.Serialization.JsonResumableConverter`1::Write(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions)
// 0x000004C1 System.Boolean System.Text.Json.Serialization.JsonResumableConverter`1::get_HandleNull()
// 0x000004C2 System.Void System.Text.Json.Serialization.JsonResumableConverter`1::.ctor()
// 0x000004C3 System.Text.Json.Serialization.ReferenceResolver System.Text.Json.Serialization.PreserveReferenceHandler::CreateResolver()
extern void PreserveReferenceHandler_CreateResolver_m61A6B3CD749479AE6FAC893ED47B60D93483637A (void);
// 0x000004C4 System.Text.Json.Serialization.ReferenceResolver System.Text.Json.Serialization.PreserveReferenceHandler::CreateResolver(System.Boolean)
extern void PreserveReferenceHandler_CreateResolver_mAFC8110F8A4B135C5247E51560121FAFFFE5C611 (void);
// 0x000004C5 System.Void System.Text.Json.Serialization.PreserveReferenceHandler::.ctor()
extern void PreserveReferenceHandler__ctor_m7BDF9B35F7B769D3A6CB136767C82D4B9D38F3C5 (void);
// 0x000004C6 System.Void System.Text.Json.Serialization.PreserveReferenceResolver::.ctor(System.Boolean)
extern void PreserveReferenceResolver__ctor_m49E87B8658136E0FD110E906BF1C29F6D3C8B9B1 (void);
// 0x000004C7 System.Void System.Text.Json.Serialization.PreserveReferenceResolver::AddReference(System.String,System.Object)
extern void PreserveReferenceResolver_AddReference_m3DF7BEEE19A9EB216AD4B189FE8AA49782315DA6 (void);
// 0x000004C8 System.String System.Text.Json.Serialization.PreserveReferenceResolver::GetReference(System.Object,System.Boolean&)
extern void PreserveReferenceResolver_GetReference_m1658DC64E6481408E2FFE3CA51C475E0E1345EF3 (void);
// 0x000004C9 System.Object System.Text.Json.Serialization.PreserveReferenceResolver::ResolveReference(System.String)
extern void PreserveReferenceResolver_ResolveReference_mF998D0F7D8D4EA8062423B586318BE5DB1A53AFC (void);
// 0x000004CA System.Text.Json.Serialization.ReferenceResolver System.Text.Json.Serialization.ReferenceHandler::CreateResolver()
// 0x000004CB System.Text.Json.Serialization.ReferenceResolver System.Text.Json.Serialization.ReferenceHandler::CreateResolver(System.Boolean)
extern void ReferenceHandler_CreateResolver_m0F2A28E295E7D943D7D437AECF26A6B14C6F81FB (void);
// 0x000004CC System.Void System.Text.Json.Serialization.ReferenceHandler::.ctor()
extern void ReferenceHandler__ctor_mE34153A3037B9E3F9E724C1C7E745ECE6DCEE20E (void);
// 0x000004CD System.Void System.Text.Json.Serialization.ReferenceHandler::.cctor()
extern void ReferenceHandler__cctor_m96BFCF6E4DBCDCA2F2DEC6CC24B033DB1EE68847 (void);
// 0x000004CE System.Void System.Text.Json.Serialization.ReferenceResolver::AddReference(System.String,System.Object)
// 0x000004CF System.String System.Text.Json.Serialization.ReferenceResolver::GetReference(System.Object,System.Boolean&)
// 0x000004D0 System.Object System.Text.Json.Serialization.ReferenceResolver::ResolveReference(System.String)
// 0x000004D1 System.Void System.Text.Json.Serialization.ReferenceResolver::.ctor()
extern void ReferenceResolver__ctor_m0424148914AB26FF4CCA0EC9E12DDDCABDA9E28D (void);
// 0x000004D2 System.Text.Json.JsonClassInfo/ConstructorDelegate System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateConstructor(System.Type)
extern void ReflectionEmitMemberAccessor_CreateConstructor_m221DFDF2407A6C1CCF2ED82D823FE121920E9917 (void);
// 0x000004D3 System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`1<T> System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo)
// 0x000004D4 System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo)
extern void ReflectionEmitMemberAccessor_CreateParameterizedConstructor_m7920618ADC17968F4F28966E1FE092E825095FD8 (void);
// 0x000004D5 System.Text.Json.JsonClassInfo/ParameterizedConstructorDelegate`5<T,TArg0,TArg1,TArg2,TArg3> System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo)
// 0x000004D6 System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateParameterizedConstructor(System.Reflection.ConstructorInfo,System.Type,System.Type,System.Type,System.Type)
extern void ReflectionEmitMemberAccessor_CreateParameterizedConstructor_m5A960715365178AAF387F753417C7FDD328DEE07 (void);
// 0x000004D7 System.Action`2<TCollection,System.Object> System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateAddMethodDelegate()
// 0x000004D8 System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateAddMethodDelegate(System.Type)
extern void ReflectionEmitMemberAccessor_CreateAddMethodDelegate_m2B0F4B22C1D2C091CDD9D8F81A3AF90AE10324B9 (void);
// 0x000004D9 System.Func`2<System.Collections.Generic.IEnumerable`1<TElement>,TCollection> System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateImmutableEnumerableCreateRangeDelegate()
// 0x000004DA System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateImmutableEnumerableCreateRangeDelegate(System.Type,System.Type,System.Type)
extern void ReflectionEmitMemberAccessor_CreateImmutableEnumerableCreateRangeDelegate_mEFB4815C703391571D64424D2459EFDBA7634277 (void);
// 0x000004DB System.Func`2<System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,TElement>>,TCollection> System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateImmutableDictionaryCreateRangeDelegate()
// 0x000004DC System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateImmutableDictionaryCreateRangeDelegate(System.Type,System.Type,System.Type)
extern void ReflectionEmitMemberAccessor_CreateImmutableDictionaryCreateRangeDelegate_m288E3B2E8DB0D3FA4AD1503F1A44DE93256C093F (void);
// 0x000004DD System.Func`2<System.Object,TProperty> System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreatePropertyGetter(System.Reflection.PropertyInfo)
// 0x000004DE System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreatePropertyGetter(System.Reflection.PropertyInfo,System.Type)
extern void ReflectionEmitMemberAccessor_CreatePropertyGetter_m7F674907D6D359360B0BD659533368035C45B21B (void);
// 0x000004DF System.Action`2<System.Object,TProperty> System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreatePropertySetter(System.Reflection.PropertyInfo)
// 0x000004E0 System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreatePropertySetter(System.Reflection.PropertyInfo,System.Type)
extern void ReflectionEmitMemberAccessor_CreatePropertySetter_mA11A734C19B083FA596E2DC9BC07537D52CD93CE (void);
// 0x000004E1 System.Func`2<System.Object,TProperty> System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateFieldGetter(System.Reflection.FieldInfo)
// 0x000004E2 System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateFieldGetter(System.Reflection.FieldInfo,System.Type)
extern void ReflectionEmitMemberAccessor_CreateFieldGetter_mE912BAADEBC2AEA8B3B7C5E40105971CC4FA7F99 (void);
// 0x000004E3 System.Action`2<System.Object,TProperty> System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateFieldSetter(System.Reflection.FieldInfo)
// 0x000004E4 System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateFieldSetter(System.Reflection.FieldInfo,System.Type)
extern void ReflectionEmitMemberAccessor_CreateFieldSetter_mA9CC0A624BFEB1288262AF266B9024C0CD1A85A8 (void);
// 0x000004E5 System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateGetterMethod(System.String,System.Type)
extern void ReflectionEmitMemberAccessor_CreateGetterMethod_mF4677AF8926517787BC70414DC546A932B6E91DF (void);
// 0x000004E6 System.Reflection.Emit.DynamicMethod System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateSetterMethod(System.String,System.Type)
extern void ReflectionEmitMemberAccessor_CreateSetterMethod_m88900BFD67C6F3A6E693AA79CC8627C6A7A2C9E9 (void);
// 0x000004E7 T System.Text.Json.Serialization.ReflectionEmitMemberAccessor::CreateDelegate(System.Reflection.Emit.DynamicMethod)
// 0x000004E8 System.Void System.Text.Json.Serialization.ReflectionEmitMemberAccessor::.ctor()
extern void ReflectionEmitMemberAccessor__ctor_mEEDBC30DA17766C88F940C0FF959D62EDE6625F4 (void);
// 0x000004E9 System.Boolean System.Text.Json.Serialization.Converters.ArrayConverter`2::get_CanHaveIdMetadata()
// 0x000004EA System.Void System.Text.Json.Serialization.Converters.ArrayConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x000004EB System.Void System.Text.Json.Serialization.Converters.ArrayConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x000004EC System.Void System.Text.Json.Serialization.Converters.ArrayConverter`2::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x000004ED System.Boolean System.Text.Json.Serialization.Converters.ArrayConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004EE System.Void System.Text.Json.Serialization.Converters.ArrayConverter`2::.ctor()
// 0x000004EF System.Void System.Text.Json.Serialization.Converters.ConcurrentQueueOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x000004F0 System.Void System.Text.Json.Serialization.Converters.ConcurrentQueueOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x000004F1 System.Boolean System.Text.Json.Serialization.Converters.ConcurrentQueueOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004F2 System.Void System.Text.Json.Serialization.Converters.ConcurrentQueueOfTConverter`2::.ctor()
// 0x000004F3 System.Void System.Text.Json.Serialization.Converters.ConcurrentStackOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x000004F4 System.Void System.Text.Json.Serialization.Converters.ConcurrentStackOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x000004F5 System.Boolean System.Text.Json.Serialization.Converters.ConcurrentStackOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004F6 System.Void System.Text.Json.Serialization.Converters.ConcurrentStackOfTConverter`2::.ctor()
// 0x000004F7 System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x000004F8 System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x000004F9 System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x000004FA System.Type System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::get_ElementType()
// 0x000004FB System.Text.Json.Serialization.JsonConverter`1<TValue> System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::GetValueConverter(System.Text.Json.JsonClassInfo)
// 0x000004FC System.Text.Json.Serialization.JsonConverter`1<TKey> System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::GetKeyConverter(System.Type,System.Text.Json.JsonSerializerOptions)
// 0x000004FD System.Boolean System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,TCollection&)
// 0x000004FE System.Boolean System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::OnTryWrite(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000004FF System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::CreateInstanceForReferenceResolver(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000500 System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::.ctor()
// 0x00000501 System.Void System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::.cctor()
// 0x00000502 TKey System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3::<OnTryRead>g__ReadDictionaryKey|12_0(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.Serialization.Converters.DictionaryDefaultConverter`3/<>c__DisplayClass12_0<TCollection,TKey,TValue>&)
// 0x00000503 System.Void System.Text.Json.Serialization.Converters.DictionaryOfTKeyTValueConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x00000504 System.Void System.Text.Json.Serialization.Converters.DictionaryOfTKeyTValueConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x00000505 System.Boolean System.Text.Json.Serialization.Converters.DictionaryOfTKeyTValueConverter`3::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000506 System.Void System.Text.Json.Serialization.Converters.DictionaryOfTKeyTValueConverter`3::.ctor()
// 0x00000507 System.Void System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000508 System.Void System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000509 System.Boolean System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000050A System.Type System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::get_RuntimeType()
// 0x0000050B System.Void System.Text.Json.Serialization.Converters.ICollectionOfTConverter`2::.ctor()
// 0x0000050C System.Void System.Text.Json.Serialization.Converters.IDictionaryConverter`1::Add(System.String,System.Object& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x0000050D System.Text.Json.Serialization.JsonConverter`1<System.Object> System.Text.Json.Serialization.Converters.IDictionaryConverter`1::GetObjectKeyConverter(System.Text.Json.JsonSerializerOptions)
// 0x0000050E System.Void System.Text.Json.Serialization.Converters.IDictionaryConverter`1::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x0000050F System.Boolean System.Text.Json.Serialization.Converters.IDictionaryConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000510 System.Type System.Text.Json.Serialization.Converters.IDictionaryConverter`1::get_RuntimeType()
// 0x00000511 System.Void System.Text.Json.Serialization.Converters.IDictionaryConverter`1::.ctor()
// 0x00000512 System.Void System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x00000513 System.Void System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x00000514 System.Boolean System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000515 System.Type System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::get_RuntimeType()
// 0x00000516 System.Void System.Text.Json.Serialization.Converters.IDictionaryOfTKeyTValueConverter`3::.ctor()
// 0x00000517 System.Void System.Text.Json.Serialization.Converters.IEnumerableConverter`1::Add(System.Object& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000518 System.Void System.Text.Json.Serialization.Converters.IEnumerableConverter`1::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000519 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000051A System.Type System.Text.Json.Serialization.Converters.IEnumerableConverter`1::get_RuntimeType()
// 0x0000051B System.Void System.Text.Json.Serialization.Converters.IEnumerableConverter`1::.ctor()
// 0x0000051C System.Boolean System.Text.Json.Serialization.Converters.IEnumerableConverterFactory::CanConvert(System.Type)
extern void IEnumerableConverterFactory_CanConvert_m231374E9CF8BAB67CB2765097519B3D4447867AF (void);
// 0x0000051D System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.IEnumerableConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void IEnumerableConverterFactory_CreateConverter_mDDBEE8EA353EBF17168729A4B66455C4FB36B01B (void);
// 0x0000051E System.Void System.Text.Json.Serialization.Converters.IEnumerableConverterFactory::.ctor()
extern void IEnumerableConverterFactory__ctor_mDABCD1B66296E504F4DC3A8E1149D5125C3AA23C (void);
// 0x0000051F System.Void System.Text.Json.Serialization.Converters.IEnumerableConverterFactory::.cctor()
extern void IEnumerableConverterFactory__cctor_m721A13DC766EE5A00B5DE10566AF44E3FBA7E843 (void);
// 0x00000520 System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000521 System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000522 System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000523 System.Text.Json.Serialization.JsonConverter`1<TElement> System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::GetElementConverter(System.Text.Json.JsonClassInfo)
// 0x00000524 System.Text.Json.Serialization.JsonConverter`1<TElement> System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::GetElementConverter(System.Text.Json.WriteStack&)
// 0x00000525 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,TCollection&)
// 0x00000526 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::OnTryWrite(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000527 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000528 System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::CreateInstanceForReferenceResolver(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000529 System.Void System.Text.Json.Serialization.Converters.IEnumerableDefaultConverter`2::.ctor()
// 0x0000052A System.Void System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x0000052B System.Void System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000052C System.Boolean System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000052D System.Type System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::get_RuntimeType()
// 0x0000052E System.Void System.Text.Json.Serialization.Converters.IEnumerableOfTConverter`2::.ctor()
// 0x0000052F System.Void System.Text.Json.Serialization.Converters.IEnumerableWithAddMethodConverter`1::Add(System.Object& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000530 System.Void System.Text.Json.Serialization.Converters.IEnumerableWithAddMethodConverter`1::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000531 System.Boolean System.Text.Json.Serialization.Converters.IEnumerableWithAddMethodConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000532 System.Void System.Text.Json.Serialization.Converters.IEnumerableWithAddMethodConverter`1::.ctor()
// 0x00000533 System.Void System.Text.Json.Serialization.Converters.IListConverter`1::Add(System.Object& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000534 System.Void System.Text.Json.Serialization.Converters.IListConverter`1::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000535 System.Boolean System.Text.Json.Serialization.Converters.IListConverter`1::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000536 System.Type System.Text.Json.Serialization.Converters.IListConverter`1::get_RuntimeType()
// 0x00000537 System.Void System.Text.Json.Serialization.Converters.IListConverter`1::.ctor()
// 0x00000538 System.Void System.Text.Json.Serialization.Converters.IListOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000539 System.Void System.Text.Json.Serialization.Converters.IListOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000053A System.Boolean System.Text.Json.Serialization.Converters.IListOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000053B System.Type System.Text.Json.Serialization.Converters.IListOfTConverter`2::get_RuntimeType()
// 0x0000053C System.Void System.Text.Json.Serialization.Converters.IListOfTConverter`2::.ctor()
// 0x0000053D System.Void System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x0000053E System.Boolean System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::get_CanHaveIdMetadata()
// 0x0000053F System.Void System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x00000540 System.Void System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000541 System.Boolean System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000542 System.Void System.Text.Json.Serialization.Converters.ImmutableDictionaryOfTKeyTValueConverter`3::.ctor()
// 0x00000543 System.Void System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000544 System.Boolean System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::get_CanHaveIdMetadata()
// 0x00000545 System.Void System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000546 System.Void System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::ConvertCollection(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000547 System.Boolean System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000548 System.Void System.Text.Json.Serialization.Converters.ImmutableEnumerableOfTConverter`2::.ctor()
// 0x00000549 System.Void System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::Add(TKey,TValue& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&)
// 0x0000054A System.Void System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&)
// 0x0000054B System.Boolean System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000054C System.Type System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::get_RuntimeType()
// 0x0000054D System.Void System.Text.Json.Serialization.Converters.IReadOnlyDictionaryOfTKeyTValueConverter`3::.ctor()
// 0x0000054E System.Void System.Text.Json.Serialization.Converters.ISetOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x0000054F System.Void System.Text.Json.Serialization.Converters.ISetOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000550 System.Boolean System.Text.Json.Serialization.Converters.ISetOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000551 System.Type System.Text.Json.Serialization.Converters.ISetOfTConverter`2::get_RuntimeType()
// 0x00000552 System.Void System.Text.Json.Serialization.Converters.ISetOfTConverter`2::.ctor()
// 0x00000553 System.Void System.Text.Json.Serialization.Converters.ListOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000554 System.Void System.Text.Json.Serialization.Converters.ListOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000555 System.Boolean System.Text.Json.Serialization.Converters.ListOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x00000556 System.Void System.Text.Json.Serialization.Converters.ListOfTConverter`2::.ctor()
// 0x00000557 System.Void System.Text.Json.Serialization.Converters.QueueOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x00000558 System.Void System.Text.Json.Serialization.Converters.QueueOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000559 System.Boolean System.Text.Json.Serialization.Converters.QueueOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000055A System.Void System.Text.Json.Serialization.Converters.QueueOfTConverter`2::.ctor()
// 0x0000055B System.Void System.Text.Json.Serialization.Converters.StackOfTConverter`2::Add(TElement& modreq(System.Runtime.InteropServices.InAttribute),System.Text.Json.ReadStack&)
// 0x0000055C System.Void System.Text.Json.Serialization.Converters.StackOfTConverter`2::CreateCollection(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x0000055D System.Boolean System.Text.Json.Serialization.Converters.StackOfTConverter`2::OnWriteResume(System.Text.Json.Utf8JsonWriter,TCollection,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000055E System.Void System.Text.Json.Serialization.Converters.StackOfTConverter`2::.ctor()
// 0x0000055F System.Void System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::Initialize(System.Text.Json.JsonSerializerOptions)
// 0x00000560 System.Boolean System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::TryLookupConstructorParameter(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.JsonParameterInfo&)
// 0x00000561 System.Void System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::EndRead(System.Text.Json.ReadStack&)
// 0x00000562 System.Boolean System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::FoundKeyProperty(System.String,System.Boolean)
// 0x00000563 System.Boolean System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::FoundValueProperty(System.String,System.Boolean)
// 0x00000564 System.Void System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::.ctor()
// 0x00000565 System.Void System.Text.Json.Serialization.Converters.KeyValuePairConverter`2::.cctor()
// 0x00000566 System.Boolean System.Text.Json.Serialization.Converters.ObjectConverterFactory::CanConvert(System.Type)
extern void ObjectConverterFactory_CanConvert_mB99DFBE073030B840D2EEC29BB93D030F0B3BE85 (void);
// 0x00000567 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.ObjectConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverterFactory_CreateConverter_mED2E71E2B9CC41698416C44DABAAE5C0F2D86035 (void);
// 0x00000568 System.Boolean System.Text.Json.Serialization.Converters.ObjectConverterFactory::IsKeyValuePair(System.Type)
extern void ObjectConverterFactory_IsKeyValuePair_mD31CC5779AA7290D8BCD0370AC17627E7A3188FD (void);
// 0x00000569 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.ObjectConverterFactory::CreateKeyValuePairConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverterFactory_CreateKeyValuePairConverter_m83BA2C182434B64E5B9ED7CF562DC6B9A65B2695 (void);
// 0x0000056A System.Reflection.ConstructorInfo System.Text.Json.Serialization.Converters.ObjectConverterFactory::GetDeserializationConstructor(System.Type)
extern void ObjectConverterFactory_GetDeserializationConstructor_m5ACBE28777CD74A128A057353CE7B4FB50519D88 (void);
// 0x0000056B System.Void System.Text.Json.Serialization.Converters.ObjectConverterFactory::.ctor()
extern void ObjectConverterFactory__ctor_m7435CDB6BC1739F0DB6034CCFCD7427978BB540A (void);
// 0x0000056C System.Boolean System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,T&)
// 0x0000056D System.Boolean System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::OnTryWrite(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x0000056E System.Void System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::ReadPropertyValue(System.Object,System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonPropertyInfo,System.Boolean)
// 0x0000056F System.Boolean System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::ReadAheadPropertyValue(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonPropertyInfo)
// 0x00000570 System.Void System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::CreateInstanceForReferenceResolver(System.Text.Json.Utf8JsonReader&,System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000571 System.Void System.Text.Json.Serialization.Converters.ObjectDefaultConverter`1::.ctor()
// 0x00000572 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::OnTryRead(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions,System.Text.Json.ReadStack&,T&)
// 0x00000573 System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::InitializeConstructorArgumentCaches(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000574 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::ReadAndCacheConstructorArgument(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo)
// 0x00000575 System.Object System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::CreateObject(System.Text.Json.ReadStackFrame&)
// 0x00000576 System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::ReadConstructorArguments(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions)
// 0x00000577 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::ReadConstructorArgumentsWithContinuation(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions)
// 0x00000578 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::HandleConstructorArgumentWithContinuation(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo)
// 0x00000579 System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::HandlePropertyWithContinuation(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonPropertyInfo)
// 0x0000057A System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::BeginRead(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions)
// 0x0000057B System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::EndRead(System.Text.Json.ReadStack&)
// 0x0000057C System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::TryLookupConstructorParameter(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonSerializerOptions,System.Text.Json.JsonParameterInfo&)
// 0x0000057D System.Boolean System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::get_ConstructorIsParameterized()
// 0x0000057E System.Void System.Text.Json.Serialization.Converters.ObjectWithParameterizedConstructorConverter`1::.ctor()
// 0x0000057F System.Boolean System.Text.Json.Serialization.Converters.LargeObjectWithParameterizedConstructorConverter`1::ReadAndCacheConstructorArgument(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo)
// 0x00000580 System.Object System.Text.Json.Serialization.Converters.LargeObjectWithParameterizedConstructorConverter`1::CreateObject(System.Text.Json.ReadStackFrame&)
// 0x00000581 System.Void System.Text.Json.Serialization.Converters.LargeObjectWithParameterizedConstructorConverter`1::InitializeConstructorArgumentCaches(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000582 System.Void System.Text.Json.Serialization.Converters.LargeObjectWithParameterizedConstructorConverter`1::.ctor()
// 0x00000583 System.Object System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::CreateObject(System.Text.Json.ReadStackFrame&)
// 0x00000584 System.Boolean System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::ReadAndCacheConstructorArgument(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo)
// 0x00000585 System.Boolean System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::TryRead(System.Text.Json.ReadStack&,System.Text.Json.Utf8JsonReader&,System.Text.Json.JsonParameterInfo,TArg&)
// 0x00000586 System.Void System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::InitializeConstructorArgumentCaches(System.Text.Json.ReadStack&,System.Text.Json.JsonSerializerOptions)
// 0x00000587 System.Void System.Text.Json.Serialization.Converters.SmallObjectWithParameterizedConstructorConverter`5::.ctor()
// 0x00000588 System.Boolean System.Text.Json.Serialization.Converters.BooleanConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void BooleanConverter_Read_mA060CFE94A3EDF88396699A47FBEA35D745EE6F6 (void);
// 0x00000589 System.Void System.Text.Json.Serialization.Converters.BooleanConverter::Write(System.Text.Json.Utf8JsonWriter,System.Boolean,System.Text.Json.JsonSerializerOptions)
extern void BooleanConverter_Write_mEF6EB096A12DC91C0A1E8E191C8E85CE30323B1C (void);
// 0x0000058A System.Boolean System.Text.Json.Serialization.Converters.BooleanConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void BooleanConverter_ReadWithQuotes_mAA5D212236BD74C3EABB2061AC413892A17F1D98 (void);
// 0x0000058B System.Void System.Text.Json.Serialization.Converters.BooleanConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Boolean,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void BooleanConverter_WriteWithQuotes_mB078C7BC7C8D76413F4AC2B03C0AA9F8A98AEF9A (void);
// 0x0000058C System.Void System.Text.Json.Serialization.Converters.BooleanConverter::.ctor()
extern void BooleanConverter__ctor_m73520293AA4F7B4AA13B4996422D06E9FC59219F (void);
// 0x0000058D System.Byte[] System.Text.Json.Serialization.Converters.ByteArrayConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void ByteArrayConverter_Read_mC0700F13A826469FFF3EF807A6ED2502C9111DE6 (void);
// 0x0000058E System.Void System.Text.Json.Serialization.Converters.ByteArrayConverter::Write(System.Text.Json.Utf8JsonWriter,System.Byte[],System.Text.Json.JsonSerializerOptions)
extern void ByteArrayConverter_Write_m65263ACC309C67044C0CBFA8A650928B3986CD0B (void);
// 0x0000058F System.Void System.Text.Json.Serialization.Converters.ByteArrayConverter::.ctor()
extern void ByteArrayConverter__ctor_m53090E4CF9ECB8DE38281AF72B1EF8D7FE4964D8 (void);
// 0x00000590 System.Void System.Text.Json.Serialization.Converters.ByteConverter::.ctor()
extern void ByteConverter__ctor_mA55101DFFF8A5C93C7A499A54244939462B8B3B3 (void);
// 0x00000591 System.Byte System.Text.Json.Serialization.Converters.ByteConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void ByteConverter_Read_mDC3E0CBCB4890A61CEF8C497965CFF8592A50F9A (void);
// 0x00000592 System.Void System.Text.Json.Serialization.Converters.ByteConverter::Write(System.Text.Json.Utf8JsonWriter,System.Byte,System.Text.Json.JsonSerializerOptions)
extern void ByteConverter_Write_m19B75553D83C5290DD9E9F12BEC96C1D6BA2AB55 (void);
// 0x00000593 System.Byte System.Text.Json.Serialization.Converters.ByteConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void ByteConverter_ReadWithQuotes_m5A34F4540B33EF89BBD2FD49A30F71389AE214D1 (void);
// 0x00000594 System.Void System.Text.Json.Serialization.Converters.ByteConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Byte,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void ByteConverter_WriteWithQuotes_m6B063B136D27B744C1A4DD215F0C841B269976E0 (void);
// 0x00000595 System.Byte System.Text.Json.Serialization.Converters.ByteConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void ByteConverter_ReadNumberWithCustomHandling_mB99BC6EFD402AE1D9B2C7B842115D0427C59CAAA (void);
// 0x00000596 System.Void System.Text.Json.Serialization.Converters.ByteConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Byte,System.Text.Json.Serialization.JsonNumberHandling)
extern void ByteConverter_WriteNumberWithCustomHandling_m02A08FD12E7B89FA2F7E1B63382A2E61375399D4 (void);
// 0x00000597 System.Char System.Text.Json.Serialization.Converters.CharConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void CharConverter_Read_m7AAEF695EE3A245DE05FAE6D134A45FCC82DCAA0 (void);
// 0x00000598 System.Void System.Text.Json.Serialization.Converters.CharConverter::Write(System.Text.Json.Utf8JsonWriter,System.Char,System.Text.Json.JsonSerializerOptions)
extern void CharConverter_Write_mE5A3749A222B385F5317DB00835B689BB2A08BF6 (void);
// 0x00000599 System.Char System.Text.Json.Serialization.Converters.CharConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void CharConverter_ReadWithQuotes_mF739825C3E7772DEF7AFC53A1D17A5AA83DB214A (void);
// 0x0000059A System.Void System.Text.Json.Serialization.Converters.CharConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Char,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void CharConverter_WriteWithQuotes_m3C431FBCDEFAABB469A3C5C192C8A33690FFFDD3 (void);
// 0x0000059B System.Void System.Text.Json.Serialization.Converters.CharConverter::.ctor()
extern void CharConverter__ctor_m0FFE22128925F6ABF4434B3DB47EAB50A42587CB (void);
// 0x0000059C System.DateTime System.Text.Json.Serialization.Converters.DateTimeConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void DateTimeConverter_Read_m3A191F15CD1100212E16C805D823CF5CE588A291 (void);
// 0x0000059D System.Void System.Text.Json.Serialization.Converters.DateTimeConverter::Write(System.Text.Json.Utf8JsonWriter,System.DateTime,System.Text.Json.JsonSerializerOptions)
extern void DateTimeConverter_Write_m3B662D066AF10FD768E0DF0B37D90EBE25B991C9 (void);
// 0x0000059E System.DateTime System.Text.Json.Serialization.Converters.DateTimeConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void DateTimeConverter_ReadWithQuotes_m9EFBBF9AC5A9612142821FB8D62A3F172BE1534D (void);
// 0x0000059F System.Void System.Text.Json.Serialization.Converters.DateTimeConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.DateTime,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void DateTimeConverter_WriteWithQuotes_m50C81ECDEB97861420E3EB853BC67EEE5F4383D9 (void);
// 0x000005A0 System.Void System.Text.Json.Serialization.Converters.DateTimeConverter::.ctor()
extern void DateTimeConverter__ctor_m7ADE373DABAA4C6945FEBA3DC7D0CFE0EB09DA08 (void);
// 0x000005A1 System.DateTimeOffset System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void DateTimeOffsetConverter_Read_mB66E0433DC0829796F3FEF1C272D3894478F1083 (void);
// 0x000005A2 System.Void System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::Write(System.Text.Json.Utf8JsonWriter,System.DateTimeOffset,System.Text.Json.JsonSerializerOptions)
extern void DateTimeOffsetConverter_Write_m515BB4D2F68EE4CC4BEFA9F270B138D31D34C133 (void);
// 0x000005A3 System.DateTimeOffset System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void DateTimeOffsetConverter_ReadWithQuotes_m1FA9C3E4123136059CFB6868D4CCD749E178BC64 (void);
// 0x000005A4 System.Void System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.DateTimeOffset,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void DateTimeOffsetConverter_WriteWithQuotes_mD350D908ADCD5753EB8741D8A7A766F631B9C603 (void);
// 0x000005A5 System.Void System.Text.Json.Serialization.Converters.DateTimeOffsetConverter::.ctor()
extern void DateTimeOffsetConverter__ctor_m06F16EBCABD6B5429D93A11BA016EEC23F0DAD35 (void);
// 0x000005A6 System.Void System.Text.Json.Serialization.Converters.DecimalConverter::.ctor()
extern void DecimalConverter__ctor_m71F64B31AB7035005DC0BA5A3FCD3453A08CB81C (void);
// 0x000005A7 System.Decimal System.Text.Json.Serialization.Converters.DecimalConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void DecimalConverter_Read_mB9806D6C3DB04E65EE1C75E294F9AF19AFF9A0BF (void);
// 0x000005A8 System.Void System.Text.Json.Serialization.Converters.DecimalConverter::Write(System.Text.Json.Utf8JsonWriter,System.Decimal,System.Text.Json.JsonSerializerOptions)
extern void DecimalConverter_Write_m299FE718D677710F4BB035326B6DB66C60DC9AF9 (void);
// 0x000005A9 System.Decimal System.Text.Json.Serialization.Converters.DecimalConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void DecimalConverter_ReadWithQuotes_m7F355209DB610CE31A4B29C533B6D54C35A94C7A (void);
// 0x000005AA System.Void System.Text.Json.Serialization.Converters.DecimalConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Decimal,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void DecimalConverter_WriteWithQuotes_mFDAB142954E870EFB984320E4A386B33013AF2BF (void);
// 0x000005AB System.Decimal System.Text.Json.Serialization.Converters.DecimalConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void DecimalConverter_ReadNumberWithCustomHandling_mACB2E1CCB5852C68EA7B35A0B26DDEBABBCA2622 (void);
// 0x000005AC System.Void System.Text.Json.Serialization.Converters.DecimalConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Decimal,System.Text.Json.Serialization.JsonNumberHandling)
extern void DecimalConverter_WriteNumberWithCustomHandling_m48A84AD7028DF6B9423F7B1C07182888CE9E8F15 (void);
// 0x000005AD System.Void System.Text.Json.Serialization.Converters.DoubleConverter::.ctor()
extern void DoubleConverter__ctor_m92667AE837A19FBE60BF73C99E70A4E1E60FC3A7 (void);
// 0x000005AE System.Double System.Text.Json.Serialization.Converters.DoubleConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void DoubleConverter_Read_m306BBC93E2EC97020D4DA1711C2B3A07B990E796 (void);
// 0x000005AF System.Void System.Text.Json.Serialization.Converters.DoubleConverter::Write(System.Text.Json.Utf8JsonWriter,System.Double,System.Text.Json.JsonSerializerOptions)
extern void DoubleConverter_Write_m1C9598EF4151552915C4DDF9EA8EA907F59C4EB4 (void);
// 0x000005B0 System.Double System.Text.Json.Serialization.Converters.DoubleConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void DoubleConverter_ReadWithQuotes_mB065E5F48659B9A8F4184CF0A63FE674164E0631 (void);
// 0x000005B1 System.Void System.Text.Json.Serialization.Converters.DoubleConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Double,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void DoubleConverter_WriteWithQuotes_m15026451CAEBCB918FDB5203E24D18064A23D820 (void);
// 0x000005B2 System.Double System.Text.Json.Serialization.Converters.DoubleConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void DoubleConverter_ReadNumberWithCustomHandling_m85D49248F9A54DE696D54AAD2014465820B77AB3 (void);
// 0x000005B3 System.Void System.Text.Json.Serialization.Converters.DoubleConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Double,System.Text.Json.Serialization.JsonNumberHandling)
extern void DoubleConverter_WriteNumberWithCustomHandling_mA1CD28C82CE72537466654C7BE65D671CE9F0431 (void);
// 0x000005B4 System.Boolean System.Text.Json.Serialization.Converters.EnumConverter`1::CanConvert(System.Type)
// 0x000005B5 System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::.ctor(System.Text.Json.Serialization.Converters.EnumConverterOptions,System.Text.Json.JsonSerializerOptions)
// 0x000005B6 System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::.ctor(System.Text.Json.Serialization.Converters.EnumConverterOptions,System.Text.Json.JsonNamingPolicy,System.Text.Json.JsonSerializerOptions)
// 0x000005B7 T System.Text.Json.Serialization.Converters.EnumConverter`1::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x000005B8 System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::Write(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions)
// 0x000005B9 System.UInt64 System.Text.Json.Serialization.Converters.EnumConverter`1::ConvertToUInt64(System.Object)
// 0x000005BA System.Boolean System.Text.Json.Serialization.Converters.EnumConverter`1::IsValidIdentifier(System.String)
// 0x000005BB System.Text.Json.JsonEncodedText System.Text.Json.Serialization.Converters.EnumConverter`1::FormatEnumValue(System.String,System.Text.Encodings.Web.JavaScriptEncoder)
// 0x000005BC System.String System.Text.Json.Serialization.Converters.EnumConverter`1::FormatEnumValueToString(System.String,System.Text.Encodings.Web.JavaScriptEncoder)
// 0x000005BD T System.Text.Json.Serialization.Converters.EnumConverter`1::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
// 0x000005BE System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,T,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
// 0x000005BF System.Void System.Text.Json.Serialization.Converters.EnumConverter`1::.cctor()
// 0x000005C0 System.Void System.Text.Json.Serialization.Converters.EnumConverterFactory::.ctor()
extern void EnumConverterFactory__ctor_m15A64190A6484A0B17931CB9BDF8B83B48CA25E6 (void);
// 0x000005C1 System.Boolean System.Text.Json.Serialization.Converters.EnumConverterFactory::CanConvert(System.Type)
extern void EnumConverterFactory_CanConvert_mA049159C43F1C65F2419AFF90174445956C1B549 (void);
// 0x000005C2 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.EnumConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void EnumConverterFactory_CreateConverter_m9961C58C3723DF7506C2103327794C0D9CF394A6 (void);
// 0x000005C3 System.Guid System.Text.Json.Serialization.Converters.GuidConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void GuidConverter_Read_mB0F5E39B557DACE2AC1B7BCE57983730EBBE45C1 (void);
// 0x000005C4 System.Void System.Text.Json.Serialization.Converters.GuidConverter::Write(System.Text.Json.Utf8JsonWriter,System.Guid,System.Text.Json.JsonSerializerOptions)
extern void GuidConverter_Write_m9DFCC7FC6E77B8C456D6BADF88F164495D352A3D (void);
// 0x000005C5 System.Guid System.Text.Json.Serialization.Converters.GuidConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void GuidConverter_ReadWithQuotes_m8BB9C17128565576A475634CF060057C322464C2 (void);
// 0x000005C6 System.Void System.Text.Json.Serialization.Converters.GuidConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Guid,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void GuidConverter_WriteWithQuotes_m757E11C8356B6DBF6710F8045522525C17B4A9DB (void);
// 0x000005C7 System.Void System.Text.Json.Serialization.Converters.GuidConverter::.ctor()
extern void GuidConverter__ctor_mF590625FEA9B6CBC91D8EC8C14C0536253560F21 (void);
// 0x000005C8 System.Void System.Text.Json.Serialization.Converters.Int16Converter::.ctor()
extern void Int16Converter__ctor_m86D524897BCCD20229BE08AF9C1679EA6B576FCA (void);
// 0x000005C9 System.Int16 System.Text.Json.Serialization.Converters.Int16Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void Int16Converter_Read_mD87641DE857747D4472598CE10F0622790EE163B (void);
// 0x000005CA System.Void System.Text.Json.Serialization.Converters.Int16Converter::Write(System.Text.Json.Utf8JsonWriter,System.Int16,System.Text.Json.JsonSerializerOptions)
extern void Int16Converter_Write_m5BC7AB83FC8EEB9276B73044468697F4EE2E8B4C (void);
// 0x000005CB System.Int16 System.Text.Json.Serialization.Converters.Int16Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void Int16Converter_ReadWithQuotes_m04208165BBB6E4EF0432172F1131B5B0E0ED9ED9 (void);
// 0x000005CC System.Void System.Text.Json.Serialization.Converters.Int16Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Int16,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void Int16Converter_WriteWithQuotes_m3439C486B6ACAE8127387AD1BE7E4FA0603D8C48 (void);
// 0x000005CD System.Int16 System.Text.Json.Serialization.Converters.Int16Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int16Converter_ReadNumberWithCustomHandling_m27677800F58180D2B0E122DF7D71EE040284A0DA (void);
// 0x000005CE System.Void System.Text.Json.Serialization.Converters.Int16Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Int16,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int16Converter_WriteNumberWithCustomHandling_mD26208F738CE162BBCA52CAC638D219AD9881787 (void);
// 0x000005CF System.Void System.Text.Json.Serialization.Converters.Int32Converter::.ctor()
extern void Int32Converter__ctor_mCB08B1BDE0301CC5409FF5D5C83FF331F0B2AB67 (void);
// 0x000005D0 System.Int32 System.Text.Json.Serialization.Converters.Int32Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void Int32Converter_Read_mC629132D77640298A138493168F9A02CEA556859 (void);
// 0x000005D1 System.Void System.Text.Json.Serialization.Converters.Int32Converter::Write(System.Text.Json.Utf8JsonWriter,System.Int32,System.Text.Json.JsonSerializerOptions)
extern void Int32Converter_Write_m661425DEE9165EE96C47BE35CE260D9111AC2FD4 (void);
// 0x000005D2 System.Int32 System.Text.Json.Serialization.Converters.Int32Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void Int32Converter_ReadWithQuotes_mE669B7CBE3DF0F76EFB08EA7A02427DD3EA6857F (void);
// 0x000005D3 System.Void System.Text.Json.Serialization.Converters.Int32Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Int32,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void Int32Converter_WriteWithQuotes_m08F626C30DF839F718FFE140AC62D2F7424AA41D (void);
// 0x000005D4 System.Int32 System.Text.Json.Serialization.Converters.Int32Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int32Converter_ReadNumberWithCustomHandling_mE977076A9DF61259DAE6431C25519F0753BD37D4 (void);
// 0x000005D5 System.Void System.Text.Json.Serialization.Converters.Int32Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Int32,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int32Converter_WriteNumberWithCustomHandling_m0069DE3BEE343D2C68D48794AA0F589AEEC64562 (void);
// 0x000005D6 System.Void System.Text.Json.Serialization.Converters.Int64Converter::.ctor()
extern void Int64Converter__ctor_m8770BF9B89BAA0D9B1785DDFBDFFEE6EBB2C4DBC (void);
// 0x000005D7 System.Int64 System.Text.Json.Serialization.Converters.Int64Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void Int64Converter_Read_m3A9C416AD2A723AC96B16AEE7B7EED2C77D87572 (void);
// 0x000005D8 System.Void System.Text.Json.Serialization.Converters.Int64Converter::Write(System.Text.Json.Utf8JsonWriter,System.Int64,System.Text.Json.JsonSerializerOptions)
extern void Int64Converter_Write_mFDEF66D026EE6C924DA62067E7AB08E1A634D96F (void);
// 0x000005D9 System.Int64 System.Text.Json.Serialization.Converters.Int64Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void Int64Converter_ReadWithQuotes_m0CFCA760D2727B472D65006DAAEBC340BBFD8BFD (void);
// 0x000005DA System.Void System.Text.Json.Serialization.Converters.Int64Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Int64,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void Int64Converter_WriteWithQuotes_mC9E2927647C239E8386DD3D29A5A1C613F163C6E (void);
// 0x000005DB System.Int64 System.Text.Json.Serialization.Converters.Int64Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int64Converter_ReadNumberWithCustomHandling_m7E8D2EFF8F6F51DEA9A6178C395175CCCF83ECAA (void);
// 0x000005DC System.Void System.Text.Json.Serialization.Converters.Int64Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Int64,System.Text.Json.Serialization.JsonNumberHandling)
extern void Int64Converter_WriteNumberWithCustomHandling_mE8750771068DB3262795A466E37327577BC8841B (void);
// 0x000005DD System.Text.Json.JsonDocument System.Text.Json.Serialization.Converters.JsonDocumentConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void JsonDocumentConverter_Read_m22C677023EE78581F7A17117657463C573F6102D (void);
// 0x000005DE System.Void System.Text.Json.Serialization.Converters.JsonDocumentConverter::Write(System.Text.Json.Utf8JsonWriter,System.Text.Json.JsonDocument,System.Text.Json.JsonSerializerOptions)
extern void JsonDocumentConverter_Write_m785E1CCA7D96A293C3C950286D5C5BB41CE3C45E (void);
// 0x000005DF System.Void System.Text.Json.Serialization.Converters.JsonDocumentConverter::.ctor()
extern void JsonDocumentConverter__ctor_m5685557D2205CE842FD9DA942ADD43038B19B2C3 (void);
// 0x000005E0 System.Text.Json.JsonElement System.Text.Json.Serialization.Converters.JsonElementConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void JsonElementConverter_Read_m7DAB7D809BE1759D05DA2E6E01B4AA6D655D9F02 (void);
// 0x000005E1 System.Void System.Text.Json.Serialization.Converters.JsonElementConverter::Write(System.Text.Json.Utf8JsonWriter,System.Text.Json.JsonElement,System.Text.Json.JsonSerializerOptions)
extern void JsonElementConverter_Write_mC9196D59EDD7AA14E635570DFED3B82E40C9F507 (void);
// 0x000005E2 System.Void System.Text.Json.Serialization.Converters.JsonElementConverter::.ctor()
extern void JsonElementConverter__ctor_m58273CA280B619F513BA2C282EF715CC0D123ED3 (void);
// 0x000005E3 System.Void System.Text.Json.Serialization.Converters.NullableConverter`1::.ctor(System.Text.Json.Serialization.JsonConverter`1<T>)
// 0x000005E4 System.Nullable`1<T> System.Text.Json.Serialization.Converters.NullableConverter`1::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
// 0x000005E5 System.Void System.Text.Json.Serialization.Converters.NullableConverter`1::Write(System.Text.Json.Utf8JsonWriter,System.Nullable`1<T>,System.Text.Json.JsonSerializerOptions)
// 0x000005E6 System.Nullable`1<T> System.Text.Json.Serialization.Converters.NullableConverter`1::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
// 0x000005E7 System.Void System.Text.Json.Serialization.Converters.NullableConverter`1::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Nullable`1<T>,System.Text.Json.Serialization.JsonNumberHandling)
// 0x000005E8 System.Boolean System.Text.Json.Serialization.Converters.NullableConverterFactory::CanConvert(System.Type)
extern void NullableConverterFactory_CanConvert_mD59B0889DB3FA18479000666074236DF712F37FA (void);
// 0x000005E9 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.NullableConverterFactory::CreateConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void NullableConverterFactory_CreateConverter_m1C25C5A9992FC110FD51D6BD93FE13205279207C (void);
// 0x000005EA System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.NullableConverterFactory::CreateValueConverter(System.Type,System.Text.Json.Serialization.JsonConverter)
extern void NullableConverterFactory_CreateValueConverter_mA04BCC0718271A323490045E2A1A2A866E6EC41B (void);
// 0x000005EB System.Void System.Text.Json.Serialization.Converters.NullableConverterFactory::.ctor()
extern void NullableConverterFactory__ctor_mE0171FEEB922C21A6FFCE32602A43BA9A83BDC45 (void);
// 0x000005EC System.Object System.Text.Json.Serialization.Converters.ObjectConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverter_Read_m7D02AF462AC6A294545480280759B17D769D34BC (void);
// 0x000005ED System.Void System.Text.Json.Serialization.Converters.ObjectConverter::Write(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverter_Write_m33F55DD85790AA5724DEDBFDC28D77D0E4097569 (void);
// 0x000005EE System.Object System.Text.Json.Serialization.Converters.ObjectConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void ObjectConverter_ReadWithQuotes_mFCE9722383C95BBF3414B179BDC171039FE84E75 (void);
// 0x000005EF System.Void System.Text.Json.Serialization.Converters.ObjectConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Object,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void ObjectConverter_WriteWithQuotes_m58045B9F88612C239727514984B0B8B094CBCE9E (void);
// 0x000005F0 System.Text.Json.Serialization.JsonConverter System.Text.Json.Serialization.Converters.ObjectConverter::GetRuntimeConverter(System.Type,System.Text.Json.JsonSerializerOptions)
extern void ObjectConverter_GetRuntimeConverter_mDF4C471A97B3A1B1B63AC08DA54D966C8084E699 (void);
// 0x000005F1 System.Void System.Text.Json.Serialization.Converters.ObjectConverter::.ctor()
extern void ObjectConverter__ctor_m467BB63B2F6F648856A50C2FB5A032417197C6B3 (void);
// 0x000005F2 System.Void System.Text.Json.Serialization.Converters.SByteConverter::.ctor()
extern void SByteConverter__ctor_mE78030BB54D847713565A1EE2947D0B41D13DB6E (void);
// 0x000005F3 System.SByte System.Text.Json.Serialization.Converters.SByteConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void SByteConverter_Read_m2312E1F3B4500FEEBE58865A9E3068812029B7F8 (void);
// 0x000005F4 System.Void System.Text.Json.Serialization.Converters.SByteConverter::Write(System.Text.Json.Utf8JsonWriter,System.SByte,System.Text.Json.JsonSerializerOptions)
extern void SByteConverter_Write_mC3B4E520F0FC579B6DA4F5B8934EAE1E2400BFBE (void);
// 0x000005F5 System.SByte System.Text.Json.Serialization.Converters.SByteConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void SByteConverter_ReadWithQuotes_mED3F7F0E3A56B3CFB8D22C9C90D6DD32D635CE11 (void);
// 0x000005F6 System.Void System.Text.Json.Serialization.Converters.SByteConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.SByte,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void SByteConverter_WriteWithQuotes_m4AE9887AD3B80334AC275630E07AAF1F7BC499FD (void);
// 0x000005F7 System.SByte System.Text.Json.Serialization.Converters.SByteConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void SByteConverter_ReadNumberWithCustomHandling_m998184C8E5D53B4069B5E750F0E8753772B495A3 (void);
// 0x000005F8 System.Void System.Text.Json.Serialization.Converters.SByteConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.SByte,System.Text.Json.Serialization.JsonNumberHandling)
extern void SByteConverter_WriteNumberWithCustomHandling_m1930B514F1D67945E731EAD6BC21E3095AA9FEA6 (void);
// 0x000005F9 System.Void System.Text.Json.Serialization.Converters.SingleConverter::.ctor()
extern void SingleConverter__ctor_mAB5D16C009474673ECEC4623B965416835ED55A1 (void);
// 0x000005FA System.Single System.Text.Json.Serialization.Converters.SingleConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void SingleConverter_Read_mCA530140EF771ED162DEBF49D996542D7D30D082 (void);
// 0x000005FB System.Void System.Text.Json.Serialization.Converters.SingleConverter::Write(System.Text.Json.Utf8JsonWriter,System.Single,System.Text.Json.JsonSerializerOptions)
extern void SingleConverter_Write_mF5678401B8FF2D4D42302FC199619D7A50530F2D (void);
// 0x000005FC System.Single System.Text.Json.Serialization.Converters.SingleConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void SingleConverter_ReadWithQuotes_mC7C3FC17B07CD57C7BF1186E5716FEBD6EF93F92 (void);
// 0x000005FD System.Void System.Text.Json.Serialization.Converters.SingleConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.Single,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void SingleConverter_WriteWithQuotes_mC11D28C5ADD14F30CB4C61DD60B1E5382B3F0DBE (void);
// 0x000005FE System.Single System.Text.Json.Serialization.Converters.SingleConverter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void SingleConverter_ReadNumberWithCustomHandling_mC2B5EF3CE86BCC7EF0E8692CB133E02B50BCC465 (void);
// 0x000005FF System.Void System.Text.Json.Serialization.Converters.SingleConverter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.Single,System.Text.Json.Serialization.JsonNumberHandling)
extern void SingleConverter_WriteNumberWithCustomHandling_m718E47005E82F60C438A5CCF5621F016FCDE8F04 (void);
// 0x00000600 System.String System.Text.Json.Serialization.Converters.StringConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void StringConverter_Read_m63DAABF404AABDFA81B2A8A35C4B56063D531618 (void);
// 0x00000601 System.Void System.Text.Json.Serialization.Converters.StringConverter::Write(System.Text.Json.Utf8JsonWriter,System.String,System.Text.Json.JsonSerializerOptions)
extern void StringConverter_Write_mBFF51D23748E2097EC16786A5D55D9E4611B629A (void);
// 0x00000602 System.String System.Text.Json.Serialization.Converters.StringConverter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void StringConverter_ReadWithQuotes_m9019E186DE8BA663F52F4EB9D99525F668ED2A5A (void);
// 0x00000603 System.Void System.Text.Json.Serialization.Converters.StringConverter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.String,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void StringConverter_WriteWithQuotes_mB4EBAFD89D394971AC8A306E555F101CB71C1732 (void);
// 0x00000604 System.Void System.Text.Json.Serialization.Converters.StringConverter::.ctor()
extern void StringConverter__ctor_m8C0DE7B66476F75C66052B85214154F85D6FE935 (void);
// 0x00000605 System.Type System.Text.Json.Serialization.Converters.TypeConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void TypeConverter_Read_m3095C07F1808F050CC61DCB5FBE12616C9794A1C (void);
// 0x00000606 System.Void System.Text.Json.Serialization.Converters.TypeConverter::Write(System.Text.Json.Utf8JsonWriter,System.Type,System.Text.Json.JsonSerializerOptions)
extern void TypeConverter_Write_m5432822B54114CC9DBF55A1F8DEEB692A669FC85 (void);
// 0x00000607 System.Void System.Text.Json.Serialization.Converters.TypeConverter::.ctor()
extern void TypeConverter__ctor_m05A345D313E72F986686736087B892D1FB4E0E88 (void);
// 0x00000608 System.Void System.Text.Json.Serialization.Converters.UInt16Converter::.ctor()
extern void UInt16Converter__ctor_mA2C387BBC597B9D7EF97EBCC7DF90C652E52CEB0 (void);
// 0x00000609 System.UInt16 System.Text.Json.Serialization.Converters.UInt16Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void UInt16Converter_Read_m6D7450E228F8E7AF6C1DB6B21FDE9C53098436FA (void);
// 0x0000060A System.Void System.Text.Json.Serialization.Converters.UInt16Converter::Write(System.Text.Json.Utf8JsonWriter,System.UInt16,System.Text.Json.JsonSerializerOptions)
extern void UInt16Converter_Write_m20085E8B4C37F1841838E7A30490E9B3DDF59903 (void);
// 0x0000060B System.UInt16 System.Text.Json.Serialization.Converters.UInt16Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void UInt16Converter_ReadWithQuotes_m714FF9DF75950DF48CC178E8AED33178578D1911 (void);
// 0x0000060C System.Void System.Text.Json.Serialization.Converters.UInt16Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.UInt16,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void UInt16Converter_WriteWithQuotes_m12C6D51D2A7EC43DC6A98AE25EE1B8D288BCEBCE (void);
// 0x0000060D System.UInt16 System.Text.Json.Serialization.Converters.UInt16Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt16Converter_ReadNumberWithCustomHandling_mEA553EB68F2972D0BA040527D52F4A97E3127557 (void);
// 0x0000060E System.Void System.Text.Json.Serialization.Converters.UInt16Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.UInt16,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt16Converter_WriteNumberWithCustomHandling_mEF01FA814FFED395E66F4D2FA44B4CFE1E8997A6 (void);
// 0x0000060F System.Void System.Text.Json.Serialization.Converters.UInt32Converter::.ctor()
extern void UInt32Converter__ctor_mA850D06A31828B8B9FCCED129CCF97A4EC42F57E (void);
// 0x00000610 System.UInt32 System.Text.Json.Serialization.Converters.UInt32Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void UInt32Converter_Read_m7A1253925990C5482073E6CEA368FD8D075A097E (void);
// 0x00000611 System.Void System.Text.Json.Serialization.Converters.UInt32Converter::Write(System.Text.Json.Utf8JsonWriter,System.UInt32,System.Text.Json.JsonSerializerOptions)
extern void UInt32Converter_Write_m583E699545AD78316A39ABD56B08041A3B36A3AD (void);
// 0x00000612 System.UInt32 System.Text.Json.Serialization.Converters.UInt32Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void UInt32Converter_ReadWithQuotes_m51AF839875B04C7008A186A38C39563312C0A33F (void);
// 0x00000613 System.Void System.Text.Json.Serialization.Converters.UInt32Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.UInt32,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void UInt32Converter_WriteWithQuotes_mF3E8795F7DFAFF2BE7E9EDF15DF5F00F18AB9726 (void);
// 0x00000614 System.UInt32 System.Text.Json.Serialization.Converters.UInt32Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt32Converter_ReadNumberWithCustomHandling_m8C2076E4B461CDDC8F15145903407EE7408A4D3A (void);
// 0x00000615 System.Void System.Text.Json.Serialization.Converters.UInt32Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.UInt32,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt32Converter_WriteNumberWithCustomHandling_mBF70E74261DA438E9E8F47EB11F2C418E9607ACA (void);
// 0x00000616 System.Void System.Text.Json.Serialization.Converters.UInt64Converter::.ctor()
extern void UInt64Converter__ctor_m1A91E6155CFBC2E4FB7114C4495224C6690AB7D8 (void);
// 0x00000617 System.UInt64 System.Text.Json.Serialization.Converters.UInt64Converter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void UInt64Converter_Read_m61F45A0B0E0A19DB21993C3E8D96A1BDCCC38AFB (void);
// 0x00000618 System.Void System.Text.Json.Serialization.Converters.UInt64Converter::Write(System.Text.Json.Utf8JsonWriter,System.UInt64,System.Text.Json.JsonSerializerOptions)
extern void UInt64Converter_Write_mE77B2428AD27F58E36BD22398725ABCA8F1828B8 (void);
// 0x00000619 System.UInt64 System.Text.Json.Serialization.Converters.UInt64Converter::ReadWithQuotes(System.Text.Json.Utf8JsonReader&)
extern void UInt64Converter_ReadWithQuotes_m8F774E85EDA4F37BB61E745AA125749993ECFC6B (void);
// 0x0000061A System.Void System.Text.Json.Serialization.Converters.UInt64Converter::WriteWithQuotes(System.Text.Json.Utf8JsonWriter,System.UInt64,System.Text.Json.JsonSerializerOptions,System.Text.Json.WriteStack&)
extern void UInt64Converter_WriteWithQuotes_mB40244929B49DD209E9CAF4F0F5BB0A22E090281 (void);
// 0x0000061B System.UInt64 System.Text.Json.Serialization.Converters.UInt64Converter::ReadNumberWithCustomHandling(System.Text.Json.Utf8JsonReader&,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt64Converter_ReadNumberWithCustomHandling_m55EBD0E07D476D8F263139A2BCB2D47F7EA13915 (void);
// 0x0000061C System.Void System.Text.Json.Serialization.Converters.UInt64Converter::WriteNumberWithCustomHandling(System.Text.Json.Utf8JsonWriter,System.UInt64,System.Text.Json.Serialization.JsonNumberHandling)
extern void UInt64Converter_WriteNumberWithCustomHandling_mA4344C181271BF3C587A75B52645E5E7F0DD6B54 (void);
// 0x0000061D System.Uri System.Text.Json.Serialization.Converters.UriConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void UriConverter_Read_m95D07717B24C17A0128D02B5EBD9D692352D3DCF (void);
// 0x0000061E System.Void System.Text.Json.Serialization.Converters.UriConverter::Write(System.Text.Json.Utf8JsonWriter,System.Uri,System.Text.Json.JsonSerializerOptions)
extern void UriConverter_Write_m9F22D832BB748D7F9F22D561C9502B7D9285442B (void);
// 0x0000061F System.Void System.Text.Json.Serialization.Converters.UriConverter::.ctor()
extern void UriConverter__ctor_m40EF5BDDEF48CA59C01581AA9E2F791B1BEDF227 (void);
// 0x00000620 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m49E2E4F63C1CBDA71284D33E4FFA5C738240F7D9 (void);
static Il2CppMethodPointer s_methodPointers[1568] = 
{
	EmbeddedAttribute__ctor_m57062416AA0B0C88D87FFE5636D49A61F7010DE4,
	IsReadOnlyAttribute__ctor_m8D50662C3DFF9E5EB44208928F00DAC3DE0D7D69,
	IsByRefLikeAttribute__ctor_m03D16E2B157C52538B122D32A7008A2B8E74529B,
	NullableAttribute__ctor_m7287C1DF6EDAD6325A9A8B2581C1602640707D7B,
	NullableContextAttribute__ctor_mDAA05787EC832A8F14D58A91E9DA3A7569B982D6,
	NullablePublicOnlyAttribute__ctor_m3409379E5ED0F4B8D7DB99E48AC717C2D1677A55,
	HexConverter_ToCharUpper_m1EB698BB21EA438E644EF4A4ED2ABD5AB90C4929,
	HexConverter_FromChar_m550EA11F67C43F1430F5AA8A3CD991D8062C9AD9,
	HexConverter_IsHexChar_mC6DAD65E9943236A7C67BDD73A19B77D25CCD6FA,
	HexConverter_get_CharToHexLookup_m734AAFFA259562CE33DB2D34AED8E0B679145E5E,
	SR_UsingResourceKeys_mDC2DAF1D8F4990443E9DA23C5AA2441DCF0C5CD3,
	SR_GetResourceString_m02D10C3BA46D92C0D4F70D01FC25DFD4F3F4E276,
	SR_Format_mD6D7C45289B8602CA7F2EA0692244E53FF55C8DF,
	SR_Format_m3477D4175CB8E27C4C1753CE70687768BBD2B60F,
	SR_Format_mBA031A77F7C9E51B2BCDC8385FEE7AFD795C8474,
	SR_Format_m4E2280EF70B69103437BCADA5DDAB7C825A3DA07,
	SR_get_ResourceManager_m583D2FBABEDB90189BE00E73DE8E9B1D132C9D10,
	SR_get_ArrayDepthTooLarge_m42201D2763F3CCD2A8914674917E70355F639DFC,
	SR_get_CannotReadIncompleteUTF16_m944367C7ECB4D121DCDB34E023E29C253555B85D,
	SR_get_CannotReadInvalidUTF16_mF95679BADED663876D59BCC0C8CA683D3C3ED6DB,
	SR_get_CannotStartObjectArrayAfterPrimitiveOrClose_m8777602CBE6CB87E09CF87E63C5ECABEC1E57433,
	SR_get_CannotStartObjectArrayWithoutProperty_mFED3E92F24ACF5FF6D753EBA6E89559E64DDE44A,
	SR_get_CannotTranscodeInvalidUtf8_m35D92692E6BC08640662F9B9E8FE8209B3E84579,
	SR_get_CannotDecodeInvalidBase64_mBAD093ED9F405A8684ACA1D593A12F592CC824EE,
	SR_get_CannotTranscodeInvalidUtf16_mE65FCC4D0FF86F063F074ED5AC4E0A8B890D9619,
	SR_get_CannotEncodeInvalidUTF16_m475504A3662BB4D3BADF25DE623556C3F1DB7A45,
	SR_get_CannotEncodeInvalidUTF8_m1F969CCE265A1A7886F4413856F63EDB7E08D8F7,
	SR_get_CannotWritePropertyWithinArray_m98A0BFB0A4DE2AB2B60FEF2F2F6E12B2E0A3356C,
	SR_get_CannotWritePropertyAfterProperty_m9496F5B2837CF6C844126AE533AECF3466393437,
	SR_get_CannotWriteValueAfterPrimitiveOrClose_mB63AADF2F8301C81C5B7E11C0EE43EA067708AEF,
	SR_get_CannotWriteValueWithinObject_mA5FA81DA8CF33F1A208D619F7FA0EC0736B33F54,
	SR_get_DepthTooLarge_mB0589B173A594380338004E7489B57DE41162AAC,
	SR_get_EndOfCommentNotFound_m9B50FF177B87E1EE818EDEE2F8DFDDCC9D2BF9F8,
	SR_get_EndOfStringNotFound_mF6AFA0E2F889B80281BC679767D03ABD159D0624,
	SR_get_ExpectedEndAfterSingleJson_mC8A25E8EAD9414C376832A43B8906C6EC84A434D,
	SR_get_ExpectedEndOfDigitNotFound_mFC63AB23ACA9E4C5126FABF43654FD62552F1BB1,
	SR_get_ExpectedFalse_mAFD561299B6913F6BBBEF585B766D11AC1365E94,
	SR_get_ExpectedJsonTokens_mB32485B7F496984825183B6B0E337F72E5497959,
	SR_get_ExpectedOneCompleteToken_mB2B9B8F5110736B8CEDD7264740F842CD5B8684E,
	SR_get_ExpectedNextDigitEValueNotFound_m3CD5E5989BC1D79D2D9C672BF5A0FA5366AF2B79,
	SR_get_ExpectedNull_m1F6D91F25CB3CB209FB0FD387C4ED2C6B5F13F24,
	SR_get_ExpectedSeparatorAfterPropertyNameNotFound_m094EC61DD8D2B78F8A324EE5167815927B5446F6,
	SR_get_ExpectedStartOfPropertyNotFound_mE619D505227886A17E58271844782E833C8FF32C,
	SR_get_ExpectedStartOfPropertyOrValueNotFound_mD895BA183797E6136DC12277A96C345EE853988F,
	SR_get_ExpectedStartOfValueNotFound_mAC2C01E3639D1E3CA30EF277EAD9B42BE8AAADC9,
	SR_get_ExpectedTrue_m6606A88E12781E1E74F42766367AF157AFC1A45A,
	SR_get_ExpectedValueAfterPropertyNameNotFound_mA12BC622B845109BEDC4BA0709E5C07E77A960CA,
	SR_get_FailedToGetLargerSpan_mC6A78FD7DBF417802D87AB7CF4BFCE9D1262DEC1,
	SR_get_FoundInvalidCharacter_m684715313507A3908483C9701A966AE7F2020201,
	SR_get_InvalidCast_m4765E42F23B957FD767339E6E82F985954E8049A,
	SR_get_InvalidCharacterAfterEscapeWithinString_mB8E8F0BA4CDE2240D12ADB05739FC35B778061AC,
	SR_get_InvalidCharacterWithinString_m4A6DD434A1E5DFAC1D3A723D15F3125E63B6AE5D,
	SR_get_InvalidEndOfJsonNonPrimitive_m663B13FD3A43B1274D418CC8B9D6B9C529A2B2F2,
	SR_get_InvalidHexCharacterWithinString_m5DCE6F8C8568A4492B5E24F404037A8686687987,
	SR_get_JsonDocumentDoesNotSupportComments_mA201632FDA65628C0896F111966E8ECC344274D7,
	SR_get_JsonElementHasWrongType_m1D09105C4084618BB38D6CB31CAB03BAF2FD01A2,
	SR_get_MaxDepthMustBePositive_mEF176814E001949851FEDD2D9236DF6EA256FD86,
	SR_get_CommentHandlingMustBeValid_m054623C5A1C932ABC065112C9C7805A9C6622F9C,
	SR_get_MismatchedObjectArray_m46D72F2BA23CD73AE10B67EB45802D16FDA45F58,
	SR_get_CannotWriteEndAfterProperty_mE0DBAD298E5211671FDCAE72140F59DE09286064,
	SR_get_ObjectDepthTooLarge_m2E9BDB6520C02291742E6F7B3BF1B7D7127AC2FB,
	SR_get_PropertyNameTooLarge_mAD27EA72AE6A6687579BC45EA2729F6A5145594A,
	SR_get_FormatDecimal_mFE14407BA8C956175CD21C4C8FC78D506894A07B,
	SR_get_FormatDouble_m47646195A9A67DA6AAE37E57E33E52609C26E789,
	SR_get_FormatInt32_m2D9194959286497E57C3D2FEBEC6ECEF606A0545,
	SR_get_FormatInt64_m94CE8F1D75F5ACD76A91027BB9BA6DA1633076F3,
	SR_get_FormatSingle_m2E5BAC80D30378742097778A48AE38BD379DC539,
	SR_get_FormatUInt32_m0A8D6D4FF312A518DE93DF736154B88E09300257,
	SR_get_FormatUInt64_m92955C632B6C395C1551C5D44E954BA92A534607,
	SR_get_RequiredDigitNotFoundAfterDecimal_mFE209CED59EA456CC2B949D7E841B13DBC8AB3E7,
	SR_get_RequiredDigitNotFoundAfterSign_mBD2EEFCE2750E5DDF34B0B865985888ADFD9E947,
	SR_get_RequiredDigitNotFoundEndOfData_m703BDAE0498CB850C64A5A49A87A314F03F02E37,
	SR_get_SpecialNumberValuesNotSupported_m6B42A74BCA4B396AA9BF6CD2E072C5D38B82E97A,
	SR_get_ValueTooLarge_m63BD834F730B5047341CF85B5B6433569B12FC56,
	SR_get_ZeroDepthAtEnd_m84EF3B9084B657AD36F6F4DF561DEF9BC2C54361,
	SR_get_DeserializeUnableToConvertValue_m0E0B9A3CA7AD02904B530584D637BDEAB96DB77E,
	SR_get_BufferWriterAdvancedTooFar_m99BE0E80A2137618B1DB1C71DDD9E5C4B9C68C40,
	SR_get_FormatDateTime_m7EFB48E294A725881CB03632A563946061D649A7,
	SR_get_FormatDateTimeOffset_m9849F3417114F1E724BA31702FE8B2F153728155,
	SR_get_FormatGuid_m7A1A536CA15EC5BBA8DC92C49CA44D7C38EC2863,
	SR_get_ExpectedStartOfPropertyOrValueAfterComment_mED814A86D624CAA0430521B7D48534D6327DF923,
	SR_get_TrailingCommaNotAllowedBeforeArrayEnd_mB904FDB19B7E0F9BA59D359BF0C01C3B3F857A6B,
	SR_get_TrailingCommaNotAllowedBeforeObjectEnd_mE3B14EFBD3D465B6553FCE68330AAE478938AA13,
	SR_get_SerializerOptionsImmutable_mF1C8D5D0819B80B783A9DE9E0438360A1FCC200C,
	SR_get_SerializerPropertyNameConflict_m311ABE143B44E390CBBD6FEF07D37DD1E1E97DA3,
	SR_get_SerializerPropertyNameNull_m363B36EFBEDDBAF7970311F3D75FC9FF997D94A4,
	SR_get_SerializationDataExtensionPropertyInvalid_mAA7EF613733E82BDCD57647BB7ABBAD13FBAD413,
	SR_get_SerializationDuplicateTypeAttribute_m6C887253D35FA1258A842F35411881519909BE4A,
	SR_get_SerializationNotSupportedType_m50366E9C21059F0A4A4A26E910B5FAEC3774CA85,
	SR_get_InvalidCharacterAtStartOfComment_m3659D1047146A39D588F0971CD5A3962D6B81942,
	SR_get_UnexpectedEndOfDataWhileReadingComment_m4F1285062C69545771CA99434210FAD9EA2C452E,
	SR_get_CannotSkip_mCB40E4DCC66433C0B5CCEF72F551567A5A56571E,
	SR_get_NotEnoughData_mA27E56766957D9E955DAA6181E413FBCA89320FF,
	SR_get_UnexpectedEndOfLineSeparator_m8A21E48439B2859498B1D7027054B6B296C98DFE,
	SR_get_DeserializeNoConstructor_m9BDE5BD3E9788C75F6AC924AFF86CC88DAD20C7E,
	SR_get_DeserializePolymorphicInterface_mC7A869FEEF8FF96D7F2A022827DFBBF8E63C7F51,
	SR_get_SerializationConverterOnAttributeNotCompatible_m3F1415FA25600BE6C4A54093A4B8D4CE8B6D68EE,
	SR_get_SerializationConverterOnAttributeInvalid_m299BA13F75F2A8CFC6B0A5D312630D911E1D2074,
	SR_get_SerializationConverterRead_mE5BF4C7022874FAB823D03F78710AFABFD074E75,
	SR_get_SerializationConverterNotCompatible_m137B152C460F8E6DE9C830EC8BBE8D901069A30B,
	SR_get_SerializationConverterWrite_m4E4ADAEB485B27F9A1E090A3EEB64FB0EE408ED0,
	SR_get_NamingPolicyReturnNull_m117D80023B270D57599E1F14D1898B64DC3895EF,
	SR_get_SerializationDuplicateAttribute_m12970C66F746F878F2C6C151CD3B6224C8A081A9,
	SR_get_SerializeUnableToSerialize_m55B1E260F720800E11E001569DA2A06201F42C18,
	SR_get_FormatByte_m3D487820B74021CBEEBAEE3AECFCBBBB7E63E16D,
	SR_get_FormatInt16_m9841915E590428B04F883DA31ED8CAC7167FFF90,
	SR_get_FormatSByte_mB243F9693025F63AE7D97F4C9E2B9DD285D1FDE3,
	SR_get_FormatUInt16_m3D2E561525B5C247DCCEC1C78C983A3550AB3E68,
	SR_get_SerializerCycleDetected_m94EAC7F070FEB6C0A113F0104C119E5A49711CB2,
	SR_get_InvalidLeadingZeroInNumber_mAEE042FD33F97A117F9A97549FECDBA75B23A726,
	SR_get_MetadataCannotParsePreservedObjectToImmutable_m30326102F9C88F587E1F5B28F397FBFE48AA3167,
	SR_get_MetadataDuplicateIdFound_mBB6FC2B830F7E293EB1FF18DBBAD5C2D81AA8BDB,
	SR_get_MetadataIdIsNotFirstProperty_mC2DB7C2423B816C5D010FEA1F1EA3ECC7E8C6BE2,
	SR_get_MetadataInvalidReferenceToValueType_m5A739ADDEBE9ECB04FAB1EE067D4CCF7DAE05F6F,
	SR_get_MetadataInvalidTokenAfterValues_m95B2159ECC07F378B9F9DA8E9992343DB109C67D,
	SR_get_MetadataPreservedArrayFailed_mD377318C7455ECBDAAAB61DBDCD0AAB974EFF207,
	SR_get_MetadataPreservedArrayInvalidProperty_mBD3E16B2BAFD2B9883530B2F8660EAC0FA2CAD66,
	SR_get_MetadataPreservedArrayPropertyNotFound_m20136FD7E40494CC5BCC48FC6B15F2321A641616,
	SR_get_MetadataReferenceCannotContainOtherProperties_mFFCE2478326F28E2C3C6E18ECEF81E4E0F29DCAF,
	SR_get_MetadataReferenceNotFound_m0D072B743C9AD166D347B2F70FE0043EA5F1B6E5,
	SR_get_MetadataValueWasNotString_m8AFEEF170E2202B2D3BDEBC53DEDE33A2C706D02,
	SR_get_MetadataInvalidPropertyWithLeadingDollarSign_mADECBFF91824081CAE7961C3F64B5B5BE0FC2BFB,
	SR_get_MultipleMembersBindWithConstructorParameter_mAEC260022C23BF38ADCA1B767B60B53E8875D572,
	SR_get_ConstructorParamIncompleteBinding_mC581DBF82C64641E6BC0B40CC8DA5CB47E6D7A88,
	SR_get_ConstructorMaxOf64Parameters_mBB5AFF1FEDD827305F95B3117F73C460627249FD,
	SR_get_ObjectWithParameterizedCtorRefMetadataNotHonored_m8996B39C9B5E96C4A95B624B0548B660F6391713,
	SR_get_SerializerConverterFactoryReturnsNull_m3118B7812A487C6358D67C3FCB52D9037459F63F,
	SR_get_SerializationNotSupportedParentType_m07FAC518AD295B0A26B231A1BCA47442AB6B3D9A,
	SR_get_ExtensionDataCannotBindToCtorParam_m420FD3FD24F813F00880D64F68AECA269125BE47,
	SR_get_BufferMaximumSizeExceeded_m431F865C69355E6C41A8E3F80FFFBF7AC3FA4EC8,
	SR_get_CannotSerializeInvalidType_m7C952A313DC1993F9B3232F38D6C80D3D046678B,
	SR_get_SerializeTypeInstanceNotSupported_m2AB8B23DAF1CD7C6AEBDE5F2CCDE4285F1575ACE,
	SR_get_JsonIncludeOnNonPublicInvalid_mF62600692DFAB089F35D4F093D1E87A75BC25B0E,
	SR_get_CannotSerializeInvalidMember_mBD7FBF18C19F170E88B5992329F0ECC8422BA434,
	SR_get_CannotPopulateCollection_mB97D398A7585F7EB1436376EE5878C80D9DCB5FC,
	SR_get_FormatBoolean_mE269628FC3DFBA7EDD02281BE138A1C3CE081DC1,
	SR_get_DictionaryKeyTypeNotSupported_mDA7E2E872C601CC7EB461C2C484FEBCFE92556AB,
	SR_get_IgnoreConditionOnValueTypeInvalid_m7419DD1B9527C4EE15F293E640157C4B1D4D6AF2,
	SR_get_NumberHandlingConverterMustBeBuiltIn_m27905EE924069B6B7F7075BCAE99EDB79F97EF93,
	SR_get_NumberHandlingOnPropertyTypeMustBeNumberOrCollection_m8A2EADDB6529F46977892B674E769ED6EC2A2C4A,
	SR_get_ConverterCanConvertNullableRedundant_mFB45D61F9A1E72FC84765E2221D49BA9245E027B,
	SR_get_MetadataReferenceOfTypeCannotBeAssignedToType_m7F1BF09FD2E7834FE2710A120298FCCDFA1C2CA4,
	SR_get_DeserializeUnableToAssignValue_m8285893FC06745EF5DAF15239A53254EC2EF0642,
	SR_get_DeserializeUnableToAssignNull_m255E8E315DEE64BA5151D37E7DA15F4EBE971CA0,
	SR__cctor_mE3BD13B00C21BC7B674A7F3CC07024B3553DC202,
	ReferenceEqualityComparer__ctor_m34B573993FD142FE3743042DE404EE5B460B87F7,
	ReferenceEqualityComparer_get_Instance_mD073B163F8FCF8802F3DD0710ADE60E4AE9C70D1,
	ReferenceEqualityComparer_Equals_mD317F0D5B41F522C68D89521F8B9BE893846886D,
	ReferenceEqualityComparer_GetHashCode_m1D4FDFFFD7E03A6556AB19067C3C22798B96FE1A,
	ReferenceEqualityComparer__cctor_mF61AE9229CB4EE4FDB0A7F1D8F10062C66F837F9,
	DynamicallyAccessedMembersAttribute__ctor_m09C8E63DE16193F2BF50CA9B0013974A2BD3F117,
	DisallowNullAttribute__ctor_m219EE54312A960E5EF2A2EC11B8CE905B2E74AA5,
	MaybeNullWhenAttribute__ctor_mE1D907652B18CE48FAF3ED10DA1C5B2E4394C64A,
	NotNullWhenAttribute__ctor_mBA27378DCF373BEC96DA61866A9B2ECA6E4A9963,
	NotNullIfNotNullAttribute__ctor_mBE4276E7861F6AFF93745199B2CA3E919DE0CB2E,
	DoesNotReturnAttribute__ctor_m147B1C7E5EF5B98AB3161A70862E2F53A233F5B3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PooledByteBufferWriter__ctor_mA2CDADEA601F4B449CB29C59BC45CB30F460F895,
	PooledByteBufferWriter_get_WrittenMemory_m9EA1D7168A9612DF33F3DE0F679F3B8B7A0632A0,
	PooledByteBufferWriter_ClearHelper_mBD6BA55E55056AC390D8C66A4F237243D6877D61,
	PooledByteBufferWriter_Dispose_m8706065BB3DE3495BA0AF0900DC3E8B065851BDD,
	PooledByteBufferWriter_Advance_m2D5F5B7394CC615CE4B53CC4BE579A093014586E,
	PooledByteBufferWriter_GetMemory_m23D69042BE9652FBB2A39785F104CBF30F17D353,
	PooledByteBufferWriter_CheckAndResizeBuffer_m4D69B85CDDB2E57BD02D2E2C10B29376B282B476,
	ThrowHelper_ThrowOutOfMemoryException_BufferMaximumSizeExceeded_m79696E7BFDA49E5D410358EA30E5A82D093BF59D,
	ThrowHelper_GetArgumentOutOfRangeException_MaxDepthMustBePositive_m750E289BFD3B7A66BD128EAEFEEB57B3373D55B0,
	ThrowHelper_GetArgumentOutOfRangeException_mEA1F2D0D8BB8DC705E57649C1F766FC5B218EEA1,
	ThrowHelper_GetArgumentOutOfRangeException_CommentEnumMustBeInRange_mD4C28C947D91AD6C634C592231BD52C910C0802C,
	ThrowHelper_GetArgumentException_mACA6F87FA5A34AAAB64CF58A133D986308CD2FC5,
	ThrowHelper_ThrowArgumentException_PropertyNameTooLarge_m591F53BAC4D2B10A55D1389C8A1A907157A0E63E,
	ThrowHelper_ThrowArgumentException_ValueTooLarge_m98E94862DE95E1CD322A446B4B845268A124C055,
	ThrowHelper_ThrowArgumentException_ValueNotSupported_m579817B44127511B903F738EBBC7D02FDE054F53,
	ThrowHelper_ThrowInvalidOperationException_NeedLargerSpan_mCCE8D5F2D1147F2E3BDB377D4916507B12561F28,
	ThrowHelper_ThrowInvalidOperationException_m7D22016093BF6ECF1EF1B98DD672E6D7FC51ED70,
	ThrowHelper_ThrowInvalidOperationException_m8DEC1F901C1321582AE466E5C280EFAB23511C4F,
	ThrowHelper_GetInvalidOperationException_mEEE0CBC4C08A945CD63041ACD80775F5056FD394,
	ThrowHelper_GetInvalidOperationException_ExpectedNumber_m5EEA4C741057DC0BF0CBAB746B63B1131955506B,
	ThrowHelper_GetInvalidOperationException_ExpectedBoolean_mA8F271F7AAD4502C3E7FD563A6258A0B2AB93326,
	ThrowHelper_GetInvalidOperationException_ExpectedString_mBD6F5B8CC77AC75D80B1FBEF9AC5B88B32C16317,
	ThrowHelper_GetInvalidOperationException_CannotSkipOnPartial_m29D6936DDE65B674D1262C02F7FFCD168EFA6F0A,
	ThrowHelper_GetInvalidOperationException_m5FE9606EDE8F607F2F4661CEF82080D5DEE35597,
	ThrowHelper_GetJsonElementWrongTypeException_m86CA67C05608A212950C235A8BBBA936B2B7A004,
	ThrowHelper_GetJsonElementWrongTypeException_mCB63BC840CD43F6D8F13CCB3023ACD9287E21C1C,
	ThrowHelper_ThrowJsonReaderException_m86265B8897AA1A8AA169F05BA7421ECF0260EE82,
	ThrowHelper_GetJsonReaderException_m64F661A9A7D85E2596AB156BAF319034CA4BDFF3,
	ThrowHelper_IsPrintable_m2E0D5128B77C215AE99AC6D18A162ACE1A71AC20,
	ThrowHelper_GetPrintableString_m40E31A72DA7DFAA32ED8B7A06A6609797EA0F551,
	ThrowHelper_GetResourceString_mE47C527A4103CFD253D1489FD20B7AF1121BC3FA,
	ThrowHelper_ThrowInvalidOperationException_mE97A968AF5F73C0C37D09D7C9AAEB27EF4785516,
	ThrowHelper_ThrowArgumentException_InvalidUTF8_m61926CFA2D24DCE662F6BC2B2D1A2E4B66B98498,
	ThrowHelper_ThrowArgumentException_InvalidUTF16_m063FC3F74E3C16AAC9D6C6F79AFC414D443EB4DE,
	ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_m4A32F5D71AC0CCB638A23C7D369287B7FE350B25,
	ThrowHelper_ThrowInvalidOperationException_ReadInvalidUTF16_m891B6B9BC2208ED7FE09E655D0D6A9880E90D392,
	ThrowHelper_GetInvalidOperationException_ReadInvalidUTF8_mE44B06A595A70AF59AF82E964F4234B189AD8CBA,
	ThrowHelper_GetArgumentException_ReadInvalidUTF16_m252AF38593FFB0FD4E807A9F0D2DCAD0656370E5,
	ThrowHelper_GetInvalidOperationException_m32A1F2595A56602FB6FFBB5241AABE3FBF959773,
	ThrowHelper_GetInvalidOperationException_m47C288150449D789996BE18DBD7DCF2D5DFD2393,
	ThrowHelper_GetResourceString_mA42609E40D339ED9AABB16A1923814DC4841AB9D,
	ThrowHelper_GetFormatException_mFB5DB5DD18152932CDB9D3EC5229F1F4A659F88E,
	ThrowHelper_GetFormatException_m544F59063DA3B19472A42E104FA8DB8738F19C98,
	ThrowHelper_GetFormatException_m01DB51AA6723798546C757FBD716FB83AD457EFC,
	ThrowHelper_GetInvalidOperationException_ExpectedChar_mA357290B52AE1CCBE21E4D44D81798306B15131A,
	ThrowHelper_ThrowNotSupportedException_SerializationNotSupported_mC24229F331EB2F60F8B977CE5BB4BAFAEA326828,
	ThrowHelper_ThrowNotSupportedException_ConstructorMaxOf64Parameters_m4CF7BD9AAE68BE7A348E93A2FEB9E58E13BF9743,
	ThrowHelper_ThrowNotSupportedException_DictionaryKeyTypeNotSupported_mF60A3FF91D6CD3C9FB8FC242A3584658A59ABCA0,
	ThrowHelper_ThrowJsonException_DeserializeUnableToConvertValue_m45C862293B0F080C12A4EECB3084FBF0B8CDD83D,
	ThrowHelper_ThrowInvalidCastException_DeserializeUnableToAssignValue_mEDC3F06B4CE394CA1F7FDC3FF2E9902B02173DC5,
	ThrowHelper_ThrowInvalidOperationException_DeserializeUnableToAssignNull_mEADD0C71C1896AB70526F02F16CCA9DF430A427B,
	ThrowHelper_ThrowJsonException_SerializationConverterRead_m661AF889A7B9F40C97180860B0CCA8117410C1C9,
	ThrowHelper_ThrowJsonException_SerializationConverterWrite_mD4203A8BB078A26D1F944BAAB79C87BBB5650179,
	ThrowHelper_ThrowJsonException_SerializerCycleDetected_mEA299568BAF8CA18E6A119E98ED7714D41FB0E38,
	ThrowHelper_ThrowJsonException_m8A46CAA703623FA179B048050F106600A3722FE8,
	ThrowHelper_ThrowInvalidOperationException_CannotSerializeInvalidType_mBA5982C757C417BE964B629A43A9CEF2124BA660,
	ThrowHelper_ThrowInvalidOperationException_SerializationConverterNotCompatible_m551F2D6AF89BE3277AB0C3E2D619D9A60B8C2187,
	ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeInvalid_mBC7789EBE47721D6E34286B5F42EBF8F490E5236,
	ThrowHelper_ThrowInvalidOperationException_SerializationConverterOnAttributeNotCompatible_m2DF8EE86D37F018A4CE6E02074C89CCAAC9D6E24,
	ThrowHelper_ThrowInvalidOperationException_SerializerOptionsImmutable_mFC63B8BBB752B3F456704C8B4FA9A0369804D896,
	ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameConflict_m468997F3DEF6CA979BB5F162D8BA858A51D02C2E,
	ThrowHelper_ThrowInvalidOperationException_SerializerPropertyNameNull_m5CFD2A6DEA5E46DEDA5366C029D1FB7489A82E40,
	ThrowHelper_ThrowInvalidOperationException_NamingPolicyReturnNull_mB518C297AE2F92A385AA8CE79EDA0C152A0FC046,
	ThrowHelper_ThrowInvalidOperationException_SerializerConverterFactoryReturnsNull_m3D01132E2B95AF5852EC9EBA9D692B23E7FF622A,
	ThrowHelper_ThrowInvalidOperationException_MultiplePropertiesBindToConstructorParameters_m4DAE587048436F628F4628A61230CC083F397AB7,
	ThrowHelper_ThrowInvalidOperationException_ConstructorParameterIncompleteBinding_m04767FC2344112C92B1A6CD19126C73F16C70426,
	ThrowHelper_ThrowInvalidOperationException_ExtensionDataCannotBindToCtorParam_mF02345D3CC2FCA07275CB3081D6A081474A577B1,
	ThrowHelper_ThrowInvalidOperationException_JsonIncludeOnNonPublicInvalid_mEBF3157BA9B05074D4C9EB8D24E89592FDB8E354,
	ThrowHelper_ThrowInvalidOperationException_IgnoreConditionOnValueTypeInvalid_m22375B31A81C111155F76CD369373E22E9F5BC0A,
	ThrowHelper_ThrowInvalidOperationException_NumberHandlingOnPropertyInvalid_mDA9C9F343B33690C02EB21BA707294E8DF79684D,
	ThrowHelper_ThrowInvalidOperationException_ConverterCanConvertNullableRedundant_mD3EEE3DFDF279293BDC4F46C71609D731929032C,
	ThrowHelper_ThrowNotSupportedException_ObjectWithParameterizedCtorRefMetadataNotHonored_m4A50791211BA9F37D478B0488D6D6EFD102BA1CD,
	ThrowHelper_ReThrowWithPath_m78EE5E489A425E56E74E87DBCE71776EF7BA2808,
	ThrowHelper_ReThrowWithPath_m878D9F148EB22BFBA260F8DD9F2D15A28F03F843,
	ThrowHelper_AddJsonExceptionInformation_m93DA5D5ECE14280C710EA2CE1960B7FA22ABAE03,
	ThrowHelper_ReThrowWithPath_m0807C880321B63D0277EFF7BE510A1F0FBD21A57,
	ThrowHelper_AddJsonExceptionInformation_m112F77F7E113AD4E3C1F5F86117CE950EFB1F934,
	ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateAttribute_mD696CFD27EA6ABDC13DCD058D698C0E94B9E6E2E,
	ThrowHelper_ThrowInvalidOperationException_SerializationDuplicateTypeAttribute_m893BBAB25A9BCBB6161AC6D3C6CB43FD902D0216,
	NULL,
	ThrowHelper_ThrowInvalidOperationException_SerializationDataExtensionPropertyInvalid_m5EB0D9D7A1ED0FA5D31729E323FCB04D3DB23735,
	ThrowHelper_ThrowNotSupportedException_mE938772C865354567463F8EB9643524AC591A67E,
	ThrowHelper_ThrowNotSupportedException_mF66B063BE62D70EF91197DEB39C5BF34D05DAB7D,
	ThrowHelper_ThrowNotSupportedException_DeserializeNoConstructor_m3B8416576117E4CF29B7466BBFFDE06F08BE0E5E,
	ThrowHelper_ThrowNotSupportedException_CannotPopulateCollection_m545753EB67DF0A42857B280D6B3E35EC82CC7812,
	ThrowHelper_ThrowJsonException_MetadataValuesInvalidToken_mC4B686B0F521BAD7FFF91AC86135B3461D048731,
	ThrowHelper_ThrowJsonException_MetadataReferenceNotFound_m8BE143B132A6882F7DF48C413E40C2845E2E9B5B,
	ThrowHelper_ThrowJsonException_MetadataValueWasNotString_m0256A7850A68C37BFCD0DC892D510785CA9C2882,
	ThrowHelper_ThrowJsonException_MetadataValueWasNotString_mD81E092228AF69DAD1EDB6707B3AD1B81765735B,
	ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_mDD09A1CDEECB496920D33515903B54FB816CD6CE,
	ThrowHelper_ThrowJsonException_MetadataReferenceObjectCannotContainOtherProperties_m1F2786A4970058AB222A15EBDC5A8B21733797E3,
	ThrowHelper_ThrowJsonException_MetadataIdIsNotFirstProperty_m599DC6EE20D8CE9ED6FCE9F3C17C4FC7AA71FC52,
	ThrowHelper_ThrowJsonException_MetadataMissingIdBeforeValues_mD247FCE14C0D9E3BAB67B6614029B082F841B3B1,
	ThrowHelper_ThrowJsonException_MetadataInvalidPropertyWithLeadingDollarSign_mECFAEA7A557630248C78CB073637AC76CB6FF722,
	ThrowHelper_ThrowJsonException_MetadataDuplicateIdFound_m11947A1B06CBC9C4035D6DC610417AF31D6005FC,
	ThrowHelper_ThrowJsonException_MetadataInvalidReferenceToValueType_m9D05E3528B59C04A67B2C9125CBB17C2FA144AFC,
	ThrowHelper_ThrowJsonException_MetadataPreservedArrayInvalidProperty_m889853362A58AB715BB4FFB0C287A993D4A72FDD,
	ThrowHelper_ThrowJsonException_MetadataPreservedArrayValuesNotFound_m88AE8B99500E8629944E0F935D7EFA5560E31070,
	ThrowHelper_ThrowJsonException_MetadataCannotParsePreservedObjectIntoImmutable_m8781A043C3D02AD141FFFF8A70DE7C2DADA595F5,
	ThrowHelper_ThrowInvalidOperationException_MetadataReferenceOfTypeCannotBeAssignedToType_m0D5C9B04DBCFFFB1EEBF400B90519B14EA822EA7,
	ThrowHelper_ThrowUnexpectedMetadataException_m052DF778174E9595C074513E1256F4CF9B8ADDC9,
	BitStack_get_CurrentDepth_m29C47DB4A4B69D9C76A93EBA1637E97A243FC869,
	BitStack_PushTrue_mBEF1D3C195EA821E02716255C61AFB88258D5BAB,
	BitStack_PushFalse_m83CDC7E063685BF4FAD27FDCD7F51CF8D49EBAD3,
	BitStack_PushToArray_mE3486049BF012A03170FBC4C4BF1CE139F81F6C0,
	BitStack_Pop_mEBB94B5C8DD6FACC49DCB2A2A7D365E5A75A4FD2,
	BitStack_PopFromArray_mC5F5242B631329764A700C94FC08270FE1803691,
	BitStack_DoubleArray_m46DB748394540255EF86FBD4970988C81DF3395F,
	BitStack_SetFirstBit_m9263D9E5C36F9375CDA674AC231BCF1FDF858472,
	BitStack_ResetFirstBit_m71CD0C8BD6150A422ABAABF746E6F0EC53FD8359,
	BitStack_Div32Rem_m2EF455EE5BC45D67DFEF697EB03E0C7F53D9592F,
	JsonDocument_get_IsDisposable_m212A389096307FEAD14943ACE082EE06653D22EA,
	JsonDocument_get_RootElement_m40BA0A219E870618E0E6DD88367CFB484F2C5195,
	JsonDocument__ctor_m84EA59E1DB006199769A7D58D844F7A86B08173F,
	JsonDocument_Dispose_m2CEFC6A03073BF3504F21DEA2E09E19042383ED6,
	JsonDocument_WriteTo_m3E1525459DB06CB769488220F558516BA1E3DFDE,
	JsonDocument_GetJsonTokenType_mB89F7B88FE549403D51C44AE479C23F511571BA9,
	JsonDocument_GetEndIndex_m7649DD254640741203DDD17105B1BD42F1ABA2BD,
	JsonDocument_GetRawValue_m041DE8FDAF9BBF04627C80067AB84EF58A1C48F0,
	JsonDocument_GetPropertyRawValue_m136F2D737AB4DEB3BDE96250B4F93C61CF0711B9,
	JsonDocument_GetString_m67D5DF0C839EF45BC9C1B800981786AB46EDDA27,
	JsonDocument_TextEquals_m7C64ABF5685A8004EA85D98366FDDED93463D775,
	JsonDocument_TryGetValue_mA6324944BBAC3741B7CB48ADB8CF13EC53D14677,
	JsonDocument_GetRawValueAsString_m81007913ABD82375532603DFD76454D7CA784108,
	JsonDocument_GetPropertyRawValueAsString_mC5D5361947421D454D9EA948160BDD834F2A7D31,
	JsonDocument_CloneElement_mCA0507E07D125635120AE95AD28206686C61B712,
	JsonDocument_WriteElementTo_m6B43E581F2E379DF5F53E09AFF9E5992CF544C21,
	JsonDocument_WriteComplexElement_mEEDFB2BD62CF0D2CB4AB76FFAA45459F493CBE42,
	JsonDocument_UnescapeString_m3426A69B8BA315520180E6FBEE38905E213EFB79,
	JsonDocument_ClearAndReturn_mEA37B5007D8C568187F058DEAB1C3C4506D06CC8,
	JsonDocument_WritePropertyName_m5377F1DD3708153B4E0841F81C1DCC39C01A0E1C,
	JsonDocument_WriteString_mBFC04E9D7FEEE29A2EC49FD91CE60AD096BDA3D3,
	JsonDocument_Parse_m88DDF34A946810A62CE6560350C85D2B9E8F270E,
	JsonDocument_CheckNotDisposed_m406A04C85C33DFCF4FF505BEF37E88CE399583C0,
	JsonDocument_CheckExpectedType_m354D00E8EB7C96836FB54421A7F35A007A2FEB36,
	JsonDocument_CheckSupportedOptions_mB43D28A15D6478A0954926BC4BF12ED1D9FE457C,
	JsonDocument_Parse_m31EAD3B41D1A91C11B685C0133863F61628B65AC,
	JsonDocument_Parse_m290785DE223664D929CAC7888BAEE7693B60DFC2,
	JsonDocument_ParseValue_m088FB4B1ACC350C2FED324556EC221C06C6CB786,
	JsonDocument_TryParseValue_mFAE1CD76E9A91AE946FF037CA0A3BEAA5C2A102A,
	JsonDocument_Parse_m4DB8A56DDE364571F37E232634DDFEB6B991F271,
	JsonDocument_TryGetNamedPropertyValue_m19024E1EA661AC1CB7758B34270A236EEB7A00DD,
	JsonDocument_TryGetNamedPropertyValue_m5942E503498F83A6D769CE0360EFA5DC1624200C,
	DbRow_get_Location_m26C010A9C88629FBF5B9BE4AB8DA90E22422CFAC,
	DbRow_get_SizeOrLength_m84D8BF651739FC1923CCEA2500DF7D9929DD8ACF,
	DbRow_get_IsUnknownSize_m72AF174FA7DFE73898270932D83924CB0DA5C52A,
	DbRow_get_HasComplexChildren_m8900AC0494A74A8EE1BCA197C6223110CF35B997,
	DbRow_get_NumberOfRows_m1C82FF779E2D2653D42E81A38364AE4CC2B7DCAE,
	DbRow_get_TokenType_mCCE39C67D2D66F0053D567BD45AD095D34977063,
	DbRow__ctor_m5A251F6E7871A6B66B5CC2E73937621035F95178,
	DbRow_get_IsSimpleValue_m2038B00EE08F425ABCD2BC3C24A151E7EDAE3927,
	MetadataDb_get_Length_m22933560D56DEB73CDA8422130DFB5A6089E523C,
	MetadataDb_set_Length_m1E5804A10F184287716024455518919240C3E635,
	MetadataDb__ctor_m39EBC88D82771277ACD7B74A804CC43263D5B706,
	MetadataDb__ctor_m032FBF24011241945608F14DD9D7A12DB7CB8535,
	MetadataDb_Dispose_m77CE10F7F04101B702C044AC728B6B6E8E912569,
	MetadataDb_TrimExcess_m990B41FACFAE4EC6756F75A83B7130A8FD6EF21C,
	MetadataDb_Append_m2537E212C18338E2810CC9B80A5DD82B897DAFE7,
	MetadataDb_Enlarge_mDE37991365E52CA27B9FAE0F2A8964AA102BDD5A,
	MetadataDb_SetLength_mFC66B57EF58895AFFE2DAD33297935F63C5B8F79,
	MetadataDb_SetNumberOfRows_m5D37816F2AD652AB316EFBE4B0A2643457B72C82,
	MetadataDb_SetHasComplexChildren_mB746E8ED47FF55757A24BD397A0E4D91E18F8EAE,
	MetadataDb_FindIndexOfFirstUnsetSizeOrLength_m9BB4939039E8059B31868E36E84F888B26BAE35E,
	MetadataDb_FindOpenElement_m8CA68EDBB9605702DCA7BB048DC51689A38ADCB7,
	MetadataDb_Get_m2EE4C63C4D6B477C49D85DA90D6CFFC009A0B662,
	MetadataDb_GetJsonTokenType_mF9DF07B766B9E488CD4A203A1F0BE3D09551A41E,
	MetadataDb_CopySegment_mF7BE8719D6FE1E244B55E9E3251855DD065A4B1E,
	StackRow__ctor_m2A8EFF70E90A96B442084512D9788BBA144AA25F,
	StackRowStack__ctor_mA8C386AAC2C88F726223EF8F9CED4C3E79948976,
	StackRowStack_Dispose_m442EF445F037EC392B2F1494ACEFF8D4E6B2A052,
	StackRowStack_Push_m4F9FD4AE22507CABB234B17F9D72F52F0BF9215A,
	StackRowStack_Pop_m59CEFB7385936EBFB490BCFEB5001B63F6E79B31,
	StackRowStack_Enlarge_mB7433A4A8E2C398B72C14512663278632CA70D89,
	JsonDocumentOptions_get_CommentHandling_m9486C1460A2DC728E08C0E0FAED390C30503FAC7,
	JsonDocumentOptions_get_MaxDepth_mB728B90176B962CD67750F71D6AF5FCB816CBF15,
	JsonDocumentOptions_get_AllowTrailingCommas_m03274EEEEC6A5BC79CCCE3E89FCE91179F7DE313,
	JsonDocumentOptions_GetReaderOptions_mBC8F60D77D1AE7A350DFF94D8869A6177015540A,
	JsonElement__ctor_m8E42B93E64E1C54BD78FF8163CD5C482E0EAA44B,
	JsonElement_get_TokenType_mD6DC1A601E15744E583F39CFEAED4C161168C456,
	JsonElement_get_ValueKind_m14EFE30FAA112F5199CBD9C42DC9C3AEF6ADA1B5,
	JsonElement_GetProperty_mB0BB5EA6AC89BB64A9F3BD39EE474AD2D26F72D7,
	JsonElement_TryGetProperty_m11C90F70F1EE0DA94CD3E752FD0259C599687D46,
	JsonElement_TryGetProperty_m7322A7C18BB189A5E607ED747340376EB5FC173E,
	JsonElement_GetString_m7AE007D2F1B4016AA1B53BF79B1A1DD1EA42EB94,
	JsonElement_TryGetInt32_m5FCAA7B399C4469AFFE24400FDDBE78F8C60041E,
	JsonElement_GetInt32_m21DEB1B177269FFB57C09E9B094DF8C719926A73,
	JsonElement_GetRawText_m054A9C6C86FA951676F21FEB0ABAAEE0835A11EE,
	JsonElement_GetPropertyRawText_m575B8D8F5C386BE964EB43964706F19BC0E72233,
	JsonElement_TextEqualsHelper_mE1DE405E05A250F42B364021CDE5BDD629E7C6F5,
	JsonElement_WriteTo_m8802C6ABB5C038C5E111274B0FED5702BFB72B10,
	JsonElement_EnumerateArray_m9B35AF71578078459E6DAF2D7416D07F327E6E38,
	JsonElement_EnumerateObject_mD4E563B682ABDCB35E2D27A573BB903CE3EA7D76,
	JsonElement_ToString_m9E0942575D303AD18247677C192C7E065AE83634,
	JsonElement_Clone_mCD2B3D79FE552E8FA4A29C9244675D1926B7EC50,
	JsonElement_CheckValidInstance_m175423F0C65D579DB09D5B1C9ABE2E643064DC8E,
	ArrayEnumerator__ctor_m1D7B7BD7496B0CD223EE9399B5A57725AEEF7381,
	ArrayEnumerator_get_Current_m5F5C30DD80B5F2331BF392DA21D4EB06E050F827,
	ArrayEnumerator_GetEnumerator_m938DD30A37AEE868DC5935D0CDE254FA6CAFCF97,
	ArrayEnumerator_System_Collections_IEnumerable_GetEnumerator_m2E636672376124E9810A741FED6ACE7BFD20939E,
	ArrayEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonElementU3E_GetEnumerator_m9ABEE9A87DD3D50D065C9738B683D691A143539B,
	ArrayEnumerator_Dispose_mD4E9E4516CF8CC2CEA173A59B8407A94271C34D2,
	ArrayEnumerator_Reset_m3DB230A1965DE1E4F9DBEC7BF58635D2AC1FA823,
	ArrayEnumerator_System_Collections_IEnumerator_get_Current_mE0067C8DCE7157F9C9695C3FF3FDD5CEED826422,
	ArrayEnumerator_MoveNext_mF1B0AFD404AA0995A6BE8106CE602684BBE47433,
	ObjectEnumerator__ctor_m703D9D5B13C21AC9E29F92FAB758D52A102C8F01,
	ObjectEnumerator_get_Current_mC1865163B29F8CBEAC81A2E5C4A26AF3B997C7BC,
	ObjectEnumerator_GetEnumerator_mEE9CACFE7559A6D381D802909FE21EDA46FC103C,
	ObjectEnumerator_System_Collections_IEnumerable_GetEnumerator_mA2F0FD6FAB9BEF8BA08389BE66483F44BAF90BC5,
	ObjectEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonPropertyU3E_GetEnumerator_m2F6759C0BBDA3C73CB81373048E97DE7E10D0AB0,
	ObjectEnumerator_Dispose_mE6013E4D463D4DFAE6D9A4458393358F84F4115A,
	ObjectEnumerator_Reset_m29B204531B2E013AE91F1B6CD394EC9E32447E5C,
	ObjectEnumerator_System_Collections_IEnumerator_get_Current_mC740D819F66BC7DB0738A495920A650C31444B56,
	ObjectEnumerator_MoveNext_m7D620190FB99A0075BE003767FC70105507DA629,
	JsonProperty_get_Value_m6ED717E9179F9D4A00DF42D36A191A8E2BB8C402,
	JsonProperty__ctor_m3447EAB80FB5BA6813D3F9F18EBAD08B7FCFB312,
	JsonProperty_EscapedNameEquals_mF067EB713ECF2C089B42447941C7A34C50C02352,
	JsonProperty_ToString_m9F70631770655A6F3B6AEAA2C3717D17A1D0F573,
	JsonConstants_get_TrueValue_m7875CFE6FDF00788A7122ABE0D7EE9734C4909C4,
	JsonConstants_get_FalseValue_mF6E16F3E048CAA53A90759098574839A72FB4381,
	JsonConstants_get_NullValue_mA5886DDBB16FC136D274B471F912CEEC7A9847AF,
	JsonConstants_get_NaNValue_mBD4A12A27DC7CAB18F7BD8E0B46268074B646B8E,
	JsonConstants_get_PositiveInfinityValue_m15BAD40B6D9D8BC432804DA52F246A8673007722,
	JsonConstants_get_NegativeInfinityValue_m6118A86711FE3D1277570ED96C7D89007376ADDD,
	JsonConstants_get_Delimiters_m2173CDD642F01C58C897DCB96B27183F35B1A1B7,
	JsonConstants_get_EscapableChars_m54662DEE4FD6B912A08DA47422AD6A4544CDBC8D,
	JsonEncodedText_get_EncodedUtf8Bytes_m7D24410A44ADF371EDDBA9059403D34F98B463A1,
	JsonEncodedText__ctor_m7F8DBA12F3C948E827627FF1300E95AA48E42CEA,
	JsonEncodedText_Encode_m2861A2FED41292077761CCC009958C8D5371F988,
	JsonEncodedText_Encode_m3F96860D13424084A29B9D1B6C9925F35FEC3046,
	JsonEncodedText_TranscodeAndEncode_mB7FDCDAC54BE481FDF2EFA0A5026D258383616DA,
	JsonEncodedText_EncodeHelper_m4D3F6AACFAC1CCB0CBAE0B0B66A20964BF5DE98F,
	JsonEncodedText_Equals_m26E3AB94F984E7BAE83FB67FD47A7C5A0BB116E4,
	JsonEncodedText_Equals_mE176157268A0949E8C234E0175B00F9FF800B6C9,
	JsonEncodedText_ToString_mA0D2C5B818AE0E302B31B5FB722ADBFA30F5BA8B,
	JsonEncodedText_GetHashCode_mCFF493ABAB2853A884DE0BCDFBB1BC35BE1C04A6,
	JsonException__ctor_m1C432F1AB17DC969900631573632DB232CB33047,
	JsonException__ctor_m1E761A74D58F7F5B1CBB73BA03385A15D72B533F,
	JsonException__ctor_m40BA7C406CA01A0E34788450DF7D370FF7532DE9,
	JsonException__ctor_m728B5CA35DCEB2546A6B4BCEB0149C2152EEFD49,
	JsonException__ctor_mDEC3E59EFA840EC27C3946B5B9E2075098A5625B,
	JsonException__ctor_m31777501B4846AC4AAC92FD7CA8BAA5BBC3EA1DF,
	JsonException_get_AppendPathInformation_mA5B0D2C79D49996A94B3D1F91CD212B722B24582,
	JsonException_set_AppendPathInformation_m3DDECA6D2488441D7922477ADF57A49DA1F90605,
	JsonException_GetObjectData_m8E0016AB0548FE7A8BDF35FAEE8E8705EC13E390,
	JsonException_get_LineNumber_mDF9F960ED9C7280C7B11E844A5BAD60480DB4EDA,
	JsonException_set_LineNumber_m6C62627B0C62AD93C308E8EC768A92061394DC78,
	JsonException_get_BytePositionInLine_mE44FEC698C6386B30A3BC61394AF59E9B5EA5B0C,
	JsonException_set_BytePositionInLine_mC4D58939BBBC83722253ABA6BDE1A83C232397C4,
	JsonException_get_Path_mEEBE83065DDE2080206A9FD4543C296CF08AEA3C,
	JsonException_set_Path_mB5FD61A615FEF3A1CB865A643014E6D0AB55A32B,
	JsonException_get_Message_mD662F1821A73A4F62CA214ED78E53995819682EA,
	JsonException_SetMessage_m30E6A9D577ADCE49E7E6EC886351CFBD9684C540,
	JsonHelpers_GetSpan_m094FA1F64075E4554D87D94CC6D1971C4961FB8B,
	JsonHelpers_IsInRangeInclusive_m5A57568AE3106614F199BB8CA2D86C636B709020,
	JsonHelpers_IsInRangeInclusive_mA3EBC817F38C1240438A438AF5D7F1AC0D1179D3,
	JsonHelpers_IsInRangeInclusive_m218E16DFBF192F48B74D08EBF9384E45FBADB02F,
	JsonHelpers_IsDigit_mEC357A2F49C864A81517680770AAD23F8EF84689,
	JsonHelpers_ReadWithVerify_mC62AC4ED73712CFF23DB3E2A432E7D25EF71A86F,
	JsonHelpers_Utf8GetString_m76C5E13DB097957F1383ECF2C351ED4D6D10AC9A,
	NULL,
	JsonHelpers_IsFinite_m7A314B8340B765C430A9D82C6BA0E299F8BBFF77,
	JsonHelpers_IsFinite_mAE82735137E005E75F6535A32F4C0E1CF28B9D5D,
	JsonHelpers_IsValidDateTimeOffsetParseLength_mF4C79BB778330902E5250C000092EE723722F5C4,
	JsonHelpers_IsValidDateTimeOffsetParseLength_m46C66CF4904AAC707A86D55F2D1A5E97835034C9,
	JsonHelpers_TryParseAsISO_m2507DBB2887E5A78083371D322098729F0EF1399,
	JsonHelpers_TryParseAsISO_mDAFD46B648F3C83E7D027043C48171F3753E592A,
	JsonHelpers_TryParseDateTimeOffset_m96EC4831B67C0669D200F3C8F75DEB2CD71D5635,
	JsonHelpers_TryGetNextTwoDigits_m6E3351EBB772DD1CD455B346FC59CC07C90E9F65,
	JsonHelpers_TryCreateDateTimeOffset_mA2C13B99E1B0AF9A0C8610DBE6C127562A5B3C86,
	JsonHelpers_TryCreateDateTimeOffset_m8911B050AA61D053EF7DC6FD8A0394FB21F55CD3,
	JsonHelpers_TryCreateDateTimeOffsetInterpretingDataAsLocalTime_m7432A7F6E64532D7B9E231C000CCCD7366013D3C,
	JsonHelpers_TryCreateDateTime_mB28773F8B027544DACCFA3FC5724DAAEA152DDE5,
	JsonHelpers_GetEscapedPropertyNameSection_m783398E43BBF77DAED66330ABD8842AF57C0C007,
	JsonHelpers_EscapeValue_m3E2E9D0720AC01CEEEB056913005AB731D072B18,
	JsonHelpers_GetEscapedPropertyNameSection_m2CB915E336BB039CF3018EF219B3C5A18014349D,
	JsonHelpers_GetPropertyNameSection_m65A8494205AA2FBC7557A05CECFE9017F8CBEF7D,
	JsonHelpers__cctor_m093F39C2E6018C5F89AF682FFDE27F0D8A0CF4B9,
	JsonHelpers_U3CTryParseDateTimeOffsetU3Eg__ParseOffsetU7C21_0_m6E9B55C841FFDF04FC7AE12075C9FB911D9F90A8,
	DateTimeParseData_get_OffsetNegative_m6E163581952036148996FE989BA72F37B7178A3F,
	JsonReaderException__ctor_m14B8A4DFCA96A371826CF3FAD95FD304B599FE27,
	JsonReaderException__ctor_m397CF90939F7B7BC02AA246A16C297E27429F122,
	JsonReaderHelper_CountNewLines_m9B18D4780FC63C9B7787C34FE01282BDBB46A69D,
	JsonReaderHelper_ToValueKind_m1F17C11CB61DCAEF1D12E3B9E5D3C179B9A4C927,
	JsonReaderHelper_IsTokenTypePrimitive_m3875B80E4B968F0B3AB821DD40B93DFCE5D94284,
	JsonReaderHelper_IsHexDigit_m48B7245415C91527D78B34EE7488C0AAF632306F,
	JsonReaderHelper_IndexOfQuoteOrAnyControlOrBackSlash_m11D4FB3397E71F357AE1EF90148D6A48072B1795,
	JsonReaderHelper_IndexOfOrLessThan_m474697A56E76C9C5495A254F1FFE380EFF8F2A1B,
	JsonReaderHelper_LocateFirstFoundByte_m60AD06C7AE32AA2DCAC5C6BD7C549531370843F7,
	JsonReaderHelper_LocateFirstFoundByte_mEF86A92884308A164D027B38113917D8E115AC4B,
	JsonReaderHelper_TryGetEscapedDateTime_mB524532A1C43E4ED359E2BB16C0936CA160FBE3D,
	JsonReaderHelper_TryGetEscapedDateTimeOffset_m3C682D1CF044AC310FC84818273CB008FD8B3350,
	JsonReaderHelper_TryGetEscapedGuid_mD1C277DFD799FAE37646D5FD767A51D8DA2DEB68,
	JsonReaderHelper_GetFloatingPointStandardParseFormat_mB23C1B1EBBD4F4DAAC029F401D150785F6DB4AB5,
	JsonReaderHelper_TryGetFloatingPointConstant_m078B4EA3A481A4E6FF1C72800C4225E87038EA3E,
	JsonReaderHelper_TryGetFloatingPointConstant_m767E23B30F8253F20ED0268D73D737BEFD0D510D,
	JsonReaderHelper_TryGetUnescapedBase64Bytes_m04BFB37655BB206FCDE2FB38D53F7A071621B244,
	JsonReaderHelper_GetUnescapedString_m02CC3AA07EA7E8683C2D0E929934D33B95665E40,
	JsonReaderHelper_GetUnescapedSpan_m3A30EECA3C64BFC31103789E917A1B8C114F3D11,
	JsonReaderHelper_UnescapeAndCompare_m6E913E97842F4832A7CD91E4630431DCC0F8E85D,
	JsonReaderHelper_TryDecodeBase64InPlace_mD30B156D52BEA1265DA38A811C5A0B2CAFF66F5C,
	JsonReaderHelper_TryDecodeBase64_mF72B51F191B3EA1A36C7A6E1EDA281C6794E4547,
	JsonReaderHelper_TranscodeHelper_m7D2B2896AD860AE873EF6AA8F3C42DB2266D8F86,
	JsonReaderHelper_GetUtf8ByteCount_m10368357E772BAD837731771AB26854EBBFB916A,
	JsonReaderHelper_GetUtf8FromText_m08B25D4555626637C50AD6639EFDAA1643E4DAFD,
	JsonReaderHelper_GetTextFromUtf8_m8E608A928E234BA9AE70A69F77D7829BBCEDB9A8,
	JsonReaderHelper_Unescape_m78415A45DF58099511B97E969EF2BD51EEFB41AA,
	JsonReaderHelper_EncodeToUtf8Bytes_m6E588C811CA1B083C63540B60EF697E32C96FDC6,
	JsonReaderHelper__cctor_m090B44C9FF4641AF05F6203A860D178F63E7B591,
	JsonReaderOptions_get_CommentHandling_m785941C88C0D78EE452E47079133CA53EAA86918,
	JsonReaderOptions_set_CommentHandling_m3A14D93E8354AB403D202AC5352A0355DEF47641,
	JsonReaderOptions_get_MaxDepth_m2F0DA8E50E5D545945DB32DFFFA74E941849F3F6,
	JsonReaderOptions_set_MaxDepth_m2AEBFDFBFEC808E1E4902FFF561FAC990FD8420F,
	JsonReaderOptions_get_AllowTrailingCommas_m8D5027DF6AD1F91F789AC6AF9A7BF550E81B256D,
	JsonReaderOptions_set_AllowTrailingCommas_mB7C7B82225E31EFECAC172BC8CBF073C09A4036B,
	JsonReaderState__ctor_mD1D4FD65E3D005FD3B169BE5F949DCBCB638D5B2,
	JsonReaderState_get_Options_mC7B2AAB4CD55B90A1EB81F03B82BDEB9E8ED3894,
	Utf8JsonReader_get_IsLastSpan_m0D510710CF414FB8B8403E4051E06289C14E5B17,
	Utf8JsonReader_get_OriginalSequence_mFC7BF57B6E80D112834FAF24855EF4FC82F928B0,
	Utf8JsonReader_get_OriginalSpan_mCA0CC6DA6C7595E089AE833D3E8FAD70196ED6D3,
	Utf8JsonReader_get_ValueSpan_mD7C0AA41BFEC523AEF71D7AF4BFF784F84CC9AF8,
	Utf8JsonReader_set_ValueSpan_m37AB280FA0870B85790F31CC32FE61A145BB73DE,
	Utf8JsonReader_get_BytesConsumed_mEF9EB31417202F307C10482D117C0D1F1383746B,
	Utf8JsonReader_get_TokenStartIndex_m616B80F3D48063970EF25BC002FE2B7512948D3E,
	Utf8JsonReader_set_TokenStartIndex_m391AF219C657ED98FC368E82D7129EA13E4B744F,
	Utf8JsonReader_get_CurrentDepth_m09EED2F622F6DF3BE234674E7532337C2728EBA9,
	Utf8JsonReader_get_IsInArray_m5633237E6C12D3D53E4F623953D307DC58C3E4EE,
	Utf8JsonReader_get_TokenType_mCE6BF109ADE03F304F8C16D68AF519C5CFBA631A,
	Utf8JsonReader_get_HasValueSequence_m3EA84B4D984814A502FD58E70EC154B6DE181A5F,
	Utf8JsonReader_set_HasValueSequence_m16BA0D508AFFA53D77D2924821C4BFB400EACBA4,
	Utf8JsonReader_get_IsFinalBlock_m4FF1E669D83E57A86A0B60261A8027A3D2A9D8E5,
	Utf8JsonReader_get_ValueSequence_m881899F712004242C5F89AE5899A105AEBBC295F,
	Utf8JsonReader_set_ValueSequence_mCB1696984F939D6F8012728EA22DEC31D54F2B02,
	Utf8JsonReader_get_CurrentState_mA51B5336D4893034ED7344FED22DB0FFA6E7C796,
	Utf8JsonReader__ctor_mF8257CB8E2F367ADFA10F6A9B1B1E22A92039A75,
	Utf8JsonReader_Read_mD210940FACF194A87F83C11308A87397B77DCB07,
	Utf8JsonReader_Skip_m4DA7165BB21473A73328812E2541A62476BFBA5D,
	Utf8JsonReader_SkipHelper_mF4192AF21A1478DCD7D1313EC666F98462376E6D,
	Utf8JsonReader_TrySkip_mAA1EF79F9187B277465240CE3699015D57F85196,
	Utf8JsonReader_TrySkipHelper_m2DDAA3DB1C0BF9547970FCED4E3F7DE403489017,
	Utf8JsonReader_StartObject_mC83FBAF3E163D146444B2868D4AFCC959FA19F7C,
	Utf8JsonReader_EndObject_m5F1441EE29712895D6102136C79EF57D88B84A2B,
	Utf8JsonReader_StartArray_m6DA2C6FA69288410291C62C4A3976ABA06E9EC06,
	Utf8JsonReader_EndArray_m7B69D7C38934493393321F3A27365A4B67FBB4C3,
	Utf8JsonReader_UpdateBitStackOnEndToken_m384055041D18215523F5DCB0DC4F9BF8B564DA11,
	Utf8JsonReader_ReadSingleSegment_m947CD352F825A73A97962979917C1DE44CC2A548,
	Utf8JsonReader_HasMoreData_mF3B6BF4E12A6153B8D0696EB6E4DEFC68FC40B23,
	Utf8JsonReader_HasMoreData_m50F0F11A42D737D72C7BFA79D53F0133C69D1BC9,
	Utf8JsonReader_ReadFirstToken_mD90BB812B023F17A3EBBD30A3983D9791639372E,
	Utf8JsonReader_SkipWhiteSpace_m04627FB93ABE8F3D19F2EBB6EEBAE3E1F40B5E68,
	Utf8JsonReader_ConsumeValue_m846BBF871AFF435D23E18421514C2C3BF8CE156F,
	Utf8JsonReader_ConsumeLiteral_m89BFF61B6087D786F73E77F1D812F611126CAB10,
	Utf8JsonReader_CheckLiteral_mC50D33BAC348FD003A97DA41F7BCCE3BB0DB9988,
	Utf8JsonReader_ThrowInvalidLiteral_m3182F7C44BEF0C05A91ADFA8087F952FDFF2064A,
	Utf8JsonReader_ConsumeNumber_m40F72939079F09EA3A3F8C2FCE2E05FE48C049AF,
	Utf8JsonReader_ConsumePropertyName_m049CE9287BA5B4C7AF8F8C7D68EBA79C82A7451A,
	Utf8JsonReader_ConsumeString_mF022344616A784A7765F0569118543E35C5F9498,
	Utf8JsonReader_ConsumeStringAndValidate_mAFEBEA2CDF74CC836345B04881A2903E05C1985B,
	Utf8JsonReader_ValidateHexDigits_m0309E92E7DC46970B49FCBAD399D28E39F10069A,
	Utf8JsonReader_TryGetNumber_mDDD3CE6A95A382184215D8DB6A31D767AFECD6CF,
	Utf8JsonReader_ConsumeNegativeSign_m4C8B15B4D156BE3117809A55CD2C19B8725DABEC,
	Utf8JsonReader_ConsumeZero_mDAB03B486171BA7CBCCA27B47FA6BF43186E299C,
	Utf8JsonReader_ConsumeIntegerDigits_m220F559BD087754C8B69A37459241981A0C82783,
	Utf8JsonReader_ConsumeDecimalDigits_mFD41DF26D628251C7BBD82174BF64846A0C83B2E,
	Utf8JsonReader_ConsumeSign_m0620EDBAA824B5FC333F926075A607FC985C476D,
	Utf8JsonReader_ConsumeNextTokenOrRollback_m417EB8BF1E5C468E57452D00D30806FA775542E4,
	Utf8JsonReader_ConsumeNextToken_m7D4010DE7EC28BF0391950239A30A423715CDF51,
	Utf8JsonReader_ConsumeNextTokenFromLastNonCommentToken_mBA25C73B75A78924E0E71612292917FB405C8B5B,
	Utf8JsonReader_SkipAllComments_m2D586CC874AC235E5DAA1A76BCCECCEDF8C10F34,
	Utf8JsonReader_SkipAllComments_m35A788BF6302E95161122D7993E69BC373C64F9F,
	Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkipped_m7E03615D1717381CA3DAC23B30AE4E009B96F2BC,
	Utf8JsonReader_SkipComment_mB8CC711E5F3D6D24E5EE25198DBDB7F0B3B8B52B,
	Utf8JsonReader_SkipSingleLineComment_mEFE4A7A109C1F3E01064F405520C52B76E793DC3,
	Utf8JsonReader_FindLineSeparator_m9C0187692AE1FC17F7B40A7DDB0ACB7FBFEB340A,
	Utf8JsonReader_ThrowOnDangerousLineSeparator_mCA676223806C01CA53EC299BFC2C5BC48060EBD2,
	Utf8JsonReader_SkipMultiLineComment_m8C37CD84ABC6DA62709D69A8D23C779043D025F5,
	Utf8JsonReader_ConsumeComment_mF43CB1C22ECE1F624E0B562D93A099C01216B78E,
	Utf8JsonReader_ConsumeSingleLineComment_m55604E2C6DEA3EA494E5663AC035A2131010D844,
	Utf8JsonReader_ConsumeMultiLineComment_m27C5FF33B76C1C46A721951202BE2983F4F0961B,
	Utf8JsonReader_GetUnescapedSpan_mFAEB005896BE9873061EC7843FD2CDC107B3DE3F,
	Utf8JsonReader_ReadMultiSegment_m34ED95DFD5361E1AF24A36E81C9059A00853C644,
	Utf8JsonReader_ValidateStateAtEndOfData_mE1A59B817BD48A6F2809EEBC7F464A0E28196FF7,
	Utf8JsonReader_HasMoreDataMultiSegment_m31E6091999973DF6F5E877267CD93D1CCDE10AA9,
	Utf8JsonReader_HasMoreDataMultiSegment_mF1B6563CC059AB8F085A0D8A33A11756EDB0B22C,
	Utf8JsonReader_GetNextSpan_m9C9704D9B74FE08A99E2D13820494D10BD775BF3,
	Utf8JsonReader_ReadFirstTokenMultiSegment_mE99805A003E45117C29863C73E136DF8622096CB,
	Utf8JsonReader_SkipWhiteSpaceMultiSegment_m738D7B895E031A7F201D91EC074AAFEE27429A1E,
	Utf8JsonReader_ConsumeValueMultiSegment_mCE9C63707898C340F9731E600FEE7A2F138D0DBC,
	Utf8JsonReader_ConsumeLiteralMultiSegment_m8CE5FBA9EBF636905ABA1189DEA46B55877C255C,
	Utf8JsonReader_CheckLiteralMultiSegment_m29DBBFB73EDCEDE193F51D9FDDE5CDE7327DF107,
	Utf8JsonReader_FindMismatch_m3CFC046BA471A51916E4A98AF3C2B055513722FC,
	Utf8JsonReader_GetInvalidLiteralMultiSegment_m1D4FD07D765B89E82B186D3132EEDA513C420D5D,
	Utf8JsonReader_ConsumeNumberMultiSegment_mE9CA9359DADDC9A32A0B332B5CE97D5917A99C81,
	Utf8JsonReader_ConsumePropertyNameMultiSegment_m3A62F15386C832D140978B9FB7C4533F22F19BDC,
	Utf8JsonReader_ConsumeStringMultiSegment_m9AC39085975494E255DE8A0E3F6F319895CFDBA8,
	Utf8JsonReader_ConsumeStringNextSegment_m85FB7C7822B8165B3A01A6A5E7943EF09212AE1A,
	Utf8JsonReader_ConsumeStringAndValidateMultiSegment_mB975AD804E2DE7E80CCE8106DE68C961A6443BE3,
	Utf8JsonReader_RollBackState_m14899B240A51A2C5298DE5D9CC5C20B68337A8F5,
	Utf8JsonReader_TryGetNumberMultiSegment_mB941922D23D7625DA33A2411425322B95CB49758,
	Utf8JsonReader_ConsumeNegativeSignMultiSegment_mDBEB1BD82E16D0A858BB9176369250A7BC16A7B0,
	Utf8JsonReader_ConsumeZeroMultiSegment_m2F501FEFAD08DB5672B8820A8C3739FACE5167B8,
	Utf8JsonReader_ConsumeIntegerDigitsMultiSegment_m5B05767D5DFC620AAAEDF25D188A1B2F7B1F3F61,
	Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_m8B3F9AAC81565DC842D722B03F6EBF9E038EC2C6,
	Utf8JsonReader_ConsumeSignMultiSegment_m0BDC08DBB245EB51E464A0456FD1F9A08022A78A,
	Utf8JsonReader_ConsumeNextTokenOrRollbackMultiSegment_m1558F3E362E36E672D9370F677E80CF59027A03B,
	Utf8JsonReader_ConsumeNextTokenMultiSegment_m768B8C95D7DBF55ED9EEE1A3A7E6C397A3FF2162,
	Utf8JsonReader_ConsumeNextTokenFromLastNonCommentTokenMultiSegment_mCBD2A8ECC218C80D1637EE6F4AFAD5DE7EC84E1E,
	Utf8JsonReader_SkipAllCommentsMultiSegment_m9F26B47E140957BFC485A5B1194A67FFA80AFF38,
	Utf8JsonReader_SkipAllCommentsMultiSegment_m6BDCBE0CE1EA8656FE6ECF632FC8BFD7925039D5,
	Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment_m927DB030F5F92F4CB68854405095A7AC372E1CF9,
	Utf8JsonReader_SkipOrConsumeCommentMultiSegmentWithRollback_m4A93250DEBD59CE9497CE8D857E1D1AFFAECE616,
	Utf8JsonReader_SkipCommentMultiSegment_m41581C771A4EAD318B08C195AE89705757923A12,
	Utf8JsonReader_SkipSingleLineCommentMultiSegment_m64E237F76BE411B9D1C000094FB64D3E3D921EDC,
	Utf8JsonReader_FindLineSeparatorMultiSegment_m7409FC03149D983045F95B254E219425B2AF125F,
	Utf8JsonReader_ThrowOnDangerousLineSeparatorMultiSegment_m71F825306CAE9334C56ACD50EA17AC32E5F53231,
	Utf8JsonReader_SkipMultiLineCommentMultiSegment_m927C47BED86E20BDB7136DE116E105CE8E52187E,
	Utf8JsonReader_CaptureState_mE854A6A21277EB2333CF3387664CDA0072756A30,
	Utf8JsonReader_GetString_m3AA74B2BD8AE8211C2F45748764CA0F8D0772F11,
	Utf8JsonReader_GetBoolean_m4A0CCA2B3FF77D11CDEF65077936B5CA951962C6,
	Utf8JsonReader_GetBytesFromBase64_mBD400780F573FB4057B04E9B6CE298AC931ACD5A,
	Utf8JsonReader_GetByte_mEC280899A1AD56A59AABAF6F35E061C5A257A232,
	Utf8JsonReader_GetByteWithQuotes_mDAF5E947FAA492D8510E6BFA0AF8CC8BECA26C05,
	Utf8JsonReader_GetSByte_m86C5D95B6393261601AB9C07BEABB0675C00846A,
	Utf8JsonReader_GetSByteWithQuotes_mCB8C7C905A515C7695ABC62CAEA61496AA408688,
	Utf8JsonReader_GetInt16_m486F6B00F180588F76CF037EA708079C921D5C2B,
	Utf8JsonReader_GetInt16WithQuotes_mFBCA9FB631328EFD1F4A727A40E464784A59C4F3,
	Utf8JsonReader_GetInt32_m9994A31A6BBC68EC25BFEA942924684639E0A350,
	Utf8JsonReader_GetInt32WithQuotes_m26F4EF8D42082CD49AE7E2FE9683CCA264E9F746,
	Utf8JsonReader_GetInt64_m8F101B63B25099413DEC5D297BB5FFE578DE165A,
	Utf8JsonReader_GetInt64WithQuotes_m3096847DAFC9F239BE4FAC096D247502AD492147,
	Utf8JsonReader_GetUInt16_mDADEC8D0157630B34BA4BED95BEC8FC0FB103F36,
	Utf8JsonReader_GetUInt16WithQuotes_m3AFF72BAC86A2C8D95E30D2CE927C58C01AD4CF5,
	Utf8JsonReader_GetUInt32_m727AE5A5ECBB3D29C4E592193706C81080C3AA75,
	Utf8JsonReader_GetUInt32WithQuotes_m4A9C9DF5E09E76CABB9355907A996C884EBFE5A7,
	Utf8JsonReader_GetUInt64_mD82438E5E150BE6DCB19E9AA8527032FE1073A7B,
	Utf8JsonReader_GetUInt64WithQuotes_mDF51094C764240D18BE44B978CFFF09900AB4AE7,
	Utf8JsonReader_GetSingle_mE9EEE464C1E81A3F019BBA55FC21105525AEEE42,
	Utf8JsonReader_GetSingleWithQuotes_m8BDBB093E18B06F604D53E31CABDE152B79FCEFD,
	Utf8JsonReader_GetSingleFloatingPointConstant_m557E5950D17D08737B79AE2B41C281FC62BA1E4A,
	Utf8JsonReader_GetDouble_m8B6B801EAD7C30EFEFCEE8CEF6D1522CA4778EE1,
	Utf8JsonReader_GetDoubleWithQuotes_m3C6612DDA71C26099B2C314761AAF3F7CD733658,
	Utf8JsonReader_GetDoubleFloatingPointConstant_m7649E11BD1B5F7CFEABE646638133CBBF0F69976,
	Utf8JsonReader_GetDecimal_m0F141F3F50CD249A6EA21E01188DE311EAE86F41,
	Utf8JsonReader_GetDecimalWithQuotes_m8EBE26F40D4FACB3B92BD0C7F75C4AE8E5E509B3,
	Utf8JsonReader_GetDateTime_mE98D9A548388D8526C4F0CC35899D2F14D43B22F,
	Utf8JsonReader_GetDateTimeNoValidation_mB25F994C72A93EA8103DD71FEE99A87D6634B104,
	Utf8JsonReader_GetDateTimeOffset_m34029D75821CEB4740BF1D8B359747D6475C88B9,
	Utf8JsonReader_GetDateTimeOffsetNoValidation_m32A0E97EF22CB407C923E5E795301AE87FE45309,
	Utf8JsonReader_GetGuid_mF53227165B93F42BCC17F4212C98AE792D47699A,
	Utf8JsonReader_GetGuidNoValidation_mF56895F5C26C43EAC16F16A589C002B6699C7771,
	Utf8JsonReader_TryGetBytesFromBase64_m7DEA918E54E58B402F89A126FD98D29D1D4F64A4,
	Utf8JsonReader_TryGetByte_m3A5B27FD2CA5F304075022FF7E1BC05D2441E4BB,
	Utf8JsonReader_TryGetByteCore_m37008192DDB31799D7864470CC49F8C8A0CBD778,
	Utf8JsonReader_TryGetSByte_m59FC81E3CF3CC2BA6161271E603F4C8F96CD6329,
	Utf8JsonReader_TryGetSByteCore_m136CD23B2D669BC9E205AEED6B9CA0A2D79DAC43,
	Utf8JsonReader_TryGetInt16_mDE28715B75D564D7EB68209FCC19C1A6357DD372,
	Utf8JsonReader_TryGetInt16Core_mCBDC2DF50B262CB386B03CD349C1D60176C53AFA,
	Utf8JsonReader_TryGetInt32_m98C94CE92457BDD3B53CAED4A8701BF93EAC2617,
	Utf8JsonReader_TryGetInt32Core_m088C7F21848C7409B390A4D9BCF66E1BFDFAAFD8,
	Utf8JsonReader_TryGetInt64_m2A2CEAA1791795E6DB1420BB381D71B6C593272F,
	Utf8JsonReader_TryGetInt64Core_m3993E1A0CB78D318C835E868E467414537576332,
	Utf8JsonReader_TryGetUInt16_m22C896C87811291DFD23ED0457B38E7825D6D296,
	Utf8JsonReader_TryGetUInt16Core_m9A0768CC1CF4BAC47EA82BAAD39CDF0E24C6664C,
	Utf8JsonReader_TryGetUInt32_mA2AA2EEC55C6FEDBD9844AD8473698D2214636BA,
	Utf8JsonReader_TryGetUInt32Core_m40A9CCB041D399F181EDE2B459EA7D77EBE6FB63,
	Utf8JsonReader_TryGetUInt64_m9728DDCC0E176DC03C0FEEB2C3FFC4566B2D72B9,
	Utf8JsonReader_TryGetUInt64Core_m73F5FB752D9F173DDA97E4679DBF61BFB82C3C71,
	Utf8JsonReader_TryGetSingle_m94587873A494E75F4825C1CA091504E3BB8FEF2D,
	Utf8JsonReader_TryGetDouble_m13353F863EF92179E6798D51791CDBD278180CEB,
	Utf8JsonReader_TryGetDecimal_m3B8E8434E5962329BBF1F17FD4FC6F4CF186D8F2,
	Utf8JsonReader_TryGetDateTime_m07EA74E20E656F735523B9F5C04C0DFA45D8AFC5,
	Utf8JsonReader_TryGetDateTimeCore_mD25A6F6750D17FD2E4D61338DECD3F546E1B1931,
	Utf8JsonReader_TryGetDateTimeOffset_mDBA3CEC29896D95E62041F3AB3C851BBF1B7D8AD,
	Utf8JsonReader_TryGetDateTimeOffsetCore_mD9E6F3076962699F2C5876A47B83E133E9B33533,
	Utf8JsonReader_TryGetGuid_mA3FD580BF871065767D82031AD0A499DB0EB8BD8,
	Utf8JsonReader_TryGetGuidCore_m44AE06756804CEB5DBA738D40527CA2254094530,
	PartialStateForRollback__ctor_m0F515AA3F80A7125D3079909A6578BD37B6EB3BB,
	PartialStateForRollback_GetStartPosition_m57397FA90C6F268F5711E7E5131D5237E52F41B0,
	NULL,
	ArgumentState__ctor_m4F54AB0DAFC7A03AFC29261512C3AD0ED1853D05,
	JsonCamelCaseNamingPolicy_ConvertName_mFF8B31FCFD0ACD691C193A2072E7D1581A6DD3E9,
	JsonCamelCaseNamingPolicy_FixCasing_m269D622EE92ED2E000DEE250533E8C3206687BDF,
	JsonCamelCaseNamingPolicy__ctor_m8854A22CDF025E31E6CB2C027B25AEED30C16312,
	JsonClassInfo_get_CreateObject_m3EBF112EA9432FA43BFA1E1D879507F57856E3F2,
	JsonClassInfo_set_CreateObject_m34917503915617101E42AD20C1D74EFDEC9F2822,
	JsonClassInfo_get_CreateObjectWithArgs_m6912350EF7B13944758447C444D4436B39C630F1,
	JsonClassInfo_set_CreateObjectWithArgs_m9E5EFAC1AF224E53D2183769AF6BADF6AD44BB40,
	JsonClassInfo_get_AddMethodDelegate_m7468AC11B693425EFCF34C4887F0B15FF1F3824F,
	JsonClassInfo_set_AddMethodDelegate_m28A0B9710051599AAB0D4505DA3B9070A89BA838,
	JsonClassInfo_get_ClassType_mB58B47E20ED61254BDFAFFA920187F81F5F57189,
	JsonClassInfo_set_ClassType_m6E03618BC777E3BFD9B238A6FDA54735C76E69A3,
	JsonClassInfo_get_DataExtensionProperty_mBEF729DF2A3F4EB8B567B5B4A4C8CE9E77B52C97,
	JsonClassInfo_set_DataExtensionProperty_mF78B121F29FE973427984392D38467BA457B9231,
	JsonClassInfo_get_ElementClassInfo_mC307A666007CD376510C711088774003D490696E,
	JsonClassInfo_get_ElementType_mFAE161CCB42C2611182480DA837FA59895C97EDD,
	JsonClassInfo_set_ElementType_m8AD53CEED0B02CE93E0F4F45E4097E68C638F743,
	JsonClassInfo_get_Options_m455CFEE519679BA34B37450E658EA2892CAC46E5,
	JsonClassInfo_set_Options_mB86EA19543A41CA89EFDB334170B32EC0341CD00,
	JsonClassInfo_get_Type_mADC140B544ABD66F415F4CC722A44401CADDA2DF,
	JsonClassInfo_set_Type_mC2132BC8095379C552F973C5F1207017D33838FC,
	JsonClassInfo_get_PropertyInfoForClassInfo_mA5E941F592678AC615E03EAB08F7C4852EF1FBDF,
	JsonClassInfo_set_PropertyInfoForClassInfo_m0F7EB242F99630CB150E15DC5059DF0B0C8C30B8,
	JsonClassInfo__ctor_mB0FFE94BDFC6E5191DE7029A75A2B42EBE3660A2,
	JsonClassInfo_CacheMember_mDE451A3FFF1932D024C5E87216349DD71A13A9EE,
	JsonClassInfo_InitializeConstructorParameters_m0D4D7729F0AC4CA827E230A5E676CEBC0FE6BEAE,
	JsonClassInfo_PropertyIsOverridenAndIgnored_m58CA0851B95234F54FD442D696002956E66F321F,
	JsonClassInfo_PropertyIsVirtual_m89E8D941D30BF148B3EB940A4FF97F15584133B2,
	JsonClassInfo_DetermineExtensionDataProperty_mED80FAB663CA5DC121EF5E563A40D70303855E9F,
	JsonClassInfo_GetPropertyWithUniqueAttribute_m949E9EDBEABE05FC1C6E9A14B30D40E26DA85F8D,
	JsonClassInfo_AddConstructorParameter_m95BD7C701497F9EA77D1F62A97CDE7351BA10973,
	JsonClassInfo_GetConverter_m4D8FCA3F0612E829323DD0A04F320F5BD9240EE0,
	JsonClassInfo_ValidateType_mBAB0C4E7BE4A099E1636D940E6AE1368D7AC0E67,
	JsonClassInfo_IsInvalidForSerialization_m8711EFCAE07559D74A27DBE78C7AD6B52FC9F92D,
	JsonClassInfo_IsByRefLike_m35555FEAF733150FC3A25A17EBB58F23211E08B6,
	JsonClassInfo_GetNumberHandlingForType_mE28062630549D5A7FB1D308533CF0B2A559379E5,
	JsonClassInfo_get_ParameterCount_m40342A48D50A73E4009D410636E99363D2D78050,
	JsonClassInfo_set_ParameterCount_m724623350D2C95B221A10C84C0EFDA7872952176,
	JsonClassInfo_AddProperty_m1A38048CF13A18F1170C3EAF348EBA8BBBE6DFEE,
	JsonClassInfo_CreateProperty_m37DF3D2728C0A9C9A4C07183C36FCA4F5A46BA7C,
	JsonClassInfo_CreatePropertyInfoForClassInfo_mA733C3680077D46EC8880E83E72981DA56981A64,
	JsonClassInfo_GetProperty_m4AD766C54EA3A1D37706F74AD38173F375B76506,
	JsonClassInfo_GetParameter_mD05AE1588E681D5092CC833222DB24E00614259D,
	JsonClassInfo_IsPropertyRefEqual_mF49B94C280F0A7C83B637E102015E60FCD4C4F1C,
	JsonClassInfo_IsParameterRefEqual_mF9F55586BE7F3213556DFED8BA3786A19AAA871B,
	JsonClassInfo_GetKey_mA405F4C97CF779AC1BB3AE849B0AB5F8A2522018,
	JsonClassInfo_UpdateSortedPropertyCache_m93D69873D48BA0E8E8D6DE3AE3C6AF21BDB168AB,
	JsonClassInfo_UpdateSortedParameterCache_mFB7B316FD7C2009DEBE2457F2648D00E0E3FD9CE,
	JsonClassInfo__cctor_m914422BBC28D3DE010CCD1AA3E7569D5DD1FB9A4,
	JsonClassInfo_U3CInitializeConstructorParametersU3Eg__GetMemberTypeU7C46_0_m4DB5544FEDE2BFFFCDBD0C71F871A97E52BEE698,
	ConstructorDelegate__ctor_mD390978F74D17842A5B524334220F182F0FFE5A2,
	ConstructorDelegate_Invoke_m64AFA88C347ABBF8E71F60DF07F0D94D853C6EE9,
	NULL,
	NULL,
	NULL,
	NULL,
	ParameterLookupKey__ctor_m8DA6397018FAE0536BF32BE115B712CCA3D54A01,
	ParameterLookupKey_get_Name_mFEBA84A3AABA84F665B6225A40910FCA4035DF9B,
	ParameterLookupKey_get_Type_mBA2A95BA0E212EBA50A6A6BA1DD07F9418C35A90,
	ParameterLookupKey_GetHashCode_m941A3A9109B92C2949699D93BE49AC04835C392D,
	ParameterLookupKey_Equals_m03A9EC5CACB1EF489D151E7EDFB30161EBE327DD,
	ParameterLookupValue__ctor_mA7337C487E75BB0463EA0E263A728EBB0F8EB7D6,
	ParameterLookupValue_get_DuplicateName_mEF39EDF507355719BE0C657CDAD196E4D862A9C6,
	ParameterLookupValue_set_DuplicateName_m2E5925A1C4EB54A46A0D8027EC79094CE910A5B1,
	ParameterLookupValue_get_JsonPropertyInfo_m3B8BDAF0D81391028FEE43EBCFF523404F2600AB,
	JsonDefaultNamingPolicy_ConvertName_m42E74A92E3FE81040CD72A02792EBCA736527B3E,
	JsonDefaultNamingPolicy__ctor_m4435B81DC2DA68A9B1191F818BF265C550D8D819,
	JsonNamingPolicy__ctor_mDA3AFFECBC22A7C0FB171A00AFC1E1F00BC79204,
	NULL,
	JsonNamingPolicy__cctor_mC6F9988BFAEB4FC160002FE854DAF4C9EFD12FF6,
	JsonParameterInfo_get_ConverterBase_mCC08551310814992064D684373C6615BFA1257D5,
	JsonParameterInfo_set_ConverterBase_m93143D7C5A636B19EB4E2F6DCA2164C1B6221EDC,
	JsonParameterInfo_get_DefaultValue_mD3A36A05BE23345A76389D6D77930755D17E880A,
	JsonParameterInfo_set_DefaultValue_mD9EC1B414F7C6B4635348B54D081285F8A2611A5,
	JsonParameterInfo_get_IgnoreDefaultValuesOnRead_mA4ECA944598FE5BADC46249FDA9E3189FB546E0B,
	JsonParameterInfo_set_IgnoreDefaultValuesOnRead_m2779C8F1629C24F418F5D6FEC30FEAD15362DD71,
	JsonParameterInfo_get_Options_m7F95CBCFF8FE693469ABC96F51164729A55A8EEF,
	JsonParameterInfo_set_Options_mBF96669910F25F29C17297FDC0F4BB0FE35099F1,
	JsonParameterInfo_get_NameAsUtf8Bytes_m688D4F37A8BAA98F54FB8AB50EA226074556D91D,
	JsonParameterInfo_set_NameAsUtf8Bytes_m9A4C3C3E99D222691ECC2D75D22D196B6D966B55,
	JsonParameterInfo_get_NumberHandling_mC0C94DACE6C1990B7D6B232A27730FA513F6A5CF,
	JsonParameterInfo_set_NumberHandling_mADA24929F092015A0F64BC2EDAEF302618BB20E0,
	JsonParameterInfo_get_Position_m568DD91DFCBC41C1AB61BE19D4AE03B11ECACF42,
	JsonParameterInfo_set_Position_m6E579FF1797800B3BAED9D35A646E9E2B32D41D3,
	JsonParameterInfo_get_RuntimeClassInfo_m9385118A9259F52F310B62874E90EF7BD0D83759,
	JsonParameterInfo_get_RuntimePropertyType_mDC2CE8AA4F39F0447C0DCAA8B8050A93968465BC,
	JsonParameterInfo_set_RuntimePropertyType_mECB4C8ADF8951CE7563A0860825196EAC3C7E3BE,
	JsonParameterInfo_get_ShouldDeserialize_m0805077B1A252FA8DA64409F2F077354D105D3CA,
	JsonParameterInfo_set_ShouldDeserialize_m3EFD6DB8AA2A9DA33B98BAD73394E7BFCAA01265,
	JsonParameterInfo_Initialize_m8152E501631F2148C3E048D0259B6B0865FFBCD1,
	JsonParameterInfo_CreateIgnoredParameterPlaceholder_m7B3C05C55E8A8ADE239FC2659778096B52A559C2,
	JsonParameterInfo__ctor_mEA4711A4328921BD302B5EF10375222C7B5308B2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonPropertyInfo_GetPropertyPlaceholder_m8DDE8771A9D400F54491B0E9131BC9864FA1A400,
	JsonPropertyInfo_CreateIgnoredPropertyPlaceholder_m48161A419A057A5A255DC902A0864AD45798DD77,
	JsonPropertyInfo_get_DeclaredPropertyType_mA7E5DAEF07385CD52EA765681C042819778D4E23,
	JsonPropertyInfo_set_DeclaredPropertyType_m35C010F02946728DE042F6D6343DECD703D780D5,
	JsonPropertyInfo_GetPolicies_m348B854F6CD69667E92E1A4FAAE41DCE26F7BDAD,
	JsonPropertyInfo_DeterminePropertyName_m7DB7DFC642DD42E791262DF3837035C6ECE925C0,
	JsonPropertyInfo_DetermineSerializationCapabilities_mBA5E636AEEBBFD9B64B909E6C3E0260EA4545E8A,
	JsonPropertyInfo_DetermineIgnoreCondition_m1A9DA7C8F29E66CF3D7D75DCEA496CE09EECA422,
	JsonPropertyInfo_DetermineNumberHandling_mC32578B27199802D8790E888F303E2E1ABEE7DF8,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonPropertyInfo_get_HasGetter_m025A2B5681CD7CCEBBC7E3F57C04C6A339C980A7,
	JsonPropertyInfo_set_HasGetter_m3A356164FCCD506528DCFB55D33E603FBDC10F29,
	JsonPropertyInfo_get_HasSetter_m5EEEE5CAD0764FDEA90B717276823A4E9460E13B,
	JsonPropertyInfo_set_HasSetter_mFEA57CBF1D0DC3C17C890C4199B9741C159D681E,
	JsonPropertyInfo_Initialize_m452B07B8CBC1C066ED5A69933A6F731CB75379B4,
	JsonPropertyInfo_get_IgnoreDefaultValuesOnRead_m728486838F41D007DA5C66D1E32132B5A977AC36,
	JsonPropertyInfo_set_IgnoreDefaultValuesOnRead_m1CB085CFB0CF60A74A7917881F912C0274110E3F,
	JsonPropertyInfo_get_IgnoreDefaultValuesOnWrite_m6014C570FD13D67F0DD65DF4C5DECB59459C02B1,
	JsonPropertyInfo_set_IgnoreDefaultValuesOnWrite_m37324953D9E5DD8939747E2B250562699A0C1AB0,
	JsonPropertyInfo_get_IsForClassInfo_mB3FBB996FFE12A31E528E5DCE807D9F689ED953B,
	JsonPropertyInfo_set_IsForClassInfo_mAF881BB94BFB8B2C9ACFA7F5678D748E4766E78D,
	JsonPropertyInfo_get_NameAsString_m11191ABD723F638610ABCF00711196121E7E5B33,
	JsonPropertyInfo_set_NameAsString_m60C685E0FDA27B1FA13BADB4C5EDC1AD21A75556,
	JsonPropertyInfo_get_Options_m6A4F9F50FB8D18A8D87E918C5257DF56CC171D92,
	JsonPropertyInfo_set_Options_mEEF7A440A2F290AE89E5F74577EF042ECB193BAA,
	JsonPropertyInfo_ReadJsonAndAddExtensionProperty_mBAF58461BED720ED2D5E5CDAB4A1B6242B2DF3B9,
	NULL,
	NULL,
	JsonPropertyInfo_ReadJsonExtensionDataValue_m98174C8D99A68C441F14DDFDB3845DD95DB77F6D,
	JsonPropertyInfo_get_ParentClassType_m81E11A124BF5BF4161D1E85664A29CC826EE5654,
	JsonPropertyInfo_set_ParentClassType_m53899BF0AF2631BB72C321DD9E9A8E35332782C9,
	JsonPropertyInfo_get_MemberInfo_mD28BFADF987F4D6B3040E01220E7ECEB2B892842,
	JsonPropertyInfo_set_MemberInfo_mCEBE51C89CE4B8A2AA463D598DD4C29A821A3A86,
	JsonPropertyInfo_get_RuntimeClassInfo_mF77D80FC7CCE7E7E8D4F56E4CB2A629713978EB3,
	JsonPropertyInfo_get_RuntimePropertyType_mE9E72D4AB3EE30C9C20D661CE1127645DB143466,
	JsonPropertyInfo_set_RuntimePropertyType_mCBB9ECD02C7CC07549F4B7D2706140D769A6A348,
	NULL,
	JsonPropertyInfo_get_ShouldSerialize_mC0489A6FED6EC669A944D044A73D205F14FA21E2,
	JsonPropertyInfo_set_ShouldSerialize_m10AB20E671B7E2C15BED04BCCC99E3FB79872E93,
	JsonPropertyInfo_get_ShouldDeserialize_mC0F6FB0AC12102EADC4C22A84B009D7FA903108D,
	JsonPropertyInfo_set_ShouldDeserialize_m9013BB660E0FA69F093B58C6EF325A88B07CA99F,
	JsonPropertyInfo_get_IsIgnored_m7570E12D5A959200E91D1F1BFA6BFF9CD42FE78F,
	JsonPropertyInfo_set_IsIgnored_mBA15B57ADD5B0972DC91A6DF5F18D2E70A4073E7,
	JsonPropertyInfo_get_NumberHandling_m97868DFAB3D0971C720873E936E4ED5FE6222166,
	JsonPropertyInfo_set_NumberHandling_m183C90FA1664FD8250BDD4211AC8229640E290F2,
	JsonPropertyInfo__ctor_mF8AE5468031A60DA2D460931063E2C81702F0D3E,
	JsonPropertyInfo__cctor_mC06E09EE5C8788EACD0A71D78A3729C647E04EE7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonSerializer_TryReadAheadMetadataAndSetState_m6B18228F63C23C3EBEAD05B281F7F210B3EAA364,
	JsonSerializer_GetMetadataPropertyName_m04EA82F5813FCC311E79BF04990FF7C6E0C9B8EB,
	JsonSerializer_TryGetReferenceFromJsonElement_mB6DD6DDE9BD124B2FF3753C01945404B82E5A975,
	NULL,
	JsonSerializer_LookupProperty_mD8AE09F693994C7A922EE7812796D6A07A231719,
	JsonSerializer_GetPropertyName_mC1C0556CD85CCF682207683A331349038C9B9FDB,
	JsonSerializer_CreateDataExtensionProperty_m7E6DDAC1AEBBAEB9A4ED744C11CED6B446EF9A96,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonSerializer_WriteReferenceForObject_m50C78FADF91B563E516BDB5E206D98F1DF8B94E8,
	JsonSerializer_WriteReferenceForCollection_m114018149E92E3C03D39390A1401D78CBAEB9BB6,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonSerializer__cctor_m4E7DDB69F78AC9C224F51B9B5D45E8F9C92C1595,
	JsonSerializerOptions_GetDefaultSimpleConverters_m2EE0299A75B9DB1DD205CEDFF2CE2A30C763E1FE,
	JsonSerializerOptions_GetDictionaryKeyConverter_mCF912E4307003471C53EEBD39A9207E44BF51221,
	JsonSerializerOptions_GetDictionaryKeyConverters_mA2A9D36A4ECB24BE030C9B48CA38EB25E4900481,
	JsonSerializerOptions_get_Converters_m8E101768CD4E9529474EB92573A0E68E8FBA92A2,
	JsonSerializerOptions_DetermineConverter_mE313161067E843257F33620EE88955B63893C524,
	JsonSerializerOptions_GetConverter_mA1E8ADC867E97387A72326D4FD6AE68D7FB1C1BE,
	JsonSerializerOptions_GetConverterFromAttribute_mF069BED8CED47DE0931D861C2A09BF014AE47659,
	JsonSerializerOptions_GetAttributeThatCanHaveMultiple_m5874EBF0FA09BF4B0BC8769905E8EEA11F27C290,
	JsonSerializerOptions_GetAttributeThatCanHaveMultiple_m2A03FC9FB47A97A81C90254D67EC3176F65D3737,
	JsonSerializerOptions_GetAttributeThatCanHaveMultiple_m81E89DACD7113BBF7C75498C8445E920F9BB00F9,
	JsonSerializerOptions_get__lastClass_mCDEFA43D861813355210E3618634C7AB6989CEAE,
	JsonSerializerOptions_set__lastClass_m075FD70E92ECCA58B0F36BFFC7383F87140BE846,
	JsonSerializerOptions__ctor_m9431C3AEE8FE5AF4D2ACAE80B23CA0CC18CC4281,
	JsonSerializerOptions_get_AllowTrailingCommas_m01F28C687E038924AB6160A6876EC268C61A5A08,
	JsonSerializerOptions_get_DefaultBufferSize_mC38EE8C3CF396E251E76982164CCA47EABE0B24E,
	JsonSerializerOptions_get_Encoder_m259F8A917736DFDA61A9C95D33975208707B20C4,
	JsonSerializerOptions_get_DictionaryKeyPolicy_m33DEE0C33A09A92D55C9F111E9988410DBDCFE52,
	JsonSerializerOptions_get_IgnoreNullValues_m6256070F160F5CBF7BB77B3A7F6033ADD1B2B750,
	JsonSerializerOptions_get_DefaultIgnoreCondition_mDC7106616086AFEAB7A75DD18AFDB9FF8ECC0F7F,
	JsonSerializerOptions_get_NumberHandling_mDF95C8EE4D68BE61C604B68AB1F744960426C3BE,
	JsonSerializerOptions_get_IgnoreReadOnlyProperties_mF9CE0A5C472896550773A8C88B2504A2DA21D422,
	JsonSerializerOptions_get_IgnoreReadOnlyFields_m099F0E064483BA6C3D5097F1760E0F9FFF2FF6EB,
	JsonSerializerOptions_get_IncludeFields_m9035956C431B293552FB195C6B740C3BF29DA6FF,
	JsonSerializerOptions_get_MaxDepth_m406F3FAD7524C2F3EC3D0EF53BCD2E151FAAFBE1,
	JsonSerializerOptions_get_EffectiveMaxDepth_mCC1A5D0D20B923ED0F846BEB13020E8119AC1DDA,
	JsonSerializerOptions_get_PropertyNamingPolicy_m799A8243B78ED65A708A57E838B88C902D5403FF,
	JsonSerializerOptions_get_PropertyNameCaseInsensitive_m42B624BEFB6E510442F5486AF45A3F7E0B3C421B,
	JsonSerializerOptions_get_ReadCommentHandling_m426DAA2198333E198D0C0BC03F82C3D60E6B8C11,
	JsonSerializerOptions_get_WriteIndented_m27E15D67417240BEAE4EBF64C2AF4E81098C99F8,
	JsonSerializerOptions_get_ReferenceHandler_m103B32EEF3E853E416576B461142BC497342ECED,
	JsonSerializerOptions_get_MemberAccessorStrategy_m4A5B819731E3EBA23BDB9DBBD7606148A4620B88,
	JsonSerializerOptions_GetOrAddClass_m0843F531DE2D905BB015DBAE8C4FEF0C5E00D574,
	JsonSerializerOptions_GetOrAddClassForRootType_m5FEDB77AB83AE1637D8DC6FD9295281D122CE4DE,
	JsonSerializerOptions_TypeIsCached_m7494E4E476E801188093642E5E13763963D0B16F,
	JsonSerializerOptions_GetReaderOptions_m3DFC2A2CF3AB3DB94DE7CBEDBF717CEC878966DA,
	JsonSerializerOptions_GetWriterOptions_m2831442D630359B6FFF6FB69CD71A31ABF4F5033,
	JsonSerializerOptions_VerifyMutable_m131F3518E80AE52B84A26FB14EA95FB154CCA324,
	JsonSerializerOptions__cctor_m2EF8DD15009E63DFB2440CF25EEB487C987FDBB1,
	JsonSerializerOptions_U3CGetDefaultSimpleConvertersU3Eg__AddU7C3_0_mEA8BDB668D0A86C811CD04F3FF1239DA7B5022C8,
	JsonSerializerOptions_U3CGetDictionaryKeyConverterU3Eg__GetEnumConverterU7C4_0_m26851F02B29FB81551992789E5E7F304ABC8174C,
	JsonSerializerOptions_U3CGetDictionaryKeyConvertersU3Eg__AddU7C6_0_m871391C97F5C4D6D9CE6ECC72C4372DA176EAE6C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MemberAccessor__ctor_m16A4BAC1382B1E770361738C12D604B051948066,
	ParameterRef__ctor_mD6F9E352509A2D16FBBFB12B05A40A70FB04672A,
	PropertyRef__ctor_mBB574D64A64A57C51A58468AC6514DE568D533E5,
	ReadStack_get_IsContinuation_mCEC1851A10A829FFFE42BBAD53711A0A990526F8,
	ReadStack_AddCurrent_m3B0106DB1337C3B7F26B2F874E40B80FA7FCBD18,
	ReadStack_Initialize_m4CCFB4EF4B2C80815463CE81FE499FFF8528BB3B,
	ReadStack_Push_m1F2599D52633E428D94A9D90E77A833FE64BC0BC,
	ReadStack_Pop_mD20CAC0E80A32CAE52481E175241AFDE02D4C9DD,
	ReadStack_JsonPath_m4DF8F1F7BB216E3E6F9EFE4066E9B94D270E49C5,
	ReadStack_SetConstructorArgumentState_mCE9355370E6E636D813179995CFCB4B869A20033,
	ReadStack__cctor_mC4E80F9C51420C1613550EEEDA9E5500E6533CB1,
	ReadStack_U3CJsonPathU3Eg__AppendStackFrameU7C19_0_m09DC1845C124681041777206875E96719B692E6A,
	ReadStack_U3CJsonPathU3Eg__GetCountU7C19_1_m3DFAE067EBF1971BBC7F7870F3F41DE01F59004F,
	ReadStack_U3CJsonPathU3Eg__AppendPropertyNameU7C19_2_mA3A779D7C6AEC6E48320FC93C5E3B87657C7D295,
	ReadStack_U3CJsonPathU3Eg__GetPropertyNameU7C19_3_m50B37A112F20DD2EEE4684007F8D886D5A92EB88,
	ReadStackFrame_EndConstructorParameter_m8D04A7A3CE2D19DA2C0CCFA8937859DEF1151A14,
	ReadStackFrame_EndProperty_mBF44974D081F8BDCC44666395A7CC82DB3E557E4,
	ReadStackFrame_EndElement_mA9D0401AD7A3F30475D97C6E49C671AA98558979,
	ReadStackFrame_IsProcessingDictionary_mBFC8EA6ACB5BA357E8887719A4CD2F61F14DD2AE,
	ReadStackFrame_IsProcessingEnumerable_m3A7B122E538645493032819C697D4FD0D7BB40E3,
	ReadStackFrame_Reset_m9998E100B9CF5B2FC93FDE5C50FDB77227903C9A,
	WriteStack_get_IsContinuation_mBDA5ACA7E19BAF628ED4BDCE702B8B46E6AA9BDC,
	WriteStack_AddCurrent_mCA9F10EA1D6E24E25E27295386208F3E71A3654E,
	WriteStack_Initialize_mD5B4E7AF1DE653A9D2E614A452799636C009F9CC,
	WriteStack_Push_m7609AF43966C67D97FE3A16525E55D53FA2569AA,
	WriteStack_Pop_mC6F6750427C0EEEEAC7B9ED1BF9ECC07677F7000,
	WriteStack_PropertyPath_mAE92BA9B54387C839164F69A7944CF0401165039,
	WriteStack_U3CPropertyPathU3Eg__AppendStackFrameU7C13_0_m74ADF97F41495445B46D2A01592312F0C76412E6,
	WriteStack_U3CPropertyPathU3Eg__AppendPropertyNameU7C13_1_mE397598417E5CB94719735BB1B4D60A0E99FFC95,
	WriteStackFrame_EndDictionaryElement_m0E57022D7831F92A528248C5A25CE8B5E0A28F0C,
	WriteStackFrame_EndProperty_m713E4D5585A2354CD0CE2DF56B7DA3B9E7FD31C8,
	WriteStackFrame_GetPolymorphicJsonPropertyInfo_m8C493C533AC6BB1D6E45E7F27F0664F157CDD602,
	WriteStackFrame_InitializeReEntry_mDA0CD4D8B09439188519F97297A2A2D85D3BD619,
	WriteStackFrame_Reset_mC74400A4F12DD1688E4D4C1EC4C6DC305B96E56E,
	TypeExtensions_IsNullableValueType_m4DB0F40A9B037168E72346EAC4F5E36CA3388916,
	TypeExtensions_IsNullableType_mAF8621F34981409231CA30DD00F28B6096765F68,
	TypeExtensions_IsAssignableFromInternal_m984159CD43E51B85C367651715071C56FE692379,
	JsonWriterHelper_WriteIndentation_m3FF1948E81F7765D1C4D8ED825664DD6CFF8537C,
	JsonWriterHelper_ValidateProperty_m1F34A8D5BFC3B8AEF6935B5B42C1A0045D07162B,
	JsonWriterHelper_ValidateValue_mD8CCD8D9A6983372F1D9E36E37291A855324C3BA,
	JsonWriterHelper_ValidateBytes_m66574E5CBB54C01C8DD0CF770AB713231151ABDE,
	JsonWriterHelper_ValidateDouble_m273C268E501F4DE8A73411EC0E4F91913F5C7447,
	JsonWriterHelper_ValidateSingle_mB5D28135F97269867F8DC0389E382DEA7317DF21,
	JsonWriterHelper_ValidateProperty_m870A88B693769B0670D79885D858B52192EE93E6,
	JsonWriterHelper_ValidateValue_mFB60C5BD2132B5D147FAD6A9DE920C2F765DA2B0,
	JsonWriterHelper_ValidateNumber_mD57FF09936333FB187C8E1FBBF58F1C8CECBFDA6,
	JsonWriterHelper_WriteDateTimeTrimmed_mEAF361B4FF5284DDDB39A5B0A0F1FF24A12FD65F,
	JsonWriterHelper_WriteDateTimeOffsetTrimmed_m054EF2DB0488EF8100A719D92F5EB7DAABF47CA1,
	JsonWriterHelper_TrimDateTimeOffset_mAC5CD5A22C382458B18E1A5316289454789490BC,
	JsonWriterHelper_DivMod_mD6FC02993BB66E34D1611A3CB0803C548EE1F3A5,
	JsonWriterHelper_get_AllowList_mA28609B6B9076FA68AC62A6E99A56F15278670D3,
	JsonWriterHelper_NeedsEscaping_mFE93A07F36E809CB539B11A56C653D48554C9395,
	JsonWriterHelper_NeedsEscapingNoBoundsCheck_mD12552627BD85E3DD84860062048DA6D9BF57878,
	JsonWriterHelper_NeedsEscaping_m2C8304CA115F8E6DEF835C700499D77387F13CA8,
	JsonWriterHelper_NeedsEscaping_m1EE16B480DFB8212DF545CAE1531E7741B705C9E,
	JsonWriterHelper_GetMaxEscapedLength_mE4CCE091405F6401E43DAE665652621A358326C4,
	JsonWriterHelper_EscapeString_m300A0A3F3C73F32440B227F5FA7CD90146D852C4,
	JsonWriterHelper_EscapeString_m564702FF6E13FE62D005D7AFEC973C086288877D,
	JsonWriterHelper_EscapeNextBytes_mA3984914396004727102D290DD1BD96A3E4DE830,
	JsonWriterHelper_IsAsciiValue_mB771D4B77A06BDE98B39D52324FAD6A8E1F33E57,
	JsonWriterHelper_IsAsciiValue_m95272B549B92BDD457C999FBC9F41B51226673B6,
	JsonWriterHelper_EscapeString_m6A3C10DB5FDFA81CF6BEAF45725802FFE0CAA868,
	JsonWriterHelper_EscapeString_mD98ADCFA56B4C5896030D35AE577B6210588B383,
	JsonWriterHelper_EscapeNextChars_mCDD04B1D68EAFB21573130C3C2645DBE31307C39,
	JsonWriterHelper_WriteHex_m7A120B39639623607153F51F1E3D78A8C43A47FE,
	JsonWriterHelper_ToUtf8_m0EC452325B9038CDF4AFC9F74E55A7194674B4F6,
	JsonWriterHelper_PtrDiff_m38A892D52BF473A2345B38153C2F143007339D33,
	JsonWriterHelper_PtrDiff_mD1A028CB8E273EE4BA081972D46DBBF5E1152425,
	JsonWriterHelper__cctor_m3E983799FC90D8DEB824C22C20E0462A5FF2FABC,
	JsonWriterOptions_get_Encoder_mAE5C32DE2F9E54D87346B602EA2DB2C0676D7406,
	JsonWriterOptions_set_Encoder_m5E357AEFC0AB885F7858AE1D70B7060273AED306,
	JsonWriterOptions_get_Indented_mDF7E8A47E47FF6E43BE1E577F434CA7525E4AC0A,
	JsonWriterOptions_set_Indented_m5418A714E77EA6D9734E6A497FFD407D799AF8DB,
	JsonWriterOptions_get_SkipValidation_m9113D9C54C02B4EBA452A57485A91F8A30E74DB0,
	JsonWriterOptions_set_SkipValidation_m805F1EC553421C91697F04ED05567BB51981F43A,
	JsonWriterOptions_get_IndentedOrNotSkipValidation_mFC205FBEF9620AD791C2EAC0A7F776281E2D9900,
	Utf8JsonWriter_get_BytesPending_m28D90DE67217F118AFA594E9F9D055183474E452,
	Utf8JsonWriter_set_BytesPending_m74D2AA59465ED7058538164D4B1DFB19E58C2ACD,
	Utf8JsonWriter_get_BytesCommitted_mEAF5E6159D99CD34796639914CCA84473295AACF,
	Utf8JsonWriter_set_BytesCommitted_m0611AE9EF0E556F695ED2D1C71DA272DC0506B4C,
	Utf8JsonWriter_get_Indentation_m829020AB9C3F388A3F1BD34EE21E9534AA6861CF,
	Utf8JsonWriter_get_CurrentDepth_m8C0AF093B68AFDD3BC097AB985DB8FF76CD7BA47,
	Utf8JsonWriter__ctor_m036DB06EA633C229C4147A6DE656934C77AB794F,
	Utf8JsonWriter_ResetHelper_m75A417EA64C4EBF6D4E2AF8C54D90B95770E49B3,
	Utf8JsonWriter_CheckNotDisposed_m3023C02F6F75AB548E99526BE817223286AC82EA,
	Utf8JsonWriter_Flush_m71B82C161B4ECCB5D90DCBBF667D3647404A7799,
	Utf8JsonWriter_Dispose_mBAFB87EB612CD2B6E911A32B5BDDC41308AB433F,
	Utf8JsonWriter_WriteStartArray_mD418C2E501B88084EA5837B9BE9038EA75CA1337,
	Utf8JsonWriter_WriteStartObject_mA1EF1E8CCF738E49D16F99CDA5F1F9E6868D88B2,
	Utf8JsonWriter_WriteStart_mA8A609C59C9BC1B5BFB4D82BD18328952019FDA1,
	Utf8JsonWriter_WriteStartMinimized_mB1A7139369CD1119EDD231484C804B9DFBA883EC,
	Utf8JsonWriter_WriteStartSlow_m0A135B5009203BA02F5341E152E27654552DC02C,
	Utf8JsonWriter_ValidateStart_m6731F9FFACF4565D12DC27106FF4BF655650EEF8,
	Utf8JsonWriter_WriteStartIndented_m7BB700FADBAC557FE53D6A264734B0F0597FDB39,
	Utf8JsonWriter_WriteStartArray_m03E814CDF1C585AD239AC26E0B0EEBA6EF370F6F,
	Utf8JsonWriter_WriteStartHelper_m2A737E064AA3D2FDB19B629304F533A8FE583090,
	Utf8JsonWriter_WriteStartByOptions_mE98BD442E748E3950B2943CE3B1136B08DF92FAF,
	Utf8JsonWriter_WriteEndArray_m5777DA78B4AD16920865AFBF6800D5A0AE7463EB,
	Utf8JsonWriter_WriteEndObject_mCBB8ED094BB4B5196B0D6496B2C8EC2A40C71EFA,
	Utf8JsonWriter_WriteEnd_m77890C8C2A9C911157F9972E4FF9CBFA712D02AF,
	Utf8JsonWriter_WriteEndMinimized_mA33335B7FDB7413EE731D3C1844B45026F25EB3A,
	Utf8JsonWriter_WriteEndSlow_mECA3F6EB97FB3CBE19FBDABA77010DC1EE8290D4,
	Utf8JsonWriter_ValidateEnd_m1F0FF13359265179652859C5FD0A4E3219F45B3E,
	Utf8JsonWriter_WriteEndIndented_m39BDB70B1110F40D7C17E13095CA802DCD0CD457,
	Utf8JsonWriter_WriteNewLine_mE43F1B48D99F3E69397722AD89C068FECF60DBF2,
	Utf8JsonWriter_UpdateBitStackOnStart_m12969BF6585ED46DCD0106D02A9D0C9344495337,
	Utf8JsonWriter_Grow_m45CE325E04A63A8160A6DE6D507B0FBD324914EF,
	Utf8JsonWriter_FirstCallToGetMemory_m421AB335FA4A40AB99E7DBEC68C5C93B45BA300B,
	Utf8JsonWriter_SetFlagToAddListSeparatorBeforeNextItem_mC49F581160BBC99B7CB7AEA4B9BD5D9C0C02A271,
	Utf8JsonWriter_WritePropertyName_mDAE1CD6CD231D4BDBD36ED2DEFA7A9AB1DBB27F0,
	Utf8JsonWriter_WritePropertyName_m6E24B02F2463E24E463A2FBACC3FABD86C712149,
	Utf8JsonWriter_WritePropertyName_m48CEDC9718A9DC59E65A3BDB8E61FF58F0F43CA4,
	Utf8JsonWriter_WritePropertyName_m9745E77C67CE1CE4A7FFEBFBD1A49B215616BA21,
	Utf8JsonWriter_WritePropertyName_mF18FCE3386F464FCF73C67F457114F3AAB3174CE,
	Utf8JsonWriter_WritePropertyName_m9E5769869B82691F435838443F4B1F32A9134D55,
	Utf8JsonWriter_ValidateDepth_mB823685607B5551DBB7711165324EEDAB31C5680,
	Utf8JsonWriter_ValidateWritingProperty_mA774D19194EDA65A154C7188ACB9363727C22381,
	Utf8JsonWriter_ValidateWritingProperty_mC581A96093E5B8C0F28A38802637D6BA4E6B831B,
	Utf8JsonWriter_WritePropertyNameMinimized_m54CBAC2EF2D453DCA1C9B748662092292F6F61A8,
	Utf8JsonWriter_WritePropertyNameIndented_mE93A9AF3608723700069C12733B8D67BBF56F81C,
	Utf8JsonWriter_TranscodeAndWrite_m02497951FB0FC80A851B31DC706BA4E315B5BEB2,
	Utf8JsonWriter_WriteNull_m6DFE540D01400EA53C8981F2381BB9A808F19753,
	Utf8JsonWriter_WriteNullSection_m64DDF170B1C5AA3DBFB28E42B70851C8C36B7BF0,
	Utf8JsonWriter_WriteLiteralHelper_mB50BD28FAB2D9A9D8372DDA1587CDA618C575DAD,
	Utf8JsonWriter_WriteLiteralByOptions_mF40749C21DDE2ECCD83A84C932B531870879B3DE,
	Utf8JsonWriter_WriteLiteralMinimized_mCF37DA0347C180E374CD50A5B7699612CB89B07F,
	Utf8JsonWriter_WriteLiteralSection_mED2F87320316A0959FA57AB10B79FC801F25E0F5,
	Utf8JsonWriter_WriteLiteralIndented_mADA2844178A4819A5B286C59B67ED38A3B11F1A2,
	Utf8JsonWriter_WritePropertyName_m18A003196843D1D3F241ABC7AB9E290B57C04DCD,
	Utf8JsonWriter_WritePropertyName_m58C006A592B878D9777F4E5793E423027DD75FF6,
	Utf8JsonWriter_WritePropertyName_m04C61608930141086671CD87B191CCB42B7CAAD2,
	Utf8JsonWriter_WritePropertyName_m4EC2DAE071F9EF28604782A57F202CD14D9F5A3A,
	Utf8JsonWriter_WritePropertyNameSection_mE4638818A05308F694887A41B259D807CFF0D885,
	Utf8JsonWriter_WritePropertyNameHelper_m8A3CEF329C2682F61605A1C57EAF563196D58E55,
	Utf8JsonWriter_WritePropertyName_m996942A298DED618DCBE28108F27E373B2E5D95F,
	Utf8JsonWriter_WritePropertyName_mD5C11D2F04DD4116BD2609292A3B2D05E1C357B2,
	Utf8JsonWriter_WriteStringEscapeProperty_mCD08606811907E99503A17C244B37E19D66831C9,
	Utf8JsonWriter_WriteStringByOptionsPropertyName_mD1E3902B7A1A32B78FD5845C5756B954118412A4,
	Utf8JsonWriter_WriteStringMinimizedPropertyName_m235EA20291770CBE9C0EF62ACB96CE02E213236F,
	Utf8JsonWriter_WriteStringIndentedPropertyName_mCFD4644263D11B444714D883359C350CFFE46C3F,
	Utf8JsonWriter_WritePropertyName_m3FAD66D83E22D24D7263E1079BB6890810EFC2A5,
	Utf8JsonWriter_WritePropertyNameUnescaped_m09979AE493AA8956D5205CE26CECC1F6B66D6AE2,
	Utf8JsonWriter_WriteStringEscapeProperty_mD9E3F7403C744CFF3A069A9DFE3BCD459A768211,
	Utf8JsonWriter_WriteStringByOptionsPropertyName_m39086B7083EDA5468602039C03E6F04B30D255FC,
	Utf8JsonWriter_WriteStringMinimizedPropertyName_m7629BDC4144A3DB500FEDA53A2FBF3EB536D4FE0,
	Utf8JsonWriter_WriteStringPropertyNameSection_m44FD370CB27FCBB506A84E3C9358639F5E38217E,
	Utf8JsonWriter_WriteStringIndentedPropertyName_m18DEAB086D3C46BBD359C512045A1A77EB513141,
	Utf8JsonWriter_WriteString_m384F27B26D0420B7FA6F228E6F703E4D6FA97374,
	Utf8JsonWriter_WriteString_mEA5D1F20772424C9F65052E81FE89F0686254F29,
	Utf8JsonWriter_WriteStringHelperEscapeValue_m4DD21A58FA2D0B801070396213E1E4A5D58E2F06,
	Utf8JsonWriter_WriteStringEscapeValueOnly_m7611554B512BFA5371D7478F894ACA4B7B207529,
	Utf8JsonWriter_WriteStringByOptions_m89197627FBA2D52B62559D6F4C6B820F24BC5A16,
	Utf8JsonWriter_WriteStringMinimized_m9AC498A1407ACE4CBC2B12A2E875A962E580AC26,
	Utf8JsonWriter_WriteStringIndented_m35BE4CEEF434283CB28B295C89A14B0F4899BF60,
	Utf8JsonWriter_WritePropertyName_mAA130A04E9004C4A74F36C3F1396B4FE5868E921,
	Utf8JsonWriter_WritePropertyName_m670F9DE802D05679BF7EB5701E9ADB3027402C11,
	Utf8JsonWriter_WriteBase64StringValue_m4CC0DAD92DF2723492C5DF18B8B3028BA6009C00,
	Utf8JsonWriter_WriteBase64ByOptions_m55F12E59982BAB3D259EB44ABCAE8A57CB157ECC,
	Utf8JsonWriter_WriteBase64Minimized_m160BE7FFE69E00FD96281A193DA692A48E51672A,
	Utf8JsonWriter_WriteBase64Indented_m2A0919AFD3140A557347B134520E5FEDEFEF8A6E,
	Utf8JsonWriter_WriteStringValue_mE130AF1B2A2C307C9A4BE3282CDCA46E708BDAC0,
	Utf8JsonWriter_WriteStringValueMinimized_mE92796D227AC277168F1B2DFBF30EE80C5877C9D,
	Utf8JsonWriter_WriteStringValueIndented_mACD09DBFF483579AB0421C839E4491979D549CEC,
	Utf8JsonWriter_WriteStringValue_mDB0C7D389DBCAA8408DB0FFD6BE056F4DB179732,
	Utf8JsonWriter_WriteStringValueMinimized_mC19B4EDA129797DCDDDAA706633FBEBC6F1C7C7B,
	Utf8JsonWriter_WriteStringValueIndented_mF97D6265D42C31C9CD5B61346DA24B279FF69EFB,
	Utf8JsonWriter_WriteNumberValue_m95366FE40CF93FC6091A43A618B70867460869D6,
	Utf8JsonWriter_WriteNumberValueMinimized_m4F0299106C4EAB1CF9373B92AD508A79B8E114ED,
	Utf8JsonWriter_WriteNumberValueIndented_m2E96C373C9B770E458A9F1CD50B2C603842F2CC5,
	Utf8JsonWriter_WriteNumberValueAsString_m6838AE0510AFCF321853096B370CFA8FF08D6747,
	Utf8JsonWriter_WriteNumberValue_mF264282A28EF611A3174E363C56C810583DE103E,
	Utf8JsonWriter_WriteNumberValueMinimized_mEB0F2A86C89610DEF4BA8247805357A8C6254474,
	Utf8JsonWriter_WriteNumberValueIndented_m977A5605F0FD013FA8D556B37A5903BCF10C2217,
	Utf8JsonWriter_TryFormatDouble_m1E46A985C1868AFE99C22B1C4385D3E55409C631,
	Utf8JsonWriter_WriteNumberValueAsString_m7BE06043327957908883709EA7A8C5FF0F777E89,
	Utf8JsonWriter_WriteFloatingPointConstant_m9B685089C517B3CED834A0EABC369AD77830292C,
	Utf8JsonWriter_WriteNumberValue_m94C41B349B7D55EB269FA36E4454661C72DED51D,
	Utf8JsonWriter_WriteNumberValueMinimized_m58D6CF2A5BB517EFB6EF5D090C42DCEC11C5F1B6,
	Utf8JsonWriter_WriteNumberValueIndented_mE3555B7FFB2B32EFB3F1D37B4EF919F8A4D2DCE1,
	Utf8JsonWriter_TryFormatSingle_mDCEC63E655C729E9BB8ECF387BDBDA0804AFCE4E,
	Utf8JsonWriter_WriteNumberValueAsString_mAC5CE62CE5ADB88A61DC78AD17359B1311C61DC7,
	Utf8JsonWriter_WriteFloatingPointConstant_mC67C37D7A585CBFA4C2C0A70254F5C8567FE28F9,
	Utf8JsonWriter_WriteNumberValue_m60A639D1ADCDC797A1127B323B98F16CD8BAE893,
	Utf8JsonWriter_WriteNumberValueMinimized_mEF778935C8C0B6CB9D0FC62459DC0F8E0F0F1416,
	Utf8JsonWriter_WriteNumberValueIndented_m70FB4B419767F277A8FEF5E6147CECA4DED043C1,
	Utf8JsonWriter_WriteStringValue_m3797F39346137A2D7232677B84B2F690896C4E6A,
	Utf8JsonWriter_WriteStringValueMinimized_m3041C690044C0EED2D5E0B28C58EADC9F73DB5BB,
	Utf8JsonWriter_WriteStringValueIndented_mA63D85F1B2CE38CED7A4CD8CA249914F6D305D74,
	Utf8JsonWriter_ValidateWritingValue_m1EDE5D0CAC37BCB2E4805B0455348BE0832B78A9,
	Utf8JsonWriter_Base64EncodeAndWrite_m4E354E57EB2F24CAB3C90CDA20487D73B6922E7B,
	Utf8JsonWriter_WriteNullValue_mD363DC1B38AAE17A2666646629B9A3640C898004,
	Utf8JsonWriter_WriteBooleanValue_mA04EC87538557C4D69DA626CE79295F85A722BFF,
	Utf8JsonWriter_WriteLiteralByOptions_m3755A2C70936A990E52C40F30BD55AFA97F8725A,
	Utf8JsonWriter_WriteLiteralMinimized_m84ED52B356CB9F4101AE7F94A2D2B5A89FCD270B,
	Utf8JsonWriter_WriteLiteralIndented_mFFA6AFBB368388771828D0F54CA09FEECAAB9220,
	Utf8JsonWriter_WriteNumberValue_m8A2EB2DC02416A0F4B0CA97AFA70B873FBCC3B7D,
	Utf8JsonWriter_WriteNumberValue_m04B3E8A97F3FFECFFCCD5F17B74488E285482FA1,
	Utf8JsonWriter_WriteNumberValueMinimized_mE627304636D30A948A8EE0DA7292EBA8A2F8C2E8,
	Utf8JsonWriter_WriteNumberValueIndented_m3621E4629BDFBC8D31107EACD21F4932575CF463,
	Utf8JsonWriter_WriteNumberValueAsString_m3BB71B7C2534669326AE6C75F57E6E4AD429F05D,
	Utf8JsonWriter_WriteStringValue_m09F57C202C9D190F1EEE5D6AF6C6F11B36ADAAB2,
	Utf8JsonWriter_WriteStringValue_m348BACAA9A8628253717C938871B084D8AB48D80,
	Utf8JsonWriter_WriteStringValue_mE4B6D84C62301EE664A48706F72A91583690A6FC,
	Utf8JsonWriter_WriteStringEscape_m62161ADC6931F29D6D6477EC6EA0C33ED68E3065,
	Utf8JsonWriter_WriteStringByOptions_mB9F334C1CDC0D38156C588C17ADF3499E52A8378,
	Utf8JsonWriter_WriteStringMinimized_mCDE83318D5992A162E60B45316CC28EDB29DF31C,
	Utf8JsonWriter_WriteStringIndented_mC739CAADEF3039DDEE8567FD158DF0B9C609E1EA,
	Utf8JsonWriter_WriteStringEscapeValue_m5F92419D0847CAF6993B1EB667B83A5DCF936007,
	Utf8JsonWriter_WriteStringValue_mAC062B9CFDB5DFD4F1E55081AF5E390D0671616A,
	Utf8JsonWriter_WriteStringEscape_mB59D9F02536516EA8EF1FD9096CE40A2EA996451,
	Utf8JsonWriter_WriteStringByOptions_m6FFDC7E75661DC2BBA64B77C53B8E262814AFD57,
	Utf8JsonWriter_WriteStringMinimized_mC68E8AD0C2D230CD87ADAB875F7CF843D37EDD5A,
	Utf8JsonWriter_WriteStringIndented_mE4AB77B76E3FB99A9836D4A32D8442273D0E3B41,
	Utf8JsonWriter_WriteStringEscapeValue_m10974C00896F712FB5766F448315918EA56BFA91,
	Utf8JsonWriter_WriteNumberValueAsStringUnescaped_m5F0697B41CEABFEAB9EE4CCF8202C31BCCE0CD13,
	Utf8JsonWriter_WriteNumberValue_m89BBC1D792A2DD977776AA582F1DB31A7A76E7B0,
	Utf8JsonWriter_WriteNumberValue_m3075EC515D272E426FE6A49DF02814388AECFBD0,
	Utf8JsonWriter_WriteNumberValueMinimized_mE7D6AF009796F6D436D5688CA8AD185D2F9C2D70,
	Utf8JsonWriter_WriteNumberValueIndented_m8D4E40F1D48E202EE3964E001154CF75F36A5542,
	Utf8JsonWriter_WriteNumberValueAsString_m65853705FCD729A943F6F47CFE0851D8B067011F,
	Utf8JsonWriter__cctor_m65152A2DCCF94AD3B43EA4A0CAB34CD9160F75CE,
	JsonConverterAttribute_get_ConverterType_m4F859C91463CB5C3485CF230A347929B0DFB6923,
	JsonConverterAttribute_CreateConverter_mA104EBFE9E69E3EA640082F5D61F12761FDDF1FF,
	JsonIgnoreAttribute_get_Condition_m352D83EB9F4BF8EAA3C61F69259CBC3E2FEE5C88,
	JsonNumberHandlingAttribute_get_Handling_mEB49A88D50DD9959865987E2A067F73D8E8960BB,
	JsonPropertyNameAttribute_get_Name_mC37BF00D1224BB51B5165DA09B92E48A090FB0E2,
	ConverterList__ctor_m4E3C5A32A90624E0ED5A0DF62DDF8BF134F695E0,
	ConverterList_get_Item_m79FDDF8D8C41749EC27D74A578AA5D376C99FCB9,
	ConverterList_set_Item_mB3A908ACE6F9BE728544B4AA53DD70BDA1356A5E,
	ConverterList_get_Count_mD6915CE1800C4C21C5B5ED83934570F3509D5FDE,
	ConverterList_get_IsReadOnly_mDD798CA9C6464F9846CE1FA15FE4B68FF9F37650,
	ConverterList_Add_m980F6953ECE005778CE39AF01E80BA737D24C016,
	ConverterList_Clear_m52C2EE6E20254E31312DD99B935290AD641DD0E2,
	ConverterList_Contains_m5BBF3F2DEB647C13064D4958948F4574C1214C70,
	ConverterList_CopyTo_mE79D8B1CCA4BAF4CB1BCFEF1EEBD61E7C1F5EBFB,
	ConverterList_GetEnumerator_m363F6BC3F8246EB73EE94B101D403425EE91B2C5,
	ConverterList_IndexOf_m00E33929E63DBF007A48057D66BE5E91C7E2FD7D,
	ConverterList_Insert_mF9607457DB86C593C5D6791318F02B52FE13928F,
	ConverterList_Remove_mF1668BE4C4D66A63642E6BCA2F3DD65137BBF014,
	ConverterList_RemoveAt_m5B3158281E9A5D8FC06FB8AE6175585EB25032C7,
	ConverterList_System_Collections_IEnumerable_GetEnumerator_m6E6DD8CC8B66665EBA5560079712AB72F11A9A7F,
	IEnumerableConverterFactoryHelpers_GetCompatibleGenericBaseClass_m533A7F2E411C4988EC4D744EA5925FA05DB20F9A,
	IEnumerableConverterFactoryHelpers_GetCompatibleGenericInterface_m8E23E177924E5C6D83FA669354E9CE6C977ABC41,
	IEnumerableConverterFactoryHelpers_IsImmutableDictionaryType_m08B2157F3DEFDA39B118F37FCF14353F8A463C53,
	IEnumerableConverterFactoryHelpers_IsImmutableEnumerableType_mDB655BAA8E2BDF78B243B848CB865ED245ACE3DD,
	IEnumerableConverterFactoryHelpers_GetImmutableEnumerableCreateRangeMethod_m821F4E086E5DA87DF7CD1A4B12E723E236B1870D,
	IEnumerableConverterFactoryHelpers_GetImmutableDictionaryCreateRangeMethod_m433C377D25F9CC52A0E9B13E4E2405F34E7F3943,
	IEnumerableConverterFactoryHelpers_GetImmutableEnumerableConstructingType_mC9082C942B14AFBD0426E03155444F22353BE1E1,
	IEnumerableConverterFactoryHelpers_GetImmutableDictionaryConstructingType_m4A915E05D75EC8E6BB552AE5DA5ED8FBD81F08AE,
	IEnumerableConverterFactoryHelpers_IsNonGenericStackOrQueue_m724A1A43FDADC65DC1A640DD140A55616D2542F9,
	IEnumerableConverterFactoryHelpers_GetTypeIfExists_m9605BB4E0549221D4373EC8E201788B2F4D88462,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonConverter__ctor_m23EFDEA2C29481891A2DA60D4E39F996475129EF,
	NULL,
	NULL,
	JsonConverter_get_CanUseDirectReadOrWrite_mE1D5C56BEAC0A084D137B109347864E236355287,
	JsonConverter_set_CanUseDirectReadOrWrite_m0E51E0526E0BD47E24A839266D241496EC091261,
	JsonConverter_get_CanHaveIdMetadata_m27650670CDB655B2030CDEEBF8E469D2F4D1AFE8,
	JsonConverter_get_CanBePolymorphic_m70362671690C01E809BC9121656CF387E1BABF78,
	JsonConverter_set_CanBePolymorphic_m64C5B8CEE959D5087C91F323CB33AA690D78B840,
	NULL,
	NULL,
	NULL,
	JsonConverter_get_IsValueType_mD0A2E2F1397C567881231F647B24B9301A6665F2,
	JsonConverter_set_IsValueType_mFF06DEF51DA822C23B4DD2A4B6B6D25000E69F10,
	JsonConverter_get_IsInternalConverter_m3F6D0C0722D8FD57533189C2645030FB07A96AEE,
	JsonConverter_set_IsInternalConverter_mB054A28BB915D42B82F816CA0E11C9CD57EB0000,
	NULL,
	JsonConverter_get_RuntimeType_m5EEBFDDE9072D9B99D767DFE36D5DC819753C181,
	JsonConverter_ShouldFlush_m66E1D9591D7D86B244C7FC4280FA3E7214425FA1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonConverter_get_ConstructorIsParameterized_mB2021F0E53B080BCABDA8420269391C3661B7174,
	JsonConverter_get_ConstructorInfo_m025ED1FC090F35D33CDABDA3CDB651B0DB96EA9F,
	JsonConverter_set_ConstructorInfo_mFBBC3F48DAA76C204E110C8F905E7EC1FF9F621B,
	JsonConverter_Initialize_mE4E6E9CAC22D9DF6CC40EDAAE657AFC8B61860C4,
	JsonConverter_CreateInstanceForReferenceResolver_m9470CF210E5182867CFBB046E7A024905A473F98,
	JsonConverter_SingleValueReadWithReadAhead_mBF2F841A38AD31B2C37E924F836E769C4B8C8EE9,
	JsonConverter_DoSingleValueReadWithReadAhead_mF6825E41E994CDC4001A8D711F5E80EAFBD5CA67,
	JsonConverterFactory__ctor_m411B07ED997EEDA8B37628F05D4F8DA392BB1956,
	JsonConverterFactory_get_ClassType_m4E2428FF151D03A6D7C4B0564F8A957B04275E68,
	NULL,
	JsonConverterFactory_CreateJsonPropertyInfo_m4A768485F2154AD0D83B102D05C69EDC482B4703,
	JsonConverterFactory_CreateJsonParameterInfo_mC8B045F4D21D063AA97C9492F7E0D71D70EC3C0D,
	JsonConverterFactory_get_ElementType_m56332CF4413D09D8788C4713A8CE4CEC888C3249,
	JsonConverterFactory_GetConverterInternal_m56BD0E54C66E7862785D9D2F92615DC179929EAA,
	JsonConverterFactory_ReadCoreAsObject_m19F79BFD1D59A655393D6423723DEF3ADA3E2E1E,
	JsonConverterFactory_TryReadAsObject_mF2E8D9B0B3033B868331F6287F6FBCC8DEC04E8E,
	JsonConverterFactory_TryWriteAsObject_m7CD0F76414173623EBA5FBA8BE8426F53468AA90,
	JsonConverterFactory_get_TypeToConvert_mEBC0D6524B9C810A51A935D00E6EBD4BC71320D0,
	JsonConverterFactory_WriteCoreAsObject_m7EBC5DED957FAE1C4AE026076BB956E2C17AB2EF,
	JsonConverterFactory_WriteWithQuotesAsObject_m4F587F5EDDF4E25CEFF97328025105FE7AE8CEBD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PreserveReferenceHandler_CreateResolver_m61A6B3CD749479AE6FAC893ED47B60D93483637A,
	PreserveReferenceHandler_CreateResolver_mAFC8110F8A4B135C5247E51560121FAFFFE5C611,
	PreserveReferenceHandler__ctor_m7BDF9B35F7B769D3A6CB136767C82D4B9D38F3C5,
	PreserveReferenceResolver__ctor_m49E87B8658136E0FD110E906BF1C29F6D3C8B9B1,
	PreserveReferenceResolver_AddReference_m3DF7BEEE19A9EB216AD4B189FE8AA49782315DA6,
	PreserveReferenceResolver_GetReference_m1658DC64E6481408E2FFE3CA51C475E0E1345EF3,
	PreserveReferenceResolver_ResolveReference_mF998D0F7D8D4EA8062423B586318BE5DB1A53AFC,
	NULL,
	ReferenceHandler_CreateResolver_m0F2A28E295E7D943D7D437AECF26A6B14C6F81FB,
	ReferenceHandler__ctor_mE34153A3037B9E3F9E724C1C7E745ECE6DCEE20E,
	ReferenceHandler__cctor_m96BFCF6E4DBCDCA2F2DEC6CC24B033DB1EE68847,
	NULL,
	NULL,
	NULL,
	ReferenceResolver__ctor_m0424148914AB26FF4CCA0EC9E12DDDCABDA9E28D,
	ReflectionEmitMemberAccessor_CreateConstructor_m221DFDF2407A6C1CCF2ED82D823FE121920E9917,
	NULL,
	ReflectionEmitMemberAccessor_CreateParameterizedConstructor_m7920618ADC17968F4F28966E1FE092E825095FD8,
	NULL,
	ReflectionEmitMemberAccessor_CreateParameterizedConstructor_m5A960715365178AAF387F753417C7FDD328DEE07,
	NULL,
	ReflectionEmitMemberAccessor_CreateAddMethodDelegate_m2B0F4B22C1D2C091CDD9D8F81A3AF90AE10324B9,
	NULL,
	ReflectionEmitMemberAccessor_CreateImmutableEnumerableCreateRangeDelegate_mEFB4815C703391571D64424D2459EFDBA7634277,
	NULL,
	ReflectionEmitMemberAccessor_CreateImmutableDictionaryCreateRangeDelegate_m288E3B2E8DB0D3FA4AD1503F1A44DE93256C093F,
	NULL,
	ReflectionEmitMemberAccessor_CreatePropertyGetter_m7F674907D6D359360B0BD659533368035C45B21B,
	NULL,
	ReflectionEmitMemberAccessor_CreatePropertySetter_mA11A734C19B083FA596E2DC9BC07537D52CD93CE,
	NULL,
	ReflectionEmitMemberAccessor_CreateFieldGetter_mE912BAADEBC2AEA8B3B7C5E40105971CC4FA7F99,
	NULL,
	ReflectionEmitMemberAccessor_CreateFieldSetter_mA9CC0A624BFEB1288262AF266B9024C0CD1A85A8,
	ReflectionEmitMemberAccessor_CreateGetterMethod_mF4677AF8926517787BC70414DC546A932B6E91DF,
	ReflectionEmitMemberAccessor_CreateSetterMethod_m88900BFD67C6F3A6E693AA79CC8627C6A7A2C9E9,
	NULL,
	ReflectionEmitMemberAccessor__ctor_mEEDBC30DA17766C88F940C0FF959D62EDE6625F4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IEnumerableConverterFactory_CanConvert_m231374E9CF8BAB67CB2765097519B3D4447867AF,
	IEnumerableConverterFactory_CreateConverter_mDDBEE8EA353EBF17168729A4B66455C4FB36B01B,
	IEnumerableConverterFactory__ctor_mDABCD1B66296E504F4DC3A8E1149D5125C3AA23C,
	IEnumerableConverterFactory__cctor_m721A13DC766EE5A00B5DE10566AF44E3FBA7E843,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObjectConverterFactory_CanConvert_mB99DFBE073030B840D2EEC29BB93D030F0B3BE85,
	ObjectConverterFactory_CreateConverter_mED2E71E2B9CC41698416C44DABAAE5C0F2D86035,
	ObjectConverterFactory_IsKeyValuePair_mD31CC5779AA7290D8BCD0370AC17627E7A3188FD,
	ObjectConverterFactory_CreateKeyValuePairConverter_m83BA2C182434B64E5B9ED7CF562DC6B9A65B2695,
	ObjectConverterFactory_GetDeserializationConstructor_m5ACBE28777CD74A128A057353CE7B4FB50519D88,
	ObjectConverterFactory__ctor_m7435CDB6BC1739F0DB6034CCFCD7427978BB540A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BooleanConverter_Read_mA060CFE94A3EDF88396699A47FBEA35D745EE6F6,
	BooleanConverter_Write_mEF6EB096A12DC91C0A1E8E191C8E85CE30323B1C,
	BooleanConverter_ReadWithQuotes_mAA5D212236BD74C3EABB2061AC413892A17F1D98,
	BooleanConverter_WriteWithQuotes_mB078C7BC7C8D76413F4AC2B03C0AA9F8A98AEF9A,
	BooleanConverter__ctor_m73520293AA4F7B4AA13B4996422D06E9FC59219F,
	ByteArrayConverter_Read_mC0700F13A826469FFF3EF807A6ED2502C9111DE6,
	ByteArrayConverter_Write_m65263ACC309C67044C0CBFA8A650928B3986CD0B,
	ByteArrayConverter__ctor_m53090E4CF9ECB8DE38281AF72B1EF8D7FE4964D8,
	ByteConverter__ctor_mA55101DFFF8A5C93C7A499A54244939462B8B3B3,
	ByteConverter_Read_mDC3E0CBCB4890A61CEF8C497965CFF8592A50F9A,
	ByteConverter_Write_m19B75553D83C5290DD9E9F12BEC96C1D6BA2AB55,
	ByteConverter_ReadWithQuotes_m5A34F4540B33EF89BBD2FD49A30F71389AE214D1,
	ByteConverter_WriteWithQuotes_m6B063B136D27B744C1A4DD215F0C841B269976E0,
	ByteConverter_ReadNumberWithCustomHandling_mB99BC6EFD402AE1D9B2C7B842115D0427C59CAAA,
	ByteConverter_WriteNumberWithCustomHandling_m02A08FD12E7B89FA2F7E1B63382A2E61375399D4,
	CharConverter_Read_m7AAEF695EE3A245DE05FAE6D134A45FCC82DCAA0,
	CharConverter_Write_mE5A3749A222B385F5317DB00835B689BB2A08BF6,
	CharConverter_ReadWithQuotes_mF739825C3E7772DEF7AFC53A1D17A5AA83DB214A,
	CharConverter_WriteWithQuotes_m3C431FBCDEFAABB469A3C5C192C8A33690FFFDD3,
	CharConverter__ctor_m0FFE22128925F6ABF4434B3DB47EAB50A42587CB,
	DateTimeConverter_Read_m3A191F15CD1100212E16C805D823CF5CE588A291,
	DateTimeConverter_Write_m3B662D066AF10FD768E0DF0B37D90EBE25B991C9,
	DateTimeConverter_ReadWithQuotes_m9EFBBF9AC5A9612142821FB8D62A3F172BE1534D,
	DateTimeConverter_WriteWithQuotes_m50C81ECDEB97861420E3EB853BC67EEE5F4383D9,
	DateTimeConverter__ctor_m7ADE373DABAA4C6945FEBA3DC7D0CFE0EB09DA08,
	DateTimeOffsetConverter_Read_mB66E0433DC0829796F3FEF1C272D3894478F1083,
	DateTimeOffsetConverter_Write_m515BB4D2F68EE4CC4BEFA9F270B138D31D34C133,
	DateTimeOffsetConverter_ReadWithQuotes_m1FA9C3E4123136059CFB6868D4CCD749E178BC64,
	DateTimeOffsetConverter_WriteWithQuotes_mD350D908ADCD5753EB8741D8A7A766F631B9C603,
	DateTimeOffsetConverter__ctor_m06F16EBCABD6B5429D93A11BA016EEC23F0DAD35,
	DecimalConverter__ctor_m71F64B31AB7035005DC0BA5A3FCD3453A08CB81C,
	DecimalConverter_Read_mB9806D6C3DB04E65EE1C75E294F9AF19AFF9A0BF,
	DecimalConverter_Write_m299FE718D677710F4BB035326B6DB66C60DC9AF9,
	DecimalConverter_ReadWithQuotes_m7F355209DB610CE31A4B29C533B6D54C35A94C7A,
	DecimalConverter_WriteWithQuotes_mFDAB142954E870EFB984320E4A386B33013AF2BF,
	DecimalConverter_ReadNumberWithCustomHandling_mACB2E1CCB5852C68EA7B35A0B26DDEBABBCA2622,
	DecimalConverter_WriteNumberWithCustomHandling_m48A84AD7028DF6B9423F7B1C07182888CE9E8F15,
	DoubleConverter__ctor_m92667AE837A19FBE60BF73C99E70A4E1E60FC3A7,
	DoubleConverter_Read_m306BBC93E2EC97020D4DA1711C2B3A07B990E796,
	DoubleConverter_Write_m1C9598EF4151552915C4DDF9EA8EA907F59C4EB4,
	DoubleConverter_ReadWithQuotes_mB065E5F48659B9A8F4184CF0A63FE674164E0631,
	DoubleConverter_WriteWithQuotes_m15026451CAEBCB918FDB5203E24D18064A23D820,
	DoubleConverter_ReadNumberWithCustomHandling_m85D49248F9A54DE696D54AAD2014465820B77AB3,
	DoubleConverter_WriteNumberWithCustomHandling_mA1CD28C82CE72537466654C7BE65D671CE9F0431,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EnumConverterFactory__ctor_m15A64190A6484A0B17931CB9BDF8B83B48CA25E6,
	EnumConverterFactory_CanConvert_mA049159C43F1C65F2419AFF90174445956C1B549,
	EnumConverterFactory_CreateConverter_m9961C58C3723DF7506C2103327794C0D9CF394A6,
	GuidConverter_Read_mB0F5E39B557DACE2AC1B7BCE57983730EBBE45C1,
	GuidConverter_Write_m9DFCC7FC6E77B8C456D6BADF88F164495D352A3D,
	GuidConverter_ReadWithQuotes_m8BB9C17128565576A475634CF060057C322464C2,
	GuidConverter_WriteWithQuotes_m757E11C8356B6DBF6710F8045522525C17B4A9DB,
	GuidConverter__ctor_mF590625FEA9B6CBC91D8EC8C14C0536253560F21,
	Int16Converter__ctor_m86D524897BCCD20229BE08AF9C1679EA6B576FCA,
	Int16Converter_Read_mD87641DE857747D4472598CE10F0622790EE163B,
	Int16Converter_Write_m5BC7AB83FC8EEB9276B73044468697F4EE2E8B4C,
	Int16Converter_ReadWithQuotes_m04208165BBB6E4EF0432172F1131B5B0E0ED9ED9,
	Int16Converter_WriteWithQuotes_m3439C486B6ACAE8127387AD1BE7E4FA0603D8C48,
	Int16Converter_ReadNumberWithCustomHandling_m27677800F58180D2B0E122DF7D71EE040284A0DA,
	Int16Converter_WriteNumberWithCustomHandling_mD26208F738CE162BBCA52CAC638D219AD9881787,
	Int32Converter__ctor_mCB08B1BDE0301CC5409FF5D5C83FF331F0B2AB67,
	Int32Converter_Read_mC629132D77640298A138493168F9A02CEA556859,
	Int32Converter_Write_m661425DEE9165EE96C47BE35CE260D9111AC2FD4,
	Int32Converter_ReadWithQuotes_mE669B7CBE3DF0F76EFB08EA7A02427DD3EA6857F,
	Int32Converter_WriteWithQuotes_m08F626C30DF839F718FFE140AC62D2F7424AA41D,
	Int32Converter_ReadNumberWithCustomHandling_mE977076A9DF61259DAE6431C25519F0753BD37D4,
	Int32Converter_WriteNumberWithCustomHandling_m0069DE3BEE343D2C68D48794AA0F589AEEC64562,
	Int64Converter__ctor_m8770BF9B89BAA0D9B1785DDFBDFFEE6EBB2C4DBC,
	Int64Converter_Read_m3A9C416AD2A723AC96B16AEE7B7EED2C77D87572,
	Int64Converter_Write_mFDEF66D026EE6C924DA62067E7AB08E1A634D96F,
	Int64Converter_ReadWithQuotes_m0CFCA760D2727B472D65006DAAEBC340BBFD8BFD,
	Int64Converter_WriteWithQuotes_mC9E2927647C239E8386DD3D29A5A1C613F163C6E,
	Int64Converter_ReadNumberWithCustomHandling_m7E8D2EFF8F6F51DEA9A6178C395175CCCF83ECAA,
	Int64Converter_WriteNumberWithCustomHandling_mE8750771068DB3262795A466E37327577BC8841B,
	JsonDocumentConverter_Read_m22C677023EE78581F7A17117657463C573F6102D,
	JsonDocumentConverter_Write_m785E1CCA7D96A293C3C950286D5C5BB41CE3C45E,
	JsonDocumentConverter__ctor_m5685557D2205CE842FD9DA942ADD43038B19B2C3,
	JsonElementConverter_Read_m7DAB7D809BE1759D05DA2E6E01B4AA6D655D9F02,
	JsonElementConverter_Write_mC9196D59EDD7AA14E635570DFED3B82E40C9F507,
	JsonElementConverter__ctor_m58273CA280B619F513BA2C282EF715CC0D123ED3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NullableConverterFactory_CanConvert_mD59B0889DB3FA18479000666074236DF712F37FA,
	NullableConverterFactory_CreateConverter_m1C25C5A9992FC110FD51D6BD93FE13205279207C,
	NullableConverterFactory_CreateValueConverter_mA04BCC0718271A323490045E2A1A2A866E6EC41B,
	NullableConverterFactory__ctor_mE0171FEEB922C21A6FFCE32602A43BA9A83BDC45,
	ObjectConverter_Read_m7D02AF462AC6A294545480280759B17D769D34BC,
	ObjectConverter_Write_m33F55DD85790AA5724DEDBFDC28D77D0E4097569,
	ObjectConverter_ReadWithQuotes_mFCE9722383C95BBF3414B179BDC171039FE84E75,
	ObjectConverter_WriteWithQuotes_m58045B9F88612C239727514984B0B8B094CBCE9E,
	ObjectConverter_GetRuntimeConverter_mDF4C471A97B3A1B1B63AC08DA54D966C8084E699,
	ObjectConverter__ctor_m467BB63B2F6F648856A50C2FB5A032417197C6B3,
	SByteConverter__ctor_mE78030BB54D847713565A1EE2947D0B41D13DB6E,
	SByteConverter_Read_m2312E1F3B4500FEEBE58865A9E3068812029B7F8,
	SByteConverter_Write_mC3B4E520F0FC579B6DA4F5B8934EAE1E2400BFBE,
	SByteConverter_ReadWithQuotes_mED3F7F0E3A56B3CFB8D22C9C90D6DD32D635CE11,
	SByteConverter_WriteWithQuotes_m4AE9887AD3B80334AC275630E07AAF1F7BC499FD,
	SByteConverter_ReadNumberWithCustomHandling_m998184C8E5D53B4069B5E750F0E8753772B495A3,
	SByteConverter_WriteNumberWithCustomHandling_m1930B514F1D67945E731EAD6BC21E3095AA9FEA6,
	SingleConverter__ctor_mAB5D16C009474673ECEC4623B965416835ED55A1,
	SingleConverter_Read_mCA530140EF771ED162DEBF49D996542D7D30D082,
	SingleConverter_Write_mF5678401B8FF2D4D42302FC199619D7A50530F2D,
	SingleConverter_ReadWithQuotes_mC7C3FC17B07CD57C7BF1186E5716FEBD6EF93F92,
	SingleConverter_WriteWithQuotes_mC11D28C5ADD14F30CB4C61DD60B1E5382B3F0DBE,
	SingleConverter_ReadNumberWithCustomHandling_mC2B5EF3CE86BCC7EF0E8692CB133E02B50BCC465,
	SingleConverter_WriteNumberWithCustomHandling_m718E47005E82F60C438A5CCF5621F016FCDE8F04,
	StringConverter_Read_m63DAABF404AABDFA81B2A8A35C4B56063D531618,
	StringConverter_Write_mBFF51D23748E2097EC16786A5D55D9E4611B629A,
	StringConverter_ReadWithQuotes_m9019E186DE8BA663F52F4EB9D99525F668ED2A5A,
	StringConverter_WriteWithQuotes_mB4EBAFD89D394971AC8A306E555F101CB71C1732,
	StringConverter__ctor_m8C0DE7B66476F75C66052B85214154F85D6FE935,
	TypeConverter_Read_m3095C07F1808F050CC61DCB5FBE12616C9794A1C,
	TypeConverter_Write_m5432822B54114CC9DBF55A1F8DEEB692A669FC85,
	TypeConverter__ctor_m05A345D313E72F986686736087B892D1FB4E0E88,
	UInt16Converter__ctor_mA2C387BBC597B9D7EF97EBCC7DF90C652E52CEB0,
	UInt16Converter_Read_m6D7450E228F8E7AF6C1DB6B21FDE9C53098436FA,
	UInt16Converter_Write_m20085E8B4C37F1841838E7A30490E9B3DDF59903,
	UInt16Converter_ReadWithQuotes_m714FF9DF75950DF48CC178E8AED33178578D1911,
	UInt16Converter_WriteWithQuotes_m12C6D51D2A7EC43DC6A98AE25EE1B8D288BCEBCE,
	UInt16Converter_ReadNumberWithCustomHandling_mEA553EB68F2972D0BA040527D52F4A97E3127557,
	UInt16Converter_WriteNumberWithCustomHandling_mEF01FA814FFED395E66F4D2FA44B4CFE1E8997A6,
	UInt32Converter__ctor_mA850D06A31828B8B9FCCED129CCF97A4EC42F57E,
	UInt32Converter_Read_m7A1253925990C5482073E6CEA368FD8D075A097E,
	UInt32Converter_Write_m583E699545AD78316A39ABD56B08041A3B36A3AD,
	UInt32Converter_ReadWithQuotes_m51AF839875B04C7008A186A38C39563312C0A33F,
	UInt32Converter_WriteWithQuotes_mF3E8795F7DFAFF2BE7E9EDF15DF5F00F18AB9726,
	UInt32Converter_ReadNumberWithCustomHandling_m8C2076E4B461CDDC8F15145903407EE7408A4D3A,
	UInt32Converter_WriteNumberWithCustomHandling_mBF70E74261DA438E9E8F47EB11F2C418E9607ACA,
	UInt64Converter__ctor_m1A91E6155CFBC2E4FB7114C4495224C6690AB7D8,
	UInt64Converter_Read_m61F45A0B0E0A19DB21993C3E8D96A1BDCCC38AFB,
	UInt64Converter_Write_mE77B2428AD27F58E36BD22398725ABCA8F1828B8,
	UInt64Converter_ReadWithQuotes_m8F774E85EDA4F37BB61E745AA125749993ECFC6B,
	UInt64Converter_WriteWithQuotes_mB40244929B49DD209E9CAF4F0F5BB0A22E090281,
	UInt64Converter_ReadNumberWithCustomHandling_m55EBD0E07D476D8F263139A2BCB2D47F7EA13915,
	UInt64Converter_WriteNumberWithCustomHandling_mA4344C181271BF3C587A75B52645E5E7F0DD6B54,
	UriConverter_Read_m95D07717B24C17A0128D02B5EBD9D692352D3DCF,
	UriConverter_Write_m9F22D832BB748D7F9F22D561C9502B7D9285442B,
	UriConverter__ctor_m40EF5BDDEF48CA59C01581AA9E2F791B1BEDF227,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m49E2E4F63C1CBDA71284D33E4FFA5C738240F7D9,
};
extern void BitStack_get_CurrentDepth_m29C47DB4A4B69D9C76A93EBA1637E97A243FC869_AdjustorThunk (void);
extern void BitStack_PushTrue_mBEF1D3C195EA821E02716255C61AFB88258D5BAB_AdjustorThunk (void);
extern void BitStack_PushFalse_m83CDC7E063685BF4FAD27FDCD7F51CF8D49EBAD3_AdjustorThunk (void);
extern void BitStack_PushToArray_mE3486049BF012A03170FBC4C4BF1CE139F81F6C0_AdjustorThunk (void);
extern void BitStack_Pop_mEBB94B5C8DD6FACC49DCB2A2A7D365E5A75A4FD2_AdjustorThunk (void);
extern void BitStack_PopFromArray_mC5F5242B631329764A700C94FC08270FE1803691_AdjustorThunk (void);
extern void BitStack_DoubleArray_m46DB748394540255EF86FBD4970988C81DF3395F_AdjustorThunk (void);
extern void BitStack_SetFirstBit_m9263D9E5C36F9375CDA674AC231BCF1FDF858472_AdjustorThunk (void);
extern void BitStack_ResetFirstBit_m71CD0C8BD6150A422ABAABF746E6F0EC53FD8359_AdjustorThunk (void);
extern void DbRow_get_Location_m26C010A9C88629FBF5B9BE4AB8DA90E22422CFAC_AdjustorThunk (void);
extern void DbRow_get_SizeOrLength_m84D8BF651739FC1923CCEA2500DF7D9929DD8ACF_AdjustorThunk (void);
extern void DbRow_get_IsUnknownSize_m72AF174FA7DFE73898270932D83924CB0DA5C52A_AdjustorThunk (void);
extern void DbRow_get_HasComplexChildren_m8900AC0494A74A8EE1BCA197C6223110CF35B997_AdjustorThunk (void);
extern void DbRow_get_NumberOfRows_m1C82FF779E2D2653D42E81A38364AE4CC2B7DCAE_AdjustorThunk (void);
extern void DbRow_get_TokenType_mCCE39C67D2D66F0053D567BD45AD095D34977063_AdjustorThunk (void);
extern void DbRow__ctor_m5A251F6E7871A6B66B5CC2E73937621035F95178_AdjustorThunk (void);
extern void DbRow_get_IsSimpleValue_m2038B00EE08F425ABCD2BC3C24A151E7EDAE3927_AdjustorThunk (void);
extern void MetadataDb_get_Length_m22933560D56DEB73CDA8422130DFB5A6089E523C_AdjustorThunk (void);
extern void MetadataDb_set_Length_m1E5804A10F184287716024455518919240C3E635_AdjustorThunk (void);
extern void MetadataDb__ctor_m39EBC88D82771277ACD7B74A804CC43263D5B706_AdjustorThunk (void);
extern void MetadataDb__ctor_m032FBF24011241945608F14DD9D7A12DB7CB8535_AdjustorThunk (void);
extern void MetadataDb_Dispose_m77CE10F7F04101B702C044AC728B6B6E8E912569_AdjustorThunk (void);
extern void MetadataDb_TrimExcess_m990B41FACFAE4EC6756F75A83B7130A8FD6EF21C_AdjustorThunk (void);
extern void MetadataDb_Append_m2537E212C18338E2810CC9B80A5DD82B897DAFE7_AdjustorThunk (void);
extern void MetadataDb_Enlarge_mDE37991365E52CA27B9FAE0F2A8964AA102BDD5A_AdjustorThunk (void);
extern void MetadataDb_SetLength_mFC66B57EF58895AFFE2DAD33297935F63C5B8F79_AdjustorThunk (void);
extern void MetadataDb_SetNumberOfRows_m5D37816F2AD652AB316EFBE4B0A2643457B72C82_AdjustorThunk (void);
extern void MetadataDb_SetHasComplexChildren_mB746E8ED47FF55757A24BD397A0E4D91E18F8EAE_AdjustorThunk (void);
extern void MetadataDb_FindIndexOfFirstUnsetSizeOrLength_m9BB4939039E8059B31868E36E84F888B26BAE35E_AdjustorThunk (void);
extern void MetadataDb_FindOpenElement_m8CA68EDBB9605702DCA7BB048DC51689A38ADCB7_AdjustorThunk (void);
extern void MetadataDb_Get_m2EE4C63C4D6B477C49D85DA90D6CFFC009A0B662_AdjustorThunk (void);
extern void MetadataDb_GetJsonTokenType_mF9DF07B766B9E488CD4A203A1F0BE3D09551A41E_AdjustorThunk (void);
extern void MetadataDb_CopySegment_mF7BE8719D6FE1E244B55E9E3251855DD065A4B1E_AdjustorThunk (void);
extern void StackRow__ctor_m2A8EFF70E90A96B442084512D9788BBA144AA25F_AdjustorThunk (void);
extern void StackRowStack__ctor_mA8C386AAC2C88F726223EF8F9CED4C3E79948976_AdjustorThunk (void);
extern void StackRowStack_Dispose_m442EF445F037EC392B2F1494ACEFF8D4E6B2A052_AdjustorThunk (void);
extern void StackRowStack_Push_m4F9FD4AE22507CABB234B17F9D72F52F0BF9215A_AdjustorThunk (void);
extern void StackRowStack_Pop_m59CEFB7385936EBFB490BCFEB5001B63F6E79B31_AdjustorThunk (void);
extern void StackRowStack_Enlarge_mB7433A4A8E2C398B72C14512663278632CA70D89_AdjustorThunk (void);
extern void JsonDocumentOptions_get_CommentHandling_m9486C1460A2DC728E08C0E0FAED390C30503FAC7_AdjustorThunk (void);
extern void JsonDocumentOptions_get_MaxDepth_mB728B90176B962CD67750F71D6AF5FCB816CBF15_AdjustorThunk (void);
extern void JsonDocumentOptions_get_AllowTrailingCommas_m03274EEEEC6A5BC79CCCE3E89FCE91179F7DE313_AdjustorThunk (void);
extern void JsonDocumentOptions_GetReaderOptions_mBC8F60D77D1AE7A350DFF94D8869A6177015540A_AdjustorThunk (void);
extern void JsonElement__ctor_m8E42B93E64E1C54BD78FF8163CD5C482E0EAA44B_AdjustorThunk (void);
extern void JsonElement_get_TokenType_mD6DC1A601E15744E583F39CFEAED4C161168C456_AdjustorThunk (void);
extern void JsonElement_get_ValueKind_m14EFE30FAA112F5199CBD9C42DC9C3AEF6ADA1B5_AdjustorThunk (void);
extern void JsonElement_GetProperty_mB0BB5EA6AC89BB64A9F3BD39EE474AD2D26F72D7_AdjustorThunk (void);
extern void JsonElement_TryGetProperty_m11C90F70F1EE0DA94CD3E752FD0259C599687D46_AdjustorThunk (void);
extern void JsonElement_TryGetProperty_m7322A7C18BB189A5E607ED747340376EB5FC173E_AdjustorThunk (void);
extern void JsonElement_GetString_m7AE007D2F1B4016AA1B53BF79B1A1DD1EA42EB94_AdjustorThunk (void);
extern void JsonElement_TryGetInt32_m5FCAA7B399C4469AFFE24400FDDBE78F8C60041E_AdjustorThunk (void);
extern void JsonElement_GetInt32_m21DEB1B177269FFB57C09E9B094DF8C719926A73_AdjustorThunk (void);
extern void JsonElement_GetRawText_m054A9C6C86FA951676F21FEB0ABAAEE0835A11EE_AdjustorThunk (void);
extern void JsonElement_GetPropertyRawText_m575B8D8F5C386BE964EB43964706F19BC0E72233_AdjustorThunk (void);
extern void JsonElement_TextEqualsHelper_mE1DE405E05A250F42B364021CDE5BDD629E7C6F5_AdjustorThunk (void);
extern void JsonElement_WriteTo_m8802C6ABB5C038C5E111274B0FED5702BFB72B10_AdjustorThunk (void);
extern void JsonElement_EnumerateArray_m9B35AF71578078459E6DAF2D7416D07F327E6E38_AdjustorThunk (void);
extern void JsonElement_EnumerateObject_mD4E563B682ABDCB35E2D27A573BB903CE3EA7D76_AdjustorThunk (void);
extern void JsonElement_ToString_m9E0942575D303AD18247677C192C7E065AE83634_AdjustorThunk (void);
extern void JsonElement_Clone_mCD2B3D79FE552E8FA4A29C9244675D1926B7EC50_AdjustorThunk (void);
extern void JsonElement_CheckValidInstance_m175423F0C65D579DB09D5B1C9ABE2E643064DC8E_AdjustorThunk (void);
extern void ArrayEnumerator__ctor_m1D7B7BD7496B0CD223EE9399B5A57725AEEF7381_AdjustorThunk (void);
extern void ArrayEnumerator_get_Current_m5F5C30DD80B5F2331BF392DA21D4EB06E050F827_AdjustorThunk (void);
extern void ArrayEnumerator_GetEnumerator_m938DD30A37AEE868DC5935D0CDE254FA6CAFCF97_AdjustorThunk (void);
extern void ArrayEnumerator_System_Collections_IEnumerable_GetEnumerator_m2E636672376124E9810A741FED6ACE7BFD20939E_AdjustorThunk (void);
extern void ArrayEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonElementU3E_GetEnumerator_m9ABEE9A87DD3D50D065C9738B683D691A143539B_AdjustorThunk (void);
extern void ArrayEnumerator_Dispose_mD4E9E4516CF8CC2CEA173A59B8407A94271C34D2_AdjustorThunk (void);
extern void ArrayEnumerator_Reset_m3DB230A1965DE1E4F9DBEC7BF58635D2AC1FA823_AdjustorThunk (void);
extern void ArrayEnumerator_System_Collections_IEnumerator_get_Current_mE0067C8DCE7157F9C9695C3FF3FDD5CEED826422_AdjustorThunk (void);
extern void ArrayEnumerator_MoveNext_mF1B0AFD404AA0995A6BE8106CE602684BBE47433_AdjustorThunk (void);
extern void ObjectEnumerator__ctor_m703D9D5B13C21AC9E29F92FAB758D52A102C8F01_AdjustorThunk (void);
extern void ObjectEnumerator_get_Current_mC1865163B29F8CBEAC81A2E5C4A26AF3B997C7BC_AdjustorThunk (void);
extern void ObjectEnumerator_GetEnumerator_mEE9CACFE7559A6D381D802909FE21EDA46FC103C_AdjustorThunk (void);
extern void ObjectEnumerator_System_Collections_IEnumerable_GetEnumerator_mA2F0FD6FAB9BEF8BA08389BE66483F44BAF90BC5_AdjustorThunk (void);
extern void ObjectEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonPropertyU3E_GetEnumerator_m2F6759C0BBDA3C73CB81373048E97DE7E10D0AB0_AdjustorThunk (void);
extern void ObjectEnumerator_Dispose_mE6013E4D463D4DFAE6D9A4458393358F84F4115A_AdjustorThunk (void);
extern void ObjectEnumerator_Reset_m29B204531B2E013AE91F1B6CD394EC9E32447E5C_AdjustorThunk (void);
extern void ObjectEnumerator_System_Collections_IEnumerator_get_Current_mC740D819F66BC7DB0738A495920A650C31444B56_AdjustorThunk (void);
extern void ObjectEnumerator_MoveNext_m7D620190FB99A0075BE003767FC70105507DA629_AdjustorThunk (void);
extern void JsonProperty_get_Value_m6ED717E9179F9D4A00DF42D36A191A8E2BB8C402_AdjustorThunk (void);
extern void JsonProperty__ctor_m3447EAB80FB5BA6813D3F9F18EBAD08B7FCFB312_AdjustorThunk (void);
extern void JsonProperty_EscapedNameEquals_mF067EB713ECF2C089B42447941C7A34C50C02352_AdjustorThunk (void);
extern void JsonProperty_ToString_m9F70631770655A6F3B6AEAA2C3717D17A1D0F573_AdjustorThunk (void);
extern void JsonEncodedText_get_EncodedUtf8Bytes_m7D24410A44ADF371EDDBA9059403D34F98B463A1_AdjustorThunk (void);
extern void JsonEncodedText__ctor_m7F8DBA12F3C948E827627FF1300E95AA48E42CEA_AdjustorThunk (void);
extern void JsonEncodedText_Equals_m26E3AB94F984E7BAE83FB67FD47A7C5A0BB116E4_AdjustorThunk (void);
extern void JsonEncodedText_Equals_mE176157268A0949E8C234E0175B00F9FF800B6C9_AdjustorThunk (void);
extern void JsonEncodedText_ToString_mA0D2C5B818AE0E302B31B5FB722ADBFA30F5BA8B_AdjustorThunk (void);
extern void JsonEncodedText_GetHashCode_mCFF493ABAB2853A884DE0BCDFBB1BC35BE1C04A6_AdjustorThunk (void);
extern void DateTimeParseData_get_OffsetNegative_m6E163581952036148996FE989BA72F37B7178A3F_AdjustorThunk (void);
extern void JsonReaderOptions_get_CommentHandling_m785941C88C0D78EE452E47079133CA53EAA86918_AdjustorThunk (void);
extern void JsonReaderOptions_set_CommentHandling_m3A14D93E8354AB403D202AC5352A0355DEF47641_AdjustorThunk (void);
extern void JsonReaderOptions_get_MaxDepth_m2F0DA8E50E5D545945DB32DFFFA74E941849F3F6_AdjustorThunk (void);
extern void JsonReaderOptions_set_MaxDepth_m2AEBFDFBFEC808E1E4902FFF561FAC990FD8420F_AdjustorThunk (void);
extern void JsonReaderOptions_get_AllowTrailingCommas_m8D5027DF6AD1F91F789AC6AF9A7BF550E81B256D_AdjustorThunk (void);
extern void JsonReaderOptions_set_AllowTrailingCommas_mB7C7B82225E31EFECAC172BC8CBF073C09A4036B_AdjustorThunk (void);
extern void JsonReaderState__ctor_mD1D4FD65E3D005FD3B169BE5F949DCBCB638D5B2_AdjustorThunk (void);
extern void JsonReaderState_get_Options_mC7B2AAB4CD55B90A1EB81F03B82BDEB9E8ED3894_AdjustorThunk (void);
extern void Utf8JsonReader_get_IsLastSpan_m0D510710CF414FB8B8403E4051E06289C14E5B17_AdjustorThunk (void);
extern void Utf8JsonReader_get_OriginalSequence_mFC7BF57B6E80D112834FAF24855EF4FC82F928B0_AdjustorThunk (void);
extern void Utf8JsonReader_get_OriginalSpan_mCA0CC6DA6C7595E089AE833D3E8FAD70196ED6D3_AdjustorThunk (void);
extern void Utf8JsonReader_get_ValueSpan_mD7C0AA41BFEC523AEF71D7AF4BFF784F84CC9AF8_AdjustorThunk (void);
extern void Utf8JsonReader_set_ValueSpan_m37AB280FA0870B85790F31CC32FE61A145BB73DE_AdjustorThunk (void);
extern void Utf8JsonReader_get_BytesConsumed_mEF9EB31417202F307C10482D117C0D1F1383746B_AdjustorThunk (void);
extern void Utf8JsonReader_get_TokenStartIndex_m616B80F3D48063970EF25BC002FE2B7512948D3E_AdjustorThunk (void);
extern void Utf8JsonReader_set_TokenStartIndex_m391AF219C657ED98FC368E82D7129EA13E4B744F_AdjustorThunk (void);
extern void Utf8JsonReader_get_CurrentDepth_m09EED2F622F6DF3BE234674E7532337C2728EBA9_AdjustorThunk (void);
extern void Utf8JsonReader_get_IsInArray_m5633237E6C12D3D53E4F623953D307DC58C3E4EE_AdjustorThunk (void);
extern void Utf8JsonReader_get_TokenType_mCE6BF109ADE03F304F8C16D68AF519C5CFBA631A_AdjustorThunk (void);
extern void Utf8JsonReader_get_HasValueSequence_m3EA84B4D984814A502FD58E70EC154B6DE181A5F_AdjustorThunk (void);
extern void Utf8JsonReader_set_HasValueSequence_m16BA0D508AFFA53D77D2924821C4BFB400EACBA4_AdjustorThunk (void);
extern void Utf8JsonReader_get_IsFinalBlock_m4FF1E669D83E57A86A0B60261A8027A3D2A9D8E5_AdjustorThunk (void);
extern void Utf8JsonReader_get_ValueSequence_m881899F712004242C5F89AE5899A105AEBBC295F_AdjustorThunk (void);
extern void Utf8JsonReader_set_ValueSequence_mCB1696984F939D6F8012728EA22DEC31D54F2B02_AdjustorThunk (void);
extern void Utf8JsonReader_get_CurrentState_mA51B5336D4893034ED7344FED22DB0FFA6E7C796_AdjustorThunk (void);
extern void Utf8JsonReader__ctor_mF8257CB8E2F367ADFA10F6A9B1B1E22A92039A75_AdjustorThunk (void);
extern void Utf8JsonReader_Read_mD210940FACF194A87F83C11308A87397B77DCB07_AdjustorThunk (void);
extern void Utf8JsonReader_Skip_m4DA7165BB21473A73328812E2541A62476BFBA5D_AdjustorThunk (void);
extern void Utf8JsonReader_SkipHelper_mF4192AF21A1478DCD7D1313EC666F98462376E6D_AdjustorThunk (void);
extern void Utf8JsonReader_TrySkip_mAA1EF79F9187B277465240CE3699015D57F85196_AdjustorThunk (void);
extern void Utf8JsonReader_TrySkipHelper_m2DDAA3DB1C0BF9547970FCED4E3F7DE403489017_AdjustorThunk (void);
extern void Utf8JsonReader_StartObject_mC83FBAF3E163D146444B2868D4AFCC959FA19F7C_AdjustorThunk (void);
extern void Utf8JsonReader_EndObject_m5F1441EE29712895D6102136C79EF57D88B84A2B_AdjustorThunk (void);
extern void Utf8JsonReader_StartArray_m6DA2C6FA69288410291C62C4A3976ABA06E9EC06_AdjustorThunk (void);
extern void Utf8JsonReader_EndArray_m7B69D7C38934493393321F3A27365A4B67FBB4C3_AdjustorThunk (void);
extern void Utf8JsonReader_UpdateBitStackOnEndToken_m384055041D18215523F5DCB0DC4F9BF8B564DA11_AdjustorThunk (void);
extern void Utf8JsonReader_ReadSingleSegment_m947CD352F825A73A97962979917C1DE44CC2A548_AdjustorThunk (void);
extern void Utf8JsonReader_HasMoreData_mF3B6BF4E12A6153B8D0696EB6E4DEFC68FC40B23_AdjustorThunk (void);
extern void Utf8JsonReader_HasMoreData_m50F0F11A42D737D72C7BFA79D53F0133C69D1BC9_AdjustorThunk (void);
extern void Utf8JsonReader_ReadFirstToken_mD90BB812B023F17A3EBBD30A3983D9791639372E_AdjustorThunk (void);
extern void Utf8JsonReader_SkipWhiteSpace_m04627FB93ABE8F3D19F2EBB6EEBAE3E1F40B5E68_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeValue_m846BBF871AFF435D23E18421514C2C3BF8CE156F_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeLiteral_m89BFF61B6087D786F73E77F1D812F611126CAB10_AdjustorThunk (void);
extern void Utf8JsonReader_CheckLiteral_mC50D33BAC348FD003A97DA41F7BCCE3BB0DB9988_AdjustorThunk (void);
extern void Utf8JsonReader_ThrowInvalidLiteral_m3182F7C44BEF0C05A91ADFA8087F952FDFF2064A_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNumber_m40F72939079F09EA3A3F8C2FCE2E05FE48C049AF_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumePropertyName_m049CE9287BA5B4C7AF8F8C7D68EBA79C82A7451A_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeString_mF022344616A784A7765F0569118543E35C5F9498_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeStringAndValidate_mAFEBEA2CDF74CC836345B04881A2903E05C1985B_AdjustorThunk (void);
extern void Utf8JsonReader_ValidateHexDigits_m0309E92E7DC46970B49FCBAD399D28E39F10069A_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetNumber_mDDD3CE6A95A382184215D8DB6A31D767AFECD6CF_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNegativeSign_m4C8B15B4D156BE3117809A55CD2C19B8725DABEC_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeZero_mDAB03B486171BA7CBCCA27B47FA6BF43186E299C_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeIntegerDigits_m220F559BD087754C8B69A37459241981A0C82783_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeDecimalDigits_mFD41DF26D628251C7BBD82174BF64846A0C83B2E_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeSign_m0620EDBAA824B5FC333F926075A607FC985C476D_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenOrRollback_m417EB8BF1E5C468E57452D00D30806FA775542E4_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextToken_m7D4010DE7EC28BF0391950239A30A423715CDF51_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenFromLastNonCommentToken_mBA25C73B75A78924E0E71612292917FB405C8B5B_AdjustorThunk (void);
extern void Utf8JsonReader_SkipAllComments_m2D586CC874AC235E5DAA1A76BCCECCEDF8C10F34_AdjustorThunk (void);
extern void Utf8JsonReader_SkipAllComments_m35A788BF6302E95161122D7993E69BC373C64F9F_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkipped_m7E03615D1717381CA3DAC23B30AE4E009B96F2BC_AdjustorThunk (void);
extern void Utf8JsonReader_SkipComment_mB8CC711E5F3D6D24E5EE25198DBDB7F0B3B8B52B_AdjustorThunk (void);
extern void Utf8JsonReader_SkipSingleLineComment_mEFE4A7A109C1F3E01064F405520C52B76E793DC3_AdjustorThunk (void);
extern void Utf8JsonReader_FindLineSeparator_m9C0187692AE1FC17F7B40A7DDB0ACB7FBFEB340A_AdjustorThunk (void);
extern void Utf8JsonReader_ThrowOnDangerousLineSeparator_mCA676223806C01CA53EC299BFC2C5BC48060EBD2_AdjustorThunk (void);
extern void Utf8JsonReader_SkipMultiLineComment_m8C37CD84ABC6DA62709D69A8D23C779043D025F5_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeComment_mF43CB1C22ECE1F624E0B562D93A099C01216B78E_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeSingleLineComment_m55604E2C6DEA3EA494E5663AC035A2131010D844_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeMultiLineComment_m27C5FF33B76C1C46A721951202BE2983F4F0961B_AdjustorThunk (void);
extern void Utf8JsonReader_GetUnescapedSpan_mFAEB005896BE9873061EC7843FD2CDC107B3DE3F_AdjustorThunk (void);
extern void Utf8JsonReader_ReadMultiSegment_m34ED95DFD5361E1AF24A36E81C9059A00853C644_AdjustorThunk (void);
extern void Utf8JsonReader_ValidateStateAtEndOfData_mE1A59B817BD48A6F2809EEBC7F464A0E28196FF7_AdjustorThunk (void);
extern void Utf8JsonReader_HasMoreDataMultiSegment_m31E6091999973DF6F5E877267CD93D1CCDE10AA9_AdjustorThunk (void);
extern void Utf8JsonReader_HasMoreDataMultiSegment_mF1B6563CC059AB8F085A0D8A33A11756EDB0B22C_AdjustorThunk (void);
extern void Utf8JsonReader_GetNextSpan_m9C9704D9B74FE08A99E2D13820494D10BD775BF3_AdjustorThunk (void);
extern void Utf8JsonReader_ReadFirstTokenMultiSegment_mE99805A003E45117C29863C73E136DF8622096CB_AdjustorThunk (void);
extern void Utf8JsonReader_SkipWhiteSpaceMultiSegment_m738D7B895E031A7F201D91EC074AAFEE27429A1E_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeValueMultiSegment_mCE9C63707898C340F9731E600FEE7A2F138D0DBC_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeLiteralMultiSegment_m8CE5FBA9EBF636905ABA1189DEA46B55877C255C_AdjustorThunk (void);
extern void Utf8JsonReader_CheckLiteralMultiSegment_m29DBBFB73EDCEDE193F51D9FDDE5CDE7327DF107_AdjustorThunk (void);
extern void Utf8JsonReader_FindMismatch_m3CFC046BA471A51916E4A98AF3C2B055513722FC_AdjustorThunk (void);
extern void Utf8JsonReader_GetInvalidLiteralMultiSegment_m1D4FD07D765B89E82B186D3132EEDA513C420D5D_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNumberMultiSegment_mE9CA9359DADDC9A32A0B332B5CE97D5917A99C81_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumePropertyNameMultiSegment_m3A62F15386C832D140978B9FB7C4533F22F19BDC_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeStringMultiSegment_m9AC39085975494E255DE8A0E3F6F319895CFDBA8_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeStringNextSegment_m85FB7C7822B8165B3A01A6A5E7943EF09212AE1A_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeStringAndValidateMultiSegment_mB975AD804E2DE7E80CCE8106DE68C961A6443BE3_AdjustorThunk (void);
extern void Utf8JsonReader_RollBackState_m14899B240A51A2C5298DE5D9CC5C20B68337A8F5_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetNumberMultiSegment_mB941922D23D7625DA33A2411425322B95CB49758_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNegativeSignMultiSegment_mDBEB1BD82E16D0A858BB9176369250A7BC16A7B0_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeZeroMultiSegment_m2F501FEFAD08DB5672B8820A8C3739FACE5167B8_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeIntegerDigitsMultiSegment_m5B05767D5DFC620AAAEDF25D188A1B2F7B1F3F61_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_m8B3F9AAC81565DC842D722B03F6EBF9E038EC2C6_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeSignMultiSegment_m0BDC08DBB245EB51E464A0456FD1F9A08022A78A_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenOrRollbackMultiSegment_m1558F3E362E36E672D9370F677E80CF59027A03B_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenMultiSegment_m768B8C95D7DBF55ED9EEE1A3A7E6C397A3FF2162_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenFromLastNonCommentTokenMultiSegment_mCBD2A8ECC218C80D1637EE6F4AFAD5DE7EC84E1E_AdjustorThunk (void);
extern void Utf8JsonReader_SkipAllCommentsMultiSegment_m9F26B47E140957BFC485A5B1194A67FFA80AFF38_AdjustorThunk (void);
extern void Utf8JsonReader_SkipAllCommentsMultiSegment_m6BDCBE0CE1EA8656FE6ECF632FC8BFD7925039D5_AdjustorThunk (void);
extern void Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment_m927DB030F5F92F4CB68854405095A7AC372E1CF9_AdjustorThunk (void);
extern void Utf8JsonReader_SkipOrConsumeCommentMultiSegmentWithRollback_m4A93250DEBD59CE9497CE8D857E1D1AFFAECE616_AdjustorThunk (void);
extern void Utf8JsonReader_SkipCommentMultiSegment_m41581C771A4EAD318B08C195AE89705757923A12_AdjustorThunk (void);
extern void Utf8JsonReader_SkipSingleLineCommentMultiSegment_m64E237F76BE411B9D1C000094FB64D3E3D921EDC_AdjustorThunk (void);
extern void Utf8JsonReader_FindLineSeparatorMultiSegment_m7409FC03149D983045F95B254E219425B2AF125F_AdjustorThunk (void);
extern void Utf8JsonReader_ThrowOnDangerousLineSeparatorMultiSegment_m71F825306CAE9334C56ACD50EA17AC32E5F53231_AdjustorThunk (void);
extern void Utf8JsonReader_SkipMultiLineCommentMultiSegment_m927C47BED86E20BDB7136DE116E105CE8E52187E_AdjustorThunk (void);
extern void Utf8JsonReader_CaptureState_mE854A6A21277EB2333CF3387664CDA0072756A30_AdjustorThunk (void);
extern void Utf8JsonReader_GetString_m3AA74B2BD8AE8211C2F45748764CA0F8D0772F11_AdjustorThunk (void);
extern void Utf8JsonReader_GetBoolean_m4A0CCA2B3FF77D11CDEF65077936B5CA951962C6_AdjustorThunk (void);
extern void Utf8JsonReader_GetBytesFromBase64_mBD400780F573FB4057B04E9B6CE298AC931ACD5A_AdjustorThunk (void);
extern void Utf8JsonReader_GetByte_mEC280899A1AD56A59AABAF6F35E061C5A257A232_AdjustorThunk (void);
extern void Utf8JsonReader_GetByteWithQuotes_mDAF5E947FAA492D8510E6BFA0AF8CC8BECA26C05_AdjustorThunk (void);
extern void Utf8JsonReader_GetSByte_m86C5D95B6393261601AB9C07BEABB0675C00846A_AdjustorThunk (void);
extern void Utf8JsonReader_GetSByteWithQuotes_mCB8C7C905A515C7695ABC62CAEA61496AA408688_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt16_m486F6B00F180588F76CF037EA708079C921D5C2B_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt16WithQuotes_mFBCA9FB631328EFD1F4A727A40E464784A59C4F3_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt32_m9994A31A6BBC68EC25BFEA942924684639E0A350_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt32WithQuotes_m26F4EF8D42082CD49AE7E2FE9683CCA264E9F746_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt64_m8F101B63B25099413DEC5D297BB5FFE578DE165A_AdjustorThunk (void);
extern void Utf8JsonReader_GetInt64WithQuotes_m3096847DAFC9F239BE4FAC096D247502AD492147_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt16_mDADEC8D0157630B34BA4BED95BEC8FC0FB103F36_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt16WithQuotes_m3AFF72BAC86A2C8D95E30D2CE927C58C01AD4CF5_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt32_m727AE5A5ECBB3D29C4E592193706C81080C3AA75_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt32WithQuotes_m4A9C9DF5E09E76CABB9355907A996C884EBFE5A7_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt64_mD82438E5E150BE6DCB19E9AA8527032FE1073A7B_AdjustorThunk (void);
extern void Utf8JsonReader_GetUInt64WithQuotes_mDF51094C764240D18BE44B978CFFF09900AB4AE7_AdjustorThunk (void);
extern void Utf8JsonReader_GetSingle_mE9EEE464C1E81A3F019BBA55FC21105525AEEE42_AdjustorThunk (void);
extern void Utf8JsonReader_GetSingleWithQuotes_m8BDBB093E18B06F604D53E31CABDE152B79FCEFD_AdjustorThunk (void);
extern void Utf8JsonReader_GetSingleFloatingPointConstant_m557E5950D17D08737B79AE2B41C281FC62BA1E4A_AdjustorThunk (void);
extern void Utf8JsonReader_GetDouble_m8B6B801EAD7C30EFEFCEE8CEF6D1522CA4778EE1_AdjustorThunk (void);
extern void Utf8JsonReader_GetDoubleWithQuotes_m3C6612DDA71C26099B2C314761AAF3F7CD733658_AdjustorThunk (void);
extern void Utf8JsonReader_GetDoubleFloatingPointConstant_m7649E11BD1B5F7CFEABE646638133CBBF0F69976_AdjustorThunk (void);
extern void Utf8JsonReader_GetDecimal_m0F141F3F50CD249A6EA21E01188DE311EAE86F41_AdjustorThunk (void);
extern void Utf8JsonReader_GetDecimalWithQuotes_m8EBE26F40D4FACB3B92BD0C7F75C4AE8E5E509B3_AdjustorThunk (void);
extern void Utf8JsonReader_GetDateTime_mE98D9A548388D8526C4F0CC35899D2F14D43B22F_AdjustorThunk (void);
extern void Utf8JsonReader_GetDateTimeNoValidation_mB25F994C72A93EA8103DD71FEE99A87D6634B104_AdjustorThunk (void);
extern void Utf8JsonReader_GetDateTimeOffset_m34029D75821CEB4740BF1D8B359747D6475C88B9_AdjustorThunk (void);
extern void Utf8JsonReader_GetDateTimeOffsetNoValidation_m32A0E97EF22CB407C923E5E795301AE87FE45309_AdjustorThunk (void);
extern void Utf8JsonReader_GetGuid_mF53227165B93F42BCC17F4212C98AE792D47699A_AdjustorThunk (void);
extern void Utf8JsonReader_GetGuidNoValidation_mF56895F5C26C43EAC16F16A589C002B6699C7771_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetBytesFromBase64_m7DEA918E54E58B402F89A126FD98D29D1D4F64A4_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetByte_m3A5B27FD2CA5F304075022FF7E1BC05D2441E4BB_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetByteCore_m37008192DDB31799D7864470CC49F8C8A0CBD778_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetSByte_m59FC81E3CF3CC2BA6161271E603F4C8F96CD6329_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetSByteCore_m136CD23B2D669BC9E205AEED6B9CA0A2D79DAC43_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt16_mDE28715B75D564D7EB68209FCC19C1A6357DD372_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt16Core_mCBDC2DF50B262CB386B03CD349C1D60176C53AFA_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt32_m98C94CE92457BDD3B53CAED4A8701BF93EAC2617_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt32Core_m088C7F21848C7409B390A4D9BCF66E1BFDFAAFD8_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt64_m2A2CEAA1791795E6DB1420BB381D71B6C593272F_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetInt64Core_m3993E1A0CB78D318C835E868E467414537576332_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt16_m22C896C87811291DFD23ED0457B38E7825D6D296_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt16Core_m9A0768CC1CF4BAC47EA82BAAD39CDF0E24C6664C_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt32_mA2AA2EEC55C6FEDBD9844AD8473698D2214636BA_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt32Core_m40A9CCB041D399F181EDE2B459EA7D77EBE6FB63_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt64_m9728DDCC0E176DC03C0FEEB2C3FFC4566B2D72B9_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetUInt64Core_m73F5FB752D9F173DDA97E4679DBF61BFB82C3C71_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetSingle_m94587873A494E75F4825C1CA091504E3BB8FEF2D_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDouble_m13353F863EF92179E6798D51791CDBD278180CEB_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDecimal_m3B8E8434E5962329BBF1F17FD4FC6F4CF186D8F2_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDateTime_m07EA74E20E656F735523B9F5C04C0DFA45D8AFC5_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDateTimeCore_mD25A6F6750D17FD2E4D61338DECD3F546E1B1931_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDateTimeOffset_mDBA3CEC29896D95E62041F3AB3C851BBF1B7D8AD_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetDateTimeOffsetCore_mD9E6F3076962699F2C5876A47B83E133E9B33533_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetGuid_mA3FD580BF871065767D82031AD0A499DB0EB8BD8_AdjustorThunk (void);
extern void Utf8JsonReader_TryGetGuidCore_m44AE06756804CEB5DBA738D40527CA2254094530_AdjustorThunk (void);
extern void PartialStateForRollback__ctor_m0F515AA3F80A7125D3079909A6578BD37B6EB3BB_AdjustorThunk (void);
extern void PartialStateForRollback_GetStartPosition_m57397FA90C6F268F5711E7E5131D5237E52F41B0_AdjustorThunk (void);
extern void ParameterRef__ctor_mD6F9E352509A2D16FBBFB12B05A40A70FB04672A_AdjustorThunk (void);
extern void PropertyRef__ctor_mBB574D64A64A57C51A58468AC6514DE568D533E5_AdjustorThunk (void);
extern void ReadStack_get_IsContinuation_mCEC1851A10A829FFFE42BBAD53711A0A990526F8_AdjustorThunk (void);
extern void ReadStack_AddCurrent_m3B0106DB1337C3B7F26B2F874E40B80FA7FCBD18_AdjustorThunk (void);
extern void ReadStack_Initialize_m4CCFB4EF4B2C80815463CE81FE499FFF8528BB3B_AdjustorThunk (void);
extern void ReadStack_Push_m1F2599D52633E428D94A9D90E77A833FE64BC0BC_AdjustorThunk (void);
extern void ReadStack_Pop_mD20CAC0E80A32CAE52481E175241AFDE02D4C9DD_AdjustorThunk (void);
extern void ReadStack_JsonPath_m4DF8F1F7BB216E3E6F9EFE4066E9B94D270E49C5_AdjustorThunk (void);
extern void ReadStack_SetConstructorArgumentState_mCE9355370E6E636D813179995CFCB4B869A20033_AdjustorThunk (void);
extern void ReadStackFrame_EndConstructorParameter_m8D04A7A3CE2D19DA2C0CCFA8937859DEF1151A14_AdjustorThunk (void);
extern void ReadStackFrame_EndProperty_mBF44974D081F8BDCC44666395A7CC82DB3E557E4_AdjustorThunk (void);
extern void ReadStackFrame_EndElement_mA9D0401AD7A3F30475D97C6E49C671AA98558979_AdjustorThunk (void);
extern void ReadStackFrame_IsProcessingDictionary_mBFC8EA6ACB5BA357E8887719A4CD2F61F14DD2AE_AdjustorThunk (void);
extern void ReadStackFrame_IsProcessingEnumerable_m3A7B122E538645493032819C697D4FD0D7BB40E3_AdjustorThunk (void);
extern void ReadStackFrame_Reset_m9998E100B9CF5B2FC93FDE5C50FDB77227903C9A_AdjustorThunk (void);
extern void WriteStack_get_IsContinuation_mBDA5ACA7E19BAF628ED4BDCE702B8B46E6AA9BDC_AdjustorThunk (void);
extern void WriteStack_AddCurrent_mCA9F10EA1D6E24E25E27295386208F3E71A3654E_AdjustorThunk (void);
extern void WriteStack_Initialize_mD5B4E7AF1DE653A9D2E614A452799636C009F9CC_AdjustorThunk (void);
extern void WriteStack_Push_m7609AF43966C67D97FE3A16525E55D53FA2569AA_AdjustorThunk (void);
extern void WriteStack_Pop_mC6F6750427C0EEEEAC7B9ED1BF9ECC07677F7000_AdjustorThunk (void);
extern void WriteStack_PropertyPath_mAE92BA9B54387C839164F69A7944CF0401165039_AdjustorThunk (void);
extern void WriteStackFrame_EndDictionaryElement_m0E57022D7831F92A528248C5A25CE8B5E0A28F0C_AdjustorThunk (void);
extern void WriteStackFrame_EndProperty_m713E4D5585A2354CD0CE2DF56B7DA3B9E7FD31C8_AdjustorThunk (void);
extern void WriteStackFrame_GetPolymorphicJsonPropertyInfo_m8C493C533AC6BB1D6E45E7F27F0664F157CDD602_AdjustorThunk (void);
extern void WriteStackFrame_InitializeReEntry_mDA0CD4D8B09439188519F97297A2A2D85D3BD619_AdjustorThunk (void);
extern void WriteStackFrame_Reset_mC74400A4F12DD1688E4D4C1EC4C6DC305B96E56E_AdjustorThunk (void);
extern void JsonWriterOptions_get_Encoder_mAE5C32DE2F9E54D87346B602EA2DB2C0676D7406_AdjustorThunk (void);
extern void JsonWriterOptions_set_Encoder_m5E357AEFC0AB885F7858AE1D70B7060273AED306_AdjustorThunk (void);
extern void JsonWriterOptions_get_Indented_mDF7E8A47E47FF6E43BE1E577F434CA7525E4AC0A_AdjustorThunk (void);
extern void JsonWriterOptions_set_Indented_m5418A714E77EA6D9734E6A497FFD407D799AF8DB_AdjustorThunk (void);
extern void JsonWriterOptions_get_SkipValidation_m9113D9C54C02B4EBA452A57485A91F8A30E74DB0_AdjustorThunk (void);
extern void JsonWriterOptions_set_SkipValidation_m805F1EC553421C91697F04ED05567BB51981F43A_AdjustorThunk (void);
extern void JsonWriterOptions_get_IndentedOrNotSkipValidation_mFC205FBEF9620AD791C2EAC0A7F776281E2D9900_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[292] = 
{
	{ 0x0600010B, BitStack_get_CurrentDepth_m29C47DB4A4B69D9C76A93EBA1637E97A243FC869_AdjustorThunk },
	{ 0x0600010C, BitStack_PushTrue_mBEF1D3C195EA821E02716255C61AFB88258D5BAB_AdjustorThunk },
	{ 0x0600010D, BitStack_PushFalse_m83CDC7E063685BF4FAD27FDCD7F51CF8D49EBAD3_AdjustorThunk },
	{ 0x0600010E, BitStack_PushToArray_mE3486049BF012A03170FBC4C4BF1CE139F81F6C0_AdjustorThunk },
	{ 0x0600010F, BitStack_Pop_mEBB94B5C8DD6FACC49DCB2A2A7D365E5A75A4FD2_AdjustorThunk },
	{ 0x06000110, BitStack_PopFromArray_mC5F5242B631329764A700C94FC08270FE1803691_AdjustorThunk },
	{ 0x06000111, BitStack_DoubleArray_m46DB748394540255EF86FBD4970988C81DF3395F_AdjustorThunk },
	{ 0x06000112, BitStack_SetFirstBit_m9263D9E5C36F9375CDA674AC231BCF1FDF858472_AdjustorThunk },
	{ 0x06000113, BitStack_ResetFirstBit_m71CD0C8BD6150A422ABAABF746E6F0EC53FD8359_AdjustorThunk },
	{ 0x06000135, DbRow_get_Location_m26C010A9C88629FBF5B9BE4AB8DA90E22422CFAC_AdjustorThunk },
	{ 0x06000136, DbRow_get_SizeOrLength_m84D8BF651739FC1923CCEA2500DF7D9929DD8ACF_AdjustorThunk },
	{ 0x06000137, DbRow_get_IsUnknownSize_m72AF174FA7DFE73898270932D83924CB0DA5C52A_AdjustorThunk },
	{ 0x06000138, DbRow_get_HasComplexChildren_m8900AC0494A74A8EE1BCA197C6223110CF35B997_AdjustorThunk },
	{ 0x06000139, DbRow_get_NumberOfRows_m1C82FF779E2D2653D42E81A38364AE4CC2B7DCAE_AdjustorThunk },
	{ 0x0600013A, DbRow_get_TokenType_mCCE39C67D2D66F0053D567BD45AD095D34977063_AdjustorThunk },
	{ 0x0600013B, DbRow__ctor_m5A251F6E7871A6B66B5CC2E73937621035F95178_AdjustorThunk },
	{ 0x0600013C, DbRow_get_IsSimpleValue_m2038B00EE08F425ABCD2BC3C24A151E7EDAE3927_AdjustorThunk },
	{ 0x0600013D, MetadataDb_get_Length_m22933560D56DEB73CDA8422130DFB5A6089E523C_AdjustorThunk },
	{ 0x0600013E, MetadataDb_set_Length_m1E5804A10F184287716024455518919240C3E635_AdjustorThunk },
	{ 0x0600013F, MetadataDb__ctor_m39EBC88D82771277ACD7B74A804CC43263D5B706_AdjustorThunk },
	{ 0x06000140, MetadataDb__ctor_m032FBF24011241945608F14DD9D7A12DB7CB8535_AdjustorThunk },
	{ 0x06000141, MetadataDb_Dispose_m77CE10F7F04101B702C044AC728B6B6E8E912569_AdjustorThunk },
	{ 0x06000142, MetadataDb_TrimExcess_m990B41FACFAE4EC6756F75A83B7130A8FD6EF21C_AdjustorThunk },
	{ 0x06000143, MetadataDb_Append_m2537E212C18338E2810CC9B80A5DD82B897DAFE7_AdjustorThunk },
	{ 0x06000144, MetadataDb_Enlarge_mDE37991365E52CA27B9FAE0F2A8964AA102BDD5A_AdjustorThunk },
	{ 0x06000145, MetadataDb_SetLength_mFC66B57EF58895AFFE2DAD33297935F63C5B8F79_AdjustorThunk },
	{ 0x06000146, MetadataDb_SetNumberOfRows_m5D37816F2AD652AB316EFBE4B0A2643457B72C82_AdjustorThunk },
	{ 0x06000147, MetadataDb_SetHasComplexChildren_mB746E8ED47FF55757A24BD397A0E4D91E18F8EAE_AdjustorThunk },
	{ 0x06000148, MetadataDb_FindIndexOfFirstUnsetSizeOrLength_m9BB4939039E8059B31868E36E84F888B26BAE35E_AdjustorThunk },
	{ 0x06000149, MetadataDb_FindOpenElement_m8CA68EDBB9605702DCA7BB048DC51689A38ADCB7_AdjustorThunk },
	{ 0x0600014A, MetadataDb_Get_m2EE4C63C4D6B477C49D85DA90D6CFFC009A0B662_AdjustorThunk },
	{ 0x0600014B, MetadataDb_GetJsonTokenType_mF9DF07B766B9E488CD4A203A1F0BE3D09551A41E_AdjustorThunk },
	{ 0x0600014C, MetadataDb_CopySegment_mF7BE8719D6FE1E244B55E9E3251855DD065A4B1E_AdjustorThunk },
	{ 0x0600014D, StackRow__ctor_m2A8EFF70E90A96B442084512D9788BBA144AA25F_AdjustorThunk },
	{ 0x0600014E, StackRowStack__ctor_mA8C386AAC2C88F726223EF8F9CED4C3E79948976_AdjustorThunk },
	{ 0x0600014F, StackRowStack_Dispose_m442EF445F037EC392B2F1494ACEFF8D4E6B2A052_AdjustorThunk },
	{ 0x06000150, StackRowStack_Push_m4F9FD4AE22507CABB234B17F9D72F52F0BF9215A_AdjustorThunk },
	{ 0x06000151, StackRowStack_Pop_m59CEFB7385936EBFB490BCFEB5001B63F6E79B31_AdjustorThunk },
	{ 0x06000152, StackRowStack_Enlarge_mB7433A4A8E2C398B72C14512663278632CA70D89_AdjustorThunk },
	{ 0x06000153, JsonDocumentOptions_get_CommentHandling_m9486C1460A2DC728E08C0E0FAED390C30503FAC7_AdjustorThunk },
	{ 0x06000154, JsonDocumentOptions_get_MaxDepth_mB728B90176B962CD67750F71D6AF5FCB816CBF15_AdjustorThunk },
	{ 0x06000155, JsonDocumentOptions_get_AllowTrailingCommas_m03274EEEEC6A5BC79CCCE3E89FCE91179F7DE313_AdjustorThunk },
	{ 0x06000156, JsonDocumentOptions_GetReaderOptions_mBC8F60D77D1AE7A350DFF94D8869A6177015540A_AdjustorThunk },
	{ 0x06000157, JsonElement__ctor_m8E42B93E64E1C54BD78FF8163CD5C482E0EAA44B_AdjustorThunk },
	{ 0x06000158, JsonElement_get_TokenType_mD6DC1A601E15744E583F39CFEAED4C161168C456_AdjustorThunk },
	{ 0x06000159, JsonElement_get_ValueKind_m14EFE30FAA112F5199CBD9C42DC9C3AEF6ADA1B5_AdjustorThunk },
	{ 0x0600015A, JsonElement_GetProperty_mB0BB5EA6AC89BB64A9F3BD39EE474AD2D26F72D7_AdjustorThunk },
	{ 0x0600015B, JsonElement_TryGetProperty_m11C90F70F1EE0DA94CD3E752FD0259C599687D46_AdjustorThunk },
	{ 0x0600015C, JsonElement_TryGetProperty_m7322A7C18BB189A5E607ED747340376EB5FC173E_AdjustorThunk },
	{ 0x0600015D, JsonElement_GetString_m7AE007D2F1B4016AA1B53BF79B1A1DD1EA42EB94_AdjustorThunk },
	{ 0x0600015E, JsonElement_TryGetInt32_m5FCAA7B399C4469AFFE24400FDDBE78F8C60041E_AdjustorThunk },
	{ 0x0600015F, JsonElement_GetInt32_m21DEB1B177269FFB57C09E9B094DF8C719926A73_AdjustorThunk },
	{ 0x06000160, JsonElement_GetRawText_m054A9C6C86FA951676F21FEB0ABAAEE0835A11EE_AdjustorThunk },
	{ 0x06000161, JsonElement_GetPropertyRawText_m575B8D8F5C386BE964EB43964706F19BC0E72233_AdjustorThunk },
	{ 0x06000162, JsonElement_TextEqualsHelper_mE1DE405E05A250F42B364021CDE5BDD629E7C6F5_AdjustorThunk },
	{ 0x06000163, JsonElement_WriteTo_m8802C6ABB5C038C5E111274B0FED5702BFB72B10_AdjustorThunk },
	{ 0x06000164, JsonElement_EnumerateArray_m9B35AF71578078459E6DAF2D7416D07F327E6E38_AdjustorThunk },
	{ 0x06000165, JsonElement_EnumerateObject_mD4E563B682ABDCB35E2D27A573BB903CE3EA7D76_AdjustorThunk },
	{ 0x06000166, JsonElement_ToString_m9E0942575D303AD18247677C192C7E065AE83634_AdjustorThunk },
	{ 0x06000167, JsonElement_Clone_mCD2B3D79FE552E8FA4A29C9244675D1926B7EC50_AdjustorThunk },
	{ 0x06000168, JsonElement_CheckValidInstance_m175423F0C65D579DB09D5B1C9ABE2E643064DC8E_AdjustorThunk },
	{ 0x06000169, ArrayEnumerator__ctor_m1D7B7BD7496B0CD223EE9399B5A57725AEEF7381_AdjustorThunk },
	{ 0x0600016A, ArrayEnumerator_get_Current_m5F5C30DD80B5F2331BF392DA21D4EB06E050F827_AdjustorThunk },
	{ 0x0600016B, ArrayEnumerator_GetEnumerator_m938DD30A37AEE868DC5935D0CDE254FA6CAFCF97_AdjustorThunk },
	{ 0x0600016C, ArrayEnumerator_System_Collections_IEnumerable_GetEnumerator_m2E636672376124E9810A741FED6ACE7BFD20939E_AdjustorThunk },
	{ 0x0600016D, ArrayEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonElementU3E_GetEnumerator_m9ABEE9A87DD3D50D065C9738B683D691A143539B_AdjustorThunk },
	{ 0x0600016E, ArrayEnumerator_Dispose_mD4E9E4516CF8CC2CEA173A59B8407A94271C34D2_AdjustorThunk },
	{ 0x0600016F, ArrayEnumerator_Reset_m3DB230A1965DE1E4F9DBEC7BF58635D2AC1FA823_AdjustorThunk },
	{ 0x06000170, ArrayEnumerator_System_Collections_IEnumerator_get_Current_mE0067C8DCE7157F9C9695C3FF3FDD5CEED826422_AdjustorThunk },
	{ 0x06000171, ArrayEnumerator_MoveNext_mF1B0AFD404AA0995A6BE8106CE602684BBE47433_AdjustorThunk },
	{ 0x06000172, ObjectEnumerator__ctor_m703D9D5B13C21AC9E29F92FAB758D52A102C8F01_AdjustorThunk },
	{ 0x06000173, ObjectEnumerator_get_Current_mC1865163B29F8CBEAC81A2E5C4A26AF3B997C7BC_AdjustorThunk },
	{ 0x06000174, ObjectEnumerator_GetEnumerator_mEE9CACFE7559A6D381D802909FE21EDA46FC103C_AdjustorThunk },
	{ 0x06000175, ObjectEnumerator_System_Collections_IEnumerable_GetEnumerator_mA2F0FD6FAB9BEF8BA08389BE66483F44BAF90BC5_AdjustorThunk },
	{ 0x06000176, ObjectEnumerator_System_Collections_Generic_IEnumerableU3CSystem_Text_Json_JsonPropertyU3E_GetEnumerator_m2F6759C0BBDA3C73CB81373048E97DE7E10D0AB0_AdjustorThunk },
	{ 0x06000177, ObjectEnumerator_Dispose_mE6013E4D463D4DFAE6D9A4458393358F84F4115A_AdjustorThunk },
	{ 0x06000178, ObjectEnumerator_Reset_m29B204531B2E013AE91F1B6CD394EC9E32447E5C_AdjustorThunk },
	{ 0x06000179, ObjectEnumerator_System_Collections_IEnumerator_get_Current_mC740D819F66BC7DB0738A495920A650C31444B56_AdjustorThunk },
	{ 0x0600017A, ObjectEnumerator_MoveNext_m7D620190FB99A0075BE003767FC70105507DA629_AdjustorThunk },
	{ 0x0600017B, JsonProperty_get_Value_m6ED717E9179F9D4A00DF42D36A191A8E2BB8C402_AdjustorThunk },
	{ 0x0600017C, JsonProperty__ctor_m3447EAB80FB5BA6813D3F9F18EBAD08B7FCFB312_AdjustorThunk },
	{ 0x0600017D, JsonProperty_EscapedNameEquals_mF067EB713ECF2C089B42447941C7A34C50C02352_AdjustorThunk },
	{ 0x0600017E, JsonProperty_ToString_m9F70631770655A6F3B6AEAA2C3717D17A1D0F573_AdjustorThunk },
	{ 0x06000187, JsonEncodedText_get_EncodedUtf8Bytes_m7D24410A44ADF371EDDBA9059403D34F98B463A1_AdjustorThunk },
	{ 0x06000188, JsonEncodedText__ctor_m7F8DBA12F3C948E827627FF1300E95AA48E42CEA_AdjustorThunk },
	{ 0x0600018D, JsonEncodedText_Equals_m26E3AB94F984E7BAE83FB67FD47A7C5A0BB116E4_AdjustorThunk },
	{ 0x0600018E, JsonEncodedText_Equals_mE176157268A0949E8C234E0175B00F9FF800B6C9_AdjustorThunk },
	{ 0x0600018F, JsonEncodedText_ToString_mA0D2C5B818AE0E302B31B5FB722ADBFA30F5BA8B_AdjustorThunk },
	{ 0x06000190, JsonEncodedText_GetHashCode_mCFF493ABAB2853A884DE0BCDFBB1BC35BE1C04A6_AdjustorThunk },
	{ 0x060001BC, DateTimeParseData_get_OffsetNegative_m6E163581952036148996FE989BA72F37B7178A3F_AdjustorThunk },
	{ 0x060001DA, JsonReaderOptions_get_CommentHandling_m785941C88C0D78EE452E47079133CA53EAA86918_AdjustorThunk },
	{ 0x060001DB, JsonReaderOptions_set_CommentHandling_m3A14D93E8354AB403D202AC5352A0355DEF47641_AdjustorThunk },
	{ 0x060001DC, JsonReaderOptions_get_MaxDepth_m2F0DA8E50E5D545945DB32DFFFA74E941849F3F6_AdjustorThunk },
	{ 0x060001DD, JsonReaderOptions_set_MaxDepth_m2AEBFDFBFEC808E1E4902FFF561FAC990FD8420F_AdjustorThunk },
	{ 0x060001DE, JsonReaderOptions_get_AllowTrailingCommas_m8D5027DF6AD1F91F789AC6AF9A7BF550E81B256D_AdjustorThunk },
	{ 0x060001DF, JsonReaderOptions_set_AllowTrailingCommas_mB7C7B82225E31EFECAC172BC8CBF073C09A4036B_AdjustorThunk },
	{ 0x060001E0, JsonReaderState__ctor_mD1D4FD65E3D005FD3B169BE5F949DCBCB638D5B2_AdjustorThunk },
	{ 0x060001E1, JsonReaderState_get_Options_mC7B2AAB4CD55B90A1EB81F03B82BDEB9E8ED3894_AdjustorThunk },
	{ 0x060001E2, Utf8JsonReader_get_IsLastSpan_m0D510710CF414FB8B8403E4051E06289C14E5B17_AdjustorThunk },
	{ 0x060001E3, Utf8JsonReader_get_OriginalSequence_mFC7BF57B6E80D112834FAF24855EF4FC82F928B0_AdjustorThunk },
	{ 0x060001E4, Utf8JsonReader_get_OriginalSpan_mCA0CC6DA6C7595E089AE833D3E8FAD70196ED6D3_AdjustorThunk },
	{ 0x060001E5, Utf8JsonReader_get_ValueSpan_mD7C0AA41BFEC523AEF71D7AF4BFF784F84CC9AF8_AdjustorThunk },
	{ 0x060001E6, Utf8JsonReader_set_ValueSpan_m37AB280FA0870B85790F31CC32FE61A145BB73DE_AdjustorThunk },
	{ 0x060001E7, Utf8JsonReader_get_BytesConsumed_mEF9EB31417202F307C10482D117C0D1F1383746B_AdjustorThunk },
	{ 0x060001E8, Utf8JsonReader_get_TokenStartIndex_m616B80F3D48063970EF25BC002FE2B7512948D3E_AdjustorThunk },
	{ 0x060001E9, Utf8JsonReader_set_TokenStartIndex_m391AF219C657ED98FC368E82D7129EA13E4B744F_AdjustorThunk },
	{ 0x060001EA, Utf8JsonReader_get_CurrentDepth_m09EED2F622F6DF3BE234674E7532337C2728EBA9_AdjustorThunk },
	{ 0x060001EB, Utf8JsonReader_get_IsInArray_m5633237E6C12D3D53E4F623953D307DC58C3E4EE_AdjustorThunk },
	{ 0x060001EC, Utf8JsonReader_get_TokenType_mCE6BF109ADE03F304F8C16D68AF519C5CFBA631A_AdjustorThunk },
	{ 0x060001ED, Utf8JsonReader_get_HasValueSequence_m3EA84B4D984814A502FD58E70EC154B6DE181A5F_AdjustorThunk },
	{ 0x060001EE, Utf8JsonReader_set_HasValueSequence_m16BA0D508AFFA53D77D2924821C4BFB400EACBA4_AdjustorThunk },
	{ 0x060001EF, Utf8JsonReader_get_IsFinalBlock_m4FF1E669D83E57A86A0B60261A8027A3D2A9D8E5_AdjustorThunk },
	{ 0x060001F0, Utf8JsonReader_get_ValueSequence_m881899F712004242C5F89AE5899A105AEBBC295F_AdjustorThunk },
	{ 0x060001F1, Utf8JsonReader_set_ValueSequence_mCB1696984F939D6F8012728EA22DEC31D54F2B02_AdjustorThunk },
	{ 0x060001F2, Utf8JsonReader_get_CurrentState_mA51B5336D4893034ED7344FED22DB0FFA6E7C796_AdjustorThunk },
	{ 0x060001F3, Utf8JsonReader__ctor_mF8257CB8E2F367ADFA10F6A9B1B1E22A92039A75_AdjustorThunk },
	{ 0x060001F4, Utf8JsonReader_Read_mD210940FACF194A87F83C11308A87397B77DCB07_AdjustorThunk },
	{ 0x060001F5, Utf8JsonReader_Skip_m4DA7165BB21473A73328812E2541A62476BFBA5D_AdjustorThunk },
	{ 0x060001F6, Utf8JsonReader_SkipHelper_mF4192AF21A1478DCD7D1313EC666F98462376E6D_AdjustorThunk },
	{ 0x060001F7, Utf8JsonReader_TrySkip_mAA1EF79F9187B277465240CE3699015D57F85196_AdjustorThunk },
	{ 0x060001F8, Utf8JsonReader_TrySkipHelper_m2DDAA3DB1C0BF9547970FCED4E3F7DE403489017_AdjustorThunk },
	{ 0x060001F9, Utf8JsonReader_StartObject_mC83FBAF3E163D146444B2868D4AFCC959FA19F7C_AdjustorThunk },
	{ 0x060001FA, Utf8JsonReader_EndObject_m5F1441EE29712895D6102136C79EF57D88B84A2B_AdjustorThunk },
	{ 0x060001FB, Utf8JsonReader_StartArray_m6DA2C6FA69288410291C62C4A3976ABA06E9EC06_AdjustorThunk },
	{ 0x060001FC, Utf8JsonReader_EndArray_m7B69D7C38934493393321F3A27365A4B67FBB4C3_AdjustorThunk },
	{ 0x060001FD, Utf8JsonReader_UpdateBitStackOnEndToken_m384055041D18215523F5DCB0DC4F9BF8B564DA11_AdjustorThunk },
	{ 0x060001FE, Utf8JsonReader_ReadSingleSegment_m947CD352F825A73A97962979917C1DE44CC2A548_AdjustorThunk },
	{ 0x060001FF, Utf8JsonReader_HasMoreData_mF3B6BF4E12A6153B8D0696EB6E4DEFC68FC40B23_AdjustorThunk },
	{ 0x06000200, Utf8JsonReader_HasMoreData_m50F0F11A42D737D72C7BFA79D53F0133C69D1BC9_AdjustorThunk },
	{ 0x06000201, Utf8JsonReader_ReadFirstToken_mD90BB812B023F17A3EBBD30A3983D9791639372E_AdjustorThunk },
	{ 0x06000202, Utf8JsonReader_SkipWhiteSpace_m04627FB93ABE8F3D19F2EBB6EEBAE3E1F40B5E68_AdjustorThunk },
	{ 0x06000203, Utf8JsonReader_ConsumeValue_m846BBF871AFF435D23E18421514C2C3BF8CE156F_AdjustorThunk },
	{ 0x06000204, Utf8JsonReader_ConsumeLiteral_m89BFF61B6087D786F73E77F1D812F611126CAB10_AdjustorThunk },
	{ 0x06000205, Utf8JsonReader_CheckLiteral_mC50D33BAC348FD003A97DA41F7BCCE3BB0DB9988_AdjustorThunk },
	{ 0x06000206, Utf8JsonReader_ThrowInvalidLiteral_m3182F7C44BEF0C05A91ADFA8087F952FDFF2064A_AdjustorThunk },
	{ 0x06000207, Utf8JsonReader_ConsumeNumber_m40F72939079F09EA3A3F8C2FCE2E05FE48C049AF_AdjustorThunk },
	{ 0x06000208, Utf8JsonReader_ConsumePropertyName_m049CE9287BA5B4C7AF8F8C7D68EBA79C82A7451A_AdjustorThunk },
	{ 0x06000209, Utf8JsonReader_ConsumeString_mF022344616A784A7765F0569118543E35C5F9498_AdjustorThunk },
	{ 0x0600020A, Utf8JsonReader_ConsumeStringAndValidate_mAFEBEA2CDF74CC836345B04881A2903E05C1985B_AdjustorThunk },
	{ 0x0600020B, Utf8JsonReader_ValidateHexDigits_m0309E92E7DC46970B49FCBAD399D28E39F10069A_AdjustorThunk },
	{ 0x0600020C, Utf8JsonReader_TryGetNumber_mDDD3CE6A95A382184215D8DB6A31D767AFECD6CF_AdjustorThunk },
	{ 0x0600020D, Utf8JsonReader_ConsumeNegativeSign_m4C8B15B4D156BE3117809A55CD2C19B8725DABEC_AdjustorThunk },
	{ 0x0600020E, Utf8JsonReader_ConsumeZero_mDAB03B486171BA7CBCCA27B47FA6BF43186E299C_AdjustorThunk },
	{ 0x0600020F, Utf8JsonReader_ConsumeIntegerDigits_m220F559BD087754C8B69A37459241981A0C82783_AdjustorThunk },
	{ 0x06000210, Utf8JsonReader_ConsumeDecimalDigits_mFD41DF26D628251C7BBD82174BF64846A0C83B2E_AdjustorThunk },
	{ 0x06000211, Utf8JsonReader_ConsumeSign_m0620EDBAA824B5FC333F926075A607FC985C476D_AdjustorThunk },
	{ 0x06000212, Utf8JsonReader_ConsumeNextTokenOrRollback_m417EB8BF1E5C468E57452D00D30806FA775542E4_AdjustorThunk },
	{ 0x06000213, Utf8JsonReader_ConsumeNextToken_m7D4010DE7EC28BF0391950239A30A423715CDF51_AdjustorThunk },
	{ 0x06000214, Utf8JsonReader_ConsumeNextTokenFromLastNonCommentToken_mBA25C73B75A78924E0E71612292917FB405C8B5B_AdjustorThunk },
	{ 0x06000215, Utf8JsonReader_SkipAllComments_m2D586CC874AC235E5DAA1A76BCCECCEDF8C10F34_AdjustorThunk },
	{ 0x06000216, Utf8JsonReader_SkipAllComments_m35A788BF6302E95161122D7993E69BC373C64F9F_AdjustorThunk },
	{ 0x06000217, Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkipped_m7E03615D1717381CA3DAC23B30AE4E009B96F2BC_AdjustorThunk },
	{ 0x06000218, Utf8JsonReader_SkipComment_mB8CC711E5F3D6D24E5EE25198DBDB7F0B3B8B52B_AdjustorThunk },
	{ 0x06000219, Utf8JsonReader_SkipSingleLineComment_mEFE4A7A109C1F3E01064F405520C52B76E793DC3_AdjustorThunk },
	{ 0x0600021A, Utf8JsonReader_FindLineSeparator_m9C0187692AE1FC17F7B40A7DDB0ACB7FBFEB340A_AdjustorThunk },
	{ 0x0600021B, Utf8JsonReader_ThrowOnDangerousLineSeparator_mCA676223806C01CA53EC299BFC2C5BC48060EBD2_AdjustorThunk },
	{ 0x0600021C, Utf8JsonReader_SkipMultiLineComment_m8C37CD84ABC6DA62709D69A8D23C779043D025F5_AdjustorThunk },
	{ 0x0600021D, Utf8JsonReader_ConsumeComment_mF43CB1C22ECE1F624E0B562D93A099C01216B78E_AdjustorThunk },
	{ 0x0600021E, Utf8JsonReader_ConsumeSingleLineComment_m55604E2C6DEA3EA494E5663AC035A2131010D844_AdjustorThunk },
	{ 0x0600021F, Utf8JsonReader_ConsumeMultiLineComment_m27C5FF33B76C1C46A721951202BE2983F4F0961B_AdjustorThunk },
	{ 0x06000220, Utf8JsonReader_GetUnescapedSpan_mFAEB005896BE9873061EC7843FD2CDC107B3DE3F_AdjustorThunk },
	{ 0x06000221, Utf8JsonReader_ReadMultiSegment_m34ED95DFD5361E1AF24A36E81C9059A00853C644_AdjustorThunk },
	{ 0x06000222, Utf8JsonReader_ValidateStateAtEndOfData_mE1A59B817BD48A6F2809EEBC7F464A0E28196FF7_AdjustorThunk },
	{ 0x06000223, Utf8JsonReader_HasMoreDataMultiSegment_m31E6091999973DF6F5E877267CD93D1CCDE10AA9_AdjustorThunk },
	{ 0x06000224, Utf8JsonReader_HasMoreDataMultiSegment_mF1B6563CC059AB8F085A0D8A33A11756EDB0B22C_AdjustorThunk },
	{ 0x06000225, Utf8JsonReader_GetNextSpan_m9C9704D9B74FE08A99E2D13820494D10BD775BF3_AdjustorThunk },
	{ 0x06000226, Utf8JsonReader_ReadFirstTokenMultiSegment_mE99805A003E45117C29863C73E136DF8622096CB_AdjustorThunk },
	{ 0x06000227, Utf8JsonReader_SkipWhiteSpaceMultiSegment_m738D7B895E031A7F201D91EC074AAFEE27429A1E_AdjustorThunk },
	{ 0x06000228, Utf8JsonReader_ConsumeValueMultiSegment_mCE9C63707898C340F9731E600FEE7A2F138D0DBC_AdjustorThunk },
	{ 0x06000229, Utf8JsonReader_ConsumeLiteralMultiSegment_m8CE5FBA9EBF636905ABA1189DEA46B55877C255C_AdjustorThunk },
	{ 0x0600022A, Utf8JsonReader_CheckLiteralMultiSegment_m29DBBFB73EDCEDE193F51D9FDDE5CDE7327DF107_AdjustorThunk },
	{ 0x0600022B, Utf8JsonReader_FindMismatch_m3CFC046BA471A51916E4A98AF3C2B055513722FC_AdjustorThunk },
	{ 0x0600022C, Utf8JsonReader_GetInvalidLiteralMultiSegment_m1D4FD07D765B89E82B186D3132EEDA513C420D5D_AdjustorThunk },
	{ 0x0600022D, Utf8JsonReader_ConsumeNumberMultiSegment_mE9CA9359DADDC9A32A0B332B5CE97D5917A99C81_AdjustorThunk },
	{ 0x0600022E, Utf8JsonReader_ConsumePropertyNameMultiSegment_m3A62F15386C832D140978B9FB7C4533F22F19BDC_AdjustorThunk },
	{ 0x0600022F, Utf8JsonReader_ConsumeStringMultiSegment_m9AC39085975494E255DE8A0E3F6F319895CFDBA8_AdjustorThunk },
	{ 0x06000230, Utf8JsonReader_ConsumeStringNextSegment_m85FB7C7822B8165B3A01A6A5E7943EF09212AE1A_AdjustorThunk },
	{ 0x06000231, Utf8JsonReader_ConsumeStringAndValidateMultiSegment_mB975AD804E2DE7E80CCE8106DE68C961A6443BE3_AdjustorThunk },
	{ 0x06000232, Utf8JsonReader_RollBackState_m14899B240A51A2C5298DE5D9CC5C20B68337A8F5_AdjustorThunk },
	{ 0x06000233, Utf8JsonReader_TryGetNumberMultiSegment_mB941922D23D7625DA33A2411425322B95CB49758_AdjustorThunk },
	{ 0x06000234, Utf8JsonReader_ConsumeNegativeSignMultiSegment_mDBEB1BD82E16D0A858BB9176369250A7BC16A7B0_AdjustorThunk },
	{ 0x06000235, Utf8JsonReader_ConsumeZeroMultiSegment_m2F501FEFAD08DB5672B8820A8C3739FACE5167B8_AdjustorThunk },
	{ 0x06000236, Utf8JsonReader_ConsumeIntegerDigitsMultiSegment_m5B05767D5DFC620AAAEDF25D188A1B2F7B1F3F61_AdjustorThunk },
	{ 0x06000237, Utf8JsonReader_ConsumeDecimalDigitsMultiSegment_m8B3F9AAC81565DC842D722B03F6EBF9E038EC2C6_AdjustorThunk },
	{ 0x06000238, Utf8JsonReader_ConsumeSignMultiSegment_m0BDC08DBB245EB51E464A0456FD1F9A08022A78A_AdjustorThunk },
	{ 0x06000239, Utf8JsonReader_ConsumeNextTokenOrRollbackMultiSegment_m1558F3E362E36E672D9370F677E80CF59027A03B_AdjustorThunk },
	{ 0x0600023A, Utf8JsonReader_ConsumeNextTokenMultiSegment_m768B8C95D7DBF55ED9EEE1A3A7E6C397A3FF2162_AdjustorThunk },
	{ 0x0600023B, Utf8JsonReader_ConsumeNextTokenFromLastNonCommentTokenMultiSegment_mCBD2A8ECC218C80D1637EE6F4AFAD5DE7EC84E1E_AdjustorThunk },
	{ 0x0600023C, Utf8JsonReader_SkipAllCommentsMultiSegment_m9F26B47E140957BFC485A5B1194A67FFA80AFF38_AdjustorThunk },
	{ 0x0600023D, Utf8JsonReader_SkipAllCommentsMultiSegment_m6BDCBE0CE1EA8656FE6ECF632FC8BFD7925039D5_AdjustorThunk },
	{ 0x0600023E, Utf8JsonReader_ConsumeNextTokenUntilAfterAllCommentsAreSkippedMultiSegment_m927DB030F5F92F4CB68854405095A7AC372E1CF9_AdjustorThunk },
	{ 0x0600023F, Utf8JsonReader_SkipOrConsumeCommentMultiSegmentWithRollback_m4A93250DEBD59CE9497CE8D857E1D1AFFAECE616_AdjustorThunk },
	{ 0x06000240, Utf8JsonReader_SkipCommentMultiSegment_m41581C771A4EAD318B08C195AE89705757923A12_AdjustorThunk },
	{ 0x06000241, Utf8JsonReader_SkipSingleLineCommentMultiSegment_m64E237F76BE411B9D1C000094FB64D3E3D921EDC_AdjustorThunk },
	{ 0x06000242, Utf8JsonReader_FindLineSeparatorMultiSegment_m7409FC03149D983045F95B254E219425B2AF125F_AdjustorThunk },
	{ 0x06000243, Utf8JsonReader_ThrowOnDangerousLineSeparatorMultiSegment_m71F825306CAE9334C56ACD50EA17AC32E5F53231_AdjustorThunk },
	{ 0x06000244, Utf8JsonReader_SkipMultiLineCommentMultiSegment_m927C47BED86E20BDB7136DE116E105CE8E52187E_AdjustorThunk },
	{ 0x06000245, Utf8JsonReader_CaptureState_mE854A6A21277EB2333CF3387664CDA0072756A30_AdjustorThunk },
	{ 0x06000246, Utf8JsonReader_GetString_m3AA74B2BD8AE8211C2F45748764CA0F8D0772F11_AdjustorThunk },
	{ 0x06000247, Utf8JsonReader_GetBoolean_m4A0CCA2B3FF77D11CDEF65077936B5CA951962C6_AdjustorThunk },
	{ 0x06000248, Utf8JsonReader_GetBytesFromBase64_mBD400780F573FB4057B04E9B6CE298AC931ACD5A_AdjustorThunk },
	{ 0x06000249, Utf8JsonReader_GetByte_mEC280899A1AD56A59AABAF6F35E061C5A257A232_AdjustorThunk },
	{ 0x0600024A, Utf8JsonReader_GetByteWithQuotes_mDAF5E947FAA492D8510E6BFA0AF8CC8BECA26C05_AdjustorThunk },
	{ 0x0600024B, Utf8JsonReader_GetSByte_m86C5D95B6393261601AB9C07BEABB0675C00846A_AdjustorThunk },
	{ 0x0600024C, Utf8JsonReader_GetSByteWithQuotes_mCB8C7C905A515C7695ABC62CAEA61496AA408688_AdjustorThunk },
	{ 0x0600024D, Utf8JsonReader_GetInt16_m486F6B00F180588F76CF037EA708079C921D5C2B_AdjustorThunk },
	{ 0x0600024E, Utf8JsonReader_GetInt16WithQuotes_mFBCA9FB631328EFD1F4A727A40E464784A59C4F3_AdjustorThunk },
	{ 0x0600024F, Utf8JsonReader_GetInt32_m9994A31A6BBC68EC25BFEA942924684639E0A350_AdjustorThunk },
	{ 0x06000250, Utf8JsonReader_GetInt32WithQuotes_m26F4EF8D42082CD49AE7E2FE9683CCA264E9F746_AdjustorThunk },
	{ 0x06000251, Utf8JsonReader_GetInt64_m8F101B63B25099413DEC5D297BB5FFE578DE165A_AdjustorThunk },
	{ 0x06000252, Utf8JsonReader_GetInt64WithQuotes_m3096847DAFC9F239BE4FAC096D247502AD492147_AdjustorThunk },
	{ 0x06000253, Utf8JsonReader_GetUInt16_mDADEC8D0157630B34BA4BED95BEC8FC0FB103F36_AdjustorThunk },
	{ 0x06000254, Utf8JsonReader_GetUInt16WithQuotes_m3AFF72BAC86A2C8D95E30D2CE927C58C01AD4CF5_AdjustorThunk },
	{ 0x06000255, Utf8JsonReader_GetUInt32_m727AE5A5ECBB3D29C4E592193706C81080C3AA75_AdjustorThunk },
	{ 0x06000256, Utf8JsonReader_GetUInt32WithQuotes_m4A9C9DF5E09E76CABB9355907A996C884EBFE5A7_AdjustorThunk },
	{ 0x06000257, Utf8JsonReader_GetUInt64_mD82438E5E150BE6DCB19E9AA8527032FE1073A7B_AdjustorThunk },
	{ 0x06000258, Utf8JsonReader_GetUInt64WithQuotes_mDF51094C764240D18BE44B978CFFF09900AB4AE7_AdjustorThunk },
	{ 0x06000259, Utf8JsonReader_GetSingle_mE9EEE464C1E81A3F019BBA55FC21105525AEEE42_AdjustorThunk },
	{ 0x0600025A, Utf8JsonReader_GetSingleWithQuotes_m8BDBB093E18B06F604D53E31CABDE152B79FCEFD_AdjustorThunk },
	{ 0x0600025B, Utf8JsonReader_GetSingleFloatingPointConstant_m557E5950D17D08737B79AE2B41C281FC62BA1E4A_AdjustorThunk },
	{ 0x0600025C, Utf8JsonReader_GetDouble_m8B6B801EAD7C30EFEFCEE8CEF6D1522CA4778EE1_AdjustorThunk },
	{ 0x0600025D, Utf8JsonReader_GetDoubleWithQuotes_m3C6612DDA71C26099B2C314761AAF3F7CD733658_AdjustorThunk },
	{ 0x0600025E, Utf8JsonReader_GetDoubleFloatingPointConstant_m7649E11BD1B5F7CFEABE646638133CBBF0F69976_AdjustorThunk },
	{ 0x0600025F, Utf8JsonReader_GetDecimal_m0F141F3F50CD249A6EA21E01188DE311EAE86F41_AdjustorThunk },
	{ 0x06000260, Utf8JsonReader_GetDecimalWithQuotes_m8EBE26F40D4FACB3B92BD0C7F75C4AE8E5E509B3_AdjustorThunk },
	{ 0x06000261, Utf8JsonReader_GetDateTime_mE98D9A548388D8526C4F0CC35899D2F14D43B22F_AdjustorThunk },
	{ 0x06000262, Utf8JsonReader_GetDateTimeNoValidation_mB25F994C72A93EA8103DD71FEE99A87D6634B104_AdjustorThunk },
	{ 0x06000263, Utf8JsonReader_GetDateTimeOffset_m34029D75821CEB4740BF1D8B359747D6475C88B9_AdjustorThunk },
	{ 0x06000264, Utf8JsonReader_GetDateTimeOffsetNoValidation_m32A0E97EF22CB407C923E5E795301AE87FE45309_AdjustorThunk },
	{ 0x06000265, Utf8JsonReader_GetGuid_mF53227165B93F42BCC17F4212C98AE792D47699A_AdjustorThunk },
	{ 0x06000266, Utf8JsonReader_GetGuidNoValidation_mF56895F5C26C43EAC16F16A589C002B6699C7771_AdjustorThunk },
	{ 0x06000267, Utf8JsonReader_TryGetBytesFromBase64_m7DEA918E54E58B402F89A126FD98D29D1D4F64A4_AdjustorThunk },
	{ 0x06000268, Utf8JsonReader_TryGetByte_m3A5B27FD2CA5F304075022FF7E1BC05D2441E4BB_AdjustorThunk },
	{ 0x06000269, Utf8JsonReader_TryGetByteCore_m37008192DDB31799D7864470CC49F8C8A0CBD778_AdjustorThunk },
	{ 0x0600026A, Utf8JsonReader_TryGetSByte_m59FC81E3CF3CC2BA6161271E603F4C8F96CD6329_AdjustorThunk },
	{ 0x0600026B, Utf8JsonReader_TryGetSByteCore_m136CD23B2D669BC9E205AEED6B9CA0A2D79DAC43_AdjustorThunk },
	{ 0x0600026C, Utf8JsonReader_TryGetInt16_mDE28715B75D564D7EB68209FCC19C1A6357DD372_AdjustorThunk },
	{ 0x0600026D, Utf8JsonReader_TryGetInt16Core_mCBDC2DF50B262CB386B03CD349C1D60176C53AFA_AdjustorThunk },
	{ 0x0600026E, Utf8JsonReader_TryGetInt32_m98C94CE92457BDD3B53CAED4A8701BF93EAC2617_AdjustorThunk },
	{ 0x0600026F, Utf8JsonReader_TryGetInt32Core_m088C7F21848C7409B390A4D9BCF66E1BFDFAAFD8_AdjustorThunk },
	{ 0x06000270, Utf8JsonReader_TryGetInt64_m2A2CEAA1791795E6DB1420BB381D71B6C593272F_AdjustorThunk },
	{ 0x06000271, Utf8JsonReader_TryGetInt64Core_m3993E1A0CB78D318C835E868E467414537576332_AdjustorThunk },
	{ 0x06000272, Utf8JsonReader_TryGetUInt16_m22C896C87811291DFD23ED0457B38E7825D6D296_AdjustorThunk },
	{ 0x06000273, Utf8JsonReader_TryGetUInt16Core_m9A0768CC1CF4BAC47EA82BAAD39CDF0E24C6664C_AdjustorThunk },
	{ 0x06000274, Utf8JsonReader_TryGetUInt32_mA2AA2EEC55C6FEDBD9844AD8473698D2214636BA_AdjustorThunk },
	{ 0x06000275, Utf8JsonReader_TryGetUInt32Core_m40A9CCB041D399F181EDE2B459EA7D77EBE6FB63_AdjustorThunk },
	{ 0x06000276, Utf8JsonReader_TryGetUInt64_m9728DDCC0E176DC03C0FEEB2C3FFC4566B2D72B9_AdjustorThunk },
	{ 0x06000277, Utf8JsonReader_TryGetUInt64Core_m73F5FB752D9F173DDA97E4679DBF61BFB82C3C71_AdjustorThunk },
	{ 0x06000278, Utf8JsonReader_TryGetSingle_m94587873A494E75F4825C1CA091504E3BB8FEF2D_AdjustorThunk },
	{ 0x06000279, Utf8JsonReader_TryGetDouble_m13353F863EF92179E6798D51791CDBD278180CEB_AdjustorThunk },
	{ 0x0600027A, Utf8JsonReader_TryGetDecimal_m3B8E8434E5962329BBF1F17FD4FC6F4CF186D8F2_AdjustorThunk },
	{ 0x0600027B, Utf8JsonReader_TryGetDateTime_m07EA74E20E656F735523B9F5C04C0DFA45D8AFC5_AdjustorThunk },
	{ 0x0600027C, Utf8JsonReader_TryGetDateTimeCore_mD25A6F6750D17FD2E4D61338DECD3F546E1B1931_AdjustorThunk },
	{ 0x0600027D, Utf8JsonReader_TryGetDateTimeOffset_mDBA3CEC29896D95E62041F3AB3C851BBF1B7D8AD_AdjustorThunk },
	{ 0x0600027E, Utf8JsonReader_TryGetDateTimeOffsetCore_mD9E6F3076962699F2C5876A47B83E133E9B33533_AdjustorThunk },
	{ 0x0600027F, Utf8JsonReader_TryGetGuid_mA3FD580BF871065767D82031AD0A499DB0EB8BD8_AdjustorThunk },
	{ 0x06000280, Utf8JsonReader_TryGetGuidCore_m44AE06756804CEB5DBA738D40527CA2254094530_AdjustorThunk },
	{ 0x06000281, PartialStateForRollback__ctor_m0F515AA3F80A7125D3079909A6578BD37B6EB3BB_AdjustorThunk },
	{ 0x06000282, PartialStateForRollback_GetStartPosition_m57397FA90C6F268F5711E7E5131D5237E52F41B0_AdjustorThunk },
	{ 0x06000370, ParameterRef__ctor_mD6F9E352509A2D16FBBFB12B05A40A70FB04672A_AdjustorThunk },
	{ 0x06000371, PropertyRef__ctor_mBB574D64A64A57C51A58468AC6514DE568D533E5_AdjustorThunk },
	{ 0x06000372, ReadStack_get_IsContinuation_mCEC1851A10A829FFFE42BBAD53711A0A990526F8_AdjustorThunk },
	{ 0x06000373, ReadStack_AddCurrent_m3B0106DB1337C3B7F26B2F874E40B80FA7FCBD18_AdjustorThunk },
	{ 0x06000374, ReadStack_Initialize_m4CCFB4EF4B2C80815463CE81FE499FFF8528BB3B_AdjustorThunk },
	{ 0x06000375, ReadStack_Push_m1F2599D52633E428D94A9D90E77A833FE64BC0BC_AdjustorThunk },
	{ 0x06000376, ReadStack_Pop_mD20CAC0E80A32CAE52481E175241AFDE02D4C9DD_AdjustorThunk },
	{ 0x06000377, ReadStack_JsonPath_m4DF8F1F7BB216E3E6F9EFE4066E9B94D270E49C5_AdjustorThunk },
	{ 0x06000378, ReadStack_SetConstructorArgumentState_mCE9355370E6E636D813179995CFCB4B869A20033_AdjustorThunk },
	{ 0x0600037E, ReadStackFrame_EndConstructorParameter_m8D04A7A3CE2D19DA2C0CCFA8937859DEF1151A14_AdjustorThunk },
	{ 0x0600037F, ReadStackFrame_EndProperty_mBF44974D081F8BDCC44666395A7CC82DB3E557E4_AdjustorThunk },
	{ 0x06000380, ReadStackFrame_EndElement_mA9D0401AD7A3F30475D97C6E49C671AA98558979_AdjustorThunk },
	{ 0x06000381, ReadStackFrame_IsProcessingDictionary_mBFC8EA6ACB5BA357E8887719A4CD2F61F14DD2AE_AdjustorThunk },
	{ 0x06000382, ReadStackFrame_IsProcessingEnumerable_m3A7B122E538645493032819C697D4FD0D7BB40E3_AdjustorThunk },
	{ 0x06000383, ReadStackFrame_Reset_m9998E100B9CF5B2FC93FDE5C50FDB77227903C9A_AdjustorThunk },
	{ 0x06000384, WriteStack_get_IsContinuation_mBDA5ACA7E19BAF628ED4BDCE702B8B46E6AA9BDC_AdjustorThunk },
	{ 0x06000385, WriteStack_AddCurrent_mCA9F10EA1D6E24E25E27295386208F3E71A3654E_AdjustorThunk },
	{ 0x06000386, WriteStack_Initialize_mD5B4E7AF1DE653A9D2E614A452799636C009F9CC_AdjustorThunk },
	{ 0x06000387, WriteStack_Push_m7609AF43966C67D97FE3A16525E55D53FA2569AA_AdjustorThunk },
	{ 0x06000388, WriteStack_Pop_mC6F6750427C0EEEEAC7B9ED1BF9ECC07677F7000_AdjustorThunk },
	{ 0x06000389, WriteStack_PropertyPath_mAE92BA9B54387C839164F69A7944CF0401165039_AdjustorThunk },
	{ 0x0600038C, WriteStackFrame_EndDictionaryElement_m0E57022D7831F92A528248C5A25CE8B5E0A28F0C_AdjustorThunk },
	{ 0x0600038D, WriteStackFrame_EndProperty_m713E4D5585A2354CD0CE2DF56B7DA3B9E7FD31C8_AdjustorThunk },
	{ 0x0600038E, WriteStackFrame_GetPolymorphicJsonPropertyInfo_m8C493C533AC6BB1D6E45E7F27F0664F157CDD602_AdjustorThunk },
	{ 0x0600038F, WriteStackFrame_InitializeReEntry_mDA0CD4D8B09439188519F97297A2A2D85D3BD619_AdjustorThunk },
	{ 0x06000390, WriteStackFrame_Reset_mC74400A4F12DD1688E4D4C1EC4C6DC305B96E56E_AdjustorThunk },
	{ 0x060003B4, JsonWriterOptions_get_Encoder_mAE5C32DE2F9E54D87346B602EA2DB2C0676D7406_AdjustorThunk },
	{ 0x060003B5, JsonWriterOptions_set_Encoder_m5E357AEFC0AB885F7858AE1D70B7060273AED306_AdjustorThunk },
	{ 0x060003B6, JsonWriterOptions_get_Indented_mDF7E8A47E47FF6E43BE1E577F434CA7525E4AC0A_AdjustorThunk },
	{ 0x060003B7, JsonWriterOptions_set_Indented_m5418A714E77EA6D9734E6A497FFD407D799AF8DB_AdjustorThunk },
	{ 0x060003B8, JsonWriterOptions_get_SkipValidation_m9113D9C54C02B4EBA452A57485A91F8A30E74DB0_AdjustorThunk },
	{ 0x060003B9, JsonWriterOptions_set_SkipValidation_m805F1EC553421C91697F04ED05567BB51981F43A_AdjustorThunk },
	{ 0x060003BA, JsonWriterOptions_get_IndentedOrNotSkipValidation_mFC205FBEF9620AD791C2EAC0A7F776281E2D9900_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1568] = 
{
	9442,
	9442,
	9442,
	7469,
	7469,
	7469,
	14150,
	13749,
	13595,
	14394,
	14423,
	12869,
	12869,
	11811,
	11157,
	12869,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14458,
	14502,
	9442,
	14458,
	2761,
	6353,
	14502,
	7557,
	9442,
	7469,
	7469,
	7597,
	9442,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	7557,
	9032,
	9442,
	9442,
	7557,
	4748,
	7557,
	14267,
	13922,
	12869,
	13922,
	13922,
	14252,
	14252,
	14502,
	14502,
	14252,
	14256,
	13922,
	13898,
	13898,
	13898,
	14458,
	12854,
	12823,
	12823,
	11240,
	11098,
	13587,
	13898,
	11099,
	11284,
	14231,
	14252,
	14252,
	14502,
	13922,
	13922,
	12869,
	11114,
	11114,
	14458,
	13914,
	13914,
	13898,
	14256,
	13266,
	14256,
	14256,
	13266,
	14256,
	14256,
	14256,
	14252,
	14256,
	12197,
	13266,
	13266,
	12197,
	14502,
	13266,
	13266,
	14256,
	14256,
	10651,
	13266,
	12197,
	13266,
	14256,
	14256,
	13266,
	12029,
	13152,
	12036,
	12036,
	13152,
	13152,
	12197,
	13266,
	0,
	13266,
	12036,
	13152,
	12165,
	12165,
	14242,
	14256,
	14242,
	14242,
	13135,
	14502,
	13135,
	13141,
	12029,
	14256,
	14256,
	12092,
	13152,
	14256,
	12197,
	12029,
	9247,
	9442,
	9442,
	7469,
	9161,
	9161,
	7557,
	9442,
	9442,
	12712,
	9161,
	9256,
	1505,
	9442,
	7597,
	5439,
	3123,
	2530,
	4795,
	3438,
	909,
	2697,
	6739,
	6739,
	6653,
	4037,
	4037,
	2546,
	14228,
	3641,
	3641,
	11219,
	9442,
	3659,
	13198,
	12813,
	12866,
	13894,
	11482,
	11748,
	1812,
	912,
	9247,
	9247,
	9161,
	9161,
	9247,
	9161,
	2248,
	9161,
	9247,
	7557,
	7597,
	7557,
	9442,
	9442,
	2248,
	9442,
	4002,
	4002,
	7557,
	6263,
	6263,
	8073,
	5439,
	4522,
	4002,
	7557,
	9442,
	7832,
	9546,
	9442,
	9161,
	9247,
	9161,
	9260,
	4359,
	9161,
	9161,
	6654,
	2752,
	2612,
	9289,
	5304,
	9247,
	9289,
	9289,
	1781,
	7597,
	9547,
	9548,
	9289,
	9256,
	9442,
	7566,
	9256,
	9547,
	9289,
	9289,
	9442,
	9442,
	9289,
	9161,
	7566,
	9259,
	9548,
	9289,
	9289,
	9442,
	9442,
	9289,
	9161,
	9256,
	4324,
	5211,
	9289,
	14394,
	14394,
	14394,
	14394,
	14394,
	14394,
	14394,
	14394,
	9036,
	7597,
	12799,
	12798,
	12798,
	12797,
	5448,
	5478,
	9289,
	9247,
	834,
	1690,
	4368,
	7597,
	9442,
	4375,
	9161,
	7469,
	4375,
	8997,
	7313,
	8997,
	7313,
	9289,
	7597,
	9289,
	7597,
	13464,
	11573,
	11511,
	11518,
	13587,
	14235,
	13887,
	0,
	13590,
	13605,
	13595,
	13596,
	12334,
	12334,
	12334,
	12334,
	11501,
	12357,
	12581,
	11581,
	12815,
	11749,
	11749,
	13887,
	14502,
	12356,
	9161,
	2406,
	4375,
	13490,
	13587,
	13587,
	13587,
	13728,
	10164,
	13730,
	13758,
	12334,
	12334,
	12334,
	14141,
	12334,
	12334,
	11470,
	12814,
	12267,
	12333,
	12341,
	12334,
	13887,
	13729,
	12676,
	13887,
	11217,
	12228,
	14502,
	9161,
	7469,
	9247,
	7557,
	9161,
	7469,
	7570,
	9260,
	9161,
	9035,
	9036,
	9036,
	7345,
	9248,
	9248,
	7558,
	9247,
	9161,
	9161,
	9161,
	7469,
	9161,
	9035,
	7344,
	9261,
	2221,
	9161,
	9442,
	9442,
	9161,
	9161,
	9442,
	9442,
	9442,
	9442,
	9442,
	9161,
	9161,
	5439,
	5361,
	9442,
	5361,
	2610,
	2608,
	7345,
	9161,
	9161,
	9161,
	2611,
	2611,
	2609,
	2628,
	2628,
	2628,
	2628,
	2628,
	5361,
	5361,
	9161,
	5304,
	2630,
	5361,
	9161,
	2609,
	6173,
	7345,
	2609,
	9161,
	2611,
	2611,
	9036,
	9161,
	9161,
	9161,
	5439,
	9161,
	5361,
	9442,
	5361,
	2610,
	1779,
	3032,
	6712,
	9161,
	9161,
	9161,
	9161,
	2611,
	3637,
	2609,
	1788,
	1788,
	2628,
	1788,
	1788,
	5361,
	5361,
	9161,
	5304,
	2630,
	5361,
	9161,
	5304,
	2609,
	3034,
	3615,
	5211,
	9620,
	9289,
	9161,
	9289,
	9161,
	9161,
	9336,
	9336,
	9246,
	9246,
	9247,
	9247,
	9248,
	9248,
	9421,
	9421,
	9422,
	9422,
	9423,
	9423,
	9343,
	9343,
	9343,
	9189,
	9189,
	9189,
	9186,
	9186,
	9184,
	9184,
	9185,
	9185,
	9221,
	9221,
	5304,
	5304,
	2627,
	5304,
	2627,
	5304,
	2627,
	5304,
	2627,
	5304,
	2627,
	5304,
	2627,
	5304,
	2627,
	5304,
	2627,
	5304,
	5304,
	5304,
	5304,
	5304,
	5304,
	5304,
	5304,
	5304,
	1607,
	6857,
	0,
	9442,
	6745,
	14234,
	9442,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9289,
	7597,
	9289,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	4368,
	399,
	7597,
	12478,
	13600,
	5478,
	11811,
	11811,
	10399,
	11353,
	13600,
	13600,
	13405,
	9247,
	7557,
	10398,
	9776,
	11157,
	2012,
	2012,
	11479,
	11479,
	14178,
	7423,
	7423,
	14502,
	13922,
	4362,
	9289,
	0,
	0,
	0,
	0,
	4368,
	9289,
	9289,
	9247,
	5478,
	7597,
	9289,
	7597,
	9289,
	6745,
	9442,
	9442,
	0,
	14502,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	8998,
	7315,
	9247,
	7557,
	9289,
	9289,
	7597,
	9161,
	7469,
	1717,
	13922,
	9442,
	0,
	0,
	0,
	0,
	0,
	0,
	14458,
	12869,
	9289,
	7597,
	2216,
	9442,
	7314,
	3612,
	7315,
	0,
	0,
	0,
	0,
	9161,
	7469,
	9161,
	7469,
	75,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	1851,
	0,
	0,
	1788,
	9289,
	7597,
	9289,
	7597,
	9289,
	9289,
	7597,
	0,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	8998,
	7315,
	9442,
	14502,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	11482,
	13728,
	11492,
	0,
	10370,
	11444,
	13266,
	0,
	0,
	0,
	0,
	10939,
	10939,
	0,
	0,
	0,
	0,
	14502,
	14458,
	6745,
	14458,
	9289,
	2094,
	6745,
	1462,
	11811,
	12869,
	11157,
	9289,
	7597,
	9442,
	9161,
	9247,
	9289,
	9289,
	9161,
	9247,
	9247,
	9161,
	9161,
	9161,
	9247,
	9247,
	9289,
	9161,
	9161,
	9161,
	9289,
	9289,
	6745,
	6745,
	5478,
	9260,
	9262,
	9442,
	14502,
	13250,
	6718,
	13250,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9442,
	2483,
	2483,
	9161,
	9442,
	2414,
	9442,
	7469,
	9289,
	9442,
	14502,
	13250,
	13753,
	13266,
	13894,
	9442,
	9442,
	9442,
	9161,
	9161,
	9442,
	9161,
	9442,
	2088,
	9442,
	7469,
	9289,
	13250,
	13266,
	9442,
	9442,
	9289,
	2094,
	9442,
	13600,
	13600,
	12478,
	13139,
	14231,
	14231,
	14231,
	14245,
	14263,
	14232,
	14232,
	14231,
	12030,
	12031,
	13138,
	11907,
	14394,
	13587,
	13611,
	12673,
	12680,
	12714,
	11218,
	10532,
	12106,
	13587,
	13611,
	11221,
	10533,
	12223,
	11645,
	10814,
	12689,
	12689,
	14502,
	9289,
	7597,
	9161,
	7469,
	9161,
	7469,
	9161,
	9247,
	7557,
	9248,
	7558,
	9247,
	9247,
	4365,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	7469,
	7469,
	7469,
	9442,
	7469,
	7567,
	3616,
	3616,
	9442,
	9442,
	7469,
	7469,
	7469,
	7469,
	7469,
	7360,
	7469,
	7557,
	7557,
	9442,
	7496,
	7497,
	7498,
	7501,
	7646,
	7534,
	9442,
	9442,
	7469,
	3616,
	3616,
	3619,
	7567,
	7345,
	3613,
	3613,
	3613,
	3613,
	3613,
	7469,
	7557,
	7558,
	7567,
	7345,
	7345,
	7597,
	7346,
	3623,
	7346,
	7346,
	7346,
	7345,
	7345,
	3617,
	7345,
	7345,
	7345,
	7345,
	4326,
	4325,
	3614,
	2218,
	3614,
	3614,
	3614,
	7717,
	7718,
	7345,
	7345,
	7345,
	7345,
	7496,
	7496,
	7496,
	7497,
	7497,
	7497,
	7498,
	7498,
	7498,
	7498,
	7501,
	7501,
	7501,
	11503,
	7501,
	7501,
	7646,
	7646,
	7646,
	11563,
	7646,
	7646,
	7345,
	7345,
	7345,
	7534,
	7534,
	7534,
	9442,
	2219,
	9442,
	7469,
	7345,
	7345,
	7345,
	7557,
	7558,
	7558,
	7558,
	7558,
	7567,
	7597,
	7346,
	7346,
	7346,
	7346,
	7346,
	3623,
	7345,
	7345,
	7345,
	7345,
	7345,
	3617,
	7345,
	7717,
	7718,
	7718,
	7718,
	7718,
	14502,
	9289,
	6745,
	9247,
	9247,
	9289,
	7597,
	6739,
	4037,
	9247,
	9161,
	7597,
	9442,
	5478,
	4359,
	9289,
	6353,
	4037,
	5478,
	7557,
	9289,
	12869,
	12869,
	13600,
	13600,
	12869,
	12869,
	13922,
	13922,
	13600,
	13922,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9442,
	0,
	0,
	9161,
	7469,
	9161,
	9161,
	7469,
	0,
	0,
	0,
	9161,
	7469,
	9161,
	7469,
	0,
	9289,
	2752,
	0,
	0,
	0,
	0,
	0,
	9161,
	9289,
	7597,
	7597,
	2227,
	11497,
	12357,
	9442,
	9161,
	0,
	9289,
	9289,
	9289,
	3462,
	2017,
	902,
	954,
	9289,
	954,
	1713,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9289,
	6723,
	9442,
	7469,
	4368,
	3454,
	6745,
	0,
	6723,
	9442,
	14502,
	0,
	0,
	0,
	9442,
	6745,
	0,
	13922,
	0,
	10409,
	0,
	13922,
	0,
	11811,
	0,
	11811,
	0,
	12869,
	0,
	12869,
	0,
	12869,
	0,
	12869,
	12869,
	12869,
	0,
	9442,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	5478,
	3462,
	9442,
	14502,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	5478,
	3462,
	5478,
	3462,
	6745,
	9442,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	1795,
	2358,
	5304,
	1630,
	9442,
	2018,
	2423,
	9442,
	9442,
	1795,
	2358,
	5304,
	1630,
	2630,
	2357,
	2154,
	2443,
	6977,
	1733,
	9442,
	1934,
	2363,
	5878,
	1636,
	9442,
	1937,
	2365,
	5887,
	1637,
	9442,
	9442,
	1940,
	2367,
	5894,
	1638,
	2981,
	2366,
	9442,
	1942,
	2369,
	5907,
	1641,
	2982,
	2368,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	9442,
	5478,
	3462,
	1945,
	2371,
	5958,
	1642,
	9442,
	9442,
	1948,
	2375,
	5985,
	1643,
	2994,
	2374,
	9442,
	1959,
	2390,
	6246,
	1678,
	3044,
	2387,
	9442,
	1994,
	2407,
	6618,
	1687,
	3363,
	2405,
	2018,
	2423,
	9442,
	2007,
	2410,
	9442,
	0,
	0,
	0,
	0,
	0,
	5478,
	3462,
	12869,
	9442,
	2018,
	2423,
	6718,
	1713,
	3462,
	9442,
	9442,
	2133,
	2432,
	6846,
	1725,
	3501,
	2431,
	9442,
	2135,
	2435,
	6864,
	1726,
	3503,
	2434,
	2018,
	2423,
	6718,
	1713,
	9442,
	2018,
	2423,
	9442,
	9442,
	2154,
	2443,
	6977,
	1733,
	3529,
	2442,
	9442,
	2157,
	2446,
	6984,
	1735,
	3534,
	2445,
	9442,
	2162,
	2449,
	6990,
	1736,
	3540,
	2448,
	2018,
	2423,
	9442,
	14171,
};
static const Il2CppTokenRangePair s_rgctxIndices[60] = 
{
	{ 0x02000013, { 0, 12 } },
	{ 0x0200003E, { 15, 3 } },
	{ 0x02000040, { 20, 29 } },
	{ 0x02000061, { 69, 4 } },
	{ 0x02000062, { 73, 3 } },
	{ 0x02000063, { 76, 3 } },
	{ 0x02000066, { 79, 29 } },
	{ 0x02000069, { 108, 5 } },
	{ 0x0200006F, { 139, 15 } },
	{ 0x02000070, { 154, 14 } },
	{ 0x02000071, { 168, 14 } },
	{ 0x02000072, { 182, 24 } },
	{ 0x02000074, { 206, 21 } },
	{ 0x02000075, { 227, 19 } },
	{ 0x02000076, { 246, 12 } },
	{ 0x02000077, { 258, 25 } },
	{ 0x02000078, { 283, 8 } },
	{ 0x0200007A, { 291, 17 } },
	{ 0x0200007B, { 308, 17 } },
	{ 0x0200007C, { 325, 11 } },
	{ 0x0200007D, { 336, 8 } },
	{ 0x0200007E, { 344, 19 } },
	{ 0x0200007F, { 363, 25 } },
	{ 0x02000080, { 388, 19 } },
	{ 0x02000081, { 407, 24 } },
	{ 0x02000082, { 431, 20 } },
	{ 0x02000083, { 451, 14 } },
	{ 0x02000084, { 465, 15 } },
	{ 0x02000085, { 480, 15 } },
	{ 0x02000086, { 495, 13 } },
	{ 0x02000088, { 508, 8 } },
	{ 0x02000089, { 516, 19 } },
	{ 0x0200008A, { 535, 10 } },
	{ 0x0200008B, { 545, 24 } },
	{ 0x02000094, { 573, 15 } },
	{ 0x0200009D, { 588, 12 } },
	{ 0x060001A9, { 12, 3 } },
	{ 0x060002EF, { 18, 2 } },
	{ 0x06000328, { 49, 1 } },
	{ 0x06000329, { 50, 1 } },
	{ 0x0600032D, { 51, 2 } },
	{ 0x06000331, { 53, 1 } },
	{ 0x06000332, { 54, 3 } },
	{ 0x06000333, { 57, 2 } },
	{ 0x06000334, { 59, 1 } },
	{ 0x06000337, { 60, 3 } },
	{ 0x06000338, { 63, 3 } },
	{ 0x06000339, { 66, 2 } },
	{ 0x0600033A, { 68, 1 } },
	{ 0x060004D3, { 113, 1 } },
	{ 0x060004D5, { 114, 5 } },
	{ 0x060004D7, { 119, 2 } },
	{ 0x060004D9, { 121, 4 } },
	{ 0x060004DB, { 125, 4 } },
	{ 0x060004DD, { 129, 2 } },
	{ 0x060004DF, { 131, 2 } },
	{ 0x060004E1, { 133, 2 } },
	{ 0x060004E3, { 135, 2 } },
	{ 0x060004E7, { 137, 2 } },
	{ 0x06000585, { 569, 4 } },
};
extern const uint32_t g_rgctx_MemoryExtensions_AsMemory_TisT_tA40AE9A1C457E02D08F5C56BB330D6F0824C67FC_m77657FD9A165C0213DC903A229C7D42D1064DC86;
extern const uint32_t g_rgctx_Memory_1_op_Implicit_m4C5BCA4EEB7657957B1123BC5624C1D681EF1425;
extern const uint32_t g_rgctx_Memory_1_t90A0C30EBF1E00C6297DCD3DFCCBA0053BEEEA49;
extern const uint32_t g_rgctx_MemoryExtensions_AsSpan_TisT_tA40AE9A1C457E02D08F5C56BB330D6F0824C67FC_mCF7F9AA15AD54596111BFD85C71EDE4122F83FDB;
extern const uint32_t g_rgctx_Span_1_Clear_mDEB54AE4B12DAD2965A38C42A711D306BD3A8F82;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_ThrowInvalidOperationException_AdvancedTooFar_m0A45E21368167FD52F92A4AFB0D226CD0FCFB4EB;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_tBAC052AAF17A817C9E895AD8DD5A0295F28C65DE;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_CheckAndResizeBuffer_m0DC9B191C3FAAEFDA6FA191D9ACCA7E5A0120CC8;
extern const uint32_t g_rgctx_MemoryExtensions_AsMemory_TisT_tA40AE9A1C457E02D08F5C56BB330D6F0824C67FC_m33E66584396BA0B8FED2FACED932C5B8DD6984B1;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_get_FreeCapacity_mEEB12BC953404EB085821B6B5A402B490FDC40A7;
extern const uint32_t g_rgctx_ArrayBufferWriter_1_ThrowOutOfMemoryException_m2C874FB04D7CA06256F03E9DBAA41464C2BD9F43;
extern const uint32_t g_rgctx_Array_Resize_TisT_tA40AE9A1C457E02D08F5C56BB330D6F0824C67FC_m355483BA72929D749014983A84E0A9BDFE291059;
extern const uint32_t g_rgctx_Dictionary_2_t33E1725ED7FF92038C7BEE2A63B4B26FC9DA843A;
extern const uint32_t g_rgctx_Dictionary_2_ContainsKey_mC24800622CF4FDC2FBC97717108BE2E3AA0DF0C6;
extern const uint32_t g_rgctx_Dictionary_2_set_Item_mD20119DE218BF07667F6BFB8659E91F7371BAC3F;
extern const uint32_t g_rgctx_T_t1ACC684643F85F6C8E5324691613D8E543CAD721;
extern const uint32_t g_rgctx_JsonParameterInfo_1_set_TypedDefaultValue_m3C74360B1B74B86BD65664003D2F8E993A28FEF3;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m89E9891EEB7C2F1BD829A9B61F10617C004E561D;
extern const uint32_t g_rgctx_TAttribute_tCFEA2E6846437D50965183E52672352B7A75FB37;
extern const uint32_t g_rgctx_TAttribute_tCFEA2E6846437D50965183E52672352B7A75FB37;
extern const uint32_t g_rgctx_MemberAccessor_CreatePropertyGetter_TisT_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_m595C1B2C3BC5BD8EA3D4FB17FC855A3F36A77BEB;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_set_Get_m4A871E94AB7443759497EF0D89F9812017129E5D;
extern const uint32_t g_rgctx_MemberAccessor_CreatePropertySetter_TisT_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_mD2A24ABDF7E004A012A28B45EDFD52992865CB7B;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_set_Set_mCB6B9579B4953FF7AE71BD8DE50FED4F906F8E7F;
extern const uint32_t g_rgctx_MemberAccessor_CreateFieldGetter_TisT_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_m98B237FB5531A4431A0F7CF012DDE6C678B84F20;
extern const uint32_t g_rgctx_MemberAccessor_CreateFieldSetter_TisT_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_mB7F2C75C38350609F54358738FA084761F019B72;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_get_Converter_mC8E33EA21BFAC5B88F765D5A41D0476EACA550EC;
extern const uint32_t g_rgctx_JsonConverter_1_tB491B3D61580B4454329269D8D7F54F428061CD4;
extern const uint32_t g_rgctx_JsonConverter_1_get_CanBeNull_mAD8C927888B7A7823B9CB176676B7DC2F18CC0B7;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_set_Converter_m5C09D57FFA7213CA040996127D220EA9D35310A4;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_get_Get_mCCF1F469F1D7BA2FAB27EBB99C88D5B93FA7684E;
extern const uint32_t g_rgctx_Func_2_tFD604DDB18DA57040905BBA67CA04603E4E5737B;
extern const uint32_t g_rgctx_Func_2_Invoke_mDBCBB8B89BE48E6C167823DFBA3CF8347B64D471;
extern const uint32_t g_rgctx_T_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_mEC94B731540D5077702E532708C32FA3C9F4247F;
extern const uint32_t g_rgctx_EqualityComparer_1_tCA198ABE7175F31BAD8A230AAB26AFE6860FB3CD;
extern const uint32_t g_rgctx_EqualityComparer_1_tCA198ABE7175F31BAD8A230AAB26AFE6860FB3CD;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m99D530921ACC10E8E987EEEDFA5C8CD5BC9620A7;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNullOnWrite_m4D8E029E027E458B1E8957ABE1BDBC83EA050102;
extern const uint32_t g_rgctx_JsonConverter_1_Write_mDFFB07D67B905477FA91927B60850ACAB8BD4781;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m83873E39FC64B21B116216195A94F18216FFE842;
extern const uint32_t g_rgctx_JsonConverter_1_TryWriteDataExtensionProperty_m5B6D7194EECBE9CA6CA24A65F1C9768BEC311C59;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNullOnRead_m0666DC3498F643DD223B2CB8123A9283A9B6B531;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_get_Set_mC587C5490A0914839056AFBFEC42D08BFFC450D8;
extern const uint32_t g_rgctx_Action_2_t4698AA8CDE0EDA2676C1E781DDE56EE96A1D60B2;
extern const uint32_t g_rgctx_Action_2_Invoke_m78C22F8A5D006B8CD021A7290B3172F35ADA617A;
extern const uint32_t g_rgctx_JsonConverter_1_Read_m346818073759147217EB9D2A6FFF7C2CF02E432C;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_m9F212D43E42A396A5A97598FE0A38D7E3EC27BDC;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3;
extern const uint32_t g_rgctx_JsonSerializer_ValidateValueIsCorrectType_TisT_t3E2926710EF8ACEEDACD31E1873C4BCDBAFB774A_m1FB6952A44FF553EDAC6FD20358BBA80BDC89794;
extern const uint32_t g_rgctx_JsonSerializer_ValidateValueIsCorrectType_TisT_tD16B70018644DDD44625688F16858EA83C2E9AE1_m2ECD2781B78D228DB80F3C2DF1811FBB25EBA515;
extern const uint32_t g_rgctx_T_tE34B7119F90B8A8DCBF400060D2975BC1936FF82;
extern const uint32_t g_rgctx_T_tE34B7119F90B8A8DCBF400060D2975BC1936FF82;
extern const uint32_t g_rgctx_JsonSerializer_ReadCore_TisTValue_tB4BC81D4105D04AC3776CF62467ED5A8C2D4E437_m51FF8FE9D3443A55B55556C0128556A92B2FEBF0;
extern const uint32_t g_rgctx_JsonConverter_1_t0A96F0F49D79F1688FE0AE2DDC52B57E2AA12722;
extern const uint32_t g_rgctx_JsonConverter_1_ReadCore_mFBDD9B4FA38EB8A9080FCCC950E5B45D81FC58C1;
extern const uint32_t g_rgctx_TValue_tDE495677ADFE4DA5C09A9386458B2037E402DAC8;
extern const uint32_t g_rgctx_TValue_t0D3DD6F5808D0148D4A7651C2CDCD05DFA18C9A9;
extern const uint32_t g_rgctx_JsonSerializer_Deserialize_TisTValue_t0D3DD6F5808D0148D4A7651C2CDCD05DFA18C9A9_mCC54610C4257B1076F716B0CCA4CAAC5A20CE836;
extern const uint32_t g_rgctx_JsonSerializer_ReadCore_TisTValue_tA8D40E187F663C0EBF8A216DAD90FD068D65E95D_m2EF329400C8450A4EB3A4BB277C2A9CF0448AE01;
extern const uint32_t g_rgctx_TValue_tB1A0258CE4FA26EE8109589ED222E42D413FD00E;
extern const Il2CppRGCTXConstrainedData g_rgctx_TValue_tB1A0258CE4FA26EE8109589ED222E42D413FD00E_Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3;
extern const uint32_t g_rgctx_JsonSerializer_WriteCore_TisTValue_tB1A0258CE4FA26EE8109589ED222E42D413FD00E_m3C080239201F3B28A6C4094DC9B4F07CA39081AD;
extern const uint32_t g_rgctx_JsonConverter_1_tBC475169FE150BDA903142A8CCAB2AEBC01BF4F9;
extern const uint32_t g_rgctx_JsonConverter_1_WriteCore_m34BAB53B8BF3F796156DDB824A4293FC52F5F238;
extern const uint32_t g_rgctx_TValue_tE5B174CE95B472B7F5634321973402C4BAA02CF9;
extern const uint32_t g_rgctx_TValue_t9FD5D416FDE83F91837BF2CA2542165A362F1931;
extern const uint32_t g_rgctx_JsonSerializer_Serialize_TisTValue_t9FD5D416FDE83F91837BF2CA2542165A362F1931_mF1688D4B068193728FD5AA0AF4EFAF3907F7B734;
extern const uint32_t g_rgctx_JsonSerializer_WriteCore_TisTValue_tA5E9E572BB69B5AEC92F58ECEDEE6E2E7B87C8DB_m098AB2EDEDB99E14E819C11CBD1C9FF30E229326;
extern const uint32_t g_rgctx_TElement_t1AE69815B8DA920D4145B5C48AE600C602C24D2D;
extern const uint32_t g_rgctx_JsonResumableConverter_1__ctor_m4E7427CDCEE1F4AC4FEDB72C5AB7EF3521CE1C15;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t8380F5109164BDA95E80BF33071364147720B0DB;
extern const uint32_t g_rgctx_JsonConverter_1_t52B4721B740701ED7BAC354C51C110660E292773;
extern const uint32_t g_rgctx_JsonResumableConverter_1__ctor_m0F8E68540E78E9019B0DAC67FBAA7C13C67CEA22;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tFBC7918119D9B5576CDE4A7813E5082661BBD43C;
extern const uint32_t g_rgctx_JsonConverter_1_tFAA4828A4741A87A6EF744394FDEB1C3F9B9439C;
extern const uint32_t g_rgctx_JsonResumableConverter_1__ctor_m118F948BDF0A08D23A5B2145F0D914C6A549C334;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tF88DB8ADA94B8B51CF5F4EFD4F88AD11307E7F04;
extern const uint32_t g_rgctx_JsonConverter_1_tFE36F64C2C8201732D89BFDAB3DE34BFEBC30F2E;
extern const uint32_t g_rgctx_JsonConverter_1_ReadCore_m82C77272A57DF8DCF1FE816E15E1FA5BEF0148FC;
extern const uint32_t g_rgctx_T_tD7339923E0CD58E3B0809D33BA14506E69479289;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_mAF4C53202AFBC9B08E553447D10A1F4E804F2E33;
extern const uint32_t g_rgctx_JsonConverter_1_WriteCore_mE88C9E17DF4E356AAAD055E32B8CDD59E6BF78FB;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m40915022FC3FA775F5FE42373BB580F757F530FC;
extern const uint32_t g_rgctx_JsonConverter_1_t0DDD3D2B38F6FDCC21CBB4AB9CD92749E52F6068;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNull_mDF5444469FFBABE6E28F70F49BF301574B4A6B8C;
extern const uint32_t g_rgctx_JsonConverter_1_set_HandleNullOnRead_mFDFF6429DE67A4E8D3F9A2A921120106B8E46A8A;
extern const uint32_t g_rgctx_JsonConverter_1_set_HandleNullOnWrite_m8E4B9952445F21D6B1E704B8A3A031D9A563B1D6;
extern const uint32_t g_rgctx_T_tD7339923E0CD58E3B0809D33BA14506E69479289;
extern const uint32_t g_rgctx_JsonPropertyInfo_1_t1B7EF7E13ACB99867B26075DF18E8B7CAAE1FAF9;
extern const uint32_t g_rgctx_JsonPropertyInfo_1__ctor_mB491F91A647FDC26E35972EFD1C0D6B6D7B30E51;
extern const uint32_t g_rgctx_JsonParameterInfo_1_tF1BCFA850D6EAF1EC6257F8B2E5F5D11E97A4191;
extern const uint32_t g_rgctx_JsonParameterInfo_1__ctor_mF735C1AD426113B4A1AEE21D6AFCFB44EF67A628;
extern const uint32_t g_rgctx_JsonConverter_1_get_CanBeNull_mD7FA296C8D7189FBA86398CB23B474BA2590B215;
extern const uint32_t g_rgctx_JsonConverter_1_Write_mF96C6974E92ACFCA14A441C21E78C0FD1F942401;
extern const uint32_t g_rgctx_JsonConverter_1_Read_mED3F8CB452791104CDCBF9FA47216AECB7F762DF;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNullOnRead_m9CB4E057CE02BD9D9D19C85F41B033576B7F03FA;
extern const uint32_t g_rgctx_JsonConverter_1_ReadNumberWithCustomHandling_m698FF95237957013DD72E45796A1075DDF3BBB4A;
extern const uint32_t g_rgctx_JsonConverter_1_VerifyRead_m9FDF74C2545F5C0A877CB730B394863987B8DC00;
extern const uint32_t g_rgctx_JsonConverter_1_OnTryRead_m11CE784ADFFAF2A8B517984299A4E7081DF10319;
extern const uint32_t g_rgctx_JsonConverter_1_get_HandleNullOnWrite_m0E622464A9383A9A392FC9CB2768183702386E72;
extern const uint32_t g_rgctx_JsonConverter_1_VerifyWrite_mE6AAF4C18EBD27BACF64C10B8A3448A280116069;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_tD7339923E0CD58E3B0809D33BA14506E69479289_Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3;
extern const uint32_t g_rgctx_JsonConverter_1_WriteNumberWithCustomHandling_mFC7A56F56C209CD1333DBABF1DC5750B1132CDC8;
extern const uint32_t g_rgctx_JsonConverter_1_OnTryWrite_mC63B9B04F18A170836FD32B748FB32B78D346FE2;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_tC087909408BE26CE89850F27307FACD0D65450E2;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_OnWriteResume_m2270D7F76FE3115DEAB90010B721DDEACB0D224B;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_m90AFBCFC8C0F40D2195E6175D56295C163011C4F;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_mEA3665F26ED602BF1A7CB20976B04A078C1C4C75;
extern const uint32_t g_rgctx_T_tDB38290D1789CAEE6EC985D497AB8D684C53806E;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m0A0905D2D6802A472FFAE28735CCC40442949003;
extern const uint32_t g_rgctx_JsonConverter_1__ctor_m36EC3D6498D295B255C36AFDDFFA29D48FDA94C9;
extern const uint32_t g_rgctx_JsonConverter_1_t6CDECAEE1BD9961CBC4B710E6ECA2167C215C340;
extern const uint32_t g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisParameterizedConstructorDelegate_1_t13E72AEEB31CA05E4715EFD93B92A809529DF3AC_m4FFA0F178A66287E136FAE6A3204B4976098C456;
extern const uint32_t g_rgctx_TArg0_t3941217589CC324BB016E746C699DF186059F3F4;
extern const uint32_t g_rgctx_TArg1_tB3BA8CF4389D6C54F48EAB0A210DFAD7D09602D6;
extern const uint32_t g_rgctx_TArg2_t333E77888D29CF7A42A4E19940D771617422E38B;
extern const uint32_t g_rgctx_TArg3_tA9553F602CAD122EF7E3C9D3D9E98D684C4348BA;
extern const uint32_t g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisParameterizedConstructorDelegate_5_t892C275BE9831890C910B6A69169AC8CD8989B9A_m52DB4A54DDFEC015DC7853BD0F9568AC90FBC9BF;
extern const uint32_t g_rgctx_TCollection_t09E064CD5B64DB6FF5049F4A1AD0C707D36DB082;
extern const uint32_t g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisAction_2_t4B70AE5083569E1358F878E70576564E1C6212CC_m7FEA4CC638F0215795823FFBA0DAA05978A39678;
extern const uint32_t g_rgctx_TCollection_t55057DF41F97B563F9B7F2C422B34F305AC59066;
extern const uint32_t g_rgctx_TElement_t472E7BDEAA1D91CF6FCA7BFF60F73C65B9BEE494;
extern const uint32_t g_rgctx_IEnumerable_1_t88E41F03C9E5AC89DCBA67A253728A4C49460E69;
extern const uint32_t g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisFunc_2_t2A8C23A1286213A5DB0AC482B465338DBD6C052F_m5B74B31EA744E02E6282ADE41D8CB9C881CDCEA1;
extern const uint32_t g_rgctx_TCollection_tB28D690AA7E6DFBB2B284A3CFA7458737FF12BA2;
extern const uint32_t g_rgctx_TElement_t61516DC240367FA216FE81D9482854543ECD1605;
extern const uint32_t g_rgctx_IEnumerable_1_t007FE4A1A705AA7F956A322A43889BC768192AA2;
extern const uint32_t g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisFunc_2_t4F3A172E8957261FA0D61CBE3A6A68C8E455C11D_mF8402B2F30FAB6A07B0B38481164ADA120018849;
extern const uint32_t g_rgctx_TProperty_tEBE9DBBF641BE459CA13C4B2DC0486565FCA04CA;
extern const uint32_t g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisFunc_2_t458AB41C56A22D8CD32C750FA0C033E26A20B18F_m72541CAE1CDD048164D0468DF5F031CC7CC3A99D;
extern const uint32_t g_rgctx_TProperty_t898B394F11DBC64766E3A13A9C8EDC932F273E34;
extern const uint32_t g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisAction_2_tC206642FD3047B15ABC4EAED17A699DE43236809_m357C29D71657E5FD7E7D399F8F42435C77861E74;
extern const uint32_t g_rgctx_TProperty_tB1943FD17E67BE8E542C2FDA5A6D7B2B6EA35B48;
extern const uint32_t g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisFunc_2_t09B2E99621962966EED02865B7F7E6795E218CC2_mB412EC84B0AEEFEE26010ADF1E8B79670274C7EE;
extern const uint32_t g_rgctx_TProperty_tED2F155BF0CFCD2B4BC16892A56D5ABA934D5C31;
extern const uint32_t g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisAction_2_t80207F11797CF475EC8C17EAFEE81319F66110C4_mC4E60D151AEF37AAC2E6094EE4B3BB1C1F3E640B;
extern const uint32_t g_rgctx_T_tBCB57AF807045415E1CE59FABCDE5477A4CDE0CC;
extern const uint32_t g_rgctx_T_tBCB57AF807045415E1CE59FABCDE5477A4CDE0CC;
extern const uint32_t g_rgctx_List_1_tC19CD21B3D8BB13D0F80B28D89E2E0C7B011DA4D;
extern const uint32_t g_rgctx_List_1_Add_mE7DDCACD787A0ADD781A0332444762B6788BA217;
extern const uint32_t g_rgctx_List_1__ctor_mAF0AFDB7BEBDC6B1840A3822A3BD0CF41596BC4C;
extern const uint32_t g_rgctx_List_1_ToArray_m4E6844F7AF049858D08A162F925020124881C0D7;
extern const uint32_t g_rgctx_TCollection_tB8C1FD8A26A5643B24359BF7077FD7A2EBDAB202;
extern const uint32_t g_rgctx_TElementU5BU5D_t1F5A0E9D0A21BE4A4D5654D862008D7F88BEF9D1;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m00C94C85281FF18BC0579633684F5E341945A13A;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t6E205663D31D77F9E7845A93203CE0E886D35B21;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t8DDC182523F88D102BB288F0AC6B00D0E33DBCD5;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t9E32816B3C99EA5C1050145A6950291C221A0B53;
extern const uint32_t g_rgctx_JsonConverter_1_tC19789DD849F20006A5864BFD2456104F5EB4D49;
extern const uint32_t g_rgctx_JsonConverter_1_t0831FCAA238C9DB92CB0FFCAFE8DB3103F9F1AD3;
extern const uint32_t g_rgctx_JsonConverter_1_Write_m9A67FD1DF2C75D00B1626305074E6A8884F17CC3;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m7EDD5832CCF2CFA76F99E09B1734CA4CE15273B5;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m687408B3103ADB07206F85465170C420E559F2A1;
extern const uint32_t g_rgctx_TCollection_t675149C8AF04CB8444423B6D15B2039A3D82D332;
extern const uint32_t g_rgctx_ConcurrentQueue_1_tCF0B031197FA5EEBC63BD20F24CE6A4B5ABEFA8F;
extern const uint32_t g_rgctx_ConcurrentQueue_1_Enqueue_m45B7E5C10B72EA3C9A42DFD821C58FEBBCA708F9;
extern const uint32_t g_rgctx_ConcurrentQueue_1_GetEnumerator_m6A138A82296718DCACA0E5B9C05942C6255740B9;
extern const uint32_t g_rgctx_IEnumerator_1_tFF0C3AB4C365B19F7527C38AF4F03AC2634E43A5;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m1A720E51E97B198B46722F4D2A5020FA50D784B4;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t69C09F100FC571F525AFB4A9DFA4CC648EA60B43;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tFFE9ABF0AADCB97013D95654A727A1FF301BC985;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t0BA8925ACF53949118D658600132D89992E7B502;
extern const uint32_t g_rgctx_JsonConverter_1_t8FB660384EF0CFAA12DEA76E03245D4C40D196FA;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m14EF60AABA19E8B6C0D78D507BEC675317BB467C;
extern const uint32_t g_rgctx_JsonConverter_1_t21FD9DBF7C504514ACD36BB27AEEBDEAFFB1BCEB;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m790CE80D1E7990710CFEB5A85841BB0BAE2E4A4E;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_mFCD5FFCAE7CC68883380439FB691EE5CC7935261;
extern const uint32_t g_rgctx_TCollection_t8D9B58F532C32FB82474A72D5F945B46B6FA8397;
extern const uint32_t g_rgctx_ConcurrentStack_1_t77367A6D7597A3292D4C267CAB5C646703CB96BE;
extern const uint32_t g_rgctx_ConcurrentStack_1_Push_mE58D4DD1E081D8DD5DA3808C8D66C350F58349C9;
extern const uint32_t g_rgctx_ConcurrentStack_1_GetEnumerator_mF4440B9C6E46510286178B8175D491DFFAEB22A5;
extern const uint32_t g_rgctx_IEnumerator_1_tAC9C08D1EA77809DD7C18FF4134274B9D0004386;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m5BC5933E56F93D5B04698C5FA4B0F4CB98A7D520;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t7B37363A44C180A1692C379294B318AFF04DC08A;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tB7AB5626C1DFCA69236DF9637B9E42322C5FC487;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tA42A6C9D2240C080D9EF5D2057C9363811ECC748;
extern const uint32_t g_rgctx_JsonConverter_1_tB1E8783B3103EF6B8CEA2702B771A4D13825E641;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m28E54482BCEB6D5BA792FFB96FCF0B587A0A685D;
extern const uint32_t g_rgctx_JsonConverter_1_t69E19CA421B1E2AE50193E350E25BD09407D7106;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mBFF44E4B86C4C7EA2344D08A1A5166C8D0198895;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m84DFD8222E8E8D9C449CF27429F61461E7B2ACC7;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_t0B233067DD1C957B9DE5EDAF115831EDEDBD37A5;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t411F6E57621E4D8106D49FB2C766094CDCB29F97;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t9570A0EC63F6315490038C67C7468004FF290423;
extern const uint32_t g_rgctx_JsonConverter_1_tC689AEB507F9C01FEC75B2D56E4F372EFCF67DD3;
extern const uint32_t g_rgctx_JsonConverter_1_tD8A4756B872EA90428C8E210BAFDD99CF1357156;
extern const uint32_t g_rgctx_JsonConverter_1_t6E9FEFBCB012AC5FA9ABEEF0E3B37B8FBA62CF37;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_t0B233067DD1C957B9DE5EDAF115831EDEDBD37A5;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_CreateCollection_mBA2F53A5E6C6F3EE929C248A1199F02DFDD8FE7B;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mA9F524EB3EA87842BEF90546229E83E44D54CCB6;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_U3COnTryReadU3Eg__ReadDictionaryKeyU7C12_0_mDA39BFA1388B915F67D50E4556A3381444CC4115;
extern const uint32_t g_rgctx_JsonConverter_1_Read_m6FBE2A8A9E48D2ACF8D9CD52B91C397C88AFD9C0;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_Add_m5F59A5F09A46AD7BF42C8C8958703B95966F83ED;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_mFC572AF140B80BBBF379FDBE9641D2D050E1E65D;
extern const uint32_t g_rgctx_JsonSerializer_ResolveMetadataForJsonObject_TisTCollection_t01A5069B8828912E75F26ED12D6A738536A2C1F1_m775E6BC0F2C2B4761BE750A8F802E8CB06CEBF1F;
extern const uint32_t g_rgctx_TCollection_t01A5069B8828912E75F26ED12D6A738536A2C1F1;
extern const uint32_t g_rgctx_TKey_t8136787C6870DB0A3BFA03E1ED59F44AD2EC2250;
extern const uint32_t g_rgctx_TValue_t3EA828DFEE924BE1D12435C6A5D9052A1F3D1277;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_ConvertCollection_mE9C68571717F5C1E779A24D274EC494E411DB04B;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t411F6E57621E4D8106D49FB2C766094CDCB29F97;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_OnWriteResume_m2C9C62E7A213E8C3B49093088FD1646167C4173D;
extern const uint32_t g_rgctx_TKey_t8136787C6870DB0A3BFA03E1ED59F44AD2EC2250;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1__ctor_m73DC9E0D5516AB68E306F43CC847788971B5B970;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m7020907E6F66C47E324F6FFCD2BECD064D8217CF;
extern const uint32_t g_rgctx_JsonConverter_1_ReadWithQuotes_mB3ABEAA48FF94CE2DB91C36A946B98DFB42ACE45;
extern const uint32_t g_rgctx_TCollection_t00F3E9098471CD4DFA6D00AB92D2D3606D3916D4;
extern const uint32_t g_rgctx_Dictionary_2_t5A8B2B1C52D9422087EC56F65BD7A5C76C674EC9;
extern const uint32_t g_rgctx_Dictionary_2_set_Item_mDA22D1FCDE62446BE9167F28F031DF4F15D150AB;
extern const uint32_t g_rgctx_Dictionary_2_GetEnumerator_mC8D73ACDBF854FADE4E706F8AF24D2E49D4FD9CE;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m8712EFE9598053CF09AD65EB79E2F3F82B3C26D9;
extern const uint32_t g_rgctx_Enumerator_t824B3A036C96D1E60A16E5B3811F06BCC965690B;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m79B2BA43705EF87265EDD2342B8A77DF602C0AD7;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_tB96119E4C1E293321B02467E95CBF03CFB65DE79;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t11EB0BCD92119A6E7BEBA25AF4DDDD35C77B54CC;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t378B48852AF248DD37E7EE23DCBDC1D804B88820;
extern const uint32_t g_rgctx_JsonConverter_1_tFBBEBB91A132D0A522BF2E9AE242B3433221AE29;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m309F600CC905E152270A4111F0DB2A8A7A93F62E;
extern const uint32_t g_rgctx_Enumerator_get_Current_mCF46EE5216E0A3F994209069762431847850F678;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m94EC3F66AF88FA93F0BEEFC8A4EC570F33464879;
extern const uint32_t g_rgctx_JsonConverter_1_t012B70B47D93C53B77F5C0C626A23C85B149749A;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_m7819569B23B85A712F80E6519D88EBDB0F00829E;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_m766E7281E22F11119EE016E66826DF8D46525DAE;
extern const uint32_t g_rgctx_JsonConverter_1_tDC3BFDC5C021AFAF2F929F0C04653DC3F02D3A4D;
extern const uint32_t g_rgctx_JsonConverter_1_Write_m3996AC2C1AEA07D6DD2A02AB7AF0C65233DB2A2D;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m3D06445A824BA544C7304C56AF1E6089B0CFA41F;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_m8C2DBB20086019BC6C68E19DFFD788BFE7018434;
extern const uint32_t g_rgctx_ICollection_1_t8457F777AA23E8681B8A14DF36D9155895D6226F;
extern const uint32_t g_rgctx_ICollection_1_Add_m22E14ED4D111238D0A232ACE41361A69813EF90A;
extern const uint32_t g_rgctx_List_1_tAA5353DE7CD7FF2EECB595BBA4A8FC5B164E4250;
extern const uint32_t g_rgctx_List_1__ctor_mD518C625F109CADA2DFEB40989FDC37E3EC18612;
extern const uint32_t g_rgctx_TCollection_tBA1190D7F9BBFD194309EF7D4B85A50B4F3DBC22;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tBA1190D7F9BBFD194309EF7D4B85A50B4F3DBC22_ICollection_1_get_IsReadOnly_m41951822E28FAE58DFB6AF69AF4B4AB3F6685801;
extern const uint32_t g_rgctx_IEnumerable_1_t996A4BE879A00F49B408811FCAF8661C16C6C9D6;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tBA1190D7F9BBFD194309EF7D4B85A50B4F3DBC22_IEnumerable_1_GetEnumerator_mBCAD29AD6C1963559C011B258E83FD8CE24CDAF2;
extern const uint32_t g_rgctx_IEnumerator_1_t5F70601CEBB4A81852B2DBCFB27DCF4228148350;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m2D93423DA4B75EC447620D18CDE60FB5A36A7760;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t39147C2A74BEB19289E90E01DEE8582D7FF3E14B;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t210B7BE31E27D478112D37C0407BD96A0468B5A2;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tA8311C863415AE360B287C326B16452FBDF05E16;
extern const uint32_t g_rgctx_JsonConverter_1_t1C24E8E4CAA1C7925CF888003CB0514C14667624;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_mA4E1144A91F20E5DCBB25657B1F621D578DBF176;
extern const uint32_t g_rgctx_JsonConverter_1_tFFE8113FF72D1F0CBE3C49E37ABEB305620F0690;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mEC6957E8B18B63048AF9EA5E03FF47AEDEA681D9;
extern const uint32_t g_rgctx_List_1_tAA5353DE7CD7FF2EECB595BBA4A8FC5B164E4250;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_mE8CA7DBBE4E0823DC1880E3043EC9220C336FE6E;
extern const uint32_t g_rgctx_TCollection_tC7D4BFCDCDA0A17D9E2A3FC15F3445E1727402D8;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tC7D4BFCDCDA0A17D9E2A3FC15F3445E1727402D8_IDictionary_get_IsReadOnly_mA50EB3929C2FFDAD6B44CCD2114D45ED688FE73A;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tC7D4BFCDCDA0A17D9E2A3FC15F3445E1727402D8_IDictionary_GetEnumerator_m755A2ADF1298A0D930C08A309E890838600B4DBA;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mED881F8A97EE020144721253D00CCF143B822DA8;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_t6394A26AA4EF32F099BCD33887504F91F4AF7E96;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t5410511662503DDA21756F05F57AB207B148ABF5;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tE4E30BF94408F27B56691BF5B114B5409A5D9C4A;
extern const uint32_t g_rgctx_JsonConverter_1_t24F66509AB3EE0B6B48B501D42452321554C99AC;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m3D2CE6DA5D3D114CD24C786C7B204E0D230DD65F;
extern const uint32_t g_rgctx_IDictionaryConverter_1_GetObjectKeyConverter_mFF121A41CE894661B5714F31103FEC8D81730F25;
extern const uint32_t g_rgctx_IDictionaryConverter_1_t7633E4DAB4E0800621A8BB4A120AFCBA73EFCDEC;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_mE6F64B58C31B60A89E305F3B10EF35DA80BA5C9A;
extern const uint32_t g_rgctx_TCollection_tBF9A2E17D407B10FADFC3B11FDE8A669DDC51B1D;
extern const uint32_t g_rgctx_IDictionary_2_t9F52DCD3E9BF7DDC6454A0DDB6BF574AB4E7C200;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tBF9A2E17D407B10FADFC3B11FDE8A669DDC51B1D_IDictionary_2_set_Item_m7269D6BF8F1071D75F5B2D55C6E462F27F01C152;
extern const uint32_t g_rgctx_Dictionary_2_tB22A2789146E8EA389B1582D6711C8F08F0ABAB7;
extern const uint32_t g_rgctx_Dictionary_2__ctor_mC1244141070D94DF0AA6B4E00BFC05962F67EEA5;
extern const uint32_t g_rgctx_ICollection_1_t85A94DC5C7444EA044235740F2EB67BD48FFE6FB;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tBF9A2E17D407B10FADFC3B11FDE8A669DDC51B1D_ICollection_1_get_IsReadOnly_m7C5B1373B6B36F7F31AA950980A9B40A1373222C;
extern const uint32_t g_rgctx_IEnumerable_1_t28A799C268393CADEF10FAE5CD73D145E0BD7946;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tBF9A2E17D407B10FADFC3B11FDE8A669DDC51B1D_IEnumerable_1_GetEnumerator_mA6A30A62695ADBAC5FD4F31F3CBFF7130C6E3645;
extern const uint32_t g_rgctx_IEnumerator_1_t8F9F0717EC707556D64042F88A7FD0CB26439FF4;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m5A03A5C352401828BD751A5D1A3B24ABB8CA602E;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_t67A0FBEEFAA1D791F67E5DF7283606B33B1CC67A;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t17E676BA48F3BE08DB4C492F3373CAB36DC1FE95;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t583ABED96FDA8A3F5EEB76BE38E519A82B0D1B8D;
extern const uint32_t g_rgctx_JsonConverter_1_tC62BA61041A4E29E577402DD07F9DBE91CED186F;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mE419B3FCF7CA1452FEAD5BD21E760FF5E76D528A;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_mF8157C9A096A1427B0F05E4F5CBDDDBCB2F8B243;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m13DE05489B55F2FD6B9A88ACAE791ABF5806CD73;
extern const uint32_t g_rgctx_JsonConverter_1_tCEECF0FE67B242244A3C1C85103014CB8F5EFDCD;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_m5D0F1ABF170B4EC5AEABAAC246C249BA77CD2ECD;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_mF7602B57B11AAB3CB2CC3FFA22A5E69E1DE403EC;
extern const uint32_t g_rgctx_JsonConverter_1_t0D63BA06435AE8F83AD42176B691DE885CE8788A;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mBBF7EB3DF87F7AECDA6C0FE5A2A9E23231A8EE63;
extern const uint32_t g_rgctx_Dictionary_2_t0E2646A5C68B4B94CD6D7BDB97795B5EC3FE9864;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_m1FA0DD8D1A2A812CD30F99E4B5F777E124A54BF6;
extern const uint32_t g_rgctx_TCollection_t003D1EF6147C89C48A4490B3B2D54CA13A29F303;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t003D1EF6147C89C48A4490B3B2D54CA13A29F303_IEnumerable_GetEnumerator_m4265B954378E12AC1B6D9F21CE6B373F423C53A0;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m7CB7D24EB688D3EA01A2DA7B4E05CB44D66A0D0B;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t10EC588DEB4E07B93AC7D5EBE29FA9290FF2BAAB;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t8C3189FFB80E592C4CDB836FF79A208004E3976D;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t33BA5B702B0900D6A4B2FBDC3735E02D5A83976B;
extern const uint32_t g_rgctx_JsonConverter_1_t87B4D7E4BED6B27B976D0006C2C5F0D82873F853;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m78BC3C2CFE853B9BB2CACB557A922613899FB038;
extern const uint32_t g_rgctx_JsonConverter_1_t65AA2F2D1FCE03437C1D9DAAFCB14565B0C92F5B;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t3137B948230D1792CF1A778C03BA00C98AD9CF7B;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_CreateCollection_mE7B8161F7D43E56958BCC33A1F95E6D87C74EEF5;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mC5599F38A4AD35E82350B19C19C0C5F5C5D3DED9;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t3137B948230D1792CF1A778C03BA00C98AD9CF7B;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t5C8BF742C383F0411A5B63C5E6D9941215E96CB1;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t6F4C3545859325214F40D6BD7AD005C202EAF265;
extern const uint32_t g_rgctx_JsonConverter_1_tB96431DA83EB2DB8DFEA7C6903FFECF10D846BC4;
extern const uint32_t g_rgctx_JsonConverter_1_Read_mA45FBD76C05849B7694EDC38BF46519FCB9F9580;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_Add_mD01C549B2DA9DF97C1BB1D5A399B926F331A8B7B;
extern const uint32_t g_rgctx_TElement_tAE40820C5AE75965D808D8E9E2BAC2969B1F7B42;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_m5AA471A97ED2A6FFDAFBE6B028FDCF1917B1ED53;
extern const uint32_t g_rgctx_JsonSerializer_ResolveMetadataForJsonArray_TisTCollection_t4649B0D01A80384F08E5491E881819EF7E300CAA_m402B75507E80715ACD1737C9F746614DE26BFC7C;
extern const uint32_t g_rgctx_TCollection_t4649B0D01A80384F08E5491E881819EF7E300CAA;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_ConvertCollection_mFB39A14A746095CA88D64A5F0FF108502641342F;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_OnWriteResume_m271C3229FD80D21A8A19F14CA2236E584CEA3D16;
extern const uint32_t g_rgctx_JsonCollectionConverter_2__ctor_m09CD36F410E956CB27D5A7BC00ED47E1930257F2;
extern const uint32_t g_rgctx_List_1_tA14A7F95BAB023CDB60B1C895A22D8DD2EFE9348;
extern const uint32_t g_rgctx_List_1_Add_mF1C053D8605FF1236A2E0429FBF95EDD0C32A816;
extern const uint32_t g_rgctx_List_1__ctor_mEDF7F0FDE60691F1B45FD2A54CE01F40F8596B72;
extern const uint32_t g_rgctx_TCollection_t6387D2966E86FEB8FB32D13D1B985CDEE26186C6;
extern const uint32_t g_rgctx_IEnumerable_1_tE08EAF36AAEF1706B2364896D8542403BCC38926;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t6387D2966E86FEB8FB32D13D1B985CDEE26186C6_IEnumerable_1_GetEnumerator_mC7116D264B064F4C1E6FF867548F7C9AA2A73524;
extern const uint32_t g_rgctx_IEnumerator_1_t23BA32096121310AF36E129F4C9BCCE8393B0FEB;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m974F289AFA4D32DFACC66BA8CCC3F1E798458D93;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t32D1872A599CAC6093C8ED4A3A826DD7BFC083CC;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tB0F259EB2D562EFADC7B0B90FA3EAB6E1C2EB150;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tD51CD663AA1CA91D54138D6710E3566921149648;
extern const uint32_t g_rgctx_JsonConverter_1_t1F2074DD72E8A9236011CBA194D8DD4B89D3CCA1;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m2DC4A5447318280BF044863E5E2979A1ECC7E695;
extern const uint32_t g_rgctx_JsonConverter_1_t9A3989FE0E2B0915B8C95BD23743F2D4908F3C68;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mE9BD0A83194E1A450E702DEDCAD8C3E6BE3301F9;
extern const uint32_t g_rgctx_List_1_tA14A7F95BAB023CDB60B1C895A22D8DD2EFE9348;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m952FA8204B3CE7EAC955B1C49D21818059447550;
extern const uint32_t g_rgctx_Action_2_tEA2EEE1BC71F5F22A6F2AC4EF8C18DCD4F701323;
extern const uint32_t g_rgctx_TCollection_t43259AAF7B7F2D7FC665F2AEBC1CF00CE5DDEF95;
extern const uint32_t g_rgctx_Action_2_Invoke_mFF3FF23F6456EDBE545401A2760210849AF1EC04;
extern const uint32_t g_rgctx_MemberAccessor_CreateAddMethodDelegate_TisTCollection_t43259AAF7B7F2D7FC665F2AEBC1CF00CE5DDEF95_m0DC9388004B3F69B2AF72D86CD548A7E95D5ADCE;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t43259AAF7B7F2D7FC665F2AEBC1CF00CE5DDEF95_IEnumerable_GetEnumerator_m4265B954378E12AC1B6D9F21CE6B373F423C53A0;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m7AB49AC60F2AA8B4A02E06FF5A5AAFEE3A979FFB;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t72E695A32B171E46671B8AA847D756441DD651F7;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t956117A4186A74CDB064BC63E12D80850A608CB3;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tF5064B0FD66CB0CB67DABF05EF20CD44D32F31C3;
extern const uint32_t g_rgctx_JsonConverter_1_t2A909D65701BE89691C7C0FC97616F29C952CDC7;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m3E19E663355C74EB56E4A10053082FBAD57E06D5;
extern const uint32_t g_rgctx_TCollection_t196DBCB56680D17F2CE93490AA59F5E05411E547;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t196DBCB56680D17F2CE93490AA59F5E05411E547_IList_get_IsReadOnly_mE5E5B403BB8FF044EE55F0972CD3221ADF935C27;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mD710D682C079501D9D88C957CD5F467059D8ECCA;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tA5A58EE16639EFD3FEDB976EF90FB5D983C1D87A;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tC2D343C754D5D9862810D76C4844CA991239100A;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tD6178E5FCF5B7254E678E0D850D4FDD00310D0CC;
extern const uint32_t g_rgctx_JsonConverter_1_t9268BF8E2ABD95390D36EDE4E0A4A0540572E581;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m7A6FFEB7486ADB4586A6496F00995C9E122EC870;
extern const uint32_t g_rgctx_TCollection_t0A1D11095EC604F0F067CCE703C82AC9009771C4;
extern const uint32_t g_rgctx_ICollection_1_tF79A626C69A179047CDB18F9877D3235988C62E5;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t0A1D11095EC604F0F067CCE703C82AC9009771C4_ICollection_1_Add_m3ED89D0469A4A4B04CCB7AA12770B4ACDAE6947E;
extern const uint32_t g_rgctx_List_1_tA2DB79D1E93E292E66768239C04CBF7A01DF0346;
extern const uint32_t g_rgctx_List_1__ctor_mBCCBA87E642E31E4A547082A50B9B77896083B28;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t0A1D11095EC604F0F067CCE703C82AC9009771C4_ICollection_1_get_IsReadOnly_m1AAC5133ECF33AF4569C7BD6E5DBCFEADA8C0E70;
extern const uint32_t g_rgctx_IEnumerable_1_t4A46546D43DFB82971C770B239D35015A8B55088;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t0A1D11095EC604F0F067CCE703C82AC9009771C4_IEnumerable_1_GetEnumerator_mD6BC2B16E4309929D7F8C95E8B76EC35E7E0B69B;
extern const uint32_t g_rgctx_IEnumerator_1_t28492F9F7500839BD270E52E447FA85856BBB5DA;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m210A8CE950129F28D67D585366F43680CD6C5D02;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tB499838FDC358B7C026D8DF004CD33BDA847B245;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t3375DE57CC6783B581913AF25DFE29357858B7CF;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tA8692C6FE7A5209F93652C35E50439B93BE894E4;
extern const uint32_t g_rgctx_JsonConverter_1_tEA8D332A916544953122311A62117798B96832DD;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m335ED65CE1CB395EEE08D11B42C45155BE6F908F;
extern const uint32_t g_rgctx_JsonConverter_1_t2C0B69C552D048AB00F919CE6988A0094C2990B2;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m56E1334F9F2103183D224F04615A84D683C257DB;
extern const uint32_t g_rgctx_List_1_tA2DB79D1E93E292E66768239C04CBF7A01DF0346;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_mBC3E6E8CC1502808CF37B21CA1E12388BB7F03E3;
extern const uint32_t g_rgctx_Dictionary_2_tAAB501F742C2A685A3496816666C712035DA0B3D;
extern const uint32_t g_rgctx_Dictionary_2_set_Item_m8199D7E45565E35C582A76B8F095722FB4203D90;
extern const uint32_t g_rgctx_Dictionary_2_tC746A775C8E57B921977C9B5B8CA7F8B40699FCD;
extern const uint32_t g_rgctx_Dictionary_2__ctor_m1B623EBBCFE59645641E45B70CD05B15586851D9;
extern const uint32_t g_rgctx_Func_2_t494E325510E2E6475ED02E9D6145340A86D37347;
extern const uint32_t g_rgctx_MemberAccessor_CreateImmutableDictionaryCreateRangeDelegate_TisTValue_tBD514929FB6FFFF05CA8F7409C1D0115A3598758_TisTCollection_t2A6E13736A792EF659D2EAE6B70286E1276ACE93_m97EF539F10019DC6334E0C40166CA20E8091F79B;
extern const uint32_t g_rgctx_Func_2_Invoke_m495B3E76A7AC0FBAC6D2DA91ED505DFB2964208C;
extern const uint32_t g_rgctx_TCollection_t2A6E13736A792EF659D2EAE6B70286E1276ACE93;
extern const uint32_t g_rgctx_IEnumerable_1_t6CB5825CF181AA6D011E40BDFEA0704A001F1484;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t2A6E13736A792EF659D2EAE6B70286E1276ACE93_IEnumerable_1_GetEnumerator_mE6376A344B9778051E01EAE759475AB17BEE51D9;
extern const uint32_t g_rgctx_IEnumerator_1_tDB9D0B5DDC2E9D306494CBC27465231DC36DD82C;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m9F96943B63CF2A09DD169E5B773F3492FFC72838;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_tE81F3695013B6F8F466119981A45CFD0F74FCCD2;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t5B9D0D7E6E4C1D73A15F63D139A69BC97385FD24;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t32C43716ED2596E9D3C0371C6D0E46D4A629E67E;
extern const uint32_t g_rgctx_JsonConverter_1_tA6B951BEA5574815E8020D7FDA02B2ED2AF1411C;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mCF23B2FE9F7C6D7696730FC424A8E1491A51734B;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m152375CF2A0416C1D8133B86C6623ACB29D97423;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m168E3CA8F1EFA2741FFA01C3F76CDA14394D78CE;
extern const uint32_t g_rgctx_JsonConverter_1_tEAA8A18BF99BD4E5484FCF19DA844DCD91333CE2;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_mFE51C560F721087D75B4457888E64CD331160013;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_m415867837EFA11E21572234B43AF3A1C3D8CE10F;
extern const uint32_t g_rgctx_JsonConverter_1_tC9484DD1CE90228C5843FC79F57B90D0160E2FFE;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m58FE86FA41377BD5B4753252262F4569E099808D;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_m7EE7AF51DB882B901D1E290F39D438C31DDC1564;
extern const uint32_t g_rgctx_List_1_t5D1572D9A71176FD0448E876A05A764F119F374F;
extern const uint32_t g_rgctx_List_1_Add_mAB0AE047073F99A97CB68262EBD9B53F239BC406;
extern const uint32_t g_rgctx_List_1__ctor_m0BED04D204467AE7D1FC72C99351282985F99B18;
extern const uint32_t g_rgctx_Func_2_tC0E9B465A9C0A7A88C553328372FED49B5417AAE;
extern const uint32_t g_rgctx_MemberAccessor_CreateImmutableEnumerableCreateRangeDelegate_TisTElement_tF846BA1E8EAAB226B8831BFD36E4558004EBBA3F_TisTCollection_t0554E18FC0B2071F4E9937AEDB30783BF05A1352_mB6CA2E69F98EDC360718408EFEFD36F7EAB1771F;
extern const uint32_t g_rgctx_Func_2_Invoke_m03F8603938F5B283F6C62F96A0E8F073FB19840E;
extern const uint32_t g_rgctx_TCollection_t0554E18FC0B2071F4E9937AEDB30783BF05A1352;
extern const uint32_t g_rgctx_IEnumerable_1_t4422373607392BA2654E7C027041337FD44C85D0;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t0554E18FC0B2071F4E9937AEDB30783BF05A1352_IEnumerable_1_GetEnumerator_mA1B5D9D00E74839C1737B3073F3D8DECCF29F8BC;
extern const uint32_t g_rgctx_IEnumerator_1_tF58BB06E6C81DE1FBEC13E79ADF0207022FDDDF9;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mCF14C03F8C3615D2370985BED1D146BB04F19FD7;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t531C23F3B5FAD310A68535743B992E87419D4741;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t9BFE796E6EE4DCB83DE16B7AD27B2FDFC3E48C59;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tD5C348E7BC136F90AB3B3626B0944E9DB2EAC1C6;
extern const uint32_t g_rgctx_JsonConverter_1_t9442173F012FB57EF64A352E53C5BAB1CEB9BC8C;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m075C3E3E228A66821C454E40A0B0A1164C7663D6;
extern const uint32_t g_rgctx_JsonConverter_1_tBEF782DC199F580BC088E82ABE643DC38DA34C91;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mE71E461EE4733CCF66C388AA16D14B071443C1BF;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m8179ED063811F141A23B8C0E85EC28B9B4530DE7;
extern const uint32_t g_rgctx_Dictionary_2_t638A56AA014FFED92A57CC3EDCF319E4C4037B69;
extern const uint32_t g_rgctx_Dictionary_2_set_Item_m036D8C14BF7B639AF1B37C60E30AD1B386BBBEC0;
extern const uint32_t g_rgctx_Dictionary_2_tE9C1241028924D1D18C9F4F8647F876F162E67D6;
extern const uint32_t g_rgctx_Dictionary_2__ctor_m4099E953375307210F006F0DC36C8355BEEA5679;
extern const uint32_t g_rgctx_TCollection_t85EDCA0BB5D949125B066DF7430ED54D58070F1D;
extern const uint32_t g_rgctx_IEnumerable_1_tF6B2F07F4D96912C97D169B7F871D12FE16F692A;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_t85EDCA0BB5D949125B066DF7430ED54D58070F1D_IEnumerable_1_GetEnumerator_m9DD9793A24A75647845306876E139DA5D899BF1C;
extern const uint32_t g_rgctx_Enumerator_t6749DB4C43CFA585A0227C2C62B5A240A947E748;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_mEF66ED2D37C5C7CD35401C1E10F023B7A3ABAD6B;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_t30D9BE46E61426EC0C1BE1055F03AF6FA6655B64;
extern const uint32_t g_rgctx_JsonDictionaryConverter_1_t2433F18E25510FDBFFECF2BE4AE4CE56D41D6F0D;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t26641F6C47C365E800AF99623399B12BF453ABFB;
extern const uint32_t g_rgctx_JsonConverter_1_t2C7E7AEB3B84C8AE016338FA917A908EB5B6D1E2;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mD351E0117A9B6AEC88A9E5E122AC0512A17A7F1A;
extern const uint32_t g_rgctx_IEnumerator_1_t5D8B7C0C162CA96CCCB69D9D6D1A3A61C4EE29BC;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m74CE3D19644EB75A7147B4100DE13698B9BB4883;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m3143F3202268ACC42038D3825658C8CE04D628C1;
extern const uint32_t g_rgctx_JsonConverter_1_tCEFEF018CB413AFB86879AD5DCE13A116B7CB898;
extern const uint32_t g_rgctx_JsonConverter_1_WriteWithQuotes_mE2538DA6762D1A1B69C47DB2147247713077283C;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_m2C2CE39AEB10F06BEBF94DA0EB0B50C0E3D2CF12;
extern const uint32_t g_rgctx_JsonConverter_1_t502DA0340B50DC12E1E34332FC07536780760F00;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m6116E76446C074A069B8D722C963BA69470382CA;
extern const uint32_t g_rgctx_Dictionary_2_t638A56AA014FFED92A57CC3EDCF319E4C4037B69;
extern const uint32_t g_rgctx_DictionaryDefaultConverter_3__ctor_m6C2717C866ED8D50C4692178E87AC31A1BC4C675;
extern const uint32_t g_rgctx_TCollection_tD6E60897C2E5EE5150D61B275634107E7D6519DC;
extern const uint32_t g_rgctx_ISet_1_tD29CF3D02013A2312240271D0A50CA61D96511EF;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tD6E60897C2E5EE5150D61B275634107E7D6519DC_ISet_1_Add_m21BC5B710529AF709AA4AFE37DA3ACEC5A260784;
extern const uint32_t g_rgctx_HashSet_1_tF6AD039CEDAC33162F2F6F050099B405E014A1BE;
extern const uint32_t g_rgctx_HashSet_1__ctor_m499D51C3974733EE9788E5E81307F608EC1C2D8F;
extern const uint32_t g_rgctx_ICollection_1_t1E4D52D5C8AD3C3CCA94A97993A7297CB636D805;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tD6E60897C2E5EE5150D61B275634107E7D6519DC_ICollection_1_get_IsReadOnly_m7C4F322CF7F9D4D14874BD0E59A8F9E4B1DBEC4C;
extern const uint32_t g_rgctx_IEnumerable_1_t6E7F4BB0C6EA941B221D9D38FF5BAEDB4B0135AC;
extern const Il2CppRGCTXConstrainedData g_rgctx_TCollection_tD6E60897C2E5EE5150D61B275634107E7D6519DC_IEnumerable_1_GetEnumerator_mC5F7DCF20603B9F6485F6409275015BD5E5D8755;
extern const uint32_t g_rgctx_IEnumerator_1_t89ED099CAF3218F3C5AD972FF27DF2D874A28E50;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mB9BD3383165441071E2A61FC742ADBD0BE9FD8F0;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tC0FD6306BC9AF3BB87B083DB15E7E617BA5F1959;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t380217BD52DE6CF525DD4799ACA428E29B96D7BE;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t1B9CFD5CEA125FFD3A613F9EB3C8185BED9AFF45;
extern const uint32_t g_rgctx_JsonConverter_1_tCE3E6814D0EDC56C095F25D3EA6760D1960A3DF4;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_mD85285D23743F2E0600A9A46B06B915B0D8C8148;
extern const uint32_t g_rgctx_JsonConverter_1_tF9641FBD3DB1D0E9835835C8DAB46BC85FC4D4B8;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m59F206B4ECE012EC150EF33B7D4BE0D3E6E59B07;
extern const uint32_t g_rgctx_HashSet_1_tF6AD039CEDAC33162F2F6F050099B405E014A1BE;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_mD7F8D62D9121D2FAD67D434990FCC1412F49605B;
extern const uint32_t g_rgctx_TCollection_t9F22616172873F2FEE9AC40B7E581BD5BB6D4572;
extern const uint32_t g_rgctx_List_1_tECE03481F6879E14B4ECFFFF8FE3198B3C081754;
extern const uint32_t g_rgctx_List_1_Add_mB9A41C9D43AC0738FBDB3C22D7657FB761E142A9;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m199C0B0B142DC6E9B2884E4CE1E45F081EF69ACD;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_t0042356FA6FE60226FC4F8B185AB1CAB35067176;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tCBADD8DD43F0D2F3A38A1B141109BA8472F7AC3C;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t729917A0DC21999EC426912B63EDC52F576F7074;
extern const uint32_t g_rgctx_JsonConverter_1_t5FBC055A1297B78CA362E5FD869EF078D3877F76;
extern const uint32_t g_rgctx_List_1_get_Item_mB1D8600B2BC8D53FF10F922836BEBF6B19C92C0C;
extern const uint32_t g_rgctx_JsonConverter_1_t91EEC4EA0B80EC56F1199BAA160B2A2AB088639A;
extern const uint32_t g_rgctx_JsonConverter_1_Write_mDCB1CBC0E30CFF17793220CC16B8C352924CC168;
extern const uint32_t g_rgctx_List_1_get_Count_mA08BCF11C5BDBCC67A9E447D5A3D4C35034D05CF;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_m5C109B0BFFBA70E18BBB76A1B733CEBF4F6DA776;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m84E7D7DC2CE9A78B7CEDC43B2FA6FBE6AAA63A74;
extern const uint32_t g_rgctx_TCollection_t64C6B76B8C95C904FB42CE5E1C670AE705668EDF;
extern const uint32_t g_rgctx_Queue_1_t83CC3F01E545ABE139E14EDDF944C381E1505AEF;
extern const uint32_t g_rgctx_Queue_1_Enqueue_m8B7A36493528BF7811D431211139D1CAC141480D;
extern const uint32_t g_rgctx_Queue_1_GetEnumerator_mD059066BA634C08A50F614094F2D275D88B2C787;
extern const uint32_t g_rgctx_Enumerator_t98FC87963C4BAFBE32153D5A71FABD97FA35AF6F;
extern const uint32_t g_rgctx_IEnumerator_1_tFA006739BD733776A3DE6F7E393F22303F5A4DFE;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m4200CE553B11E0096A7DC7548ED3F59B2D5854B2;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tA485C12F29EFB8A69C6E24C2765EAD8BA16B0905;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_tF0DD0A08037C986ACE0F9A90674A5DBDF8A4D0BA;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tAD2D489BE3D3C14360943EC80614ED81962D6F1A;
extern const uint32_t g_rgctx_JsonConverter_1_t84A8A4EA23DE3B797EDE16202AB392A73FDB2A0E;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m9D69E9749A649534477D2FC46DBEA6506BBAB014;
extern const uint32_t g_rgctx_JsonConverter_1_t5360C6C8E8270796A750A26020C2972C2436F058;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mB570B3067BFEAFD47479EC94360BBEBB730AAE7F;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m1F620BFDE99754F2220A932E49DCF1B30FE25233;
extern const uint32_t g_rgctx_TCollection_tB9FD01C8EDFE9F5FF51206A2E5612D1BE6E6BB70;
extern const uint32_t g_rgctx_Stack_1_t39925D474A0105731F5C7B3161F716C78B3CF5F4;
extern const uint32_t g_rgctx_Stack_1_Push_m7CA75794C041550C066584EC81934B943B654F82;
extern const uint32_t g_rgctx_Stack_1_GetEnumerator_mC7096A05E7068BA640EF1C1BB41C0BF66076EBA0;
extern const uint32_t g_rgctx_Enumerator_t605551D76237842039674591C83620BA1ADA7FAD;
extern const uint32_t g_rgctx_IEnumerator_1_tA082880A2FCAA3F4BB69258C447E1242479A3A79;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m39820F235E6DC01F8991A7194DC864C0CDF24ADB;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2_tC24DF6AE1930A1A043DBA0E0D40099A876EEA65C;
extern const uint32_t g_rgctx_JsonCollectionConverter_2_t832CA1C01D112791027653C59FEEB492069E5045;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tB293D531C938D23BB24ECFD5A67EFF63D530E129;
extern const uint32_t g_rgctx_JsonConverter_1_t7DEFF1D5FF0D57D4221EC52291DD5289498CAFEB;
extern const uint32_t g_rgctx_IEnumerator_1_get_Current_m40A65C1E4B6189B89DA99642B8DD91133DAE2E29;
extern const uint32_t g_rgctx_JsonConverter_1_t5FC6F80CD6267D7C966E7409695BF38868329E85;
extern const uint32_t g_rgctx_JsonConverter_1_TryWrite_mD422CCE9A785691AFEBE1D23442262883DFD3422;
extern const uint32_t g_rgctx_IEnumerableDefaultConverter_2__ctor_m4D4C62293EED172F0A50EC8925F10246490AD552;
extern const uint32_t g_rgctx_KeyValuePairConverter_2_t64BBA9ED601006E9587D301CC9FF332AC272E3D0;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_tC86B454F51DB78B3AF391EF635AD82FC8C83AA2C;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_t4D48D99915BF1CF9457076C0F3594ED720FB2F83;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_t74932E33DD562EF701F2B106CB315954362576BD;
extern const uint32_t g_rgctx_JsonObjectConverter_1_t2CC9C20D8C73855F215AA53107D5011658F827CF;
extern const uint32_t g_rgctx_JsonResumableConverter_1_tA5924CF75F3C90C66E692EAA61010B48A7D20C8E;
extern const uint32_t g_rgctx_JsonConverter_1_t7A3E0841593E6638ABC7FA12589B77AC4347728B;
extern const uint32_t g_rgctx_KeyValuePairConverter_2_FoundKeyProperty_m8D0C1953590742CFFF8B5E74A8E41BB71003CF8E;
extern const uint32_t g_rgctx_KeyValuePairConverter_2_FoundValueProperty_m47BFA6BD8A1EA1213E2AFC58B37519EDCC6C128E;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5__ctor_m14F775A10E479B67F97A33431593677E438217C6;
extern const uint32_t g_rgctx_KeyValuePair_2_tA4BCFAF72105A91AE0A02D4020A50AEDE461FEAC;
extern const uint32_t g_rgctx_TKey_tA382EC4F70B9BAC96939A56B27ACF750D2C1DA77;
extern const uint32_t g_rgctx_TValue_tB0CA706FFD088F33C0203AEE25ED0C87A6112FA3;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_ReadPropertyValue_m7F55EE2ADF51299B87EC435C3F0FBC19747FDE88;
extern const uint32_t g_rgctx_JsonSerializer_ResolveMetadataForJsonObject_TisT_t287FE2F6E60E6C743EE88E99504E5792202E6C24_m374D6DC3C8B4F505B9C5AF180116D8EE50FDA1FF;
extern const uint32_t g_rgctx_T_t287FE2F6E60E6C743EE88E99504E5792202E6C24;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_ReadAheadPropertyValue_m6B948A23693C5F0A36B42605AA1BA9A9AA7F7E76;
extern const uint32_t g_rgctx_JsonObjectConverter_1__ctor_mA1BF7BE3DA160847FF34C293A0D514297C48AE36;
extern const uint32_t g_rgctx_JsonObjectConverter_1_t0914FB6C256E4424A629A1FA63728CC0E9C1068F;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t310EF14609C149DA5258FF1B266A81C70DB54D86;
extern const uint32_t g_rgctx_JsonConverter_1_t305B26107A8E9B03D4BC8E2884A91C73E2604C1E;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadConstructorArguments_m3227484126AEB8ADE914E61BE97B7E9B98EF05ED;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_tEEED1475354D3C24BCCD800252395F9A319692F3;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_CreateObject_m20683E353A666AC0E7F42D580D970DB513B97868;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_ReadPropertyValue_mE470E259A9CBCF3BD8D534D78A9EE0B035917B78;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_BeginRead_mDDA6496E75011CB360B3BA729DE570C65AC202F9;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadConstructorArgumentsWithContinuation_mDAE696C0C64A0F4B693AF18EBCC30E9E05A9BEF6;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_EndRead_m8F29F7BE5EB6D7A9F9D532F395FF7AFAC2321A99;
extern const uint32_t g_rgctx_T_t1616A25A7065F35DE236E47F4557B35630CD5FEC;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_TryLookupConstructorParameter_m7AF2E6099E67A449216433F18292924A1DFCF275;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadAndCacheConstructorArgument_mDB7358FC8287C952670637C41193462E51C5EC7C;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_HandleConstructorArgumentWithContinuation_mD77A7CF84556A99EE07C2CA9E9C6C72AC450344D;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_HandlePropertyWithContinuation_mF84FC527DA15C2FB30A6B7C189EE413EBA1BEEB4;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_ReadAheadPropertyValue_m50FC7C64BE768407017AB9CBE553BF53412118FB;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_InitializeConstructorArgumentCaches_m1487F570CFF5A17AF1C48E21132A379343CA6B52;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1__ctor_m239B9E2401ED47864E70924C37950BBAA9289700;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_tFEEC4314277435DC85DA3FF86E95E280343509FA;
extern const uint32_t g_rgctx_JsonObjectConverter_1_t7B8837C27FAD8B8CB33748948EB2C0F42DF1B9A9;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t51B8AD548D60AEA1F96F3823C3D77F45EAC6F593;
extern const uint32_t g_rgctx_JsonConverter_1_t6833039C608745FC2E16F6F3F164CE0C7922EE8B;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_1_tA3985362697AE32AE8A01166E78D6BF60CD9BA1C;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_1_Invoke_m3176AEA7AA114F9D555BF1BD47402CD3BF405DBC;
extern const uint32_t g_rgctx_T_t9912E5B8B30854DBB260C86458564E60DFF5353D;
extern const uint32_t g_rgctx_MemberAccessor_CreateParameterizedConstructor_TisT_t9912E5B8B30854DBB260C86458564E60DFF5353D_m00AC1577F211EF1E38DD10E070DCD3FDE473F00F;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1__ctor_m3D1A63C10E9E35908BB1A4A368E05C1D1441426A;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_t98933288273CF60EB264D350860416ED17F47851;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_t34C9390A3381C79D2DB4D72654131DD6D592CCF6;
extern const uint32_t g_rgctx_JsonObjectConverter_1_t09F60F0B81B861DB28F3CD218D0F415A7A75ABA9;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t073EEFE898DEDC320046E92A75B8A70946C3A6B7;
extern const uint32_t g_rgctx_JsonConverter_1_t2A0202B428AE9BC3A82EF5720050E87AE070D74A;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_5_t8DD5C8DECBC93AB24B173F8773B3257AEEAF4B46;
extern const uint32_t g_rgctx_Arguments_4_t4A9E7135DE4FA467371AC9511AD324D226BC5F50;
extern const uint32_t g_rgctx_ParameterizedConstructorDelegate_5_Invoke_mFCA4425F3ED06F610D3A08C6EAC4AE47056ACE25;
extern const uint32_t g_rgctx_T_tB4D207CEE30F0E0957F76ACB6B58CD93730F0B49;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg0_tD7FFD5267526E81889EDB6BF6FDD94BFB75FEE87_m25334347723E5C57859F611A05D64F4F4E2272DA;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg1_tED9AF6DF042763F72683B4F96257CE07487815AB_mFB23737E2E7FBB4609495DB5987D3CE0674E93E7;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg2_t77D49D4685249DB962025324196F747B729724BB_m273D89A0CA57593F3E296360A46783F1E23EEB01;
extern const uint32_t g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg3_tC7D5360EECB8A558575B71674EA3F6CEF614564B_m0A1FA4063407383E5174734BA8BC10176E028997;
extern const uint32_t g_rgctx_MemberAccessor_CreateParameterizedConstructor_TisT_tB4D207CEE30F0E0957F76ACB6B58CD93730F0B49_TisTArg0_tD7FFD5267526E81889EDB6BF6FDD94BFB75FEE87_TisTArg1_tED9AF6DF042763F72683B4F96257CE07487815AB_TisTArg2_t77D49D4685249DB962025324196F747B729724BB_TisTArg3_tC7D5360EECB8A558575B71674EA3F6CEF614564B_m3F9C901AB15DF365A77366755F49DD74499943B9;
extern const uint32_t g_rgctx_Arguments_4__ctor_mA878CDA8F562E298F30BFE3B135286D79B70B0FB;
extern const uint32_t g_rgctx_JsonParameterInfo_1_tB18562E746B25972727D45A8ECE543FCC787AD37;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m9EB504BF8B4CABEDB30440DE9325501EB2D4F04D;
extern const uint32_t g_rgctx_JsonParameterInfo_1_tBA750EE03AD441C53566A4E4F78794576B0F1683;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_mB7270495EA1BB0F905716F176FF6F1641791FDF6;
extern const uint32_t g_rgctx_JsonParameterInfo_1_t2152656B119EA0478DB869E987E967635AC89BC6;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m21BDDA4B1C5C8F62C6AA1E0AA1E1CAE2A67A19FE;
extern const uint32_t g_rgctx_JsonParameterInfo_1_t6FA67072B13831CBB2D52A277D7ABAA8C510F626;
extern const uint32_t g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m6A6F94EB8059757F484F2FEAB77499490AEA17B0;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1__ctor_m84B3063A12641C309B3B15AAED1444D755963983;
extern const uint32_t g_rgctx_ObjectWithParameterizedConstructorConverter_1_t16E2ED436AE1310A6A69F9011B2588D8DB266150;
extern const uint32_t g_rgctx_ObjectDefaultConverter_1_tD6B1377AD0F46A96CBAD452164E88D75FF9FAA95;
extern const uint32_t g_rgctx_JsonObjectConverter_1_tF7EB076CD2962153D7020A22B7CA8D7804681DDE;
extern const uint32_t g_rgctx_JsonResumableConverter_1_t43B2EAB31E52FA42998361FD0DD81A2248F34609;
extern const uint32_t g_rgctx_JsonConverter_1_t65F98290BB99759B5EA0A76A80884B6705303085;
extern const uint32_t g_rgctx_JsonParameterInfo_1_tEBAC5C927B94147B5A0F7688397F21E310DE6748;
extern const uint32_t g_rgctx_JsonConverter_1_t3975D446C7B759AD1B7D3E4B2E8E741A879AC17F;
extern const uint32_t g_rgctx_JsonConverter_1_TryRead_m027E3154D4F1EC35DCBFADDB9503F4BDA0A0632A;
extern const uint32_t g_rgctx_TArg_tABDBF2D60A145C630EC22349095CF36BCA2A8CDB;
extern const uint32_t g_rgctx_EnumConverter_1__ctor_m281E7F806D250781D1F10A6F1315679C7C811E9A;
extern const uint32_t g_rgctx_JsonConverter_1__ctor_m57BC55941E8FA115AB9BC6537D85ECE65BB5BF5C;
extern const uint32_t g_rgctx_JsonConverter_1_tFA8D936269CB4A76EC777061003CF7CEBD47D860;
extern const uint32_t g_rgctx_T_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46;
extern const uint32_t g_rgctx_EnumConverter_1_ConvertToUInt64_m26ABDC57FE53EAFBCDB92980543381D60E01A22F;
extern const uint32_t g_rgctx_EnumConverter_1_t3FA6695C8FDBFC1B036271AE0566957C32356834;
extern const uint32_t g_rgctx_EnumConverter_1_FormatEnumValue_m78732064642FC15F876C5057F74B0C6F70ED26AB;
extern const uint32_t g_rgctx_JsonConverter_1_tFA8D936269CB4A76EC777061003CF7CEBD47D860;
extern const uint32_t g_rgctx_JsonConverter_1_ReadWithQuotes_m4E30C5357E7E6986C743D1989232D8B0789836A1;
extern const Il2CppRGCTXConstrainedData g_rgctx_T_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F;
extern const uint32_t g_rgctx_EnumConverter_1_IsValidIdentifier_mCCDC485715150081D3F8ACBAF0D2EA3FD365B63E;
extern const uint32_t g_rgctx_EnumConverter_1_FormatEnumValueToString_m073420713EC426EDFBDB889446F12F68F09B6AFE;
extern const uint32_t g_rgctx_Enum_TryParse_TisT_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46_m1D8EE4537A405DF61F2EC977F382E22BF7853FB3;
extern const uint32_t g_rgctx_Enum_TryParse_TisT_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46_m6438782F4E9530066835954A2ED4FB270F85E8C4;
extern const uint32_t g_rgctx_T_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46;
extern const uint32_t g_rgctx_JsonConverter_1__ctor_m9F047AF4220A2E102CBB23A0F46C13194539ADB4;
extern const uint32_t g_rgctx_JsonConverter_1_t60424EEEA9AA40D4E10081DA7231C0DEA59CA78A;
extern const uint32_t g_rgctx_T_t208D4602FC467225F066F1EF59C89104811E313B;
extern const uint32_t g_rgctx_JsonConverter_1_tD7DDC1631442B3D97CD43D2B37448BA11F2EE1F1;
extern const uint32_t g_rgctx_JsonConverter_1_Read_m807899C16CD5715C134BB67F1C3B052CD33E5E98;
extern const uint32_t g_rgctx_Nullable_1_t5F08EED533FF4AE4B1C235CC4EF3BC93A41EFF4D;
extern const uint32_t g_rgctx_Nullable_1__ctor_mCB2082DD9053F4C2E6642BB7A6F331657469EC70;
extern const uint32_t g_rgctx_Nullable_1_get_HasValue_mD87D4B638EA8F7BDAB3BA85AF53BD2EE80DC44DD;
extern const uint32_t g_rgctx_Nullable_1_get_Value_m28E21D2A2DCCC1C6D1036289CF78C4055657C118;
extern const uint32_t g_rgctx_JsonConverter_1_Write_m143C9FF223EC87C820B6192FE369A02E6D31BA3A;
extern const uint32_t g_rgctx_JsonConverter_1_ReadNumberWithCustomHandling_m3375D982C493FAAC8EE104BFE28C6DBA6768DB19;
extern const uint32_t g_rgctx_JsonConverter_1_WriteNumberWithCustomHandling_mECDF3FAC8A48DB8A096AA53A5E92F90F15DD3ADD;
static const Il2CppRGCTXDefinition s_rgctxValues[600] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemoryExtensions_AsMemory_TisT_tA40AE9A1C457E02D08F5C56BB330D6F0824C67FC_m77657FD9A165C0213DC903A229C7D42D1064DC86 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Memory_1_op_Implicit_m4C5BCA4EEB7657957B1123BC5624C1D681EF1425 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Memory_1_t90A0C30EBF1E00C6297DCD3DFCCBA0053BEEEA49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemoryExtensions_AsSpan_TisT_tA40AE9A1C457E02D08F5C56BB330D6F0824C67FC_mCF7F9AA15AD54596111BFD85C71EDE4122F83FDB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Span_1_Clear_mDEB54AE4B12DAD2965A38C42A711D306BD3A8F82 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArrayBufferWriter_1_ThrowInvalidOperationException_AdvancedTooFar_m0A45E21368167FD52F92A4AFB0D226CD0FCFB4EB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ArrayBufferWriter_1_tBAC052AAF17A817C9E895AD8DD5A0295F28C65DE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArrayBufferWriter_1_CheckAndResizeBuffer_m0DC9B191C3FAAEFDA6FA191D9ACCA7E5A0120CC8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemoryExtensions_AsMemory_TisT_tA40AE9A1C457E02D08F5C56BB330D6F0824C67FC_m33E66584396BA0B8FED2FACED932C5B8DD6984B1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArrayBufferWriter_1_get_FreeCapacity_mEEB12BC953404EB085821B6B5A402B490FDC40A7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ArrayBufferWriter_1_ThrowOutOfMemoryException_m2C874FB04D7CA06256F03E9DBAA41464C2BD9F43 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Array_Resize_TisT_tA40AE9A1C457E02D08F5C56BB330D6F0824C67FC_m355483BA72929D749014983A84E0A9BDFE291059 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_t33E1725ED7FF92038C7BEE2A63B4B26FC9DA843A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_ContainsKey_mC24800622CF4FDC2FBC97717108BE2E3AA0DF0C6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_set_Item_mD20119DE218BF07667F6BFB8659E91F7371BAC3F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t1ACC684643F85F6C8E5324691613D8E543CAD721 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonParameterInfo_1_set_TypedDefaultValue_m3C74360B1B74B86BD65664003D2F8E993A28FEF3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m89E9891EEB7C2F1BD829A9B61F10617C004E561D },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TAttribute_tCFEA2E6846437D50965183E52672352B7A75FB37 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TAttribute_tCFEA2E6846437D50965183E52672352B7A75FB37 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemberAccessor_CreatePropertyGetter_TisT_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_m595C1B2C3BC5BD8EA3D4FB17FC855A3F36A77BEB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonPropertyInfo_1_set_Get_m4A871E94AB7443759497EF0D89F9812017129E5D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemberAccessor_CreatePropertySetter_TisT_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_mD2A24ABDF7E004A012A28B45EDFD52992865CB7B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonPropertyInfo_1_set_Set_mCB6B9579B4953FF7AE71BD8DE50FED4F906F8E7F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemberAccessor_CreateFieldGetter_TisT_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_m98B237FB5531A4431A0F7CF012DDE6C678B84F20 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemberAccessor_CreateFieldSetter_TisT_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_mB7F2C75C38350609F54358738FA084761F019B72 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonPropertyInfo_1_get_Converter_mC8E33EA21BFAC5B88F765D5A41D0476EACA550EC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tB491B3D61580B4454329269D8D7F54F428061CD4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_get_CanBeNull_mAD8C927888B7A7823B9CB176676B7DC2F18CC0B7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonPropertyInfo_1_set_Converter_m5C09D57FFA7213CA040996127D220EA9D35310A4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonPropertyInfo_1_get_Get_mCCF1F469F1D7BA2FAB27EBB99C88D5B93FA7684E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tFD604DDB18DA57040905BBA67CA04603E4E5737B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_mDBCBB8B89BE48E6C167823DFBA3CF8347B64D471 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_mEC94B731540D5077702E532708C32FA3C9F4247F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tCA198ABE7175F31BAD8A230AAB26AFE6860FB3CD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_tCA198ABE7175F31BAD8A230AAB26AFE6860FB3CD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m99D530921ACC10E8E987EEEDFA5C8CD5BC9620A7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_get_HandleNullOnWrite_m4D8E029E027E458B1E8957ABE1BDBC83EA050102 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Write_mDFFB07D67B905477FA91927B60850ACAB8BD4781 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m83873E39FC64B21B116216195A94F18216FFE842 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWriteDataExtensionProperty_m5B6D7194EECBE9CA6CA24A65F1C9768BEC311C59 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_get_HandleNullOnRead_m0666DC3498F643DD223B2CB8123A9283A9B6B531 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonPropertyInfo_1_get_Set_mC587C5490A0914839056AFBFEC42D08BFFC450D8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_t4698AA8CDE0EDA2676C1E781DDE56EE96A1D60B2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_m78C22F8A5D006B8CD021A7290B3172F35ADA617A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Read_m346818073759147217EB9D2A6FFF7C2CF02E432C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryRead_m9F212D43E42A396A5A97598FE0A38D7E3EC27BDC },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_t4DD1F7AB54CEA2134FB918349CFE8CA0EA14CAA5_Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_ValidateValueIsCorrectType_TisT_t3E2926710EF8ACEEDACD31E1873C4BCDBAFB774A_m1FB6952A44FF553EDAC6FD20358BBA80BDC89794 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_ValidateValueIsCorrectType_TisT_tD16B70018644DDD44625688F16858EA83C2E9AE1_m2ECD2781B78D228DB80F3C2DF1811FBB25EBA515 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tE34B7119F90B8A8DCBF400060D2975BC1936FF82 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tE34B7119F90B8A8DCBF400060D2975BC1936FF82 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_ReadCore_TisTValue_tB4BC81D4105D04AC3776CF62467ED5A8C2D4E437_m51FF8FE9D3443A55B55556C0128556A92B2FEBF0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t0A96F0F49D79F1688FE0AE2DDC52B57E2AA12722 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_ReadCore_mFBDD9B4FA38EB8A9080FCCC950E5B45D81FC58C1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TValue_tDE495677ADFE4DA5C09A9386458B2037E402DAC8 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TValue_t0D3DD6F5808D0148D4A7651C2CDCD05DFA18C9A9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_Deserialize_TisTValue_t0D3DD6F5808D0148D4A7651C2CDCD05DFA18C9A9_mCC54610C4257B1076F716B0CCA4CAAC5A20CE836 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_ReadCore_TisTValue_tA8D40E187F663C0EBF8A216DAD90FD068D65E95D_m2EF329400C8450A4EB3A4BB277C2A9CF0448AE01 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TValue_tB1A0258CE4FA26EE8109589ED222E42D413FD00E },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TValue_tB1A0258CE4FA26EE8109589ED222E42D413FD00E_Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_WriteCore_TisTValue_tB1A0258CE4FA26EE8109589ED222E42D413FD00E_m3C080239201F3B28A6C4094DC9B4F07CA39081AD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tBC475169FE150BDA903142A8CCAB2AEBC01BF4F9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_WriteCore_m34BAB53B8BF3F796156DDB824A4293FC52F5F238 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TValue_tE5B174CE95B472B7F5634321973402C4BAA02CF9 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TValue_t9FD5D416FDE83F91837BF2CA2542165A362F1931 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_Serialize_TisTValue_t9FD5D416FDE83F91837BF2CA2542165A362F1931_mF1688D4B068193728FD5AA0AF4EFAF3907F7B734 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_WriteCore_TisTValue_tA5E9E572BB69B5AEC92F58ECEDEE6E2E7B87C8DB_m098AB2EDEDB99E14E819C11CBD1C9FF30E229326 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TElement_t1AE69815B8DA920D4145B5C48AE600C602C24D2D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonResumableConverter_1__ctor_m4E7427CDCEE1F4AC4FEDB72C5AB7EF3521CE1C15 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t8380F5109164BDA95E80BF33071364147720B0DB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t52B4721B740701ED7BAC354C51C110660E292773 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonResumableConverter_1__ctor_m0F8E68540E78E9019B0DAC67FBAA7C13C67CEA22 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tFBC7918119D9B5576CDE4A7813E5082661BBD43C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tFAA4828A4741A87A6EF744394FDEB1C3F9B9439C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonResumableConverter_1__ctor_m118F948BDF0A08D23A5B2145F0D914C6A549C334 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tF88DB8ADA94B8B51CF5F4EFD4F88AD11307E7F04 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tFE36F64C2C8201732D89BFDAB3DE34BFEBC30F2E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_ReadCore_m82C77272A57DF8DCF1FE816E15E1FA5BEF0148FC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tD7339923E0CD58E3B0809D33BA14506E69479289 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryRead_mAF4C53202AFBC9B08E553447D10A1F4E804F2E33 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_WriteCore_mE88C9E17DF4E356AAAD055E32B8CDD59E6BF78FB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m40915022FC3FA775F5FE42373BB580F757F530FC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t0DDD3D2B38F6FDCC21CBB4AB9CD92749E52F6068 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_get_HandleNull_mDF5444469FFBABE6E28F70F49BF301574B4A6B8C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_set_HandleNullOnRead_mFDFF6429DE67A4E8D3F9A2A921120106B8E46A8A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_set_HandleNullOnWrite_m8E4B9952445F21D6B1E704B8A3A031D9A563B1D6 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tD7339923E0CD58E3B0809D33BA14506E69479289 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonPropertyInfo_1_t1B7EF7E13ACB99867B26075DF18E8B7CAAE1FAF9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonPropertyInfo_1__ctor_mB491F91A647FDC26E35972EFD1C0D6B6D7B30E51 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonParameterInfo_1_tF1BCFA850D6EAF1EC6257F8B2E5F5D11E97A4191 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonParameterInfo_1__ctor_mF735C1AD426113B4A1AEE21D6AFCFB44EF67A628 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_get_CanBeNull_mD7FA296C8D7189FBA86398CB23B474BA2590B215 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Write_mF96C6974E92ACFCA14A441C21E78C0FD1F942401 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Read_mED3F8CB452791104CDCBF9FA47216AECB7F762DF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_get_HandleNullOnRead_m9CB4E057CE02BD9D9D19C85F41B033576B7F03FA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_ReadNumberWithCustomHandling_m698FF95237957013DD72E45796A1075DDF3BBB4A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_VerifyRead_m9FDF74C2545F5C0A877CB730B394863987B8DC00 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_OnTryRead_m11CE784ADFFAF2A8B517984299A4E7081DF10319 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_get_HandleNullOnWrite_m0E622464A9383A9A392FC9CB2768183702386E72 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_VerifyWrite_mE6AAF4C18EBD27BACF64C10B8A3448A280116069 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_tD7339923E0CD58E3B0809D33BA14506E69479289_Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_WriteNumberWithCustomHandling_mFC7A56F56C209CD1333DBABF1DC5750B1132CDC8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_OnTryWrite_mC63B9B04F18A170836FD32B748FB32B78D346FE2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonDictionaryConverter_1_tC087909408BE26CE89850F27307FACD0D65450E2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonDictionaryConverter_1_OnWriteResume_m2270D7F76FE3115DEAB90010B721DDEACB0D224B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_WriteWithQuotes_m90AFBCFC8C0F40D2195E6175D56295C163011C4F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryRead_mEA3665F26ED602BF1A7CB20976B04A078C1C4C75 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tDB38290D1789CAEE6EC985D497AB8D684C53806E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m0A0905D2D6802A472FFAE28735CCC40442949003 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1__ctor_m36EC3D6498D295B255C36AFDDFFA29D48FDA94C9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t6CDECAEE1BD9961CBC4B710E6ECA2167C215C340 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisParameterizedConstructorDelegate_1_t13E72AEEB31CA05E4715EFD93B92A809529DF3AC_m4FFA0F178A66287E136FAE6A3204B4976098C456 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TArg0_t3941217589CC324BB016E746C699DF186059F3F4 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TArg1_tB3BA8CF4389D6C54F48EAB0A210DFAD7D09602D6 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TArg2_t333E77888D29CF7A42A4E19940D771617422E38B },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TArg3_tA9553F602CAD122EF7E3C9D3D9E98D684C4348BA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisParameterizedConstructorDelegate_5_t892C275BE9831890C910B6A69169AC8CD8989B9A_m52DB4A54DDFEC015DC7853BD0F9568AC90FBC9BF },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TCollection_t09E064CD5B64DB6FF5049F4A1AD0C707D36DB082 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisAction_2_t4B70AE5083569E1358F878E70576564E1C6212CC_m7FEA4CC638F0215795823FFBA0DAA05978A39678 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TCollection_t55057DF41F97B563F9B7F2C422B34F305AC59066 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TElement_t472E7BDEAA1D91CF6FCA7BFF60F73C65B9BEE494 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_IEnumerable_1_t88E41F03C9E5AC89DCBA67A253728A4C49460E69 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisFunc_2_t2A8C23A1286213A5DB0AC482B465338DBD6C052F_m5B74B31EA744E02E6282ADE41D8CB9C881CDCEA1 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TCollection_tB28D690AA7E6DFBB2B284A3CFA7458737FF12BA2 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TElement_t61516DC240367FA216FE81D9482854543ECD1605 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_IEnumerable_1_t007FE4A1A705AA7F956A322A43889BC768192AA2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisFunc_2_t4F3A172E8957261FA0D61CBE3A6A68C8E455C11D_mF8402B2F30FAB6A07B0B38481164ADA120018849 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TProperty_tEBE9DBBF641BE459CA13C4B2DC0486565FCA04CA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisFunc_2_t458AB41C56A22D8CD32C750FA0C033E26A20B18F_m72541CAE1CDD048164D0468DF5F031CC7CC3A99D },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TProperty_t898B394F11DBC64766E3A13A9C8EDC932F273E34 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisAction_2_tC206642FD3047B15ABC4EAED17A699DE43236809_m357C29D71657E5FD7E7D399F8F42435C77861E74 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TProperty_tB1943FD17E67BE8E542C2FDA5A6D7B2B6EA35B48 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisFunc_2_t09B2E99621962966EED02865B7F7E6795E218CC2_mB412EC84B0AEEFEE26010ADF1E8B79670274C7EE },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TProperty_tED2F155BF0CFCD2B4BC16892A56D5ABA934D5C31 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ReflectionEmitMemberAccessor_CreateDelegate_TisAction_2_t80207F11797CF475EC8C17EAFEE81319F66110C4_mC4E60D151AEF37AAC2E6094EE4B3BB1C1F3E640B },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tBCB57AF807045415E1CE59FABCDE5477A4CDE0CC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tBCB57AF807045415E1CE59FABCDE5477A4CDE0CC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tC19CD21B3D8BB13D0F80B28D89E2E0C7B011DA4D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mE7DDCACD787A0ADD781A0332444762B6788BA217 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mAF0AFDB7BEBDC6B1840A3822A3BD0CF41596BC4C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_ToArray_m4E6844F7AF049858D08A162F925020124881C0D7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_tB8C1FD8A26A5643B24359BF7077FD7A2EBDAB202 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TElementU5BU5D_t1F5A0E9D0A21BE4A4D5654D862008D7F88BEF9D1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m00C94C85281FF18BC0579633684F5E341945A13A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t6E205663D31D77F9E7845A93203CE0E886D35B21 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_t8DDC182523F88D102BB288F0AC6B00D0E33DBCD5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t9E32816B3C99EA5C1050145A6950291C221A0B53 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tC19789DD849F20006A5864BFD2456104F5EB4D49 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t0831FCAA238C9DB92CB0FFCAFE8DB3103F9F1AD3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Write_m9A67FD1DF2C75D00B1626305074E6A8884F17CC3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m7EDD5832CCF2CFA76F99E09B1734CA4CE15273B5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m687408B3103ADB07206F85465170C420E559F2A1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t675149C8AF04CB8444423B6D15B2039A3D82D332 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ConcurrentQueue_1_tCF0B031197FA5EEBC63BD20F24CE6A4B5ABEFA8F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ConcurrentQueue_1_Enqueue_m45B7E5C10B72EA3C9A42DFD821C58FEBBCA708F9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ConcurrentQueue_1_GetEnumerator_m6A138A82296718DCACA0E5B9C05942C6255740B9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tFF0C3AB4C365B19F7527C38AF4F03AC2634E43A5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m1A720E51E97B198B46722F4D2A5020FA50D784B4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t69C09F100FC571F525AFB4A9DFA4CC648EA60B43 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_tFFE9ABF0AADCB97013D95654A727A1FF301BC985 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t0BA8925ACF53949118D658600132D89992E7B502 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t8FB660384EF0CFAA12DEA76E03245D4C40D196FA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m14EF60AABA19E8B6C0D78D507BEC675317BB467C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t21FD9DBF7C504514ACD36BB27AEEBDEAFFB1BCEB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m790CE80D1E7990710CFEB5A85841BB0BAE2E4A4E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_mFCD5FFCAE7CC68883380439FB691EE5CC7935261 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t8D9B58F532C32FB82474A72D5F945B46B6FA8397 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ConcurrentStack_1_t77367A6D7597A3292D4C267CAB5C646703CB96BE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ConcurrentStack_1_Push_mE58D4DD1E081D8DD5DA3808C8D66C350F58349C9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ConcurrentStack_1_GetEnumerator_mF4440B9C6E46510286178B8175D491DFFAEB22A5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tAC9C08D1EA77809DD7C18FF4134274B9D0004386 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m5BC5933E56F93D5B04698C5FA4B0F4CB98A7D520 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t7B37363A44C180A1692C379294B318AFF04DC08A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_tB7AB5626C1DFCA69236DF9637B9E42322C5FC487 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tA42A6C9D2240C080D9EF5D2057C9363811ECC748 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tB1E8783B3103EF6B8CEA2702B771A4D13825E641 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m28E54482BCEB6D5BA792FFB96FCF0B587A0A685D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t69E19CA421B1E2AE50193E350E25BD09407D7106 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_mBFF44E4B86C4C7EA2344D08A1A5166C8D0198895 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m84DFD8222E8E8D9C449CF27429F61461E7B2ACC7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_DictionaryDefaultConverter_3_t0B233067DD1C957B9DE5EDAF115831EDEDBD37A5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonDictionaryConverter_1_t411F6E57621E4D8106D49FB2C766094CDCB29F97 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t9570A0EC63F6315490038C67C7468004FF290423 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tC689AEB507F9C01FEC75B2D56E4F372EFCF67DD3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tD8A4756B872EA90428C8E210BAFDD99CF1357156 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t6E9FEFBCB012AC5FA9ABEEF0E3B37B8FBA62CF37 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_DictionaryDefaultConverter_3_t0B233067DD1C957B9DE5EDAF115831EDEDBD37A5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_CreateCollection_mBA2F53A5E6C6F3EE929C248A1199F02DFDD8FE7B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mA9F524EB3EA87842BEF90546229E83E44D54CCB6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_U3COnTryReadU3Eg__ReadDictionaryKeyU7C12_0_mDA39BFA1388B915F67D50E4556A3381444CC4115 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Read_m6FBE2A8A9E48D2ACF8D9CD52B91C397C88AFD9C0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_Add_m5F59A5F09A46AD7BF42C8C8958703B95966F83ED },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryRead_mFC572AF140B80BBBF379FDBE9641D2D050E1E65D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_ResolveMetadataForJsonObject_TisTCollection_t01A5069B8828912E75F26ED12D6A738536A2C1F1_m775E6BC0F2C2B4761BE750A8F802E8CB06CEBF1F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t01A5069B8828912E75F26ED12D6A738536A2C1F1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TKey_t8136787C6870DB0A3BFA03E1ED59F44AD2EC2250 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TValue_t3EA828DFEE924BE1D12435C6A5D9052A1F3D1277 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_ConvertCollection_mE9C68571717F5C1E779A24D274EC494E411DB04B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonDictionaryConverter_1_t411F6E57621E4D8106D49FB2C766094CDCB29F97 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonDictionaryConverter_1_OnWriteResume_m2C9C62E7A213E8C3B49093088FD1646167C4173D },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TKey_t8136787C6870DB0A3BFA03E1ED59F44AD2EC2250 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonDictionaryConverter_1__ctor_m73DC9E0D5516AB68E306F43CC847788971B5B970 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m7020907E6F66C47E324F6FFCD2BECD064D8217CF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_ReadWithQuotes_mB3ABEAA48FF94CE2DB91C36A946B98DFB42ACE45 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t00F3E9098471CD4DFA6D00AB92D2D3606D3916D4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_t5A8B2B1C52D9422087EC56F65BD7A5C76C674EC9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_set_Item_mDA22D1FCDE62446BE9167F28F031DF4F15D150AB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_GetEnumerator_mC8D73ACDBF854FADE4E706F8AF24D2E49D4FD9CE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m8712EFE9598053CF09AD65EB79E2F3F82B3C26D9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t824B3A036C96D1E60A16E5B3811F06BCC965690B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m79B2BA43705EF87265EDD2342B8A77DF602C0AD7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_DictionaryDefaultConverter_3_tB96119E4C1E293321B02467E95CBF03CFB65DE79 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonDictionaryConverter_1_t11EB0BCD92119A6E7BEBA25AF4DDDD35C77B54CC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t378B48852AF248DD37E7EE23DCBDC1D804B88820 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tFBBEBB91A132D0A522BF2E9AE242B3433221AE29 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_m309F600CC905E152270A4111F0DB2A8A7A93F62E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_mCF46EE5216E0A3F994209069762431847850F678 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Key_m94EC3F66AF88FA93F0BEEFC8A4EC570F33464879 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t012B70B47D93C53B77F5C0C626A23C85B149749A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_WriteWithQuotes_m7819569B23B85A712F80E6519D88EBDB0F00829E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Value_m766E7281E22F11119EE016E66826DF8D46525DAE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tDC3BFDC5C021AFAF2F929F0C04653DC3F02D3A4D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Write_m3996AC2C1AEA07D6DD2A02AB7AF0C65233DB2A2D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m3D06445A824BA544C7304C56AF1E6089B0CFA41F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3__ctor_m8C2DBB20086019BC6C68E19DFFD788BFE7018434 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_t8457F777AA23E8681B8A14DF36D9155895D6226F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ICollection_1_Add_m22E14ED4D111238D0A232ACE41361A69813EF90A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tAA5353DE7CD7FF2EECB595BBA4A8FC5B164E4250 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mD518C625F109CADA2DFEB40989FDC37E3EC18612 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_tBA1190D7F9BBFD194309EF7D4B85A50B4F3DBC22 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tBA1190D7F9BBFD194309EF7D4B85A50B4F3DBC22_ICollection_1_get_IsReadOnly_m41951822E28FAE58DFB6AF69AF4B4AB3F6685801 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t996A4BE879A00F49B408811FCAF8661C16C6C9D6 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tBA1190D7F9BBFD194309EF7D4B85A50B4F3DBC22_IEnumerable_1_GetEnumerator_mBCAD29AD6C1963559C011B258E83FD8CE24CDAF2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t5F70601CEBB4A81852B2DBCFB27DCF4228148350 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m2D93423DA4B75EC447620D18CDE60FB5A36A7760 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t39147C2A74BEB19289E90E01DEE8582D7FF3E14B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_t210B7BE31E27D478112D37C0407BD96A0468B5A2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tA8311C863415AE360B287C326B16452FBDF05E16 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t1C24E8E4CAA1C7925CF888003CB0514C14667624 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_mA4E1144A91F20E5DCBB25657B1F621D578DBF176 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tFFE8113FF72D1F0CBE3C49E37ABEB305620F0690 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_mEC6957E8B18B63048AF9EA5E03FF47AEDEA681D9 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_List_1_tAA5353DE7CD7FF2EECB595BBA4A8FC5B164E4250 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_mE8CA7DBBE4E0823DC1880E3043EC9220C336FE6E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_tC7D4BFCDCDA0A17D9E2A3FC15F3445E1727402D8 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tC7D4BFCDCDA0A17D9E2A3FC15F3445E1727402D8_IDictionary_get_IsReadOnly_mA50EB3929C2FFDAD6B44CCD2114D45ED688FE73A },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tC7D4BFCDCDA0A17D9E2A3FC15F3445E1727402D8_IDictionary_GetEnumerator_m755A2ADF1298A0D930C08A309E890838600B4DBA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mED881F8A97EE020144721253D00CCF143B822DA8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_DictionaryDefaultConverter_3_t6394A26AA4EF32F099BCD33887504F91F4AF7E96 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonDictionaryConverter_1_t5410511662503DDA21756F05F57AB207B148ABF5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tE4E30BF94408F27B56691BF5B114B5409A5D9C4A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t24F66509AB3EE0B6B48B501D42452321554C99AC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m3D2CE6DA5D3D114CD24C786C7B204E0D230DD65F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionaryConverter_1_GetObjectKeyConverter_mFF121A41CE894661B5714F31103FEC8D81730F25 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IDictionaryConverter_1_t7633E4DAB4E0800621A8BB4A120AFCBA73EFCDEC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3__ctor_mE6F64B58C31B60A89E305F3B10EF35DA80BA5C9A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_tBF9A2E17D407B10FADFC3B11FDE8A669DDC51B1D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IDictionary_2_t9F52DCD3E9BF7DDC6454A0DDB6BF574AB4E7C200 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tBF9A2E17D407B10FADFC3B11FDE8A669DDC51B1D_IDictionary_2_set_Item_m7269D6BF8F1071D75F5B2D55C6E462F27F01C152 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_tB22A2789146E8EA389B1582D6711C8F08F0ABAB7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_mC1244141070D94DF0AA6B4E00BFC05962F67EEA5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_t85A94DC5C7444EA044235740F2EB67BD48FFE6FB },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tBF9A2E17D407B10FADFC3B11FDE8A669DDC51B1D_ICollection_1_get_IsReadOnly_m7C5B1373B6B36F7F31AA950980A9B40A1373222C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t28A799C268393CADEF10FAE5CD73D145E0BD7946 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tBF9A2E17D407B10FADFC3B11FDE8A669DDC51B1D_IEnumerable_1_GetEnumerator_mA6A30A62695ADBAC5FD4F31F3CBFF7130C6E3645 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t8F9F0717EC707556D64042F88A7FD0CB26439FF4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m5A03A5C352401828BD751A5D1A3B24ABB8CA602E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_DictionaryDefaultConverter_3_t67A0FBEEFAA1D791F67E5DF7283606B33B1CC67A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonDictionaryConverter_1_t17E676BA48F3BE08DB4C492F3373CAB36DC1FE95 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t583ABED96FDA8A3F5EEB76BE38E519A82B0D1B8D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tC62BA61041A4E29E577402DD07F9DBE91CED186F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mE419B3FCF7CA1452FEAD5BD21E760FF5E76D528A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_mF8157C9A096A1427B0F05E4F5CBDDDBCB2F8B243 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Key_m13DE05489B55F2FD6B9A88ACAE791ABF5806CD73 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tCEECF0FE67B242244A3C1C85103014CB8F5EFDCD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_WriteWithQuotes_m5D0F1ABF170B4EC5AEABAAC246C249BA77CD2ECD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Value_mF7602B57B11AAB3CB2CC3FFA22A5E69E1DE403EC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t0D63BA06435AE8F83AD42176B691DE885CE8788A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_mBBF7EB3DF87F7AECDA6C0FE5A2A9E23231A8EE63 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_Dictionary_2_t0E2646A5C68B4B94CD6D7BDB97795B5EC3FE9864 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3__ctor_m1FA0DD8D1A2A812CD30F99E4B5F777E124A54BF6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t003D1EF6147C89C48A4490B3B2D54CA13A29F303 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t003D1EF6147C89C48A4490B3B2D54CA13A29F303_IEnumerable_GetEnumerator_m4265B954378E12AC1B6D9F21CE6B373F423C53A0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m7CB7D24EB688D3EA01A2DA7B4E05CB44D66A0D0B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t10EC588DEB4E07B93AC7D5EBE29FA9290FF2BAAB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_t8C3189FFB80E592C4CDB836FF79A208004E3976D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t33BA5B702B0900D6A4B2FBDC3735E02D5A83976B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t87B4D7E4BED6B27B976D0006C2C5F0D82873F853 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m78BC3C2CFE853B9BB2CACB557A922613899FB038 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t65AA2F2D1FCE03437C1D9DAAFCB14565B0C92F5B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t3137B948230D1792CF1A778C03BA00C98AD9CF7B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_CreateCollection_mE7B8161F7D43E56958BCC33A1F95E6D87C74EEF5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mC5599F38A4AD35E82350B19C19C0C5F5C5D3DED9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t3137B948230D1792CF1A778C03BA00C98AD9CF7B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_t5C8BF742C383F0411A5B63C5E6D9941215E96CB1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t6F4C3545859325214F40D6BD7AD005C202EAF265 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tB96431DA83EB2DB8DFEA7C6903FFECF10D846BC4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Read_mA45FBD76C05849B7694EDC38BF46519FCB9F9580 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_Add_mD01C549B2DA9DF97C1BB1D5A399B926F331A8B7B },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TElement_tAE40820C5AE75965D808D8E9E2BAC2969B1F7B42 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryRead_m5AA471A97ED2A6FFDAFBE6B028FDCF1917B1ED53 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_ResolveMetadataForJsonArray_TisTCollection_t4649B0D01A80384F08E5491E881819EF7E300CAA_m402B75507E80715ACD1737C9F746614DE26BFC7C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t4649B0D01A80384F08E5491E881819EF7E300CAA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_ConvertCollection_mFB39A14A746095CA88D64A5F0FF108502641342F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_OnWriteResume_m271C3229FD80D21A8A19F14CA2236E584CEA3D16 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonCollectionConverter_2__ctor_m09CD36F410E956CB27D5A7BC00ED47E1930257F2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tA14A7F95BAB023CDB60B1C895A22D8DD2EFE9348 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mF1C053D8605FF1236A2E0429FBF95EDD0C32A816 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mEDF7F0FDE60691F1B45FD2A54CE01F40F8596B72 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t6387D2966E86FEB8FB32D13D1B985CDEE26186C6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_tE08EAF36AAEF1706B2364896D8542403BCC38926 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t6387D2966E86FEB8FB32D13D1B985CDEE26186C6_IEnumerable_1_GetEnumerator_mC7116D264B064F4C1E6FF867548F7C9AA2A73524 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t23BA32096121310AF36E129F4C9BCCE8393B0FEB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m974F289AFA4D32DFACC66BA8CCC3F1E798458D93 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t32D1872A599CAC6093C8ED4A3A826DD7BFC083CC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_tB0F259EB2D562EFADC7B0B90FA3EAB6E1C2EB150 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tD51CD663AA1CA91D54138D6710E3566921149648 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t1F2074DD72E8A9236011CBA194D8DD4B89D3CCA1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m2DC4A5447318280BF044863E5E2979A1ECC7E695 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t9A3989FE0E2B0915B8C95BD23743F2D4908F3C68 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_mE9BD0A83194E1A450E702DEDCAD8C3E6BE3301F9 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_List_1_tA14A7F95BAB023CDB60B1C895A22D8DD2EFE9348 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m952FA8204B3CE7EAC955B1C49D21818059447550 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_2_tEA2EEE1BC71F5F22A6F2AC4EF8C18DCD4F701323 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t43259AAF7B7F2D7FC665F2AEBC1CF00CE5DDEF95 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_2_Invoke_mFF3FF23F6456EDBE545401A2760210849AF1EC04 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemberAccessor_CreateAddMethodDelegate_TisTCollection_t43259AAF7B7F2D7FC665F2AEBC1CF00CE5DDEF95_m0DC9388004B3F69B2AF72D86CD548A7E95D5ADCE },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t43259AAF7B7F2D7FC665F2AEBC1CF00CE5DDEF95_IEnumerable_GetEnumerator_m4265B954378E12AC1B6D9F21CE6B373F423C53A0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m7AB49AC60F2AA8B4A02E06FF5A5AAFEE3A979FFB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t72E695A32B171E46671B8AA847D756441DD651F7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_t956117A4186A74CDB064BC63E12D80850A608CB3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tF5064B0FD66CB0CB67DABF05EF20CD44D32F31C3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t2A909D65701BE89691C7C0FC97616F29C952CDC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m3E19E663355C74EB56E4A10053082FBAD57E06D5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t196DBCB56680D17F2CE93490AA59F5E05411E547 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t196DBCB56680D17F2CE93490AA59F5E05411E547_IList_get_IsReadOnly_mE5E5B403BB8FF044EE55F0972CD3221ADF935C27 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mD710D682C079501D9D88C957CD5F467059D8ECCA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_tA5A58EE16639EFD3FEDB976EF90FB5D983C1D87A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_tC2D343C754D5D9862810D76C4844CA991239100A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tD6178E5FCF5B7254E678E0D850D4FDD00310D0CC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t9268BF8E2ABD95390D36EDE4E0A4A0540572E581 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m7A6FFEB7486ADB4586A6496F00995C9E122EC870 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t0A1D11095EC604F0F067CCE703C82AC9009771C4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_tF79A626C69A179047CDB18F9877D3235988C62E5 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t0A1D11095EC604F0F067CCE703C82AC9009771C4_ICollection_1_Add_m3ED89D0469A4A4B04CCB7AA12770B4ACDAE6947E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tA2DB79D1E93E292E66768239C04CBF7A01DF0346 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_mBCCBA87E642E31E4A547082A50B9B77896083B28 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t0A1D11095EC604F0F067CCE703C82AC9009771C4_ICollection_1_get_IsReadOnly_m1AAC5133ECF33AF4569C7BD6E5DBCFEADA8C0E70 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t4A46546D43DFB82971C770B239D35015A8B55088 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t0A1D11095EC604F0F067CCE703C82AC9009771C4_IEnumerable_1_GetEnumerator_mD6BC2B16E4309929D7F8C95E8B76EC35E7E0B69B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t28492F9F7500839BD270E52E447FA85856BBB5DA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m210A8CE950129F28D67D585366F43680CD6C5D02 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_tB499838FDC358B7C026D8DF004CD33BDA847B245 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_t3375DE57CC6783B581913AF25DFE29357858B7CF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tA8692C6FE7A5209F93652C35E50439B93BE894E4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tEA8D332A916544953122311A62117798B96832DD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m335ED65CE1CB395EEE08D11B42C45155BE6F908F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t2C0B69C552D048AB00F919CE6988A0094C2990B2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m56E1334F9F2103183D224F04615A84D683C257DB },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_List_1_tA2DB79D1E93E292E66768239C04CBF7A01DF0346 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_mBC3E6E8CC1502808CF37B21CA1E12388BB7F03E3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_tAAB501F742C2A685A3496816666C712035DA0B3D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_set_Item_m8199D7E45565E35C582A76B8F095722FB4203D90 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_tC746A775C8E57B921977C9B5B8CA7F8B40699FCD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_m1B623EBBCFE59645641E45B70CD05B15586851D9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t494E325510E2E6475ED02E9D6145340A86D37347 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemberAccessor_CreateImmutableDictionaryCreateRangeDelegate_TisTValue_tBD514929FB6FFFF05CA8F7409C1D0115A3598758_TisTCollection_t2A6E13736A792EF659D2EAE6B70286E1276ACE93_m97EF539F10019DC6334E0C40166CA20E8091F79B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m495B3E76A7AC0FBAC6D2DA91ED505DFB2964208C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t2A6E13736A792EF659D2EAE6B70286E1276ACE93 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t6CB5825CF181AA6D011E40BDFEA0704A001F1484 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t2A6E13736A792EF659D2EAE6B70286E1276ACE93_IEnumerable_1_GetEnumerator_mE6376A344B9778051E01EAE759475AB17BEE51D9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tDB9D0B5DDC2E9D306494CBC27465231DC36DD82C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_m9F96943B63CF2A09DD169E5B773F3492FFC72838 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_DictionaryDefaultConverter_3_tE81F3695013B6F8F466119981A45CFD0F74FCCD2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonDictionaryConverter_1_t5B9D0D7E6E4C1D73A15F63D139A69BC97385FD24 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t32C43716ED2596E9D3C0371C6D0E46D4A629E67E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tA6B951BEA5574815E8020D7FDA02B2ED2AF1411C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mCF23B2FE9F7C6D7696730FC424A8E1491A51734B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m152375CF2A0416C1D8133B86C6623ACB29D97423 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Key_m168E3CA8F1EFA2741FFA01C3F76CDA14394D78CE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tEAA8A18BF99BD4E5484FCF19DA844DCD91333CE2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_WriteWithQuotes_mFE51C560F721087D75B4457888E64CD331160013 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Value_m415867837EFA11E21572234B43AF3A1C3D8CE10F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tC9484DD1CE90228C5843FC79F57B90D0160E2FFE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m58FE86FA41377BD5B4753252262F4569E099808D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3__ctor_m7EE7AF51DB882B901D1E290F39D438C31DDC1564 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t5D1572D9A71176FD0448E876A05A764F119F374F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mAB0AE047073F99A97CB68262EBD9B53F239BC406 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_m0BED04D204467AE7D1FC72C99351282985F99B18 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tC0E9B465A9C0A7A88C553328372FED49B5417AAE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemberAccessor_CreateImmutableEnumerableCreateRangeDelegate_TisTElement_tF846BA1E8EAAB226B8831BFD36E4558004EBBA3F_TisTCollection_t0554E18FC0B2071F4E9937AEDB30783BF05A1352_mB6CA2E69F98EDC360718408EFEFD36F7EAB1771F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m03F8603938F5B283F6C62F96A0E8F073FB19840E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t0554E18FC0B2071F4E9937AEDB30783BF05A1352 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t4422373607392BA2654E7C027041337FD44C85D0 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t0554E18FC0B2071F4E9937AEDB30783BF05A1352_IEnumerable_1_GetEnumerator_mA1B5D9D00E74839C1737B3073F3D8DECCF29F8BC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tF58BB06E6C81DE1FBEC13E79ADF0207022FDDDF9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mCF14C03F8C3615D2370985BED1D146BB04F19FD7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t531C23F3B5FAD310A68535743B992E87419D4741 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_t9BFE796E6EE4DCB83DE16B7AD27B2FDFC3E48C59 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tD5C348E7BC136F90AB3B3626B0944E9DB2EAC1C6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t9442173F012FB57EF64A352E53C5BAB1CEB9BC8C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m075C3E3E228A66821C454E40A0B0A1164C7663D6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tBEF782DC199F580BC088E82ABE643DC38DA34C91 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_mE71E461EE4733CCF66C388AA16D14B071443C1BF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m8179ED063811F141A23B8C0E85EC28B9B4530DE7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_t638A56AA014FFED92A57CC3EDCF319E4C4037B69 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_set_Item_m036D8C14BF7B639AF1B37C60E30AD1B386BBBEC0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_tE9C1241028924D1D18C9F4F8647F876F162E67D6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_m4099E953375307210F006F0DC36C8355BEEA5679 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t85EDCA0BB5D949125B066DF7430ED54D58070F1D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_tF6B2F07F4D96912C97D169B7F871D12FE16F692A },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_t85EDCA0BB5D949125B066DF7430ED54D58070F1D_IEnumerable_1_GetEnumerator_m9DD9793A24A75647845306876E139DA5D899BF1C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t6749DB4C43CFA585A0227C2C62B5A240A947E748 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetKeyConverter_mEF66ED2D37C5C7CD35401C1E10F023B7A3ABAD6B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_DictionaryDefaultConverter_3_t30D9BE46E61426EC0C1BE1055F03AF6FA6655B64 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonDictionaryConverter_1_t2433F18E25510FDBFFECF2BE4AE4CE56D41D6F0D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t26641F6C47C365E800AF99623399B12BF453ABFB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t2C7E7AEB3B84C8AE016338FA917A908EB5B6D1E2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3_GetValueConverter_mD351E0117A9B6AEC88A9E5E122AC0512A17A7F1A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t5D8B7C0C162CA96CCCB69D9D6D1A3A61C4EE29BC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m74CE3D19644EB75A7147B4100DE13698B9BB4883 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Key_m3143F3202268ACC42038D3825658C8CE04D628C1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tCEFEF018CB413AFB86879AD5DCE13A116B7CB898 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_WriteWithQuotes_mE2538DA6762D1A1B69C47DB2147247713077283C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Value_m2C2CE39AEB10F06BEBF94DA0EB0B50C0E3D2CF12 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t502DA0340B50DC12E1E34332FC07536780760F00 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m6116E76446C074A069B8D722C963BA69470382CA },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_Dictionary_2_t638A56AA014FFED92A57CC3EDCF319E4C4037B69 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryDefaultConverter_3__ctor_m6C2717C866ED8D50C4692178E87AC31A1BC4C675 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_tD6E60897C2E5EE5150D61B275634107E7D6519DC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ISet_1_tD29CF3D02013A2312240271D0A50CA61D96511EF },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tD6E60897C2E5EE5150D61B275634107E7D6519DC_ISet_1_Add_m21BC5B710529AF709AA4AFE37DA3ACEC5A260784 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_HashSet_1_tF6AD039CEDAC33162F2F6F050099B405E014A1BE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_HashSet_1__ctor_m499D51C3974733EE9788E5E81307F608EC1C2D8F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ICollection_1_t1E4D52D5C8AD3C3CCA94A97993A7297CB636D805 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tD6E60897C2E5EE5150D61B275634107E7D6519DC_ICollection_1_get_IsReadOnly_m7C4F322CF7F9D4D14874BD0E59A8F9E4B1DBEC4C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerable_1_t6E7F4BB0C6EA941B221D9D38FF5BAEDB4B0135AC },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_TCollection_tD6E60897C2E5EE5150D61B275634107E7D6519DC_IEnumerable_1_GetEnumerator_mC5F7DCF20603B9F6485F6409275015BD5E5D8755 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_t89ED099CAF3218F3C5AD972FF27DF2D874A28E50 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_mB9BD3383165441071E2A61FC742ADBD0BE9FD8F0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_tC0FD6306BC9AF3BB87B083DB15E7E617BA5F1959 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_t380217BD52DE6CF525DD4799ACA428E29B96D7BE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t1B9CFD5CEA125FFD3A613F9EB3C8185BED9AFF45 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tCE3E6814D0EDC56C095F25D3EA6760D1960A3DF4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_mD85285D23743F2E0600A9A46B06B915B0D8C8148 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tF9641FBD3DB1D0E9835835C8DAB46BC85FC4D4B8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m59F206B4ECE012EC150EF33B7D4BE0D3E6E59B07 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_HashSet_1_tF6AD039CEDAC33162F2F6F050099B405E014A1BE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_mD7F8D62D9121D2FAD67D434990FCC1412F49605B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t9F22616172873F2FEE9AC40B7E581BD5BB6D4572 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tECE03481F6879E14B4ECFFFF8FE3198B3C081754 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mB9A41C9D43AC0738FBDB3C22D7657FB761E142A9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m199C0B0B142DC6E9B2884E4CE1E45F081EF69ACD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_t0042356FA6FE60226FC4F8B185AB1CAB35067176 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_tCBADD8DD43F0D2F3A38A1B141109BA8472F7AC3C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t729917A0DC21999EC426912B63EDC52F576F7074 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t5FBC055A1297B78CA362E5FD869EF078D3877F76 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_mB1D8600B2BC8D53FF10F922836BEBF6B19C92C0C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t91EEC4EA0B80EC56F1199BAA160B2A2AB088639A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Write_mDCB1CBC0E30CFF17793220CC16B8C352924CC168 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_mA08BCF11C5BDBCC67A9E447D5A3D4C35034D05CF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_m5C109B0BFFBA70E18BBB76A1B733CEBF4F6DA776 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m84E7D7DC2CE9A78B7CEDC43B2FA6FBE6AAA63A74 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_t64C6B76B8C95C904FB42CE5E1C670AE705668EDF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Queue_1_t83CC3F01E545ABE139E14EDDF944C381E1505AEF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_Enqueue_m8B7A36493528BF7811D431211139D1CAC141480D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Queue_1_GetEnumerator_mD059066BA634C08A50F614094F2D275D88B2C787 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t98FC87963C4BAFBE32153D5A71FABD97FA35AF6F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tFA006739BD733776A3DE6F7E393F22303F5A4DFE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m4200CE553B11E0096A7DC7548ED3F59B2D5854B2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_tA485C12F29EFB8A69C6E24C2765EAD8BA16B0905 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_tF0DD0A08037C986ACE0F9A90674A5DBDF8A4D0BA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tAD2D489BE3D3C14360943EC80614ED81962D6F1A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t84A8A4EA23DE3B797EDE16202AB392A73FDB2A0E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m9D69E9749A649534477D2FC46DBEA6506BBAB014 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t5360C6C8E8270796A750A26020C2972C2436F058 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_mB570B3067BFEAFD47479EC94360BBEBB730AAE7F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m1F620BFDE99754F2220A932E49DCF1B30FE25233 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TCollection_tB9FD01C8EDFE9F5FF51206A2E5612D1BE6E6BB70 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Stack_1_t39925D474A0105731F5C7B3161F716C78B3CF5F4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Push_m7CA75794C041550C066584EC81934B943B654F82 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_GetEnumerator_mC7096A05E7068BA640EF1C1BB41C0BF66076EBA0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_t605551D76237842039674591C83620BA1ADA7FAD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerator_1_tA082880A2FCAA3F4BB69258C447E1242479A3A79 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_GetElementConverter_m39820F235E6DC01F8991A7194DC864C0CDF24ADB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IEnumerableDefaultConverter_2_tC24DF6AE1930A1A043DBA0E0D40099A876EEA65C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonCollectionConverter_2_t832CA1C01D112791027653C59FEEB492069E5045 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tB293D531C938D23BB24ECFD5A67EFF63D530E129 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t7DEFF1D5FF0D57D4221EC52291DD5289498CAFEB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerator_1_get_Current_m40A65C1E4B6189B89DA99642B8DD91133DAE2E29 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t5FC6F80CD6267D7C966E7409695BF38868329E85 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryWrite_mD422CCE9A785691AFEBE1D23442262883DFD3422 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IEnumerableDefaultConverter_2__ctor_m4D4C62293EED172F0A50EC8925F10246490AD552 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_KeyValuePairConverter_2_t64BBA9ED601006E9587D301CC9FF332AC272E3D0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_tC86B454F51DB78B3AF391EF635AD82FC8C83AA2C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_t4D48D99915BF1CF9457076C0F3594ED720FB2F83 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ObjectDefaultConverter_1_t74932E33DD562EF701F2B106CB315954362576BD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonObjectConverter_1_t2CC9C20D8C73855F215AA53107D5011658F827CF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_tA5924CF75F3C90C66E692EAA61010B48A7D20C8E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t7A3E0841593E6638ABC7FA12589B77AC4347728B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePairConverter_2_FoundKeyProperty_m8D0C1953590742CFFF8B5E74A8E41BB71003CF8E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePairConverter_2_FoundValueProperty_m47BFA6BD8A1EA1213E2AFC58B37519EDCC6C128E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5__ctor_m14F775A10E479B67F97A33431593677E438217C6 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_KeyValuePair_2_tA4BCFAF72105A91AE0A02D4020A50AEDE461FEAC },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TKey_tA382EC4F70B9BAC96939A56B27ACF750D2C1DA77 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TValue_tB0CA706FFD088F33C0203AEE25ED0C87A6112FA3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectDefaultConverter_1_ReadPropertyValue_m7F55EE2ADF51299B87EC435C3F0FBC19747FDE88 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_ResolveMetadataForJsonObject_TisT_t287FE2F6E60E6C743EE88E99504E5792202E6C24_m374D6DC3C8B4F505B9C5AF180116D8EE50FDA1FF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t287FE2F6E60E6C743EE88E99504E5792202E6C24 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectDefaultConverter_1_ReadAheadPropertyValue_m6B948A23693C5F0A36B42605AA1BA9A9AA7F7E76 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonObjectConverter_1__ctor_mA1BF7BE3DA160847FF34C293A0D514297C48AE36 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonObjectConverter_1_t0914FB6C256E4424A629A1FA63728CC0E9C1068F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t310EF14609C149DA5258FF1B266A81C70DB54D86 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t305B26107A8E9B03D4BC8E2884A91C73E2604C1E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadConstructorArguments_m3227484126AEB8ADE914E61BE97B7E9B98EF05ED },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_tEEED1475354D3C24BCCD800252395F9A319692F3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_CreateObject_m20683E353A666AC0E7F42D580D970DB513B97868 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectDefaultConverter_1_ReadPropertyValue_mE470E259A9CBCF3BD8D534D78A9EE0B035917B78 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_BeginRead_mDDA6496E75011CB360B3BA729DE570C65AC202F9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadConstructorArgumentsWithContinuation_mDAE696C0C64A0F4B693AF18EBCC30E9E05A9BEF6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_EndRead_m8F29F7BE5EB6D7A9F9D532F395FF7AFAC2321A99 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t1616A25A7065F35DE236E47F4557B35630CD5FEC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_TryLookupConstructorParameter_m7AF2E6099E67A449216433F18292924A1DFCF275 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_ReadAndCacheConstructorArgument_mDB7358FC8287C952670637C41193462E51C5EC7C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_HandleConstructorArgumentWithContinuation_mD77A7CF84556A99EE07C2CA9E9C6C72AC450344D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_HandlePropertyWithContinuation_mF84FC527DA15C2FB30A6B7C189EE413EBA1BEEB4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectDefaultConverter_1_ReadAheadPropertyValue_m50FC7C64BE768407017AB9CBE553BF53412118FB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_InitializeConstructorArgumentCaches_m1487F570CFF5A17AF1C48E21132A379343CA6B52 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectDefaultConverter_1__ctor_m239B9E2401ED47864E70924C37950BBAA9289700 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ObjectDefaultConverter_1_tFEEC4314277435DC85DA3FF86E95E280343509FA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonObjectConverter_1_t7B8837C27FAD8B8CB33748948EB2C0F42DF1B9A9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t51B8AD548D60AEA1F96F3823C3D77F45EAC6F593 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t6833039C608745FC2E16F6F3F164CE0C7922EE8B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ParameterizedConstructorDelegate_1_tA3985362697AE32AE8A01166E78D6BF60CD9BA1C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ParameterizedConstructorDelegate_1_Invoke_m3176AEA7AA114F9D555BF1BD47402CD3BF405DBC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t9912E5B8B30854DBB260C86458564E60DFF5353D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemberAccessor_CreateParameterizedConstructor_TisT_t9912E5B8B30854DBB260C86458564E60DFF5353D_m00AC1577F211EF1E38DD10E070DCD3FDE473F00F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1__ctor_m3D1A63C10E9E35908BB1A4A368E05C1D1441426A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_t98933288273CF60EB264D350860416ED17F47851 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ObjectDefaultConverter_1_t34C9390A3381C79D2DB4D72654131DD6D592CCF6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonObjectConverter_1_t09F60F0B81B861DB28F3CD218D0F415A7A75ABA9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t073EEFE898DEDC320046E92A75B8A70946C3A6B7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t2A0202B428AE9BC3A82EF5720050E87AE070D74A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ParameterizedConstructorDelegate_5_t8DD5C8DECBC93AB24B173F8773B3257AEEAF4B46 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Arguments_4_t4A9E7135DE4FA467371AC9511AD324D226BC5F50 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ParameterizedConstructorDelegate_5_Invoke_mFCA4425F3ED06F610D3A08C6EAC4AE47056ACE25 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tB4D207CEE30F0E0957F76ACB6B58CD93730F0B49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg0_tD7FFD5267526E81889EDB6BF6FDD94BFB75FEE87_m25334347723E5C57859F611A05D64F4F4E2272DA },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg1_tED9AF6DF042763F72683B4F96257CE07487815AB_mFB23737E2E7FBB4609495DB5987D3CE0674E93E7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg2_t77D49D4685249DB962025324196F747B729724BB_m273D89A0CA57593F3E296360A46783F1E23EEB01 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_SmallObjectWithParameterizedConstructorConverter_5_TryRead_TisTArg3_tC7D5360EECB8A558575B71674EA3F6CEF614564B_m0A1FA4063407383E5174734BA8BC10176E028997 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MemberAccessor_CreateParameterizedConstructor_TisT_tB4D207CEE30F0E0957F76ACB6B58CD93730F0B49_TisTArg0_tD7FFD5267526E81889EDB6BF6FDD94BFB75FEE87_TisTArg1_tED9AF6DF042763F72683B4F96257CE07487815AB_TisTArg2_t77D49D4685249DB962025324196F747B729724BB_TisTArg3_tC7D5360EECB8A558575B71674EA3F6CEF614564B_m3F9C901AB15DF365A77366755F49DD74499943B9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Arguments_4__ctor_mA878CDA8F562E298F30BFE3B135286D79B70B0FB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonParameterInfo_1_tB18562E746B25972727D45A8ECE543FCC787AD37 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m9EB504BF8B4CABEDB30440DE9325501EB2D4F04D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonParameterInfo_1_tBA750EE03AD441C53566A4E4F78794576B0F1683 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_mB7270495EA1BB0F905716F176FF6F1641791FDF6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonParameterInfo_1_t2152656B119EA0478DB869E987E967635AC89BC6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m21BDDA4B1C5C8F62C6AA1E0AA1E1CAE2A67A19FE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonParameterInfo_1_t6FA67072B13831CBB2D52A277D7ABAA8C510F626 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonParameterInfo_1_get_TypedDefaultValue_m6A6F94EB8059757F484F2FEAB77499490AEA17B0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1__ctor_m84B3063A12641C309B3B15AAED1444D755963983 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ObjectWithParameterizedConstructorConverter_1_t16E2ED436AE1310A6A69F9011B2588D8DB266150 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_ObjectDefaultConverter_1_tD6B1377AD0F46A96CBAD452164E88D75FF9FAA95 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonObjectConverter_1_tF7EB076CD2962153D7020A22B7CA8D7804681DDE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonResumableConverter_1_t43B2EAB31E52FA42998361FD0DD81A2248F34609 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t65F98290BB99759B5EA0A76A80884B6705303085 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonParameterInfo_1_tEBAC5C927B94147B5A0F7688397F21E310DE6748 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t3975D446C7B759AD1B7D3E4B2E8E741A879AC17F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_TryRead_m027E3154D4F1EC35DCBFADDB9503F4BDA0A0632A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TArg_tABDBF2D60A145C630EC22349095CF36BCA2A8CDB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EnumConverter_1__ctor_m281E7F806D250781D1F10A6F1315679C7C811E9A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1__ctor_m57BC55941E8FA115AB9BC6537D85ECE65BB5BF5C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tFA8D936269CB4A76EC777061003CF7CEBD47D860 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EnumConverter_1_ConvertToUInt64_m26ABDC57FE53EAFBCDB92980543381D60E01A22F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EnumConverter_1_t3FA6695C8FDBFC1B036271AE0566957C32356834 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EnumConverter_1_FormatEnumValue_m78732064642FC15F876C5057F74B0C6F70ED26AB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tFA8D936269CB4A76EC777061003CF7CEBD47D860 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_ReadWithQuotes_m4E30C5357E7E6986C743D1989232D8B0789836A1 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_T_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46_Object_ToString_mF8AC1EB9D85AB52EC8FD8B8BDD131E855E69673F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EnumConverter_1_IsValidIdentifier_mCCDC485715150081D3F8ACBAF0D2EA3FD365B63E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EnumConverter_1_FormatEnumValueToString_m073420713EC426EDFBDB889446F12F68F09B6AFE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enum_TryParse_TisT_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46_m1D8EE4537A405DF61F2EC977F382E22BF7853FB3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enum_TryParse_TisT_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46_m6438782F4E9530066835954A2ED4FB270F85E8C4 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t8CDB4BFE366CCF3CF61A46650A3DAD1BE79FEE46 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1__ctor_m9F047AF4220A2E102CBB23A0F46C13194539ADB4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_t60424EEEA9AA40D4E10081DA7231C0DEA59CA78A },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t208D4602FC467225F066F1EF59C89104811E313B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_JsonConverter_1_tD7DDC1631442B3D97CD43D2B37448BA11F2EE1F1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Read_m807899C16CD5715C134BB67F1C3B052CD33E5E98 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Nullable_1_t5F08EED533FF4AE4B1C235CC4EF3BC93A41EFF4D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Nullable_1__ctor_mCB2082DD9053F4C2E6642BB7A6F331657469EC70 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Nullable_1_get_HasValue_mD87D4B638EA8F7BDAB3BA85AF53BD2EE80DC44DD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Nullable_1_get_Value_m28E21D2A2DCCC1C6D1036289CF78C4055657C118 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_Write_m143C9FF223EC87C820B6192FE369A02E6D31BA3A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_ReadNumberWithCustomHandling_m3375D982C493FAAC8EE104BFE28C6DBA6768DB19 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonConverter_1_WriteNumberWithCustomHandling_mECDF3FAC8A48DB8A096AA53A5E92F90F15DD3ADD },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Text_Json_CodeGenModule;
const Il2CppCodeGenModule g_System_Text_Json_CodeGenModule = 
{
	"System.Text.Json.dll",
	1568,
	s_methodPointers,
	292,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	60,
	s_rgctxIndices,
	600,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
