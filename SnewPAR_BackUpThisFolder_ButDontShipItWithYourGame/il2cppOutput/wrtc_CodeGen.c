﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CallApp::Awake()
extern void CallApp_Awake_m4F4DE17D537AABD0381F08DA5B29F368957CF709 (void);
// 0x00000002 System.Void CallApp::Start()
extern void CallApp_Start_m4AE3C7C5819727A0AFA2A05218DBA7B1DFEDA251 (void);
// 0x00000003 System.Void CallApp::OnCallFactoryReady()
extern void CallApp_OnCallFactoryReady_mFF37D74D4D3A14B782012FAA490FAF967E78E9F2 (void);
// 0x00000004 System.Void CallApp::OnCallFactoryFailed(System.String)
extern void CallApp_OnCallFactoryFailed_m2E0BBD202CCEE03BB37ADEEC33270630F5198348 (void);
// 0x00000005 System.Void CallApp::OnDestroy()
extern void CallApp_OnDestroy_mF7C79CEDB712B00FCB7A6AEAFD70715D927EAAEF (void);
// 0x00000006 System.Void CallApp::Update()
extern void CallApp_Update_m39BE0CBC1186FD711283DEB839DCE38C16B14B93 (void);
// 0x00000007 Byn.Awrtc.NetworkConfig CallApp::CreateNetworkConfig()
extern void CallApp_CreateNetworkConfig_m8A084222C30627BAA4FC71DA39F274ADFCB9C5AC (void);
// 0x00000008 System.Void CallApp::SetupCall()
extern void CallApp_SetupCall_m6359B6495B1E314E91E676F70B78CF7EC3980C9F (void);
// 0x00000009 Byn.Awrtc.ICall CallApp::CreateCall(Byn.Awrtc.NetworkConfig)
extern void CallApp_CreateCall_mC83044232C40DAEB3CDEBF8326262BBDA5562AD0 (void);
// 0x0000000A System.Void CallApp::Call_CallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void CallApp_Call_CallEvent_mB8749AA593D37943C5886813BA60F21C01576E56 (void);
// 0x0000000B System.Void CallApp::CleanupCall()
extern void CallApp_CleanupCall_m668FDEE82ABA12B9666EDE726442EFAA2B046040 (void);
// 0x0000000C Byn.Awrtc.MediaConfig CallApp::CreateMediaConfig()
extern void CallApp_CreateMediaConfig_mFE25D02EC086DD8D9272E26D726F11F7D611F84D (void);
// 0x0000000D System.Void CallApp::ResetCall()
extern void CallApp_ResetCall_m4318A1CA6BB048B8ED33C58ECF0A21378B6937A4 (void);
// 0x0000000E System.Void CallApp::StopAutoRejoin()
extern void CallApp_StopAutoRejoin_m267E7C00BFC9C8C8A24B66EBB9115C01F5B2EAD0 (void);
// 0x0000000F System.Void CallApp::TriggerRejoinTimer()
extern void CallApp_TriggerRejoinTimer_m8BC20C3F2847BDBA5AF417FBC33688EB9E1DC68E (void);
// 0x00000010 System.Void CallApp::InternalResetCall()
extern void CallApp_InternalResetCall_m678D2746B8460B795D84F757B3C4B38C42D885FB (void);
// 0x00000011 System.Void CallApp::SetRemoteVolume(System.Single)
extern void CallApp_SetRemoteVolume_mE603EAD0B925090EB9586311366E7944DF73150C (void);
// 0x00000012 System.String[] CallApp::GetVideoDevices()
extern void CallApp_GetVideoDevices_m29DA1B4F16929CFF9C2C8266BDB96F4C034D1DDA (void);
// 0x00000013 System.Boolean CallApp::CanSelectVideoDevice()
extern void CallApp_CanSelectVideoDevice_m16610AE08EBD8DA497CFB898EC2985D5857DAA2F (void);
// 0x00000014 System.Void CallApp::Join(System.String)
extern void CallApp_Join_m09AEEFC9253D1F6C7FB115588802EE68D2EFA4F2 (void);
// 0x00000015 System.Void CallApp::InternalJoin()
extern void CallApp_InternalJoin_mD4AC42F07FF6609E1555BAE14563CD38C8DE0428 (void);
// 0x00000016 System.Collections.IEnumerator CallApp::CoroutineRejoin()
extern void CallApp_CoroutineRejoin_mFAF3383FEBEEC85CC591F8EB2B48E6422A31023F (void);
// 0x00000017 System.Void CallApp::Send(System.String)
extern void CallApp_Send_m903F3A7FD9115F1F64896308CC5685E4413D73C6 (void);
// 0x00000018 System.Void CallApp::SetAudio(System.Boolean)
extern void CallApp_SetAudio_m7B606EBE2FCFC2990109BDB9F292EDA1D2DD8CE2 (void);
// 0x00000019 System.Void CallApp::SetVideo(System.Boolean)
extern void CallApp_SetVideo_m43B467BA8FCDA45438E816112BDEFBF0F4FD9770 (void);
// 0x0000001A System.Void CallApp::SetVideoDevice(System.String)
extern void CallApp_SetVideoDevice_m7F2FB84729977857B2BF0E57BDB7CA7BF1C46B48 (void);
// 0x0000001B System.Void CallApp::SetIdealResolution(System.Int32,System.Int32)
extern void CallApp_SetIdealResolution_m0861F2671347FFB3BE67FC1A70AA7A45EC42A556 (void);
// 0x0000001C System.Void CallApp::SetIdealFps(System.Int32)
extern void CallApp_SetIdealFps_m891F1FFB9368E6747F3EB567B15E903423349FDA (void);
// 0x0000001D System.Void CallApp::SetShowLocalVideo(System.Boolean)
extern void CallApp_SetShowLocalVideo_m8E974DB781539BD07232C9AE855D20431796D901 (void);
// 0x0000001E System.Void CallApp::SetAutoRejoin(System.Boolean,System.Single)
extern void CallApp_SetAutoRejoin_mCB386299BAC53D59BC58DB883361096D7FC63FE8 (void);
// 0x0000001F System.Boolean CallApp::GetLoudspeakerStatus()
extern void CallApp_GetLoudspeakerStatus_m327E37EFAE8499CD38898B0343CBFC87916D5B69 (void);
// 0x00000020 System.Void CallApp::SetLoudspeakerStatus(System.Boolean)
extern void CallApp_SetLoudspeakerStatus_m8EDD40050CD5CCDB57DC6FE00C5B2AD73518333D (void);
// 0x00000021 System.Void CallApp::SetMute(System.Boolean)
extern void CallApp_SetMute_m76883C41681C6B319A0B07D2615A0F67DF018669 (void);
// 0x00000022 System.Boolean CallApp::IsMute()
extern void CallApp_IsMute_m7E5216F00A08CF9998E3BAEC027D946FC05FA954 (void);
// 0x00000023 System.Void CallApp::UpdateFrame(Byn.Awrtc.FrameUpdateEventArgs)
extern void CallApp_UpdateFrame_m1729D9729D94D4614D3FFDFA2D83F936DE24D83A (void);
// 0x00000024 System.Void CallApp::Append(System.String)
extern void CallApp_Append_mE8F87CCB6B47370505FCB27998D74989074CBAB8 (void);
// 0x00000025 System.Void CallApp::.ctor()
extern void CallApp__ctor_m1B0CD2D693D05AD0B8B8D88212C017BC3B871D32 (void);
// 0x00000026 System.Void CallApp/<CoroutineRejoin>d__40::.ctor(System.Int32)
extern void U3CCoroutineRejoinU3Ed__40__ctor_m351C25BBEDE56AB148977AF75EBFD5CFE2F8206A (void);
// 0x00000027 System.Void CallApp/<CoroutineRejoin>d__40::System.IDisposable.Dispose()
extern void U3CCoroutineRejoinU3Ed__40_System_IDisposable_Dispose_m9EAD48BC8B4CE80C4C3297D88460F25CF3ED7FF0 (void);
// 0x00000028 System.Boolean CallApp/<CoroutineRejoin>d__40::MoveNext()
extern void U3CCoroutineRejoinU3Ed__40_MoveNext_m3094DBB2EE6BDE13DBD41C1AA6D2A56ACAF28207 (void);
// 0x00000029 System.Object CallApp/<CoroutineRejoin>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineRejoinU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3397DB9943C9F264D5C374F758B93916F1020BEA (void);
// 0x0000002A System.Void CallApp/<CoroutineRejoin>d__40::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineRejoinU3Ed__40_System_Collections_IEnumerator_Reset_mCF0B2E6416765806DC029A45659AB7A76E38C5AB (void);
// 0x0000002B System.Object CallApp/<CoroutineRejoin>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineRejoinU3Ed__40_System_Collections_IEnumerator_get_Current_mB9DAA9C9A4E1CBB1D6BC538BA79492E274116F0E (void);
// 0x0000002C System.Void CallAppUi::Awake()
extern void CallAppUi_Awake_mE0543A35B69AA16D12DA1A2C10F2FD2049FAC27A (void);
// 0x0000002D System.Void CallAppUi::Start()
extern void CallAppUi_Start_m47CF5AF8FB50C6E5DC8438CE4666205DAB61116E (void);
// 0x0000002E System.Void CallAppUi::SaveSettings()
extern void CallAppUi_SaveSettings_mC9C02BC93F796E671D1DA7FB5D738BA3E9C3535C (void);
// 0x0000002F System.Void CallAppUi::LoadSettings()
extern void CallAppUi_LoadSettings_m4F88D7932AA5809C07626A778EB0CA1244A36AD3 (void);
// 0x00000030 System.Void CallAppUi::ResetSettings()
extern void CallAppUi_ResetSettings_m755784E26B724E42B8B205ECB8FB656446CAE3A5 (void);
// 0x00000031 System.Void CallAppUi::CheckSettings()
extern void CallAppUi_CheckSettings_mD8BA99B08FE4D4B207A84C57888D8EA56218CDF5 (void);
// 0x00000032 System.Void CallAppUi::OnAudioSettingsChanged()
extern void CallAppUi_OnAudioSettingsChanged_m841EED7F453C3CC65997B6924AF954F5591245AE (void);
// 0x00000033 System.Void CallAppUi::OnVideoSettingsChanged()
extern void CallAppUi_OnVideoSettingsChanged_m5160C3EC04C40F5C0BDA51B1B4650C668BE9555C (void);
// 0x00000034 System.Collections.IEnumerator CallAppUi::RequestAudioPermissions()
extern void CallAppUi_RequestAudioPermissions_mE29840136497FB533ED651EDF6033991A9B26D96 (void);
// 0x00000035 System.Collections.IEnumerator CallAppUi::RequestVideoPermissions()
extern void CallAppUi_RequestVideoPermissions_mDE742C626FF5A07095105A476A8B054F01652FC0 (void);
// 0x00000036 System.Boolean CallAppUi::PlayerPrefsGetBool(System.String,System.Boolean)
extern void CallAppUi_PlayerPrefsGetBool_m0EF91E09F1C39844BC06A221E01D20EF9097A50D (void);
// 0x00000037 System.Void CallAppUi::PlayerPrefsSetBool(System.String,System.Boolean)
extern void CallAppUi_PlayerPrefsSetBool_m3CFBE099190608FEBE250B35C8A67657626E22B9 (void);
// 0x00000038 System.String CallAppUi::GetSelectedVideoDevice()
extern void CallAppUi_GetSelectedVideoDevice_m402A489A10BF0750111A7C268F285CDBC659C388 (void);
// 0x00000039 System.Int32 CallAppUi::TryParseInt(System.String,System.Int32)
extern void CallAppUi_TryParseInt_m94CBBB8286D3D1F0347B08B1A79972232711916C (void);
// 0x0000003A System.Void CallAppUi::SetupCallApp()
extern void CallAppUi_SetupCallApp_mBB70F8D62EC81E0A3AE6FACB45AF2F5013D6A47A (void);
// 0x0000003B System.Void CallAppUi::ToggleSettings()
extern void CallAppUi_ToggleSettings_m1609F42DFC816B9E64EC3D7983D7BBACFF532047 (void);
// 0x0000003C System.Void CallAppUi::ToggleSetup()
extern void CallAppUi_ToggleSetup_m99439C79D0F17BBB92964E85DAA53AC2B200E956 (void);
// 0x0000003D System.Void CallAppUi::UpdateLocalTexture(Byn.Awrtc.IFrame,Byn.Awrtc.FramePixelFormat)
extern void CallAppUi_UpdateLocalTexture_mA3C43A67BBA410BA69A22374DCA5A6BBCC90CB7B (void);
// 0x0000003E System.Void CallAppUi::UpdateRemoteTexture(Byn.Awrtc.IFrame,Byn.Awrtc.FramePixelFormat)
extern void CallAppUi_UpdateRemoteTexture_mAB684398EBB0C79551A8EC4A150C41876D96F732 (void);
// 0x0000003F System.Void CallAppUi::UpdateVideoDropdown()
extern void CallAppUi_UpdateVideoDropdown_mC6DDBF93338E9EEB06D8E281355ACC306DE984AE (void);
// 0x00000040 System.Void CallAppUi::VideoDropdownOnValueChanged(System.Int32)
extern void CallAppUi_VideoDropdownOnValueChanged_m3A852CC926553F05E52CD195761A9F66E69BDE49 (void);
// 0x00000041 System.Void CallAppUi::Append(System.String)
extern void CallAppUi_Append_m2F1CCA542A0BD13C51E43E5169CF06BFA35703C4 (void);
// 0x00000042 System.Void CallAppUi::SetFullscreen(System.Boolean)
extern void CallAppUi_SetFullscreen_mFCE504D4300010F59200147926138B9E5705BBD6 (void);
// 0x00000043 System.Void CallAppUi::Fullscreen()
extern void CallAppUi_Fullscreen_m7DB27478ABE00FB8674F32789AFBC84748FD0BAD (void);
// 0x00000044 System.Void CallAppUi::ShowOverlay()
extern void CallAppUi_ShowOverlay_mDABCA28F98418A92E8F22656353DDF8D936B461D (void);
// 0x00000045 System.Void CallAppUi::SetGuiState(System.Boolean)
extern void CallAppUi_SetGuiState_mD440C6A738E8E5D79D2BE673AACFC4ACF810BAAF (void);
// 0x00000046 System.Void CallAppUi::JoinButtonPressed()
extern void CallAppUi_JoinButtonPressed_m0C0E1B114260614B31C891CBB9288AA33C70D1D0 (void);
// 0x00000047 System.Void CallAppUi::EnsureLength()
extern void CallAppUi_EnsureLength_m30E718EECE72A84984C095400854C612FE3C1E8C (void);
// 0x00000048 System.String CallAppUi::GetRoomname()
extern void CallAppUi_GetRoomname_m5F7EBC3EED44D4007E5923D385EDF382A81F85DF (void);
// 0x00000049 System.Void CallAppUi::SendButtonPressed()
extern void CallAppUi_SendButtonPressed_m5E99B0A23C94FBCAE69C3E8B19294A1B12CCD738 (void);
// 0x0000004A System.Void CallAppUi::InputOnEndEdit()
extern void CallAppUi_InputOnEndEdit_m13FF8E315F542F40775E820D5D1DED284343D454 (void);
// 0x0000004B System.Void CallAppUi::SendMsg(System.String)
extern void CallAppUi_SendMsg_mA41DD37F50A048F0E8E7151372939B1187253E54 (void);
// 0x0000004C System.Void CallAppUi::ShutdownButtonPressed()
extern void CallAppUi_ShutdownButtonPressed_m771B252752301D7824A58BB27C274A58EFEC520F (void);
// 0x0000004D System.Void CallAppUi::OnVolumeChanged(System.Single)
extern void CallAppUi_OnVolumeChanged_m5A26A08C7418267440F410A0F624BFA148B9CF7C (void);
// 0x0000004E System.Void CallAppUi::OnLoudspeakerToggle()
extern void CallAppUi_OnLoudspeakerToggle_mE5F8A6E50334E5A574202511B26C3F9B89913704 (void);
// 0x0000004F System.Void CallAppUi::RefreshLoudspeakerToggle()
extern void CallAppUi_RefreshLoudspeakerToggle_m712D3B16AAA902B49BA3F9113F35A317ACBEDF04 (void);
// 0x00000050 System.Void CallAppUi::OnMuteToggle()
extern void CallAppUi_OnMuteToggle_m204348599E9B9CCADE707B6CB788711E81B7CC94 (void);
// 0x00000051 System.Void CallAppUi::RefreshMuteToggle()
extern void CallAppUi_RefreshMuteToggle_m4CF146AB1DB7AC17BD735466B16AC944C0F89C2A (void);
// 0x00000052 System.Void CallAppUi::Update()
extern void CallAppUi_Update_m6179B9422FF0881A8FE5811FEDBB6259934B21CF (void);
// 0x00000053 System.Void CallAppUi::.ctor()
extern void CallAppUi__ctor_mD445E97B6CC9DF1F0CE5873C3FCBFF3D9F4E6736 (void);
// 0x00000054 System.Void CallAppUi::.cctor()
extern void CallAppUi__cctor_mA88D97F17785507D67D6F08D17503165061E7ACC (void);
// 0x00000055 System.Void CallAppUi/<RequestAudioPermissions>d__74::.ctor(System.Int32)
extern void U3CRequestAudioPermissionsU3Ed__74__ctor_m23AFECA2492A62EE362B67024439DCF012EF4058 (void);
// 0x00000056 System.Void CallAppUi/<RequestAudioPermissions>d__74::System.IDisposable.Dispose()
extern void U3CRequestAudioPermissionsU3Ed__74_System_IDisposable_Dispose_mB11B7CE1FC5AEF6E0E31868CA1FD107D1BFBEF85 (void);
// 0x00000057 System.Boolean CallAppUi/<RequestAudioPermissions>d__74::MoveNext()
extern void U3CRequestAudioPermissionsU3Ed__74_MoveNext_mEF81087ED6FF2BDB23F1F5E46250A484CDFB7E9B (void);
// 0x00000058 System.Object CallAppUi/<RequestAudioPermissions>d__74::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestAudioPermissionsU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3DED2F431ADBF548BA0CE1AD64246FB398EA0C12 (void);
// 0x00000059 System.Void CallAppUi/<RequestAudioPermissions>d__74::System.Collections.IEnumerator.Reset()
extern void U3CRequestAudioPermissionsU3Ed__74_System_Collections_IEnumerator_Reset_m271F09A42DD92FA6B1631B9AD0A311563BCF4D45 (void);
// 0x0000005A System.Object CallAppUi/<RequestAudioPermissions>d__74::System.Collections.IEnumerator.get_Current()
extern void U3CRequestAudioPermissionsU3Ed__74_System_Collections_IEnumerator_get_Current_m0A8B537BC98AF46FF83C8BEEAA45942B6B13586C (void);
// 0x0000005B System.Void CallAppUi/<RequestVideoPermissions>d__75::.ctor(System.Int32)
extern void U3CRequestVideoPermissionsU3Ed__75__ctor_mA5CFB0928E7E8474156819722C2F8070D6EA103E (void);
// 0x0000005C System.Void CallAppUi/<RequestVideoPermissions>d__75::System.IDisposable.Dispose()
extern void U3CRequestVideoPermissionsU3Ed__75_System_IDisposable_Dispose_mDE20F2B67099B4C04BDD4963937541FEDFC8198A (void);
// 0x0000005D System.Boolean CallAppUi/<RequestVideoPermissions>d__75::MoveNext()
extern void U3CRequestVideoPermissionsU3Ed__75_MoveNext_m9C3E79F1A4581AFEB6AC3767F9876559969CC245 (void);
// 0x0000005E System.Object CallAppUi/<RequestVideoPermissions>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestVideoPermissionsU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m628786529EB8B6B4E161FB8C590A630D772F20BF (void);
// 0x0000005F System.Void CallAppUi/<RequestVideoPermissions>d__75::System.Collections.IEnumerator.Reset()
extern void U3CRequestVideoPermissionsU3Ed__75_System_Collections_IEnumerator_Reset_m539DD620E1337B8B2F6450C51ECDB57D31CA61DE (void);
// 0x00000060 System.Object CallAppUi/<RequestVideoPermissions>d__75::System.Collections.IEnumerator.get_Current()
extern void U3CRequestVideoPermissionsU3Ed__75_System_Collections_IEnumerator_get_Current_m21E160A03571ACDA2D7597C5E2575ADAC1C7582B (void);
// 0x00000061 System.Void ImgFitter::Update()
extern void ImgFitter_Update_m7F85A642FA5BD5915DEF2D816236999CBA475EEA (void);
// 0x00000062 UnityEngine.Vector2 ImgFitter::Abs(UnityEngine.Vector2)
extern void ImgFitter_Abs_m5CB3F95BDA653E57EE91CCAF31130E3FE4F80982 (void);
// 0x00000063 System.Void ImgFitter::.ctor()
extern void ImgFitter__ctor_m8978F2C18CCD6FB3FA4CDFFE96FAA5EB2E7149F2 (void);
// 0x00000064 System.Void VideoPanelEventHandler::Start()
extern void VideoPanelEventHandler_Start_m9D72F720286849D3C206B7B05D324FA5D9458C68 (void);
// 0x00000065 System.Void VideoPanelEventHandler::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void VideoPanelEventHandler_OnPointerClick_m808D8DADB22B9F0E98FC7829FE78C2834D5FBC4C (void);
// 0x00000066 System.Void VideoPanelEventHandler::.ctor()
extern void VideoPanelEventHandler__ctor_m118572D8D56EB86C0A87E4DD44364C93518EB731 (void);
// 0x00000067 System.Void ChatApp::Start()
extern void ChatApp_Start_m10342CA0C702C01BF3B8B272BCD494923A8EAF09 (void);
// 0x00000068 System.Void ChatApp::OnCallFactoryReady()
extern void ChatApp_OnCallFactoryReady_m30155DB49D427ED648EC58C3C2296B11E0A160B0 (void);
// 0x00000069 System.Void ChatApp::OnCallFactoryFailed(System.String)
extern void ChatApp_OnCallFactoryFailed_m8443155627E90BD9F9D30FA0841E7AE27B2C5F97 (void);
// 0x0000006A System.Void ChatApp::Setup()
extern void ChatApp_Setup_mF58ECF671400B027E46F6AD7A43B10EE6C89B93E (void);
// 0x0000006B System.Void ChatApp::ResetApp()
extern void ChatApp_ResetApp_m0FE8FD820684E867ACC1BEE6740C0DB43F15E8D1 (void);
// 0x0000006C System.Void ChatApp::Cleanup()
extern void ChatApp_Cleanup_mB03126342565D15125B0469E0582FD590BAB2CFC (void);
// 0x0000006D System.Void ChatApp::OnDestroy()
extern void ChatApp_OnDestroy_m3BA70BB113D4F8494E965660768FD17C72D5DB3C (void);
// 0x0000006E System.Void ChatApp::FixedUpdate()
extern void ChatApp_FixedUpdate_m7341787D8A4E850BEE290D1AE078E091CBA0278D (void);
// 0x0000006F System.Void ChatApp::HandleNetwork()
extern void ChatApp_HandleNetwork_mF88EC8D9F769F8AB427A9EB4F95565952C3E9B03 (void);
// 0x00000070 System.Void ChatApp::HandleIncommingMessage(Byn.Awrtc.NetworkEvent&)
extern void ChatApp_HandleIncommingMessage_mD0DD4D5FD4A3092BBA52CB4966AD89C2C55F15D1 (void);
// 0x00000071 System.Void ChatApp::SendString(System.String,System.Boolean)
extern void ChatApp_SendString_mE4F66EA5239667018ECC4DEE2315A164C5D91D16 (void);
// 0x00000072 System.Void ChatApp::Append(System.String)
extern void ChatApp_Append_m0DA1B3709C8DCF8CF86636BA6E9904DBFB7A8190 (void);
// 0x00000073 System.Void ChatApp::SetGuiState(System.Boolean)
extern void ChatApp_SetGuiState_m0059FBA4A193C363BA47071A8B8BA8D92A413930 (void);
// 0x00000074 System.Void ChatApp::JoinRoomButtonPressed()
extern void ChatApp_JoinRoomButtonPressed_mD007F07597FA0C4F6A06019FF8DC99B73799400D (void);
// 0x00000075 System.Void ChatApp::EnsureLength()
extern void ChatApp_EnsureLength_mA7890B0024593F26F83974E72834A87F3E271712 (void);
// 0x00000076 System.Void ChatApp::LeaveButtonPressed()
extern void ChatApp_LeaveButtonPressed_mF89CEF834EE2F7C35B1BF1EB3EACBA506767CE96 (void);
// 0x00000077 System.Void ChatApp::OpenRoomButtonPressed()
extern void ChatApp_OpenRoomButtonPressed_mFF56C0C52F732D191FBEC9C6C41784B218A10684 (void);
// 0x00000078 System.Void ChatApp::InputOnEndEdit()
extern void ChatApp_InputOnEndEdit_mCD79A0ED800333D5A35653B8EA45F096D8A66872 (void);
// 0x00000079 System.Void ChatApp::SendButtonPressed()
extern void ChatApp_SendButtonPressed_mC6E628A0BBDAC5BA359A4EDF072105FE81642E6E (void);
// 0x0000007A System.Void ChatApp::.ctor()
extern void ChatApp__ctor_mF65D6A54D4DD6871CFD59038753B681380B4CD28 (void);
// 0x0000007B System.Void MessageList::Awake()
extern void MessageList_Awake_m15E3852FEFB42CC4D2D319551513EE9FBCB83DB1 (void);
// 0x0000007C System.Void MessageList::Start()
extern void MessageList_Start_mEACD5D23B4055B9B9ED1FAE776CF02D39FB9144C (void);
// 0x0000007D System.Void MessageList::AddTextEntry(System.String)
extern void MessageList_AddTextEntry_m4FE8154758B8DF8B4BB25F06DF42E5B189B97E63 (void);
// 0x0000007E System.Void MessageList::Update()
extern void MessageList_Update_m8EA59310D0B5DF1260371550510D0A7D58AB7B62 (void);
// 0x0000007F System.Void MessageList::.ctor()
extern void MessageList__ctor_m076548586A36E2A50D962F8CE581B91E62D933EE (void);
// 0x00000080 System.Void GridManager::Update()
extern void GridManager_Update_m4A32CF8AD6BDEABC921341D743BE2AA9C7518870 (void);
// 0x00000081 System.Void GridManager::Refresh()
extern void GridManager_Refresh_m3C9181FCAA9F6F580474D9DE70E1982AF7F31F15 (void);
// 0x00000082 System.Void GridManager::.ctor()
extern void GridManager__ctor_mDABE66EFE2CB611D4A6FDDC4BA2BB68B763037BC (void);
// 0x00000083 System.Void MenuScript::Awake()
extern void MenuScript_Awake_mB15D4611A6309D5EBE1D72AC5412B9B0D6F958FF (void);
// 0x00000084 System.Void MenuScript::Start()
extern void MenuScript_Start_mC5C1984E8FE4A894ED0E2DE2A13EC4098CC42107 (void);
// 0x00000085 System.Void MenuScript::PrintDeviceDebug()
extern void MenuScript_PrintDeviceDebug_m593DC9320D29AC8ABEC9DB3E70AE78192DB6C53A (void);
// 0x00000086 System.Void MenuScript::Update()
extern void MenuScript_Update_mD9979BD21636C05C48C221CA8A0CA4E9E5463354 (void);
// 0x00000087 System.Void MenuScript::Initialize()
extern void MenuScript_Initialize_mAEB66998D3459C12E8438B2D410A4BE779DCC2CD (void);
// 0x00000088 System.Void MenuScript::ExampleUi()
extern void MenuScript_ExampleUi_mCAE051D785C0F2E2E08CBAF17CB1CE08F1BDD8AC (void);
// 0x00000089 System.Void MenuScript::MenuUi()
extern void MenuScript_MenuUi_m3F00E183872214AD0B3A043C279D8C4D9F4962DD (void);
// 0x0000008A System.Void MenuScript::LoadExample(System.String)
extern void MenuScript_LoadExample_mECACD81713E828CC75F0D1EBC687B18C01740A3B (void);
// 0x0000008B System.Void MenuScript::LoadChatExample()
extern void MenuScript_LoadChatExample_m6CE22DF0B824432CC32A7F2E791BB40D26421CF9 (void);
// 0x0000008C System.Void MenuScript::LoadCallExample()
extern void MenuScript_LoadCallExample_mD1096AE0204895C613F259ED96F8CA509AA17EBA (void);
// 0x0000008D System.Void MenuScript::LoadConference()
extern void MenuScript_LoadConference_m21668389A5DF95469ADFD3C59546D8C0C0E02038 (void);
// 0x0000008E System.Void MenuScript::LoadMenu()
extern void MenuScript_LoadMenu_m54563261B39716CF68A02B524AA0C8D05D9F49A9 (void);
// 0x0000008F System.Void MenuScript::.ctor()
extern void MenuScript__ctor_m19057CB72315C7560F0E5B908359256160E54654 (void);
// 0x00000090 System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsFrontFacing(System.String)
extern void AndroidHelper_IsFrontFacing_mE78E567474B7E94F9A4192AE4B018B8EBA195B4E (void);
// 0x00000091 System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsBackFacing(System.String)
extern void AndroidHelper_IsBackFacing_m9FD15B8C57A0756A8EF64DC07E71C977C7301543 (void);
// 0x00000092 System.Void Byn.Awrtc.Unity.AndroidHelper::SetSpeakerOn(System.Boolean)
extern void AndroidHelper_SetSpeakerOn_mD1A2D13722145BADDD9B110BD203FD18FBC6008B (void);
// 0x00000093 System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsSpeakerOn()
extern void AndroidHelper_IsSpeakerOn_mDDD004F8ED629C86CADD16518D983438CDCCB181 (void);
// 0x00000094 System.Int32 Byn.Awrtc.Unity.AndroidHelper::GetMode()
extern void AndroidHelper_GetMode_m903BE309103B9E80289DBCE5A2C3FA956A9A302D (void);
// 0x00000095 System.Void Byn.Awrtc.Unity.AndroidHelper::SetMode(System.Int32)
extern void AndroidHelper_SetMode_m9AEAF40FAED6AA6A287D821BDA6482DB63EAB803 (void);
// 0x00000096 System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsModeInCommunication()
extern void AndroidHelper_IsModeInCommunication_m39839F303CA0E03DF4CD7D02F69CE2A97F37C34C (void);
// 0x00000097 System.Void Byn.Awrtc.Unity.AndroidHelper::SetModeInCommunicaion()
extern void AndroidHelper_SetModeInCommunicaion_mC4021AEA86C4AA32FE964F1FA9A436A2226F4AE2 (void);
// 0x00000098 System.Int32 Byn.Awrtc.Unity.AndroidHelper::GetAudioManagerMode()
extern void AndroidHelper_GetAudioManagerMode_m8C67C20CBF9BC580F9502031BFCA210925A4CD16 (void);
// 0x00000099 System.Void Byn.Awrtc.Unity.AndroidHelper::SetModeNormal()
extern void AndroidHelper_SetModeNormal_mB8B5DFECED413B87C6AC4AC571F844CC7A2271EF (void);
// 0x0000009A System.Int32 Byn.Awrtc.Unity.AndroidHelper::GetStreamVolume()
extern void AndroidHelper_GetStreamVolume_m49D3ADBDD2C5489E576848A32B4739794C96B11E (void);
// 0x0000009B System.Void Byn.Awrtc.Unity.AndroidHelper::SetStreamVolume(System.Int32)
extern void AndroidHelper_SetStreamVolume_mA6198C2E82ED9ADA3E1E03E1C61FC8D7756D074A (void);
// 0x0000009C System.Void Byn.Awrtc.Unity.AndroidHelper::SetMute(System.Boolean)
extern void AndroidHelper_SetMute_m97057E15C1267203069BD7CD45AE0AFA0EEE0DA0 (void);
// 0x0000009D System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsMute()
extern void AndroidHelper_IsMute_m3306C12AA6D6B1C6EC1FCA9D941D2153B1F8001F (void);
// 0x0000009E UnityEngine.AndroidJavaObject Byn.Awrtc.Unity.AndroidHelper::GetAudioManager()
extern void AndroidHelper_GetAudioManager_m26BE8F9FA9A36F910971C18396516F6E293B7970 (void);
// 0x0000009F UnityEngine.AndroidJavaObject Byn.Awrtc.Unity.AndroidHelper::GetActivity()
extern void AndroidHelper_GetActivity_mFA6D683726C8049824CF96305F78D46AD1F4BE15 (void);
// 0x000000A0 System.Int32 Byn.Awrtc.Unity.AndroidHelper::GetAudioManagerFlag(System.String)
extern void AndroidHelper_GetAudioManagerFlag_mA44CBA316D5EEBE827C547385376255E839F1C06 (void);
// 0x000000A1 System.Boolean Byn.Awrtc.Unity.AndroidHelper::CheckPermissionMicrophone()
extern void AndroidHelper_CheckPermissionMicrophone_m34144495AB2BB6A3B84DDFE001522B627A1F2189 (void);
// 0x000000A2 System.Boolean Byn.Awrtc.Unity.AndroidHelper::CheckPermissionCamera()
extern void AndroidHelper_CheckPermissionCamera_m5D0B45707C10B44DBB07158E325C31A22DA63A38 (void);
// 0x000000A3 System.Boolean Byn.Awrtc.Unity.AndroidHelper::CheckPermissionAudioSettings()
extern void AndroidHelper_CheckPermissionAudioSettings_m7EBF13EF420BE7CBC1044115F14B8785606CBDB4 (void);
// 0x000000A4 System.Boolean Byn.Awrtc.Unity.AndroidHelper::CheckPermissionNetwork()
extern void AndroidHelper_CheckPermissionNetwork_mAFDD3F80D21FF09470002137B5B9A0BCA2C8B134 (void);
// 0x000000A5 System.Boolean Byn.Awrtc.Unity.AndroidHelper::HasRuntimePermissions()
extern void AndroidHelper_HasRuntimePermissions_mA0B3E37D50D43D2994BF130579144D88FEB2B64A (void);
// 0x000000A6 System.Void Byn.Awrtc.Unity.AndroidHelper::RequestPermissions(System.Boolean,System.Boolean,System.Boolean,System.Int32)
extern void AndroidHelper_RequestPermissions_m000FE65602C7A07F46583ACEA18AB0C5EE2E2220 (void);
// 0x000000A7 System.Void Byn.Awrtc.Unity.AndroidHelper::OpenPermissionView()
extern void AndroidHelper_OpenPermissionView_m5C119111578F3DFC1D08508A8BC9E9CE893016A0 (void);
// 0x000000A8 System.Void Byn.Awrtc.Unity.AndroidHelper::setBluetoothScoOn(System.Boolean)
extern void AndroidHelper_setBluetoothScoOn_mA9C3C7CA990904A1092B29D5482E15B466466306 (void);
// 0x000000A9 System.Void Byn.Awrtc.Unity.AndroidHelper::startBluetoothSco()
extern void AndroidHelper_startBluetoothSco_m285F41C7EEB192DA622B50FECECA66E0ACD95755 (void);
// 0x000000AA System.Void Byn.Awrtc.Unity.AndroidHelper::stopBluetoothSco()
extern void AndroidHelper_stopBluetoothSco_mE7040328537784D2D42A2FF766769574157FE8D0 (void);
// 0x000000AB System.Boolean Byn.Awrtc.Unity.AndroidHelper::IsHeadsetOn()
extern void AndroidHelper_IsHeadsetOn_m413157C5F37D2F1BCA156DCDFD0FADE23490A4C1 (void);
// 0x000000AC System.Void Byn.Awrtc.Unity.AndroidHelper::SetBluetoothOn(System.Boolean)
extern void AndroidHelper_SetBluetoothOn_mBCBB3C5347988CAEAF7A752B385FB5A5006001A1 (void);
// 0x000000AD System.Void Byn.Awrtc.Unity.AndroidHelper::.ctor()
extern void AndroidHelper__ctor_m8C08EFD99660DC66FA93DB4EC78ABC722220A352 (void);
// 0x000000AE System.Void Byn.Awrtc.Unity.AndroidHelper::.cctor()
extern void AndroidHelper__cctor_m1B58702CCCFE917ACC9A9CB0E94F669E8D7DE5F5 (void);
// 0x000000AF System.Void Byn.Awrtc.Unity.UnityHelper::PtrFromColor32(UnityEngine.Color32[],System.Action`2<System.IntPtr,System.UInt32>)
extern void UnityHelper_PtrFromColor32_m6343D6747A963C70E598A40A651A4327CD92FC66 (void);
// 0x000000B0 Byn.Awrtc.Unity.UnityCallFactory Byn.Awrtc.Unity.UnityCallFactory::get_Instance()
extern void UnityCallFactory_get_Instance_m369C480ACF81C431FE8975235D48B868B1D2C29F (void);
// 0x000000B1 System.Void Byn.Awrtc.Unity.UnityCallFactory::AddInitCallbacks(System.Action,System.Action`1<System.String>)
extern void UnityCallFactory_AddInitCallbacks_mDC8B59B10F02A843A84A82812E2CD06AAA1A645A (void);
// 0x000000B2 System.Void Byn.Awrtc.Unity.UnityCallFactory::EnsureInit(System.Action)
extern void UnityCallFactory_EnsureInit_mA557FD93F048A79B3193E9105F31E756F09F51B1 (void);
// 0x000000B3 System.Void Byn.Awrtc.Unity.UnityCallFactory::EnsureInit(System.Action,System.Action`1<System.String>)
extern void UnityCallFactory_EnsureInit_m6F16468E64FD542B4CF5211F63C177BEFFA1597D (void);
// 0x000000B4 System.Void Byn.Awrtc.Unity.UnityCallFactory::Init()
extern void UnityCallFactory_Init_m71EAA6ABF8601E962B8CC15CCF80CD2AE1A3D9B3 (void);
// 0x000000B5 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerOnInitFailed(System.Exception)
extern void UnityCallFactory_TriggerOnInitFailed_mDA4078FE7C3F2549C5E447E4D5681546972E6F23 (void);
// 0x000000B6 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerInitFailed(System.String)
extern void UnityCallFactory_TriggerInitFailed_mE09E2A95F8114758E0AEB1B32CDE2AF73AA2BF4D (void);
// 0x000000B7 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerInitSuccess()
extern void UnityCallFactory_TriggerInitSuccess_mD1CD3B233C33268CE88EA777EB3E6450E2D96E7E (void);
// 0x000000B8 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerFailedCallback(System.Action`1<System.String>,System.String)
extern void UnityCallFactory_TriggerFailedCallback_m06A211E755217F05246976DDC600FB6729184B82 (void);
// 0x000000B9 System.Void Byn.Awrtc.Unity.UnityCallFactory::TriggerSuccessCallback(System.Action)
extern void UnityCallFactory_TriggerSuccessCallback_mC458DCD62DAA59E96FEC0B8D6A20CD705E62E0A4 (void);
// 0x000000BA System.Collections.IEnumerator Byn.Awrtc.Unity.UnityCallFactory::CoroutineInitAsync()
extern void UnityCallFactory_CoroutineInitAsync_m413E1A9C5DA0124BAE50A78478ADB19534AF4065 (void);
// 0x000000BB System.Void Byn.Awrtc.Unity.UnityCallFactory::CreateSingleton()
extern void UnityCallFactory_CreateSingleton_m7DC4AECCF4CFC42AC1CDE08801DF62B385D3040A (void);
// 0x000000BC System.Boolean Byn.Awrtc.Unity.UnityCallFactory::InitStatic()
extern void UnityCallFactory_InitStatic_m3820F22CF15B2AD71FF9701616D9C55235F649AD (void);
// 0x000000BD Byn.Awrtc.Unity.UnityCallFactory/InitStatusUpdate Byn.Awrtc.Unity.UnityCallFactory::IsInitStaticComplete()
extern void UnityCallFactory_IsInitStaticComplete_m19C8C6330B26C5A835F69654E259506F36A7E100 (void);
// 0x000000BE System.Void Byn.Awrtc.Unity.UnityCallFactory::InitObj()
extern void UnityCallFactory_InitObj_m337891799CC4CCBC3D74792E35BCC5DBA15F0EFC (void);
// 0x000000BF System.Void Byn.Awrtc.Unity.UnityCallFactory::OnDestroy()
extern void UnityCallFactory_OnDestroy_mCB711593EABB4F7026F3428857B6E5E2A82C3AFA (void);
// 0x000000C0 Byn.Awrtc.IAwrtcFactory Byn.Awrtc.Unity.UnityCallFactory::get_InternalFactory()
extern void UnityCallFactory_get_InternalFactory_mD1C17870B4991D8BBFD91A9A461B68B0CECC2ABC (void);
// 0x000000C1 Byn.Awrtc.Native.NativeVideoInput Byn.Awrtc.Unity.UnityCallFactory::get_VideoInput()
extern void UnityCallFactory_get_VideoInput_mF4DDA7AADC21C08BE65EF8D5126E442F6BAF9EF1 (void);
// 0x000000C2 System.Void Byn.Awrtc.Unity.UnityCallFactory::Awake()
extern void UnityCallFactory_Awake_m919C33A9C84BAAFE99385BDE2CD1F7762774B91B (void);
// 0x000000C3 System.Void Byn.Awrtc.Unity.UnityCallFactory::Start()
extern void UnityCallFactory_Start_mA123D87B4BDD12B7230C13AC597585CDA5CB36A0 (void);
// 0x000000C4 System.Void Byn.Awrtc.Unity.UnityCallFactory::Update()
extern void UnityCallFactory_Update_m28A0D658E0FB3AB05F54A48FB67FA370B5DA3EAB (void);
// 0x000000C5 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::IsNativePlatform()
extern void UnityCallFactory_IsNativePlatform_mAF9853157A51A70AEFB0EF8C0173BCD76EBE2C9B (void);
// 0x000000C6 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::StaticInitBrowser()
extern void UnityCallFactory_StaticInitBrowser_m891BD2D0AAA73FD55E179ED473592548FD0C0582 (void);
// 0x000000C7 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::StaticInitAndroid()
extern void UnityCallFactory_StaticInitAndroid_mB3F4D1F15C6CEC3EB70F2256EDA0566C7C92BC38 (void);
// 0x000000C8 Byn.Awrtc.ICall Byn.Awrtc.Unity.UnityCallFactory::Create(Byn.Awrtc.NetworkConfig)
extern void UnityCallFactory_Create_m70FC113A91529B1244888B6BA682EF46EA1DE0BC (void);
// 0x000000C9 Byn.Awrtc.IMediaNetwork Byn.Awrtc.Unity.UnityCallFactory::CreateMediaNetwork(Byn.Awrtc.NetworkConfig)
extern void UnityCallFactory_CreateMediaNetwork_m0C15E243A7EBA5920BC6777B6B122C3D9D3799FB (void);
// 0x000000CA Byn.Awrtc.IWebRtcNetwork Byn.Awrtc.Unity.UnityCallFactory::CreateBasicNetwork(System.String,Byn.Awrtc.IceServer[])
extern void UnityCallFactory_CreateBasicNetwork_m1D390E969173915C6134F288B675FB676AEAC5B9 (void);
// 0x000000CB System.String[] Byn.Awrtc.Unity.UnityCallFactory::GetVideoDevices()
extern void UnityCallFactory_GetVideoDevices_m31EC8E3E9EA124F8888F79B8C7060894F3FE91AD (void);
// 0x000000CC System.Boolean Byn.Awrtc.Unity.UnityCallFactory::CanSelectVideoDevice()
extern void UnityCallFactory_CanSelectVideoDevice_mD8ED787BAF7DDB1607192D75ED80F6B3DBC0ACFE (void);
// 0x000000CD System.Boolean Byn.Awrtc.Unity.UnityCallFactory::IsFrontFacing(System.String)
extern void UnityCallFactory_IsFrontFacing_mE062E1E37F64C45183A244789701D7C976438FCB (void);
// 0x000000CE System.String Byn.Awrtc.Unity.UnityCallFactory::GetDefaultVideoDevice()
extern void UnityCallFactory_GetDefaultVideoDevice_m1B734C3FBF461D85CC51BDD4552E9F77E42677EB (void);
// 0x000000CF System.Void Byn.Awrtc.Unity.UnityCallFactory::EnterActiveCallState()
extern void UnityCallFactory_EnterActiveCallState_mDE70120635DCB4ED7F0423814DAAEC48D9B4FCDD (void);
// 0x000000D0 System.Void Byn.Awrtc.Unity.UnityCallFactory::LeaveActiveCallState()
extern void UnityCallFactory_LeaveActiveCallState_mE85AD1A37CE91D0D992788915AA7AD6175AD69A0 (void);
// 0x000000D1 System.Void Byn.Awrtc.Unity.UnityCallFactory::Dispose()
extern void UnityCallFactory_Dispose_m813BFA0CBDB32CD89D20A2746A71E7632781A76E (void);
// 0x000000D2 System.Void Byn.Awrtc.Unity.UnityCallFactory::SetLoudspeakerStatus(System.Boolean)
extern void UnityCallFactory_SetLoudspeakerStatus_mF3DB0DA455E2C786403B452CE6112CFEC1963457 (void);
// 0x000000D3 System.Boolean Byn.Awrtc.Unity.UnityCallFactory::GetLoudspeakerStatus()
extern void UnityCallFactory_GetLoudspeakerStatus_m6F7842D4889594C54BAB24901E78CB78B41AC86A (void);
// 0x000000D4 System.Void Byn.Awrtc.Unity.UnityCallFactory::RequestLogLevel(Byn.Awrtc.Unity.UnityCallFactory/LogLevel)
extern void UnityCallFactory_RequestLogLevel_m96F67CFDA348EDF254F0816C19FC4A55A8F175CA (void);
// 0x000000D5 System.Void Byn.Awrtc.Unity.UnityCallFactory::SetDefaultLogger(System.Boolean,System.Boolean)
extern void UnityCallFactory_SetDefaultLogger_m8C95FB5EC987AA4432C1BEF57872979DBC81A1E7 (void);
// 0x000000D6 System.Void Byn.Awrtc.Unity.UnityCallFactory::UpdateLogLevel_WebGl()
extern void UnityCallFactory_UpdateLogLevel_WebGl_m684AC4779EA146B3EDB4C9BD1D39BEFC83B6FABD (void);
// 0x000000D7 System.Void Byn.Awrtc.Unity.UnityCallFactory::OnLog(System.Object,System.String[])
extern void UnityCallFactory_OnLog_mF29534A4F0301F376025EFCFEA5AC1D825E59010 (void);
// 0x000000D8 System.Void Byn.Awrtc.Unity.UnityCallFactory::UnityLog(System.String,System.Action`1<System.Object>)
extern void UnityCallFactory_UnityLog_m4321F3BFF8617CCC17E2E58D7F9A33DABDDD9CCC (void);
// 0x000000D9 System.String[] Byn.Awrtc.Unity.UnityCallFactory::SplitLongMsgs(System.String)
extern void UnityCallFactory_SplitLongMsgs_m0EFC3E5F2426226EE1D36F790CA6157B05750755 (void);
// 0x000000DA System.Void Byn.Awrtc.Unity.UnityCallFactory::.ctor()
extern void UnityCallFactory__ctor_m177C30DC11994226C7F041F27DF6995004DC708A (void);
// 0x000000DB System.Void Byn.Awrtc.Unity.UnityCallFactory::.cctor()
extern void UnityCallFactory__cctor_mA2D29ABBF4949AF92CE2CEA832B10971F5E51193 (void);
// 0x000000DC System.Void Byn.Awrtc.Unity.UnityCallFactory/InitCallbacks::.ctor(System.Action,System.Action`1<System.String>)
extern void InitCallbacks__ctor_m80449E9119A279F440D06F9EE39C6F1477BA913A (void);
// 0x000000DD System.Void Byn.Awrtc.Unity.UnityCallFactory/<CoroutineInitAsync>d__28::.ctor(System.Int32)
extern void U3CCoroutineInitAsyncU3Ed__28__ctor_mB6E18A297FDB6590235ABAAC397D4E235C67CAAC (void);
// 0x000000DE System.Void Byn.Awrtc.Unity.UnityCallFactory/<CoroutineInitAsync>d__28::System.IDisposable.Dispose()
extern void U3CCoroutineInitAsyncU3Ed__28_System_IDisposable_Dispose_m1E4DBCA43A7E563CD26AED7FF91EF9322C5572D8 (void);
// 0x000000DF System.Boolean Byn.Awrtc.Unity.UnityCallFactory/<CoroutineInitAsync>d__28::MoveNext()
extern void U3CCoroutineInitAsyncU3Ed__28_MoveNext_mEF435500A4F99F6E656FA387DC78F8197AF573C4 (void);
// 0x000000E0 System.Object Byn.Awrtc.Unity.UnityCallFactory/<CoroutineInitAsync>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineInitAsyncU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m24BEA6EA97BDE1375F66B72E6BED07B29A46E222 (void);
// 0x000000E1 System.Void Byn.Awrtc.Unity.UnityCallFactory/<CoroutineInitAsync>d__28::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineInitAsyncU3Ed__28_System_Collections_IEnumerator_Reset_m2A907BDC42C8E6CEFA5B38AC61F297D3A2DA5F33 (void);
// 0x000000E2 System.Object Byn.Awrtc.Unity.UnityCallFactory/<CoroutineInitAsync>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineInitAsyncU3Ed__28_System_Collections_IEnumerator_get_Current_m196848C2AACD9AF5E3D43A1377007CDB3C372B58 (void);
// 0x000000E3 System.Void Byn.Awrtc.Unity.UnityCallFactory/<>c::.cctor()
extern void U3CU3Ec__cctor_m513A98D6E8E9F6093C1E29D310F9D1D4AC24A261 (void);
// 0x000000E4 System.Void Byn.Awrtc.Unity.UnityCallFactory/<>c::.ctor()
extern void U3CU3Ec__ctor_m25A5C16B8F849B22C1717E0CFBCF16CEF7F53B50 (void);
// 0x000000E5 System.Boolean Byn.Awrtc.Unity.UnityCallFactory/<>c::<InitStatic>b__30_0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void U3CU3Ec_U3CInitStaticU3Eb__30_0_m04C4E5918CAA3415F9B957E6CA7B672F8E2FFA29 (void);
// 0x000000E6 System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::UpdateTexture(UnityEngine.Texture2D&,Byn.Awrtc.RawFrame,Byn.Awrtc.FramePixelFormat)
extern void UnityMediaHelper_UpdateTexture_m95773C599FE7C0F22C854DAFD79EE8C33FBBDDB2 (void);
// 0x000000E7 System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::UpdateTexture(Byn.Awrtc.IFrame,UnityEngine.Texture2D&)
extern void UnityMediaHelper_UpdateTexture_mAD6B256246FE8077C2CC3A2279E01F2C2B2B0D12 (void);
// 0x000000E8 System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::EnsureTex(System.Int32,System.Int32,UnityEngine.TextureFormat,UnityEngine.Texture2D&)
extern void UnityMediaHelper_EnsureTex_m4BC9A821BB9C0BEAD449AA78A314FBEC56C6E7C1 (void);
// 0x000000E9 System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::UpdateTexture(Byn.Awrtc.IDirectMemoryFrame,UnityEngine.Texture2D&,UnityEngine.Texture2D&,UnityEngine.Texture2D&)
extern void UnityMediaHelper_UpdateTexture_mD596081A4A9DBA46BCDDDA73A7C8D0455DE33CFA (void);
// 0x000000EA System.Boolean Byn.Awrtc.Unity.UnityMediaHelper::UpdateRawImage(UnityEngine.UI.RawImage,Byn.Awrtc.IFrame)
extern void UnityMediaHelper_UpdateRawImage_mB1B4BE5F2F2877EB28E973B060E5E6E9815B276B (void);
// 0x000000EB System.Void Byn.Awrtc.Unity.UnityMediaHelper::.ctor()
extern void UnityMediaHelper__ctor_mC2784EE92E724E436EA9B46D4C1CEE60F99598CE (void);
// 0x000000EC System.Void Byn.Awrtc.Unity.UnityMediaHelper::.cctor()
extern void UnityMediaHelper__cctor_mC51FF53EC84FB49189D449EF9274090905625F73 (void);
// 0x000000ED T Byn.Awrtc.Unity.UnitySingleton`1::get_Instance()
// 0x000000EE System.Void Byn.Awrtc.Unity.UnitySingleton`1::OnDestroy()
// 0x000000EF System.Void Byn.Awrtc.Unity.UnitySingleton`1::.ctor()
// 0x000000F0 System.Void Byn.Awrtc.Unity.UnitySingleton`1::.cctor()
// 0x000000F1 System.String Byn.Unity.Examples.ExampleGlobals::get_SignalingProtocol()
extern void ExampleGlobals_get_SignalingProtocol_m193A10489D7B7F4C3E35007B177287838B7663CF (void);
// 0x000000F2 System.String Byn.Unity.Examples.ExampleGlobals::get_Signaling()
extern void ExampleGlobals_get_Signaling_mCD456E78D2CAEF4CE3076195F57FBD38710580A6 (void);
// 0x000000F3 System.String Byn.Unity.Examples.ExampleGlobals::get_SharedSignaling()
extern void ExampleGlobals_get_SharedSignaling_m780E8BB1A46AF1BE443DA34C06BB1FD961388535 (void);
// 0x000000F4 System.String Byn.Unity.Examples.ExampleGlobals::get_SignalingConference()
extern void ExampleGlobals_get_SignalingConference_m18B611209786B7D99A1DD58BA54E901594DB5A6E (void);
// 0x000000F5 Byn.Awrtc.IceServer Byn.Unity.Examples.ExampleGlobals::get_DefaultIceServer()
extern void ExampleGlobals_get_DefaultIceServer_mDB61401F149F855822963A605FABAC4B54E1C987 (void);
// 0x000000F6 System.Boolean Byn.Unity.Examples.ExampleGlobals::HasAudioPermission()
extern void ExampleGlobals_HasAudioPermission_m5851E4193C8E3B181CD102C4D69D3EF94423EDE4 (void);
// 0x000000F7 System.Boolean Byn.Unity.Examples.ExampleGlobals::HasVideoPermission()
extern void ExampleGlobals_HasVideoPermission_m0545FFE083B63422527A02691C2E58628EE91E18 (void);
// 0x000000F8 System.Collections.IEnumerator Byn.Unity.Examples.ExampleGlobals::RequestAudioPermission()
extern void ExampleGlobals_RequestAudioPermission_m4B4C4C343944C48A6F1236C3F47AAFBD12F64045 (void);
// 0x000000F9 System.Collections.IEnumerator Byn.Unity.Examples.ExampleGlobals::RequestVideoPermission()
extern void ExampleGlobals_RequestVideoPermission_m746D829F9305051FCCB0D06AC3E727E8FA5FBC4D (void);
// 0x000000FA System.Collections.IEnumerator Byn.Unity.Examples.ExampleGlobals::RequestPermissions(System.Boolean,System.Boolean)
extern void ExampleGlobals_RequestPermissions_m8BFD568467E19026E1E39E3C4D6793ED4D4EAE88 (void);
// 0x000000FB System.Void Byn.Unity.Examples.ExampleGlobals::.ctor()
extern void ExampleGlobals__ctor_mC6EBB9B4514667B64A3552ED6A939BC4BE571BC1 (void);
// 0x000000FC System.Void Byn.Unity.Examples.ExampleGlobals::.cctor()
extern void ExampleGlobals__cctor_mD9C0E46E1985162AC20A20EEF3EC7B284790986F (void);
// 0x000000FD System.Void Byn.Unity.Examples.ExampleGlobals/<RequestAudioPermission>d__17::.ctor(System.Int32)
extern void U3CRequestAudioPermissionU3Ed__17__ctor_m89B8F701A129F334146F26B7C1D14DDCF3CA1708 (void);
// 0x000000FE System.Void Byn.Unity.Examples.ExampleGlobals/<RequestAudioPermission>d__17::System.IDisposable.Dispose()
extern void U3CRequestAudioPermissionU3Ed__17_System_IDisposable_Dispose_m48CC7ECB48D4C573C9666E08D9C2FB061CD192F9 (void);
// 0x000000FF System.Boolean Byn.Unity.Examples.ExampleGlobals/<RequestAudioPermission>d__17::MoveNext()
extern void U3CRequestAudioPermissionU3Ed__17_MoveNext_mD0FE6DE3399F1AB95E8B7137E5488C07A904C1EB (void);
// 0x00000100 System.Object Byn.Unity.Examples.ExampleGlobals/<RequestAudioPermission>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestAudioPermissionU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD961F6C6B0BC84EF1FBAB5D8CA91DB6678E16CC6 (void);
// 0x00000101 System.Void Byn.Unity.Examples.ExampleGlobals/<RequestAudioPermission>d__17::System.Collections.IEnumerator.Reset()
extern void U3CRequestAudioPermissionU3Ed__17_System_Collections_IEnumerator_Reset_m7EBAF077EA756DB8A02504F4B357237071AAE82C (void);
// 0x00000102 System.Object Byn.Unity.Examples.ExampleGlobals/<RequestAudioPermission>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CRequestAudioPermissionU3Ed__17_System_Collections_IEnumerator_get_Current_m05962D94434746E0495C8E28ED39BD0B1A244E0B (void);
// 0x00000103 System.Void Byn.Unity.Examples.ExampleGlobals/<RequestVideoPermission>d__18::.ctor(System.Int32)
extern void U3CRequestVideoPermissionU3Ed__18__ctor_m97C13F1373822FF375B7358D6EEFD062DF860ABF (void);
// 0x00000104 System.Void Byn.Unity.Examples.ExampleGlobals/<RequestVideoPermission>d__18::System.IDisposable.Dispose()
extern void U3CRequestVideoPermissionU3Ed__18_System_IDisposable_Dispose_mFEB928530A6FA1C6945849B70860583A8E48CA91 (void);
// 0x00000105 System.Boolean Byn.Unity.Examples.ExampleGlobals/<RequestVideoPermission>d__18::MoveNext()
extern void U3CRequestVideoPermissionU3Ed__18_MoveNext_m27E507BA6F7B190970B07D3154041EF179BAEB52 (void);
// 0x00000106 System.Object Byn.Unity.Examples.ExampleGlobals/<RequestVideoPermission>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestVideoPermissionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D0D963EEF564BF35DBDBB63CBB43869F8F00E6D (void);
// 0x00000107 System.Void Byn.Unity.Examples.ExampleGlobals/<RequestVideoPermission>d__18::System.Collections.IEnumerator.Reset()
extern void U3CRequestVideoPermissionU3Ed__18_System_Collections_IEnumerator_Reset_m960F72293794A4444E25E8006952B3AB8C7713F5 (void);
// 0x00000108 System.Object Byn.Unity.Examples.ExampleGlobals/<RequestVideoPermission>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CRequestVideoPermissionU3Ed__18_System_Collections_IEnumerator_get_Current_m236589E2D006C51D499F8A45E4589E41DCEAF3AC (void);
// 0x00000109 System.Void Byn.Unity.Examples.ExampleGlobals/<RequestPermissions>d__19::.ctor(System.Int32)
extern void U3CRequestPermissionsU3Ed__19__ctor_m9BF63405458B730ACE6E7DEEB2CA68569E4C77C2 (void);
// 0x0000010A System.Void Byn.Unity.Examples.ExampleGlobals/<RequestPermissions>d__19::System.IDisposable.Dispose()
extern void U3CRequestPermissionsU3Ed__19_System_IDisposable_Dispose_m34F296A5EEF85940FBA9CBDDF9FB0EA68CDC2B97 (void);
// 0x0000010B System.Boolean Byn.Unity.Examples.ExampleGlobals/<RequestPermissions>d__19::MoveNext()
extern void U3CRequestPermissionsU3Ed__19_MoveNext_mB38502843AE5150D29EF2B64B2397812D0DC5436 (void);
// 0x0000010C System.Object Byn.Unity.Examples.ExampleGlobals/<RequestPermissions>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestPermissionsU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE215DE4492F873919E43348DF032A66B542E248C (void);
// 0x0000010D System.Void Byn.Unity.Examples.ExampleGlobals/<RequestPermissions>d__19::System.Collections.IEnumerator.Reset()
extern void U3CRequestPermissionsU3Ed__19_System_Collections_IEnumerator_Reset_mB626B8873850B3F1D66FB26C3821E83AE9E2C024 (void);
// 0x0000010E System.Object Byn.Unity.Examples.ExampleGlobals/<RequestPermissions>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CRequestPermissionsU3Ed__19_System_Collections_IEnumerator_get_Current_m8A9A013406A160049C3788F1D02EEE3DEBD22288 (void);
// 0x0000010F System.Void Byn.Unity.Examples.MinimalCall::Start()
extern void MinimalCall_Start_m2DAAA613DB6A4EDC3A2D35F3316AF485AF20445B (void);
// 0x00000110 System.Void Byn.Unity.Examples.MinimalCall::OnCallFactoryReady()
extern void MinimalCall_OnCallFactoryReady_m228031208BD03B76DE1D63B6281097D8005B74F8 (void);
// 0x00000111 System.Void Byn.Unity.Examples.MinimalCall::OnCallFactoryFailed(System.String)
extern void MinimalCall_OnCallFactoryFailed_mA8E066DCAF4439663CBC56C67F8408473C9B8747 (void);
// 0x00000112 System.Void Byn.Unity.Examples.MinimalCall::InitExample()
extern void MinimalCall_InitExample_mC665DBF423D2676FF4C5C4ABE02889667177AB60 (void);
// 0x00000113 System.Void Byn.Unity.Examples.MinimalCall::SetupReceiver()
extern void MinimalCall_SetupReceiver_mC11AC8CCB63B46A0FC58A0E22DA779D09229BD44 (void);
// 0x00000114 System.Void Byn.Unity.Examples.MinimalCall::Update()
extern void MinimalCall_Update_m90A0BC1F88C36D8A142B4DC6DCA83106D5730C65 (void);
// 0x00000115 System.Void Byn.Unity.Examples.MinimalCall::Receiver_CallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void MinimalCall_Receiver_CallEvent_m3DF710C0FFF720763BE739554F3BB05DE3AF2BF1 (void);
// 0x00000116 System.Void Byn.Unity.Examples.MinimalCall::SenderSetup()
extern void MinimalCall_SenderSetup_m938390CF8CE2303F6E0672901413EB42E9C5573D (void);
// 0x00000117 System.Void Byn.Unity.Examples.MinimalCall::Sender_CallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void MinimalCall_Sender_CallEvent_m5CD69E28F45CCD77F3FF87DADAF2BFE2E322FA2D (void);
// 0x00000118 System.Void Byn.Unity.Examples.MinimalCall::OnDestroy()
extern void MinimalCall_OnDestroy_m23EB3347D0F783D8C849C3BE1F6FA4BC092E2A2D (void);
// 0x00000119 System.Void Byn.Unity.Examples.MinimalCall::.ctor()
extern void MinimalCall__ctor_m2C05881C160BCB6C3F79F0421AA7D68E2D5C4990 (void);
// 0x0000011A System.Void Byn.Unity.Examples.MinimalConference::Start()
extern void MinimalConference_Start_m7C0E1A975B6B17488A8A69C143C31CE3DCE15714 (void);
// 0x0000011B System.Void Byn.Unity.Examples.MinimalConference::OnCallFactoryReady()
extern void MinimalConference_OnCallFactoryReady_mB477F718987696A387F29F39B00BA4A67E2D7F33 (void);
// 0x0000011C System.Void Byn.Unity.Examples.MinimalConference::OnCallFactoryFailed(System.String)
extern void MinimalConference_OnCallFactoryFailed_m03A099F568531BF10E4746284F31235CF08BEB55 (void);
// 0x0000011D System.Void Byn.Unity.Examples.MinimalConference::InitExample()
extern void MinimalConference_InitExample_mD62322BD87BB6F4E5B8FBB2BFD61F813F208D464 (void);
// 0x0000011E System.Void Byn.Unity.Examples.MinimalConference::SetupCalls()
extern void MinimalConference_SetupCalls_m0710B8A0C0096D8FDC7CE96CCCF54819148F8474 (void);
// 0x0000011F System.Void Byn.Unity.Examples.MinimalConference::OnCallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void MinimalConference_OnCallEvent_mE48F740401C9CE3BBE7D66F256D13BEE5EC99EC2 (void);
// 0x00000120 System.Void Byn.Unity.Examples.MinimalConference::OnDestroy()
extern void MinimalConference_OnDestroy_mE5C7376943481D3BDFF9F601D2AFCF9B4312F74D (void);
// 0x00000121 System.Void Byn.Unity.Examples.MinimalConference::Update()
extern void MinimalConference_Update_mA07B1B374B9CE905930D51C4D4CCC77BDA95AAE3 (void);
// 0x00000122 System.Void Byn.Unity.Examples.MinimalConference::.ctor()
extern void MinimalConference__ctor_m3B9AC68CEFCA7B1DC7FA445E7F45576BDBE15257 (void);
// 0x00000123 System.Void Byn.Unity.Examples.MinimalMediaNetwork::Start()
extern void MinimalMediaNetwork_Start_m0768797B6AD970F0E7F26A6FBC9364D3805F2555 (void);
// 0x00000124 System.Void Byn.Unity.Examples.MinimalMediaNetwork::OnCallFactoryReady()
extern void MinimalMediaNetwork_OnCallFactoryReady_mC54EF11B81C4F7A61DF5A6C06C2AA377E9DACEE8 (void);
// 0x00000125 System.Void Byn.Unity.Examples.MinimalMediaNetwork::OnCallFactoryFailed(System.String)
extern void MinimalMediaNetwork_OnCallFactoryFailed_m59780B613B4BD8F067C2A3ED411C9E228C6E5DA5 (void);
// 0x00000126 System.Void Byn.Unity.Examples.MinimalMediaNetwork::InitExample()
extern void MinimalMediaNetwork_InitExample_m7176A7EE6A74FBD965B18563F27E923C7A84AAD9 (void);
// 0x00000127 System.Void Byn.Unity.Examples.MinimalMediaNetwork::SetupReceiver()
extern void MinimalMediaNetwork_SetupReceiver_mD7EE2525EFA7EC8014C6AE47AF43C339367CA5E6 (void);
// 0x00000128 System.Void Byn.Unity.Examples.MinimalMediaNetwork::UpdateReceiver()
extern void MinimalMediaNetwork_UpdateReceiver_mB7BBD16290CF4D4E5F31B6C6017032BF2E6E4DC6 (void);
// 0x00000129 System.Void Byn.Unity.Examples.MinimalMediaNetwork::SenderSetup()
extern void MinimalMediaNetwork_SenderSetup_m06D4A7CCA39E0539A26F8737F550D0F94C932428 (void);
// 0x0000012A System.Void Byn.Unity.Examples.MinimalMediaNetwork::UpdateSender()
extern void MinimalMediaNetwork_UpdateSender_m22DC2C4D3C15EA3014E0DB861E4667F9E6B559E8 (void);
// 0x0000012B System.Void Byn.Unity.Examples.MinimalMediaNetwork::OnDestroy()
extern void MinimalMediaNetwork_OnDestroy_m6C21A28E8FC3611AAD36C585386F66A3A4A2D1A6 (void);
// 0x0000012C System.Void Byn.Unity.Examples.MinimalMediaNetwork::Update()
extern void MinimalMediaNetwork_Update_mC8009F0598C8DD27365AF987C1EE75BD421548D2 (void);
// 0x0000012D System.Void Byn.Unity.Examples.MinimalMediaNetwork::.ctor()
extern void MinimalMediaNetwork__ctor_m7FB552CB95575627E821B943ACC27FD26A3829C9 (void);
// 0x0000012E System.Void Byn.Unity.Examples.SimpleCall::Start()
extern void SimpleCall_Start_m655D331BED4650F486795EB90C50E5BE961A15C8 (void);
// 0x0000012F System.Void Byn.Unity.Examples.SimpleCall::OnCallFactoryReady()
extern void SimpleCall_OnCallFactoryReady_m333402A7EA8A0202D964CC0D72F6E9C4C1C0E648 (void);
// 0x00000130 System.Void Byn.Unity.Examples.SimpleCall::OnCallFactoryFailed(System.String)
extern void SimpleCall_OnCallFactoryFailed_mA28F6437A8F1163538DFDC3338E80634C1D69339 (void);
// 0x00000131 System.Void Byn.Unity.Examples.SimpleCall::InitExample()
extern void SimpleCall_InitExample_mB898C400FF90DEFBDE7A97E0933D3A20D0379E77 (void);
// 0x00000132 System.Void Byn.Unity.Examples.SimpleCall::Configure()
extern void SimpleCall_Configure_m04BFAECF58DC722758280DB36BF4DE3C1EE64312 (void);
// 0x00000133 System.Collections.IEnumerator Byn.Unity.Examples.SimpleCall::ConfigureDelayed(System.Single)
extern void SimpleCall_ConfigureDelayed_m0CC5FEEB3A505ED7927053E1FA1FD82289D942DE (void);
// 0x00000134 System.Void Byn.Unity.Examples.SimpleCall::Update()
extern void SimpleCall_Update_mA1C0C83D4E11473AEB35AD3AB5C03F9C72ECE08C (void);
// 0x00000135 System.Void Byn.Unity.Examples.SimpleCall::OnDestroy()
extern void SimpleCall_OnDestroy_m82A5BF5F3C248FE4B215F0FF5E2841374D110022 (void);
// 0x00000136 System.Void Byn.Unity.Examples.SimpleCall::Cleanup()
extern void SimpleCall_Cleanup_m73BB4C8F392A93856B3707BB326B5D4070DA1739 (void);
// 0x00000137 System.Void Byn.Unity.Examples.SimpleCall::Call_CallEvent(System.Object,Byn.Awrtc.CallEventArgs)
extern void SimpleCall_Call_CallEvent_m74D1C0869CBAE860022A1220974371CCB6F106C5 (void);
// 0x00000138 System.Void Byn.Unity.Examples.SimpleCall::Call()
extern void SimpleCall_Call_m00D6B3B624E1F963BDC9A3BD5DEC8C2B800D66A0 (void);
// 0x00000139 System.Void Byn.Unity.Examples.SimpleCall::Error(System.String)
extern void SimpleCall_Error_m8FA5941B26214B85F93444A87337E365B2A07E4B (void);
// 0x0000013A System.Void Byn.Unity.Examples.SimpleCall::Log(System.String)
extern void SimpleCall_Log_m60476DAE27FDC86D26F66EDFACCE44F1CB80DE08 (void);
// 0x0000013B System.Void Byn.Unity.Examples.SimpleCall::.ctor()
extern void SimpleCall__ctor_m242CED737135D3E9D0367D78E7B9FC26B1401DD6 (void);
// 0x0000013C System.Void Byn.Unity.Examples.SimpleCall/<ConfigureDelayed>d__12::.ctor(System.Int32)
extern void U3CConfigureDelayedU3Ed__12__ctor_mDD7166D2DD0257103F71CED95E15B18416E2BF10 (void);
// 0x0000013D System.Void Byn.Unity.Examples.SimpleCall/<ConfigureDelayed>d__12::System.IDisposable.Dispose()
extern void U3CConfigureDelayedU3Ed__12_System_IDisposable_Dispose_m6C9952A067032960423DC4B2C7C614EDE466D6D3 (void);
// 0x0000013E System.Boolean Byn.Unity.Examples.SimpleCall/<ConfigureDelayed>d__12::MoveNext()
extern void U3CConfigureDelayedU3Ed__12_MoveNext_m24CF23B0D63AD047D924F927688E11FB1587F9BE (void);
// 0x0000013F System.Object Byn.Unity.Examples.SimpleCall/<ConfigureDelayed>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConfigureDelayedU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C0E76F7D7A2434D995F23A5B391589812122E73 (void);
// 0x00000140 System.Void Byn.Unity.Examples.SimpleCall/<ConfigureDelayed>d__12::System.Collections.IEnumerator.Reset()
extern void U3CConfigureDelayedU3Ed__12_System_Collections_IEnumerator_Reset_m4887E29AA49CA297D26E070EA170F4CCDD651CA9 (void);
// 0x00000141 System.Object Byn.Unity.Examples.SimpleCall/<ConfigureDelayed>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CConfigureDelayedU3Ed__12_System_Collections_IEnumerator_get_Current_m2B90F640EDCB409B04F869B611398970DFE944D9 (void);
// 0x00000142 System.Void Byn.Unity.Examples.OneToMany::Start()
extern void OneToMany_Start_mB3EB36A88F6BCC8A395A81EBC643942F5222BF11 (void);
// 0x00000143 System.Void Byn.Unity.Examples.OneToMany::OnCallFactoryReady()
extern void OneToMany_OnCallFactoryReady_m0B8114F0D6C8465CB5A9F803B2B97584C629D110 (void);
// 0x00000144 System.Void Byn.Unity.Examples.OneToMany::OnCallFactoryFailed(System.String)
extern void OneToMany_OnCallFactoryFailed_m245C991F1F7407001272260FEB2BB6246819F4C3 (void);
// 0x00000145 System.Collections.IEnumerator Byn.Unity.Examples.OneToMany::InitExample()
extern void OneToMany_InitExample_m1AF3AB53222E54043114278934A7AA3ECB80F437 (void);
// 0x00000146 System.Void Byn.Unity.Examples.OneToMany::OnDestroy()
extern void OneToMany_OnDestroy_mE67EDDD768E00912177B607A725B645EC37C71EB (void);
// 0x00000147 System.Void Byn.Unity.Examples.OneToMany::Update()
extern void OneToMany_Update_m15B3453D1EC96E56A8DE2963DD59E470BF881E66 (void);
// 0x00000148 System.Void Byn.Unity.Examples.OneToMany::HandleMediaEvents()
extern void OneToMany_HandleMediaEvents_m60B402EBEF44DCE8A9B34CD5854F53F438DA0DF2 (void);
// 0x00000149 System.Void Byn.Unity.Examples.OneToMany::Log(System.String)
extern void OneToMany_Log_m0591709E724BBC94B9F97DDC7B1FA69D2451449E (void);
// 0x0000014A System.Void Byn.Unity.Examples.OneToMany::HandleNetworkEvent(Byn.Awrtc.NetworkEvent)
extern void OneToMany_HandleNetworkEvent_m87BCEEFDE59448D165C721281C85AA9F7D16C566 (void);
// 0x0000014B System.Void Byn.Unity.Examples.OneToMany::UpdateTexture(Byn.Awrtc.IFrame)
extern void OneToMany_UpdateTexture_m7B7B2A6D6FDCD7631FB75A90B0BBD7B40D947044 (void);
// 0x0000014C System.Boolean Byn.Unity.Examples.OneToMany::UpdateTexture(UnityEngine.Texture2D&,Byn.Awrtc.IFrame)
extern void OneToMany_UpdateTexture_mF29C33B0479C0BB76A30730CA90CBF195B48CBA3 (void);
// 0x0000014D System.Void Byn.Unity.Examples.OneToMany::.ctor()
extern void OneToMany__ctor_m4F5700BD35CBAFA0EF931B81F7263C642D21A1AB (void);
// 0x0000014E System.Void Byn.Unity.Examples.OneToMany/<InitExample>d__15::.ctor(System.Int32)
extern void U3CInitExampleU3Ed__15__ctor_m08DE7F7794440272171252010AF69C01D56AA072 (void);
// 0x0000014F System.Void Byn.Unity.Examples.OneToMany/<InitExample>d__15::System.IDisposable.Dispose()
extern void U3CInitExampleU3Ed__15_System_IDisposable_Dispose_m634FF8F5DA5B828270A1410ECC11D952124067D9 (void);
// 0x00000150 System.Boolean Byn.Unity.Examples.OneToMany/<InitExample>d__15::MoveNext()
extern void U3CInitExampleU3Ed__15_MoveNext_m539C24B2AC9C6495A037D3494A173BC9632DCFD4 (void);
// 0x00000151 System.Object Byn.Unity.Examples.OneToMany/<InitExample>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitExampleU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16FA4F6AF898169AF40D9A20B54CAEC19FB4DA27 (void);
// 0x00000152 System.Void Byn.Unity.Examples.OneToMany/<InitExample>d__15::System.Collections.IEnumerator.Reset()
extern void U3CInitExampleU3Ed__15_System_Collections_IEnumerator_Reset_mBD5B78879DFE86B744A9A65D1237D3D637589029 (void);
// 0x00000153 System.Object Byn.Unity.Examples.OneToMany/<InitExample>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CInitExampleU3Ed__15_System_Collections_IEnumerator_get_Current_mA9D6328DF1CC0C11F02B1A001B22ACEC8B70D5DB (void);
// 0x00000154 System.Void Byn.Unity.Examples.VideoInputApp::Start()
extern void VideoInputApp_Start_mE87D5BC03DAD5D0CA9E0F8F77000552C27E39028 (void);
// 0x00000155 System.Collections.IEnumerator Byn.Unity.Examples.VideoInputApp::CoroutineRefreshLater()
extern void VideoInputApp_CoroutineRefreshLater_m0E9D223F0A5451FACE419333AB3364F60A2E5355 (void);
// 0x00000156 System.Void Byn.Unity.Examples.VideoInputApp::.ctor()
extern void VideoInputApp__ctor_mDE762E15CAD1DBDF9A7431DCCE9C30C8ABF11E24 (void);
// 0x00000157 System.Void Byn.Unity.Examples.VideoInputApp/<CoroutineRefreshLater>d__1::.ctor(System.Int32)
extern void U3CCoroutineRefreshLaterU3Ed__1__ctor_mE49DA66C692B0AE846A7AD5CCE6CFB7C7F9CB705 (void);
// 0x00000158 System.Void Byn.Unity.Examples.VideoInputApp/<CoroutineRefreshLater>d__1::System.IDisposable.Dispose()
extern void U3CCoroutineRefreshLaterU3Ed__1_System_IDisposable_Dispose_mA60F05CEF5DE77FD5B0FCCB56F4210EAC531FA35 (void);
// 0x00000159 System.Boolean Byn.Unity.Examples.VideoInputApp/<CoroutineRefreshLater>d__1::MoveNext()
extern void U3CCoroutineRefreshLaterU3Ed__1_MoveNext_mA7A1DE828C4CC597D6FBB2BACE823B1307439C5E (void);
// 0x0000015A System.Object Byn.Unity.Examples.VideoInputApp/<CoroutineRefreshLater>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineRefreshLaterU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55B0D74ECBA98511F5BC862794C5C88402421FED (void);
// 0x0000015B System.Void Byn.Unity.Examples.VideoInputApp/<CoroutineRefreshLater>d__1::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineRefreshLaterU3Ed__1_System_Collections_IEnumerator_Reset_m1F62EF6A80BC368E2A4BBA1DEA612349D9B9F98A (void);
// 0x0000015C System.Object Byn.Unity.Examples.VideoInputApp/<CoroutineRefreshLater>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineRefreshLaterU3Ed__1_System_Collections_IEnumerator_get_Current_m7A4767ACF35EE8D6448046683948201DA7368B16 (void);
// 0x0000015D System.Void Byn.Unity.Examples.VirtualCamera::Awake()
extern void VirtualCamera_Awake_m4D1F61B40D64AE0169D223A945E8233AE8E3D58A (void);
// 0x0000015E System.Void Byn.Unity.Examples.VirtualCamera::Start()
extern void VirtualCamera_Start_mE2B92E441DFE632ED6728128A9138369EA63C00A (void);
// 0x0000015F System.Void Byn.Unity.Examples.VirtualCamera::OnDestroy()
extern void VirtualCamera_OnDestroy_m4284B922068B602BD52D271581CF93A2EA5AF25A (void);
// 0x00000160 System.Void Byn.Unity.Examples.VirtualCamera::Update()
extern void VirtualCamera_Update_m2E92D03654CE6C390E706020F75B257106813373 (void);
// 0x00000161 System.Void Byn.Unity.Examples.VirtualCamera::.ctor()
extern void VirtualCamera__ctor_mFE161CD90F88CBF77E293FA532ED3C70DB6243AB (void);
static Il2CppMethodPointer s_methodPointers[353] = 
{
	CallApp_Awake_m4F4DE17D537AABD0381F08DA5B29F368957CF709,
	CallApp_Start_m4AE3C7C5819727A0AFA2A05218DBA7B1DFEDA251,
	CallApp_OnCallFactoryReady_mFF37D74D4D3A14B782012FAA490FAF967E78E9F2,
	CallApp_OnCallFactoryFailed_m2E0BBD202CCEE03BB37ADEEC33270630F5198348,
	CallApp_OnDestroy_mF7C79CEDB712B00FCB7A6AEAFD70715D927EAAEF,
	CallApp_Update_m39BE0CBC1186FD711283DEB839DCE38C16B14B93,
	CallApp_CreateNetworkConfig_m8A084222C30627BAA4FC71DA39F274ADFCB9C5AC,
	CallApp_SetupCall_m6359B6495B1E314E91E676F70B78CF7EC3980C9F,
	CallApp_CreateCall_mC83044232C40DAEB3CDEBF8326262BBDA5562AD0,
	CallApp_Call_CallEvent_mB8749AA593D37943C5886813BA60F21C01576E56,
	CallApp_CleanupCall_m668FDEE82ABA12B9666EDE726442EFAA2B046040,
	CallApp_CreateMediaConfig_mFE25D02EC086DD8D9272E26D726F11F7D611F84D,
	CallApp_ResetCall_m4318A1CA6BB048B8ED33C58ECF0A21378B6937A4,
	CallApp_StopAutoRejoin_m267E7C00BFC9C8C8A24B66EBB9115C01F5B2EAD0,
	CallApp_TriggerRejoinTimer_m8BC20C3F2847BDBA5AF417FBC33688EB9E1DC68E,
	CallApp_InternalResetCall_m678D2746B8460B795D84F757B3C4B38C42D885FB,
	CallApp_SetRemoteVolume_mE603EAD0B925090EB9586311366E7944DF73150C,
	CallApp_GetVideoDevices_m29DA1B4F16929CFF9C2C8266BDB96F4C034D1DDA,
	CallApp_CanSelectVideoDevice_m16610AE08EBD8DA497CFB898EC2985D5857DAA2F,
	CallApp_Join_m09AEEFC9253D1F6C7FB115588802EE68D2EFA4F2,
	CallApp_InternalJoin_mD4AC42F07FF6609E1555BAE14563CD38C8DE0428,
	CallApp_CoroutineRejoin_mFAF3383FEBEEC85CC591F8EB2B48E6422A31023F,
	CallApp_Send_m903F3A7FD9115F1F64896308CC5685E4413D73C6,
	CallApp_SetAudio_m7B606EBE2FCFC2990109BDB9F292EDA1D2DD8CE2,
	CallApp_SetVideo_m43B467BA8FCDA45438E816112BDEFBF0F4FD9770,
	CallApp_SetVideoDevice_m7F2FB84729977857B2BF0E57BDB7CA7BF1C46B48,
	CallApp_SetIdealResolution_m0861F2671347FFB3BE67FC1A70AA7A45EC42A556,
	CallApp_SetIdealFps_m891F1FFB9368E6747F3EB567B15E903423349FDA,
	CallApp_SetShowLocalVideo_m8E974DB781539BD07232C9AE855D20431796D901,
	CallApp_SetAutoRejoin_mCB386299BAC53D59BC58DB883361096D7FC63FE8,
	CallApp_GetLoudspeakerStatus_m327E37EFAE8499CD38898B0343CBFC87916D5B69,
	CallApp_SetLoudspeakerStatus_m8EDD40050CD5CCDB57DC6FE00C5B2AD73518333D,
	CallApp_SetMute_m76883C41681C6B319A0B07D2615A0F67DF018669,
	CallApp_IsMute_m7E5216F00A08CF9998E3BAEC027D946FC05FA954,
	CallApp_UpdateFrame_m1729D9729D94D4614D3FFDFA2D83F936DE24D83A,
	CallApp_Append_mE8F87CCB6B47370505FCB27998D74989074CBAB8,
	CallApp__ctor_m1B0CD2D693D05AD0B8B8D88212C017BC3B871D32,
	U3CCoroutineRejoinU3Ed__40__ctor_m351C25BBEDE56AB148977AF75EBFD5CFE2F8206A,
	U3CCoroutineRejoinU3Ed__40_System_IDisposable_Dispose_m9EAD48BC8B4CE80C4C3297D88460F25CF3ED7FF0,
	U3CCoroutineRejoinU3Ed__40_MoveNext_m3094DBB2EE6BDE13DBD41C1AA6D2A56ACAF28207,
	U3CCoroutineRejoinU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3397DB9943C9F264D5C374F758B93916F1020BEA,
	U3CCoroutineRejoinU3Ed__40_System_Collections_IEnumerator_Reset_mCF0B2E6416765806DC029A45659AB7A76E38C5AB,
	U3CCoroutineRejoinU3Ed__40_System_Collections_IEnumerator_get_Current_mB9DAA9C9A4E1CBB1D6BC538BA79492E274116F0E,
	CallAppUi_Awake_mE0543A35B69AA16D12DA1A2C10F2FD2049FAC27A,
	CallAppUi_Start_m47CF5AF8FB50C6E5DC8438CE4666205DAB61116E,
	CallAppUi_SaveSettings_mC9C02BC93F796E671D1DA7FB5D738BA3E9C3535C,
	CallAppUi_LoadSettings_m4F88D7932AA5809C07626A778EB0CA1244A36AD3,
	CallAppUi_ResetSettings_m755784E26B724E42B8B205ECB8FB656446CAE3A5,
	CallAppUi_CheckSettings_mD8BA99B08FE4D4B207A84C57888D8EA56218CDF5,
	CallAppUi_OnAudioSettingsChanged_m841EED7F453C3CC65997B6924AF954F5591245AE,
	CallAppUi_OnVideoSettingsChanged_m5160C3EC04C40F5C0BDA51B1B4650C668BE9555C,
	CallAppUi_RequestAudioPermissions_mE29840136497FB533ED651EDF6033991A9B26D96,
	CallAppUi_RequestVideoPermissions_mDE742C626FF5A07095105A476A8B054F01652FC0,
	CallAppUi_PlayerPrefsGetBool_m0EF91E09F1C39844BC06A221E01D20EF9097A50D,
	CallAppUi_PlayerPrefsSetBool_m3CFBE099190608FEBE250B35C8A67657626E22B9,
	CallAppUi_GetSelectedVideoDevice_m402A489A10BF0750111A7C268F285CDBC659C388,
	CallAppUi_TryParseInt_m94CBBB8286D3D1F0347B08B1A79972232711916C,
	CallAppUi_SetupCallApp_mBB70F8D62EC81E0A3AE6FACB45AF2F5013D6A47A,
	CallAppUi_ToggleSettings_m1609F42DFC816B9E64EC3D7983D7BBACFF532047,
	CallAppUi_ToggleSetup_m99439C79D0F17BBB92964E85DAA53AC2B200E956,
	CallAppUi_UpdateLocalTexture_mA3C43A67BBA410BA69A22374DCA5A6BBCC90CB7B,
	CallAppUi_UpdateRemoteTexture_mAB684398EBB0C79551A8EC4A150C41876D96F732,
	CallAppUi_UpdateVideoDropdown_mC6DDBF93338E9EEB06D8E281355ACC306DE984AE,
	CallAppUi_VideoDropdownOnValueChanged_m3A852CC926553F05E52CD195761A9F66E69BDE49,
	CallAppUi_Append_m2F1CCA542A0BD13C51E43E5169CF06BFA35703C4,
	CallAppUi_SetFullscreen_mFCE504D4300010F59200147926138B9E5705BBD6,
	CallAppUi_Fullscreen_m7DB27478ABE00FB8674F32789AFBC84748FD0BAD,
	CallAppUi_ShowOverlay_mDABCA28F98418A92E8F22656353DDF8D936B461D,
	CallAppUi_SetGuiState_mD440C6A738E8E5D79D2BE673AACFC4ACF810BAAF,
	CallAppUi_JoinButtonPressed_m0C0E1B114260614B31C891CBB9288AA33C70D1D0,
	CallAppUi_EnsureLength_m30E718EECE72A84984C095400854C612FE3C1E8C,
	CallAppUi_GetRoomname_m5F7EBC3EED44D4007E5923D385EDF382A81F85DF,
	CallAppUi_SendButtonPressed_m5E99B0A23C94FBCAE69C3E8B19294A1B12CCD738,
	CallAppUi_InputOnEndEdit_m13FF8E315F542F40775E820D5D1DED284343D454,
	CallAppUi_SendMsg_mA41DD37F50A048F0E8E7151372939B1187253E54,
	CallAppUi_ShutdownButtonPressed_m771B252752301D7824A58BB27C274A58EFEC520F,
	CallAppUi_OnVolumeChanged_m5A26A08C7418267440F410A0F624BFA148B9CF7C,
	CallAppUi_OnLoudspeakerToggle_mE5F8A6E50334E5A574202511B26C3F9B89913704,
	CallAppUi_RefreshLoudspeakerToggle_m712D3B16AAA902B49BA3F9113F35A317ACBEDF04,
	CallAppUi_OnMuteToggle_m204348599E9B9CCADE707B6CB788711E81B7CC94,
	CallAppUi_RefreshMuteToggle_m4CF146AB1DB7AC17BD735466B16AC944C0F89C2A,
	CallAppUi_Update_m6179B9422FF0881A8FE5811FEDBB6259934B21CF,
	CallAppUi__ctor_mD445E97B6CC9DF1F0CE5873C3FCBFF3D9F4E6736,
	CallAppUi__cctor_mA88D97F17785507D67D6F08D17503165061E7ACC,
	U3CRequestAudioPermissionsU3Ed__74__ctor_m23AFECA2492A62EE362B67024439DCF012EF4058,
	U3CRequestAudioPermissionsU3Ed__74_System_IDisposable_Dispose_mB11B7CE1FC5AEF6E0E31868CA1FD107D1BFBEF85,
	U3CRequestAudioPermissionsU3Ed__74_MoveNext_mEF81087ED6FF2BDB23F1F5E46250A484CDFB7E9B,
	U3CRequestAudioPermissionsU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3DED2F431ADBF548BA0CE1AD64246FB398EA0C12,
	U3CRequestAudioPermissionsU3Ed__74_System_Collections_IEnumerator_Reset_m271F09A42DD92FA6B1631B9AD0A311563BCF4D45,
	U3CRequestAudioPermissionsU3Ed__74_System_Collections_IEnumerator_get_Current_m0A8B537BC98AF46FF83C8BEEAA45942B6B13586C,
	U3CRequestVideoPermissionsU3Ed__75__ctor_mA5CFB0928E7E8474156819722C2F8070D6EA103E,
	U3CRequestVideoPermissionsU3Ed__75_System_IDisposable_Dispose_mDE20F2B67099B4C04BDD4963937541FEDFC8198A,
	U3CRequestVideoPermissionsU3Ed__75_MoveNext_m9C3E79F1A4581AFEB6AC3767F9876559969CC245,
	U3CRequestVideoPermissionsU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m628786529EB8B6B4E161FB8C590A630D772F20BF,
	U3CRequestVideoPermissionsU3Ed__75_System_Collections_IEnumerator_Reset_m539DD620E1337B8B2F6450C51ECDB57D31CA61DE,
	U3CRequestVideoPermissionsU3Ed__75_System_Collections_IEnumerator_get_Current_m21E160A03571ACDA2D7597C5E2575ADAC1C7582B,
	ImgFitter_Update_m7F85A642FA5BD5915DEF2D816236999CBA475EEA,
	ImgFitter_Abs_m5CB3F95BDA653E57EE91CCAF31130E3FE4F80982,
	ImgFitter__ctor_m8978F2C18CCD6FB3FA4CDFFE96FAA5EB2E7149F2,
	VideoPanelEventHandler_Start_m9D72F720286849D3C206B7B05D324FA5D9458C68,
	VideoPanelEventHandler_OnPointerClick_m808D8DADB22B9F0E98FC7829FE78C2834D5FBC4C,
	VideoPanelEventHandler__ctor_m118572D8D56EB86C0A87E4DD44364C93518EB731,
	ChatApp_Start_m10342CA0C702C01BF3B8B272BCD494923A8EAF09,
	ChatApp_OnCallFactoryReady_m30155DB49D427ED648EC58C3C2296B11E0A160B0,
	ChatApp_OnCallFactoryFailed_m8443155627E90BD9F9D30FA0841E7AE27B2C5F97,
	ChatApp_Setup_mF58ECF671400B027E46F6AD7A43B10EE6C89B93E,
	ChatApp_ResetApp_m0FE8FD820684E867ACC1BEE6740C0DB43F15E8D1,
	ChatApp_Cleanup_mB03126342565D15125B0469E0582FD590BAB2CFC,
	ChatApp_OnDestroy_m3BA70BB113D4F8494E965660768FD17C72D5DB3C,
	ChatApp_FixedUpdate_m7341787D8A4E850BEE290D1AE078E091CBA0278D,
	ChatApp_HandleNetwork_mF88EC8D9F769F8AB427A9EB4F95565952C3E9B03,
	ChatApp_HandleIncommingMessage_mD0DD4D5FD4A3092BBA52CB4966AD89C2C55F15D1,
	ChatApp_SendString_mE4F66EA5239667018ECC4DEE2315A164C5D91D16,
	ChatApp_Append_m0DA1B3709C8DCF8CF86636BA6E9904DBFB7A8190,
	ChatApp_SetGuiState_m0059FBA4A193C363BA47071A8B8BA8D92A413930,
	ChatApp_JoinRoomButtonPressed_mD007F07597FA0C4F6A06019FF8DC99B73799400D,
	ChatApp_EnsureLength_mA7890B0024593F26F83974E72834A87F3E271712,
	ChatApp_LeaveButtonPressed_mF89CEF834EE2F7C35B1BF1EB3EACBA506767CE96,
	ChatApp_OpenRoomButtonPressed_mFF56C0C52F732D191FBEC9C6C41784B218A10684,
	ChatApp_InputOnEndEdit_mCD79A0ED800333D5A35653B8EA45F096D8A66872,
	ChatApp_SendButtonPressed_mC6E628A0BBDAC5BA359A4EDF072105FE81642E6E,
	ChatApp__ctor_mF65D6A54D4DD6871CFD59038753B681380B4CD28,
	MessageList_Awake_m15E3852FEFB42CC4D2D319551513EE9FBCB83DB1,
	MessageList_Start_mEACD5D23B4055B9B9ED1FAE776CF02D39FB9144C,
	MessageList_AddTextEntry_m4FE8154758B8DF8B4BB25F06DF42E5B189B97E63,
	MessageList_Update_m8EA59310D0B5DF1260371550510D0A7D58AB7B62,
	MessageList__ctor_m076548586A36E2A50D962F8CE581B91E62D933EE,
	GridManager_Update_m4A32CF8AD6BDEABC921341D743BE2AA9C7518870,
	GridManager_Refresh_m3C9181FCAA9F6F580474D9DE70E1982AF7F31F15,
	GridManager__ctor_mDABE66EFE2CB611D4A6FDDC4BA2BB68B763037BC,
	MenuScript_Awake_mB15D4611A6309D5EBE1D72AC5412B9B0D6F958FF,
	MenuScript_Start_mC5C1984E8FE4A894ED0E2DE2A13EC4098CC42107,
	MenuScript_PrintDeviceDebug_m593DC9320D29AC8ABEC9DB3E70AE78192DB6C53A,
	MenuScript_Update_mD9979BD21636C05C48C221CA8A0CA4E9E5463354,
	MenuScript_Initialize_mAEB66998D3459C12E8438B2D410A4BE779DCC2CD,
	MenuScript_ExampleUi_mCAE051D785C0F2E2E08CBAF17CB1CE08F1BDD8AC,
	MenuScript_MenuUi_m3F00E183872214AD0B3A043C279D8C4D9F4962DD,
	MenuScript_LoadExample_mECACD81713E828CC75F0D1EBC687B18C01740A3B,
	MenuScript_LoadChatExample_m6CE22DF0B824432CC32A7F2E791BB40D26421CF9,
	MenuScript_LoadCallExample_mD1096AE0204895C613F259ED96F8CA509AA17EBA,
	MenuScript_LoadConference_m21668389A5DF95469ADFD3C59546D8C0C0E02038,
	MenuScript_LoadMenu_m54563261B39716CF68A02B524AA0C8D05D9F49A9,
	MenuScript__ctor_m19057CB72315C7560F0E5B908359256160E54654,
	AndroidHelper_IsFrontFacing_mE78E567474B7E94F9A4192AE4B018B8EBA195B4E,
	AndroidHelper_IsBackFacing_m9FD15B8C57A0756A8EF64DC07E71C977C7301543,
	AndroidHelper_SetSpeakerOn_mD1A2D13722145BADDD9B110BD203FD18FBC6008B,
	AndroidHelper_IsSpeakerOn_mDDD004F8ED629C86CADD16518D983438CDCCB181,
	AndroidHelper_GetMode_m903BE309103B9E80289DBCE5A2C3FA956A9A302D,
	AndroidHelper_SetMode_m9AEAF40FAED6AA6A287D821BDA6482DB63EAB803,
	AndroidHelper_IsModeInCommunication_m39839F303CA0E03DF4CD7D02F69CE2A97F37C34C,
	AndroidHelper_SetModeInCommunicaion_mC4021AEA86C4AA32FE964F1FA9A436A2226F4AE2,
	AndroidHelper_GetAudioManagerMode_m8C67C20CBF9BC580F9502031BFCA210925A4CD16,
	AndroidHelper_SetModeNormal_mB8B5DFECED413B87C6AC4AC571F844CC7A2271EF,
	AndroidHelper_GetStreamVolume_m49D3ADBDD2C5489E576848A32B4739794C96B11E,
	AndroidHelper_SetStreamVolume_mA6198C2E82ED9ADA3E1E03E1C61FC8D7756D074A,
	AndroidHelper_SetMute_m97057E15C1267203069BD7CD45AE0AFA0EEE0DA0,
	AndroidHelper_IsMute_m3306C12AA6D6B1C6EC1FCA9D941D2153B1F8001F,
	AndroidHelper_GetAudioManager_m26BE8F9FA9A36F910971C18396516F6E293B7970,
	AndroidHelper_GetActivity_mFA6D683726C8049824CF96305F78D46AD1F4BE15,
	AndroidHelper_GetAudioManagerFlag_mA44CBA316D5EEBE827C547385376255E839F1C06,
	AndroidHelper_CheckPermissionMicrophone_m34144495AB2BB6A3B84DDFE001522B627A1F2189,
	AndroidHelper_CheckPermissionCamera_m5D0B45707C10B44DBB07158E325C31A22DA63A38,
	AndroidHelper_CheckPermissionAudioSettings_m7EBF13EF420BE7CBC1044115F14B8785606CBDB4,
	AndroidHelper_CheckPermissionNetwork_mAFDD3F80D21FF09470002137B5B9A0BCA2C8B134,
	AndroidHelper_HasRuntimePermissions_mA0B3E37D50D43D2994BF130579144D88FEB2B64A,
	AndroidHelper_RequestPermissions_m000FE65602C7A07F46583ACEA18AB0C5EE2E2220,
	AndroidHelper_OpenPermissionView_m5C119111578F3DFC1D08508A8BC9E9CE893016A0,
	AndroidHelper_setBluetoothScoOn_mA9C3C7CA990904A1092B29D5482E15B466466306,
	AndroidHelper_startBluetoothSco_m285F41C7EEB192DA622B50FECECA66E0ACD95755,
	AndroidHelper_stopBluetoothSco_mE7040328537784D2D42A2FF766769574157FE8D0,
	AndroidHelper_IsHeadsetOn_m413157C5F37D2F1BCA156DCDFD0FADE23490A4C1,
	AndroidHelper_SetBluetoothOn_mBCBB3C5347988CAEAF7A752B385FB5A5006001A1,
	AndroidHelper__ctor_m8C08EFD99660DC66FA93DB4EC78ABC722220A352,
	AndroidHelper__cctor_m1B58702CCCFE917ACC9A9CB0E94F669E8D7DE5F5,
	UnityHelper_PtrFromColor32_m6343D6747A963C70E598A40A651A4327CD92FC66,
	UnityCallFactory_get_Instance_m369C480ACF81C431FE8975235D48B868B1D2C29F,
	UnityCallFactory_AddInitCallbacks_mDC8B59B10F02A843A84A82812E2CD06AAA1A645A,
	UnityCallFactory_EnsureInit_mA557FD93F048A79B3193E9105F31E756F09F51B1,
	UnityCallFactory_EnsureInit_m6F16468E64FD542B4CF5211F63C177BEFFA1597D,
	UnityCallFactory_Init_m71EAA6ABF8601E962B8CC15CCF80CD2AE1A3D9B3,
	UnityCallFactory_TriggerOnInitFailed_mDA4078FE7C3F2549C5E447E4D5681546972E6F23,
	UnityCallFactory_TriggerInitFailed_mE09E2A95F8114758E0AEB1B32CDE2AF73AA2BF4D,
	UnityCallFactory_TriggerInitSuccess_mD1CD3B233C33268CE88EA777EB3E6450E2D96E7E,
	UnityCallFactory_TriggerFailedCallback_m06A211E755217F05246976DDC600FB6729184B82,
	UnityCallFactory_TriggerSuccessCallback_mC458DCD62DAA59E96FEC0B8D6A20CD705E62E0A4,
	UnityCallFactory_CoroutineInitAsync_m413E1A9C5DA0124BAE50A78478ADB19534AF4065,
	UnityCallFactory_CreateSingleton_m7DC4AECCF4CFC42AC1CDE08801DF62B385D3040A,
	UnityCallFactory_InitStatic_m3820F22CF15B2AD71FF9701616D9C55235F649AD,
	UnityCallFactory_IsInitStaticComplete_m19C8C6330B26C5A835F69654E259506F36A7E100,
	UnityCallFactory_InitObj_m337891799CC4CCBC3D74792E35BCC5DBA15F0EFC,
	UnityCallFactory_OnDestroy_mCB711593EABB4F7026F3428857B6E5E2A82C3AFA,
	UnityCallFactory_get_InternalFactory_mD1C17870B4991D8BBFD91A9A461B68B0CECC2ABC,
	UnityCallFactory_get_VideoInput_mF4DDA7AADC21C08BE65EF8D5126E442F6BAF9EF1,
	UnityCallFactory_Awake_m919C33A9C84BAAFE99385BDE2CD1F7762774B91B,
	UnityCallFactory_Start_mA123D87B4BDD12B7230C13AC597585CDA5CB36A0,
	UnityCallFactory_Update_m28A0D658E0FB3AB05F54A48FB67FA370B5DA3EAB,
	UnityCallFactory_IsNativePlatform_mAF9853157A51A70AEFB0EF8C0173BCD76EBE2C9B,
	UnityCallFactory_StaticInitBrowser_m891BD2D0AAA73FD55E179ED473592548FD0C0582,
	UnityCallFactory_StaticInitAndroid_mB3F4D1F15C6CEC3EB70F2256EDA0566C7C92BC38,
	UnityCallFactory_Create_m70FC113A91529B1244888B6BA682EF46EA1DE0BC,
	UnityCallFactory_CreateMediaNetwork_m0C15E243A7EBA5920BC6777B6B122C3D9D3799FB,
	UnityCallFactory_CreateBasicNetwork_m1D390E969173915C6134F288B675FB676AEAC5B9,
	UnityCallFactory_GetVideoDevices_m31EC8E3E9EA124F8888F79B8C7060894F3FE91AD,
	UnityCallFactory_CanSelectVideoDevice_mD8ED787BAF7DDB1607192D75ED80F6B3DBC0ACFE,
	UnityCallFactory_IsFrontFacing_mE062E1E37F64C45183A244789701D7C976438FCB,
	UnityCallFactory_GetDefaultVideoDevice_m1B734C3FBF461D85CC51BDD4552E9F77E42677EB,
	UnityCallFactory_EnterActiveCallState_mDE70120635DCB4ED7F0423814DAAEC48D9B4FCDD,
	UnityCallFactory_LeaveActiveCallState_mE85AD1A37CE91D0D992788915AA7AD6175AD69A0,
	UnityCallFactory_Dispose_m813BFA0CBDB32CD89D20A2746A71E7632781A76E,
	UnityCallFactory_SetLoudspeakerStatus_mF3DB0DA455E2C786403B452CE6112CFEC1963457,
	UnityCallFactory_GetLoudspeakerStatus_m6F7842D4889594C54BAB24901E78CB78B41AC86A,
	UnityCallFactory_RequestLogLevel_m96F67CFDA348EDF254F0816C19FC4A55A8F175CA,
	UnityCallFactory_SetDefaultLogger_m8C95FB5EC987AA4432C1BEF57872979DBC81A1E7,
	UnityCallFactory_UpdateLogLevel_WebGl_m684AC4779EA146B3EDB4C9BD1D39BEFC83B6FABD,
	UnityCallFactory_OnLog_mF29534A4F0301F376025EFCFEA5AC1D825E59010,
	UnityCallFactory_UnityLog_m4321F3BFF8617CCC17E2E58D7F9A33DABDDD9CCC,
	UnityCallFactory_SplitLongMsgs_m0EFC3E5F2426226EE1D36F790CA6157B05750755,
	UnityCallFactory__ctor_m177C30DC11994226C7F041F27DF6995004DC708A,
	UnityCallFactory__cctor_mA2D29ABBF4949AF92CE2CEA832B10971F5E51193,
	InitCallbacks__ctor_m80449E9119A279F440D06F9EE39C6F1477BA913A,
	U3CCoroutineInitAsyncU3Ed__28__ctor_mB6E18A297FDB6590235ABAAC397D4E235C67CAAC,
	U3CCoroutineInitAsyncU3Ed__28_System_IDisposable_Dispose_m1E4DBCA43A7E563CD26AED7FF91EF9322C5572D8,
	U3CCoroutineInitAsyncU3Ed__28_MoveNext_mEF435500A4F99F6E656FA387DC78F8197AF573C4,
	U3CCoroutineInitAsyncU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m24BEA6EA97BDE1375F66B72E6BED07B29A46E222,
	U3CCoroutineInitAsyncU3Ed__28_System_Collections_IEnumerator_Reset_m2A907BDC42C8E6CEFA5B38AC61F297D3A2DA5F33,
	U3CCoroutineInitAsyncU3Ed__28_System_Collections_IEnumerator_get_Current_m196848C2AACD9AF5E3D43A1377007CDB3C372B58,
	U3CU3Ec__cctor_m513A98D6E8E9F6093C1E29D310F9D1D4AC24A261,
	U3CU3Ec__ctor_m25A5C16B8F849B22C1717E0CFBCF16CEF7F53B50,
	U3CU3Ec_U3CInitStaticU3Eb__30_0_m04C4E5918CAA3415F9B957E6CA7B672F8E2FFA29,
	UnityMediaHelper_UpdateTexture_m95773C599FE7C0F22C854DAFD79EE8C33FBBDDB2,
	UnityMediaHelper_UpdateTexture_mAD6B256246FE8077C2CC3A2279E01F2C2B2B0D12,
	UnityMediaHelper_EnsureTex_m4BC9A821BB9C0BEAD449AA78A314FBEC56C6E7C1,
	UnityMediaHelper_UpdateTexture_mD596081A4A9DBA46BCDDDA73A7C8D0455DE33CFA,
	UnityMediaHelper_UpdateRawImage_mB1B4BE5F2F2877EB28E973B060E5E6E9815B276B,
	UnityMediaHelper__ctor_mC2784EE92E724E436EA9B46D4C1CEE60F99598CE,
	UnityMediaHelper__cctor_mC51FF53EC84FB49189D449EF9274090905625F73,
	NULL,
	NULL,
	NULL,
	NULL,
	ExampleGlobals_get_SignalingProtocol_m193A10489D7B7F4C3E35007B177287838B7663CF,
	ExampleGlobals_get_Signaling_mCD456E78D2CAEF4CE3076195F57FBD38710580A6,
	ExampleGlobals_get_SharedSignaling_m780E8BB1A46AF1BE443DA34C06BB1FD961388535,
	ExampleGlobals_get_SignalingConference_m18B611209786B7D99A1DD58BA54E901594DB5A6E,
	ExampleGlobals_get_DefaultIceServer_mDB61401F149F855822963A605FABAC4B54E1C987,
	ExampleGlobals_HasAudioPermission_m5851E4193C8E3B181CD102C4D69D3EF94423EDE4,
	ExampleGlobals_HasVideoPermission_m0545FFE083B63422527A02691C2E58628EE91E18,
	ExampleGlobals_RequestAudioPermission_m4B4C4C343944C48A6F1236C3F47AAFBD12F64045,
	ExampleGlobals_RequestVideoPermission_m746D829F9305051FCCB0D06AC3E727E8FA5FBC4D,
	ExampleGlobals_RequestPermissions_m8BFD568467E19026E1E39E3C4D6793ED4D4EAE88,
	ExampleGlobals__ctor_mC6EBB9B4514667B64A3552ED6A939BC4BE571BC1,
	ExampleGlobals__cctor_mD9C0E46E1985162AC20A20EEF3EC7B284790986F,
	U3CRequestAudioPermissionU3Ed__17__ctor_m89B8F701A129F334146F26B7C1D14DDCF3CA1708,
	U3CRequestAudioPermissionU3Ed__17_System_IDisposable_Dispose_m48CC7ECB48D4C573C9666E08D9C2FB061CD192F9,
	U3CRequestAudioPermissionU3Ed__17_MoveNext_mD0FE6DE3399F1AB95E8B7137E5488C07A904C1EB,
	U3CRequestAudioPermissionU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD961F6C6B0BC84EF1FBAB5D8CA91DB6678E16CC6,
	U3CRequestAudioPermissionU3Ed__17_System_Collections_IEnumerator_Reset_m7EBAF077EA756DB8A02504F4B357237071AAE82C,
	U3CRequestAudioPermissionU3Ed__17_System_Collections_IEnumerator_get_Current_m05962D94434746E0495C8E28ED39BD0B1A244E0B,
	U3CRequestVideoPermissionU3Ed__18__ctor_m97C13F1373822FF375B7358D6EEFD062DF860ABF,
	U3CRequestVideoPermissionU3Ed__18_System_IDisposable_Dispose_mFEB928530A6FA1C6945849B70860583A8E48CA91,
	U3CRequestVideoPermissionU3Ed__18_MoveNext_m27E507BA6F7B190970B07D3154041EF179BAEB52,
	U3CRequestVideoPermissionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D0D963EEF564BF35DBDBB63CBB43869F8F00E6D,
	U3CRequestVideoPermissionU3Ed__18_System_Collections_IEnumerator_Reset_m960F72293794A4444E25E8006952B3AB8C7713F5,
	U3CRequestVideoPermissionU3Ed__18_System_Collections_IEnumerator_get_Current_m236589E2D006C51D499F8A45E4589E41DCEAF3AC,
	U3CRequestPermissionsU3Ed__19__ctor_m9BF63405458B730ACE6E7DEEB2CA68569E4C77C2,
	U3CRequestPermissionsU3Ed__19_System_IDisposable_Dispose_m34F296A5EEF85940FBA9CBDDF9FB0EA68CDC2B97,
	U3CRequestPermissionsU3Ed__19_MoveNext_mB38502843AE5150D29EF2B64B2397812D0DC5436,
	U3CRequestPermissionsU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE215DE4492F873919E43348DF032A66B542E248C,
	U3CRequestPermissionsU3Ed__19_System_Collections_IEnumerator_Reset_mB626B8873850B3F1D66FB26C3821E83AE9E2C024,
	U3CRequestPermissionsU3Ed__19_System_Collections_IEnumerator_get_Current_m8A9A013406A160049C3788F1D02EEE3DEBD22288,
	MinimalCall_Start_m2DAAA613DB6A4EDC3A2D35F3316AF485AF20445B,
	MinimalCall_OnCallFactoryReady_m228031208BD03B76DE1D63B6281097D8005B74F8,
	MinimalCall_OnCallFactoryFailed_mA8E066DCAF4439663CBC56C67F8408473C9B8747,
	MinimalCall_InitExample_mC665DBF423D2676FF4C5C4ABE02889667177AB60,
	MinimalCall_SetupReceiver_mC11AC8CCB63B46A0FC58A0E22DA779D09229BD44,
	MinimalCall_Update_m90A0BC1F88C36D8A142B4DC6DCA83106D5730C65,
	MinimalCall_Receiver_CallEvent_m3DF710C0FFF720763BE739554F3BB05DE3AF2BF1,
	MinimalCall_SenderSetup_m938390CF8CE2303F6E0672901413EB42E9C5573D,
	MinimalCall_Sender_CallEvent_m5CD69E28F45CCD77F3FF87DADAF2BFE2E322FA2D,
	MinimalCall_OnDestroy_m23EB3347D0F783D8C849C3BE1F6FA4BC092E2A2D,
	MinimalCall__ctor_m2C05881C160BCB6C3F79F0421AA7D68E2D5C4990,
	MinimalConference_Start_m7C0E1A975B6B17488A8A69C143C31CE3DCE15714,
	MinimalConference_OnCallFactoryReady_mB477F718987696A387F29F39B00BA4A67E2D7F33,
	MinimalConference_OnCallFactoryFailed_m03A099F568531BF10E4746284F31235CF08BEB55,
	MinimalConference_InitExample_mD62322BD87BB6F4E5B8FBB2BFD61F813F208D464,
	MinimalConference_SetupCalls_m0710B8A0C0096D8FDC7CE96CCCF54819148F8474,
	MinimalConference_OnCallEvent_mE48F740401C9CE3BBE7D66F256D13BEE5EC99EC2,
	MinimalConference_OnDestroy_mE5C7376943481D3BDFF9F601D2AFCF9B4312F74D,
	MinimalConference_Update_mA07B1B374B9CE905930D51C4D4CCC77BDA95AAE3,
	MinimalConference__ctor_m3B9AC68CEFCA7B1DC7FA445E7F45576BDBE15257,
	MinimalMediaNetwork_Start_m0768797B6AD970F0E7F26A6FBC9364D3805F2555,
	MinimalMediaNetwork_OnCallFactoryReady_mC54EF11B81C4F7A61DF5A6C06C2AA377E9DACEE8,
	MinimalMediaNetwork_OnCallFactoryFailed_m59780B613B4BD8F067C2A3ED411C9E228C6E5DA5,
	MinimalMediaNetwork_InitExample_m7176A7EE6A74FBD965B18563F27E923C7A84AAD9,
	MinimalMediaNetwork_SetupReceiver_mD7EE2525EFA7EC8014C6AE47AF43C339367CA5E6,
	MinimalMediaNetwork_UpdateReceiver_mB7BBD16290CF4D4E5F31B6C6017032BF2E6E4DC6,
	MinimalMediaNetwork_SenderSetup_m06D4A7CCA39E0539A26F8737F550D0F94C932428,
	MinimalMediaNetwork_UpdateSender_m22DC2C4D3C15EA3014E0DB861E4667F9E6B559E8,
	MinimalMediaNetwork_OnDestroy_m6C21A28E8FC3611AAD36C585386F66A3A4A2D1A6,
	MinimalMediaNetwork_Update_mC8009F0598C8DD27365AF987C1EE75BD421548D2,
	MinimalMediaNetwork__ctor_m7FB552CB95575627E821B943ACC27FD26A3829C9,
	SimpleCall_Start_m655D331BED4650F486795EB90C50E5BE961A15C8,
	SimpleCall_OnCallFactoryReady_m333402A7EA8A0202D964CC0D72F6E9C4C1C0E648,
	SimpleCall_OnCallFactoryFailed_mA28F6437A8F1163538DFDC3338E80634C1D69339,
	SimpleCall_InitExample_mB898C400FF90DEFBDE7A97E0933D3A20D0379E77,
	SimpleCall_Configure_m04BFAECF58DC722758280DB36BF4DE3C1EE64312,
	SimpleCall_ConfigureDelayed_m0CC5FEEB3A505ED7927053E1FA1FD82289D942DE,
	SimpleCall_Update_mA1C0C83D4E11473AEB35AD3AB5C03F9C72ECE08C,
	SimpleCall_OnDestroy_m82A5BF5F3C248FE4B215F0FF5E2841374D110022,
	SimpleCall_Cleanup_m73BB4C8F392A93856B3707BB326B5D4070DA1739,
	SimpleCall_Call_CallEvent_m74D1C0869CBAE860022A1220974371CCB6F106C5,
	SimpleCall_Call_m00D6B3B624E1F963BDC9A3BD5DEC8C2B800D66A0,
	SimpleCall_Error_m8FA5941B26214B85F93444A87337E365B2A07E4B,
	SimpleCall_Log_m60476DAE27FDC86D26F66EDFACCE44F1CB80DE08,
	SimpleCall__ctor_m242CED737135D3E9D0367D78E7B9FC26B1401DD6,
	U3CConfigureDelayedU3Ed__12__ctor_mDD7166D2DD0257103F71CED95E15B18416E2BF10,
	U3CConfigureDelayedU3Ed__12_System_IDisposable_Dispose_m6C9952A067032960423DC4B2C7C614EDE466D6D3,
	U3CConfigureDelayedU3Ed__12_MoveNext_m24CF23B0D63AD047D924F927688E11FB1587F9BE,
	U3CConfigureDelayedU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C0E76F7D7A2434D995F23A5B391589812122E73,
	U3CConfigureDelayedU3Ed__12_System_Collections_IEnumerator_Reset_m4887E29AA49CA297D26E070EA170F4CCDD651CA9,
	U3CConfigureDelayedU3Ed__12_System_Collections_IEnumerator_get_Current_m2B90F640EDCB409B04F869B611398970DFE944D9,
	OneToMany_Start_mB3EB36A88F6BCC8A395A81EBC643942F5222BF11,
	OneToMany_OnCallFactoryReady_m0B8114F0D6C8465CB5A9F803B2B97584C629D110,
	OneToMany_OnCallFactoryFailed_m245C991F1F7407001272260FEB2BB6246819F4C3,
	OneToMany_InitExample_m1AF3AB53222E54043114278934A7AA3ECB80F437,
	OneToMany_OnDestroy_mE67EDDD768E00912177B607A725B645EC37C71EB,
	OneToMany_Update_m15B3453D1EC96E56A8DE2963DD59E470BF881E66,
	OneToMany_HandleMediaEvents_m60B402EBEF44DCE8A9B34CD5854F53F438DA0DF2,
	OneToMany_Log_m0591709E724BBC94B9F97DDC7B1FA69D2451449E,
	OneToMany_HandleNetworkEvent_m87BCEEFDE59448D165C721281C85AA9F7D16C566,
	OneToMany_UpdateTexture_m7B7B2A6D6FDCD7631FB75A90B0BBD7B40D947044,
	OneToMany_UpdateTexture_mF29C33B0479C0BB76A30730CA90CBF195B48CBA3,
	OneToMany__ctor_m4F5700BD35CBAFA0EF931B81F7263C642D21A1AB,
	U3CInitExampleU3Ed__15__ctor_m08DE7F7794440272171252010AF69C01D56AA072,
	U3CInitExampleU3Ed__15_System_IDisposable_Dispose_m634FF8F5DA5B828270A1410ECC11D952124067D9,
	U3CInitExampleU3Ed__15_MoveNext_m539C24B2AC9C6495A037D3494A173BC9632DCFD4,
	U3CInitExampleU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16FA4F6AF898169AF40D9A20B54CAEC19FB4DA27,
	U3CInitExampleU3Ed__15_System_Collections_IEnumerator_Reset_mBD5B78879DFE86B744A9A65D1237D3D637589029,
	U3CInitExampleU3Ed__15_System_Collections_IEnumerator_get_Current_mA9D6328DF1CC0C11F02B1A001B22ACEC8B70D5DB,
	VideoInputApp_Start_mE87D5BC03DAD5D0CA9E0F8F77000552C27E39028,
	VideoInputApp_CoroutineRefreshLater_m0E9D223F0A5451FACE419333AB3364F60A2E5355,
	VideoInputApp__ctor_mDE762E15CAD1DBDF9A7431DCCE9C30C8ABF11E24,
	U3CCoroutineRefreshLaterU3Ed__1__ctor_mE49DA66C692B0AE846A7AD5CCE6CFB7C7F9CB705,
	U3CCoroutineRefreshLaterU3Ed__1_System_IDisposable_Dispose_mA60F05CEF5DE77FD5B0FCCB56F4210EAC531FA35,
	U3CCoroutineRefreshLaterU3Ed__1_MoveNext_mA7A1DE828C4CC597D6FBB2BACE823B1307439C5E,
	U3CCoroutineRefreshLaterU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55B0D74ECBA98511F5BC862794C5C88402421FED,
	U3CCoroutineRefreshLaterU3Ed__1_System_Collections_IEnumerator_Reset_m1F62EF6A80BC368E2A4BBA1DEA612349D9B9F98A,
	U3CCoroutineRefreshLaterU3Ed__1_System_Collections_IEnumerator_get_Current_m7A4767ACF35EE8D6448046683948201DA7368B16,
	VirtualCamera_Awake_m4D1F61B40D64AE0169D223A945E8233AE8E3D58A,
	VirtualCamera_Start_mE2B92E441DFE632ED6728128A9138369EA63C00A,
	VirtualCamera_OnDestroy_m4284B922068B602BD52D271581CF93A2EA5AF25A,
	VirtualCamera_Update_m2E92D03654CE6C390E706020F75B257106813373,
	VirtualCamera__ctor_mFE161CD90F88CBF77E293FA532ED3C70DB6243AB,
};
static const int32_t s_InvokerIndices[353] = 
{
	9442,
	9442,
	9442,
	7597,
	9442,
	9442,
	9289,
	9442,
	6745,
	4368,
	9442,
	9289,
	9442,
	9442,
	9442,
	9442,
	7646,
	9289,
	9161,
	7597,
	9442,
	9289,
	7597,
	7469,
	7469,
	7597,
	4002,
	7557,
	7469,
	3665,
	9161,
	7469,
	7469,
	9161,
	7597,
	7597,
	9442,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9289,
	9289,
	12474,
	13251,
	9289,
	12727,
	9442,
	9442,
	9442,
	4359,
	4359,
	9442,
	7557,
	7597,
	7469,
	9442,
	9442,
	7469,
	9442,
	9442,
	9289,
	9442,
	9442,
	7597,
	9442,
	7646,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	14502,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	9442,
	7008,
	9442,
	9442,
	7597,
	9442,
	9442,
	9442,
	7597,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	7423,
	4345,
	7597,
	7469,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	7597,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	7597,
	9442,
	9442,
	9442,
	9442,
	9442,
	13600,
	13600,
	14242,
	14423,
	14447,
	14252,
	14423,
	14502,
	14447,
	14502,
	14447,
	14252,
	14242,
	14423,
	14458,
	14458,
	13753,
	14423,
	14423,
	14423,
	14423,
	14423,
	11271,
	14502,
	14242,
	14502,
	14502,
	14423,
	14242,
	9442,
	14502,
	13266,
	14458,
	13266,
	14256,
	13266,
	14502,
	7597,
	14256,
	14502,
	13266,
	14256,
	9289,
	14502,
	14423,
	14447,
	9442,
	9442,
	9289,
	9289,
	9442,
	9442,
	9442,
	14423,
	14423,
	14423,
	6745,
	6745,
	3462,
	9289,
	9161,
	5478,
	9289,
	9442,
	9442,
	9442,
	7469,
	9161,
	7557,
	3659,
	9442,
	13266,
	13266,
	13922,
	9442,
	14502,
	4368,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	14502,
	9442,
	956,
	11495,
	12473,
	10736,
	10748,
	12478,
	9442,
	14502,
	0,
	0,
	0,
	0,
	14458,
	14458,
	14458,
	14458,
	14458,
	14423,
	14423,
	14458,
	14458,
	12823,
	9442,
	14502,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	9442,
	9442,
	7597,
	9442,
	9442,
	9442,
	4368,
	9442,
	4368,
	9442,
	9442,
	9442,
	9442,
	7597,
	9442,
	9442,
	4368,
	9442,
	9442,
	9442,
	9442,
	9442,
	7597,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	9442,
	7597,
	9442,
	9442,
	6750,
	9442,
	9442,
	9442,
	4368,
	9442,
	7597,
	7597,
	9442,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	9442,
	9442,
	7597,
	9289,
	9442,
	9442,
	9442,
	7597,
	7596,
	7597,
	2631,
	9442,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	9442,
	9289,
	9442,
	7557,
	9442,
	9161,
	9289,
	9442,
	9289,
	9442,
	9442,
	9442,
	9442,
	9442,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x02000017, { 0, 4 } },
};
extern const uint32_t g_rgctx_UnitySingleton_1_t6D15457E7F52728A5E82A8F1F55B78FDDF493DC2;
extern const uint32_t g_rgctx_T_tA1DF95DD7608C2C6DF43F3DEE4B3BBEDE6D3B658;
extern const uint32_t g_rgctx_T_tA1DF95DD7608C2C6DF43F3DEE4B3BBEDE6D3B658;
extern const uint32_t g_rgctx_GameObject_AddComponent_TisT_tA1DF95DD7608C2C6DF43F3DEE4B3BBEDE6D3B658_mE68BD6B5C8F552E13A0FB5F8025CD6EF78F2EB0F;
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_UnitySingleton_1_t6D15457E7F52728A5E82A8F1F55B78FDDF493DC2 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tA1DF95DD7608C2C6DF43F3DEE4B3BBEDE6D3B658 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tA1DF95DD7608C2C6DF43F3DEE4B3BBEDE6D3B658 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObject_AddComponent_TisT_tA1DF95DD7608C2C6DF43F3DEE4B3BBEDE6D3B658_mE68BD6B5C8F552E13A0FB5F8025CD6EF78F2EB0F },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_wrtc_CodeGenModule;
const Il2CppCodeGenModule g_wrtc_CodeGenModule = 
{
	"wrtc.dll",
	353,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
