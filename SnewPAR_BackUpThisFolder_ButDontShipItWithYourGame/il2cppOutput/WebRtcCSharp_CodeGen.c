﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m34EBF8A5064761FA3A1489B58F0FC88F22A91CD7_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_m1A7F32CCCD497644F0D4F54AD803BC783B68531E_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_mA3831677B0C3CB59A79EAB9F63570D4FCAE5206F_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB95333E5EEE5C6FFCF1A8B9EFEB60E7301498FDD_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m8290EEE144C47E4E40A26518F15E4456E66428BB_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_m79369FABDFC4B7A658FB90AA83A833582BA50B29_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_mB3905F1943D8A52F63FF86D33C8B55F4D2BFE1BC_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m0DDAA0B11C0577104A3C337CD95BCEB0474778CD_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m63B0AB9D01F05113151C6F5AEF564CA18A712C4C_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_mAD82A0B9BB052EE4E2C9494A5C06D29561CF350F_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_m1678867024287040EAEF9E1A6B603632E949C81C_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_mE49ACFFEB5D1BDCA69BA5306BDC2BA5764FCA627_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_m941534271CDCF96CC46373C3D001D38B687A5AAE_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_mA8895E77BEDB03935FAD2AF1684802D8B9CA66FB_RuntimeMethod_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_m38DA578E6839BE86909501F1021E85EC709E4F2A_RuntimeMethod_var;



// 0x00000001 System.Void WebRtcCSharp.AsyncDataChannel::.ctor(System.IntPtr,System.Boolean)
extern void AsyncDataChannel__ctor_m19FD25BB6ECA9C84502BCB2F4F6F0F5DEB7C116C (void);
// 0x00000002 System.Runtime.InteropServices.HandleRef WebRtcCSharp.AsyncDataChannel::getCPtr(WebRtcCSharp.AsyncDataChannel)
extern void AsyncDataChannel_getCPtr_m1FEB17D4F8E72B852E1F3E4630960DE20C8D84CE (void);
// 0x00000003 System.Void WebRtcCSharp.AsyncDataChannel::Finalize()
extern void AsyncDataChannel_Finalize_mFBBFFB96BFD494B8E8DC1A20E0FB374365ED2EF5 (void);
// 0x00000004 System.Void WebRtcCSharp.AsyncDataChannel::Dispose()
extern void AsyncDataChannel_Dispose_m6706A8AEAC29AFF0D336CC7D4C92EB35A3952B42 (void);
// 0x00000005 System.Void WebRtcCSharp.AsyncDataChannel::Init(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t)
extern void AsyncDataChannel_Init_mB9D2C99E0F50B855D88EE08F3935C6D77397E1B6 (void);
// 0x00000006 System.Int32 WebRtcCSharp.AsyncDataChannel::GetId()
extern void AsyncDataChannel_GetId_m5998F49AE2CAF3F93FEBD33156F3B6F8A60C1630 (void);
// 0x00000007 System.String WebRtcCSharp.AsyncDataChannel::GetLabel()
extern void AsyncDataChannel_GetLabel_m635564D1D9B5B3351539E887507494DD9EB41C92 (void);
// 0x00000008 System.Boolean WebRtcCSharp.AsyncDataChannel::IsInitialized()
extern void AsyncDataChannel_IsInitialized_m294E1B640D3CAD9AFD9ADEE766ED0C22C66ADCFF (void);
// 0x00000009 WebRtcCSharp.DataChannelInterface/DataState WebRtcCSharp.AsyncDataChannel::GetState()
extern void AsyncDataChannel_GetState_m21DC62A443DD9054EFF658577420A93AEC799073 (void);
// 0x0000000A System.Boolean WebRtcCSharp.AsyncDataChannel::HasMessage()
extern void AsyncDataChannel_HasMessage_m0228C09C9A4E7B41F7DF6C0E845BF07D05823073 (void);
// 0x0000000B System.Boolean WebRtcCSharp.AsyncDataChannel::DequeueMessage(WebRtcCSharp.DataBuffer)
extern void AsyncDataChannel_DequeueMessage_mAA8415D8AEBE422D4C9819002BFEC0C5F5190F70 (void);
// 0x0000000C WebRtcCSharp.Message WebRtcCSharp.AsyncDataChannel::DequeueMessage()
extern void AsyncDataChannel_DequeueMessage_m52B071BB4A6A3AAC83AC367EAD74326D26F8FAD7 (void);
// 0x0000000D System.Void WebRtcCSharp.AsyncDataChannel::Send(System.Byte[],System.UInt32,System.UInt32)
extern void AsyncDataChannel_Send_m763BB5D83E779D8BA25122B396A43409EADA5C42 (void);
// 0x0000000E System.Void WebRtcCSharp.AsyncDataChannelRef::.ctor(System.IntPtr,System.Boolean)
extern void AsyncDataChannelRef__ctor_mDAA5FB563F32AB416DCEBC24152D2F0A0CFDD755 (void);
// 0x0000000F System.Runtime.InteropServices.HandleRef WebRtcCSharp.AsyncDataChannelRef::getCPtr(WebRtcCSharp.AsyncDataChannelRef)
extern void AsyncDataChannelRef_getCPtr_m4DA4862C166C75E486E08896E92A55013A246EC4 (void);
// 0x00000010 System.Void WebRtcCSharp.AsyncDataChannelRef::Finalize()
extern void AsyncDataChannelRef_Finalize_mEEBB18C9A83ADC38D6B942B478D297D268275E00 (void);
// 0x00000011 System.Void WebRtcCSharp.AsyncDataChannelRef::Dispose()
extern void AsyncDataChannelRef_Dispose_mA3F62BFCF83E1EC11730CCA53D5641AF2ED41999 (void);
// 0x00000012 System.Void WebRtcCSharp.AsyncDataChannelRef::.ctor()
extern void AsyncDataChannelRef__ctor_m8BF1EA8B608889057DEB5C8DF47F6B45BA5904E4 (void);
// 0x00000013 System.Void WebRtcCSharp.AsyncDataChannelRef::.ctor(WebRtcCSharp.AsyncDataChannel)
extern void AsyncDataChannelRef__ctor_m2AB777F3F07058BB91672916BE1BFD883F391444 (void);
// 0x00000014 System.Void WebRtcCSharp.AsyncDataChannelRef::.ctor(WebRtcCSharp.AsyncDataChannelRef)
extern void AsyncDataChannelRef__ctor_m1C5D1068882C9095418AC5429F4E4D8E30490A12 (void);
// 0x00000015 WebRtcCSharp.AsyncDataChannel WebRtcCSharp.AsyncDataChannelRef::get()
extern void AsyncDataChannelRef_get_m0D02FA0DB33FF1DA46F0CDD772CC95C19F676751 (void);
// 0x00000016 WebRtcCSharp.AsyncDataChannel WebRtcCSharp.AsyncDataChannelRef::__deref__()
extern void AsyncDataChannelRef___deref___m2A289DF418851EDDE477D1FD88DEABC154752C92 (void);
// 0x00000017 WebRtcCSharp.AsyncDataChannel WebRtcCSharp.AsyncDataChannelRef::release()
extern void AsyncDataChannelRef_release_mF7EB507A752E717A695497676285A7A08C7B4F35 (void);
// 0x00000018 System.Void WebRtcCSharp.AsyncDataChannelRef::swap(WebRtcCSharp.SWIGTYPE_p_p_AsyncDataChannel)
extern void AsyncDataChannelRef_swap_m3ABCB2F2DD5BDF2D98522C7061AB742BD2E5CF43 (void);
// 0x00000019 System.Void WebRtcCSharp.AsyncDataChannelRef::swap(WebRtcCSharp.AsyncDataChannelRef)
extern void AsyncDataChannelRef_swap_mDFDE86B44CC15FD6CC7CCB769758CEA4CB5F21FD (void);
// 0x0000001A System.Void WebRtcCSharp.AsyncDataChannelRef::Init(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t)
extern void AsyncDataChannelRef_Init_mC2DB549AFB4F3A6F6B7FD9CC2E12D61D6CCE0BE8 (void);
// 0x0000001B System.Int32 WebRtcCSharp.AsyncDataChannelRef::GetId()
extern void AsyncDataChannelRef_GetId_m7CE2E36F89E45A63F81371B41208C3E66EEAEA42 (void);
// 0x0000001C System.String WebRtcCSharp.AsyncDataChannelRef::GetLabel()
extern void AsyncDataChannelRef_GetLabel_mC2D2C24AD91A087C717E4CD2CA6B3B77A86F5DB0 (void);
// 0x0000001D System.Boolean WebRtcCSharp.AsyncDataChannelRef::IsInitialized()
extern void AsyncDataChannelRef_IsInitialized_m63370C1258FFD029D34633FF0AF24DBB16E29766 (void);
// 0x0000001E WebRtcCSharp.DataChannelInterface/DataState WebRtcCSharp.AsyncDataChannelRef::GetState()
extern void AsyncDataChannelRef_GetState_m267DED3350D1CA81A727A5B46761A5001453185F (void);
// 0x0000001F System.Boolean WebRtcCSharp.AsyncDataChannelRef::HasMessage()
extern void AsyncDataChannelRef_HasMessage_m6C9A1DD91E7A28D37396322F1994C8DDADCDDD1E (void);
// 0x00000020 System.Boolean WebRtcCSharp.AsyncDataChannelRef::DequeueMessage(WebRtcCSharp.DataBuffer)
extern void AsyncDataChannelRef_DequeueMessage_mF26AE1B006D7537D812F3344C4C02F0BA67BB8D2 (void);
// 0x00000021 WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t WebRtcCSharp.AsyncDataChannelRef::DequeueMessage()
extern void AsyncDataChannelRef_DequeueMessage_m5E0C0674E24875A19F79BC753BB4782A3D6B611A (void);
// 0x00000022 System.Void WebRtcCSharp.AsyncDataChannelRef::Send(System.Byte[],System.UInt32,System.UInt32)
extern void AsyncDataChannelRef_Send_m2247CC9DB38CAA147EF141937C77C930A2D53BD9 (void);
// 0x00000023 System.Void WebRtcCSharp.AsyncDataChannelRef::AddRef()
extern void AsyncDataChannelRef_AddRef_mEB78DA3850005594095ADB812200139CC56E075A (void);
// 0x00000024 WebRtcCSharp.RefCountReleaseStatus WebRtcCSharp.AsyncDataChannelRef::Release()
extern void AsyncDataChannelRef_Release_mB243E9AB84E90A666E710543A202C688BB8CEB2F (void);
// 0x00000025 System.Void WebRtcCSharp.AsyncPeer::.ctor(System.IntPtr,System.Boolean)
extern void AsyncPeer__ctor_mDF3C08B17B007AD6A60B630006EE1561DEEE1C64 (void);
// 0x00000026 System.Runtime.InteropServices.HandleRef WebRtcCSharp.AsyncPeer::getCPtr(WebRtcCSharp.AsyncPeer)
extern void AsyncPeer_getCPtr_m6B155B71D6572830FD4C51C36EEC4FE69A6D0D30 (void);
// 0x00000027 System.Void WebRtcCSharp.AsyncPeer::Finalize()
extern void AsyncPeer_Finalize_mB07A6FFC84F0ABE5286E86BDDF0583DC8EDD81FA (void);
// 0x00000028 System.Void WebRtcCSharp.AsyncPeer::Dispose()
extern void AsyncPeer_Dispose_m931BBAEA988CE086BCBDF757C06176ED1F9CF062 (void);
// 0x00000029 System.Void WebRtcCSharp.AsyncPeer::Update()
extern void AsyncPeer_Update_m1F1EAE395D7B73ABD229E328B62D79D5AA248EA3 (void);
// 0x0000002A System.Void WebRtcCSharp.AsyncPeer::Flush()
extern void AsyncPeer_Flush_m6EF242F215FB4DB0EF260F98480F311E44A54153 (void);
// 0x0000002B WebRtcCSharp.AsyncPeer/ConnectionState WebRtcCSharp.AsyncPeer::GetConnectionState()
extern void AsyncPeer_GetConnectionState_m1DBDCF76F265DEDAB72D077496B4102B45CE6AFA (void);
// 0x0000002C System.Void WebRtcCSharp.AsyncPeer::CreateOffer()
extern void AsyncPeer_CreateOffer_mE0DDFE21013CFB261D498FC8796833E506650E01 (void);
// 0x0000002D System.Void WebRtcCSharp.AsyncPeer::CreateAnswer()
extern void AsyncPeer_CreateAnswer_m2EA4E572DCE8E00F91CC5A4F2100FE36C1C15497 (void);
// 0x0000002E System.Void WebRtcCSharp.AsyncPeer::SetLocalDescription(System.String)
extern void AsyncPeer_SetLocalDescription_mCCD4069C54EC8E7486DE737F4489A830D81713C1 (void);
// 0x0000002F System.Void WebRtcCSharp.AsyncPeer::SetRemoteDescription(System.String)
extern void AsyncPeer_SetRemoteDescription_m96DF578A441A6FF1AEBAA59C0A6A0F2A7C1A78E2 (void);
// 0x00000030 System.String WebRtcCSharp.AsyncPeer::GetLocalDescription()
extern void AsyncPeer_GetLocalDescription_m72D8FEC97F29196FA82EE3E3BB9B5215A8CF3A1F (void);
// 0x00000031 System.Void WebRtcCSharp.AsyncPeer::AddIceCandidate(System.String)
extern void AsyncPeer_AddIceCandidate_m3AD26B4BF9239EBE3A61A5A1EC95F595572F21E4 (void);
// 0x00000032 WebRtcCSharp.AsyncPeerEventArgs WebRtcCSharp.AsyncPeer::DequeueEvent()
extern void AsyncPeer_DequeueEvent_mCCA2AD5FE39FE927A6F9A3F52088F9F3F0CAF623 (void);
// 0x00000033 System.Void WebRtcCSharp.AsyncPeer::Close()
extern void AsyncPeer_Close_m332D627B822A6098DCF9D29FF1CC3FF57C4AA618 (void);
// 0x00000034 System.Void WebRtcCSharp.AsyncPeer::Cleanup()
extern void AsyncPeer_Cleanup_mBC2834789661F9A0D1E83B2B114EB328CF8AD504 (void);
// 0x00000035 WebRtcCSharp.AsyncDataChannelRef WebRtcCSharp.AsyncPeer::CreateDataChannel(System.String,WebRtcCSharp.DataChannelInit)
extern void AsyncPeer_CreateDataChannel_m460E0B4847B15C1E1EEAE933AC0BC152DBDCE4C0 (void);
// 0x00000036 System.Void WebRtcCSharp.AsyncPeer::AddLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void AsyncPeer_AddLocalStream_m86B8087EE604E970D41FDC3BFB5708A2D75B6026 (void);
// 0x00000037 System.Void WebRtcCSharp.AsyncPeer::AddLocalStream(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t)
extern void AsyncPeer_AddLocalStream_mEA9AFFB48FA6DA5A10758C99246272DED156D2CC (void);
// 0x00000038 System.Void WebRtcCSharp.AsyncPeer::RemoveLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void AsyncPeer_RemoveLocalStream_m8828510A20B47CB9B26BE4DEC35A67D86E2E6F01 (void);
// 0x00000039 System.Void WebRtcCSharp.AsyncPeerEventArgs::.ctor(System.IntPtr,System.Boolean)
extern void AsyncPeerEventArgs__ctor_mEC4BB058B8F5BBDF8D6FA016339EAF2F2B73BE59 (void);
// 0x0000003A System.Runtime.InteropServices.HandleRef WebRtcCSharp.AsyncPeerEventArgs::getCPtr(WebRtcCSharp.AsyncPeerEventArgs)
extern void AsyncPeerEventArgs_getCPtr_m37CB9BCBEADCA8F3897B63A12FDBBDDA9B3D549F (void);
// 0x0000003B System.Void WebRtcCSharp.AsyncPeerEventArgs::Finalize()
extern void AsyncPeerEventArgs_Finalize_mAABC2416C47964487CC53EADC422C6DE7CA3BC34 (void);
// 0x0000003C System.Void WebRtcCSharp.AsyncPeerEventArgs::Dispose()
extern void AsyncPeerEventArgs_Dispose_m32EDD92393B24F83C7FDC409EC723A08C6448DC8 (void);
// 0x0000003D System.Void WebRtcCSharp.AsyncPeerEventArgs::.ctor(WebRtcCSharp.AsyncPeerEventType)
extern void AsyncPeerEventArgs__ctor_m22D9A88F57799A462631CDB7F054E78E658FF1C6 (void);
// 0x0000003E WebRtcCSharp.AsyncPeerEventType WebRtcCSharp.AsyncPeerEventArgs::GetEventType()
extern void AsyncPeerEventArgs_GetEventType_mA56B480B834152C0F60D5DF56E108B2DB9E1DBCD (void);
// 0x0000003F WebRtcCSharp.AsyncPeerEventContentType WebRtcCSharp.AsyncPeerEventArgs::GetContentType()
extern void AsyncPeerEventArgs_GetContentType_mB1C4C82A3CA656C382EAA9FC023AF8F62EDFC7EF (void);
// 0x00000040 WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.AsyncPeerEventArgs::GetStream()
extern void AsyncPeerEventArgs_GetStream_m5A7BF002E9A858AA50484E4E52BE423538C005B1 (void);
// 0x00000041 WebRtcCSharp.AsyncDataChannelRef WebRtcCSharp.AsyncPeerEventArgs::GetDataChannel()
extern void AsyncPeerEventArgs_GetDataChannel_m35725DF8A7462CF87E4FD239E9D5AACE8B5BD994 (void);
// 0x00000042 System.String WebRtcCSharp.AsyncPeerEventArgs::GetString()
extern void AsyncPeerEventArgs_GetString_mBD8077874E6023488D457A56EB5BE42FBBD0FBBC (void);
// 0x00000043 System.Void WebRtcCSharp.AsyncPeerEventDataChannelArgs::.ctor(System.IntPtr,System.Boolean)
extern void AsyncPeerEventDataChannelArgs__ctor_m603FFADD2F057C20E32BD73E7F481706AB970196 (void);
// 0x00000044 System.Runtime.InteropServices.HandleRef WebRtcCSharp.AsyncPeerEventDataChannelArgs::getCPtr(WebRtcCSharp.AsyncPeerEventDataChannelArgs)
extern void AsyncPeerEventDataChannelArgs_getCPtr_mAA9992467480DDF9B1DE92772171E658C9056438 (void);
// 0x00000045 System.Void WebRtcCSharp.AsyncPeerEventDataChannelArgs::Finalize()
extern void AsyncPeerEventDataChannelArgs_Finalize_mE93E97A90C88AE5CB024D725CC48085793ECCFE0 (void);
// 0x00000046 System.Void WebRtcCSharp.AsyncPeerEventDataChannelArgs::Dispose()
extern void AsyncPeerEventDataChannelArgs_Dispose_mD65F342515978E10C1BF8ACA3EBAE48133A3EA06 (void);
// 0x00000047 System.Void WebRtcCSharp.AsyncPeerEventDataChannelArgs::.ctor(WebRtcCSharp.AsyncPeerEventType,WebRtcCSharp.AsyncDataChannelRef)
extern void AsyncPeerEventDataChannelArgs__ctor_m6B7C6E2AE6699E88FDDA106F49638D4BA3BA322E (void);
// 0x00000048 WebRtcCSharp.AsyncDataChannelRef WebRtcCSharp.AsyncPeerEventDataChannelArgs::GetDataChannel()
extern void AsyncPeerEventDataChannelArgs_GetDataChannel_mB9EF512B5F276E6B30A0B257AE32AED02EE2E632 (void);
// 0x00000049 System.Void WebRtcCSharp.AsyncPeerEventMediaStreamArgs::.ctor(System.IntPtr,System.Boolean)
extern void AsyncPeerEventMediaStreamArgs__ctor_mFF9F7B8D9BAC6554C5BFB079010242139BAED5BA (void);
// 0x0000004A System.Runtime.InteropServices.HandleRef WebRtcCSharp.AsyncPeerEventMediaStreamArgs::getCPtr(WebRtcCSharp.AsyncPeerEventMediaStreamArgs)
extern void AsyncPeerEventMediaStreamArgs_getCPtr_m5D5D4C32FF41BB82A493CAF88940BA6A1D412F55 (void);
// 0x0000004B System.Void WebRtcCSharp.AsyncPeerEventMediaStreamArgs::Finalize()
extern void AsyncPeerEventMediaStreamArgs_Finalize_m9CE08629DA8BB7FE47DD15BB6478560904D7E157 (void);
// 0x0000004C System.Void WebRtcCSharp.AsyncPeerEventMediaStreamArgs::Dispose()
extern void AsyncPeerEventMediaStreamArgs_Dispose_mD0DF152F2D8825593D4BB5CA39EC0EC9880D6B72 (void);
// 0x0000004D System.Void WebRtcCSharp.AsyncPeerEventMediaStreamArgs::.ctor(WebRtcCSharp.AsyncPeerEventType,WebRtcCSharp.PollingMediaStreamRef)
extern void AsyncPeerEventMediaStreamArgs__ctor_mB6FF787B6A0CFB93855CF705ADA7492A3D471E31 (void);
// 0x0000004E WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.AsyncPeerEventMediaStreamArgs::GetStream()
extern void AsyncPeerEventMediaStreamArgs_GetStream_m60686E129F229B2C42601E568D316AE1786F84CA (void);
// 0x0000004F System.Void WebRtcCSharp.AsyncPeerEventStringArgs::.ctor(System.IntPtr,System.Boolean)
extern void AsyncPeerEventStringArgs__ctor_mF8DC86E5B997DACB060B209CED4E1DD3DD220168 (void);
// 0x00000050 System.Runtime.InteropServices.HandleRef WebRtcCSharp.AsyncPeerEventStringArgs::getCPtr(WebRtcCSharp.AsyncPeerEventStringArgs)
extern void AsyncPeerEventStringArgs_getCPtr_m0EA7EBCE3F906AE28DF101D85EF8AFD29EA8D182 (void);
// 0x00000051 System.Void WebRtcCSharp.AsyncPeerEventStringArgs::Finalize()
extern void AsyncPeerEventStringArgs_Finalize_m578B0F5D7811E24BDB0C76DE69364BBDF25341DB (void);
// 0x00000052 System.Void WebRtcCSharp.AsyncPeerEventStringArgs::Dispose()
extern void AsyncPeerEventStringArgs_Dispose_m146E7A8264F38DC79AACEB800005F262B2253402 (void);
// 0x00000053 System.Void WebRtcCSharp.AsyncPeerEventStringArgs::.ctor(WebRtcCSharp.AsyncPeerEventType,System.String)
extern void AsyncPeerEventStringArgs__ctor_m1F550A4E523B51055E4BE0E29976961C0486CA42 (void);
// 0x00000054 System.String WebRtcCSharp.AsyncPeerEventStringArgs::GetString()
extern void AsyncPeerEventStringArgs_GetString_m921A20B5716358EFCF5F0C1840F983A0A65248CA (void);
// 0x00000055 WebRtcCSharp.AsyncPeerEventStringArgs WebRtcCSharp.AsyncPeerEventStringArgs::Cast(WebRtcCSharp.AsyncPeerEventArgs)
extern void AsyncPeerEventStringArgs_Cast_m198FCC7C5A7774478C8B346947FDC3EAAA054646 (void);
// 0x00000056 System.Void WebRtcCSharp.AsyncPeerRef::.ctor(System.IntPtr,System.Boolean)
extern void AsyncPeerRef__ctor_m9D1576B32E3820E0E0A5CCC83D702A97FA0EF5FC (void);
// 0x00000057 System.Runtime.InteropServices.HandleRef WebRtcCSharp.AsyncPeerRef::getCPtr(WebRtcCSharp.AsyncPeerRef)
extern void AsyncPeerRef_getCPtr_mB730207BF0385D7F508547B28DABF6A913585EAD (void);
// 0x00000058 System.Void WebRtcCSharp.AsyncPeerRef::Finalize()
extern void AsyncPeerRef_Finalize_m73F4905B7320DD7035BA2B36D0C799D9A39BC7B7 (void);
// 0x00000059 System.Void WebRtcCSharp.AsyncPeerRef::Dispose()
extern void AsyncPeerRef_Dispose_mEDB8F5557C1B898F1823B460CCF645BB1D09EEB2 (void);
// 0x0000005A System.Void WebRtcCSharp.AsyncPeerRef::.ctor()
extern void AsyncPeerRef__ctor_m3EBE86A56394F0E9CE8477E425225D67FC927A02 (void);
// 0x0000005B System.Void WebRtcCSharp.AsyncPeerRef::.ctor(WebRtcCSharp.AsyncPeer)
extern void AsyncPeerRef__ctor_mB18064D50E3D37EC7F1B5672C88BB72325C5A871 (void);
// 0x0000005C System.Void WebRtcCSharp.AsyncPeerRef::.ctor(WebRtcCSharp.AsyncPeerRef)
extern void AsyncPeerRef__ctor_m1AAF6287E24DF94F5F402F4119CCE684C80F8B65 (void);
// 0x0000005D WebRtcCSharp.AsyncPeer WebRtcCSharp.AsyncPeerRef::get()
extern void AsyncPeerRef_get_m5DFEA5E1968AB978B60A4AF39232EC7FD647857C (void);
// 0x0000005E WebRtcCSharp.AsyncPeer WebRtcCSharp.AsyncPeerRef::__deref__()
extern void AsyncPeerRef___deref___mCE7697230E896AE5E764EE4E763E7DD1E329A41A (void);
// 0x0000005F WebRtcCSharp.AsyncPeer WebRtcCSharp.AsyncPeerRef::release()
extern void AsyncPeerRef_release_mB1D10D6219E9B4960A7751DE0E8887C0D3D36C63 (void);
// 0x00000060 System.Void WebRtcCSharp.AsyncPeerRef::swap(WebRtcCSharp.SWIGTYPE_p_p_AsyncPeer)
extern void AsyncPeerRef_swap_m0A583A9860DDC31DB01170DF13EE86CA5291415F (void);
// 0x00000061 System.Void WebRtcCSharp.AsyncPeerRef::swap(WebRtcCSharp.AsyncPeerRef)
extern void AsyncPeerRef_swap_m18B0EC86E8C0396A7BCCCD21EFD2FA2CA806844A (void);
// 0x00000062 System.Void WebRtcCSharp.AsyncPeerRef::Update()
extern void AsyncPeerRef_Update_m470E04F07098DAB66ED6DA52965FD170E3535F05 (void);
// 0x00000063 System.Void WebRtcCSharp.AsyncPeerRef::Flush()
extern void AsyncPeerRef_Flush_m2FC13316278F0BC52125AE60A2012747D37C3881 (void);
// 0x00000064 WebRtcCSharp.AsyncPeer/ConnectionState WebRtcCSharp.AsyncPeerRef::GetConnectionState()
extern void AsyncPeerRef_GetConnectionState_mC6D2E93D7335F7DB3FADE68E2C9D7FD054F4C88F (void);
// 0x00000065 System.Void WebRtcCSharp.AsyncPeerRef::CreateOffer()
extern void AsyncPeerRef_CreateOffer_m577101B9B2FC0415CAF4C64C6399AB76368C3714 (void);
// 0x00000066 System.Void WebRtcCSharp.AsyncPeerRef::CreateAnswer()
extern void AsyncPeerRef_CreateAnswer_m500DF0AEE3AC2CFEFDF6330C974CC5E9FCEC1F77 (void);
// 0x00000067 System.Void WebRtcCSharp.AsyncPeerRef::SetLocalDescription(System.String)
extern void AsyncPeerRef_SetLocalDescription_m82515C335502737822B959C3AB6599605AA18577 (void);
// 0x00000068 System.Void WebRtcCSharp.AsyncPeerRef::SetRemoteDescription(System.String)
extern void AsyncPeerRef_SetRemoteDescription_m1C51C75CAE2B3AB6ADCDC2CC183FA4A6824AC305 (void);
// 0x00000069 System.String WebRtcCSharp.AsyncPeerRef::GetLocalDescription()
extern void AsyncPeerRef_GetLocalDescription_mB94F3285572E89EB8E8754C4C9A328CEC7DA5303 (void);
// 0x0000006A System.Void WebRtcCSharp.AsyncPeerRef::AddIceCandidate(System.String)
extern void AsyncPeerRef_AddIceCandidate_m9008E7A0BD4B748F82E546502515461BAF95A127 (void);
// 0x0000006B WebRtcCSharp.AsyncPeerEventArgs WebRtcCSharp.AsyncPeerRef::DequeueEvent()
extern void AsyncPeerRef_DequeueEvent_m0F2C5657BE776D91149FCB33F851840B2CFB39A5 (void);
// 0x0000006C System.Void WebRtcCSharp.AsyncPeerRef::Close()
extern void AsyncPeerRef_Close_m9D57310755F959D6D1E2B08A988A146D12C7632E (void);
// 0x0000006D System.Void WebRtcCSharp.AsyncPeerRef::Cleanup()
extern void AsyncPeerRef_Cleanup_m6A955E5EC0E37E4D38024581FF95AE3760671881 (void);
// 0x0000006E WebRtcCSharp.AsyncDataChannelRef WebRtcCSharp.AsyncPeerRef::CreateDataChannel(System.String,WebRtcCSharp.DataChannelInit)
extern void AsyncPeerRef_CreateDataChannel_m27F7CA4F0E3954C7EAEBFFE35DDB49A759B493E4 (void);
// 0x0000006F System.Void WebRtcCSharp.AsyncPeerRef::AddLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void AsyncPeerRef_AddLocalStream_m0645068979C43789C9FB555A5A43EACD5A52F597 (void);
// 0x00000070 System.Void WebRtcCSharp.AsyncPeerRef::AddLocalStream(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t)
extern void AsyncPeerRef_AddLocalStream_mCE98242FD577B10A4C5AB09D87B9AA057795CC38 (void);
// 0x00000071 System.Void WebRtcCSharp.AsyncPeerRef::RemoveLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void AsyncPeerRef_RemoveLocalStream_m491400FE90FC7F1D8161B14F64068CE6B133CCAA (void);
// 0x00000072 System.Void WebRtcCSharp.AsyncPeerRef::AddRef()
extern void AsyncPeerRef_AddRef_m1997EBE433FDA4F8F124EB88DC9964E94E9D8DB6 (void);
// 0x00000073 WebRtcCSharp.RefCountReleaseStatus WebRtcCSharp.AsyncPeerRef::Release()
extern void AsyncPeerRef_Release_m77432702CAE783E4513442E3B9B26CC0D5C23D52 (void);
// 0x00000074 System.Void WebRtcCSharp.AudioOptions::.ctor(System.IntPtr,System.Boolean)
extern void AudioOptions__ctor_m0FEC32FFD2CDC60531B6D66D546D7CCA3EF1DD0B (void);
// 0x00000075 System.Runtime.InteropServices.HandleRef WebRtcCSharp.AudioOptions::getCPtr(WebRtcCSharp.AudioOptions)
extern void AudioOptions_getCPtr_mC13930839D53631456B461964E0615B2A444794F (void);
// 0x00000076 System.Void WebRtcCSharp.AudioOptions::Finalize()
extern void AudioOptions_Finalize_mA94EE36EA1614A5D70A85EB84AFDDF3F02AD9EB7 (void);
// 0x00000077 System.Void WebRtcCSharp.AudioOptions::Dispose()
extern void AudioOptions_Dispose_m1648ABAF6E83AAD3BA2ED4E491191C2286F674DC (void);
// 0x00000078 System.Void WebRtcCSharp.AudioOptions::set_echo_cancellation_set(System.Boolean)
extern void AudioOptions_set_echo_cancellation_set_mA86B7818E22B47D25A2446299BD3893EC89EB041 (void);
// 0x00000079 System.Boolean WebRtcCSharp.AudioOptions::get_echo_cancellation_set()
extern void AudioOptions_get_echo_cancellation_set_mC2B567567DA7D12903F2F5BEF33F41AAC4502483 (void);
// 0x0000007A System.Void WebRtcCSharp.AudioOptions::set_echo_cancellation(System.Boolean)
extern void AudioOptions_set_echo_cancellation_mFF1AB47F4F8EAC70EDC8E0EA83FCECBBD64A0E0B (void);
// 0x0000007B System.Boolean WebRtcCSharp.AudioOptions::get_echo_cancellation()
extern void AudioOptions_get_echo_cancellation_mD9020BC7B58BC847047B285B170D64508634A8F1 (void);
// 0x0000007C System.Void WebRtcCSharp.AudioOptions::set_extended_filter_aec_set(System.Boolean)
extern void AudioOptions_set_extended_filter_aec_set_mCD247F2661CB3878608D821023F8E3E16FB82324 (void);
// 0x0000007D System.Boolean WebRtcCSharp.AudioOptions::get_extended_filter_aec_set()
extern void AudioOptions_get_extended_filter_aec_set_mAAB3B4A7EE121D35E0C46544B997C9AB8FF9218D (void);
// 0x0000007E System.Void WebRtcCSharp.AudioOptions::set_extended_filter_aec(System.Boolean)
extern void AudioOptions_set_extended_filter_aec_m3BA270C429F474183DE15F46AB2FB41841B0D684 (void);
// 0x0000007F System.Boolean WebRtcCSharp.AudioOptions::get_extended_filter_aec()
extern void AudioOptions_get_extended_filter_aec_mCBB98D18FFBD04507DE4C7A0642DB6581B4AE5E4 (void);
// 0x00000080 System.Void WebRtcCSharp.AudioOptions::set_delay_agnostic_aec_set(System.Boolean)
extern void AudioOptions_set_delay_agnostic_aec_set_m920C066C2A2C0B1B764F5228B206895C0F895502 (void);
// 0x00000081 System.Boolean WebRtcCSharp.AudioOptions::get_delay_agnostic_aec_set()
extern void AudioOptions_get_delay_agnostic_aec_set_mE8D66B4CAA8C8147852A60FF009567717BF05FDA (void);
// 0x00000082 System.Void WebRtcCSharp.AudioOptions::set_delay_agnostic_aec(System.Boolean)
extern void AudioOptions_set_delay_agnostic_aec_m548F7ADC512B2F2FA182FA18AC4DF03260E1C545 (void);
// 0x00000083 System.Boolean WebRtcCSharp.AudioOptions::get_delay_agnostic_aec()
extern void AudioOptions_get_delay_agnostic_aec_m81A357F21D10C3176DCC03F53248EDA50151CDC4 (void);
// 0x00000084 System.Void WebRtcCSharp.AudioOptions::set_noise_suppression_set(System.Boolean)
extern void AudioOptions_set_noise_suppression_set_mA5ED3C1F55FCC4374590E044836606479D422A04 (void);
// 0x00000085 System.Boolean WebRtcCSharp.AudioOptions::get_noise_suppression_set()
extern void AudioOptions_get_noise_suppression_set_mB9CA79F8C64A8351A9C0356C9050445CF97E5FC0 (void);
// 0x00000086 System.Void WebRtcCSharp.AudioOptions::set_noise_suppression(System.Boolean)
extern void AudioOptions_set_noise_suppression_m99E93D9641EBC6D644043CD8444D756875DE7E01 (void);
// 0x00000087 System.Boolean WebRtcCSharp.AudioOptions::get_noise_suppression()
extern void AudioOptions_get_noise_suppression_m455D51BABEC732950F8BB11E41EE7368BD1FDB90 (void);
// 0x00000088 System.Void WebRtcCSharp.AudioOptions::set_auto_gain_control_set(System.Boolean)
extern void AudioOptions_set_auto_gain_control_set_m89A511418D68B9DB948822DEE1141552B26C210C (void);
// 0x00000089 System.Boolean WebRtcCSharp.AudioOptions::get_auto_gain_control_set()
extern void AudioOptions_get_auto_gain_control_set_m83094654D0156176A0A409F017F1DBB65092E8BD (void);
// 0x0000008A System.Void WebRtcCSharp.AudioOptions::set_auto_gain_control(System.Boolean)
extern void AudioOptions_set_auto_gain_control_mBD2DF568908573E391EFB01B09A72982872CB5BF (void);
// 0x0000008B System.Boolean WebRtcCSharp.AudioOptions::get_auto_gain_control()
extern void AudioOptions_get_auto_gain_control_m8872A029B91A27FFD7DD5B78E4F5E4D84960791A (void);
// 0x0000008C System.Void WebRtcCSharp.AudioOptions::.ctor()
extern void AudioOptions__ctor_m959D928A19668442F89F05032B046723A76B72D0 (void);
// 0x0000008D System.Void WebRtcCSharp.CopyOnWriteBuffer::.ctor(System.IntPtr,System.Boolean)
extern void CopyOnWriteBuffer__ctor_m178FCED34CF8A75D6986B88FB0D68A7A1D58FB5D (void);
// 0x0000008E System.Runtime.InteropServices.HandleRef WebRtcCSharp.CopyOnWriteBuffer::getCPtr(WebRtcCSharp.CopyOnWriteBuffer)
extern void CopyOnWriteBuffer_getCPtr_m460A44F124916129D36F2D838B63B109F84F0E3D (void);
// 0x0000008F System.Void WebRtcCSharp.CopyOnWriteBuffer::Finalize()
extern void CopyOnWriteBuffer_Finalize_m2ED43C6119845D43487132DC2FEA39B1B2315D99 (void);
// 0x00000090 System.Void WebRtcCSharp.CopyOnWriteBuffer::Dispose()
extern void CopyOnWriteBuffer_Dispose_mA371BD136886AAE6E1E5A342DFF95F2FC5802171 (void);
// 0x00000091 System.Void WebRtcCSharp.CopyOnWriteBuffer::.ctor()
extern void CopyOnWriteBuffer__ctor_m9C2A8757B71ECE4EB6AE727C848AC8BF4EDC50AD (void);
// 0x00000092 System.Void WebRtcCSharp.CopyOnWriteBuffer::.ctor(WebRtcCSharp.CopyOnWriteBuffer)
extern void CopyOnWriteBuffer__ctor_mE0ECDCE9C6040E37131FC710167139630DA04477 (void);
// 0x00000093 System.Void WebRtcCSharp.CopyOnWriteBuffer::.ctor(System.String)
extern void CopyOnWriteBuffer__ctor_m0DC48FFF0CB2FF6FC4C4F54C04F99B38D32525E5 (void);
// 0x00000094 System.Void WebRtcCSharp.CopyOnWriteBuffer::.ctor(System.UInt32)
extern void CopyOnWriteBuffer__ctor_m52F22B2A29F927CA81D79BDE9B287663CD6F8762 (void);
// 0x00000095 System.Void WebRtcCSharp.CopyOnWriteBuffer::.ctor(System.UInt32,System.UInt32)
extern void CopyOnWriteBuffer__ctor_mCF43DEE25E6C1D007D88B9BC1351ECB431FFE2C3 (void);
// 0x00000096 System.UInt32 WebRtcCSharp.CopyOnWriteBuffer::size()
extern void CopyOnWriteBuffer_size_m1EC084659E2B601964908C397FD99CEFD4EC885D (void);
// 0x00000097 System.UInt32 WebRtcCSharp.CopyOnWriteBuffer::capacity()
extern void CopyOnWriteBuffer_capacity_mA0BA1CC5C95D4AFA1FC338EC8893C39AEB86B3EE (void);
// 0x00000098 System.Void WebRtcCSharp.CopyOnWriteBuffer::SetData(WebRtcCSharp.CopyOnWriteBuffer)
extern void CopyOnWriteBuffer_SetData_mBA19C0B8C50B92969807D09F584B83D65D55754C (void);
// 0x00000099 System.Void WebRtcCSharp.CopyOnWriteBuffer::AppendData(WebRtcCSharp.CopyOnWriteBuffer)
extern void CopyOnWriteBuffer_AppendData_m27A3C21130139FC0465D01C39C9638FE481A4266 (void);
// 0x0000009A System.Void WebRtcCSharp.CopyOnWriteBuffer::SetSize(System.UInt32)
extern void CopyOnWriteBuffer_SetSize_mEBCE32D3D9EA0C2732E0AFCAB0288CE9C49FB909 (void);
// 0x0000009B System.Void WebRtcCSharp.CopyOnWriteBuffer::EnsureCapacity(System.UInt32)
extern void CopyOnWriteBuffer_EnsureCapacity_mFD5D144C4B48041113FDD39A35A6C88BAFB3FF40 (void);
// 0x0000009C System.Void WebRtcCSharp.CopyOnWriteBuffer::Clear()
extern void CopyOnWriteBuffer_Clear_m1ABCCBF8B3880EE50DC2D2F877A8940E56AF8EC4 (void);
// 0x0000009D System.Void WebRtcCSharp.DataBuffer::.ctor(System.IntPtr,System.Boolean)
extern void DataBuffer__ctor_m3A9184D650712D88FBD9EDF0C7FA7C52509015DA (void);
// 0x0000009E System.Runtime.InteropServices.HandleRef WebRtcCSharp.DataBuffer::getCPtr(WebRtcCSharp.DataBuffer)
extern void DataBuffer_getCPtr_m79A560F93A90862B2787212B3722D569C4C904C4 (void);
// 0x0000009F System.Void WebRtcCSharp.DataBuffer::Finalize()
extern void DataBuffer_Finalize_mD4464E0092D91CB5326E0845900E4E9A6DAE1506 (void);
// 0x000000A0 System.Void WebRtcCSharp.DataBuffer::Dispose()
extern void DataBuffer_Dispose_mA89C22FFFE39544383ACB2047E689B45E0B4A558 (void);
// 0x000000A1 System.Void WebRtcCSharp.DataBuffer::.ctor(WebRtcCSharp.CopyOnWriteBuffer,System.Boolean)
extern void DataBuffer__ctor_mEE9638CD6A7C1F110B1EB8298669C6D4FFC42D5C (void);
// 0x000000A2 System.Void WebRtcCSharp.DataBuffer::.ctor(System.String)
extern void DataBuffer__ctor_mAD6DCA0A88C8371A987A099ECC201D0258FD4B4B (void);
// 0x000000A3 System.UInt32 WebRtcCSharp.DataBuffer::size()
extern void DataBuffer_size_m03FD715A15BB89D22C25A340B01A0F92205472AF (void);
// 0x000000A4 System.Void WebRtcCSharp.DataBuffer::set_data(WebRtcCSharp.CopyOnWriteBuffer)
extern void DataBuffer_set_data_m520FF9B62402D4AE05BC61426DF073BBE32AEE97 (void);
// 0x000000A5 WebRtcCSharp.CopyOnWriteBuffer WebRtcCSharp.DataBuffer::get_data()
extern void DataBuffer_get_data_mC0AA16E14EDCCAD2F4438C2BA0C77CF7489BE1E6 (void);
// 0x000000A6 System.Void WebRtcCSharp.DataBuffer::set_binary(System.Boolean)
extern void DataBuffer_set_binary_m74FE93602C11CE5EEA2561DD5C9717A6E5B70230 (void);
// 0x000000A7 System.Boolean WebRtcCSharp.DataBuffer::get_binary()
extern void DataBuffer_get_binary_mAFEC325A6249CE53EA8A9E4524F0B68273801D25 (void);
// 0x000000A8 System.Void WebRtcCSharp.DataChannelInit::.ctor(System.IntPtr,System.Boolean)
extern void DataChannelInit__ctor_mABE44453EF9FA29C340DFDA29DC34DD7A34F68AB (void);
// 0x000000A9 System.Runtime.InteropServices.HandleRef WebRtcCSharp.DataChannelInit::getCPtr(WebRtcCSharp.DataChannelInit)
extern void DataChannelInit_getCPtr_m19E2FD6DB1FADFC8986350F746F3B48110B490F4 (void);
// 0x000000AA System.Void WebRtcCSharp.DataChannelInit::Finalize()
extern void DataChannelInit_Finalize_m942BFA54DDECE6832A585BCCD2A43192A4ADB52E (void);
// 0x000000AB System.Void WebRtcCSharp.DataChannelInit::Dispose()
extern void DataChannelInit_Dispose_mD2769CCD4E69D7E97E8806B81F20A9EE1698396C (void);
// 0x000000AC System.Void WebRtcCSharp.DataChannelInit::set_reliable(System.Boolean)
extern void DataChannelInit_set_reliable_mDB7157E2EEA34CCB488C288A3B24C1AB00214DC4 (void);
// 0x000000AD System.Boolean WebRtcCSharp.DataChannelInit::get_reliable()
extern void DataChannelInit_get_reliable_m1036E4033A741C94490B3936A64AAF7A16F7007C (void);
// 0x000000AE System.Void WebRtcCSharp.DataChannelInit::set_ordered(System.Boolean)
extern void DataChannelInit_set_ordered_m556EC589482B3FE973FC2E308682349757CA56C2 (void);
// 0x000000AF System.Boolean WebRtcCSharp.DataChannelInit::get_ordered()
extern void DataChannelInit_get_ordered_m37DF521854E673D65168700755C40445893576BD (void);
// 0x000000B0 System.Void WebRtcCSharp.DataChannelInit::set_maxRetransmitTime(System.Int32)
extern void DataChannelInit_set_maxRetransmitTime_m2609AD3FBD62AFE4FF79E6BF8C8D9AEE85663DD5 (void);
// 0x000000B1 System.Int32 WebRtcCSharp.DataChannelInit::get_maxRetransmitTime()
extern void DataChannelInit_get_maxRetransmitTime_m98E6FD4EC11D3BE8CB43EA0EFDF677BA66BCF008 (void);
// 0x000000B2 System.Void WebRtcCSharp.DataChannelInit::set_maxRetransmits(System.Int32)
extern void DataChannelInit_set_maxRetransmits_m721FD5B3F7FA3946B1B74AA3C89CEEC5FB29B51D (void);
// 0x000000B3 System.Int32 WebRtcCSharp.DataChannelInit::get_maxRetransmits()
extern void DataChannelInit_get_maxRetransmits_mD682CC03A85B9BEAA1B3B93031DF24FBA5354C84 (void);
// 0x000000B4 System.Void WebRtcCSharp.DataChannelInit::set_protocol(System.String)
extern void DataChannelInit_set_protocol_m50D1E9DD76072F6E07F02A17B89230FE77445C83 (void);
// 0x000000B5 System.String WebRtcCSharp.DataChannelInit::get_protocol()
extern void DataChannelInit_get_protocol_m11CD4FDC24122F25C274FD8FFFA33F17C924A03D (void);
// 0x000000B6 System.Void WebRtcCSharp.DataChannelInit::set_negotiated(System.Boolean)
extern void DataChannelInit_set_negotiated_m90D1F850073EBF5F9947E09022B0EF55900A3560 (void);
// 0x000000B7 System.Boolean WebRtcCSharp.DataChannelInit::get_negotiated()
extern void DataChannelInit_get_negotiated_m46D5094A5CF67363C804676B972B232EA890EAF1 (void);
// 0x000000B8 System.Void WebRtcCSharp.DataChannelInit::set_id(System.Int32)
extern void DataChannelInit_set_id_m15ABECE1073E2A0EB1589ADD82A10E91D1BF5521 (void);
// 0x000000B9 System.Int32 WebRtcCSharp.DataChannelInit::get_id()
extern void DataChannelInit_get_id_mA5757968354AF6D79764F7325D6A9542F8EEFDEB (void);
// 0x000000BA System.Void WebRtcCSharp.DataChannelInit::.ctor()
extern void DataChannelInit__ctor_mAE4407E7CF996E6F2A381EC2A0B361817614265D (void);
// 0x000000BB System.Void WebRtcCSharp.DataChannelInterface::.ctor(System.IntPtr,System.Boolean)
extern void DataChannelInterface__ctor_mF946CE31FA7CDBAB9A6410E78D16CC1BC0BF1B2F (void);
// 0x000000BC System.Runtime.InteropServices.HandleRef WebRtcCSharp.DataChannelInterface::getCPtr(WebRtcCSharp.DataChannelInterface)
extern void DataChannelInterface_getCPtr_m3C24141E819219BAE39B63CA90AB171A3710D8EE (void);
// 0x000000BD System.Void WebRtcCSharp.DataChannelInterface::Dispose()
extern void DataChannelInterface_Dispose_m2A7D30178488D48E0AF75670F3AFBFA2116878D3 (void);
// 0x000000BE System.String WebRtcCSharp.DataChannelInterface::DataStateString(WebRtcCSharp.DataChannelInterface/DataState)
extern void DataChannelInterface_DataStateString_mB6961A812EC0ED6848F6D814D145DCC45BE3D381 (void);
// 0x000000BF System.Void WebRtcCSharp.DataChannelInterface::RegisterObserver(WebRtcCSharp.DataChannelObserver)
extern void DataChannelInterface_RegisterObserver_m7B14DBCAE730C54814E65AF2C53F7F78BD6A856D (void);
// 0x000000C0 System.Void WebRtcCSharp.DataChannelInterface::UnregisterObserver()
extern void DataChannelInterface_UnregisterObserver_mC1044D982281146186557A4E92760D55ECF4B592 (void);
// 0x000000C1 System.String WebRtcCSharp.DataChannelInterface::label()
extern void DataChannelInterface_label_m8312A5EE6594477A8760C98A54A020CB350CF75A (void);
// 0x000000C2 System.Boolean WebRtcCSharp.DataChannelInterface::reliable()
extern void DataChannelInterface_reliable_m79D93ACDED86EED25C2422827EDE07EA4E71E357 (void);
// 0x000000C3 System.Boolean WebRtcCSharp.DataChannelInterface::ordered()
extern void DataChannelInterface_ordered_m6F33EF6FE8A53B5529A1A662AE6F9B3B94B66FB3 (void);
// 0x000000C4 System.UInt16 WebRtcCSharp.DataChannelInterface::maxRetransmitTime()
extern void DataChannelInterface_maxRetransmitTime_m8F2AC9EB53EE53953119E3129E7338124242C15C (void);
// 0x000000C5 System.UInt16 WebRtcCSharp.DataChannelInterface::maxRetransmits()
extern void DataChannelInterface_maxRetransmits_mD30284715BFFBFB99711262E7D459DE06ED833ED (void);
// 0x000000C6 System.String WebRtcCSharp.DataChannelInterface::protocol()
extern void DataChannelInterface_protocol_m5D457E9F5B1B1DAC6F406D493AC524544332158A (void);
// 0x000000C7 System.Boolean WebRtcCSharp.DataChannelInterface::negotiated()
extern void DataChannelInterface_negotiated_m02C23887CE992923B13A7A8C5E695B8BEB42E3B4 (void);
// 0x000000C8 System.Int32 WebRtcCSharp.DataChannelInterface::id()
extern void DataChannelInterface_id_mCF06FF0082CB10B6F4E3AB5C51C13ABA137A54E0 (void);
// 0x000000C9 WebRtcCSharp.DataChannelInterface/DataState WebRtcCSharp.DataChannelInterface::state()
extern void DataChannelInterface_state_m38500733401D79E311573D57E7495179AAB93350 (void);
// 0x000000CA System.UInt32 WebRtcCSharp.DataChannelInterface::messages_sent()
extern void DataChannelInterface_messages_sent_m0F91709C068E4074B124EC11D8465C0A1AD92156 (void);
// 0x000000CB System.UInt64 WebRtcCSharp.DataChannelInterface::bytes_sent()
extern void DataChannelInterface_bytes_sent_m0F81C14230C2FBD141A6C09062CEAF73501C0478 (void);
// 0x000000CC System.UInt32 WebRtcCSharp.DataChannelInterface::messages_received()
extern void DataChannelInterface_messages_received_mB4B586CBC3FEF526B844B41CF23E74EB3F383363 (void);
// 0x000000CD System.UInt64 WebRtcCSharp.DataChannelInterface::bytes_received()
extern void DataChannelInterface_bytes_received_m739F468428A1D2D12DE4B03F275F570B685EBE0F (void);
// 0x000000CE System.UInt64 WebRtcCSharp.DataChannelInterface::buffered_amount()
extern void DataChannelInterface_buffered_amount_mE728FDE12E371813C7C11E8C6EB077413892A399 (void);
// 0x000000CF System.Void WebRtcCSharp.DataChannelInterface::Close()
extern void DataChannelInterface_Close_m20B06F2ADD4CD850EB07ACA154DC04528647BCFC (void);
// 0x000000D0 System.Boolean WebRtcCSharp.DataChannelInterface::Send(WebRtcCSharp.DataBuffer)
extern void DataChannelInterface_Send_m39A3C3A3473712AE0A18846EA12440E6BB428D1C (void);
// 0x000000D1 System.Void WebRtcCSharp.DataChannelObserver::.ctor(System.IntPtr,System.Boolean)
extern void DataChannelObserver__ctor_mACBD9FC16EBA3F7871D8955BEAF05B61188A1A11 (void);
// 0x000000D2 System.Runtime.InteropServices.HandleRef WebRtcCSharp.DataChannelObserver::getCPtr(WebRtcCSharp.DataChannelObserver)
extern void DataChannelObserver_getCPtr_mB19F6EB923AEAC286371FF8B92A2FB38E608CB0E (void);
// 0x000000D3 System.Void WebRtcCSharp.DataChannelObserver::Dispose()
extern void DataChannelObserver_Dispose_mB439E00F179FD8B0B3135BC88696B74436A2826A (void);
// 0x000000D4 System.Void WebRtcCSharp.DataChannelObserver::OnStateChange()
extern void DataChannelObserver_OnStateChange_m7374EFD945C7C70B22DE61A5B23DD52C3CFE0C4D (void);
// 0x000000D5 System.Void WebRtcCSharp.DataChannelObserver::OnMessage(WebRtcCSharp.DataBuffer)
extern void DataChannelObserver_OnMessage_mA62F84E472EB7C043976CB03170BAFAE33E5F625 (void);
// 0x000000D6 System.Void WebRtcCSharp.DataChannelObserver::OnBufferedAmountChange(System.UInt64)
extern void DataChannelObserver_OnBufferedAmountChange_m79C65D93FE93DF10962E51F51087DFC6350690DA (void);
// 0x000000D7 System.Void WebRtcCSharp.GlobalStats::.ctor(System.IntPtr,System.Boolean)
extern void GlobalStats__ctor_mAD91CCEEFC9F6B4818F323D3A5A267EE36B42147 (void);
// 0x000000D8 System.Runtime.InteropServices.HandleRef WebRtcCSharp.GlobalStats::getCPtr(WebRtcCSharp.GlobalStats)
extern void GlobalStats_getCPtr_mC3EABC6594D8E9622302AEBFE5686B4CA9C7EC07 (void);
// 0x000000D9 System.Void WebRtcCSharp.GlobalStats::Finalize()
extern void GlobalStats_Finalize_m400F1498DC7E11B97096322437FE1F6454AB6535 (void);
// 0x000000DA System.Void WebRtcCSharp.GlobalStats::Dispose()
extern void GlobalStats_Dispose_m0428F3826F9DC06D6C4864E43D795AEDB2582958 (void);
// 0x000000DB System.Void WebRtcCSharp.GlobalStats::SetActive(System.Boolean)
extern void GlobalStats_SetActive_m49D79838C34E66C2DED57180FFFCE2B902BE14A1 (void);
// 0x000000DC System.Boolean WebRtcCSharp.GlobalStats::IsActive()
extern void GlobalStats_IsActive_m17105CF3A75F6A6DC87154B09F5852B4209C6944 (void);
// 0x000000DD System.Void WebRtcCSharp.GlobalStats::SetRefreshTime(System.Int32)
extern void GlobalStats_SetRefreshTime_m66425DEABDCAB60AA155CB7979F7225D2C6118B8 (void);
// 0x000000DE System.Boolean WebRtcCSharp.GlobalStats::HasStats()
extern void GlobalStats_HasStats_mAE771771514B9950945324BDEBB1BF1B126DF40C (void);
// 0x000000DF System.String WebRtcCSharp.GlobalStats::Dequeue()
extern void GlobalStats_Dequeue_mA40DD183EBD7DE1BA00998670E75748DAD14B2A5 (void);
// 0x000000E0 System.Void WebRtcCSharp.GlobalStats::.ctor()
extern void GlobalStats__ctor_m56820FA68AE50F23192400263A058F1AF1654EA0 (void);
// 0x000000E1 System.Void WebRtcCSharp.IceServers::.ctor(System.IntPtr,System.Boolean)
extern void IceServers__ctor_mC2803014BCA0FBF46B8918FD33FF84EA7934A0EA (void);
// 0x000000E2 System.Runtime.InteropServices.HandleRef WebRtcCSharp.IceServers::getCPtr(WebRtcCSharp.IceServers)
extern void IceServers_getCPtr_m98772C9B231B814F7FC0F4606507C8B7CAE299E3 (void);
// 0x000000E3 System.Void WebRtcCSharp.IceServers::Finalize()
extern void IceServers_Finalize_m923E104F501DD227937323662400025BCBCAF048 (void);
// 0x000000E4 System.Void WebRtcCSharp.IceServers::Dispose()
extern void IceServers_Dispose_m2561D4367044F44CD38E4CB0CD97EE66FDA1C9DB (void);
// 0x000000E5 System.Void WebRtcCSharp.IceServers::.ctor(System.Collections.ICollection)
extern void IceServers__ctor_m5D0AAD5EFF2F4156C9789038C32456A7289DA2A6 (void);
// 0x000000E6 System.Boolean WebRtcCSharp.IceServers::get_IsFixedSize()
extern void IceServers_get_IsFixedSize_m5E6BFE856EFC9EC148700F3857661220BC005E2E (void);
// 0x000000E7 System.Boolean WebRtcCSharp.IceServers::get_IsReadOnly()
extern void IceServers_get_IsReadOnly_m422F25C939A71766B5878FC1BFD08FF3281DA146 (void);
// 0x000000E8 WebRtcCSharp.PeerConnectionInterface/IceServer WebRtcCSharp.IceServers::get_Item(System.Int32)
extern void IceServers_get_Item_mA7ABD6955D404F248C74B686BF359011804DB2CB (void);
// 0x000000E9 System.Void WebRtcCSharp.IceServers::set_Item(System.Int32,WebRtcCSharp.PeerConnectionInterface/IceServer)
extern void IceServers_set_Item_m0D8C91E3F64136CAA8F81C711C36EE2AD117CC84 (void);
// 0x000000EA System.Int32 WebRtcCSharp.IceServers::get_Capacity()
extern void IceServers_get_Capacity_m509B85E918C9D39CE7560C21FB6E6F85C413F48D (void);
// 0x000000EB System.Void WebRtcCSharp.IceServers::set_Capacity(System.Int32)
extern void IceServers_set_Capacity_mF9EB75189E55F31396C56DC20441712D136C77CD (void);
// 0x000000EC System.Int32 WebRtcCSharp.IceServers::get_Count()
extern void IceServers_get_Count_mD35F7670A35EBCF99F86A0B8EEAA22AC9F50DBB1 (void);
// 0x000000ED System.Boolean WebRtcCSharp.IceServers::get_IsSynchronized()
extern void IceServers_get_IsSynchronized_mE7BA758056D93934515B07869107EFCEC46EBD59 (void);
// 0x000000EE System.Void WebRtcCSharp.IceServers::CopyTo(WebRtcCSharp.PeerConnectionInterface/IceServer[])
extern void IceServers_CopyTo_m52F96F73C5FCC6774CD830E4FDD97D77A886E0E3 (void);
// 0x000000EF System.Void WebRtcCSharp.IceServers::CopyTo(WebRtcCSharp.PeerConnectionInterface/IceServer[],System.Int32)
extern void IceServers_CopyTo_m7A744766C6CE6C883FDCE078776A709DC2AFC6F0 (void);
// 0x000000F0 System.Void WebRtcCSharp.IceServers::CopyTo(System.Int32,WebRtcCSharp.PeerConnectionInterface/IceServer[],System.Int32,System.Int32)
extern void IceServers_CopyTo_m40666D8195235F4F579AA938D7D74CE8483E2F47 (void);
// 0x000000F1 System.Collections.Generic.IEnumerator`1<WebRtcCSharp.PeerConnectionInterface/IceServer> WebRtcCSharp.IceServers::global::System.Collections.Generic.IEnumerable<WebRtcCSharp.PeerConnectionInterface.IceServer>.GetEnumerator()
extern void IceServers_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CWebRtcCSharp_PeerConnectionInterface_IceServerU3E_GetEnumerator_m1F14031F1F27A27EE83F156C90DA25ADFC8D9E44 (void);
// 0x000000F2 System.Collections.IEnumerator WebRtcCSharp.IceServers::global::System.Collections.IEnumerable.GetEnumerator()
extern void IceServers_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_m39A8EA7B1B227C7018C665BE9D872394596FEA8B (void);
// 0x000000F3 WebRtcCSharp.IceServers/IceServersEnumerator WebRtcCSharp.IceServers::GetEnumerator()
extern void IceServers_GetEnumerator_m9366E5933BB2BD0A36F5CB32E31C11E909042346 (void);
// 0x000000F4 System.Void WebRtcCSharp.IceServers::Clear()
extern void IceServers_Clear_m950554A0EDBF856B2291545961795A73A35EBB93 (void);
// 0x000000F5 System.Void WebRtcCSharp.IceServers::Add(WebRtcCSharp.PeerConnectionInterface/IceServer)
extern void IceServers_Add_mE757CDB38F298BBC0A39BEE332447ED87F840BFD (void);
// 0x000000F6 System.UInt32 WebRtcCSharp.IceServers::size()
extern void IceServers_size_m2B38F0916D8B4A6A222ED24E3AC91850E738F3F6 (void);
// 0x000000F7 System.UInt32 WebRtcCSharp.IceServers::capacity()
extern void IceServers_capacity_m0EE7024CB0F19EDF6BDD3B9916253602408D7040 (void);
// 0x000000F8 System.Void WebRtcCSharp.IceServers::reserve(System.UInt32)
extern void IceServers_reserve_m323575D62CB8A8EF9637F87036469EB64D6BD55E (void);
// 0x000000F9 System.Void WebRtcCSharp.IceServers::.ctor()
extern void IceServers__ctor_mE1B96AA1CF11BA42D9C32D17D24753C35EB1E5CD (void);
// 0x000000FA System.Void WebRtcCSharp.IceServers::.ctor(WebRtcCSharp.IceServers)
extern void IceServers__ctor_m1A6581330F23C870769436FDC38A45FD8385E515 (void);
// 0x000000FB System.Void WebRtcCSharp.IceServers::.ctor(System.Int32)
extern void IceServers__ctor_m8D9D1BAFA2AC76F46D029065CF97451980DC8916 (void);
// 0x000000FC WebRtcCSharp.PeerConnectionInterface/IceServer WebRtcCSharp.IceServers::getitemcopy(System.Int32)
extern void IceServers_getitemcopy_m773F0203F601E7DEEEBEF503DC8394EADB8A5FFD (void);
// 0x000000FD WebRtcCSharp.PeerConnectionInterface/IceServer WebRtcCSharp.IceServers::getitem(System.Int32)
extern void IceServers_getitem_m5272E4F1B69FFC54E238577B27BAD728D4A827BB (void);
// 0x000000FE System.Void WebRtcCSharp.IceServers::setitem(System.Int32,WebRtcCSharp.PeerConnectionInterface/IceServer)
extern void IceServers_setitem_mF9F8CF571F70CC0D38ABB0523DAEE7BDCC425A5D (void);
// 0x000000FF System.Void WebRtcCSharp.IceServers::AddRange(WebRtcCSharp.IceServers)
extern void IceServers_AddRange_m5C6EF8A1EDF634DA719152B3A5815203D3ECCD89 (void);
// 0x00000100 WebRtcCSharp.IceServers WebRtcCSharp.IceServers::GetRange(System.Int32,System.Int32)
extern void IceServers_GetRange_mA5D1621641E185A4122C83C40247AECD34312575 (void);
// 0x00000101 System.Void WebRtcCSharp.IceServers::Insert(System.Int32,WebRtcCSharp.PeerConnectionInterface/IceServer)
extern void IceServers_Insert_m09DF1901FEC4CF7AC0B4138495063BA5DBE9907C (void);
// 0x00000102 System.Void WebRtcCSharp.IceServers::InsertRange(System.Int32,WebRtcCSharp.IceServers)
extern void IceServers_InsertRange_mFB5ADB0D0FC3799C7E5E4D4E6C193DE1E6F08C53 (void);
// 0x00000103 System.Void WebRtcCSharp.IceServers::RemoveAt(System.Int32)
extern void IceServers_RemoveAt_mBAA038BF0C00188CA7D75B4397B407B5D5B45220 (void);
// 0x00000104 System.Void WebRtcCSharp.IceServers::RemoveRange(System.Int32,System.Int32)
extern void IceServers_RemoveRange_m58BC8E433827CA58DC6806EE08C594D8DA3E81EB (void);
// 0x00000105 WebRtcCSharp.IceServers WebRtcCSharp.IceServers::Repeat(WebRtcCSharp.PeerConnectionInterface/IceServer,System.Int32)
extern void IceServers_Repeat_mBE146BEE6437A5AA5DC1302B3884DC02521C068E (void);
// 0x00000106 System.Void WebRtcCSharp.IceServers::Reverse()
extern void IceServers_Reverse_mE96E75782C4C619CD7CD59EBD097968F2D8CE923 (void);
// 0x00000107 System.Void WebRtcCSharp.IceServers::Reverse(System.Int32,System.Int32)
extern void IceServers_Reverse_mBCCD7415ECBC33FEA1BF5B49179D0A9B573F73CB (void);
// 0x00000108 System.Void WebRtcCSharp.IceServers::SetRange(System.Int32,WebRtcCSharp.IceServers)
extern void IceServers_SetRange_mD5F2146476754D4E27059D665B9D181C0EF49D43 (void);
// 0x00000109 System.Void WebRtcCSharp.IceServers/IceServersEnumerator::.ctor(WebRtcCSharp.IceServers)
extern void IceServersEnumerator__ctor_mCD61CF0990325DBEFC2CDD92378F95B613C3DF56 (void);
// 0x0000010A WebRtcCSharp.PeerConnectionInterface/IceServer WebRtcCSharp.IceServers/IceServersEnumerator::get_Current()
extern void IceServersEnumerator_get_Current_m3A1F66EEBE36B0456DABA198EE71FB05AFF0DAC8 (void);
// 0x0000010B System.Object WebRtcCSharp.IceServers/IceServersEnumerator::global::System.Collections.IEnumerator.get_Current()
extern void IceServersEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mC386DE830EE0A31201E9142F389AB92F922F849E (void);
// 0x0000010C System.Boolean WebRtcCSharp.IceServers/IceServersEnumerator::MoveNext()
extern void IceServersEnumerator_MoveNext_mF28C4E631C33CD18CBB2190B04BA9FAD44662510 (void);
// 0x0000010D System.Void WebRtcCSharp.IceServers/IceServersEnumerator::Reset()
extern void IceServersEnumerator_Reset_m80B28F70AAADFAC375A82E8ABE87E6E6EC33D182 (void);
// 0x0000010E System.Void WebRtcCSharp.IceServers/IceServersEnumerator::Dispose()
extern void IceServersEnumerator_Dispose_m57656E6C973E0BCE61B2E66C46F340604315FD1D (void);
// 0x0000010F System.Void WebRtcCSharp.IosHelper::.ctor(System.IntPtr,System.Boolean)
extern void IosHelper__ctor_m00C8C21BEA2C1DB3C5E792A0C86A8E190C9DAC46 (void);
// 0x00000110 System.Runtime.InteropServices.HandleRef WebRtcCSharp.IosHelper::getCPtr(WebRtcCSharp.IosHelper)
extern void IosHelper_getCPtr_mA833E9312B1E14B2F9BCF8651C4718D4D61983D4 (void);
// 0x00000111 System.Void WebRtcCSharp.IosHelper::Finalize()
extern void IosHelper_Finalize_m1AC19A5DB6382464E948BA83FCD996382FD7DFDE (void);
// 0x00000112 System.Void WebRtcCSharp.IosHelper::Dispose()
extern void IosHelper_Dispose_m0BC986B9C4FA88B03C1090C8D9D83ECB942E3621 (void);
// 0x00000113 System.Boolean WebRtcCSharp.IosHelper::IsAvailable()
extern void IosHelper_IsAvailable_m574904F801A2363C24E9A368865CC559916ECCC7 (void);
// 0x00000114 System.Void WebRtcCSharp.IosHelper::InitAudioLayer()
extern void IosHelper_InitAudioLayer_m92D687B8BB4DDE5ED88A501B96D14A8E48CEFF65 (void);
// 0x00000115 System.Void WebRtcCSharp.IosHelper::SetLoudspeakerStatus(System.Boolean)
extern void IosHelper_SetLoudspeakerStatus_mF36DD2FC9697DB14911E869D4A54AC00B365FE45 (void);
// 0x00000116 System.Boolean WebRtcCSharp.IosHelper::GetLoudspeakerStatus()
extern void IosHelper_GetLoudspeakerStatus_mA1D8D295CECF15D1493A3BB4B73F00F0CC6B2CD5 (void);
// 0x00000117 System.Void WebRtcCSharp.IosHelper::IosKeepAudioActive(System.Boolean)
extern void IosHelper_IosKeepAudioActive_mFACC298B808D20D21EEC60CE095FBE7B52798D48 (void);
// 0x00000118 System.Void WebRtcCSharp.IosHelper::.ctor()
extern void IosHelper__ctor_mA89E962F7FFFB2923113FA2422C57527F76DA163 (void);
// 0x00000119 System.Void WebRtcCSharp.MediaConstraints::.ctor(System.IntPtr,System.Boolean)
extern void MediaConstraints__ctor_m866380CE81B93DB05BF311253CB8B474EB5D8007 (void);
// 0x0000011A System.Runtime.InteropServices.HandleRef WebRtcCSharp.MediaConstraints::getCPtr(WebRtcCSharp.MediaConstraints)
extern void MediaConstraints_getCPtr_m78384A67058BA895902BC4C141436F3D1C9195BC (void);
// 0x0000011B System.Void WebRtcCSharp.MediaConstraints::Finalize()
extern void MediaConstraints_Finalize_m1EB709040E676E8D0EF8F06C5818BA3846FCE1D1 (void);
// 0x0000011C System.Void WebRtcCSharp.MediaConstraints::Dispose()
extern void MediaConstraints_Dispose_m792FEE24EE43278BC0A449EACA4FEE4B021C78A0 (void);
// 0x0000011D System.Void WebRtcCSharp.MediaConstraints::set_audio(System.Boolean)
extern void MediaConstraints_set_audio_m34996BCDEB475423D7518D12F6F6A7C651CAD880 (void);
// 0x0000011E System.Boolean WebRtcCSharp.MediaConstraints::get_audio()
extern void MediaConstraints_get_audio_mD0982EC766D7CA0BB9C5339E00894E2DE607A41D (void);
// 0x0000011F System.Void WebRtcCSharp.MediaConstraints::set_video(System.Boolean)
extern void MediaConstraints_set_video_mD4EC8D7545514FF1580D85B1243F4102923E622B (void);
// 0x00000120 System.Boolean WebRtcCSharp.MediaConstraints::get_video()
extern void MediaConstraints_get_video_mA0E7FE9015D1E6E2FC3A30CFC8D016A99CB008E1 (void);
// 0x00000121 System.Void WebRtcCSharp.MediaConstraints::set_videoDeviceName(System.String)
extern void MediaConstraints_set_videoDeviceName_m1C0B2B31A70BF6D85A69ADF757F83BEC93A5C3EF (void);
// 0x00000122 System.String WebRtcCSharp.MediaConstraints::get_videoDeviceName()
extern void MediaConstraints_get_videoDeviceName_m7116AFDA2D870DE244D7372A6D7C73CCCBBB4C2A (void);
// 0x00000123 System.Void WebRtcCSharp.MediaConstraints::set_idealWidth(System.Int32)
extern void MediaConstraints_set_idealWidth_m7ED671A1907B7DFFB3BC8D3A64C22D54BE115C64 (void);
// 0x00000124 System.Int32 WebRtcCSharp.MediaConstraints::get_idealWidth()
extern void MediaConstraints_get_idealWidth_m53AE277402C5ABD7B53B498A4C67AC0907A18BBB (void);
// 0x00000125 System.Void WebRtcCSharp.MediaConstraints::set_idealHeight(System.Int32)
extern void MediaConstraints_set_idealHeight_mC6D08F631DBC10A58E1B7E8C02A2E67E877E9173 (void);
// 0x00000126 System.Int32 WebRtcCSharp.MediaConstraints::get_idealHeight()
extern void MediaConstraints_get_idealHeight_mA21CB7FA3B8EDECC3B3BB87BC735F35ED04FB304 (void);
// 0x00000127 System.Void WebRtcCSharp.MediaConstraints::set_minWidth(System.Int32)
extern void MediaConstraints_set_minWidth_mA9420AD87C0F57060F37126D1E9D72465785824F (void);
// 0x00000128 System.Int32 WebRtcCSharp.MediaConstraints::get_minWidth()
extern void MediaConstraints_get_minWidth_m56C1491CD6FE5FBFE9EF5F13F38D873442322318 (void);
// 0x00000129 System.Void WebRtcCSharp.MediaConstraints::set_minHeight(System.Int32)
extern void MediaConstraints_set_minHeight_m59898D0DDD0D0E01DD01ABF66E51F76C8A3E2603 (void);
// 0x0000012A System.Int32 WebRtcCSharp.MediaConstraints::get_minHeight()
extern void MediaConstraints_get_minHeight_mE468E84B3E6D4FD075ABAE1DCA3600D6C027D65A (void);
// 0x0000012B System.Void WebRtcCSharp.MediaConstraints::set_maxWidth(System.Int32)
extern void MediaConstraints_set_maxWidth_mC4B19DE0C4487C7BE5297913E54822D4A02DB8B0 (void);
// 0x0000012C System.Int32 WebRtcCSharp.MediaConstraints::get_maxWidth()
extern void MediaConstraints_get_maxWidth_m8491AC6F1A3845D5071873390E5395B8F0A0E89D (void);
// 0x0000012D System.Void WebRtcCSharp.MediaConstraints::set_maxHeight(System.Int32)
extern void MediaConstraints_set_maxHeight_mA37FFD0FAAD83481A596678DA2F5EC0A6DD3A55F (void);
// 0x0000012E System.Int32 WebRtcCSharp.MediaConstraints::get_maxHeight()
extern void MediaConstraints_get_maxHeight_m7D69B6BE9B0A67069A83DD0CD0E241DAD1365924 (void);
// 0x0000012F System.Void WebRtcCSharp.MediaConstraints::set_idealFrameRate(System.Int32)
extern void MediaConstraints_set_idealFrameRate_mB77BA994CDAD34BB3F21AB1DE22D61957C384F3F (void);
// 0x00000130 System.Int32 WebRtcCSharp.MediaConstraints::get_idealFrameRate()
extern void MediaConstraints_get_idealFrameRate_m285207EB37795CA9D7B415D1553CBAE7DCD922FE (void);
// 0x00000131 System.Void WebRtcCSharp.MediaConstraints::set_maxFrameRate(System.Int32)
extern void MediaConstraints_set_maxFrameRate_m9ECBA395C17220DEB3CAF42579DAE3A20A191295 (void);
// 0x00000132 System.Int32 WebRtcCSharp.MediaConstraints::get_maxFrameRate()
extern void MediaConstraints_get_maxFrameRate_mFC432371763F8BF879158E0A97E9EC159A86BD53 (void);
// 0x00000133 System.Void WebRtcCSharp.MediaConstraints::set_minFrameRate(System.Int32)
extern void MediaConstraints_set_minFrameRate_m74D17C1171138BA54872EA407DB59D626F90F516 (void);
// 0x00000134 System.Int32 WebRtcCSharp.MediaConstraints::get_minFrameRate()
extern void MediaConstraints_get_minFrameRate_m5325D9ADB2023D117D2BFA6B32B0AB96017DA837 (void);
// 0x00000135 System.Void WebRtcCSharp.MediaConstraints::.ctor()
extern void MediaConstraints__ctor_m5D709FE5CD7567F77ABBC4459CE8DE6A0BF7B3A9 (void);
// 0x00000136 System.Void WebRtcCSharp.Message::.ctor(System.IntPtr,System.Boolean)
extern void Message__ctor_m89237E352D96A6543F440E2F8A8DEA7DD2A3A926 (void);
// 0x00000137 System.Runtime.InteropServices.HandleRef WebRtcCSharp.Message::getCPtr(WebRtcCSharp.Message)
extern void Message_getCPtr_m7E138B00179CA6FBBEC7420B44C120F91146F86E (void);
// 0x00000138 System.Void WebRtcCSharp.Message::Finalize()
extern void Message_Finalize_m21CF408DCB90F9B585ED0A188E19552CB827E62A (void);
// 0x00000139 System.Void WebRtcCSharp.Message::Dispose()
extern void Message_Dispose_mD1BE76908B93B2916CF1A3C7424AC56021D3B9F0 (void);
// 0x0000013A System.Void WebRtcCSharp.Message::.ctor()
extern void Message__ctor_m37989203344B8A5A839398EA86C17FF441E77ED7 (void);
// 0x0000013B System.UInt32 WebRtcCSharp.Message::GetSize()
extern void Message_GetSize_m9AD3DF7CC192607EE0AAD46235946D613DB95333 (void);
// 0x0000013C System.Boolean WebRtcCSharp.Message::GetData(WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.UInt32)
extern void Message_GetData_m51ACD93291BABB3BFFF17AB058C383A0E451BCC5 (void);
// 0x0000013D System.Void WebRtcCSharp.NV12ToI420Scaler::.ctor(System.IntPtr,System.Boolean)
extern void NV12ToI420Scaler__ctor_m9244720F1DC182922239D745C0D154686E437DE2 (void);
// 0x0000013E System.Runtime.InteropServices.HandleRef WebRtcCSharp.NV12ToI420Scaler::getCPtr(WebRtcCSharp.NV12ToI420Scaler)
extern void NV12ToI420Scaler_getCPtr_mE13D07AA760B18A1738C395355A92D2AB38295A6 (void);
// 0x0000013F System.Void WebRtcCSharp.NV12ToI420Scaler::Finalize()
extern void NV12ToI420Scaler_Finalize_m8505803161EDF8CF3751A92C8F69C91D4223CD9F (void);
// 0x00000140 System.Void WebRtcCSharp.NV12ToI420Scaler::Dispose()
extern void NV12ToI420Scaler_Dispose_mC899617F5CF278944BA95AE9F48FA0E152D26B5D (void);
// 0x00000141 System.Void WebRtcCSharp.NV12ToI420Scaler::.ctor()
extern void NV12ToI420Scaler__ctor_m938020211669DDF68491738BD67F0F2D83E30300 (void);
// 0x00000142 System.Void WebRtcCSharp.NV12ToI420Scaler::NV12ToI420Scale(WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.Int32,WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.Int32,System.Int32,System.Int32,WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.Int32,WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.Int32,WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.Int32,System.Int32,System.Int32)
extern void NV12ToI420Scaler_NV12ToI420Scale_mD3F4D48DAD429B3073E212EFBF027CC59A5CD4F9 (void);
// 0x00000143 System.Void WebRtcCSharp.PeerConnectionInterface::.ctor(System.IntPtr,System.Boolean)
extern void PeerConnectionInterface__ctor_m4283CD1BAE853C7C6048F4356C4E86351A209F10 (void);
// 0x00000144 System.Runtime.InteropServices.HandleRef WebRtcCSharp.PeerConnectionInterface::getCPtr(WebRtcCSharp.PeerConnectionInterface)
extern void PeerConnectionInterface_getCPtr_mABA0DFA24718E24E072DD2A45C2FA002E6CDE300 (void);
// 0x00000145 System.Void WebRtcCSharp.PeerConnectionInterface::Dispose()
extern void PeerConnectionInterface_Dispose_mE8CCDA7694CE0D0123F825E1FFED6FCAA0DD793E (void);
// 0x00000146 WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t WebRtcCSharp.PeerConnectionInterface::local_streams()
extern void PeerConnectionInterface_local_streams_m47A10FB7E5DD12A59037CD6AE7E6061893A34705 (void);
// 0x00000147 WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t WebRtcCSharp.PeerConnectionInterface::remote_streams()
extern void PeerConnectionInterface_remote_streams_m68DBECB6629E4CC48B7BDCC7015C819A202D22B9 (void);
// 0x00000148 System.Boolean WebRtcCSharp.PeerConnectionInterface::AddStream(WebRtcCSharp.SWIGTYPE_p_MediaStreamInterface)
extern void PeerConnectionInterface_AddStream_m259FB6C5FC7FAC4951D9DBB15F2891AC57E4256E (void);
// 0x00000149 System.Void WebRtcCSharp.PeerConnectionInterface::RemoveStream(WebRtcCSharp.SWIGTYPE_p_MediaStreamInterface)
extern void PeerConnectionInterface_RemoveStream_mCDD2875F8B68AF56A53D8425F8B178D1C1D78470 (void);
// 0x0000014A System.Boolean WebRtcCSharp.PeerConnectionInterface::RemoveTrack(WebRtcCSharp.SWIGTYPE_p_RtpSenderInterface)
extern void PeerConnectionInterface_RemoveTrack_mA29D378F8340F2266093AC843B424ECF49B90168 (void);
// 0x0000014B WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t WebRtcCSharp.PeerConnectionInterface::CreateSender(System.String,System.String)
extern void PeerConnectionInterface_CreateSender_m312C6FE40BF084C38DCB0E20F155273813D0CFC5 (void);
// 0x0000014C WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t WebRtcCSharp.PeerConnectionInterface::GetSenders()
extern void PeerConnectionInterface_GetSenders_m93BFF74F3850A9E53917D896C72018B95EDF32C6 (void);
// 0x0000014D WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t WebRtcCSharp.PeerConnectionInterface::GetReceivers()
extern void PeerConnectionInterface_GetReceivers_mE5F6FE5A2C55DDD8C8C7AF91BFB2D9B86BEF1DD9 (void);
// 0x0000014E WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t WebRtcCSharp.PeerConnectionInterface::GetTransceivers()
extern void PeerConnectionInterface_GetTransceivers_mD140934B2D10434CA62A0F046FC9EAF3EFCC3BEE (void);
// 0x0000014F System.Boolean WebRtcCSharp.PeerConnectionInterface::GetStats(WebRtcCSharp.SWIGTYPE_p_webrtc__StatsObserver,WebRtcCSharp.SWIGTYPE_p_MediaStreamTrackInterface,WebRtcCSharp.PeerConnectionInterface/StatsOutputLevel)
extern void PeerConnectionInterface_GetStats_mBFB968D0ECF853DFD35B85DB363E168940078D5C (void);
// 0x00000150 System.Void WebRtcCSharp.PeerConnectionInterface::GetStats(WebRtcCSharp.SWIGTYPE_p_RTCStatsCollectorCallback)
extern void PeerConnectionInterface_GetStats_mEE38F41756D1CE723FC5D1E0B7982BCA400BB7A2 (void);
// 0x00000151 System.Void WebRtcCSharp.PeerConnectionInterface::GetStats(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t,WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t)
extern void PeerConnectionInterface_GetStats_m6A82BD5C2A985163FB0F41B268E9CE7ACE04A7EC (void);
// 0x00000152 System.Void WebRtcCSharp.PeerConnectionInterface::GetStats(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t,WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t)
extern void PeerConnectionInterface_GetStats_m80754975CEFB2FF92111FDDEFD439EC772E52BFE (void);
// 0x00000153 System.Void WebRtcCSharp.PeerConnectionInterface::ClearStatsCache()
extern void PeerConnectionInterface_ClearStatsCache_m822BC7F1AC9D816FDC309D3602ED5E373C0481A9 (void);
// 0x00000154 WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t WebRtcCSharp.PeerConnectionInterface::CreateDataChannel(System.String,WebRtcCSharp.DataChannelInit)
extern void PeerConnectionInterface_CreateDataChannel_m9EBC96C45F008CCC8345FEFA4F43C38C63652CC2 (void);
// 0x00000155 WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface WebRtcCSharp.PeerConnectionInterface::local_description()
extern void PeerConnectionInterface_local_description_mF2449D57A3C8062A7CE9FB80940BF3F02C3AF563 (void);
// 0x00000156 WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface WebRtcCSharp.PeerConnectionInterface::remote_description()
extern void PeerConnectionInterface_remote_description_m2411A05DCE8BB5E75903F69B1EAFA765F206BDF8 (void);
// 0x00000157 WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface WebRtcCSharp.PeerConnectionInterface::current_local_description()
extern void PeerConnectionInterface_current_local_description_m3BD6503504EBC401931EC8D08FFA584809331511 (void);
// 0x00000158 WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface WebRtcCSharp.PeerConnectionInterface::current_remote_description()
extern void PeerConnectionInterface_current_remote_description_m0743BB57BFA4349C4217F28B42BC656501332299 (void);
// 0x00000159 WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface WebRtcCSharp.PeerConnectionInterface::pending_local_description()
extern void PeerConnectionInterface_pending_local_description_m4519DB9733BC7AE9C6B9258EC36A33768AC88605 (void);
// 0x0000015A WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface WebRtcCSharp.PeerConnectionInterface::pending_remote_description()
extern void PeerConnectionInterface_pending_remote_description_m6BDA057C33A082A632D7E3BDD75FE52F58B7A1E7 (void);
// 0x0000015B System.Void WebRtcCSharp.PeerConnectionInterface::CreateOffer(WebRtcCSharp.SWIGTYPE_p_CreateSessionDescriptionObserver,WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions)
extern void PeerConnectionInterface_CreateOffer_mB1286C2D1DB966A755EE6DA2B6C5BCA3A2202D93 (void);
// 0x0000015C System.Void WebRtcCSharp.PeerConnectionInterface::CreateAnswer(WebRtcCSharp.SWIGTYPE_p_CreateSessionDescriptionObserver,WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions)
extern void PeerConnectionInterface_CreateAnswer_m51AB2DA2E4979991DEF859AE9A75BDD36DBD6047 (void);
// 0x0000015D System.Void WebRtcCSharp.PeerConnectionInterface::SetLocalDescription(WebRtcCSharp.SWIGTYPE_p_SetSessionDescriptionObserver,WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface)
extern void PeerConnectionInterface_SetLocalDescription_m3F5D20182A0A43FEA5B08562D7182DDB70F4390B (void);
// 0x0000015E WebRtcCSharp.PeerConnectionInterface/RTCConfiguration WebRtcCSharp.PeerConnectionInterface::GetConfiguration()
extern void PeerConnectionInterface_GetConfiguration_m7CED4B814AAEC6024C51E214507E1469B8048EEE (void);
// 0x0000015F System.Boolean WebRtcCSharp.PeerConnectionInterface::SetConfiguration(WebRtcCSharp.PeerConnectionInterface/RTCConfiguration,WebRtcCSharp.SWIGTYPE_p_RTCError)
extern void PeerConnectionInterface_SetConfiguration_m13942C439D1449B39E1820EC9B742F1D91AA62D9 (void);
// 0x00000160 System.Boolean WebRtcCSharp.PeerConnectionInterface::SetConfiguration(WebRtcCSharp.PeerConnectionInterface/RTCConfiguration)
extern void PeerConnectionInterface_SetConfiguration_m796C906CCBC5198F9F3545F61C6F5057102A26AD (void);
// 0x00000161 System.Boolean WebRtcCSharp.PeerConnectionInterface::AddIceCandidate(WebRtcCSharp.SWIGTYPE_p_IceCandidateInterface)
extern void PeerConnectionInterface_AddIceCandidate_m6A6319F7F47BA01E8BA6ABDC031B846D3C86DB7C (void);
// 0x00000162 System.Boolean WebRtcCSharp.PeerConnectionInterface::RemoveIceCandidates(WebRtcCSharp.SWIGTYPE_p_std__vectorT_cricket__Candidate_t)
extern void PeerConnectionInterface_RemoveIceCandidates_mE1B4D0D9A419FA6D6A9CE783FEBBD98C0F1E356E (void);
// 0x00000163 System.Void WebRtcCSharp.PeerConnectionInterface::SetAudioPlayout(System.Boolean)
extern void PeerConnectionInterface_SetAudioPlayout_m5B14E60E75534953C32BEAFBCD0B28BAF26C65A3 (void);
// 0x00000164 System.Void WebRtcCSharp.PeerConnectionInterface::SetAudioRecording(System.Boolean)
extern void PeerConnectionInterface_SetAudioRecording_mE91361C2A745848A3F8B61ADFB8CCAE63275147E (void);
// 0x00000165 WebRtcCSharp.PeerConnectionInterface/SignalingState WebRtcCSharp.PeerConnectionInterface::signaling_state()
extern void PeerConnectionInterface_signaling_state_m606ACE57E2A19061AE94E7E79A7484951A5F54AA (void);
// 0x00000166 WebRtcCSharp.PeerConnectionInterface/IceConnectionState WebRtcCSharp.PeerConnectionInterface::ice_connection_state()
extern void PeerConnectionInterface_ice_connection_state_m6171601BFCDF77B007A9A92BCFAFA3054ADF1FB4 (void);
// 0x00000167 WebRtcCSharp.PeerConnectionInterface/PeerConnectionState WebRtcCSharp.PeerConnectionInterface::peer_connection_state()
extern void PeerConnectionInterface_peer_connection_state_mA11AB2C4BE7661EF59F73624B680DCFD73AF2FF5 (void);
// 0x00000168 WebRtcCSharp.PeerConnectionInterface/IceGatheringState WebRtcCSharp.PeerConnectionInterface::ice_gathering_state()
extern void PeerConnectionInterface_ice_gathering_state_m76A8AD85C6D18C002BDF32C54AF52C5E83EB784C (void);
// 0x00000169 System.Void WebRtcCSharp.PeerConnectionInterface::StopRtcEventLog()
extern void PeerConnectionInterface_StopRtcEventLog_mEF83C4509EE490EFDF37D87FF11E29A78A44F13F (void);
// 0x0000016A System.Void WebRtcCSharp.PeerConnectionInterface::Close()
extern void PeerConnectionInterface_Close_mD5265B50A19399641BE69263C5DB85292F3CA13C (void);
// 0x0000016B System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::.ctor(System.IntPtr,System.Boolean)
extern void IceServer__ctor_m900596C7F05D922465D62CDD05067ABEC01DD0AD (void);
// 0x0000016C System.Runtime.InteropServices.HandleRef WebRtcCSharp.PeerConnectionInterface/IceServer::getCPtr(WebRtcCSharp.PeerConnectionInterface/IceServer)
extern void IceServer_getCPtr_m8D83DA46593711F7140E7D0506477FBCB8D9E7FD (void);
// 0x0000016D System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::Finalize()
extern void IceServer_Finalize_m5E3C70BF13B413AA202601D09E50C833F89EE2C4 (void);
// 0x0000016E System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::Dispose()
extern void IceServer_Dispose_mE7145513E62EB3A44175AC62034B4DBEC35A02CC (void);
// 0x0000016F System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::.ctor()
extern void IceServer__ctor_mEB025BA812A0AC5B4BFD1F59B8671AEBEB24CB86 (void);
// 0x00000170 System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::.ctor(WebRtcCSharp.PeerConnectionInterface/IceServer)
extern void IceServer__ctor_mDE714E11DF06F7952DB31A0BD3398B0C5C21A3E2 (void);
// 0x00000171 System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::set_uri(System.String)
extern void IceServer_set_uri_m65AEC2DDD98BA3A6B7B9A09D886AF3BADAC5AFB8 (void);
// 0x00000172 System.String WebRtcCSharp.PeerConnectionInterface/IceServer::get_uri()
extern void IceServer_get_uri_mA13AFDF14D2EA55E9036D8969EFEAEA85407A20A (void);
// 0x00000173 System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::set_urls(WebRtcCSharp.StringVector)
extern void IceServer_set_urls_m74F453AA769D0C50DF0201F6AEBDB67950F886FA (void);
// 0x00000174 WebRtcCSharp.StringVector WebRtcCSharp.PeerConnectionInterface/IceServer::get_urls()
extern void IceServer_get_urls_m4028E7A76A9626A61AA59467B1018FC5EA2DFD85 (void);
// 0x00000175 System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::set_username(System.String)
extern void IceServer_set_username_mADC5FF93D2502493F6A785E27F7ED19EEE430CD8 (void);
// 0x00000176 System.String WebRtcCSharp.PeerConnectionInterface/IceServer::get_username()
extern void IceServer_get_username_m252A57DB19973FF37DF5B2B81BD6AD1F7C35081D (void);
// 0x00000177 System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::set_password(System.String)
extern void IceServer_set_password_m6876E3D21E169E4EB1BF652749E79A8A44455B42 (void);
// 0x00000178 System.String WebRtcCSharp.PeerConnectionInterface/IceServer::get_password()
extern void IceServer_get_password_m00A21795B436339DEA388DAA7570E0DE5E7D5A41 (void);
// 0x00000179 System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::set_tls_cert_policy(WebRtcCSharp.PeerConnectionInterface/TlsCertPolicy)
extern void IceServer_set_tls_cert_policy_m2135F0C1C2ECA4AE961E5EC21BCD6A61BBBCC012 (void);
// 0x0000017A WebRtcCSharp.PeerConnectionInterface/TlsCertPolicy WebRtcCSharp.PeerConnectionInterface/IceServer::get_tls_cert_policy()
extern void IceServer_get_tls_cert_policy_m862FBA61E1473013D61F021688052FF050ED258A (void);
// 0x0000017B System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::set_hostname(System.String)
extern void IceServer_set_hostname_m11AFE418C877D1B6792538488F61FCA8260297A6 (void);
// 0x0000017C System.String WebRtcCSharp.PeerConnectionInterface/IceServer::get_hostname()
extern void IceServer_get_hostname_m7954FACB2B18AA4855E65C9F24866F1B15604D69 (void);
// 0x0000017D System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::set_tls_alpn_protocols(WebRtcCSharp.StringVector)
extern void IceServer_set_tls_alpn_protocols_m34882B2565D7C1C33F42F0E7562F9369837B6460 (void);
// 0x0000017E WebRtcCSharp.StringVector WebRtcCSharp.PeerConnectionInterface/IceServer::get_tls_alpn_protocols()
extern void IceServer_get_tls_alpn_protocols_m1E60AEEF523588BE15E29DB2BB48C52C322CD931 (void);
// 0x0000017F System.Void WebRtcCSharp.PeerConnectionInterface/IceServer::set_tls_elliptic_curves(WebRtcCSharp.StringVector)
extern void IceServer_set_tls_elliptic_curves_m4416ED2533E010757015C87EBF97E146DB0636D1 (void);
// 0x00000180 WebRtcCSharp.StringVector WebRtcCSharp.PeerConnectionInterface/IceServer::get_tls_elliptic_curves()
extern void IceServer_get_tls_elliptic_curves_m61325137ED0024FC8AEBFB75BED9014E727CF61E (void);
// 0x00000181 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::.ctor(System.IntPtr,System.Boolean)
extern void RTCConfiguration__ctor_m1F9B671F80EA3B0863565449B7E1E33B1C83CB96 (void);
// 0x00000182 System.Runtime.InteropServices.HandleRef WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::getCPtr(WebRtcCSharp.PeerConnectionInterface/RTCConfiguration)
extern void RTCConfiguration_getCPtr_m6F1F6CB416659B654526DD0D508EB1BAF65C9E9B (void);
// 0x00000183 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::Finalize()
extern void RTCConfiguration_Finalize_mE3F659BD0D16147CE1F5D7573C2317CA045D4E9A (void);
// 0x00000184 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::Dispose()
extern void RTCConfiguration_Dispose_m62F000D1B83433445BA5C625B76616F11E03EC53 (void);
// 0x00000185 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::.ctor()
extern void RTCConfiguration__ctor_mA5A485A24AB4A9088D0F6E2FCA96AAD29D07E690 (void);
// 0x00000186 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::.ctor(WebRtcCSharp.PeerConnectionInterface/RTCConfiguration)
extern void RTCConfiguration__ctor_m7B515BA8018191561E206DEB9ED35246CDBA6D0D (void);
// 0x00000187 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::.ctor(WebRtcCSharp.PeerConnectionInterface/RTCConfigurationType)
extern void RTCConfiguration__ctor_m04F007212E7161D054F17991C3ECA1ACEA96C81C (void);
// 0x00000188 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::dscp()
extern void RTCConfiguration_dscp_m91D7FB0A47B3E8D3346A9B09CFA08B54C7CB9D1E (void);
// 0x00000189 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_dscp(System.Boolean)
extern void RTCConfiguration_set_dscp_m845A5D427C3408535721579D8BFD4DC47725F432 (void);
// 0x0000018A System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::cpu_adaptation()
extern void RTCConfiguration_cpu_adaptation_m9A09526D037946C484451F600478FC18702BD5BC (void);
// 0x0000018B System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_cpu_adaptation(System.Boolean)
extern void RTCConfiguration_set_cpu_adaptation_m254598EC1EAC5B6D6AEC50EAE249DA41F73743BB (void);
// 0x0000018C System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::suspend_below_min_bitrate()
extern void RTCConfiguration_suspend_below_min_bitrate_m505E1E8DF36B558F9DD790B7A910637B981705F9 (void);
// 0x0000018D System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_suspend_below_min_bitrate(System.Boolean)
extern void RTCConfiguration_set_suspend_below_min_bitrate_m70AF85CBF9BC5DCFFB809BECE897504302689376 (void);
// 0x0000018E System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::prerenderer_smoothing()
extern void RTCConfiguration_prerenderer_smoothing_mF4108DF4A5EBCA2FCB3E87B6C21E9D29913634D9 (void);
// 0x0000018F System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_prerenderer_smoothing(System.Boolean)
extern void RTCConfiguration_set_prerenderer_smoothing_m6D3197B240E733ADE163631D038E8A51F8314C86 (void);
// 0x00000190 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::experiment_cpu_load_estimator()
extern void RTCConfiguration_experiment_cpu_load_estimator_mCB1F88D443215A334FB1C47D387A692B006D5AA8 (void);
// 0x00000191 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_experiment_cpu_load_estimator(System.Boolean)
extern void RTCConfiguration_set_experiment_cpu_load_estimator_m43D916D6C42C98E311C7016BEE840CC3DAA5CB36 (void);
// 0x00000192 System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::audio_rtcp_report_interval_ms()
extern void RTCConfiguration_audio_rtcp_report_interval_ms_m78B2FFFDABC43C974E2FA951A45CD22AE3094F75 (void);
// 0x00000193 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_audio_rtcp_report_interval_ms(System.Int32)
extern void RTCConfiguration_set_audio_rtcp_report_interval_ms_m012C4F82625EA71F4E061415D18E652DE5B2FE0A (void);
// 0x00000194 System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::video_rtcp_report_interval_ms()
extern void RTCConfiguration_video_rtcp_report_interval_ms_m53CA0BA5AA0AFBB0EFCECDA237D93F09AF5D4470 (void);
// 0x00000195 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_video_rtcp_report_interval_ms(System.Int32)
extern void RTCConfiguration_set_video_rtcp_report_interval_ms_mEA562E4D34D4FB15D9B62F180B0FFE70309B2E3C (void);
// 0x00000196 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_servers(WebRtcCSharp.IceServers)
extern void RTCConfiguration_set_servers_mFED9C6CE275C5DC4A94628A48D9B0CB45DF729CA (void);
// 0x00000197 WebRtcCSharp.IceServers WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_servers()
extern void RTCConfiguration_get_servers_mF9AFF8C786FB68FFD89061850074C409EE011D6F (void);
// 0x00000198 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_type(WebRtcCSharp.PeerConnectionInterface/IceTransportsType)
extern void RTCConfiguration_set_type_mEBB1F5182D751568667B252D6282CA9750F9D688 (void);
// 0x00000199 WebRtcCSharp.PeerConnectionInterface/IceTransportsType WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_type()
extern void RTCConfiguration_get_type_m06BB09DC44CAD7E3AD8F146C2D8519B8B9EC92CF (void);
// 0x0000019A System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_bundle_policy(WebRtcCSharp.PeerConnectionInterface/BundlePolicy)
extern void RTCConfiguration_set_bundle_policy_m4136D582FB52192495A66D7FC63FFABCC459FF36 (void);
// 0x0000019B WebRtcCSharp.PeerConnectionInterface/BundlePolicy WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_bundle_policy()
extern void RTCConfiguration_get_bundle_policy_m2A6CF293F9DB8D683108574D9F674F9A82B7168A (void);
// 0x0000019C System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_rtcp_mux_policy(WebRtcCSharp.PeerConnectionInterface/RtcpMuxPolicy)
extern void RTCConfiguration_set_rtcp_mux_policy_mF92FDED977E892307F8F0EF52B480A8DE269995A (void);
// 0x0000019D WebRtcCSharp.PeerConnectionInterface/RtcpMuxPolicy WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_rtcp_mux_policy()
extern void RTCConfiguration_get_rtcp_mux_policy_m9B6591915C9B2823177B4AD9F21043EAAA01AEB2 (void);
// 0x0000019E System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_certificates(WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t)
extern void RTCConfiguration_set_certificates_m9D7137C8B07F4EB4E590F6D25085AECE140D2E07 (void);
// 0x0000019F WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_certificates()
extern void RTCConfiguration_get_certificates_m00576B89F21BC2F8C7277BD1F63826188D921EA4 (void);
// 0x000001A0 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_ice_candidate_pool_size(System.Int32)
extern void RTCConfiguration_set_ice_candidate_pool_size_mC3109B8259D66ED9B82E376D4705775D4081F865 (void);
// 0x000001A1 System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_ice_candidate_pool_size()
extern void RTCConfiguration_get_ice_candidate_pool_size_m245232AFC0AED8934AE3CA5C4574197712606E98 (void);
// 0x000001A2 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_disable_ipv6(System.Boolean)
extern void RTCConfiguration_set_disable_ipv6_m7D88485C6E4E5DE8CC2508D997520479AD184634 (void);
// 0x000001A3 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_disable_ipv6()
extern void RTCConfiguration_get_disable_ipv6_m5B3EC12D91F555E3BB12AC9DD4A3008B6FBDE076 (void);
// 0x000001A4 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_disable_ipv6_on_wifi(System.Boolean)
extern void RTCConfiguration_set_disable_ipv6_on_wifi_mBAB53EF4CF698E5A3618D0C94A6372F29108CDB7 (void);
// 0x000001A5 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_disable_ipv6_on_wifi()
extern void RTCConfiguration_get_disable_ipv6_on_wifi_m35F6A7209E625E57DE29B07BB200FE3ABA818466 (void);
// 0x000001A6 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_max_ipv6_networks(System.Int32)
extern void RTCConfiguration_set_max_ipv6_networks_mA60DE4013A3844D72B99CEB7CCAF8D48F076DFBB (void);
// 0x000001A7 System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_max_ipv6_networks()
extern void RTCConfiguration_get_max_ipv6_networks_mA9F838C7EF2C3232EC1DAB445C640CDFE7EB7BA9 (void);
// 0x000001A8 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_disable_link_local_networks(System.Boolean)
extern void RTCConfiguration_set_disable_link_local_networks_mF60F72FD3021C8784CE6431799B3CFC24089D32B (void);
// 0x000001A9 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_disable_link_local_networks()
extern void RTCConfiguration_get_disable_link_local_networks_m88A4A388064EF30D2CE13B00B76956CF6F821254 (void);
// 0x000001AA System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_enable_rtp_data_channel(System.Boolean)
extern void RTCConfiguration_set_enable_rtp_data_channel_m64B3828FE63D2400D7131F0179F4FE5FCCB43348 (void);
// 0x000001AB System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_enable_rtp_data_channel()
extern void RTCConfiguration_get_enable_rtp_data_channel_m0C25662A0A8E8F36857A36CC18DEE824A0451267 (void);
// 0x000001AC System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_screencast_min_bitrate(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void RTCConfiguration_set_screencast_min_bitrate_mA5841B7A040F6DB93320D9DFFAF499A78946035D (void);
// 0x000001AD WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_screencast_min_bitrate()
extern void RTCConfiguration_get_screencast_min_bitrate_m29E7325C8B7CA74CAB69B354A69F00F5B091B2B6 (void);
// 0x000001AE System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_combined_audio_video_bwe(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_bool_t)
extern void RTCConfiguration_set_combined_audio_video_bwe_mF17CD5181D399BC191CB046CA4E62E611C049BF1 (void);
// 0x000001AF WebRtcCSharp.SWIGTYPE_p_absl__optionalT_bool_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_combined_audio_video_bwe()
extern void RTCConfiguration_get_combined_audio_video_bwe_mB00CF245CC4C596E555150FD404F2F346DEC11C8 (void);
// 0x000001B0 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_enable_dtls_srtp(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_bool_t)
extern void RTCConfiguration_set_enable_dtls_srtp_mD64192A066DB9E44817B9102234C7E82AE70D573 (void);
// 0x000001B1 WebRtcCSharp.SWIGTYPE_p_absl__optionalT_bool_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_enable_dtls_srtp()
extern void RTCConfiguration_get_enable_dtls_srtp_mA343512EB8609493B5F04EDA6A5FED1A29416F4B (void);
// 0x000001B2 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_tcp_candidate_policy(WebRtcCSharp.PeerConnectionInterface/TcpCandidatePolicy)
extern void RTCConfiguration_set_tcp_candidate_policy_mDE95DF3BC29DE486674F4B15359541EBA43057DC (void);
// 0x000001B3 WebRtcCSharp.PeerConnectionInterface/TcpCandidatePolicy WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_tcp_candidate_policy()
extern void RTCConfiguration_get_tcp_candidate_policy_mC79F2FC0B3A6C7D62D6ED2336B518D46D49CB494 (void);
// 0x000001B4 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_candidate_network_policy(WebRtcCSharp.PeerConnectionInterface/CandidateNetworkPolicy)
extern void RTCConfiguration_set_candidate_network_policy_m083B5D153A43C889B0A60E019E7E2E63395FCE76 (void);
// 0x000001B5 WebRtcCSharp.PeerConnectionInterface/CandidateNetworkPolicy WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_candidate_network_policy()
extern void RTCConfiguration_get_candidate_network_policy_m589BDD3F70A514992A8C51AD3C98B84E564A4FBA (void);
// 0x000001B6 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_audio_jitter_buffer_max_packets(System.Int32)
extern void RTCConfiguration_set_audio_jitter_buffer_max_packets_m7FAA7A18F2C0E78D3FF5ED2A478D25F590C6BD01 (void);
// 0x000001B7 System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_audio_jitter_buffer_max_packets()
extern void RTCConfiguration_get_audio_jitter_buffer_max_packets_m1EDE918F6C89710764294028178B9296083976F3 (void);
// 0x000001B8 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_audio_jitter_buffer_fast_accelerate(System.Boolean)
extern void RTCConfiguration_set_audio_jitter_buffer_fast_accelerate_m3AC2E2CC869E9F5D7FEE353A1A885B9DFA26FA39 (void);
// 0x000001B9 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_audio_jitter_buffer_fast_accelerate()
extern void RTCConfiguration_get_audio_jitter_buffer_fast_accelerate_mF03E1D1F48ABA0F50B3FD92E7DE9AFA0BF81EA54 (void);
// 0x000001BA System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_audio_jitter_buffer_min_delay_ms(System.Int32)
extern void RTCConfiguration_set_audio_jitter_buffer_min_delay_ms_m7B201D55856245513B26455FA3FF18E7A5DF0164 (void);
// 0x000001BB System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_audio_jitter_buffer_min_delay_ms()
extern void RTCConfiguration_get_audio_jitter_buffer_min_delay_ms_m16125C0D394439E8C008FE242BC4ED124D3D7075 (void);
// 0x000001BC System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_ice_connection_receiving_timeout(System.Int32)
extern void RTCConfiguration_set_ice_connection_receiving_timeout_m486D66A285B4C2A5FD4F8240D658D507496C4CFC (void);
// 0x000001BD System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_ice_connection_receiving_timeout()
extern void RTCConfiguration_get_ice_connection_receiving_timeout_m32B98711C320CABD1A23F6DF29227EB60231A77D (void);
// 0x000001BE System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_ice_backup_candidate_pair_ping_interval(System.Int32)
extern void RTCConfiguration_set_ice_backup_candidate_pair_ping_interval_m881884455C19B24CDD309C299093902379C4707B (void);
// 0x000001BF System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_ice_backup_candidate_pair_ping_interval()
extern void RTCConfiguration_get_ice_backup_candidate_pair_ping_interval_mB765114BEFA6FD97E7CC414968534EDBC02ED589 (void);
// 0x000001C0 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_continual_gathering_policy(WebRtcCSharp.PeerConnectionInterface/ContinualGatheringPolicy)
extern void RTCConfiguration_set_continual_gathering_policy_m2BAC226D94230FB1CB53666DE2134ED9798FB64E (void);
// 0x000001C1 WebRtcCSharp.PeerConnectionInterface/ContinualGatheringPolicy WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_continual_gathering_policy()
extern void RTCConfiguration_get_continual_gathering_policy_m0229EC1A18D5EB7BF2A892C880AC01BF6F7B0B27 (void);
// 0x000001C2 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_prioritize_most_likely_ice_candidate_pairs(System.Boolean)
extern void RTCConfiguration_set_prioritize_most_likely_ice_candidate_pairs_m7FD71F468397DE73509C45B07285F0232315025B (void);
// 0x000001C3 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_prioritize_most_likely_ice_candidate_pairs()
extern void RTCConfiguration_get_prioritize_most_likely_ice_candidate_pairs_m6E02448936415E7E69703CE278D74CE885CCB857 (void);
// 0x000001C4 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_media_config(WebRtcCSharp.SWIGTYPE_p_cricket__MediaConfig)
extern void RTCConfiguration_set_media_config_m37DF461993253C095EE8B944E6B278C096DC2A13 (void);
// 0x000001C5 WebRtcCSharp.SWIGTYPE_p_cricket__MediaConfig WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_media_config()
extern void RTCConfiguration_get_media_config_m03AF6B8B19A0753F86183DFB7562308881D6F4FC (void);
// 0x000001C6 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_prune_turn_ports(System.Boolean)
extern void RTCConfiguration_set_prune_turn_ports_m000B9ABF8A62AF41C77105D4268CD9CE395A3EB2 (void);
// 0x000001C7 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_prune_turn_ports()
extern void RTCConfiguration_get_prune_turn_ports_m95F6F95A9D6997E4EB55057D47B980A80D3D0FF0 (void);
// 0x000001C8 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_presume_writable_when_fully_relayed(System.Boolean)
extern void RTCConfiguration_set_presume_writable_when_fully_relayed_m165CB3439F2ADC9F8846D3240AFF868577A827E5 (void);
// 0x000001C9 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_presume_writable_when_fully_relayed()
extern void RTCConfiguration_get_presume_writable_when_fully_relayed_m39FB629C7C63656E9650CA1DF61DFD2B8DA84E52 (void);
// 0x000001CA System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_enable_ice_renomination(System.Boolean)
extern void RTCConfiguration_set_enable_ice_renomination_m32CC4634A13D00E975EF7D444AE78FC1F84F0C5A (void);
// 0x000001CB System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_enable_ice_renomination()
extern void RTCConfiguration_get_enable_ice_renomination_mD20FC716DB659683481EE0EA669B8EBA13FF1839 (void);
// 0x000001CC System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_redetermine_role_on_ice_restart(System.Boolean)
extern void RTCConfiguration_set_redetermine_role_on_ice_restart_m0A6CEC0C3492979ED5F7B5A2C3272357D38801C1 (void);
// 0x000001CD System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_redetermine_role_on_ice_restart()
extern void RTCConfiguration_get_redetermine_role_on_ice_restart_m7155A21F3F759EFBE43331C8E6720DA8487433B0 (void);
// 0x000001CE System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_ice_check_interval_strong_connectivity(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void RTCConfiguration_set_ice_check_interval_strong_connectivity_m91C4398E4D6FFEDE7B85963A02447F8A697B504E (void);
// 0x000001CF WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_ice_check_interval_strong_connectivity()
extern void RTCConfiguration_get_ice_check_interval_strong_connectivity_m7100806652E7B1928D20D8EE382F8BA6A797B027 (void);
// 0x000001D0 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_ice_check_interval_weak_connectivity(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void RTCConfiguration_set_ice_check_interval_weak_connectivity_m5A394A75EEC3659CA452C0E2F2685ADA5B7ED320 (void);
// 0x000001D1 WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_ice_check_interval_weak_connectivity()
extern void RTCConfiguration_get_ice_check_interval_weak_connectivity_m6780B808B00C220488809DF6A8B3070A67AB93EC (void);
// 0x000001D2 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_ice_check_min_interval(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void RTCConfiguration_set_ice_check_min_interval_mCC26D3EE397DA09BBC15731F6DF24222A699C062 (void);
// 0x000001D3 WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_ice_check_min_interval()
extern void RTCConfiguration_get_ice_check_min_interval_mC258CBA793EEDB91D9CB897EDDF714278F4F0801 (void);
// 0x000001D4 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_ice_unwritable_timeout(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void RTCConfiguration_set_ice_unwritable_timeout_m1A7D51D0D5BF111B7BBA242EECD2BB70F6A3332F (void);
// 0x000001D5 WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_ice_unwritable_timeout()
extern void RTCConfiguration_get_ice_unwritable_timeout_mFE1F5508FFCDA7EBF0BF36DECC09DE5AA0E331CC (void);
// 0x000001D6 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_ice_unwritable_min_checks(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void RTCConfiguration_set_ice_unwritable_min_checks_mD5138DA92F59A56882CB1C577FB286507EBF676B (void);
// 0x000001D7 WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_ice_unwritable_min_checks()
extern void RTCConfiguration_get_ice_unwritable_min_checks_mC9FECB2E4DD7E75DB94405FA0773D3736F521417 (void);
// 0x000001D8 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_stun_candidate_keepalive_interval(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void RTCConfiguration_set_stun_candidate_keepalive_interval_m26C8448071C161B96A59B6427C47021E896119CE (void);
// 0x000001D9 WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_stun_candidate_keepalive_interval()
extern void RTCConfiguration_get_stun_candidate_keepalive_interval_m306F2EF50FBB966DE68EA7A5F2C5232BF594972E (void);
// 0x000001DA System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_ice_regather_interval_range(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t)
extern void RTCConfiguration_set_ice_regather_interval_range_mAE53A0150B9EC07A318E02EDE6049FB68A91890F (void);
// 0x000001DB WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_ice_regather_interval_range()
extern void RTCConfiguration_get_ice_regather_interval_range_m63FD1298695364E43A11D899554939BDFACC235B (void);
// 0x000001DC System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_turn_customizer(WebRtcCSharp.SWIGTYPE_p_webrtc__TurnCustomizer)
extern void RTCConfiguration_set_turn_customizer_m1EF815D1495B045B6F7AB0BE7087F79C9291BA9E (void);
// 0x000001DD WebRtcCSharp.SWIGTYPE_p_webrtc__TurnCustomizer WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_turn_customizer()
extern void RTCConfiguration_get_turn_customizer_m1D9136850B323FB81D08BFC6F0BD085CE6EB7050 (void);
// 0x000001DE System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_network_preference(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t)
extern void RTCConfiguration_set_network_preference_mDB5205DEF9B68A3E8D53431818688B28314AA8A5 (void);
// 0x000001DF WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_network_preference()
extern void RTCConfiguration_get_network_preference_mB46C2674B0247D27F82FBBFAB96E22B16B0391EA (void);
// 0x000001E0 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_sdp_semantics(WebRtcCSharp.SdpSemantics)
extern void RTCConfiguration_set_sdp_semantics_m10A680F052D0F73ECF887DC96BE7A027B67291FD (void);
// 0x000001E1 WebRtcCSharp.SdpSemantics WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_sdp_semantics()
extern void RTCConfiguration_get_sdp_semantics_m811413A9D65CBD80BC1F55C3D11A65C4A18E8F01 (void);
// 0x000001E2 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_active_reset_srtp_params(System.Boolean)
extern void RTCConfiguration_set_active_reset_srtp_params_m42AAD972B9260B05C77676BFD990A41514286B04 (void);
// 0x000001E3 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_active_reset_srtp_params()
extern void RTCConfiguration_get_active_reset_srtp_params_m094096C57B4D22418CA2DF994E7647E023E9A66A (void);
// 0x000001E4 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_use_media_transport(System.Boolean)
extern void RTCConfiguration_set_use_media_transport_m49D457F5BD2807E1692037591226354A119AB067 (void);
// 0x000001E5 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_use_media_transport()
extern void RTCConfiguration_get_use_media_transport_mA1279AAB8F9CE6AC10FA04FC991E54DD6523A6EC (void);
// 0x000001E6 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_use_media_transport_for_data_channels(System.Boolean)
extern void RTCConfiguration_set_use_media_transport_for_data_channels_mF10275EA17CE1A692026F911A3B1F31D84A5C2D2 (void);
// 0x000001E7 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_use_media_transport_for_data_channels()
extern void RTCConfiguration_get_use_media_transport_for_data_channels_m1D9E24B7D9B4542E6233E56F3C749BB4088C2EE4 (void);
// 0x000001E8 System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_crypto_options(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_CryptoOptions_t)
extern void RTCConfiguration_set_crypto_options_m4798532B1C2DCB2F75D02E68655FFE639F8D54A5 (void);
// 0x000001E9 WebRtcCSharp.SWIGTYPE_p_absl__optionalT_CryptoOptions_t WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_crypto_options()
extern void RTCConfiguration_get_crypto_options_m1C0E7BE1EBF30B0991540FEDCA967A4DFD5D9EA9 (void);
// 0x000001EA System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::set_offer_extmap_allow_mixed(System.Boolean)
extern void RTCConfiguration_set_offer_extmap_allow_mixed_m81E792CC615742CB43BF469AFC43F7EE02E34070 (void);
// 0x000001EB System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::get_offer_extmap_allow_mixed()
extern void RTCConfiguration_get_offer_extmap_allow_mixed_m151171B14731DCD28FEC08824E08299A9C23A7C7 (void);
// 0x000001EC System.Void WebRtcCSharp.PeerConnectionInterface/RTCConfiguration::.cctor()
extern void RTCConfiguration__cctor_m0A0BB8022E2354D2813FC69F3BC8DAC97BD61C2D (void);
// 0x000001ED System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::.ctor(System.IntPtr,System.Boolean)
extern void RTCOfferAnswerOptions__ctor_mCC1F36D07B6C958710164FFC714CA2B1913642EE (void);
// 0x000001EE System.Runtime.InteropServices.HandleRef WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::getCPtr(WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions)
extern void RTCOfferAnswerOptions_getCPtr_mB27FC47F29D3C514E7F232EC63C894488FD4D0B4 (void);
// 0x000001EF System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::Finalize()
extern void RTCOfferAnswerOptions_Finalize_m7DBD44585FA35009A411830E52BD78297A0F3F3B (void);
// 0x000001F0 System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::Dispose()
extern void RTCOfferAnswerOptions_Dispose_mBEBBCF7639DC8A7ABFFD48B4E9EE67BCA0B0C9BF (void);
// 0x000001F1 System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::set_offer_to_receive_video(System.Int32)
extern void RTCOfferAnswerOptions_set_offer_to_receive_video_m7DA58713F7C68472D56CA4108AE120C9F115D7C4 (void);
// 0x000001F2 System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::get_offer_to_receive_video()
extern void RTCOfferAnswerOptions_get_offer_to_receive_video_m869EE75E6D89DC4A66748EF9881670694E90EBE9 (void);
// 0x000001F3 System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::set_offer_to_receive_audio(System.Int32)
extern void RTCOfferAnswerOptions_set_offer_to_receive_audio_mC703FB2C77DFEB54AE6BB0EAAC059977847FCE59 (void);
// 0x000001F4 System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::get_offer_to_receive_audio()
extern void RTCOfferAnswerOptions_get_offer_to_receive_audio_m5E459D77F18A2B7B144CBDEF24B33C0555994519 (void);
// 0x000001F5 System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::set_voice_activity_detection(System.Boolean)
extern void RTCOfferAnswerOptions_set_voice_activity_detection_m5CDF4D402EB6EBC83F7956FB03CA1446E95C6003 (void);
// 0x000001F6 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::get_voice_activity_detection()
extern void RTCOfferAnswerOptions_get_voice_activity_detection_m3E7938E1D111D62EAAA624A1E5BEC83D6F6880A5 (void);
// 0x000001F7 System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::set_ice_restart(System.Boolean)
extern void RTCOfferAnswerOptions_set_ice_restart_m5535AB0B652EA6ABCD3F9D57078067A6945D72E9 (void);
// 0x000001F8 System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::get_ice_restart()
extern void RTCOfferAnswerOptions_get_ice_restart_mE628D0FFB6722E558038EDF3ABE2DAE51E50A61B (void);
// 0x000001F9 System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::set_use_rtp_mux(System.Boolean)
extern void RTCOfferAnswerOptions_set_use_rtp_mux_m1F23B9494CC5749E714ECC0768095FA078BA852A (void);
// 0x000001FA System.Boolean WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::get_use_rtp_mux()
extern void RTCOfferAnswerOptions_get_use_rtp_mux_m2940C16C1F76D620DA9718A713BC3F0C6A7831BC (void);
// 0x000001FB System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::set_num_simulcast_layers(System.Int32)
extern void RTCOfferAnswerOptions_set_num_simulcast_layers_m7BF484D59BC743E349DD42F87C5DB023208EAB80 (void);
// 0x000001FC System.Int32 WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::get_num_simulcast_layers()
extern void RTCOfferAnswerOptions_get_num_simulcast_layers_mC6109659B5ED2443DB27BC94CCA11B46417439A8 (void);
// 0x000001FD System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::.ctor()
extern void RTCOfferAnswerOptions__ctor_mA69EC84E5090D0B98E2F0DFB54E9E6769952C673 (void);
// 0x000001FE System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::.ctor(System.Int32,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void RTCOfferAnswerOptions__ctor_m2D27D37C4C984C59EA76DD06BECFF3A5481483E7 (void);
// 0x000001FF System.Void WebRtcCSharp.PeerConnectionInterface/RTCOfferAnswerOptions::.cctor()
extern void RTCOfferAnswerOptions__cctor_m539BACA60577191998ADCFF98A76760F43420B05 (void);
// 0x00000200 System.Void WebRtcCSharp.PeerConnectionInterface/BitrateParameters::.ctor(System.IntPtr,System.Boolean)
extern void BitrateParameters__ctor_m46A2CADAE9487F4B06A28CA42A6348251FEB51FD (void);
// 0x00000201 System.Runtime.InteropServices.HandleRef WebRtcCSharp.PeerConnectionInterface/BitrateParameters::getCPtr(WebRtcCSharp.PeerConnectionInterface/BitrateParameters)
extern void BitrateParameters_getCPtr_m99B24536F99DEB11D424C1E3807F2BC8B49A7CC5 (void);
// 0x00000202 System.Void WebRtcCSharp.PeerConnectionInterface/BitrateParameters::Finalize()
extern void BitrateParameters_Finalize_m212F63650DB8F01B626A7109D6688F29314BD874 (void);
// 0x00000203 System.Void WebRtcCSharp.PeerConnectionInterface/BitrateParameters::Dispose()
extern void BitrateParameters_Dispose_m0079B66ED17E7208B2362C3BA39BAEC2BE2AC102 (void);
// 0x00000204 System.Void WebRtcCSharp.PeerConnectionInterface/BitrateParameters::.ctor()
extern void BitrateParameters__ctor_m96ECE693260B9D8DDFC9DA5D814DCFE35D1D39E4 (void);
// 0x00000205 System.Void WebRtcCSharp.PeerConnectionInterface/BitrateParameters::set_min_bitrate_bps(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void BitrateParameters_set_min_bitrate_bps_m71331813BC43E5B8E085E94EAE5C6E34C0C016CC (void);
// 0x00000206 WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/BitrateParameters::get_min_bitrate_bps()
extern void BitrateParameters_get_min_bitrate_bps_m26A8FAF2416126A24E707BD21263B8F8B0C54AA0 (void);
// 0x00000207 System.Void WebRtcCSharp.PeerConnectionInterface/BitrateParameters::set_current_bitrate_bps(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void BitrateParameters_set_current_bitrate_bps_m2D39BDCD6DA4CA939C416EAACBDB7FE629FE1B28 (void);
// 0x00000208 WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/BitrateParameters::get_current_bitrate_bps()
extern void BitrateParameters_get_current_bitrate_bps_m147B818F3BE557C70FD84B883D28ACA07429D7A2 (void);
// 0x00000209 System.Void WebRtcCSharp.PeerConnectionInterface/BitrateParameters::set_max_bitrate_bps(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void BitrateParameters_set_max_bitrate_bps_m0C353D35825B152843EC20D7ED14C19EBB885907 (void);
// 0x0000020A WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t WebRtcCSharp.PeerConnectionInterface/BitrateParameters::get_max_bitrate_bps()
extern void BitrateParameters_get_max_bitrate_bps_m1E6B24D69F31EE1587E63FF3E640EC7B883E396F (void);
// 0x0000020B System.Void WebRtcCSharp.PollingMediaStream::.ctor(System.IntPtr,System.Boolean)
extern void PollingMediaStream__ctor_mBDD37D99CBEB6CC88C65AF6D2392595A03D92F30 (void);
// 0x0000020C System.Runtime.InteropServices.HandleRef WebRtcCSharp.PollingMediaStream::getCPtr(WebRtcCSharp.PollingMediaStream)
extern void PollingMediaStream_getCPtr_m61D21899EFD8E259A952476B246A2F65B000C7C4 (void);
// 0x0000020D System.Void WebRtcCSharp.PollingMediaStream::Finalize()
extern void PollingMediaStream_Finalize_mC3473521EAE13AFBD135207AB716F14C04D457CD (void);
// 0x0000020E System.Void WebRtcCSharp.PollingMediaStream::Dispose()
extern void PollingMediaStream_Dispose_mC2E736C60455AF90AB85DDB895328D2C25359D6E (void);
// 0x0000020F WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t WebRtcCSharp.PollingMediaStream::GetInternal()
extern void PollingMediaStream_GetInternal_m5935D28D2E651A8DD3ABEA1DA0F1DD97BBDF10A7 (void);
// 0x00000210 System.Boolean WebRtcCSharp.PollingMediaStream::HasVideoTrack()
extern void PollingMediaStream_HasVideoTrack_mF6D00EA2A963F8D4CB93EFF412FF0FCCF1A64AE0 (void);
// 0x00000211 System.Boolean WebRtcCSharp.PollingMediaStream::HasAudioTrack()
extern void PollingMediaStream_HasAudioTrack_mA7B028C5924B39FA1173492260FBDC9A11D74119 (void);
// 0x00000212 System.Boolean WebRtcCSharp.PollingMediaStream::IsMute()
extern void PollingMediaStream_IsMute_mC2EFC8C85B10B8EEB6917657108BE49A03D5AF93 (void);
// 0x00000213 System.Void WebRtcCSharp.PollingMediaStream::SetMute(System.Boolean)
extern void PollingMediaStream_SetMute_m22B38623030518B7A55B65D95A7E77AD365ABD9C (void);
// 0x00000214 System.UInt32 WebRtcCSharp.PollingMediaStream::CalculateByteSize(WebRtcCSharp.VideoType)
extern void PollingMediaStream_CalculateByteSize_m2B0FC5BD138E0386EEE3FF9FF7717DB492C086A2 (void);
// 0x00000215 System.UInt32 WebRtcCSharp.PollingMediaStream::GetImageData(WebRtcCSharp.VideoType,System.Byte[],System.UInt32)
extern void PollingMediaStream_GetImageData_m00DD35B4B24CEB235AD43D37FFFD74B9FF940D9C (void);
// 0x00000216 System.Boolean WebRtcCSharp.PollingMediaStream::SupportsI420Access()
extern void PollingMediaStream_SupportsI420Access_mB3F4B449415FA34C58F34D44F0CE6B6D8046639F (void);
// 0x00000217 System.UInt32 WebRtcCSharp.PollingMediaStream::GetI420Size()
extern void PollingMediaStream_GetI420Size_mAEDB3C5435488219A0BE1FD21812328262FFE22C (void);
// 0x00000218 System.IntPtr WebRtcCSharp.PollingMediaStream::GetI420Ptr()
extern void PollingMediaStream_GetI420Ptr_m0D34500CDD475CEC28B0F5D534C67D78ABF6B225 (void);
// 0x00000219 System.Boolean WebRtcCSharp.PollingMediaStream::HasFrame()
extern void PollingMediaStream_HasFrame_m158432FD42564D244422CFDCAFA52EA57ADA7589 (void);
// 0x0000021A System.Void WebRtcCSharp.PollingMediaStream::FreeCurrentImage()
extern void PollingMediaStream_FreeCurrentImage_mE55E22E3084453909DEF4F3366AA8BD69B604D51 (void);
// 0x0000021B System.Int32 WebRtcCSharp.PollingMediaStream::GetWidth()
extern void PollingMediaStream_GetWidth_m25054F5F2FDA6F59C68454E8C0ECA1E1CF3860FB (void);
// 0x0000021C System.Int32 WebRtcCSharp.PollingMediaStream::GetHeight()
extern void PollingMediaStream_GetHeight_mD470C2017797463894870C3F8CF566238A7D9248 (void);
// 0x0000021D System.Int32 WebRtcCSharp.PollingMediaStream::GetRotation()
extern void PollingMediaStream_GetRotation_m53AC1D1F4745B37F1176D09CEC0E06B30EFF1780 (void);
// 0x0000021E System.Void WebRtcCSharp.PollingMediaStream::SetVolume(System.Double)
extern void PollingMediaStream_SetVolume_m1D9F82341FBFB19F8C510C60697429AB100064C6 (void);
// 0x0000021F System.Void WebRtcCSharp.PollingMediaStreamRef::.ctor(System.IntPtr,System.Boolean)
extern void PollingMediaStreamRef__ctor_m3B1F2F5B56A549507C4E86EF8400F6AD105CC4F9 (void);
// 0x00000220 System.Runtime.InteropServices.HandleRef WebRtcCSharp.PollingMediaStreamRef::getCPtr(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingMediaStreamRef_getCPtr_mCD05CA842410D480194987AC030E400A3D0B03CB (void);
// 0x00000221 System.Void WebRtcCSharp.PollingMediaStreamRef::Finalize()
extern void PollingMediaStreamRef_Finalize_mB8DF34EEE329DD8BBD7CE751137AD6DAB0545033 (void);
// 0x00000222 System.Void WebRtcCSharp.PollingMediaStreamRef::Dispose()
extern void PollingMediaStreamRef_Dispose_mE003F6182E21195A2FC17D606E2C9174C088F87B (void);
// 0x00000223 System.Void WebRtcCSharp.PollingMediaStreamRef::.ctor()
extern void PollingMediaStreamRef__ctor_m338B43EF03DA71E5397BA20C597CC39EC657E0A8 (void);
// 0x00000224 System.Void WebRtcCSharp.PollingMediaStreamRef::.ctor(WebRtcCSharp.PollingMediaStream)
extern void PollingMediaStreamRef__ctor_mA536FF48AA1C939166877891CCDF284C0888892B (void);
// 0x00000225 System.Void WebRtcCSharp.PollingMediaStreamRef::.ctor(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingMediaStreamRef__ctor_m2F81C8940A1B4F6CF40F37B234EDA64003F2BE0E (void);
// 0x00000226 WebRtcCSharp.PollingMediaStream WebRtcCSharp.PollingMediaStreamRef::get()
extern void PollingMediaStreamRef_get_mEE1E98B7FB71D40B51D50AEE94BAFBD9D1DC354C (void);
// 0x00000227 WebRtcCSharp.PollingMediaStream WebRtcCSharp.PollingMediaStreamRef::__deref__()
extern void PollingMediaStreamRef___deref___mA9919A1F2269113B175F53EE46A4BF206FCD0466 (void);
// 0x00000228 WebRtcCSharp.PollingMediaStream WebRtcCSharp.PollingMediaStreamRef::release()
extern void PollingMediaStreamRef_release_m8CF91EC23F6301BE79133CFFC9E0950AF58D2470 (void);
// 0x00000229 System.Void WebRtcCSharp.PollingMediaStreamRef::swap(WebRtcCSharp.SWIGTYPE_p_p_PollingMediaStream)
extern void PollingMediaStreamRef_swap_mDCBBAD8AE4C1D65968DF9DF367A14FCD5C77BFA5 (void);
// 0x0000022A System.Void WebRtcCSharp.PollingMediaStreamRef::swap(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingMediaStreamRef_swap_m56905A86C97AD86273A6F03178655A13EE1FB630 (void);
// 0x0000022B WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t WebRtcCSharp.PollingMediaStreamRef::GetInternal()
extern void PollingMediaStreamRef_GetInternal_mBB4632D167BC4C2FA78E2F0F249EEF25C6007BCF (void);
// 0x0000022C System.Boolean WebRtcCSharp.PollingMediaStreamRef::HasVideoTrack()
extern void PollingMediaStreamRef_HasVideoTrack_m61EB91E6BB6CE3E9AEB3F111C1A476E9EEF67E57 (void);
// 0x0000022D System.Boolean WebRtcCSharp.PollingMediaStreamRef::HasAudioTrack()
extern void PollingMediaStreamRef_HasAudioTrack_mF3F8E476D9046FEEC7F1AC8FDF10F2ADF91CD903 (void);
// 0x0000022E System.Boolean WebRtcCSharp.PollingMediaStreamRef::IsMute()
extern void PollingMediaStreamRef_IsMute_m03A27ECA1AAFF8F6C1D55983C04934198B98B22C (void);
// 0x0000022F System.Void WebRtcCSharp.PollingMediaStreamRef::SetMute(System.Boolean)
extern void PollingMediaStreamRef_SetMute_m0CA30C49BC3D7436D40A7C7FC6BFA459FCB1EB41 (void);
// 0x00000230 System.UInt32 WebRtcCSharp.PollingMediaStreamRef::CalculateByteSize(WebRtcCSharp.VideoType)
extern void PollingMediaStreamRef_CalculateByteSize_m8676F764D433F8865127362916A91917040C6F05 (void);
// 0x00000231 System.UInt32 WebRtcCSharp.PollingMediaStreamRef::GetImageData(WebRtcCSharp.VideoType,System.Byte[],System.UInt32)
extern void PollingMediaStreamRef_GetImageData_m2FBE98FADAE67379C0B47823BCFE8B1A8DE5B176 (void);
// 0x00000232 System.Boolean WebRtcCSharp.PollingMediaStreamRef::SupportsI420Access()
extern void PollingMediaStreamRef_SupportsI420Access_m61A8B29FE9E01D2D6F6548D56556D8B2B597B148 (void);
// 0x00000233 System.UInt32 WebRtcCSharp.PollingMediaStreamRef::GetI420Size()
extern void PollingMediaStreamRef_GetI420Size_m708FE6180103E39037DC0211DBD82E86186A5EEA (void);
// 0x00000234 System.IntPtr WebRtcCSharp.PollingMediaStreamRef::GetI420Ptr()
extern void PollingMediaStreamRef_GetI420Ptr_m1F7EA1C8BFC4C32A6E375CA9914CA0AA0CC20F70 (void);
// 0x00000235 System.Boolean WebRtcCSharp.PollingMediaStreamRef::HasFrame()
extern void PollingMediaStreamRef_HasFrame_mCD40F63DC56DF8D3492601BCD11D040771489BE4 (void);
// 0x00000236 System.Void WebRtcCSharp.PollingMediaStreamRef::FreeCurrentImage()
extern void PollingMediaStreamRef_FreeCurrentImage_m59AF70EA27BA13C252B37ABE3A24091BE72F35A5 (void);
// 0x00000237 System.Int32 WebRtcCSharp.PollingMediaStreamRef::GetWidth()
extern void PollingMediaStreamRef_GetWidth_m589380472D357FE8A6D712A1F2CE498A6ED6DDFA (void);
// 0x00000238 System.Int32 WebRtcCSharp.PollingMediaStreamRef::GetHeight()
extern void PollingMediaStreamRef_GetHeight_mCF06564C451E61001F4A1151BCD2859EE7C8ED3B (void);
// 0x00000239 System.Int32 WebRtcCSharp.PollingMediaStreamRef::GetRotation()
extern void PollingMediaStreamRef_GetRotation_m0FA796DE76B3186BD4D497221EDD02543F4A78C5 (void);
// 0x0000023A System.Void WebRtcCSharp.PollingMediaStreamRef::SetVolume(System.Double)
extern void PollingMediaStreamRef_SetVolume_m9EE8681F394A0AAFF8CC12B0948D6974E59EBC4E (void);
// 0x0000023B System.Void WebRtcCSharp.PollingMediaStreamRef::AddRef()
extern void PollingMediaStreamRef_AddRef_m79E6ADF4C46FA8C0DCD97EC2B9656A4C733E01B8 (void);
// 0x0000023C WebRtcCSharp.RefCountReleaseStatus WebRtcCSharp.PollingMediaStreamRef::Release()
extern void PollingMediaStreamRef_Release_m2C1D9C83714170FC3032584DF2E456130BED65A8 (void);
// 0x0000023D System.Void WebRtcCSharp.PollingPeer::.ctor(System.IntPtr,System.Boolean)
extern void PollingPeer__ctor_mA68EB2346F759CBED5C8187E74576D5EE4AE37D3 (void);
// 0x0000023E System.Runtime.InteropServices.HandleRef WebRtcCSharp.PollingPeer::getCPtr(WebRtcCSharp.PollingPeer)
extern void PollingPeer_getCPtr_mA9C44DF501C3081F84354A15FA241C025AD33A6E (void);
// 0x0000023F System.Void WebRtcCSharp.PollingPeer::Finalize()
extern void PollingPeer_Finalize_m78405492884FB2DE01B440028E8AD42434805543 (void);
// 0x00000240 System.Void WebRtcCSharp.PollingPeer::Dispose()
extern void PollingPeer_Dispose_mB599BB360A4460FA8331E4F2F664F27CC6C35A43 (void);
// 0x00000241 System.Void WebRtcCSharp.PollingPeer::Update()
extern void PollingPeer_Update_m06827D9C482F0CF08BE47CA7F1C35633C32F10D7 (void);
// 0x00000242 System.Void WebRtcCSharp.PollingPeer::Flush()
extern void PollingPeer_Flush_mF78D24FB60ED8C055AF09BDD22434030E01E0EF4 (void);
// 0x00000243 WebRtcCSharp.PollingPeer/ConnectionState WebRtcCSharp.PollingPeer::GetConnectionState()
extern void PollingPeer_GetConnectionState_mA1EB541DF5FAFE8FCB80057FCFA17F79D719FEDC (void);
// 0x00000244 System.Boolean WebRtcCSharp.PollingPeer::HasSignalingMessage()
extern void PollingPeer_HasSignalingMessage_mDF6074B66DCA797609FDC7D800B1C5B5AEB6B9BE (void);
// 0x00000245 System.String WebRtcCSharp.PollingPeer::DequeueSignalingMessage()
extern void PollingPeer_DequeueSignalingMessage_m07AF670E3DF01A531EFF5F3D8FF940C4CD5396A8 (void);
// 0x00000246 System.Void WebRtcCSharp.PollingPeer::AddSignalingMessage(System.String)
extern void PollingPeer_AddSignalingMessage_mE95ED3BF90FC1B975FA457644FB9770002053B45 (void);
// 0x00000247 System.Boolean WebRtcCSharp.PollingPeer::HasSignalingError()
extern void PollingPeer_HasSignalingError_m6EEF11B3D92D440B8AAA71E289645C35F342E765 (void);
// 0x00000248 System.String WebRtcCSharp.PollingPeer::DequeueSignalingError()
extern void PollingPeer_DequeueSignalingError_m564F2E6E68A910C59722FE4F2C8CA31FD8BCEEAC (void);
// 0x00000249 System.Void WebRtcCSharp.PollingPeer::CreateOffer()
extern void PollingPeer_CreateOffer_m7DE07BA80B4C896DD81274ACDF1E489E363C0263 (void);
// 0x0000024A System.Void WebRtcCSharp.PollingPeer::CreateAnswer()
extern void PollingPeer_CreateAnswer_m6EB19E328E1FBA79E7B1D841253AB358E0E1ABF8 (void);
// 0x0000024B System.Void WebRtcCSharp.PollingPeer::Close()
extern void PollingPeer_Close_m8F425355BEFA0A4E642C712FCF33F2B4C27377A4 (void);
// 0x0000024C System.Void WebRtcCSharp.PollingPeer::CloseInternal()
extern void PollingPeer_CloseInternal_mC4E34411D11831F45959DBAB0217FA5B1698E623 (void);
// 0x0000024D System.Void WebRtcCSharp.PollingPeer::Cleanup()
extern void PollingPeer_Cleanup_m00E6DCCDCD558CFF24B4FA8C7FF5CCD4219BD88B (void);
// 0x0000024E System.Int32 WebRtcCSharp.PollingPeer::CountOpenedDataChannels()
extern void PollingPeer_CountOpenedDataChannels_mE41EEEDE357FFC3E9F8CE0254D6E9A5391FCDDCD (void);
// 0x0000024F System.Boolean WebRtcCSharp.PollingPeer::HasDataChannelMessage()
extern void PollingPeer_HasDataChannelMessage_mE04EC3BAB823138D280E34AA6D5601E589D5D5B6 (void);
// 0x00000250 System.Boolean WebRtcCSharp.PollingPeer::DequeueDataChannelMessage(System.Int32&,WebRtcCSharp.DataBuffer)
extern void PollingPeer_DequeueDataChannelMessage_mF8F21FA7B31448250B94085E42E415CF8CE7C9E9 (void);
// 0x00000251 System.Boolean WebRtcCSharp.PollingPeer::Send(System.Int32,System.Byte[],System.UInt32,System.UInt32)
extern void PollingPeer_Send_m98986054E14B888EDF6C8992590E9E5B2FB59151 (void);
// 0x00000252 System.Int32 WebRtcCSharp.PollingPeer::GetBufferedAmount(System.Int32)
extern void PollingPeer_GetBufferedAmount_m2D0902FB2A5734BB18946DC00D9E526AED30B40A (void);
// 0x00000253 System.Void WebRtcCSharp.PollingPeer::CreateDataChannel(System.String,WebRtcCSharp.DataChannelInit)
extern void PollingPeer_CreateDataChannel_m5977101B02E0C6008D1DE5EDBBA8714CF3415C79 (void);
// 0x00000254 System.String WebRtcCSharp.PollingPeer::GetDataChannelLabel(System.Int32)
extern void PollingPeer_GetDataChannelLabel_m97A1F2A3A74E09C93006D5A98E7DD15FC1B61D98 (void);
// 0x00000255 WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.PollingPeer::GetRemoteStream()
extern void PollingPeer_GetRemoteStream_m804B6BF012F3D66F2482FBF6A4F46FAB2D06C443 (void);
// 0x00000256 System.Void WebRtcCSharp.PollingPeer::AddLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingPeer_AddLocalStream_m4568C9DBDDDE900F2A41E8BE83642EB9276A0A8C (void);
// 0x00000257 System.Void WebRtcCSharp.PollingPeer::RemoveLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingPeer_RemoveLocalStream_m112A33BA861784959A0D2B145F1DD5A37D89C593 (void);
// 0x00000258 System.Void WebRtcCSharp.PollingPeer::RequestStats(System.Boolean,System.Boolean,System.Boolean)
extern void PollingPeer_RequestStats_m8D11E4AA65B20EAC031F68205D4D61E032C278AE (void);
// 0x00000259 System.Boolean WebRtcCSharp.PollingPeer::HasStats()
extern void PollingPeer_HasStats_m01FB20FDC47DD8F2AE8D36ADC697C46C86FFFE92 (void);
// 0x0000025A System.Boolean WebRtcCSharp.PollingPeer::DequeueStats(WebRtcCSharp.SWIGTYPE_p_std__string)
extern void PollingPeer_DequeueStats_mC74CAC6389C8FF66694990AD665BFB32411345C4 (void);
// 0x0000025B System.Void WebRtcCSharp.PollingPeerRef::.ctor(System.IntPtr,System.Boolean)
extern void PollingPeerRef__ctor_mD3C8C89EF9F37C3FF8B0DA6DA4B0B436C699BD6C (void);
// 0x0000025C System.Runtime.InteropServices.HandleRef WebRtcCSharp.PollingPeerRef::getCPtr(WebRtcCSharp.PollingPeerRef)
extern void PollingPeerRef_getCPtr_m94B625AD35EFA192CAF7A29EE655EF118427FEF9 (void);
// 0x0000025D System.Void WebRtcCSharp.PollingPeerRef::Finalize()
extern void PollingPeerRef_Finalize_mDB470DAD862A09E34F5AD185EC91D0625D9BBDD8 (void);
// 0x0000025E System.Void WebRtcCSharp.PollingPeerRef::Dispose()
extern void PollingPeerRef_Dispose_mC6E468328109EEED7876959CAD2FBBCDE6D50FF5 (void);
// 0x0000025F System.Void WebRtcCSharp.PollingPeerRef::.ctor()
extern void PollingPeerRef__ctor_mF19E7141B0C7FF99AEF170BC64C7DBC155BA6F7D (void);
// 0x00000260 System.Void WebRtcCSharp.PollingPeerRef::.ctor(WebRtcCSharp.PollingPeer)
extern void PollingPeerRef__ctor_mC91E9D7D5C078147D49DA017ADE7E946C39514D5 (void);
// 0x00000261 System.Void WebRtcCSharp.PollingPeerRef::.ctor(WebRtcCSharp.PollingPeerRef)
extern void PollingPeerRef__ctor_mBA43E26CDF74784E55A8D7EDE6206A50686E1F04 (void);
// 0x00000262 WebRtcCSharp.PollingPeer WebRtcCSharp.PollingPeerRef::get()
extern void PollingPeerRef_get_m4F9F4379BBE4E35FBF21E398812A9F55C189EAC5 (void);
// 0x00000263 WebRtcCSharp.PollingPeer WebRtcCSharp.PollingPeerRef::__deref__()
extern void PollingPeerRef___deref___m32ADD68ADACF009209D36B03F67455604954A3F9 (void);
// 0x00000264 WebRtcCSharp.PollingPeer WebRtcCSharp.PollingPeerRef::release()
extern void PollingPeerRef_release_mC2376E76019451E665D9D358089A200AC28E1133 (void);
// 0x00000265 System.Void WebRtcCSharp.PollingPeerRef::swap(WebRtcCSharp.SWIGTYPE_p_p_PollingPeer)
extern void PollingPeerRef_swap_mCCBF250D15C972506EFCF41E22643F1CA6431D55 (void);
// 0x00000266 System.Void WebRtcCSharp.PollingPeerRef::swap(WebRtcCSharp.PollingPeerRef)
extern void PollingPeerRef_swap_mDEAB0C96C4E34689221B64A4CABF9B0AD2F86F9D (void);
// 0x00000267 System.Void WebRtcCSharp.PollingPeerRef::Update()
extern void PollingPeerRef_Update_mED0760B9B7E04219ADD1DB2553C6450CED327495 (void);
// 0x00000268 System.Void WebRtcCSharp.PollingPeerRef::Flush()
extern void PollingPeerRef_Flush_mB042EB916F2739BBDD36547AAF515DEF8E84C33D (void);
// 0x00000269 WebRtcCSharp.PollingPeer/ConnectionState WebRtcCSharp.PollingPeerRef::GetConnectionState()
extern void PollingPeerRef_GetConnectionState_m01FACCF3FAEB47F214137BD27152446BC2E89135 (void);
// 0x0000026A System.Boolean WebRtcCSharp.PollingPeerRef::HasSignalingMessage()
extern void PollingPeerRef_HasSignalingMessage_m4BCAA09B152992B0DBEBB2AC0CEBCD801ED1C953 (void);
// 0x0000026B System.String WebRtcCSharp.PollingPeerRef::DequeueSignalingMessage()
extern void PollingPeerRef_DequeueSignalingMessage_mB693DE18977208672214E6D340865949F2103931 (void);
// 0x0000026C System.Void WebRtcCSharp.PollingPeerRef::AddSignalingMessage(System.String)
extern void PollingPeerRef_AddSignalingMessage_mAAB3CF3D6EF9DF41684CE27B6B7FE793056E3477 (void);
// 0x0000026D System.Boolean WebRtcCSharp.PollingPeerRef::HasSignalingError()
extern void PollingPeerRef_HasSignalingError_m0F5888B19B1472555C81CFC09A61353AF90E8341 (void);
// 0x0000026E System.String WebRtcCSharp.PollingPeerRef::DequeueSignalingError()
extern void PollingPeerRef_DequeueSignalingError_m415761502E8BBED85909D7D741E264697ACD3EC5 (void);
// 0x0000026F System.Void WebRtcCSharp.PollingPeerRef::CreateOffer()
extern void PollingPeerRef_CreateOffer_mDF2740EA5975E3A4845088439A057E7C9404025B (void);
// 0x00000270 System.Void WebRtcCSharp.PollingPeerRef::CreateAnswer()
extern void PollingPeerRef_CreateAnswer_m867064FE35919A0CB498577EEFA890F4F92D6FC5 (void);
// 0x00000271 System.Void WebRtcCSharp.PollingPeerRef::Close()
extern void PollingPeerRef_Close_m1CC3A8CCAE226AAE7EA099DD87604C9136A29C67 (void);
// 0x00000272 System.Void WebRtcCSharp.PollingPeerRef::CloseInternal()
extern void PollingPeerRef_CloseInternal_m52270E5AF1FE12396F5B03B4B5BB8C5027472F90 (void);
// 0x00000273 System.Void WebRtcCSharp.PollingPeerRef::Cleanup()
extern void PollingPeerRef_Cleanup_m027F1AB124C37DCDFDC512574EF7D35CD7DCA102 (void);
// 0x00000274 System.Int32 WebRtcCSharp.PollingPeerRef::CountOpenedDataChannels()
extern void PollingPeerRef_CountOpenedDataChannels_m36FF198425BAE8BDD1249E30258E598CC65144AC (void);
// 0x00000275 System.Boolean WebRtcCSharp.PollingPeerRef::HasDataChannelMessage()
extern void PollingPeerRef_HasDataChannelMessage_m5430B65234F0964E2F1B5C8617B11A402FAB57F5 (void);
// 0x00000276 System.Boolean WebRtcCSharp.PollingPeerRef::DequeueDataChannelMessage(System.Int32&,WebRtcCSharp.DataBuffer)
extern void PollingPeerRef_DequeueDataChannelMessage_mB3B1C0C8EB6331D722B4C7309B76DDB807BD138E (void);
// 0x00000277 System.Boolean WebRtcCSharp.PollingPeerRef::Send(System.Int32,System.Byte[],System.UInt32,System.UInt32)
extern void PollingPeerRef_Send_m335661AEB4038297F76C139F5F7A5AEA4D49F7D4 (void);
// 0x00000278 System.Int32 WebRtcCSharp.PollingPeerRef::GetBufferedAmount(System.Int32)
extern void PollingPeerRef_GetBufferedAmount_mDCC1F687960D4FEDC3EF7318766ABB2D73595F0D (void);
// 0x00000279 System.Void WebRtcCSharp.PollingPeerRef::CreateDataChannel(System.String,WebRtcCSharp.DataChannelInit)
extern void PollingPeerRef_CreateDataChannel_m026DDDCA8435951865138754CEB7F5E47A9AAC0A (void);
// 0x0000027A System.String WebRtcCSharp.PollingPeerRef::GetDataChannelLabel(System.Int32)
extern void PollingPeerRef_GetDataChannelLabel_m6A048CF81C612FB7FD7EB6B2E757703585240764 (void);
// 0x0000027B WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.PollingPeerRef::GetRemoteStream()
extern void PollingPeerRef_GetRemoteStream_mA67B1380A9442148E90F765153E6828B0394C8AE (void);
// 0x0000027C System.Void WebRtcCSharp.PollingPeerRef::AddLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingPeerRef_AddLocalStream_m22E8605B59A13EF216641FFDEC2D632C67072F70 (void);
// 0x0000027D System.Void WebRtcCSharp.PollingPeerRef::RemoveLocalStream(WebRtcCSharp.PollingMediaStreamRef)
extern void PollingPeerRef_RemoveLocalStream_m95221E8327FA7BC59C871B15CEAFCAB47BCCBE81 (void);
// 0x0000027E System.Void WebRtcCSharp.PollingPeerRef::RequestStats(System.Boolean,System.Boolean,System.Boolean)
extern void PollingPeerRef_RequestStats_m093392E1072CC36F90085D9874E0A90AE6E6EEE3 (void);
// 0x0000027F System.Boolean WebRtcCSharp.PollingPeerRef::HasStats()
extern void PollingPeerRef_HasStats_m131B7BF034E90E4407EC78129D3767495C53C1E0 (void);
// 0x00000280 System.Boolean WebRtcCSharp.PollingPeerRef::DequeueStats(WebRtcCSharp.SWIGTYPE_p_std__string)
extern void PollingPeerRef_DequeueStats_mDD640F18A79343AE22BC51643E19E3FD0B887127 (void);
// 0x00000281 System.Void WebRtcCSharp.PollingPeerRef::AddRef()
extern void PollingPeerRef_AddRef_m06D32ED4C4F0636A5A26E393890245313FF2BB5B (void);
// 0x00000282 WebRtcCSharp.RefCountReleaseStatus WebRtcCSharp.PollingPeerRef::Release()
extern void PollingPeerRef_Release_m06C209319D5126AD73A0B0DA513001356D2B6CB8 (void);
// 0x00000283 System.Void WebRtcCSharp.RTCLog::.ctor(System.IntPtr,System.Boolean)
extern void RTCLog__ctor_m91433C403B8B87F4CE92FE8DFF174FC746D94A2F (void);
// 0x00000284 System.Runtime.InteropServices.HandleRef WebRtcCSharp.RTCLog::getCPtr(WebRtcCSharp.RTCLog)
extern void RTCLog_getCPtr_m33DD58FFB5CD2C358035602AC7041A92C1E3BD32 (void);
// 0x00000285 System.Void WebRtcCSharp.RTCLog::Finalize()
extern void RTCLog_Finalize_m1883638026836A908DE459919BA5C7702A1959C8 (void);
// 0x00000286 System.Void WebRtcCSharp.RTCLog::Dispose()
extern void RTCLog_Dispose_mC5E02235B969AFEF3FF99120683A37EBB89E7025 (void);
// 0x00000287 System.Void WebRtcCSharp.RTCLog::L(System.String)
extern void RTCLog_L_mCF8F735ABE248CE20B6ECC6072AB8301D79EDF01 (void);
// 0x00000288 System.Void WebRtcCSharp.RTCLog::LW(System.String)
extern void RTCLog_LW_m4DE6465BE53CA5080E5F7D0CB0BC7B0FF3BF14E1 (void);
// 0x00000289 System.Void WebRtcCSharp.RTCLog::LE(System.String)
extern void RTCLog_LE_m0A4B1D7FC84A3B479BB3E77BC1BAD085952F2B6B (void);
// 0x0000028A System.Void WebRtcCSharp.RTCLog::.ctor()
extern void RTCLog__ctor_m0E307DB9E2CB94FFBC17D1DE63BD229EBF38D742 (void);
// 0x0000028B System.Void WebRtcCSharp.RTCPeerConnectionFactory::.ctor(System.IntPtr,System.Boolean)
extern void RTCPeerConnectionFactory__ctor_mE3AC1AA6E60F24DC5D6BF848733DE179E9C860B4 (void);
// 0x0000028C System.Runtime.InteropServices.HandleRef WebRtcCSharp.RTCPeerConnectionFactory::getCPtr(WebRtcCSharp.RTCPeerConnectionFactory)
extern void RTCPeerConnectionFactory_getCPtr_mF678806CDFEFB7F13DA37C2F65597660E24B81F6 (void);
// 0x0000028D System.Void WebRtcCSharp.RTCPeerConnectionFactory::Finalize()
extern void RTCPeerConnectionFactory_Finalize_m62684DCF29A6AC89E7AC1C0BCFFD696F36CB13C7 (void);
// 0x0000028E System.Void WebRtcCSharp.RTCPeerConnectionFactory::Dispose()
extern void RTCPeerConnectionFactory_Dispose_m81F26F4276C30D95FC458FCA8D91996CCC60CB47 (void);
// 0x0000028F WebRtcCSharp.RTCPeerConnectionFactoryRef WebRtcCSharp.RTCPeerConnectionFactory::Create()
extern void RTCPeerConnectionFactory_Create_m56F686A85B9E044509B807C486BBCCC3A137E7A1 (void);
// 0x00000290 System.Void WebRtcCSharp.RTCPeerConnectionFactory::SetObosoleteVideo(System.Boolean)
extern void RTCPeerConnectionFactory_SetObosoleteVideo_m149631FF0FA794D612920C1FC4C73FFE7FC31678 (void);
// 0x00000291 System.Void WebRtcCSharp.RTCPeerConnectionFactory::ForceDummyAudio(System.Boolean)
extern void RTCPeerConnectionFactory_ForceDummyAudio_m6CA2E2399CC816BF40380BF539748260615101ED (void);
// 0x00000292 System.Void WebRtcCSharp.RTCPeerConnectionFactory::AddVideoCaptureFactory(WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t)
extern void RTCPeerConnectionFactory_AddVideoCaptureFactory_m30DA8D9E136265E1B9D594E83D195D571E3AC5CA (void);
// 0x00000293 System.Boolean WebRtcCSharp.RTCPeerConnectionFactory::IsInitialized()
extern void RTCPeerConnectionFactory_IsInitialized_m61BFD56A558AA7B9E2615054BC996CD40BE35EB0 (void);
// 0x00000294 System.Boolean WebRtcCSharp.RTCPeerConnectionFactory::Initialize()
extern void RTCPeerConnectionFactory_Initialize_m049E43A29B41463CCCD4A62A3BC959D3265C32FD (void);
// 0x00000295 System.Void WebRtcCSharp.RTCPeerConnectionFactory::SetGlobalSpeakerMute(System.Boolean)
extern void RTCPeerConnectionFactory_SetGlobalSpeakerMute_mBE696799648D51AE707AF8A839D8DDA6FE4B0817 (void);
// 0x00000296 System.Boolean WebRtcCSharp.RTCPeerConnectionFactory::GetGlobalSpeakerMute()
extern void RTCPeerConnectionFactory_GetGlobalSpeakerMute_m8ADE623CE809CA8FDA27DE7F7BFFD50AE0FF353F (void);
// 0x00000297 System.Boolean WebRtcCSharp.RTCPeerConnectionFactory::HasGlobalSpeakerMute()
extern void RTCPeerConnectionFactory_HasGlobalSpeakerMute_mD1136221C946658D7B8338931383D39CE7970F4B (void);
// 0x00000298 WebRtcCSharp.PollingPeerRef WebRtcCSharp.RTCPeerConnectionFactory::CreatePollingPeer(WebRtcCSharp.PeerConnectionInterface/RTCConfiguration)
extern void RTCPeerConnectionFactory_CreatePollingPeer_mAD8A82E58BB1F69EA8339515D08B22E8F63776A8 (void);
// 0x00000299 WebRtcCSharp.AsyncPeerRef WebRtcCSharp.RTCPeerConnectionFactory::CreateAsyncPeer(WebRtcCSharp.PeerConnectionInterface/RTCConfiguration)
extern void RTCPeerConnectionFactory_CreateAsyncPeer_m5D74BA293BA00D0F911333E0430E5F593E0B0560 (void);
// 0x0000029A System.String WebRtcCSharp.RTCPeerConnectionFactory::GetDebugDeviceInfo()
extern void RTCPeerConnectionFactory_GetDebugDeviceInfo_m28F7C898B2A266C9D422A9FBBF315FE7AE2EF799 (void);
// 0x0000029B System.String WebRtcCSharp.RTCPeerConnectionFactory::GetDebugDeviceInfo_Old()
extern void RTCPeerConnectionFactory_GetDebugDeviceInfo_Old_m47C02E8DDF8420929450DEF84F98A8BC042E2B6B (void);
// 0x0000029C WebRtcCSharp.SWIGTYPE_p_RTCMediaStream WebRtcCSharp.RTCPeerConnectionFactory::CreateMediaStream(WebRtcCSharp.MediaConstraints)
extern void RTCPeerConnectionFactory_CreateMediaStream_m8826B5D488CAA3C8069C421B5CB634C1EA041432 (void);
// 0x0000029D WebRtcCSharp.SWIGTYPE_p_RTCMediaStream WebRtcCSharp.RTCPeerConnectionFactory::CreateMediaStream(WebRtcCSharp.MediaConstraints,WebRtcCSharp.AudioOptions)
extern void RTCPeerConnectionFactory_CreateMediaStream_mB7BEF1AC6812DE99953D4CFF660274FFEC2EAD92 (void);
// 0x0000029E WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.RTCPeerConnectionFactory::CreatePollingMediaStream(WebRtcCSharp.MediaConstraints)
extern void RTCPeerConnectionFactory_CreatePollingMediaStream_mA3CFA317C582D855B7E9451E88E29A1EDCB17135 (void);
// 0x0000029F WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.RTCPeerConnectionFactory::CreatePollingMediaStream(WebRtcCSharp.MediaConstraints,WebRtcCSharp.AudioOptions)
extern void RTCPeerConnectionFactory_CreatePollingMediaStream_m52519AEB2B7F97A6B6DF0B3BA5B83B4E9F23277E (void);
// 0x000002A0 WebRtcCSharp.StringVector WebRtcCSharp.RTCPeerConnectionFactory::GetVideoDevices()
extern void RTCPeerConnectionFactory_GetVideoDevices_mD7AA505EFD7F28FA938EB88A3456DBC1A8439C5B (void);
// 0x000002A1 WebRtcCSharp.VideoInputRef WebRtcCSharp.RTCPeerConnectionFactory::GetVideoInput()
extern void RTCPeerConnectionFactory_GetVideoInput_m9F940178B74C82FF12103A7A889BC4FB1530090A (void);
// 0x000002A2 WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t WebRtcCSharp.RTCPeerConnectionFactory::GetThreads()
extern void RTCPeerConnectionFactory_GetThreads_m06F79899FAC7B80B978A7E2FC33FC3A033EE979D (void);
// 0x000002A3 WebRtcCSharp.SWIGTYPE_p_rtc__Thread WebRtcCSharp.RTCPeerConnectionFactory::GetSignalingThread()
extern void RTCPeerConnectionFactory_GetSignalingThread_m017F05B705EE54645B787491C414AD33254E4015 (void);
// 0x000002A4 WebRtcCSharp.SWIGTYPE_p_webrtc__AudioDeviceModule WebRtcCSharp.RTCPeerConnectionFactory::GetAudioDeviceModule()
extern void RTCPeerConnectionFactory_GetAudioDeviceModule_m02397F16906B2D4C151276A69AE92966A9CC201C (void);
// 0x000002A5 System.Boolean WebRtcCSharp.RTCPeerConnectionFactory::InitAndroidContext(System.IntPtr)
extern void RTCPeerConnectionFactory_InitAndroidContext_mAED59B8F44AE89F01060D84B19FD0D7DF74A27C6 (void);
// 0x000002A6 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::.ctor(System.IntPtr,System.Boolean)
extern void RTCPeerConnectionFactoryRef__ctor_m81863D5234990F361AE953AEA0601AEE4861AE46 (void);
// 0x000002A7 System.Runtime.InteropServices.HandleRef WebRtcCSharp.RTCPeerConnectionFactoryRef::getCPtr(WebRtcCSharp.RTCPeerConnectionFactoryRef)
extern void RTCPeerConnectionFactoryRef_getCPtr_mC64FC9FFE23497340ED18227C01B8DC16E217D77 (void);
// 0x000002A8 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::Finalize()
extern void RTCPeerConnectionFactoryRef_Finalize_m5DC2D183F5EF6E51900B2C20123A06F681AB204B (void);
// 0x000002A9 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::Dispose()
extern void RTCPeerConnectionFactoryRef_Dispose_mE960024FEE5F9288A6D8444B641E771CEAE34585 (void);
// 0x000002AA System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::.ctor()
extern void RTCPeerConnectionFactoryRef__ctor_m475AAFC463F6FA114B45500547709D3F115EAA7E (void);
// 0x000002AB System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::.ctor(WebRtcCSharp.RTCPeerConnectionFactory)
extern void RTCPeerConnectionFactoryRef__ctor_mB7F7C427CDC29DF23CDF29647E7F2C984977EBEE (void);
// 0x000002AC System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::.ctor(WebRtcCSharp.RTCPeerConnectionFactoryRef)
extern void RTCPeerConnectionFactoryRef__ctor_m486D562C45DC49D429F322C9F2C2E755C98FC983 (void);
// 0x000002AD WebRtcCSharp.RTCPeerConnectionFactory WebRtcCSharp.RTCPeerConnectionFactoryRef::get()
extern void RTCPeerConnectionFactoryRef_get_m45590D6F80F3AF74E484867DFD743D4CB0F0288F (void);
// 0x000002AE WebRtcCSharp.RTCPeerConnectionFactory WebRtcCSharp.RTCPeerConnectionFactoryRef::__deref__()
extern void RTCPeerConnectionFactoryRef___deref___m7DE998A1CF036920F57EE14B3373E25C7A743AE0 (void);
// 0x000002AF WebRtcCSharp.RTCPeerConnectionFactory WebRtcCSharp.RTCPeerConnectionFactoryRef::release()
extern void RTCPeerConnectionFactoryRef_release_m5AD80C561B9D04058D418CC1ED104DF0FAF41F99 (void);
// 0x000002B0 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::swap(WebRtcCSharp.SWIGTYPE_p_p_RTCPeerConnectionFactory)
extern void RTCPeerConnectionFactoryRef_swap_mCF5358A8BB29F594F0FCE7FC68165F9BEAB86CBA (void);
// 0x000002B1 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::swap(WebRtcCSharp.RTCPeerConnectionFactoryRef)
extern void RTCPeerConnectionFactoryRef_swap_mFEDB7A20DF02D9B5433B27A23A40F92D28BCF13D (void);
// 0x000002B2 WebRtcCSharp.RTCPeerConnectionFactoryRef WebRtcCSharp.RTCPeerConnectionFactoryRef::Create()
extern void RTCPeerConnectionFactoryRef_Create_m06EB6FD3CA8B38FACEF9CD58A956B2C9EDDE798F (void);
// 0x000002B3 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::SetObosoleteVideo(System.Boolean)
extern void RTCPeerConnectionFactoryRef_SetObosoleteVideo_m3509C48D91549087A39500426507175E628268FF (void);
// 0x000002B4 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::ForceDummyAudio(System.Boolean)
extern void RTCPeerConnectionFactoryRef_ForceDummyAudio_mD25C6E9A36B668C54A7EEAD0BD61004D0DEEBE27 (void);
// 0x000002B5 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::AddVideoCaptureFactory(WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t)
extern void RTCPeerConnectionFactoryRef_AddVideoCaptureFactory_m1EA7FA2B06878080F3EAAE97807376514C3322C6 (void);
// 0x000002B6 System.Boolean WebRtcCSharp.RTCPeerConnectionFactoryRef::IsInitialized()
extern void RTCPeerConnectionFactoryRef_IsInitialized_m9D4221C4ED3CEE91C13850AC4E4A443213F9A2BA (void);
// 0x000002B7 System.Boolean WebRtcCSharp.RTCPeerConnectionFactoryRef::Initialize()
extern void RTCPeerConnectionFactoryRef_Initialize_mC24B195FB3576C4BADA4C052167AC8E329DFB4EF (void);
// 0x000002B8 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::SetGlobalSpeakerMute(System.Boolean)
extern void RTCPeerConnectionFactoryRef_SetGlobalSpeakerMute_m660448ED167A5B827E3950BB477FCCE0C68E73F6 (void);
// 0x000002B9 System.Boolean WebRtcCSharp.RTCPeerConnectionFactoryRef::GetGlobalSpeakerMute()
extern void RTCPeerConnectionFactoryRef_GetGlobalSpeakerMute_mB4633F14EC6F454F9057666323578DA9FCDAF456 (void);
// 0x000002BA System.Boolean WebRtcCSharp.RTCPeerConnectionFactoryRef::HasGlobalSpeakerMute()
extern void RTCPeerConnectionFactoryRef_HasGlobalSpeakerMute_mBC7A1F975F53D089BE6F67BB153E6591D94EC9A1 (void);
// 0x000002BB WebRtcCSharp.PollingPeerRef WebRtcCSharp.RTCPeerConnectionFactoryRef::CreatePollingPeer(WebRtcCSharp.PeerConnectionInterface/RTCConfiguration)
extern void RTCPeerConnectionFactoryRef_CreatePollingPeer_m82F28AE7CFDD8BC26B8C0FE7B0D7DABA2E62AD61 (void);
// 0x000002BC WebRtcCSharp.AsyncPeerRef WebRtcCSharp.RTCPeerConnectionFactoryRef::CreateAsyncPeer(WebRtcCSharp.PeerConnectionInterface/RTCConfiguration)
extern void RTCPeerConnectionFactoryRef_CreateAsyncPeer_mE61C2D738D9E23E7AB49B971E9ED2F93E5F1F8EC (void);
// 0x000002BD System.String WebRtcCSharp.RTCPeerConnectionFactoryRef::GetDebugDeviceInfo()
extern void RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_m8585A62907E2E20D6EF2F7C3C765564B30EB1977 (void);
// 0x000002BE System.String WebRtcCSharp.RTCPeerConnectionFactoryRef::GetDebugDeviceInfo_Old()
extern void RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old_m843D18ED76BE60A5DE3CFE5E7B23CC5C2B5E9BF1 (void);
// 0x000002BF WebRtcCSharp.SWIGTYPE_p_RTCMediaStream WebRtcCSharp.RTCPeerConnectionFactoryRef::CreateMediaStream(WebRtcCSharp.MediaConstraints)
extern void RTCPeerConnectionFactoryRef_CreateMediaStream_mB650BD1FE6BA3945D7F2E4E4219731F5F13BA481 (void);
// 0x000002C0 WebRtcCSharp.SWIGTYPE_p_RTCMediaStream WebRtcCSharp.RTCPeerConnectionFactoryRef::CreateMediaStream(WebRtcCSharp.MediaConstraints,WebRtcCSharp.AudioOptions)
extern void RTCPeerConnectionFactoryRef_CreateMediaStream_mA083CDF688EF7E579B79B5ACB6E4AF033034BDF1 (void);
// 0x000002C1 WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.RTCPeerConnectionFactoryRef::CreatePollingMediaStream(WebRtcCSharp.MediaConstraints)
extern void RTCPeerConnectionFactoryRef_CreatePollingMediaStream_mE1CE6A6AC63D1E5A4026FC24E077A9FC3B492F75 (void);
// 0x000002C2 WebRtcCSharp.PollingMediaStreamRef WebRtcCSharp.RTCPeerConnectionFactoryRef::CreatePollingMediaStream(WebRtcCSharp.MediaConstraints,WebRtcCSharp.AudioOptions)
extern void RTCPeerConnectionFactoryRef_CreatePollingMediaStream_mE6BA517CC385CD29E08D7379345D0F172AB3DD18 (void);
// 0x000002C3 WebRtcCSharp.StringVector WebRtcCSharp.RTCPeerConnectionFactoryRef::GetVideoDevices()
extern void RTCPeerConnectionFactoryRef_GetVideoDevices_m3D7E5B5186AD09CC90687043428640CA08E31AA9 (void);
// 0x000002C4 WebRtcCSharp.VideoInputRef WebRtcCSharp.RTCPeerConnectionFactoryRef::GetVideoInput()
extern void RTCPeerConnectionFactoryRef_GetVideoInput_mACA86CAFE6E265387A5785A40CA253A3C5CF49B7 (void);
// 0x000002C5 WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t WebRtcCSharp.RTCPeerConnectionFactoryRef::GetThreads()
extern void RTCPeerConnectionFactoryRef_GetThreads_mCEE99FB2C8D963BDA1B1616AC063008A9C89BCD4 (void);
// 0x000002C6 WebRtcCSharp.SWIGTYPE_p_rtc__Thread WebRtcCSharp.RTCPeerConnectionFactoryRef::GetSignalingThread()
extern void RTCPeerConnectionFactoryRef_GetSignalingThread_m31CB53CF768825AF9D448D5CB3F8713348DD3EE9 (void);
// 0x000002C7 WebRtcCSharp.SWIGTYPE_p_webrtc__AudioDeviceModule WebRtcCSharp.RTCPeerConnectionFactoryRef::GetAudioDeviceModule()
extern void RTCPeerConnectionFactoryRef_GetAudioDeviceModule_m672EE03AFE92A162438A3D5ED2F824BB7290063A (void);
// 0x000002C8 System.Boolean WebRtcCSharp.RTCPeerConnectionFactoryRef::InitAndroidContext(System.IntPtr)
extern void RTCPeerConnectionFactoryRef_InitAndroidContext_m5D900D024D143595F7117DAA055BDD5348B8A0C0 (void);
// 0x000002C9 System.Void WebRtcCSharp.RTCPeerConnectionFactoryRef::AddRef()
extern void RTCPeerConnectionFactoryRef_AddRef_mCA93C021A3D3794A7EE15E0B48560E063ECA0009 (void);
// 0x000002CA WebRtcCSharp.RefCountReleaseStatus WebRtcCSharp.RTCPeerConnectionFactoryRef::Release()
extern void RTCPeerConnectionFactoryRef_Release_mB31668494EEC2EFFF557C9B0F49ED1E912520D95 (void);
// 0x000002CB System.Void WebRtcCSharp.RefCountInterface::.ctor(System.IntPtr,System.Boolean)
extern void RefCountInterface__ctor_m7354666FC1071992A5AD8806678E41192B44A360 (void);
// 0x000002CC System.Runtime.InteropServices.HandleRef WebRtcCSharp.RefCountInterface::getCPtr(WebRtcCSharp.RefCountInterface)
extern void RefCountInterface_getCPtr_m13B445445D26F9197F7E098711A93C30E6A59146 (void);
// 0x000002CD System.Void WebRtcCSharp.RefCountInterface::Dispose()
extern void RefCountInterface_Dispose_m96E99B5CBC31020A823CA51065842532905FBD63 (void);
// 0x000002CE System.Void WebRtcCSharp.RefCountInterface::AddRef()
extern void RefCountInterface_AddRef_m6D6BD413929D07E207D7DFA5D839509135379B2E (void);
// 0x000002CF WebRtcCSharp.RefCountReleaseStatus WebRtcCSharp.RefCountInterface::Release()
extern void RefCountInterface_Release_m0630177E0EFBC86AEBC595A647DE1B604BB71E73 (void);
// 0x000002D0 System.Void WebRtcCSharp.SWIGTYPE_p_CreateSessionDescriptionObserver::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_CreateSessionDescriptionObserver__ctor_m5BF1E7109BE0D998141741AF858653453309A09A (void);
// 0x000002D1 System.Void WebRtcCSharp.SWIGTYPE_p_CreateSessionDescriptionObserver::.ctor()
extern void SWIGTYPE_p_CreateSessionDescriptionObserver__ctor_m5B4052B9505C07A3D52720B1A250C62E4C944363 (void);
// 0x000002D2 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_CreateSessionDescriptionObserver::getCPtr(WebRtcCSharp.SWIGTYPE_p_CreateSessionDescriptionObserver)
extern void SWIGTYPE_p_CreateSessionDescriptionObserver_getCPtr_m38E423909D8C1A38499CA4B1D5C1BE674AA82D08 (void);
// 0x000002D3 System.Void WebRtcCSharp.SWIGTYPE_p_FILE::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_FILE__ctor_m45A773847AD97C7D6994F3B13A687C2F2571160B (void);
// 0x000002D4 System.Void WebRtcCSharp.SWIGTYPE_p_FILE::.ctor()
extern void SWIGTYPE_p_FILE__ctor_mAF4DCF05EF6D960B7E2CA2C640A6A3C04FB3EF02 (void);
// 0x000002D5 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_FILE::getCPtr(WebRtcCSharp.SWIGTYPE_p_FILE)
extern void SWIGTYPE_p_FILE_getCPtr_m7DFD535754E53F4BACBA2DF58252C5C120A3D46A (void);
// 0x000002D6 System.Void WebRtcCSharp.SWIGTYPE_p_I420BufferInterface::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_I420BufferInterface__ctor_m4D86880864845D2BC9BDE762F25C82597FE9F28B (void);
// 0x000002D7 System.Void WebRtcCSharp.SWIGTYPE_p_I420BufferInterface::.ctor()
extern void SWIGTYPE_p_I420BufferInterface__ctor_m08D0AFE90D22965BD0BD89FA0D6CC4BA402EB847 (void);
// 0x000002D8 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_I420BufferInterface::getCPtr(WebRtcCSharp.SWIGTYPE_p_I420BufferInterface)
extern void SWIGTYPE_p_I420BufferInterface_getCPtr_mE64427050F332A9BD08EBAA2F04D565DF904E8DD (void);
// 0x000002D9 System.Void WebRtcCSharp.SWIGTYPE_p_IceCandidateInterface::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_IceCandidateInterface__ctor_m148EE648EDBB7E1C6B5FDC5DF0A3EE55799185F7 (void);
// 0x000002DA System.Void WebRtcCSharp.SWIGTYPE_p_IceCandidateInterface::.ctor()
extern void SWIGTYPE_p_IceCandidateInterface__ctor_mD37D258BD50B5BE3ADC2312137ADE5FC7688CE9C (void);
// 0x000002DB System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_IceCandidateInterface::getCPtr(WebRtcCSharp.SWIGTYPE_p_IceCandidateInterface)
extern void SWIGTYPE_p_IceCandidateInterface_getCPtr_m062B780C04DBA7D461CBBAD1BDB9A4E9E57D448F (void);
// 0x000002DC System.Void WebRtcCSharp.SWIGTYPE_p_MediaStreamInterface::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_MediaStreamInterface__ctor_mFA65B86C8F0E857AB255156D0CD6930C69D44613 (void);
// 0x000002DD System.Void WebRtcCSharp.SWIGTYPE_p_MediaStreamInterface::.ctor()
extern void SWIGTYPE_p_MediaStreamInterface__ctor_m58781A9057C0F87A4B58831129E466546299DD47 (void);
// 0x000002DE System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_MediaStreamInterface::getCPtr(WebRtcCSharp.SWIGTYPE_p_MediaStreamInterface)
extern void SWIGTYPE_p_MediaStreamInterface_getCPtr_m01C359A1360177D155AC4B78812DCFDB2DF3D4F2 (void);
// 0x000002DF System.Void WebRtcCSharp.SWIGTYPE_p_MediaStreamTrackInterface::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_MediaStreamTrackInterface__ctor_mC36E5779B66CF0AB046A8FDABFE9E1B1859D63F5 (void);
// 0x000002E0 System.Void WebRtcCSharp.SWIGTYPE_p_MediaStreamTrackInterface::.ctor()
extern void SWIGTYPE_p_MediaStreamTrackInterface__ctor_m410BA54DC2314BF978476A3EB30EDE939D83030F (void);
// 0x000002E1 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_MediaStreamTrackInterface::getCPtr(WebRtcCSharp.SWIGTYPE_p_MediaStreamTrackInterface)
extern void SWIGTYPE_p_MediaStreamTrackInterface_getCPtr_m613D6110B2BB011B009F4727F164D438EE3CF99C (void);
// 0x000002E2 System.Void WebRtcCSharp.SWIGTYPE_p_RTCError::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_RTCError__ctor_mFCACEAFBF69843A947529171E6C96AA062921019 (void);
// 0x000002E3 System.Void WebRtcCSharp.SWIGTYPE_p_RTCError::.ctor()
extern void SWIGTYPE_p_RTCError__ctor_mB3666F6577B722888E87F80065227CFCAE7E25BF (void);
// 0x000002E4 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_RTCError::getCPtr(WebRtcCSharp.SWIGTYPE_p_RTCError)
extern void SWIGTYPE_p_RTCError_getCPtr_m29E1538A7579A68374C306FE459C89092FBE49E6 (void);
// 0x000002E5 System.Void WebRtcCSharp.SWIGTYPE_p_RTCMediaStream::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_RTCMediaStream__ctor_m1AFD95C7AC7D8B35B5BCE79097369664E1CB2DFE (void);
// 0x000002E6 System.Void WebRtcCSharp.SWIGTYPE_p_RTCMediaStream::.ctor()
extern void SWIGTYPE_p_RTCMediaStream__ctor_mF62EA81EA2DAF42A0DB9ABF48C2E288D4D9046B3 (void);
// 0x000002E7 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_RTCMediaStream::getCPtr(WebRtcCSharp.SWIGTYPE_p_RTCMediaStream)
extern void SWIGTYPE_p_RTCMediaStream_getCPtr_mAB2239DD414B257890F3ED50FAC18A1FE8251B35 (void);
// 0x000002E8 System.Void WebRtcCSharp.SWIGTYPE_p_RTCStatsCollectorCallback::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_RTCStatsCollectorCallback__ctor_m680E519D393E2BB7393B0173974364906718C125 (void);
// 0x000002E9 System.Void WebRtcCSharp.SWIGTYPE_p_RTCStatsCollectorCallback::.ctor()
extern void SWIGTYPE_p_RTCStatsCollectorCallback__ctor_mAD9033225BFD8F6CED64F22D61613466D3A016AB (void);
// 0x000002EA System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_RTCStatsCollectorCallback::getCPtr(WebRtcCSharp.SWIGTYPE_p_RTCStatsCollectorCallback)
extern void SWIGTYPE_p_RTCStatsCollectorCallback_getCPtr_m288A5BFEC7B0A502BAF7AC7243A65E2E566CD742 (void);
// 0x000002EB System.Void WebRtcCSharp.SWIGTYPE_p_RtpSenderInterface::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_RtpSenderInterface__ctor_mDA47EA2CE741CFEE50630CB5B0C7292D2A7F1DE4 (void);
// 0x000002EC System.Void WebRtcCSharp.SWIGTYPE_p_RtpSenderInterface::.ctor()
extern void SWIGTYPE_p_RtpSenderInterface__ctor_m8975EAB7E208777E9CE552E841ABBC7604419798 (void);
// 0x000002ED System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_RtpSenderInterface::getCPtr(WebRtcCSharp.SWIGTYPE_p_RtpSenderInterface)
extern void SWIGTYPE_p_RtpSenderInterface_getCPtr_m560B736D2306F03F085452837098CE6E17AC9013 (void);
// 0x000002EE System.Void WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_SessionDescriptionInterface__ctor_m24F10A2A7A8C6CE39880D9AE59DC075C293881B0 (void);
// 0x000002EF System.Void WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface::.ctor()
extern void SWIGTYPE_p_SessionDescriptionInterface__ctor_mF7D2A79A667355C43C6471BC61FE65BCD624F647 (void);
// 0x000002F0 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface::getCPtr(WebRtcCSharp.SWIGTYPE_p_SessionDescriptionInterface)
extern void SWIGTYPE_p_SessionDescriptionInterface_getCPtr_m2CF286D59B1B0CCD9601C0DCC8BEB78B9179762C (void);
// 0x000002F1 System.Void WebRtcCSharp.SWIGTYPE_p_SetSessionDescriptionObserver::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_SetSessionDescriptionObserver__ctor_m068E125C2A828AE978A837E8821249AF73E9104D (void);
// 0x000002F2 System.Void WebRtcCSharp.SWIGTYPE_p_SetSessionDescriptionObserver::.ctor()
extern void SWIGTYPE_p_SetSessionDescriptionObserver__ctor_m8350BD0C36E3632295DD2F8AC2F00E2E7D307C35 (void);
// 0x000002F3 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_SetSessionDescriptionObserver::getCPtr(WebRtcCSharp.SWIGTYPE_p_SetSessionDescriptionObserver)
extern void SWIGTYPE_p_SetSessionDescriptionObserver_getCPtr_m2A2769A5E325242B98CA0DCAF4077D86FC37886A (void);
// 0x000002F4 System.Void WebRtcCSharp.SWIGTYPE_p_VideoFrame::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_VideoFrame__ctor_mE2BE51C8A4834036F53B0CD87066A7BB5A246E68 (void);
// 0x000002F5 System.Void WebRtcCSharp.SWIGTYPE_p_VideoFrame::.ctor()
extern void SWIGTYPE_p_VideoFrame__ctor_mF114B8304ED571635F52C5092CE80A8A27449BCA (void);
// 0x000002F6 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_VideoFrame::getCPtr(WebRtcCSharp.SWIGTYPE_p_VideoFrame)
extern void SWIGTYPE_p_VideoFrame_getCPtr_m7F194161D52A176B13237D9F47C1566472D69BA2 (void);
// 0x000002F7 System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_CryptoOptions_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_absl__optionalT_CryptoOptions_t__ctor_mD1D83FB794AC35F33323EBB0C032411977FF2514 (void);
// 0x000002F8 System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_CryptoOptions_t::.ctor()
extern void SWIGTYPE_p_absl__optionalT_CryptoOptions_t__ctor_m73B41E9F9B35ECE901D6F9D0BFBF7D7EDDF314EA (void);
// 0x000002F9 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_absl__optionalT_CryptoOptions_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_CryptoOptions_t)
extern void SWIGTYPE_p_absl__optionalT_CryptoOptions_t_getCPtr_mFAB7656CFF070B1AA1B9045F66BBDC6A4652BBC0 (void);
// 0x000002FA System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_bool_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_absl__optionalT_bool_t__ctor_m5D43F03EB89E81CA528FFE441F76F89419B391DC (void);
// 0x000002FB System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_bool_t::.ctor()
extern void SWIGTYPE_p_absl__optionalT_bool_t__ctor_m290A15BC114FD1E30C9EAA1831FA743CFA465ADA (void);
// 0x000002FC System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_absl__optionalT_bool_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_bool_t)
extern void SWIGTYPE_p_absl__optionalT_bool_t_getCPtr_m3FFA499AA49C3AEA6099EED60F18FB27894E02C4 (void);
// 0x000002FD System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_absl__optionalT_int_t__ctor_m731CDD3EAFCE75F9EFC85D88A9D8BC5349FC106C (void);
// 0x000002FE System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t::.ctor()
extern void SWIGTYPE_p_absl__optionalT_int_t__ctor_mF866B66C11E38A6C429A5CCE189EFBE637969493 (void);
// 0x000002FF System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_int_t)
extern void SWIGTYPE_p_absl__optionalT_int_t_getCPtr_m2DE83335D8EC6B6E08C215FF36C7E29DB31EC598 (void);
// 0x00000300 System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t__ctor_m451C6225DAF83F989EC624830F1303482705A739 (void);
// 0x00000301 System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t::.ctor()
extern void SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t__ctor_m1324FADACF5302A51574784EC555C3608F1046EB (void);
// 0x00000302 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t)
extern void SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t_getCPtr_mEC9D7CD94F0F90296C7C2BA9626A7EB611D6E775 (void);
// 0x00000303 System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t__ctor_mBD8D7645669B675F1C6EB1A2B4D75B33DBCA552F (void);
// 0x00000304 System.Void WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t::.ctor()
extern void SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t__ctor_mBC75038EB83E1300FC2DC4D05803D1D554479F49 (void);
// 0x00000305 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t)
extern void SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t_getCPtr_mCD39E20AF15018235EBACDB64009118BC376C695 (void);
// 0x00000306 System.Void WebRtcCSharp.SWIGTYPE_p_cricket__MediaConfig::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_cricket__MediaConfig__ctor_m0B86807D4F977F2215ED5A6F3738610344381B05 (void);
// 0x00000307 System.Void WebRtcCSharp.SWIGTYPE_p_cricket__MediaConfig::.ctor()
extern void SWIGTYPE_p_cricket__MediaConfig__ctor_m031878A35166E7D18F2F1F00844D90C9FB0C1286 (void);
// 0x00000308 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_cricket__MediaConfig::getCPtr(WebRtcCSharp.SWIGTYPE_p_cricket__MediaConfig)
extern void SWIGTYPE_p_cricket__MediaConfig_getCPtr_m7147AC26708D5BB5A240FEA00ECD62A97C7602C5 (void);
// 0x00000309 System.Void WebRtcCSharp.SWIGTYPE_p_libyuv__RotationMode::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_libyuv__RotationMode__ctor_mCD6B67FC71971B2D934B22038016F13773246470 (void);
// 0x0000030A System.Void WebRtcCSharp.SWIGTYPE_p_libyuv__RotationMode::.ctor()
extern void SWIGTYPE_p_libyuv__RotationMode__ctor_mB454BD60DF27E45C876E1FE66308899D4F6CFCD1 (void);
// 0x0000030B System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_libyuv__RotationMode::getCPtr(WebRtcCSharp.SWIGTYPE_p_libyuv__RotationMode)
extern void SWIGTYPE_p_libyuv__RotationMode_getCPtr_m5CA732DFAD5C10D3DCEFC3818285EE0EAE4DC357 (void);
// 0x0000030C System.Void WebRtcCSharp.SWIGTYPE_p_p_AsyncDataChannel::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_p_AsyncDataChannel__ctor_mFFD0528499144D811C81F47126A714C959C21911 (void);
// 0x0000030D System.Void WebRtcCSharp.SWIGTYPE_p_p_AsyncDataChannel::.ctor()
extern void SWIGTYPE_p_p_AsyncDataChannel__ctor_m598B2DB8F7784A9C63CD018A2602A8941B82496D (void);
// 0x0000030E System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_p_AsyncDataChannel::getCPtr(WebRtcCSharp.SWIGTYPE_p_p_AsyncDataChannel)
extern void SWIGTYPE_p_p_AsyncDataChannel_getCPtr_mD8A1CAFD6490D06B665BC95B08CD1F96CE33C5FB (void);
// 0x0000030F System.Void WebRtcCSharp.SWIGTYPE_p_p_AsyncPeer::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_p_AsyncPeer__ctor_m8A9E72608927D43EA3177749864159771F8CA99E (void);
// 0x00000310 System.Void WebRtcCSharp.SWIGTYPE_p_p_AsyncPeer::.ctor()
extern void SWIGTYPE_p_p_AsyncPeer__ctor_m299D42E13B822CA49E7547743D7C79F447C7B0E7 (void);
// 0x00000311 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_p_AsyncPeer::getCPtr(WebRtcCSharp.SWIGTYPE_p_p_AsyncPeer)
extern void SWIGTYPE_p_p_AsyncPeer_getCPtr_m528918A9907712AD3FD4E21206A4AF9B436413F4 (void);
// 0x00000312 System.Void WebRtcCSharp.SWIGTYPE_p_p_PollingMediaStream::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_p_PollingMediaStream__ctor_mFD7244A2B72E0A62A3C315FDCAEFEE28F44FC17F (void);
// 0x00000313 System.Void WebRtcCSharp.SWIGTYPE_p_p_PollingMediaStream::.ctor()
extern void SWIGTYPE_p_p_PollingMediaStream__ctor_mB9F332724FDDA7652EC8EDBEA0F5F5EE20AFF635 (void);
// 0x00000314 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_p_PollingMediaStream::getCPtr(WebRtcCSharp.SWIGTYPE_p_p_PollingMediaStream)
extern void SWIGTYPE_p_p_PollingMediaStream_getCPtr_m21A745A507A88DDC8FE34BBBE7C7CD9995E12326 (void);
// 0x00000315 System.Void WebRtcCSharp.SWIGTYPE_p_p_PollingPeer::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_p_PollingPeer__ctor_m3AB36E18D8E9877D9DD11D50B6206F40BEC0E1AA (void);
// 0x00000316 System.Void WebRtcCSharp.SWIGTYPE_p_p_PollingPeer::.ctor()
extern void SWIGTYPE_p_p_PollingPeer__ctor_mF0A50C4D1745FE615B463C45562B8212F2180877 (void);
// 0x00000317 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_p_PollingPeer::getCPtr(WebRtcCSharp.SWIGTYPE_p_p_PollingPeer)
extern void SWIGTYPE_p_p_PollingPeer_getCPtr_m08D77C57158AF9B1146BA79C65B0F739210BE3BA (void);
// 0x00000318 System.Void WebRtcCSharp.SWIGTYPE_p_p_RTCPeerConnectionFactory::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_p_RTCPeerConnectionFactory__ctor_m8567C1421DDAC3CAD18FB0D4DF76D875A819689E (void);
// 0x00000319 System.Void WebRtcCSharp.SWIGTYPE_p_p_RTCPeerConnectionFactory::.ctor()
extern void SWIGTYPE_p_p_RTCPeerConnectionFactory__ctor_m478DED90B324A35780C0F38BC807BB029BA02C0C (void);
// 0x0000031A System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_p_RTCPeerConnectionFactory::getCPtr(WebRtcCSharp.SWIGTYPE_p_p_RTCPeerConnectionFactory)
extern void SWIGTYPE_p_p_RTCPeerConnectionFactory_getCPtr_m06C899DD97E54B80F682FD900D44C965912AEC55 (void);
// 0x0000031B System.Void WebRtcCSharp.SWIGTYPE_p_p_VideoInput::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_p_VideoInput__ctor_m52DD447F4BD46EDA1F3C34C295F79897CF4E6DB9 (void);
// 0x0000031C System.Void WebRtcCSharp.SWIGTYPE_p_p_VideoInput::.ctor()
extern void SWIGTYPE_p_p_VideoInput__ctor_mA66793AC333CD065B896EFD461F84039BF204C1A (void);
// 0x0000031D System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_p_VideoInput::getCPtr(WebRtcCSharp.SWIGTYPE_p_p_VideoInput)
extern void SWIGTYPE_p_p_VideoInput_getCPtr_m46B6A1F1B2716CCD92A3D84DA3F8990E5A04AF3A (void);
// 0x0000031E System.Void WebRtcCSharp.SWIGTYPE_p_rtc__Thread::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_rtc__Thread__ctor_mCAF3AF9566ED26E09F0A3F4E14CC7EFD65BB6D6D (void);
// 0x0000031F System.Void WebRtcCSharp.SWIGTYPE_p_rtc__Thread::.ctor()
extern void SWIGTYPE_p_rtc__Thread__ctor_m31580414FE8131A12F62C92559C14B18343B9714 (void);
// 0x00000320 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_rtc__Thread::getCPtr(WebRtcCSharp.SWIGTYPE_p_rtc__Thread)
extern void SWIGTYPE_p_rtc__Thread_getCPtr_mEDE170E9B2838011A9291258621375DE05C72335 (void);
// 0x00000321 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t__ctor_mBD706571C4A7AA01A7B57063D7A38B1A29A6F4EA (void);
// 0x00000322 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t::.ctor()
extern void SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t__ctor_mC02FA0D09166B2EB14A1B9BF0016847EEB263DBA (void);
// 0x00000323 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t)
extern void SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t_getCPtr_m26E7378DA30E59463B98D15225CFF97857D4E1A2 (void);
// 0x00000324 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t__ctor_mE19F0240492AE600C5A5441E275638DA7B2DD5DF (void);
// 0x00000325 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t::.ctor()
extern void SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t__ctor_m73EB3D70E8073A6AF5A82ABCCD26ABF1CD645D83 (void);
// 0x00000326 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t)
extern void SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t_getCPtr_m73CB71CE80F4D5359E9913FCDC9109513738B936 (void);
// 0x00000327 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t__ctor_mA09A713B10836944F5CD5B5D95C4B7859FE432A9 (void);
// 0x00000328 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t::.ctor()
extern void SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t__ctor_m545BBEF67DAF697798474EFD68352F80AD86B5AD (void);
// 0x00000329 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t)
extern void SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t_getCPtr_m36744972E736A2AFBA01FBF1C4389D85B3E142B5 (void);
// 0x0000032A System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t__ctor_m8594D547C08DE1D086D97D3A45D4E5A3753A6858 (void);
// 0x0000032B System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t::.ctor()
extern void SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t__ctor_m1E6368E43E2696FFE6F197E2212E2F960E9E2A01 (void);
// 0x0000032C System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t)
extern void SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t_getCPtr_m8CB74883C94A8761898E955D821D8264150399B2 (void);
// 0x0000032D System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t__ctor_m728DABF6B70844457E2F7E0F6DD070A9CBB08E64 (void);
// 0x0000032E System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t::.ctor()
extern void SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t__ctor_mAB40EFB467A3612BBB2D4CB413F59F1344554948 (void);
// 0x0000032F System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t)
extern void SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t_getCPtr_mE0AFDE3609912024D38465F3AFAE2D6339885B3B (void);
// 0x00000330 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t__ctor_m977254FD1FD15E9FA94E7B809BFE49011C086CF6 (void);
// 0x00000331 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t::.ctor()
extern void SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t__ctor_mB1E69A63761DA7F4856A24E83C8451326A31E93B (void);
// 0x00000332 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t)
extern void SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t_getCPtr_m95AC999D655AB69B2ED760FD414E650F264931BA (void);
// 0x00000333 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t__ctor_m2553635777C8F4BB38F34B7A1C1357337707ED51 (void);
// 0x00000334 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t::.ctor()
extern void SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t__ctor_m2C6AA371A056D0879013DF59F7E358C216855FC8 (void);
// 0x00000335 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t)
extern void SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t_getCPtr_m9842F054BD6A8761F718B76B7F57980AC9BB2648 (void);
// 0x00000336 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t__ctor_mCB603D0F1218E82E1A51652A4E4319BB64450D88 (void);
// 0x00000337 System.Void WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t::.ctor()
extern void SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t__ctor_mD49A4CB6E679D533E6853BB7E8F6F14BC634D8BA (void);
// 0x00000338 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t)
extern void SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t_getCPtr_mF86E8BBA9CEA269A1F83ED35CECE037FEC7E0C7B (void);
// 0x00000339 System.Void WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t__ctor_mB9119D7CB0FBE879338CA5B54410E5D0E990608C (void);
// 0x0000033A System.Void WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t::.ctor()
extern void SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t__ctor_mD9881F608D3801E1C77070D30BEDE2BEAE514026 (void);
// 0x0000033B System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t)
extern void SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t_getCPtr_m08E81649570DE09346889B8EB4E8F785C9F66399 (void);
// 0x0000033C System.Void WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t__ctor_m69E441D8416466AB365FA59590AA0101E78FD9B7 (void);
// 0x0000033D System.Void WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t::.ctor()
extern void SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t__ctor_mDD240A2D4B383A7D7FE55C09E6EFD546EE3A0E7C (void);
// 0x0000033E System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t)
extern void SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t_getCPtr_mFABE54B9E12C007ADBD5C1CE5A2E04A6BFC843C6 (void);
// 0x0000033F System.Void WebRtcCSharp.SWIGTYPE_p_std__string::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_std__string__ctor_m1930E822BCA9000409EA7E030E36C98AC43AF2CC (void);
// 0x00000340 System.Void WebRtcCSharp.SWIGTYPE_p_std__string::.ctor()
extern void SWIGTYPE_p_std__string__ctor_mEAA3EA8D304DB0735E94C95C4B09DEF8122DF2ED (void);
// 0x00000341 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_std__string::getCPtr(WebRtcCSharp.SWIGTYPE_p_std__string)
extern void SWIGTYPE_p_std__string_getCPtr_m7E89C1A38180B6B378A359B86FE5E8D9DD06CA17 (void);
// 0x00000342 System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_cricket__Candidate_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_std__vectorT_cricket__Candidate_t__ctor_mB66D28B56B2CD2651D29E36B2EDBC23181A6EC00 (void);
// 0x00000343 System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_cricket__Candidate_t::.ctor()
extern void SWIGTYPE_p_std__vectorT_cricket__Candidate_t__ctor_m4B2CA5C5ACFB514A0EAFE0C8B86A0F1A779F1686 (void);
// 0x00000344 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_std__vectorT_cricket__Candidate_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_std__vectorT_cricket__Candidate_t)
extern void SWIGTYPE_p_std__vectorT_cricket__Candidate_t_getCPtr_m6FEB0A316FF86EF02D26C645AD275429A13A5FA5 (void);
// 0x00000345 System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t__ctor_mEB129BDC287F25C951B1A127994214CBA7B93A73 (void);
// 0x00000346 System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t::.ctor()
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t__ctor_m96EE98401B31B45E3AB2FF396C2F2890EDC1D25D (void);
// 0x00000347 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t)
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t_getCPtr_m46645AF17210C1D9638230E70FD0D44C2255BF2B (void);
// 0x00000348 System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t__ctor_m9C619744F223466D970113EAB5DA777774B08331 (void);
// 0x00000349 System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t::.ctor()
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t__ctor_mF4D40F7F2FBBE44B7AF5ABDD1BE6B1BB5B74CBA3 (void);
// 0x0000034A System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t)
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t_getCPtr_mB0C2B7D5F3CC87E1A50C28A20D269CD9678262AD (void);
// 0x0000034B System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t__ctor_m11257E37C68B2A9BCEE027E11E924B5928A9739F (void);
// 0x0000034C System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t::.ctor()
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t__ctor_m905C73652A6B34D69C03C380B3C7AD2E4CB395B6 (void);
// 0x0000034D System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t)
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t_getCPtr_mA0BA2364A3D295B80AC3AE499AF0667720422B23 (void);
// 0x0000034E System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t__ctor_mBC0CC1DA5CE494521163D7ADDC315269102CF6C6 (void);
// 0x0000034F System.Void WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t::.ctor()
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t__ctor_mF5FADE3FE0295971D7AB87CD9A090FFAC40F47FB (void);
// 0x00000350 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t::getCPtr(WebRtcCSharp.SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t)
extern void SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t_getCPtr_m7EF1C6E4679A4515972404F49EAC8382BE10B347 (void);
// 0x00000351 System.Void WebRtcCSharp.SWIGTYPE_p_unsigned_char::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_unsigned_char__ctor_m98C1DA311CEB3123228A1F08EF80E605F44EA8C8 (void);
// 0x00000352 System.Void WebRtcCSharp.SWIGTYPE_p_unsigned_char::.ctor()
extern void SWIGTYPE_p_unsigned_char__ctor_mEB072D02A84A0797947C4D6D1F703636CB8D3AEE (void);
// 0x00000353 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_unsigned_char::getCPtr(WebRtcCSharp.SWIGTYPE_p_unsigned_char)
extern void SWIGTYPE_p_unsigned_char_getCPtr_m1D96804BC931F06A7BCB99F46F5B3473DE047C5B (void);
// 0x00000354 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__AudioDeviceModule::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_webrtc__AudioDeviceModule__ctor_m0D542C6F29113F54DE45D7E5B7D52DE13E8C43F5 (void);
// 0x00000355 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__AudioDeviceModule::.ctor()
extern void SWIGTYPE_p_webrtc__AudioDeviceModule__ctor_mEB23E005370889478D0AA7F66DAAD61DC733455D (void);
// 0x00000356 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_webrtc__AudioDeviceModule::getCPtr(WebRtcCSharp.SWIGTYPE_p_webrtc__AudioDeviceModule)
extern void SWIGTYPE_p_webrtc__AudioDeviceModule_getCPtr_m6E33D964D374D469C8B49639C122AEC16FD2BF6E (void);
// 0x00000357 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__I420Buffer::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_webrtc__I420Buffer__ctor_mE82E3F5DB877689F5A332F9956B2372475C4440F (void);
// 0x00000358 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__I420Buffer::.ctor()
extern void SWIGTYPE_p_webrtc__I420Buffer__ctor_m13ED4C310BB7379F135D053A57EE9B433D9C2D02 (void);
// 0x00000359 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_webrtc__I420Buffer::getCPtr(WebRtcCSharp.SWIGTYPE_p_webrtc__I420Buffer)
extern void SWIGTYPE_p_webrtc__I420Buffer_getCPtr_m575FF16E90C3E5E233CD6309573A5EFB7D03D8C1 (void);
// 0x0000035A System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__IceCandidateInterface::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_webrtc__IceCandidateInterface__ctor_m7F65CADCBDE87D258E321EF3AA52D01574EA58EC (void);
// 0x0000035B System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__IceCandidateInterface::.ctor()
extern void SWIGTYPE_p_webrtc__IceCandidateInterface__ctor_mF768784D4882CCF54101662F6D1B803AF518BCB2 (void);
// 0x0000035C System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_webrtc__IceCandidateInterface::getCPtr(WebRtcCSharp.SWIGTYPE_p_webrtc__IceCandidateInterface)
extern void SWIGTYPE_p_webrtc__IceCandidateInterface_getCPtr_m3784EBA98DE4E6038A64F55E5581FD1D026715A5 (void);
// 0x0000035D System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__SessionDescriptionInterface::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_webrtc__SessionDescriptionInterface__ctor_m94E247126D623BC8EF75B631F348BF08E4096F37 (void);
// 0x0000035E System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__SessionDescriptionInterface::.ctor()
extern void SWIGTYPE_p_webrtc__SessionDescriptionInterface__ctor_m32A43B30677861C0FE45B8A6B9120F7168F1FF5D (void);
// 0x0000035F System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_webrtc__SessionDescriptionInterface::getCPtr(WebRtcCSharp.SWIGTYPE_p_webrtc__SessionDescriptionInterface)
extern void SWIGTYPE_p_webrtc__SessionDescriptionInterface_getCPtr_m2BAF39254C361B11C7B342E4D36D721AC43B9F67 (void);
// 0x00000360 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__StatsObserver::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_webrtc__StatsObserver__ctor_m86166867C08CD750F77858B510295EEB2F1FA162 (void);
// 0x00000361 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__StatsObserver::.ctor()
extern void SWIGTYPE_p_webrtc__StatsObserver__ctor_mECAF37CAB1FB2F021F3B5EF407FA213E651B4351 (void);
// 0x00000362 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_webrtc__StatsObserver::getCPtr(WebRtcCSharp.SWIGTYPE_p_webrtc__StatsObserver)
extern void SWIGTYPE_p_webrtc__StatsObserver_getCPtr_m49C5FE2D0831EE1A85872C1ED2D89D9625D5067F (void);
// 0x00000363 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__TurnCustomizer::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_webrtc__TurnCustomizer__ctor_m3283D23C430B8473F11D0CA80BE0118CC890DE4C (void);
// 0x00000364 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__TurnCustomizer::.ctor()
extern void SWIGTYPE_p_webrtc__TurnCustomizer__ctor_m1D1C94B86305569016AFE43F941CC60B14D71987 (void);
// 0x00000365 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_webrtc__TurnCustomizer::getCPtr(WebRtcCSharp.SWIGTYPE_p_webrtc__TurnCustomizer)
extern void SWIGTYPE_p_webrtc__TurnCustomizer_getCPtr_mFEA3EC195F9256D4BF1890FC294F084DB7F9539A (void);
// 0x00000366 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__VideoRotation::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_webrtc__VideoRotation__ctor_m702EC747BD4954716FC8329A0FB524AD7BAA5508 (void);
// 0x00000367 System.Void WebRtcCSharp.SWIGTYPE_p_webrtc__VideoRotation::.ctor()
extern void SWIGTYPE_p_webrtc__VideoRotation__ctor_m4AD927E4BB348435244D1E13CB20D14F8BCB8D9F (void);
// 0x00000368 System.Runtime.InteropServices.HandleRef WebRtcCSharp.SWIGTYPE_p_webrtc__VideoRotation::getCPtr(WebRtcCSharp.SWIGTYPE_p_webrtc__VideoRotation)
extern void SWIGTYPE_p_webrtc__VideoRotation_getCPtr_m209C7EBCFC578DB74F97B98A32EB7F0000152538 (void);
// 0x00000369 System.Void WebRtcCSharp.StringVector::.ctor(System.IntPtr,System.Boolean)
extern void StringVector__ctor_mB044E3E7A76C2CBA36B056C40C23E5ACA4A17D1E (void);
// 0x0000036A System.Runtime.InteropServices.HandleRef WebRtcCSharp.StringVector::getCPtr(WebRtcCSharp.StringVector)
extern void StringVector_getCPtr_m647ED4F6F4CBD803435F0096BBC487F29855D5F5 (void);
// 0x0000036B System.Void WebRtcCSharp.StringVector::Finalize()
extern void StringVector_Finalize_mF2D790D5BAEB3C1E7728E048D4CA94331A634B20 (void);
// 0x0000036C System.Void WebRtcCSharp.StringVector::Dispose()
extern void StringVector_Dispose_m46BD32E86491C0D7476A5E81E9523C0C7973D25D (void);
// 0x0000036D System.Void WebRtcCSharp.StringVector::.ctor(System.Collections.ICollection)
extern void StringVector__ctor_mAE07E9631DADCC755E686077681BB4627638DA88 (void);
// 0x0000036E System.Boolean WebRtcCSharp.StringVector::get_IsFixedSize()
extern void StringVector_get_IsFixedSize_mF12150CE5AA10A362B8EA31D39903C848FEF06C3 (void);
// 0x0000036F System.Boolean WebRtcCSharp.StringVector::get_IsReadOnly()
extern void StringVector_get_IsReadOnly_mFC5A9D830ACEFEBF8B677AC74778F49D3374F6FC (void);
// 0x00000370 System.String WebRtcCSharp.StringVector::get_Item(System.Int32)
extern void StringVector_get_Item_mD61AB93D19ACA6CF003404D996AE0085D03A8710 (void);
// 0x00000371 System.Void WebRtcCSharp.StringVector::set_Item(System.Int32,System.String)
extern void StringVector_set_Item_m6B9D0B694A83C7ADB3F6BA5CEE1FE472CB6F392F (void);
// 0x00000372 System.Int32 WebRtcCSharp.StringVector::get_Capacity()
extern void StringVector_get_Capacity_m25A8C7CE7385DB5ADA2CFEA479933C3D53C322C3 (void);
// 0x00000373 System.Void WebRtcCSharp.StringVector::set_Capacity(System.Int32)
extern void StringVector_set_Capacity_m0AFE20CF7BAE50BDFC868DEC780D882BAFEC9EA7 (void);
// 0x00000374 System.Int32 WebRtcCSharp.StringVector::get_Count()
extern void StringVector_get_Count_m8A00F2553FF5AB793C23DBA0A69837DE5D04148D (void);
// 0x00000375 System.Boolean WebRtcCSharp.StringVector::get_IsSynchronized()
extern void StringVector_get_IsSynchronized_mFF8DEA5123DAD14B7EEAE7F3051E5EA30A304FB4 (void);
// 0x00000376 System.Void WebRtcCSharp.StringVector::CopyTo(System.String[])
extern void StringVector_CopyTo_m5C6891053496E69BEC2B0407D021D29BAC7ED368 (void);
// 0x00000377 System.Void WebRtcCSharp.StringVector::CopyTo(System.String[],System.Int32)
extern void StringVector_CopyTo_m0CE07652F2D760AF6C4844F1E94DFB3FE84B37BE (void);
// 0x00000378 System.Void WebRtcCSharp.StringVector::CopyTo(System.Int32,System.String[],System.Int32,System.Int32)
extern void StringVector_CopyTo_m2A9BB98E5EC87CA20501B40A8BC6CB2729D518EB (void);
// 0x00000379 System.Collections.Generic.IEnumerator`1<System.String> WebRtcCSharp.StringVector::global::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void StringVector_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m22CAD77E5556E6E8925DF3CC22BE4A8B1332CA30 (void);
// 0x0000037A System.Collections.IEnumerator WebRtcCSharp.StringVector::global::System.Collections.IEnumerable.GetEnumerator()
extern void StringVector_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mF5DBAEEA9E5D91B007821C2B222AECA1478B6655 (void);
// 0x0000037B WebRtcCSharp.StringVector/StringVectorEnumerator WebRtcCSharp.StringVector::GetEnumerator()
extern void StringVector_GetEnumerator_mCC5816AAF97846CA4CCA17639AB53722E8906656 (void);
// 0x0000037C System.Void WebRtcCSharp.StringVector::Clear()
extern void StringVector_Clear_m3B553DDC7451E44A0378DA37D4B984A052CB14E8 (void);
// 0x0000037D System.Void WebRtcCSharp.StringVector::Add(System.String)
extern void StringVector_Add_mCD2DFAA0ECAB9FE63BECDB8FC26EF1AAB74E400F (void);
// 0x0000037E System.UInt32 WebRtcCSharp.StringVector::size()
extern void StringVector_size_m8443AFDDED368738C7F8C9D2DD9A52661C5577E5 (void);
// 0x0000037F System.UInt32 WebRtcCSharp.StringVector::capacity()
extern void StringVector_capacity_mEF5027E7F73CB6D370017E39E1321504D8D79450 (void);
// 0x00000380 System.Void WebRtcCSharp.StringVector::reserve(System.UInt32)
extern void StringVector_reserve_mEC60DB42F62AB46765ACCB8C8772AB3F94EA4689 (void);
// 0x00000381 System.Void WebRtcCSharp.StringVector::.ctor()
extern void StringVector__ctor_mEAA56D42F5D7CDD4440D56517FB53978430913BF (void);
// 0x00000382 System.Void WebRtcCSharp.StringVector::.ctor(WebRtcCSharp.StringVector)
extern void StringVector__ctor_mA561B1608E05D4EC39DF314300A27E41A24861AF (void);
// 0x00000383 System.Void WebRtcCSharp.StringVector::.ctor(System.Int32)
extern void StringVector__ctor_m0AFC9A3F9C196D96BCA3C848F78AD497C4FAFECE (void);
// 0x00000384 System.String WebRtcCSharp.StringVector::getitemcopy(System.Int32)
extern void StringVector_getitemcopy_mD8AD2E42C8F34376D36D8380EB79FBE61E9CA0E9 (void);
// 0x00000385 System.String WebRtcCSharp.StringVector::getitem(System.Int32)
extern void StringVector_getitem_m07E78008F6FF8DAB46D6CCC261075E924B19067A (void);
// 0x00000386 System.Void WebRtcCSharp.StringVector::setitem(System.Int32,System.String)
extern void StringVector_setitem_m7DC29A202B2F36D7449BC635DD6229F12DAABCB9 (void);
// 0x00000387 System.Void WebRtcCSharp.StringVector::AddRange(WebRtcCSharp.StringVector)
extern void StringVector_AddRange_mF4223F95FE7F8C2F1D81EBA2073C1A1E07102F9B (void);
// 0x00000388 WebRtcCSharp.StringVector WebRtcCSharp.StringVector::GetRange(System.Int32,System.Int32)
extern void StringVector_GetRange_m57E9C6B6F65A48BA67054011078DF3C153B95A8F (void);
// 0x00000389 System.Void WebRtcCSharp.StringVector::Insert(System.Int32,System.String)
extern void StringVector_Insert_mF858179020281D07DDE2AF0B629645FC38B605F5 (void);
// 0x0000038A System.Void WebRtcCSharp.StringVector::InsertRange(System.Int32,WebRtcCSharp.StringVector)
extern void StringVector_InsertRange_m8B5376012296EDFEF9364AC3D8598D599DB528D9 (void);
// 0x0000038B System.Void WebRtcCSharp.StringVector::RemoveAt(System.Int32)
extern void StringVector_RemoveAt_m833045A12706B1F643DA8511F754C7E142158128 (void);
// 0x0000038C System.Void WebRtcCSharp.StringVector::RemoveRange(System.Int32,System.Int32)
extern void StringVector_RemoveRange_m66E1E1E6AD71916514F4F64AD6B2053435B8E0C1 (void);
// 0x0000038D WebRtcCSharp.StringVector WebRtcCSharp.StringVector::Repeat(System.String,System.Int32)
extern void StringVector_Repeat_m1A3C64E5D31538A039F8BC3ED50A02CF621AB8A6 (void);
// 0x0000038E System.Void WebRtcCSharp.StringVector::Reverse()
extern void StringVector_Reverse_m7A7A5926A5C9D5F4F90BD3C611358D36653E2DBF (void);
// 0x0000038F System.Void WebRtcCSharp.StringVector::Reverse(System.Int32,System.Int32)
extern void StringVector_Reverse_m71A5F75B13F471537647683CC81BAA5407E7D522 (void);
// 0x00000390 System.Void WebRtcCSharp.StringVector::SetRange(System.Int32,WebRtcCSharp.StringVector)
extern void StringVector_SetRange_m53404232DDE551EE8DD8D86CAF9422D2DDE41F4C (void);
// 0x00000391 System.Boolean WebRtcCSharp.StringVector::Contains(System.String)
extern void StringVector_Contains_mB231F52B5E1964B193026926A08D422DD4695BC2 (void);
// 0x00000392 System.Int32 WebRtcCSharp.StringVector::IndexOf(System.String)
extern void StringVector_IndexOf_mA74D9345DB8679591C3D560FA5616A21B77766EC (void);
// 0x00000393 System.Int32 WebRtcCSharp.StringVector::LastIndexOf(System.String)
extern void StringVector_LastIndexOf_m72860A469A7D414036956826203DDED86218BFFE (void);
// 0x00000394 System.Boolean WebRtcCSharp.StringVector::Remove(System.String)
extern void StringVector_Remove_m8EC3D3733E983BDB6B672538210350BF8069AB20 (void);
// 0x00000395 System.Void WebRtcCSharp.StringVector/StringVectorEnumerator::.ctor(WebRtcCSharp.StringVector)
extern void StringVectorEnumerator__ctor_mE47C0DFCBFA6322348B468D00427DDD23510B218 (void);
// 0x00000396 System.String WebRtcCSharp.StringVector/StringVectorEnumerator::get_Current()
extern void StringVectorEnumerator_get_Current_mB7C1982335B4B7743A590DCFE86685FD49E51C5A (void);
// 0x00000397 System.Object WebRtcCSharp.StringVector/StringVectorEnumerator::global::System.Collections.IEnumerator.get_Current()
extern void StringVectorEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mE11C8E9FE11D83B0B43119B01A693738690556BD (void);
// 0x00000398 System.Boolean WebRtcCSharp.StringVector/StringVectorEnumerator::MoveNext()
extern void StringVectorEnumerator_MoveNext_mF03A2ECA82AFA46F92805CE9D33B932DB3516D7A (void);
// 0x00000399 System.Void WebRtcCSharp.StringVector/StringVectorEnumerator::Reset()
extern void StringVectorEnumerator_Reset_m0637EECA94516C16AE01F3866784AEDEE05BE169 (void);
// 0x0000039A System.Void WebRtcCSharp.StringVector/StringVectorEnumerator::Dispose()
extern void StringVectorEnumerator_Dispose_m1EE00336F8E3D571134D741C555338A1546E0A65 (void);
// 0x0000039B System.Void WebRtcCSharp.TestsInternal::.ctor(System.IntPtr,System.Boolean)
extern void TestsInternal__ctor_mD9D0D041035D14C2A3CD13A51C7AE06736D3C75B (void);
// 0x0000039C System.Runtime.InteropServices.HandleRef WebRtcCSharp.TestsInternal::getCPtr(WebRtcCSharp.TestsInternal)
extern void TestsInternal_getCPtr_m3264DD0840679C2054DA559AB9B6C1CB89C99879 (void);
// 0x0000039D System.Void WebRtcCSharp.TestsInternal::Finalize()
extern void TestsInternal_Finalize_m363ED8A0251B7E26879DDFE71D1658978AD1F919 (void);
// 0x0000039E System.Void WebRtcCSharp.TestsInternal::Dispose()
extern void TestsInternal_Dispose_m06EB8BBE3F5BDFA0BAA091290628023BE7C8D407 (void);
// 0x0000039F System.Int32 WebRtcCSharp.TestsInternal::PollingDataChannelTest()
extern void TestsInternal_PollingDataChannelTest_m6E69B8FF3213E7075EDA95BADBC108A5142BDB01 (void);
// 0x000003A0 System.Int32 WebRtcCSharp.TestsInternal::PollingVideoTest()
extern void TestsInternal_PollingVideoTest_m9D134E22BAC0DBAAFF1BAA64FE54CF5615663376 (void);
// 0x000003A1 System.Void WebRtcCSharp.TestsInternal::.ctor()
extern void TestsInternal__ctor_m12F3895B2205D69232A07B654EDF14ACE44AF24F (void);
// 0x000003A2 System.Void WebRtcCSharp.VideoFormat::.ctor(System.IntPtr,System.Boolean)
extern void VideoFormat__ctor_mB8C3DD92A0FE815F387EFA6E612CB14886951C51 (void);
// 0x000003A3 System.Runtime.InteropServices.HandleRef WebRtcCSharp.VideoFormat::getCPtr(WebRtcCSharp.VideoFormat)
extern void VideoFormat_getCPtr_mFAC5AF61879F8D437382228D3848F94DE0907CB5 (void);
// 0x000003A4 System.Void WebRtcCSharp.VideoFormat::Finalize()
extern void VideoFormat_Finalize_mFBC66E31490DC7E0D665CB93FBB4575D14EA43F5 (void);
// 0x000003A5 System.Void WebRtcCSharp.VideoFormat::Dispose()
extern void VideoFormat_Dispose_m638DF4D3EED9D10F5636B8A9ED8C19C55218F58E (void);
// 0x000003A6 System.Void WebRtcCSharp.VideoFormat::.ctor()
extern void VideoFormat__ctor_m2F3E9D062685D4C40A04150A960761DB5B45DCC1 (void);
// 0x000003A7 System.Void WebRtcCSharp.VideoFormat::.ctor(System.Int32,System.Int32,System.Int64,System.UInt32)
extern void VideoFormat__ctor_m676CD62CC7719E28333B564C784935A4EBE2B9F1 (void);
// 0x000003A8 System.Void WebRtcCSharp.VideoFormat::.ctor(WebRtcCSharp.VideoFormatPod)
extern void VideoFormat__ctor_mE916BC18C8D2D06D3DC168F95DE3DDB3DD8BA1F6 (void);
// 0x000003A9 System.Void WebRtcCSharp.VideoFormat::Construct(System.Int32,System.Int32,System.Int64,System.UInt32)
extern void VideoFormat_Construct_m3C26F75F18256020EA1C89470B18C1EDCC5EA016 (void);
// 0x000003AA System.Int64 WebRtcCSharp.VideoFormat::FpsToInterval(System.Int32)
extern void VideoFormat_FpsToInterval_m6B030F15A2D82C6632889D2BBC6A4DDCF6EE2A84 (void);
// 0x000003AB System.Int32 WebRtcCSharp.VideoFormat::IntervalToFps(System.Int64)
extern void VideoFormat_IntervalToFps_mE56AF6D2E5726AA2B92BD1287C5701EF75B2DA3B (void);
// 0x000003AC System.Single WebRtcCSharp.VideoFormat::IntervalToFpsFloat(System.Int64)
extern void VideoFormat_IntervalToFpsFloat_mB9EB5F6143E029C067081D136B054058C5A95B62 (void);
// 0x000003AD System.Int32 WebRtcCSharp.VideoFormat::framerate()
extern void VideoFormat_framerate_mD77EDFA6152C4334CE101E038C1B441E1925A63E (void);
// 0x000003AE System.Boolean WebRtcCSharp.VideoFormat::IsSize0x0()
extern void VideoFormat_IsSize0x0_mFEAA776C567EB44B7747A1048562823B02B0A7DD (void);
// 0x000003AF System.Boolean WebRtcCSharp.VideoFormat::IsPixelRateLess(WebRtcCSharp.VideoFormat)
extern void VideoFormat_IsPixelRateLess_m73AC0E70BA83046333DAE622E54FFD8277846BB8 (void);
// 0x000003B0 System.String WebRtcCSharp.VideoFormat::ToString()
extern void VideoFormat_ToString_m4DF3F23FD9CD96052AE772F1C2B039DF2D0E01D7 (void);
// 0x000003B1 System.Void WebRtcCSharp.VideoFormat::.cctor()
extern void VideoFormat__cctor_m937AAA6DF8FB28F3C12F2CFA338E59DB3F0807BA (void);
// 0x000003B2 System.Void WebRtcCSharp.VideoFormatPod::.ctor(System.IntPtr,System.Boolean)
extern void VideoFormatPod__ctor_mD718A721E63DD6AB8C6FA630696CF5526D07BD24 (void);
// 0x000003B3 System.Runtime.InteropServices.HandleRef WebRtcCSharp.VideoFormatPod::getCPtr(WebRtcCSharp.VideoFormatPod)
extern void VideoFormatPod_getCPtr_m8EBA6D68630CC211E93807A8C3731B8D053E03CD (void);
// 0x000003B4 System.Void WebRtcCSharp.VideoFormatPod::Finalize()
extern void VideoFormatPod_Finalize_m7B223C4CDFDB3211B01E85FF0A5F9A81A8627DFD (void);
// 0x000003B5 System.Void WebRtcCSharp.VideoFormatPod::Dispose()
extern void VideoFormatPod_Dispose_m8C48FA6656E2E058A5F229EB9B6BC8A3FCFE1BC8 (void);
// 0x000003B6 System.Void WebRtcCSharp.VideoFormatPod::set_width(System.Int32)
extern void VideoFormatPod_set_width_m14661118301690F6E87B6A0C6727B5F934BC0ACF (void);
// 0x000003B7 System.Int32 WebRtcCSharp.VideoFormatPod::get_width()
extern void VideoFormatPod_get_width_mFA0A3E5304C5E1748B117A76ACEFD72595C7BBE8 (void);
// 0x000003B8 System.Void WebRtcCSharp.VideoFormatPod::set_height(System.Int32)
extern void VideoFormatPod_set_height_mCA84618CBD96FA8BF8DE4250C621B47891D13667 (void);
// 0x000003B9 System.Int32 WebRtcCSharp.VideoFormatPod::get_height()
extern void VideoFormatPod_get_height_mF2C4E93023F9B74F7BCA814A048E3C638BAF00E5 (void);
// 0x000003BA System.Void WebRtcCSharp.VideoFormatPod::set_interval(System.Int64)
extern void VideoFormatPod_set_interval_mC19D641C1B656580B468390151EFF0439518EF31 (void);
// 0x000003BB System.Int64 WebRtcCSharp.VideoFormatPod::get_interval()
extern void VideoFormatPod_get_interval_mDAE00D809BAF3D32A4A6F099C36A162A452AAC6D (void);
// 0x000003BC System.Void WebRtcCSharp.VideoFormatPod::set_fourcc(System.UInt32)
extern void VideoFormatPod_set_fourcc_m3B5FB21626C4C1352D706AE1481C0107740C780C (void);
// 0x000003BD System.UInt32 WebRtcCSharp.VideoFormatPod::get_fourcc()
extern void VideoFormatPod_get_fourcc_m47828DC183B2F3E50DEDE114933F848E8B0D7FA1 (void);
// 0x000003BE System.Void WebRtcCSharp.VideoFormatPod::.ctor()
extern void VideoFormatPod__ctor_mEB43A19EA077C1948D087A402B5429ED937273B0 (void);
// 0x000003BF System.Void WebRtcCSharp.VideoInput::.ctor(System.IntPtr,System.Boolean)
extern void VideoInput__ctor_mCA616A150EA7E0A36D64A417576C3F304689AB6B (void);
// 0x000003C0 System.Runtime.InteropServices.HandleRef WebRtcCSharp.VideoInput::getCPtr(WebRtcCSharp.VideoInput)
extern void VideoInput_getCPtr_m31193C73059292333813D2CEC4F9BA06F907883D (void);
// 0x000003C1 System.Void WebRtcCSharp.VideoInput::Finalize()
extern void VideoInput_Finalize_m4AB629FB98709CC82B73D982F5CD7E962C3F0DFD (void);
// 0x000003C2 System.Void WebRtcCSharp.VideoInput::Dispose()
extern void VideoInput_Dispose_m484CD43EFDA80596925144A39D1877AAB5C1614E (void);
// 0x000003C3 System.Void WebRtcCSharp.VideoInput::AddDevice(System.String,System.Int32,System.Int32,System.Int32)
extern void VideoInput_AddDevice_mA117BB2F642450ADF7CF74E6A8598702859612CF (void);
// 0x000003C4 System.Boolean WebRtcCSharp.VideoInput::UpdateFrame(System.String,System.Byte[],System.Int32,System.Int32,System.Int32,WebRtcCSharp.VideoType,System.Int32,System.Boolean)
extern void VideoInput_UpdateFrame_m387FC0FCCB532A7AFCAE299FA91B68D72CF32DE6 (void);
// 0x000003C5 System.Boolean WebRtcCSharp.VideoInput::UpdateFrame2(System.String,System.IntPtr,System.Int32,System.Int32,System.Int32,WebRtcCSharp.VideoType,System.Int32,System.Boolean)
extern void VideoInput_UpdateFrame2_mDC8354980F848DC74BAEFCC4ED4D1976844CDD9D (void);
// 0x000003C6 System.Void WebRtcCSharp.VideoInput::RemoveDevice(System.String)
extern void VideoInput_RemoveDevice_mA44338C78EC26BB29E5019DA8B01C7B5C7464E44 (void);
// 0x000003C7 System.Void WebRtcCSharp.VideoInputRef::.ctor(System.IntPtr,System.Boolean)
extern void VideoInputRef__ctor_mEFB506E8B6E401C52BE1A724FF5E192C68BA58A0 (void);
// 0x000003C8 System.Runtime.InteropServices.HandleRef WebRtcCSharp.VideoInputRef::getCPtr(WebRtcCSharp.VideoInputRef)
extern void VideoInputRef_getCPtr_m215E3ADE6803E233DA2FCE33A371933191DACEA8 (void);
// 0x000003C9 System.Void WebRtcCSharp.VideoInputRef::Finalize()
extern void VideoInputRef_Finalize_m766E8100CAAAA76943842C64E327F020C452212B (void);
// 0x000003CA System.Void WebRtcCSharp.VideoInputRef::Dispose()
extern void VideoInputRef_Dispose_mAC81DA901E79A9A4E3DBFD5578A25C4546561EE1 (void);
// 0x000003CB System.Void WebRtcCSharp.VideoInputRef::.ctor()
extern void VideoInputRef__ctor_mAB4ADE9D2C1F7C7C0620F403A805F594922C9318 (void);
// 0x000003CC System.Void WebRtcCSharp.VideoInputRef::.ctor(WebRtcCSharp.VideoInput)
extern void VideoInputRef__ctor_m0549D7E1738859EE48B2C6A6C4553F7800F626F3 (void);
// 0x000003CD System.Void WebRtcCSharp.VideoInputRef::.ctor(WebRtcCSharp.VideoInputRef)
extern void VideoInputRef__ctor_mCCD1BD798E26B3BD0380D130CB05009C82A4CBA5 (void);
// 0x000003CE WebRtcCSharp.VideoInput WebRtcCSharp.VideoInputRef::get()
extern void VideoInputRef_get_m8B17AC094427D90C00C18DBA286655848BA33058 (void);
// 0x000003CF WebRtcCSharp.VideoInput WebRtcCSharp.VideoInputRef::__deref__()
extern void VideoInputRef___deref___m239216813F2A4E1AB7ECF261769EEC4809026D28 (void);
// 0x000003D0 WebRtcCSharp.VideoInput WebRtcCSharp.VideoInputRef::release()
extern void VideoInputRef_release_m5D3A92CB04C67B6C5C685A8F7EFA06C5D0A17ACD (void);
// 0x000003D1 System.Void WebRtcCSharp.VideoInputRef::swap(WebRtcCSharp.SWIGTYPE_p_p_VideoInput)
extern void VideoInputRef_swap_m9ECF7BB61BF4C800F3D55C560618259DC896B7CE (void);
// 0x000003D2 System.Void WebRtcCSharp.VideoInputRef::swap(WebRtcCSharp.VideoInputRef)
extern void VideoInputRef_swap_m7A8FCFA35C45B709803361004F64BEE2C84E11D5 (void);
// 0x000003D3 System.Void WebRtcCSharp.VideoInputRef::AddDevice(System.String,System.Int32,System.Int32,System.Int32)
extern void VideoInputRef_AddDevice_mC8AA44DFF330F70308B14A9A783CEC946FBFCB18 (void);
// 0x000003D4 System.Boolean WebRtcCSharp.VideoInputRef::UpdateFrame(System.String,System.Byte[],System.Int32,System.Int32,System.Int32,WebRtcCSharp.VideoType,System.Int32,System.Boolean)
extern void VideoInputRef_UpdateFrame_mC82C903116DEF38C1A031E8775CAAFD14A338814 (void);
// 0x000003D5 System.Boolean WebRtcCSharp.VideoInputRef::UpdateFrame2(System.String,System.IntPtr,System.Int32,System.Int32,System.Int32,WebRtcCSharp.VideoType,System.Int32,System.Boolean)
extern void VideoInputRef_UpdateFrame2_m81967F03D65297DC5AA793C4214B41BC2E89CB9B (void);
// 0x000003D6 System.Void WebRtcCSharp.VideoInputRef::RemoveDevice(System.String)
extern void VideoInputRef_RemoveDevice_mC58204AF39E214194130A7ECF20D0948EEAB782F (void);
// 0x000003D7 System.Void WebRtcCSharp.VideoInputRef::AddRef()
extern void VideoInputRef_AddRef_m234EED3927078C4E817A1D820C8F304889523F84 (void);
// 0x000003D8 WebRtcCSharp.RefCountReleaseStatus WebRtcCSharp.VideoInputRef::Release()
extern void VideoInputRef_Release_mA2DC18C6B7E8496AB20ED5AB8A2ECDBBEBF06B6F (void);
// 0x000003D9 System.Double WebRtcCSharp.WebRtcSwig::sin(System.Double)
extern void WebRtcSwig_sin_m254F1FB82CA078FD311DD3F9D2479727AC72E7F1 (void);
// 0x000003DA System.Double WebRtcCSharp.WebRtcSwig::get_kPerfectPSNR()
extern void WebRtcSwig_get_kPerfectPSNR_m9DE2D3E25D0C178E2C0C7770CEE7B9F9301C36D6 (void);
// 0x000003DB WebRtcCSharp.VideoType WebRtcCSharp.WebRtcSwig::get_kI420()
extern void WebRtcSwig_get_kI420_mB91B6C7FC4D7193F9847046068DDB3D450B9D6EB (void);
// 0x000003DC System.UInt32 WebRtcCSharp.WebRtcSwig::CalcBufferSize(WebRtcCSharp.VideoType,System.Int32,System.Int32)
extern void WebRtcSwig_CalcBufferSize_mCE4191B04A98CB6D6B40FEFC12BE423D792DBC6E (void);
// 0x000003DD System.Int32 WebRtcCSharp.WebRtcSwig::PrintVideoFrame(WebRtcCSharp.SWIGTYPE_p_VideoFrame,WebRtcCSharp.SWIGTYPE_p_FILE)
extern void WebRtcSwig_PrintVideoFrame_m2108908725E514708BC2C2292A58AC7B24837A6C (void);
// 0x000003DE System.Int32 WebRtcCSharp.WebRtcSwig::PrintVideoFrame(WebRtcCSharp.SWIGTYPE_p_I420BufferInterface,WebRtcCSharp.SWIGTYPE_p_FILE)
extern void WebRtcSwig_PrintVideoFrame_m341EEE7E9526E5CBCD8C97E49834E1974DDAE183 (void);
// 0x000003DF System.Int32 WebRtcCSharp.WebRtcSwig::ExtractBuffer(WebRtcCSharp.SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t,System.UInt32,WebRtcCSharp.SWIGTYPE_p_unsigned_char)
extern void WebRtcSwig_ExtractBuffer_m81E8113D4F2ED583D820A2AA0A1284CF153BF9BA (void);
// 0x000003E0 System.Int32 WebRtcCSharp.WebRtcSwig::ExtractBuffer(WebRtcCSharp.SWIGTYPE_p_VideoFrame,System.UInt32,WebRtcCSharp.SWIGTYPE_p_unsigned_char)
extern void WebRtcSwig_ExtractBuffer_m763FB1F4E719E9C4FEAD0BE9EFF6B02EB3924BC1 (void);
// 0x000003E1 System.Int32 WebRtcCSharp.WebRtcSwig::ConvertFromI420(WebRtcCSharp.SWIGTYPE_p_VideoFrame,WebRtcCSharp.VideoType,System.Int32,WebRtcCSharp.SWIGTYPE_p_unsigned_char)
extern void WebRtcSwig_ConvertFromI420_mA870B4C8D34B57EBECD75B7902D06E8A6FB556BA (void);
// 0x000003E2 System.Double WebRtcCSharp.WebRtcSwig::I420PSNR(WebRtcCSharp.SWIGTYPE_p_VideoFrame,WebRtcCSharp.SWIGTYPE_p_VideoFrame)
extern void WebRtcSwig_I420PSNR_m18CF8F4DA96E92CD64F9DC9345787B2BC92290F2 (void);
// 0x000003E3 System.Double WebRtcCSharp.WebRtcSwig::I420PSNR(WebRtcCSharp.SWIGTYPE_p_I420BufferInterface,WebRtcCSharp.SWIGTYPE_p_I420BufferInterface)
extern void WebRtcSwig_I420PSNR_m59F4505BFE12AEEE1E3FEA24FA8479FCF73BBE83 (void);
// 0x000003E4 System.Double WebRtcCSharp.WebRtcSwig::I420SSIM(WebRtcCSharp.SWIGTYPE_p_VideoFrame,WebRtcCSharp.SWIGTYPE_p_VideoFrame)
extern void WebRtcSwig_I420SSIM_mEC829729DDF4E60058BA5701DECA5723E79E91E4 (void);
// 0x000003E5 System.Double WebRtcCSharp.WebRtcSwig::I420SSIM(WebRtcCSharp.SWIGTYPE_p_I420BufferInterface,WebRtcCSharp.SWIGTYPE_p_I420BufferInterface)
extern void WebRtcSwig_I420SSIM_mC71A8E53F7D7FD098667AD19EB7B3D8E90D136DC (void);
// 0x000003E6 System.Void WebRtcCSharp.WebRtcSwig::NV12Scale(WebRtcCSharp.SWIGTYPE_p_unsigned_char,WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.Int32,WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.Int32,System.Int32,System.Int32,WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.Int32,WebRtcCSharp.SWIGTYPE_p_unsigned_char,System.Int32,System.Int32,System.Int32)
extern void WebRtcSwig_NV12Scale_mC0CF2B9752D17BE1DB6C1C49B47295170EAA5F8D (void);
// 0x000003E7 System.Int32 WebRtcCSharp.WebRtcSwig::ConvertVideoType(WebRtcCSharp.VideoType)
extern void WebRtcSwig_ConvertVideoType_m990A5D4E61E312A35FD5273E4B3379AC61C99DAF (void);
// 0x000003E8 System.UInt32 WebRtcCSharp.WebRtcSwig::get_kDummyVideoSsrc()
extern void WebRtcSwig_get_kDummyVideoSsrc_mCC61684C5E3D3363334A8F1104A5EFA6AD7F7887 (void);
// 0x000003E9 System.UInt32 WebRtcCSharp.WebRtcSwig::get_FOURCC_ANY()
extern void WebRtcSwig_get_FOURCC_ANY_m8095F26F4B5CF6447215B9E01D3F6662EF5D923A (void);
// 0x000003EA System.UInt32 WebRtcCSharp.WebRtcSwig::CanonicalFourCC(System.UInt32)
extern void WebRtcSwig_CanonicalFourCC_mDD657897FE364BD672F800738133EF8EAD5CA42C (void);
// 0x000003EB System.String WebRtcCSharp.WebRtcSwig::GetFourccName(System.UInt32)
extern void WebRtcSwig_GetFourccName_m941A53D81DB3707C4056E9ED01AE1E56D777EA27 (void);
// 0x000003EC System.Int32 WebRtcCSharp.WebRtcSwig::WrapTestConnection_PollingDataChannelTest()
extern void WebRtcSwig_WrapTestConnection_PollingDataChannelTest_m3D09D693F2438400ACF7E818938879E4E8B4082E (void);
// 0x000003ED System.Int32 WebRtcCSharp.WebRtcSwig::WrapTestConnection_PollingVideoTest()
extern void WebRtcSwig_WrapTestConnection_PollingVideoTest_m356411BEE31531D699B9793C3541E453C7129C92 (void);
// 0x000003EE System.Void WebRtcCSharp.WebRtcSwig::.ctor()
extern void WebRtcSwig__ctor_m399F93C7A2C84E13EAC8DEFCCD3ED7B6B7B0CE26 (void);
// 0x000003EF System.Void WebRtcCSharp.WebRtcSwigPINVOKE::.cctor()
extern void WebRtcSwigPINVOKE__cctor_m9F0045712625FC645A04AD308FB46A4B7AA60496 (void);
// 0x000003F0 System.Double WebRtcCSharp.WebRtcSwigPINVOKE::sin(System.Double)
extern void WebRtcSwigPINVOKE_sin_m3515EF1E83AE22078B9EDCAF229936F718045601 (void);
// 0x000003F1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RefCountInterface_AddRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RefCountInterface_AddRef_mF9A1BF0038C4639F0748EE6E2F6588FAF21F50DA (void);
// 0x000003F2 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::RefCountInterface_Release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RefCountInterface_Release_mEAD1E637CE912C6E38A77F87E9EA8D5E932F01D1 (void);
// 0x000003F3 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_CopyOnWriteBuffer__SWIG_0()
extern void WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_0_m633F3F64CD6CF89A4CDEE0F5FFCEEEE466445E4F (void);
// 0x000003F4 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_CopyOnWriteBuffer__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_1_mB4F9DA566F0F31801DB194649096B6C756A947D2 (void);
// 0x000003F5 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_CopyOnWriteBuffer__SWIG_3(System.String)
extern void WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_3_m7FE8E17F1D98F182924F3C8E1C0B8CC6F834A7EA (void);
// 0x000003F6 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_CopyOnWriteBuffer__SWIG_4(System.UInt32)
extern void WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_4_mFD439FCB07219127A35D680ABB7FCF472961EF05 (void);
// 0x000003F7 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_CopyOnWriteBuffer__SWIG_5(System.UInt32,System.UInt32)
extern void WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_5_mD831BA41E48C91CD09C85467DE250595DB4B5BE8 (void);
// 0x000003F8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_CopyOnWriteBuffer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_CopyOnWriteBuffer_mA08087A14EEA8CB65159E00EBE81CB5EE661B6B6 (void);
// 0x000003F9 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::CopyOnWriteBuffer_size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_CopyOnWriteBuffer_size_m9F9F2D9207D5FDC61C641B956230138EADFE679E (void);
// 0x000003FA System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::CopyOnWriteBuffer_capacity(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_CopyOnWriteBuffer_capacity_m506DBB386DE0280DC2869413AAFB4D521AF0FE35 (void);
// 0x000003FB System.Void WebRtcCSharp.WebRtcSwigPINVOKE::CopyOnWriteBuffer_SetData(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_CopyOnWriteBuffer_SetData_mE27690A591DAACDA7400DC4E8625C7B7645F5C0F (void);
// 0x000003FC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::CopyOnWriteBuffer_AppendData(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_CopyOnWriteBuffer_AppendData_m8FC3381AB902C5BFECB5A01F08A8353FFC3ABF0D (void);
// 0x000003FD System.Void WebRtcCSharp.WebRtcSwigPINVOKE::CopyOnWriteBuffer_SetSize(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void WebRtcSwigPINVOKE_CopyOnWriteBuffer_SetSize_m57ED93A498AB6BB25C2C3804445D8DA4F75BC3B3 (void);
// 0x000003FE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::CopyOnWriteBuffer_EnsureCapacity(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void WebRtcSwigPINVOKE_CopyOnWriteBuffer_EnsureCapacity_mB60701102A62F1206EFEAEDFDDC6DDB7A64F6D0D (void);
// 0x000003FF System.Void WebRtcCSharp.WebRtcSwigPINVOKE::CopyOnWriteBuffer_Clear(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_CopyOnWriteBuffer_Clear_m5F70E2C223C4A5A02F33ED859249619EE45D1F7C (void);
// 0x00000400 System.Double WebRtcCSharp.WebRtcSwigPINVOKE::kPerfectPSNR_get()
extern void WebRtcSwigPINVOKE_kPerfectPSNR_get_mED88504BF7DA0C12AA6B41CA4D1D4323C8BC61A8 (void);
// 0x00000401 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::kI420_get()
extern void WebRtcSwigPINVOKE_kI420_get_mCAC4F3A58DB1AB018BB20516478380D8201A3665 (void);
// 0x00000402 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::CalcBufferSize(System.Int32,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_CalcBufferSize_m6930C6660168AAB58B73E310B6E0C31C6A8F1245 (void);
// 0x00000403 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PrintVideoFrame__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PrintVideoFrame__SWIG_0_m9F44D5876C3907B8DEB94EFA1DF594FFA596B694 (void);
// 0x00000404 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PrintVideoFrame__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PrintVideoFrame__SWIG_1_m301F12E5434222E23648CAF45CE551669544DC14 (void);
// 0x00000405 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::ExtractBuffer__SWIG_0(System.Runtime.InteropServices.HandleRef,System.UInt32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_ExtractBuffer__SWIG_0_m78C91601CBC4DD33047E666D3969CE301925E0BA (void);
// 0x00000406 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::ExtractBuffer__SWIG_1(System.Runtime.InteropServices.HandleRef,System.UInt32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_ExtractBuffer__SWIG_1_mA6EEE9BAFF64A4140F3CD6C945BE744E3196E04A (void);
// 0x00000407 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::ConvertFromI420(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_ConvertFromI420_m9D62757A7A84B6E6A8EC30D9368E8307644481F8 (void);
// 0x00000408 System.Double WebRtcCSharp.WebRtcSwigPINVOKE::I420PSNR__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_I420PSNR__SWIG_0_m9A4AE6AF5A4E720AFDDF898BE70A7FF8491B9446 (void);
// 0x00000409 System.Double WebRtcCSharp.WebRtcSwigPINVOKE::I420PSNR__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_I420PSNR__SWIG_1_mA673D776A7BA48ABBBFF70A2D7275A30AFE654A0 (void);
// 0x0000040A System.Double WebRtcCSharp.WebRtcSwigPINVOKE::I420SSIM__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_I420SSIM__SWIG_0_m229178CA1331AF33CE656FE8106DC7C64BA6A4AC (void);
// 0x0000040B System.Double WebRtcCSharp.WebRtcSwigPINVOKE::I420SSIM__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_I420SSIM__SWIG_1_m4C8484BE609E3D3C96795D40D1363233C2DDAB01 (void);
// 0x0000040C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::NV12Scale(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32,System.Int32,System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_NV12Scale_m2D44E9DC31105C20FA760AC4F8A7FF1F6756FEED (void);
// 0x0000040D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_NV12ToI420Scaler()
extern void WebRtcSwigPINVOKE_new_NV12ToI420Scaler_m6E7BC4A5D6F2E0FEA0129C51F024441A0FFC30A2 (void);
// 0x0000040E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_NV12ToI420Scaler(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_NV12ToI420Scaler_mEC9F36A842EE18558A764AFCE438F934B989D7F3 (void);
// 0x0000040F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::NV12ToI420Scaler_NV12ToI420Scale(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32,System.Int32,System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_NV12ToI420Scaler_NV12ToI420Scale_m9775DCCC1A2AE0BA85D31CC1AB78272D9ED363ED (void);
// 0x00000410 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::ConvertVideoType(System.Int32)
extern void WebRtcSwigPINVOKE_ConvertVideoType_m5D3961E4BF492A4FAFEF29A015F6BDA128A00CEB (void);
// 0x00000411 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::kDummyVideoSsrc_get()
extern void WebRtcSwigPINVOKE_kDummyVideoSsrc_get_mE45C4A172B3C144802E445CEB6BDC9E2D0B5424D (void);
// 0x00000412 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::FOURCC_ANY_get()
extern void WebRtcSwigPINVOKE_FOURCC_ANY_get_m3863D46F637FFB36C15256BC2675F3EF3248E4D6 (void);
// 0x00000413 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::CanonicalFourCC(System.UInt32)
extern void WebRtcSwigPINVOKE_CanonicalFourCC_m7F3C5F285AA25E317A5E1F9FD3EADBB63559A7FF (void);
// 0x00000414 System.String WebRtcCSharp.WebRtcSwigPINVOKE::GetFourccName(System.UInt32)
extern void WebRtcSwigPINVOKE_GetFourccName_m26078498C29E773587AAAC98DF32AE0F6FA49C74 (void);
// 0x00000415 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormatPod_width_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_VideoFormatPod_width_set_m02112977470F4573EC15B95DEF8609678523CE25 (void);
// 0x00000416 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormatPod_width_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoFormatPod_width_get_mF1B220626E08AD42BAAB4523BDCB4D6838219AB8 (void);
// 0x00000417 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormatPod_height_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_VideoFormatPod_height_set_m0132D52160571B134AEB3F14E610AB3C03940A27 (void);
// 0x00000418 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormatPod_height_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoFormatPod_height_get_mC95F40A175CB3D46C671F6273EF37F9C89719758 (void);
// 0x00000419 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormatPod_interval_set(System.Runtime.InteropServices.HandleRef,System.Int64)
extern void WebRtcSwigPINVOKE_VideoFormatPod_interval_set_m9AA9FC8FC00FAB2EB490BFA4135C961E3F2E295E (void);
// 0x0000041A System.Int64 WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormatPod_interval_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoFormatPod_interval_get_mEE367E79B150C8E85D285025D375B4D586159E61 (void);
// 0x0000041B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormatPod_fourcc_set(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void WebRtcSwigPINVOKE_VideoFormatPod_fourcc_set_m1D90C2B79985324607B54F8DBACCECEB74F86D49 (void);
// 0x0000041C System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormatPod_fourcc_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoFormatPod_fourcc_get_m216A1678C2A927AEEDCF5A38F2385999D6109B7D (void);
// 0x0000041D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_VideoFormatPod()
extern void WebRtcSwigPINVOKE_new_VideoFormatPod_m22F6BBEB8CFBD0744B2BEE5C2B14ADCA51282737 (void);
// 0x0000041E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_VideoFormatPod(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_VideoFormatPod_m0980823C92DF464C0B38A1DCD05EB66C31A6B590 (void);
// 0x0000041F System.Int64 WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_kMinimumInterval_get()
extern void WebRtcSwigPINVOKE_VideoFormat_kMinimumInterval_get_m2D36F06D571E16AB5F454CED101B6BB48321BC02 (void);
// 0x00000420 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_VideoFormat__SWIG_0()
extern void WebRtcSwigPINVOKE_new_VideoFormat__SWIG_0_m2A8B376454CAB6AE2DCE7D8ACB3751667A51A12C (void);
// 0x00000421 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_VideoFormat__SWIG_1(System.Int32,System.Int32,System.Int64,System.UInt32)
extern void WebRtcSwigPINVOKE_new_VideoFormat__SWIG_1_mD676E4B6529EB6883A91DC05741D371653D3FF79 (void);
// 0x00000422 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_VideoFormat__SWIG_2(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_VideoFormat__SWIG_2_m7250F350110FB8C8728EF188AD6B36E2D5823757 (void);
// 0x00000423 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_Construct(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32,System.Int64,System.UInt32)
extern void WebRtcSwigPINVOKE_VideoFormat_Construct_m776DD132F4D536B536BD03B666B60BD6DFA60B32 (void);
// 0x00000424 System.Int64 WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_FpsToInterval(System.Int32)
extern void WebRtcSwigPINVOKE_VideoFormat_FpsToInterval_m4A53D5C8A0356398CA73F6543DBEFA86B9F28359 (void);
// 0x00000425 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_IntervalToFps(System.Int64)
extern void WebRtcSwigPINVOKE_VideoFormat_IntervalToFps_m537BB9A9D68E8C59D6BC573DB7A5AD0D23DBD480 (void);
// 0x00000426 System.Single WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_IntervalToFpsFloat(System.Int64)
extern void WebRtcSwigPINVOKE_VideoFormat_IntervalToFpsFloat_mBD2E7EC642E851CB4FC0A3480D9924D6EB5B9EE3 (void);
// 0x00000427 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_framerate(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoFormat_framerate_m303586D1265F912DF322D20710BF1FC32870AD55 (void);
// 0x00000428 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_IsSize0x0(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoFormat_IsSize0x0_m0D51158D3C3BFA5C5DB87FA625D9A4EA032E9585 (void);
// 0x00000429 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_IsPixelRateLess(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoFormat_IsPixelRateLess_mDA85C16AD3D79D62979B80BE5835171CAB465B19 (void);
// 0x0000042A System.String WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_ToString(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoFormat_ToString_m21CB0422ED82007EF140170D7199AAFB15D8F380 (void);
// 0x0000042B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_VideoFormat(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_VideoFormat_m8EC44AA5FD5109D5E5D274BDFAA08EA7B7B8F08D (void);
// 0x0000042C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_reliable_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_DataChannelInit_reliable_set_mE330356B8BD61FE1CD3166C1285D576FC8854BB5 (void);
// 0x0000042D System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_reliable_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInit_reliable_get_m0AE310D03A21417DD84A7FE2376E91832F2DFAFC (void);
// 0x0000042E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_ordered_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_DataChannelInit_ordered_set_m5AF02C4430BCC4A167EAFA39BB86B68CABD4523A (void);
// 0x0000042F System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_ordered_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInit_ordered_get_mBF5B641DA447F11E67968CCFC743E6F9177E84AF (void);
// 0x00000430 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_maxRetransmitTime_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_DataChannelInit_maxRetransmitTime_set_mBB45927B5EF0A29612C7BF3AF171DE81F1EC46CF (void);
// 0x00000431 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_maxRetransmitTime_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInit_maxRetransmitTime_get_m937680FB786FE1C09F8BF351A09762C8C0C56F1F (void);
// 0x00000432 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_maxRetransmits_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_DataChannelInit_maxRetransmits_set_m2497C7CC3F6A70F93AF6C3CC32AA3A964C1F6AB6 (void);
// 0x00000433 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_maxRetransmits_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInit_maxRetransmits_get_mBEBCF705723190B42BD186687C17DA00149FDC67 (void);
// 0x00000434 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_protocol_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_DataChannelInit_protocol_set_m7C037FDF43EE929F284131F1313D3E8F3EB77EA5 (void);
// 0x00000435 System.String WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_protocol_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInit_protocol_get_m30C0F2FA23CC7C880CDDD97CCD736587FB055AFF (void);
// 0x00000436 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_negotiated_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_DataChannelInit_negotiated_set_m0B23D86F1B3EBEB1E31C649DB8A6B464C8F184FC (void);
// 0x00000437 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_negotiated_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInit_negotiated_get_m473CCF7D9EAE3B7EEE56583BB716BE13AD460321 (void);
// 0x00000438 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_id_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_DataChannelInit_id_set_m01FC6DF7A49B705A56DE43A0D6F008BC3046C398 (void);
// 0x00000439 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInit_id_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInit_id_get_m09ADE6495C22FE15CA0EB830563EF919F9E8EB63 (void);
// 0x0000043A System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_DataChannelInit()
extern void WebRtcSwigPINVOKE_new_DataChannelInit_m01BD8BD2995B23947345D0AC2AE7BEF576ECFB22 (void);
// 0x0000043B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_DataChannelInit(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_DataChannelInit_mF9B1F5DD484DAA610466D9707B85FA3E8D949BD9 (void);
// 0x0000043C System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_DataBuffer__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_new_DataBuffer__SWIG_0_m9BF9E7B5CFB77EF4AEB6E6D80F444BF1F9E6B64B (void);
// 0x0000043D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_DataBuffer__SWIG_1(System.String)
extern void WebRtcSwigPINVOKE_new_DataBuffer__SWIG_1_m6B741C037B87DEDBF4BC6519510BAFEBEEEB91A5 (void);
// 0x0000043E System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::DataBuffer_size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataBuffer_size_mA5AAE4F3E4EDF62346D68B0504D1FCBDAC0A13B7 (void);
// 0x0000043F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataBuffer_data_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataBuffer_data_set_m1958C552A13E3A961527D1F5C1874252E4B979A3 (void);
// 0x00000440 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::DataBuffer_data_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataBuffer_data_get_mC5AFDA4DC47FBB2349CF479D9D1CD748E8BFAB01 (void);
// 0x00000441 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataBuffer_binary_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_DataBuffer_binary_set_m99156CAD23B59A0E8B7B56F6E17CC9A8F5E5E758 (void);
// 0x00000442 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::DataBuffer_binary_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataBuffer_binary_get_mA3B01D6CABEF84A16DFC3853C3B7CE2BB8B1319E (void);
// 0x00000443 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_DataBuffer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_DataBuffer_mFAE81B89BE3960DF366B6810DFA8BDFBBDF64D57 (void);
// 0x00000444 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelObserver_OnStateChange(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelObserver_OnStateChange_m1661FC0F287546E0369266FC0D9C21D1D3FB19ED (void);
// 0x00000445 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelObserver_OnMessage(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelObserver_OnMessage_mB8B1FDB3535136FCA0A1A0272ADD763D54ABE207 (void);
// 0x00000446 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelObserver_OnBufferedAmountChange(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern void WebRtcSwigPINVOKE_DataChannelObserver_OnBufferedAmountChange_m926A616152A843FBA8E91DDAACC51D6BEA9E7C12 (void);
// 0x00000447 System.String WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_DataStateString(System.Int32)
extern void WebRtcSwigPINVOKE_DataChannelInterface_DataStateString_m6CBE775E89961DAE05833A07536BB1EF62F4A19B (void);
// 0x00000448 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_RegisterObserver(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_RegisterObserver_m21D617B705B39773AB5AAF65CBB2DE171E1E1328 (void);
// 0x00000449 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_UnregisterObserver(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_UnregisterObserver_mC63D0680AE1BCCCC908B2D16005897F05DA052ED (void);
// 0x0000044A System.String WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_label(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_label_m58EAC564A343CD0385A04EC5D74ACD6BA6288514 (void);
// 0x0000044B System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_reliable(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_reliable_mFAAA926D67E062844A8E5D17F17D8F05A5CA8B8E (void);
// 0x0000044C System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_ordered(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_ordered_m196F803AB3EB7E8100DF5D4128406BD9636DE485 (void);
// 0x0000044D System.UInt16 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_maxRetransmitTime(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_maxRetransmitTime_m1651C8866060B38305297AE3BC5DE48A0AE4EB98 (void);
// 0x0000044E System.UInt16 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_maxRetransmits(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_maxRetransmits_m15600EB64F07A5477F497760B6CB617AB2F24259 (void);
// 0x0000044F System.String WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_protocol(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_protocol_m3EB67A7D7428CACA203CABC072BBE1B5E9B631CB (void);
// 0x00000450 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_negotiated(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_negotiated_m7CBA9B6CECDF0BA320FC2EAA2346D3B143DCDC82 (void);
// 0x00000451 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_id(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_id_mF542D4D63A5E1E807281E667460C82040EB3507E (void);
// 0x00000452 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_state(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_state_m3FC3F5D8DE178909C1CEC0B23494AEE72C56CA47 (void);
// 0x00000453 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_messages_sent(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_messages_sent_m97B2947E709FAD74D1B8675285A86CDA462136CE (void);
// 0x00000454 System.UInt64 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_bytes_sent(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_bytes_sent_mEBA2B37FF53EFF0CB4705E4B829969713394AFC2 (void);
// 0x00000455 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_messages_received(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_messages_received_mC1AD7C8BB3C5DDF7D8648159D83CB62CAD4A2FAC (void);
// 0x00000456 System.UInt64 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_bytes_received(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_bytes_received_mCB7E077570DE924D90B11D5882A81B7631ECCE60 (void);
// 0x00000457 System.UInt64 WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_buffered_amount(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_buffered_amount_m4F9FC0C653C9F1FF2BD5B34E211177038C580FEF (void);
// 0x00000458 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_Close(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_Close_mDD6436EE590FB5F09DBE7F6CF4AC5307B36F98DC (void);
// 0x00000459 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_Send(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_DataChannelInterface_Send_mA2347F6D43E269CB3148D980BDFDB5C35CD53951 (void);
// 0x0000045A System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Clear(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_StringVector_Clear_m3E4623D52C125A97EAF73E21E02C8196F64555F3 (void);
// 0x0000045B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Add(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_StringVector_Add_m895ACECA491EE3EE904F040CC86FBE6469AC8829 (void);
// 0x0000045C System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_StringVector_size_mB35FEBB84CA8582811CE14B8837188992738A80A (void);
// 0x0000045D System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_capacity(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_StringVector_capacity_m1E1E9F990EBBEC03F9C913BC017160837B6AA36C (void);
// 0x0000045E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_reserve(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void WebRtcSwigPINVOKE_StringVector_reserve_mA68BE8B4092B4F9AF49A3C3529AD45620DBDCD38 (void);
// 0x0000045F System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_StringVector__SWIG_0()
extern void WebRtcSwigPINVOKE_new_StringVector__SWIG_0_m13D65C9FA67F475B1BEC422E695E4335B4DE83F9 (void);
// 0x00000460 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_StringVector__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_StringVector__SWIG_1_m31E9AF47B9ACB72F18490C78C53B30A8B7AA8AC1 (void);
// 0x00000461 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_StringVector__SWIG_2(System.Int32)
extern void WebRtcSwigPINVOKE_new_StringVector__SWIG_2_m8401E4B4E88090FB0B2676CCEF3212F61AC820CB (void);
// 0x00000462 System.String WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_getitemcopy_m1749C9BFAE8CA482BE4880327748B284BBD6B803 (void);
// 0x00000463 System.String WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_getitem_m8E95690F6AEE669B0DF428AD9BF1597EA346EBF4 (void);
// 0x00000464 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.String)
extern void WebRtcSwigPINVOKE_StringVector_setitem_m629F089B217D56293A99C5483F9D87D5321F0643 (void);
// 0x00000465 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_AddRange(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_StringVector_AddRange_m9DC2B48DBC66AFFE24687A6309A2E002B7B241C5 (void);
// 0x00000466 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_GetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_GetRange_mE9E75A19061F106D722D4B1B7F65C5EF2CDF57DE (void);
// 0x00000467 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.String)
extern void WebRtcSwigPINVOKE_StringVector_Insert_m20B05277ECF3FBF61FC7BBCE42CFB1E855074634 (void);
// 0x00000468 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_InsertRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_StringVector_InsertRange_m51162F4F6EAC6ADD91BC072C088BACD50A5A0588 (void);
// 0x00000469 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_RemoveAt_mF4724A7CE9C68D1C73925DB137A83E9D4B725E40 (void);
// 0x0000046A System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_RemoveRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_RemoveRange_m9B87AF2D779F0D743767D092C246A4F1F34F7289 (void);
// 0x0000046B System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Repeat(System.String,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_Repeat_mB2F5C08DCD5E6817CF7AC7D628BA695E1AB0DE5F (void);
// 0x0000046C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Reverse__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_StringVector_Reverse__SWIG_0_mA228381086EE9EB25A891A8AD4B3E870467F3B04 (void);
// 0x0000046D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Reverse__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_StringVector_Reverse__SWIG_1_mEBD3CE7428B68C5A58CB4AB66325EA73A53C5267 (void);
// 0x0000046E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_SetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_StringVector_SetRange_mD1F078BB4200DEE92F512E3D2155C01811414E59 (void);
// 0x0000046F System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Contains(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_StringVector_Contains_m45BDE1C9ED0FA185D7CEC60801AF34E0F6B1A089 (void);
// 0x00000470 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_IndexOf(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_StringVector_IndexOf_m70A56431F19CBD53B48E270B219B966E25F51B5C (void);
// 0x00000471 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_LastIndexOf(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_StringVector_LastIndexOf_m41F443AA4809D1B8CDAD667E7B3CF56F569BA71F (void);
// 0x00000472 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::StringVector_Remove(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_StringVector_Remove_m5544A894058460A42A85B1CEA77731726B84BCFD (void);
// 0x00000473 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_StringVector(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_StringVector_mDCE6A372535A36C51EE6CC2ED2E37785D0B4D7BD (void);
// 0x00000474 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_Clear(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_Clear_m5BBD24E86FB111BB430EA16515D9A92E3C9D14C5 (void);
// 0x00000475 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_Add(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_Add_m06C32CD7B8EA282AF895418D71F56B3F5F38C126 (void);
// 0x00000476 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_size_mD91C0C4DA469ED49119EEFD63472056D8019ED08 (void);
// 0x00000477 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_capacity(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_capacity_mCDF1B3D793AAC7F19850583F0BFB8510B242C49F (void);
// 0x00000478 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_reserve(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void WebRtcSwigPINVOKE_IceServers_reserve_mA8DF3AE1DE0F6EE99486BD07D87963C223A429D2 (void);
// 0x00000479 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_IceServers__SWIG_0()
extern void WebRtcSwigPINVOKE_new_IceServers__SWIG_0_mB7FCD08B9FC21F703329BD8AC5309B430BB9975D (void);
// 0x0000047A System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_IceServers__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_IceServers__SWIG_1_mA3A9D16A5E1A39F90D08883DB64159AB846C5803 (void);
// 0x0000047B System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_IceServers__SWIG_2(System.Int32)
extern void WebRtcSwigPINVOKE_new_IceServers__SWIG_2_m8AA5275CB1D87DE86AD4EDDE015FB62259808D3A (void);
// 0x0000047C System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_IceServers_getitemcopy_m0B1B5133F2F211B0CD8798C8FA27DDB3974C1AC7 (void);
// 0x0000047D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_IceServers_getitem_mE5170A6DF20577B658ACEB18A950E5BF75CED782 (void);
// 0x0000047E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_setitem_m342AA2995EDCE164A66C60141ED98FB7D17F821C (void);
// 0x0000047F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_AddRange(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_AddRange_mF21A4938B01928C632E060D11196EE9F53E4B685 (void);
// 0x00000480 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_GetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_IceServers_GetRange_m7660D5231D8754D122F64B45FC104A85EAB53A4E (void);
// 0x00000481 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_Insert_mC6847A3A89B68F02779F352DC5EB6E42B7627D45 (void);
// 0x00000482 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_InsertRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_InsertRange_m9F2350739851AA57D6EB4A8CA86A42CC24A1EDA9 (void);
// 0x00000483 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_IceServers_RemoveAt_m6369C6A4BEFBB90B020C5F4A3B4E7E9045082D50 (void);
// 0x00000484 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_RemoveRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_IceServers_RemoveRange_mCE18330110B56F66A4D9FF110F6DD1022EC3C173 (void);
// 0x00000485 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_Repeat(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_IceServers_Repeat_m5F21BF385B8CE94CD4EE3EA33B4F3EE364C40B0A (void);
// 0x00000486 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_Reverse__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_Reverse__SWIG_0_mDA437369F53EC8AEEE0841A45908C1DC4A7277EB (void);
// 0x00000487 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_Reverse__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_IceServers_Reverse__SWIG_1_m67211813072719691382C9F0A223FCCB0786F376 (void);
// 0x00000488 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IceServers_SetRange(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_IceServers_SetRange_mFBFF5E3A4424A048A1077A373611B4ADEB997034 (void);
// 0x00000489 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_IceServers(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_IceServers_m15B43CBF2996FD00ABF86A29669B2C4507C81958 (void);
// 0x0000048A System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_IceServer__SWIG_0()
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_IceServer__SWIG_0_mB4F56222DE7C1618D6054E164B121091852EE2E2 (void);
// 0x0000048B System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_IceServer__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_IceServer__SWIG_1_mA130D69D7D9AD1034D3378B10F0810335AF29306 (void);
// 0x0000048C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PeerConnectionInterface_IceServer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PeerConnectionInterface_IceServer_m2EFC26FFC68E7B77BF61B2F608EBB03E36DBA3FC (void);
// 0x0000048D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_uri_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_uri_set_m5A89E9F3AE5A2D843D8B09E300FF4F9A35730B51 (void);
// 0x0000048E System.String WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_uri_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_uri_get_mE06C8FFFC20A5DF58650065C50532B460B1ACED7 (void);
// 0x0000048F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_urls_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_urls_set_mFEB4F5BEB4D885093BB2A18BF17EE23E92BA7505 (void);
// 0x00000490 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_urls_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_urls_get_m7759EDDC38C2792916B4FD4DF98FBD94D072BBA8 (void);
// 0x00000491 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_username_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_username_set_mE113888294AAAE2B301CE941A23012BEFE7B2038 (void);
// 0x00000492 System.String WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_username_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_username_get_m78A821FDD021B0BF957FF2DCF21450AA5EC3C04F (void);
// 0x00000493 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_password_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_password_set_m0435113B6F9CCBCD807C01C7CC15DEDE4F22BB19 (void);
// 0x00000494 System.String WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_password_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_password_get_mB72062ED34313D6F9FC6CFE6B36C045D2F243044 (void);
// 0x00000495 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_tls_cert_policy_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_cert_policy_set_mDACCBA95CAF87E12ACD17C5B8D2E2A9DCD275F12 (void);
// 0x00000496 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_tls_cert_policy_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_cert_policy_get_m55EDCB3D63F3CB5A9561DA3A95E4F18339F95A07 (void);
// 0x00000497 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_hostname_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_hostname_set_m9594B1C7F5ACC5996CA5145F3A3C877042A951DE (void);
// 0x00000498 System.String WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_hostname_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_hostname_get_m4DC776B95D107B6EEFD8FE4C714A85B6F8ACFAC2 (void);
// 0x00000499 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_tls_alpn_protocols_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_alpn_protocols_set_m8856C5900D6083A23116DCFFD1433774553A9A8E (void);
// 0x0000049A System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_tls_alpn_protocols_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_alpn_protocols_get_mB3039A4CFD51C0A91D1BE209B4BADE94485A4FA7 (void);
// 0x0000049B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_tls_elliptic_curves_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_elliptic_curves_set_m3D2C5F9D07106364F289A6231B41F93B1319464E (void);
// 0x0000049C System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_IceServer_tls_elliptic_curves_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_elliptic_curves_get_m0CC6F65C6E25BC4F0B43C0996541B341EE27C201 (void);
// 0x0000049D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_RTCConfiguration__SWIG_0()
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCConfiguration__SWIG_0_mEC455A075BB0FFD04BE47047BBBD0189AD939702 (void);
// 0x0000049E System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_RTCConfiguration__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCConfiguration__SWIG_1_mAA8F42FEED214F058B9B5292ECD186CA2E7B2ACE (void);
// 0x0000049F System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_RTCConfiguration__SWIG_2(System.Int32)
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCConfiguration__SWIG_2_m5308A2849AABE00B3DEE9EF3587E5F90B965625C (void);
// 0x000004A0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PeerConnectionInterface_RTCConfiguration(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PeerConnectionInterface_RTCConfiguration_m3338D9932C6AED3E4FA6D89D2CEF773E773E486A (void);
// 0x000004A1 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_dscp(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_dscp_mE3488550F8EFE7E9D4FC697D05D41FE89E359931 (void);
// 0x000004A2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_set_dscp(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_dscp_m11C608CD35A9172A762A71271D665AA8FBA2E96E (void);
// 0x000004A3 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_cpu_adaptation(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_cpu_adaptation_mD77AD7376E70F26FAD24547BD191055AB2884849 (void);
// 0x000004A4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_set_cpu_adaptation(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_cpu_adaptation_m39B85CA8BCB90726F81DD58D79330D32E703B9BB (void);
// 0x000004A5 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_suspend_below_min_bitrate(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_suspend_below_min_bitrate_m10823E4C418526CC7EB94E5A99989CD2B9553178 (void);
// 0x000004A6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_set_suspend_below_min_bitrate(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_suspend_below_min_bitrate_mD12ECB4012F73E74DB7605FFDFA41EA32EEE528F (void);
// 0x000004A7 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_prerenderer_smoothing(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prerenderer_smoothing_mAB8CE050D859D8065B13C12CAD1F344FFE52AFCE (void);
// 0x000004A8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_set_prerenderer_smoothing(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_prerenderer_smoothing_m83EEDCA13531C716F049C83D69CB60F7E5D61DDA (void);
// 0x000004A9 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_experiment_cpu_load_estimator(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_experiment_cpu_load_estimator_mBF4A99B3831990265105ACC82E5B0140B241B764 (void);
// 0x000004AA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_set_experiment_cpu_load_estimator(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_experiment_cpu_load_estimator_mEF9D3C26E8D6BC1A63B99C4F877D6F4C7B97E6CA (void);
// 0x000004AB System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_audio_rtcp_report_interval_ms(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_rtcp_report_interval_ms_m99FD22F517905B0E035D56FAB432F52A4E08E13A (void);
// 0x000004AC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_set_audio_rtcp_report_interval_ms(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_audio_rtcp_report_interval_ms_mB046FE9AC82787D53B3FA55E4FBA5A68A9DB643F (void);
// 0x000004AD System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_video_rtcp_report_interval_ms(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_video_rtcp_report_interval_ms_mDE5B3D0791BF174AE3D3B9C42F26121767764C7C (void);
// 0x000004AE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_set_video_rtcp_report_interval_ms(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_video_rtcp_report_interval_ms_mFCBAE3AE5BD3274D2CE5D9F7FB7014F777785D42 (void);
// 0x000004AF System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_kUndefined_get()
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kUndefined_get_mC57A1E0A058CCBCB1DFBBBFFC39DC90C243CC7D4 (void);
// 0x000004B0 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_kAudioJitterBufferMaxPackets_get()
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kAudioJitterBufferMaxPackets_get_m8A030FF39AD521E247631DF59EA7F037C6F42C48 (void);
// 0x000004B1 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_kAggressiveIceConnectionReceivingTimeout_get()
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kAggressiveIceConnectionReceivingTimeout_get_m24B9C2AB098C12B9ECEB9A83E2068435F554CA11 (void);
// 0x000004B2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_servers_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_servers_set_m8B50462B8F11BCE5BB3AA043053ACC3BD5BA6D5C (void);
// 0x000004B3 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_servers_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_servers_get_m8CD3ABCA706711DB84E92728CFF3525ACADEEFF0 (void);
// 0x000004B4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_type_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_type_set_m1A8896328B627729861AE071AE791391FE2888FB (void);
// 0x000004B5 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_type_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_type_get_m9B6499792E41E0108FC161F1E29F8AAD53DF7A76 (void);
// 0x000004B6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_bundle_policy_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_bundle_policy_set_mFEFED6FC917F19D16FABA5AF5E5D1743874E6AC2 (void);
// 0x000004B7 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_bundle_policy_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_bundle_policy_get_m14CCCA5B0CF6F7C9216A856B692FEEAE13A6A4C0 (void);
// 0x000004B8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_rtcp_mux_policy_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_rtcp_mux_policy_set_m86E0986490D75BBA9F25859991FD5BB114A0D279 (void);
// 0x000004B9 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_rtcp_mux_policy_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_rtcp_mux_policy_get_m6075D47F07BBD3CC902E0EDDD2E921FB7612E067 (void);
// 0x000004BA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_certificates_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_certificates_set_mEAD15650F56026A8E85A690B5CEF0E0E041AF933 (void);
// 0x000004BB System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_certificates_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_certificates_get_m56B261C81D93F173F8C5B4C28F01255B702C0953 (void);
// 0x000004BC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_candidate_pool_size_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_candidate_pool_size_set_mAA03B6B1389434E302A97299E0BD1902178C4432 (void);
// 0x000004BD System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_candidate_pool_size_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_candidate_pool_size_get_m31AEAA6294A7487624E8BE1410ED1EBA74551521 (void);
// 0x000004BE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_disable_ipv6_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_ipv6_set_mDEEF217CD0119D748D87311E78224041F1D7B45F (void);
// 0x000004BF System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_disable_ipv6_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_ipv6_get_mF0016616066BD9541F3A5736A26084355C210F02 (void);
// 0x000004C0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_disable_ipv6_on_wifi_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_ipv6_on_wifi_set_mB21ECE0B9BFEC5F9689976A0FAD6E8F59CD9B719 (void);
// 0x000004C1 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_disable_ipv6_on_wifi_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_ipv6_on_wifi_get_m20021AB80A9DE2B5147E4CB18AD744C48B5F10B5 (void);
// 0x000004C2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_max_ipv6_networks_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_max_ipv6_networks_set_m4FFD5D58F9F55490F2CC13BCFBBC71947C699303 (void);
// 0x000004C3 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_max_ipv6_networks_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_max_ipv6_networks_get_m0C3302F903BD6F5E507609F9A523B3436342139F (void);
// 0x000004C4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_disable_link_local_networks_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_link_local_networks_set_m137B9A4ECF61CD62C5678870202CF39402B324A4 (void);
// 0x000004C5 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_disable_link_local_networks_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_link_local_networks_get_mCFBFDC00C72E0DF86009C1BBA4D3E40D56AAD168 (void);
// 0x000004C6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_enable_rtp_data_channel_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_rtp_data_channel_set_mBF99CB8810F6EBF0CA898B16154C23697E15696E (void);
// 0x000004C7 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_enable_rtp_data_channel_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_rtp_data_channel_get_m5E32F2357AA9762F79E2B3DABC36486A5DBC8235 (void);
// 0x000004C8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_screencast_min_bitrate_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_screencast_min_bitrate_set_m944A41B630985AC86BDA748F7C8CCC73B4A22504 (void);
// 0x000004C9 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_screencast_min_bitrate_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_screencast_min_bitrate_get_mE14AB988737F0139B3E48E7BD18AE81783D4C389 (void);
// 0x000004CA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_combined_audio_video_bwe_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_combined_audio_video_bwe_set_mC035361D39C9081758CA335941D6CF6B0A0AC55E (void);
// 0x000004CB System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_combined_audio_video_bwe_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_combined_audio_video_bwe_get_mCFF577E5D4490F8BDF44649F03D2F78BEA4B179C (void);
// 0x000004CC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_enable_dtls_srtp_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_dtls_srtp_set_m316EE3C88E5CE8AD64DD5BC924762159D6DC57D1 (void);
// 0x000004CD System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_enable_dtls_srtp_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_dtls_srtp_get_mAC9C455194097F67A8D5B5E18948407DC46FE9AA (void);
// 0x000004CE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_tcp_candidate_policy_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_tcp_candidate_policy_set_mF6EBBD77A4E1610F038EF478A504823FF18B368E (void);
// 0x000004CF System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_tcp_candidate_policy_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_tcp_candidate_policy_get_mFBF421F07184E9A7BDA978026C12DB25A242BA7A (void);
// 0x000004D0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_candidate_network_policy_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_candidate_network_policy_set_mFD1C38393850A433741929BB12715B42E91F207F (void);
// 0x000004D1 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_candidate_network_policy_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_candidate_network_policy_get_mDC7AE1EED25BEC7C7B76EA7057D325FF165FE06D (void);
// 0x000004D2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_max_packets_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_max_packets_set_m5CF5BB4A706C8F6B85DC9E5D40DC182DE44ECE9F (void);
// 0x000004D3 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_max_packets_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_max_packets_get_mEA81327A203F4AA60EDB5AA8A57D45F261D5BB4A (void);
// 0x000004D4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_fast_accelerate_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_fast_accelerate_set_mC606418A2CD5DE082AC5262E6F2B9F97BBEA5C26 (void);
// 0x000004D5 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_fast_accelerate_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_fast_accelerate_get_m23E82E573B717DF5FCF57B3E1AE610ADAB282A35 (void);
// 0x000004D6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_min_delay_ms_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_min_delay_ms_set_m3E839830ECDF263B710AA302D6F19DC06A261179 (void);
// 0x000004D7 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_min_delay_ms_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_min_delay_ms_get_mD0292AF575DE82CD11E484C8216B6674CC748692 (void);
// 0x000004D8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_connection_receiving_timeout_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_connection_receiving_timeout_set_m2465C82653AEAE0BD940D7E927829F3736245154 (void);
// 0x000004D9 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_connection_receiving_timeout_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_connection_receiving_timeout_get_m3E6F43CAAFD814C48766AA608F0B219B2B0ABB20 (void);
// 0x000004DA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_backup_candidate_pair_ping_interval_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_backup_candidate_pair_ping_interval_set_mDA57D62C3A73E0F3CDC4372592495C4D6E43776B (void);
// 0x000004DB System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_backup_candidate_pair_ping_interval_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_backup_candidate_pair_ping_interval_get_mE0F40BF6E0D8FFB561A500783CCB711D858CC22B (void);
// 0x000004DC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_continual_gathering_policy_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_continual_gathering_policy_set_m11F5BC09C2B27A48086DF32D2A0AFDB562809AB4 (void);
// 0x000004DD System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_continual_gathering_policy_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_continual_gathering_policy_get_mB969007BBF74F0D95D2921302BC9D71EEDAE0470 (void);
// 0x000004DE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_prioritize_most_likely_ice_candidate_pairs_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prioritize_most_likely_ice_candidate_pairs_set_m2D5F146BA5E22E869223CBCDA50585CF078572D1 (void);
// 0x000004DF System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_prioritize_most_likely_ice_candidate_pairs_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prioritize_most_likely_ice_candidate_pairs_get_mF8043351D6F135475C60A5AC2E5C199FBAFEC9F4 (void);
// 0x000004E0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_media_config_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_media_config_set_m0A096CE060BF166C5236DC27C3DBC1893E917E96 (void);
// 0x000004E1 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_media_config_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_media_config_get_m2F71784E3FD5217CBA00F71CF13D896839CD521B (void);
// 0x000004E2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_prune_turn_ports_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prune_turn_ports_set_mA7EDAD177574BBD2A979F6650635D7E2A7541C07 (void);
// 0x000004E3 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_prune_turn_ports_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prune_turn_ports_get_m23B86F484D50EE5BD164864227FD3052E970590F (void);
// 0x000004E4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_presume_writable_when_fully_relayed_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_presume_writable_when_fully_relayed_set_m1E0E4BDE4E8980AA74A37EB756D0EE74A7EC5997 (void);
// 0x000004E5 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_presume_writable_when_fully_relayed_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_presume_writable_when_fully_relayed_get_mF4BB882ED9F6C42A73A932CD2244FE036C871582 (void);
// 0x000004E6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_enable_ice_renomination_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_ice_renomination_set_mDB17670C6AC33A0002231493AEB23D0C943C86B6 (void);
// 0x000004E7 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_enable_ice_renomination_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_ice_renomination_get_m60C78C531CE6A440F7528EE4677764B05679C3B4 (void);
// 0x000004E8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_redetermine_role_on_ice_restart_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_redetermine_role_on_ice_restart_set_m0AA0ECC41B7582B31E06E2B78821CD0198E4D872 (void);
// 0x000004E9 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_redetermine_role_on_ice_restart_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_redetermine_role_on_ice_restart_get_mA1E3B705A4E2E82C7754F9B70486AC169828D9A8 (void);
// 0x000004EA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_check_interval_strong_connectivity_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_interval_strong_connectivity_set_m03C3B8B043369894273F36D8A76DDBEDF7464BC3 (void);
// 0x000004EB System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_check_interval_strong_connectivity_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_interval_strong_connectivity_get_mBFC63B9A50D4E4C7786CDA62149AE81F01F94C34 (void);
// 0x000004EC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_check_interval_weak_connectivity_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_interval_weak_connectivity_set_mF9F587B78299EADEBCC117C6448DA63C1EF68A53 (void);
// 0x000004ED System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_check_interval_weak_connectivity_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_interval_weak_connectivity_get_m5CAC35C85FBB7102664037FBB2B53CE17D20BDA4 (void);
// 0x000004EE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_check_min_interval_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_min_interval_set_mC361E77C7ED18A2FE8F4DCA71465E33A27B4F6DB (void);
// 0x000004EF System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_check_min_interval_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_min_interval_get_m82A65C339D6AC6E19C37160457BB5CA5A47BAF38 (void);
// 0x000004F0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_unwritable_timeout_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_unwritable_timeout_set_mAA3AA5BA5049D2DE4DF3978F5EA6CA3C4CE32A09 (void);
// 0x000004F1 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_unwritable_timeout_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_unwritable_timeout_get_m84CB346898775D95F5F2F7D1CFF6376DE860091E (void);
// 0x000004F2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_unwritable_min_checks_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_unwritable_min_checks_set_mAFC38377B151464D96D8140C9C22F12BA2E995AC (void);
// 0x000004F3 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_unwritable_min_checks_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_unwritable_min_checks_get_mC690855D7066DA1E943E547E60C13093CC785B62 (void);
// 0x000004F4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_stun_candidate_keepalive_interval_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_stun_candidate_keepalive_interval_set_mEAB8B62C0AAAF19A6A9019C696C63739D20CF124 (void);
// 0x000004F5 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_stun_candidate_keepalive_interval_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_stun_candidate_keepalive_interval_get_m3F958F7E5CBE44F53B9556FBFDB8201C6E935AB9 (void);
// 0x000004F6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_regather_interval_range_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_regather_interval_range_set_m49F303E21B1ACAAC99BCEF9B8A6D8FD45010A96C (void);
// 0x000004F7 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_ice_regather_interval_range_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_regather_interval_range_get_m8DE5355094E098877D38BE4F9217C01EE200F46C (void);
// 0x000004F8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_turn_customizer_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_turn_customizer_set_m3158B814B74721793683B0D3E726CBEA3B029BB2 (void);
// 0x000004F9 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_turn_customizer_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_turn_customizer_get_m98046DC17D160D372A6084693D1EA6CED6278092 (void);
// 0x000004FA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_network_preference_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_network_preference_set_m850EB4C7BD0276D592058D7C9327FE4DCDB9DEBE (void);
// 0x000004FB System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_network_preference_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_network_preference_get_mC445CAF7586975CB61F1D16ADB7578195A8EC965 (void);
// 0x000004FC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_sdp_semantics_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_sdp_semantics_set_m43065160C30BD2F45368A8AEA6023F827D28BB1D (void);
// 0x000004FD System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_sdp_semantics_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_sdp_semantics_get_m548C0D589C8B0690330A7BEE2E48764B38913D17 (void);
// 0x000004FE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_active_reset_srtp_params_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_active_reset_srtp_params_set_m103E4B7FB90A9AF6AC822204F0B7BFB88ABC85ED (void);
// 0x000004FF System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_active_reset_srtp_params_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_active_reset_srtp_params_get_mC9DDB07BB7C634D4CE131EAD64DA6200FE53F1B9 (void);
// 0x00000500 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_use_media_transport_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_use_media_transport_set_m5DCDF18367F4228DD6DFDDA8C601C41DBA98A418 (void);
// 0x00000501 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_use_media_transport_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_use_media_transport_get_m49E486EF3B9D0C072F620E5BF510FA7A5213033C (void);
// 0x00000502 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_use_media_transport_for_data_channels_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_use_media_transport_for_data_channels_set_m006113467C0900510F42EB0959148D93C0CD38A6 (void);
// 0x00000503 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_use_media_transport_for_data_channels_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_use_media_transport_for_data_channels_get_m12CAEF64F457AD93A0AF7F7CBE521E5EC3F61399 (void);
// 0x00000504 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_crypto_options_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_crypto_options_set_m1060CE36B6D954849454FD38E25E9046DB312EBA (void);
// 0x00000505 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_crypto_options_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_crypto_options_get_m3450A0FD578374035685D52B691C9077CD3D4C7A (void);
// 0x00000506 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_offer_extmap_allow_mixed_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_offer_extmap_allow_mixed_set_m7F2AC16BF3EF39D9FBA1CC3C3030AABBF3BAC0DE (void);
// 0x00000507 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCConfiguration_offer_extmap_allow_mixed_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_offer_extmap_allow_mixed_get_mB044FF39D2E9871A3A21415B2DD7DD7B5AB2297E (void);
// 0x00000508 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_kUndefined_get()
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_kUndefined_get_m40DB439EAF1B0FEE39EAFE9E021232A12F014550 (void);
// 0x00000509 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_kMaxOfferToReceiveMedia_get()
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_kMaxOfferToReceiveMedia_get_m8C220AC327D3EDD5CDC9990AFA680395961EAF1B (void);
// 0x0000050A System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_kOfferToReceiveMediaTrue_get()
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_kOfferToReceiveMediaTrue_get_mA77C3C786E12B0F16D42975AF0EAE82BB8DD5631 (void);
// 0x0000050B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_video_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_video_set_m6B69A9DDA6FFBEF556DD66B6585C5EE9C30F3FAE (void);
// 0x0000050C System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_video_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_video_get_mB3177CF4214112CFB550256CF149F708D5ACFFB5 (void);
// 0x0000050D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_audio_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_audio_set_mF548A2821C71C5C9CA0977113071EF0EEB0B11BA (void);
// 0x0000050E System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_audio_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_audio_get_m5414F0B7418C602704F7B9FE2C1C3589926C99B2 (void);
// 0x0000050F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_voice_activity_detection_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_voice_activity_detection_set_m623F8AD60919354559D459CD6CD6821CEDA45346 (void);
// 0x00000510 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_voice_activity_detection_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_voice_activity_detection_get_mBE6B785736D69102375219C0542BB55779500E7A (void);
// 0x00000511 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_ice_restart_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_ice_restart_set_mC8C99E1E9B9E82D271DA41374878AF3786BA9E88 (void);
// 0x00000512 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_ice_restart_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_ice_restart_get_m17405E0E5ED19ED4C4F62AD1DD49227746588559 (void);
// 0x00000513 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_use_rtp_mux_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_use_rtp_mux_set_mE992A997F1A95BFE1DF7060FDEBC7B155B2083F6 (void);
// 0x00000514 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_use_rtp_mux_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_use_rtp_mux_get_mDDF9D21BC7F881992C4BA7E691D267EB33178AB7 (void);
// 0x00000515 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_num_simulcast_layers_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_num_simulcast_layers_set_mDCFED87DB6E65CF7A8F345999D1C905466E1C005 (void);
// 0x00000516 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RTCOfferAnswerOptions_num_simulcast_layers_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_num_simulcast_layers_get_m0251DE3490D518F54D657A859EFC3039DE8091E4 (void);
// 0x00000517 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_RTCOfferAnswerOptions__SWIG_0()
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCOfferAnswerOptions__SWIG_0_m4F8489E88910E1BF744C5E76AAA3BBD9D0462144 (void);
// 0x00000518 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_RTCOfferAnswerOptions__SWIG_1(System.Int32,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCOfferAnswerOptions__SWIG_1_m748BF1CB029B3CFE10A524D7D5136148537D7B47 (void);
// 0x00000519 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PeerConnectionInterface_RTCOfferAnswerOptions(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PeerConnectionInterface_RTCOfferAnswerOptions_m934F691F7C1247412C3621A0E22F81526A9E23A4 (void);
// 0x0000051A System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_local_streams(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_local_streams_mAF4D33FA333DE848F5911E6B2EA9EE199E739ABF (void);
// 0x0000051B System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_remote_streams(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_remote_streams_m9EBFF7D254B9738008193FDFCE321B1331A667C3 (void);
// 0x0000051C System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_AddStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_AddStream_m0F6E56D3693AC35312CC95E97A585BBD4C5C876D (void);
// 0x0000051D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RemoveStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RemoveStream_mBDBFF15016F8644AE40D49C4300D8BE78D604834 (void);
// 0x0000051E System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RemoveTrack(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RemoveTrack_m83D49457C10B965699682A669FFF630DBE228D12 (void);
// 0x0000051F System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_CreateSender(System.Runtime.InteropServices.HandleRef,System.String,System.String)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_CreateSender_m42F46462EE570CB924F071F58D5EDFAE5D9DBAA6 (void);
// 0x00000520 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_GetSenders(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_GetSenders_m4867A9A3A3AB18E3CF12C8B4E3C4246EAA96C1DC (void);
// 0x00000521 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_GetReceivers(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_GetReceivers_m143C6C7207B9CB00D9010390B72064DB0851B9B6 (void);
// 0x00000522 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_GetTransceivers(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_GetTransceivers_m78B92996D32DB0D0184570B7AD6DD8C569F21E91 (void);
// 0x00000523 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_GetStats__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_GetStats__SWIG_0_m50FFB790815499B634E000FD1F8999DE347728DD (void);
// 0x00000524 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_GetStats__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_GetStats__SWIG_1_m116A78C394E2793210D3F77B298FA3A51AEC6533 (void);
// 0x00000525 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_GetStats__SWIG_2(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_GetStats__SWIG_2_mDEBAF436D060E4C83D60E495C2D751FECB6D9BD1 (void);
// 0x00000526 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_GetStats__SWIG_3(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_GetStats__SWIG_3_m940CDC5B8731ECCA7EDE7BA2CB4A5C1EF0C39DDB (void);
// 0x00000527 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_ClearStatsCache(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_ClearStatsCache_m2D49F03973712296A665E266835016307194EECE (void);
// 0x00000528 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_CreateDataChannel(System.Runtime.InteropServices.HandleRef,System.String,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_CreateDataChannel_m97BA3107149FBC95E40A686425EB38D0F6802FED (void);
// 0x00000529 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_local_description(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_local_description_mEBE7037346679FA7BC12A32975DF97C32070AF40 (void);
// 0x0000052A System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_remote_description(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_remote_description_mF7931B7A7CE4477181F1E884928EC1AD6592DFD9 (void);
// 0x0000052B System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_current_local_description(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_current_local_description_m150872EDC2BCAAE72A01E058A0AE7096290CA383 (void);
// 0x0000052C System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_current_remote_description(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_current_remote_description_m1981BCF7FBA340F7488DFFCE1416430AA6995FAB (void);
// 0x0000052D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_pending_local_description(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_pending_local_description_m65111ABF1EC31294D8754877FA8BDD6214B83953 (void);
// 0x0000052E System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_pending_remote_description(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_pending_remote_description_mD7F4BA43DF2B5A60332B1C0C45C69098383099AB (void);
// 0x0000052F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_CreateOffer(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_CreateOffer_m82FCDC9ECD95CA1354D4DFCE735327EB5F214F07 (void);
// 0x00000530 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_CreateAnswer(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_CreateAnswer_mEEBA97455D56F8AAACEDABD9F7EC3EBFC776A9CC (void);
// 0x00000531 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_SetLocalDescription(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_SetLocalDescription_mEE0B479FD109B977E15CBAC52B3E4E322DDA36D4 (void);
// 0x00000532 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_GetConfiguration(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_GetConfiguration_m454AE0E917F2B672CA79EA9BFC8595F88E5EB426 (void);
// 0x00000533 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_SetConfiguration__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_SetConfiguration__SWIG_0_m2AD2A482CD22D39C1C5C450BEB17E7E607A65278 (void);
// 0x00000534 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_SetConfiguration__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_SetConfiguration__SWIG_1_m4335F38EF6E55BF0BEE9E68F5CD08418071830DD (void);
// 0x00000535 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_AddIceCandidate(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_AddIceCandidate_m42A4B5A2BD7298FD4EBD38BD438C19DC7972BB7E (void);
// 0x00000536 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_RemoveIceCandidates(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_RemoveIceCandidates_m63FEDF64D9ADFD4C1B6EF89EAD33F676756BE3FC (void);
// 0x00000537 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PeerConnectionInterface_BitrateParameters()
extern void WebRtcSwigPINVOKE_new_PeerConnectionInterface_BitrateParameters_m4C19C9348D76C3551B511514386B514028387B20 (void);
// 0x00000538 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PeerConnectionInterface_BitrateParameters(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PeerConnectionInterface_BitrateParameters_m44BCBD69E7ED76013E762F34ECEAF4255F30C769 (void);
// 0x00000539 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_BitrateParameters_min_bitrate_bps_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_min_bitrate_bps_set_m4EE109C1589A419110AEE9AB9A5B8BF5311B6068 (void);
// 0x0000053A System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_BitrateParameters_min_bitrate_bps_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_min_bitrate_bps_get_m512B3E83B8ADC217C7E3992C6BC662F595FEB28D (void);
// 0x0000053B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_BitrateParameters_current_bitrate_bps_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_current_bitrate_bps_set_mB286B448111CAF39EB0114B48F78E969A42D928F (void);
// 0x0000053C System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_BitrateParameters_current_bitrate_bps_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_current_bitrate_bps_get_mC9A561DFEA5C034966648062958AF36870736C74 (void);
// 0x0000053D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_BitrateParameters_max_bitrate_bps_set(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_max_bitrate_bps_set_m17A6B6E692E4FA12ABC50A6A92B37C29E7670957 (void);
// 0x0000053E System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_BitrateParameters_max_bitrate_bps_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_max_bitrate_bps_get_mBDF481D30C570E432B7B240B60AB1BF310A893F2 (void);
// 0x0000053F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_SetAudioPlayout(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_SetAudioPlayout_m9BFDA11EB9DD6B4A03645E02E080C6F8568D8988 (void);
// 0x00000540 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_SetAudioRecording(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_SetAudioRecording_m5B1B3D3AB346E37219F805E6BE3976831C0A6294 (void);
// 0x00000541 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_signaling_state(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_signaling_state_m8EB4AD2784B94817BF5F45AEA14ED95FEE182E4B (void);
// 0x00000542 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_ice_connection_state(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_ice_connection_state_m44370C24B18B8AFC61CFB4DCD16D645A9ADEF09C (void);
// 0x00000543 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_peer_connection_state(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_peer_connection_state_mF01ECC51FAD737BB9D2D351E110C7F7E4D67E0E2 (void);
// 0x00000544 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_ice_gathering_state(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_ice_gathering_state_mDAC351C737ADE16BE4BF27689C065AF34A07F682 (void);
// 0x00000545 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_StopRtcEventLog(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_StopRtcEventLog_m1146054A0924A295F6E7E5C99A0810C7D028654C (void);
// 0x00000546 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_Close(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_Close_mD7EB0EB6018C4DD99F3AF12E2AF071147494663C (void);
// 0x00000547 System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_kSessionDescriptionTypeName_get()
extern void WebRtcSwigPINVOKE_WebRtcWrap_kSessionDescriptionTypeName_get_m201B3A26512D71B59DF9524152E9B34329F1F1DF (void);
// 0x00000548 System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_kSessionDescriptionSdpName_get()
extern void WebRtcSwigPINVOKE_WebRtcWrap_kSessionDescriptionSdpName_get_mD063D22B1D4352F1927EBB1E898929070D97070C (void);
// 0x00000549 System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_kCandidateSdpMidName_get()
extern void WebRtcSwigPINVOKE_WebRtcWrap_kCandidateSdpMidName_get_mD32D4479D08704E3235406EBDC807A13DBC2F090 (void);
// 0x0000054A System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_kCandidateSdpMlineIndexName_get()
extern void WebRtcSwigPINVOKE_WebRtcWrap_kCandidateSdpMlineIndexName_get_m99E2A3D43103925BCE6AD06432B52B4E4B550039 (void);
// 0x0000054B System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_kCandidateSdpName_get()
extern void WebRtcSwigPINVOKE_WebRtcWrap_kCandidateSdpName_get_mBDA447387D98A414E7C16EE9292CED0730A0956D (void);
// 0x0000054C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_WebRtcWrap(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_WebRtcWrap_mB60F38E6ED373AA4E27E1C05F674266D4C12DF9F (void);
// 0x0000054D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_TestLog(WebRtcCSharp.LogCallback)
extern void WebRtcSwigPINVOKE_WebRtcWrap_TestLog_m92D3D005A66D977CE7838C3D4C253D296313BEFF (void);
// 0x0000054E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_SetLogCallback(WebRtcCSharp.LogCallback,System.Int32)
extern void WebRtcSwigPINVOKE_WebRtcWrap_SetLogCallback_mAE9482F5BA94C172FC7F32C0DFFE74A8BC91FA17 (void);
// 0x0000054F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_ResetLogCallback()
extern void WebRtcSwigPINVOKE_WebRtcWrap_ResetLogCallback_m394741C48341BB0F6F932F80C647EF97FF389E47 (void);
// 0x00000550 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_SetDebugLogLevel(System.Int32)
extern void WebRtcSwigPINVOKE_WebRtcWrap_SetDebugLogLevel_mAFE919ED3157A4FC1EA1E03D00767B0C3F1DD986 (void);
// 0x00000551 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_Log(System.String,System.Int32)
extern void WebRtcSwigPINVOKE_WebRtcWrap_Log_m334DCD0EBCD0B247EE7459BEB296B54B565907C9 (void);
// 0x00000552 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_IsSessionDescriptionJson(System.String)
extern void WebRtcSwigPINVOKE_WebRtcWrap_IsSessionDescriptionJson_m51DB241403DEFDBA2ECB61D57DEE8F7B0BED2FA9 (void);
// 0x00000553 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_IsIceCandidateJson(System.String)
extern void WebRtcSwigPINVOKE_WebRtcWrap_IsIceCandidateJson_mCCEB75E5777AA64424C33DDBD569459254CB3EE1 (void);
// 0x00000554 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_ParseJsonSessionDescription(System.String)
extern void WebRtcSwigPINVOKE_WebRtcWrap_ParseJsonSessionDescription_mBF7D9087DDBD9103E28309BBA68B715346A9AE88 (void);
// 0x00000555 System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_ToJson__SWIG_0(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_WebRtcWrap_ToJson__SWIG_0_m7484787A2096F0EA03D2EA41AA1E02B0D01E65F9 (void);
// 0x00000556 System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_SessionDescriptionToJson(System.String,System.String)
extern void WebRtcSwigPINVOKE_WebRtcWrap_SessionDescriptionToJson_mEB1EFE2681AD10AFC53B07C0BDE924C1BF4C5433 (void);
// 0x00000557 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_ParseJsonIceCandidate(System.String)
extern void WebRtcSwigPINVOKE_WebRtcWrap_ParseJsonIceCandidate_m077EA9E9D0473882FBF20675607F7FE0541629AE (void);
// 0x00000558 System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_ToJson__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_WebRtcWrap_ToJson__SWIG_1_mC64D320FC62B25B6E59C928D770E0ECE753D93E3 (void);
// 0x00000559 System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_IceCandidateToJson(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_WebRtcWrap_IceCandidateToJson_m9D3DDD16CB11D711DC8D25B684D43B5AEC600E50 (void);
// 0x0000055A System.Void WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_Copy__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UInt32)
extern void WebRtcSwigPINVOKE_WebRtcWrap_Copy__SWIG_0_mECCCF7F8DE66F13AC2858FA46FA65E86794C78DD (void);
// 0x0000055B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_Copy__SWIG_1(System.Byte[],System.UInt32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_WebRtcWrap_Copy__SWIG_1_m089DB704EF0ADF660FA3F322D35D2D5C6A17A57D (void);
// 0x0000055C System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_GetWebRtcVersion()
extern void WebRtcSwigPINVOKE_WebRtcWrap_GetWebRtcVersion_mA2DE4862174B94B63D049E6F8E57CFB20B7C7308 (void);
// 0x0000055D System.String WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_GetVersion()
extern void WebRtcSwigPINVOKE_WebRtcWrap_GetVersion_mF8C666FF1FC895C5B4C4B5636D72B89DC9CF1B9D (void);
// 0x0000055E System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_IsDebugBuild()
extern void WebRtcSwigPINVOKE_WebRtcWrap_IsDebugBuild_m061BE1C34499B1575F31D9851DC8028582ED4E95 (void);
// 0x0000055F System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_ConvertRotationMode(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_WebRtcWrap_ConvertRotationMode_m1B4F85A9424C3E74AAB1389F062BA1B6C27017AF (void);
// 0x00000560 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::WebRtcWrap_ScaleToI420(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_WebRtcWrap_ScaleToI420_m97BC698CC964BF13F1FAE0E378702A62FAA20574 (void);
// 0x00000561 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::WrapTestConnection_PollingDataChannelTest()
extern void WebRtcSwigPINVOKE_WrapTestConnection_PollingDataChannelTest_mD961212786515AA6A3A8BB8B8EDA9EA246A1D9B3 (void);
// 0x00000562 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::WrapTestConnection_PollingVideoTest()
extern void WebRtcSwigPINVOKE_WrapTestConnection_PollingVideoTest_mE9BC2F84B682A4036C47C9F810664F54F8FD2A09 (void);
// 0x00000563 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::TestsInternal_PollingDataChannelTest()
extern void WebRtcSwigPINVOKE_TestsInternal_PollingDataChannelTest_mC7EC43C1ADBACA7E538D22017AA5936D9AF0FA9A (void);
// 0x00000564 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::TestsInternal_PollingVideoTest()
extern void WebRtcSwigPINVOKE_TestsInternal_PollingVideoTest_mE3B92D12AA2AC4F1751FE8D7E18C9547D2B29665 (void);
// 0x00000565 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_TestsInternal()
extern void WebRtcSwigPINVOKE_new_TestsInternal_m2DCC4A8036042AAA27F496F3A6594BEFB5A443AA (void);
// 0x00000566 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_TestsInternal(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_TestsInternal_m84C7BE08076E542658FD1774AB14F9E90ED2889E (void);
// 0x00000567 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_echo_cancellation_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_set_mBD611544F86C6512DC83407E28905F7E371070C1 (void);
// 0x00000568 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_echo_cancellation_set_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_get_m9FC30C392C638000E7A9AFD31D6DAE8AA4B804C4 (void);
// 0x00000569 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_echo_cancellation_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_mA76F9E945157319D9DBD149AB9719EF6143C46BF (void);
// 0x0000056A System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_echo_cancellation_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_get_m63A7C44CFFCC95D88603AF034BE62E55EE44ECE2 (void);
// 0x0000056B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_extended_filter_aec_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_set_mA7AECE24A483A13297D39E8765E0D858434E2AB1 (void);
// 0x0000056C System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_extended_filter_aec_set_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_get_m568917A1832EEEBA497143B72790042E82602CAD (void);
// 0x0000056D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_extended_filter_aec_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_m7AE74292669BCB9BF57CE31C400245EA57823A27 (void);
// 0x0000056E System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_extended_filter_aec_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_get_mC77D63417BA4496760236C76E578596FF6F381F7 (void);
// 0x0000056F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_delay_agnostic_aec_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_set_m3BD82AC0D7344BB78BB3796EEC0A2D29149A09A3 (void);
// 0x00000570 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_delay_agnostic_aec_set_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_get_m68871DD5A88218F4D0B95A295BCFABEA3C5023CC (void);
// 0x00000571 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_delay_agnostic_aec_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_mA6356486CDDA3D8D41F37999B539C478B4B383BA (void);
// 0x00000572 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_delay_agnostic_aec_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_get_mAD2C67892B274859E919208A19E436B7DE9BE3EB (void);
// 0x00000573 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_noise_suppression_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_set_mEEFB43AA89E59531EF2F6FB682FB4333BFF99DB6 (void);
// 0x00000574 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_noise_suppression_set_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_get_mF98CC57489A47DCABBB4FF54377382E261A2FA23 (void);
// 0x00000575 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_noise_suppression_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_mA8698B87AC67CBBEA45894D41FB970ECB5F77ABB (void);
// 0x00000576 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_noise_suppression_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_noise_suppression_get_mC269DBEDDB7BC268AE36D6637657FBAF168F6160 (void);
// 0x00000577 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_auto_gain_control_set_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_set_mD883F072BBCBDFE5689985253139ECDC42D6B8E8 (void);
// 0x00000578 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_auto_gain_control_set_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_get_mF17636B8E2B62B501667D8194A7B40838F5684F6 (void);
// 0x00000579 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_auto_gain_control_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_m46BF758C04AD6ABD17E1F884E5E5DB4150CCF0A3 (void);
// 0x0000057A System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AudioOptions_auto_gain_control_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_get_mE2ED601724A0A441DAC95731B1503CBF4ABB75BA (void);
// 0x0000057B System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AudioOptions()
extern void WebRtcSwigPINVOKE_new_AudioOptions_mADE4EDE7AFC5013026C1F34DAE85948C2D8DCAD0 (void);
// 0x0000057C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AudioOptions(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AudioOptions_m3832E64976C34FA1BFBE3A65475BC2B8FE4C000D (void);
// 0x0000057D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_audio_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_MediaConstraints_audio_set_m9187172D50B79D59A3CA90249E1D26F6A19FADC0 (void);
// 0x0000057E System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_audio_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_audio_get_m89C1EAD98F5B746D594DCD78D1598A3EF7B36DB6 (void);
// 0x0000057F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_video_set(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_MediaConstraints_video_set_mBB2164A702694B3F45709F66A168A8F17A01BC10 (void);
// 0x00000580 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_video_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_video_get_mB0EED2AB511AD2F1B2B095EB3608FDC80E5D56B2 (void);
// 0x00000581 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_videoDeviceName_set(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_MediaConstraints_videoDeviceName_set_m5D68646344F8B70CA6646ADD3A837D0559B5E460 (void);
// 0x00000582 System.String WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_videoDeviceName_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_videoDeviceName_get_mE7104F757622DF2E33DD1F32AE8076BF03103668 (void);
// 0x00000583 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_idealWidth_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_idealWidth_set_m1494ADE404C0D4B2B62A0A2A4FF43CFE1A85EA6B (void);
// 0x00000584 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_idealWidth_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_idealWidth_get_m2095BC7DA10D5E7151E70AA13367EF7703699180 (void);
// 0x00000585 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_idealHeight_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_idealHeight_set_mEA44AF19C66F3F376C10259D28B93561C25FCD40 (void);
// 0x00000586 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_idealHeight_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_idealHeight_get_m31134681AD2F2279BBE37E9A286D80ADB388D5E5 (void);
// 0x00000587 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_minWidth_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_minWidth_set_m92342117B1398E0A20C2E4C3722DEA385AAB598F (void);
// 0x00000588 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_minWidth_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_minWidth_get_m511D4B93E228CFE07354921223A9D73E02276DF3 (void);
// 0x00000589 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_minHeight_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_minHeight_set_m49207B860CA4D3A58E2D56F79D4A2583DFD3A302 (void);
// 0x0000058A System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_minHeight_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_minHeight_get_m73FEEF11188566FB015AF3195E45441806FEF728 (void);
// 0x0000058B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_maxWidth_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_maxWidth_set_m793FFE65C4AA7DD029E4D6041B303B0EDCE4E40E (void);
// 0x0000058C System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_maxWidth_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_maxWidth_get_m14750B550A72CF72964BBF7A73306E60B8E93309 (void);
// 0x0000058D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_maxHeight_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_maxHeight_set_mCDA31FD4DB6AD24BFFFB6E6A5BEA460260AD988A (void);
// 0x0000058E System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_maxHeight_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_maxHeight_get_m745C43F9B1DE1B2F6DB561FD7E99E546A43B9333 (void);
// 0x0000058F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_idealFrameRate_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_idealFrameRate_set_m4198BEDC30EA24E42A114AF19CBB0A201698A484 (void);
// 0x00000590 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_idealFrameRate_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_idealFrameRate_get_m355FE5E4CC54A16DA81BF27724706A6856E566C4 (void);
// 0x00000591 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_maxFrameRate_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_maxFrameRate_set_m6819F1356ED55B519A49359DC142F1A7332741D3 (void);
// 0x00000592 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_maxFrameRate_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_maxFrameRate_get_m1280DB735A5FEEE6E116A0128036A244CAC7CE05 (void);
// 0x00000593 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_minFrameRate_set(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_MediaConstraints_minFrameRate_set_m4EC329C20C64A74EDEBA71ED802391E245C26871 (void);
// 0x00000594 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::MediaConstraints_minFrameRate_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_MediaConstraints_minFrameRate_get_mF9BB11312F1474BB371D0AA021E27F4C7ECBFE9A (void);
// 0x00000595 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_MediaConstraints()
extern void WebRtcSwigPINVOKE_new_MediaConstraints_m95825C791CB663588522F7A856DADED827651F63 (void);
// 0x00000596 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_MediaConstraints(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_MediaConstraints_mE97AADA67D9D9B8AA2EF17B23C6963F380C95468 (void);
// 0x00000597 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PollingMediaStreamRef__SWIG_0()
extern void WebRtcSwigPINVOKE_new_PollingMediaStreamRef__SWIG_0_mE0C96B5871FA860839507D6B93C4E44938B9939C (void);
// 0x00000598 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PollingMediaStreamRef__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_PollingMediaStreamRef__SWIG_1_mE97F135D751C31D98962925E769783045E1B0A15 (void);
// 0x00000599 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PollingMediaStreamRef__SWIG_2(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_PollingMediaStreamRef__SWIG_2_m8A15C39998FAFF04C7F91E09C523CA9CC7EA8B78 (void);
// 0x0000059A System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PollingMediaStreamRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PollingMediaStreamRef_m115E949245BA07CF0CC883A35598FF79FC167C57 (void);
// 0x0000059B System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_get_m691942A19EEC0C8E06271136F5436F9D95ADA86B (void);
// 0x0000059C System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef___deref__(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef___deref___m8470FED5DA46FCDE31F2A3C9B1611441788FE1C9 (void);
// 0x0000059D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_release_m43D749BC54ECE26151E92CEE667B8AD755BEA3CE (void);
// 0x0000059E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_swap__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_swap__SWIG_0_m6FE150D3AE46C12BB6BE6A484CDC4300AE14B987 (void);
// 0x0000059F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_swap__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_swap__SWIG_1_mEC4A10CF2E157CA08E2C7F19CC982E4C1BC9634E (void);
// 0x000005A0 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetInternal(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetInternal_m648550DDACB363E41029337C16DA7341478412AC (void);
// 0x000005A1 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_HasVideoTrack(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_HasVideoTrack_mA12D24E17A432D10C45FF9A7C33E925432EC7337 (void);
// 0x000005A2 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_HasAudioTrack(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_HasAudioTrack_m721479ED4031278BB567296D7D82F0E2D0C784A0 (void);
// 0x000005A3 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_IsMute(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_IsMute_m9B78435995A4EAB37D433DB7B9E28811B9E75742 (void);
// 0x000005A4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_SetMute(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_SetMute_m914B07165D6652A10F2B64D57EF5FD5BBAE6002C (void);
// 0x000005A5 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_CalculateByteSize(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_CalculateByteSize_m9CCDC6576060B778AC6C2D0DA030B2753C1B8E14 (void);
// 0x000005A6 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetImageData(System.Runtime.InteropServices.HandleRef,System.Int32,System.IntPtr,System.UInt32)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetImageData_m8F387D1BC0F827E82707874203274C18C93DF1FE (void);
// 0x000005A7 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_SupportsI420Access(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_SupportsI420Access_m07C823126F4CD086315DE9057B19D7665B2631B9 (void);
// 0x000005A8 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetI420Size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetI420Size_mEEDE4A7422BA6A7714220496DCA91027BDA7179F (void);
// 0x000005A9 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetI420Ptr(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetI420Ptr_m46912085021AE6210119ABCBA0811F3A094D801D (void);
// 0x000005AA System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_HasFrame(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_HasFrame_m3E02D09003F06946502141586BDF272ED26D3D0F (void);
// 0x000005AB System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_FreeCurrentImage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_FreeCurrentImage_mF54C24E557BAEE8DD748990D0E6DE0A4E64CCAE1 (void);
// 0x000005AC System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetWidth(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetWidth_m9CBF2D2F3D68986FAF205A124DA9DD77BC8364C7 (void);
// 0x000005AD System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetHeight(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetHeight_mBA6537A178868C2705391FA0FF94D112D92C1772 (void);
// 0x000005AE System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_GetRotation(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_GetRotation_m8CDA2749E39F94B373EF15F975A40D9DCD6850E1 (void);
// 0x000005AF System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_SetVolume(System.Runtime.InteropServices.HandleRef,System.Double)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_SetVolume_mC2F68934218B7F8FA9B0F6E6CF22A24A9F0F2A51 (void);
// 0x000005B0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_AddRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_AddRef_m25C43095FA9323AC64C97A28E639C0AFE032E41D (void);
// 0x000005B1 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStreamRef_Release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStreamRef_Release_m8AF8C119C6A409B5114685F296F372F52054EC0D (void);
// 0x000005B2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PollingMediaStream(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PollingMediaStream_mA9B1C66D87DAED355A57CB94A8B6FDD882B0A96B (void);
// 0x000005B3 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_GetInternal(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_GetInternal_m8344F848D429DDB095430EDB74DAB18269F70BF5 (void);
// 0x000005B4 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_HasVideoTrack(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_HasVideoTrack_m0F659F888E6D59875A0A472921AF35EF8CA1EBD7 (void);
// 0x000005B5 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_HasAudioTrack(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_HasAudioTrack_m9F014E6BB78D74E8886ECD36E0C8D53760236B08 (void);
// 0x000005B6 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_IsMute(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_IsMute_m98093BD0C753DDB976D6CBEBA5B449D7A109D86B (void);
// 0x000005B7 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_SetMute(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_PollingMediaStream_SetMute_mEDAC97634F49B648FEF4CD7F01D1C10270A7FC89 (void);
// 0x000005B8 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_CalculateByteSize(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PollingMediaStream_CalculateByteSize_m47FE6F2D845F337FE8010AA1A1987077DC76AFD7 (void);
// 0x000005B9 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_GetImageData(System.Runtime.InteropServices.HandleRef,System.Int32,System.IntPtr,System.UInt32)
extern void WebRtcSwigPINVOKE_PollingMediaStream_GetImageData_m57C45BADFE159D54F837FBB9AEDD72BD73CF7664 (void);
// 0x000005BA System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_SupportsI420Access(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_SupportsI420Access_m6EDF5B9DF8566AE94E15CB0E9FA9C130F7DA649C (void);
// 0x000005BB System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_GetI420Size(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_GetI420Size_m9F7185775A7573EFB0A26C3E4F33C5AD6C8F0622 (void);
// 0x000005BC System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_GetI420Ptr(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_GetI420Ptr_m626160BED7C51A47CE55D9ED544B5DD9F22D444D (void);
// 0x000005BD System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_HasFrame(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_HasFrame_mAC1FFA205C8F072D71860FBEE8E5CD0373AB2353 (void);
// 0x000005BE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_FreeCurrentImage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_FreeCurrentImage_mEA300ED1128FF523EFD85149EE5ABF970CBB29E0 (void);
// 0x000005BF System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_GetWidth(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_GetWidth_m1049174DEE8E4BF9348D6EBB26CB2DDFD4291BA5 (void);
// 0x000005C0 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_GetHeight(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_GetHeight_m0FF503685F3E43B91E9538CBADC6FD44E2FB0366 (void);
// 0x000005C1 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_GetRotation(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingMediaStream_GetRotation_m41CD57807D48954263D2CAA6C7F029C2C8FCE900 (void);
// 0x000005C2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_SetVolume(System.Runtime.InteropServices.HandleRef,System.Double)
extern void WebRtcSwigPINVOKE_PollingMediaStream_SetVolume_mD989A23ACD68EE76EEA655E5E690885D07925253 (void);
// 0x000005C3 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PollingPeerRef__SWIG_0()
extern void WebRtcSwigPINVOKE_new_PollingPeerRef__SWIG_0_m6F53D5CDDC4F1BA358860C4C82422CAA3845852A (void);
// 0x000005C4 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PollingPeerRef__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_PollingPeerRef__SWIG_1_m18F5FB780BEEAFB3C4BCD5FBE43FAA3424ABF044 (void);
// 0x000005C5 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_PollingPeerRef__SWIG_2(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_PollingPeerRef__SWIG_2_m37929EF908CA07A9E84D568D8E0BA22B526EBC4B (void);
// 0x000005C6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PollingPeerRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PollingPeerRef_m98D97B839B560A7475A5E3DC8F1D947115BD1768 (void);
// 0x000005C7 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_get_m685E2BFEC0F8A6C18130AA460AAD0F5FA2D66481 (void);
// 0x000005C8 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef___deref__(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef___deref___m2741662EAA3D3CE61BCA8FCE0BA734BB3EB113DE (void);
// 0x000005C9 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_release_m6C2EB9C19CEB2B2937CA2ADBB754CD9781C7F834 (void);
// 0x000005CA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_swap__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_swap__SWIG_0_mB5EB17B644004DB37B6829CA7594EA3466751140 (void);
// 0x000005CB System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_swap__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_swap__SWIG_1_mBD5105778F99CE2C403C108634AE2A9EDFAAFC74 (void);
// 0x000005CC System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Update(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Update_m59EF36160DDBB1D2BC2B6A203E8A936A13677512 (void);
// 0x000005CD System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Flush(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Flush_m7C5A2EB75E58AF808647A6B5D4A7B61F973D12FF (void);
// 0x000005CE System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_GetConnectionState(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_GetConnectionState_m876EBA1AD2605AE14A407705BE031BE03EC68608 (void);
// 0x000005CF System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_HasSignalingMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_HasSignalingMessage_m6ECD492BC77FB5015208D1714C348D8BCCDAA006 (void);
// 0x000005D0 System.String WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_DequeueSignalingMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_DequeueSignalingMessage_m895DB98A1AD839B372EF1538A94F9368689BDE83 (void);
// 0x000005D1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_AddSignalingMessage(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_PollingPeerRef_AddSignalingMessage_mE13716F7C8A834223F213C1A2626C7B6C9641D00 (void);
// 0x000005D2 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_HasSignalingError(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_HasSignalingError_m44A57B3128856A5F0C9773EABA0D4CC145DC17F6 (void);
// 0x000005D3 System.String WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_DequeueSignalingError(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_DequeueSignalingError_m0529991DA5FDB43DAB3B8EEC91A15841F1F07945 (void);
// 0x000005D4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_CreateOffer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_CreateOffer_mBA4CC86D5734AB87B4CBD0CF4F1154AA2D71BA59 (void);
// 0x000005D5 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_CreateAnswer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_CreateAnswer_m101E7CAD39B105A5EA6DF57A5AAB80F5864BD87E (void);
// 0x000005D6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Close(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Close_mEFEB84250DE0C53E3BD880FB02DF16FE081FF837 (void);
// 0x000005D7 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_CloseInternal(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_CloseInternal_mBFF8E7C79273346006A117CA13A9100B3130DC56 (void);
// 0x000005D8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Cleanup(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Cleanup_m8838D08F9D0E254D642BB880BDF7CFC0E3AF3F61 (void);
// 0x000005D9 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_CountOpenedDataChannels(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_CountOpenedDataChannels_mC8FA0B0678A5CC388D86CC0B344642DF7FADC09D (void);
// 0x000005DA System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_HasDataChannelMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_HasDataChannelMessage_m9AC6FAAEB194C26BBC0FB2511FAA259A1D459415 (void);
// 0x000005DB System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_DequeueDataChannelMessage(System.Runtime.InteropServices.HandleRef,System.Int32&,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_DequeueDataChannelMessage_m53F910D042CC2918E6445594863C317B6E37E61C (void);
// 0x000005DC System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Send(System.Runtime.InteropServices.HandleRef,System.Int32,System.Byte[],System.UInt32,System.UInt32)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Send_mD30C51A7199D2119B7E3FD4648FD48B950D2F5F0 (void);
// 0x000005DD System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_GetBufferedAmount(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PollingPeerRef_GetBufferedAmount_m3E232A992BFED30A62E7979828397C1CD33118ED (void);
// 0x000005DE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_CreateDataChannel(System.Runtime.InteropServices.HandleRef,System.String,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_CreateDataChannel_m9F3DB69BB035AFC04442126CDFABCFEAB7A09349 (void);
// 0x000005DF System.String WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_GetDataChannelLabel(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PollingPeerRef_GetDataChannelLabel_m0659E39F4B1E7DDCFFB68229A3CBEEA85EC64551 (void);
// 0x000005E0 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_GetRemoteStream(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_GetRemoteStream_mF91C29FDCECE63989B6724E6ECBCED21EB3CF595 (void);
// 0x000005E1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_AddLocalStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_AddLocalStream_m4C50843594456D830F8819CA7F6E7CBA245B4C18 (void);
// 0x000005E2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_RemoveLocalStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_RemoveLocalStream_m99C3C4A0D61A4097D6E5FBAFFD822957395D82C9 (void);
// 0x000005E3 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_RequestStats(System.Runtime.InteropServices.HandleRef,System.Boolean,System.Boolean,System.Boolean)
extern void WebRtcSwigPINVOKE_PollingPeerRef_RequestStats_m257BBBF3CDCA8C249876B8A42FDD4438F9607C25 (void);
// 0x000005E4 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_HasStats(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_HasStats_m11E10E9974ECDB4C124577ABFC1D525130A94757 (void);
// 0x000005E5 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_DequeueStats(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_DequeueStats_mBBCC62454DC0C72054A0D65DE784B6534A568D16 (void);
// 0x000005E6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_AddRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_AddRef_m0B5175B8B32242A0CCE11C199EAAE322C5A4496B (void);
// 0x000005E7 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeerRef_Release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeerRef_Release_m28F777AC9C9BE1CF8EC8A42E760B4FD48B1D5876 (void);
// 0x000005E8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_PollingPeer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_PollingPeer_mB7B755314DC6F634903675CDC5FA6568F44FDBB5 (void);
// 0x000005E9 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_Update(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_Update_m1ED1CBCD6A6EF95AD980ED3BF6B24C1F8AD51629 (void);
// 0x000005EA System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_Flush(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_Flush_mB3BF795459C1C709FD5FEDFD8B428047B97B998E (void);
// 0x000005EB System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_GetConnectionState(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_GetConnectionState_m6A60615D8F69CCCC3C69E0034D9DEDBF3B2F3D03 (void);
// 0x000005EC System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_HasSignalingMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_HasSignalingMessage_m40D94A840541BCC92B5ADB93B0C4BAD98BAE2EB1 (void);
// 0x000005ED System.String WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_DequeueSignalingMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_DequeueSignalingMessage_mC89983721FD1E031DB60E0CBDFEA8C520590E635 (void);
// 0x000005EE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_AddSignalingMessage(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_PollingPeer_AddSignalingMessage_m1297D28878F79FDDE6CBF0D1F2BFE4CB365FC374 (void);
// 0x000005EF System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_HasSignalingError(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_HasSignalingError_m22022635AF2CF5167C2DFB4C2B9C0C0B8C854D0B (void);
// 0x000005F0 System.String WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_DequeueSignalingError(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_DequeueSignalingError_m88A8F54A9350C2D48BC212D4312AE6E501CCCEC6 (void);
// 0x000005F1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_CreateOffer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_CreateOffer_m725957990B9E15E235A72AB80E2CBDB45924EECA (void);
// 0x000005F2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_CreateAnswer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_CreateAnswer_mB0944B067E296ACC89D8EE15CCEBF6D2D1F12DCE (void);
// 0x000005F3 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_Close(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_Close_m7254EF9C9522D17311D59270C3F7A073768A8F84 (void);
// 0x000005F4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_CloseInternal(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_CloseInternal_mBA31AE7A0FAEB08FE4E0BE75F1B91C15DE3B0532 (void);
// 0x000005F5 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_Cleanup(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_Cleanup_mF66DF8359DC05DC94942D4EE74CEF37C1120FE75 (void);
// 0x000005F6 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_CountOpenedDataChannels(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_CountOpenedDataChannels_mD58402C69AB244071EE54BD295A5EC3AF89E2220 (void);
// 0x000005F7 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_HasDataChannelMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_HasDataChannelMessage_m94E687707749346A070D837AC7D1ABC252559476 (void);
// 0x000005F8 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_DequeueDataChannelMessage(System.Runtime.InteropServices.HandleRef,System.Int32&,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_DequeueDataChannelMessage_m46BD360D6074C7146B4BDEBC9078DCC2532232C8 (void);
// 0x000005F9 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_Send(System.Runtime.InteropServices.HandleRef,System.Int32,System.Byte[],System.UInt32,System.UInt32)
extern void WebRtcSwigPINVOKE_PollingPeer_Send_m0CFC7AB5FAB35A2A699B4429EA7194229E4A9DEE (void);
// 0x000005FA System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_GetBufferedAmount(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PollingPeer_GetBufferedAmount_m591B44D694A3BBC447094C4EC8FF12C3D4703758 (void);
// 0x000005FB System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_CreateDataChannel(System.Runtime.InteropServices.HandleRef,System.String,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_CreateDataChannel_m71203AC90CEAE955FD7BD4F2EEF97ED424AC8217 (void);
// 0x000005FC System.String WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_GetDataChannelLabel(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void WebRtcSwigPINVOKE_PollingPeer_GetDataChannelLabel_m866A8D9B4CA635E49E1278A614DC731F5F3F7DF8 (void);
// 0x000005FD System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_GetRemoteStream(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_GetRemoteStream_m91CFF66D0872429D8DBCF3497CD043D7345CB67E (void);
// 0x000005FE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_AddLocalStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_AddLocalStream_mC0708DF6F8F83621A35ED964FC1D34E77D215971 (void);
// 0x000005FF System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_RemoveLocalStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_RemoveLocalStream_m38A9B3042983F254B9AC75C5BF80B51F9DDAAE8E (void);
// 0x00000600 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_RequestStats(System.Runtime.InteropServices.HandleRef,System.Boolean,System.Boolean,System.Boolean)
extern void WebRtcSwigPINVOKE_PollingPeer_RequestStats_m9B29D69B6FBBDE6126BF897B40154ADF5382E2AE (void);
// 0x00000601 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_HasStats(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_HasStats_mAEC9FF75831643DE6704014304B6038BBD274E9C (void);
// 0x00000602 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_DequeueStats(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_PollingPeer_DequeueStats_m4BC9DFD6409FF4FD1C201B9945A138BF72655730 (void);
// 0x00000603 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncPeerEventArgs(System.Int32)
extern void WebRtcSwigPINVOKE_new_AsyncPeerEventArgs_mB1AFA5B02EBD1737E0ECE7173533DA6285E6C184 (void);
// 0x00000604 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AsyncPeerEventArgs(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AsyncPeerEventArgs_m4015CE821AE7F91931E5907319982C3535983E5B (void);
// 0x00000605 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventArgs_GetEventType(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetEventType_m722A0B3BCC09309A97FDA7B4F7E418C1A6765703 (void);
// 0x00000606 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventArgs_GetContentType(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetContentType_m7444AB62B144BEC66951AE4FAB1EABC91652D563 (void);
// 0x00000607 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventArgs_GetStream(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetStream_mEB436434C32FDC0B1F9D69F2A870B096AFD69410 (void);
// 0x00000608 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventArgs_GetDataChannel(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetDataChannel_m32A2EA8724D94D196FF9FBF6DF5EF6DEFE8EE6BD (void);
// 0x00000609 System.String WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventArgs_GetString(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetString_m23C1D7483EFC47D389064ABEBCDBEE0488066B81 (void);
// 0x0000060A System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncPeerEventStringArgs(System.Int32,System.String)
extern void WebRtcSwigPINVOKE_new_AsyncPeerEventStringArgs_m7DAE3AE88BD2ADC61B375F8A7D44BEBA74D18401 (void);
// 0x0000060B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AsyncPeerEventStringArgs(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AsyncPeerEventStringArgs_mD71807333ECE012C0E20CC7ADB09BD6549426DF3 (void);
// 0x0000060C System.String WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventStringArgs_GetString(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerEventStringArgs_GetString_m4F373230BC867E2A1BEBE6C6B8E503677D33A8DE (void);
// 0x0000060D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventStringArgs_Cast(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerEventStringArgs_Cast_m1BC01D907DDF6CA01838EBEF7C59D7E46DA8CB44 (void);
// 0x0000060E System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncPeerEventMediaStreamArgs(System.Int32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_AsyncPeerEventMediaStreamArgs_m52C4ED17658159318CC80A4A7A089AE6993D511F (void);
// 0x0000060F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AsyncPeerEventMediaStreamArgs(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AsyncPeerEventMediaStreamArgs_mB5250C19BF69F0ED03A2D97DFBF411AEC49629AF (void);
// 0x00000610 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventMediaStreamArgs_GetStream(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerEventMediaStreamArgs_GetStream_m1265D57FA5AFF137B7148515C81F16831AA9ADE7 (void);
// 0x00000611 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncPeerEventDataChannelArgs(System.Int32,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_AsyncPeerEventDataChannelArgs_m4A37046D2C318B0FA011F0EDE11CDB510EE02700 (void);
// 0x00000612 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AsyncPeerEventDataChannelArgs(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AsyncPeerEventDataChannelArgs_mAA594600B736C50FFE015A2B461B2132D1F94A7F (void);
// 0x00000613 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventDataChannelArgs_GetDataChannel(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerEventDataChannelArgs_GetDataChannel_m2FEC3F3B2525A00738E937B86DE39D9616EFFC00 (void);
// 0x00000614 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncPeerRef__SWIG_0()
extern void WebRtcSwigPINVOKE_new_AsyncPeerRef__SWIG_0_mF45B5AF38D9197C228B9654464E9B75FD9A1BE01 (void);
// 0x00000615 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncPeerRef__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_AsyncPeerRef__SWIG_1_m51AF2B29B9F49DE414CEF134A42FEABD1DDC6F37 (void);
// 0x00000616 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncPeerRef__SWIG_2(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_AsyncPeerRef__SWIG_2_mC7CE56D78546AB0962FE023BB1DEB96EBEA9628D (void);
// 0x00000617 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AsyncPeerRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AsyncPeerRef_m02A37ADBA76E41D36C1489BFD43D56DA4141C113 (void);
// 0x00000618 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_get_mA6358975C4CBBA1BF42F54956237A0A98BBA9F06 (void);
// 0x00000619 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef___deref__(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef___deref___mE62031F4C353CD4ECFAAA736DE24D4293615CBD2 (void);
// 0x0000061A System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_release_m84E33CDFBB51724F4BD931AD228EC98650DD94BF (void);
// 0x0000061B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_swap__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_swap__SWIG_0_mB9B434109DE6CC5FECCB56905D16124EEC849868 (void);
// 0x0000061C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_swap__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_swap__SWIG_1_mF1D4349483F155421BDF8EF63531CBA9C43D108C (void);
// 0x0000061D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_Update(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_Update_m8ADFFCB6B3933673855D41D4A752E75F7DEA837D (void);
// 0x0000061E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_Flush(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_Flush_mB4DEA57E0F197D99D8CBFB82DEBE8E2289CD9AEC (void);
// 0x0000061F System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_GetConnectionState(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_GetConnectionState_m7439DE1D061769EEDDE6F64B04CD34A69EEBA336 (void);
// 0x00000620 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_CreateOffer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_CreateOffer_mCF081BCF636DEC353F8F4F118489FF748BA10820 (void);
// 0x00000621 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_CreateAnswer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_CreateAnswer_mD488B793FEAFFC980F484E2F253FF7CDA4DFFA91 (void);
// 0x00000622 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_SetLocalDescription(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_SetLocalDescription_mE7FB2FA17CEB3120E0E5EDCB3720B610ADFC97D2 (void);
// 0x00000623 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_SetRemoteDescription(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_SetRemoteDescription_m2122D262723C678ED1ABFE097F4661ECAC4BA5B6 (void);
// 0x00000624 System.String WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_GetLocalDescription(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_GetLocalDescription_m1B62EE7A4E0CB8783FC6D32A567F08A17B76B68C (void);
// 0x00000625 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_AddIceCandidate(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_AddIceCandidate_mD2B364C37E867B4893AEE6733166AF033C66A0CE (void);
// 0x00000626 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_DequeueEvent(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_DequeueEvent_m868403A29887DB7D8A784EDA952D9071416E8BEB (void);
// 0x00000627 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_Close(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_Close_m85C71DEB5E9B36178C57DC32BDF19B884D407B99 (void);
// 0x00000628 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_Cleanup(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_Cleanup_m4E5CBCE89F1C0AA3AFB0880283ED2B42AE465CBC (void);
// 0x00000629 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_CreateDataChannel(System.Runtime.InteropServices.HandleRef,System.String,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_CreateDataChannel_m81012A6616914BE8D8FE1F43CC65B3036F70CB04 (void);
// 0x0000062A System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_AddLocalStream__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_AddLocalStream__SWIG_0_mC6D512BA79E44208FEC446448EE589637B724390 (void);
// 0x0000062B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_AddLocalStream__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_AddLocalStream__SWIG_1_m4D4EC438BA9B582C55F194C2E23453D1DF9FB29A (void);
// 0x0000062C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_RemoveLocalStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_RemoveLocalStream_m8CB848E4C381A638A745D532BA75FA2C43FCD256 (void);
// 0x0000062D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_AddRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_AddRef_m3448305DAEDFA8E2552AB3E1B2FB27C48287C3E7 (void);
// 0x0000062E System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerRef_Release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeerRef_Release_mB6DA68B45FF96C4615963191E47BCA6BF069E323 (void);
// 0x0000062F System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncDataChannelRef__SWIG_0()
extern void WebRtcSwigPINVOKE_new_AsyncDataChannelRef__SWIG_0_m8DB5B58C69103C4F568CBA14B8735C0B0C3A6F63 (void);
// 0x00000630 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncDataChannelRef__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_AsyncDataChannelRef__SWIG_1_m8BFB26E5F7D00B0D71FFA117877D6915C4277519 (void);
// 0x00000631 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_AsyncDataChannelRef__SWIG_2(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_AsyncDataChannelRef__SWIG_2_mF3040D34CEF91FDBF55DC82DBFD19D34EAFC47E0 (void);
// 0x00000632 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AsyncDataChannelRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AsyncDataChannelRef_m16A16268912A888D2AA12D531C833D6E6D234FC5 (void);
// 0x00000633 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_get_mE2F9FA96F5F3A2336E25953AE66C46309EDBE8CC (void);
// 0x00000634 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef___deref__(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef___deref___m007595E26B1DB77042D4B7099D3B996556066D22 (void);
// 0x00000635 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_release_mA381E9A872AE230AFC4DCBCC4EA1234A4DDA4803 (void);
// 0x00000636 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_swap__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_swap__SWIG_0_mC01783F34555724AFA29A227D411BAEAAF10D127 (void);
// 0x00000637 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_swap__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_swap__SWIG_1_m2E9D984944EC5D437B5337A18169E8010C6F3309 (void);
// 0x00000638 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_Init(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_Init_mC250621637DF9436D82999B3174723875EC214E4 (void);
// 0x00000639 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_GetId(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_GetId_m40C3860CB12BD88F4CF1EB1BD1976B1D4B220F31 (void);
// 0x0000063A System.String WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_GetLabel(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_GetLabel_m1F55DFDC27396ABA15907B9B32CA34255DB43DE0 (void);
// 0x0000063B System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_IsInitialized(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_IsInitialized_mC3DE2981EB320F657237D3828BBDEC3F8296537C (void);
// 0x0000063C System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_GetState(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_GetState_mCF90EF3608CC62F6C3DE312BB51EAF429984785B (void);
// 0x0000063D System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_HasMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_HasMessage_m59B9325078AF62121AA8D2F54BCA8E45761F51D9 (void);
// 0x0000063E System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_DequeueMessage__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_DequeueMessage__SWIG_0_mC0967F33976690B6684A1CA3107FC899D96E49F9 (void);
// 0x0000063F System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_DequeueMessage__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_DequeueMessage__SWIG_1_m6C44EB17E6D63B6A23688CAD18CEC97728E05830 (void);
// 0x00000640 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_Send(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.UInt32,System.UInt32)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_Send_m408F2417FCCDA68278AD4B7C38763D0985854DBC (void);
// 0x00000641 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_AddRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_AddRef_m614730759DCE2A9386F5E6EB963EB9CBC1EEB05F (void);
// 0x00000642 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannelRef_Release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannelRef_Release_mF6B04880C3CBFEDB2D8BB7ABB90364B46AE9E8AC (void);
// 0x00000643 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_Message()
extern void WebRtcSwigPINVOKE_new_Message_m208B419EDEC072D288092DBDA61E1BAE38AC15CE (void);
// 0x00000644 System.UInt32 WebRtcCSharp.WebRtcSwigPINVOKE::Message_GetSize(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_Message_GetSize_mC7146CCE72F02AB297B46469EB9BF6C73FA3E7BA (void);
// 0x00000645 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::Message_GetData(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void WebRtcSwigPINVOKE_Message_GetData_mC3F03EBACA68D3B4FCF0DDC4A551E88B8905F6FF (void);
// 0x00000646 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_Message(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_Message_m90429E5BC0579FDD2C80BB0E5C0F255B5FA01E50 (void);
// 0x00000647 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AsyncDataChannel(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AsyncDataChannel_mD8D3FB2FD16A05EF6FE53CC1FD1CA47B6F3751B7 (void);
// 0x00000648 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_Init(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_Init_mE5FF315DCB962A61FAC911713C02D724924CB95E (void);
// 0x00000649 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_GetId(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_GetId_m597C6C0ACEB512FAFC4816BEEFF7B42FF9B56729 (void);
// 0x0000064A System.String WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_GetLabel(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_GetLabel_m1D1F06FF8485731EE570F0F56F14E29537421E53 (void);
// 0x0000064B System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_IsInitialized(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_IsInitialized_mB4A9ED344576EEDE72476710094D6B42DD70C119 (void);
// 0x0000064C System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_GetState(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_GetState_mE7C442330F0755DB7F4F3C86E44E852B44A65E63 (void);
// 0x0000064D System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_HasMessage(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_HasMessage_m88CDF929F3AA4EB16FB1C7F000EF5A68A6563BB7 (void);
// 0x0000064E System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_DequeueMessage__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_DequeueMessage__SWIG_0_m1393BDBDD9F36F39537971A3A49FF5A140D75051 (void);
// 0x0000064F System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_DequeueMessage__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_DequeueMessage__SWIG_1_mD253E3CD810D1EC0728B03BFD2E8CBFF08ADC101 (void);
// 0x00000650 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_Send(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.UInt32,System.UInt32)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_Send_mCCA1CF6E8F232692B235636949BD9861001F43B2 (void);
// 0x00000651 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_AsyncPeer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_AsyncPeer_m8B168C9988394FD2B83B94D2F1793DD493B37563 (void);
// 0x00000652 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_Update(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_Update_mEA0CD797C98E7879221E5BA8275C3E5987DBB657 (void);
// 0x00000653 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_Flush(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_Flush_m291FA454C64D2AE056BC3D5C56965C70BEA71E83 (void);
// 0x00000654 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_GetConnectionState(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_GetConnectionState_m335E9D3AEF1F6DEEE355174717ACADB7060D527A (void);
// 0x00000655 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_CreateOffer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_CreateOffer_m29C2292F5753FC27A0F1E115BF8DD231516AAC38 (void);
// 0x00000656 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_CreateAnswer(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_CreateAnswer_mC67BE60681960E9B95A8E490851C068266FAD87A (void);
// 0x00000657 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_SetLocalDescription(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_AsyncPeer_SetLocalDescription_m473E996D8E62134CE584E42ED26510C86D1FA073 (void);
// 0x00000658 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_SetRemoteDescription(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_AsyncPeer_SetRemoteDescription_m76AB548C87BBCE5E3A843826DFFD59E18090585A (void);
// 0x00000659 System.String WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_GetLocalDescription(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_GetLocalDescription_m9CDF4CF14553497F0C667C6FB179A3401F5C640D (void);
// 0x0000065A System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_AddIceCandidate(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_AsyncPeer_AddIceCandidate_mF44BF154B90CB0411DB881E38D7CC3A2E4794BA2 (void);
// 0x0000065B System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_DequeueEvent(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_DequeueEvent_m57A64BFAAF0EE61BB01B660FC819F65AE973BFF8 (void);
// 0x0000065C System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_Close(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_Close_m517E1DAF57F7A7F72E4C7FFF376445F877B87446 (void);
// 0x0000065D System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_Cleanup(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_Cleanup_m5749DF83670A064600233AA2E8A5997FA21678F6 (void);
// 0x0000065E System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_CreateDataChannel(System.Runtime.InteropServices.HandleRef,System.String,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_CreateDataChannel_m13058BACA1C354F3AC9F85D040CE9CC68E2989D2 (void);
// 0x0000065F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_AddLocalStream__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_AddLocalStream__SWIG_0_m348393F724A03B2A495BCDEF7A20208C7B0B3E94 (void);
// 0x00000660 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_AddLocalStream__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_AddLocalStream__SWIG_1_mB1D85048A347B1C6A1131530E2C5C48A9DB31762 (void);
// 0x00000661 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_RemoveLocalStream(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_AsyncPeer_RemoveLocalStream_m8325CF98EDA942BF20B8B7801897C7CC2A074F4F (void);
// 0x00000662 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_VideoInputRef__SWIG_0()
extern void WebRtcSwigPINVOKE_new_VideoInputRef__SWIG_0_m3EB9CE836AE1D35CD1CFB298E529EA3AFAE575DB (void);
// 0x00000663 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_VideoInputRef__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_VideoInputRef__SWIG_1_m86AEEFBCC468858C5E37C672DB38AAF1AC618317 (void);
// 0x00000664 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_VideoInputRef__SWIG_2(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_VideoInputRef__SWIG_2_mCD8FB397CE3C8EF698E7C219AA5EF43ACB312354 (void);
// 0x00000665 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_VideoInputRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_VideoInputRef_mF9A0B5BBB80BB220AEE4E5BC563553982DE773FD (void);
// 0x00000666 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoInputRef_get_m870554B6B22624CFCB17E525D5D36DE79515B667 (void);
// 0x00000667 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef___deref__(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoInputRef___deref___m010D06F2B2DD0BA238611687E35311D4E9F498A4 (void);
// 0x00000668 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoInputRef_release_m768465B9177E0C453CCF2F747C744FB8ECE00706 (void);
// 0x00000669 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_swap__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoInputRef_swap__SWIG_0_mB933C57B6C61BFC9FED257C1E3FE2BA2E7CB307E (void);
// 0x0000066A System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_swap__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoInputRef_swap__SWIG_1_m6EA9C75647F60B884217D1B8CAD38A3A46885AE7 (void);
// 0x0000066B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_AddDevice(System.Runtime.InteropServices.HandleRef,System.String,System.Int32,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_VideoInputRef_AddDevice_m4F6590A364BCF702A765E462372E2FC48FB92F15 (void);
// 0x0000066C System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_UpdateFrame(System.Runtime.InteropServices.HandleRef,System.String,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WebRtcSwigPINVOKE_VideoInputRef_UpdateFrame_mA6819B0F9F320499EFB54DE2E09485A1418BFA6D (void);
// 0x0000066D System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_UpdateFrame2(System.Runtime.InteropServices.HandleRef,System.String,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WebRtcSwigPINVOKE_VideoInputRef_UpdateFrame2_m3C7D8FBC00D30389B1F7CF7E55F46A4816DC759F (void);
// 0x0000066E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_RemoveDevice(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_VideoInputRef_RemoveDevice_mBB94B9E6DFBF1ADD6532E772FAA9301EA3F139E2 (void);
// 0x0000066F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_AddRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoInputRef_AddRef_m9F321A7B1353677E668DAB1C04D928C1181F4312 (void);
// 0x00000670 System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::VideoInputRef_Release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_VideoInputRef_Release_mF34E3BE921E01EE15CB009A9058E03D0265AA143 (void);
// 0x00000671 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_VideoInput(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_VideoInput_m45C706CB41B83E65FAECAE3BA5A3EED1601C513D (void);
// 0x00000672 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoInput_AddDevice(System.Runtime.InteropServices.HandleRef,System.String,System.Int32,System.Int32,System.Int32)
extern void WebRtcSwigPINVOKE_VideoInput_AddDevice_mFC25A164901FFCEA9522274F4108CA81365C327D (void);
// 0x00000673 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::VideoInput_UpdateFrame(System.Runtime.InteropServices.HandleRef,System.String,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WebRtcSwigPINVOKE_VideoInput_UpdateFrame_m53BEC834801E4E073C7D5DFA4A07ABF26621025F (void);
// 0x00000674 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::VideoInput_UpdateFrame2(System.Runtime.InteropServices.HandleRef,System.String,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WebRtcSwigPINVOKE_VideoInput_UpdateFrame2_m6CFCE3A919EC897176AF63BF2039A0A296D9058B (void);
// 0x00000675 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::VideoInput_RemoveDevice(System.Runtime.InteropServices.HandleRef,System.String)
extern void WebRtcSwigPINVOKE_VideoInput_RemoveDevice_mDC34402544493A7CCAD08A86F516BE1E496DB389 (void);
// 0x00000676 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCLog_L(System.String)
extern void WebRtcSwigPINVOKE_RTCLog_L_mFE5E54667E1D0F5BCADED27E9A5ED243A3AFD4FB (void);
// 0x00000677 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCLog_LW(System.String)
extern void WebRtcSwigPINVOKE_RTCLog_LW_m9D5F87080272E7D6ACE5483BD5F14E62A0B9FA99 (void);
// 0x00000678 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCLog_LE(System.String)
extern void WebRtcSwigPINVOKE_RTCLog_LE_m786337D585D5D1353A5338D750CB310E28D2CF2E (void);
// 0x00000679 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_RTCLog()
extern void WebRtcSwigPINVOKE_new_RTCLog_m3CC4A8089EAB78024303263AEC6B1CFD3EF3A373 (void);
// 0x0000067A System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_RTCLog(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_RTCLog_m06A757E13EB77B120E10391E4EB0AD4D9D763416 (void);
// 0x0000067B System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_RTCPeerConnectionFactoryRef__SWIG_0()
extern void WebRtcSwigPINVOKE_new_RTCPeerConnectionFactoryRef__SWIG_0_m8EFC4BAD1396BB03143A33B0D450A10B9A2355C9 (void);
// 0x0000067C System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_RTCPeerConnectionFactoryRef__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_RTCPeerConnectionFactoryRef__SWIG_1_mD29C1FC471D1DF88382720A07393503350A245F6 (void);
// 0x0000067D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_RTCPeerConnectionFactoryRef__SWIG_2(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_new_RTCPeerConnectionFactoryRef__SWIG_2_mFD969ABA7D67BB6B6A08566D5C40E5D0CF9DCE94 (void);
// 0x0000067E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_RTCPeerConnectionFactoryRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_RTCPeerConnectionFactoryRef_m17F62F857CB87F253B20290FA34F234A312E2299 (void);
// 0x0000067F System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_get(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_get_m26C1DE33F6FE9CA6D94F70DD43C31FCCFBAC24B3 (void);
// 0x00000680 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef___deref__(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef___deref___m9015CA38DC4AEC699CC30CF7FFAAA7F120ADBA69 (void);
// 0x00000681 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_release_mF7C49C0C011536EF82954DF5CF3AE99C4F196283 (void);
// 0x00000682 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_swap__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_swap__SWIG_0_m52941FA7F91B23B86350892EF42171A51BEE6902 (void);
// 0x00000683 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_swap__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_swap__SWIG_1_mFA58DBF164675009485224D6C4CC356B5C1B1F58 (void);
// 0x00000684 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_Create(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_Create_mFFBF6BA64F97A294CF4ADD9332A4EC7B429D89C0 (void);
// 0x00000685 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_SetObosoleteVideo(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_SetObosoleteVideo_mF7F643E0811E0E902F0E750E5D8EBBDA1066A9B6 (void);
// 0x00000686 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_ForceDummyAudio(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_ForceDummyAudio_mF761EF79FABDC7193C5488BEECB279EBFF64F302 (void);
// 0x00000687 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_AddVideoCaptureFactory(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_AddVideoCaptureFactory_mE7A12C6C54C03F32F331C817F6790F7A258A9299 (void);
// 0x00000688 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_IsInitialized(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_IsInitialized_mD6EA43B0B7B2172E04C7F71FA65F221C9BDCB524 (void);
// 0x00000689 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_Initialize(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_Initialize_mC1633844B00D3135B37887FBE72C779965D598CE (void);
// 0x0000068A System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_SetGlobalSpeakerMute(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_SetGlobalSpeakerMute_mC64ACED8F7FCD71964741DCF6B2C26B2A57C73B3 (void);
// 0x0000068B System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetGlobalSpeakerMute(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetGlobalSpeakerMute_m79D14778401B816C41E4C3EF84E4AD703F4C196D (void);
// 0x0000068C System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_HasGlobalSpeakerMute(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_HasGlobalSpeakerMute_m071E2C473EB1A173BAB672683BC9DFA7FA7F8CF8 (void);
// 0x0000068D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_CreatePollingPeer(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingPeer_m94D84F8805E4846E78C92B538502AD9E51807028 (void);
// 0x0000068E System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_CreateAsyncPeer(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreateAsyncPeer_m4581DE4F0180737B1B97B5FDF265615708BEBAED (void);
// 0x0000068F System.String WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetDebugDeviceInfo(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_m595FAA6C0739F8EA3292AD17A6FC7F33BF1D50F7 (void);
// 0x00000690 System.String WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old_m2928AE71588C39CA98CECFBD408B6EE5EFEDA8FC (void);
// 0x00000691 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_CreateMediaStream__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreateMediaStream__SWIG_0_m568A65885547B976C95DC9F42C5F03A596AE4721 (void);
// 0x00000692 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_CreateMediaStream__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreateMediaStream__SWIG_1_m4B29112C4CC1ED9E1AFAAB1C954C1C96584AAB91 (void);
// 0x00000693 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_CreatePollingMediaStream__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingMediaStream__SWIG_0_m3999330087BD81D1FCD0267F3E0991659C1C8597 (void);
// 0x00000694 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_CreatePollingMediaStream__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingMediaStream__SWIG_1_m8197175FDB2105E8D1D8E83759957D3C710661E1 (void);
// 0x00000695 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetVideoDevices(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetVideoDevices_m98CA00081AF179472614755566C1D276046885E1 (void);
// 0x00000696 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetVideoInput(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetVideoInput_m2751861765F27524AABF0012E5266C65EDD55743 (void);
// 0x00000697 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetThreads(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetThreads_m8DBAE7E66B3C20FBF578C40F8FA815E73DD92145 (void);
// 0x00000698 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetSignalingThread(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetSignalingThread_m88486566B737AC7D246B5E511E81553DA802FB43 (void);
// 0x00000699 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_GetAudioDeviceModule(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetAudioDeviceModule_m3CAFFEF2F171A4A009AD91FC0D1D3E7CD9F7930E (void);
// 0x0000069A System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_InitAndroidContext(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_InitAndroidContext_m95A9BD00FC4964B11BD633145AB39ED560ED1CE9 (void);
// 0x0000069B System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_AddRef(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_AddRef_m3320DFCE956A38F98269C5CA70467522359999B5 (void);
// 0x0000069C System.Int32 WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactoryRef_Release(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_Release_m35E4E0C34D51BE7010D0D355D64C67DABEC201E6 (void);
// 0x0000069D System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_Create()
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_Create_mB3B3A0055311942838D4AD3621B1088C460E8649 (void);
// 0x0000069E System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_SetObosoleteVideo(System.Boolean)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_SetObosoleteVideo_mB1F57A02BF0ACE8DA721BBD1C86441E923A0A5BE (void);
// 0x0000069F System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_ForceDummyAudio(System.Boolean)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_ForceDummyAudio_m76E228204CB2F7C2D745BB65BDEB07731DE9907B (void);
// 0x000006A0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_RTCPeerConnectionFactory(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_RTCPeerConnectionFactory_m17EA5AC0E1A67D40B76D87914F38FEDBCA811937 (void);
// 0x000006A1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_AddVideoCaptureFactory(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_AddVideoCaptureFactory_mF8FB014A0D757A59001DDF4DCA29576497FB30DB (void);
// 0x000006A2 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_IsInitialized(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_IsInitialized_mC9FB48CD0770F58A936E8228B83A4EE3F8C94287 (void);
// 0x000006A3 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_Initialize(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_Initialize_m7828E66FCEA3CE0C5FC75845D82817585CBAA660 (void);
// 0x000006A4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_SetGlobalSpeakerMute(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_SetGlobalSpeakerMute_m6464E661B915D9AF1D4DF225926E6CBB8B435D8F (void);
// 0x000006A5 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_GetGlobalSpeakerMute(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetGlobalSpeakerMute_mE2A1E242E196F61B0A8ACCCAA67E6A0500A0F17E (void);
// 0x000006A6 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_HasGlobalSpeakerMute(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_HasGlobalSpeakerMute_mB4F87E1B818A25ABE7A0F24B6E645ADA777477BA (void);
// 0x000006A7 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_CreatePollingPeer(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreatePollingPeer_mE9DADB52FD725586FD73CFA20D407EBB1A92FA09 (void);
// 0x000006A8 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_CreateAsyncPeer(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreateAsyncPeer_mF98DD421E99DB07DC260223CC2F7ED80557B0F09 (void);
// 0x000006A9 System.String WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_GetDebugDeviceInfo()
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetDebugDeviceInfo_m70A93488FFD3E421A9D34A56925F305D2BE00CC6 (void);
// 0x000006AA System.String WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_GetDebugDeviceInfo_Old()
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetDebugDeviceInfo_Old_mE4EB9896419C4D0601E98BDD6A054E01736215E6 (void);
// 0x000006AB System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_CreateMediaStream__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreateMediaStream__SWIG_0_m367ADF12FB6EE0D9A7690285AF2D53636FE813A3 (void);
// 0x000006AC System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_CreateMediaStream__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreateMediaStream__SWIG_1_mBB25D0F1197EB89C8681386DE766D0457E5A11DC (void);
// 0x000006AD System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_CreatePollingMediaStream__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreatePollingMediaStream__SWIG_0_m15C7DD44FDA15803C9020D82548B4406F0603370 (void);
// 0x000006AE System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_CreatePollingMediaStream__SWIG_1(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreatePollingMediaStream__SWIG_1_mC71126F6442826D71C00EC99E3612F2EF2159B1F (void);
// 0x000006AF System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_GetVideoDevices(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetVideoDevices_m01A814B53ECAC2E78370099DA81AF88E74219532 (void);
// 0x000006B0 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_GetVideoInput(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetVideoInput_m7C5DF7776951425CC0C6C054677A266530FE5CD1 (void);
// 0x000006B1 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_GetThreads(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetThreads_mADE72072F224D6A91AD6072864014F142A45838B (void);
// 0x000006B2 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_GetSignalingThread(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetSignalingThread_mCDAD5A4086A5BF1A302AB8C21AABDBAB993E0A1A (void);
// 0x000006B3 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_GetAudioDeviceModule(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetAudioDeviceModule_m52F80D76D07B624C163DBC344BA47B70C8C4CE7E (void);
// 0x000006B4 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_InitAndroidContext(System.IntPtr)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_InitAndroidContext_m1C611023295C36B80D034DC8717D5F93B457A816 (void);
// 0x000006B5 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::GlobalStats_SetActive(System.Boolean)
extern void WebRtcSwigPINVOKE_GlobalStats_SetActive_m515CB87BCEA9962E4D1C58B12854E0A268DB89B2 (void);
// 0x000006B6 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::GlobalStats_IsActive()
extern void WebRtcSwigPINVOKE_GlobalStats_IsActive_mDBC465B10821A8CE14E4AA2C7581E88A602E568A (void);
// 0x000006B7 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::GlobalStats_SetRefreshTime(System.Int32)
extern void WebRtcSwigPINVOKE_GlobalStats_SetRefreshTime_m576E7E9384A19EC5C47EF5E65D1CDE3FE228EC76 (void);
// 0x000006B8 System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::GlobalStats_HasStats()
extern void WebRtcSwigPINVOKE_GlobalStats_HasStats_m27C7C15C1B4EE15CF28D4B429DF250BB405018C8 (void);
// 0x000006B9 System.String WebRtcCSharp.WebRtcSwigPINVOKE::GlobalStats_Dequeue()
extern void WebRtcSwigPINVOKE_GlobalStats_Dequeue_mC61F313C764F55F539238CA96186040466075DEB (void);
// 0x000006BA System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_GlobalStats()
extern void WebRtcSwigPINVOKE_new_GlobalStats_m0CC9CD635C008FF6BEF1BD5AD7AB79A0866C65A7 (void);
// 0x000006BB System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_GlobalStats(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_GlobalStats_m8D36E67BDBD4D46C0455C1D019E7C7420565D984 (void);
// 0x000006BC System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::IosHelper_IsAvailable()
extern void WebRtcSwigPINVOKE_IosHelper_IsAvailable_m2D5FA7C479DEA06332E1322E1C7E8079A02AD122 (void);
// 0x000006BD System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IosHelper_InitAudioLayer()
extern void WebRtcSwigPINVOKE_IosHelper_InitAudioLayer_m1D89C0DD0CED1E362645AD350F9A59DEB38555CF (void);
// 0x000006BE System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IosHelper_SetLoudspeakerStatus(System.Boolean)
extern void WebRtcSwigPINVOKE_IosHelper_SetLoudspeakerStatus_mF806DC2F966238B75CA7FA029880D3567216165E (void);
// 0x000006BF System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE::IosHelper_GetLoudspeakerStatus()
extern void WebRtcSwigPINVOKE_IosHelper_GetLoudspeakerStatus_mC526A140FF4319E640D83899120E288369DD91C8 (void);
// 0x000006C0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::IosHelper_IosKeepAudioActive(System.Boolean)
extern void WebRtcSwigPINVOKE_IosHelper_IosKeepAudioActive_m2926161BC5C7BD9EED6C96EDF039D534DB7E912F (void);
// 0x000006C1 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::new_IosHelper()
extern void WebRtcSwigPINVOKE_new_IosHelper_m53587720A06418BD2051E604B52C81F79D32FED6 (void);
// 0x000006C2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE::delete_IosHelper(System.Runtime.InteropServices.HandleRef)
extern void WebRtcSwigPINVOKE_delete_IosHelper_m9DF0F3603BC9B3E4A422BACF98E9F044E71BC514 (void);
// 0x000006C3 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::VideoFormat_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_VideoFormat_SWIGUpcast_mB17513610DFEF1B44D56E72944F0ACAB9FDB96FF (void);
// 0x000006C4 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::DataChannelInterface_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_DataChannelInterface_SWIGUpcast_m8821D81867404D22000802DDBB63CBD8EB1EAA18 (void);
// 0x000006C5 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PeerConnectionInterface_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_PeerConnectionInterface_SWIGUpcast_m4F1A38FDD370D1B33E9A983176F668B262BFC059 (void);
// 0x000006C6 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingMediaStream_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_PollingMediaStream_SWIGUpcast_m2035616DDD305CE1B3962123DE9FF6F597909058 (void);
// 0x000006C7 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::PollingPeer_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_PollingPeer_SWIGUpcast_m6A10B5B44C87DBDB73DBC7BB2AEEF10F23144E1D (void);
// 0x000006C8 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventStringArgs_SWIGSmartPtrUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_AsyncPeerEventStringArgs_SWIGSmartPtrUpcast_mB4AAC7D520BD333D2DB110A854FB0655F41A12FD (void);
// 0x000006C9 System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventMediaStreamArgs_SWIGSmartPtrUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_AsyncPeerEventMediaStreamArgs_SWIGSmartPtrUpcast_mB8F5983AEA840CE6201397B2B15E4D7E9C27A567 (void);
// 0x000006CA System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeerEventDataChannelArgs_SWIGSmartPtrUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_AsyncPeerEventDataChannelArgs_SWIGSmartPtrUpcast_mCA4949B764CC049B91002275526997A830B96587 (void);
// 0x000006CB System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncDataChannel_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_AsyncDataChannel_SWIGUpcast_m76B985290D04E4C953F3688024C4629F8732F142 (void);
// 0x000006CC System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::AsyncPeer_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_AsyncPeer_SWIGUpcast_m4D530C326FFB26154AE152F037423AE4346AD92B (void);
// 0x000006CD System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::VideoInput_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_VideoInput_SWIGUpcast_mF6A1A146CCD57F3D45EFB44A5B6107A9B86A9674 (void);
// 0x000006CE System.IntPtr WebRtcCSharp.WebRtcSwigPINVOKE::RTCPeerConnectionFactory_SWIGUpcast(System.IntPtr)
extern void WebRtcSwigPINVOKE_RTCPeerConnectionFactory_SWIGUpcast_m801B5A6302B387D3A52087F15BB6FD03A320F0C9 (void);
// 0x000006CF System.Void WebRtcCSharp.WebRtcSwigPINVOKE::.ctor()
extern void WebRtcSwigPINVOKE__ctor_mA6D767678D44BAEED257CB06D981ED2D80FDBB6B (void);
// 0x000006D0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_WebRtcSwig(WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_WebRtcSwig_mE2674A81F559F4ABC42618A05A7820A7733F8712 (void);
// 0x000006D1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_WebRtcSwig(WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_WebRtcSwig_m574844BB23B961E8F451862FC98E559ABBC18E5C (void);
// 0x000006D2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern void SWIGExceptionHelper_SetPendingApplicationException_m34EBF8A5064761FA3A1489B58F0FC88F22A91CD7 (void);
// 0x000006D3 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern void SWIGExceptionHelper_SetPendingArithmeticException_m8290EEE144C47E4E40A26518F15E4456E66428BB (void);
// 0x000006D4 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern void SWIGExceptionHelper_SetPendingDivideByZeroException_m79369FABDFC4B7A658FB90AA83A833582BA50B29 (void);
// 0x000006D5 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m0DDAA0B11C0577104A3C337CD95BCEB0474778CD (void);
// 0x000006D6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidCastException_m63B0AB9D01F05113151C6F5AEF564CA18A712C4C (void);
// 0x000006D7 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidOperationException_mAD82A0B9BB052EE4E2C9494A5C06D29561CF350F (void);
// 0x000006D8 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern void SWIGExceptionHelper_SetPendingIOException_mB3905F1943D8A52F63FF86D33C8B55F4D2BFE1BC (void);
// 0x000006D9 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern void SWIGExceptionHelper_SetPendingNullReferenceException_m1678867024287040EAEF9E1A6B603632E949C81C (void);
// 0x000006DA System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern void SWIGExceptionHelper_SetPendingOutOfMemoryException_mE49ACFFEB5D1BDCA69BA5306BDC2BA5764FCA627 (void);
// 0x000006DB System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern void SWIGExceptionHelper_SetPendingOverflowException_m941534271CDCF96CC46373C3D001D38B687A5AAE (void);
// 0x000006DC System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern void SWIGExceptionHelper_SetPendingSystemException_mA8895E77BEDB03935FAD2AF1684802D8B9CA66FB (void);
// 0x000006DD System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentException_m1A7F32CCCD497644F0D4F54AD803BC783B68531E (void);
// 0x000006DE System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentNullException_mA3831677B0C3CB59A79EAB9F63570D4FCAE5206F (void);
// 0x000006DF System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB95333E5EEE5C6FFCF1A8B9EFEB60E7301498FDD (void);
// 0x000006E0 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::.cctor()
extern void SWIGExceptionHelper__cctor_m9A2AC49E5565D72585A7D5C39EC304CFB402DAD9 (void);
// 0x000006E1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper::.ctor()
extern void SWIGExceptionHelper__ctor_mD199EDDA31FF78BECA863CE55EC499D86FA7497F (void);
// 0x000006E2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionDelegate__ctor_mDEB8E0006B6FBE8D17612D71B6AAEF18CA1F2BFB (void);
// 0x000006E3 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern void ExceptionDelegate_Invoke_m9EC896D2C89EA9FC2417AC879F54603DABF652A2 (void);
// 0x000006E4 System.IAsyncResult WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ExceptionDelegate_BeginInvoke_mBC8512D05FBC223B5734CFCEEEBC7893FAA63224 (void);
// 0x000006E5 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionDelegate_EndInvoke_mC46EE8D0799B8DBA8A575913529835AD4A96AF33 (void);
// 0x000006E6 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionArgumentDelegate__ctor_mE53DD4E636AFEFA860A74C66DBCD22EB2CA5FC2B (void);
// 0x000006E7 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern void ExceptionArgumentDelegate_Invoke_m055EED3E5E1A9DFCD017E05642B433FA4999F430 (void);
// 0x000006E8 System.IAsyncResult WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void ExceptionArgumentDelegate_BeginInvoke_mD001EECFE6F02BD79269A7C2934CF7C2043B5406 (void);
// 0x000006E9 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionArgumentDelegate_EndInvoke_m42FA221703F90A30204917FE52946749339A8A2D (void);
// 0x000006EA System.Boolean WebRtcCSharp.WebRtcSwigPINVOKE/SWIGPendingException::get_Pending()
extern void SWIGPendingException_get_Pending_m9D7C57725172B3859CC1753DA69B613817C79B54 (void);
// 0x000006EB System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGPendingException::Set(System.Exception)
extern void SWIGPendingException_Set_m7E6B6622888484216F2A13456DF8E19D9903E0A6 (void);
// 0x000006EC System.Exception WebRtcCSharp.WebRtcSwigPINVOKE/SWIGPendingException::Retrieve()
extern void SWIGPendingException_Retrieve_mB7CB6383FAE280390ADE774FA195AB3A1CCA3FEF (void);
// 0x000006ED System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGPendingException::.ctor()
extern void SWIGPendingException__ctor_m46EE8F3F43A740DAE12CFC4B00E5B6C150C9B6CF (void);
// 0x000006EE System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGPendingException::.cctor()
extern void SWIGPendingException__cctor_m6C128E6FE4518D21F0A0F39B4D49DA209E22F693 (void);
// 0x000006EF System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_WebRtcSwig(WebRtcCSharp.WebRtcSwigPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern void SWIGStringHelper_SWIGRegisterStringCallback_WebRtcSwig_m44538ACE045CF7A595A459FD2583ADBF50A1CF79 (void);
// 0x000006F0 System.String WebRtcCSharp.WebRtcSwigPINVOKE/SWIGStringHelper::CreateString(System.String)
extern void SWIGStringHelper_CreateString_m38DA578E6839BE86909501F1021E85EC709E4F2A (void);
// 0x000006F1 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGStringHelper::.cctor()
extern void SWIGStringHelper__cctor_m75FB3CD968CB6CDF765511BE3C035D53B2116ACF (void);
// 0x000006F2 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGStringHelper::.ctor()
extern void SWIGStringHelper__ctor_m125BF570A9740D187730B13933B6874412DA2716 (void);
// 0x000006F3 System.Void WebRtcCSharp.WebRtcSwigPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIGStringDelegate__ctor_m4F67F956CEAE329CC1AE313B1861A9EDB4275225 (void);
// 0x000006F4 System.String WebRtcCSharp.WebRtcSwigPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern void SWIGStringDelegate_Invoke_mF041672ED7A1B6D26E8DEEAB938115B700D8A77F (void);
// 0x000006F5 System.IAsyncResult WebRtcCSharp.WebRtcSwigPINVOKE/SWIGStringHelper/SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void SWIGStringDelegate_BeginInvoke_m5C38CE8D009CF1C7E4588B33ACCAB6CAA4068D1D (void);
// 0x000006F6 System.String WebRtcCSharp.WebRtcSwigPINVOKE/SWIGStringHelper/SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern void SWIGStringDelegate_EndInvoke_m235E4339B54823E23FEB31FAAEE929982F5C6FA7 (void);
// 0x000006F7 System.Void WebRtcCSharp.WebRtcWrap::.ctor(System.IntPtr,System.Boolean)
extern void WebRtcWrap__ctor_m2E688E11B460A3C0DA503E291FFF3BC38ED2285A (void);
// 0x000006F8 System.Runtime.InteropServices.HandleRef WebRtcCSharp.WebRtcWrap::getCPtr(WebRtcCSharp.WebRtcWrap)
extern void WebRtcWrap_getCPtr_m2EEC3DC1C693CB85B1BA7A1A52BFE134776987CC (void);
// 0x000006F9 System.Void WebRtcCSharp.WebRtcWrap::Finalize()
extern void WebRtcWrap_Finalize_mE071FF2AC77CB292EC0A73A818D3659B5A37D7B3 (void);
// 0x000006FA System.Void WebRtcCSharp.WebRtcWrap::Dispose()
extern void WebRtcWrap_Dispose_m04DBCCBFB96F9A59DA028F08D8B48466640AC00C (void);
// 0x000006FB System.String WebRtcCSharp.WebRtcWrap::get_kSessionDescriptionTypeName()
extern void WebRtcWrap_get_kSessionDescriptionTypeName_m6181E3AFDF49107811974C97FF32CA2144A1CBCC (void);
// 0x000006FC System.String WebRtcCSharp.WebRtcWrap::get_kSessionDescriptionSdpName()
extern void WebRtcWrap_get_kSessionDescriptionSdpName_m03F1FDDA9443C23D9E56E8855C2DCAFBAE0D9879 (void);
// 0x000006FD System.String WebRtcCSharp.WebRtcWrap::get_kCandidateSdpMidName()
extern void WebRtcWrap_get_kCandidateSdpMidName_m97726CB8D2D284DAB82679E4B728F90D8EF252ED (void);
// 0x000006FE System.String WebRtcCSharp.WebRtcWrap::get_kCandidateSdpMlineIndexName()
extern void WebRtcWrap_get_kCandidateSdpMlineIndexName_m308341DC8B4BDAF42008DCCF0326777FE3C67B7D (void);
// 0x000006FF System.String WebRtcCSharp.WebRtcWrap::get_kCandidateSdpName()
extern void WebRtcWrap_get_kCandidateSdpName_mC07EEC50A7221017DB4275730A8A1EE88A57D817 (void);
// 0x00000700 System.Void WebRtcCSharp.WebRtcWrap::TestLog(WebRtcCSharp.LogCallback)
extern void WebRtcWrap_TestLog_m553448FB929A8AAC82003E22A4BE0E9C5F69260F (void);
// 0x00000701 System.Void WebRtcCSharp.WebRtcWrap::SetLogCallback(WebRtcCSharp.LogCallback,WebRtcCSharp.LoggingSeverity)
extern void WebRtcWrap_SetLogCallback_mA005255597F4FFD9AFB51234CCFB4CCFF4D8B2D3 (void);
// 0x00000702 System.Void WebRtcCSharp.WebRtcWrap::ResetLogCallback()
extern void WebRtcWrap_ResetLogCallback_mF7D38F4FCAA67F8F55BDE1178896316970DCEB29 (void);
// 0x00000703 System.Void WebRtcCSharp.WebRtcWrap::SetDebugLogLevel(WebRtcCSharp.LoggingSeverity)
extern void WebRtcWrap_SetDebugLogLevel_mB114A6F43E604E30006C282F406BBFF4B85F9BFB (void);
// 0x00000704 System.Void WebRtcCSharp.WebRtcWrap::Log(System.String,WebRtcCSharp.LoggingSeverity)
extern void WebRtcWrap_Log_mB2A85CC6DC6D5074F6ECCB675070F22A97ECD572 (void);
// 0x00000705 System.Boolean WebRtcCSharp.WebRtcWrap::IsSessionDescriptionJson(System.String)
extern void WebRtcWrap_IsSessionDescriptionJson_m14E8CD79F17587E760093F63804D05171D2602E2 (void);
// 0x00000706 System.Boolean WebRtcCSharp.WebRtcWrap::IsIceCandidateJson(System.String)
extern void WebRtcWrap_IsIceCandidateJson_mA76DED218D9075539821780A7474B59560679CFE (void);
// 0x00000707 WebRtcCSharp.SWIGTYPE_p_webrtc__SessionDescriptionInterface WebRtcCSharp.WebRtcWrap::ParseJsonSessionDescription(System.String)
extern void WebRtcWrap_ParseJsonSessionDescription_m221A084B998A892A033CEA6FA66601C83C17C8E3 (void);
// 0x00000708 System.String WebRtcCSharp.WebRtcWrap::ToJson(WebRtcCSharp.SWIGTYPE_p_webrtc__SessionDescriptionInterface)
extern void WebRtcWrap_ToJson_mEA45B4A01B1A3BEED3CD7F568EF5FC9637F3C7AA (void);
// 0x00000709 System.String WebRtcCSharp.WebRtcWrap::SessionDescriptionToJson(System.String,System.String)
extern void WebRtcWrap_SessionDescriptionToJson_mD3463D047AB9E0614A733ACD81996D230C7DC4CA (void);
// 0x0000070A WebRtcCSharp.SWIGTYPE_p_webrtc__IceCandidateInterface WebRtcCSharp.WebRtcWrap::ParseJsonIceCandidate(System.String)
extern void WebRtcWrap_ParseJsonIceCandidate_m285C91F4A5622F604D0DEA423055387D578F45BB (void);
// 0x0000070B System.String WebRtcCSharp.WebRtcWrap::ToJson(WebRtcCSharp.SWIGTYPE_p_webrtc__IceCandidateInterface)
extern void WebRtcWrap_ToJson_m27E4DADE20F546862877B4B0233D8CF1D6648327 (void);
// 0x0000070C System.String WebRtcCSharp.WebRtcWrap::IceCandidateToJson(WebRtcCSharp.SWIGTYPE_p_webrtc__IceCandidateInterface)
extern void WebRtcWrap_IceCandidateToJson_m6EF874CE41C2D5C9D3D50637D423E560BD6A21F3 (void);
// 0x0000070D System.Void WebRtcCSharp.WebRtcWrap::Copy(WebRtcCSharp.DataBuffer,System.Byte[],System.UInt32)
extern void WebRtcWrap_Copy_mAE68A337B1DFA6FFABE8BDD16880331DD499807D (void);
// 0x0000070E System.Void WebRtcCSharp.WebRtcWrap::Copy(System.Byte[],System.UInt32,WebRtcCSharp.DataBuffer)
extern void WebRtcWrap_Copy_mB76C351A459CB3FC6686B4C761239D491A5397B5 (void);
// 0x0000070F System.String WebRtcCSharp.WebRtcWrap::GetWebRtcVersion()
extern void WebRtcWrap_GetWebRtcVersion_mD846FA2E6ED8B95BC6AE8C680A45CB55B383D083 (void);
// 0x00000710 System.String WebRtcCSharp.WebRtcWrap::GetVersion()
extern void WebRtcWrap_GetVersion_m423D2AD6306662056A3225BA50E8065ACDCC8E24 (void);
// 0x00000711 System.Boolean WebRtcCSharp.WebRtcWrap::IsDebugBuild()
extern void WebRtcWrap_IsDebugBuild_mCEDD30DFEA4B9FBC1C7BA61C8A9290477B96A210 (void);
// 0x00000712 WebRtcCSharp.SWIGTYPE_p_libyuv__RotationMode WebRtcCSharp.WebRtcWrap::ConvertRotationMode(WebRtcCSharp.SWIGTYPE_p_webrtc__VideoRotation)
extern void WebRtcWrap_ConvertRotationMode_m395D4D09333B58A9E4B0E57315EEDC4F5E1A5B45 (void);
// 0x00000713 System.Int32 WebRtcCSharp.WebRtcWrap::ScaleToI420(WebRtcCSharp.SWIGTYPE_p_webrtc__I420Buffer,WebRtcCSharp.SWIGTYPE_p_webrtc__I420Buffer)
extern void WebRtcWrap_ScaleToI420_mF355B0898673FAB4A32511430DF436ED1E5A7A4C (void);
// 0x00000714 System.Void WebRtcCSharp.LogCallback::.ctor(System.Object,System.IntPtr)
extern void LogCallback__ctor_m83C1FEEF2E89DB819BB2A2AC637C9A23CB0DED2E (void);
// 0x00000715 System.Void WebRtcCSharp.LogCallback::Invoke(System.String)
extern void LogCallback_Invoke_m61E228049482540E190905C702ED18297BBD1E98 (void);
// 0x00000716 System.IAsyncResult WebRtcCSharp.LogCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void LogCallback_BeginInvoke_m3694A7F76024F31AAC49BC2CC0D1087AC0FBB00F (void);
// 0x00000717 System.Void WebRtcCSharp.LogCallback::EndInvoke(System.IAsyncResult)
extern void LogCallback_EndInvoke_mCAD7B91FD78CD1C02ACF419500D3A3B79BA9FCC0 (void);
// 0x00000718 System.Void WebRtcCSharp.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_mEF36AF2E79BE146D6DFBAE273374B93A24E37065 (void);
static Il2CppMethodPointer s_methodPointers[1816] = 
{
	AsyncDataChannel__ctor_m19FD25BB6ECA9C84502BCB2F4F6F0F5DEB7C116C,
	AsyncDataChannel_getCPtr_m1FEB17D4F8E72B852E1F3E4630960DE20C8D84CE,
	AsyncDataChannel_Finalize_mFBBFFB96BFD494B8E8DC1A20E0FB374365ED2EF5,
	AsyncDataChannel_Dispose_m6706A8AEAC29AFF0D336CC7D4C92EB35A3952B42,
	AsyncDataChannel_Init_mB9D2C99E0F50B855D88EE08F3935C6D77397E1B6,
	AsyncDataChannel_GetId_m5998F49AE2CAF3F93FEBD33156F3B6F8A60C1630,
	AsyncDataChannel_GetLabel_m635564D1D9B5B3351539E887507494DD9EB41C92,
	AsyncDataChannel_IsInitialized_m294E1B640D3CAD9AFD9ADEE766ED0C22C66ADCFF,
	AsyncDataChannel_GetState_m21DC62A443DD9054EFF658577420A93AEC799073,
	AsyncDataChannel_HasMessage_m0228C09C9A4E7B41F7DF6C0E845BF07D05823073,
	AsyncDataChannel_DequeueMessage_mAA8415D8AEBE422D4C9819002BFEC0C5F5190F70,
	AsyncDataChannel_DequeueMessage_m52B071BB4A6A3AAC83AC367EAD74326D26F8FAD7,
	AsyncDataChannel_Send_m763BB5D83E779D8BA25122B396A43409EADA5C42,
	AsyncDataChannelRef__ctor_mDAA5FB563F32AB416DCEBC24152D2F0A0CFDD755,
	AsyncDataChannelRef_getCPtr_m4DA4862C166C75E486E08896E92A55013A246EC4,
	AsyncDataChannelRef_Finalize_mEEBB18C9A83ADC38D6B942B478D297D268275E00,
	AsyncDataChannelRef_Dispose_mA3F62BFCF83E1EC11730CCA53D5641AF2ED41999,
	AsyncDataChannelRef__ctor_m8BF1EA8B608889057DEB5C8DF47F6B45BA5904E4,
	AsyncDataChannelRef__ctor_m2AB777F3F07058BB91672916BE1BFD883F391444,
	AsyncDataChannelRef__ctor_m1C5D1068882C9095418AC5429F4E4D8E30490A12,
	AsyncDataChannelRef_get_m0D02FA0DB33FF1DA46F0CDD772CC95C19F676751,
	AsyncDataChannelRef___deref___m2A289DF418851EDDE477D1FD88DEABC154752C92,
	AsyncDataChannelRef_release_mF7EB507A752E717A695497676285A7A08C7B4F35,
	AsyncDataChannelRef_swap_m3ABCB2F2DD5BDF2D98522C7061AB742BD2E5CF43,
	AsyncDataChannelRef_swap_mDFDE86B44CC15FD6CC7CCB769758CEA4CB5F21FD,
	AsyncDataChannelRef_Init_mC2DB549AFB4F3A6F6B7FD9CC2E12D61D6CCE0BE8,
	AsyncDataChannelRef_GetId_m7CE2E36F89E45A63F81371B41208C3E66EEAEA42,
	AsyncDataChannelRef_GetLabel_mC2D2C24AD91A087C717E4CD2CA6B3B77A86F5DB0,
	AsyncDataChannelRef_IsInitialized_m63370C1258FFD029D34633FF0AF24DBB16E29766,
	AsyncDataChannelRef_GetState_m267DED3350D1CA81A727A5B46761A5001453185F,
	AsyncDataChannelRef_HasMessage_m6C9A1DD91E7A28D37396322F1994C8DDADCDDD1E,
	AsyncDataChannelRef_DequeueMessage_mF26AE1B006D7537D812F3344C4C02F0BA67BB8D2,
	AsyncDataChannelRef_DequeueMessage_m5E0C0674E24875A19F79BC753BB4782A3D6B611A,
	AsyncDataChannelRef_Send_m2247CC9DB38CAA147EF141937C77C930A2D53BD9,
	AsyncDataChannelRef_AddRef_mEB78DA3850005594095ADB812200139CC56E075A,
	AsyncDataChannelRef_Release_mB243E9AB84E90A666E710543A202C688BB8CEB2F,
	AsyncPeer__ctor_mDF3C08B17B007AD6A60B630006EE1561DEEE1C64,
	AsyncPeer_getCPtr_m6B155B71D6572830FD4C51C36EEC4FE69A6D0D30,
	AsyncPeer_Finalize_mB07A6FFC84F0ABE5286E86BDDF0583DC8EDD81FA,
	AsyncPeer_Dispose_m931BBAEA988CE086BCBDF757C06176ED1F9CF062,
	AsyncPeer_Update_m1F1EAE395D7B73ABD229E328B62D79D5AA248EA3,
	AsyncPeer_Flush_m6EF242F215FB4DB0EF260F98480F311E44A54153,
	AsyncPeer_GetConnectionState_m1DBDCF76F265DEDAB72D077496B4102B45CE6AFA,
	AsyncPeer_CreateOffer_mE0DDFE21013CFB261D498FC8796833E506650E01,
	AsyncPeer_CreateAnswer_m2EA4E572DCE8E00F91CC5A4F2100FE36C1C15497,
	AsyncPeer_SetLocalDescription_mCCD4069C54EC8E7486DE737F4489A830D81713C1,
	AsyncPeer_SetRemoteDescription_m96DF578A441A6FF1AEBAA59C0A6A0F2A7C1A78E2,
	AsyncPeer_GetLocalDescription_m72D8FEC97F29196FA82EE3E3BB9B5215A8CF3A1F,
	AsyncPeer_AddIceCandidate_m3AD26B4BF9239EBE3A61A5A1EC95F595572F21E4,
	AsyncPeer_DequeueEvent_mCCA2AD5FE39FE927A6F9A3F52088F9F3F0CAF623,
	AsyncPeer_Close_m332D627B822A6098DCF9D29FF1CC3FF57C4AA618,
	AsyncPeer_Cleanup_mBC2834789661F9A0D1E83B2B114EB328CF8AD504,
	AsyncPeer_CreateDataChannel_m460E0B4847B15C1E1EEAE933AC0BC152DBDCE4C0,
	AsyncPeer_AddLocalStream_m86B8087EE604E970D41FDC3BFB5708A2D75B6026,
	AsyncPeer_AddLocalStream_mEA9AFFB48FA6DA5A10758C99246272DED156D2CC,
	AsyncPeer_RemoveLocalStream_m8828510A20B47CB9B26BE4DEC35A67D86E2E6F01,
	AsyncPeerEventArgs__ctor_mEC4BB058B8F5BBDF8D6FA016339EAF2F2B73BE59,
	AsyncPeerEventArgs_getCPtr_m37CB9BCBEADCA8F3897B63A12FDBBDDA9B3D549F,
	AsyncPeerEventArgs_Finalize_mAABC2416C47964487CC53EADC422C6DE7CA3BC34,
	AsyncPeerEventArgs_Dispose_m32EDD92393B24F83C7FDC409EC723A08C6448DC8,
	AsyncPeerEventArgs__ctor_m22D9A88F57799A462631CDB7F054E78E658FF1C6,
	AsyncPeerEventArgs_GetEventType_mA56B480B834152C0F60D5DF56E108B2DB9E1DBCD,
	AsyncPeerEventArgs_GetContentType_mB1C4C82A3CA656C382EAA9FC023AF8F62EDFC7EF,
	AsyncPeerEventArgs_GetStream_m5A7BF002E9A858AA50484E4E52BE423538C005B1,
	AsyncPeerEventArgs_GetDataChannel_m35725DF8A7462CF87E4FD239E9D5AACE8B5BD994,
	AsyncPeerEventArgs_GetString_mBD8077874E6023488D457A56EB5BE42FBBD0FBBC,
	AsyncPeerEventDataChannelArgs__ctor_m603FFADD2F057C20E32BD73E7F481706AB970196,
	AsyncPeerEventDataChannelArgs_getCPtr_mAA9992467480DDF9B1DE92772171E658C9056438,
	AsyncPeerEventDataChannelArgs_Finalize_mE93E97A90C88AE5CB024D725CC48085793ECCFE0,
	AsyncPeerEventDataChannelArgs_Dispose_mD65F342515978E10C1BF8ACA3EBAE48133A3EA06,
	AsyncPeerEventDataChannelArgs__ctor_m6B7C6E2AE6699E88FDDA106F49638D4BA3BA322E,
	AsyncPeerEventDataChannelArgs_GetDataChannel_mB9EF512B5F276E6B30A0B257AE32AED02EE2E632,
	AsyncPeerEventMediaStreamArgs__ctor_mFF9F7B8D9BAC6554C5BFB079010242139BAED5BA,
	AsyncPeerEventMediaStreamArgs_getCPtr_m5D5D4C32FF41BB82A493CAF88940BA6A1D412F55,
	AsyncPeerEventMediaStreamArgs_Finalize_m9CE08629DA8BB7FE47DD15BB6478560904D7E157,
	AsyncPeerEventMediaStreamArgs_Dispose_mD0DF152F2D8825593D4BB5CA39EC0EC9880D6B72,
	AsyncPeerEventMediaStreamArgs__ctor_mB6FF787B6A0CFB93855CF705ADA7492A3D471E31,
	AsyncPeerEventMediaStreamArgs_GetStream_m60686E129F229B2C42601E568D316AE1786F84CA,
	AsyncPeerEventStringArgs__ctor_mF8DC86E5B997DACB060B209CED4E1DD3DD220168,
	AsyncPeerEventStringArgs_getCPtr_m0EA7EBCE3F906AE28DF101D85EF8AFD29EA8D182,
	AsyncPeerEventStringArgs_Finalize_m578B0F5D7811E24BDB0C76DE69364BBDF25341DB,
	AsyncPeerEventStringArgs_Dispose_m146E7A8264F38DC79AACEB800005F262B2253402,
	AsyncPeerEventStringArgs__ctor_m1F550A4E523B51055E4BE0E29976961C0486CA42,
	AsyncPeerEventStringArgs_GetString_m921A20B5716358EFCF5F0C1840F983A0A65248CA,
	AsyncPeerEventStringArgs_Cast_m198FCC7C5A7774478C8B346947FDC3EAAA054646,
	AsyncPeerRef__ctor_m9D1576B32E3820E0E0A5CCC83D702A97FA0EF5FC,
	AsyncPeerRef_getCPtr_mB730207BF0385D7F508547B28DABF6A913585EAD,
	AsyncPeerRef_Finalize_m73F4905B7320DD7035BA2B36D0C799D9A39BC7B7,
	AsyncPeerRef_Dispose_mEDB8F5557C1B898F1823B460CCF645BB1D09EEB2,
	AsyncPeerRef__ctor_m3EBE86A56394F0E9CE8477E425225D67FC927A02,
	AsyncPeerRef__ctor_mB18064D50E3D37EC7F1B5672C88BB72325C5A871,
	AsyncPeerRef__ctor_m1AAF6287E24DF94F5F402F4119CCE684C80F8B65,
	AsyncPeerRef_get_m5DFEA5E1968AB978B60A4AF39232EC7FD647857C,
	AsyncPeerRef___deref___mCE7697230E896AE5E764EE4E763E7DD1E329A41A,
	AsyncPeerRef_release_mB1D10D6219E9B4960A7751DE0E8887C0D3D36C63,
	AsyncPeerRef_swap_m0A583A9860DDC31DB01170DF13EE86CA5291415F,
	AsyncPeerRef_swap_m18B0EC86E8C0396A7BCCCD21EFD2FA2CA806844A,
	AsyncPeerRef_Update_m470E04F07098DAB66ED6DA52965FD170E3535F05,
	AsyncPeerRef_Flush_m2FC13316278F0BC52125AE60A2012747D37C3881,
	AsyncPeerRef_GetConnectionState_mC6D2E93D7335F7DB3FADE68E2C9D7FD054F4C88F,
	AsyncPeerRef_CreateOffer_m577101B9B2FC0415CAF4C64C6399AB76368C3714,
	AsyncPeerRef_CreateAnswer_m500DF0AEE3AC2CFEFDF6330C974CC5E9FCEC1F77,
	AsyncPeerRef_SetLocalDescription_m82515C335502737822B959C3AB6599605AA18577,
	AsyncPeerRef_SetRemoteDescription_m1C51C75CAE2B3AB6ADCDC2CC183FA4A6824AC305,
	AsyncPeerRef_GetLocalDescription_mB94F3285572E89EB8E8754C4C9A328CEC7DA5303,
	AsyncPeerRef_AddIceCandidate_m9008E7A0BD4B748F82E546502515461BAF95A127,
	AsyncPeerRef_DequeueEvent_m0F2C5657BE776D91149FCB33F851840B2CFB39A5,
	AsyncPeerRef_Close_m9D57310755F959D6D1E2B08A988A146D12C7632E,
	AsyncPeerRef_Cleanup_m6A955E5EC0E37E4D38024581FF95AE3760671881,
	AsyncPeerRef_CreateDataChannel_m27F7CA4F0E3954C7EAEBFFE35DDB49A759B493E4,
	AsyncPeerRef_AddLocalStream_m0645068979C43789C9FB555A5A43EACD5A52F597,
	AsyncPeerRef_AddLocalStream_mCE98242FD577B10A4C5AB09D87B9AA057795CC38,
	AsyncPeerRef_RemoveLocalStream_m491400FE90FC7F1D8161B14F64068CE6B133CCAA,
	AsyncPeerRef_AddRef_m1997EBE433FDA4F8F124EB88DC9964E94E9D8DB6,
	AsyncPeerRef_Release_m77432702CAE783E4513442E3B9B26CC0D5C23D52,
	AudioOptions__ctor_m0FEC32FFD2CDC60531B6D66D546D7CCA3EF1DD0B,
	AudioOptions_getCPtr_mC13930839D53631456B461964E0615B2A444794F,
	AudioOptions_Finalize_mA94EE36EA1614A5D70A85EB84AFDDF3F02AD9EB7,
	AudioOptions_Dispose_m1648ABAF6E83AAD3BA2ED4E491191C2286F674DC,
	AudioOptions_set_echo_cancellation_set_mA86B7818E22B47D25A2446299BD3893EC89EB041,
	AudioOptions_get_echo_cancellation_set_mC2B567567DA7D12903F2F5BEF33F41AAC4502483,
	AudioOptions_set_echo_cancellation_mFF1AB47F4F8EAC70EDC8E0EA83FCECBBD64A0E0B,
	AudioOptions_get_echo_cancellation_mD9020BC7B58BC847047B285B170D64508634A8F1,
	AudioOptions_set_extended_filter_aec_set_mCD247F2661CB3878608D821023F8E3E16FB82324,
	AudioOptions_get_extended_filter_aec_set_mAAB3B4A7EE121D35E0C46544B997C9AB8FF9218D,
	AudioOptions_set_extended_filter_aec_m3BA270C429F474183DE15F46AB2FB41841B0D684,
	AudioOptions_get_extended_filter_aec_mCBB98D18FFBD04507DE4C7A0642DB6581B4AE5E4,
	AudioOptions_set_delay_agnostic_aec_set_m920C066C2A2C0B1B764F5228B206895C0F895502,
	AudioOptions_get_delay_agnostic_aec_set_mE8D66B4CAA8C8147852A60FF009567717BF05FDA,
	AudioOptions_set_delay_agnostic_aec_m548F7ADC512B2F2FA182FA18AC4DF03260E1C545,
	AudioOptions_get_delay_agnostic_aec_m81A357F21D10C3176DCC03F53248EDA50151CDC4,
	AudioOptions_set_noise_suppression_set_mA5ED3C1F55FCC4374590E044836606479D422A04,
	AudioOptions_get_noise_suppression_set_mB9CA79F8C64A8351A9C0356C9050445CF97E5FC0,
	AudioOptions_set_noise_suppression_m99E93D9641EBC6D644043CD8444D756875DE7E01,
	AudioOptions_get_noise_suppression_m455D51BABEC732950F8BB11E41EE7368BD1FDB90,
	AudioOptions_set_auto_gain_control_set_m89A511418D68B9DB948822DEE1141552B26C210C,
	AudioOptions_get_auto_gain_control_set_m83094654D0156176A0A409F017F1DBB65092E8BD,
	AudioOptions_set_auto_gain_control_mBD2DF568908573E391EFB01B09A72982872CB5BF,
	AudioOptions_get_auto_gain_control_m8872A029B91A27FFD7DD5B78E4F5E4D84960791A,
	AudioOptions__ctor_m959D928A19668442F89F05032B046723A76B72D0,
	CopyOnWriteBuffer__ctor_m178FCED34CF8A75D6986B88FB0D68A7A1D58FB5D,
	CopyOnWriteBuffer_getCPtr_m460A44F124916129D36F2D838B63B109F84F0E3D,
	CopyOnWriteBuffer_Finalize_m2ED43C6119845D43487132DC2FEA39B1B2315D99,
	CopyOnWriteBuffer_Dispose_mA371BD136886AAE6E1E5A342DFF95F2FC5802171,
	CopyOnWriteBuffer__ctor_m9C2A8757B71ECE4EB6AE727C848AC8BF4EDC50AD,
	CopyOnWriteBuffer__ctor_mE0ECDCE9C6040E37131FC710167139630DA04477,
	CopyOnWriteBuffer__ctor_m0DC48FFF0CB2FF6FC4C4F54C04F99B38D32525E5,
	CopyOnWriteBuffer__ctor_m52F22B2A29F927CA81D79BDE9B287663CD6F8762,
	CopyOnWriteBuffer__ctor_mCF43DEE25E6C1D007D88B9BC1351ECB431FFE2C3,
	CopyOnWriteBuffer_size_m1EC084659E2B601964908C397FD99CEFD4EC885D,
	CopyOnWriteBuffer_capacity_mA0BA1CC5C95D4AFA1FC338EC8893C39AEB86B3EE,
	CopyOnWriteBuffer_SetData_mBA19C0B8C50B92969807D09F584B83D65D55754C,
	CopyOnWriteBuffer_AppendData_m27A3C21130139FC0465D01C39C9638FE481A4266,
	CopyOnWriteBuffer_SetSize_mEBCE32D3D9EA0C2732E0AFCAB0288CE9C49FB909,
	CopyOnWriteBuffer_EnsureCapacity_mFD5D144C4B48041113FDD39A35A6C88BAFB3FF40,
	CopyOnWriteBuffer_Clear_m1ABCCBF8B3880EE50DC2D2F877A8940E56AF8EC4,
	DataBuffer__ctor_m3A9184D650712D88FBD9EDF0C7FA7C52509015DA,
	DataBuffer_getCPtr_m79A560F93A90862B2787212B3722D569C4C904C4,
	DataBuffer_Finalize_mD4464E0092D91CB5326E0845900E4E9A6DAE1506,
	DataBuffer_Dispose_mA89C22FFFE39544383ACB2047E689B45E0B4A558,
	DataBuffer__ctor_mEE9638CD6A7C1F110B1EB8298669C6D4FFC42D5C,
	DataBuffer__ctor_mAD6DCA0A88C8371A987A099ECC201D0258FD4B4B,
	DataBuffer_size_m03FD715A15BB89D22C25A340B01A0F92205472AF,
	DataBuffer_set_data_m520FF9B62402D4AE05BC61426DF073BBE32AEE97,
	DataBuffer_get_data_mC0AA16E14EDCCAD2F4438C2BA0C77CF7489BE1E6,
	DataBuffer_set_binary_m74FE93602C11CE5EEA2561DD5C9717A6E5B70230,
	DataBuffer_get_binary_mAFEC325A6249CE53EA8A9E4524F0B68273801D25,
	DataChannelInit__ctor_mABE44453EF9FA29C340DFDA29DC34DD7A34F68AB,
	DataChannelInit_getCPtr_m19E2FD6DB1FADFC8986350F746F3B48110B490F4,
	DataChannelInit_Finalize_m942BFA54DDECE6832A585BCCD2A43192A4ADB52E,
	DataChannelInit_Dispose_mD2769CCD4E69D7E97E8806B81F20A9EE1698396C,
	DataChannelInit_set_reliable_mDB7157E2EEA34CCB488C288A3B24C1AB00214DC4,
	DataChannelInit_get_reliable_m1036E4033A741C94490B3936A64AAF7A16F7007C,
	DataChannelInit_set_ordered_m556EC589482B3FE973FC2E308682349757CA56C2,
	DataChannelInit_get_ordered_m37DF521854E673D65168700755C40445893576BD,
	DataChannelInit_set_maxRetransmitTime_m2609AD3FBD62AFE4FF79E6BF8C8D9AEE85663DD5,
	DataChannelInit_get_maxRetransmitTime_m98E6FD4EC11D3BE8CB43EA0EFDF677BA66BCF008,
	DataChannelInit_set_maxRetransmits_m721FD5B3F7FA3946B1B74AA3C89CEEC5FB29B51D,
	DataChannelInit_get_maxRetransmits_mD682CC03A85B9BEAA1B3B93031DF24FBA5354C84,
	DataChannelInit_set_protocol_m50D1E9DD76072F6E07F02A17B89230FE77445C83,
	DataChannelInit_get_protocol_m11CD4FDC24122F25C274FD8FFFA33F17C924A03D,
	DataChannelInit_set_negotiated_m90D1F850073EBF5F9947E09022B0EF55900A3560,
	DataChannelInit_get_negotiated_m46D5094A5CF67363C804676B972B232EA890EAF1,
	DataChannelInit_set_id_m15ABECE1073E2A0EB1589ADD82A10E91D1BF5521,
	DataChannelInit_get_id_mA5757968354AF6D79764F7325D6A9542F8EEFDEB,
	DataChannelInit__ctor_mAE4407E7CF996E6F2A381EC2A0B361817614265D,
	DataChannelInterface__ctor_mF946CE31FA7CDBAB9A6410E78D16CC1BC0BF1B2F,
	DataChannelInterface_getCPtr_m3C24141E819219BAE39B63CA90AB171A3710D8EE,
	DataChannelInterface_Dispose_m2A7D30178488D48E0AF75670F3AFBFA2116878D3,
	DataChannelInterface_DataStateString_mB6961A812EC0ED6848F6D814D145DCC45BE3D381,
	DataChannelInterface_RegisterObserver_m7B14DBCAE730C54814E65AF2C53F7F78BD6A856D,
	DataChannelInterface_UnregisterObserver_mC1044D982281146186557A4E92760D55ECF4B592,
	DataChannelInterface_label_m8312A5EE6594477A8760C98A54A020CB350CF75A,
	DataChannelInterface_reliable_m79D93ACDED86EED25C2422827EDE07EA4E71E357,
	DataChannelInterface_ordered_m6F33EF6FE8A53B5529A1A662AE6F9B3B94B66FB3,
	DataChannelInterface_maxRetransmitTime_m8F2AC9EB53EE53953119E3129E7338124242C15C,
	DataChannelInterface_maxRetransmits_mD30284715BFFBFB99711262E7D459DE06ED833ED,
	DataChannelInterface_protocol_m5D457E9F5B1B1DAC6F406D493AC524544332158A,
	DataChannelInterface_negotiated_m02C23887CE992923B13A7A8C5E695B8BEB42E3B4,
	DataChannelInterface_id_mCF06FF0082CB10B6F4E3AB5C51C13ABA137A54E0,
	DataChannelInterface_state_m38500733401D79E311573D57E7495179AAB93350,
	DataChannelInterface_messages_sent_m0F91709C068E4074B124EC11D8465C0A1AD92156,
	DataChannelInterface_bytes_sent_m0F81C14230C2FBD141A6C09062CEAF73501C0478,
	DataChannelInterface_messages_received_mB4B586CBC3FEF526B844B41CF23E74EB3F383363,
	DataChannelInterface_bytes_received_m739F468428A1D2D12DE4B03F275F570B685EBE0F,
	DataChannelInterface_buffered_amount_mE728FDE12E371813C7C11E8C6EB077413892A399,
	DataChannelInterface_Close_m20B06F2ADD4CD850EB07ACA154DC04528647BCFC,
	DataChannelInterface_Send_m39A3C3A3473712AE0A18846EA12440E6BB428D1C,
	DataChannelObserver__ctor_mACBD9FC16EBA3F7871D8955BEAF05B61188A1A11,
	DataChannelObserver_getCPtr_mB19F6EB923AEAC286371FF8B92A2FB38E608CB0E,
	DataChannelObserver_Dispose_mB439E00F179FD8B0B3135BC88696B74436A2826A,
	DataChannelObserver_OnStateChange_m7374EFD945C7C70B22DE61A5B23DD52C3CFE0C4D,
	DataChannelObserver_OnMessage_mA62F84E472EB7C043976CB03170BAFAE33E5F625,
	DataChannelObserver_OnBufferedAmountChange_m79C65D93FE93DF10962E51F51087DFC6350690DA,
	GlobalStats__ctor_mAD91CCEEFC9F6B4818F323D3A5A267EE36B42147,
	GlobalStats_getCPtr_mC3EABC6594D8E9622302AEBFE5686B4CA9C7EC07,
	GlobalStats_Finalize_m400F1498DC7E11B97096322437FE1F6454AB6535,
	GlobalStats_Dispose_m0428F3826F9DC06D6C4864E43D795AEDB2582958,
	GlobalStats_SetActive_m49D79838C34E66C2DED57180FFFCE2B902BE14A1,
	GlobalStats_IsActive_m17105CF3A75F6A6DC87154B09F5852B4209C6944,
	GlobalStats_SetRefreshTime_m66425DEABDCAB60AA155CB7979F7225D2C6118B8,
	GlobalStats_HasStats_mAE771771514B9950945324BDEBB1BF1B126DF40C,
	GlobalStats_Dequeue_mA40DD183EBD7DE1BA00998670E75748DAD14B2A5,
	GlobalStats__ctor_m56820FA68AE50F23192400263A058F1AF1654EA0,
	IceServers__ctor_mC2803014BCA0FBF46B8918FD33FF84EA7934A0EA,
	IceServers_getCPtr_m98772C9B231B814F7FC0F4606507C8B7CAE299E3,
	IceServers_Finalize_m923E104F501DD227937323662400025BCBCAF048,
	IceServers_Dispose_m2561D4367044F44CD38E4CB0CD97EE66FDA1C9DB,
	IceServers__ctor_m5D0AAD5EFF2F4156C9789038C32456A7289DA2A6,
	IceServers_get_IsFixedSize_m5E6BFE856EFC9EC148700F3857661220BC005E2E,
	IceServers_get_IsReadOnly_m422F25C939A71766B5878FC1BFD08FF3281DA146,
	IceServers_get_Item_mA7ABD6955D404F248C74B686BF359011804DB2CB,
	IceServers_set_Item_m0D8C91E3F64136CAA8F81C711C36EE2AD117CC84,
	IceServers_get_Capacity_m509B85E918C9D39CE7560C21FB6E6F85C413F48D,
	IceServers_set_Capacity_mF9EB75189E55F31396C56DC20441712D136C77CD,
	IceServers_get_Count_mD35F7670A35EBCF99F86A0B8EEAA22AC9F50DBB1,
	IceServers_get_IsSynchronized_mE7BA758056D93934515B07869107EFCEC46EBD59,
	IceServers_CopyTo_m52F96F73C5FCC6774CD830E4FDD97D77A886E0E3,
	IceServers_CopyTo_m7A744766C6CE6C883FDCE078776A709DC2AFC6F0,
	IceServers_CopyTo_m40666D8195235F4F579AA938D7D74CE8483E2F47,
	IceServers_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CWebRtcCSharp_PeerConnectionInterface_IceServerU3E_GetEnumerator_m1F14031F1F27A27EE83F156C90DA25ADFC8D9E44,
	IceServers_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_m39A8EA7B1B227C7018C665BE9D872394596FEA8B,
	IceServers_GetEnumerator_m9366E5933BB2BD0A36F5CB32E31C11E909042346,
	IceServers_Clear_m950554A0EDBF856B2291545961795A73A35EBB93,
	IceServers_Add_mE757CDB38F298BBC0A39BEE332447ED87F840BFD,
	IceServers_size_m2B38F0916D8B4A6A222ED24E3AC91850E738F3F6,
	IceServers_capacity_m0EE7024CB0F19EDF6BDD3B9916253602408D7040,
	IceServers_reserve_m323575D62CB8A8EF9637F87036469EB64D6BD55E,
	IceServers__ctor_mE1B96AA1CF11BA42D9C32D17D24753C35EB1E5CD,
	IceServers__ctor_m1A6581330F23C870769436FDC38A45FD8385E515,
	IceServers__ctor_m8D9D1BAFA2AC76F46D029065CF97451980DC8916,
	IceServers_getitemcopy_m773F0203F601E7DEEEBEF503DC8394EADB8A5FFD,
	IceServers_getitem_m5272E4F1B69FFC54E238577B27BAD728D4A827BB,
	IceServers_setitem_mF9F8CF571F70CC0D38ABB0523DAEE7BDCC425A5D,
	IceServers_AddRange_m5C6EF8A1EDF634DA719152B3A5815203D3ECCD89,
	IceServers_GetRange_mA5D1621641E185A4122C83C40247AECD34312575,
	IceServers_Insert_m09DF1901FEC4CF7AC0B4138495063BA5DBE9907C,
	IceServers_InsertRange_mFB5ADB0D0FC3799C7E5E4D4E6C193DE1E6F08C53,
	IceServers_RemoveAt_mBAA038BF0C00188CA7D75B4397B407B5D5B45220,
	IceServers_RemoveRange_m58BC8E433827CA58DC6806EE08C594D8DA3E81EB,
	IceServers_Repeat_mBE146BEE6437A5AA5DC1302B3884DC02521C068E,
	IceServers_Reverse_mE96E75782C4C619CD7CD59EBD097968F2D8CE923,
	IceServers_Reverse_mBCCD7415ECBC33FEA1BF5B49179D0A9B573F73CB,
	IceServers_SetRange_mD5F2146476754D4E27059D665B9D181C0EF49D43,
	IceServersEnumerator__ctor_mCD61CF0990325DBEFC2CDD92378F95B613C3DF56,
	IceServersEnumerator_get_Current_m3A1F66EEBE36B0456DABA198EE71FB05AFF0DAC8,
	IceServersEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mC386DE830EE0A31201E9142F389AB92F922F849E,
	IceServersEnumerator_MoveNext_mF28C4E631C33CD18CBB2190B04BA9FAD44662510,
	IceServersEnumerator_Reset_m80B28F70AAADFAC375A82E8ABE87E6E6EC33D182,
	IceServersEnumerator_Dispose_m57656E6C973E0BCE61B2E66C46F340604315FD1D,
	IosHelper__ctor_m00C8C21BEA2C1DB3C5E792A0C86A8E190C9DAC46,
	IosHelper_getCPtr_mA833E9312B1E14B2F9BCF8651C4718D4D61983D4,
	IosHelper_Finalize_m1AC19A5DB6382464E948BA83FCD996382FD7DFDE,
	IosHelper_Dispose_m0BC986B9C4FA88B03C1090C8D9D83ECB942E3621,
	IosHelper_IsAvailable_m574904F801A2363C24E9A368865CC559916ECCC7,
	IosHelper_InitAudioLayer_m92D687B8BB4DDE5ED88A501B96D14A8E48CEFF65,
	IosHelper_SetLoudspeakerStatus_mF36DD2FC9697DB14911E869D4A54AC00B365FE45,
	IosHelper_GetLoudspeakerStatus_mA1D8D295CECF15D1493A3BB4B73F00F0CC6B2CD5,
	IosHelper_IosKeepAudioActive_mFACC298B808D20D21EEC60CE095FBE7B52798D48,
	IosHelper__ctor_mA89E962F7FFFB2923113FA2422C57527F76DA163,
	MediaConstraints__ctor_m866380CE81B93DB05BF311253CB8B474EB5D8007,
	MediaConstraints_getCPtr_m78384A67058BA895902BC4C141436F3D1C9195BC,
	MediaConstraints_Finalize_m1EB709040E676E8D0EF8F06C5818BA3846FCE1D1,
	MediaConstraints_Dispose_m792FEE24EE43278BC0A449EACA4FEE4B021C78A0,
	MediaConstraints_set_audio_m34996BCDEB475423D7518D12F6F6A7C651CAD880,
	MediaConstraints_get_audio_mD0982EC766D7CA0BB9C5339E00894E2DE607A41D,
	MediaConstraints_set_video_mD4EC8D7545514FF1580D85B1243F4102923E622B,
	MediaConstraints_get_video_mA0E7FE9015D1E6E2FC3A30CFC8D016A99CB008E1,
	MediaConstraints_set_videoDeviceName_m1C0B2B31A70BF6D85A69ADF757F83BEC93A5C3EF,
	MediaConstraints_get_videoDeviceName_m7116AFDA2D870DE244D7372A6D7C73CCCBBB4C2A,
	MediaConstraints_set_idealWidth_m7ED671A1907B7DFFB3BC8D3A64C22D54BE115C64,
	MediaConstraints_get_idealWidth_m53AE277402C5ABD7B53B498A4C67AC0907A18BBB,
	MediaConstraints_set_idealHeight_mC6D08F631DBC10A58E1B7E8C02A2E67E877E9173,
	MediaConstraints_get_idealHeight_mA21CB7FA3B8EDECC3B3BB87BC735F35ED04FB304,
	MediaConstraints_set_minWidth_mA9420AD87C0F57060F37126D1E9D72465785824F,
	MediaConstraints_get_minWidth_m56C1491CD6FE5FBFE9EF5F13F38D873442322318,
	MediaConstraints_set_minHeight_m59898D0DDD0D0E01DD01ABF66E51F76C8A3E2603,
	MediaConstraints_get_minHeight_mE468E84B3E6D4FD075ABAE1DCA3600D6C027D65A,
	MediaConstraints_set_maxWidth_mC4B19DE0C4487C7BE5297913E54822D4A02DB8B0,
	MediaConstraints_get_maxWidth_m8491AC6F1A3845D5071873390E5395B8F0A0E89D,
	MediaConstraints_set_maxHeight_mA37FFD0FAAD83481A596678DA2F5EC0A6DD3A55F,
	MediaConstraints_get_maxHeight_m7D69B6BE9B0A67069A83DD0CD0E241DAD1365924,
	MediaConstraints_set_idealFrameRate_mB77BA994CDAD34BB3F21AB1DE22D61957C384F3F,
	MediaConstraints_get_idealFrameRate_m285207EB37795CA9D7B415D1553CBAE7DCD922FE,
	MediaConstraints_set_maxFrameRate_m9ECBA395C17220DEB3CAF42579DAE3A20A191295,
	MediaConstraints_get_maxFrameRate_mFC432371763F8BF879158E0A97E9EC159A86BD53,
	MediaConstraints_set_minFrameRate_m74D17C1171138BA54872EA407DB59D626F90F516,
	MediaConstraints_get_minFrameRate_m5325D9ADB2023D117D2BFA6B32B0AB96017DA837,
	MediaConstraints__ctor_m5D709FE5CD7567F77ABBC4459CE8DE6A0BF7B3A9,
	Message__ctor_m89237E352D96A6543F440E2F8A8DEA7DD2A3A926,
	Message_getCPtr_m7E138B00179CA6FBBEC7420B44C120F91146F86E,
	Message_Finalize_m21CF408DCB90F9B585ED0A188E19552CB827E62A,
	Message_Dispose_mD1BE76908B93B2916CF1A3C7424AC56021D3B9F0,
	Message__ctor_m37989203344B8A5A839398EA86C17FF441E77ED7,
	Message_GetSize_m9AD3DF7CC192607EE0AAD46235946D613DB95333,
	Message_GetData_m51ACD93291BABB3BFFF17AB058C383A0E451BCC5,
	NV12ToI420Scaler__ctor_m9244720F1DC182922239D745C0D154686E437DE2,
	NV12ToI420Scaler_getCPtr_mE13D07AA760B18A1738C395355A92D2AB38295A6,
	NV12ToI420Scaler_Finalize_m8505803161EDF8CF3751A92C8F69C91D4223CD9F,
	NV12ToI420Scaler_Dispose_mC899617F5CF278944BA95AE9F48FA0E152D26B5D,
	NV12ToI420Scaler__ctor_m938020211669DDF68491738BD67F0F2D83E30300,
	NV12ToI420Scaler_NV12ToI420Scale_mD3F4D48DAD429B3073E212EFBF027CC59A5CD4F9,
	PeerConnectionInterface__ctor_m4283CD1BAE853C7C6048F4356C4E86351A209F10,
	PeerConnectionInterface_getCPtr_mABA0DFA24718E24E072DD2A45C2FA002E6CDE300,
	PeerConnectionInterface_Dispose_mE8CCDA7694CE0D0123F825E1FFED6FCAA0DD793E,
	PeerConnectionInterface_local_streams_m47A10FB7E5DD12A59037CD6AE7E6061893A34705,
	PeerConnectionInterface_remote_streams_m68DBECB6629E4CC48B7BDCC7015C819A202D22B9,
	PeerConnectionInterface_AddStream_m259FB6C5FC7FAC4951D9DBB15F2891AC57E4256E,
	PeerConnectionInterface_RemoveStream_mCDD2875F8B68AF56A53D8425F8B178D1C1D78470,
	PeerConnectionInterface_RemoveTrack_mA29D378F8340F2266093AC843B424ECF49B90168,
	PeerConnectionInterface_CreateSender_m312C6FE40BF084C38DCB0E20F155273813D0CFC5,
	PeerConnectionInterface_GetSenders_m93BFF74F3850A9E53917D896C72018B95EDF32C6,
	PeerConnectionInterface_GetReceivers_mE5F6FE5A2C55DDD8C8C7AF91BFB2D9B86BEF1DD9,
	PeerConnectionInterface_GetTransceivers_mD140934B2D10434CA62A0F046FC9EAF3EFCC3BEE,
	PeerConnectionInterface_GetStats_mBFB968D0ECF853DFD35B85DB363E168940078D5C,
	PeerConnectionInterface_GetStats_mEE38F41756D1CE723FC5D1E0B7982BCA400BB7A2,
	PeerConnectionInterface_GetStats_m6A82BD5C2A985163FB0F41B268E9CE7ACE04A7EC,
	PeerConnectionInterface_GetStats_m80754975CEFB2FF92111FDDEFD439EC772E52BFE,
	PeerConnectionInterface_ClearStatsCache_m822BC7F1AC9D816FDC309D3602ED5E373C0481A9,
	PeerConnectionInterface_CreateDataChannel_m9EBC96C45F008CCC8345FEFA4F43C38C63652CC2,
	PeerConnectionInterface_local_description_mF2449D57A3C8062A7CE9FB80940BF3F02C3AF563,
	PeerConnectionInterface_remote_description_m2411A05DCE8BB5E75903F69B1EAFA765F206BDF8,
	PeerConnectionInterface_current_local_description_m3BD6503504EBC401931EC8D08FFA584809331511,
	PeerConnectionInterface_current_remote_description_m0743BB57BFA4349C4217F28B42BC656501332299,
	PeerConnectionInterface_pending_local_description_m4519DB9733BC7AE9C6B9258EC36A33768AC88605,
	PeerConnectionInterface_pending_remote_description_m6BDA057C33A082A632D7E3BDD75FE52F58B7A1E7,
	PeerConnectionInterface_CreateOffer_mB1286C2D1DB966A755EE6DA2B6C5BCA3A2202D93,
	PeerConnectionInterface_CreateAnswer_m51AB2DA2E4979991DEF859AE9A75BDD36DBD6047,
	PeerConnectionInterface_SetLocalDescription_m3F5D20182A0A43FEA5B08562D7182DDB70F4390B,
	PeerConnectionInterface_GetConfiguration_m7CED4B814AAEC6024C51E214507E1469B8048EEE,
	PeerConnectionInterface_SetConfiguration_m13942C439D1449B39E1820EC9B742F1D91AA62D9,
	PeerConnectionInterface_SetConfiguration_m796C906CCBC5198F9F3545F61C6F5057102A26AD,
	PeerConnectionInterface_AddIceCandidate_m6A6319F7F47BA01E8BA6ABDC031B846D3C86DB7C,
	PeerConnectionInterface_RemoveIceCandidates_mE1B4D0D9A419FA6D6A9CE783FEBBD98C0F1E356E,
	PeerConnectionInterface_SetAudioPlayout_m5B14E60E75534953C32BEAFBCD0B28BAF26C65A3,
	PeerConnectionInterface_SetAudioRecording_mE91361C2A745848A3F8B61ADFB8CCAE63275147E,
	PeerConnectionInterface_signaling_state_m606ACE57E2A19061AE94E7E79A7484951A5F54AA,
	PeerConnectionInterface_ice_connection_state_m6171601BFCDF77B007A9A92BCFAFA3054ADF1FB4,
	PeerConnectionInterface_peer_connection_state_mA11AB2C4BE7661EF59F73624B680DCFD73AF2FF5,
	PeerConnectionInterface_ice_gathering_state_m76A8AD85C6D18C002BDF32C54AF52C5E83EB784C,
	PeerConnectionInterface_StopRtcEventLog_mEF83C4509EE490EFDF37D87FF11E29A78A44F13F,
	PeerConnectionInterface_Close_mD5265B50A19399641BE69263C5DB85292F3CA13C,
	IceServer__ctor_m900596C7F05D922465D62CDD05067ABEC01DD0AD,
	IceServer_getCPtr_m8D83DA46593711F7140E7D0506477FBCB8D9E7FD,
	IceServer_Finalize_m5E3C70BF13B413AA202601D09E50C833F89EE2C4,
	IceServer_Dispose_mE7145513E62EB3A44175AC62034B4DBEC35A02CC,
	IceServer__ctor_mEB025BA812A0AC5B4BFD1F59B8671AEBEB24CB86,
	IceServer__ctor_mDE714E11DF06F7952DB31A0BD3398B0C5C21A3E2,
	IceServer_set_uri_m65AEC2DDD98BA3A6B7B9A09D886AF3BADAC5AFB8,
	IceServer_get_uri_mA13AFDF14D2EA55E9036D8969EFEAEA85407A20A,
	IceServer_set_urls_m74F453AA769D0C50DF0201F6AEBDB67950F886FA,
	IceServer_get_urls_m4028E7A76A9626A61AA59467B1018FC5EA2DFD85,
	IceServer_set_username_mADC5FF93D2502493F6A785E27F7ED19EEE430CD8,
	IceServer_get_username_m252A57DB19973FF37DF5B2B81BD6AD1F7C35081D,
	IceServer_set_password_m6876E3D21E169E4EB1BF652749E79A8A44455B42,
	IceServer_get_password_m00A21795B436339DEA388DAA7570E0DE5E7D5A41,
	IceServer_set_tls_cert_policy_m2135F0C1C2ECA4AE961E5EC21BCD6A61BBBCC012,
	IceServer_get_tls_cert_policy_m862FBA61E1473013D61F021688052FF050ED258A,
	IceServer_set_hostname_m11AFE418C877D1B6792538488F61FCA8260297A6,
	IceServer_get_hostname_m7954FACB2B18AA4855E65C9F24866F1B15604D69,
	IceServer_set_tls_alpn_protocols_m34882B2565D7C1C33F42F0E7562F9369837B6460,
	IceServer_get_tls_alpn_protocols_m1E60AEEF523588BE15E29DB2BB48C52C322CD931,
	IceServer_set_tls_elliptic_curves_m4416ED2533E010757015C87EBF97E146DB0636D1,
	IceServer_get_tls_elliptic_curves_m61325137ED0024FC8AEBFB75BED9014E727CF61E,
	RTCConfiguration__ctor_m1F9B671F80EA3B0863565449B7E1E33B1C83CB96,
	RTCConfiguration_getCPtr_m6F1F6CB416659B654526DD0D508EB1BAF65C9E9B,
	RTCConfiguration_Finalize_mE3F659BD0D16147CE1F5D7573C2317CA045D4E9A,
	RTCConfiguration_Dispose_m62F000D1B83433445BA5C625B76616F11E03EC53,
	RTCConfiguration__ctor_mA5A485A24AB4A9088D0F6E2FCA96AAD29D07E690,
	RTCConfiguration__ctor_m7B515BA8018191561E206DEB9ED35246CDBA6D0D,
	RTCConfiguration__ctor_m04F007212E7161D054F17991C3ECA1ACEA96C81C,
	RTCConfiguration_dscp_m91D7FB0A47B3E8D3346A9B09CFA08B54C7CB9D1E,
	RTCConfiguration_set_dscp_m845A5D427C3408535721579D8BFD4DC47725F432,
	RTCConfiguration_cpu_adaptation_m9A09526D037946C484451F600478FC18702BD5BC,
	RTCConfiguration_set_cpu_adaptation_m254598EC1EAC5B6D6AEC50EAE249DA41F73743BB,
	RTCConfiguration_suspend_below_min_bitrate_m505E1E8DF36B558F9DD790B7A910637B981705F9,
	RTCConfiguration_set_suspend_below_min_bitrate_m70AF85CBF9BC5DCFFB809BECE897504302689376,
	RTCConfiguration_prerenderer_smoothing_mF4108DF4A5EBCA2FCB3E87B6C21E9D29913634D9,
	RTCConfiguration_set_prerenderer_smoothing_m6D3197B240E733ADE163631D038E8A51F8314C86,
	RTCConfiguration_experiment_cpu_load_estimator_mCB1F88D443215A334FB1C47D387A692B006D5AA8,
	RTCConfiguration_set_experiment_cpu_load_estimator_m43D916D6C42C98E311C7016BEE840CC3DAA5CB36,
	RTCConfiguration_audio_rtcp_report_interval_ms_m78B2FFFDABC43C974E2FA951A45CD22AE3094F75,
	RTCConfiguration_set_audio_rtcp_report_interval_ms_m012C4F82625EA71F4E061415D18E652DE5B2FE0A,
	RTCConfiguration_video_rtcp_report_interval_ms_m53CA0BA5AA0AFBB0EFCECDA237D93F09AF5D4470,
	RTCConfiguration_set_video_rtcp_report_interval_ms_mEA562E4D34D4FB15D9B62F180B0FFE70309B2E3C,
	RTCConfiguration_set_servers_mFED9C6CE275C5DC4A94628A48D9B0CB45DF729CA,
	RTCConfiguration_get_servers_mF9AFF8C786FB68FFD89061850074C409EE011D6F,
	RTCConfiguration_set_type_mEBB1F5182D751568667B252D6282CA9750F9D688,
	RTCConfiguration_get_type_m06BB09DC44CAD7E3AD8F146C2D8519B8B9EC92CF,
	RTCConfiguration_set_bundle_policy_m4136D582FB52192495A66D7FC63FFABCC459FF36,
	RTCConfiguration_get_bundle_policy_m2A6CF293F9DB8D683108574D9F674F9A82B7168A,
	RTCConfiguration_set_rtcp_mux_policy_mF92FDED977E892307F8F0EF52B480A8DE269995A,
	RTCConfiguration_get_rtcp_mux_policy_m9B6591915C9B2823177B4AD9F21043EAAA01AEB2,
	RTCConfiguration_set_certificates_m9D7137C8B07F4EB4E590F6D25085AECE140D2E07,
	RTCConfiguration_get_certificates_m00576B89F21BC2F8C7277BD1F63826188D921EA4,
	RTCConfiguration_set_ice_candidate_pool_size_mC3109B8259D66ED9B82E376D4705775D4081F865,
	RTCConfiguration_get_ice_candidate_pool_size_m245232AFC0AED8934AE3CA5C4574197712606E98,
	RTCConfiguration_set_disable_ipv6_m7D88485C6E4E5DE8CC2508D997520479AD184634,
	RTCConfiguration_get_disable_ipv6_m5B3EC12D91F555E3BB12AC9DD4A3008B6FBDE076,
	RTCConfiguration_set_disable_ipv6_on_wifi_mBAB53EF4CF698E5A3618D0C94A6372F29108CDB7,
	RTCConfiguration_get_disable_ipv6_on_wifi_m35F6A7209E625E57DE29B07BB200FE3ABA818466,
	RTCConfiguration_set_max_ipv6_networks_mA60DE4013A3844D72B99CEB7CCAF8D48F076DFBB,
	RTCConfiguration_get_max_ipv6_networks_mA9F838C7EF2C3232EC1DAB445C640CDFE7EB7BA9,
	RTCConfiguration_set_disable_link_local_networks_mF60F72FD3021C8784CE6431799B3CFC24089D32B,
	RTCConfiguration_get_disable_link_local_networks_m88A4A388064EF30D2CE13B00B76956CF6F821254,
	RTCConfiguration_set_enable_rtp_data_channel_m64B3828FE63D2400D7131F0179F4FE5FCCB43348,
	RTCConfiguration_get_enable_rtp_data_channel_m0C25662A0A8E8F36857A36CC18DEE824A0451267,
	RTCConfiguration_set_screencast_min_bitrate_mA5841B7A040F6DB93320D9DFFAF499A78946035D,
	RTCConfiguration_get_screencast_min_bitrate_m29E7325C8B7CA74CAB69B354A69F00F5B091B2B6,
	RTCConfiguration_set_combined_audio_video_bwe_mF17CD5181D399BC191CB046CA4E62E611C049BF1,
	RTCConfiguration_get_combined_audio_video_bwe_mB00CF245CC4C596E555150FD404F2F346DEC11C8,
	RTCConfiguration_set_enable_dtls_srtp_mD64192A066DB9E44817B9102234C7E82AE70D573,
	RTCConfiguration_get_enable_dtls_srtp_mA343512EB8609493B5F04EDA6A5FED1A29416F4B,
	RTCConfiguration_set_tcp_candidate_policy_mDE95DF3BC29DE486674F4B15359541EBA43057DC,
	RTCConfiguration_get_tcp_candidate_policy_mC79F2FC0B3A6C7D62D6ED2336B518D46D49CB494,
	RTCConfiguration_set_candidate_network_policy_m083B5D153A43C889B0A60E019E7E2E63395FCE76,
	RTCConfiguration_get_candidate_network_policy_m589BDD3F70A514992A8C51AD3C98B84E564A4FBA,
	RTCConfiguration_set_audio_jitter_buffer_max_packets_m7FAA7A18F2C0E78D3FF5ED2A478D25F590C6BD01,
	RTCConfiguration_get_audio_jitter_buffer_max_packets_m1EDE918F6C89710764294028178B9296083976F3,
	RTCConfiguration_set_audio_jitter_buffer_fast_accelerate_m3AC2E2CC869E9F5D7FEE353A1A885B9DFA26FA39,
	RTCConfiguration_get_audio_jitter_buffer_fast_accelerate_mF03E1D1F48ABA0F50B3FD92E7DE9AFA0BF81EA54,
	RTCConfiguration_set_audio_jitter_buffer_min_delay_ms_m7B201D55856245513B26455FA3FF18E7A5DF0164,
	RTCConfiguration_get_audio_jitter_buffer_min_delay_ms_m16125C0D394439E8C008FE242BC4ED124D3D7075,
	RTCConfiguration_set_ice_connection_receiving_timeout_m486D66A285B4C2A5FD4F8240D658D507496C4CFC,
	RTCConfiguration_get_ice_connection_receiving_timeout_m32B98711C320CABD1A23F6DF29227EB60231A77D,
	RTCConfiguration_set_ice_backup_candidate_pair_ping_interval_m881884455C19B24CDD309C299093902379C4707B,
	RTCConfiguration_get_ice_backup_candidate_pair_ping_interval_mB765114BEFA6FD97E7CC414968534EDBC02ED589,
	RTCConfiguration_set_continual_gathering_policy_m2BAC226D94230FB1CB53666DE2134ED9798FB64E,
	RTCConfiguration_get_continual_gathering_policy_m0229EC1A18D5EB7BF2A892C880AC01BF6F7B0B27,
	RTCConfiguration_set_prioritize_most_likely_ice_candidate_pairs_m7FD71F468397DE73509C45B07285F0232315025B,
	RTCConfiguration_get_prioritize_most_likely_ice_candidate_pairs_m6E02448936415E7E69703CE278D74CE885CCB857,
	RTCConfiguration_set_media_config_m37DF461993253C095EE8B944E6B278C096DC2A13,
	RTCConfiguration_get_media_config_m03AF6B8B19A0753F86183DFB7562308881D6F4FC,
	RTCConfiguration_set_prune_turn_ports_m000B9ABF8A62AF41C77105D4268CD9CE395A3EB2,
	RTCConfiguration_get_prune_turn_ports_m95F6F95A9D6997E4EB55057D47B980A80D3D0FF0,
	RTCConfiguration_set_presume_writable_when_fully_relayed_m165CB3439F2ADC9F8846D3240AFF868577A827E5,
	RTCConfiguration_get_presume_writable_when_fully_relayed_m39FB629C7C63656E9650CA1DF61DFD2B8DA84E52,
	RTCConfiguration_set_enable_ice_renomination_m32CC4634A13D00E975EF7D444AE78FC1F84F0C5A,
	RTCConfiguration_get_enable_ice_renomination_mD20FC716DB659683481EE0EA669B8EBA13FF1839,
	RTCConfiguration_set_redetermine_role_on_ice_restart_m0A6CEC0C3492979ED5F7B5A2C3272357D38801C1,
	RTCConfiguration_get_redetermine_role_on_ice_restart_m7155A21F3F759EFBE43331C8E6720DA8487433B0,
	RTCConfiguration_set_ice_check_interval_strong_connectivity_m91C4398E4D6FFEDE7B85963A02447F8A697B504E,
	RTCConfiguration_get_ice_check_interval_strong_connectivity_m7100806652E7B1928D20D8EE382F8BA6A797B027,
	RTCConfiguration_set_ice_check_interval_weak_connectivity_m5A394A75EEC3659CA452C0E2F2685ADA5B7ED320,
	RTCConfiguration_get_ice_check_interval_weak_connectivity_m6780B808B00C220488809DF6A8B3070A67AB93EC,
	RTCConfiguration_set_ice_check_min_interval_mCC26D3EE397DA09BBC15731F6DF24222A699C062,
	RTCConfiguration_get_ice_check_min_interval_mC258CBA793EEDB91D9CB897EDDF714278F4F0801,
	RTCConfiguration_set_ice_unwritable_timeout_m1A7D51D0D5BF111B7BBA242EECD2BB70F6A3332F,
	RTCConfiguration_get_ice_unwritable_timeout_mFE1F5508FFCDA7EBF0BF36DECC09DE5AA0E331CC,
	RTCConfiguration_set_ice_unwritable_min_checks_mD5138DA92F59A56882CB1C577FB286507EBF676B,
	RTCConfiguration_get_ice_unwritable_min_checks_mC9FECB2E4DD7E75DB94405FA0773D3736F521417,
	RTCConfiguration_set_stun_candidate_keepalive_interval_m26C8448071C161B96A59B6427C47021E896119CE,
	RTCConfiguration_get_stun_candidate_keepalive_interval_m306F2EF50FBB966DE68EA7A5F2C5232BF594972E,
	RTCConfiguration_set_ice_regather_interval_range_mAE53A0150B9EC07A318E02EDE6049FB68A91890F,
	RTCConfiguration_get_ice_regather_interval_range_m63FD1298695364E43A11D899554939BDFACC235B,
	RTCConfiguration_set_turn_customizer_m1EF815D1495B045B6F7AB0BE7087F79C9291BA9E,
	RTCConfiguration_get_turn_customizer_m1D9136850B323FB81D08BFC6F0BD085CE6EB7050,
	RTCConfiguration_set_network_preference_mDB5205DEF9B68A3E8D53431818688B28314AA8A5,
	RTCConfiguration_get_network_preference_mB46C2674B0247D27F82FBBFAB96E22B16B0391EA,
	RTCConfiguration_set_sdp_semantics_m10A680F052D0F73ECF887DC96BE7A027B67291FD,
	RTCConfiguration_get_sdp_semantics_m811413A9D65CBD80BC1F55C3D11A65C4A18E8F01,
	RTCConfiguration_set_active_reset_srtp_params_m42AAD972B9260B05C77676BFD990A41514286B04,
	RTCConfiguration_get_active_reset_srtp_params_m094096C57B4D22418CA2DF994E7647E023E9A66A,
	RTCConfiguration_set_use_media_transport_m49D457F5BD2807E1692037591226354A119AB067,
	RTCConfiguration_get_use_media_transport_mA1279AAB8F9CE6AC10FA04FC991E54DD6523A6EC,
	RTCConfiguration_set_use_media_transport_for_data_channels_mF10275EA17CE1A692026F911A3B1F31D84A5C2D2,
	RTCConfiguration_get_use_media_transport_for_data_channels_m1D9E24B7D9B4542E6233E56F3C749BB4088C2EE4,
	RTCConfiguration_set_crypto_options_m4798532B1C2DCB2F75D02E68655FFE639F8D54A5,
	RTCConfiguration_get_crypto_options_m1C0E7BE1EBF30B0991540FEDCA967A4DFD5D9EA9,
	RTCConfiguration_set_offer_extmap_allow_mixed_m81E792CC615742CB43BF469AFC43F7EE02E34070,
	RTCConfiguration_get_offer_extmap_allow_mixed_m151171B14731DCD28FEC08824E08299A9C23A7C7,
	RTCConfiguration__cctor_m0A0BB8022E2354D2813FC69F3BC8DAC97BD61C2D,
	RTCOfferAnswerOptions__ctor_mCC1F36D07B6C958710164FFC714CA2B1913642EE,
	RTCOfferAnswerOptions_getCPtr_mB27FC47F29D3C514E7F232EC63C894488FD4D0B4,
	RTCOfferAnswerOptions_Finalize_m7DBD44585FA35009A411830E52BD78297A0F3F3B,
	RTCOfferAnswerOptions_Dispose_mBEBBCF7639DC8A7ABFFD48B4E9EE67BCA0B0C9BF,
	RTCOfferAnswerOptions_set_offer_to_receive_video_m7DA58713F7C68472D56CA4108AE120C9F115D7C4,
	RTCOfferAnswerOptions_get_offer_to_receive_video_m869EE75E6D89DC4A66748EF9881670694E90EBE9,
	RTCOfferAnswerOptions_set_offer_to_receive_audio_mC703FB2C77DFEB54AE6BB0EAAC059977847FCE59,
	RTCOfferAnswerOptions_get_offer_to_receive_audio_m5E459D77F18A2B7B144CBDEF24B33C0555994519,
	RTCOfferAnswerOptions_set_voice_activity_detection_m5CDF4D402EB6EBC83F7956FB03CA1446E95C6003,
	RTCOfferAnswerOptions_get_voice_activity_detection_m3E7938E1D111D62EAAA624A1E5BEC83D6F6880A5,
	RTCOfferAnswerOptions_set_ice_restart_m5535AB0B652EA6ABCD3F9D57078067A6945D72E9,
	RTCOfferAnswerOptions_get_ice_restart_mE628D0FFB6722E558038EDF3ABE2DAE51E50A61B,
	RTCOfferAnswerOptions_set_use_rtp_mux_m1F23B9494CC5749E714ECC0768095FA078BA852A,
	RTCOfferAnswerOptions_get_use_rtp_mux_m2940C16C1F76D620DA9718A713BC3F0C6A7831BC,
	RTCOfferAnswerOptions_set_num_simulcast_layers_m7BF484D59BC743E349DD42F87C5DB023208EAB80,
	RTCOfferAnswerOptions_get_num_simulcast_layers_mC6109659B5ED2443DB27BC94CCA11B46417439A8,
	RTCOfferAnswerOptions__ctor_mA69EC84E5090D0B98E2F0DFB54E9E6769952C673,
	RTCOfferAnswerOptions__ctor_m2D27D37C4C984C59EA76DD06BECFF3A5481483E7,
	RTCOfferAnswerOptions__cctor_m539BACA60577191998ADCFF98A76760F43420B05,
	BitrateParameters__ctor_m46A2CADAE9487F4B06A28CA42A6348251FEB51FD,
	BitrateParameters_getCPtr_m99B24536F99DEB11D424C1E3807F2BC8B49A7CC5,
	BitrateParameters_Finalize_m212F63650DB8F01B626A7109D6688F29314BD874,
	BitrateParameters_Dispose_m0079B66ED17E7208B2362C3BA39BAEC2BE2AC102,
	BitrateParameters__ctor_m96ECE693260B9D8DDFC9DA5D814DCFE35D1D39E4,
	BitrateParameters_set_min_bitrate_bps_m71331813BC43E5B8E085E94EAE5C6E34C0C016CC,
	BitrateParameters_get_min_bitrate_bps_m26A8FAF2416126A24E707BD21263B8F8B0C54AA0,
	BitrateParameters_set_current_bitrate_bps_m2D39BDCD6DA4CA939C416EAACBDB7FE629FE1B28,
	BitrateParameters_get_current_bitrate_bps_m147B818F3BE557C70FD84B883D28ACA07429D7A2,
	BitrateParameters_set_max_bitrate_bps_m0C353D35825B152843EC20D7ED14C19EBB885907,
	BitrateParameters_get_max_bitrate_bps_m1E6B24D69F31EE1587E63FF3E640EC7B883E396F,
	PollingMediaStream__ctor_mBDD37D99CBEB6CC88C65AF6D2392595A03D92F30,
	PollingMediaStream_getCPtr_m61D21899EFD8E259A952476B246A2F65B000C7C4,
	PollingMediaStream_Finalize_mC3473521EAE13AFBD135207AB716F14C04D457CD,
	PollingMediaStream_Dispose_mC2E736C60455AF90AB85DDB895328D2C25359D6E,
	PollingMediaStream_GetInternal_m5935D28D2E651A8DD3ABEA1DA0F1DD97BBDF10A7,
	PollingMediaStream_HasVideoTrack_mF6D00EA2A963F8D4CB93EFF412FF0FCCF1A64AE0,
	PollingMediaStream_HasAudioTrack_mA7B028C5924B39FA1173492260FBDC9A11D74119,
	PollingMediaStream_IsMute_mC2EFC8C85B10B8EEB6917657108BE49A03D5AF93,
	PollingMediaStream_SetMute_m22B38623030518B7A55B65D95A7E77AD365ABD9C,
	PollingMediaStream_CalculateByteSize_m2B0FC5BD138E0386EEE3FF9FF7717DB492C086A2,
	PollingMediaStream_GetImageData_m00DD35B4B24CEB235AD43D37FFFD74B9FF940D9C,
	PollingMediaStream_SupportsI420Access_mB3F4B449415FA34C58F34D44F0CE6B6D8046639F,
	PollingMediaStream_GetI420Size_mAEDB3C5435488219A0BE1FD21812328262FFE22C,
	PollingMediaStream_GetI420Ptr_m0D34500CDD475CEC28B0F5D534C67D78ABF6B225,
	PollingMediaStream_HasFrame_m158432FD42564D244422CFDCAFA52EA57ADA7589,
	PollingMediaStream_FreeCurrentImage_mE55E22E3084453909DEF4F3366AA8BD69B604D51,
	PollingMediaStream_GetWidth_m25054F5F2FDA6F59C68454E8C0ECA1E1CF3860FB,
	PollingMediaStream_GetHeight_mD470C2017797463894870C3F8CF566238A7D9248,
	PollingMediaStream_GetRotation_m53AC1D1F4745B37F1176D09CEC0E06B30EFF1780,
	PollingMediaStream_SetVolume_m1D9F82341FBFB19F8C510C60697429AB100064C6,
	PollingMediaStreamRef__ctor_m3B1F2F5B56A549507C4E86EF8400F6AD105CC4F9,
	PollingMediaStreamRef_getCPtr_mCD05CA842410D480194987AC030E400A3D0B03CB,
	PollingMediaStreamRef_Finalize_mB8DF34EEE329DD8BBD7CE751137AD6DAB0545033,
	PollingMediaStreamRef_Dispose_mE003F6182E21195A2FC17D606E2C9174C088F87B,
	PollingMediaStreamRef__ctor_m338B43EF03DA71E5397BA20C597CC39EC657E0A8,
	PollingMediaStreamRef__ctor_mA536FF48AA1C939166877891CCDF284C0888892B,
	PollingMediaStreamRef__ctor_m2F81C8940A1B4F6CF40F37B234EDA64003F2BE0E,
	PollingMediaStreamRef_get_mEE1E98B7FB71D40B51D50AEE94BAFBD9D1DC354C,
	PollingMediaStreamRef___deref___mA9919A1F2269113B175F53EE46A4BF206FCD0466,
	PollingMediaStreamRef_release_m8CF91EC23F6301BE79133CFFC9E0950AF58D2470,
	PollingMediaStreamRef_swap_mDCBBAD8AE4C1D65968DF9DF367A14FCD5C77BFA5,
	PollingMediaStreamRef_swap_m56905A86C97AD86273A6F03178655A13EE1FB630,
	PollingMediaStreamRef_GetInternal_mBB4632D167BC4C2FA78E2F0F249EEF25C6007BCF,
	PollingMediaStreamRef_HasVideoTrack_m61EB91E6BB6CE3E9AEB3F111C1A476E9EEF67E57,
	PollingMediaStreamRef_HasAudioTrack_mF3F8E476D9046FEEC7F1AC8FDF10F2ADF91CD903,
	PollingMediaStreamRef_IsMute_m03A27ECA1AAFF8F6C1D55983C04934198B98B22C,
	PollingMediaStreamRef_SetMute_m0CA30C49BC3D7436D40A7C7FC6BFA459FCB1EB41,
	PollingMediaStreamRef_CalculateByteSize_m8676F764D433F8865127362916A91917040C6F05,
	PollingMediaStreamRef_GetImageData_m2FBE98FADAE67379C0B47823BCFE8B1A8DE5B176,
	PollingMediaStreamRef_SupportsI420Access_m61A8B29FE9E01D2D6F6548D56556D8B2B597B148,
	PollingMediaStreamRef_GetI420Size_m708FE6180103E39037DC0211DBD82E86186A5EEA,
	PollingMediaStreamRef_GetI420Ptr_m1F7EA1C8BFC4C32A6E375CA9914CA0AA0CC20F70,
	PollingMediaStreamRef_HasFrame_mCD40F63DC56DF8D3492601BCD11D040771489BE4,
	PollingMediaStreamRef_FreeCurrentImage_m59AF70EA27BA13C252B37ABE3A24091BE72F35A5,
	PollingMediaStreamRef_GetWidth_m589380472D357FE8A6D712A1F2CE498A6ED6DDFA,
	PollingMediaStreamRef_GetHeight_mCF06564C451E61001F4A1151BCD2859EE7C8ED3B,
	PollingMediaStreamRef_GetRotation_m0FA796DE76B3186BD4D497221EDD02543F4A78C5,
	PollingMediaStreamRef_SetVolume_m9EE8681F394A0AAFF8CC12B0948D6974E59EBC4E,
	PollingMediaStreamRef_AddRef_m79E6ADF4C46FA8C0DCD97EC2B9656A4C733E01B8,
	PollingMediaStreamRef_Release_m2C1D9C83714170FC3032584DF2E456130BED65A8,
	PollingPeer__ctor_mA68EB2346F759CBED5C8187E74576D5EE4AE37D3,
	PollingPeer_getCPtr_mA9C44DF501C3081F84354A15FA241C025AD33A6E,
	PollingPeer_Finalize_m78405492884FB2DE01B440028E8AD42434805543,
	PollingPeer_Dispose_mB599BB360A4460FA8331E4F2F664F27CC6C35A43,
	PollingPeer_Update_m06827D9C482F0CF08BE47CA7F1C35633C32F10D7,
	PollingPeer_Flush_mF78D24FB60ED8C055AF09BDD22434030E01E0EF4,
	PollingPeer_GetConnectionState_mA1EB541DF5FAFE8FCB80057FCFA17F79D719FEDC,
	PollingPeer_HasSignalingMessage_mDF6074B66DCA797609FDC7D800B1C5B5AEB6B9BE,
	PollingPeer_DequeueSignalingMessage_m07AF670E3DF01A531EFF5F3D8FF940C4CD5396A8,
	PollingPeer_AddSignalingMessage_mE95ED3BF90FC1B975FA457644FB9770002053B45,
	PollingPeer_HasSignalingError_m6EEF11B3D92D440B8AAA71E289645C35F342E765,
	PollingPeer_DequeueSignalingError_m564F2E6E68A910C59722FE4F2C8CA31FD8BCEEAC,
	PollingPeer_CreateOffer_m7DE07BA80B4C896DD81274ACDF1E489E363C0263,
	PollingPeer_CreateAnswer_m6EB19E328E1FBA79E7B1D841253AB358E0E1ABF8,
	PollingPeer_Close_m8F425355BEFA0A4E642C712FCF33F2B4C27377A4,
	PollingPeer_CloseInternal_mC4E34411D11831F45959DBAB0217FA5B1698E623,
	PollingPeer_Cleanup_m00E6DCCDCD558CFF24B4FA8C7FF5CCD4219BD88B,
	PollingPeer_CountOpenedDataChannels_mE41EEEDE357FFC3E9F8CE0254D6E9A5391FCDDCD,
	PollingPeer_HasDataChannelMessage_mE04EC3BAB823138D280E34AA6D5601E589D5D5B6,
	PollingPeer_DequeueDataChannelMessage_mF8F21FA7B31448250B94085E42E415CF8CE7C9E9,
	PollingPeer_Send_m98986054E14B888EDF6C8992590E9E5B2FB59151,
	PollingPeer_GetBufferedAmount_m2D0902FB2A5734BB18946DC00D9E526AED30B40A,
	PollingPeer_CreateDataChannel_m5977101B02E0C6008D1DE5EDBBA8714CF3415C79,
	PollingPeer_GetDataChannelLabel_m97A1F2A3A74E09C93006D5A98E7DD15FC1B61D98,
	PollingPeer_GetRemoteStream_m804B6BF012F3D66F2482FBF6A4F46FAB2D06C443,
	PollingPeer_AddLocalStream_m4568C9DBDDDE900F2A41E8BE83642EB9276A0A8C,
	PollingPeer_RemoveLocalStream_m112A33BA861784959A0D2B145F1DD5A37D89C593,
	PollingPeer_RequestStats_m8D11E4AA65B20EAC031F68205D4D61E032C278AE,
	PollingPeer_HasStats_m01FB20FDC47DD8F2AE8D36ADC697C46C86FFFE92,
	PollingPeer_DequeueStats_mC74CAC6389C8FF66694990AD665BFB32411345C4,
	PollingPeerRef__ctor_mD3C8C89EF9F37C3FF8B0DA6DA4B0B436C699BD6C,
	PollingPeerRef_getCPtr_m94B625AD35EFA192CAF7A29EE655EF118427FEF9,
	PollingPeerRef_Finalize_mDB470DAD862A09E34F5AD185EC91D0625D9BBDD8,
	PollingPeerRef_Dispose_mC6E468328109EEED7876959CAD2FBBCDE6D50FF5,
	PollingPeerRef__ctor_mF19E7141B0C7FF99AEF170BC64C7DBC155BA6F7D,
	PollingPeerRef__ctor_mC91E9D7D5C078147D49DA017ADE7E946C39514D5,
	PollingPeerRef__ctor_mBA43E26CDF74784E55A8D7EDE6206A50686E1F04,
	PollingPeerRef_get_m4F9F4379BBE4E35FBF21E398812A9F55C189EAC5,
	PollingPeerRef___deref___m32ADD68ADACF009209D36B03F67455604954A3F9,
	PollingPeerRef_release_mC2376E76019451E665D9D358089A200AC28E1133,
	PollingPeerRef_swap_mCCBF250D15C972506EFCF41E22643F1CA6431D55,
	PollingPeerRef_swap_mDEAB0C96C4E34689221B64A4CABF9B0AD2F86F9D,
	PollingPeerRef_Update_mED0760B9B7E04219ADD1DB2553C6450CED327495,
	PollingPeerRef_Flush_mB042EB916F2739BBDD36547AAF515DEF8E84C33D,
	PollingPeerRef_GetConnectionState_m01FACCF3FAEB47F214137BD27152446BC2E89135,
	PollingPeerRef_HasSignalingMessage_m4BCAA09B152992B0DBEBB2AC0CEBCD801ED1C953,
	PollingPeerRef_DequeueSignalingMessage_mB693DE18977208672214E6D340865949F2103931,
	PollingPeerRef_AddSignalingMessage_mAAB3CF3D6EF9DF41684CE27B6B7FE793056E3477,
	PollingPeerRef_HasSignalingError_m0F5888B19B1472555C81CFC09A61353AF90E8341,
	PollingPeerRef_DequeueSignalingError_m415761502E8BBED85909D7D741E264697ACD3EC5,
	PollingPeerRef_CreateOffer_mDF2740EA5975E3A4845088439A057E7C9404025B,
	PollingPeerRef_CreateAnswer_m867064FE35919A0CB498577EEFA890F4F92D6FC5,
	PollingPeerRef_Close_m1CC3A8CCAE226AAE7EA099DD87604C9136A29C67,
	PollingPeerRef_CloseInternal_m52270E5AF1FE12396F5B03B4B5BB8C5027472F90,
	PollingPeerRef_Cleanup_m027F1AB124C37DCDFDC512574EF7D35CD7DCA102,
	PollingPeerRef_CountOpenedDataChannels_m36FF198425BAE8BDD1249E30258E598CC65144AC,
	PollingPeerRef_HasDataChannelMessage_m5430B65234F0964E2F1B5C8617B11A402FAB57F5,
	PollingPeerRef_DequeueDataChannelMessage_mB3B1C0C8EB6331D722B4C7309B76DDB807BD138E,
	PollingPeerRef_Send_m335661AEB4038297F76C139F5F7A5AEA4D49F7D4,
	PollingPeerRef_GetBufferedAmount_mDCC1F687960D4FEDC3EF7318766ABB2D73595F0D,
	PollingPeerRef_CreateDataChannel_m026DDDCA8435951865138754CEB7F5E47A9AAC0A,
	PollingPeerRef_GetDataChannelLabel_m6A048CF81C612FB7FD7EB6B2E757703585240764,
	PollingPeerRef_GetRemoteStream_mA67B1380A9442148E90F765153E6828B0394C8AE,
	PollingPeerRef_AddLocalStream_m22E8605B59A13EF216641FFDEC2D632C67072F70,
	PollingPeerRef_RemoveLocalStream_m95221E8327FA7BC59C871B15CEAFCAB47BCCBE81,
	PollingPeerRef_RequestStats_m093392E1072CC36F90085D9874E0A90AE6E6EEE3,
	PollingPeerRef_HasStats_m131B7BF034E90E4407EC78129D3767495C53C1E0,
	PollingPeerRef_DequeueStats_mDD640F18A79343AE22BC51643E19E3FD0B887127,
	PollingPeerRef_AddRef_m06D32ED4C4F0636A5A26E393890245313FF2BB5B,
	PollingPeerRef_Release_m06C209319D5126AD73A0B0DA513001356D2B6CB8,
	RTCLog__ctor_m91433C403B8B87F4CE92FE8DFF174FC746D94A2F,
	RTCLog_getCPtr_m33DD58FFB5CD2C358035602AC7041A92C1E3BD32,
	RTCLog_Finalize_m1883638026836A908DE459919BA5C7702A1959C8,
	RTCLog_Dispose_mC5E02235B969AFEF3FF99120683A37EBB89E7025,
	RTCLog_L_mCF8F735ABE248CE20B6ECC6072AB8301D79EDF01,
	RTCLog_LW_m4DE6465BE53CA5080E5F7D0CB0BC7B0FF3BF14E1,
	RTCLog_LE_m0A4B1D7FC84A3B479BB3E77BC1BAD085952F2B6B,
	RTCLog__ctor_m0E307DB9E2CB94FFBC17D1DE63BD229EBF38D742,
	RTCPeerConnectionFactory__ctor_mE3AC1AA6E60F24DC5D6BF848733DE179E9C860B4,
	RTCPeerConnectionFactory_getCPtr_mF678806CDFEFB7F13DA37C2F65597660E24B81F6,
	RTCPeerConnectionFactory_Finalize_m62684DCF29A6AC89E7AC1C0BCFFD696F36CB13C7,
	RTCPeerConnectionFactory_Dispose_m81F26F4276C30D95FC458FCA8D91996CCC60CB47,
	RTCPeerConnectionFactory_Create_m56F686A85B9E044509B807C486BBCCC3A137E7A1,
	RTCPeerConnectionFactory_SetObosoleteVideo_m149631FF0FA794D612920C1FC4C73FFE7FC31678,
	RTCPeerConnectionFactory_ForceDummyAudio_m6CA2E2399CC816BF40380BF539748260615101ED,
	RTCPeerConnectionFactory_AddVideoCaptureFactory_m30DA8D9E136265E1B9D594E83D195D571E3AC5CA,
	RTCPeerConnectionFactory_IsInitialized_m61BFD56A558AA7B9E2615054BC996CD40BE35EB0,
	RTCPeerConnectionFactory_Initialize_m049E43A29B41463CCCD4A62A3BC959D3265C32FD,
	RTCPeerConnectionFactory_SetGlobalSpeakerMute_mBE696799648D51AE707AF8A839D8DDA6FE4B0817,
	RTCPeerConnectionFactory_GetGlobalSpeakerMute_m8ADE623CE809CA8FDA27DE7F7BFFD50AE0FF353F,
	RTCPeerConnectionFactory_HasGlobalSpeakerMute_mD1136221C946658D7B8338931383D39CE7970F4B,
	RTCPeerConnectionFactory_CreatePollingPeer_mAD8A82E58BB1F69EA8339515D08B22E8F63776A8,
	RTCPeerConnectionFactory_CreateAsyncPeer_m5D74BA293BA00D0F911333E0430E5F593E0B0560,
	RTCPeerConnectionFactory_GetDebugDeviceInfo_m28F7C898B2A266C9D422A9FBBF315FE7AE2EF799,
	RTCPeerConnectionFactory_GetDebugDeviceInfo_Old_m47C02E8DDF8420929450DEF84F98A8BC042E2B6B,
	RTCPeerConnectionFactory_CreateMediaStream_m8826B5D488CAA3C8069C421B5CB634C1EA041432,
	RTCPeerConnectionFactory_CreateMediaStream_mB7BEF1AC6812DE99953D4CFF660274FFEC2EAD92,
	RTCPeerConnectionFactory_CreatePollingMediaStream_mA3CFA317C582D855B7E9451E88E29A1EDCB17135,
	RTCPeerConnectionFactory_CreatePollingMediaStream_m52519AEB2B7F97A6B6DF0B3BA5B83B4E9F23277E,
	RTCPeerConnectionFactory_GetVideoDevices_mD7AA505EFD7F28FA938EB88A3456DBC1A8439C5B,
	RTCPeerConnectionFactory_GetVideoInput_m9F940178B74C82FF12103A7A889BC4FB1530090A,
	RTCPeerConnectionFactory_GetThreads_m06F79899FAC7B80B978A7E2FC33FC3A033EE979D,
	RTCPeerConnectionFactory_GetSignalingThread_m017F05B705EE54645B787491C414AD33254E4015,
	RTCPeerConnectionFactory_GetAudioDeviceModule_m02397F16906B2D4C151276A69AE92966A9CC201C,
	RTCPeerConnectionFactory_InitAndroidContext_mAED59B8F44AE89F01060D84B19FD0D7DF74A27C6,
	RTCPeerConnectionFactoryRef__ctor_m81863D5234990F361AE953AEA0601AEE4861AE46,
	RTCPeerConnectionFactoryRef_getCPtr_mC64FC9FFE23497340ED18227C01B8DC16E217D77,
	RTCPeerConnectionFactoryRef_Finalize_m5DC2D183F5EF6E51900B2C20123A06F681AB204B,
	RTCPeerConnectionFactoryRef_Dispose_mE960024FEE5F9288A6D8444B641E771CEAE34585,
	RTCPeerConnectionFactoryRef__ctor_m475AAFC463F6FA114B45500547709D3F115EAA7E,
	RTCPeerConnectionFactoryRef__ctor_mB7F7C427CDC29DF23CDF29647E7F2C984977EBEE,
	RTCPeerConnectionFactoryRef__ctor_m486D562C45DC49D429F322C9F2C2E755C98FC983,
	RTCPeerConnectionFactoryRef_get_m45590D6F80F3AF74E484867DFD743D4CB0F0288F,
	RTCPeerConnectionFactoryRef___deref___m7DE998A1CF036920F57EE14B3373E25C7A743AE0,
	RTCPeerConnectionFactoryRef_release_m5AD80C561B9D04058D418CC1ED104DF0FAF41F99,
	RTCPeerConnectionFactoryRef_swap_mCF5358A8BB29F594F0FCE7FC68165F9BEAB86CBA,
	RTCPeerConnectionFactoryRef_swap_mFEDB7A20DF02D9B5433B27A23A40F92D28BCF13D,
	RTCPeerConnectionFactoryRef_Create_m06EB6FD3CA8B38FACEF9CD58A956B2C9EDDE798F,
	RTCPeerConnectionFactoryRef_SetObosoleteVideo_m3509C48D91549087A39500426507175E628268FF,
	RTCPeerConnectionFactoryRef_ForceDummyAudio_mD25C6E9A36B668C54A7EEAD0BD61004D0DEEBE27,
	RTCPeerConnectionFactoryRef_AddVideoCaptureFactory_m1EA7FA2B06878080F3EAAE97807376514C3322C6,
	RTCPeerConnectionFactoryRef_IsInitialized_m9D4221C4ED3CEE91C13850AC4E4A443213F9A2BA,
	RTCPeerConnectionFactoryRef_Initialize_mC24B195FB3576C4BADA4C052167AC8E329DFB4EF,
	RTCPeerConnectionFactoryRef_SetGlobalSpeakerMute_m660448ED167A5B827E3950BB477FCCE0C68E73F6,
	RTCPeerConnectionFactoryRef_GetGlobalSpeakerMute_mB4633F14EC6F454F9057666323578DA9FCDAF456,
	RTCPeerConnectionFactoryRef_HasGlobalSpeakerMute_mBC7A1F975F53D089BE6F67BB153E6591D94EC9A1,
	RTCPeerConnectionFactoryRef_CreatePollingPeer_m82F28AE7CFDD8BC26B8C0FE7B0D7DABA2E62AD61,
	RTCPeerConnectionFactoryRef_CreateAsyncPeer_mE61C2D738D9E23E7AB49B971E9ED2F93E5F1F8EC,
	RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_m8585A62907E2E20D6EF2F7C3C765564B30EB1977,
	RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old_m843D18ED76BE60A5DE3CFE5E7B23CC5C2B5E9BF1,
	RTCPeerConnectionFactoryRef_CreateMediaStream_mB650BD1FE6BA3945D7F2E4E4219731F5F13BA481,
	RTCPeerConnectionFactoryRef_CreateMediaStream_mA083CDF688EF7E579B79B5ACB6E4AF033034BDF1,
	RTCPeerConnectionFactoryRef_CreatePollingMediaStream_mE1CE6A6AC63D1E5A4026FC24E077A9FC3B492F75,
	RTCPeerConnectionFactoryRef_CreatePollingMediaStream_mE6BA517CC385CD29E08D7379345D0F172AB3DD18,
	RTCPeerConnectionFactoryRef_GetVideoDevices_m3D7E5B5186AD09CC90687043428640CA08E31AA9,
	RTCPeerConnectionFactoryRef_GetVideoInput_mACA86CAFE6E265387A5785A40CA253A3C5CF49B7,
	RTCPeerConnectionFactoryRef_GetThreads_mCEE99FB2C8D963BDA1B1616AC063008A9C89BCD4,
	RTCPeerConnectionFactoryRef_GetSignalingThread_m31CB53CF768825AF9D448D5CB3F8713348DD3EE9,
	RTCPeerConnectionFactoryRef_GetAudioDeviceModule_m672EE03AFE92A162438A3D5ED2F824BB7290063A,
	RTCPeerConnectionFactoryRef_InitAndroidContext_m5D900D024D143595F7117DAA055BDD5348B8A0C0,
	RTCPeerConnectionFactoryRef_AddRef_mCA93C021A3D3794A7EE15E0B48560E063ECA0009,
	RTCPeerConnectionFactoryRef_Release_mB31668494EEC2EFFF557C9B0F49ED1E912520D95,
	RefCountInterface__ctor_m7354666FC1071992A5AD8806678E41192B44A360,
	RefCountInterface_getCPtr_m13B445445D26F9197F7E098711A93C30E6A59146,
	RefCountInterface_Dispose_m96E99B5CBC31020A823CA51065842532905FBD63,
	RefCountInterface_AddRef_m6D6BD413929D07E207D7DFA5D839509135379B2E,
	RefCountInterface_Release_m0630177E0EFBC86AEBC595A647DE1B604BB71E73,
	SWIGTYPE_p_CreateSessionDescriptionObserver__ctor_m5BF1E7109BE0D998141741AF858653453309A09A,
	SWIGTYPE_p_CreateSessionDescriptionObserver__ctor_m5B4052B9505C07A3D52720B1A250C62E4C944363,
	SWIGTYPE_p_CreateSessionDescriptionObserver_getCPtr_m38E423909D8C1A38499CA4B1D5C1BE674AA82D08,
	SWIGTYPE_p_FILE__ctor_m45A773847AD97C7D6994F3B13A687C2F2571160B,
	SWIGTYPE_p_FILE__ctor_mAF4DCF05EF6D960B7E2CA2C640A6A3C04FB3EF02,
	SWIGTYPE_p_FILE_getCPtr_m7DFD535754E53F4BACBA2DF58252C5C120A3D46A,
	SWIGTYPE_p_I420BufferInterface__ctor_m4D86880864845D2BC9BDE762F25C82597FE9F28B,
	SWIGTYPE_p_I420BufferInterface__ctor_m08D0AFE90D22965BD0BD89FA0D6CC4BA402EB847,
	SWIGTYPE_p_I420BufferInterface_getCPtr_mE64427050F332A9BD08EBAA2F04D565DF904E8DD,
	SWIGTYPE_p_IceCandidateInterface__ctor_m148EE648EDBB7E1C6B5FDC5DF0A3EE55799185F7,
	SWIGTYPE_p_IceCandidateInterface__ctor_mD37D258BD50B5BE3ADC2312137ADE5FC7688CE9C,
	SWIGTYPE_p_IceCandidateInterface_getCPtr_m062B780C04DBA7D461CBBAD1BDB9A4E9E57D448F,
	SWIGTYPE_p_MediaStreamInterface__ctor_mFA65B86C8F0E857AB255156D0CD6930C69D44613,
	SWIGTYPE_p_MediaStreamInterface__ctor_m58781A9057C0F87A4B58831129E466546299DD47,
	SWIGTYPE_p_MediaStreamInterface_getCPtr_m01C359A1360177D155AC4B78812DCFDB2DF3D4F2,
	SWIGTYPE_p_MediaStreamTrackInterface__ctor_mC36E5779B66CF0AB046A8FDABFE9E1B1859D63F5,
	SWIGTYPE_p_MediaStreamTrackInterface__ctor_m410BA54DC2314BF978476A3EB30EDE939D83030F,
	SWIGTYPE_p_MediaStreamTrackInterface_getCPtr_m613D6110B2BB011B009F4727F164D438EE3CF99C,
	SWIGTYPE_p_RTCError__ctor_mFCACEAFBF69843A947529171E6C96AA062921019,
	SWIGTYPE_p_RTCError__ctor_mB3666F6577B722888E87F80065227CFCAE7E25BF,
	SWIGTYPE_p_RTCError_getCPtr_m29E1538A7579A68374C306FE459C89092FBE49E6,
	SWIGTYPE_p_RTCMediaStream__ctor_m1AFD95C7AC7D8B35B5BCE79097369664E1CB2DFE,
	SWIGTYPE_p_RTCMediaStream__ctor_mF62EA81EA2DAF42A0DB9ABF48C2E288D4D9046B3,
	SWIGTYPE_p_RTCMediaStream_getCPtr_mAB2239DD414B257890F3ED50FAC18A1FE8251B35,
	SWIGTYPE_p_RTCStatsCollectorCallback__ctor_m680E519D393E2BB7393B0173974364906718C125,
	SWIGTYPE_p_RTCStatsCollectorCallback__ctor_mAD9033225BFD8F6CED64F22D61613466D3A016AB,
	SWIGTYPE_p_RTCStatsCollectorCallback_getCPtr_m288A5BFEC7B0A502BAF7AC7243A65E2E566CD742,
	SWIGTYPE_p_RtpSenderInterface__ctor_mDA47EA2CE741CFEE50630CB5B0C7292D2A7F1DE4,
	SWIGTYPE_p_RtpSenderInterface__ctor_m8975EAB7E208777E9CE552E841ABBC7604419798,
	SWIGTYPE_p_RtpSenderInterface_getCPtr_m560B736D2306F03F085452837098CE6E17AC9013,
	SWIGTYPE_p_SessionDescriptionInterface__ctor_m24F10A2A7A8C6CE39880D9AE59DC075C293881B0,
	SWIGTYPE_p_SessionDescriptionInterface__ctor_mF7D2A79A667355C43C6471BC61FE65BCD624F647,
	SWIGTYPE_p_SessionDescriptionInterface_getCPtr_m2CF286D59B1B0CCD9601C0DCC8BEB78B9179762C,
	SWIGTYPE_p_SetSessionDescriptionObserver__ctor_m068E125C2A828AE978A837E8821249AF73E9104D,
	SWIGTYPE_p_SetSessionDescriptionObserver__ctor_m8350BD0C36E3632295DD2F8AC2F00E2E7D307C35,
	SWIGTYPE_p_SetSessionDescriptionObserver_getCPtr_m2A2769A5E325242B98CA0DCAF4077D86FC37886A,
	SWIGTYPE_p_VideoFrame__ctor_mE2BE51C8A4834036F53B0CD87066A7BB5A246E68,
	SWIGTYPE_p_VideoFrame__ctor_mF114B8304ED571635F52C5092CE80A8A27449BCA,
	SWIGTYPE_p_VideoFrame_getCPtr_m7F194161D52A176B13237D9F47C1566472D69BA2,
	SWIGTYPE_p_absl__optionalT_CryptoOptions_t__ctor_mD1D83FB794AC35F33323EBB0C032411977FF2514,
	SWIGTYPE_p_absl__optionalT_CryptoOptions_t__ctor_m73B41E9F9B35ECE901D6F9D0BFBF7D7EDDF314EA,
	SWIGTYPE_p_absl__optionalT_CryptoOptions_t_getCPtr_mFAB7656CFF070B1AA1B9045F66BBDC6A4652BBC0,
	SWIGTYPE_p_absl__optionalT_bool_t__ctor_m5D43F03EB89E81CA528FFE441F76F89419B391DC,
	SWIGTYPE_p_absl__optionalT_bool_t__ctor_m290A15BC114FD1E30C9EAA1831FA743CFA465ADA,
	SWIGTYPE_p_absl__optionalT_bool_t_getCPtr_m3FFA499AA49C3AEA6099EED60F18FB27894E02C4,
	SWIGTYPE_p_absl__optionalT_int_t__ctor_m731CDD3EAFCE75F9EFC85D88A9D8BC5349FC106C,
	SWIGTYPE_p_absl__optionalT_int_t__ctor_mF866B66C11E38A6C429A5CCE189EFBE637969493,
	SWIGTYPE_p_absl__optionalT_int_t_getCPtr_m2DE83335D8EC6B6E08C215FF36C7E29DB31EC598,
	SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t__ctor_m451C6225DAF83F989EC624830F1303482705A739,
	SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t__ctor_m1324FADACF5302A51574784EC555C3608F1046EB,
	SWIGTYPE_p_absl__optionalT_rtc__AdapterType_t_getCPtr_mEC9D7CD94F0F90296C7C2BA9626A7EB611D6E775,
	SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t__ctor_mBD8D7645669B675F1C6EB1A2B4D75B33DBCA552F,
	SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t__ctor_mBC75038EB83E1300FC2DC4D05803D1D554479F49,
	SWIGTYPE_p_absl__optionalT_rtc__IntervalRange_t_getCPtr_mCD39E20AF15018235EBACDB64009118BC376C695,
	SWIGTYPE_p_cricket__MediaConfig__ctor_m0B86807D4F977F2215ED5A6F3738610344381B05,
	SWIGTYPE_p_cricket__MediaConfig__ctor_m031878A35166E7D18F2F1F00844D90C9FB0C1286,
	SWIGTYPE_p_cricket__MediaConfig_getCPtr_m7147AC26708D5BB5A240FEA00ECD62A97C7602C5,
	SWIGTYPE_p_libyuv__RotationMode__ctor_mCD6B67FC71971B2D934B22038016F13773246470,
	SWIGTYPE_p_libyuv__RotationMode__ctor_mB454BD60DF27E45C876E1FE66308899D4F6CFCD1,
	SWIGTYPE_p_libyuv__RotationMode_getCPtr_m5CA732DFAD5C10D3DCEFC3818285EE0EAE4DC357,
	SWIGTYPE_p_p_AsyncDataChannel__ctor_mFFD0528499144D811C81F47126A714C959C21911,
	SWIGTYPE_p_p_AsyncDataChannel__ctor_m598B2DB8F7784A9C63CD018A2602A8941B82496D,
	SWIGTYPE_p_p_AsyncDataChannel_getCPtr_mD8A1CAFD6490D06B665BC95B08CD1F96CE33C5FB,
	SWIGTYPE_p_p_AsyncPeer__ctor_m8A9E72608927D43EA3177749864159771F8CA99E,
	SWIGTYPE_p_p_AsyncPeer__ctor_m299D42E13B822CA49E7547743D7C79F447C7B0E7,
	SWIGTYPE_p_p_AsyncPeer_getCPtr_m528918A9907712AD3FD4E21206A4AF9B436413F4,
	SWIGTYPE_p_p_PollingMediaStream__ctor_mFD7244A2B72E0A62A3C315FDCAEFEE28F44FC17F,
	SWIGTYPE_p_p_PollingMediaStream__ctor_mB9F332724FDDA7652EC8EDBEA0F5F5EE20AFF635,
	SWIGTYPE_p_p_PollingMediaStream_getCPtr_m21A745A507A88DDC8FE34BBBE7C7CD9995E12326,
	SWIGTYPE_p_p_PollingPeer__ctor_m3AB36E18D8E9877D9DD11D50B6206F40BEC0E1AA,
	SWIGTYPE_p_p_PollingPeer__ctor_mF0A50C4D1745FE615B463C45562B8212F2180877,
	SWIGTYPE_p_p_PollingPeer_getCPtr_m08D77C57158AF9B1146BA79C65B0F739210BE3BA,
	SWIGTYPE_p_p_RTCPeerConnectionFactory__ctor_m8567C1421DDAC3CAD18FB0D4DF76D875A819689E,
	SWIGTYPE_p_p_RTCPeerConnectionFactory__ctor_m478DED90B324A35780C0F38BC807BB029BA02C0C,
	SWIGTYPE_p_p_RTCPeerConnectionFactory_getCPtr_m06C899DD97E54B80F682FD900D44C965912AEC55,
	SWIGTYPE_p_p_VideoInput__ctor_m52DD447F4BD46EDA1F3C34C295F79897CF4E6DB9,
	SWIGTYPE_p_p_VideoInput__ctor_mA66793AC333CD065B896EFD461F84039BF204C1A,
	SWIGTYPE_p_p_VideoInput_getCPtr_m46B6A1F1B2716CCD92A3D84DA3F8990E5A04AF3A,
	SWIGTYPE_p_rtc__Thread__ctor_mCAF3AF9566ED26E09F0A3F4E14CC7EFD65BB6D6D,
	SWIGTYPE_p_rtc__Thread__ctor_m31580414FE8131A12F62C92559C14B18343B9714,
	SWIGTYPE_p_rtc__Thread_getCPtr_mEDE170E9B2838011A9291258621375DE05C72335,
	SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t__ctor_mBD706571C4A7AA01A7B57063D7A38B1A29A6F4EA,
	SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t__ctor_mC02FA0D09166B2EB14A1B9BF0016847EEB263DBA,
	SWIGTYPE_p_rtc__scoped_refptrT_I420BufferInterface_t_getCPtr_m26E7378DA30E59463B98D15225CFF97857D4E1A2,
	SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t__ctor_mE19F0240492AE600C5A5441E275638DA7B2DD5DF,
	SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t__ctor_m73EB3D70E8073A6AF5A82ABCCD26ABF1CD645D83,
	SWIGTYPE_p_rtc__scoped_refptrT_RTCStatsCollectorCallback_t_getCPtr_m73CB71CE80F4D5359E9913FCDC9109513738B936,
	SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t__ctor_mA09A713B10836944F5CD5B5D95C4B7859FE432A9,
	SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t__ctor_m545BBEF67DAF697798474EFD68352F80AD86B5AD,
	SWIGTYPE_p_rtc__scoped_refptrT_RtpReceiverInterface_t_getCPtr_m36744972E736A2AFBA01FBF1C4389D85B3E142B5,
	SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t__ctor_m8594D547C08DE1D086D97D3A45D4E5A3753A6858,
	SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t__ctor_m1E6368E43E2696FFE6F197E2212E2F960E9E2A01,
	SWIGTYPE_p_rtc__scoped_refptrT_RtpSenderInterface_t_getCPtr_m8CB74883C94A8761898E955D821D8264150399B2,
	SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t__ctor_m728DABF6B70844457E2F7E0F6DD070A9CBB08E64,
	SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t__ctor_mAB40EFB467A3612BBB2D4CB413F59F1344554948,
	SWIGTYPE_p_rtc__scoped_refptrT_ThreadWrap_t_getCPtr_mE0AFDE3609912024D38465F3AFAE2D6339885B3B,
	SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t__ctor_m977254FD1FD15E9FA94E7B809BFE49011C086CF6,
	SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t__ctor_mB1E69A63761DA7F4856A24E83C8451326A31E93B,
	SWIGTYPE_p_rtc__scoped_refptrT_webrtc__DataChannelInterface_t_getCPtr_m95AC999D655AB69B2ED760FD414E650F264931BA,
	SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t__ctor_m2553635777C8F4BB38F34B7A1C1357337707ED51,
	SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t__ctor_m2C6AA371A056D0879013DF59F7E358C216855FC8,
	SWIGTYPE_p_rtc__scoped_refptrT_webrtc__MediaStreamInterface_t_getCPtr_m9842F054BD6A8761F718B76B7F57980AC9BB2648,
	SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t__ctor_mCB603D0F1218E82E1A51652A4E4319BB64450D88,
	SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t__ctor_mD49A4CB6E679D533E6853BB7E8F6F14BC634D8BA,
	SWIGTYPE_p_rtc__scoped_refptrT_webrtc__StreamCollectionInterface_t_getCPtr_mF86E8BBA9CEA269A1F83ED35CECE037FEC7E0C7B,
	SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t__ctor_mB9119D7CB0FBE879338CA5B54410E5D0E990608C,
	SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t__ctor_mD9881F608D3801E1C77070D30BEDE2BEAE514026,
	SWIGTYPE_p_std__shared_ptrT_IVideoCapturerFactory_t_getCPtr_m08E81649570DE09346889B8EB4E8F785C9F66399,
	SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t__ctor_m69E441D8416466AB365FA59590AA0101E78FD9B7,
	SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t__ctor_mDD240A2D4B383A7D7FE55C09E6EFD546EE3A0E7C,
	SWIGTYPE_p_std__shared_ptrT_awrtc__Message_t_getCPtr_mFABE54B9E12C007ADBD5C1CE5A2E04A6BFC843C6,
	SWIGTYPE_p_std__string__ctor_m1930E822BCA9000409EA7E030E36C98AC43AF2CC,
	SWIGTYPE_p_std__string__ctor_mEAA3EA8D304DB0735E94C95C4B09DEF8122DF2ED,
	SWIGTYPE_p_std__string_getCPtr_m7E89C1A38180B6B378A359B86FE5E8D9DD06CA17,
	SWIGTYPE_p_std__vectorT_cricket__Candidate_t__ctor_mB66D28B56B2CD2651D29E36B2EDBC23181A6EC00,
	SWIGTYPE_p_std__vectorT_cricket__Candidate_t__ctor_m4B2CA5C5ACFB514A0EAFE0C8B86A0F1A779F1686,
	SWIGTYPE_p_std__vectorT_cricket__Candidate_t_getCPtr_m6FEB0A316FF86EF02D26C645AD275429A13A5FA5,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t__ctor_mEB129BDC287F25C951B1A127994214CBA7B93A73,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t__ctor_m96EE98401B31B45E3AB2FF396C2F2890EDC1D25D,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpReceiverInterface_t_t_getCPtr_m46645AF17210C1D9638230E70FD0D44C2255BF2B,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t__ctor_m9C619744F223466D970113EAB5DA777774B08331,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t__ctor_mF4D40F7F2FBBE44B7AF5ABDD1BE6B1BB5B74CBA3,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpSenderInterface_t_t_getCPtr_mB0C2B7D5F3CC87E1A50C28A20D269CD9678262AD,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t__ctor_m11257E37C68B2A9BCEE027E11E924B5928A9739F,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t__ctor_m905C73652A6B34D69C03C380B3C7AD2E4CB395B6,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_RtpTransceiverInterface_t_t_getCPtr_mA0BA2364A3D295B80AC3AE499AF0667720422B23,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t__ctor_mBC0CC1DA5CE494521163D7ADDC315269102CF6C6,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t__ctor_mF5FADE3FE0295971D7AB87CD9A090FFAC40F47FB,
	SWIGTYPE_p_std__vectorT_rtc__scoped_refptrT_rtc__RTCCertificate_t_t_getCPtr_m7EF1C6E4679A4515972404F49EAC8382BE10B347,
	SWIGTYPE_p_unsigned_char__ctor_m98C1DA311CEB3123228A1F08EF80E605F44EA8C8,
	SWIGTYPE_p_unsigned_char__ctor_mEB072D02A84A0797947C4D6D1F703636CB8D3AEE,
	SWIGTYPE_p_unsigned_char_getCPtr_m1D96804BC931F06A7BCB99F46F5B3473DE047C5B,
	SWIGTYPE_p_webrtc__AudioDeviceModule__ctor_m0D542C6F29113F54DE45D7E5B7D52DE13E8C43F5,
	SWIGTYPE_p_webrtc__AudioDeviceModule__ctor_mEB23E005370889478D0AA7F66DAAD61DC733455D,
	SWIGTYPE_p_webrtc__AudioDeviceModule_getCPtr_m6E33D964D374D469C8B49639C122AEC16FD2BF6E,
	SWIGTYPE_p_webrtc__I420Buffer__ctor_mE82E3F5DB877689F5A332F9956B2372475C4440F,
	SWIGTYPE_p_webrtc__I420Buffer__ctor_m13ED4C310BB7379F135D053A57EE9B433D9C2D02,
	SWIGTYPE_p_webrtc__I420Buffer_getCPtr_m575FF16E90C3E5E233CD6309573A5EFB7D03D8C1,
	SWIGTYPE_p_webrtc__IceCandidateInterface__ctor_m7F65CADCBDE87D258E321EF3AA52D01574EA58EC,
	SWIGTYPE_p_webrtc__IceCandidateInterface__ctor_mF768784D4882CCF54101662F6D1B803AF518BCB2,
	SWIGTYPE_p_webrtc__IceCandidateInterface_getCPtr_m3784EBA98DE4E6038A64F55E5581FD1D026715A5,
	SWIGTYPE_p_webrtc__SessionDescriptionInterface__ctor_m94E247126D623BC8EF75B631F348BF08E4096F37,
	SWIGTYPE_p_webrtc__SessionDescriptionInterface__ctor_m32A43B30677861C0FE45B8A6B9120F7168F1FF5D,
	SWIGTYPE_p_webrtc__SessionDescriptionInterface_getCPtr_m2BAF39254C361B11C7B342E4D36D721AC43B9F67,
	SWIGTYPE_p_webrtc__StatsObserver__ctor_m86166867C08CD750F77858B510295EEB2F1FA162,
	SWIGTYPE_p_webrtc__StatsObserver__ctor_mECAF37CAB1FB2F021F3B5EF407FA213E651B4351,
	SWIGTYPE_p_webrtc__StatsObserver_getCPtr_m49C5FE2D0831EE1A85872C1ED2D89D9625D5067F,
	SWIGTYPE_p_webrtc__TurnCustomizer__ctor_m3283D23C430B8473F11D0CA80BE0118CC890DE4C,
	SWIGTYPE_p_webrtc__TurnCustomizer__ctor_m1D1C94B86305569016AFE43F941CC60B14D71987,
	SWIGTYPE_p_webrtc__TurnCustomizer_getCPtr_mFEA3EC195F9256D4BF1890FC294F084DB7F9539A,
	SWIGTYPE_p_webrtc__VideoRotation__ctor_m702EC747BD4954716FC8329A0FB524AD7BAA5508,
	SWIGTYPE_p_webrtc__VideoRotation__ctor_m4AD927E4BB348435244D1E13CB20D14F8BCB8D9F,
	SWIGTYPE_p_webrtc__VideoRotation_getCPtr_m209C7EBCFC578DB74F97B98A32EB7F0000152538,
	StringVector__ctor_mB044E3E7A76C2CBA36B056C40C23E5ACA4A17D1E,
	StringVector_getCPtr_m647ED4F6F4CBD803435F0096BBC487F29855D5F5,
	StringVector_Finalize_mF2D790D5BAEB3C1E7728E048D4CA94331A634B20,
	StringVector_Dispose_m46BD32E86491C0D7476A5E81E9523C0C7973D25D,
	StringVector__ctor_mAE07E9631DADCC755E686077681BB4627638DA88,
	StringVector_get_IsFixedSize_mF12150CE5AA10A362B8EA31D39903C848FEF06C3,
	StringVector_get_IsReadOnly_mFC5A9D830ACEFEBF8B677AC74778F49D3374F6FC,
	StringVector_get_Item_mD61AB93D19ACA6CF003404D996AE0085D03A8710,
	StringVector_set_Item_m6B9D0B694A83C7ADB3F6BA5CEE1FE472CB6F392F,
	StringVector_get_Capacity_m25A8C7CE7385DB5ADA2CFEA479933C3D53C322C3,
	StringVector_set_Capacity_m0AFE20CF7BAE50BDFC868DEC780D882BAFEC9EA7,
	StringVector_get_Count_m8A00F2553FF5AB793C23DBA0A69837DE5D04148D,
	StringVector_get_IsSynchronized_mFF8DEA5123DAD14B7EEAE7F3051E5EA30A304FB4,
	StringVector_CopyTo_m5C6891053496E69BEC2B0407D021D29BAC7ED368,
	StringVector_CopyTo_m0CE07652F2D760AF6C4844F1E94DFB3FE84B37BE,
	StringVector_CopyTo_m2A9BB98E5EC87CA20501B40A8BC6CB2729D518EB,
	StringVector_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m22CAD77E5556E6E8925DF3CC22BE4A8B1332CA30,
	StringVector_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mF5DBAEEA9E5D91B007821C2B222AECA1478B6655,
	StringVector_GetEnumerator_mCC5816AAF97846CA4CCA17639AB53722E8906656,
	StringVector_Clear_m3B553DDC7451E44A0378DA37D4B984A052CB14E8,
	StringVector_Add_mCD2DFAA0ECAB9FE63BECDB8FC26EF1AAB74E400F,
	StringVector_size_m8443AFDDED368738C7F8C9D2DD9A52661C5577E5,
	StringVector_capacity_mEF5027E7F73CB6D370017E39E1321504D8D79450,
	StringVector_reserve_mEC60DB42F62AB46765ACCB8C8772AB3F94EA4689,
	StringVector__ctor_mEAA56D42F5D7CDD4440D56517FB53978430913BF,
	StringVector__ctor_mA561B1608E05D4EC39DF314300A27E41A24861AF,
	StringVector__ctor_m0AFC9A3F9C196D96BCA3C848F78AD497C4FAFECE,
	StringVector_getitemcopy_mD8AD2E42C8F34376D36D8380EB79FBE61E9CA0E9,
	StringVector_getitem_m07E78008F6FF8DAB46D6CCC261075E924B19067A,
	StringVector_setitem_m7DC29A202B2F36D7449BC635DD6229F12DAABCB9,
	StringVector_AddRange_mF4223F95FE7F8C2F1D81EBA2073C1A1E07102F9B,
	StringVector_GetRange_m57E9C6B6F65A48BA67054011078DF3C153B95A8F,
	StringVector_Insert_mF858179020281D07DDE2AF0B629645FC38B605F5,
	StringVector_InsertRange_m8B5376012296EDFEF9364AC3D8598D599DB528D9,
	StringVector_RemoveAt_m833045A12706B1F643DA8511F754C7E142158128,
	StringVector_RemoveRange_m66E1E1E6AD71916514F4F64AD6B2053435B8E0C1,
	StringVector_Repeat_m1A3C64E5D31538A039F8BC3ED50A02CF621AB8A6,
	StringVector_Reverse_m7A7A5926A5C9D5F4F90BD3C611358D36653E2DBF,
	StringVector_Reverse_m71A5F75B13F471537647683CC81BAA5407E7D522,
	StringVector_SetRange_m53404232DDE551EE8DD8D86CAF9422D2DDE41F4C,
	StringVector_Contains_mB231F52B5E1964B193026926A08D422DD4695BC2,
	StringVector_IndexOf_mA74D9345DB8679591C3D560FA5616A21B77766EC,
	StringVector_LastIndexOf_m72860A469A7D414036956826203DDED86218BFFE,
	StringVector_Remove_m8EC3D3733E983BDB6B672538210350BF8069AB20,
	StringVectorEnumerator__ctor_mE47C0DFCBFA6322348B468D00427DDD23510B218,
	StringVectorEnumerator_get_Current_mB7C1982335B4B7743A590DCFE86685FD49E51C5A,
	StringVectorEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mE11C8E9FE11D83B0B43119B01A693738690556BD,
	StringVectorEnumerator_MoveNext_mF03A2ECA82AFA46F92805CE9D33B932DB3516D7A,
	StringVectorEnumerator_Reset_m0637EECA94516C16AE01F3866784AEDEE05BE169,
	StringVectorEnumerator_Dispose_m1EE00336F8E3D571134D741C555338A1546E0A65,
	TestsInternal__ctor_mD9D0D041035D14C2A3CD13A51C7AE06736D3C75B,
	TestsInternal_getCPtr_m3264DD0840679C2054DA559AB9B6C1CB89C99879,
	TestsInternal_Finalize_m363ED8A0251B7E26879DDFE71D1658978AD1F919,
	TestsInternal_Dispose_m06EB8BBE3F5BDFA0BAA091290628023BE7C8D407,
	TestsInternal_PollingDataChannelTest_m6E69B8FF3213E7075EDA95BADBC108A5142BDB01,
	TestsInternal_PollingVideoTest_m9D134E22BAC0DBAAFF1BAA64FE54CF5615663376,
	TestsInternal__ctor_m12F3895B2205D69232A07B654EDF14ACE44AF24F,
	VideoFormat__ctor_mB8C3DD92A0FE815F387EFA6E612CB14886951C51,
	VideoFormat_getCPtr_mFAC5AF61879F8D437382228D3848F94DE0907CB5,
	VideoFormat_Finalize_mFBC66E31490DC7E0D665CB93FBB4575D14EA43F5,
	VideoFormat_Dispose_m638DF4D3EED9D10F5636B8A9ED8C19C55218F58E,
	VideoFormat__ctor_m2F3E9D062685D4C40A04150A960761DB5B45DCC1,
	VideoFormat__ctor_m676CD62CC7719E28333B564C784935A4EBE2B9F1,
	VideoFormat__ctor_mE916BC18C8D2D06D3DC168F95DE3DDB3DD8BA1F6,
	VideoFormat_Construct_m3C26F75F18256020EA1C89470B18C1EDCC5EA016,
	VideoFormat_FpsToInterval_m6B030F15A2D82C6632889D2BBC6A4DDCF6EE2A84,
	VideoFormat_IntervalToFps_mE56AF6D2E5726AA2B92BD1287C5701EF75B2DA3B,
	VideoFormat_IntervalToFpsFloat_mB9EB5F6143E029C067081D136B054058C5A95B62,
	VideoFormat_framerate_mD77EDFA6152C4334CE101E038C1B441E1925A63E,
	VideoFormat_IsSize0x0_mFEAA776C567EB44B7747A1048562823B02B0A7DD,
	VideoFormat_IsPixelRateLess_m73AC0E70BA83046333DAE622E54FFD8277846BB8,
	VideoFormat_ToString_m4DF3F23FD9CD96052AE772F1C2B039DF2D0E01D7,
	VideoFormat__cctor_m937AAA6DF8FB28F3C12F2CFA338E59DB3F0807BA,
	VideoFormatPod__ctor_mD718A721E63DD6AB8C6FA630696CF5526D07BD24,
	VideoFormatPod_getCPtr_m8EBA6D68630CC211E93807A8C3731B8D053E03CD,
	VideoFormatPod_Finalize_m7B223C4CDFDB3211B01E85FF0A5F9A81A8627DFD,
	VideoFormatPod_Dispose_m8C48FA6656E2E058A5F229EB9B6BC8A3FCFE1BC8,
	VideoFormatPod_set_width_m14661118301690F6E87B6A0C6727B5F934BC0ACF,
	VideoFormatPod_get_width_mFA0A3E5304C5E1748B117A76ACEFD72595C7BBE8,
	VideoFormatPod_set_height_mCA84618CBD96FA8BF8DE4250C621B47891D13667,
	VideoFormatPod_get_height_mF2C4E93023F9B74F7BCA814A048E3C638BAF00E5,
	VideoFormatPod_set_interval_mC19D641C1B656580B468390151EFF0439518EF31,
	VideoFormatPod_get_interval_mDAE00D809BAF3D32A4A6F099C36A162A452AAC6D,
	VideoFormatPod_set_fourcc_m3B5FB21626C4C1352D706AE1481C0107740C780C,
	VideoFormatPod_get_fourcc_m47828DC183B2F3E50DEDE114933F848E8B0D7FA1,
	VideoFormatPod__ctor_mEB43A19EA077C1948D087A402B5429ED937273B0,
	VideoInput__ctor_mCA616A150EA7E0A36D64A417576C3F304689AB6B,
	VideoInput_getCPtr_m31193C73059292333813D2CEC4F9BA06F907883D,
	VideoInput_Finalize_m4AB629FB98709CC82B73D982F5CD7E962C3F0DFD,
	VideoInput_Dispose_m484CD43EFDA80596925144A39D1877AAB5C1614E,
	VideoInput_AddDevice_mA117BB2F642450ADF7CF74E6A8598702859612CF,
	VideoInput_UpdateFrame_m387FC0FCCB532A7AFCAE299FA91B68D72CF32DE6,
	VideoInput_UpdateFrame2_mDC8354980F848DC74BAEFCC4ED4D1976844CDD9D,
	VideoInput_RemoveDevice_mA44338C78EC26BB29E5019DA8B01C7B5C7464E44,
	VideoInputRef__ctor_mEFB506E8B6E401C52BE1A724FF5E192C68BA58A0,
	VideoInputRef_getCPtr_m215E3ADE6803E233DA2FCE33A371933191DACEA8,
	VideoInputRef_Finalize_m766E8100CAAAA76943842C64E327F020C452212B,
	VideoInputRef_Dispose_mAC81DA901E79A9A4E3DBFD5578A25C4546561EE1,
	VideoInputRef__ctor_mAB4ADE9D2C1F7C7C0620F403A805F594922C9318,
	VideoInputRef__ctor_m0549D7E1738859EE48B2C6A6C4553F7800F626F3,
	VideoInputRef__ctor_mCCD1BD798E26B3BD0380D130CB05009C82A4CBA5,
	VideoInputRef_get_m8B17AC094427D90C00C18DBA286655848BA33058,
	VideoInputRef___deref___m239216813F2A4E1AB7ECF261769EEC4809026D28,
	VideoInputRef_release_m5D3A92CB04C67B6C5C685A8F7EFA06C5D0A17ACD,
	VideoInputRef_swap_m9ECF7BB61BF4C800F3D55C560618259DC896B7CE,
	VideoInputRef_swap_m7A8FCFA35C45B709803361004F64BEE2C84E11D5,
	VideoInputRef_AddDevice_mC8AA44DFF330F70308B14A9A783CEC946FBFCB18,
	VideoInputRef_UpdateFrame_mC82C903116DEF38C1A031E8775CAAFD14A338814,
	VideoInputRef_UpdateFrame2_m81967F03D65297DC5AA793C4214B41BC2E89CB9B,
	VideoInputRef_RemoveDevice_mC58204AF39E214194130A7ECF20D0948EEAB782F,
	VideoInputRef_AddRef_m234EED3927078C4E817A1D820C8F304889523F84,
	VideoInputRef_Release_mA2DC18C6B7E8496AB20ED5AB8A2ECDBBEBF06B6F,
	WebRtcSwig_sin_m254F1FB82CA078FD311DD3F9D2479727AC72E7F1,
	WebRtcSwig_get_kPerfectPSNR_m9DE2D3E25D0C178E2C0C7770CEE7B9F9301C36D6,
	WebRtcSwig_get_kI420_mB91B6C7FC4D7193F9847046068DDB3D450B9D6EB,
	WebRtcSwig_CalcBufferSize_mCE4191B04A98CB6D6B40FEFC12BE423D792DBC6E,
	WebRtcSwig_PrintVideoFrame_m2108908725E514708BC2C2292A58AC7B24837A6C,
	WebRtcSwig_PrintVideoFrame_m341EEE7E9526E5CBCD8C97E49834E1974DDAE183,
	WebRtcSwig_ExtractBuffer_m81E8113D4F2ED583D820A2AA0A1284CF153BF9BA,
	WebRtcSwig_ExtractBuffer_m763FB1F4E719E9C4FEAD0BE9EFF6B02EB3924BC1,
	WebRtcSwig_ConvertFromI420_mA870B4C8D34B57EBECD75B7902D06E8A6FB556BA,
	WebRtcSwig_I420PSNR_m18CF8F4DA96E92CD64F9DC9345787B2BC92290F2,
	WebRtcSwig_I420PSNR_m59F4505BFE12AEEE1E3FEA24FA8479FCF73BBE83,
	WebRtcSwig_I420SSIM_mEC829729DDF4E60058BA5701DECA5723E79E91E4,
	WebRtcSwig_I420SSIM_mC71A8E53F7D7FD098667AD19EB7B3D8E90D136DC,
	WebRtcSwig_NV12Scale_mC0CF2B9752D17BE1DB6C1C49B47295170EAA5F8D,
	WebRtcSwig_ConvertVideoType_m990A5D4E61E312A35FD5273E4B3379AC61C99DAF,
	WebRtcSwig_get_kDummyVideoSsrc_mCC61684C5E3D3363334A8F1104A5EFA6AD7F7887,
	WebRtcSwig_get_FOURCC_ANY_m8095F26F4B5CF6447215B9E01D3F6662EF5D923A,
	WebRtcSwig_CanonicalFourCC_mDD657897FE364BD672F800738133EF8EAD5CA42C,
	WebRtcSwig_GetFourccName_m941A53D81DB3707C4056E9ED01AE1E56D777EA27,
	WebRtcSwig_WrapTestConnection_PollingDataChannelTest_m3D09D693F2438400ACF7E818938879E4E8B4082E,
	WebRtcSwig_WrapTestConnection_PollingVideoTest_m356411BEE31531D699B9793C3541E453C7129C92,
	WebRtcSwig__ctor_m399F93C7A2C84E13EAC8DEFCCD3ED7B6B7B0CE26,
	WebRtcSwigPINVOKE__cctor_m9F0045712625FC645A04AD308FB46A4B7AA60496,
	WebRtcSwigPINVOKE_sin_m3515EF1E83AE22078B9EDCAF229936F718045601,
	WebRtcSwigPINVOKE_RefCountInterface_AddRef_mF9A1BF0038C4639F0748EE6E2F6588FAF21F50DA,
	WebRtcSwigPINVOKE_RefCountInterface_Release_mEAD1E637CE912C6E38A77F87E9EA8D5E932F01D1,
	WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_0_m633F3F64CD6CF89A4CDEE0F5FFCEEEE466445E4F,
	WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_1_mB4F9DA566F0F31801DB194649096B6C756A947D2,
	WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_3_m7FE8E17F1D98F182924F3C8E1C0B8CC6F834A7EA,
	WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_4_mFD439FCB07219127A35D680ABB7FCF472961EF05,
	WebRtcSwigPINVOKE_new_CopyOnWriteBuffer__SWIG_5_mD831BA41E48C91CD09C85467DE250595DB4B5BE8,
	WebRtcSwigPINVOKE_delete_CopyOnWriteBuffer_mA08087A14EEA8CB65159E00EBE81CB5EE661B6B6,
	WebRtcSwigPINVOKE_CopyOnWriteBuffer_size_m9F9F2D9207D5FDC61C641B956230138EADFE679E,
	WebRtcSwigPINVOKE_CopyOnWriteBuffer_capacity_m506DBB386DE0280DC2869413AAFB4D521AF0FE35,
	WebRtcSwigPINVOKE_CopyOnWriteBuffer_SetData_mE27690A591DAACDA7400DC4E8625C7B7645F5C0F,
	WebRtcSwigPINVOKE_CopyOnWriteBuffer_AppendData_m8FC3381AB902C5BFECB5A01F08A8353FFC3ABF0D,
	WebRtcSwigPINVOKE_CopyOnWriteBuffer_SetSize_m57ED93A498AB6BB25C2C3804445D8DA4F75BC3B3,
	WebRtcSwigPINVOKE_CopyOnWriteBuffer_EnsureCapacity_mB60701102A62F1206EFEAEDFDDC6DDB7A64F6D0D,
	WebRtcSwigPINVOKE_CopyOnWriteBuffer_Clear_m5F70E2C223C4A5A02F33ED859249619EE45D1F7C,
	WebRtcSwigPINVOKE_kPerfectPSNR_get_mED88504BF7DA0C12AA6B41CA4D1D4323C8BC61A8,
	WebRtcSwigPINVOKE_kI420_get_mCAC4F3A58DB1AB018BB20516478380D8201A3665,
	WebRtcSwigPINVOKE_CalcBufferSize_m6930C6660168AAB58B73E310B6E0C31C6A8F1245,
	WebRtcSwigPINVOKE_PrintVideoFrame__SWIG_0_m9F44D5876C3907B8DEB94EFA1DF594FFA596B694,
	WebRtcSwigPINVOKE_PrintVideoFrame__SWIG_1_m301F12E5434222E23648CAF45CE551669544DC14,
	WebRtcSwigPINVOKE_ExtractBuffer__SWIG_0_m78C91601CBC4DD33047E666D3969CE301925E0BA,
	WebRtcSwigPINVOKE_ExtractBuffer__SWIG_1_mA6EEE9BAFF64A4140F3CD6C945BE744E3196E04A,
	WebRtcSwigPINVOKE_ConvertFromI420_m9D62757A7A84B6E6A8EC30D9368E8307644481F8,
	WebRtcSwigPINVOKE_I420PSNR__SWIG_0_m9A4AE6AF5A4E720AFDDF898BE70A7FF8491B9446,
	WebRtcSwigPINVOKE_I420PSNR__SWIG_1_mA673D776A7BA48ABBBFF70A2D7275A30AFE654A0,
	WebRtcSwigPINVOKE_I420SSIM__SWIG_0_m229178CA1331AF33CE656FE8106DC7C64BA6A4AC,
	WebRtcSwigPINVOKE_I420SSIM__SWIG_1_m4C8484BE609E3D3C96795D40D1363233C2DDAB01,
	WebRtcSwigPINVOKE_NV12Scale_m2D44E9DC31105C20FA760AC4F8A7FF1F6756FEED,
	WebRtcSwigPINVOKE_new_NV12ToI420Scaler_m6E7BC4A5D6F2E0FEA0129C51F024441A0FFC30A2,
	WebRtcSwigPINVOKE_delete_NV12ToI420Scaler_mEC9F36A842EE18558A764AFCE438F934B989D7F3,
	WebRtcSwigPINVOKE_NV12ToI420Scaler_NV12ToI420Scale_m9775DCCC1A2AE0BA85D31CC1AB78272D9ED363ED,
	WebRtcSwigPINVOKE_ConvertVideoType_m5D3961E4BF492A4FAFEF29A015F6BDA128A00CEB,
	WebRtcSwigPINVOKE_kDummyVideoSsrc_get_mE45C4A172B3C144802E445CEB6BDC9E2D0B5424D,
	WebRtcSwigPINVOKE_FOURCC_ANY_get_m3863D46F637FFB36C15256BC2675F3EF3248E4D6,
	WebRtcSwigPINVOKE_CanonicalFourCC_m7F3C5F285AA25E317A5E1F9FD3EADBB63559A7FF,
	WebRtcSwigPINVOKE_GetFourccName_m26078498C29E773587AAAC98DF32AE0F6FA49C74,
	WebRtcSwigPINVOKE_VideoFormatPod_width_set_m02112977470F4573EC15B95DEF8609678523CE25,
	WebRtcSwigPINVOKE_VideoFormatPod_width_get_mF1B220626E08AD42BAAB4523BDCB4D6838219AB8,
	WebRtcSwigPINVOKE_VideoFormatPod_height_set_m0132D52160571B134AEB3F14E610AB3C03940A27,
	WebRtcSwigPINVOKE_VideoFormatPod_height_get_mC95F40A175CB3D46C671F6273EF37F9C89719758,
	WebRtcSwigPINVOKE_VideoFormatPod_interval_set_m9AA9FC8FC00FAB2EB490BFA4135C961E3F2E295E,
	WebRtcSwigPINVOKE_VideoFormatPod_interval_get_mEE367E79B150C8E85D285025D375B4D586159E61,
	WebRtcSwigPINVOKE_VideoFormatPod_fourcc_set_m1D90C2B79985324607B54F8DBACCECEB74F86D49,
	WebRtcSwigPINVOKE_VideoFormatPod_fourcc_get_m216A1678C2A927AEEDCF5A38F2385999D6109B7D,
	WebRtcSwigPINVOKE_new_VideoFormatPod_m22F6BBEB8CFBD0744B2BEE5C2B14ADCA51282737,
	WebRtcSwigPINVOKE_delete_VideoFormatPod_m0980823C92DF464C0B38A1DCD05EB66C31A6B590,
	WebRtcSwigPINVOKE_VideoFormat_kMinimumInterval_get_m2D36F06D571E16AB5F454CED101B6BB48321BC02,
	WebRtcSwigPINVOKE_new_VideoFormat__SWIG_0_m2A8B376454CAB6AE2DCE7D8ACB3751667A51A12C,
	WebRtcSwigPINVOKE_new_VideoFormat__SWIG_1_mD676E4B6529EB6883A91DC05741D371653D3FF79,
	WebRtcSwigPINVOKE_new_VideoFormat__SWIG_2_m7250F350110FB8C8728EF188AD6B36E2D5823757,
	WebRtcSwigPINVOKE_VideoFormat_Construct_m776DD132F4D536B536BD03B666B60BD6DFA60B32,
	WebRtcSwigPINVOKE_VideoFormat_FpsToInterval_m4A53D5C8A0356398CA73F6543DBEFA86B9F28359,
	WebRtcSwigPINVOKE_VideoFormat_IntervalToFps_m537BB9A9D68E8C59D6BC573DB7A5AD0D23DBD480,
	WebRtcSwigPINVOKE_VideoFormat_IntervalToFpsFloat_mBD2E7EC642E851CB4FC0A3480D9924D6EB5B9EE3,
	WebRtcSwigPINVOKE_VideoFormat_framerate_m303586D1265F912DF322D20710BF1FC32870AD55,
	WebRtcSwigPINVOKE_VideoFormat_IsSize0x0_m0D51158D3C3BFA5C5DB87FA625D9A4EA032E9585,
	WebRtcSwigPINVOKE_VideoFormat_IsPixelRateLess_mDA85C16AD3D79D62979B80BE5835171CAB465B19,
	WebRtcSwigPINVOKE_VideoFormat_ToString_m21CB0422ED82007EF140170D7199AAFB15D8F380,
	WebRtcSwigPINVOKE_delete_VideoFormat_m8EC44AA5FD5109D5E5D274BDFAA08EA7B7B8F08D,
	WebRtcSwigPINVOKE_DataChannelInit_reliable_set_mE330356B8BD61FE1CD3166C1285D576FC8854BB5,
	WebRtcSwigPINVOKE_DataChannelInit_reliable_get_m0AE310D03A21417DD84A7FE2376E91832F2DFAFC,
	WebRtcSwigPINVOKE_DataChannelInit_ordered_set_m5AF02C4430BCC4A167EAFA39BB86B68CABD4523A,
	WebRtcSwigPINVOKE_DataChannelInit_ordered_get_mBF5B641DA447F11E67968CCFC743E6F9177E84AF,
	WebRtcSwigPINVOKE_DataChannelInit_maxRetransmitTime_set_mBB45927B5EF0A29612C7BF3AF171DE81F1EC46CF,
	WebRtcSwigPINVOKE_DataChannelInit_maxRetransmitTime_get_m937680FB786FE1C09F8BF351A09762C8C0C56F1F,
	WebRtcSwigPINVOKE_DataChannelInit_maxRetransmits_set_m2497C7CC3F6A70F93AF6C3CC32AA3A964C1F6AB6,
	WebRtcSwigPINVOKE_DataChannelInit_maxRetransmits_get_mBEBCF705723190B42BD186687C17DA00149FDC67,
	WebRtcSwigPINVOKE_DataChannelInit_protocol_set_m7C037FDF43EE929F284131F1313D3E8F3EB77EA5,
	WebRtcSwigPINVOKE_DataChannelInit_protocol_get_m30C0F2FA23CC7C880CDDD97CCD736587FB055AFF,
	WebRtcSwigPINVOKE_DataChannelInit_negotiated_set_m0B23D86F1B3EBEB1E31C649DB8A6B464C8F184FC,
	WebRtcSwigPINVOKE_DataChannelInit_negotiated_get_m473CCF7D9EAE3B7EEE56583BB716BE13AD460321,
	WebRtcSwigPINVOKE_DataChannelInit_id_set_m01FC6DF7A49B705A56DE43A0D6F008BC3046C398,
	WebRtcSwigPINVOKE_DataChannelInit_id_get_m09ADE6495C22FE15CA0EB830563EF919F9E8EB63,
	WebRtcSwigPINVOKE_new_DataChannelInit_m01BD8BD2995B23947345D0AC2AE7BEF576ECFB22,
	WebRtcSwigPINVOKE_delete_DataChannelInit_mF9B1F5DD484DAA610466D9707B85FA3E8D949BD9,
	WebRtcSwigPINVOKE_new_DataBuffer__SWIG_0_m9BF9E7B5CFB77EF4AEB6E6D80F444BF1F9E6B64B,
	WebRtcSwigPINVOKE_new_DataBuffer__SWIG_1_m6B741C037B87DEDBF4BC6519510BAFEBEEEB91A5,
	WebRtcSwigPINVOKE_DataBuffer_size_mA5AAE4F3E4EDF62346D68B0504D1FCBDAC0A13B7,
	WebRtcSwigPINVOKE_DataBuffer_data_set_m1958C552A13E3A961527D1F5C1874252E4B979A3,
	WebRtcSwigPINVOKE_DataBuffer_data_get_mC5AFDA4DC47FBB2349CF479D9D1CD748E8BFAB01,
	WebRtcSwigPINVOKE_DataBuffer_binary_set_m99156CAD23B59A0E8B7B56F6E17CC9A8F5E5E758,
	WebRtcSwigPINVOKE_DataBuffer_binary_get_mA3B01D6CABEF84A16DFC3853C3B7CE2BB8B1319E,
	WebRtcSwigPINVOKE_delete_DataBuffer_mFAE81B89BE3960DF366B6810DFA8BDFBBDF64D57,
	WebRtcSwigPINVOKE_DataChannelObserver_OnStateChange_m1661FC0F287546E0369266FC0D9C21D1D3FB19ED,
	WebRtcSwigPINVOKE_DataChannelObserver_OnMessage_mB8B1FDB3535136FCA0A1A0272ADD763D54ABE207,
	WebRtcSwigPINVOKE_DataChannelObserver_OnBufferedAmountChange_m926A616152A843FBA8E91DDAACC51D6BEA9E7C12,
	WebRtcSwigPINVOKE_DataChannelInterface_DataStateString_m6CBE775E89961DAE05833A07536BB1EF62F4A19B,
	WebRtcSwigPINVOKE_DataChannelInterface_RegisterObserver_m21D617B705B39773AB5AAF65CBB2DE171E1E1328,
	WebRtcSwigPINVOKE_DataChannelInterface_UnregisterObserver_mC63D0680AE1BCCCC908B2D16005897F05DA052ED,
	WebRtcSwigPINVOKE_DataChannelInterface_label_m58EAC564A343CD0385A04EC5D74ACD6BA6288514,
	WebRtcSwigPINVOKE_DataChannelInterface_reliable_mFAAA926D67E062844A8E5D17F17D8F05A5CA8B8E,
	WebRtcSwigPINVOKE_DataChannelInterface_ordered_m196F803AB3EB7E8100DF5D4128406BD9636DE485,
	WebRtcSwigPINVOKE_DataChannelInterface_maxRetransmitTime_m1651C8866060B38305297AE3BC5DE48A0AE4EB98,
	WebRtcSwigPINVOKE_DataChannelInterface_maxRetransmits_m15600EB64F07A5477F497760B6CB617AB2F24259,
	WebRtcSwigPINVOKE_DataChannelInterface_protocol_m3EB67A7D7428CACA203CABC072BBE1B5E9B631CB,
	WebRtcSwigPINVOKE_DataChannelInterface_negotiated_m7CBA9B6CECDF0BA320FC2EAA2346D3B143DCDC82,
	WebRtcSwigPINVOKE_DataChannelInterface_id_mF542D4D63A5E1E807281E667460C82040EB3507E,
	WebRtcSwigPINVOKE_DataChannelInterface_state_m3FC3F5D8DE178909C1CEC0B23494AEE72C56CA47,
	WebRtcSwigPINVOKE_DataChannelInterface_messages_sent_m97B2947E709FAD74D1B8675285A86CDA462136CE,
	WebRtcSwigPINVOKE_DataChannelInterface_bytes_sent_mEBA2B37FF53EFF0CB4705E4B829969713394AFC2,
	WebRtcSwigPINVOKE_DataChannelInterface_messages_received_mC1AD7C8BB3C5DDF7D8648159D83CB62CAD4A2FAC,
	WebRtcSwigPINVOKE_DataChannelInterface_bytes_received_mCB7E077570DE924D90B11D5882A81B7631ECCE60,
	WebRtcSwigPINVOKE_DataChannelInterface_buffered_amount_m4F9FC0C653C9F1FF2BD5B34E211177038C580FEF,
	WebRtcSwigPINVOKE_DataChannelInterface_Close_mDD6436EE590FB5F09DBE7F6CF4AC5307B36F98DC,
	WebRtcSwigPINVOKE_DataChannelInterface_Send_mA2347F6D43E269CB3148D980BDFDB5C35CD53951,
	WebRtcSwigPINVOKE_StringVector_Clear_m3E4623D52C125A97EAF73E21E02C8196F64555F3,
	WebRtcSwigPINVOKE_StringVector_Add_m895ACECA491EE3EE904F040CC86FBE6469AC8829,
	WebRtcSwigPINVOKE_StringVector_size_mB35FEBB84CA8582811CE14B8837188992738A80A,
	WebRtcSwigPINVOKE_StringVector_capacity_m1E1E9F990EBBEC03F9C913BC017160837B6AA36C,
	WebRtcSwigPINVOKE_StringVector_reserve_mA68BE8B4092B4F9AF49A3C3529AD45620DBDCD38,
	WebRtcSwigPINVOKE_new_StringVector__SWIG_0_m13D65C9FA67F475B1BEC422E695E4335B4DE83F9,
	WebRtcSwigPINVOKE_new_StringVector__SWIG_1_m31E9AF47B9ACB72F18490C78C53B30A8B7AA8AC1,
	WebRtcSwigPINVOKE_new_StringVector__SWIG_2_m8401E4B4E88090FB0B2676CCEF3212F61AC820CB,
	WebRtcSwigPINVOKE_StringVector_getitemcopy_m1749C9BFAE8CA482BE4880327748B284BBD6B803,
	WebRtcSwigPINVOKE_StringVector_getitem_m8E95690F6AEE669B0DF428AD9BF1597EA346EBF4,
	WebRtcSwigPINVOKE_StringVector_setitem_m629F089B217D56293A99C5483F9D87D5321F0643,
	WebRtcSwigPINVOKE_StringVector_AddRange_m9DC2B48DBC66AFFE24687A6309A2E002B7B241C5,
	WebRtcSwigPINVOKE_StringVector_GetRange_mE9E75A19061F106D722D4B1B7F65C5EF2CDF57DE,
	WebRtcSwigPINVOKE_StringVector_Insert_m20B05277ECF3FBF61FC7BBCE42CFB1E855074634,
	WebRtcSwigPINVOKE_StringVector_InsertRange_m51162F4F6EAC6ADD91BC072C088BACD50A5A0588,
	WebRtcSwigPINVOKE_StringVector_RemoveAt_mF4724A7CE9C68D1C73925DB137A83E9D4B725E40,
	WebRtcSwigPINVOKE_StringVector_RemoveRange_m9B87AF2D779F0D743767D092C246A4F1F34F7289,
	WebRtcSwigPINVOKE_StringVector_Repeat_mB2F5C08DCD5E6817CF7AC7D628BA695E1AB0DE5F,
	WebRtcSwigPINVOKE_StringVector_Reverse__SWIG_0_mA228381086EE9EB25A891A8AD4B3E870467F3B04,
	WebRtcSwigPINVOKE_StringVector_Reverse__SWIG_1_mEBD3CE7428B68C5A58CB4AB66325EA73A53C5267,
	WebRtcSwigPINVOKE_StringVector_SetRange_mD1F078BB4200DEE92F512E3D2155C01811414E59,
	WebRtcSwigPINVOKE_StringVector_Contains_m45BDE1C9ED0FA185D7CEC60801AF34E0F6B1A089,
	WebRtcSwigPINVOKE_StringVector_IndexOf_m70A56431F19CBD53B48E270B219B966E25F51B5C,
	WebRtcSwigPINVOKE_StringVector_LastIndexOf_m41F443AA4809D1B8CDAD667E7B3CF56F569BA71F,
	WebRtcSwigPINVOKE_StringVector_Remove_m5544A894058460A42A85B1CEA77731726B84BCFD,
	WebRtcSwigPINVOKE_delete_StringVector_mDCE6A372535A36C51EE6CC2ED2E37785D0B4D7BD,
	WebRtcSwigPINVOKE_IceServers_Clear_m5BBD24E86FB111BB430EA16515D9A92E3C9D14C5,
	WebRtcSwigPINVOKE_IceServers_Add_m06C32CD7B8EA282AF895418D71F56B3F5F38C126,
	WebRtcSwigPINVOKE_IceServers_size_mD91C0C4DA469ED49119EEFD63472056D8019ED08,
	WebRtcSwigPINVOKE_IceServers_capacity_mCDF1B3D793AAC7F19850583F0BFB8510B242C49F,
	WebRtcSwigPINVOKE_IceServers_reserve_mA8DF3AE1DE0F6EE99486BD07D87963C223A429D2,
	WebRtcSwigPINVOKE_new_IceServers__SWIG_0_mB7FCD08B9FC21F703329BD8AC5309B430BB9975D,
	WebRtcSwigPINVOKE_new_IceServers__SWIG_1_mA3A9D16A5E1A39F90D08883DB64159AB846C5803,
	WebRtcSwigPINVOKE_new_IceServers__SWIG_2_m8AA5275CB1D87DE86AD4EDDE015FB62259808D3A,
	WebRtcSwigPINVOKE_IceServers_getitemcopy_m0B1B5133F2F211B0CD8798C8FA27DDB3974C1AC7,
	WebRtcSwigPINVOKE_IceServers_getitem_mE5170A6DF20577B658ACEB18A950E5BF75CED782,
	WebRtcSwigPINVOKE_IceServers_setitem_m342AA2995EDCE164A66C60141ED98FB7D17F821C,
	WebRtcSwigPINVOKE_IceServers_AddRange_mF21A4938B01928C632E060D11196EE9F53E4B685,
	WebRtcSwigPINVOKE_IceServers_GetRange_m7660D5231D8754D122F64B45FC104A85EAB53A4E,
	WebRtcSwigPINVOKE_IceServers_Insert_mC6847A3A89B68F02779F352DC5EB6E42B7627D45,
	WebRtcSwigPINVOKE_IceServers_InsertRange_m9F2350739851AA57D6EB4A8CA86A42CC24A1EDA9,
	WebRtcSwigPINVOKE_IceServers_RemoveAt_m6369C6A4BEFBB90B020C5F4A3B4E7E9045082D50,
	WebRtcSwigPINVOKE_IceServers_RemoveRange_mCE18330110B56F66A4D9FF110F6DD1022EC3C173,
	WebRtcSwigPINVOKE_IceServers_Repeat_m5F21BF385B8CE94CD4EE3EA33B4F3EE364C40B0A,
	WebRtcSwigPINVOKE_IceServers_Reverse__SWIG_0_mDA437369F53EC8AEEE0841A45908C1DC4A7277EB,
	WebRtcSwigPINVOKE_IceServers_Reverse__SWIG_1_m67211813072719691382C9F0A223FCCB0786F376,
	WebRtcSwigPINVOKE_IceServers_SetRange_mFBFF5E3A4424A048A1077A373611B4ADEB997034,
	WebRtcSwigPINVOKE_delete_IceServers_m15B43CBF2996FD00ABF86A29669B2C4507C81958,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_IceServer__SWIG_0_mB4F56222DE7C1618D6054E164B121091852EE2E2,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_IceServer__SWIG_1_mA130D69D7D9AD1034D3378B10F0810335AF29306,
	WebRtcSwigPINVOKE_delete_PeerConnectionInterface_IceServer_m2EFC26FFC68E7B77BF61B2F608EBB03E36DBA3FC,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_uri_set_m5A89E9F3AE5A2D843D8B09E300FF4F9A35730B51,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_uri_get_mE06C8FFFC20A5DF58650065C50532B460B1ACED7,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_urls_set_mFEB4F5BEB4D885093BB2A18BF17EE23E92BA7505,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_urls_get_m7759EDDC38C2792916B4FD4DF98FBD94D072BBA8,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_username_set_mE113888294AAAE2B301CE941A23012BEFE7B2038,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_username_get_m78A821FDD021B0BF957FF2DCF21450AA5EC3C04F,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_password_set_m0435113B6F9CCBCD807C01C7CC15DEDE4F22BB19,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_password_get_mB72062ED34313D6F9FC6CFE6B36C045D2F243044,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_cert_policy_set_mDACCBA95CAF87E12ACD17C5B8D2E2A9DCD275F12,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_cert_policy_get_m55EDCB3D63F3CB5A9561DA3A95E4F18339F95A07,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_hostname_set_m9594B1C7F5ACC5996CA5145F3A3C877042A951DE,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_hostname_get_m4DC776B95D107B6EEFD8FE4C714A85B6F8ACFAC2,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_alpn_protocols_set_m8856C5900D6083A23116DCFFD1433774553A9A8E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_alpn_protocols_get_mB3039A4CFD51C0A91D1BE209B4BADE94485A4FA7,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_elliptic_curves_set_m3D2C5F9D07106364F289A6231B41F93B1319464E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_IceServer_tls_elliptic_curves_get_m0CC6F65C6E25BC4F0B43C0996541B341EE27C201,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCConfiguration__SWIG_0_mEC455A075BB0FFD04BE47047BBBD0189AD939702,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCConfiguration__SWIG_1_mAA8F42FEED214F058B9B5292ECD186CA2E7B2ACE,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCConfiguration__SWIG_2_m5308A2849AABE00B3DEE9EF3587E5F90B965625C,
	WebRtcSwigPINVOKE_delete_PeerConnectionInterface_RTCConfiguration_m3338D9932C6AED3E4FA6D89D2CEF773E773E486A,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_dscp_mE3488550F8EFE7E9D4FC697D05D41FE89E359931,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_dscp_m11C608CD35A9172A762A71271D665AA8FBA2E96E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_cpu_adaptation_mD77AD7376E70F26FAD24547BD191055AB2884849,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_cpu_adaptation_m39B85CA8BCB90726F81DD58D79330D32E703B9BB,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_suspend_below_min_bitrate_m10823E4C418526CC7EB94E5A99989CD2B9553178,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_suspend_below_min_bitrate_mD12ECB4012F73E74DB7605FFDFA41EA32EEE528F,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prerenderer_smoothing_mAB8CE050D859D8065B13C12CAD1F344FFE52AFCE,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_prerenderer_smoothing_m83EEDCA13531C716F049C83D69CB60F7E5D61DDA,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_experiment_cpu_load_estimator_mBF4A99B3831990265105ACC82E5B0140B241B764,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_experiment_cpu_load_estimator_mEF9D3C26E8D6BC1A63B99C4F877D6F4C7B97E6CA,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_rtcp_report_interval_ms_m99FD22F517905B0E035D56FAB432F52A4E08E13A,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_audio_rtcp_report_interval_ms_mB046FE9AC82787D53B3FA55E4FBA5A68A9DB643F,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_video_rtcp_report_interval_ms_mDE5B3D0791BF174AE3D3B9C42F26121767764C7C,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_set_video_rtcp_report_interval_ms_mFCBAE3AE5BD3274D2CE5D9F7FB7014F777785D42,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kUndefined_get_mC57A1E0A058CCBCB1DFBBBFFC39DC90C243CC7D4,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kAudioJitterBufferMaxPackets_get_m8A030FF39AD521E247631DF59EA7F037C6F42C48,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_kAggressiveIceConnectionReceivingTimeout_get_m24B9C2AB098C12B9ECEB9A83E2068435F554CA11,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_servers_set_m8B50462B8F11BCE5BB3AA043053ACC3BD5BA6D5C,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_servers_get_m8CD3ABCA706711DB84E92728CFF3525ACADEEFF0,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_type_set_m1A8896328B627729861AE071AE791391FE2888FB,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_type_get_m9B6499792E41E0108FC161F1E29F8AAD53DF7A76,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_bundle_policy_set_mFEFED6FC917F19D16FABA5AF5E5D1743874E6AC2,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_bundle_policy_get_m14CCCA5B0CF6F7C9216A856B692FEEAE13A6A4C0,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_rtcp_mux_policy_set_m86E0986490D75BBA9F25859991FD5BB114A0D279,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_rtcp_mux_policy_get_m6075D47F07BBD3CC902E0EDDD2E921FB7612E067,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_certificates_set_mEAD15650F56026A8E85A690B5CEF0E0E041AF933,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_certificates_get_m56B261C81D93F173F8C5B4C28F01255B702C0953,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_candidate_pool_size_set_mAA03B6B1389434E302A97299E0BD1902178C4432,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_candidate_pool_size_get_m31AEAA6294A7487624E8BE1410ED1EBA74551521,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_ipv6_set_mDEEF217CD0119D748D87311E78224041F1D7B45F,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_ipv6_get_mF0016616066BD9541F3A5736A26084355C210F02,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_ipv6_on_wifi_set_mB21ECE0B9BFEC5F9689976A0FAD6E8F59CD9B719,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_ipv6_on_wifi_get_m20021AB80A9DE2B5147E4CB18AD744C48B5F10B5,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_max_ipv6_networks_set_m4FFD5D58F9F55490F2CC13BCFBBC71947C699303,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_max_ipv6_networks_get_m0C3302F903BD6F5E507609F9A523B3436342139F,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_link_local_networks_set_m137B9A4ECF61CD62C5678870202CF39402B324A4,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_disable_link_local_networks_get_mCFBFDC00C72E0DF86009C1BBA4D3E40D56AAD168,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_rtp_data_channel_set_mBF99CB8810F6EBF0CA898B16154C23697E15696E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_rtp_data_channel_get_m5E32F2357AA9762F79E2B3DABC36486A5DBC8235,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_screencast_min_bitrate_set_m944A41B630985AC86BDA748F7C8CCC73B4A22504,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_screencast_min_bitrate_get_mE14AB988737F0139B3E48E7BD18AE81783D4C389,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_combined_audio_video_bwe_set_mC035361D39C9081758CA335941D6CF6B0A0AC55E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_combined_audio_video_bwe_get_mCFF577E5D4490F8BDF44649F03D2F78BEA4B179C,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_dtls_srtp_set_m316EE3C88E5CE8AD64DD5BC924762159D6DC57D1,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_dtls_srtp_get_mAC9C455194097F67A8D5B5E18948407DC46FE9AA,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_tcp_candidate_policy_set_mF6EBBD77A4E1610F038EF478A504823FF18B368E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_tcp_candidate_policy_get_mFBF421F07184E9A7BDA978026C12DB25A242BA7A,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_candidate_network_policy_set_mFD1C38393850A433741929BB12715B42E91F207F,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_candidate_network_policy_get_mDC7AE1EED25BEC7C7B76EA7057D325FF165FE06D,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_max_packets_set_m5CF5BB4A706C8F6B85DC9E5D40DC182DE44ECE9F,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_max_packets_get_mEA81327A203F4AA60EDB5AA8A57D45F261D5BB4A,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_fast_accelerate_set_mC606418A2CD5DE082AC5262E6F2B9F97BBEA5C26,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_fast_accelerate_get_m23E82E573B717DF5FCF57B3E1AE610ADAB282A35,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_min_delay_ms_set_m3E839830ECDF263B710AA302D6F19DC06A261179,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_audio_jitter_buffer_min_delay_ms_get_mD0292AF575DE82CD11E484C8216B6674CC748692,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_connection_receiving_timeout_set_m2465C82653AEAE0BD940D7E927829F3736245154,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_connection_receiving_timeout_get_m3E6F43CAAFD814C48766AA608F0B219B2B0ABB20,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_backup_candidate_pair_ping_interval_set_mDA57D62C3A73E0F3CDC4372592495C4D6E43776B,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_backup_candidate_pair_ping_interval_get_mE0F40BF6E0D8FFB561A500783CCB711D858CC22B,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_continual_gathering_policy_set_m11F5BC09C2B27A48086DF32D2A0AFDB562809AB4,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_continual_gathering_policy_get_mB969007BBF74F0D95D2921302BC9D71EEDAE0470,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prioritize_most_likely_ice_candidate_pairs_set_m2D5F146BA5E22E869223CBCDA50585CF078572D1,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prioritize_most_likely_ice_candidate_pairs_get_mF8043351D6F135475C60A5AC2E5C199FBAFEC9F4,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_media_config_set_m0A096CE060BF166C5236DC27C3DBC1893E917E96,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_media_config_get_m2F71784E3FD5217CBA00F71CF13D896839CD521B,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prune_turn_ports_set_mA7EDAD177574BBD2A979F6650635D7E2A7541C07,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_prune_turn_ports_get_m23B86F484D50EE5BD164864227FD3052E970590F,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_presume_writable_when_fully_relayed_set_m1E0E4BDE4E8980AA74A37EB756D0EE74A7EC5997,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_presume_writable_when_fully_relayed_get_mF4BB882ED9F6C42A73A932CD2244FE036C871582,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_ice_renomination_set_mDB17670C6AC33A0002231493AEB23D0C943C86B6,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_enable_ice_renomination_get_m60C78C531CE6A440F7528EE4677764B05679C3B4,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_redetermine_role_on_ice_restart_set_m0AA0ECC41B7582B31E06E2B78821CD0198E4D872,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_redetermine_role_on_ice_restart_get_mA1E3B705A4E2E82C7754F9B70486AC169828D9A8,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_interval_strong_connectivity_set_m03C3B8B043369894273F36D8A76DDBEDF7464BC3,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_interval_strong_connectivity_get_mBFC63B9A50D4E4C7786CDA62149AE81F01F94C34,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_interval_weak_connectivity_set_mF9F587B78299EADEBCC117C6448DA63C1EF68A53,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_interval_weak_connectivity_get_m5CAC35C85FBB7102664037FBB2B53CE17D20BDA4,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_min_interval_set_mC361E77C7ED18A2FE8F4DCA71465E33A27B4F6DB,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_check_min_interval_get_m82A65C339D6AC6E19C37160457BB5CA5A47BAF38,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_unwritable_timeout_set_mAA3AA5BA5049D2DE4DF3978F5EA6CA3C4CE32A09,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_unwritable_timeout_get_m84CB346898775D95F5F2F7D1CFF6376DE860091E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_unwritable_min_checks_set_mAFC38377B151464D96D8140C9C22F12BA2E995AC,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_unwritable_min_checks_get_mC690855D7066DA1E943E547E60C13093CC785B62,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_stun_candidate_keepalive_interval_set_mEAB8B62C0AAAF19A6A9019C696C63739D20CF124,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_stun_candidate_keepalive_interval_get_m3F958F7E5CBE44F53B9556FBFDB8201C6E935AB9,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_regather_interval_range_set_m49F303E21B1ACAAC99BCEF9B8A6D8FD45010A96C,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_ice_regather_interval_range_get_m8DE5355094E098877D38BE4F9217C01EE200F46C,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_turn_customizer_set_m3158B814B74721793683B0D3E726CBEA3B029BB2,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_turn_customizer_get_m98046DC17D160D372A6084693D1EA6CED6278092,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_network_preference_set_m850EB4C7BD0276D592058D7C9327FE4DCDB9DEBE,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_network_preference_get_mC445CAF7586975CB61F1D16ADB7578195A8EC965,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_sdp_semantics_set_m43065160C30BD2F45368A8AEA6023F827D28BB1D,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_sdp_semantics_get_m548C0D589C8B0690330A7BEE2E48764B38913D17,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_active_reset_srtp_params_set_m103E4B7FB90A9AF6AC822204F0B7BFB88ABC85ED,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_active_reset_srtp_params_get_mC9DDB07BB7C634D4CE131EAD64DA6200FE53F1B9,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_use_media_transport_set_m5DCDF18367F4228DD6DFDDA8C601C41DBA98A418,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_use_media_transport_get_m49E486EF3B9D0C072F620E5BF510FA7A5213033C,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_use_media_transport_for_data_channels_set_m006113467C0900510F42EB0959148D93C0CD38A6,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_use_media_transport_for_data_channels_get_m12CAEF64F457AD93A0AF7F7CBE521E5EC3F61399,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_crypto_options_set_m1060CE36B6D954849454FD38E25E9046DB312EBA,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_crypto_options_get_m3450A0FD578374035685D52B691C9077CD3D4C7A,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_offer_extmap_allow_mixed_set_m7F2AC16BF3EF39D9FBA1CC3C3030AABBF3BAC0DE,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCConfiguration_offer_extmap_allow_mixed_get_mB044FF39D2E9871A3A21415B2DD7DD7B5AB2297E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_kUndefined_get_m40DB439EAF1B0FEE39EAFE9E021232A12F014550,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_kMaxOfferToReceiveMedia_get_m8C220AC327D3EDD5CDC9990AFA680395961EAF1B,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_kOfferToReceiveMediaTrue_get_mA77C3C786E12B0F16D42975AF0EAE82BB8DD5631,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_video_set_m6B69A9DDA6FFBEF556DD66B6585C5EE9C30F3FAE,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_video_get_mB3177CF4214112CFB550256CF149F708D5ACFFB5,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_audio_set_mF548A2821C71C5C9CA0977113071EF0EEB0B11BA,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_offer_to_receive_audio_get_m5414F0B7418C602704F7B9FE2C1C3589926C99B2,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_voice_activity_detection_set_m623F8AD60919354559D459CD6CD6821CEDA45346,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_voice_activity_detection_get_mBE6B785736D69102375219C0542BB55779500E7A,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_ice_restart_set_mC8C99E1E9B9E82D271DA41374878AF3786BA9E88,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_ice_restart_get_m17405E0E5ED19ED4C4F62AD1DD49227746588559,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_use_rtp_mux_set_mE992A997F1A95BFE1DF7060FDEBC7B155B2083F6,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_use_rtp_mux_get_mDDF9D21BC7F881992C4BA7E691D267EB33178AB7,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_num_simulcast_layers_set_mDCFED87DB6E65CF7A8F345999D1C905466E1C005,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RTCOfferAnswerOptions_num_simulcast_layers_get_m0251DE3490D518F54D657A859EFC3039DE8091E4,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCOfferAnswerOptions__SWIG_0_m4F8489E88910E1BF744C5E76AAA3BBD9D0462144,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_RTCOfferAnswerOptions__SWIG_1_m748BF1CB029B3CFE10A524D7D5136148537D7B47,
	WebRtcSwigPINVOKE_delete_PeerConnectionInterface_RTCOfferAnswerOptions_m934F691F7C1247412C3621A0E22F81526A9E23A4,
	WebRtcSwigPINVOKE_PeerConnectionInterface_local_streams_mAF4D33FA333DE848F5911E6B2EA9EE199E739ABF,
	WebRtcSwigPINVOKE_PeerConnectionInterface_remote_streams_m9EBFF7D254B9738008193FDFCE321B1331A667C3,
	WebRtcSwigPINVOKE_PeerConnectionInterface_AddStream_m0F6E56D3693AC35312CC95E97A585BBD4C5C876D,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RemoveStream_mBDBFF15016F8644AE40D49C4300D8BE78D604834,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RemoveTrack_m83D49457C10B965699682A669FFF630DBE228D12,
	WebRtcSwigPINVOKE_PeerConnectionInterface_CreateSender_m42F46462EE570CB924F071F58D5EDFAE5D9DBAA6,
	WebRtcSwigPINVOKE_PeerConnectionInterface_GetSenders_m4867A9A3A3AB18E3CF12C8B4E3C4246EAA96C1DC,
	WebRtcSwigPINVOKE_PeerConnectionInterface_GetReceivers_m143C6C7207B9CB00D9010390B72064DB0851B9B6,
	WebRtcSwigPINVOKE_PeerConnectionInterface_GetTransceivers_m78B92996D32DB0D0184570B7AD6DD8C569F21E91,
	WebRtcSwigPINVOKE_PeerConnectionInterface_GetStats__SWIG_0_m50FFB790815499B634E000FD1F8999DE347728DD,
	WebRtcSwigPINVOKE_PeerConnectionInterface_GetStats__SWIG_1_m116A78C394E2793210D3F77B298FA3A51AEC6533,
	WebRtcSwigPINVOKE_PeerConnectionInterface_GetStats__SWIG_2_mDEBAF436D060E4C83D60E495C2D751FECB6D9BD1,
	WebRtcSwigPINVOKE_PeerConnectionInterface_GetStats__SWIG_3_m940CDC5B8731ECCA7EDE7BA2CB4A5C1EF0C39DDB,
	WebRtcSwigPINVOKE_PeerConnectionInterface_ClearStatsCache_m2D49F03973712296A665E266835016307194EECE,
	WebRtcSwigPINVOKE_PeerConnectionInterface_CreateDataChannel_m97BA3107149FBC95E40A686425EB38D0F6802FED,
	WebRtcSwigPINVOKE_PeerConnectionInterface_local_description_mEBE7037346679FA7BC12A32975DF97C32070AF40,
	WebRtcSwigPINVOKE_PeerConnectionInterface_remote_description_mF7931B7A7CE4477181F1E884928EC1AD6592DFD9,
	WebRtcSwigPINVOKE_PeerConnectionInterface_current_local_description_m150872EDC2BCAAE72A01E058A0AE7096290CA383,
	WebRtcSwigPINVOKE_PeerConnectionInterface_current_remote_description_m1981BCF7FBA340F7488DFFCE1416430AA6995FAB,
	WebRtcSwigPINVOKE_PeerConnectionInterface_pending_local_description_m65111ABF1EC31294D8754877FA8BDD6214B83953,
	WebRtcSwigPINVOKE_PeerConnectionInterface_pending_remote_description_mD7F4BA43DF2B5A60332B1C0C45C69098383099AB,
	WebRtcSwigPINVOKE_PeerConnectionInterface_CreateOffer_m82FCDC9ECD95CA1354D4DFCE735327EB5F214F07,
	WebRtcSwigPINVOKE_PeerConnectionInterface_CreateAnswer_mEEBA97455D56F8AAACEDABD9F7EC3EBFC776A9CC,
	WebRtcSwigPINVOKE_PeerConnectionInterface_SetLocalDescription_mEE0B479FD109B977E15CBAC52B3E4E322DDA36D4,
	WebRtcSwigPINVOKE_PeerConnectionInterface_GetConfiguration_m454AE0E917F2B672CA79EA9BFC8595F88E5EB426,
	WebRtcSwigPINVOKE_PeerConnectionInterface_SetConfiguration__SWIG_0_m2AD2A482CD22D39C1C5C450BEB17E7E607A65278,
	WebRtcSwigPINVOKE_PeerConnectionInterface_SetConfiguration__SWIG_1_m4335F38EF6E55BF0BEE9E68F5CD08418071830DD,
	WebRtcSwigPINVOKE_PeerConnectionInterface_AddIceCandidate_m42A4B5A2BD7298FD4EBD38BD438C19DC7972BB7E,
	WebRtcSwigPINVOKE_PeerConnectionInterface_RemoveIceCandidates_m63FEDF64D9ADFD4C1B6EF89EAD33F676756BE3FC,
	WebRtcSwigPINVOKE_new_PeerConnectionInterface_BitrateParameters_m4C19C9348D76C3551B511514386B514028387B20,
	WebRtcSwigPINVOKE_delete_PeerConnectionInterface_BitrateParameters_m44BCBD69E7ED76013E762F34ECEAF4255F30C769,
	WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_min_bitrate_bps_set_m4EE109C1589A419110AEE9AB9A5B8BF5311B6068,
	WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_min_bitrate_bps_get_m512B3E83B8ADC217C7E3992C6BC662F595FEB28D,
	WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_current_bitrate_bps_set_mB286B448111CAF39EB0114B48F78E969A42D928F,
	WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_current_bitrate_bps_get_mC9A561DFEA5C034966648062958AF36870736C74,
	WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_max_bitrate_bps_set_m17A6B6E692E4FA12ABC50A6A92B37C29E7670957,
	WebRtcSwigPINVOKE_PeerConnectionInterface_BitrateParameters_max_bitrate_bps_get_mBDF481D30C570E432B7B240B60AB1BF310A893F2,
	WebRtcSwigPINVOKE_PeerConnectionInterface_SetAudioPlayout_m9BFDA11EB9DD6B4A03645E02E080C6F8568D8988,
	WebRtcSwigPINVOKE_PeerConnectionInterface_SetAudioRecording_m5B1B3D3AB346E37219F805E6BE3976831C0A6294,
	WebRtcSwigPINVOKE_PeerConnectionInterface_signaling_state_m8EB4AD2784B94817BF5F45AEA14ED95FEE182E4B,
	WebRtcSwigPINVOKE_PeerConnectionInterface_ice_connection_state_m44370C24B18B8AFC61CFB4DCD16D645A9ADEF09C,
	WebRtcSwigPINVOKE_PeerConnectionInterface_peer_connection_state_mF01ECC51FAD737BB9D2D351E110C7F7E4D67E0E2,
	WebRtcSwigPINVOKE_PeerConnectionInterface_ice_gathering_state_mDAC351C737ADE16BE4BF27689C065AF34A07F682,
	WebRtcSwigPINVOKE_PeerConnectionInterface_StopRtcEventLog_m1146054A0924A295F6E7E5C99A0810C7D028654C,
	WebRtcSwigPINVOKE_PeerConnectionInterface_Close_mD7EB0EB6018C4DD99F3AF12E2AF071147494663C,
	WebRtcSwigPINVOKE_WebRtcWrap_kSessionDescriptionTypeName_get_m201B3A26512D71B59DF9524152E9B34329F1F1DF,
	WebRtcSwigPINVOKE_WebRtcWrap_kSessionDescriptionSdpName_get_mD063D22B1D4352F1927EBB1E898929070D97070C,
	WebRtcSwigPINVOKE_WebRtcWrap_kCandidateSdpMidName_get_mD32D4479D08704E3235406EBDC807A13DBC2F090,
	WebRtcSwigPINVOKE_WebRtcWrap_kCandidateSdpMlineIndexName_get_m99E2A3D43103925BCE6AD06432B52B4E4B550039,
	WebRtcSwigPINVOKE_WebRtcWrap_kCandidateSdpName_get_mBDA447387D98A414E7C16EE9292CED0730A0956D,
	WebRtcSwigPINVOKE_delete_WebRtcWrap_mB60F38E6ED373AA4E27E1C05F674266D4C12DF9F,
	WebRtcSwigPINVOKE_WebRtcWrap_TestLog_m92D3D005A66D977CE7838C3D4C253D296313BEFF,
	WebRtcSwigPINVOKE_WebRtcWrap_SetLogCallback_mAE9482F5BA94C172FC7F32C0DFFE74A8BC91FA17,
	WebRtcSwigPINVOKE_WebRtcWrap_ResetLogCallback_m394741C48341BB0F6F932F80C647EF97FF389E47,
	WebRtcSwigPINVOKE_WebRtcWrap_SetDebugLogLevel_mAFE919ED3157A4FC1EA1E03D00767B0C3F1DD986,
	WebRtcSwigPINVOKE_WebRtcWrap_Log_m334DCD0EBCD0B247EE7459BEB296B54B565907C9,
	WebRtcSwigPINVOKE_WebRtcWrap_IsSessionDescriptionJson_m51DB241403DEFDBA2ECB61D57DEE8F7B0BED2FA9,
	WebRtcSwigPINVOKE_WebRtcWrap_IsIceCandidateJson_mCCEB75E5777AA64424C33DDBD569459254CB3EE1,
	WebRtcSwigPINVOKE_WebRtcWrap_ParseJsonSessionDescription_mBF7D9087DDBD9103E28309BBA68B715346A9AE88,
	WebRtcSwigPINVOKE_WebRtcWrap_ToJson__SWIG_0_m7484787A2096F0EA03D2EA41AA1E02B0D01E65F9,
	WebRtcSwigPINVOKE_WebRtcWrap_SessionDescriptionToJson_mEB1EFE2681AD10AFC53B07C0BDE924C1BF4C5433,
	WebRtcSwigPINVOKE_WebRtcWrap_ParseJsonIceCandidate_m077EA9E9D0473882FBF20675607F7FE0541629AE,
	WebRtcSwigPINVOKE_WebRtcWrap_ToJson__SWIG_1_mC64D320FC62B25B6E59C928D770E0ECE753D93E3,
	WebRtcSwigPINVOKE_WebRtcWrap_IceCandidateToJson_m9D3DDD16CB11D711DC8D25B684D43B5AEC600E50,
	WebRtcSwigPINVOKE_WebRtcWrap_Copy__SWIG_0_mECCCF7F8DE66F13AC2858FA46FA65E86794C78DD,
	WebRtcSwigPINVOKE_WebRtcWrap_Copy__SWIG_1_m089DB704EF0ADF660FA3F322D35D2D5C6A17A57D,
	WebRtcSwigPINVOKE_WebRtcWrap_GetWebRtcVersion_mA2DE4862174B94B63D049E6F8E57CFB20B7C7308,
	WebRtcSwigPINVOKE_WebRtcWrap_GetVersion_mF8C666FF1FC895C5B4C4B5636D72B89DC9CF1B9D,
	WebRtcSwigPINVOKE_WebRtcWrap_IsDebugBuild_m061BE1C34499B1575F31D9851DC8028582ED4E95,
	WebRtcSwigPINVOKE_WebRtcWrap_ConvertRotationMode_m1B4F85A9424C3E74AAB1389F062BA1B6C27017AF,
	WebRtcSwigPINVOKE_WebRtcWrap_ScaleToI420_m97BC698CC964BF13F1FAE0E378702A62FAA20574,
	WebRtcSwigPINVOKE_WrapTestConnection_PollingDataChannelTest_mD961212786515AA6A3A8BB8B8EDA9EA246A1D9B3,
	WebRtcSwigPINVOKE_WrapTestConnection_PollingVideoTest_mE9BC2F84B682A4036C47C9F810664F54F8FD2A09,
	WebRtcSwigPINVOKE_TestsInternal_PollingDataChannelTest_mC7EC43C1ADBACA7E538D22017AA5936D9AF0FA9A,
	WebRtcSwigPINVOKE_TestsInternal_PollingVideoTest_mE3B92D12AA2AC4F1751FE8D7E18C9547D2B29665,
	WebRtcSwigPINVOKE_new_TestsInternal_m2DCC4A8036042AAA27F496F3A6594BEFB5A443AA,
	WebRtcSwigPINVOKE_delete_TestsInternal_m84C7BE08076E542658FD1774AB14F9E90ED2889E,
	WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_set_mBD611544F86C6512DC83407E28905F7E371070C1,
	WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_get_m9FC30C392C638000E7A9AFD31D6DAE8AA4B804C4,
	WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_set_mA76F9E945157319D9DBD149AB9719EF6143C46BF,
	WebRtcSwigPINVOKE_AudioOptions_echo_cancellation_get_m63A7C44CFFCC95D88603AF034BE62E55EE44ECE2,
	WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_set_mA7AECE24A483A13297D39E8765E0D858434E2AB1,
	WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_get_m568917A1832EEEBA497143B72790042E82602CAD,
	WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_set_m7AE74292669BCB9BF57CE31C400245EA57823A27,
	WebRtcSwigPINVOKE_AudioOptions_extended_filter_aec_get_mC77D63417BA4496760236C76E578596FF6F381F7,
	WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_set_m3BD82AC0D7344BB78BB3796EEC0A2D29149A09A3,
	WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_get_m68871DD5A88218F4D0B95A295BCFABEA3C5023CC,
	WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_set_mA6356486CDDA3D8D41F37999B539C478B4B383BA,
	WebRtcSwigPINVOKE_AudioOptions_delay_agnostic_aec_get_mAD2C67892B274859E919208A19E436B7DE9BE3EB,
	WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_set_mEEFB43AA89E59531EF2F6FB682FB4333BFF99DB6,
	WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_get_mF98CC57489A47DCABBB4FF54377382E261A2FA23,
	WebRtcSwigPINVOKE_AudioOptions_noise_suppression_set_mA8698B87AC67CBBEA45894D41FB970ECB5F77ABB,
	WebRtcSwigPINVOKE_AudioOptions_noise_suppression_get_mC269DBEDDB7BC268AE36D6637657FBAF168F6160,
	WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_set_mD883F072BBCBDFE5689985253139ECDC42D6B8E8,
	WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_get_mF17636B8E2B62B501667D8194A7B40838F5684F6,
	WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_set_m46BF758C04AD6ABD17E1F884E5E5DB4150CCF0A3,
	WebRtcSwigPINVOKE_AudioOptions_auto_gain_control_get_mE2ED601724A0A441DAC95731B1503CBF4ABB75BA,
	WebRtcSwigPINVOKE_new_AudioOptions_mADE4EDE7AFC5013026C1F34DAE85948C2D8DCAD0,
	WebRtcSwigPINVOKE_delete_AudioOptions_m3832E64976C34FA1BFBE3A65475BC2B8FE4C000D,
	WebRtcSwigPINVOKE_MediaConstraints_audio_set_m9187172D50B79D59A3CA90249E1D26F6A19FADC0,
	WebRtcSwigPINVOKE_MediaConstraints_audio_get_m89C1EAD98F5B746D594DCD78D1598A3EF7B36DB6,
	WebRtcSwigPINVOKE_MediaConstraints_video_set_mBB2164A702694B3F45709F66A168A8F17A01BC10,
	WebRtcSwigPINVOKE_MediaConstraints_video_get_mB0EED2AB511AD2F1B2B095EB3608FDC80E5D56B2,
	WebRtcSwigPINVOKE_MediaConstraints_videoDeviceName_set_m5D68646344F8B70CA6646ADD3A837D0559B5E460,
	WebRtcSwigPINVOKE_MediaConstraints_videoDeviceName_get_mE7104F757622DF2E33DD1F32AE8076BF03103668,
	WebRtcSwigPINVOKE_MediaConstraints_idealWidth_set_m1494ADE404C0D4B2B62A0A2A4FF43CFE1A85EA6B,
	WebRtcSwigPINVOKE_MediaConstraints_idealWidth_get_m2095BC7DA10D5E7151E70AA13367EF7703699180,
	WebRtcSwigPINVOKE_MediaConstraints_idealHeight_set_mEA44AF19C66F3F376C10259D28B93561C25FCD40,
	WebRtcSwigPINVOKE_MediaConstraints_idealHeight_get_m31134681AD2F2279BBE37E9A286D80ADB388D5E5,
	WebRtcSwigPINVOKE_MediaConstraints_minWidth_set_m92342117B1398E0A20C2E4C3722DEA385AAB598F,
	WebRtcSwigPINVOKE_MediaConstraints_minWidth_get_m511D4B93E228CFE07354921223A9D73E02276DF3,
	WebRtcSwigPINVOKE_MediaConstraints_minHeight_set_m49207B860CA4D3A58E2D56F79D4A2583DFD3A302,
	WebRtcSwigPINVOKE_MediaConstraints_minHeight_get_m73FEEF11188566FB015AF3195E45441806FEF728,
	WebRtcSwigPINVOKE_MediaConstraints_maxWidth_set_m793FFE65C4AA7DD029E4D6041B303B0EDCE4E40E,
	WebRtcSwigPINVOKE_MediaConstraints_maxWidth_get_m14750B550A72CF72964BBF7A73306E60B8E93309,
	WebRtcSwigPINVOKE_MediaConstraints_maxHeight_set_mCDA31FD4DB6AD24BFFFB6E6A5BEA460260AD988A,
	WebRtcSwigPINVOKE_MediaConstraints_maxHeight_get_m745C43F9B1DE1B2F6DB561FD7E99E546A43B9333,
	WebRtcSwigPINVOKE_MediaConstraints_idealFrameRate_set_m4198BEDC30EA24E42A114AF19CBB0A201698A484,
	WebRtcSwigPINVOKE_MediaConstraints_idealFrameRate_get_m355FE5E4CC54A16DA81BF27724706A6856E566C4,
	WebRtcSwigPINVOKE_MediaConstraints_maxFrameRate_set_m6819F1356ED55B519A49359DC142F1A7332741D3,
	WebRtcSwigPINVOKE_MediaConstraints_maxFrameRate_get_m1280DB735A5FEEE6E116A0128036A244CAC7CE05,
	WebRtcSwigPINVOKE_MediaConstraints_minFrameRate_set_m4EC329C20C64A74EDEBA71ED802391E245C26871,
	WebRtcSwigPINVOKE_MediaConstraints_minFrameRate_get_mF9BB11312F1474BB371D0AA021E27F4C7ECBFE9A,
	WebRtcSwigPINVOKE_new_MediaConstraints_m95825C791CB663588522F7A856DADED827651F63,
	WebRtcSwigPINVOKE_delete_MediaConstraints_mE97AADA67D9D9B8AA2EF17B23C6963F380C95468,
	WebRtcSwigPINVOKE_new_PollingMediaStreamRef__SWIG_0_mE0C96B5871FA860839507D6B93C4E44938B9939C,
	WebRtcSwigPINVOKE_new_PollingMediaStreamRef__SWIG_1_mE97F135D751C31D98962925E769783045E1B0A15,
	WebRtcSwigPINVOKE_new_PollingMediaStreamRef__SWIG_2_m8A15C39998FAFF04C7F91E09C523CA9CC7EA8B78,
	WebRtcSwigPINVOKE_delete_PollingMediaStreamRef_m115E949245BA07CF0CC883A35598FF79FC167C57,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_get_m691942A19EEC0C8E06271136F5436F9D95ADA86B,
	WebRtcSwigPINVOKE_PollingMediaStreamRef___deref___m8470FED5DA46FCDE31F2A3C9B1611441788FE1C9,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_release_m43D749BC54ECE26151E92CEE667B8AD755BEA3CE,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_swap__SWIG_0_m6FE150D3AE46C12BB6BE6A484CDC4300AE14B987,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_swap__SWIG_1_mEC4A10CF2E157CA08E2C7F19CC982E4C1BC9634E,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetInternal_m648550DDACB363E41029337C16DA7341478412AC,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_HasVideoTrack_mA12D24E17A432D10C45FF9A7C33E925432EC7337,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_HasAudioTrack_m721479ED4031278BB567296D7D82F0E2D0C784A0,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_IsMute_m9B78435995A4EAB37D433DB7B9E28811B9E75742,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_SetMute_m914B07165D6652A10F2B64D57EF5FD5BBAE6002C,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_CalculateByteSize_m9CCDC6576060B778AC6C2D0DA030B2753C1B8E14,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetImageData_m8F387D1BC0F827E82707874203274C18C93DF1FE,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_SupportsI420Access_m07C823126F4CD086315DE9057B19D7665B2631B9,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetI420Size_mEEDE4A7422BA6A7714220496DCA91027BDA7179F,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetI420Ptr_m46912085021AE6210119ABCBA0811F3A094D801D,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_HasFrame_m3E02D09003F06946502141586BDF272ED26D3D0F,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_FreeCurrentImage_mF54C24E557BAEE8DD748990D0E6DE0A4E64CCAE1,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetWidth_m9CBF2D2F3D68986FAF205A124DA9DD77BC8364C7,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetHeight_mBA6537A178868C2705391FA0FF94D112D92C1772,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_GetRotation_m8CDA2749E39F94B373EF15F975A40D9DCD6850E1,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_SetVolume_mC2F68934218B7F8FA9B0F6E6CF22A24A9F0F2A51,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_AddRef_m25C43095FA9323AC64C97A28E639C0AFE032E41D,
	WebRtcSwigPINVOKE_PollingMediaStreamRef_Release_m8AF8C119C6A409B5114685F296F372F52054EC0D,
	WebRtcSwigPINVOKE_delete_PollingMediaStream_mA9B1C66D87DAED355A57CB94A8B6FDD882B0A96B,
	WebRtcSwigPINVOKE_PollingMediaStream_GetInternal_m8344F848D429DDB095430EDB74DAB18269F70BF5,
	WebRtcSwigPINVOKE_PollingMediaStream_HasVideoTrack_m0F659F888E6D59875A0A472921AF35EF8CA1EBD7,
	WebRtcSwigPINVOKE_PollingMediaStream_HasAudioTrack_m9F014E6BB78D74E8886ECD36E0C8D53760236B08,
	WebRtcSwigPINVOKE_PollingMediaStream_IsMute_m98093BD0C753DDB976D6CBEBA5B449D7A109D86B,
	WebRtcSwigPINVOKE_PollingMediaStream_SetMute_mEDAC97634F49B648FEF4CD7F01D1C10270A7FC89,
	WebRtcSwigPINVOKE_PollingMediaStream_CalculateByteSize_m47FE6F2D845F337FE8010AA1A1987077DC76AFD7,
	WebRtcSwigPINVOKE_PollingMediaStream_GetImageData_m57C45BADFE159D54F837FBB9AEDD72BD73CF7664,
	WebRtcSwigPINVOKE_PollingMediaStream_SupportsI420Access_m6EDF5B9DF8566AE94E15CB0E9FA9C130F7DA649C,
	WebRtcSwigPINVOKE_PollingMediaStream_GetI420Size_m9F7185775A7573EFB0A26C3E4F33C5AD6C8F0622,
	WebRtcSwigPINVOKE_PollingMediaStream_GetI420Ptr_m626160BED7C51A47CE55D9ED544B5DD9F22D444D,
	WebRtcSwigPINVOKE_PollingMediaStream_HasFrame_mAC1FFA205C8F072D71860FBEE8E5CD0373AB2353,
	WebRtcSwigPINVOKE_PollingMediaStream_FreeCurrentImage_mEA300ED1128FF523EFD85149EE5ABF970CBB29E0,
	WebRtcSwigPINVOKE_PollingMediaStream_GetWidth_m1049174DEE8E4BF9348D6EBB26CB2DDFD4291BA5,
	WebRtcSwigPINVOKE_PollingMediaStream_GetHeight_m0FF503685F3E43B91E9538CBADC6FD44E2FB0366,
	WebRtcSwigPINVOKE_PollingMediaStream_GetRotation_m41CD57807D48954263D2CAA6C7F029C2C8FCE900,
	WebRtcSwigPINVOKE_PollingMediaStream_SetVolume_mD989A23ACD68EE76EEA655E5E690885D07925253,
	WebRtcSwigPINVOKE_new_PollingPeerRef__SWIG_0_m6F53D5CDDC4F1BA358860C4C82422CAA3845852A,
	WebRtcSwigPINVOKE_new_PollingPeerRef__SWIG_1_m18F5FB780BEEAFB3C4BCD5FBE43FAA3424ABF044,
	WebRtcSwigPINVOKE_new_PollingPeerRef__SWIG_2_m37929EF908CA07A9E84D568D8E0BA22B526EBC4B,
	WebRtcSwigPINVOKE_delete_PollingPeerRef_m98D97B839B560A7475A5E3DC8F1D947115BD1768,
	WebRtcSwigPINVOKE_PollingPeerRef_get_m685E2BFEC0F8A6C18130AA460AAD0F5FA2D66481,
	WebRtcSwigPINVOKE_PollingPeerRef___deref___m2741662EAA3D3CE61BCA8FCE0BA734BB3EB113DE,
	WebRtcSwigPINVOKE_PollingPeerRef_release_m6C2EB9C19CEB2B2937CA2ADBB754CD9781C7F834,
	WebRtcSwigPINVOKE_PollingPeerRef_swap__SWIG_0_mB5EB17B644004DB37B6829CA7594EA3466751140,
	WebRtcSwigPINVOKE_PollingPeerRef_swap__SWIG_1_mBD5105778F99CE2C403C108634AE2A9EDFAAFC74,
	WebRtcSwigPINVOKE_PollingPeerRef_Update_m59EF36160DDBB1D2BC2B6A203E8A936A13677512,
	WebRtcSwigPINVOKE_PollingPeerRef_Flush_m7C5A2EB75E58AF808647A6B5D4A7B61F973D12FF,
	WebRtcSwigPINVOKE_PollingPeerRef_GetConnectionState_m876EBA1AD2605AE14A407705BE031BE03EC68608,
	WebRtcSwigPINVOKE_PollingPeerRef_HasSignalingMessage_m6ECD492BC77FB5015208D1714C348D8BCCDAA006,
	WebRtcSwigPINVOKE_PollingPeerRef_DequeueSignalingMessage_m895DB98A1AD839B372EF1538A94F9368689BDE83,
	WebRtcSwigPINVOKE_PollingPeerRef_AddSignalingMessage_mE13716F7C8A834223F213C1A2626C7B6C9641D00,
	WebRtcSwigPINVOKE_PollingPeerRef_HasSignalingError_m44A57B3128856A5F0C9773EABA0D4CC145DC17F6,
	WebRtcSwigPINVOKE_PollingPeerRef_DequeueSignalingError_m0529991DA5FDB43DAB3B8EEC91A15841F1F07945,
	WebRtcSwigPINVOKE_PollingPeerRef_CreateOffer_mBA4CC86D5734AB87B4CBD0CF4F1154AA2D71BA59,
	WebRtcSwigPINVOKE_PollingPeerRef_CreateAnswer_m101E7CAD39B105A5EA6DF57A5AAB80F5864BD87E,
	WebRtcSwigPINVOKE_PollingPeerRef_Close_mEFEB84250DE0C53E3BD880FB02DF16FE081FF837,
	WebRtcSwigPINVOKE_PollingPeerRef_CloseInternal_mBFF8E7C79273346006A117CA13A9100B3130DC56,
	WebRtcSwigPINVOKE_PollingPeerRef_Cleanup_m8838D08F9D0E254D642BB880BDF7CFC0E3AF3F61,
	WebRtcSwigPINVOKE_PollingPeerRef_CountOpenedDataChannels_mC8FA0B0678A5CC388D86CC0B344642DF7FADC09D,
	WebRtcSwigPINVOKE_PollingPeerRef_HasDataChannelMessage_m9AC6FAAEB194C26BBC0FB2511FAA259A1D459415,
	WebRtcSwigPINVOKE_PollingPeerRef_DequeueDataChannelMessage_m53F910D042CC2918E6445594863C317B6E37E61C,
	WebRtcSwigPINVOKE_PollingPeerRef_Send_mD30C51A7199D2119B7E3FD4648FD48B950D2F5F0,
	WebRtcSwigPINVOKE_PollingPeerRef_GetBufferedAmount_m3E232A992BFED30A62E7979828397C1CD33118ED,
	WebRtcSwigPINVOKE_PollingPeerRef_CreateDataChannel_m9F3DB69BB035AFC04442126CDFABCFEAB7A09349,
	WebRtcSwigPINVOKE_PollingPeerRef_GetDataChannelLabel_m0659E39F4B1E7DDCFFB68229A3CBEEA85EC64551,
	WebRtcSwigPINVOKE_PollingPeerRef_GetRemoteStream_mF91C29FDCECE63989B6724E6ECBCED21EB3CF595,
	WebRtcSwigPINVOKE_PollingPeerRef_AddLocalStream_m4C50843594456D830F8819CA7F6E7CBA245B4C18,
	WebRtcSwigPINVOKE_PollingPeerRef_RemoveLocalStream_m99C3C4A0D61A4097D6E5FBAFFD822957395D82C9,
	WebRtcSwigPINVOKE_PollingPeerRef_RequestStats_m257BBBF3CDCA8C249876B8A42FDD4438F9607C25,
	WebRtcSwigPINVOKE_PollingPeerRef_HasStats_m11E10E9974ECDB4C124577ABFC1D525130A94757,
	WebRtcSwigPINVOKE_PollingPeerRef_DequeueStats_mBBCC62454DC0C72054A0D65DE784B6534A568D16,
	WebRtcSwigPINVOKE_PollingPeerRef_AddRef_m0B5175B8B32242A0CCE11C199EAAE322C5A4496B,
	WebRtcSwigPINVOKE_PollingPeerRef_Release_m28F777AC9C9BE1CF8EC8A42E760B4FD48B1D5876,
	WebRtcSwigPINVOKE_delete_PollingPeer_mB7B755314DC6F634903675CDC5FA6568F44FDBB5,
	WebRtcSwigPINVOKE_PollingPeer_Update_m1ED1CBCD6A6EF95AD980ED3BF6B24C1F8AD51629,
	WebRtcSwigPINVOKE_PollingPeer_Flush_mB3BF795459C1C709FD5FEDFD8B428047B97B998E,
	WebRtcSwigPINVOKE_PollingPeer_GetConnectionState_m6A60615D8F69CCCC3C69E0034D9DEDBF3B2F3D03,
	WebRtcSwigPINVOKE_PollingPeer_HasSignalingMessage_m40D94A840541BCC92B5ADB93B0C4BAD98BAE2EB1,
	WebRtcSwigPINVOKE_PollingPeer_DequeueSignalingMessage_mC89983721FD1E031DB60E0CBDFEA8C520590E635,
	WebRtcSwigPINVOKE_PollingPeer_AddSignalingMessage_m1297D28878F79FDDE6CBF0D1F2BFE4CB365FC374,
	WebRtcSwigPINVOKE_PollingPeer_HasSignalingError_m22022635AF2CF5167C2DFB4C2B9C0C0B8C854D0B,
	WebRtcSwigPINVOKE_PollingPeer_DequeueSignalingError_m88A8F54A9350C2D48BC212D4312AE6E501CCCEC6,
	WebRtcSwigPINVOKE_PollingPeer_CreateOffer_m725957990B9E15E235A72AB80E2CBDB45924EECA,
	WebRtcSwigPINVOKE_PollingPeer_CreateAnswer_mB0944B067E296ACC89D8EE15CCEBF6D2D1F12DCE,
	WebRtcSwigPINVOKE_PollingPeer_Close_m7254EF9C9522D17311D59270C3F7A073768A8F84,
	WebRtcSwigPINVOKE_PollingPeer_CloseInternal_mBA31AE7A0FAEB08FE4E0BE75F1B91C15DE3B0532,
	WebRtcSwigPINVOKE_PollingPeer_Cleanup_mF66DF8359DC05DC94942D4EE74CEF37C1120FE75,
	WebRtcSwigPINVOKE_PollingPeer_CountOpenedDataChannels_mD58402C69AB244071EE54BD295A5EC3AF89E2220,
	WebRtcSwigPINVOKE_PollingPeer_HasDataChannelMessage_m94E687707749346A070D837AC7D1ABC252559476,
	WebRtcSwigPINVOKE_PollingPeer_DequeueDataChannelMessage_m46BD360D6074C7146B4BDEBC9078DCC2532232C8,
	WebRtcSwigPINVOKE_PollingPeer_Send_m0CFC7AB5FAB35A2A699B4429EA7194229E4A9DEE,
	WebRtcSwigPINVOKE_PollingPeer_GetBufferedAmount_m591B44D694A3BBC447094C4EC8FF12C3D4703758,
	WebRtcSwigPINVOKE_PollingPeer_CreateDataChannel_m71203AC90CEAE955FD7BD4F2EEF97ED424AC8217,
	WebRtcSwigPINVOKE_PollingPeer_GetDataChannelLabel_m866A8D9B4CA635E49E1278A614DC731F5F3F7DF8,
	WebRtcSwigPINVOKE_PollingPeer_GetRemoteStream_m91CFF66D0872429D8DBCF3497CD043D7345CB67E,
	WebRtcSwigPINVOKE_PollingPeer_AddLocalStream_mC0708DF6F8F83621A35ED964FC1D34E77D215971,
	WebRtcSwigPINVOKE_PollingPeer_RemoveLocalStream_m38A9B3042983F254B9AC75C5BF80B51F9DDAAE8E,
	WebRtcSwigPINVOKE_PollingPeer_RequestStats_m9B29D69B6FBBDE6126BF897B40154ADF5382E2AE,
	WebRtcSwigPINVOKE_PollingPeer_HasStats_mAEC9FF75831643DE6704014304B6038BBD274E9C,
	WebRtcSwigPINVOKE_PollingPeer_DequeueStats_m4BC9DFD6409FF4FD1C201B9945A138BF72655730,
	WebRtcSwigPINVOKE_new_AsyncPeerEventArgs_mB1AFA5B02EBD1737E0ECE7173533DA6285E6C184,
	WebRtcSwigPINVOKE_delete_AsyncPeerEventArgs_m4015CE821AE7F91931E5907319982C3535983E5B,
	WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetEventType_m722A0B3BCC09309A97FDA7B4F7E418C1A6765703,
	WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetContentType_m7444AB62B144BEC66951AE4FAB1EABC91652D563,
	WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetStream_mEB436434C32FDC0B1F9D69F2A870B096AFD69410,
	WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetDataChannel_m32A2EA8724D94D196FF9FBF6DF5EF6DEFE8EE6BD,
	WebRtcSwigPINVOKE_AsyncPeerEventArgs_GetString_m23C1D7483EFC47D389064ABEBCDBEE0488066B81,
	WebRtcSwigPINVOKE_new_AsyncPeerEventStringArgs_m7DAE3AE88BD2ADC61B375F8A7D44BEBA74D18401,
	WebRtcSwigPINVOKE_delete_AsyncPeerEventStringArgs_mD71807333ECE012C0E20CC7ADB09BD6549426DF3,
	WebRtcSwigPINVOKE_AsyncPeerEventStringArgs_GetString_m4F373230BC867E2A1BEBE6C6B8E503677D33A8DE,
	WebRtcSwigPINVOKE_AsyncPeerEventStringArgs_Cast_m1BC01D907DDF6CA01838EBEF7C59D7E46DA8CB44,
	WebRtcSwigPINVOKE_new_AsyncPeerEventMediaStreamArgs_m52C4ED17658159318CC80A4A7A089AE6993D511F,
	WebRtcSwigPINVOKE_delete_AsyncPeerEventMediaStreamArgs_mB5250C19BF69F0ED03A2D97DFBF411AEC49629AF,
	WebRtcSwigPINVOKE_AsyncPeerEventMediaStreamArgs_GetStream_m1265D57FA5AFF137B7148515C81F16831AA9ADE7,
	WebRtcSwigPINVOKE_new_AsyncPeerEventDataChannelArgs_m4A37046D2C318B0FA011F0EDE11CDB510EE02700,
	WebRtcSwigPINVOKE_delete_AsyncPeerEventDataChannelArgs_mAA594600B736C50FFE015A2B461B2132D1F94A7F,
	WebRtcSwigPINVOKE_AsyncPeerEventDataChannelArgs_GetDataChannel_m2FEC3F3B2525A00738E937B86DE39D9616EFFC00,
	WebRtcSwigPINVOKE_new_AsyncPeerRef__SWIG_0_mF45B5AF38D9197C228B9654464E9B75FD9A1BE01,
	WebRtcSwigPINVOKE_new_AsyncPeerRef__SWIG_1_m51AF2B29B9F49DE414CEF134A42FEABD1DDC6F37,
	WebRtcSwigPINVOKE_new_AsyncPeerRef__SWIG_2_mC7CE56D78546AB0962FE023BB1DEB96EBEA9628D,
	WebRtcSwigPINVOKE_delete_AsyncPeerRef_m02A37ADBA76E41D36C1489BFD43D56DA4141C113,
	WebRtcSwigPINVOKE_AsyncPeerRef_get_mA6358975C4CBBA1BF42F54956237A0A98BBA9F06,
	WebRtcSwigPINVOKE_AsyncPeerRef___deref___mE62031F4C353CD4ECFAAA736DE24D4293615CBD2,
	WebRtcSwigPINVOKE_AsyncPeerRef_release_m84E33CDFBB51724F4BD931AD228EC98650DD94BF,
	WebRtcSwigPINVOKE_AsyncPeerRef_swap__SWIG_0_mB9B434109DE6CC5FECCB56905D16124EEC849868,
	WebRtcSwigPINVOKE_AsyncPeerRef_swap__SWIG_1_mF1D4349483F155421BDF8EF63531CBA9C43D108C,
	WebRtcSwigPINVOKE_AsyncPeerRef_Update_m8ADFFCB6B3933673855D41D4A752E75F7DEA837D,
	WebRtcSwigPINVOKE_AsyncPeerRef_Flush_mB4DEA57E0F197D99D8CBFB82DEBE8E2289CD9AEC,
	WebRtcSwigPINVOKE_AsyncPeerRef_GetConnectionState_m7439DE1D061769EEDDE6F64B04CD34A69EEBA336,
	WebRtcSwigPINVOKE_AsyncPeerRef_CreateOffer_mCF081BCF636DEC353F8F4F118489FF748BA10820,
	WebRtcSwigPINVOKE_AsyncPeerRef_CreateAnswer_mD488B793FEAFFC980F484E2F253FF7CDA4DFFA91,
	WebRtcSwigPINVOKE_AsyncPeerRef_SetLocalDescription_mE7FB2FA17CEB3120E0E5EDCB3720B610ADFC97D2,
	WebRtcSwigPINVOKE_AsyncPeerRef_SetRemoteDescription_m2122D262723C678ED1ABFE097F4661ECAC4BA5B6,
	WebRtcSwigPINVOKE_AsyncPeerRef_GetLocalDescription_m1B62EE7A4E0CB8783FC6D32A567F08A17B76B68C,
	WebRtcSwigPINVOKE_AsyncPeerRef_AddIceCandidate_mD2B364C37E867B4893AEE6733166AF033C66A0CE,
	WebRtcSwigPINVOKE_AsyncPeerRef_DequeueEvent_m868403A29887DB7D8A784EDA952D9071416E8BEB,
	WebRtcSwigPINVOKE_AsyncPeerRef_Close_m85C71DEB5E9B36178C57DC32BDF19B884D407B99,
	WebRtcSwigPINVOKE_AsyncPeerRef_Cleanup_m4E5CBCE89F1C0AA3AFB0880283ED2B42AE465CBC,
	WebRtcSwigPINVOKE_AsyncPeerRef_CreateDataChannel_m81012A6616914BE8D8FE1F43CC65B3036F70CB04,
	WebRtcSwigPINVOKE_AsyncPeerRef_AddLocalStream__SWIG_0_mC6D512BA79E44208FEC446448EE589637B724390,
	WebRtcSwigPINVOKE_AsyncPeerRef_AddLocalStream__SWIG_1_m4D4EC438BA9B582C55F194C2E23453D1DF9FB29A,
	WebRtcSwigPINVOKE_AsyncPeerRef_RemoveLocalStream_m8CB848E4C381A638A745D532BA75FA2C43FCD256,
	WebRtcSwigPINVOKE_AsyncPeerRef_AddRef_m3448305DAEDFA8E2552AB3E1B2FB27C48287C3E7,
	WebRtcSwigPINVOKE_AsyncPeerRef_Release_mB6DA68B45FF96C4615963191E47BCA6BF069E323,
	WebRtcSwigPINVOKE_new_AsyncDataChannelRef__SWIG_0_m8DB5B58C69103C4F568CBA14B8735C0B0C3A6F63,
	WebRtcSwigPINVOKE_new_AsyncDataChannelRef__SWIG_1_m8BFB26E5F7D00B0D71FFA117877D6915C4277519,
	WebRtcSwigPINVOKE_new_AsyncDataChannelRef__SWIG_2_mF3040D34CEF91FDBF55DC82DBFD19D34EAFC47E0,
	WebRtcSwigPINVOKE_delete_AsyncDataChannelRef_m16A16268912A888D2AA12D531C833D6E6D234FC5,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_get_mE2F9FA96F5F3A2336E25953AE66C46309EDBE8CC,
	WebRtcSwigPINVOKE_AsyncDataChannelRef___deref___m007595E26B1DB77042D4B7099D3B996556066D22,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_release_mA381E9A872AE230AFC4DCBCC4EA1234A4DDA4803,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_swap__SWIG_0_mC01783F34555724AFA29A227D411BAEAAF10D127,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_swap__SWIG_1_m2E9D984944EC5D437B5337A18169E8010C6F3309,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_Init_mC250621637DF9436D82999B3174723875EC214E4,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_GetId_m40C3860CB12BD88F4CF1EB1BD1976B1D4B220F31,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_GetLabel_m1F55DFDC27396ABA15907B9B32CA34255DB43DE0,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_IsInitialized_mC3DE2981EB320F657237D3828BBDEC3F8296537C,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_GetState_mCF90EF3608CC62F6C3DE312BB51EAF429984785B,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_HasMessage_m59B9325078AF62121AA8D2F54BCA8E45761F51D9,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_DequeueMessage__SWIG_0_mC0967F33976690B6684A1CA3107FC899D96E49F9,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_DequeueMessage__SWIG_1_m6C44EB17E6D63B6A23688CAD18CEC97728E05830,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_Send_m408F2417FCCDA68278AD4B7C38763D0985854DBC,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_AddRef_m614730759DCE2A9386F5E6EB963EB9CBC1EEB05F,
	WebRtcSwigPINVOKE_AsyncDataChannelRef_Release_mF6B04880C3CBFEDB2D8BB7ABB90364B46AE9E8AC,
	WebRtcSwigPINVOKE_new_Message_m208B419EDEC072D288092DBDA61E1BAE38AC15CE,
	WebRtcSwigPINVOKE_Message_GetSize_mC7146CCE72F02AB297B46469EB9BF6C73FA3E7BA,
	WebRtcSwigPINVOKE_Message_GetData_mC3F03EBACA68D3B4FCF0DDC4A551E88B8905F6FF,
	WebRtcSwigPINVOKE_delete_Message_m90429E5BC0579FDD2C80BB0E5C0F255B5FA01E50,
	WebRtcSwigPINVOKE_delete_AsyncDataChannel_mD8D3FB2FD16A05EF6FE53CC1FD1CA47B6F3751B7,
	WebRtcSwigPINVOKE_AsyncDataChannel_Init_mE5FF315DCB962A61FAC911713C02D724924CB95E,
	WebRtcSwigPINVOKE_AsyncDataChannel_GetId_m597C6C0ACEB512FAFC4816BEEFF7B42FF9B56729,
	WebRtcSwigPINVOKE_AsyncDataChannel_GetLabel_m1D1F06FF8485731EE570F0F56F14E29537421E53,
	WebRtcSwigPINVOKE_AsyncDataChannel_IsInitialized_mB4A9ED344576EEDE72476710094D6B42DD70C119,
	WebRtcSwigPINVOKE_AsyncDataChannel_GetState_mE7C442330F0755DB7F4F3C86E44E852B44A65E63,
	WebRtcSwigPINVOKE_AsyncDataChannel_HasMessage_m88CDF929F3AA4EB16FB1C7F000EF5A68A6563BB7,
	WebRtcSwigPINVOKE_AsyncDataChannel_DequeueMessage__SWIG_0_m1393BDBDD9F36F39537971A3A49FF5A140D75051,
	WebRtcSwigPINVOKE_AsyncDataChannel_DequeueMessage__SWIG_1_mD253E3CD810D1EC0728B03BFD2E8CBFF08ADC101,
	WebRtcSwigPINVOKE_AsyncDataChannel_Send_mCCA1CF6E8F232692B235636949BD9861001F43B2,
	WebRtcSwigPINVOKE_delete_AsyncPeer_m8B168C9988394FD2B83B94D2F1793DD493B37563,
	WebRtcSwigPINVOKE_AsyncPeer_Update_mEA0CD797C98E7879221E5BA8275C3E5987DBB657,
	WebRtcSwigPINVOKE_AsyncPeer_Flush_m291FA454C64D2AE056BC3D5C56965C70BEA71E83,
	WebRtcSwigPINVOKE_AsyncPeer_GetConnectionState_m335E9D3AEF1F6DEEE355174717ACADB7060D527A,
	WebRtcSwigPINVOKE_AsyncPeer_CreateOffer_m29C2292F5753FC27A0F1E115BF8DD231516AAC38,
	WebRtcSwigPINVOKE_AsyncPeer_CreateAnswer_mC67BE60681960E9B95A8E490851C068266FAD87A,
	WebRtcSwigPINVOKE_AsyncPeer_SetLocalDescription_m473E996D8E62134CE584E42ED26510C86D1FA073,
	WebRtcSwigPINVOKE_AsyncPeer_SetRemoteDescription_m76AB548C87BBCE5E3A843826DFFD59E18090585A,
	WebRtcSwigPINVOKE_AsyncPeer_GetLocalDescription_m9CDF4CF14553497F0C667C6FB179A3401F5C640D,
	WebRtcSwigPINVOKE_AsyncPeer_AddIceCandidate_mF44BF154B90CB0411DB881E38D7CC3A2E4794BA2,
	WebRtcSwigPINVOKE_AsyncPeer_DequeueEvent_m57A64BFAAF0EE61BB01B660FC819F65AE973BFF8,
	WebRtcSwigPINVOKE_AsyncPeer_Close_m517E1DAF57F7A7F72E4C7FFF376445F877B87446,
	WebRtcSwigPINVOKE_AsyncPeer_Cleanup_m5749DF83670A064600233AA2E8A5997FA21678F6,
	WebRtcSwigPINVOKE_AsyncPeer_CreateDataChannel_m13058BACA1C354F3AC9F85D040CE9CC68E2989D2,
	WebRtcSwigPINVOKE_AsyncPeer_AddLocalStream__SWIG_0_m348393F724A03B2A495BCDEF7A20208C7B0B3E94,
	WebRtcSwigPINVOKE_AsyncPeer_AddLocalStream__SWIG_1_mB1D85048A347B1C6A1131530E2C5C48A9DB31762,
	WebRtcSwigPINVOKE_AsyncPeer_RemoveLocalStream_m8325CF98EDA942BF20B8B7801897C7CC2A074F4F,
	WebRtcSwigPINVOKE_new_VideoInputRef__SWIG_0_m3EB9CE836AE1D35CD1CFB298E529EA3AFAE575DB,
	WebRtcSwigPINVOKE_new_VideoInputRef__SWIG_1_m86AEEFBCC468858C5E37C672DB38AAF1AC618317,
	WebRtcSwigPINVOKE_new_VideoInputRef__SWIG_2_mCD8FB397CE3C8EF698E7C219AA5EF43ACB312354,
	WebRtcSwigPINVOKE_delete_VideoInputRef_mF9A0B5BBB80BB220AEE4E5BC563553982DE773FD,
	WebRtcSwigPINVOKE_VideoInputRef_get_m870554B6B22624CFCB17E525D5D36DE79515B667,
	WebRtcSwigPINVOKE_VideoInputRef___deref___m010D06F2B2DD0BA238611687E35311D4E9F498A4,
	WebRtcSwigPINVOKE_VideoInputRef_release_m768465B9177E0C453CCF2F747C744FB8ECE00706,
	WebRtcSwigPINVOKE_VideoInputRef_swap__SWIG_0_mB933C57B6C61BFC9FED257C1E3FE2BA2E7CB307E,
	WebRtcSwigPINVOKE_VideoInputRef_swap__SWIG_1_m6EA9C75647F60B884217D1B8CAD38A3A46885AE7,
	WebRtcSwigPINVOKE_VideoInputRef_AddDevice_m4F6590A364BCF702A765E462372E2FC48FB92F15,
	WebRtcSwigPINVOKE_VideoInputRef_UpdateFrame_mA6819B0F9F320499EFB54DE2E09485A1418BFA6D,
	WebRtcSwigPINVOKE_VideoInputRef_UpdateFrame2_m3C7D8FBC00D30389B1F7CF7E55F46A4816DC759F,
	WebRtcSwigPINVOKE_VideoInputRef_RemoveDevice_mBB94B9E6DFBF1ADD6532E772FAA9301EA3F139E2,
	WebRtcSwigPINVOKE_VideoInputRef_AddRef_m9F321A7B1353677E668DAB1C04D928C1181F4312,
	WebRtcSwigPINVOKE_VideoInputRef_Release_mF34E3BE921E01EE15CB009A9058E03D0265AA143,
	WebRtcSwigPINVOKE_delete_VideoInput_m45C706CB41B83E65FAECAE3BA5A3EED1601C513D,
	WebRtcSwigPINVOKE_VideoInput_AddDevice_mFC25A164901FFCEA9522274F4108CA81365C327D,
	WebRtcSwigPINVOKE_VideoInput_UpdateFrame_m53BEC834801E4E073C7D5DFA4A07ABF26621025F,
	WebRtcSwigPINVOKE_VideoInput_UpdateFrame2_m6CFCE3A919EC897176AF63BF2039A0A296D9058B,
	WebRtcSwigPINVOKE_VideoInput_RemoveDevice_mDC34402544493A7CCAD08A86F516BE1E496DB389,
	WebRtcSwigPINVOKE_RTCLog_L_mFE5E54667E1D0F5BCADED27E9A5ED243A3AFD4FB,
	WebRtcSwigPINVOKE_RTCLog_LW_m9D5F87080272E7D6ACE5483BD5F14E62A0B9FA99,
	WebRtcSwigPINVOKE_RTCLog_LE_m786337D585D5D1353A5338D750CB310E28D2CF2E,
	WebRtcSwigPINVOKE_new_RTCLog_m3CC4A8089EAB78024303263AEC6B1CFD3EF3A373,
	WebRtcSwigPINVOKE_delete_RTCLog_m06A757E13EB77B120E10391E4EB0AD4D9D763416,
	WebRtcSwigPINVOKE_new_RTCPeerConnectionFactoryRef__SWIG_0_m8EFC4BAD1396BB03143A33B0D450A10B9A2355C9,
	WebRtcSwigPINVOKE_new_RTCPeerConnectionFactoryRef__SWIG_1_mD29C1FC471D1DF88382720A07393503350A245F6,
	WebRtcSwigPINVOKE_new_RTCPeerConnectionFactoryRef__SWIG_2_mFD969ABA7D67BB6B6A08566D5C40E5D0CF9DCE94,
	WebRtcSwigPINVOKE_delete_RTCPeerConnectionFactoryRef_m17F62F857CB87F253B20290FA34F234A312E2299,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_get_m26C1DE33F6FE9CA6D94F70DD43C31FCCFBAC24B3,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef___deref___m9015CA38DC4AEC699CC30CF7FFAAA7F120ADBA69,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_release_mF7C49C0C011536EF82954DF5CF3AE99C4F196283,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_swap__SWIG_0_m52941FA7F91B23B86350892EF42171A51BEE6902,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_swap__SWIG_1_mFA58DBF164675009485224D6C4CC356B5C1B1F58,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_Create_mFFBF6BA64F97A294CF4ADD9332A4EC7B429D89C0,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_SetObosoleteVideo_mF7F643E0811E0E902F0E750E5D8EBBDA1066A9B6,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_ForceDummyAudio_mF761EF79FABDC7193C5488BEECB279EBFF64F302,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_AddVideoCaptureFactory_mE7A12C6C54C03F32F331C817F6790F7A258A9299,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_IsInitialized_mD6EA43B0B7B2172E04C7F71FA65F221C9BDCB524,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_Initialize_mC1633844B00D3135B37887FBE72C779965D598CE,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_SetGlobalSpeakerMute_mC64ACED8F7FCD71964741DCF6B2C26B2A57C73B3,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetGlobalSpeakerMute_m79D14778401B816C41E4C3EF84E4AD703F4C196D,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_HasGlobalSpeakerMute_m071E2C473EB1A173BAB672683BC9DFA7FA7F8CF8,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingPeer_m94D84F8805E4846E78C92B538502AD9E51807028,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreateAsyncPeer_m4581DE4F0180737B1B97B5FDF265615708BEBAED,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_m595FAA6C0739F8EA3292AD17A6FC7F33BF1D50F7,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetDebugDeviceInfo_Old_m2928AE71588C39CA98CECFBD408B6EE5EFEDA8FC,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreateMediaStream__SWIG_0_m568A65885547B976C95DC9F42C5F03A596AE4721,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreateMediaStream__SWIG_1_m4B29112C4CC1ED9E1AFAAB1C954C1C96584AAB91,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingMediaStream__SWIG_0_m3999330087BD81D1FCD0267F3E0991659C1C8597,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_CreatePollingMediaStream__SWIG_1_m8197175FDB2105E8D1D8E83759957D3C710661E1,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetVideoDevices_m98CA00081AF179472614755566C1D276046885E1,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetVideoInput_m2751861765F27524AABF0012E5266C65EDD55743,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetThreads_m8DBAE7E66B3C20FBF578C40F8FA815E73DD92145,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetSignalingThread_m88486566B737AC7D246B5E511E81553DA802FB43,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_GetAudioDeviceModule_m3CAFFEF2F171A4A009AD91FC0D1D3E7CD9F7930E,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_InitAndroidContext_m95A9BD00FC4964B11BD633145AB39ED560ED1CE9,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_AddRef_m3320DFCE956A38F98269C5CA70467522359999B5,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactoryRef_Release_m35E4E0C34D51BE7010D0D355D64C67DABEC201E6,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_Create_mB3B3A0055311942838D4AD3621B1088C460E8649,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_SetObosoleteVideo_mB1F57A02BF0ACE8DA721BBD1C86441E923A0A5BE,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_ForceDummyAudio_m76E228204CB2F7C2D745BB65BDEB07731DE9907B,
	WebRtcSwigPINVOKE_delete_RTCPeerConnectionFactory_m17EA5AC0E1A67D40B76D87914F38FEDBCA811937,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_AddVideoCaptureFactory_mF8FB014A0D757A59001DDF4DCA29576497FB30DB,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_IsInitialized_mC9FB48CD0770F58A936E8228B83A4EE3F8C94287,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_Initialize_m7828E66FCEA3CE0C5FC75845D82817585CBAA660,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_SetGlobalSpeakerMute_m6464E661B915D9AF1D4DF225926E6CBB8B435D8F,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetGlobalSpeakerMute_mE2A1E242E196F61B0A8ACCCAA67E6A0500A0F17E,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_HasGlobalSpeakerMute_mB4F87E1B818A25ABE7A0F24B6E645ADA777477BA,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreatePollingPeer_mE9DADB52FD725586FD73CFA20D407EBB1A92FA09,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreateAsyncPeer_mF98DD421E99DB07DC260223CC2F7ED80557B0F09,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetDebugDeviceInfo_m70A93488FFD3E421A9D34A56925F305D2BE00CC6,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetDebugDeviceInfo_Old_mE4EB9896419C4D0601E98BDD6A054E01736215E6,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreateMediaStream__SWIG_0_m367ADF12FB6EE0D9A7690285AF2D53636FE813A3,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreateMediaStream__SWIG_1_mBB25D0F1197EB89C8681386DE766D0457E5A11DC,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreatePollingMediaStream__SWIG_0_m15C7DD44FDA15803C9020D82548B4406F0603370,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_CreatePollingMediaStream__SWIG_1_mC71126F6442826D71C00EC99E3612F2EF2159B1F,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetVideoDevices_m01A814B53ECAC2E78370099DA81AF88E74219532,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetVideoInput_m7C5DF7776951425CC0C6C054677A266530FE5CD1,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetThreads_mADE72072F224D6A91AD6072864014F142A45838B,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetSignalingThread_mCDAD5A4086A5BF1A302AB8C21AABDBAB993E0A1A,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_GetAudioDeviceModule_m52F80D76D07B624C163DBC344BA47B70C8C4CE7E,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_InitAndroidContext_m1C611023295C36B80D034DC8717D5F93B457A816,
	WebRtcSwigPINVOKE_GlobalStats_SetActive_m515CB87BCEA9962E4D1C58B12854E0A268DB89B2,
	WebRtcSwigPINVOKE_GlobalStats_IsActive_mDBC465B10821A8CE14E4AA2C7581E88A602E568A,
	WebRtcSwigPINVOKE_GlobalStats_SetRefreshTime_m576E7E9384A19EC5C47EF5E65D1CDE3FE228EC76,
	WebRtcSwigPINVOKE_GlobalStats_HasStats_m27C7C15C1B4EE15CF28D4B429DF250BB405018C8,
	WebRtcSwigPINVOKE_GlobalStats_Dequeue_mC61F313C764F55F539238CA96186040466075DEB,
	WebRtcSwigPINVOKE_new_GlobalStats_m0CC9CD635C008FF6BEF1BD5AD7AB79A0866C65A7,
	WebRtcSwigPINVOKE_delete_GlobalStats_m8D36E67BDBD4D46C0455C1D019E7C7420565D984,
	WebRtcSwigPINVOKE_IosHelper_IsAvailable_m2D5FA7C479DEA06332E1322E1C7E8079A02AD122,
	WebRtcSwigPINVOKE_IosHelper_InitAudioLayer_m1D89C0DD0CED1E362645AD350F9A59DEB38555CF,
	WebRtcSwigPINVOKE_IosHelper_SetLoudspeakerStatus_mF806DC2F966238B75CA7FA029880D3567216165E,
	WebRtcSwigPINVOKE_IosHelper_GetLoudspeakerStatus_mC526A140FF4319E640D83899120E288369DD91C8,
	WebRtcSwigPINVOKE_IosHelper_IosKeepAudioActive_m2926161BC5C7BD9EED6C96EDF039D534DB7E912F,
	WebRtcSwigPINVOKE_new_IosHelper_m53587720A06418BD2051E604B52C81F79D32FED6,
	WebRtcSwigPINVOKE_delete_IosHelper_m9DF0F3603BC9B3E4A422BACF98E9F044E71BC514,
	WebRtcSwigPINVOKE_VideoFormat_SWIGUpcast_mB17513610DFEF1B44D56E72944F0ACAB9FDB96FF,
	WebRtcSwigPINVOKE_DataChannelInterface_SWIGUpcast_m8821D81867404D22000802DDBB63CBD8EB1EAA18,
	WebRtcSwigPINVOKE_PeerConnectionInterface_SWIGUpcast_m4F1A38FDD370D1B33E9A983176F668B262BFC059,
	WebRtcSwigPINVOKE_PollingMediaStream_SWIGUpcast_m2035616DDD305CE1B3962123DE9FF6F597909058,
	WebRtcSwigPINVOKE_PollingPeer_SWIGUpcast_m6A10B5B44C87DBDB73DBC7BB2AEEF10F23144E1D,
	WebRtcSwigPINVOKE_AsyncPeerEventStringArgs_SWIGSmartPtrUpcast_mB4AAC7D520BD333D2DB110A854FB0655F41A12FD,
	WebRtcSwigPINVOKE_AsyncPeerEventMediaStreamArgs_SWIGSmartPtrUpcast_mB8F5983AEA840CE6201397B2B15E4D7E9C27A567,
	WebRtcSwigPINVOKE_AsyncPeerEventDataChannelArgs_SWIGSmartPtrUpcast_mCA4949B764CC049B91002275526997A830B96587,
	WebRtcSwigPINVOKE_AsyncDataChannel_SWIGUpcast_m76B985290D04E4C953F3688024C4629F8732F142,
	WebRtcSwigPINVOKE_AsyncPeer_SWIGUpcast_m4D530C326FFB26154AE152F037423AE4346AD92B,
	WebRtcSwigPINVOKE_VideoInput_SWIGUpcast_mF6A1A146CCD57F3D45EFB44A5B6107A9B86A9674,
	WebRtcSwigPINVOKE_RTCPeerConnectionFactory_SWIGUpcast_m801B5A6302B387D3A52087F15BB6FD03A320F0C9,
	WebRtcSwigPINVOKE__ctor_mA6D767678D44BAEED257CB06D981ED2D80FDBB6B,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_WebRtcSwig_mE2674A81F559F4ABC42618A05A7820A7733F8712,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_WebRtcSwig_m574844BB23B961E8F451862FC98E559ABBC18E5C,
	SWIGExceptionHelper_SetPendingApplicationException_m34EBF8A5064761FA3A1489B58F0FC88F22A91CD7,
	SWIGExceptionHelper_SetPendingArithmeticException_m8290EEE144C47E4E40A26518F15E4456E66428BB,
	SWIGExceptionHelper_SetPendingDivideByZeroException_m79369FABDFC4B7A658FB90AA83A833582BA50B29,
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m0DDAA0B11C0577104A3C337CD95BCEB0474778CD,
	SWIGExceptionHelper_SetPendingInvalidCastException_m63B0AB9D01F05113151C6F5AEF564CA18A712C4C,
	SWIGExceptionHelper_SetPendingInvalidOperationException_mAD82A0B9BB052EE4E2C9494A5C06D29561CF350F,
	SWIGExceptionHelper_SetPendingIOException_mB3905F1943D8A52F63FF86D33C8B55F4D2BFE1BC,
	SWIGExceptionHelper_SetPendingNullReferenceException_m1678867024287040EAEF9E1A6B603632E949C81C,
	SWIGExceptionHelper_SetPendingOutOfMemoryException_mE49ACFFEB5D1BDCA69BA5306BDC2BA5764FCA627,
	SWIGExceptionHelper_SetPendingOverflowException_m941534271CDCF96CC46373C3D001D38B687A5AAE,
	SWIGExceptionHelper_SetPendingSystemException_mA8895E77BEDB03935FAD2AF1684802D8B9CA66FB,
	SWIGExceptionHelper_SetPendingArgumentException_m1A7F32CCCD497644F0D4F54AD803BC783B68531E,
	SWIGExceptionHelper_SetPendingArgumentNullException_mA3831677B0C3CB59A79EAB9F63570D4FCAE5206F,
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB95333E5EEE5C6FFCF1A8B9EFEB60E7301498FDD,
	SWIGExceptionHelper__cctor_m9A2AC49E5565D72585A7D5C39EC304CFB402DAD9,
	SWIGExceptionHelper__ctor_mD199EDDA31FF78BECA863CE55EC499D86FA7497F,
	ExceptionDelegate__ctor_mDEB8E0006B6FBE8D17612D71B6AAEF18CA1F2BFB,
	ExceptionDelegate_Invoke_m9EC896D2C89EA9FC2417AC879F54603DABF652A2,
	ExceptionDelegate_BeginInvoke_mBC8512D05FBC223B5734CFCEEEBC7893FAA63224,
	ExceptionDelegate_EndInvoke_mC46EE8D0799B8DBA8A575913529835AD4A96AF33,
	ExceptionArgumentDelegate__ctor_mE53DD4E636AFEFA860A74C66DBCD22EB2CA5FC2B,
	ExceptionArgumentDelegate_Invoke_m055EED3E5E1A9DFCD017E05642B433FA4999F430,
	ExceptionArgumentDelegate_BeginInvoke_mD001EECFE6F02BD79269A7C2934CF7C2043B5406,
	ExceptionArgumentDelegate_EndInvoke_m42FA221703F90A30204917FE52946749339A8A2D,
	SWIGPendingException_get_Pending_m9D7C57725172B3859CC1753DA69B613817C79B54,
	SWIGPendingException_Set_m7E6B6622888484216F2A13456DF8E19D9903E0A6,
	SWIGPendingException_Retrieve_mB7CB6383FAE280390ADE774FA195AB3A1CCA3FEF,
	SWIGPendingException__ctor_m46EE8F3F43A740DAE12CFC4B00E5B6C150C9B6CF,
	SWIGPendingException__cctor_m6C128E6FE4518D21F0A0F39B4D49DA209E22F693,
	SWIGStringHelper_SWIGRegisterStringCallback_WebRtcSwig_m44538ACE045CF7A595A459FD2583ADBF50A1CF79,
	SWIGStringHelper_CreateString_m38DA578E6839BE86909501F1021E85EC709E4F2A,
	SWIGStringHelper__cctor_m75FB3CD968CB6CDF765511BE3C035D53B2116ACF,
	SWIGStringHelper__ctor_m125BF570A9740D187730B13933B6874412DA2716,
	SWIGStringDelegate__ctor_m4F67F956CEAE329CC1AE313B1861A9EDB4275225,
	SWIGStringDelegate_Invoke_mF041672ED7A1B6D26E8DEEAB938115B700D8A77F,
	SWIGStringDelegate_BeginInvoke_m5C38CE8D009CF1C7E4588B33ACCAB6CAA4068D1D,
	SWIGStringDelegate_EndInvoke_m235E4339B54823E23FEB31FAAEE929982F5C6FA7,
	WebRtcWrap__ctor_m2E688E11B460A3C0DA503E291FFF3BC38ED2285A,
	WebRtcWrap_getCPtr_m2EEC3DC1C693CB85B1BA7A1A52BFE134776987CC,
	WebRtcWrap_Finalize_mE071FF2AC77CB292EC0A73A818D3659B5A37D7B3,
	WebRtcWrap_Dispose_m04DBCCBFB96F9A59DA028F08D8B48466640AC00C,
	WebRtcWrap_get_kSessionDescriptionTypeName_m6181E3AFDF49107811974C97FF32CA2144A1CBCC,
	WebRtcWrap_get_kSessionDescriptionSdpName_m03F1FDDA9443C23D9E56E8855C2DCAFBAE0D9879,
	WebRtcWrap_get_kCandidateSdpMidName_m97726CB8D2D284DAB82679E4B728F90D8EF252ED,
	WebRtcWrap_get_kCandidateSdpMlineIndexName_m308341DC8B4BDAF42008DCCF0326777FE3C67B7D,
	WebRtcWrap_get_kCandidateSdpName_mC07EEC50A7221017DB4275730A8A1EE88A57D817,
	WebRtcWrap_TestLog_m553448FB929A8AAC82003E22A4BE0E9C5F69260F,
	WebRtcWrap_SetLogCallback_mA005255597F4FFD9AFB51234CCFB4CCFF4D8B2D3,
	WebRtcWrap_ResetLogCallback_mF7D38F4FCAA67F8F55BDE1178896316970DCEB29,
	WebRtcWrap_SetDebugLogLevel_mB114A6F43E604E30006C282F406BBFF4B85F9BFB,
	WebRtcWrap_Log_mB2A85CC6DC6D5074F6ECCB675070F22A97ECD572,
	WebRtcWrap_IsSessionDescriptionJson_m14E8CD79F17587E760093F63804D05171D2602E2,
	WebRtcWrap_IsIceCandidateJson_mA76DED218D9075539821780A7474B59560679CFE,
	WebRtcWrap_ParseJsonSessionDescription_m221A084B998A892A033CEA6FA66601C83C17C8E3,
	WebRtcWrap_ToJson_mEA45B4A01B1A3BEED3CD7F568EF5FC9637F3C7AA,
	WebRtcWrap_SessionDescriptionToJson_mD3463D047AB9E0614A733ACD81996D230C7DC4CA,
	WebRtcWrap_ParseJsonIceCandidate_m285C91F4A5622F604D0DEA423055387D578F45BB,
	WebRtcWrap_ToJson_m27E4DADE20F546862877B4B0233D8CF1D6648327,
	WebRtcWrap_IceCandidateToJson_m6EF874CE41C2D5C9D3D50637D423E560BD6A21F3,
	WebRtcWrap_Copy_mAE68A337B1DFA6FFABE8BDD16880331DD499807D,
	WebRtcWrap_Copy_mB76C351A459CB3FC6686B4C761239D491A5397B5,
	WebRtcWrap_GetWebRtcVersion_mD846FA2E6ED8B95BC6AE8C680A45CB55B383D083,
	WebRtcWrap_GetVersion_m423D2AD6306662056A3225BA50E8065ACDCC8E24,
	WebRtcWrap_IsDebugBuild_mCEDD30DFEA4B9FBC1C7BA61C8A9290477B96A210,
	WebRtcWrap_ConvertRotationMode_m395D4D09333B58A9E4B0E57315EEDC4F5E1A5B45,
	WebRtcWrap_ScaleToI420_mF355B0898673FAB4A32511430DF436ED1E5A7A4C,
	LogCallback__ctor_m83C1FEEF2E89DB819BB2A2AC637C9A23CB0DED2E,
	LogCallback_Invoke_m61E228049482540E190905C702ED18297BBD1E98,
	LogCallback_BeginInvoke_m3694A7F76024F31AAC49BC2CC0D1087AC0FBB00F,
	LogCallback_EndInvoke_mCAD7B91FD78CD1C02ACF419500D3A3B79BA9FCC0,
	MonoPInvokeCallbackAttribute__ctor_mEF36AF2E79BE146D6DFBAE273374B93A24E37065,
};
static const int32_t s_InvokerIndices[1816] = 
{
	4313,
	13704,
	9442,
	9442,
	7597,
	9247,
	9289,
	9161,
	9247,
	9161,
	5478,
	9289,
	2447,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	7597,
	9289,
	9289,
	9289,
	7597,
	7597,
	7597,
	9247,
	9289,
	9161,
	9247,
	9161,
	5478,
	9289,
	2447,
	9442,
	9247,
	4313,
	13704,
	9442,
	9442,
	9442,
	9442,
	9247,
	9442,
	9442,
	7597,
	7597,
	9289,
	7597,
	9289,
	9442,
	9442,
	3462,
	7597,
	7597,
	7597,
	4313,
	13704,
	9442,
	9442,
	7557,
	9247,
	9247,
	9289,
	9289,
	9289,
	4313,
	13704,
	9442,
	9442,
	4037,
	9289,
	4313,
	13704,
	9442,
	9442,
	4037,
	9289,
	4313,
	13704,
	9442,
	9442,
	4037,
	9289,
	13922,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	7597,
	9289,
	9289,
	9289,
	7597,
	7597,
	9442,
	9442,
	9247,
	9442,
	9442,
	7597,
	7597,
	9289,
	7597,
	9289,
	9442,
	9442,
	3462,
	7597,
	7597,
	7597,
	9442,
	9247,
	4313,
	13704,
	9442,
	9442,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	9442,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	7597,
	7717,
	4465,
	9422,
	9422,
	7597,
	7597,
	7717,
	7717,
	9442,
	4313,
	13704,
	9442,
	9442,
	4345,
	7597,
	9422,
	7597,
	9289,
	7469,
	9161,
	4313,
	13704,
	9442,
	9442,
	7469,
	9161,
	7469,
	9161,
	7557,
	9247,
	7557,
	9247,
	7597,
	9289,
	7469,
	9161,
	7557,
	9247,
	9442,
	4313,
	13704,
	9442,
	13914,
	7597,
	9442,
	9289,
	9161,
	9161,
	9421,
	9421,
	9289,
	9161,
	9247,
	9247,
	9422,
	9423,
	9422,
	9423,
	9423,
	9442,
	5478,
	4313,
	13704,
	9442,
	9442,
	7597,
	7718,
	4313,
	13704,
	9442,
	9442,
	14242,
	14423,
	14252,
	14423,
	14458,
	9442,
	4313,
	13704,
	9442,
	9442,
	7597,
	9161,
	9161,
	6739,
	4037,
	9247,
	7557,
	9247,
	9161,
	7597,
	4359,
	1594,
	9289,
	9289,
	9289,
	9442,
	7597,
	9422,
	9422,
	7717,
	9442,
	7597,
	7557,
	6739,
	6739,
	4037,
	7597,
	3440,
	4037,
	4037,
	7557,
	4002,
	12862,
	9442,
	4002,
	4037,
	7597,
	9289,
	9289,
	9161,
	9442,
	9442,
	4313,
	13704,
	9442,
	9442,
	14423,
	14502,
	14242,
	14423,
	14242,
	9442,
	4313,
	13704,
	9442,
	9442,
	7469,
	9161,
	7469,
	9161,
	7597,
	9289,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	9442,
	4313,
	13704,
	9442,
	9442,
	9442,
	9422,
	2770,
	4313,
	13704,
	9442,
	9442,
	9442,
	13,
	4313,
	13704,
	9442,
	9289,
	9289,
	5478,
	7597,
	5478,
	3462,
	9289,
	9289,
	9289,
	1873,
	7597,
	4368,
	4368,
	9442,
	3462,
	9289,
	9289,
	9289,
	9289,
	9289,
	9289,
	4368,
	4368,
	4368,
	9289,
	2761,
	5478,
	5478,
	5478,
	7469,
	7469,
	9247,
	9247,
	9247,
	9247,
	9442,
	9442,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7557,
	9247,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	7557,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9247,
	7557,
	9247,
	7557,
	7597,
	9289,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7597,
	9289,
	7557,
	9247,
	7469,
	9161,
	7469,
	9161,
	7557,
	9247,
	7469,
	9161,
	7469,
	9161,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7469,
	9161,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7557,
	9247,
	7469,
	9161,
	7597,
	9289,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7557,
	9247,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7597,
	9289,
	7469,
	9161,
	14502,
	4313,
	13704,
	9442,
	9442,
	7557,
	9247,
	7557,
	9247,
	7469,
	9161,
	7469,
	9161,
	7469,
	9161,
	7557,
	9247,
	9442,
	754,
	14502,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	4313,
	13704,
	9442,
	9442,
	9289,
	9161,
	9161,
	9161,
	7469,
	6987,
	2159,
	9161,
	9422,
	9250,
	9161,
	9442,
	9247,
	9247,
	9247,
	7501,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	7597,
	9289,
	9289,
	9289,
	7597,
	7597,
	9289,
	9161,
	9161,
	9161,
	7469,
	6987,
	2159,
	9161,
	9422,
	9250,
	9161,
	9442,
	9247,
	9247,
	9247,
	7501,
	9442,
	9247,
	4313,
	13704,
	9442,
	9442,
	9442,
	9442,
	9247,
	9161,
	9289,
	7597,
	9161,
	9289,
	9442,
	9442,
	9442,
	9442,
	9442,
	9247,
	9161,
	2631,
	918,
	6319,
	4368,
	6739,
	9289,
	7597,
	7597,
	2242,
	9161,
	5478,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	7597,
	9289,
	9289,
	9289,
	7597,
	7597,
	9442,
	9442,
	9247,
	9161,
	9289,
	7597,
	9161,
	9289,
	9442,
	9442,
	9442,
	9442,
	9442,
	9247,
	9161,
	2631,
	918,
	6319,
	4368,
	6739,
	9289,
	7597,
	7597,
	2242,
	9161,
	5478,
	9442,
	9247,
	4313,
	13704,
	9442,
	9442,
	14256,
	14256,
	14256,
	9442,
	4313,
	13704,
	9442,
	9442,
	14458,
	14242,
	14242,
	7597,
	9161,
	9161,
	7469,
	9161,
	9161,
	6745,
	6745,
	14458,
	14458,
	6745,
	3462,
	6745,
	3462,
	9289,
	9289,
	9289,
	9289,
	9289,
	13597,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	7597,
	9289,
	9289,
	9289,
	7597,
	7597,
	9289,
	7469,
	7469,
	7597,
	9161,
	9161,
	7469,
	9161,
	9161,
	6745,
	6745,
	9289,
	9289,
	6745,
	3462,
	6745,
	3462,
	9289,
	9289,
	9289,
	9289,
	9289,
	5442,
	9442,
	9247,
	4313,
	13704,
	9442,
	9442,
	9247,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	9442,
	13704,
	4313,
	13704,
	9442,
	9442,
	7597,
	9161,
	9161,
	6739,
	4037,
	9247,
	7557,
	9247,
	9161,
	7597,
	4359,
	1594,
	9289,
	9289,
	9289,
	9442,
	7597,
	9422,
	9422,
	7717,
	9442,
	7597,
	7557,
	6739,
	6739,
	4037,
	7597,
	3440,
	4037,
	4037,
	7557,
	4002,
	12862,
	9442,
	4002,
	4037,
	5478,
	6353,
	6353,
	5478,
	7597,
	9289,
	9289,
	9161,
	9442,
	9442,
	4313,
	13704,
	9442,
	9442,
	14447,
	14447,
	9442,
	4313,
	13704,
	9442,
	9442,
	9442,
	1583,
	7597,
	1583,
	13770,
	13750,
	14028,
	9247,
	9161,
	5478,
	9289,
	14502,
	4313,
	13704,
	9442,
	9442,
	7557,
	9247,
	7557,
	9247,
	7558,
	9248,
	7717,
	9422,
	9442,
	4313,
	13704,
	9442,
	9442,
	1675,
	113,
	111,
	7597,
	4313,
	13704,
	9442,
	9442,
	9442,
	7597,
	7597,
	9289,
	9289,
	9289,
	7597,
	7597,
	1675,
	113,
	111,
	7597,
	9442,
	9247,
	13667,
	14432,
	14447,
	11904,
	12729,
	12729,
	11686,
	11686,
	10920,
	12646,
	12646,
	12646,
	12646,
	9666,
	13749,
	14497,
	14497,
	14175,
	13937,
	14447,
	14447,
	9442,
	14502,
	13667,
	14248,
	13745,
	14449,
	13789,
	13793,
	13795,
	12788,
	14248,
	14167,
	14167,
	13170,
	13170,
	13175,
	13175,
	14248,
	14432,
	14447,
	11904,
	12709,
	12709,
	11644,
	11644,
	10849,
	12640,
	12640,
	12640,
	12640,
	9665,
	14449,
	14248,
	9657,
	13749,
	14497,
	14497,
	14175,
	13937,
	13171,
	13745,
	13171,
	13745,
	13172,
	13768,
	13175,
	14167,
	14449,
	14248,
	14448,
	14449,
	11074,
	13789,
	10554,
	13770,
	13750,
	14028,
	13745,
	13593,
	12431,
	13911,
	14248,
	13168,
	13593,
	13168,
	13593,
	13171,
	13745,
	13171,
	13745,
	13174,
	13911,
	13168,
	13593,
	13171,
	13745,
	14449,
	14248,
	12770,
	13793,
	14167,
	13170,
	13789,
	13168,
	13593,
	14248,
	14248,
	13170,
	13176,
	13914,
	13170,
	14248,
	13911,
	13593,
	13593,
	14148,
	14148,
	13911,
	13593,
	13745,
	13745,
	14167,
	14185,
	14167,
	14185,
	14185,
	14248,
	12431,
	14248,
	13174,
	14167,
	14167,
	13175,
	14449,
	13789,
	13790,
	12834,
	12834,
	12121,
	13170,
	11719,
	12121,
	12119,
	13171,
	12120,
	12785,
	14248,
	12120,
	12119,
	12434,
	12711,
	12711,
	12434,
	14248,
	14248,
	13170,
	14167,
	14167,
	13175,
	14449,
	13789,
	13790,
	12772,
	12772,
	12119,
	13170,
	11719,
	12119,
	12119,
	13171,
	12120,
	12772,
	14248,
	12120,
	12119,
	14248,
	14449,
	13789,
	14248,
	13174,
	13911,
	13170,
	13789,
	13174,
	13911,
	13174,
	13911,
	13171,
	13745,
	13174,
	13911,
	13170,
	13789,
	13170,
	13789,
	14449,
	13789,
	13790,
	14248,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13745,
	13171,
	13745,
	13171,
	14447,
	14447,
	14447,
	13170,
	13789,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13170,
	13789,
	13171,
	13745,
	13168,
	13593,
	13168,
	13593,
	13171,
	13745,
	13168,
	13593,
	13168,
	13593,
	13170,
	13789,
	13170,
	13789,
	13170,
	13789,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13168,
	13593,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13168,
	13593,
	13170,
	13789,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13170,
	13789,
	13170,
	13789,
	13170,
	13789,
	13170,
	13789,
	13170,
	13789,
	13170,
	13789,
	13170,
	13789,
	13170,
	13789,
	13170,
	13789,
	13171,
	13745,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13170,
	13789,
	13168,
	13593,
	14447,
	14447,
	14447,
	13171,
	13745,
	13171,
	13745,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13171,
	13745,
	14449,
	10350,
	14248,
	13789,
	13789,
	12431,
	13170,
	12431,
	11722,
	13789,
	13789,
	13789,
	10732,
	13170,
	12117,
	12117,
	14248,
	11720,
	13789,
	13789,
	13789,
	13789,
	13789,
	13789,
	12117,
	12117,
	12117,
	13789,
	11505,
	12431,
	12431,
	12431,
	14449,
	14248,
	13170,
	13789,
	13170,
	13789,
	13170,
	13789,
	13168,
	13168,
	13745,
	13745,
	13745,
	13745,
	14248,
	14248,
	14458,
	14458,
	14458,
	14458,
	14458,
	14248,
	14256,
	13259,
	14502,
	14252,
	13259,
	13600,
	13600,
	13793,
	13911,
	12869,
	13793,
	13911,
	13911,
	12124,
	12210,
	14458,
	14458,
	14423,
	13789,
	12709,
	14447,
	14447,
	14447,
	14447,
	14449,
	14248,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	13168,
	13593,
	14449,
	14248,
	13168,
	13593,
	13168,
	13593,
	13174,
	13911,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	13171,
	13745,
	14449,
	14248,
	14449,
	13789,
	13789,
	14248,
	13789,
	13789,
	13789,
	13170,
	13170,
	13789,
	13593,
	13593,
	13593,
	13168,
	13019,
	11206,
	13593,
	14167,
	13789,
	13593,
	14248,
	13745,
	13745,
	13745,
	13169,
	14248,
	13745,
	14248,
	13789,
	13593,
	13593,
	13593,
	13168,
	13019,
	11206,
	13593,
	14167,
	13789,
	13593,
	14248,
	13745,
	13745,
	13745,
	13169,
	14449,
	13789,
	13789,
	14248,
	13789,
	13789,
	13789,
	13170,
	13170,
	14248,
	14248,
	13745,
	13593,
	13911,
	13174,
	13593,
	13911,
	14248,
	14248,
	14248,
	14248,
	14248,
	13745,
	13593,
	11504,
	10102,
	12710,
	12122,
	12834,
	13789,
	13170,
	13170,
	11279,
	13593,
	12431,
	14248,
	13745,
	14248,
	14248,
	14248,
	13745,
	13593,
	13911,
	13174,
	13593,
	13911,
	14248,
	14248,
	14248,
	14248,
	14248,
	13745,
	13593,
	11504,
	10102,
	12710,
	12122,
	12834,
	13789,
	13170,
	13170,
	11279,
	13593,
	12431,
	13790,
	14248,
	13745,
	13745,
	13789,
	13789,
	13911,
	12777,
	14248,
	13911,
	13789,
	12775,
	14248,
	13789,
	12775,
	14248,
	13789,
	14449,
	13789,
	13789,
	14248,
	13789,
	13789,
	13789,
	13170,
	13170,
	14248,
	14248,
	13745,
	14248,
	14248,
	13174,
	13174,
	13911,
	13174,
	13789,
	14248,
	14248,
	11720,
	13170,
	13170,
	13170,
	14248,
	13745,
	14449,
	13789,
	13789,
	14248,
	13789,
	13789,
	13789,
	13170,
	13170,
	13170,
	13745,
	13911,
	13593,
	13745,
	13593,
	12431,
	13789,
	11280,
	14248,
	13745,
	14449,
	14167,
	11506,
	14248,
	14248,
	13170,
	13745,
	13911,
	13593,
	13745,
	13593,
	12431,
	13789,
	11280,
	14248,
	14248,
	14248,
	13745,
	14248,
	14248,
	13174,
	13174,
	13911,
	13174,
	13789,
	14248,
	14248,
	11720,
	13170,
	13170,
	13170,
	14449,
	13789,
	13789,
	14248,
	13789,
	13789,
	13789,
	13170,
	13170,
	10555,
	9723,
	9723,
	13174,
	14248,
	13745,
	14248,
	10555,
	9723,
	9723,
	13174,
	14256,
	14256,
	14256,
	14449,
	14248,
	14449,
	13789,
	13789,
	14248,
	13789,
	13789,
	13789,
	13170,
	13170,
	13789,
	13168,
	13168,
	13170,
	13593,
	13593,
	13168,
	13593,
	13593,
	12771,
	12771,
	13911,
	13911,
	12771,
	11718,
	12771,
	11718,
	13789,
	13789,
	13789,
	13789,
	13789,
	12433,
	14248,
	13745,
	14449,
	14242,
	14242,
	14248,
	13170,
	13593,
	13593,
	13168,
	13593,
	13593,
	12771,
	12771,
	14458,
	14458,
	12771,
	11718,
	12771,
	11718,
	13789,
	13789,
	13789,
	13789,
	13789,
	13597,
	14242,
	14423,
	14252,
	14423,
	14458,
	14449,
	14248,
	14423,
	14502,
	14242,
	14423,
	14242,
	14449,
	14248,
	13792,
	13792,
	13792,
	13792,
	13792,
	13792,
	13792,
	13792,
	13792,
	13792,
	13792,
	13792,
	9442,
	9702,
	12197,
	14256,
	14256,
	14256,
	14256,
	14256,
	14256,
	14256,
	14256,
	14256,
	14256,
	14256,
	13266,
	13266,
	13266,
	14502,
	9442,
	4362,
	7597,
	2094,
	7597,
	4362,
	4368,
	1462,
	7597,
	14423,
	14256,
	14458,
	9442,
	14502,
	14256,
	13922,
	14502,
	9442,
	4362,
	6745,
	2094,
	6745,
	4313,
	13704,
	9442,
	9442,
	14458,
	14458,
	14458,
	14458,
	14458,
	14256,
	13259,
	14502,
	14252,
	13259,
	13600,
	13600,
	13922,
	13922,
	12869,
	13922,
	13922,
	13922,
	12200,
	12212,
	14458,
	14458,
	14423,
	13922,
	12729,
	4362,
	7597,
	2094,
	7597,
	7597,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[15] = 
{
	{ 0x060006D2, 94,  (void**)&SWIGExceptionHelper_SetPendingApplicationException_m34EBF8A5064761FA3A1489B58F0FC88F22A91CD7_RuntimeMethod_var, 0 },
	{ 0x060006D3, 98,  (void**)&SWIGExceptionHelper_SetPendingArithmeticException_m8290EEE144C47E4E40A26518F15E4456E66428BB_RuntimeMethod_var, 0 },
	{ 0x060006D4, 99,  (void**)&SWIGExceptionHelper_SetPendingDivideByZeroException_m79369FABDFC4B7A658FB90AA83A833582BA50B29_RuntimeMethod_var, 0 },
	{ 0x060006D5, 101,  (void**)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m0DDAA0B11C0577104A3C337CD95BCEB0474778CD_RuntimeMethod_var, 0 },
	{ 0x060006D6, 102,  (void**)&SWIGExceptionHelper_SetPendingInvalidCastException_m63B0AB9D01F05113151C6F5AEF564CA18A712C4C_RuntimeMethod_var, 0 },
	{ 0x060006D7, 103,  (void**)&SWIGExceptionHelper_SetPendingInvalidOperationException_mAD82A0B9BB052EE4E2C9494A5C06D29561CF350F_RuntimeMethod_var, 0 },
	{ 0x060006D8, 100,  (void**)&SWIGExceptionHelper_SetPendingIOException_mB3905F1943D8A52F63FF86D33C8B55F4D2BFE1BC_RuntimeMethod_var, 0 },
	{ 0x060006D9, 104,  (void**)&SWIGExceptionHelper_SetPendingNullReferenceException_m1678867024287040EAEF9E1A6B603632E949C81C_RuntimeMethod_var, 0 },
	{ 0x060006DA, 105,  (void**)&SWIGExceptionHelper_SetPendingOutOfMemoryException_mE49ACFFEB5D1BDCA69BA5306BDC2BA5764FCA627_RuntimeMethod_var, 0 },
	{ 0x060006DB, 106,  (void**)&SWIGExceptionHelper_SetPendingOverflowException_m941534271CDCF96CC46373C3D001D38B687A5AAE_RuntimeMethod_var, 0 },
	{ 0x060006DC, 107,  (void**)&SWIGExceptionHelper_SetPendingSystemException_mA8895E77BEDB03935FAD2AF1684802D8B9CA66FB_RuntimeMethod_var, 0 },
	{ 0x060006DD, 95,  (void**)&SWIGExceptionHelper_SetPendingArgumentException_m1A7F32CCCD497644F0D4F54AD803BC783B68531E_RuntimeMethod_var, 0 },
	{ 0x060006DE, 96,  (void**)&SWIGExceptionHelper_SetPendingArgumentNullException_mA3831677B0C3CB59A79EAB9F63570D4FCAE5206F_RuntimeMethod_var, 0 },
	{ 0x060006DF, 97,  (void**)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB95333E5EEE5C6FFCF1A8B9EFEB60E7301498FDD_RuntimeMethod_var, 0 },
	{ 0x060006F0, 108,  (void**)&SWIGStringHelper_CreateString_m38DA578E6839BE86909501F1021E85EC709E4F2A_RuntimeMethod_var, 0 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_WebRtcCSharp_CodeGenModule;
const Il2CppCodeGenModule g_WebRtcCSharp_CodeGenModule = 
{
	"WebRtcCSharp.dll",
	1816,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	15,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
