﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>



// System.Collections.Generic.IList`1<UnityEngine.Color>
struct IList_1_t78DB7CACF5BDC17685CA41C8A5615F4AE760CB59;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_tFB8BE2ED9A601C1259EAB8D73D1B3E96EA321FA1;
// System.Collections.Generic.IList`1<UnityEngine.Vector2>
struct IList_1_t0DF1E5F56EE58E1A7F1FE26A676FC9FBF4D52A07;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t4EEE459A249DDE104FA2E88234C593389EE5D291;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// TriLibCore.Gltf.Draco.DracoMeshLoader
struct DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF;
// TriLibCore.Gltf.GltfTempGeometryGroup
struct GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83;
// System.String
struct String_t;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute
struct DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7;
// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData
struct DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A;
// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh
struct DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED;

IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral2DC2AAA56C78AE0B1DDA1048580586A194ED4217;
IL2CPP_EXTERN_C String_t* _stringLiteral5A938D1BDDD7401E071E8EB937F4DD9D948C32AB;
IL2CPP_EXTERN_C String_t* _stringLiteral5C3DEFA23303BF83C71BBC5797AC9DCCE832ACB8;
IL2CPP_EXTERN_C String_t* _stringLiteralBCE437B17DC0B5A3A9B46AB4F7BA19E115E0EF4C;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBF85D425B2A8C44129B0267A7E6F4F5B6501A1CD 
{
};

// TriLibCore.Gltf.Draco.DracoMeshLoader
struct DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF  : public RuntimeObject
{
};

// TriLibCore.Gltf.GltfTempGeometryGroup
struct GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83  : public RuntimeObject
{
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Gltf.GltfTempGeometryGroup::Vertices
	RuntimeObject* ___Vertices_0;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Gltf.GltfTempGeometryGroup::NormalsList
	RuntimeObject* ___NormalsList_1;
	// System.Collections.Generic.IList`1<UnityEngine.Vector2> TriLibCore.Gltf.GltfTempGeometryGroup::UVsList
	RuntimeObject* ___UVsList_2;
	// System.Collections.Generic.IList`1<UnityEngine.Color> TriLibCore.Gltf.GltfTempGeometryGroup::ColorsList
	RuntimeObject* ___ColorsList_3;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Gltf.GltfTempGeometryGroup::IndicesList
	RuntimeObject* ___IndicesList_4;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute
struct DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7 
{
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute::attributeType
	int32_t ___attributeType_0;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute::dataType
	int32_t ___dataType_1;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute::numComponents
	int32_t ___numComponents_2;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute::uniqueId
	int32_t ___uniqueId_3;
};

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh
struct DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED 
{
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh::numFaces
	int32_t ___numFaces_0;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh::numVertices
	int32_t ___numVertices_1;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh::numAttributes
	int32_t ___numAttributes_2;
};

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData
struct DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A 
{
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData::dataType
	int32_t ___dataType_0;
	// System.IntPtr TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData::data
	intptr_t ___data_1;
};

// <Module>

// <Module>

// TriLibCore.Gltf.Draco.DracoMeshLoader

// TriLibCore.Gltf.Draco.DracoMeshLoader

// TriLibCore.Gltf.GltfTempGeometryGroup

// TriLibCore.Gltf.GltfTempGeometryGroup

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.ValueType

// System.ValueType

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// UnityEngine.Color

// UnityEngine.Color

// System.Int32

// System.Int32

// System.Int64

// System.Int64

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// System.Single

// System.Single

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3

// System.Void

// System.Void

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C  : public RuntimeArray
{
	ALIGN_FIELD (8) int32_t m_Items[1];

	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 m_Items[1];

	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 m_Items[1];

	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389  : public RuntimeArray
{
	ALIGN_FIELD (8) Color_tD001788D726C3A7F1379BEED0260B9591F440C1F m_Items[1];

	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F value)
	{
		m_Items[index] = value;
	}
};



// System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetMeshIndices(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DracoMeshLoader_GetMeshIndices_m9F3F739E294271FEDF72A776E237BF9A5E5A4B0D (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_mesh, DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A** ___1_indices, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader::DataTypeSize(TriLibCore.Gltf.Draco.DracoMeshLoader/DataType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42 (DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF* __this, int32_t ___0_dt, const RuntimeMethod* method) ;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void* IntPtr_op_Explicit_m2728CBA081E79B97DDCF1D4FAD77B309CA1E94BF (intptr_t ___0_value, const RuntimeMethod* method) ;
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemCpy(System.Void*,System.Void*,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177 (void* ___0_destination, void* ___1_source, int64_t ___2_size, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::ReleaseDracoData(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A** ___0_data, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttributeByType(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,TriLibCore.Gltf.Draco.DracoMeshLoader/AttributeType,System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DracoMeshLoader_GetAttributeByType_mAA4A9B33E2E5845D5EB425D5313DF7EAC29BEA1F (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_mesh, int32_t ___1_type, int32_t ___2_index, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7** ___3_attr, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttributeData(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute*,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DracoMeshLoader_GetAttributeData_mE86815972918FF808B0B27A5F03DEEF6ED977725 (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_mesh, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* ___1_attr, DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A** ___2_data, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::ReleaseDracoAttribute(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DracoMeshLoader_ReleaseDracoAttribute_m567DB1E14B44341C552B2591AA3507E8016745C2 (DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7** ___0_attr, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTempGeometryGroup::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTempGeometryGroup__ctor_mD9970225FF191449FFFDB27DC5055178B23C2B56 (GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 TriLibCore.Utils.RightHandToLeftHandConverter::ConvertVector(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 RightHandToLeftHandConverter_ConvertVector_m0E9E683F500FBA16B79BB686BBBC8E1CE566DA8B (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) ;
// System.Void System.Array::Reverse(System.Array)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Reverse_m464993603E0F56B4A68F70113212032FE7381B6C (RuntimeArray* ___0_array, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader::DecodeDracoMesh(System.Byte[],System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DracoMeshLoader_DecodeDracoMesh_m784E16569E6411367F94E2A3B4209C96F9B1FCD4 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_buffer, int32_t ___1_length, DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED** ___2_mesh, const RuntimeMethod* method) ;
// TriLibCore.Gltf.GltfTempGeometryGroup TriLibCore.Gltf.Draco.DracoMeshLoader::CreateUnityMesh(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* DracoMeshLoader_CreateUnityMesh_m831B6AC86DF643827205BF92DFF1AA89590E6BFC (DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF* __this, DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_dracoMesh, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::ReleaseDracoMesh(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DracoMeshLoader_ReleaseDracoMesh_m1D28A1C2FD9697F6519CCC8A46B814C4412EF720 (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED** ___0_mesh, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DracoMeshLoader__ctor_mAC5A9167F1E64EE1E75675021B80DAC498B7CA58 (DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF* __this, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader::ConvertDracoMeshToGltfGeometryGroup(System.Byte[],TriLibCore.Gltf.GltfTempGeometryGroup&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DracoMeshLoader_ConvertDracoMeshToGltfGeometryGroup_m3FB7E227DCB4224E60D5AB3472D706DDDDC1A543 (DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_encodedData, GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83** ___1_geometryGroup, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL ReleaseDracoMesh(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED**);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL ReleaseDracoAttribute(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL ReleaseDracoData(DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL DecodeDracoMesh(uint8_t*, int32_t, DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED**);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL GetAttribute(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, int32_t, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL GetAttributeByType(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, int32_t, int32_t, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL GetAttributeByUniqueId(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, int32_t, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL GetMeshIndices(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL GetAttributeData(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7*, DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**);
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::ReleaseDracoMesh(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DracoMeshLoader_ReleaseDracoMesh_m1D28A1C2FD9697F6519CCC8A46B814C4412EF720 (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED** ___0_mesh, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED**);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_dracodec_unity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED**);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("dracodec_unity"), "ReleaseDracoMesh", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
	reinterpret_cast<PInvokeFunc>(ReleaseDracoMesh)(___0_mesh);
	#else
	il2cppPInvokeFunc(___0_mesh);
	#endif

}
// System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::ReleaseDracoAttribute(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DracoMeshLoader_ReleaseDracoAttribute_m567DB1E14B44341C552B2591AA3507E8016745C2 (DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7** ___0_attr, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_dracodec_unity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("dracodec_unity"), "ReleaseDracoAttribute", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
	reinterpret_cast<PInvokeFunc>(ReleaseDracoAttribute)(___0_attr);
	#else
	il2cppPInvokeFunc(___0_attr);
	#endif

}
// System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::ReleaseDracoData(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A** ___0_data, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_dracodec_unity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("dracodec_unity"), "ReleaseDracoData", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
	reinterpret_cast<PInvokeFunc>(ReleaseDracoData)(___0_data);
	#else
	il2cppPInvokeFunc(___0_data);
	#endif

}
// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader::DecodeDracoMesh(System.Byte[],System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DracoMeshLoader_DecodeDracoMesh_m784E16569E6411367F94E2A3B4209C96F9B1FCD4 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_buffer, int32_t ___1_length, DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED** ___2_mesh, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint8_t*, int32_t, DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED**);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_dracodec_unity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*) + sizeof(int32_t) + sizeof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED**);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("dracodec_unity"), "DecodeDracoMesh", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___0_buffer' to native representation
	uint8_t* ____0_buffer_marshaled = NULL;
	if (___0_buffer != NULL)
	{
		____0_buffer_marshaled = reinterpret_cast<uint8_t*>((___0_buffer)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(DecodeDracoMesh)(____0_buffer_marshaled, ___1_length, ___2_mesh);
	#else
	int32_t returnValue = il2cppPInvokeFunc(____0_buffer_marshaled, ___1_length, ___2_mesh);
	#endif

	return returnValue;
}
// System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttribute(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DracoMeshLoader_GetAttribute_m3E63384A6101D48EA6CB7A14803655CB7555E13D (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_mesh, int32_t ___1_index, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7** ___2_attr, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, int32_t, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_dracodec_unity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*) + sizeof(int32_t) + sizeof(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("dracodec_unity"), "GetAttribute", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GetAttribute)(___0_mesh, ___1_index, ___2_attr);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_mesh, ___1_index, ___2_attr);
	#endif

	return static_cast<bool>(returnValue);
}
// System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttributeByType(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,TriLibCore.Gltf.Draco.DracoMeshLoader/AttributeType,System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DracoMeshLoader_GetAttributeByType_mAA4A9B33E2E5845D5EB425D5313DF7EAC29BEA1F (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_mesh, int32_t ___1_type, int32_t ___2_index, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7** ___3_attr, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, int32_t, int32_t, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_dracodec_unity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*) + sizeof(int32_t) + sizeof(int32_t) + sizeof(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("dracodec_unity"), "GetAttributeByType", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GetAttributeByType)(___0_mesh, ___1_type, ___2_index, ___3_attr);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_mesh, ___1_type, ___2_index, ___3_attr);
	#endif

	return static_cast<bool>(returnValue);
}
// System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttributeByUniqueId(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,System.Int32,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DracoMeshLoader_GetAttributeByUniqueId_m9127E962BFF3F8760126BBB7609CF6148AF74A93 (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_mesh, int32_t ___1_unique_id, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7** ___2_attr, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, int32_t, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_dracodec_unity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*) + sizeof(int32_t) + sizeof(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("dracodec_unity"), "GetAttributeByUniqueId", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GetAttributeByUniqueId)(___0_mesh, ___1_unique_id, ___2_attr);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_mesh, ___1_unique_id, ___2_attr);
	#endif

	return static_cast<bool>(returnValue);
}
// System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetMeshIndices(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DracoMeshLoader_GetMeshIndices_m9F3F739E294271FEDF72A776E237BF9A5E5A4B0D (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_mesh, DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A** ___1_indices, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_dracodec_unity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*) + sizeof(DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("dracodec_unity"), "GetMeshIndices", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GetMeshIndices)(___0_mesh, ___1_indices);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_mesh, ___1_indices);
	#endif

	return static_cast<bool>(returnValue);
}
// System.Boolean TriLibCore.Gltf.Draco.DracoMeshLoader::GetAttributeData(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute*,TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData**)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DracoMeshLoader_GetAttributeData_mE86815972918FF808B0B27A5F03DEEF6ED977725 (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_mesh, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* ___1_attr, DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A** ___2_data, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*, DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7*, DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_dracodec_unity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*) + sizeof(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7*) + sizeof(DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("dracodec_unity"), "GetAttributeData", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_dracodec_unity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GetAttributeData)(___0_mesh, ___1_attr, ___2_data);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_mesh, ___1_attr, ___2_data);
	#endif

	return static_cast<bool>(returnValue);
}
// TriLibCore.Gltf.GltfTempGeometryGroup TriLibCore.Gltf.Draco.DracoMeshLoader::CreateUnityMesh(TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* DracoMeshLoader_CreateUnityMesh_m831B6AC86DF643827205BF92DFF1AA89590E6BFC (DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF* __this, DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* ___0_dracoMesh, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2DC2AAA56C78AE0B1DDA1048580586A194ED4217);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5A938D1BDDD7401E071E8EB937F4DD9D948C32AB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5C3DEFA23303BF83C71BBC5797AC9DCCE832ACB8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBCE437B17DC0B5A3A9B46AB4F7BA19E115E0EF4C);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_0 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_1 = NULL;
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* V_2 = NULL;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_3 = NULL;
	ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* V_4 = NULL;
	DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t* V_7 = NULL;
	DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* V_8 = NULL;
	DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* V_9 = NULL;
	GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* V_10 = NULL;
	DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* V_11 = NULL;
	DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* V_12 = NULL;
	DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* V_13 = NULL;
	DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* V_14 = NULL;
	int32_t V_15 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_16;
	memset((&V_16), 0, sizeof(V_16));
	int32_t V_17 = 0;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_18;
	memset((&V_18), 0, sizeof(V_18));
	int32_t V_19 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_20;
	memset((&V_20), 0, sizeof(V_20));
	{
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_0 = ___0_dracoMesh;
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED L_1 = (*(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*)L_0);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_2 = ___0_dracoMesh;
		NullCheck(L_2);
		int32_t L_3 = L_2->___numFaces_0;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_4 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply(L_3, 3)));
		V_0 = L_4;
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_5 = ___0_dracoMesh;
		NullCheck(L_5);
		int32_t L_6 = L_5->___numVertices_1;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_7 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)L_6);
		V_1 = L_7;
		V_2 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)NULL;
		V_3 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)NULL;
		V_4 = (ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)NULL;
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_8 = ___0_dracoMesh;
		bool L_9;
		L_9 = DracoMeshLoader_GetMeshIndices_m9F3F739E294271FEDF72A776E237BF9A5E5A4B0D(L_8, (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_5)), NULL);
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_10 = V_5;
		NullCheck(L_10);
		int32_t L_11 = L_10->___dataType_0;
		int32_t L_12;
		L_12 = DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42(__this, L_11, NULL);
		V_6 = L_12;
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_13 = V_5;
		NullCheck(L_13);
		intptr_t L_14 = L_13->___data_1;
		void* L_15;
		L_15 = IntPtr_op_Explicit_m2728CBA081E79B97DDCF1D4FAD77B309CA1E94BF(L_14, NULL);
		V_7 = (int32_t*)L_15;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_16 = V_0;
		NullCheck(L_16);
		void* L_17;
		L_17 = il2cpp_codegen_unsafe_cast(((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		int32_t* L_18 = V_7;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = V_6;
		UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177(L_17, (void*)L_18, ((int64_t)((int32_t)il2cpp_codegen_multiply(((int32_t)(((RuntimeArray*)L_19)->max_length)), L_20))), NULL);
		DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D((DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_5)), NULL);
		V_8 = (DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7*)((uintptr_t)0);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_21 = ___0_dracoMesh;
		bool L_22;
		L_22 = DracoMeshLoader_GetAttributeByType_mAA4A9B33E2E5845D5EB425D5313DF7EAC29BEA1F(L_21, 0, 0, (DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
		V_9 = (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A*)((uintptr_t)0);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_23 = ___0_dracoMesh;
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_24 = V_8;
		bool L_25;
		L_25 = DracoMeshLoader_GetAttributeData_mE86815972918FF808B0B27A5F03DEEF6ED977725(L_23, L_24, (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_9)), NULL);
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_26 = V_9;
		NullCheck(L_26);
		int32_t L_27 = L_26->___dataType_0;
		int32_t L_28;
		L_28 = DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42(__this, L_27, NULL);
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_29 = V_8;
		NullCheck(L_29);
		int32_t L_30 = L_29->___numComponents_2;
		V_6 = ((int32_t)il2cpp_codegen_multiply(L_28, L_30));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_31 = V_1;
		NullCheck(L_31);
		void* L_32;
		L_32 = il2cpp_codegen_unsafe_cast(((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_33 = V_9;
		NullCheck(L_33);
		intptr_t L_34 = L_33->___data_1;
		void* L_35;
		L_35 = IntPtr_op_Explicit_m2728CBA081E79B97DDCF1D4FAD77B309CA1E94BF(L_34, NULL);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_36 = ___0_dracoMesh;
		NullCheck(L_36);
		int32_t L_37 = L_36->___numVertices_1;
		int32_t L_38 = V_6;
		UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177(L_32, L_35, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_37, L_38))), NULL);
		DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D((DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_9)), NULL);
		DracoMeshLoader_ReleaseDracoAttribute_m567DB1E14B44341C552B2591AA3507E8016745C2((DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_39 = ___0_dracoMesh;
		bool L_40;
		L_40 = DracoMeshLoader_GetAttributeByType_mAA4A9B33E2E5845D5EB425D5313DF7EAC29BEA1F(L_39, 1, 0, (DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
		if (!L_40)
		{
			goto IL_0161;
		}
	}
	{
		V_11 = (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A*)((uintptr_t)0);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_41 = ___0_dracoMesh;
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_42 = V_8;
		bool L_43;
		L_43 = DracoMeshLoader_GetAttributeData_mE86815972918FF808B0B27A5F03DEEF6ED977725(L_41, L_42, (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_11)), NULL);
		if (!L_43)
		{
			goto IL_0161;
		}
	}
	{
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_44 = V_11;
		NullCheck(L_44);
		int32_t L_45 = L_44->___dataType_0;
		int32_t L_46;
		L_46 = DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42(__this, L_45, NULL);
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_47 = V_8;
		NullCheck(L_47);
		int32_t L_48 = L_47->___numComponents_2;
		V_6 = ((int32_t)il2cpp_codegen_multiply(L_46, L_48));
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_49 = ___0_dracoMesh;
		NullCheck(L_49);
		int32_t L_50 = L_49->___numVertices_1;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_51 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)SZArrayNew(Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C_il2cpp_TypeInfo_var, (uint32_t)L_50);
		V_3 = L_51;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_52 = V_3;
		NullCheck(L_52);
		void* L_53;
		L_53 = il2cpp_codegen_unsafe_cast(((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_54 = V_11;
		NullCheck(L_54);
		intptr_t L_55 = L_54->___data_1;
		void* L_56;
		L_56 = IntPtr_op_Explicit_m2728CBA081E79B97DDCF1D4FAD77B309CA1E94BF(L_55, NULL);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_57 = ___0_dracoMesh;
		NullCheck(L_57);
		int32_t L_58 = L_57->___numVertices_1;
		int32_t L_59 = V_6;
		UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177(L_53, L_56, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_58, L_59))), NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(_stringLiteral2DC2AAA56C78AE0B1DDA1048580586A194ED4217, NULL);
		DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D((DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_11)), NULL);
		DracoMeshLoader_ReleaseDracoAttribute_m567DB1E14B44341C552B2591AA3507E8016745C2((DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
	}

IL_0161:
	{
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_60 = ___0_dracoMesh;
		bool L_61;
		L_61 = DracoMeshLoader_GetAttributeByType_mAA4A9B33E2E5845D5EB425D5313DF7EAC29BEA1F(L_60, 3, 0, (DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
		if (!L_61)
		{
			goto IL_01e3;
		}
	}
	{
		V_12 = (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A*)((uintptr_t)0);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_62 = ___0_dracoMesh;
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_63 = V_8;
		bool L_64;
		L_64 = DracoMeshLoader_GetAttributeData_mE86815972918FF808B0B27A5F03DEEF6ED977725(L_62, L_63, (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_12)), NULL);
		if (!L_64)
		{
			goto IL_01e3;
		}
	}
	{
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_65 = V_12;
		NullCheck(L_65);
		int32_t L_66 = L_65->___dataType_0;
		int32_t L_67;
		L_67 = DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42(__this, L_66, NULL);
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_68 = V_8;
		NullCheck(L_68);
		int32_t L_69 = L_68->___numComponents_2;
		V_6 = ((int32_t)il2cpp_codegen_multiply(L_67, L_69));
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_70 = ___0_dracoMesh;
		NullCheck(L_70);
		int32_t L_71 = L_70->___numVertices_1;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_72 = (Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA*)SZArrayNew(Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA_il2cpp_TypeInfo_var, (uint32_t)L_71);
		V_2 = L_72;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_73 = V_2;
		NullCheck(L_73);
		void* L_74;
		L_74 = il2cpp_codegen_unsafe_cast(((L_73)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_75 = V_12;
		NullCheck(L_75);
		intptr_t L_76 = L_75->___data_1;
		void* L_77;
		L_77 = IntPtr_op_Explicit_m2728CBA081E79B97DDCF1D4FAD77B309CA1E94BF(L_76, NULL);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_78 = ___0_dracoMesh;
		NullCheck(L_78);
		int32_t L_79 = L_78->___numVertices_1;
		int32_t L_80 = V_6;
		UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177(L_74, L_77, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_79, L_80))), NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(_stringLiteralBCE437B17DC0B5A3A9B46AB4F7BA19E115E0EF4C, NULL);
		DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D((DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_12)), NULL);
		DracoMeshLoader_ReleaseDracoAttribute_m567DB1E14B44341C552B2591AA3507E8016745C2((DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
	}

IL_01e3:
	{
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_81 = ___0_dracoMesh;
		bool L_82;
		L_82 = DracoMeshLoader_GetAttributeByType_mAA4A9B33E2E5845D5EB425D5313DF7EAC29BEA1F(L_81, 2, 0, (DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
		if (!L_82)
		{
			goto IL_0267;
		}
	}
	{
		V_13 = (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A*)((uintptr_t)0);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_83 = ___0_dracoMesh;
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_84 = V_8;
		bool L_85;
		L_85 = DracoMeshLoader_GetAttributeData_mE86815972918FF808B0B27A5F03DEEF6ED977725(L_83, L_84, (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_13)), NULL);
		if (!L_85)
		{
			goto IL_0267;
		}
	}
	{
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_86 = V_13;
		NullCheck(L_86);
		int32_t L_87 = L_86->___dataType_0;
		int32_t L_88;
		L_88 = DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42(__this, L_87, NULL);
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_89 = V_8;
		NullCheck(L_89);
		int32_t L_90 = L_89->___numComponents_2;
		V_6 = ((int32_t)il2cpp_codegen_multiply(L_88, L_90));
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_91 = ___0_dracoMesh;
		NullCheck(L_91);
		int32_t L_92 = L_91->___numVertices_1;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_93 = (ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)SZArrayNew(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var, (uint32_t)L_92);
		V_4 = L_93;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_94 = V_4;
		NullCheck(L_94);
		void* L_95;
		L_95 = il2cpp_codegen_unsafe_cast(((L_94)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_96 = V_13;
		NullCheck(L_96);
		intptr_t L_97 = L_96->___data_1;
		void* L_98;
		L_98 = IntPtr_op_Explicit_m2728CBA081E79B97DDCF1D4FAD77B309CA1E94BF(L_97, NULL);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_99 = ___0_dracoMesh;
		NullCheck(L_99);
		int32_t L_100 = L_99->___numVertices_1;
		int32_t L_101 = V_6;
		UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177(L_95, L_98, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_100, L_101))), NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(_stringLiteral5A938D1BDDD7401E071E8EB937F4DD9D948C32AB, NULL);
		DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D((DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_13)), NULL);
		DracoMeshLoader_ReleaseDracoAttribute_m567DB1E14B44341C552B2591AA3507E8016745C2((DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
	}

IL_0267:
	{
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_102 = ___0_dracoMesh;
		bool L_103;
		L_103 = DracoMeshLoader_GetAttributeByType_mAA4A9B33E2E5845D5EB425D5313DF7EAC29BEA1F(L_102, 4, 0, (DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
		if (!L_103)
		{
			goto IL_02ea;
		}
	}
	{
		V_14 = (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A*)((uintptr_t)0);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_104 = ___0_dracoMesh;
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_105 = V_8;
		bool L_106;
		L_106 = DracoMeshLoader_GetAttributeData_mE86815972918FF808B0B27A5F03DEEF6ED977725(L_104, L_105, (DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_14)), NULL);
		if (!L_106)
		{
			goto IL_02ea;
		}
	}
	{
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_107 = V_14;
		NullCheck(L_107);
		int32_t L_108 = L_107->___dataType_0;
		int32_t L_109;
		L_109 = DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42(__this, L_108, NULL);
		DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7* L_110 = V_8;
		NullCheck(L_110);
		int32_t L_111 = L_110->___numComponents_2;
		V_6 = ((int32_t)il2cpp_codegen_multiply(L_109, L_111));
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_112 = ___0_dracoMesh;
		NullCheck(L_112);
		int32_t L_113 = L_112->___numVertices_1;
		int32_t L_114 = V_6;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_115 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply(L_113, L_114)));
		NullCheck(L_115);
		void* L_116;
		L_116 = il2cpp_codegen_unsafe_cast(((L_115)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A* L_117 = V_14;
		NullCheck(L_117);
		intptr_t L_118 = L_117->___data_1;
		void* L_119;
		L_119 = IntPtr_op_Explicit_m2728CBA081E79B97DDCF1D4FAD77B309CA1E94BF(L_118, NULL);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_120 = ___0_dracoMesh;
		NullCheck(L_120);
		int32_t L_121 = L_120->___numVertices_1;
		int32_t L_122 = V_6;
		UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177(L_116, L_119, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_121, L_122))), NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(_stringLiteral5C3DEFA23303BF83C71BBC5797AC9DCCE832ACB8, NULL);
		DracoMeshLoader_ReleaseDracoData_m8CBF5A2C71E55C5106C7BE4CD62C52F5DC36E81D((DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A**)((uintptr_t)(&V_14)), NULL);
		DracoMeshLoader_ReleaseDracoAttribute_m567DB1E14B44341C552B2591AA3507E8016745C2((DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7**)((uintptr_t)(&V_8)), NULL);
	}

IL_02ea:
	{
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* L_123 = (GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83*)il2cpp_codegen_object_new(GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83_il2cpp_TypeInfo_var);
		NullCheck(L_123);
		GltfTempGeometryGroup__ctor_mD9970225FF191449FFFDB27DC5055178B23C2B56(L_123, NULL);
		V_10 = L_123;
		V_15 = 0;
		goto IL_0319;
	}

IL_02f6:
	{
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_124 = V_1;
		int32_t L_125 = V_15;
		NullCheck(L_124);
		int32_t L_126 = L_125;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_127 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126));
		V_16 = L_127;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_128 = V_16;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_129;
		L_129 = RightHandToLeftHandConverter_ConvertVector_m0E9E683F500FBA16B79BB686BBBC8E1CE566DA8B(L_128, NULL);
		V_16 = L_129;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_130 = V_1;
		int32_t L_131 = V_15;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_132 = V_16;
		NullCheck(L_130);
		(L_130)->SetAt(static_cast<il2cpp_array_size_t>(L_131), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_132);
		int32_t L_133 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_133, 1));
	}

IL_0319:
	{
		int32_t L_134 = V_15;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_135 = V_1;
		NullCheck(L_135);
		if ((((int32_t)L_134) < ((int32_t)((int32_t)(((RuntimeArray*)L_135)->max_length)))))
		{
			goto IL_02f6;
		}
	}
	{
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* L_136 = V_10;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_137 = V_1;
		NullCheck(L_136);
		L_136->___Vertices_0 = (RuntimeObject*)L_137;
		Il2CppCodeGenWriteBarrier((void**)(&L_136->___Vertices_0), (void*)(RuntimeObject*)L_137);
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_138 = V_0;
		Array_Reverse_m464993603E0F56B4A68F70113212032FE7381B6C((RuntimeArray*)L_138, NULL);
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* L_139 = V_10;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_140 = V_0;
		NullCheck(L_139);
		L_139->___IndicesList_4 = (RuntimeObject*)L_140;
		Il2CppCodeGenWriteBarrier((void**)(&L_139->___IndicesList_4), (void*)(RuntimeObject*)L_140);
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_141 = V_2;
		if (!L_141)
		{
			goto IL_037b;
		}
	}
	{
		V_17 = 0;
		goto IL_036c;
	}

IL_033e:
	{
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_142 = V_2;
		int32_t L_143 = V_17;
		NullCheck(L_142);
		int32_t L_144 = L_143;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_145 = (L_142)->GetAt(static_cast<il2cpp_array_size_t>(L_144));
		V_18 = L_145;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_146 = V_18;
		float L_147 = L_146.___y_1;
		(&V_18)->___y_1 = ((float)il2cpp_codegen_subtract((1.0f), L_147));
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_148 = V_2;
		int32_t L_149 = V_17;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_150 = V_18;
		NullCheck(L_148);
		(L_148)->SetAt(static_cast<il2cpp_array_size_t>(L_149), (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7)L_150);
		int32_t L_151 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_151, 1));
	}

IL_036c:
	{
		int32_t L_152 = V_17;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_153 = V_2;
		NullCheck(L_153);
		if ((((int32_t)L_152) < ((int32_t)((int32_t)(((RuntimeArray*)L_153)->max_length)))))
		{
			goto IL_033e;
		}
	}
	{
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* L_154 = V_10;
		Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* L_155 = V_2;
		NullCheck(L_154);
		L_154->___UVsList_2 = (RuntimeObject*)L_155;
		Il2CppCodeGenWriteBarrier((void**)(&L_154->___UVsList_2), (void*)(RuntimeObject*)L_155);
	}

IL_037b:
	{
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_156 = V_3;
		if (!L_156)
		{
			goto IL_03b5;
		}
	}
	{
		V_19 = 0;
		goto IL_03a6;
	}

IL_0383:
	{
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_157 = V_3;
		int32_t L_158 = V_19;
		NullCheck(L_157);
		int32_t L_159 = L_158;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_160 = (L_157)->GetAt(static_cast<il2cpp_array_size_t>(L_159));
		V_20 = L_160;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_161 = V_20;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_162;
		L_162 = RightHandToLeftHandConverter_ConvertVector_m0E9E683F500FBA16B79BB686BBBC8E1CE566DA8B(L_161, NULL);
		V_20 = L_162;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_163 = V_3;
		int32_t L_164 = V_19;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_165 = V_20;
		NullCheck(L_163);
		(L_163)->SetAt(static_cast<il2cpp_array_size_t>(L_164), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_165);
		int32_t L_166 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add(L_166, 1));
	}

IL_03a6:
	{
		int32_t L_167 = V_19;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_168 = V_3;
		NullCheck(L_168);
		if ((((int32_t)L_167) < ((int32_t)((int32_t)(((RuntimeArray*)L_168)->max_length)))))
		{
			goto IL_0383;
		}
	}
	{
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* L_169 = V_10;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_170 = V_3;
		NullCheck(L_169);
		L_169->___NormalsList_1 = (RuntimeObject*)L_170;
		Il2CppCodeGenWriteBarrier((void**)(&L_169->___NormalsList_1), (void*)(RuntimeObject*)L_170);
	}

IL_03b5:
	{
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_171 = V_4;
		if (!L_171)
		{
			goto IL_03c2;
		}
	}
	{
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* L_172 = V_10;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_173 = V_4;
		NullCheck(L_172);
		L_172->___ColorsList_3 = (RuntimeObject*)L_173;
		Il2CppCodeGenWriteBarrier((void**)(&L_172->___ColorsList_3), (void*)(RuntimeObject*)L_173);
	}

IL_03c2:
	{
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* L_174 = V_10;
		return L_174;
	}
}
// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader::DataTypeSize(TriLibCore.Gltf.Draco.DracoMeshLoader/DataType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DracoMeshLoader_DataTypeSize_mEC9C9E417489C93A355BB2C1D775C5D393D45C42 (DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF* __this, int32_t ___0_dt, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_dt;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_0, 1)))
		{
			case 0:
			{
				goto IL_0036;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_0038;
			}
			case 3:
			{
				goto IL_0038;
			}
			case 4:
			{
				goto IL_003a;
			}
			case 5:
			{
				goto IL_003a;
			}
			case 6:
			{
				goto IL_003c;
			}
			case 7:
			{
				goto IL_003c;
			}
			case 8:
			{
				goto IL_003e;
			}
			case 9:
			{
				goto IL_0040;
			}
			case 10:
			{
				goto IL_0042;
			}
		}
	}
	{
		goto IL_0044;
	}

IL_0036:
	{
		return 1;
	}

IL_0038:
	{
		return 2;
	}

IL_003a:
	{
		return 4;
	}

IL_003c:
	{
		return 8;
	}

IL_003e:
	{
		return 4;
	}

IL_0040:
	{
		return 8;
	}

IL_0042:
	{
		return 1;
	}

IL_0044:
	{
		return (-1);
	}
}
// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader::ConvertDracoMeshToGltfGeometryGroup(System.Byte[],TriLibCore.Gltf.GltfTempGeometryGroup&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DracoMeshLoader_ConvertDracoMeshToGltfGeometryGroup_m3FB7E227DCB4224E60D5AB3472D706DDDDC1A543 (DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_encodedData, GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83** ___1_geometryGroup, const RuntimeMethod* method) 
{
	DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* V_0 = NULL;
	{
		V_0 = (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED*)((uintptr_t)0);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_encodedData;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = ___0_encodedData;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = DracoMeshLoader_DecodeDracoMesh_m784E16569E6411367F94E2A3B4209C96F9B1FCD4(L_0, ((int32_t)(((RuntimeArray*)L_1)->max_length)), (DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED**)((uintptr_t)(&V_0)), NULL);
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83** L_3 = ___1_geometryGroup;
		*((RuntimeObject**)L_3) = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_3, (void*)(RuntimeObject*)NULL);
		return (-1);
	}

IL_0017:
	{
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83** L_4 = ___1_geometryGroup;
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_5 = V_0;
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* L_6;
		L_6 = DracoMeshLoader_CreateUnityMesh_m831B6AC86DF643827205BF92DFF1AA89590E6BFC(__this, L_5, NULL);
		*((RuntimeObject**)L_4) = (RuntimeObject*)L_6;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_4, (void*)(RuntimeObject*)L_6);
		DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->___numFaces_0;
		DracoMeshLoader_ReleaseDracoMesh_m1D28A1C2FD9697F6519CCC8A46B814C4412EF720((DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED**)((uintptr_t)(&V_0)), NULL);
		return L_8;
	}
}
// TriLibCore.Gltf.GltfTempGeometryGroup TriLibCore.Gltf.Draco.DracoMeshLoader::DracoDecompressorCallback(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* DracoMeshLoader_DracoDecompressorCallback_mF8DECDD739C6273FBC1CAE78688EDAC1C31F93DA (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_encodedData, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* V_0 = NULL;
	{
		DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF* L_0 = (DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF*)il2cpp_codegen_object_new(DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		DracoMeshLoader__ctor_mAC5A9167F1E64EE1E75675021B80DAC498B7CA58(L_0, NULL);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = ___0_encodedData;
		NullCheck(L_0);
		int32_t L_2;
		L_2 = DracoMeshLoader_ConvertDracoMeshToGltfGeometryGroup_m3FB7E227DCB4224E60D5AB3472D706DDDDC1A543(L_0, L_1, (&V_0), NULL);
		GltfTempGeometryGroup_t2E188D1244BBD14E868F7884D7744770ED0EDA83* L_3 = V_0;
		return L_3;
	}
}
// System.Void TriLibCore.Gltf.Draco.DracoMeshLoader::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DracoMeshLoader__ctor_mAC5A9167F1E64EE1E75675021B80DAC498B7CA58 (DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
