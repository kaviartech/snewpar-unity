﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void SocketIOClient.OnAnyHandler::.ctor(System.Object,System.IntPtr)
extern void OnAnyHandler__ctor_m006530A35A0B24D79F28848298BE88A309627026 (void);
// 0x00000002 System.Void SocketIOClient.OnAnyHandler::Invoke(System.String,SocketIOClient.SocketIOResponse)
extern void OnAnyHandler_Invoke_m66BB3729C336ACA085AE7C98FD697B5087640B65 (void);
// 0x00000003 System.IAsyncResult SocketIOClient.OnAnyHandler::BeginInvoke(System.String,SocketIOClient.SocketIOResponse,System.AsyncCallback,System.Object)
extern void OnAnyHandler_BeginInvoke_m70E7A811F81E72E35BAEDBAA60515A48E97CB844 (void);
// 0x00000004 System.Void SocketIOClient.OnAnyHandler::EndInvoke(System.IAsyncResult)
extern void OnAnyHandler_EndInvoke_m0F13AF6A59199DC06A3417442C5D3793725DF970 (void);
// 0x00000005 System.Void SocketIOClient.OnOpenedHandler::.ctor(System.Object,System.IntPtr)
extern void OnOpenedHandler__ctor_mA4BF313175A5882D89EC3C6E39190A0417C36ABF (void);
// 0x00000006 System.Void SocketIOClient.OnOpenedHandler::Invoke(System.String,System.Int32,System.Int32)
extern void OnOpenedHandler_Invoke_m47EC1CE4CEF7CE9D70690754002EA5018FB574BB (void);
// 0x00000007 System.IAsyncResult SocketIOClient.OnOpenedHandler::BeginInvoke(System.String,System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void OnOpenedHandler_BeginInvoke_m16C0E4162D82F3A22B3E27F5EA8CA66E3581A8ED (void);
// 0x00000008 System.Void SocketIOClient.OnOpenedHandler::EndInvoke(System.IAsyncResult)
extern void OnOpenedHandler_EndInvoke_mF09080EAD1467FB25D2A48A5632E2C2AAFC9F049 (void);
// 0x00000009 System.Void SocketIOClient.OnAck::.ctor(System.Object,System.IntPtr)
extern void OnAck__ctor_m62D1F7214D6D30DF39F4EB6C602C1A338AFD6559 (void);
// 0x0000000A System.Void SocketIOClient.OnAck::Invoke(System.Int32,System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void OnAck_Invoke_m64F71431CB334746FBA7F93BE59F6104EE5F04A5 (void);
// 0x0000000B System.IAsyncResult SocketIOClient.OnAck::BeginInvoke(System.Int32,System.Collections.Generic.List`1<System.Text.Json.JsonElement>,System.AsyncCallback,System.Object)
extern void OnAck_BeginInvoke_m0EBAEB0FE7940A7E0309D563D95544923472A474 (void);
// 0x0000000C System.Void SocketIOClient.OnAck::EndInvoke(System.IAsyncResult)
extern void OnAck_EndInvoke_mA135289B372E98A88A2876E15F9E3BB9D9BDFE26 (void);
// 0x0000000D System.Void SocketIOClient.OnBinaryAck::.ctor(System.Object,System.IntPtr)
extern void OnBinaryAck__ctor_mCEB13E387F317C3D33F1E4E43C96ABADD7AEC1AE (void);
// 0x0000000E System.Void SocketIOClient.OnBinaryAck::Invoke(System.Int32,System.Int32,System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void OnBinaryAck_Invoke_m0206A595715D3F4876717B984DCCFC3699F8F491 (void);
// 0x0000000F System.IAsyncResult SocketIOClient.OnBinaryAck::BeginInvoke(System.Int32,System.Int32,System.Collections.Generic.List`1<System.Text.Json.JsonElement>,System.AsyncCallback,System.Object)
extern void OnBinaryAck_BeginInvoke_m55E7598015620C39E28AB86549FE630838050900 (void);
// 0x00000010 System.Void SocketIOClient.OnBinaryAck::EndInvoke(System.IAsyncResult)
extern void OnBinaryAck_EndInvoke_m2F58E81655D872E9806CBB98481661E80D4D298E (void);
// 0x00000011 System.Void SocketIOClient.OnBinaryReceived::.ctor(System.Object,System.IntPtr)
extern void OnBinaryReceived__ctor_mE41839448521ED17278FEDCB4D7A2BAA26D804F6 (void);
// 0x00000012 System.Void SocketIOClient.OnBinaryReceived::Invoke(System.Int32,System.Int32,System.String,System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void OnBinaryReceived_Invoke_mEB2B314E85AEA8DF888512A103F747B6F23C43AA (void);
// 0x00000013 System.IAsyncResult SocketIOClient.OnBinaryReceived::BeginInvoke(System.Int32,System.Int32,System.String,System.Collections.Generic.List`1<System.Text.Json.JsonElement>,System.AsyncCallback,System.Object)
extern void OnBinaryReceived_BeginInvoke_m6B1848D85F526D872599EE5AADD2DF266CA14585 (void);
// 0x00000014 System.Void SocketIOClient.OnBinaryReceived::EndInvoke(System.IAsyncResult)
extern void OnBinaryReceived_EndInvoke_mB0B9E1CB3C2C2BDC5AABD1F83BC53230175A26EE (void);
// 0x00000015 System.Void SocketIOClient.OnDisconnected::.ctor(System.Object,System.IntPtr)
extern void OnDisconnected__ctor_mA111916B7799F340A6D19B5DA4C261D1E19E300F (void);
// 0x00000016 System.Void SocketIOClient.OnDisconnected::Invoke()
extern void OnDisconnected_Invoke_m91E9D1ABD233056CFFF885C372BC2021DD2D29AD (void);
// 0x00000017 System.IAsyncResult SocketIOClient.OnDisconnected::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDisconnected_BeginInvoke_m6EB042A373C70D032CD7167DCE860712F9FB503A (void);
// 0x00000018 System.Void SocketIOClient.OnDisconnected::EndInvoke(System.IAsyncResult)
extern void OnDisconnected_EndInvoke_m107E09DFB27BDDA19AB8335DDFBE005F15E776A2 (void);
// 0x00000019 System.Void SocketIOClient.OnError::.ctor(System.Object,System.IntPtr)
extern void OnError__ctor_m42A8A9BA4685F1DD8D4AD64B40B6588F9F019A1C (void);
// 0x0000001A System.Void SocketIOClient.OnError::Invoke(System.String)
extern void OnError_Invoke_m305EE33A7FBDAED9C7281128418CF3B42ED51F70 (void);
// 0x0000001B System.IAsyncResult SocketIOClient.OnError::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void OnError_BeginInvoke_m0C3E903FF7B9E0D49FFACF13D1A3013629D50E1F (void);
// 0x0000001C System.Void SocketIOClient.OnError::EndInvoke(System.IAsyncResult)
extern void OnError_EndInvoke_m44CD4C92C247556D28793B42469BEDA52CA739CF (void);
// 0x0000001D System.Void SocketIOClient.OnEventReceived::.ctor(System.Object,System.IntPtr)
extern void OnEventReceived__ctor_m6F1D0FAD0D2A51C40808CA2095708924F0850334 (void);
// 0x0000001E System.Void SocketIOClient.OnEventReceived::Invoke(System.Int32,System.String,System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void OnEventReceived_Invoke_mBB250FDCA59BB90CF5720FACBB5528F73FC481C0 (void);
// 0x0000001F System.IAsyncResult SocketIOClient.OnEventReceived::BeginInvoke(System.Int32,System.String,System.Collections.Generic.List`1<System.Text.Json.JsonElement>,System.AsyncCallback,System.Object)
extern void OnEventReceived_BeginInvoke_m5CB205213420F394F49C9600FA83547E5C9B07E8 (void);
// 0x00000020 System.Void SocketIOClient.OnEventReceived::EndInvoke(System.IAsyncResult)
extern void OnEventReceived_EndInvoke_m66A58F344876BFEF6937CBBB6D03EDCF12F18CF0 (void);
// 0x00000021 System.Void SocketIOClient.OnOpened::.ctor(System.Object,System.IntPtr)
extern void OnOpened__ctor_mA6257A95CBEE96ADDCE4576E82C8EBF70C83EC62 (void);
// 0x00000022 System.Void SocketIOClient.OnOpened::Invoke(System.String,System.Int32,System.Int32)
extern void OnOpened_Invoke_m86DDE326D4E119A85DB9917AE89C48C6E254D093 (void);
// 0x00000023 System.IAsyncResult SocketIOClient.OnOpened::BeginInvoke(System.String,System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void OnOpened_BeginInvoke_mB81DB59A775781F4E595AEBF30CFEF8F9DA0ED1B (void);
// 0x00000024 System.Void SocketIOClient.OnOpened::EndInvoke(System.IAsyncResult)
extern void OnOpened_EndInvoke_m0C9A21406FC7E55C5070610D8DD425FC49905C3A (void);
// 0x00000025 System.Void SocketIOClient.OnPing::.ctor(System.Object,System.IntPtr)
extern void OnPing__ctor_mD3C5DB5FA44BF86B6ECBA934D20808CF27185A4C (void);
// 0x00000026 System.Void SocketIOClient.OnPing::Invoke()
extern void OnPing_Invoke_mE88346B0642CAEBC48A42828744684B12D5880C9 (void);
// 0x00000027 System.IAsyncResult SocketIOClient.OnPing::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnPing_BeginInvoke_m479341279E883BC7BD6F97A6DCBC3FB556BBF638 (void);
// 0x00000028 System.Void SocketIOClient.OnPing::EndInvoke(System.IAsyncResult)
extern void OnPing_EndInvoke_mACF497902344E98501F2B3C03AC3026EE766D59B (void);
// 0x00000029 System.Void SocketIOClient.OnPong::.ctor(System.Object,System.IntPtr)
extern void OnPong__ctor_mE1373FCF7D02686BB62317F72920ED1104384BAE (void);
// 0x0000002A System.Void SocketIOClient.OnPong::Invoke()
extern void OnPong_Invoke_mCCDAA3313FB8904767CC436745C117E57F27F0B5 (void);
// 0x0000002B System.IAsyncResult SocketIOClient.OnPong::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnPong_BeginInvoke_mDC1AF8EBD3E0238320F50274F063A05934766FF2 (void);
// 0x0000002C System.Void SocketIOClient.OnPong::EndInvoke(System.IAsyncResult)
extern void OnPong_EndInvoke_m4F5DE92660B85981BB60F7728DDB31640A695785 (void);
// 0x0000002D System.Void SocketIOClient.LowLevelEvent::.ctor()
extern void LowLevelEvent__ctor_m848A81195E756F82ECA25524571553ADAE8F77F1 (void);
// 0x0000002E System.Int32 SocketIOClient.LowLevelEvent::get_PacketId()
extern void LowLevelEvent_get_PacketId_m2DB32A0587A1A7950B4253BF3342D47A85DF9DA0 (void);
// 0x0000002F System.Void SocketIOClient.LowLevelEvent::set_PacketId(System.Int32)
extern void LowLevelEvent_set_PacketId_mA4B611F54BEE19B5D65593FD75E85E72A72C5C31 (void);
// 0x00000030 System.String SocketIOClient.LowLevelEvent::get_Event()
extern void LowLevelEvent_get_Event_m46901F9A2212DC0A6522C5118C3681492119CB06 (void);
// 0x00000031 System.Void SocketIOClient.LowLevelEvent::set_Event(System.String)
extern void LowLevelEvent_set_Event_m94E56D725D55A9F726AD975441D928487C5736DD (void);
// 0x00000032 System.Int32 SocketIOClient.LowLevelEvent::get_Count()
extern void LowLevelEvent_get_Count_m2A6FA153C22F0CC513E3CCFA96DC9517A2B41B9E (void);
// 0x00000033 System.Void SocketIOClient.LowLevelEvent::set_Count(System.Int32)
extern void LowLevelEvent_set_Count_m920B4DD2D447B9FD77E0446BB4EED486071F4923 (void);
// 0x00000034 SocketIOClient.SocketIOResponse SocketIOClient.LowLevelEvent::get_Response()
extern void LowLevelEvent_get_Response_mC0BED39B2BE0E38D009C67CCAE2ACFE1D950891F (void);
// 0x00000035 System.Void SocketIOClient.LowLevelEvent::set_Response(SocketIOClient.SocketIOResponse)
extern void LowLevelEvent_set_Response_m0E31CD2E90B6140E8C520F27C434F5B8448FE775 (void);
// 0x00000036 System.Void SocketIOClient.SocketIO::.ctor(System.String)
extern void SocketIO__ctor_mAAA87CF36CAA39F99A889CD98BAEA0688DA2EACF (void);
// 0x00000037 System.Void SocketIOClient.SocketIO::.ctor(System.Uri)
extern void SocketIO__ctor_mBEC367EDEC42A2621370BE76C0391811F46EBC74 (void);
// 0x00000038 System.Void SocketIOClient.SocketIO::.ctor(System.String,SocketIOClient.SocketIOOptions)
extern void SocketIO__ctor_mC23516A8CD30540E46DB36C2622CAF253AA89E65 (void);
// 0x00000039 System.Void SocketIOClient.SocketIO::.ctor(System.Uri,SocketIOClient.SocketIOOptions)
extern void SocketIO__ctor_mAC0EFAF950869BC7D4CCB08E0AF6FBB6E1E2C6A5 (void);
// 0x0000003A System.Void SocketIOClient.SocketIO::.ctor()
extern void SocketIO__ctor_m622409BFD9E36D54CD36230FF6201DCB22993F69 (void);
// 0x0000003B System.Uri SocketIOClient.SocketIO::get_ServerUri()
extern void SocketIO_get_ServerUri_m38FDCEEE84C3E2AFA3CE3F76BC3297FF1EFED8E0 (void);
// 0x0000003C System.Void SocketIOClient.SocketIO::set_ServerUri(System.Uri)
extern void SocketIO_set_ServerUri_m41BE3B745754D77C2A539C831F42067C62D2D0A8 (void);
// 0x0000003D SocketIOClient.UrlConverter SocketIOClient.SocketIO::get_UrlConverter()
extern void SocketIO_get_UrlConverter_mB9363AA9DB3591DF98CC866896BFADA2BB5CD556 (void);
// 0x0000003E System.Void SocketIOClient.SocketIO::set_UrlConverter(SocketIOClient.UrlConverter)
extern void SocketIO_set_UrlConverter_m087BED398453E98F240B5AA1E201F077CC3E75BD (void);
// 0x0000003F SocketIOClient.WebSocketClient.IWebSocketClient SocketIOClient.SocketIO::get_Socket()
extern void SocketIO_get_Socket_m7FC7ABF2CE1F9993E6E18AB31FE29DD1DDE1B407 (void);
// 0x00000040 System.Void SocketIOClient.SocketIO::set_Socket(SocketIOClient.WebSocketClient.IWebSocketClient)
extern void SocketIO_set_Socket_m9FE4712FBC1A5100818D88F641F17D5BA4A05334 (void);
// 0x00000041 SocketIOClient.Processors.Processor SocketIOClient.SocketIO::get_MessageProcessor()
extern void SocketIO_get_MessageProcessor_m437A4557CBAD44844B079FB31A989F2E0A9E0B6C (void);
// 0x00000042 System.Void SocketIOClient.SocketIO::set_MessageProcessor(SocketIOClient.Processors.Processor)
extern void SocketIO_set_MessageProcessor_m96BCD337497E42D632F91D7F472B84F5AAA50E96 (void);
// 0x00000043 System.String SocketIOClient.SocketIO::get_Id()
extern void SocketIO_get_Id_m96D67B40C9CE00A1B252091917360C3339FEBB20 (void);
// 0x00000044 System.Void SocketIOClient.SocketIO::set_Id(System.String)
extern void SocketIO_set_Id_mD0DB47E4F716FB18F7687E9945480528BC8AC624 (void);
// 0x00000045 System.String SocketIOClient.SocketIO::get_Namespace()
extern void SocketIO_get_Namespace_m948F5002C7043B7F3363671E09C6B465A20BEAD9 (void);
// 0x00000046 System.Void SocketIOClient.SocketIO::set_Namespace(System.String)
extern void SocketIO_set_Namespace_m43D39B2023AE1CC98B396850E9BC9D9954835A3E (void);
// 0x00000047 System.Boolean SocketIOClient.SocketIO::get_Connected()
extern void SocketIO_get_Connected_m96098A9D67228D3C6DC90FC4DE28D405D32AB637 (void);
// 0x00000048 System.Void SocketIOClient.SocketIO::set_Connected(System.Boolean)
extern void SocketIO_set_Connected_m10B84B1D4B153A6159B8040A5010423D3D4AB5AE (void);
// 0x00000049 System.Int32 SocketIOClient.SocketIO::get_Attempts()
extern void SocketIO_get_Attempts_m9477FCEFA9819A91A3BB977A2F24B6D9A6BD5C7C (void);
// 0x0000004A System.Void SocketIOClient.SocketIO::set_Attempts(System.Int32)
extern void SocketIO_set_Attempts_m976DCA02C277679FECC3D23E4013539918BCE34E (void);
// 0x0000004B System.Boolean SocketIOClient.SocketIO::get_Disconnected()
extern void SocketIO_get_Disconnected_m36A50A318CDEAC3B022837E3F34479D04364955F (void);
// 0x0000004C System.Void SocketIOClient.SocketIO::set_Disconnected(System.Boolean)
extern void SocketIO_set_Disconnected_m39919C7AED5C2100861DFF3A18E79983C6969DDC (void);
// 0x0000004D SocketIOClient.SocketIOOptions SocketIOClient.SocketIO::get_Options()
extern void SocketIO_get_Options_mA96C1CC131085E59B69FEB406BD8B71D4FC6B724 (void);
// 0x0000004E SocketIOClient.JsonSerializer.IJsonSerializer SocketIOClient.SocketIO::get_JsonSerializer()
extern void SocketIO_get_JsonSerializer_m90E64B118772E0CE3D11F565E1DB4B6512ED15B2 (void);
// 0x0000004F System.Void SocketIOClient.SocketIO::set_JsonSerializer(SocketIOClient.JsonSerializer.IJsonSerializer)
extern void SocketIO_set_JsonSerializer_mA016B71F14953C0D2C55850F1667165338C9BD41 (void);
// 0x00000050 System.Void SocketIOClient.SocketIO::add_OnConnected(System.EventHandler)
extern void SocketIO_add_OnConnected_m9D658BABFE49672A000ABB70B215ED9827C73C79 (void);
// 0x00000051 System.Void SocketIOClient.SocketIO::remove_OnConnected(System.EventHandler)
extern void SocketIO_remove_OnConnected_mFF8EA7BD1F0FBF1FEED9B354D9D8963E7780DA3E (void);
// 0x00000052 System.Void SocketIOClient.SocketIO::add_OnError(System.EventHandler`1<System.String>)
extern void SocketIO_add_OnError_mED5F759501E6C56FE3C59199450F3A7BC3BB2ED5 (void);
// 0x00000053 System.Void SocketIOClient.SocketIO::remove_OnError(System.EventHandler`1<System.String>)
extern void SocketIO_remove_OnError_m4B5E1E431F80954D284FFB5E81385FB78F2BF46E (void);
// 0x00000054 System.Void SocketIOClient.SocketIO::add_OnDisconnected(System.EventHandler`1<System.String>)
extern void SocketIO_add_OnDisconnected_mEFB3A61F0C996ADC9D3EC57FF748EB4B1A64A9B2 (void);
// 0x00000055 System.Void SocketIOClient.SocketIO::remove_OnDisconnected(System.EventHandler`1<System.String>)
extern void SocketIO_remove_OnDisconnected_mCB0605263C79308B0F29EFE804E75C3BD4ABA375 (void);
// 0x00000056 System.Void SocketIOClient.SocketIO::add_OnReconnected(System.EventHandler`1<System.Int32>)
extern void SocketIO_add_OnReconnected_mDCDCDDE123A5950E636CB041AE7382B50803497E (void);
// 0x00000057 System.Void SocketIOClient.SocketIO::remove_OnReconnected(System.EventHandler`1<System.Int32>)
extern void SocketIO_remove_OnReconnected_mA53587D5D525ED2BDBA5F65B8355EDEE79BDCC68 (void);
// 0x00000058 System.Void SocketIOClient.SocketIO::add_OnReconnectAttempt(System.EventHandler`1<System.Int32>)
extern void SocketIO_add_OnReconnectAttempt_m372A2FB7EEB3BCE097BDB69DFB2C51544BFCB60E (void);
// 0x00000059 System.Void SocketIOClient.SocketIO::remove_OnReconnectAttempt(System.EventHandler`1<System.Int32>)
extern void SocketIO_remove_OnReconnectAttempt_m2BE91C7002382E9A4E17E8A17F61F6EB1DE8D375 (void);
// 0x0000005A System.Void SocketIOClient.SocketIO::add_OnReconnectError(System.EventHandler`1<System.Exception>)
extern void SocketIO_add_OnReconnectError_mAF4C3C0EE5B24159136E224C9FA3C03D13F90F81 (void);
// 0x0000005B System.Void SocketIOClient.SocketIO::remove_OnReconnectError(System.EventHandler`1<System.Exception>)
extern void SocketIO_remove_OnReconnectError_mAE1A10D96D5CFB73822421EF0D60462595ADB0C3 (void);
// 0x0000005C System.Void SocketIOClient.SocketIO::add_OnReconnectFailed(System.EventHandler)
extern void SocketIO_add_OnReconnectFailed_mD69A78E5D73C10E184C1A7D6CF3E42FA00523DCE (void);
// 0x0000005D System.Void SocketIOClient.SocketIO::remove_OnReconnectFailed(System.EventHandler)
extern void SocketIO_remove_OnReconnectFailed_m79ADBA036C81273FF3B009FE661E61CFEFF7D667 (void);
// 0x0000005E System.Void SocketIOClient.SocketIO::add_OnPing(System.EventHandler)
extern void SocketIO_add_OnPing_mB304839625FE70CCC198F4114443FFEEED3C55F5 (void);
// 0x0000005F System.Void SocketIOClient.SocketIO::remove_OnPing(System.EventHandler)
extern void SocketIO_remove_OnPing_m2C52C490C812328904B019B7FD4973E68E2C6009 (void);
// 0x00000060 System.Void SocketIOClient.SocketIO::add_OnPong(System.EventHandler`1<System.TimeSpan>)
extern void SocketIO_add_OnPong_m63722B27C2CE9A0445866A56836FF8D1E7AF8A83 (void);
// 0x00000061 System.Void SocketIOClient.SocketIO::remove_OnPong(System.EventHandler`1<System.TimeSpan>)
extern void SocketIO_remove_OnPong_m3ECD7031F62DC3710AFE23B8694FD3896D89F8A9 (void);
// 0x00000062 System.Void SocketIOClient.SocketIO::Initialize()
extern void SocketIO_Initialize_mAFD7A258BD6120302E2D2E2CF4A24AC9D2DC7827 (void);
// 0x00000063 System.Threading.Tasks.Task SocketIOClient.SocketIO::ConnectAsync()
extern void SocketIO_ConnectAsync_m30D6A30EDEF8DAA3AD16DC0477C480EF6C94522E (void);
// 0x00000064 System.Threading.Tasks.Task SocketIOClient.SocketIO::DisconnectAsync()
extern void SocketIO_DisconnectAsync_m30DFB925AFB88E6A80459BC22F2A430C3ABB5757 (void);
// 0x00000065 System.Void SocketIOClient.SocketIO::On(System.String,System.Action`1<SocketIOClient.SocketIOResponse>)
extern void SocketIO_On_mFE240648DFCCE0AF026CE1B79D426685FD01B218 (void);
// 0x00000066 System.Void SocketIOClient.SocketIO::Off(System.String)
extern void SocketIO_Off_m53BB39BB64ACBE002C0A7C9ED8492889893B13A9 (void);
// 0x00000067 System.Void SocketIOClient.SocketIO::OnAny(SocketIOClient.OnAnyHandler)
extern void SocketIO_OnAny_m7E7DB74C6CBEBD9A7675F3D45E8BC5B7773EEEF9 (void);
// 0x00000068 System.Void SocketIOClient.SocketIO::PrependAny(SocketIOClient.OnAnyHandler)
extern void SocketIO_PrependAny_mEEDE07FB210BEBF3F84E52AC0866A0B4224CDF9A (void);
// 0x00000069 System.Void SocketIOClient.SocketIO::OffAny(SocketIOClient.OnAnyHandler)
extern void SocketIO_OffAny_mCA42D44F885991BBC16A1CBAE5ABFB61704DE639 (void);
// 0x0000006A SocketIOClient.OnAnyHandler[] SocketIOClient.SocketIO::ListenersAny()
extern void SocketIO_ListenersAny_mEFE8C31C148CB8C7CFD76DB54CC91B2F984FD701 (void);
// 0x0000006B System.Threading.Tasks.Task SocketIOClient.SocketIO::EmityCoreAsync(System.String,System.Int32,System.String,System.Threading.CancellationToken)
extern void SocketIO_EmityCoreAsync_m6DB2301FD1B18DEAAE1DADD64237807C32DED6D0 (void);
// 0x0000006C System.Threading.Tasks.Task SocketIOClient.SocketIO::EmitCallbackAsync(System.Int32,System.Object[])
extern void SocketIO_EmitCallbackAsync_m662757301A057723FE2C9C71743B630CBC6F4925 (void);
// 0x0000006D System.String SocketIOClient.SocketIO::GetDataString(System.Object[])
extern void SocketIO_GetDataString_mCD5D1A56DD7F04234BAE60B6958AD1F1F02C1BC5 (void);
// 0x0000006E System.Threading.Tasks.Task SocketIOClient.SocketIO::EmitAsync(System.String,System.Object[])
extern void SocketIO_EmitAsync_m982F2923F633EDDD0AD9DA1AD7754E3BCC52E9FF (void);
// 0x0000006F System.Threading.Tasks.Task SocketIOClient.SocketIO::EmitAsync(System.String,System.Threading.CancellationToken,System.Object[])
extern void SocketIO_EmitAsync_m4B25FC0E4EF27138D79EFA3053DD4B9F276070C6 (void);
// 0x00000070 System.Threading.Tasks.Task SocketIOClient.SocketIO::EmitAsync(System.String,System.Action`1<SocketIOClient.SocketIOResponse>,System.Object[])
extern void SocketIO_EmitAsync_mBF5C618E1D4C0B494187C8A3139AAC2E6CC41CA9 (void);
// 0x00000071 System.Threading.Tasks.Task SocketIOClient.SocketIO::EmitAsync(System.String,System.Threading.CancellationToken,System.Action`1<SocketIOClient.SocketIOResponse>,System.Object[])
extern void SocketIO_EmitAsync_mD020CE38BF6170FFCBACFDE7C47C7087456A76FD (void);
// 0x00000072 System.Void SocketIOClient.SocketIO::OnTextReceived(System.String)
extern void SocketIO_OnTextReceived_m20CC6BE3956E34EA9C9789048C73B3D4BE4A2C73 (void);
// 0x00000073 System.Void SocketIOClient.SocketIO::OnBinaryReceived(System.Byte[])
extern void SocketIO_OnBinaryReceived_m1280D42929DF54865FD08FB044BC4354076DD217 (void);
// 0x00000074 System.Void SocketIOClient.SocketIO::OnClosed(System.String)
extern void SocketIO_OnClosed_mA680F7D9EFE2BE2BEBE34AEF1CE4085888A72CDB (void);
// 0x00000075 System.Void SocketIOClient.SocketIO::OpenedHandler(System.String,System.Int32,System.Int32)
extern void SocketIO_OpenedHandler_m3D53F586CA653CF53D85A9E9C8907B8D1AAA3246 (void);
// 0x00000076 System.Void SocketIOClient.SocketIO::ConnectedHandler(SocketIOClient.EioHandler.ConnectionResult)
extern void SocketIO_ConnectedHandler_m5976BCF6301B1E6F9597AA15EA9416AACD48859C (void);
// 0x00000077 System.Void SocketIOClient.SocketIO::AckHandler(System.Int32,System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void SocketIO_AckHandler_m47EAE6DBD96596740B3D210578F6AD0F4F0FEE02 (void);
// 0x00000078 System.Void SocketIOClient.SocketIO::BinaryAckHandler(System.Int32,System.Int32,System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void SocketIO_BinaryAckHandler_mFC4C716EEA2125AADC9E8121183BB419A6040AED (void);
// 0x00000079 System.Void SocketIOClient.SocketIO::BinaryReceivedHandler(System.Int32,System.Int32,System.String,System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void SocketIO_BinaryReceivedHandler_mBE0A813754295B3062A63056C4A4A99F95A89821 (void);
// 0x0000007A System.Void SocketIOClient.SocketIO::DisconnectedHandler()
extern void SocketIO_DisconnectedHandler_mF19D962F9CB4C10450C6E8B275091A89402F1A94 (void);
// 0x0000007B System.Void SocketIOClient.SocketIO::ErrorHandler(System.String)
extern void SocketIO_ErrorHandler_m3BD6210E590FF0CBCEC913A6E4E26F1C684B777D (void);
// 0x0000007C System.Void SocketIOClient.SocketIO::EventReceivedHandler(System.Int32,System.String,System.Collections.Generic.List`1<System.Text.Json.JsonElement>)
extern void SocketIO_EventReceivedHandler_mCFDBBE4D572CCB0EF1B6841179B075B405B767D5 (void);
// 0x0000007D System.Void SocketIOClient.SocketIO::PingHandler()
extern void SocketIO_PingHandler_m884E9C2E0035DC30DB3B481F8A2E67250BB273EC (void);
// 0x0000007E System.Void SocketIOClient.SocketIO::PongHandler()
extern void SocketIO_PongHandler_m9F82BF1D7D6DEAE82BFFC08291C9AA15E8EDBCF5 (void);
// 0x0000007F System.Threading.Tasks.Task SocketIOClient.SocketIO::InvokeDisconnectAsync(System.String)
extern void SocketIO_InvokeDisconnectAsync_mD2E2A0D695F1617359BF4AB559C03057D19C706C (void);
// 0x00000080 System.Threading.Tasks.Task SocketIOClient.SocketIO::StartPingAsync()
extern void SocketIO_StartPingAsync_m3A3381954FDABF8D1EB6FF103E4F15CF3A6A737B (void);
// 0x00000081 System.Void SocketIOClient.SocketIO::StopPingInterval()
extern void SocketIO_StopPingInterval_m419931B18A8AE39AEA1A1D087F7671FAE7CFAFE4 (void);
// 0x00000082 System.Void SocketIOClient.SocketIO::Dispose()
extern void SocketIO_Dispose_m1D634EC2959B5E13292E20A8D5B2DCA3FDFEF108 (void);
// 0x00000083 System.Void SocketIOClient.SocketIO/<ConnectAsync>d__87::MoveNext()
extern void U3CConnectAsyncU3Ed__87_MoveNext_m2D026BAA9B7DEADEB9EC6105F0FB51C4164D92BD (void);
// 0x00000084 System.Void SocketIOClient.SocketIO/<ConnectAsync>d__87::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConnectAsyncU3Ed__87_SetStateMachine_mC3988BF8B083971FDAB815D1DE6D8B62A25B5E31 (void);
// 0x00000085 System.Void SocketIOClient.SocketIO/<DisconnectAsync>d__88::MoveNext()
extern void U3CDisconnectAsyncU3Ed__88_MoveNext_m4EFECF9406351B59E63A3CC50813ABD6F2F2B85D (void);
// 0x00000086 System.Void SocketIOClient.SocketIO/<DisconnectAsync>d__88::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDisconnectAsyncU3Ed__88_SetStateMachine_m5882ADA1C963C1DD9ED2342145438212C4C2F555 (void);
// 0x00000087 System.Void SocketIOClient.SocketIO/<EmityCoreAsync>d__95::MoveNext()
extern void U3CEmityCoreAsyncU3Ed__95_MoveNext_mAA824FA6D9C966C0D82F04A3933375E78F55379D (void);
// 0x00000088 System.Void SocketIOClient.SocketIO/<EmityCoreAsync>d__95::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEmityCoreAsyncU3Ed__95_SetStateMachine_m195AD76D4DD1A13864C143C2357FE687D4FD293C (void);
// 0x00000089 System.Void SocketIOClient.SocketIO/<EmitCallbackAsync>d__96::MoveNext()
extern void U3CEmitCallbackAsyncU3Ed__96_MoveNext_m10AD1445FB9AC7FC933B465824E6072793915947 (void);
// 0x0000008A System.Void SocketIOClient.SocketIO/<EmitCallbackAsync>d__96::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEmitCallbackAsyncU3Ed__96_SetStateMachine_m8A68E90477DDA2E92DCAA6AE5CE470FB49B75CEC (void);
// 0x0000008B System.Void SocketIOClient.SocketIO/<EmitAsync>d__98::MoveNext()
extern void U3CEmitAsyncU3Ed__98_MoveNext_mEF3D565B8F4BEBCDC3686B3E999F9C833DBAB87D (void);
// 0x0000008C System.Void SocketIOClient.SocketIO/<EmitAsync>d__98::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEmitAsyncU3Ed__98_SetStateMachine_mA919980759FA57B357F9BBAB2489F57EE7BF3DA4 (void);
// 0x0000008D System.Void SocketIOClient.SocketIO/<EmitAsync>d__99::MoveNext()
extern void U3CEmitAsyncU3Ed__99_MoveNext_mCDD6CA506FF87040A9F61A8846A8EA420E19EF2D (void);
// 0x0000008E System.Void SocketIOClient.SocketIO/<EmitAsync>d__99::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEmitAsyncU3Ed__99_SetStateMachine_m3BE8D82221662C9C7F3FBB307B6ECCE62BABF11C (void);
// 0x0000008F System.Void SocketIOClient.SocketIO/<EmitAsync>d__100::MoveNext()
extern void U3CEmitAsyncU3Ed__100_MoveNext_mEC42B49D4F13D2E1C5029F45E0CF84086AB0986E (void);
// 0x00000090 System.Void SocketIOClient.SocketIO/<EmitAsync>d__100::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEmitAsyncU3Ed__100_SetStateMachine_m2D3248E4F0000599C77082CC1EDB814A844AA1B3 (void);
// 0x00000091 System.Void SocketIOClient.SocketIO/<EmitAsync>d__101::MoveNext()
extern void U3CEmitAsyncU3Ed__101_MoveNext_mC58A148714F7E77D70678DC32EAA2097111A8393 (void);
// 0x00000092 System.Void SocketIOClient.SocketIO/<EmitAsync>d__101::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEmitAsyncU3Ed__101_SetStateMachine_m1C2F4C27BBB76FEE1D88338A56A79471D34FB72A (void);
// 0x00000093 System.Void SocketIOClient.SocketIO/<OnClosed>d__104::MoveNext()
extern void U3COnClosedU3Ed__104_MoveNext_mF948C63F330E6DFADBCDE9D93FD410D35C192F64 (void);
// 0x00000094 System.Void SocketIOClient.SocketIO/<OnClosed>d__104::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COnClosedU3Ed__104_SetStateMachine_mFDE3990A55091422819042018614A7F1D284FCA6 (void);
// 0x00000095 System.Void SocketIOClient.SocketIO/<OpenedHandler>d__105::MoveNext()
extern void U3COpenedHandlerU3Ed__105_MoveNext_m68D99B43F82AE2000B9DC426C5642D63C22875BB (void);
// 0x00000096 System.Void SocketIOClient.SocketIO/<OpenedHandler>d__105::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COpenedHandlerU3Ed__105_SetStateMachine_mAFD0F42959271BC191F3AFD0B4854AB646A76FED (void);
// 0x00000097 System.Void SocketIOClient.SocketIO/<DisconnectedHandler>d__110::MoveNext()
extern void U3CDisconnectedHandlerU3Ed__110_MoveNext_m00D4848F9F9144B342A5837BAD2D02218D4C84BA (void);
// 0x00000098 System.Void SocketIOClient.SocketIO/<DisconnectedHandler>d__110::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDisconnectedHandlerU3Ed__110_SetStateMachine_m61BF3EC1399B740FF5D43784DB64D7EE77B3CEA2 (void);
// 0x00000099 System.Void SocketIOClient.SocketIO/<PingHandler>d__113::MoveNext()
extern void U3CPingHandlerU3Ed__113_MoveNext_mFBD465A826EAFB3A03BF248B971F7CDA777956CE (void);
// 0x0000009A System.Void SocketIOClient.SocketIO/<PingHandler>d__113::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPingHandlerU3Ed__113_SetStateMachine_mB1685B3B4D5FABFDB12A135DF76D5B9ED3F22AC8 (void);
// 0x0000009B System.Void SocketIOClient.SocketIO/<InvokeDisconnectAsync>d__115::MoveNext()
extern void U3CInvokeDisconnectAsyncU3Ed__115_MoveNext_m1EB2D6FE144438070F4222B20DE2B17B9A08A0F4 (void);
// 0x0000009C System.Void SocketIOClient.SocketIO/<InvokeDisconnectAsync>d__115::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInvokeDisconnectAsyncU3Ed__115_SetStateMachine_mE5373AA733E610BF6911F0BD1B77AF28188C2A16 (void);
// 0x0000009D System.Void SocketIOClient.SocketIO/<StartPingAsync>d__116::MoveNext()
extern void U3CStartPingAsyncU3Ed__116_MoveNext_m840437D67141837C2DAFE0E9F5DD1BD639C522E4 (void);
// 0x0000009E System.Void SocketIOClient.SocketIO/<StartPingAsync>d__116::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartPingAsyncU3Ed__116_SetStateMachine_m7B3C285ADAB5FF332FD0A98EA9269899262D388A (void);
// 0x0000009F System.Void SocketIOClient.SocketIOOptions::.ctor()
extern void SocketIOOptions__ctor_m06916B51C57F7FF8822597396952026FFEF4776E (void);
// 0x000000A0 System.String SocketIOClient.SocketIOOptions::get_Path()
extern void SocketIOOptions_get_Path_mBCCCFD05D277C258FE42404F5635018AE9932BE7 (void);
// 0x000000A1 System.Void SocketIOClient.SocketIOOptions::set_Path(System.String)
extern void SocketIOOptions_set_Path_mEFEE8854DF82E9A966DBA936DABBFDF233B96514 (void);
// 0x000000A2 System.TimeSpan SocketIOClient.SocketIOOptions::get_ConnectionTimeout()
extern void SocketIOOptions_get_ConnectionTimeout_mCFC0E37381E689D7DA3EC99BC2573DEF87225926 (void);
// 0x000000A3 System.Void SocketIOClient.SocketIOOptions::set_ConnectionTimeout(System.TimeSpan)
extern void SocketIOOptions_set_ConnectionTimeout_mCCF4559254F56A26DE6E53AB39EBA32933935C1A (void);
// 0x000000A4 System.Collections.Generic.Dictionary`2<System.String,System.String> SocketIOClient.SocketIOOptions::get_Query()
extern void SocketIOOptions_get_Query_m17B3B19930A8BEE2C662EE505BC020ADD646BE7C (void);
// 0x000000A5 System.Void SocketIOClient.SocketIOOptions::set_Query(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void SocketIOOptions_set_Query_m46C7820644E98AE3061394F4F7922E4707ED1615 (void);
// 0x000000A6 System.Boolean SocketIOClient.SocketIOOptions::get_Reconnection()
extern void SocketIOOptions_get_Reconnection_mC29D95BCD71A21C4943C3163D460CAA84356C570 (void);
// 0x000000A7 System.Void SocketIOClient.SocketIOOptions::set_Reconnection(System.Boolean)
extern void SocketIOOptions_set_Reconnection_m54AEADFFE628856F826A417F870BE8DF278B0F83 (void);
// 0x000000A8 System.Double SocketIOClient.SocketIOOptions::get_ReconnectionDelay()
extern void SocketIOOptions_get_ReconnectionDelay_m4040719E46D879DFA7CE3F9CA425058FB1DAE407 (void);
// 0x000000A9 System.Void SocketIOClient.SocketIOOptions::set_ReconnectionDelay(System.Double)
extern void SocketIOOptions_set_ReconnectionDelay_m935171E3E95AAB83AB0FA892F881DA39355345EF (void);
// 0x000000AA System.Int32 SocketIOClient.SocketIOOptions::get_ReconnectionDelayMax()
extern void SocketIOOptions_get_ReconnectionDelayMax_mCD78CDCAAE861B6ED9587BF611A7B944EC5A4C19 (void);
// 0x000000AB System.Void SocketIOClient.SocketIOOptions::set_ReconnectionDelayMax(System.Int32)
extern void SocketIOOptions_set_ReconnectionDelayMax_mD09CA48DBA4DAA2AE9A4ED350E56272BD9AEC1A7 (void);
// 0x000000AC System.Int32 SocketIOClient.SocketIOOptions::get_ReconnectionAttempts()
extern void SocketIOOptions_get_ReconnectionAttempts_m1DBF68BA62FB07798A6090AF66FC91F6EBEE864A (void);
// 0x000000AD System.Void SocketIOClient.SocketIOOptions::set_ReconnectionAttempts(System.Int32)
extern void SocketIOOptions_set_ReconnectionAttempts_m2A7A36DC8BA1CFC319A244EE23541DC1C00BC663 (void);
// 0x000000AE System.Double SocketIOClient.SocketIOOptions::get_RandomizationFactor()
extern void SocketIOOptions_get_RandomizationFactor_m65360C0C600837F694A627BFFD5006BE4A27FF1A (void);
// 0x000000AF System.Void SocketIOClient.SocketIOOptions::set_RandomizationFactor(System.Double)
extern void SocketIOOptions_set_RandomizationFactor_mC1A8CB61618E370F4C3FCFD65D1EC6B7D22A8992 (void);
// 0x000000B0 System.Int32 SocketIOClient.SocketIOOptions::get_EIO()
extern void SocketIOOptions_get_EIO_m7D2D358521675A36258DCAFB56E7D241B60F9507 (void);
// 0x000000B1 System.Void SocketIOClient.SocketIOOptions::set_EIO(System.Int32)
extern void SocketIOOptions_set_EIO_m2392652E0EA226FD23A7CEECF7D3CF86548E249D (void);
// 0x000000B2 SocketIOClient.EioHandler.IEioHandler SocketIOClient.SocketIOOptions::get_EioHandler()
extern void SocketIOOptions_get_EioHandler_m2FDA56CF08B8E84D0FA2676EF5DAC0025C309685 (void);
// 0x000000B3 System.Void SocketIOClient.SocketIOOptions::set_EioHandler(SocketIOClient.EioHandler.IEioHandler)
extern void SocketIOOptions_set_EioHandler_m00AF61CDD670982F7E6173C7286B562C26826032 (void);
// 0x000000B4 System.Void SocketIOClient.SocketIOResponse::.ctor(System.Collections.Generic.IList`1<System.Text.Json.JsonElement>,SocketIOClient.SocketIO)
extern void SocketIOResponse__ctor_m8D7582B380A52CFAF3B3303DCC2357361C178C8B (void);
// 0x000000B5 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.SocketIOResponse::get_InComingBytes()
extern void SocketIOResponse_get_InComingBytes_m6BCBB46E08477143008307B876D7F6F57B037CD7 (void);
// 0x000000B6 SocketIOClient.SocketIO SocketIOClient.SocketIOResponse::get_SocketIO()
extern void SocketIOResponse_get_SocketIO_m66CB8A47AC82FBD8A049153DADD30A9F9D9055A0 (void);
// 0x000000B7 System.Int32 SocketIOClient.SocketIOResponse::get_PacketId()
extern void SocketIOResponse_get_PacketId_m0AA89D03758291ACC1BA35E9EFA160FD81A0818C (void);
// 0x000000B8 System.Void SocketIOClient.SocketIOResponse::set_PacketId(System.Int32)
extern void SocketIOResponse_set_PacketId_mFF20A70DCB0825C3610A460534EDE0E32A97C6A6 (void);
// 0x000000B9 T SocketIOClient.SocketIOResponse::GetValue(System.Int32)
// 0x000000BA System.Text.Json.JsonElement SocketIOClient.SocketIOResponse::GetValue(System.Int32)
extern void SocketIOResponse_GetValue_m8CDEAB86046C6ABEFECDB2D5AAA602E6951DA38D (void);
// 0x000000BB System.Int32 SocketIOClient.SocketIOResponse::get_Count()
extern void SocketIOResponse_get_Count_m9501A452371B71F429B3E5D931350F9B200E7A93 (void);
// 0x000000BC System.String SocketIOClient.SocketIOResponse::ToString()
extern void SocketIOResponse_ToString_mD3EEF3B6DEF88A9BD6EC4C59CCDA434112E5D6B5 (void);
// 0x000000BD System.Threading.Tasks.Task SocketIOClient.SocketIOResponse::CallbackAsync(System.Object[])
extern void SocketIOResponse_CallbackAsync_mA5B0A112375DF655574CBFF1EE2C70656AF14365 (void);
// 0x000000BE System.Void SocketIOClient.SocketIOResponse/<CallbackAsync>d__17::MoveNext()
extern void U3CCallbackAsyncU3Ed__17_MoveNext_mE6BCFAB6E3556FA96926C61F3DB217FEC67FA0C4 (void);
// 0x000000BF System.Void SocketIOClient.SocketIOResponse/<CallbackAsync>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCallbackAsyncU3Ed__17_SetStateMachine_m061202E8AD2408AAA14AB0710F9EB5EC4D732E8E (void);
// 0x000000C0 System.Uri SocketIOClient.UrlConverter::HttpToWs(System.Uri,SocketIOClient.SocketIOOptions)
extern void UrlConverter_HttpToWs_mF16F1EB20B072DD4A66C462F8CEFC23CF88F0A85 (void);
// 0x000000C1 System.Void SocketIOClient.UrlConverter::.ctor()
extern void UrlConverter__ctor_m5734E379DB688FBCB799D46EE89FFF8CA4F8901D (void);
// 0x000000C2 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::.ctor()
extern void ClientWebSocket__ctor_mA1827C0AB87CBFB6F0D1C717BF1ECBF6ED924A99 (void);
// 0x000000C3 System.Int32 SocketIOClient.WebSocketClient.ClientWebSocket::get_ReceiveChunkSize()
extern void ClientWebSocket_get_ReceiveChunkSize_m7749B33938398A3872F179980B0873758B3FBFA9 (void);
// 0x000000C4 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::set_ReceiveChunkSize(System.Int32)
extern void ClientWebSocket_set_ReceiveChunkSize_mDC65B177200EEF759E5C57600171F794E04E435A (void);
// 0x000000C5 System.TimeSpan SocketIOClient.WebSocketClient.ClientWebSocket::get_ConnectionTimeout()
extern void ClientWebSocket_get_ConnectionTimeout_mD1E381ADF074E80D85DCCD422A85B26542FE1E51 (void);
// 0x000000C6 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::set_ConnectionTimeout(System.TimeSpan)
extern void ClientWebSocket_set_ConnectionTimeout_m35B7C9A67E8D749C5E7930DF2C3D502A221B265A (void);
// 0x000000C7 System.Action`1<System.Net.WebSockets.ClientWebSocketOptions> SocketIOClient.WebSocketClient.ClientWebSocket::get_Config()
extern void ClientWebSocket_get_Config_m5960E6BF9CA8C8BAFB1AF3DBFCF518F36ABDC89B (void);
// 0x000000C8 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::set_Config(System.Action`1<System.Net.WebSockets.ClientWebSocketOptions>)
extern void ClientWebSocket_set_Config_mD4AC6ACA28EB38E92896768434C4F3B38FC24EE2 (void);
// 0x000000C9 System.Threading.Tasks.Task SocketIOClient.WebSocketClient.ClientWebSocket::ConnectAsync(System.Uri)
extern void ClientWebSocket_ConnectAsync_mF911B0CF25FFCB1B4BB9D858C7C1BC5A15D741CB (void);
// 0x000000CA System.Threading.Tasks.Task SocketIOClient.WebSocketClient.ClientWebSocket::SendMessageAsync(System.String)
extern void ClientWebSocket_SendMessageAsync_m311F5F0039B8224F90DAD91C5E1EA0992C25FA4D (void);
// 0x000000CB System.Threading.Tasks.Task SocketIOClient.WebSocketClient.ClientWebSocket::SendMessageAsync(System.String,System.Threading.CancellationToken)
extern void ClientWebSocket_SendMessageAsync_mCABC14DC46E337DBD5AF0EA89F7E87035213877E (void);
// 0x000000CC System.Threading.Tasks.Task SocketIOClient.WebSocketClient.ClientWebSocket::SendMessageAsync(System.Byte[])
extern void ClientWebSocket_SendMessageAsync_m21FA6EF4E4B2B9B188736980F7784B052AA4AF55 (void);
// 0x000000CD System.Threading.Tasks.Task SocketIOClient.WebSocketClient.ClientWebSocket::SendMessageAsync(System.Byte[],System.Threading.CancellationToken)
extern void ClientWebSocket_SendMessageAsync_m601F6BEC7EA9D8B0229B062C4842A8CF53CBC040 (void);
// 0x000000CE System.Threading.Tasks.Task SocketIOClient.WebSocketClient.ClientWebSocket::DisconnectAsync()
extern void ClientWebSocket_DisconnectAsync_mBE6A6FF743EE95E7D3D837BDC1D1F14CEBC386AC (void);
// 0x000000CF System.Threading.Tasks.Task SocketIOClient.WebSocketClient.ClientWebSocket::ListenAsync(System.Threading.CancellationToken)
extern void ClientWebSocket_ListenAsync_mF736584345F5866A122FF3FEBC0C69DB57BD7CB9 (void);
// 0x000000D0 System.Action`1<System.String> SocketIOClient.WebSocketClient.ClientWebSocket::get_OnTextReceived()
extern void ClientWebSocket_get_OnTextReceived_mCD4C9CDB5F1499A17ECBC34C258F5BA81DE4EA1E (void);
// 0x000000D1 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::set_OnTextReceived(System.Action`1<System.String>)
extern void ClientWebSocket_set_OnTextReceived_mCBDDBD288AC1865AB45553B5B9936E1BD00AE6C7 (void);
// 0x000000D2 System.Action`1<System.Byte[]> SocketIOClient.WebSocketClient.ClientWebSocket::get_OnBinaryReceived()
extern void ClientWebSocket_get_OnBinaryReceived_mB55D86EFA0F83FC6E1D3BA4F03EF2D5C6C2A2DF7 (void);
// 0x000000D3 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::set_OnBinaryReceived(System.Action`1<System.Byte[]>)
extern void ClientWebSocket_set_OnBinaryReceived_m5FC20F2AEC0774E90F80172D9CB3B533A2CD1FBA (void);
// 0x000000D4 System.Action`1<System.String> SocketIOClient.WebSocketClient.ClientWebSocket::get_OnClosed()
extern void ClientWebSocket_get_OnClosed_m3F95A32F74877FA253A43BE7914B539925E60F2C (void);
// 0x000000D5 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::set_OnClosed(System.Action`1<System.String>)
extern void ClientWebSocket_set_OnClosed_m0B81795128A5F276D80D5DAFE6E826EA3B519B51 (void);
// 0x000000D6 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::DisposeWebSocketIfNotNull()
extern void ClientWebSocket_DisposeWebSocketIfNotNull_m780A5705D8E3084FF4C7FBA84006E22A6A1AC27F (void);
// 0x000000D7 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::DisposeListenTokenIfNotNull()
extern void ClientWebSocket_DisposeListenTokenIfNotNull_m35861CEA7EBB2BBC42BC4A02E0FC47D592E1871E (void);
// 0x000000D8 System.Void SocketIOClient.WebSocketClient.ClientWebSocket::Dispose()
extern void ClientWebSocket_Dispose_m51772362D0A2C24748031154C289626EAD941AFB (void);
// 0x000000D9 System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<ConnectAsync>d__16::MoveNext()
extern void U3CConnectAsyncU3Ed__16_MoveNext_m12909D8FA0B7EF8A1A0546A68DCDD24DE4EB381F (void);
// 0x000000DA System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<ConnectAsync>d__16::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConnectAsyncU3Ed__16_SetStateMachine_m2831C8741307B31A1F1BF32577473F8673108E95 (void);
// 0x000000DB System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<SendMessageAsync>d__17::MoveNext()
extern void U3CSendMessageAsyncU3Ed__17_MoveNext_m987B5116A558F40C630039C42CBA359CC928F3AD (void);
// 0x000000DC System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<SendMessageAsync>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendMessageAsyncU3Ed__17_SetStateMachine_mD254A3520A1B161932C624CC1275C77AFB161EFD (void);
// 0x000000DD System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<SendMessageAsync>d__18::MoveNext()
extern void U3CSendMessageAsyncU3Ed__18_MoveNext_m970AA126DE68215A0CD7F52C19C7184381DF760E (void);
// 0x000000DE System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<SendMessageAsync>d__18::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendMessageAsyncU3Ed__18_SetStateMachine_m253767F4F7CF7F0A6A373671B446C9B9CC4FC2F1 (void);
// 0x000000DF System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<SendMessageAsync>d__19::MoveNext()
extern void U3CSendMessageAsyncU3Ed__19_MoveNext_mD0CC44463FC3C8613F58BC52AF0785294C9505D5 (void);
// 0x000000E0 System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<SendMessageAsync>d__19::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendMessageAsyncU3Ed__19_SetStateMachine_mC3109B6EAF527C0B0BC16768EC3872E32E3D2033 (void);
// 0x000000E1 System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<SendMessageAsync>d__20::MoveNext()
extern void U3CSendMessageAsyncU3Ed__20_MoveNext_mE769EA29262903DC77094E30C2E9D764A511DB9C (void);
// 0x000000E2 System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<SendMessageAsync>d__20::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSendMessageAsyncU3Ed__20_SetStateMachine_m9E7ED77807BDD4FBF17C4CF9BA4C940AFA44F2FE (void);
// 0x000000E3 System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<DisconnectAsync>d__21::MoveNext()
extern void U3CDisconnectAsyncU3Ed__21_MoveNext_mBC523949C0CC63F3B76A484987411F8D06C8D0B8 (void);
// 0x000000E4 System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<DisconnectAsync>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDisconnectAsyncU3Ed__21_SetStateMachine_mBDF93F239C6BAEAB9C864CAF33087A49D951131A (void);
// 0x000000E5 System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<ListenAsync>d__22::MoveNext()
extern void U3CListenAsyncU3Ed__22_MoveNext_m8982BFD36200B01CFA1624FA97F1C0EEF6E97F5D (void);
// 0x000000E6 System.Void SocketIOClient.WebSocketClient.ClientWebSocket/<ListenAsync>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CListenAsyncU3Ed__22_SetStateMachine_m1B6E29D2A7E1528746774F705539DB49444FC347 (void);
// 0x000000E7 System.TimeSpan SocketIOClient.WebSocketClient.IWebSocketClient::get_ConnectionTimeout()
// 0x000000E8 System.Void SocketIOClient.WebSocketClient.IWebSocketClient::set_ConnectionTimeout(System.TimeSpan)
// 0x000000E9 System.Threading.Tasks.Task SocketIOClient.WebSocketClient.IWebSocketClient::ConnectAsync(System.Uri)
// 0x000000EA System.Threading.Tasks.Task SocketIOClient.WebSocketClient.IWebSocketClient::SendMessageAsync(System.String)
// 0x000000EB System.Threading.Tasks.Task SocketIOClient.WebSocketClient.IWebSocketClient::SendMessageAsync(System.String,System.Threading.CancellationToken)
// 0x000000EC System.Threading.Tasks.Task SocketIOClient.WebSocketClient.IWebSocketClient::SendMessageAsync(System.Byte[])
// 0x000000ED System.Threading.Tasks.Task SocketIOClient.WebSocketClient.IWebSocketClient::SendMessageAsync(System.Byte[],System.Threading.CancellationToken)
// 0x000000EE System.Threading.Tasks.Task SocketIOClient.WebSocketClient.IWebSocketClient::DisconnectAsync()
// 0x000000EF System.Action`1<System.String> SocketIOClient.WebSocketClient.IWebSocketClient::get_OnTextReceived()
// 0x000000F0 System.Void SocketIOClient.WebSocketClient.IWebSocketClient::set_OnTextReceived(System.Action`1<System.String>)
// 0x000000F1 System.Action`1<System.Byte[]> SocketIOClient.WebSocketClient.IWebSocketClient::get_OnBinaryReceived()
// 0x000000F2 System.Void SocketIOClient.WebSocketClient.IWebSocketClient::set_OnBinaryReceived(System.Action`1<System.Byte[]>)
// 0x000000F3 System.Action`1<System.String> SocketIOClient.WebSocketClient.IWebSocketClient::get_OnClosed()
// 0x000000F4 System.Void SocketIOClient.WebSocketClient.IWebSocketClient::set_OnClosed(System.Action`1<System.String>)
// 0x000000F5 T SocketIOClient.SocketIOSerializer.ISocketIOSeriazlier`1::Read(System.String)
// 0x000000F6 System.Void SocketIOClient.SocketIOSerializer.ISocketIOSeriazlier`1::Write()
// 0x000000F7 SocketIOClient.Response.OpenResponse SocketIOClient.SocketIOSerializer.OpenSerializer::Read(System.String)
extern void OpenSerializer_Read_mE9ECE57D1494309ECA7BD9F72106457F6A8F93EF (void);
// 0x000000F8 System.Void SocketIOClient.SocketIOSerializer.OpenSerializer::Write()
extern void OpenSerializer_Write_m63ADB4FD96ADB729DFD1D459D795F5CB5BA0336E (void);
// 0x000000F9 System.Void SocketIOClient.SocketIOSerializer.OpenSerializer::.ctor()
extern void OpenSerializer__ctor_mAD610640F85DD9060FA31E793C7C8AAC18CA1F3B (void);
// 0x000000FA System.String SocketIOClient.Response.OpenResponse::get_Sid()
extern void OpenResponse_get_Sid_m4AD9365BE2B2376B18B952A437A374716DDD5BEE (void);
// 0x000000FB System.Void SocketIOClient.Response.OpenResponse::set_Sid(System.String)
extern void OpenResponse_set_Sid_m31DDE3B566F8001C38ECFE2E1286F44E9FC97331 (void);
// 0x000000FC System.Int32 SocketIOClient.Response.OpenResponse::get_PingInterval()
extern void OpenResponse_get_PingInterval_mDE01940DFC5D5BEAA0DE568A0ADFF22B3C48898E (void);
// 0x000000FD System.Void SocketIOClient.Response.OpenResponse::set_PingInterval(System.Int32)
extern void OpenResponse_set_PingInterval_mC4AC124E92F257CABADA4D7B9EC9B12B3B8B7BAE (void);
// 0x000000FE System.Int32 SocketIOClient.Response.OpenResponse::get_PingTimeout()
extern void OpenResponse_get_PingTimeout_m13757B0A9DF82264DD45B7532696A59505EA35E7 (void);
// 0x000000FF System.Void SocketIOClient.Response.OpenResponse::set_PingTimeout(System.Int32)
extern void OpenResponse_set_PingTimeout_mAAA7816FCA4849294F6B682A790EEA108ADD1899 (void);
// 0x00000100 System.Void SocketIOClient.Response.OpenResponse::.ctor()
extern void OpenResponse__ctor_mF22493984ED552162DA76BBB74A2296722E42DC7 (void);
// 0x00000101 System.Void SocketIOClient.Processors.AckProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void AckProcessor_Process_mF07E27C9D1327C14DA7A1E52D6B9FDE607FD4CC8 (void);
// 0x00000102 System.Void SocketIOClient.Processors.AckProcessor::.ctor()
extern void AckProcessor__ctor_m2947222CD896DB37F9256C24A048F6509332106D (void);
// 0x00000103 System.Void SocketIOClient.Processors.BinaryAckProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void BinaryAckProcessor_Process_m4B0EA08831971EA7CB3D9DA613D9971FD48C10BB (void);
// 0x00000104 System.Void SocketIOClient.Processors.BinaryAckProcessor::.ctor()
extern void BinaryAckProcessor__ctor_mE00D4B4B1F68A3C924F1998C21D080C0AE74C1C6 (void);
// 0x00000105 System.Void SocketIOClient.Processors.BinaryEventProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void BinaryEventProcessor_Process_m6B3D8CC22A1C88D262508E8B6F048F4A7134E0A8 (void);
// 0x00000106 System.Void SocketIOClient.Processors.BinaryEventProcessor::.ctor()
extern void BinaryEventProcessor__ctor_mFDBD51BCDEC7EC93C4C20E4D110C7E99334173F3 (void);
// 0x00000107 System.Void SocketIOClient.Processors.ConnectedProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void ConnectedProcessor_Process_m3722C902BCDB1FD3813AB30091F1E65BAE216D46 (void);
// 0x00000108 System.Void SocketIOClient.Processors.ConnectedProcessor::.ctor()
extern void ConnectedProcessor__ctor_m1F5EABDE471A0F92758AB2E959C992E568BD0275 (void);
// 0x00000109 System.Void SocketIOClient.Processors.DisconnectedProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void DisconnectedProcessor_Process_m2E26A08A641B9E529592C305AA6F520F523F5D3D (void);
// 0x0000010A System.Void SocketIOClient.Processors.DisconnectedProcessor::.ctor()
extern void DisconnectedProcessor__ctor_m3E1066731368F107C71FAC4F7046FAD8FBD70BA8 (void);
// 0x0000010B System.Void SocketIOClient.Processors.EngineIOProtocolProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void EngineIOProtocolProcessor_Process_m5D6E08ADF3A1826D1631C18E1B1093A7C79D4493 (void);
// 0x0000010C System.Void SocketIOClient.Processors.EngineIOProtocolProcessor::.ctor()
extern void EngineIOProtocolProcessor__ctor_mF3B7CC7579414874A972335F972B8321F983C80B (void);
// 0x0000010D System.Void SocketIOClient.Processors.ErrorProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void ErrorProcessor_Process_m7F154B4926DE2742B6193BC4F8439A98979617FC (void);
// 0x0000010E System.Void SocketIOClient.Processors.ErrorProcessor::.ctor()
extern void ErrorProcessor__ctor_m6AEC45F4CEB4FA6EF7B6B354082F333C9A832AB5 (void);
// 0x0000010F System.Void SocketIOClient.Processors.EventProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void EventProcessor_Process_m89A74D1018DAF5B7DEF6F60720663B250F43DD56 (void);
// 0x00000110 System.Void SocketIOClient.Processors.EventProcessor::.ctor()
extern void EventProcessor__ctor_mB05B5CBC45E4F80A362B49E2C92BBB1B5B4AE731 (void);
// 0x00000111 System.String SocketIOClient.Processors.MessageContext::get_Message()
extern void MessageContext_get_Message_m71E856DCDFD208F0E2F2606C87B4C09A62394046 (void);
// 0x00000112 System.Void SocketIOClient.Processors.MessageContext::set_Message(System.String)
extern void MessageContext_set_Message_m9B61EF45207C592C44C4833A416501132B088BB3 (void);
// 0x00000113 System.String SocketIOClient.Processors.MessageContext::get_Namespace()
extern void MessageContext_get_Namespace_m97E935E1804A8E9EAAE4959A1B2B8755306ED809 (void);
// 0x00000114 System.Void SocketIOClient.Processors.MessageContext::set_Namespace(System.String)
extern void MessageContext_set_Namespace_m457805FBE6F1CD2169996314D38A0D9DB868E5BC (void);
// 0x00000115 SocketIOClient.EioHandler.IEioHandler SocketIOClient.Processors.MessageContext::get_EioHandler()
extern void MessageContext_get_EioHandler_m85F739B9876E5D4B0A3ED189FF6EDC57E0E08204 (void);
// 0x00000116 System.Void SocketIOClient.Processors.MessageContext::set_EioHandler(SocketIOClient.EioHandler.IEioHandler)
extern void MessageContext_set_EioHandler_mAE568D6AE500A8C187647D0A2CDDEDE5085F6523 (void);
// 0x00000117 System.Action`1<SocketIOClient.EioHandler.ConnectionResult> SocketIOClient.Processors.MessageContext::get_ConnectedHandler()
extern void MessageContext_get_ConnectedHandler_m6BCF66F7AE523FB9BF65E916B51B97F282003CE2 (void);
// 0x00000118 System.Void SocketIOClient.Processors.MessageContext::set_ConnectedHandler(System.Action`1<SocketIOClient.EioHandler.ConnectionResult>)
extern void MessageContext_set_ConnectedHandler_mDCC20F6C69421241CDA16D87EA3E8886699A851B (void);
// 0x00000119 SocketIOClient.OnAck SocketIOClient.Processors.MessageContext::get_AckHandler()
extern void MessageContext_get_AckHandler_mCDCCE8ED4018C77FD067C7165E388AB74FD2A42A (void);
// 0x0000011A System.Void SocketIOClient.Processors.MessageContext::set_AckHandler(SocketIOClient.OnAck)
extern void MessageContext_set_AckHandler_m45F9000D43D56B45C6C989B7E2CED97FD84F7E0E (void);
// 0x0000011B SocketIOClient.OnBinaryAck SocketIOClient.Processors.MessageContext::get_BinaryAckHandler()
extern void MessageContext_get_BinaryAckHandler_m4222EFC136B072544CE07357F85500E699F689F3 (void);
// 0x0000011C System.Void SocketIOClient.Processors.MessageContext::set_BinaryAckHandler(SocketIOClient.OnBinaryAck)
extern void MessageContext_set_BinaryAckHandler_m800A90E331A9E0011E15FB25F4F3342A3ABA276B (void);
// 0x0000011D SocketIOClient.OnBinaryReceived SocketIOClient.Processors.MessageContext::get_BinaryReceivedHandler()
extern void MessageContext_get_BinaryReceivedHandler_m773BC296E59FCC6384E4DBEF06BE047174B24A3D (void);
// 0x0000011E System.Void SocketIOClient.Processors.MessageContext::set_BinaryReceivedHandler(SocketIOClient.OnBinaryReceived)
extern void MessageContext_set_BinaryReceivedHandler_mEBD4FE0D0DCB4E7D686673FA817831B619526A43 (void);
// 0x0000011F SocketIOClient.OnDisconnected SocketIOClient.Processors.MessageContext::get_DisconnectedHandler()
extern void MessageContext_get_DisconnectedHandler_m445661F3962662A3414518F8A1DCB64A25AF0546 (void);
// 0x00000120 System.Void SocketIOClient.Processors.MessageContext::set_DisconnectedHandler(SocketIOClient.OnDisconnected)
extern void MessageContext_set_DisconnectedHandler_m77579D5BB89F09615A9234277C1D8B28D9B1AEEF (void);
// 0x00000121 SocketIOClient.OnError SocketIOClient.Processors.MessageContext::get_ErrorHandler()
extern void MessageContext_get_ErrorHandler_m0F3AAE6195245294654F0498CDACB8A85025515E (void);
// 0x00000122 System.Void SocketIOClient.Processors.MessageContext::set_ErrorHandler(SocketIOClient.OnError)
extern void MessageContext_set_ErrorHandler_mB719E56CD640F6D31920D4BDC78881E204CDB406 (void);
// 0x00000123 SocketIOClient.OnEventReceived SocketIOClient.Processors.MessageContext::get_EventReceivedHandler()
extern void MessageContext_get_EventReceivedHandler_m38D63B5CCE5CFC10C293027D1D57D00159CC3271 (void);
// 0x00000124 System.Void SocketIOClient.Processors.MessageContext::set_EventReceivedHandler(SocketIOClient.OnEventReceived)
extern void MessageContext_set_EventReceivedHandler_mB4A8FDCDE28F26AA7CA29FA5C3008DEF000BD398 (void);
// 0x00000125 SocketIOClient.OnOpened SocketIOClient.Processors.MessageContext::get_OpenedHandler()
extern void MessageContext_get_OpenedHandler_m741322659C5EF94DD0A60F6760767827D05DE6AF (void);
// 0x00000126 System.Void SocketIOClient.Processors.MessageContext::set_OpenedHandler(SocketIOClient.OnOpened)
extern void MessageContext_set_OpenedHandler_mA73C34A0FA50DACF2B13D59906BEF475CD8E21AA (void);
// 0x00000127 SocketIOClient.OnPing SocketIOClient.Processors.MessageContext::get_PingHandler()
extern void MessageContext_get_PingHandler_mE77DB9DE5E924DDB350A673AC7C3D2ABD20544A4 (void);
// 0x00000128 System.Void SocketIOClient.Processors.MessageContext::set_PingHandler(SocketIOClient.OnPing)
extern void MessageContext_set_PingHandler_m9494FBB8A3A79ACF7E5A9EC71E7A817D8AC92584 (void);
// 0x00000129 SocketIOClient.OnPong SocketIOClient.Processors.MessageContext::get_PongHandler()
extern void MessageContext_get_PongHandler_m6544F793D1514F9B1A5CF58443AFF698918098F7 (void);
// 0x0000012A System.Void SocketIOClient.Processors.MessageContext::set_PongHandler(SocketIOClient.OnPong)
extern void MessageContext_set_PongHandler_mED779262E734B78CCF94DFE4F50D39E620EB8C50 (void);
// 0x0000012B System.Void SocketIOClient.Processors.MessageContext::.ctor()
extern void MessageContext__ctor_mB4EDF0D99F773E531BDC37DE691AD3A194A3AC25 (void);
// 0x0000012C System.Void SocketIOClient.Processors.OpenedProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void OpenedProcessor_Process_m6744F93AA71FA58B05BA8C94EEE37FB35CF81C8D (void);
// 0x0000012D System.Void SocketIOClient.Processors.OpenedProcessor::.ctor()
extern void OpenedProcessor__ctor_m1062BE09A3A94B1D69ADAC43A161E40075ECE11D (void);
// 0x0000012E System.Void SocketIOClient.Processors.PingProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void PingProcessor_Process_m380D54D37439E798BDED79F5AE0BAEDD2818B770 (void);
// 0x0000012F System.Void SocketIOClient.Processors.PingProcessor::.ctor()
extern void PingProcessor__ctor_m93A6BDF4A1D67E2F16B645A946761E00F64BBB9B (void);
// 0x00000130 System.Void SocketIOClient.Processors.PongProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void PongProcessor_Process_m9439FFB30CC1E2E2E37AB3DA1FB828973CA06B69 (void);
// 0x00000131 System.Void SocketIOClient.Processors.PongProcessor::.ctor()
extern void PongProcessor__ctor_m182AF8B5735B359F350362D00A3F3A3A33A84335 (void);
// 0x00000132 SocketIOClient.Processors.Processor SocketIOClient.Processors.Processor::get_NextProcessor()
extern void Processor_get_NextProcessor_mBA7E94951BC8BCF6511F753DC98D2B71EC9BCEBB (void);
// 0x00000133 System.Void SocketIOClient.Processors.Processor::set_NextProcessor(SocketIOClient.Processors.Processor)
extern void Processor_set_NextProcessor_m093E51CE164B394C6AF71B5ACA3F4E7F50A20D2B (void);
// 0x00000134 System.Void SocketIOClient.Processors.Processor::Process(SocketIOClient.Processors.MessageContext)
// 0x00000135 System.Void SocketIOClient.Processors.Processor::.ctor()
extern void Processor__ctor_m37E1FCAD932D72A4F18883442C6D01D19A694D67 (void);
// 0x00000136 System.Void SocketIOClient.Processors.SocketIOProtocolProcessor::Process(SocketIOClient.Processors.MessageContext)
extern void SocketIOProtocolProcessor_Process_m561A2CFAAE1F191CDF843542A71929E0AF8B0DD1 (void);
// 0x00000137 System.Void SocketIOClient.Processors.SocketIOProtocolProcessor::.ctor()
extern void SocketIOProtocolProcessor__ctor_m92AEC6B98735B59B238410645DBFA449A4BAB958 (void);
// 0x00000138 System.Void SocketIOClient.JsonSerializer.ByteArrayConverter::.ctor(System.Int32)
extern void ByteArrayConverter__ctor_m380572C505F4C0DA951244270E44D18A4F20EF75 (void);
// 0x00000139 System.Collections.Generic.List`1<System.Byte[]> SocketIOClient.JsonSerializer.ByteArrayConverter::get_Bytes()
extern void ByteArrayConverter_get_Bytes_m750624071A1A35A353F19B39D7D07083782E07D7 (void);
// 0x0000013A System.Byte[] SocketIOClient.JsonSerializer.ByteArrayConverter::Read(System.Text.Json.Utf8JsonReader&,System.Type,System.Text.Json.JsonSerializerOptions)
extern void ByteArrayConverter_Read_m9D7C9DACD2BE9EADEF743AE985F225AE95BA8061 (void);
// 0x0000013B System.Void SocketIOClient.JsonSerializer.ByteArrayConverter::Write(System.Text.Json.Utf8JsonWriter,System.Byte[],System.Text.Json.JsonSerializerOptions)
extern void ByteArrayConverter_Write_m086A1EFF265DC229C1747A6711717EB09FA5F9E1 (void);
// 0x0000013C SocketIOClient.JsonSerializer.JsonSerializeResult SocketIOClient.JsonSerializer.IJsonSerializer::Serialize(System.Object[])
// 0x0000013D T SocketIOClient.JsonSerializer.IJsonSerializer::Deserialize(System.String)
// 0x0000013E T SocketIOClient.JsonSerializer.IJsonSerializer::Deserialize(System.String,System.Collections.Generic.IList`1<System.Byte[]>)
// 0x0000013F System.String SocketIOClient.JsonSerializer.JsonSerializeResult::get_Json()
extern void JsonSerializeResult_get_Json_m231E4CCD6D8D5596D83DF1DE6D75D1F31726E771 (void);
// 0x00000140 System.Void SocketIOClient.JsonSerializer.JsonSerializeResult::set_Json(System.String)
extern void JsonSerializeResult_set_Json_mEE2057FF674C57DEB0B8FF06B16A8CF294708EA6 (void);
// 0x00000141 System.Collections.Generic.IList`1<System.Byte[]> SocketIOClient.JsonSerializer.JsonSerializeResult::get_Bytes()
extern void JsonSerializeResult_get_Bytes_m0E741302DCF243515E5553924DD4A1D5672CC884 (void);
// 0x00000142 System.Void SocketIOClient.JsonSerializer.JsonSerializeResult::set_Bytes(System.Collections.Generic.IList`1<System.Byte[]>)
extern void JsonSerializeResult_set_Bytes_m21B44FD64109783A73527F1FB555C821B2C689FE (void);
// 0x00000143 System.Void SocketIOClient.JsonSerializer.JsonSerializeResult::.ctor()
extern void JsonSerializeResult__ctor_mF92EE011D99AFCB0EE97CB54E6C2E69BE22D70FF (void);
// 0x00000144 System.Void SocketIOClient.JsonSerializer.SystemTextJsonSerializer::.ctor(System.Int32)
extern void SystemTextJsonSerializer__ctor_m21870EFB93950E087BAC022E0BE32BAAF46EB7D5 (void);
// 0x00000145 SocketIOClient.JsonSerializer.JsonSerializeResult SocketIOClient.JsonSerializer.SystemTextJsonSerializer::Serialize(System.Object[])
extern void SystemTextJsonSerializer_Serialize_m3AEE44A88F6DED230751C9C8737D90354257A58F (void);
// 0x00000146 T SocketIOClient.JsonSerializer.SystemTextJsonSerializer::Deserialize(System.String)
// 0x00000147 T SocketIOClient.JsonSerializer.SystemTextJsonSerializer::Deserialize(System.String,System.Collections.Generic.IList`1<System.Byte[]>)
// 0x00000148 System.Text.Json.JsonSerializerOptions SocketIOClient.JsonSerializer.SystemTextJsonSerializer::CreateOptions()
extern void SystemTextJsonSerializer_CreateOptions_mAA88D5D3677D35F661C227121FD6C52FB3F7CD86 (void);
// 0x00000149 System.Void SocketIOClient.Exceptions.InvalidSocketStateException::.ctor()
extern void InvalidSocketStateException__ctor_m49F52C3A078F4C4EA17CD1754C773F1C3EF2CA73 (void);
// 0x0000014A System.Void SocketIOClient.Exceptions.InvalidSocketStateException::.ctor(System.String)
extern void InvalidSocketStateException__ctor_m9B88C392C48CB6E8EBAD69AE5E834B21A8957E19 (void);
// 0x0000014B System.String SocketIOClient.EventArguments.ReceivedEventArgs::get_Event()
extern void ReceivedEventArgs_get_Event_m20173B249611A9DB631C90587FDF0D87FF65E36D (void);
// 0x0000014C System.Void SocketIOClient.EventArguments.ReceivedEventArgs::set_Event(System.String)
extern void ReceivedEventArgs_set_Event_mF71A6535CD595DB4AC9BB9733996E9133293CED3 (void);
// 0x0000014D SocketIOClient.SocketIOResponse SocketIOClient.EventArguments.ReceivedEventArgs::get_Response()
extern void ReceivedEventArgs_get_Response_m007CC3486E75F1541DE952DA9D707704A9A2588F (void);
// 0x0000014E System.Void SocketIOClient.EventArguments.ReceivedEventArgs::set_Response(SocketIOClient.SocketIOResponse)
extern void ReceivedEventArgs_set_Response_m20B3F2D650A80711535BF84BCB8EA15711A5B51B (void);
// 0x0000014F System.Void SocketIOClient.EventArguments.ReceivedEventArgs::.ctor()
extern void ReceivedEventArgs__ctor_m3527DC83F25BF050ECA047ECD080D5025192ADEA (void);
// 0x00000150 System.Boolean SocketIOClient.EioHandler.ConnectionResult::get_Result()
extern void ConnectionResult_get_Result_m570CE16A815E97FFE2801E7A396B40AEC5BC8D64 (void);
// 0x00000151 System.Void SocketIOClient.EioHandler.ConnectionResult::set_Result(System.Boolean)
extern void ConnectionResult_set_Result_m23D8974CD35ABBABC429F6F9484EE2D6904666DF (void);
// 0x00000152 System.String SocketIOClient.EioHandler.ConnectionResult::get_Id()
extern void ConnectionResult_get_Id_mCB8A9592A4A43254DD2FFD9A653D0273AE5FA71D (void);
// 0x00000153 System.Void SocketIOClient.EioHandler.ConnectionResult::set_Id(System.String)
extern void ConnectionResult_set_Id_m7C7DD11566B6D83AA42AC08AB88152CADD3D6AC1 (void);
// 0x00000154 System.Void SocketIOClient.EioHandler.ConnectionResult::.ctor()
extern void ConnectionResult__ctor_m7C09A7F81E2A623243F38114AE9EA403D85BA195 (void);
// 0x00000155 System.String SocketIOClient.EioHandler.Eio3Handler::CreateConnectionMessage(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Eio3Handler_CreateConnectionMessage_mA83DC17D926F2DFF9D0B26AEA51A0830E333D910 (void);
// 0x00000156 SocketIOClient.EioHandler.ConnectionResult SocketIOClient.EioHandler.Eio3Handler::CheckConnection(System.String,System.String)
extern void Eio3Handler_CheckConnection_m069D2487C5B8D18257031B94DE56C22F9260F4DC (void);
// 0x00000157 System.String SocketIOClient.EioHandler.Eio3Handler::GetErrorMessage(System.String)
extern void Eio3Handler_GetErrorMessage_mD004325056FDBBEE2FE48C11DB4AD35F01194C0C (void);
// 0x00000158 System.Byte[] SocketIOClient.EioHandler.Eio3Handler::GetBytes(System.Byte[])
extern void Eio3Handler_GetBytes_m44BE3495EB15C1EF77B91D2D56F131667B371123 (void);
// 0x00000159 System.Void SocketIOClient.EioHandler.Eio3Handler::.ctor()
extern void Eio3Handler__ctor_m1EAEFE08E9D5003D0E73A52614A134123B6914BA (void);
// 0x0000015A System.String SocketIOClient.EioHandler.Eio4Handler::CreateConnectionMessage(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Eio4Handler_CreateConnectionMessage_m536D85C8303DBF02BAC67966C372DAA0E2134A40 (void);
// 0x0000015B SocketIOClient.EioHandler.ConnectionResult SocketIOClient.EioHandler.Eio4Handler::CheckConnection(System.String,System.String)
extern void Eio4Handler_CheckConnection_m784BBFDA7FD7BD433869161322C2D9D06185D7E8 (void);
// 0x0000015C System.String SocketIOClient.EioHandler.Eio4Handler::GetErrorMessage(System.String)
extern void Eio4Handler_GetErrorMessage_mBE5AF1409E7C63C489AAFE1EA25A373C61790F39 (void);
// 0x0000015D System.Byte[] SocketIOClient.EioHandler.Eio4Handler::GetBytes(System.Byte[])
extern void Eio4Handler_GetBytes_mCAE45D0CC9E1EC660FF832F6A668703216BBE56A (void);
// 0x0000015E System.Void SocketIOClient.EioHandler.Eio4Handler::.ctor()
extern void Eio4Handler__ctor_mF8065DD2A5BA41D31FFE7E7AE579D23526240D30 (void);
// 0x0000015F SocketIOClient.EioHandler.IEioHandler SocketIOClient.EioHandler.EioHandlerFactory::GetHandler(System.Int32)
extern void EioHandlerFactory_GetHandler_m2B9AEA8C5400ABF6E6BE1811F0E8AD35EBF5E222 (void);
// 0x00000160 System.String SocketIOClient.EioHandler.IEioHandler::CreateConnectionMessage(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
// 0x00000161 SocketIOClient.EioHandler.ConnectionResult SocketIOClient.EioHandler.IEioHandler::CheckConnection(System.String,System.String)
// 0x00000162 System.String SocketIOClient.EioHandler.IEioHandler::GetErrorMessage(System.String)
// 0x00000163 System.Byte[] SocketIOClient.EioHandler.IEioHandler::GetBytes(System.Byte[])
static Il2CppMethodPointer s_methodPointers[355] = 
{
	OnAnyHandler__ctor_m006530A35A0B24D79F28848298BE88A309627026,
	OnAnyHandler_Invoke_m66BB3729C336ACA085AE7C98FD697B5087640B65,
	OnAnyHandler_BeginInvoke_m70E7A811F81E72E35BAEDBAA60515A48E97CB844,
	OnAnyHandler_EndInvoke_m0F13AF6A59199DC06A3417442C5D3793725DF970,
	OnOpenedHandler__ctor_mA4BF313175A5882D89EC3C6E39190A0417C36ABF,
	OnOpenedHandler_Invoke_m47EC1CE4CEF7CE9D70690754002EA5018FB574BB,
	OnOpenedHandler_BeginInvoke_m16C0E4162D82F3A22B3E27F5EA8CA66E3581A8ED,
	OnOpenedHandler_EndInvoke_mF09080EAD1467FB25D2A48A5632E2C2AAFC9F049,
	OnAck__ctor_m62D1F7214D6D30DF39F4EB6C602C1A338AFD6559,
	OnAck_Invoke_m64F71431CB334746FBA7F93BE59F6104EE5F04A5,
	OnAck_BeginInvoke_m0EBAEB0FE7940A7E0309D563D95544923472A474,
	OnAck_EndInvoke_mA135289B372E98A88A2876E15F9E3BB9D9BDFE26,
	OnBinaryAck__ctor_mCEB13E387F317C3D33F1E4E43C96ABADD7AEC1AE,
	OnBinaryAck_Invoke_m0206A595715D3F4876717B984DCCFC3699F8F491,
	OnBinaryAck_BeginInvoke_m55E7598015620C39E28AB86549FE630838050900,
	OnBinaryAck_EndInvoke_m2F58E81655D872E9806CBB98481661E80D4D298E,
	OnBinaryReceived__ctor_mE41839448521ED17278FEDCB4D7A2BAA26D804F6,
	OnBinaryReceived_Invoke_mEB2B314E85AEA8DF888512A103F747B6F23C43AA,
	OnBinaryReceived_BeginInvoke_m6B1848D85F526D872599EE5AADD2DF266CA14585,
	OnBinaryReceived_EndInvoke_mB0B9E1CB3C2C2BDC5AABD1F83BC53230175A26EE,
	OnDisconnected__ctor_mA111916B7799F340A6D19B5DA4C261D1E19E300F,
	OnDisconnected_Invoke_m91E9D1ABD233056CFFF885C372BC2021DD2D29AD,
	OnDisconnected_BeginInvoke_m6EB042A373C70D032CD7167DCE860712F9FB503A,
	OnDisconnected_EndInvoke_m107E09DFB27BDDA19AB8335DDFBE005F15E776A2,
	OnError__ctor_m42A8A9BA4685F1DD8D4AD64B40B6588F9F019A1C,
	OnError_Invoke_m305EE33A7FBDAED9C7281128418CF3B42ED51F70,
	OnError_BeginInvoke_m0C3E903FF7B9E0D49FFACF13D1A3013629D50E1F,
	OnError_EndInvoke_m44CD4C92C247556D28793B42469BEDA52CA739CF,
	OnEventReceived__ctor_m6F1D0FAD0D2A51C40808CA2095708924F0850334,
	OnEventReceived_Invoke_mBB250FDCA59BB90CF5720FACBB5528F73FC481C0,
	OnEventReceived_BeginInvoke_m5CB205213420F394F49C9600FA83547E5C9B07E8,
	OnEventReceived_EndInvoke_m66A58F344876BFEF6937CBBB6D03EDCF12F18CF0,
	OnOpened__ctor_mA6257A95CBEE96ADDCE4576E82C8EBF70C83EC62,
	OnOpened_Invoke_m86DDE326D4E119A85DB9917AE89C48C6E254D093,
	OnOpened_BeginInvoke_mB81DB59A775781F4E595AEBF30CFEF8F9DA0ED1B,
	OnOpened_EndInvoke_m0C9A21406FC7E55C5070610D8DD425FC49905C3A,
	OnPing__ctor_mD3C5DB5FA44BF86B6ECBA934D20808CF27185A4C,
	OnPing_Invoke_mE88346B0642CAEBC48A42828744684B12D5880C9,
	OnPing_BeginInvoke_m479341279E883BC7BD6F97A6DCBC3FB556BBF638,
	OnPing_EndInvoke_mACF497902344E98501F2B3C03AC3026EE766D59B,
	OnPong__ctor_mE1373FCF7D02686BB62317F72920ED1104384BAE,
	OnPong_Invoke_mCCDAA3313FB8904767CC436745C117E57F27F0B5,
	OnPong_BeginInvoke_mDC1AF8EBD3E0238320F50274F063A05934766FF2,
	OnPong_EndInvoke_m4F5DE92660B85981BB60F7728DDB31640A695785,
	LowLevelEvent__ctor_m848A81195E756F82ECA25524571553ADAE8F77F1,
	LowLevelEvent_get_PacketId_m2DB32A0587A1A7950B4253BF3342D47A85DF9DA0,
	LowLevelEvent_set_PacketId_mA4B611F54BEE19B5D65593FD75E85E72A72C5C31,
	LowLevelEvent_get_Event_m46901F9A2212DC0A6522C5118C3681492119CB06,
	LowLevelEvent_set_Event_m94E56D725D55A9F726AD975441D928487C5736DD,
	LowLevelEvent_get_Count_m2A6FA153C22F0CC513E3CCFA96DC9517A2B41B9E,
	LowLevelEvent_set_Count_m920B4DD2D447B9FD77E0446BB4EED486071F4923,
	LowLevelEvent_get_Response_mC0BED39B2BE0E38D009C67CCAE2ACFE1D950891F,
	LowLevelEvent_set_Response_m0E31CD2E90B6140E8C520F27C434F5B8448FE775,
	SocketIO__ctor_mAAA87CF36CAA39F99A889CD98BAEA0688DA2EACF,
	SocketIO__ctor_mBEC367EDEC42A2621370BE76C0391811F46EBC74,
	SocketIO__ctor_mC23516A8CD30540E46DB36C2622CAF253AA89E65,
	SocketIO__ctor_mAC0EFAF950869BC7D4CCB08E0AF6FBB6E1E2C6A5,
	SocketIO__ctor_m622409BFD9E36D54CD36230FF6201DCB22993F69,
	SocketIO_get_ServerUri_m38FDCEEE84C3E2AFA3CE3F76BC3297FF1EFED8E0,
	SocketIO_set_ServerUri_m41BE3B745754D77C2A539C831F42067C62D2D0A8,
	SocketIO_get_UrlConverter_mB9363AA9DB3591DF98CC866896BFADA2BB5CD556,
	SocketIO_set_UrlConverter_m087BED398453E98F240B5AA1E201F077CC3E75BD,
	SocketIO_get_Socket_m7FC7ABF2CE1F9993E6E18AB31FE29DD1DDE1B407,
	SocketIO_set_Socket_m9FE4712FBC1A5100818D88F641F17D5BA4A05334,
	SocketIO_get_MessageProcessor_m437A4557CBAD44844B079FB31A989F2E0A9E0B6C,
	SocketIO_set_MessageProcessor_m96BCD337497E42D632F91D7F472B84F5AAA50E96,
	SocketIO_get_Id_m96D67B40C9CE00A1B252091917360C3339FEBB20,
	SocketIO_set_Id_mD0DB47E4F716FB18F7687E9945480528BC8AC624,
	SocketIO_get_Namespace_m948F5002C7043B7F3363671E09C6B465A20BEAD9,
	SocketIO_set_Namespace_m43D39B2023AE1CC98B396850E9BC9D9954835A3E,
	SocketIO_get_Connected_m96098A9D67228D3C6DC90FC4DE28D405D32AB637,
	SocketIO_set_Connected_m10B84B1D4B153A6159B8040A5010423D3D4AB5AE,
	SocketIO_get_Attempts_m9477FCEFA9819A91A3BB977A2F24B6D9A6BD5C7C,
	SocketIO_set_Attempts_m976DCA02C277679FECC3D23E4013539918BCE34E,
	SocketIO_get_Disconnected_m36A50A318CDEAC3B022837E3F34479D04364955F,
	SocketIO_set_Disconnected_m39919C7AED5C2100861DFF3A18E79983C6969DDC,
	SocketIO_get_Options_mA96C1CC131085E59B69FEB406BD8B71D4FC6B724,
	SocketIO_get_JsonSerializer_m90E64B118772E0CE3D11F565E1DB4B6512ED15B2,
	SocketIO_set_JsonSerializer_mA016B71F14953C0D2C55850F1667165338C9BD41,
	SocketIO_add_OnConnected_m9D658BABFE49672A000ABB70B215ED9827C73C79,
	SocketIO_remove_OnConnected_mFF8EA7BD1F0FBF1FEED9B354D9D8963E7780DA3E,
	SocketIO_add_OnError_mED5F759501E6C56FE3C59199450F3A7BC3BB2ED5,
	SocketIO_remove_OnError_m4B5E1E431F80954D284FFB5E81385FB78F2BF46E,
	SocketIO_add_OnDisconnected_mEFB3A61F0C996ADC9D3EC57FF748EB4B1A64A9B2,
	SocketIO_remove_OnDisconnected_mCB0605263C79308B0F29EFE804E75C3BD4ABA375,
	SocketIO_add_OnReconnected_mDCDCDDE123A5950E636CB041AE7382B50803497E,
	SocketIO_remove_OnReconnected_mA53587D5D525ED2BDBA5F65B8355EDEE79BDCC68,
	SocketIO_add_OnReconnectAttempt_m372A2FB7EEB3BCE097BDB69DFB2C51544BFCB60E,
	SocketIO_remove_OnReconnectAttempt_m2BE91C7002382E9A4E17E8A17F61F6EB1DE8D375,
	SocketIO_add_OnReconnectError_mAF4C3C0EE5B24159136E224C9FA3C03D13F90F81,
	SocketIO_remove_OnReconnectError_mAE1A10D96D5CFB73822421EF0D60462595ADB0C3,
	SocketIO_add_OnReconnectFailed_mD69A78E5D73C10E184C1A7D6CF3E42FA00523DCE,
	SocketIO_remove_OnReconnectFailed_m79ADBA036C81273FF3B009FE661E61CFEFF7D667,
	SocketIO_add_OnPing_mB304839625FE70CCC198F4114443FFEEED3C55F5,
	SocketIO_remove_OnPing_m2C52C490C812328904B019B7FD4973E68E2C6009,
	SocketIO_add_OnPong_m63722B27C2CE9A0445866A56836FF8D1E7AF8A83,
	SocketIO_remove_OnPong_m3ECD7031F62DC3710AFE23B8694FD3896D89F8A9,
	SocketIO_Initialize_mAFD7A258BD6120302E2D2E2CF4A24AC9D2DC7827,
	SocketIO_ConnectAsync_m30D6A30EDEF8DAA3AD16DC0477C480EF6C94522E,
	SocketIO_DisconnectAsync_m30DFB925AFB88E6A80459BC22F2A430C3ABB5757,
	SocketIO_On_mFE240648DFCCE0AF026CE1B79D426685FD01B218,
	SocketIO_Off_m53BB39BB64ACBE002C0A7C9ED8492889893B13A9,
	SocketIO_OnAny_m7E7DB74C6CBEBD9A7675F3D45E8BC5B7773EEEF9,
	SocketIO_PrependAny_mEEDE07FB210BEBF3F84E52AC0866A0B4224CDF9A,
	SocketIO_OffAny_mCA42D44F885991BBC16A1CBAE5ABFB61704DE639,
	SocketIO_ListenersAny_mEFE8C31C148CB8C7CFD76DB54CC91B2F984FD701,
	SocketIO_EmityCoreAsync_m6DB2301FD1B18DEAAE1DADD64237807C32DED6D0,
	SocketIO_EmitCallbackAsync_m662757301A057723FE2C9C71743B630CBC6F4925,
	SocketIO_GetDataString_mCD5D1A56DD7F04234BAE60B6958AD1F1F02C1BC5,
	SocketIO_EmitAsync_m982F2923F633EDDD0AD9DA1AD7754E3BCC52E9FF,
	SocketIO_EmitAsync_m4B25FC0E4EF27138D79EFA3053DD4B9F276070C6,
	SocketIO_EmitAsync_mBF5C618E1D4C0B494187C8A3139AAC2E6CC41CA9,
	SocketIO_EmitAsync_mD020CE38BF6170FFCBACFDE7C47C7087456A76FD,
	SocketIO_OnTextReceived_m20CC6BE3956E34EA9C9789048C73B3D4BE4A2C73,
	SocketIO_OnBinaryReceived_m1280D42929DF54865FD08FB044BC4354076DD217,
	SocketIO_OnClosed_mA680F7D9EFE2BE2BEBE34AEF1CE4085888A72CDB,
	SocketIO_OpenedHandler_m3D53F586CA653CF53D85A9E9C8907B8D1AAA3246,
	SocketIO_ConnectedHandler_m5976BCF6301B1E6F9597AA15EA9416AACD48859C,
	SocketIO_AckHandler_m47EAE6DBD96596740B3D210578F6AD0F4F0FEE02,
	SocketIO_BinaryAckHandler_mFC4C716EEA2125AADC9E8121183BB419A6040AED,
	SocketIO_BinaryReceivedHandler_mBE0A813754295B3062A63056C4A4A99F95A89821,
	SocketIO_DisconnectedHandler_mF19D962F9CB4C10450C6E8B275091A89402F1A94,
	SocketIO_ErrorHandler_m3BD6210E590FF0CBCEC913A6E4E26F1C684B777D,
	SocketIO_EventReceivedHandler_mCFDBBE4D572CCB0EF1B6841179B075B405B767D5,
	SocketIO_PingHandler_m884E9C2E0035DC30DB3B481F8A2E67250BB273EC,
	SocketIO_PongHandler_m9F82BF1D7D6DEAE82BFFC08291C9AA15E8EDBCF5,
	SocketIO_InvokeDisconnectAsync_mD2E2A0D695F1617359BF4AB559C03057D19C706C,
	SocketIO_StartPingAsync_m3A3381954FDABF8D1EB6FF103E4F15CF3A6A737B,
	SocketIO_StopPingInterval_m419931B18A8AE39AEA1A1D087F7671FAE7CFAFE4,
	SocketIO_Dispose_m1D634EC2959B5E13292E20A8D5B2DCA3FDFEF108,
	U3CConnectAsyncU3Ed__87_MoveNext_m2D026BAA9B7DEADEB9EC6105F0FB51C4164D92BD,
	U3CConnectAsyncU3Ed__87_SetStateMachine_mC3988BF8B083971FDAB815D1DE6D8B62A25B5E31,
	U3CDisconnectAsyncU3Ed__88_MoveNext_m4EFECF9406351B59E63A3CC50813ABD6F2F2B85D,
	U3CDisconnectAsyncU3Ed__88_SetStateMachine_m5882ADA1C963C1DD9ED2342145438212C4C2F555,
	U3CEmityCoreAsyncU3Ed__95_MoveNext_mAA824FA6D9C966C0D82F04A3933375E78F55379D,
	U3CEmityCoreAsyncU3Ed__95_SetStateMachine_m195AD76D4DD1A13864C143C2357FE687D4FD293C,
	U3CEmitCallbackAsyncU3Ed__96_MoveNext_m10AD1445FB9AC7FC933B465824E6072793915947,
	U3CEmitCallbackAsyncU3Ed__96_SetStateMachine_m8A68E90477DDA2E92DCAA6AE5CE470FB49B75CEC,
	U3CEmitAsyncU3Ed__98_MoveNext_mEF3D565B8F4BEBCDC3686B3E999F9C833DBAB87D,
	U3CEmitAsyncU3Ed__98_SetStateMachine_mA919980759FA57B357F9BBAB2489F57EE7BF3DA4,
	U3CEmitAsyncU3Ed__99_MoveNext_mCDD6CA506FF87040A9F61A8846A8EA420E19EF2D,
	U3CEmitAsyncU3Ed__99_SetStateMachine_m3BE8D82221662C9C7F3FBB307B6ECCE62BABF11C,
	U3CEmitAsyncU3Ed__100_MoveNext_mEC42B49D4F13D2E1C5029F45E0CF84086AB0986E,
	U3CEmitAsyncU3Ed__100_SetStateMachine_m2D3248E4F0000599C77082CC1EDB814A844AA1B3,
	U3CEmitAsyncU3Ed__101_MoveNext_mC58A148714F7E77D70678DC32EAA2097111A8393,
	U3CEmitAsyncU3Ed__101_SetStateMachine_m1C2F4C27BBB76FEE1D88338A56A79471D34FB72A,
	U3COnClosedU3Ed__104_MoveNext_mF948C63F330E6DFADBCDE9D93FD410D35C192F64,
	U3COnClosedU3Ed__104_SetStateMachine_mFDE3990A55091422819042018614A7F1D284FCA6,
	U3COpenedHandlerU3Ed__105_MoveNext_m68D99B43F82AE2000B9DC426C5642D63C22875BB,
	U3COpenedHandlerU3Ed__105_SetStateMachine_mAFD0F42959271BC191F3AFD0B4854AB646A76FED,
	U3CDisconnectedHandlerU3Ed__110_MoveNext_m00D4848F9F9144B342A5837BAD2D02218D4C84BA,
	U3CDisconnectedHandlerU3Ed__110_SetStateMachine_m61BF3EC1399B740FF5D43784DB64D7EE77B3CEA2,
	U3CPingHandlerU3Ed__113_MoveNext_mFBD465A826EAFB3A03BF248B971F7CDA777956CE,
	U3CPingHandlerU3Ed__113_SetStateMachine_mB1685B3B4D5FABFDB12A135DF76D5B9ED3F22AC8,
	U3CInvokeDisconnectAsyncU3Ed__115_MoveNext_m1EB2D6FE144438070F4222B20DE2B17B9A08A0F4,
	U3CInvokeDisconnectAsyncU3Ed__115_SetStateMachine_mE5373AA733E610BF6911F0BD1B77AF28188C2A16,
	U3CStartPingAsyncU3Ed__116_MoveNext_m840437D67141837C2DAFE0E9F5DD1BD639C522E4,
	U3CStartPingAsyncU3Ed__116_SetStateMachine_m7B3C285ADAB5FF332FD0A98EA9269899262D388A,
	SocketIOOptions__ctor_m06916B51C57F7FF8822597396952026FFEF4776E,
	SocketIOOptions_get_Path_mBCCCFD05D277C258FE42404F5635018AE9932BE7,
	SocketIOOptions_set_Path_mEFEE8854DF82E9A966DBA936DABBFDF233B96514,
	SocketIOOptions_get_ConnectionTimeout_mCFC0E37381E689D7DA3EC99BC2573DEF87225926,
	SocketIOOptions_set_ConnectionTimeout_mCCF4559254F56A26DE6E53AB39EBA32933935C1A,
	SocketIOOptions_get_Query_m17B3B19930A8BEE2C662EE505BC020ADD646BE7C,
	SocketIOOptions_set_Query_m46C7820644E98AE3061394F4F7922E4707ED1615,
	SocketIOOptions_get_Reconnection_mC29D95BCD71A21C4943C3163D460CAA84356C570,
	SocketIOOptions_set_Reconnection_m54AEADFFE628856F826A417F870BE8DF278B0F83,
	SocketIOOptions_get_ReconnectionDelay_m4040719E46D879DFA7CE3F9CA425058FB1DAE407,
	SocketIOOptions_set_ReconnectionDelay_m935171E3E95AAB83AB0FA892F881DA39355345EF,
	SocketIOOptions_get_ReconnectionDelayMax_mCD78CDCAAE861B6ED9587BF611A7B944EC5A4C19,
	SocketIOOptions_set_ReconnectionDelayMax_mD09CA48DBA4DAA2AE9A4ED350E56272BD9AEC1A7,
	SocketIOOptions_get_ReconnectionAttempts_m1DBF68BA62FB07798A6090AF66FC91F6EBEE864A,
	SocketIOOptions_set_ReconnectionAttempts_m2A7A36DC8BA1CFC319A244EE23541DC1C00BC663,
	SocketIOOptions_get_RandomizationFactor_m65360C0C600837F694A627BFFD5006BE4A27FF1A,
	SocketIOOptions_set_RandomizationFactor_mC1A8CB61618E370F4C3FCFD65D1EC6B7D22A8992,
	SocketIOOptions_get_EIO_m7D2D358521675A36258DCAFB56E7D241B60F9507,
	SocketIOOptions_set_EIO_m2392652E0EA226FD23A7CEECF7D3CF86548E249D,
	SocketIOOptions_get_EioHandler_m2FDA56CF08B8E84D0FA2676EF5DAC0025C309685,
	SocketIOOptions_set_EioHandler_m00AF61CDD670982F7E6173C7286B562C26826032,
	SocketIOResponse__ctor_m8D7582B380A52CFAF3B3303DCC2357361C178C8B,
	SocketIOResponse_get_InComingBytes_m6BCBB46E08477143008307B876D7F6F57B037CD7,
	SocketIOResponse_get_SocketIO_m66CB8A47AC82FBD8A049153DADD30A9F9D9055A0,
	SocketIOResponse_get_PacketId_m0AA89D03758291ACC1BA35E9EFA160FD81A0818C,
	SocketIOResponse_set_PacketId_mFF20A70DCB0825C3610A460534EDE0E32A97C6A6,
	NULL,
	SocketIOResponse_GetValue_m8CDEAB86046C6ABEFECDB2D5AAA602E6951DA38D,
	SocketIOResponse_get_Count_m9501A452371B71F429B3E5D931350F9B200E7A93,
	SocketIOResponse_ToString_mD3EEF3B6DEF88A9BD6EC4C59CCDA434112E5D6B5,
	SocketIOResponse_CallbackAsync_mA5B0A112375DF655574CBFF1EE2C70656AF14365,
	U3CCallbackAsyncU3Ed__17_MoveNext_mE6BCFAB6E3556FA96926C61F3DB217FEC67FA0C4,
	U3CCallbackAsyncU3Ed__17_SetStateMachine_m061202E8AD2408AAA14AB0710F9EB5EC4D732E8E,
	UrlConverter_HttpToWs_mF16F1EB20B072DD4A66C462F8CEFC23CF88F0A85,
	UrlConverter__ctor_m5734E379DB688FBCB799D46EE89FFF8CA4F8901D,
	ClientWebSocket__ctor_mA1827C0AB87CBFB6F0D1C717BF1ECBF6ED924A99,
	ClientWebSocket_get_ReceiveChunkSize_m7749B33938398A3872F179980B0873758B3FBFA9,
	ClientWebSocket_set_ReceiveChunkSize_mDC65B177200EEF759E5C57600171F794E04E435A,
	ClientWebSocket_get_ConnectionTimeout_mD1E381ADF074E80D85DCCD422A85B26542FE1E51,
	ClientWebSocket_set_ConnectionTimeout_m35B7C9A67E8D749C5E7930DF2C3D502A221B265A,
	ClientWebSocket_get_Config_m5960E6BF9CA8C8BAFB1AF3DBFCF518F36ABDC89B,
	ClientWebSocket_set_Config_mD4AC6ACA28EB38E92896768434C4F3B38FC24EE2,
	ClientWebSocket_ConnectAsync_mF911B0CF25FFCB1B4BB9D858C7C1BC5A15D741CB,
	ClientWebSocket_SendMessageAsync_m311F5F0039B8224F90DAD91C5E1EA0992C25FA4D,
	ClientWebSocket_SendMessageAsync_mCABC14DC46E337DBD5AF0EA89F7E87035213877E,
	ClientWebSocket_SendMessageAsync_m21FA6EF4E4B2B9B188736980F7784B052AA4AF55,
	ClientWebSocket_SendMessageAsync_m601F6BEC7EA9D8B0229B062C4842A8CF53CBC040,
	ClientWebSocket_DisconnectAsync_mBE6A6FF743EE95E7D3D837BDC1D1F14CEBC386AC,
	ClientWebSocket_ListenAsync_mF736584345F5866A122FF3FEBC0C69DB57BD7CB9,
	ClientWebSocket_get_OnTextReceived_mCD4C9CDB5F1499A17ECBC34C258F5BA81DE4EA1E,
	ClientWebSocket_set_OnTextReceived_mCBDDBD288AC1865AB45553B5B9936E1BD00AE6C7,
	ClientWebSocket_get_OnBinaryReceived_mB55D86EFA0F83FC6E1D3BA4F03EF2D5C6C2A2DF7,
	ClientWebSocket_set_OnBinaryReceived_m5FC20F2AEC0774E90F80172D9CB3B533A2CD1FBA,
	ClientWebSocket_get_OnClosed_m3F95A32F74877FA253A43BE7914B539925E60F2C,
	ClientWebSocket_set_OnClosed_m0B81795128A5F276D80D5DAFE6E826EA3B519B51,
	ClientWebSocket_DisposeWebSocketIfNotNull_m780A5705D8E3084FF4C7FBA84006E22A6A1AC27F,
	ClientWebSocket_DisposeListenTokenIfNotNull_m35861CEA7EBB2BBC42BC4A02E0FC47D592E1871E,
	ClientWebSocket_Dispose_m51772362D0A2C24748031154C289626EAD941AFB,
	U3CConnectAsyncU3Ed__16_MoveNext_m12909D8FA0B7EF8A1A0546A68DCDD24DE4EB381F,
	U3CConnectAsyncU3Ed__16_SetStateMachine_m2831C8741307B31A1F1BF32577473F8673108E95,
	U3CSendMessageAsyncU3Ed__17_MoveNext_m987B5116A558F40C630039C42CBA359CC928F3AD,
	U3CSendMessageAsyncU3Ed__17_SetStateMachine_mD254A3520A1B161932C624CC1275C77AFB161EFD,
	U3CSendMessageAsyncU3Ed__18_MoveNext_m970AA126DE68215A0CD7F52C19C7184381DF760E,
	U3CSendMessageAsyncU3Ed__18_SetStateMachine_m253767F4F7CF7F0A6A373671B446C9B9CC4FC2F1,
	U3CSendMessageAsyncU3Ed__19_MoveNext_mD0CC44463FC3C8613F58BC52AF0785294C9505D5,
	U3CSendMessageAsyncU3Ed__19_SetStateMachine_mC3109B6EAF527C0B0BC16768EC3872E32E3D2033,
	U3CSendMessageAsyncU3Ed__20_MoveNext_mE769EA29262903DC77094E30C2E9D764A511DB9C,
	U3CSendMessageAsyncU3Ed__20_SetStateMachine_m9E7ED77807BDD4FBF17C4CF9BA4C940AFA44F2FE,
	U3CDisconnectAsyncU3Ed__21_MoveNext_mBC523949C0CC63F3B76A484987411F8D06C8D0B8,
	U3CDisconnectAsyncU3Ed__21_SetStateMachine_mBDF93F239C6BAEAB9C864CAF33087A49D951131A,
	U3CListenAsyncU3Ed__22_MoveNext_m8982BFD36200B01CFA1624FA97F1C0EEF6E97F5D,
	U3CListenAsyncU3Ed__22_SetStateMachine_m1B6E29D2A7E1528746774F705539DB49444FC347,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OpenSerializer_Read_mE9ECE57D1494309ECA7BD9F72106457F6A8F93EF,
	OpenSerializer_Write_m63ADB4FD96ADB729DFD1D459D795F5CB5BA0336E,
	OpenSerializer__ctor_mAD610640F85DD9060FA31E793C7C8AAC18CA1F3B,
	OpenResponse_get_Sid_m4AD9365BE2B2376B18B952A437A374716DDD5BEE,
	OpenResponse_set_Sid_m31DDE3B566F8001C38ECFE2E1286F44E9FC97331,
	OpenResponse_get_PingInterval_mDE01940DFC5D5BEAA0DE568A0ADFF22B3C48898E,
	OpenResponse_set_PingInterval_mC4AC124E92F257CABADA4D7B9EC9B12B3B8B7BAE,
	OpenResponse_get_PingTimeout_m13757B0A9DF82264DD45B7532696A59505EA35E7,
	OpenResponse_set_PingTimeout_mAAA7816FCA4849294F6B682A790EEA108ADD1899,
	OpenResponse__ctor_mF22493984ED552162DA76BBB74A2296722E42DC7,
	AckProcessor_Process_mF07E27C9D1327C14DA7A1E52D6B9FDE607FD4CC8,
	AckProcessor__ctor_m2947222CD896DB37F9256C24A048F6509332106D,
	BinaryAckProcessor_Process_m4B0EA08831971EA7CB3D9DA613D9971FD48C10BB,
	BinaryAckProcessor__ctor_mE00D4B4B1F68A3C924F1998C21D080C0AE74C1C6,
	BinaryEventProcessor_Process_m6B3D8CC22A1C88D262508E8B6F048F4A7134E0A8,
	BinaryEventProcessor__ctor_mFDBD51BCDEC7EC93C4C20E4D110C7E99334173F3,
	ConnectedProcessor_Process_m3722C902BCDB1FD3813AB30091F1E65BAE216D46,
	ConnectedProcessor__ctor_m1F5EABDE471A0F92758AB2E959C992E568BD0275,
	DisconnectedProcessor_Process_m2E26A08A641B9E529592C305AA6F520F523F5D3D,
	DisconnectedProcessor__ctor_m3E1066731368F107C71FAC4F7046FAD8FBD70BA8,
	EngineIOProtocolProcessor_Process_m5D6E08ADF3A1826D1631C18E1B1093A7C79D4493,
	EngineIOProtocolProcessor__ctor_mF3B7CC7579414874A972335F972B8321F983C80B,
	ErrorProcessor_Process_m7F154B4926DE2742B6193BC4F8439A98979617FC,
	ErrorProcessor__ctor_m6AEC45F4CEB4FA6EF7B6B354082F333C9A832AB5,
	EventProcessor_Process_m89A74D1018DAF5B7DEF6F60720663B250F43DD56,
	EventProcessor__ctor_mB05B5CBC45E4F80A362B49E2C92BBB1B5B4AE731,
	MessageContext_get_Message_m71E856DCDFD208F0E2F2606C87B4C09A62394046,
	MessageContext_set_Message_m9B61EF45207C592C44C4833A416501132B088BB3,
	MessageContext_get_Namespace_m97E935E1804A8E9EAAE4959A1B2B8755306ED809,
	MessageContext_set_Namespace_m457805FBE6F1CD2169996314D38A0D9DB868E5BC,
	MessageContext_get_EioHandler_m85F739B9876E5D4B0A3ED189FF6EDC57E0E08204,
	MessageContext_set_EioHandler_mAE568D6AE500A8C187647D0A2CDDEDE5085F6523,
	MessageContext_get_ConnectedHandler_m6BCF66F7AE523FB9BF65E916B51B97F282003CE2,
	MessageContext_set_ConnectedHandler_mDCC20F6C69421241CDA16D87EA3E8886699A851B,
	MessageContext_get_AckHandler_mCDCCE8ED4018C77FD067C7165E388AB74FD2A42A,
	MessageContext_set_AckHandler_m45F9000D43D56B45C6C989B7E2CED97FD84F7E0E,
	MessageContext_get_BinaryAckHandler_m4222EFC136B072544CE07357F85500E699F689F3,
	MessageContext_set_BinaryAckHandler_m800A90E331A9E0011E15FB25F4F3342A3ABA276B,
	MessageContext_get_BinaryReceivedHandler_m773BC296E59FCC6384E4DBEF06BE047174B24A3D,
	MessageContext_set_BinaryReceivedHandler_mEBD4FE0D0DCB4E7D686673FA817831B619526A43,
	MessageContext_get_DisconnectedHandler_m445661F3962662A3414518F8A1DCB64A25AF0546,
	MessageContext_set_DisconnectedHandler_m77579D5BB89F09615A9234277C1D8B28D9B1AEEF,
	MessageContext_get_ErrorHandler_m0F3AAE6195245294654F0498CDACB8A85025515E,
	MessageContext_set_ErrorHandler_mB719E56CD640F6D31920D4BDC78881E204CDB406,
	MessageContext_get_EventReceivedHandler_m38D63B5CCE5CFC10C293027D1D57D00159CC3271,
	MessageContext_set_EventReceivedHandler_mB4A8FDCDE28F26AA7CA29FA5C3008DEF000BD398,
	MessageContext_get_OpenedHandler_m741322659C5EF94DD0A60F6760767827D05DE6AF,
	MessageContext_set_OpenedHandler_mA73C34A0FA50DACF2B13D59906BEF475CD8E21AA,
	MessageContext_get_PingHandler_mE77DB9DE5E924DDB350A673AC7C3D2ABD20544A4,
	MessageContext_set_PingHandler_m9494FBB8A3A79ACF7E5A9EC71E7A817D8AC92584,
	MessageContext_get_PongHandler_m6544F793D1514F9B1A5CF58443AFF698918098F7,
	MessageContext_set_PongHandler_mED779262E734B78CCF94DFE4F50D39E620EB8C50,
	MessageContext__ctor_mB4EDF0D99F773E531BDC37DE691AD3A194A3AC25,
	OpenedProcessor_Process_m6744F93AA71FA58B05BA8C94EEE37FB35CF81C8D,
	OpenedProcessor__ctor_m1062BE09A3A94B1D69ADAC43A161E40075ECE11D,
	PingProcessor_Process_m380D54D37439E798BDED79F5AE0BAEDD2818B770,
	PingProcessor__ctor_m93A6BDF4A1D67E2F16B645A946761E00F64BBB9B,
	PongProcessor_Process_m9439FFB30CC1E2E2E37AB3DA1FB828973CA06B69,
	PongProcessor__ctor_m182AF8B5735B359F350362D00A3F3A3A33A84335,
	Processor_get_NextProcessor_mBA7E94951BC8BCF6511F753DC98D2B71EC9BCEBB,
	Processor_set_NextProcessor_m093E51CE164B394C6AF71B5ACA3F4E7F50A20D2B,
	NULL,
	Processor__ctor_m37E1FCAD932D72A4F18883442C6D01D19A694D67,
	SocketIOProtocolProcessor_Process_m561A2CFAAE1F191CDF843542A71929E0AF8B0DD1,
	SocketIOProtocolProcessor__ctor_m92AEC6B98735B59B238410645DBFA449A4BAB958,
	ByteArrayConverter__ctor_m380572C505F4C0DA951244270E44D18A4F20EF75,
	ByteArrayConverter_get_Bytes_m750624071A1A35A353F19B39D7D07083782E07D7,
	ByteArrayConverter_Read_m9D7C9DACD2BE9EADEF743AE985F225AE95BA8061,
	ByteArrayConverter_Write_m086A1EFF265DC229C1747A6711717EB09FA5F9E1,
	NULL,
	NULL,
	NULL,
	JsonSerializeResult_get_Json_m231E4CCD6D8D5596D83DF1DE6D75D1F31726E771,
	JsonSerializeResult_set_Json_mEE2057FF674C57DEB0B8FF06B16A8CF294708EA6,
	JsonSerializeResult_get_Bytes_m0E741302DCF243515E5553924DD4A1D5672CC884,
	JsonSerializeResult_set_Bytes_m21B44FD64109783A73527F1FB555C821B2C689FE,
	JsonSerializeResult__ctor_mF92EE011D99AFCB0EE97CB54E6C2E69BE22D70FF,
	SystemTextJsonSerializer__ctor_m21870EFB93950E087BAC022E0BE32BAAF46EB7D5,
	SystemTextJsonSerializer_Serialize_m3AEE44A88F6DED230751C9C8737D90354257A58F,
	NULL,
	NULL,
	SystemTextJsonSerializer_CreateOptions_mAA88D5D3677D35F661C227121FD6C52FB3F7CD86,
	InvalidSocketStateException__ctor_m49F52C3A078F4C4EA17CD1754C773F1C3EF2CA73,
	InvalidSocketStateException__ctor_m9B88C392C48CB6E8EBAD69AE5E834B21A8957E19,
	ReceivedEventArgs_get_Event_m20173B249611A9DB631C90587FDF0D87FF65E36D,
	ReceivedEventArgs_set_Event_mF71A6535CD595DB4AC9BB9733996E9133293CED3,
	ReceivedEventArgs_get_Response_m007CC3486E75F1541DE952DA9D707704A9A2588F,
	ReceivedEventArgs_set_Response_m20B3F2D650A80711535BF84BCB8EA15711A5B51B,
	ReceivedEventArgs__ctor_m3527DC83F25BF050ECA047ECD080D5025192ADEA,
	ConnectionResult_get_Result_m570CE16A815E97FFE2801E7A396B40AEC5BC8D64,
	ConnectionResult_set_Result_m23D8974CD35ABBABC429F6F9484EE2D6904666DF,
	ConnectionResult_get_Id_mCB8A9592A4A43254DD2FFD9A653D0273AE5FA71D,
	ConnectionResult_set_Id_m7C7DD11566B6D83AA42AC08AB88152CADD3D6AC1,
	ConnectionResult__ctor_m7C09A7F81E2A623243F38114AE9EA403D85BA195,
	Eio3Handler_CreateConnectionMessage_mA83DC17D926F2DFF9D0B26AEA51A0830E333D910,
	Eio3Handler_CheckConnection_m069D2487C5B8D18257031B94DE56C22F9260F4DC,
	Eio3Handler_GetErrorMessage_mD004325056FDBBEE2FE48C11DB4AD35F01194C0C,
	Eio3Handler_GetBytes_m44BE3495EB15C1EF77B91D2D56F131667B371123,
	Eio3Handler__ctor_m1EAEFE08E9D5003D0E73A52614A134123B6914BA,
	Eio4Handler_CreateConnectionMessage_m536D85C8303DBF02BAC67966C372DAA0E2134A40,
	Eio4Handler_CheckConnection_m784BBFDA7FD7BD433869161322C2D9D06185D7E8,
	Eio4Handler_GetErrorMessage_mBE5AF1409E7C63C489AAFE1EA25A373C61790F39,
	Eio4Handler_GetBytes_mCAE45D0CC9E1EC660FF832F6A668703216BBE56A,
	Eio4Handler__ctor_mF8065DD2A5BA41D31FFE7E7AE579D23526240D30,
	EioHandlerFactory_GetHandler_m2B9AEA8C5400ABF6E6BE1811F0E8AD35EBF5E222,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void U3CConnectAsyncU3Ed__87_MoveNext_m2D026BAA9B7DEADEB9EC6105F0FB51C4164D92BD_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__87_SetStateMachine_mC3988BF8B083971FDAB815D1DE6D8B62A25B5E31_AdjustorThunk (void);
extern void U3CDisconnectAsyncU3Ed__88_MoveNext_m4EFECF9406351B59E63A3CC50813ABD6F2F2B85D_AdjustorThunk (void);
extern void U3CDisconnectAsyncU3Ed__88_SetStateMachine_m5882ADA1C963C1DD9ED2342145438212C4C2F555_AdjustorThunk (void);
extern void U3CEmityCoreAsyncU3Ed__95_MoveNext_mAA824FA6D9C966C0D82F04A3933375E78F55379D_AdjustorThunk (void);
extern void U3CEmityCoreAsyncU3Ed__95_SetStateMachine_m195AD76D4DD1A13864C143C2357FE687D4FD293C_AdjustorThunk (void);
extern void U3CEmitCallbackAsyncU3Ed__96_MoveNext_m10AD1445FB9AC7FC933B465824E6072793915947_AdjustorThunk (void);
extern void U3CEmitCallbackAsyncU3Ed__96_SetStateMachine_m8A68E90477DDA2E92DCAA6AE5CE470FB49B75CEC_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__98_MoveNext_mEF3D565B8F4BEBCDC3686B3E999F9C833DBAB87D_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__98_SetStateMachine_mA919980759FA57B357F9BBAB2489F57EE7BF3DA4_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__99_MoveNext_mCDD6CA506FF87040A9F61A8846A8EA420E19EF2D_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__99_SetStateMachine_m3BE8D82221662C9C7F3FBB307B6ECCE62BABF11C_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__100_MoveNext_mEC42B49D4F13D2E1C5029F45E0CF84086AB0986E_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__100_SetStateMachine_m2D3248E4F0000599C77082CC1EDB814A844AA1B3_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__101_MoveNext_mC58A148714F7E77D70678DC32EAA2097111A8393_AdjustorThunk (void);
extern void U3CEmitAsyncU3Ed__101_SetStateMachine_m1C2F4C27BBB76FEE1D88338A56A79471D34FB72A_AdjustorThunk (void);
extern void U3COnClosedU3Ed__104_MoveNext_mF948C63F330E6DFADBCDE9D93FD410D35C192F64_AdjustorThunk (void);
extern void U3COnClosedU3Ed__104_SetStateMachine_mFDE3990A55091422819042018614A7F1D284FCA6_AdjustorThunk (void);
extern void U3COpenedHandlerU3Ed__105_MoveNext_m68D99B43F82AE2000B9DC426C5642D63C22875BB_AdjustorThunk (void);
extern void U3COpenedHandlerU3Ed__105_SetStateMachine_mAFD0F42959271BC191F3AFD0B4854AB646A76FED_AdjustorThunk (void);
extern void U3CDisconnectedHandlerU3Ed__110_MoveNext_m00D4848F9F9144B342A5837BAD2D02218D4C84BA_AdjustorThunk (void);
extern void U3CDisconnectedHandlerU3Ed__110_SetStateMachine_m61BF3EC1399B740FF5D43784DB64D7EE77B3CEA2_AdjustorThunk (void);
extern void U3CPingHandlerU3Ed__113_MoveNext_mFBD465A826EAFB3A03BF248B971F7CDA777956CE_AdjustorThunk (void);
extern void U3CPingHandlerU3Ed__113_SetStateMachine_mB1685B3B4D5FABFDB12A135DF76D5B9ED3F22AC8_AdjustorThunk (void);
extern void U3CInvokeDisconnectAsyncU3Ed__115_MoveNext_m1EB2D6FE144438070F4222B20DE2B17B9A08A0F4_AdjustorThunk (void);
extern void U3CInvokeDisconnectAsyncU3Ed__115_SetStateMachine_mE5373AA733E610BF6911F0BD1B77AF28188C2A16_AdjustorThunk (void);
extern void U3CStartPingAsyncU3Ed__116_MoveNext_m840437D67141837C2DAFE0E9F5DD1BD639C522E4_AdjustorThunk (void);
extern void U3CStartPingAsyncU3Ed__116_SetStateMachine_m7B3C285ADAB5FF332FD0A98EA9269899262D388A_AdjustorThunk (void);
extern void U3CCallbackAsyncU3Ed__17_MoveNext_mE6BCFAB6E3556FA96926C61F3DB217FEC67FA0C4_AdjustorThunk (void);
extern void U3CCallbackAsyncU3Ed__17_SetStateMachine_m061202E8AD2408AAA14AB0710F9EB5EC4D732E8E_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__16_MoveNext_m12909D8FA0B7EF8A1A0546A68DCDD24DE4EB381F_AdjustorThunk (void);
extern void U3CConnectAsyncU3Ed__16_SetStateMachine_m2831C8741307B31A1F1BF32577473F8673108E95_AdjustorThunk (void);
extern void U3CSendMessageAsyncU3Ed__17_MoveNext_m987B5116A558F40C630039C42CBA359CC928F3AD_AdjustorThunk (void);
extern void U3CSendMessageAsyncU3Ed__17_SetStateMachine_mD254A3520A1B161932C624CC1275C77AFB161EFD_AdjustorThunk (void);
extern void U3CSendMessageAsyncU3Ed__18_MoveNext_m970AA126DE68215A0CD7F52C19C7184381DF760E_AdjustorThunk (void);
extern void U3CSendMessageAsyncU3Ed__18_SetStateMachine_m253767F4F7CF7F0A6A373671B446C9B9CC4FC2F1_AdjustorThunk (void);
extern void U3CSendMessageAsyncU3Ed__19_MoveNext_mD0CC44463FC3C8613F58BC52AF0785294C9505D5_AdjustorThunk (void);
extern void U3CSendMessageAsyncU3Ed__19_SetStateMachine_mC3109B6EAF527C0B0BC16768EC3872E32E3D2033_AdjustorThunk (void);
extern void U3CSendMessageAsyncU3Ed__20_MoveNext_mE769EA29262903DC77094E30C2E9D764A511DB9C_AdjustorThunk (void);
extern void U3CSendMessageAsyncU3Ed__20_SetStateMachine_m9E7ED77807BDD4FBF17C4CF9BA4C940AFA44F2FE_AdjustorThunk (void);
extern void U3CDisconnectAsyncU3Ed__21_MoveNext_mBC523949C0CC63F3B76A484987411F8D06C8D0B8_AdjustorThunk (void);
extern void U3CDisconnectAsyncU3Ed__21_SetStateMachine_mBDF93F239C6BAEAB9C864CAF33087A49D951131A_AdjustorThunk (void);
extern void U3CListenAsyncU3Ed__22_MoveNext_m8982BFD36200B01CFA1624FA97F1C0EEF6E97F5D_AdjustorThunk (void);
extern void U3CListenAsyncU3Ed__22_SetStateMachine_m1B6E29D2A7E1528746774F705539DB49444FC347_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[44] = 
{
	{ 0x06000083, U3CConnectAsyncU3Ed__87_MoveNext_m2D026BAA9B7DEADEB9EC6105F0FB51C4164D92BD_AdjustorThunk },
	{ 0x06000084, U3CConnectAsyncU3Ed__87_SetStateMachine_mC3988BF8B083971FDAB815D1DE6D8B62A25B5E31_AdjustorThunk },
	{ 0x06000085, U3CDisconnectAsyncU3Ed__88_MoveNext_m4EFECF9406351B59E63A3CC50813ABD6F2F2B85D_AdjustorThunk },
	{ 0x06000086, U3CDisconnectAsyncU3Ed__88_SetStateMachine_m5882ADA1C963C1DD9ED2342145438212C4C2F555_AdjustorThunk },
	{ 0x06000087, U3CEmityCoreAsyncU3Ed__95_MoveNext_mAA824FA6D9C966C0D82F04A3933375E78F55379D_AdjustorThunk },
	{ 0x06000088, U3CEmityCoreAsyncU3Ed__95_SetStateMachine_m195AD76D4DD1A13864C143C2357FE687D4FD293C_AdjustorThunk },
	{ 0x06000089, U3CEmitCallbackAsyncU3Ed__96_MoveNext_m10AD1445FB9AC7FC933B465824E6072793915947_AdjustorThunk },
	{ 0x0600008A, U3CEmitCallbackAsyncU3Ed__96_SetStateMachine_m8A68E90477DDA2E92DCAA6AE5CE470FB49B75CEC_AdjustorThunk },
	{ 0x0600008B, U3CEmitAsyncU3Ed__98_MoveNext_mEF3D565B8F4BEBCDC3686B3E999F9C833DBAB87D_AdjustorThunk },
	{ 0x0600008C, U3CEmitAsyncU3Ed__98_SetStateMachine_mA919980759FA57B357F9BBAB2489F57EE7BF3DA4_AdjustorThunk },
	{ 0x0600008D, U3CEmitAsyncU3Ed__99_MoveNext_mCDD6CA506FF87040A9F61A8846A8EA420E19EF2D_AdjustorThunk },
	{ 0x0600008E, U3CEmitAsyncU3Ed__99_SetStateMachine_m3BE8D82221662C9C7F3FBB307B6ECCE62BABF11C_AdjustorThunk },
	{ 0x0600008F, U3CEmitAsyncU3Ed__100_MoveNext_mEC42B49D4F13D2E1C5029F45E0CF84086AB0986E_AdjustorThunk },
	{ 0x06000090, U3CEmitAsyncU3Ed__100_SetStateMachine_m2D3248E4F0000599C77082CC1EDB814A844AA1B3_AdjustorThunk },
	{ 0x06000091, U3CEmitAsyncU3Ed__101_MoveNext_mC58A148714F7E77D70678DC32EAA2097111A8393_AdjustorThunk },
	{ 0x06000092, U3CEmitAsyncU3Ed__101_SetStateMachine_m1C2F4C27BBB76FEE1D88338A56A79471D34FB72A_AdjustorThunk },
	{ 0x06000093, U3COnClosedU3Ed__104_MoveNext_mF948C63F330E6DFADBCDE9D93FD410D35C192F64_AdjustorThunk },
	{ 0x06000094, U3COnClosedU3Ed__104_SetStateMachine_mFDE3990A55091422819042018614A7F1D284FCA6_AdjustorThunk },
	{ 0x06000095, U3COpenedHandlerU3Ed__105_MoveNext_m68D99B43F82AE2000B9DC426C5642D63C22875BB_AdjustorThunk },
	{ 0x06000096, U3COpenedHandlerU3Ed__105_SetStateMachine_mAFD0F42959271BC191F3AFD0B4854AB646A76FED_AdjustorThunk },
	{ 0x06000097, U3CDisconnectedHandlerU3Ed__110_MoveNext_m00D4848F9F9144B342A5837BAD2D02218D4C84BA_AdjustorThunk },
	{ 0x06000098, U3CDisconnectedHandlerU3Ed__110_SetStateMachine_m61BF3EC1399B740FF5D43784DB64D7EE77B3CEA2_AdjustorThunk },
	{ 0x06000099, U3CPingHandlerU3Ed__113_MoveNext_mFBD465A826EAFB3A03BF248B971F7CDA777956CE_AdjustorThunk },
	{ 0x0600009A, U3CPingHandlerU3Ed__113_SetStateMachine_mB1685B3B4D5FABFDB12A135DF76D5B9ED3F22AC8_AdjustorThunk },
	{ 0x0600009B, U3CInvokeDisconnectAsyncU3Ed__115_MoveNext_m1EB2D6FE144438070F4222B20DE2B17B9A08A0F4_AdjustorThunk },
	{ 0x0600009C, U3CInvokeDisconnectAsyncU3Ed__115_SetStateMachine_mE5373AA733E610BF6911F0BD1B77AF28188C2A16_AdjustorThunk },
	{ 0x0600009D, U3CStartPingAsyncU3Ed__116_MoveNext_m840437D67141837C2DAFE0E9F5DD1BD639C522E4_AdjustorThunk },
	{ 0x0600009E, U3CStartPingAsyncU3Ed__116_SetStateMachine_m7B3C285ADAB5FF332FD0A98EA9269899262D388A_AdjustorThunk },
	{ 0x060000BE, U3CCallbackAsyncU3Ed__17_MoveNext_mE6BCFAB6E3556FA96926C61F3DB217FEC67FA0C4_AdjustorThunk },
	{ 0x060000BF, U3CCallbackAsyncU3Ed__17_SetStateMachine_m061202E8AD2408AAA14AB0710F9EB5EC4D732E8E_AdjustorThunk },
	{ 0x060000D9, U3CConnectAsyncU3Ed__16_MoveNext_m12909D8FA0B7EF8A1A0546A68DCDD24DE4EB381F_AdjustorThunk },
	{ 0x060000DA, U3CConnectAsyncU3Ed__16_SetStateMachine_m2831C8741307B31A1F1BF32577473F8673108E95_AdjustorThunk },
	{ 0x060000DB, U3CSendMessageAsyncU3Ed__17_MoveNext_m987B5116A558F40C630039C42CBA359CC928F3AD_AdjustorThunk },
	{ 0x060000DC, U3CSendMessageAsyncU3Ed__17_SetStateMachine_mD254A3520A1B161932C624CC1275C77AFB161EFD_AdjustorThunk },
	{ 0x060000DD, U3CSendMessageAsyncU3Ed__18_MoveNext_m970AA126DE68215A0CD7F52C19C7184381DF760E_AdjustorThunk },
	{ 0x060000DE, U3CSendMessageAsyncU3Ed__18_SetStateMachine_m253767F4F7CF7F0A6A373671B446C9B9CC4FC2F1_AdjustorThunk },
	{ 0x060000DF, U3CSendMessageAsyncU3Ed__19_MoveNext_mD0CC44463FC3C8613F58BC52AF0785294C9505D5_AdjustorThunk },
	{ 0x060000E0, U3CSendMessageAsyncU3Ed__19_SetStateMachine_mC3109B6EAF527C0B0BC16768EC3872E32E3D2033_AdjustorThunk },
	{ 0x060000E1, U3CSendMessageAsyncU3Ed__20_MoveNext_mE769EA29262903DC77094E30C2E9D764A511DB9C_AdjustorThunk },
	{ 0x060000E2, U3CSendMessageAsyncU3Ed__20_SetStateMachine_m9E7ED77807BDD4FBF17C4CF9BA4C940AFA44F2FE_AdjustorThunk },
	{ 0x060000E3, U3CDisconnectAsyncU3Ed__21_MoveNext_mBC523949C0CC63F3B76A484987411F8D06C8D0B8_AdjustorThunk },
	{ 0x060000E4, U3CDisconnectAsyncU3Ed__21_SetStateMachine_mBDF93F239C6BAEAB9C864CAF33087A49D951131A_AdjustorThunk },
	{ 0x060000E5, U3CListenAsyncU3Ed__22_MoveNext_m8982BFD36200B01CFA1624FA97F1C0EEF6E97F5D_AdjustorThunk },
	{ 0x060000E6, U3CListenAsyncU3Ed__22_SetStateMachine_m1B6E29D2A7E1528746774F705539DB49444FC347_AdjustorThunk },
};
static const int32_t s_InvokerIndices[355] = 
{
	4362,
	4368,
	1462,
	7597,
	4362,
	2387,
	695,
	7597,
	4362,
	4037,
	1413,
	7597,
	4362,
	2307,
	679,
	7597,
	4362,
	1587,
	301,
	7597,
	4362,
	9442,
	3462,
	7597,
	4362,
	7597,
	2094,
	7597,
	4362,
	2319,
	683,
	7597,
	4362,
	2387,
	695,
	7597,
	4362,
	9442,
	3462,
	7597,
	4362,
	9442,
	3462,
	7597,
	9442,
	9247,
	7557,
	9289,
	7597,
	9247,
	7557,
	9289,
	7597,
	7597,
	7597,
	4368,
	4368,
	9442,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9247,
	7557,
	9161,
	7469,
	9289,
	9289,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	9442,
	9289,
	9289,
	4368,
	7597,
	7597,
	7597,
	7597,
	9289,
	1442,
	3441,
	6745,
	3462,
	2079,
	2094,
	1430,
	7597,
	7597,
	7597,
	2387,
	7597,
	4037,
	2307,
	1587,
	9442,
	7597,
	2319,
	9442,
	9442,
	6745,
	9289,
	9442,
	9442,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	9289,
	7597,
	9404,
	7702,
	9289,
	7597,
	9161,
	7469,
	9189,
	7501,
	9247,
	7557,
	9247,
	7557,
	9189,
	7501,
	9247,
	7557,
	9289,
	7597,
	4368,
	9289,
	9289,
	9247,
	7557,
	0,
	6653,
	9247,
	9289,
	6745,
	9442,
	7597,
	3462,
	9442,
	9442,
	9247,
	7557,
	9404,
	7702,
	9289,
	7597,
	6745,
	6745,
	3457,
	6745,
	3457,
	9289,
	6724,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9442,
	9442,
	9442,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6745,
	9442,
	9442,
	9289,
	7597,
	9247,
	7557,
	9247,
	7557,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	7597,
	9442,
	9289,
	7597,
	0,
	9442,
	7597,
	9442,
	7557,
	9289,
	2018,
	2423,
	0,
	0,
	0,
	9289,
	7597,
	9289,
	7597,
	9442,
	7557,
	6745,
	0,
	0,
	9289,
	9442,
	7597,
	9289,
	7597,
	9289,
	7597,
	9442,
	9161,
	7469,
	9289,
	7597,
	9442,
	3462,
	3462,
	6745,
	6745,
	9442,
	3462,
	3462,
	6745,
	6745,
	9442,
	13914,
	0,
	0,
	0,
	0,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x060000B9, { 0, 1 } },
	{ 0x06000146, { 1, 1 } },
	{ 0x06000147, { 2, 1 } },
};
extern const uint32_t g_rgctx_IJsonSerializer_Deserialize_TisT_tC8EFE59859198D4ED4A31CDF98DE50C0E7D8089B_m6549EC087BF75B65BD75A99C75B7668EF3D06283;
extern const uint32_t g_rgctx_JsonSerializer_Deserialize_TisT_tA48EA67F53A662D8546F1D75CCCF41670FC4B381_mF815AE3DA9A632663AB0FF0D5A3B506195A38F0D;
extern const uint32_t g_rgctx_JsonSerializer_Deserialize_TisT_t91A4580500B18390AB74ABEB536AE33851E87B75_mBC5931A79BCA3A4BF5FB0A3454612BD2C245ED19;
static const Il2CppRGCTXDefinition s_rgctxValues[3] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IJsonSerializer_Deserialize_TisT_tC8EFE59859198D4ED4A31CDF98DE50C0E7D8089B_m6549EC087BF75B65BD75A99C75B7668EF3D06283 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_Deserialize_TisT_tA48EA67F53A662D8546F1D75CCCF41670FC4B381_mF815AE3DA9A632663AB0FF0D5A3B506195A38F0D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_JsonSerializer_Deserialize_TisT_t91A4580500B18390AB74ABEB536AE33851E87B75_mBC5931A79BCA3A4BF5FB0A3454612BD2C245ED19 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_SocketIOClient_CodeGenModule;
const Il2CppCodeGenModule g_SocketIOClient_CodeGenModule = 
{
	"SocketIOClient.dll",
	355,
	s_methodPointers,
	44,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	3,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
