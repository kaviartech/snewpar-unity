﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif





// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB;
// System.Action`1<System.Boolean>
struct Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C;
// System.Action`1<UnityEngine.Camera>
struct Action_1_t268986DA4CF361AC17B40338506A83AFB35832EA;
// System.Action`1<UnityEngine.Font>
struct Action_1_tD91E4D0ED3C2E385D3BDD4B3EA48B5F99D39F1DC;
// System.Action`1<UnityEngine.XR.InputDevice>
struct Action_1_tFAB0B519749BBE2B7AAD946105FAE8116636A8BC;
// System.Action`1<System.Int32>
struct Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404;
// System.Action`1<System.IntPtr>
struct Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2;
// System.Action`1<UnityEngineInternal.Input.NativeInputUpdateType>
struct Action_1_t7797D4D8783204B10C3D28B96B049C48276C3B1B;
// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A;
// System.Action`1<UnityEngine.XR.XRInputSubsystem>
struct Action_1_tC867D66471C553CFFF8707FF2C59FB7AAB03086A;
// System.Action`1<UnityEngine.XR.XRNodeState>
struct Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8;
// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
struct Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8;
// System.Action`2<System.Int32,System.String>
struct Action_2_t6AAF2E215E74E16A4EEF0A0749A4A325D99F5BA6;
// System.Action`2<System.Object,System.String[]>
struct Action_2_tAD573E58BF9243F5E6BB71D7ACD67F0388BDEEC7;
// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>>
struct Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2;
// System.Action`3<System.Boolean,System.Boolean,System.Int32>
struct Action_3_t4730167C8E7EB19F1E0034580790A915D549F6CB;
// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64>
struct Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t73D7B210F00FD83B4F194279B052C32E9FCB1D04;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.ManualResetEvent>
struct Dictionary_2_t685E1072D6BF5D03945108D75216A223A8EA7FBB;
// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty>
struct Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680;
// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup>
struct Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983;
// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IMaterial>
struct Dictionary_2_t363D8F92FA9BF57C132404DC1816AEA51C3758E1;
// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel>
struct Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B;
// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.ITexture>
struct Dictionary_2_tFB71D8D0D142E37DD9146E387285F9F34A3F2E88;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.OpenXR.Features.Mock.MockRuntime/AfterFunctionDelegate>
struct Dictionary_2_t07B33916E5B9BFBEDDE5C206E597C4A2071ED5E6;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.OpenXR.Features.Mock.MockRuntime/BeforeFunctionDelegate>
struct Dictionary_2_tE800B4C2237603A792FFCCFB9BFC21EA79CB7F3F;
// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfBase,TriLibCore.ThreeMf.ThreeMfMaterial>
struct Dictionary_2_tC950A32EAFFC25F50CB10E4BA94AB529CDADA0E8;
// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfColorGroup,System.Collections.Generic.IList`1<UnityEngine.Color>>
struct Dictionary_2_t230D84F7213CBDDD7098D9E66DC0079B1EE334FE;
// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfTexture2D,TriLibCore.ThreeMf.ThreeMfMaterial>
struct Dictionary_2_tFBFB8D749ADDCE1FBCCC7A11924995880D5B6A4D;
// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfTexture2D,TriLibCore.ThreeMf.ThreeMfTexture>
struct Dictionary_2_tF76A5254FF71CC9D5D32CAB73E7A4AB583A904E9;
// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfTexture2DGroup,System.Collections.Generic.IList`1<UnityEngine.Vector2>>
struct Dictionary_2_t32E2A37AC16B1C83AE612366276E6E07A19064C8;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.ISubsystem>
struct Dictionary_2_tCDC65F572855EBDD1C12CEE33EBEBE0131F60C9C;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph>
struct Dictionary_2_tC61348D10610A6B3D7B65102D82AC3467D59EAA7;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_tF2D41B212D800E7E7D00F9BDEA817E57153988BF;
// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs>
struct EventHandler_1_tF3AA5EB88F494B5147D2E245A3AF5AB3CDF8D72B;
// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs>
struct EventHandler_1_t628C7697D75CC4686AD6125D745540AC427C28A0;
// System.EventHandler`1<Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs>
struct EventHandler_1_t9A81151178F9BE57BCBDEC74D73D78671B00EC6E;
// System.Func`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector2>>
struct Func_1_tF5F7F5DCF1679E08B2536581A6E1EEF5529155C9;
// System.Func`1<System.Boolean>
struct Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t5C7016A1FC354629920CF15E7F2AD5A6E6D06941;
// System.Func`1<System.String>
struct Func_1_t367387BB2C476D3F32DB12161B5FDC128DC3231C;
// System.Func`2<UnityEngine.UIElements.GradientSettings,Unity.VectorGraphics.InternalBridge/GradientSettingsBridge>
struct Func_2_t366DDDB5C5FB5478228D0B49FFF0721AEFC1D33F;
// System.Func`2<UnityEngineInternal.Input.NativeInputUpdateType,System.Boolean>
struct Func_2_t880CA675AE5D39E081BEEF14DC092D82674DE4F2;
// System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>>
struct Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E;
// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,System.Boolean>
struct Func_2_t407C4C2C9129DC8C4ED64BFC1245ADF5BC54144C;
// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,IxMilia.ThreeMf.ThreeMfBaseMaterials>
struct Func_2_t4FAF9B6978DD4BEBDB6080C8A5C65A89A7CA9EC9;
// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,IxMilia.ThreeMf.ThreeMfColorGroup>
struct Func_2_t3888261809E6D9256EF59C505C2E2E5974D12D88;
// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,IxMilia.ThreeMf.ThreeMfTexture2D>
struct Func_2_tDC2B856640B426865EF8F8A06D21921AC4CF86AE;
// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,IxMilia.ThreeMf.ThreeMfTexture2DGroup>
struct Func_2_tDAD1F04679E83654859656F7E338DB1304EA2FE7;
// System.Func`2<UnityEngine.UIElements.VectorImageVertex,UnityEngine.Color>
struct Func_2_t0471F7DAEBC22E8FBF8EB17B66AA9ED6750D2237;
// System.Func`2<UnityEngine.UIElements.VectorImageVertex,UnityEngine.Vector2>
struct Func_2_t3D0310BCABDAAEF12A5955F712B55633C6CB8B9C;
// System.Func`2<Unity.VectorGraphics.InternalBridge/GradientSettingsBridge,UnityEngine.UIElements.GradientSettings>
struct Func_2_t9E8CE435999D136D29DF6074600D6F34D77E48FE;
// System.Func`2<Unity.VectorGraphics.InternalBridge/VectorImageVertexBridge,UnityEngine.UIElements.VectorImageVertex>
struct Func_2_t6C07AB9155802018C389E7DDD4F2D803421EC46F;
// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute>
struct HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640;
// System.Collections.Generic.HashSet`1<UnityEngine.XR.Management.XRLoader>
struct HashSet_1_t174593AE6599738C19A33586587D63534CED9F0F;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t79D4ADB15B238AC117DF72982FEA3C42EF5AFA19;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.IList`1<UnityEngine.Color>
struct IList_1_t78DB7CACF5BDC17685CA41C8A5615F4AE760CB59;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation>
struct IList_1_t7A16CD7EF0938B36E4D20182185F284ECA5F93A2;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera>
struct IList_1_t13EA3E1B6894AF8023B793D65EA2E1ED596B6E82;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>
struct IList_1_t54EA2EAA8FF287B3E144BC90047C3E635336CB4C;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight>
struct IList_1_t95B0FF72887258CDC012A1B81E66B66AF3BBE38E;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>
struct IList_1_t0662D113B996C51F1676FFC848F7B3448D818DB7;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>
struct IList_1_tBAC2F9CBFB365F17F69446225AF2802DEF7B2956;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>
struct IList_1_t2988C79E2C0A953B91ACE72118B299F94ECFEB62;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_tFB8BE2ED9A601C1259EAB8D73D1B3E96EA321FA1;
// System.Collections.Generic.IList`1<IxMilia.ThreeMf.ThreeMfTexture2DCoordinate>
struct IList_1_tC0E80CA9C0A700B10D9C64C8E9CA3D573C44282E;
// System.Collections.Generic.IList`1<UnityEngine.Vector2>
struct IList_1_t0DF1E5F56EE58E1A7F1FE26A676FC9FBF4D52A07;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t4EEE459A249DDE104FA2E88234C593389EE5D291;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>>
struct List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_tBFF9DD9FFA06F20E74F9D7AD36610BD754D353A4;
// System.Collections.Generic.List`1<Byn.Awrtc.ConnectionId>
struct List_1_t8648F03E32A0E6434B85C75C9673EA176516EE34;
// System.Collections.Generic.List`1<System.Exception>
struct List_1_tA61E6ACDC88C89A9523D28C560996ABFF548352B;
// System.Collections.Generic.List`1<Byn.Awrtc.IceServer>
struct List_1_t671F6F061AA091834868CE5CC45E6E1A7E372A79;
// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>
struct List_1_tB66D13E0427CA48C600AEA4DBA5B173C3734C6CE;
// System.Collections.Generic.List`1<UnityEngine.IntegratedSubsystem>
struct List_1_t78E7232867D713AA9907E71F6C5B19B226F0B180;
// System.Collections.Generic.List`1<UnityEngine.IntegratedSubsystemDescriptor>
struct List_1_tACFC79734710927A89702FFC38900223BB85B5A6;
// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>
struct List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_tCD5F926D25FC8BFAF39E4BE6F879C1FA11501C76;
// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
// System.Collections.Generic.List`1<UnityEngine.Subsystem>
struct List_1_t9E8CCD70A25458CE30A64503B35F06ECA62E3052;
// System.Collections.Generic.List`1<UnityEngine.SubsystemDescriptor>
struct List_1_t15AD773D34D3739AFB67421B6DFFACEA7638F64E;
// System.Collections.Generic.List`1<UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider>
struct List_1_t2D19D6F759F401FE6C5460698E5B8249E470E044;
// System.Collections.Generic.List`1<UnityEngine.SubsystemsImplementation.SubsystemWithProvider>
struct List_1_tD834E8FB7FDC0D4243FBCF922D7FE4E3C707AAC3;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t67A1600A303BB89506DFD21B59687088B7E0675B;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A;
// System.Collections.Generic.List`1<System.UInt64>
struct List_1_tB88E7361EE76DFB3EBB7FCD60CC59ACC3E48C284;
// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystem>
struct List_1_t90832B88D7207769654164CC28440CF594CC397D;
// System.Collections.Generic.List`1<UnityEngine.XR.Management.XRLoader>
struct List_1_tA1A04BD6B1EE83992AE369D5DB31A028E9B57822;
// System.Collections.Generic.List`1<UnityEngine.XR.XRNodeState>
struct List_1_t8AE5A65CCFBC753343962D38B82C801F7BDE3C87;
// System.Collections.Generic.List`1<UnityEngine.Yoga.YogaNode>
struct List_1_t84B666107A8A3ECB0F5A24B0243137D056DA9165;
// System.Collections.Generic.List`1<UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose>
struct List_1_t8A06DB5554CD76CE072807DE6EF4198109D84E53;
// System.Collections.Generic.List`1<UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData>
struct List_1_tA8C10B8A593896280D68A4B006217C43F236DE0D;
// Firebase.Platform.MainThreadProperty`1<System.Boolean>
struct MainThreadProperty_1_t0B3DED62013B23F9A8CB8C13A6EC7B04D8F02E97;
// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>>
struct Queue_1_tBB890027A96D0D3343916455489502BC8FBEB73F;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_tDCDB1CEF747EF8D38C6708645EC5FD3799C168BA;
// System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>
struct Tuple_2_t9EC4E1A9EFB1ABACA7C36B1761CBDB693089F4DA;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t238D0D2427C6B841A01F522A41540165A2C4AE76;
// System.Collections.Generic.Dictionary`2<System.String,System.String>[]
struct Dictionary_2U5BU5D_tE4669D9AC2F1B83C2557CE335CA7669AED87E418;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t1AF33AD0B7330843448956EC4277517081658AE7;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t19A0C6D66F22DF673E9CDB37DEF566FE0EC947FA;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1506EBA524A07AD1066D6DD4D7DFC6721F1AC26B;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t427621BF8902AE33C86E7BF384D9B2B5B781F949;
// UnityEngine.TextCore.Glyph[]
struct GlyphU5BU5D_t345CEC8703A6C650639C40DB7D35269A2D467FC5;
// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[]
struct GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E;
// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[]
struct GlyphPairAdjustmentRecordU5BU5D_tD5DD2A739A4CA745E7F28ECCB2CD0BD0A65A38F7;
// UnityEngine.TextCore.GlyphRect[]
struct GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70;
// UnityEngine.HumanBone[]
struct HumanBoneU5BU5D_t443B81D55400778CBB921DF04BE932ABF14BAA52;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[]
struct PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C;
// UnityEngine.Resolution[]
struct ResolutionU5BU5D_t943BB1FE2B73E9E630A701D55F4C6EED8B54175A;
// UnityEngine.SkeletonBone[]
struct SkeletonBoneU5BU5D_t84722CE071EF9295326D5A84492716DBC7A15A85;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.UInt32[]
struct UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5;
// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07;
// Firebase.Crashlytics.AndroidImpl
struct AndroidImpl_t09BB72854905028A1DF3FBA8772392723D2CCD76;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0;
// UnityEngine.Animation
struct Animation_t6593B06C39E3B139808B19F2C719C860F3F61040;
// UnityEngine.AnimationCurve
struct AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354;
// UnityEngine.AnimationState
struct AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE;
// TriLibCore.AssetLoaderContext
struct AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C;
// UnityEngine.Experimental.XR.Interaction.BasePoseProvider
struct BasePoseProvider_t55E2883DF2C8052200284D64B68471636876FA1D;
// UnityEngine.Yoga.BaselineFunction
struct BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679;
// Byn.Awrtc.CallEventHandler
struct CallEventHandler_t10B6E9A9F0D6ACBCF8346292698855A8CEF09F05;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804;
// UnityEngine.CharacterController
struct CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A;
// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76;
// UnityEngine.Compass
struct Compass_t161CD14062E71ED7324FE12ED003EE0877EE546F;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// System.Text.Decoder
struct Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC;
// Firebase.Dispatcher
struct Dispatcher_tBD1370511B5D6C10B211FBD3AF8E4F9D6B3CD8C6;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB;
// System.Text.Encoding
struct Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095;
// System.Exception
struct Exception_t;
// Firebase.Crashlytics.ExceptionHandler
struct ExceptionHandler_t2DDC3721AFC96975EA180F4E7A4902FFB2CE138B;
// Firebase.FirebaseApp
struct FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25;
// Firebase.Crashlytics.FirebaseCrashlyticsInternal
struct FirebaseCrashlyticsInternal_t674756D0479C8ED14FE9E114B533CD274F9D6F40;
// Firebase.Messaging.FirebaseMessage
struct FirebaseMessage_t09E58AB584806FC5C23F419AD7CDCE53969FE20F;
// Firebase.Platform.FirebaseMonoBehaviour
struct FirebaseMonoBehaviour_t0DC02A14DFF90538B38698592F07B4373DB03C72;
// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t20D8E5AAFC2EB2518FCABBF40465855E797FF0D8;
// UnityEngine.Gradient
struct Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E;
// UnityEngine.Gyroscope
struct Gyroscope_tA4CEC0F47FFB4CEB90410CC6B860D052BB35BE9E;
// Firebase.Platform.IAppConfigExtensions
struct IAppConfigExtensions_t2BC1AFEF8FF5F2A7D5B45F23000CEE8FEE376418;
// Firebase.Platform.IClockService
struct IClockService_t62FFADE4F3DABB6EBE3869EEBE8C577963F1A026;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// Firebase.Platform.IFirebaseAppUtils
struct IFirebaseAppUtils_t61EDF19372DFE7348E02194135E2F3B8801E3391;
// TriLibCore.Interfaces.IGeometryGroup
struct IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262;
// ImaginationOverflow.UniversalDeepLinking.ILinkProvider
struct ILinkProvider_tF3B30886969F4D27850BC6D352C4E6AC1BB3CE7C;
// Firebase.Platform.ILoggingService
struct ILoggingService_t39576CA38C73068394A2E2032D0873F962DA9DF4;
// Byn.Awrtc.IMediaNetwork
struct IMediaNetwork_tBB462E2787D9712734C6AE95BCE4211C3E049CC5;
// TriLibCore.Interfaces.IModel
struct IModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB;
// UnityEngine.ISubsystemDescriptor
struct ISubsystemDescriptor_tEF29944D579CC7D70F52CB883150735991D54E6E;
// Firebase.Crashlytics.Impl
struct Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF;
// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler
struct LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B;
// Firebase.Crashlytics.LoggedException
struct LoggedException_t43B89090462BFFD9B76040EF52EE2EFD63359887;
// UnityEngine.Yoga.Logger
struct Logger_t092B1218ED93DD47180692D5761559B2054234A0;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t63959486AA41A113A4353D0BF4A68E77EBA0A158;
// UnityEngine.Yoga.MeasureFunction
struct MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09;
// Byn.Awrtc.MediaConfig
struct MediaConfig_t14F31B7D2B26F6D04B8FD66C917536DA92873E3A;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// UnityEngine.MeshCollider
struct MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455;
// NativeGalleryNamespace.NGCallbackHelper
struct NGCallbackHelper_t66F469013909FD93F5CEDB7C9C6689E79EB4605E;
// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid
struct NGMediaReceiveCallbackAndroid_tB8DDBFAD614592BF98999BEAC00A75E0D420CC52;
// NativeShareNamespace.NSCallbackHelper
struct NSCallbackHelper_t0FE98DE6D6CF554080E07CC69C0C7AD59C49824C;
// UnityEngineInternal.Input.NativeUpdateCallback
struct NativeUpdateCallback_tC5CA5A9117B79251968A4DA3758552EFE1D37495;
// Byn.Awrtc.NetworkConfig
struct NetworkConfig_tDE09348A384F9FCF79DC5CE5BB4DFEECF9AB0766;
// TriLibCore.Obj.ObjGeometry
struct ObjGeometry_t251EC78EED9A9E6E40B71FDD1D7C79DDDAE04BEB;
// TriLibCore.Obj.ObjMaterial
struct ObjMaterial_t333DED08F3CA57CC38255E5187814EB8821FEB9A;
// TriLibCore.Obj.ObjModel
struct ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5;
// TriLibCore.Obj.Reader.ObjReader
struct ObjReader_t526AA9305FE5DDF1EF48E7B3D4DBC0FFF1056BEB;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.ParticleSystem
struct ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1;
// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration
struct PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D;
// TriLibCore.Ply.Reader.PlyReader
struct PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F;
// System.Text.RegularExpressions.Regex
struct Regex_tE773142C2BE45C5D362B0F815AFF831707A51772;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E;
// Firebase.Crashlytics.StackFrames
struct StackFrames_t8F4306185C13967101E94E78F0D2312DDC4401AC;
// TriLibCore.Stl.StlGeometry
struct StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF;
// TriLibCore.Stl.Reader.StlReader
struct StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A;
// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE;
// System.String
struct String_t;
// UnityEngine.SubsystemsImplementation.SubsystemProvider
struct SubsystemProvider_tE1865B8FE698C81A59AED35E0E536BD53F402455;
// System.Threading.Tasks.Task
struct Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// IxMilia.ThreeMf.ThreeMfBase
struct ThreeMfBase_t019B9AAB3AF11FEDA0937D9901F9E9F7099E2CA2;
// IxMilia.ThreeMf.ThreeMfFile
struct ThreeMfFile_tD22A5D01D291BAE0C8B10321E0ECC7D350476781;
// TriLibCore.ThreeMf.Reader.ThreeMfReader
struct ThreeMfReader_t8F4D229EE9D88E8F68C2AC4FA06C66477A7545C1;
// TriLibCore.ThreeMf.ThreeMfRootModel
struct ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396;
// IxMilia.ThreeMf.ThreeMfTexture2D
struct ThreeMfTexture2D_t992803A6FCF8343D7C1F9D282390E0BC126DE3CF;
// System.Type
struct Type_t;
// System.Text.Unicode.UnicodeRange
struct UnicodeRange_t39A4F977F558B4F00AFE470AD6573FA988BDA836;
// Firebase.Unity.UnitySynchronizationContext
struct UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F;
// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript
struct UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6;
// System.Uri
struct Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.WeakReference
struct WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E;
// UnityEngine.XR.Management.XRLoader
struct XRLoader_t80B1B1934C40561C5352ABC95D567DC2A7C9C976;
// UnityEngine.XR.Management.XRManagerSettings
struct XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52;
// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345;
// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged
struct IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C;
// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback
struct OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072;
// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler
struct SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC;
// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970;
// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGStringHelper
struct SWIGStringHelper_tA8CF9EF78BD75BC8C5CB597CC99E74764BFE361B;
// Firebase.Messaging.FirebaseMessaging/Listener
struct Listener_tC16E83118F05783B5CA41A3F81ABF1A2C83D7BF7;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t953C1EA2870BB0BF9A25500F62877BBBA6D44AE9;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1;
// UnityEngine.XR.OpenXR.Features.Mock.MockRuntime/ScriptEventDelegate
struct ScriptEventDelegate_t0DE50F0234F04B68173703087BFCB08BB4922E20;
// NativeGallery/MediaPickCallback
struct MediaPickCallback_tC207F6F3EEE18F64B03E23D5C9FA8C79517A5EB6;
// NativeGallery/MediaPickMultipleCallback
struct MediaPickMultipleCallback_t717A5D378751E2CACB53E6BA25386FF8DB41627B;
// NativeShare/ShareResultCallback
struct ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4;
// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey3
struct U3CSendU3Ec__AnonStorey3_t0877E086BABDA1BDD2F95E0E90A5C52922B00ABB;
// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1
struct U3CSendCoroutineU3Ec__AnonStorey1_tB73A38DA19EEBC025199918DCC1326071057950C;
// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir
struct SynchronizationContextBehavoir_t77EF1109836806C11E4F2C8ECF2867D9531FEBBA;
// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_t6DD292ED79129AF67D909EDA603DA9DC5C762462;
// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_tE1FA51515FC895E3A02E317912FBDCDCF390A411;
// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_tDEDADF060B051C2953122274EBF3896AF48735E8;
// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct TimeEventHandler_t8F77A92148F1A0F37AE8DE9CA7E25D90D001DC1E;
// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_tE99B0B8FC5C781502FEC342E761FC29F4DB986B8;
// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB;
// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t37770D7DA2368AE963BFF83DE2B3FC28CC710959;
// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate
struct MessageReceivedDelegate_t0426330610D984E008DF662FD99680187AEA49DC;
// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate
struct TokenReceivedDelegate_t3893429AE7EEC18BBD788219BAE8961E21EC4AE0;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_t1B9CEFDA449A0679DCC7B832B88DB9E6864A4110;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5;
// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t3DABDDBE55902B8931CDE0C135374D8CBDD52CE6;

struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com;
struct ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801;
struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9;
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_com;
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke;
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com;
struct Resolution_tDF215F567EEFFD07B9A8FB7CEACC08EA6B8B9525;
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_com;
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke;
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t1AF33AD0B7330843448956EC4277517081658AE7* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t238D0D2427C6B841A01F522A41540165A2C4AE76* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// Byn.Awrtc.Base.AWebRtcCall
struct AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD  : public RuntimeObject
{
	// Byn.Awrtc.NetworkConfig Byn.Awrtc.Base.AWebRtcCall::mNetworkConfig
	NetworkConfig_tDE09348A384F9FCF79DC5CE5BB4DFEECF9AB0766* ___mNetworkConfig_1;
	// Byn.Awrtc.MediaConfig Byn.Awrtc.Base.AWebRtcCall::mMediaConfig
	MediaConfig_t14F31B7D2B26F6D04B8FD66C917536DA92873E3A* ___mMediaConfig_2;
	// Byn.Awrtc.CallEventHandler Byn.Awrtc.Base.AWebRtcCall::CallEvent
	CallEventHandler_t10B6E9A9F0D6ACBCF8346292698855A8CEF09F05* ___CallEvent_3;
	// Byn.Awrtc.IMediaNetwork Byn.Awrtc.Base.AWebRtcCall::mNetwork
	RuntimeObject* ___mNetwork_4;
	// System.Boolean Byn.Awrtc.Base.AWebRtcCall::mConferenceMode
	bool ___mConferenceMode_5;
	// Byn.Awrtc.Base.AWebRtcCall/CallState Byn.Awrtc.Base.AWebRtcCall::mState
	int32_t ___mState_8;
	// System.Boolean Byn.Awrtc.Base.AWebRtcCall::mIsDisposed
	bool ___mIsDisposed_9;
	// System.Boolean Byn.Awrtc.Base.AWebRtcCall::mLocalFrameEvents
	bool ___mLocalFrameEvents_10;
	// System.Boolean Byn.Awrtc.Base.AWebRtcCall::mServerInactive
	bool ___mServerInactive_11;
	// System.Collections.Generic.List`1<Byn.Awrtc.ConnectionId> Byn.Awrtc.Base.AWebRtcCall::mConnectionIds
	List_1_t8648F03E32A0E6434B85C75C9673EA176516EE34* ___mConnectionIds_12;
	// System.Boolean Byn.Awrtc.Base.AWebRtcCall::mPendingListenCall
	bool ___mPendingListenCall_13;
	// System.Boolean Byn.Awrtc.Base.AWebRtcCall::mPendingCallCall
	bool ___mPendingCallCall_14;
	// System.String Byn.Awrtc.Base.AWebRtcCall::mPendingAddress
	String_t* ___mPendingAddress_15;
};

// UnityEngine.Analytics.AnalyticsSessionInfo
struct AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76  : public RuntimeObject
{
};

// ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider
struct AndroidLinkProvider_t25725DADB9911DB50E702686EA6A3796B2F17EBC  : public RuntimeObject
{
	// UnityEngine.AndroidJavaClass ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::_unityPlayer
	AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03* ____unityPlayer_0;
	// UnityEngine.AndroidJavaObject ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::_currentActivity
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* ____currentActivity_1;
	// UnityEngine.AndroidJavaObject ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::_deepLinkIntent
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* ____deepLinkIntent_2;
	// System.String ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::_deepLink
	String_t* ____deepLink_3;
	// System.Action`1<System.String> ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider::_linkReceived
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ____linkReceived_4;
};

// Firebase.Platform.Default.AppConfigExtensions
struct AppConfigExtensions_t6B8627CD4EFF8F05D2F749CC406E5E12F04CEE48  : public RuntimeObject
{
};

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration
struct AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D  : public RuntimeObject
{
	// System.String ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::_steamId
	String_t* ____steamId_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::_displayName
	String_t* ____displayName_1;
	// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::_globalConfiguration
	PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D* ____globalConfiguration_2;
	// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration[] ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration::_customDeepLinkingProtocols
	PlatformLinkingConfigurationU5BU5D_tC80FECD9BD6CE97CAEC1FF562FAB642841E8A02C* ____customDeepLinkingProtocols_3;
};

// System.Attribute
struct Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA  : public RuntimeObject
{
};

// UnityEngine.Experimental.Audio.AudioSampleProvider
struct AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2  : public RuntimeObject
{
	// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler UnityEngine.Experimental.Audio.AudioSampleProvider::sampleFramesAvailable
	SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30* ___sampleFramesAvailable_0;
	// UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler UnityEngine.Experimental.Audio.AudioSampleProvider::sampleFramesOverflow
	SampleFramesHandler_tFE84FF9BBCEFB880D46227188F375BEF680AAA30* ___sampleFramesOverflow_1;
};

// UnityEngine.AudioSettings
struct AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD  : public RuntimeObject
{
};

// Byn.Awrtc.BufferedFrame
struct BufferedFrame_tC838AB09A0FAE3B7465B85D8B99E956D1F44F4D2  : public RuntimeObject
{
	// System.Byte[] Byn.Awrtc.BufferedFrame::mBuffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___mBuffer_0;
	// System.Int32 Byn.Awrtc.BufferedFrame::mWidth
	int32_t ___mWidth_1;
	// System.Int32 Byn.Awrtc.BufferedFrame::mHeight
	int32_t ___mHeight_2;
	// System.Int32 Byn.Awrtc.BufferedFrame::mRotation
	int32_t ___mRotation_3;
	// System.Boolean Byn.Awrtc.BufferedFrame::mTopRowFirst
	bool ___mTopRowFirst_4;
	// Byn.Awrtc.FramePixelFormat Byn.Awrtc.BufferedFrame::mFormat
	int32_t ___mFormat_5;
};

// ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage
struct ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738  : public RuntimeObject
{
};

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE
struct CrashlyticsInternalPINVOKE_t691F261E9A85343DA87F1275433EFC3767B4981B  : public RuntimeObject
{
};

// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617  : public RuntimeObject
{
};

// Firebase.Platform.DebugLogger
struct DebugLogger_t06BB6B0EBB4D36FCB961CCF80EE1FE4F0A88B4C1  : public RuntimeObject
{
};

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager
struct DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F  : public RuntimeObject
{
	// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::<IsSteamBuild>k__BackingField
	bool ___U3CIsSteamBuildU3Ek__BackingField_1;
	// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_activated
	LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* ____activated_2;
	// ImaginationOverflow.UniversalDeepLinking.ILinkProvider ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_currProvider
	RuntimeObject* ____currProvider_3;
	// UnityEngine.GameObject ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_go
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ____go_4;
	// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_storedActivation
	String_t* ____storedActivation_5;
};

// Firebase.Dispatcher
struct Dispatcher_tBD1370511B5D6C10B211FBD3AF8E4F9D6B3CD8C6  : public RuntimeObject
{
	// System.Int32 Firebase.Dispatcher::ownerThreadId
	int32_t ___ownerThreadId_0;
	// System.Collections.Generic.Queue`1<System.Action> Firebase.Dispatcher::queue
	Queue_1_tDCDB1CEF747EF8D38C6708645EC5FD3799C168BA* ___queue_1;
};

// TriLibCore.Gltf.Draco.DracoMeshLoader
struct DracoMeshLoader_t20AFB8166FEAA1C604D818BF2707262748C295FF  : public RuntimeObject
{
};

// ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider
struct DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923  : public RuntimeObject
{
	// System.Action`1<System.String> ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider::LinkReceived
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___LinkReceived_0;
};

// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider
struct EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D  : public RuntimeObject
{
	// System.Action`1<System.String> ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::LinkReceived
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___LinkReceived_1;
};

// System.EventArgs
struct EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377  : public RuntimeObject
{
};

// Firebase.ExceptionAggregator
struct ExceptionAggregator_t79259EF5E7CCF32340080CFAC267848A89DA90E3  : public RuntimeObject
{
};

// Firebase.Crashlytics.ExceptionHandler
struct ExceptionHandler_t2DDC3721AFC96975EA180F4E7A4902FFB2CE138B  : public RuntimeObject
{
	// System.Boolean Firebase.Crashlytics.ExceptionHandler::isRegistered
	bool ___isRegistered_0;
};

// Firebase.Platform.FirebaseAppUtilsStub
struct FirebaseAppUtilsStub_tA2C798DD03529908BA1CC968E5CD737BA58F5A02  : public RuntimeObject
{
};

// Firebase.Platform.FirebaseEditorDispatcher
struct FirebaseEditorDispatcher_tC36AA65B08B38AB2721AADB81D51BA16CE86239C  : public RuntimeObject
{
};

// Firebase.Platform.FirebaseHandler
struct FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586  : public RuntimeObject
{
	// System.Boolean Firebase.Platform.FirebaseHandler::<IsPlayMode>k__BackingField
	bool ___U3CIsPlayModeU3Ek__BackingField_4;
	// System.EventHandler`1<System.EventArgs> Firebase.Platform.FirebaseHandler::Updated
	EventHandler_1_tF2D41B212D800E7E7D00F9BDEA817E57153988BF* ___Updated_6;
	// System.Action Firebase.Platform.FirebaseHandler::UpdatedEventWrapper
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___UpdatedEventWrapper_7;
	// System.EventHandler`1<Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs> Firebase.Platform.FirebaseHandler::ApplicationFocusChanged
	EventHandler_1_t9A81151178F9BE57BCBDEC74D73D78671B00EC6E* ___ApplicationFocusChanged_8;
};

// Firebase.Platform.FirebaseLogger
struct FirebaseLogger_t17574FB770AB2CB02AF3459B2EF9D27FC9C24F42  : public RuntimeObject
{
};

// Firebase.Messaging.FirebaseMessaging
struct FirebaseMessaging_tAF3E6E43968D1C156E887454B8F9BDB7723036D3  : public RuntimeObject
{
};

// Firebase.Messaging.FirebaseMessagingPINVOKE
struct FirebaseMessagingPINVOKE_t8D162519505D2E2C12BFCD6037CFE376283ABF4F  : public RuntimeObject
{
};

// UnityEngine.TextCore.LowLevel.FontEngine
struct FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A  : public RuntimeObject
{
};

// TriLibCore.Geometries.Geometry
struct Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147  : public RuntimeObject
{
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Geometries.Geometry::<OriginalVertexIndices>k__BackingField
	RuntimeObject* ___U3COriginalVertexIndicesU3Ek__BackingField_0;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Geometries.Geometry::<VertexDataIndices>k__BackingField
	RuntimeObject* ___U3CVertexDataIndicesU3Ek__BackingField_1;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Geometries.Geometry::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_2;
	// System.Int32 TriLibCore.Geometries.Geometry::<MaterialIndex>k__BackingField
	int32_t ___U3CMaterialIndexU3Ek__BackingField_3;
	// System.Boolean TriLibCore.Geometries.Geometry::<IsQuad>k__BackingField
	bool ___U3CIsQuadU3Ek__BackingField_4;
	// System.Int32 TriLibCore.Geometries.Geometry::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_5;
	// System.Int32 TriLibCore.Geometries.Geometry::<OriginalIndex>k__BackingField
	int32_t ___U3COriginalIndexU3Ek__BackingField_6;
	// System.Boolean TriLibCore.Geometries.Geometry::_disposed
	bool ____disposed_7;
};

// UnityEngine.Gyroscope
struct Gyroscope_tA4CEC0F47FFB4CEB90410CC6B860D052BB35BE9E  : public RuntimeObject
{
	// System.Int32 UnityEngine.Gyroscope::m_GyroIndex
	int32_t ___m_GyroIndex_0;
};

// Firebase.Crashlytics.Impl
struct Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF  : public RuntimeObject
{
};

// UnityEngine.Input
struct Input_t47D83E2A50E6AF7F8A47AA06FBEF9EBE6BBC22BB  : public RuntimeObject
{
};

// UnityEngine.XR.InputDevices
struct InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputDevices
struct InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.XR.InputDevices
struct InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_marshaled_com
{
};

// UnityEngine.XR.InputTracking
struct InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1  : public RuntimeObject
{
};

// ImaginationOverflow.UniversalDeepLinking.LinkActivation
struct LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9  : public RuntimeObject
{
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::<Uri>k__BackingField
	String_t* ___U3CUriU3Ek__BackingField_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::<RawQueryString>k__BackingField
	String_t* ___U3CRawQueryStringU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.LinkActivation::<QueryString>k__BackingField
	Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___U3CQueryStringU3Ek__BackingField_2;
};

// ImaginationOverflow.UniversalDeepLinking.LinkInformation
struct LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A  : public RuntimeObject
{
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkInformation::_scheme
	String_t* ____scheme_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkInformation::_host
	String_t* ____host_1;
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkInformation::_path
	String_t* ____path_2;
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// Firebase.Crashlytics.Metadata
struct Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094  : public RuntimeObject
{
	// System.String Firebase.Crashlytics.Metadata::uv
	String_t* ___uv_0;
	// System.Boolean Firebase.Crashlytics.Metadata::idb
	bool ___idb_1;
	// System.String Firebase.Crashlytics.Metadata::pt
	String_t* ___pt_2;
	// System.Int32 Firebase.Crashlytics.Metadata::pc
	int32_t ___pc_3;
	// System.Int32 Firebase.Crashlytics.Metadata::pf
	int32_t ___pf_4;
	// System.Int32 Firebase.Crashlytics.Metadata::sms
	int32_t ___sms_5;
	// System.Int32 Firebase.Crashlytics.Metadata::gms
	int32_t ___gms_6;
	// System.Int32 Firebase.Crashlytics.Metadata::gdid
	int32_t ___gdid_7;
	// System.Int32 Firebase.Crashlytics.Metadata::gdvid
	int32_t ___gdvid_8;
	// System.String Firebase.Crashlytics.Metadata::gdn
	String_t* ___gdn_9;
	// System.String Firebase.Crashlytics.Metadata::gdv
	String_t* ___gdv_10;
	// System.String Firebase.Crashlytics.Metadata::gdver
	String_t* ___gdver_11;
	// UnityEngine.Rendering.GraphicsDeviceType Firebase.Crashlytics.Metadata::gdt
	int32_t ___gdt_12;
	// System.Int32 Firebase.Crashlytics.Metadata::gsl
	int32_t ___gsl_13;
	// System.Int32 Firebase.Crashlytics.Metadata::grtc
	int32_t ___grtc_14;
	// UnityEngine.Rendering.CopyTextureSupport Firebase.Crashlytics.Metadata::gcts
	int32_t ___gcts_15;
	// System.Int32 Firebase.Crashlytics.Metadata::gmts
	int32_t ___gmts_16;
	// System.String Firebase.Crashlytics.Metadata::ss
	String_t* ___ss_17;
	// System.Single Firebase.Crashlytics.Metadata::sdpi
	float ___sdpi_18;
	// System.Int32 Firebase.Crashlytics.Metadata::srr
	int32_t ___srr_19;
};

// Firebase.Crashlytics.MetadataBuilder
struct MetadataBuilder_t7BB701F903E4674E17AF9A4C8EE07943B6616FE9  : public RuntimeObject
{
};

// UnityEngine.Networking.MultipartFormDataSection
struct MultipartFormDataSection_t655FED91F26A4D9444E0707D13C82C6A732288BC  : public RuntimeObject
{
	// System.String UnityEngine.Networking.MultipartFormDataSection::name
	String_t* ___name_0;
	// System.Byte[] UnityEngine.Networking.MultipartFormDataSection::data
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data_1;
	// System.String UnityEngine.Networking.MultipartFormDataSection::content
	String_t* ___content_2;
};

// UnityEngine.Networking.MultipartFormFileSection
struct MultipartFormFileSection_t87881191832E7F9AC25612810A3BE55555D398DB  : public RuntimeObject
{
	// System.String UnityEngine.Networking.MultipartFormFileSection::name
	String_t* ___name_0;
	// System.Byte[] UnityEngine.Networking.MultipartFormFileSection::data
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data_1;
	// System.String UnityEngine.Networking.MultipartFormFileSection::file
	String_t* ___file_2;
	// System.String UnityEngine.Networking.MultipartFormFileSection::content
	String_t* ___content_3;
};

// NativeGallery
struct NativeGallery_t5E55EACA16F196F4214E0A3AB3A39B5CF555EB0B  : public RuntimeObject
{
};

// UnityEngineInternal.Input.NativeInputSystem
struct NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE  : public RuntimeObject
{
};

// NativeShare
struct NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B  : public RuntimeObject
{
	// System.String NativeShare::subject
	String_t* ___subject_2;
	// System.String NativeShare::text
	String_t* ___text_3;
	// System.String NativeShare::title
	String_t* ___title_4;
	// System.String NativeShare::url
	String_t* ___url_5;
	// System.Collections.Generic.List`1<System.String> NativeShare::targetPackages
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___targetPackages_6;
	// System.Collections.Generic.List`1<System.String> NativeShare::targetClasses
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___targetClasses_7;
	// System.Collections.Generic.List`1<System.String> NativeShare::files
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___files_8;
	// System.Collections.Generic.List`1<System.String> NativeShare::mimes
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___mimes_9;
	// NativeShare/ShareResultCallback NativeShare::callback
	ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* ___callback_10;
};

// Byn.Awrtc.NetworkConfig
struct NetworkConfig_tDE09348A384F9FCF79DC5CE5BB4DFEECF9AB0766  : public RuntimeObject
{
	// System.Collections.Generic.List`1<Byn.Awrtc.IceServer> Byn.Awrtc.NetworkConfig::mIceServers
	List_1_t671F6F061AA091834868CE5CC45E6E1A7E372A79* ___mIceServers_0;
	// System.String Byn.Awrtc.NetworkConfig::mSignalingUrl
	String_t* ___mSignalingUrl_1;
	// System.Boolean Byn.Awrtc.NetworkConfig::mAllowRenegotiation
	bool ___mAllowRenegotiation_2;
	// System.Boolean Byn.Awrtc.NetworkConfig::mIsConference
	bool ___mIsConference_3;
};

// TriLibCore.Obj.ObjMaterial
struct ObjMaterial_t333DED08F3CA57CC38255E5187814EB8821FEB9A  : public RuntimeObject
{
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> TriLibCore.Obj.ObjMaterial::_properties
	RuntimeObject* ____properties_0;
	// System.String TriLibCore.Obj.ObjMaterial::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Boolean TriLibCore.Obj.ObjMaterial::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_2;
	// System.Int32 TriLibCore.Obj.ObjMaterial::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_3;
	// System.Boolean TriLibCore.Obj.ObjMaterial::<Processing>k__BackingField
	bool ___U3CProcessingU3Ek__BackingField_4;
	// System.Boolean TriLibCore.Obj.ObjMaterial::<Processed>k__BackingField
	bool ___U3CProcessedU3Ek__BackingField_5;
};

// UnityEngine.Physics
struct Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56  : public RuntimeObject
{
};

// UnityEngine.Physics2D
struct Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D  : public RuntimeObject
{
};

// Firebase.Platform.PlatformInformation
struct PlatformInformation_t6F0BE1D51BAD01D2A08CEC7ED766A21511F40E94  : public RuntimeObject
{
};

// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration
struct PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D  : public RuntimeObject
{
	// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::_domainProtocols
	List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ____domainProtocols_0;
	// System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation> ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::_deepLinkingProtocols
	List_1_t57E1B345BBE5193FC2EB5DD1F577C2D8542C3EC8* ____deepLinkingProtocols_1;
	// System.Boolean ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration::_initialized
	bool ____initialized_2;
};

// TriLibCore.Ply.PlyElement
struct PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<System.Int64,TriLibCore.Ply.PlyProperty> TriLibCore.Ply.PlyElement::Properties
	Dictionary_2_t3590525077E0FD6F543FB7039AB1D8A322E78680* ___Properties_0;
	// System.Int32 TriLibCore.Ply.PlyElement::Count
	int32_t ___Count_1;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<TriLibCore.Ply.PlyValue>> TriLibCore.Ply.PlyElement::Data
	List_1_t4FF1C54FF732A29B740EFAE0990CF8D49D1154C5* ___Data_2;
};

// TriLibCore.Ply.PlyMaterial
struct PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6  : public RuntimeObject
{
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> TriLibCore.Ply.PlyMaterial::_properties
	RuntimeObject* ____properties_0;
	// System.String TriLibCore.Ply.PlyMaterial::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Boolean TriLibCore.Ply.PlyMaterial::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_2;
	// System.Int32 TriLibCore.Ply.PlyMaterial::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_3;
	// System.Boolean TriLibCore.Ply.PlyMaterial::<Processing>k__BackingField
	bool ___U3CProcessingU3Ek__BackingField_4;
	// System.Boolean TriLibCore.Ply.PlyMaterial::<Processed>k__BackingField
	bool ___U3CProcessedU3Ek__BackingField_5;
};

// TriLibCore.Ply.PlyProcessor
struct PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F  : public RuntimeObject
{
	// TriLibCore.Ply.Reader.PlyReader TriLibCore.Ply.PlyProcessor::_reader
	PlyReader_t928287D5EE9717516589F3156C6D123C66E0C66F* ____reader_2;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Ply.PlyProcessor::_allVertices
	RuntimeObject* ____allVertices_3;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Ply.PlyProcessor::_allNormals
	RuntimeObject* ____allNormals_4;
	// System.Collections.Generic.IList`1<UnityEngine.Color> TriLibCore.Ply.PlyProcessor::_allColors
	RuntimeObject* ____allColors_5;
	// System.Collections.Generic.IList`1<UnityEngine.Vector2> TriLibCore.Ply.PlyProcessor::_allUVs
	RuntimeObject* ____allUVs_6;
	// System.Int32 TriLibCore.Ply.PlyProcessor::_floatCount
	int32_t ____floatCount_7;
	// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute> TriLibCore.Ply.PlyProcessor::_vertexAttributes
	HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* ____vertexAttributes_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Ply.PlyProcessor::_geometryGroup
	RuntimeObject* ____geometryGroup_9;
};

// TriLibCore.Ply.PlyProperty
struct PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362  : public RuntimeObject
{
	// TriLibCore.Ply.PlyPropertyType TriLibCore.Ply.PlyProperty::Type
	int32_t ___Type_0;
	// System.Int32 TriLibCore.Ply.PlyProperty::Offset
	int32_t ___Offset_1;
};

// UnityEngine.SpatialTracking.PoseDataSource
struct PoseDataSource_tF08072CF4DBA9641E6AE6D1DBA1D421DA9C46858  : public RuntimeObject
{
};

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449  : public RuntimeObject
{
	// System.String[] TriLibCore.ReaderBase::_loadingStepEnumNames
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____loadingStepEnumNames_1;
	// TriLibCore.AssetLoaderContext TriLibCore.ReaderBase::<AssetLoaderContext>k__BackingField
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___U3CAssetLoaderContextU3Ek__BackingField_2;
	// System.String TriLibCore.ReaderBase::_filename
	String_t* ____filename_3;
	// System.Action`2<TriLibCore.AssetLoaderContext,System.Single> TriLibCore.ReaderBase::_onProgress
	Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ____onProgress_4;
	// System.Int32 TriLibCore.ReaderBase::_nameCounter
	int32_t ____nameCounter_5;
	// System.Int32 TriLibCore.ReaderBase::_materialCounter
	int32_t ____materialCounter_6;
	// System.Int32 TriLibCore.ReaderBase::_textureCounter
	int32_t ____textureCounter_7;
	// System.Int32 TriLibCore.ReaderBase::_geometryGroupCounter
	int32_t ____geometryGroupCounter_8;
	// System.Int32 TriLibCore.ReaderBase::_animationCounter
	int32_t ____animationCounter_9;
};

// UnityEngine.RectTransformUtility
struct RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244  : public RuntimeObject
{
};

// UnityEngine.RemoteSettings
struct RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250  : public RuntimeObject
{
};

// HDRLoader.Rgbe
struct Rgbe_tAF48B77C07C8D792D42D719D60C440F719B9BA96  : public RuntimeObject
{
};

// Byn.Awrtc.SLog
struct SLog_tB306D01548C5F9057FEBA18F1046B6FBCCBB1721  : public RuntimeObject
{
};

// Firebase.Platform.Services
struct Services_t4EDE95322734B379A0DA3AAEF079E28E9132D69E  : public RuntimeObject
{
};

// Firebase.Crashlytics.StackTraceParser
struct StackTraceParser_tCD308CD049C1C2B3A198DBBDB3357B628F793B7D  : public RuntimeObject
{
};

// UnityEngine.SubsystemDescriptor
struct SubsystemDescriptor_tF417D2751C69A8B0DD86162EBCE55F84D3493A71  : public RuntimeObject
{
	// System.String UnityEngine.SubsystemDescriptor::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
};

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore
struct SubsystemDescriptorStore_tEF3761B84B8C25EA4B93F94A487551820B268250  : public RuntimeObject
{
};

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider
struct SubsystemDescriptorWithProvider_t2A61A2C951A4A179E898CF207726BF6B5AF474D5  : public RuntimeObject
{
	// System.String UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.Type UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider::<providerType>k__BackingField
	Type_t* ___U3CproviderTypeU3Ek__BackingField_1;
	// System.Type UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider::<subsystemTypeOverride>k__BackingField
	Type_t* ___U3CsubsystemTypeOverrideU3Ek__BackingField_2;
};

// UnityEngine.SubsystemManager
struct SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824  : public RuntimeObject
{
};

// UnityEngine.SubsystemsImplementation.SubsystemProvider
struct SubsystemProvider_tE1865B8FE698C81A59AED35E0E536BD53F402455  : public RuntimeObject
{
	// System.Boolean UnityEngine.SubsystemsImplementation.SubsystemProvider::m_Running
	bool ___m_Running_0;
};

// UnityEngine.SubsystemsImplementation.SubsystemWithProvider
struct SubsystemWithProvider_tC72E35EE2D413A4B0635B058154BABF265F31242  : public RuntimeObject
{
	// System.Boolean UnityEngine.SubsystemsImplementation.SubsystemWithProvider::<running>k__BackingField
	bool ___U3CrunningU3Ek__BackingField_0;
	// UnityEngine.SubsystemsImplementation.SubsystemProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider::<providerBase>k__BackingField
	SubsystemProvider_tE1865B8FE698C81A59AED35E0E536BD53F402455* ___U3CproviderBaseU3Ek__BackingField_1;
};

// System.Threading.SynchronizationContext
struct SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0  : public RuntimeObject
{
	// System.Threading.SynchronizationContextProperties System.Threading.SynchronizationContext::_props
	int32_t ____props_0;
};

// Firebase.Platform.Default.SystemClock
struct SystemClock_t56839C5A7890CD3F89A336EE0173FAA84AB3FA13  : public RuntimeObject
{
};

// TriLibCore.ThreeMf.ThreeMfMaterial
struct ThreeMfMaterial_tFBC1F52BB8A3BE4740BAF19E80D1C1C52F312424  : public RuntimeObject
{
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> TriLibCore.ThreeMf.ThreeMfMaterial::_properties
	RuntimeObject* ____properties_0;
	// System.String TriLibCore.ThreeMf.ThreeMfMaterial::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Boolean TriLibCore.ThreeMf.ThreeMfMaterial::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_2;
	// System.Int32 TriLibCore.ThreeMf.ThreeMfMaterial::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_3;
	// System.Boolean TriLibCore.ThreeMf.ThreeMfMaterial::<Processing>k__BackingField
	bool ___U3CProcessingU3Ek__BackingField_4;
	// System.Boolean TriLibCore.ThreeMf.ThreeMfMaterial::<Processed>k__BackingField
	bool ___U3CProcessedU3Ek__BackingField_5;
};

// TriLibCore.ThreeMf.ThreeMfProcessor
struct ThreeMfProcessor_t15100D0D50076EC723D3D17136EE2280EB2A4E8C  : public RuntimeObject
{
	// TriLibCore.ThreeMf.Reader.ThreeMfReader TriLibCore.ThreeMf.ThreeMfProcessor::_reader
	ThreeMfReader_t8F4D229EE9D88E8F68C2AC4FA06C66477A7545C1* ____reader_4;
	// TriLibCore.ThreeMf.ThreeMfRootModel TriLibCore.ThreeMf.ThreeMfProcessor::_rootModel
	ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396* ____rootModel_5;
	// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfTexture2D,TriLibCore.ThreeMf.ThreeMfTexture> TriLibCore.ThreeMf.ThreeMfProcessor::_allTextures
	Dictionary_2_tF76A5254FF71CC9D5D32CAB73E7A4AB583A904E9* ____allTextures_6;
	// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfBase,TriLibCore.ThreeMf.ThreeMfMaterial> TriLibCore.ThreeMf.ThreeMfProcessor::_allMaterials
	Dictionary_2_tC950A32EAFFC25F50CB10E4BA94AB529CDADA0E8* ____allMaterials_7;
	// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfTexture2D,TriLibCore.ThreeMf.ThreeMfMaterial> TriLibCore.ThreeMf.ThreeMfProcessor::_allTextureMaterials
	Dictionary_2_tFBFB8D749ADDCE1FBCCC7A11924995880D5B6A4D* ____allTextureMaterials_8;
	// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfColorGroup,System.Collections.Generic.IList`1<UnityEngine.Color>> TriLibCore.ThreeMf.ThreeMfProcessor::_allConvertedColorGroups
	Dictionary_2_t230D84F7213CBDDD7098D9E66DC0079B1EE334FE* ____allConvertedColorGroups_9;
	// System.Collections.Generic.Dictionary`2<IxMilia.ThreeMf.ThreeMfTexture2DGroup,System.Collections.Generic.IList`1<UnityEngine.Vector2>> TriLibCore.ThreeMf.ThreeMfProcessor::_allConvertedCoordinates
	Dictionary_2_t32E2A37AC16B1C83AE612366276E6E07A19064C8* ____allConvertedCoordinates_10;
};

// UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription
struct TrackedPoseDriverDataDescription_tDD80D37EB4F938007BCE0FAB9705032594E4D9FF  : public RuntimeObject
{
};

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative
struct UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938  : public RuntimeObject
{
};

// System.Text.Unicode.UnicodeHelpers
struct UnicodeHelpers_t5A5A5BBFB4BD837753ED0CAC899C31F43C701F71  : public RuntimeObject
{
};

// System.Text.Unicode.UnicodeRange
struct UnicodeRange_t39A4F977F558B4F00AFE470AD6573FA988BDA836  : public RuntimeObject
{
	// System.Int32 System.Text.Unicode.UnicodeRange::<FirstCodePoint>k__BackingField
	int32_t ___U3CFirstCodePointU3Ek__BackingField_0;
	// System.Int32 System.Text.Unicode.UnicodeRange::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_1;
};

// System.Text.Unicode.UnicodeRanges
struct UnicodeRanges_tE4E52421F705A0AC65C0E17C05CA14DA03E037C7  : public RuntimeObject
{
};

// Firebase.Unity.UnityLoggingService
struct UnityLoggingService_t6BE39D9F1F1EFE495EB479964B33753AA2C87CBB  : public RuntimeObject
{
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.WWWForm
struct WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045  : public RuntimeObject
{
	// System.Collections.Generic.List`1<System.Byte[]> UnityEngine.WWWForm::formData
	List_1_tBFF9DD9FFA06F20E74F9D7AD36610BD754D353A4* ___formData_0;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fieldNames
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___fieldNames_1;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fileNames
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___fileNames_2;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::types
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___types_3;
	// System.Byte[] UnityEngine.WWWForm::boundary
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___boundary_4;
	// System.Boolean UnityEngine.WWWForm::containsFiles
	bool ___containsFiles_5;
};

// UnityEngine.WWWTranscoder
struct WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B  : public RuntimeObject
{
};

// UnityEngineInternal.WebRequestUtils
struct WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443  : public RuntimeObject
{
};

// UnityEngine.XR.XRDevice
struct XRDevice_tD076A68EFE413B3EEEEA362BE0364A488B58F194  : public RuntimeObject
{
};

// UnityEngine.XR.Management.XRManagementAnalytics
struct XRManagementAnalytics_t9A6F8603FBB618B8FEC7A6BF3FD9615C61C1063F  : public RuntimeObject
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// Firebase.Crashlytics.AndroidImpl/<Log>c__AnonStorey0
struct U3CLogU3Ec__AnonStorey0_tDB91FA9FBE3AC12BEF4C7C6281D3DA6E16AE8AB2  : public RuntimeObject
{
	// System.String Firebase.Crashlytics.AndroidImpl/<Log>c__AnonStorey0::message
	String_t* ___message_0;
	// Firebase.Crashlytics.AndroidImpl Firebase.Crashlytics.AndroidImpl/<Log>c__AnonStorey0::$this
	AndroidImpl_t09BB72854905028A1DF3FBA8772392723D2CCD76* ___U24this_1;
};

// Firebase.Crashlytics.AndroidImpl/<LogException>c__AnonStorey3
struct U3CLogExceptionU3Ec__AnonStorey3_t848B0B308CEC5E67144B287AB58F9E773C4393FA  : public RuntimeObject
{
	// Firebase.Crashlytics.LoggedException Firebase.Crashlytics.AndroidImpl/<LogException>c__AnonStorey3::loggedException
	LoggedException_t43B89090462BFFD9B76040EF52EE2EFD63359887* ___loggedException_0;
	// Firebase.Crashlytics.StackFrames Firebase.Crashlytics.AndroidImpl/<LogException>c__AnonStorey3::frames
	StackFrames_t8F4306185C13967101E94E78F0D2312DDC4401AC* ___frames_1;
	// Firebase.Crashlytics.AndroidImpl Firebase.Crashlytics.AndroidImpl/<LogException>c__AnonStorey3::$this
	AndroidImpl_t09BB72854905028A1DF3FBA8772392723D2CCD76* ___U24this_2;
};

// Firebase.Crashlytics.AndroidImpl/<SetCrashlyticsCollectionEnabled>c__AnonStorey4
struct U3CSetCrashlyticsCollectionEnabledU3Ec__AnonStorey4_tEC1D936F42B52A3B8DB09F7440CBC7DC100EE46F  : public RuntimeObject
{
	// System.Boolean Firebase.Crashlytics.AndroidImpl/<SetCrashlyticsCollectionEnabled>c__AnonStorey4::enabled
	bool ___enabled_0;
	// Firebase.Crashlytics.AndroidImpl Firebase.Crashlytics.AndroidImpl/<SetCrashlyticsCollectionEnabled>c__AnonStorey4::$this
	AndroidImpl_t09BB72854905028A1DF3FBA8772392723D2CCD76* ___U24this_1;
};

// Firebase.Crashlytics.AndroidImpl/<SetCustomKey>c__AnonStorey1
struct U3CSetCustomKeyU3Ec__AnonStorey1_t812094D30223D5102E7F6EFC1CE9A4E5CA7FE73F  : public RuntimeObject
{
	// System.String Firebase.Crashlytics.AndroidImpl/<SetCustomKey>c__AnonStorey1::key
	String_t* ___key_0;
	// System.String Firebase.Crashlytics.AndroidImpl/<SetCustomKey>c__AnonStorey1::value
	String_t* ___value_1;
	// Firebase.Crashlytics.AndroidImpl Firebase.Crashlytics.AndroidImpl/<SetCustomKey>c__AnonStorey1::$this
	AndroidImpl_t09BB72854905028A1DF3FBA8772392723D2CCD76* ___U24this_2;
};

// Firebase.Crashlytics.AndroidImpl/<SetUserId>c__AnonStorey2
struct U3CSetUserIdU3Ec__AnonStorey2_tA0BB257062BA7E82DFE15BDD46B0E34921249A83  : public RuntimeObject
{
	// System.String Firebase.Crashlytics.AndroidImpl/<SetUserId>c__AnonStorey2::identifier
	String_t* ___identifier_0;
	// Firebase.Crashlytics.AndroidImpl Firebase.Crashlytics.AndroidImpl/<SetUserId>c__AnonStorey2::$this
	AndroidImpl_t09BB72854905028A1DF3FBA8772392723D2CCD76* ___U24this_1;
};

// UnityEngine.Animation/Enumerator
struct Enumerator_t81434F7D5603121F3D7BD6DB916FE1C755307530  : public RuntimeObject
{
	// UnityEngine.Animation UnityEngine.Animation/Enumerator::m_Outer
	Animation_t6593B06C39E3B139808B19F2C719C860F3F61040* ___m_Outer_0;
	// System.Int32 UnityEngine.Animation/Enumerator::m_CurrentIndex
	int32_t ___m_CurrentIndex_1;
};

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c
struct U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44  : public RuntimeObject
{
};

// UnityEngine.AudioSettings/Mobile
struct Mobile_t304A73480DF447472BDB16BA19A9E4FE2C8CB2DD  : public RuntimeObject
{
};

// Firebase.Crashlytics.Crashlytics/PlatformAccessor
struct PlatformAccessor_tB02E4C5B35E2A951376B828D3F51E6623A65336D  : public RuntimeObject
{
};

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970  : public RuntimeObject
{
};

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGPendingException
struct SWIGPendingException_t9C3365ED3C047C6935279BA7C05F89F3BDE54E35  : public RuntimeObject
{
};

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGStringHelper
struct SWIGStringHelper_tA8CF9EF78BD75BC8C5CB597CC99E74764BFE361B  : public RuntimeObject
{
};

// Firebase.Platform.FirebaseHandler/<CreatePartialOnMainThread>c__AnonStorey0
struct U3CCreatePartialOnMainThreadU3Ec__AnonStorey0_t910286566361BA0521D1C41AAA1B4F473AD5B2FE  : public RuntimeObject
{
	// Firebase.Platform.IFirebaseAppUtils Firebase.Platform.FirebaseHandler/<CreatePartialOnMainThread>c__AnonStorey0::appUtils
	RuntimeObject* ___appUtils_0;
};

// Firebase.Messaging.FirebaseMessaging/Listener
struct Listener_tC16E83118F05783B5CA41A3F81ABF1A2C83D7BF7  : public RuntimeObject
{
	// Firebase.Messaging.FirebaseMessaging/Listener/MessageReceivedDelegate Firebase.Messaging.FirebaseMessaging/Listener::messageReceivedDelegate
	MessageReceivedDelegate_t0426330610D984E008DF662FD99680187AEA49DC* ___messageReceivedDelegate_0;
	// Firebase.Messaging.FirebaseMessaging/Listener/TokenReceivedDelegate Firebase.Messaging.FirebaseMessaging/Listener::tokenReceivedDelegate
	TokenReceivedDelegate_t3893429AE7EEC18BBD788219BAE8961E21EC4AE0* ___tokenReceivedDelegate_1;
	// Firebase.FirebaseApp Firebase.Messaging.FirebaseMessaging/Listener::app
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___app_2;
};

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C  : public RuntimeObject
{
};

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException
struct SWIGPendingException_t0409902F046D41D7A8D2333129C7E51E76EC1909  : public RuntimeObject
{
};

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t953C1EA2870BB0BF9A25500F62877BBBA6D44AE9  : public RuntimeObject
{
};

// Unity.VectorGraphics.InternalBridge/<>c
struct U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3  : public RuntimeObject
{
};

// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_tD80C32708DF9E47C151599335772558DC8B3D9E8  : public RuntimeObject
{
	// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass4_0::<>4__this
	NGMediaReceiveCallbackAndroid_tB8DDBFAD614592BF98999BEAC00A75E0D420CC52* ___U3CU3E4__this_0;
	// System.String NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass4_0::path
	String_t* ___path_1;
};

// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_tAE5229FA2486EA375EA2E7C0D014785D19289D9B  : public RuntimeObject
{
	// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass5_0::<>4__this
	NGMediaReceiveCallbackAndroid_tB8DDBFAD614592BF98999BEAC00A75E0D420CC52* ___U3CU3E4__this_0;
	// System.String[] NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass5_0::result
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___result_1;
};

// HDRLoader.Rgbe/Header
struct Header_tA6CD0237F1CA9F61AFCA3C7FA51ECEBC4353E808  : public RuntimeObject
{
	// System.Int32 HDRLoader.Rgbe/Header::_valid
	int32_t ____valid_0;
	// System.Single HDRLoader.Rgbe/Header::<Exposure>k__BackingField
	float ___U3CExposureU3Ek__BackingField_1;
	// System.Single HDRLoader.Rgbe/Header::<Gamma>k__BackingField
	float ___U3CGammaU3Ek__BackingField_2;
	// System.Int32 HDRLoader.Rgbe/Header::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_3;
	// System.String HDRLoader.Rgbe/Header::<ProgramType>k__BackingField
	String_t* ___U3CProgramTypeU3Ek__BackingField_4;
	// System.Int32 HDRLoader.Rgbe/Header::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_5;
};

// Firebase.Crashlytics.StackFrames/StackFramesEnumerator
struct StackFramesEnumerator_tDC4E1B8FC390A2688F9CAF90ACF938CBD75605A7  : public RuntimeObject
{
	// Firebase.Crashlytics.StackFrames Firebase.Crashlytics.StackFrames/StackFramesEnumerator::collectionRef
	StackFrames_t8F4306185C13967101E94E78F0D2312DDC4401AC* ___collectionRef_0;
	// System.Int32 Firebase.Crashlytics.StackFrames/StackFramesEnumerator::currentIndex
	int32_t ___currentIndex_1;
	// System.Object Firebase.Crashlytics.StackFrames/StackFramesEnumerator::currentObject
	RuntimeObject* ___currentObject_2;
	// System.Int32 Firebase.Crashlytics.StackFrames/StackFramesEnumerator::currentSize
	int32_t ___currentSize_3;
};

// TriLibCore.ThreeMf.ThreeMfProcessor/<>c
struct U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138  : public RuntimeObject
{
};

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey3
struct U3CSendU3Ec__AnonStorey3_t0877E086BABDA1BDD2F95E0E90A5C52922B00ABB  : public RuntimeObject
{
	// System.Threading.SendOrPostCallback Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey3::d
	SendOrPostCallback_t5C292A12062F24027A98492F52ECFE9802AA6F0E* ___d_0;
};

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey4
struct U3CSendU3Ec__AnonStorey4_tBD2906EC335BDD635256AEFAF7A0DD5534EB873D  : public RuntimeObject
{
	// System.Threading.ManualResetEvent Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey4::newSignal
	ManualResetEvent_t63959486AA41A113A4353D0BF4A68E77EBA0A158* ___newSignal_0;
	// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey3 Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey4::<>f__ref$3
	U3CSendU3Ec__AnonStorey3_t0877E086BABDA1BDD2F95E0E90A5C52922B00ABB* ___U3CU3Ef__refU243_1;
};

// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1
struct U3CSendCoroutineU3Ec__AnonStorey1_tB73A38DA19EEBC025199918DCC1326071057950C  : public RuntimeObject
{
	// System.Func`1<System.Collections.IEnumerator> Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1::coroutine
	Func_1_t5C7016A1FC354629920CF15E7F2AD5A6E6D06941* ___coroutine_0;
	// Firebase.Unity.UnitySynchronizationContext Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1::$this
	UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690* ___U24this_1;
};

// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2
struct U3CSendCoroutineU3Ec__AnonStorey2_tF3318E0B5D0C9626BEE7239B69C936A6ADEB446E  : public RuntimeObject
{
	// System.Threading.ManualResetEvent Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2::newSignal
	ManualResetEvent_t63959486AA41A113A4353D0BF4A68E77EBA0A158* ___newSignal_0;
	// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1 Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2::<>f__ref$1
	U3CSendCoroutineU3Ec__AnonStorey1_tB73A38DA19EEBC025199918DCC1326071057950C* ___U3CU3Ef__refU241_1;
};

// Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0
struct U3CSignaledCoroutineU3Ec__Iterator0_tC58703BC1C0EB16184A306B8AACAE68243FF19CC  : public RuntimeObject
{
	// System.Func`1<System.Collections.IEnumerator> Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::coroutine
	Func_1_t5C7016A1FC354629920CF15E7F2AD5A6E6D06941* ___coroutine_0;
	// System.Threading.ManualResetEvent Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::newSignal
	ManualResetEvent_t63959486AA41A113A4353D0BF4A68E77EBA0A158* ___newSignal_1;
	// System.Object Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::$current
	RuntimeObject* ___U24current_2;
	// System.Boolean Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_4;
};

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4
struct U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D  : public RuntimeObject
{
	// System.Int32 ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::<>4__this
	UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* ___U3CU3E4__this_2;
};

// Firebase.Messaging.FirebaseMessaging/Listener/<TokenReceivedDelegateMethod>c__AnonStorey1
struct U3CTokenReceivedDelegateMethodU3Ec__AnonStorey1_t7B9B1F8EA01DA9EDF63D27A60D3D214AC248BD48  : public RuntimeObject
{
	// System.String Firebase.Messaging.FirebaseMessaging/Listener/<TokenReceivedDelegateMethod>c__AnonStorey1::token
	String_t* ___token_0;
};

// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t222A0004720CA576CAC089134B24CBDA80693E68  : public RuntimeObject
{
	// System.Tuple`2<System.Threading.SendOrPostCallback,System.Object> Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::<entry>__0
	Tuple_2_t9EC4E1A9EFB1ABACA7C36B1761CBDB693089F4DA* ___U3CentryU3E__0_0;
	// System.Object Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$this
	SynchronizationContextBehavoir_t77EF1109836806C11E4F2C8ECF2867D9531FEBBA* ___U24this_2;
	// System.Object Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$current
	RuntimeObject* ___U24current_3;
	// System.Boolean Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::$PC
	int32_t ___U24PC_5;
};

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.Management.XRLoader>
struct Enumerator_tE4C063339BE1A617569244A7F77DF2FBF2ABFA4A 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tA1A04BD6B1EE83992AE369D5DB31A028E9B57822* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	XRLoader_t80B1B1934C40561C5352ABC95D567DC2A7C9C976* ____current_3;
};

// UnityEngine.XR.InputFeatureUsage`1<System.Boolean>
struct InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 
{
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.Eyes>
struct InputFeatureUsage_1_tD3FEDCAE0D7F51C7B42182162ACE726E68166B38 
{
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.Hand>
struct InputFeatureUsage_1_t64C1AA42D6E8BD57C54C7E891BD79A70A0F3A170 
{
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.InputTrackingState>
struct InputFeatureUsage_1_t4EF7DDCAC35EE23BA72694AC2AB76CF4A879FFD9 
{
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion>
struct InputFeatureUsage_1_t8489CEC68B1EC178F2634079A9D7CD9E90D3CF5D 
{
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<System.Single>
struct InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 
{
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>
struct InputFeatureUsage_1_tEB160A05BCDCCA4F96072CBA0866498D06B9A27C 
{
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3>
struct InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 
{
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
#define InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com_define
struct InputFeatureUsage_1_t66EDAF8AFFA2E9DDC0248C48B76ADAB8E2728858_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// Unity.Collections.NativeArray`1<System.Byte>
struct NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF 
{
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;
};

// Unity.Collections.NativeArray`1<System.Char>
struct NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A 
{
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;
};

// System.Nullable`1<System.Double>
struct Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	double ___value_1;
};

// System.Text.Internal.AllowedCharactersBitmap
struct AllowedCharactersBitmap_tA1BC142B179A112F4EAA6A6BF578B0BFD58E8BED 
{
	// System.UInt32[] System.Text.Internal.AllowedCharactersBitmap::_allowedCharacters
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ____allowedCharacters_0;
};
// Native definition for P/Invoke marshalling of System.Text.Internal.AllowedCharactersBitmap
struct AllowedCharactersBitmap_tA1BC142B179A112F4EAA6A6BF578B0BFD58E8BED_marshaled_pinvoke
{
	Il2CppSafeArray/*NONE*/* ____allowedCharacters_0;
};
// Native definition for COM marshalling of System.Text.Internal.AllowedCharactersBitmap
struct AllowedCharactersBitmap_tA1BC142B179A112F4EAA6A6BF578B0BFD58E8BED_marshaled_com
{
	Il2CppSafeArray/*NONE*/* ____allowedCharacters_0;
};

// Firebase.Crashlytics.AndroidImpl
struct AndroidImpl_t09BB72854905028A1DF3FBA8772392723D2CCD76  : public Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF
{
	// Firebase.Crashlytics.FirebaseCrashlyticsInternal Firebase.Crashlytics.AndroidImpl::crashlyticsInternal
	FirebaseCrashlyticsInternal_t674756D0479C8ED14FE9E114B533CD274F9D6F40* ___crashlyticsInternal_6;
	// Firebase.FirebaseApp Firebase.Crashlytics.AndroidImpl::firebaseApp
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___firebaseApp_7;
};

// UnityEngine.AnimatorClipInfo
struct AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 
{
	// System.Int32 UnityEngine.AnimatorClipInfo::m_ClipInstanceID
	int32_t ___m_ClipInstanceID_0;
	// System.Single UnityEngine.AnimatorClipInfo::m_Weight
	float ___m_Weight_1;
};

// UnityEngine.AnimatorStateInfo
struct AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 
{
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;
};

// UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD 
{
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_FullPath
	int32_t ___m_FullPath_0;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_UserName
	int32_t ___m_UserName_1;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_Name
	int32_t ___m_Name_2;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_HasFixedDuration
	bool ___m_HasFixedDuration_3;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_Duration
	float ___m_Duration_4;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_NormalizedTime
	float ___m_NormalizedTime_5;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_AnyState
	bool ___m_AnyState_6;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_TransitionType
	int32_t ___m_TransitionType_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_pinvoke
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};
// Native definition for COM marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD_marshaled_com
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};

// UnityEngine.AssetFileNameExtensionAttribute
struct AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.AssetFileNameExtensionAttribute::<preferredExtension>k__BackingField
	String_t* ___U3CpreferredExtensionU3Ek__BackingField_0;
	// System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.AssetFileNameExtensionAttribute::<otherExtensions>k__BackingField
	RuntimeObject* ___U3CotherExtensionsU3Ek__BackingField_1;
};

// UnityEngine.XR.Bone
struct Bone_t2558B1DD1E4F405EA4D76A3B8D5149CA16011975 
{
	// System.UInt64 UnityEngine.XR.Bone::m_DeviceId
	uint64_t ___m_DeviceId_0;
	// System.UInt32 UnityEngine.XR.Bone::m_FeatureIndex
	uint32_t ___m_FeatureIndex_1;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// Byn.Awrtc.ConnectionId
struct ConnectionId_tEC0FFB08249CB4253D31AF1E9166778F5D22E2F7 
{
	// System.Int16 Byn.Awrtc.ConnectionId::id
	int16_t ___id_1;
};

// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_tD065D7D14CC8AA548815166AB8B8210D1B3C699F  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean System.Runtime.Serialization.DataContractAttribute::isReference
	bool ___isReference_0;
};

// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t8AE446BE9032B9BC8E7B2EDC785F5C6FA0E5BB73  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String System.Runtime.Serialization.DataMemberAttribute::name
	String_t* ___name_0;
	// System.Int32 System.Runtime.Serialization.DataMemberAttribute::order
	int32_t ___order_1;
	// System.Boolean System.Runtime.Serialization.DataMemberAttribute::isRequired
	bool ___isRequired_2;
	// System.Boolean System.Runtime.Serialization.DataMemberAttribute::emitDefaultValue
	bool ___emitDefaultValue_3;
};

// System.DateTime
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D 
{
	// System.UInt64 System.DateTime::_dateData
	uint64_t ____dateData_46;
};

// UnityEngine.UIElements.UIR.DrawBufferRange
struct DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4 
{
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::firstIndex
	int32_t ___firstIndex_0;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::indexCount
	int32_t ___indexCount_1;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::minIndexVal
	int32_t ___minIndexVal_2;
	// System.Int32 UnityEngine.UIElements.UIR.DrawBufferRange::vertsReferenced
	int32_t ___vertsReferenced_3;
};

// System.Runtime.Serialization.EnumMemberAttribute
struct EnumMemberAttribute_t65B5E85E642C96791DD6AE5EAD0276350954126F  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String System.Runtime.Serialization.EnumMemberAttribute::value
	String_t* ___value_0;
};

// UnityEngine.XR.Eyes
struct Eyes_t9FD3821575977E294F11E0EB92D2A4CA509ED8C7 
{
	// System.UInt64 UnityEngine.XR.Eyes::m_DeviceId
	uint64_t ___m_DeviceId_0;
	// System.UInt32 UnityEngine.XR.Eyes::m_FeatureIndex
	uint32_t ___m_FeatureIndex_1;
};

// UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756 
{
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_FaceIndex
	int32_t ___m_FaceIndex_0;
	// System.String UnityEngine.TextCore.FaceInfo::m_FamilyName
	String_t* ___m_FamilyName_1;
	// System.String UnityEngine.TextCore.FaceInfo::m_StyleName
	String_t* ___m_StyleName_2;
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_PointSize
	int32_t ___m_PointSize_3;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Scale
	float ___m_Scale_4;
	// System.Int32 UnityEngine.TextCore.FaceInfo::m_UnitsPerEM
	int32_t ___m_UnitsPerEM_5;
	// System.Single UnityEngine.TextCore.FaceInfo::m_LineHeight
	float ___m_LineHeight_6;
	// System.Single UnityEngine.TextCore.FaceInfo::m_AscentLine
	float ___m_AscentLine_7;
	// System.Single UnityEngine.TextCore.FaceInfo::m_CapLine
	float ___m_CapLine_8;
	// System.Single UnityEngine.TextCore.FaceInfo::m_MeanLine
	float ___m_MeanLine_9;
	// System.Single UnityEngine.TextCore.FaceInfo::m_Baseline
	float ___m_Baseline_10;
	// System.Single UnityEngine.TextCore.FaceInfo::m_DescentLine
	float ___m_DescentLine_11;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptOffset
	float ___m_SuperscriptOffset_12;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SuperscriptSize
	float ___m_SuperscriptSize_13;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptOffset
	float ___m_SubscriptOffset_14;
	// System.Single UnityEngine.TextCore.FaceInfo::m_SubscriptSize
	float ___m_SubscriptSize_15;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineOffset
	float ___m_UnderlineOffset_16;
	// System.Single UnityEngine.TextCore.FaceInfo::m_UnderlineThickness
	float ___m_UnderlineThickness_17;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughOffset
	float ___m_StrikethroughOffset_18;
	// System.Single UnityEngine.TextCore.FaceInfo::m_StrikethroughThickness
	float ___m_StrikethroughThickness_19;
	// System.Single UnityEngine.TextCore.FaceInfo::m_TabWidth
	float ___m_TabWidth_20;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_pinvoke
{
	int32_t ___m_FaceIndex_0;
	char* ___m_FamilyName_1;
	char* ___m_StyleName_2;
	int32_t ___m_PointSize_3;
	float ___m_Scale_4;
	int32_t ___m_UnitsPerEM_5;
	float ___m_LineHeight_6;
	float ___m_AscentLine_7;
	float ___m_CapLine_8;
	float ___m_MeanLine_9;
	float ___m_Baseline_10;
	float ___m_DescentLine_11;
	float ___m_SuperscriptOffset_12;
	float ___m_SuperscriptSize_13;
	float ___m_SubscriptOffset_14;
	float ___m_SubscriptSize_15;
	float ___m_UnderlineOffset_16;
	float ___m_UnderlineThickness_17;
	float ___m_StrikethroughOffset_18;
	float ___m_StrikethroughThickness_19;
	float ___m_TabWidth_20;
};
// Native definition for COM marshalling of UnityEngine.TextCore.FaceInfo
struct FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756_marshaled_com
{
	int32_t ___m_FaceIndex_0;
	Il2CppChar* ___m_FamilyName_1;
	Il2CppChar* ___m_StyleName_2;
	int32_t ___m_PointSize_3;
	float ___m_Scale_4;
	int32_t ___m_UnitsPerEM_5;
	float ___m_LineHeight_6;
	float ___m_AscentLine_7;
	float ___m_CapLine_8;
	float ___m_MeanLine_9;
	float ___m_Baseline_10;
	float ___m_DescentLine_11;
	float ___m_SuperscriptOffset_12;
	float ___m_SuperscriptSize_13;
	float ___m_SubscriptOffset_14;
	float ___m_SubscriptSize_15;
	float ___m_UnderlineOffset_16;
	float ___m_UnderlineThickness_17;
	float ___m_StrikethroughOffset_18;
	float ___m_StrikethroughThickness_19;
	float ___m_TabWidth_20;
};

// UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172 
{
	// System.String UnityEngine.TextCore.LowLevel.FontReference::familyName
	String_t* ___familyName_0;
	// System.String UnityEngine.TextCore.LowLevel.FontReference::styleName
	String_t* ___styleName_1;
	// System.Int32 UnityEngine.TextCore.LowLevel.FontReference::faceIndex
	int32_t ___faceIndex_2;
	// System.String UnityEngine.TextCore.LowLevel.FontReference::filePath
	String_t* ___filePath_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_pinvoke
{
	char* ___familyName_0;
	char* ___styleName_1;
	int32_t ___faceIndex_2;
	char* ___filePath_3;
};
// Native definition for COM marshalling of UnityEngine.TextCore.LowLevel.FontReference
struct FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172_marshaled_com
{
	Il2CppChar* ___familyName_0;
	Il2CppChar* ___styleName_1;
	int32_t ___faceIndex_2;
	Il2CppChar* ___filePath_3;
};

// UnityEngine.TextCore.GlyphMetrics
struct GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A 
{
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Width
	float ___m_Width_0;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Height
	float ___m_Height_1;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingX
	float ___m_HorizontalBearingX_2;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingY
	float ___m_HorizontalBearingY_3;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalAdvance
	float ___m_HorizontalAdvance_4;
};

// UnityEngine.TextCore.GlyphRect
struct GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D 
{
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Height
	int32_t ___m_Height_3;
};

// UnityEngine.TextCore.LowLevel.GlyphValueRecord
struct GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E 
{
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XPlacement
	float ___m_XPlacement_0;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YPlacement
	float ___m_YPlacement_1;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_XAdvance
	float ___m_XAdvance_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphValueRecord::m_YAdvance
	float ___m_YAdvance_3;
};

// System.Guid
struct Guid_t 
{
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;
};

// UnityEngine.XR.Hand
struct Hand_t67B90BC0D36CBC92DF7E38BD15463B925CB5912C 
{
	// System.UInt64 UnityEngine.XR.Hand::m_DeviceId
	uint64_t ___m_DeviceId_0;
	// System.UInt32 UnityEngine.XR.Hand::m_FeatureIndex
	uint32_t ___m_FeatureIndex_1;
};

// UnityEngine.HumanDescription
struct HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44 
{
	// UnityEngine.HumanBone[] UnityEngine.HumanDescription::human
	HumanBoneU5BU5D_t443B81D55400778CBB921DF04BE932ABF14BAA52* ___human_0;
	// UnityEngine.SkeletonBone[] UnityEngine.HumanDescription::skeleton
	SkeletonBoneU5BU5D_t84722CE071EF9295326D5A84492716DBC7A15A85* ___skeleton_1;
	// System.Single UnityEngine.HumanDescription::m_ArmTwist
	float ___m_ArmTwist_2;
	// System.Single UnityEngine.HumanDescription::m_ForeArmTwist
	float ___m_ForeArmTwist_3;
	// System.Single UnityEngine.HumanDescription::m_UpperLegTwist
	float ___m_UpperLegTwist_4;
	// System.Single UnityEngine.HumanDescription::m_LegTwist
	float ___m_LegTwist_5;
	// System.Single UnityEngine.HumanDescription::m_ArmStretch
	float ___m_ArmStretch_6;
	// System.Single UnityEngine.HumanDescription::m_LegStretch
	float ___m_LegStretch_7;
	// System.Single UnityEngine.HumanDescription::m_FeetSpacing
	float ___m_FeetSpacing_8;
	// System.Single UnityEngine.HumanDescription::m_GlobalScale
	float ___m_GlobalScale_9;
	// System.String UnityEngine.HumanDescription::m_RootMotionBoneName
	String_t* ___m_RootMotionBoneName_10;
	// System.Boolean UnityEngine.HumanDescription::m_HasTranslationDoF
	bool ___m_HasTranslationDoF_11;
	// System.Boolean UnityEngine.HumanDescription::m_HasExtraRoot
	bool ___m_HasExtraRoot_12;
	// System.Boolean UnityEngine.HumanDescription::m_SkeletonHasParents
	bool ___m_SkeletonHasParents_13;
};
// Native definition for P/Invoke marshalling of UnityEngine.HumanDescription
struct HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44_marshaled_pinvoke
{
	HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke* ___human_0;
	SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke* ___skeleton_1;
	float ___m_ArmTwist_2;
	float ___m_ForeArmTwist_3;
	float ___m_UpperLegTwist_4;
	float ___m_LegTwist_5;
	float ___m_ArmStretch_6;
	float ___m_LegStretch_7;
	float ___m_FeetSpacing_8;
	float ___m_GlobalScale_9;
	char* ___m_RootMotionBoneName_10;
	int32_t ___m_HasTranslationDoF_11;
	int32_t ___m_HasExtraRoot_12;
	int32_t ___m_SkeletonHasParents_13;
};
// Native definition for COM marshalling of UnityEngine.HumanDescription
struct HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44_marshaled_com
{
	HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_com* ___human_0;
	SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_com* ___skeleton_1;
	float ___m_ArmTwist_2;
	float ___m_ForeArmTwist_3;
	float ___m_UpperLegTwist_4;
	float ___m_LegTwist_5;
	float ___m_ArmStretch_6;
	float ___m_LegStretch_7;
	float ___m_FeetSpacing_8;
	float ___m_GlobalScale_9;
	Il2CppChar* ___m_RootMotionBoneName_10;
	int32_t ___m_HasTranslationDoF_11;
	int32_t ___m_HasExtraRoot_12;
	int32_t ___m_SkeletonHasParents_13;
};

// UnityEngine.Bindings.IgnoreAttribute
struct IgnoreAttribute_tAB3F6C4808BA16CD585D60A6353B3E0599DFCE4D  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.IgnoreAttribute::<DoesNotContributeToSize>k__BackingField
	bool ___U3CDoesNotContributeToSizeU3Ek__BackingField_0;
};

// UnityEngine.XR.InputDevice
struct InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD 
{
	// System.UInt64 UnityEngine.XR.InputDevice::m_DeviceId
	uint64_t ___m_DeviceId_1;
	// System.Boolean UnityEngine.XR.InputDevice::m_Initialized
	bool ___m_Initialized_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputDevice
struct InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD_marshaled_pinvoke
{
	uint64_t ___m_DeviceId_1;
	int32_t ___m_Initialized_2;
};
// Native definition for COM marshalling of UnityEngine.XR.InputDevice
struct InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD_marshaled_com
{
	uint64_t ___m_DeviceId_1;
	int32_t ___m_Initialized_2;
};

// UnityEngine.XR.InputFeatureUsage
struct InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599 
{
	// System.String UnityEngine.XR.InputFeatureUsage::m_Name
	String_t* ___m_Name_0;
	// UnityEngine.XR.InputFeatureType UnityEngine.XR.InputFeatureUsage::m_InternalType
	uint32_t ___m_InternalType_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage
struct InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599_marshaled_pinvoke
{
	char* ___m_Name_0;
	uint32_t ___m_InternalType_1;
};
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage
struct InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599_marshaled_com
{
	Il2CppChar* ___m_Name_0;
	uint32_t ___m_InternalType_1;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// UnityEngine.LayerMask
struct LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB 
{
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

// UnityEngine.XR.MeshId
struct MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC 
{
	// System.UInt64 UnityEngine.XR.MeshId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.XR.MeshId::m_SubId2
	uint64_t ___m_SubId2_2;
};

// Firebase.Messaging.MessageReceivedEventArgs
struct MessageReceivedEventArgs_tAFDBCC69B3DEC0F5EB76ACAA8D97A404E61F73E5  : public EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377
{
	// Firebase.Messaging.FirebaseMessage Firebase.Messaging.MessageReceivedEventArgs::<Message>k__BackingField
	FirebaseMessage_t09E58AB584806FC5C23F419AD7CDCE53969FE20F* ___U3CMessageU3Ek__BackingField_1;
};

// UnityEngine.NativeClassAttribute
struct NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.NativeClassAttribute::<QualifiedNativeName>k__BackingField
	String_t* ___U3CQualifiedNativeNameU3Ek__BackingField_0;
	// System.String UnityEngine.NativeClassAttribute::<Declaration>k__BackingField
	String_t* ___U3CDeclarationU3Ek__BackingField_1;
};

// UnityEngine.Bindings.NativeConditionalAttribute
struct NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeConditionalAttribute::<Condition>k__BackingField
	String_t* ___U3CConditionU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Bindings.NativeConditionalAttribute::<Enabled>k__BackingField
	bool ___U3CEnabledU3Ek__BackingField_1;
};

// UnityEngine.Bindings.NativeHeaderAttribute
struct NativeHeaderAttribute_t35DDAA41C31EEE4C94D2586F33D3EB26C0EA6F51  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeHeaderAttribute::<Header>k__BackingField
	String_t* ___U3CHeaderU3Ek__BackingField_0;
};

// UnityEngineInternal.Input.NativeInputEvent
struct NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8 
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					// UnityEngineInternal.Input.NativeInputEventType UnityEngineInternal.Input.NativeInputEvent::type
					int32_t ___type_1;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					int32_t ___type_1_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___sizeInBytes_2_OffsetPadding[4];
					// System.UInt16 UnityEngineInternal.Input.NativeInputEvent::sizeInBytes
					uint16_t ___sizeInBytes_2;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___sizeInBytes_2_OffsetPadding_forAlignmentOnly[4];
					uint16_t ___sizeInBytes_2_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceId_3_OffsetPadding[6];
					// System.UInt16 UnityEngineInternal.Input.NativeInputEvent::deviceId
					uint16_t ___deviceId_3;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___deviceId_3_OffsetPadding_forAlignmentOnly[6];
					uint16_t ___deviceId_3_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___time_4_OffsetPadding[8];
					// System.Double UnityEngineInternal.Input.NativeInputEvent::time
					double ___time_4;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___time_4_OffsetPadding_forAlignmentOnly[8];
					double ___time_4_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___eventId_5_OffsetPadding[16];
					// System.Int32 UnityEngineInternal.Input.NativeInputEvent::eventId
					int32_t ___eventId_5;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___eventId_5_OffsetPadding_forAlignmentOnly[16];
					int32_t ___eventId_5_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
			};
		};
		uint8_t NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8__padding[20];
	};
};

// UnityEngineInternal.Input.NativeInputEventBuffer
struct NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD 
{
	union
	{
		struct
		{
			union
			{
				#pragma pack(push, tp, 1)
				struct
				{
					// System.Void* UnityEngineInternal.Input.NativeInputEventBuffer::eventBuffer
					void* ___eventBuffer_0;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					void* ___eventBuffer_0_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___eventCount_1_OffsetPadding[8];
					// System.Int32 UnityEngineInternal.Input.NativeInputEventBuffer::eventCount
					int32_t ___eventCount_1;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___eventCount_1_OffsetPadding_forAlignmentOnly[8];
					int32_t ___eventCount_1_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___sizeInBytes_2_OffsetPadding[12];
					// System.Int32 UnityEngineInternal.Input.NativeInputEventBuffer::sizeInBytes
					int32_t ___sizeInBytes_2;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___sizeInBytes_2_OffsetPadding_forAlignmentOnly[12];
					int32_t ___sizeInBytes_2_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___capacityInBytes_3_OffsetPadding[16];
					// System.Int32 UnityEngineInternal.Input.NativeInputEventBuffer::capacityInBytes
					int32_t ___capacityInBytes_3;
				};
				#pragma pack(pop, tp)
				#pragma pack(push, tp, 1)
				struct
				{
					char ___capacityInBytes_3_OffsetPadding_forAlignmentOnly[16];
					int32_t ___capacityInBytes_3_forAlignmentOnly;
				};
				#pragma pack(pop, tp)
			};
		};
		uint8_t NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD__padding[20];
	};
};

// UnityEngine.Bindings.NativeMethodAttribute
struct NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeMethodAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<IsThreadSafe>k__BackingField
	bool ___U3CIsThreadSafeU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<IsFreeFunction>k__BackingField
	bool ___U3CIsFreeFunctionU3Ek__BackingField_2;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<ThrowsException>k__BackingField
	bool ___U3CThrowsExceptionU3Ek__BackingField_3;
	// System.Boolean UnityEngine.Bindings.NativeMethodAttribute::<HasExplicitThis>k__BackingField
	bool ___U3CHasExplicitThisU3Ek__BackingField_4;
};

// UnityEngine.Bindings.NativeNameAttribute
struct NativeNameAttribute_t222751782B5418807DFE2A88CA0B24CA691B8621  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeNameAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeThrowsAttribute
struct NativeThrowsAttribute_t211CE8D047A8D45676C9ED399D5AA3B4A2C3E625  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.NativeThrowsAttribute::<ThrowsException>k__BackingField
	bool ___U3CThrowsExceptionU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NativeTypeAttribute
struct NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NativeTypeAttribute::<Header>k__BackingField
	String_t* ___U3CHeaderU3Ek__BackingField_0;
	// System.String UnityEngine.Bindings.NativeTypeAttribute::<IntermediateScriptingStructName>k__BackingField
	String_t* ___U3CIntermediateScriptingStructNameU3Ek__BackingField_1;
	// UnityEngine.Bindings.CodegenOptions UnityEngine.Bindings.NativeTypeAttribute::<CodegenOptions>k__BackingField
	int32_t ___U3CCodegenOptionsU3Ek__BackingField_2;
};

// UnityEngine.Bindings.NativeWritableSelfAttribute
struct NativeWritableSelfAttribute_t2ABC353836DDC2F15B1FBED9C0CF2E5ED0D1686C  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean UnityEngine.Bindings.NativeWritableSelfAttribute::<WritableSelf>k__BackingField
	bool ___U3CWritableSelfU3Ek__BackingField_0;
};

// UnityEngine.Bindings.NotNullAttribute
struct NotNullAttribute_t2E29B7802E8ED55CEA04EC4A6C254C6B60272DF7  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.NotNullAttribute::<Exception>k__BackingField
	String_t* ___U3CExceptionU3Ek__BackingField_0;
};

// System.Runtime.CompilerServices.NullableAttribute
struct NullableAttribute_tCBFFAB20AD9E68064976C7DC9D9AD53E3130BA14  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Byte[] System.Runtime.CompilerServices.NullableAttribute::NullableFlags
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___NullableFlags_0;
};

// System.Runtime.CompilerServices.NullableContextAttribute
struct NullableContextAttribute_t0F2E3CF46D8AB038E589C88FA392D5D8B69150FE  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Byte System.Runtime.CompilerServices.NullableContextAttribute::Flag
	uint8_t ___Flag_0;
};

// System.Runtime.CompilerServices.NullablePublicOnlyAttribute
struct NullablePublicOnlyAttribute_t76ED56E08ED096A598020BA0FF5FD30635089077  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.Boolean System.Runtime.CompilerServices.NullablePublicOnlyAttribute::IncludesInternals
	bool ___IncludesInternals_0;
};

// TriLibCore.Obj.ObjGeometry
struct ObjGeometry_t251EC78EED9A9E6E40B71FDD1D7C79DDDAE04BEB  : public Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147
{
	// System.String TriLibCore.Obj.ObjGeometry::MaterialName
	String_t* ___MaterialName_8;
};

// TriLibCore.Obj.Reader.ObjReader
struct ObjReader_t526AA9305FE5DDF1EF48E7B3D4DBC0FFF1056BEB  : public ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449
{
};

// UnityEngine.PhysicsScene
struct PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE 
{
	// System.Int32 UnityEngine.PhysicsScene::m_Handle
	int32_t ___m_Handle_0;
};

// UnityEngine.PhysicsScene2D
struct PhysicsScene2D_t550D023B9E77BE6844564BB4F9FA291EEA10FDC9 
{
	// System.Int32 UnityEngine.PhysicsScene2D::m_Handle
	int32_t ___m_Handle_0;
};

// TriLibCore.Ply.PlyListProperty
struct PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E  : public PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362
{
	// TriLibCore.Ply.PlyPropertyType TriLibCore.Ply.PlyListProperty::CounterType
	int32_t ___CounterType_2;
	// TriLibCore.Ply.PlyPropertyType TriLibCore.Ply.PlyListProperty::ItemType
	int32_t ___ItemType_3;
};

// TriLibCore.Ply.PlyValue
struct PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 
{
	// System.UInt32 TriLibCore.Ply.PlyValue::_intValue
	uint32_t ____intValue_0;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

// UnityEngine.RectInt
struct RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 
{
	// System.Int32 UnityEngine.RectInt::m_XMin
	int32_t ___m_XMin_0;
	// System.Int32 UnityEngine.RectInt::m_YMin
	int32_t ___m_YMin_1;
	// System.Int32 UnityEngine.RectInt::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.RectInt::m_Height
	int32_t ___m_Height_3;
};

// UnityEngine.RenderTextureDescriptor
struct RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 
{
	// System.Int32 UnityEngine.RenderTextureDescriptor::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_0;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_1;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<msaaSamples>k__BackingField
	int32_t ___U3CmsaaSamplesU3Ek__BackingField_2;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<volumeDepth>k__BackingField
	int32_t ___U3CvolumeDepthU3Ek__BackingField_3;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<mipCount>k__BackingField
	int32_t ___U3CmipCountU3Ek__BackingField_4;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::_graphicsFormat
	int32_t ____graphicsFormat_5;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::<stencilFormat>k__BackingField
	int32_t ___U3CstencilFormatU3Ek__BackingField_6;
	// UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::<depthStencilFormat>k__BackingField
	int32_t ___U3CdepthStencilFormatU3Ek__BackingField_7;
	// UnityEngine.Rendering.TextureDimension UnityEngine.RenderTextureDescriptor::<dimension>k__BackingField
	int32_t ___U3CdimensionU3Ek__BackingField_8;
	// UnityEngine.Rendering.ShadowSamplingMode UnityEngine.RenderTextureDescriptor::<shadowSamplingMode>k__BackingField
	int32_t ___U3CshadowSamplingModeU3Ek__BackingField_9;
	// UnityEngine.VRTextureUsage UnityEngine.RenderTextureDescriptor::<vrUsage>k__BackingField
	int32_t ___U3CvrUsageU3Ek__BackingField_10;
	// UnityEngine.RenderTextureCreationFlags UnityEngine.RenderTextureDescriptor::_flags
	int32_t ____flags_11;
	// UnityEngine.RenderTextureMemoryless UnityEngine.RenderTextureDescriptor::<memoryless>k__BackingField
	int32_t ___U3CmemorylessU3Ek__BackingField_12;
};

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<GenerateProxy>k__BackingField
	bool ___U3CGenerateProxyU3Ek__BackingField_2;
};

// UnityEngine.Bindings.StaticAccessorAttribute
struct StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Bindings.StaticAccessorAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// UnityEngine.Bindings.StaticAccessorType UnityEngine.Bindings.StaticAccessorAttribute::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;
};

// TriLibCore.Stl.Reader.StlReader
struct StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A  : public ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449
{
};

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
};

// TriLibCore.ThreeMf.ThreeMfGeometry
struct ThreeMfGeometry_t0B0D7084532A5FB23D4AE3C1263F1ACCA896B9A5  : public Geometry_t9DA9B9652E3E92AD194E9898A2EB36E95FE2E147
{
	// IxMilia.ThreeMf.ThreeMfTexture2D TriLibCore.ThreeMf.ThreeMfGeometry::ThreeMfTexture2D
	ThreeMfTexture2D_t992803A6FCF8343D7C1F9D282390E0BC126DE3CF* ___ThreeMfTexture2D_8;
	// IxMilia.ThreeMf.ThreeMfBase TriLibCore.ThreeMf.ThreeMfGeometry::ThreeMfBase
	ThreeMfBase_t019B9AAB3AF11FEDA0937D9901F9E9F7099E2CA2* ___ThreeMfBase_9;
};

// Firebase.Messaging.TokenReceivedEventArgs
struct TokenReceivedEventArgs_t91FA07EA184701BF44700D7581779A5C84C7B98C  : public EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377
{
	// System.String Firebase.Messaging.TokenReceivedEventArgs::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_1;
};

// UnityEngine.UILineInfo
struct UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC 
{
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;
};

// System.UIntPtr
struct UIntPtr_t 
{
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;
};

// Firebase.Platform.Default.UnityConfigExtensions
struct UnityConfigExtensions_tA9B2A38D4A5718276B9A2E776C138969F0575152  : public AppConfigExtensions_t6B8627CD4EFF8F05D2F749CC406E5E12F04CEE48
{
};

// Firebase.Unity.UnitySynchronizationContext
struct UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690  : public SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0
{
	// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.Unity.UnitySynchronizationContext::queue
	Queue_1_tBB890027A96D0D3343916455489502BC8FBEB73F* ___queue_7;
	// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir Firebase.Unity.UnitySynchronizationContext::behavior
	SynchronizationContextBehavoir_t77EF1109836806C11E4F2C8ECF2867D9531FEBBA* ___behavior_8;
	// System.Int32 Firebase.Unity.UnitySynchronizationContext::mainThreadId
	int32_t ___mainThreadId_9;
};

// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3FE9A7CDCC6A3A4122D8BF44F1D0A37BB38894C1  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.Scripting.UsedByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

// UnityEngine.WWW
struct WWW_tEADA9A43B98FC277E498F8E3206A3B8C4E5AF3FB  : public CustomYieldInstruction_t6B81A50D5D210C1ACAAE247FB53B65CDFFEB7617
{
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ____uwr_0;
};

// UnityEngine.WebCamDevice
struct WebCamDevice_t3D72A89B7F52FD27192023A67254D19E89D9738C 
{
	// System.String UnityEngine.WebCamDevice::m_Name
	String_t* ___m_Name_0;
	// System.String UnityEngine.WebCamDevice::m_DepthCameraName
	String_t* ___m_DepthCameraName_1;
	// System.Int32 UnityEngine.WebCamDevice::m_Flags
	int32_t ___m_Flags_2;
	// UnityEngine.WebCamKind UnityEngine.WebCamDevice::m_Kind
	int32_t ___m_Kind_3;
	// UnityEngine.Resolution[] UnityEngine.WebCamDevice::m_Resolutions
	ResolutionU5BU5D_t943BB1FE2B73E9E630A701D55F4C6EED8B54175A* ___m_Resolutions_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.WebCamDevice
struct WebCamDevice_t3D72A89B7F52FD27192023A67254D19E89D9738C_marshaled_pinvoke
{
	char* ___m_Name_0;
	char* ___m_DepthCameraName_1;
	int32_t ___m_Flags_2;
	int32_t ___m_Kind_3;
	Resolution_tDF215F567EEFFD07B9A8FB7CEACC08EA6B8B9525* ___m_Resolutions_4;
};
// Native definition for COM marshalling of UnityEngine.WebCamDevice
struct WebCamDevice_t3D72A89B7F52FD27192023A67254D19E89D9738C_marshaled_com
{
	Il2CppChar* ___m_Name_0;
	Il2CppChar* ___m_DepthCameraName_1;
	int32_t ___m_Flags_2;
	int32_t ___m_Kind_3;
	Resolution_tDF215F567EEFFD07B9A8FB7CEACC08EA6B8B9525* ___m_Resolutions_4;
};

// UnityEngine.XR.Management.XRConfigurationDataAttribute
struct XRConfigurationDataAttribute_tFCF093318B5224BCC0E168E99C562AB50DDFC0B2  : public Attribute_tFDA8EFEFB0711976D22474794576DAF28F7440AA
{
	// System.String UnityEngine.XR.Management.XRConfigurationDataAttribute::<displayName>k__BackingField
	String_t* ___U3CdisplayNameU3Ek__BackingField_0;
	// System.String UnityEngine.XR.Management.XRConfigurationDataAttribute::<buildSettingsKey>k__BackingField
	String_t* ___U3CbuildSettingsKeyU3Ek__BackingField_1;
};

// UnityEngine.Yoga.YogaSize
struct YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA 
{
	// System.Single UnityEngine.Yoga.YogaSize::width
	float ___width_0;
	// System.Single UnityEngine.Yoga.YogaSize::height
	float ___height_1;
};

// UnityEngine.Yoga.YogaValue
struct YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C 
{
	// System.Single UnityEngine.Yoga.YogaValue::value
	float ___value_0;
	// UnityEngine.Yoga.YogaUnit UnityEngine.Yoga.YogaValue::unit
	int32_t ___unit_1;
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128
struct __StaticArrayInitTypeSizeU3D128_tE7EEBFDBD77CAEE63DB6AFD4F5162D364EF66602 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D128_tE7EEBFDBD77CAEE63DB6AFD4F5162D364EF66602__padding[128];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256
struct __StaticArrayInitTypeSizeU3D256_t801718944F76C36638EA43F29012837CBA710948 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_t801718944F76C36638EA43F29012837CBA710948__padding[256];
	};
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=8192
struct __StaticArrayInitTypeSizeU3D8192_t6B42A11F775F676F25B0A63261E746DABAF39CB5 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D8192_t6B42A11F775F676F25B0A63261E746DABAF39CB5__padding[8192];
	};
};

// <PrivateImplementationDetails>{f1ff318e-91e6-436a-9a52-ac02be4f7d0e}/$ArrayType=20
#pragma pack(push, tp, 1)
struct U24ArrayTypeU3D20_tE46FE4B62B2712F036B53CB84CBD53BFB6EEC1A8 
{
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_tE46FE4B62B2712F036B53CB84CBD53BFB6EEC1A8__padding[20];
	};
};
#pragma pack(pop, tp)

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser
struct UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061  : public Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83
{
	// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_14;
	// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::<Query>k__BackingField
	String_t* ___U3CQueryU3Ek__BackingField_15;
};

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute
struct DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7 
{
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute::attributeType
	int32_t ___attributeType_0;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute::dataType
	int32_t ___dataType_1;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute::numComponents
	int32_t ___numComponents_2;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute::uniqueId
	int32_t ___uniqueId_3;
};

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh
struct DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED 
{
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh::numFaces
	int32_t ___numFaces_0;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh::numVertices
	int32_t ___numVertices_1;
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh::numAttributes
	int32_t ___numAttributes_2;
};

// Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs
struct ApplicationFocusChangedEventArgs_t690673A946CDA5EEC650A2D8E6A2848783AC9BAD  : public EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377
{
	// System.Boolean Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs::<HasFocus>k__BackingField
	bool ___U3CHasFocusU3Ek__BackingField_1;
};

// NativeGallery/ImageProperties
struct ImageProperties_t7C971D51A4992A01CD205754169B919B56696F59 
{
	// System.Int32 NativeGallery/ImageProperties::width
	int32_t ___width_0;
	// System.Int32 NativeGallery/ImageProperties::height
	int32_t ___height_1;
	// System.String NativeGallery/ImageProperties::mimeType
	String_t* ___mimeType_2;
	// NativeGallery/ImageOrientation NativeGallery/ImageProperties::orientation
	int32_t ___orientation_3;
};
// Native definition for P/Invoke marshalling of NativeGallery/ImageProperties
struct ImageProperties_t7C971D51A4992A01CD205754169B919B56696F59_marshaled_pinvoke
{
	int32_t ___width_0;
	int32_t ___height_1;
	char* ___mimeType_2;
	int32_t ___orientation_3;
};
// Native definition for COM marshalling of NativeGallery/ImageProperties
struct ImageProperties_t7C971D51A4992A01CD205754169B919B56696F59_marshaled_com
{
	int32_t ___width_0;
	int32_t ___height_1;
	Il2CppChar* ___mimeType_2;
	int32_t ___orientation_3;
};

// NativeGallery/VideoProperties
struct VideoProperties_t11DD7C4EACACE50EA3D710F3E2595A68B17103A5 
{
	// System.Int32 NativeGallery/VideoProperties::width
	int32_t ___width_0;
	// System.Int32 NativeGallery/VideoProperties::height
	int32_t ___height_1;
	// System.Int64 NativeGallery/VideoProperties::duration
	int64_t ___duration_2;
	// System.Single NativeGallery/VideoProperties::rotation
	float ___rotation_3;
};

// UnityEngine.ParticleSystem/MainModule
struct MainModule_tC7ECD8330C14B0808478A748048988A6085CE2A9 
{
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_tC7ECD8330C14B0808478A748048988A6085CE2A9_marshaled_pinvoke
{
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_tC7ECD8330C14B0808478A748048988A6085CE2A9_marshaled_com
{
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23 
{
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem/MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMin
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMax
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;
};

// UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314_marshaled_pinvoke
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};
// Native definition for COM marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314_marshaled_com
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_0;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___camera_1;
};

// UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData
struct PoseData_t67ECD1DA6257206C1935CCD030E1E1EF57E1443A 
{
	// System.Collections.Generic.List`1<System.String> UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData::PoseNames
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___PoseNames_0;
	// System.Collections.Generic.List`1<UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose> UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData::Poses
	List_1_t8A06DB5554CD76CE072807DE6EF4198109D84E53* ___Poses_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData
struct PoseData_t67ECD1DA6257206C1935CCD030E1E1EF57E1443A_marshaled_pinvoke
{
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___PoseNames_0;
	List_1_t8A06DB5554CD76CE072807DE6EF4198109D84E53* ___Poses_1;
};
// Native definition for COM marshalling of UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData
struct PoseData_t67ECD1DA6257206C1935CCD030E1E1EF57E1443A_marshaled_com
{
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___PoseNames_0;
	List_1_t8A06DB5554CD76CE072807DE6EF4198109D84E53* ___Poses_1;
};

// UnityEngine.XR.Management.XRManagementAnalytics/BuildEvent
struct BuildEvent_tE21D64E5354C87B5E40D2D2648C677B96CCA811E 
{
	// System.String UnityEngine.XR.Management.XRManagementAnalytics/BuildEvent::buildGuid
	String_t* ___buildGuid_0;
	// System.String UnityEngine.XR.Management.XRManagementAnalytics/BuildEvent::buildTarget
	String_t* ___buildTarget_1;
	// System.String UnityEngine.XR.Management.XRManagementAnalytics/BuildEvent::buildTargetGroup
	String_t* ___buildTargetGroup_2;
	// System.String[] UnityEngine.XR.Management.XRManagementAnalytics/BuildEvent::assigned_loaders
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___assigned_loaders_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.Management.XRManagementAnalytics/BuildEvent
struct BuildEvent_tE21D64E5354C87B5E40D2D2648C677B96CCA811E_marshaled_pinvoke
{
	char* ___buildGuid_0;
	char* ___buildTarget_1;
	char* ___buildTargetGroup_2;
	char** ___assigned_loaders_3;
};
// Native definition for COM marshalling of UnityEngine.XR.Management.XRManagementAnalytics/BuildEvent
struct BuildEvent_tE21D64E5354C87B5E40D2D2648C677B96CCA811E_marshaled_com
{
	Il2CppChar* ___buildGuid_0;
	Il2CppChar* ___buildTarget_1;
	Il2CppChar* ___buildTargetGroup_2;
	Il2CppChar** ___assigned_loaders_3;
};

// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
struct U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37 
{
	union
	{
		struct
		{
			// System.Boolean System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer::FixedElementField
			bool ___FixedElementField_0;
		};
		uint8_t U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37__padding[128];
	};
};
// Native definition for P/Invoke marshalling of System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
struct U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37_marshaled_pinvoke
{
	union
	{
		struct
		{
			int32_t ___FixedElementField_0;
		};
		uint8_t U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37__padding[128];
	};
};
// Native definition for COM marshalling of System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer
struct U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37_marshaled_com
{
	union
	{
		struct
		{
			int32_t ___FixedElementField_0;
		};
		uint8_t U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37__padding[128];
	};
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t6EB70CB763ED0F38063A7F9D39E7E5A0EDFBBB26  : public RuntimeObject
{
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t03A72430945148CB9D382B7B249DAE2DDF6F2843  : public RuntimeObject
{
};

// <PrivateImplementationDetails>{f1ff318e-91e6-436a-9a52-ac02be4f7d0e}
struct U3CPrivateImplementationDetailsU3EU7Bf1ff318eU2D91e6U2D436aU2D9a52U2Dac02be4f7d0eU7D_t54B8FF32EAD70318A421BB5FB5ECE4E1E50CE79C  : public RuntimeObject
{
};

// UnityEngine.AndroidJavaProxy
struct AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D  : public RuntimeObject
{
	// UnityEngine.AndroidJavaClass UnityEngine.AndroidJavaProxy::javaInterface
	AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03* ___javaInterface_0;
	// System.IntPtr UnityEngine.AndroidJavaProxy::proxyObject
	intptr_t ___proxyObject_1;
};

// UnityEngine.Animations.AnimationHumanStream
struct AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC 
{
	// System.IntPtr UnityEngine.Animations.AnimationHumanStream::stream
	intptr_t ___stream_0;
};

// UnityEngine.Animations.AnimationStream
struct AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A 
{
	// System.UInt32 UnityEngine.Animations.AnimationStream::m_AnimatorBindingsVersion
	uint32_t ___m_AnimatorBindingsVersion_0;
	// System.IntPtr UnityEngine.Animations.AnimationStream::constant
	intptr_t ___constant_1;
	// System.IntPtr UnityEngine.Animations.AnimationStream::input
	intptr_t ___input_2;
	// System.IntPtr UnityEngine.Animations.AnimationStream::output
	intptr_t ___output_3;
	// System.IntPtr UnityEngine.Animations.AnimationStream::workspace
	intptr_t ___workspace_4;
	// System.IntPtr UnityEngine.Animations.AnimationStream::inputStreamAccessor
	intptr_t ___inputStreamAccessor_5;
	// System.IntPtr UnityEngine.Animations.AnimationStream::animationHandleBinder
	intptr_t ___animationHandleBinder_6;
};

// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB* ___m_completeCallback_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0  : public RuntimeObject
{
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	// UnityEngine.Component UnityEngine.Collision::m_Body
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	// System.Int32 UnityEngine.Collision::m_ContactCount
	int32_t ___m_ContactCount_4;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_ReusedContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_ReusedContacts_5;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_LegacyContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_LegacyContacts_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_pinvoke
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_com
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};

// UnityEngine.Collision2D
struct Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B  : public RuntimeObject
{
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_4;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_5;
	// System.Int32 UnityEngine.Collision2D::m_ContactCount
	int32_t ___m_ContactCount_6;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_ReusedContacts
	ContactPoint2DU5BU5D_t427621BF8902AE33C86E7BF384D9B2B5B781F949* ___m_ReusedContacts_7;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_LegacyContacts
	ContactPoint2DU5BU5D_t427621BF8902AE33C86E7BF384D9B2B5B781F949* ___m_LegacyContacts_8;
};
// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_ReusedContacts_7;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_LegacyContacts_8;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_ReusedContacts_7;
	ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801* ___m_LegacyContacts_8;
};

// UnityEngine.XR.CommonUsages
struct CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1  : public RuntimeObject
{
};

// UnityEngine.ContactFilter2D
struct ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14 
{
	// System.Boolean UnityEngine.ContactFilter2D::useTriggers
	bool ___useTriggers_0;
	// System.Boolean UnityEngine.ContactFilter2D::useLayerMask
	bool ___useLayerMask_1;
	// System.Boolean UnityEngine.ContactFilter2D::useDepth
	bool ___useDepth_2;
	// System.Boolean UnityEngine.ContactFilter2D::useOutsideDepth
	bool ___useOutsideDepth_3;
	// System.Boolean UnityEngine.ContactFilter2D::useNormalAngle
	bool ___useNormalAngle_4;
	// System.Boolean UnityEngine.ContactFilter2D::useOutsideNormalAngle
	bool ___useOutsideNormalAngle_5;
	// UnityEngine.LayerMask UnityEngine.ContactFilter2D::layerMask
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___layerMask_6;
	// System.Single UnityEngine.ContactFilter2D::minDepth
	float ___minDepth_7;
	// System.Single UnityEngine.ContactFilter2D::maxDepth
	float ___maxDepth_8;
	// System.Single UnityEngine.ContactFilter2D::minNormalAngle
	float ___minNormalAngle_9;
	// System.Single UnityEngine.ContactFilter2D::maxNormalAngle
	float ___maxNormalAngle_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.ContactFilter2D
struct ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14_marshaled_pinvoke
{
	int32_t ___useTriggers_0;
	int32_t ___useLayerMask_1;
	int32_t ___useDepth_2;
	int32_t ___useOutsideDepth_3;
	int32_t ___useNormalAngle_4;
	int32_t ___useOutsideNormalAngle_5;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___layerMask_6;
	float ___minDepth_7;
	float ___maxDepth_8;
	float ___minNormalAngle_9;
	float ___maxNormalAngle_10;
};
// Native definition for COM marshalling of UnityEngine.ContactFilter2D
struct ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14_marshaled_com
{
	int32_t ___useTriggers_0;
	int32_t ___useLayerMask_1;
	int32_t ___useDepth_2;
	int32_t ___useOutsideDepth_3;
	int32_t ___useNormalAngle_4;
	int32_t ___useOutsideNormalAngle_5;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___layerMask_6;
	float ___minDepth_7;
	float ___maxDepth_8;
	float ___minNormalAngle_9;
	float ___maxNormalAngle_10;
};

// UnityEngine.ContactPoint
struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 
{
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.Int32 UnityEngine.ContactPoint::m_ThisColliderInstanceID
	int32_t ___m_ThisColliderInstanceID_2;
	// System.Int32 UnityEngine.ContactPoint::m_OtherColliderInstanceID
	int32_t ___m_OtherColliderInstanceID_3;
	// System.Single UnityEngine.ContactPoint::m_Separation
	float ___m_Separation_4;
};

// UnityEngine.ContactPoint2D
struct ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801 
{
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Point
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point_0;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Normal
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal_1;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_RelativeVelocity
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RelativeVelocity_2;
	// System.Single UnityEngine.ContactPoint2D::m_Separation
	float ___m_Separation_3;
	// System.Single UnityEngine.ContactPoint2D::m_NormalImpulse
	float ___m_NormalImpulse_4;
	// System.Single UnityEngine.ContactPoint2D::m_TangentImpulse
	float ___m_TangentImpulse_5;
	// System.Int32 UnityEngine.ContactPoint2D::m_Collider
	int32_t ___m_Collider_6;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherCollider
	int32_t ___m_OtherCollider_7;
	// System.Int32 UnityEngine.ContactPoint2D::m_Rigidbody
	int32_t ___m_Rigidbody_8;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_9;
	// System.Int32 UnityEngine.ContactPoint2D::m_Enabled
	int32_t ___m_Enabled_10;
};

// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92  : public RuntimeObject
{
	// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::m_Controller
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	// UnityEngine.Collider UnityEngine.ControllerColliderHit::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_MoveDirection
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	// System.Single UnityEngine.ControllerColliderHit::m_MoveLength
	float ___m_MoveLength_5;
	// System.Int32 UnityEngine.ControllerColliderHit::m_Push
	int32_t ___m_Push_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92_marshaled_pinvoke
{
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
// Native definition for COM marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92_marshaled_com
{
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ___m_Controller_0;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_2;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};

// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange
struct GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97 
{
	// System.UInt32 UnityEngine.UIElements.UIR.GfxUpdateBufferRange::offsetFromWriteStart
	uint32_t ___offsetFromWriteStart_0;
	// System.UInt32 UnityEngine.UIElements.UIR.GfxUpdateBufferRange::size
	uint32_t ___size_1;
	// System.UIntPtr UnityEngine.UIElements.UIR.GfxUpdateBufferRange::source
	uintptr_t ___source_2;
};

// UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F  : public RuntimeObject
{
	// System.UInt32 UnityEngine.TextCore.Glyph::m_Index
	uint32_t ___m_Index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.Glyph::m_Metrics
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.Glyph::m_GlyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	// System.Single UnityEngine.TextCore.Glyph::m_Scale
	float ___m_Scale_3;
	// System.Int32 UnityEngine.TextCore.Glyph::m_AtlasIndex
	int32_t ___m_AtlasIndex_4;
	// UnityEngine.TextCore.GlyphClassDefinitionType UnityEngine.TextCore.Glyph::m_ClassDefinitionType
	int32_t ___m_ClassDefinitionType_5;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_pinvoke
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
	int32_t ___m_ClassDefinitionType_5;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F_marshaled_com
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___m_Metrics_1;
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
	int32_t ___m_ClassDefinitionType_5;
};

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord
struct GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 
{
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphIndex
	uint32_t ___m_GlyphIndex_0;
	// UnityEngine.TextCore.LowLevel.GlyphValueRecord UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord::m_GlyphValueRecord
	GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E ___m_GlyphValueRecord_1;
};

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct
struct GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C 
{
	// System.UInt32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::index
	uint32_t ___index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::metrics
	GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A ___metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::glyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___glyphRect_2;
	// System.Single UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::scale
	float ___scale_3;
	// System.Int32 UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::atlasIndex
	int32_t ___atlasIndex_4;
	// UnityEngine.TextCore.GlyphClassDefinitionType UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct::classDefinitionType
	int32_t ___classDefinitionType_5;
};

// System.Runtime.InteropServices.HandleRef
struct HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F 
{
	// System.Object System.Runtime.InteropServices.HandleRef::_wrapper
	RuntimeObject* ____wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::_handle
	intptr_t ____handle_1;
};

// UnityEngine.HumanLimit
struct HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E 
{
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Min
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Min_0;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Max
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Max_1;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Center
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Center_2;
	// System.Single UnityEngine.HumanLimit::m_AxisLength
	float ___m_AxisLength_3;
	// System.Int32 UnityEngine.HumanLimit::m_UseDefaultValues
	int32_t ___m_UseDefaultValues_4;
};

// UnityEngine.IntegratedSubsystem
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3  : public RuntimeObject
{
	// System.IntPtr UnityEngine.IntegratedSubsystem::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.ISubsystemDescriptor UnityEngine.IntegratedSubsystem::m_SubsystemDescriptor
	RuntimeObject* ___m_SubsystemDescriptor_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystem
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_SubsystemDescriptor_1;
};
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystem
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3_marshaled_com
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_SubsystemDescriptor_1;
};

// UnityEngine.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D  : public RuntimeObject
{
	// System.IntPtr UnityEngine.IntegratedSubsystemDescriptor::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.XR.MeshGenerationResult
struct MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C 
{
	// UnityEngine.XR.MeshId UnityEngine.XR.MeshGenerationResult::<MeshId>k__BackingField
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___U3CMeshIdU3Ek__BackingField_0;
	// UnityEngine.Mesh UnityEngine.XR.MeshGenerationResult::<Mesh>k__BackingField
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___U3CMeshU3Ek__BackingField_1;
	// UnityEngine.MeshCollider UnityEngine.XR.MeshGenerationResult::<MeshCollider>k__BackingField
	MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455* ___U3CMeshColliderU3Ek__BackingField_2;
	// UnityEngine.XR.MeshGenerationStatus UnityEngine.XR.MeshGenerationResult::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_3;
	// UnityEngine.XR.MeshVertexAttributes UnityEngine.XR.MeshGenerationResult::<Attributes>k__BackingField
	int32_t ___U3CAttributesU3Ek__BackingField_4;
	// System.UInt64 UnityEngine.XR.MeshGenerationResult::<Timestamp>k__BackingField
	uint64_t ___U3CTimestampU3Ek__BackingField_5;
	// UnityEngine.Vector3 UnityEngine.XR.MeshGenerationResult::<Position>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CPositionU3Ek__BackingField_6;
	// UnityEngine.Quaternion UnityEngine.XR.MeshGenerationResult::<Rotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CRotationU3Ek__BackingField_7;
	// UnityEngine.Vector3 UnityEngine.XR.MeshGenerationResult::<Scale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CScaleU3Ek__BackingField_8;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.MeshGenerationResult
struct MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C_marshaled_pinvoke
{
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___U3CMeshIdU3Ek__BackingField_0;
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___U3CMeshU3Ek__BackingField_1;
	MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455* ___U3CMeshColliderU3Ek__BackingField_2;
	int32_t ___U3CStatusU3Ek__BackingField_3;
	int32_t ___U3CAttributesU3Ek__BackingField_4;
	uint64_t ___U3CTimestampU3Ek__BackingField_5;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CPositionU3Ek__BackingField_6;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CRotationU3Ek__BackingField_7;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CScaleU3Ek__BackingField_8;
};
// Native definition for COM marshalling of UnityEngine.XR.MeshGenerationResult
struct MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C_marshaled_com
{
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___U3CMeshIdU3Ek__BackingField_0;
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___U3CMeshU3Ek__BackingField_1;
	MeshCollider_tB525E4DDE383252364ED0BDD32CF2B53914EE455* ___U3CMeshColliderU3Ek__BackingField_2;
	int32_t ___U3CStatusU3Ek__BackingField_3;
	int32_t ___U3CAttributesU3Ek__BackingField_4;
	uint64_t ___U3CTimestampU3Ek__BackingField_5;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CPositionU3Ek__BackingField_6;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CRotationU3Ek__BackingField_7;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CScaleU3Ek__BackingField_8;
};

// UnityEngine.XR.MeshInfo
struct MeshInfo_t0BB7A8C2C586FD5EAD40E9E4E1749150915FDFFA 
{
	// UnityEngine.XR.MeshId UnityEngine.XR.MeshInfo::<MeshId>k__BackingField
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___U3CMeshIdU3Ek__BackingField_0;
	// UnityEngine.XR.MeshChangeState UnityEngine.XR.MeshInfo::<ChangeState>k__BackingField
	int32_t ___U3CChangeStateU3Ek__BackingField_1;
	// System.Int32 UnityEngine.XR.MeshInfo::<PriorityHint>k__BackingField
	int32_t ___U3CPriorityHintU3Ek__BackingField_2;
};

// UnityEngine.XR.MeshTransform
struct MeshTransform_t669BDD0278EB7AC2976F16FD341CC1C4747FAA6A 
{
	// UnityEngine.XR.MeshId UnityEngine.XR.MeshTransform::<MeshId>k__BackingField
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___U3CMeshIdU3Ek__BackingField_0;
	// System.UInt64 UnityEngine.XR.MeshTransform::<Timestamp>k__BackingField
	uint64_t ___U3CTimestampU3Ek__BackingField_1;
	// UnityEngine.Vector3 UnityEngine.XR.MeshTransform::<Position>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CPositionU3Ek__BackingField_2;
	// UnityEngine.Quaternion UnityEngine.XR.MeshTransform::<Rotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CRotationU3Ek__BackingField_3;
	// UnityEngine.Vector3 UnityEngine.XR.MeshTransform::<Scale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CScaleU3Ek__BackingField_4;
};

// UnityEngine.ModifiableContactPair
struct ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960 
{
	// System.IntPtr UnityEngine.ModifiableContactPair::actor
	intptr_t ___actor_0;
	// System.IntPtr UnityEngine.ModifiableContactPair::otherActor
	intptr_t ___otherActor_1;
	// System.IntPtr UnityEngine.ModifiableContactPair::shape
	intptr_t ___shape_2;
	// System.IntPtr UnityEngine.ModifiableContactPair::otherShape
	intptr_t ___otherShape_3;
	// UnityEngine.Quaternion UnityEngine.ModifiableContactPair::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_4;
	// UnityEngine.Vector3 UnityEngine.ModifiableContactPair::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_5;
	// UnityEngine.Quaternion UnityEngine.ModifiableContactPair::otherRotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___otherRotation_6;
	// UnityEngine.Vector3 UnityEngine.ModifiableContactPair::otherPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___otherPosition_7;
	// System.Int32 UnityEngine.ModifiableContactPair::numContacts
	int32_t ___numContacts_8;
	// System.IntPtr UnityEngine.ModifiableContactPair::contacts
	intptr_t ___contacts_9;
};

// UnityEngine.Bindings.NativePropertyAttribute
struct NativePropertyAttribute_tAF7FB03BF7FFE9E8AB0E75B0F842FC0AA22AE607  : public NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270
{
	// UnityEngine.Bindings.TargetType UnityEngine.Bindings.NativePropertyAttribute::<TargetType>k__BackingField
	int32_t ___U3CTargetTypeU3Ek__BackingField_5;
};

// Byn.Awrtc.NetworkEvent
struct NetworkEvent_t6D46042D56AE8CB787CC307645DFBF5C5F48A90E 
{
	// Byn.Awrtc.NetEventType Byn.Awrtc.NetworkEvent::type
	uint8_t ___type_0;
	// Byn.Awrtc.ConnectionId Byn.Awrtc.NetworkEvent::connectionId
	ConnectionId_tEC0FFB08249CB4253D31AF1E9166778F5D22E2F7 ___connectionId_1;
	// System.Object Byn.Awrtc.NetworkEvent::data
	RuntimeObject* ___data_2;
};
// Native definition for P/Invoke marshalling of Byn.Awrtc.NetworkEvent
struct NetworkEvent_t6D46042D56AE8CB787CC307645DFBF5C5F48A90E_marshaled_pinvoke
{
	uint8_t ___type_0;
	ConnectionId_tEC0FFB08249CB4253D31AF1E9166778F5D22E2F7 ___connectionId_1;
	Il2CppIUnknown* ___data_2;
};
// Native definition for COM marshalling of Byn.Awrtc.NetworkEvent
struct NetworkEvent_t6D46042D56AE8CB787CC307645DFBF5C5F48A90E_marshaled_com
{
	uint8_t ___type_0;
	ConnectionId_tEC0FFB08249CB4253D31AF1E9166778F5D22E2F7 ___connectionId_1;
	Il2CppIUnknown* ___data_2;
};

// TriLibCore.Obj.ObjModel
struct ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5  : public RuntimeObject
{
	// System.String TriLibCore.Obj.ObjModel::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Obj.ObjModel::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// UnityEngine.Vector3 TriLibCore.Obj.ObjModel::<LocalPosition>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalPositionU3Ek__BackingField_2;
	// UnityEngine.Quaternion TriLibCore.Obj.ObjModel::<LocalRotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CLocalRotationU3Ek__BackingField_3;
	// UnityEngine.Vector3 TriLibCore.Obj.ObjModel::<LocalScale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalScaleU3Ek__BackingField_4;
	// System.Boolean TriLibCore.Obj.ObjModel::<Visibility>k__BackingField
	bool ___U3CVisibilityU3Ek__BackingField_5;
	// TriLibCore.Interfaces.IModel TriLibCore.Obj.ObjModel::<Parent>k__BackingField
	RuntimeObject* ___U3CParentU3Ek__BackingField_6;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Obj.ObjModel::<Children>k__BackingField
	RuntimeObject* ___U3CChildrenU3Ek__BackingField_7;
	// System.Boolean TriLibCore.Obj.ObjModel::<IsBone>k__BackingField
	bool ___U3CIsBoneU3Ek__BackingField_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Obj.ObjModel::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_9;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Obj.ObjModel::<Bones>k__BackingField
	RuntimeObject* ___U3CBonesU3Ek__BackingField_10;
	// UnityEngine.Matrix4x4[] TriLibCore.Obj.ObjModel::<BindPoses>k__BackingField
	Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___U3CBindPosesU3Ek__BackingField_11;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Obj.ObjModel::<MaterialIndices>k__BackingField
	RuntimeObject* ___U3CMaterialIndicesU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Obj.ObjModel::<UserProperties>k__BackingField
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___U3CUserPropertiesU3Ek__BackingField_13;
};

// TriLibCore.Obj.ObjProcessor
struct ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115  : public RuntimeObject
{
	// System.Collections.Generic.List`1<System.String> TriLibCore.Obj.ObjProcessor::_processedMaterials
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ____processedMaterials_5;
	// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IMaterial> TriLibCore.Obj.ObjProcessor::_materialLibraries
	Dictionary_2_t363D8F92FA9BF57C132404DC1816AEA51C3758E1* ____materialLibraries_6;
	// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Obj.ObjProcessor::_geometryGroups
	Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* ____geometryGroups_7;
	// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel> TriLibCore.Obj.ObjProcessor::_models
	Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* ____models_8;
	// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.ITexture> TriLibCore.Obj.ObjProcessor::_textures
	Dictionary_2_tFB71D8D0D142E37DD9146E387285F9F34A3F2E88* ____textures_9;
	// System.String TriLibCore.Obj.ObjProcessor::_geometryAndMaterialName
	String_t* ____geometryAndMaterialName_10;
	// System.String TriLibCore.Obj.ObjProcessor::_geometryGroupName
	String_t* ____geometryGroupName_11;
	// System.String TriLibCore.Obj.ObjProcessor::_materialName
	String_t* ____materialName_12;
	// System.String TriLibCore.Obj.ObjProcessor::_objectName
	String_t* ____objectName_13;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Obj.ObjProcessor::_activeGeometryGroup
	RuntimeObject* ____activeGeometryGroup_14;
	// TriLibCore.Obj.ObjGeometry TriLibCore.Obj.ObjProcessor::_activeGeometry
	ObjGeometry_t251EC78EED9A9E6E40B71FDD1D7C79DDDAE04BEB* ____activeGeometry_15;
	// TriLibCore.Obj.ObjMaterial TriLibCore.Obj.ObjProcessor::_activeMaterial
	ObjMaterial_t333DED08F3CA57CC38255E5187814EB8821FEB9A* ____activeMaterial_16;
	// TriLibCore.Obj.ObjModel TriLibCore.Obj.ObjProcessor::_activeModel
	ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5* ____activeModel_17;
	// System.Int32 TriLibCore.Obj.ObjProcessor::_unnamedTextureCount
	int32_t ____unnamedTextureCount_18;
	// TriLibCore.Obj.Reader.ObjReader TriLibCore.Obj.ObjProcessor::_reader
	ObjReader_t526AA9305FE5DDF1EF48E7B3D4DBC0FFF1056BEB* ____reader_19;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Obj.ObjProcessor::_allVertices
	RuntimeObject* ____allVertices_21;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Obj.ObjProcessor::_allNormals
	RuntimeObject* ____allNormals_22;
	// System.Collections.Generic.IList`1<UnityEngine.Vector2> TriLibCore.Obj.ObjProcessor::_allUVs
	RuntimeObject* ____allUVs_23;
	// System.Collections.Generic.IList`1<UnityEngine.Color> TriLibCore.Obj.ObjProcessor::_allColors
	RuntimeObject* ____allColors_24;
	// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute> TriLibCore.Obj.ObjProcessor::_vertexAttributes
	HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* ____vertexAttributes_25;
	// System.Int32 TriLibCore.Obj.ObjProcessor::_floatAttributesCount
	int32_t ____floatAttributesCount_26;
};

// TriLibCore.Obj.ObjTexture
struct ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43  : public RuntimeObject
{
	// System.String TriLibCore.Obj.ObjTexture::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Obj.ObjTexture::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// System.Byte[] TriLibCore.Obj.ObjTexture::<Data>k__BackingField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___U3CDataU3Ek__BackingField_2;
	// System.IO.Stream TriLibCore.Obj.ObjTexture::<DataStream>k__BackingField
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___U3CDataStreamU3Ek__BackingField_3;
	// System.String TriLibCore.Obj.ObjTexture::<Filename>k__BackingField
	String_t* ___U3CFilenameU3Ek__BackingField_4;
	// UnityEngine.TextureWrapMode TriLibCore.Obj.ObjTexture::<WrapModeU>k__BackingField
	int32_t ___U3CWrapModeUU3Ek__BackingField_5;
	// UnityEngine.TextureWrapMode TriLibCore.Obj.ObjTexture::<WrapModeV>k__BackingField
	int32_t ___U3CWrapModeVU3Ek__BackingField_6;
	// UnityEngine.Vector2 TriLibCore.Obj.ObjTexture::<Tiling>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CTilingU3Ek__BackingField_7;
	// UnityEngine.Vector2 TriLibCore.Obj.ObjTexture::<Offset>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3COffsetU3Ek__BackingField_8;
	// System.Int32 TriLibCore.Obj.ObjTexture::<TextureId>k__BackingField
	int32_t ___U3CTextureIdU3Ek__BackingField_9;
	// System.String TriLibCore.Obj.ObjTexture::<ResolvedFilename>k__BackingField
	String_t* ___U3CResolvedFilenameU3Ek__BackingField_10;
	// System.Boolean TriLibCore.Obj.ObjTexture::<HasAlpha>k__BackingField
	bool ___U3CHasAlphaU3Ek__BackingField_11;
	// TriLibCore.General.TextureFormat TriLibCore.Obj.ObjTexture::<TextureFormat>k__BackingField
	int32_t ___U3CTextureFormatU3Ek__BackingField_12;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Playables.PlayableHandle
struct PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 
{
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;
};

// UnityEngine.Playables.PlayableOutputHandle
struct PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 
{
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	uint32_t ___m_Version_1;
};

// TriLibCore.Ply.PlyModel
struct PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2  : public RuntimeObject
{
	// System.String TriLibCore.Ply.PlyModel::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Ply.PlyModel::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// UnityEngine.Vector3 TriLibCore.Ply.PlyModel::<LocalPosition>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalPositionU3Ek__BackingField_2;
	// UnityEngine.Quaternion TriLibCore.Ply.PlyModel::<LocalRotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CLocalRotationU3Ek__BackingField_3;
	// UnityEngine.Vector3 TriLibCore.Ply.PlyModel::<LocalScale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalScaleU3Ek__BackingField_4;
	// System.Boolean TriLibCore.Ply.PlyModel::<Visibility>k__BackingField
	bool ___U3CVisibilityU3Ek__BackingField_5;
	// TriLibCore.Interfaces.IModel TriLibCore.Ply.PlyModel::<Parent>k__BackingField
	RuntimeObject* ___U3CParentU3Ek__BackingField_6;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Ply.PlyModel::<Children>k__BackingField
	RuntimeObject* ___U3CChildrenU3Ek__BackingField_7;
	// System.Boolean TriLibCore.Ply.PlyModel::<IsBone>k__BackingField
	bool ___U3CIsBoneU3Ek__BackingField_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Ply.PlyModel::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_9;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Ply.PlyModel::<Bones>k__BackingField
	RuntimeObject* ___U3CBonesU3Ek__BackingField_10;
	// UnityEngine.Matrix4x4[] TriLibCore.Ply.PlyModel::<BindPoses>k__BackingField
	Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___U3CBindPosesU3Ek__BackingField_11;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Ply.PlyModel::<MaterialIndices>k__BackingField
	RuntimeObject* ___U3CMaterialIndicesU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Ply.PlyModel::<UserProperties>k__BackingField
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___U3CUserPropertiesU3Ek__BackingField_13;
};

// TriLibCore.Ply.PlyTexture
struct PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C  : public RuntimeObject
{
	// System.String TriLibCore.Ply.PlyTexture::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Ply.PlyTexture::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// System.Byte[] TriLibCore.Ply.PlyTexture::<Data>k__BackingField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___U3CDataU3Ek__BackingField_2;
	// System.IO.Stream TriLibCore.Ply.PlyTexture::<DataStream>k__BackingField
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___U3CDataStreamU3Ek__BackingField_3;
	// System.String TriLibCore.Ply.PlyTexture::<Filename>k__BackingField
	String_t* ___U3CFilenameU3Ek__BackingField_4;
	// UnityEngine.TextureWrapMode TriLibCore.Ply.PlyTexture::<WrapModeU>k__BackingField
	int32_t ___U3CWrapModeUU3Ek__BackingField_5;
	// UnityEngine.TextureWrapMode TriLibCore.Ply.PlyTexture::<WrapModeV>k__BackingField
	int32_t ___U3CWrapModeVU3Ek__BackingField_6;
	// UnityEngine.Vector2 TriLibCore.Ply.PlyTexture::<Tiling>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CTilingU3Ek__BackingField_7;
	// UnityEngine.Vector2 TriLibCore.Ply.PlyTexture::<Offset>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3COffsetU3Ek__BackingField_8;
	// System.Int32 TriLibCore.Ply.PlyTexture::<TextureId>k__BackingField
	int32_t ___U3CTextureIdU3Ek__BackingField_9;
	// System.String TriLibCore.Ply.PlyTexture::<ResolvedFilename>k__BackingField
	String_t* ___U3CResolvedFilenameU3Ek__BackingField_10;
	// System.Boolean TriLibCore.Ply.PlyTexture::<HasAlpha>k__BackingField
	bool ___U3CHasAlphaU3Ek__BackingField_11;
	// TriLibCore.General.TextureFormat TriLibCore.Ply.PlyTexture::<TextureFormat>k__BackingField
	int32_t ___U3CTextureFormatU3Ek__BackingField_12;
};

// UnityEngine.Pose
struct Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 
{
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_1;
};

// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD 
{
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;
};

// UnityEngine.RaycastHit
struct RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 
{
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA 
{
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52  : public RuntimeObject
{
	// System.IntPtr UnityEngine.RemoteConfigSettings::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<System.Boolean> UnityEngine.RemoteConfigSettings::Updated
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___Updated_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
// Native definition for COM marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};

// UnityEngine.Rendering.RenderTargetIdentifier
struct RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B 
{
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;
	// System.IntPtr UnityEngine.Rendering.RenderTargetIdentifier::m_BufferPointer
	intptr_t ___m_BufferPointer_3;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_MipLevel
	int32_t ___m_MipLevel_4;
	// UnityEngine.CubemapFace UnityEngine.Rendering.RenderTargetIdentifier::m_CubeFace
	int32_t ___m_CubeFace_5;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_DepthSlice
	int32_t ___m_DepthSlice_6;
};

// UnityEngine.SendMouseEvents
struct SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39  : public RuntimeObject
{
};

// UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126 
{
	// System.String UnityEngine.SkeletonBone::name
	String_t* ___name_0;
	// System.String UnityEngine.SkeletonBone::parentName
	String_t* ___parentName_1;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	// UnityEngine.Quaternion UnityEngine.SkeletonBone::rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::scale
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_pinvoke
{
	char* ___name_0;
	char* ___parentName_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};
// Native definition for COM marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___parentName_1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_2;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation_3;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___scale_4;
};

// TriLibCore.Stl.StlModel
struct StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA  : public RuntimeObject
{
	// System.String TriLibCore.Stl.StlModel::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Stl.StlModel::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// UnityEngine.Vector3 TriLibCore.Stl.StlModel::<LocalPosition>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalPositionU3Ek__BackingField_2;
	// UnityEngine.Quaternion TriLibCore.Stl.StlModel::<LocalRotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CLocalRotationU3Ek__BackingField_3;
	// UnityEngine.Vector3 TriLibCore.Stl.StlModel::<LocalScale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalScaleU3Ek__BackingField_4;
	// System.Boolean TriLibCore.Stl.StlModel::<Visibility>k__BackingField
	bool ___U3CVisibilityU3Ek__BackingField_5;
	// TriLibCore.Interfaces.IModel TriLibCore.Stl.StlModel::<Parent>k__BackingField
	RuntimeObject* ___U3CParentU3Ek__BackingField_6;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlModel::<Children>k__BackingField
	RuntimeObject* ___U3CChildrenU3Ek__BackingField_7;
	// System.Boolean TriLibCore.Stl.StlModel::<IsBone>k__BackingField
	bool ___U3CIsBoneU3Ek__BackingField_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Stl.StlModel::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_9;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlModel::<Bones>k__BackingField
	RuntimeObject* ___U3CBonesU3Ek__BackingField_10;
	// UnityEngine.Matrix4x4[] TriLibCore.Stl.StlModel::<BindPoses>k__BackingField
	Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___U3CBindPosesU3Ek__BackingField_11;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Stl.StlModel::<MaterialIndices>k__BackingField
	RuntimeObject* ___U3CMaterialIndicesU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Stl.StlModel::<UserProperties>k__BackingField
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___U3CUserPropertiesU3Ek__BackingField_13;
};

// TriLibCore.Stl.StlProcessor
struct StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Stl.StlProcessor::_geometryGroups
	Dictionary_2_t11F334CD06F0CA7F4E6DAC18D5282192E1803983* ____geometryGroups_3;
	// System.Collections.Generic.Dictionary`2<System.String,TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlProcessor::_models
	Dictionary_2_tCD882897E065F2BF6F3F9878D136A63B57927F5B* ____models_4;
	// TriLibCore.Stl.Reader.StlReader TriLibCore.Stl.StlProcessor::_reader
	StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A* ____reader_5;
	// System.String TriLibCore.Stl.StlProcessor::_groupName
	String_t* ____groupName_6;
	// System.String TriLibCore.Stl.StlProcessor::_lastGeometryGroupName
	String_t* ____lastGeometryGroupName_7;
	// System.Int32 TriLibCore.Stl.StlProcessor::_lastLoopNumber
	int32_t ____lastLoopNumber_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Stl.StlProcessor::_activeGeometryGroup
	RuntimeObject* ____activeGeometryGroup_9;
	// TriLibCore.Stl.StlGeometry TriLibCore.Stl.StlProcessor::_activeGeometry
	StlGeometry_t5B0D0CE30519F8725CABBDF0D0DB02921C5FDABF* ____activeGeometry_10;
	// System.Int32 TriLibCore.Stl.StlProcessor::_loopNumber
	int32_t ____loopNumber_11;
	// UnityEngine.Vector3 TriLibCore.Stl.StlProcessor::_facetNormal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ____facetNormal_12;
	// UnityEngine.Color TriLibCore.Stl.StlProcessor::_partColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ____partColor_21;
	// UnityEngine.Color TriLibCore.Stl.StlProcessor::_facetColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ____facetColor_22;
	// System.Int32 TriLibCore.Stl.StlProcessor::_floatCount
	int32_t ____floatCount_23;
	// System.Collections.Generic.HashSet`1<UnityEngine.Rendering.VertexAttribute> TriLibCore.Stl.StlProcessor::_vertexAttributes
	HashSet_1_t17CC6C8527489146D9154E22DE059EF4A4550640* ____vertexAttributes_24;
};

// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B  : public TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7
{
	// System.IO.Stream System.IO.StreamReader::_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ____stream_3;
	// System.Text.Encoding System.IO.StreamReader::_encoding
	Encoding_t65CDEF28CF20A7B8C92E85A4E808920C2465F095* ____encoding_4;
	// System.Text.Decoder System.IO.StreamReader::_decoder
	Decoder_tE16E789E38B25DD304004FC630EA8B21000ECBBC* ____decoder_5;
	// System.Byte[] System.IO.StreamReader::_byteBuffer
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____byteBuffer_6;
	// System.Char[] System.IO.StreamReader::_charBuffer
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____charBuffer_7;
	// System.Int32 System.IO.StreamReader::_charPos
	int32_t ____charPos_8;
	// System.Int32 System.IO.StreamReader::_charLen
	int32_t ____charLen_9;
	// System.Int32 System.IO.StreamReader::_byteLen
	int32_t ____byteLen_10;
	// System.Int32 System.IO.StreamReader::_bytePos
	int32_t ____bytePos_11;
	// System.Int32 System.IO.StreamReader::_maxCharsPerBuffer
	int32_t ____maxCharsPerBuffer_12;
	// System.Boolean System.IO.StreamReader::_detectEncoding
	bool ____detectEncoding_13;
	// System.Boolean System.IO.StreamReader::_checkPreamble
	bool ____checkPreamble_14;
	// System.Boolean System.IO.StreamReader::_isBlocked
	bool ____isBlocked_15;
	// System.Boolean System.IO.StreamReader::_closable
	bool ____closable_16;
	// System.Threading.Tasks.Task System.IO.StreamReader::_asyncReadTask
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ____asyncReadTask_17;
};

// System.Text.Encodings.Web.TextEncoderSettings
struct TextEncoderSettings_t893E26A25D5CD77AC6CD12AAF45CC8C14AB1296B  : public RuntimeObject
{
	// System.Text.Internal.AllowedCharactersBitmap System.Text.Encodings.Web.TextEncoderSettings::_allowedCharactersBitmap
	AllowedCharactersBitmap_tA1BC142B179A112F4EAA6A6BF578B0BFD58E8BED ____allowedCharactersBitmap_0;
};

// UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3 
{
	// UnityEngine.Font UnityEngine.TextGenerationSettings::font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	// UnityEngine.Color UnityEngine.TextGenerationSettings::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	// System.Int32 UnityEngine.TextGenerationSettings::fontSize
	int32_t ___fontSize_2;
	// System.Single UnityEngine.TextGenerationSettings::lineSpacing
	float ___lineSpacing_3;
	// System.Boolean UnityEngine.TextGenerationSettings::richText
	bool ___richText_4;
	// System.Single UnityEngine.TextGenerationSettings::scaleFactor
	float ___scaleFactor_5;
	// UnityEngine.FontStyle UnityEngine.TextGenerationSettings::fontStyle
	int32_t ___fontStyle_6;
	// UnityEngine.TextAnchor UnityEngine.TextGenerationSettings::textAnchor
	int32_t ___textAnchor_7;
	// System.Boolean UnityEngine.TextGenerationSettings::alignByGeometry
	bool ___alignByGeometry_8;
	// System.Boolean UnityEngine.TextGenerationSettings::resizeTextForBestFit
	bool ___resizeTextForBestFit_9;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMinSize
	int32_t ___resizeTextMinSize_10;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMaxSize
	int32_t ___resizeTextMaxSize_11;
	// System.Boolean UnityEngine.TextGenerationSettings::updateBounds
	bool ___updateBounds_12;
	// UnityEngine.VerticalWrapMode UnityEngine.TextGenerationSettings::verticalOverflow
	int32_t ___verticalOverflow_13;
	// UnityEngine.HorizontalWrapMode UnityEngine.TextGenerationSettings::horizontalOverflow
	int32_t ___horizontalOverflow_14;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::generationExtents
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::pivot
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	// System.Boolean UnityEngine.TextGenerationSettings::generateOutOfBounds
	bool ___generateOutOfBounds_17;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_pinvoke
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};
// Native definition for COM marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_com
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_0;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___generationExtents_15;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};

// UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062 
{
	// System.String UnityEngine.UIElements.TextNativeSettings::text
	String_t* ___text_0;
	// UnityEngine.Font UnityEngine.UIElements.TextNativeSettings::font
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	// System.Int32 UnityEngine.UIElements.TextNativeSettings::size
	int32_t ___size_2;
	// System.Single UnityEngine.UIElements.TextNativeSettings::scaling
	float ___scaling_3;
	// UnityEngine.FontStyle UnityEngine.UIElements.TextNativeSettings::style
	int32_t ___style_4;
	// UnityEngine.Color UnityEngine.UIElements.TextNativeSettings::color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	// UnityEngine.TextAnchor UnityEngine.UIElements.TextNativeSettings::anchor
	int32_t ___anchor_6;
	// System.Boolean UnityEngine.UIElements.TextNativeSettings::wordWrap
	bool ___wordWrap_7;
	// System.Single UnityEngine.UIElements.TextNativeSettings::wordWrapWidth
	float ___wordWrapWidth_8;
	// System.Boolean UnityEngine.UIElements.TextNativeSettings::richText
	bool ___richText_9;
};
// Native definition for P/Invoke marshalling of UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062_marshaled_pinvoke
{
	char* ___text_0;
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	int32_t ___size_2;
	float ___scaling_3;
	int32_t ___style_4;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	int32_t ___anchor_6;
	int32_t ___wordWrap_7;
	float ___wordWrapWidth_8;
	int32_t ___richText_9;
};
// Native definition for COM marshalling of UnityEngine.UIElements.TextNativeSettings
struct TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062_marshaled_com
{
	Il2CppChar* ___text_0;
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___font_1;
	int32_t ___size_2;
	float ___scaling_3;
	int32_t ___style_4;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color_5;
	int32_t ___anchor_6;
	int32_t ___wordWrap_7;
	float ___wordWrapWidth_8;
	int32_t ___richText_9;
};

// UnityEngine.UIElements.TextVertex
struct TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3 
{
	// UnityEngine.Vector3 UnityEngine.UIElements.TextVertex::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Color32 UnityEngine.UIElements.TextVertex::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_1;
	// UnityEngine.Vector2 UnityEngine.UIElements.TextVertex::uv0
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___uv0_2;
};

// TriLibCore.ThreeMf.ThreeMfModel
struct ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2  : public RuntimeObject
{
	// System.String TriLibCore.ThreeMf.ThreeMfModel::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.ThreeMf.ThreeMfModel::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// UnityEngine.Vector3 TriLibCore.ThreeMf.ThreeMfModel::<LocalPosition>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalPositionU3Ek__BackingField_2;
	// UnityEngine.Quaternion TriLibCore.ThreeMf.ThreeMfModel::<LocalRotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CLocalRotationU3Ek__BackingField_3;
	// UnityEngine.Vector3 TriLibCore.ThreeMf.ThreeMfModel::<LocalScale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalScaleU3Ek__BackingField_4;
	// System.Boolean TriLibCore.ThreeMf.ThreeMfModel::<Visibility>k__BackingField
	bool ___U3CVisibilityU3Ek__BackingField_5;
	// TriLibCore.Interfaces.IModel TriLibCore.ThreeMf.ThreeMfModel::<Parent>k__BackingField
	RuntimeObject* ___U3CParentU3Ek__BackingField_6;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.ThreeMf.ThreeMfModel::<Children>k__BackingField
	RuntimeObject* ___U3CChildrenU3Ek__BackingField_7;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.ThreeMf.ThreeMfModel::<Bones>k__BackingField
	RuntimeObject* ___U3CBonesU3Ek__BackingField_8;
	// System.Boolean TriLibCore.ThreeMf.ThreeMfModel::<IsBone>k__BackingField
	bool ___U3CIsBoneU3Ek__BackingField_9;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.ThreeMf.ThreeMfModel::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_10;
	// UnityEngine.Matrix4x4[] TriLibCore.ThreeMf.ThreeMfModel::<BindPoses>k__BackingField
	Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___U3CBindPosesU3Ek__BackingField_11;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.ThreeMf.ThreeMfModel::<MaterialIndices>k__BackingField
	RuntimeObject* ___U3CMaterialIndicesU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.ThreeMf.ThreeMfModel::<UserProperties>k__BackingField
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___U3CUserPropertiesU3Ek__BackingField_13;
};

// TriLibCore.ThreeMf.Reader.ThreeMfReader
struct ThreeMfReader_t8F4D229EE9D88E8F68C2AC4FA06C66477A7545C1  : public ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449
{
	// System.Int32 TriLibCore.ThreeMf.Reader.ThreeMfReader::<ModelCount>k__BackingField
	int32_t ___U3CModelCountU3Ek__BackingField_10;
};

// TriLibCore.ThreeMf.ThreeMfTexture
struct ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22  : public RuntimeObject
{
	// System.Collections.Generic.IList`1<IxMilia.ThreeMf.ThreeMfTexture2DCoordinate> TriLibCore.ThreeMf.ThreeMfTexture::Coordinates
	RuntimeObject* ___Coordinates_0;
	// System.String TriLibCore.ThreeMf.ThreeMfTexture::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Boolean TriLibCore.ThreeMf.ThreeMfTexture::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_2;
	// System.Byte[] TriLibCore.ThreeMf.ThreeMfTexture::<Data>k__BackingField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___U3CDataU3Ek__BackingField_3;
	// System.IO.Stream TriLibCore.ThreeMf.ThreeMfTexture::<DataStream>k__BackingField
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___U3CDataStreamU3Ek__BackingField_4;
	// System.String TriLibCore.ThreeMf.ThreeMfTexture::<Filename>k__BackingField
	String_t* ___U3CFilenameU3Ek__BackingField_5;
	// UnityEngine.TextureWrapMode TriLibCore.ThreeMf.ThreeMfTexture::<WrapModeU>k__BackingField
	int32_t ___U3CWrapModeUU3Ek__BackingField_6;
	// UnityEngine.TextureWrapMode TriLibCore.ThreeMf.ThreeMfTexture::<WrapModeV>k__BackingField
	int32_t ___U3CWrapModeVU3Ek__BackingField_7;
	// UnityEngine.Vector2 TriLibCore.ThreeMf.ThreeMfTexture::<Tiling>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CTilingU3Ek__BackingField_8;
	// UnityEngine.Vector2 TriLibCore.ThreeMf.ThreeMfTexture::<Offset>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3COffsetU3Ek__BackingField_9;
	// System.Int32 TriLibCore.ThreeMf.ThreeMfTexture::<TextureId>k__BackingField
	int32_t ___U3CTextureIdU3Ek__BackingField_10;
	// System.String TriLibCore.ThreeMf.ThreeMfTexture::<ResolvedFilename>k__BackingField
	String_t* ___U3CResolvedFilenameU3Ek__BackingField_11;
	// System.Boolean TriLibCore.ThreeMf.ThreeMfTexture::<HasAlpha>k__BackingField
	bool ___U3CHasAlphaU3Ek__BackingField_12;
	// TriLibCore.General.TextureFormat TriLibCore.ThreeMf.ThreeMfTexture::<TextureFormat>k__BackingField
	int32_t ___U3CTextureFormatU3Ek__BackingField_13;
};

// UnityEngine.Touch
struct Touch_t03E51455ED508492B3F278903A0114FA0E87B417 
{
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;
};

// UnityEngine.UICharInfo
struct UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD 
{
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;
};

// UnityEngine.UIVertex
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 
{
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal_1;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___tangent_2;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color_3;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv0
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv0_4;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv1
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv1_5;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv2
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv2_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv3
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv3_7;
};

// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.XR.XRNodeState
struct XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A 
{
	// UnityEngine.XR.XRNode UnityEngine.XR.XRNodeState::m_Type
	int32_t ___m_Type_0;
	// UnityEngine.XR.AvailableTrackingData UnityEngine.XR.XRNodeState::m_AvailableFields
	int32_t ___m_AvailableFields_1;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_Position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Position_2;
	// UnityEngine.Quaternion UnityEngine.XR.XRNodeState::m_Rotation
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___m_Rotation_3;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_Velocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Velocity_4;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_AngularVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AngularVelocity_5;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_Acceleration
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Acceleration_6;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_AngularAcceleration
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AngularAcceleration_7;
	// System.Int32 UnityEngine.XR.XRNodeState::m_Tracked
	int32_t ___m_Tracked_8;
	// System.UInt64 UnityEngine.XR.XRNodeState::m_UniqueID
	uint64_t ___m_UniqueID_9;
};

// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Yoga.YogaConfig::_ygConfig
	intptr_t ____ygConfig_1;
	// UnityEngine.Yoga.Logger UnityEngine.Yoga.YogaConfig::_logger
	Logger_t092B1218ED93DD47180692D5761559B2054234A0* ____logger_2;
};

// UnityEngine.Yoga.YogaNode
struct YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Yoga.YogaNode::_ygNode
	intptr_t ____ygNode_0;
	// UnityEngine.Yoga.YogaConfig UnityEngine.Yoga.YogaNode::_config
	YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345* ____config_1;
	// System.WeakReference UnityEngine.Yoga.YogaNode::_parent
	WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* ____parent_2;
	// System.Collections.Generic.List`1<UnityEngine.Yoga.YogaNode> UnityEngine.Yoga.YogaNode::_children
	List_1_t84B666107A8A3ECB0F5A24B0243137D056DA9165* ____children_3;
	// UnityEngine.Yoga.MeasureFunction UnityEngine.Yoga.YogaNode::_measureFunction
	MeasureFunction_t60EBED1328F5328D4FA7E26335967E59E73B4D09* ____measureFunction_4;
	// UnityEngine.Yoga.BaselineFunction UnityEngine.Yoga.YogaNode::_baselineFunction
	BaselineFunction_t13AFADEF52F63320B2159C237635948AEB801679* ____baselineFunction_5;
	// System.Object UnityEngine.Yoga.YogaNode::_data
	RuntimeObject* ____data_6;
};

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData
struct DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A 
{
	// System.Int32 TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData::dataType
	int32_t ___dataType_0;
	// System.IntPtr TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData::data
	intptr_t ___data_1;
};

// Unity.VectorGraphics.InternalBridge/GradientSettingsBridge
struct GradientSettingsBridge_t3724705E3C6E74096502E2041E29482B09F05544 
{
	// Unity.VectorGraphics.InternalBridge/GradientTypeBridge Unity.VectorGraphics.InternalBridge/GradientSettingsBridge::gradientType
	int32_t ___gradientType_0;
	// Unity.VectorGraphics.InternalBridge/AddressModeBridge Unity.VectorGraphics.InternalBridge/GradientSettingsBridge::addressMode
	int32_t ___addressMode_1;
	// UnityEngine.Vector2 Unity.VectorGraphics.InternalBridge/GradientSettingsBridge::radialFocus
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___radialFocus_2;
	// UnityEngine.RectInt Unity.VectorGraphics.InternalBridge/GradientSettingsBridge::location
	RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 ___location_3;
};

// Unity.VectorGraphics.InternalBridge/VectorImageVertexBridge
struct VectorImageVertexBridge_t2843746D575BD491159E12640C7D2BC8E4CF0285 
{
	// UnityEngine.Vector3 Unity.VectorGraphics.InternalBridge/VectorImageVertexBridge::position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position_0;
	// UnityEngine.Color32 Unity.VectorGraphics.InternalBridge/VectorImageVertexBridge::tint
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___tint_1;
	// UnityEngine.Vector2 Unity.VectorGraphics.InternalBridge/VectorImageVertexBridge::uv
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___uv_2;
	// System.UInt32 Unity.VectorGraphics.InternalBridge/VectorImageVertexBridge::settingIndex
	uint32_t ___settingIndex_3;
};

// UnityEngine.LocationService/HeadingInfo
struct HeadingInfo_tDE66705847BB57D5815F1D570980E7C306B513BC 
{
	// System.Single UnityEngine.LocationService/HeadingInfo::magneticHeading
	float ___magneticHeading_0;
	// System.Single UnityEngine.LocationService/HeadingInfo::trueHeading
	float ___trueHeading_1;
	// System.Single UnityEngine.LocationService/HeadingInfo::headingAccuracy
	float ___headingAccuracy_2;
	// UnityEngine.Vector3 UnityEngine.LocationService/HeadingInfo::raw
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___raw_3;
	// System.Double UnityEngine.LocationService/HeadingInfo::timestamp
	double ___timestamp_4;
};

// UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_tFF31B8EC2855D0074AB86E8B37BEA6609070AC69 
{
	// UnityEngine.ParticleSystemGradientMode UnityEngine.ParticleSystem/MinMaxGradient::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMin
	Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E* ___m_GradientMin_1;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMax
	Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E* ___m_GradientMax_2;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMin
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_ColorMin_3;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMax
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_ColorMax_4;
};

// UnityEngine.ParticleSystem/Particle
struct Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D 
{
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_ParentRandomSeed
	uint32_t ___m_ParentRandomSeed_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_12;
	// System.Int32 UnityEngine.ParticleSystem/Particle::m_MeshIndex
	int32_t ___m_MeshIndex_13;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_14;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_15;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_Flags
	uint32_t ___m_Flags_16;
};

// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
struct AsciiNeedsEscapingData_t00F9B997CA974017BCD982A68921BC34F4D1C2FC 
{
	// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData::Data
	U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37 ___Data_0;
};
// Native definition for P/Invoke marshalling of System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
struct AsciiNeedsEscapingData_t00F9B997CA974017BCD982A68921BC34F4D1C2FC_marshaled_pinvoke
{
	U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37_marshaled_pinvoke ___Data_0;
};
// Native definition for COM marshalling of System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData
struct AsciiNeedsEscapingData_t00F9B997CA974017BCD982A68921BC34F4D1C2FC_marshaled_com
{
	U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37_marshaled_com ___Data_0;
};

// UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc
struct XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0 
{
	// System.IntPtr UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc::displaySubsystemInstance
	intptr_t ___displaySubsystemInstance_0;
	// System.Boolean UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc::nativeBlitAvailable
	bool ___nativeBlitAvailable_1;
	// System.Boolean UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc::nativeBlitInvalidStates
	bool ___nativeBlitInvalidStates_2;
	// System.Int32 UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc::blitParamsCount
	int32_t ___blitParamsCount_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc
struct XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0_marshaled_pinvoke
{
	intptr_t ___displaySubsystemInstance_0;
	int32_t ___nativeBlitAvailable_1;
	int32_t ___nativeBlitInvalidStates_2;
	int32_t ___blitParamsCount_3;
};
// Native definition for COM marshalling of UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc
struct XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0_marshaled_com
{
	intptr_t ___displaySubsystemInstance_0;
	int32_t ___nativeBlitAvailable_1;
	int32_t ___nativeBlitInvalidStates_2;
	int32_t ___blitParamsCount_3;
};

// UnityEngine.XR.Management.XRManagerSettings/<InitializeLoader>d__24
struct U3CInitializeLoaderU3Ed__24_t02DE083CB86C55D65BAF82701483413204DE18C7  : public RuntimeObject
{
	// System.Int32 UnityEngine.XR.Management.XRManagerSettings/<InitializeLoader>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.XR.Management.XRManagerSettings/<InitializeLoader>d__24::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// UnityEngine.XR.Management.XRManagerSettings UnityEngine.XR.Management.XRManagerSettings/<InitializeLoader>d__24::<>4__this
	XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52* ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.Management.XRLoader> UnityEngine.XR.Management.XRManagerSettings/<InitializeLoader>d__24::<>7__wrap1
	Enumerator_tE4C063339BE1A617569244A7F77DF2FBF2ABFA4A ___U3CU3E7__wrap1_3;
};

// UnityEngine.XR.XRMeshSubsystem/MeshTransformList
struct MeshTransformList_t761D725D4B30CFD7DDF57B3725004994FB3B561F 
{
	// System.IntPtr UnityEngine.XR.XRMeshSubsystem/MeshTransformList::m_Self
	intptr_t ___m_Self_0;
};

// Firebase.Messaging.FirebaseMessaging/Listener/<MessageReceivedDelegateMethod>c__AnonStorey0
struct U3CMessageReceivedDelegateMethodU3Ec__AnonStorey0_t522D1A0BD277B4D9560EE11B8C4B6147D72D9C6C  : public RuntimeObject
{
	// System.IntPtr Firebase.Messaging.FirebaseMessaging/Listener/<MessageReceivedDelegateMethod>c__AnonStorey0::message
	intptr_t ___message_0;
};

// UnityEngine.IntegratedSubsystem`1<UnityEngine.XR.XRDisplaySubsystemDescriptor>
struct IntegratedSubsystem_1_t8312865F01EEA1EDE4B24A973E47ADD526616848  : public IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3
{
};

// UnityEngine.IntegratedSubsystem`1<UnityEngine.XR.XRInputSubsystemDescriptor>
struct IntegratedSubsystem_1_tF93BC76362E85BDD215312162457BE510FC76D3B  : public IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3
{
};

// Firebase.Messaging.AndroidNotificationParams
struct AndroidNotificationParams_t5D31ABDF15208A4697A746B167C7C87A20CD5B1A  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.Messaging.AndroidNotificationParams::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.Messaging.AndroidNotificationParams::swigCMemOwn
	bool ___swigCMemOwn_1;
};

// UnityEngine.Animations.AnimationClipPlayable
struct AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174  : public RuntimeObject
{
	// System.Single UnityEngine.AnimationEvent::m_Time
	float ___m_Time_0;
	// System.String UnityEngine.AnimationEvent::m_FunctionName
	String_t* ___m_FunctionName_1;
	// System.String UnityEngine.AnimationEvent::m_StringParameter
	String_t* ___m_StringParameter_2;
	// UnityEngine.Object UnityEngine.AnimationEvent::m_ObjectReferenceParameter
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___m_ObjectReferenceParameter_3;
	// System.Single UnityEngine.AnimationEvent::m_FloatParameter
	float ___m_FloatParameter_4;
	// System.Int32 UnityEngine.AnimationEvent::m_IntParameter
	int32_t ___m_IntParameter_5;
	// System.Int32 UnityEngine.AnimationEvent::m_MessageOptions
	int32_t ___m_MessageOptions_6;
	// UnityEngine.AnimationEventSource UnityEngine.AnimationEvent::m_Source
	int32_t ___m_Source_7;
	// UnityEngine.AnimationState UnityEngine.AnimationEvent::m_StateSender
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::m_AnimatorStateInfo
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::m_AnimatorClipInfo
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174_marshaled_pinvoke
{
	float ___m_Time_0;
	char* ___m_FunctionName_1;
	char* ___m_StringParameter_2;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};
// Native definition for COM marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174_marshaled_com
{
	float ___m_Time_0;
	Il2CppChar* ___m_FunctionName_1;
	Il2CppChar* ___m_StringParameter_2;
	Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com* ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_tC704F25A20169025B6CFDC2F00DC84152B5B73CE* ___m_StateSender_8;
	AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2 ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03 ___m_AnimatorClipInfo_10;
};

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationMixerPlayable
struct AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable
struct AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationOffsetPlayable
struct AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationPlayableOutput
struct AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E 
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationPosePlayable
struct AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationPosePlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationRemoveScalePlayable
struct AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationRemoveScalePlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimationScriptPlayable
struct AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationScriptPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Animations.AnimatorControllerPlayable
struct AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.AudioClip
struct AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t3396D9613664F0AFF65FB91018FD0F901CC16F1E* ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t8D7135A2FB40647CAEC93F5254AD59E18DEB6072* ___m_PCMSetPositionCallback_5;
};

// UnityEngine.Audio.AudioClipPlayable
struct AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Audio.AudioMixerPlayable
struct AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.Audio.AudioPlayableOutput
struct AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20 
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Audio.AudioPlayableOutput::m_Handle
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle_0;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Networking.DownloadHandlerAudioClip
struct DownloadHandlerAudioClip_t11D829901BD9F3137CBB5D7BEA99FEAD976E56AC  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.DownloadHandlerAudioClip::m_NativeData
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerAudioClip
struct DownloadHandlerAudioClip_t11D829901BD9F3137CBB5D7BEA99FEAD976E56AC_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerAudioClip
struct DownloadHandlerAudioClip_t11D829901BD9F3137CBB5D7BEA99FEAD976E56AC_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};

// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.DownloadHandlerBuffer::m_NativeData
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
};

// UnityEngine.Networking.DownloadHandlerTexture
struct DownloadHandlerTexture_t45E2D719000AA1594E648810F0B57A77FA7C568C  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.DownloadHandlerTexture::m_NativeData
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
	// UnityEngine.Texture2D UnityEngine.Networking.DownloadHandlerTexture::mTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___mTexture_2;
	// System.Boolean UnityEngine.Networking.DownloadHandlerTexture::mHasTexture
	bool ___mHasTexture_3;
	// System.Boolean UnityEngine.Networking.DownloadHandlerTexture::mNonReadable
	bool ___mNonReadable_4;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerTexture
struct DownloadHandlerTexture_t45E2D719000AA1594E648810F0B57A77FA7C568C_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___mTexture_2;
	int32_t ___mHasTexture_3;
	int32_t ___mNonReadable_4;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerTexture
struct DownloadHandlerTexture_t45E2D719000AA1594E648810F0B57A77FA7C568C_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_NativeData_1;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___mTexture_2;
	int32_t ___mHasTexture_3;
	int32_t ___mNonReadable_4;
};

// Firebase.Crashlytics.FirebaseCrashlyticsFrame
struct FirebaseCrashlyticsFrame_tF9A1EF7CF973F7510734D32E076E07391C77B7D6  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.Crashlytics.FirebaseCrashlyticsFrame::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.Crashlytics.FirebaseCrashlyticsFrame::swigCMemOwn
	bool ___swigCMemOwn_1;
};

// Firebase.Crashlytics.FirebaseCrashlyticsInternal
struct FirebaseCrashlyticsInternal_t674756D0479C8ED14FE9E114B533CD274F9D6F40  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.Crashlytics.FirebaseCrashlyticsInternal::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.Crashlytics.FirebaseCrashlyticsInternal::swigCMemOwn
	bool ___swigCMemOwn_1;
};

// Firebase.Messaging.FirebaseMessage
struct FirebaseMessage_t09E58AB584806FC5C23F419AD7CDCE53969FE20F  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.Messaging.FirebaseMessage::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.Messaging.FirebaseMessage::swigCMemOwn
	bool ___swigCMemOwn_1;
};

// Firebase.Messaging.FirebaseNotification
struct FirebaseNotification_t17D8D8BE158C522C9DBA575BB7295EBE3413EB0A  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.Messaging.FirebaseNotification::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.Messaging.FirebaseNotification::swigCMemOwn
	bool ___swigCMemOwn_1;
};

// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t76D5E172DF8AA57E67763D453AAC40F0961D09B1* ___m_FontTextureRebuildCallback_5;
};

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord
struct GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E 
{
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_FirstAdjustmentRecord
	GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 ___m_FirstAdjustmentRecord_0;
	// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_SecondAdjustmentRecord
	GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7 ___m_SecondAdjustmentRecord_1;
	// UnityEngine.TextCore.LowLevel.FontFeatureLookupFlags UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord::m_FeatureLookupFlags
	int32_t ___m_FeatureLookupFlags_2;
};

// UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8 
{
	// System.String UnityEngine.HumanBone::m_BoneName
	String_t* ___m_BoneName_0;
	// System.String UnityEngine.HumanBone::m_HumanName
	String_t* ___m_HumanName_1;
	// UnityEngine.HumanLimit UnityEngine.HumanBone::limit
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_pinvoke
{
	char* ___m_BoneName_0;
	char* ___m_HumanName_1;
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};
// Native definition for COM marshalling of UnityEngine.HumanBone
struct HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8_marshaled_com
{
	Il2CppChar* ___m_BoneName_0;
	Il2CppChar* ___m_HumanName_1;
	HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E ___limit_2;
};

// Firebase.Crashlytics.LoggedException
struct LoggedException_t43B89090462BFFD9B76040EF52EE2EFD63359887  : public Exception_t
{
	// System.String Firebase.Crashlytics.LoggedException::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_18;
	// System.String Firebase.Crashlytics.LoggedException::<CustomStackTrace>k__BackingField
	String_t* ___U3CCustomStackTraceU3Ek__BackingField_19;
	// System.Collections.Generic.Dictionary`2<System.String,System.String>[] Firebase.Crashlytics.LoggedException::<ParsedStackTrace>k__BackingField
	Dictionary_2U5BU5D_tE4669D9AC2F1B83C2557CE335CA7669AED87E418* ___U3CParsedStackTraceU3Ek__BackingField_20;
};

// Firebase.Messaging.MessagingOptions
struct MessagingOptions_t8815B50D7DA2B2B7E1A353CD49EAE4E81997E65B  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.Messaging.MessagingOptions::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.Messaging.MessagingOptions::swigCMemOwn
	bool ___swigCMemOwn_1;
};

// UnityEngine.Motion
struct Motion_tBCD49FBF5608AD21FC03B63C8182FABCEF2707AC  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
	// System.Boolean UnityEngine.Motion::<isAnimatorMotion>k__BackingField
	bool ___U3CisAnimatorMotionU3Ek__BackingField_4;
};

// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid
struct NGMediaReceiveCallbackAndroid_tB8DDBFAD614592BF98999BEAC00A75E0D420CC52  : public AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D
{
	// NativeGallery/MediaPickCallback NativeGalleryNamespace.NGMediaReceiveCallbackAndroid::callback
	MediaPickCallback_tC207F6F3EEE18F64B03E23D5C9FA8C79517A5EB6* ___callback_4;
	// NativeGallery/MediaPickMultipleCallback NativeGalleryNamespace.NGMediaReceiveCallbackAndroid::callbackMultiple
	MediaPickMultipleCallback_t717A5D378751E2CACB53E6BA25386FF8DB41627B* ___callbackMultiple_5;
	// NativeGalleryNamespace.NGCallbackHelper NativeGalleryNamespace.NGMediaReceiveCallbackAndroid::callbackHelper
	NGCallbackHelper_t66F469013909FD93F5CEDB7C9C6689E79EB4605E* ___callbackHelper_6;
};

// NativeGalleryNamespace.NGPermissionCallbackAndroid
struct NGPermissionCallbackAndroid_t6824CF0103F77A92558E94C46463D4DECB491EEB  : public AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D
{
	// System.Object NativeGalleryNamespace.NGPermissionCallbackAndroid::threadLock
	RuntimeObject* ___threadLock_4;
	// System.Int32 NativeGalleryNamespace.NGPermissionCallbackAndroid::<Result>k__BackingField
	int32_t ___U3CResultU3Ek__BackingField_5;
};

// NativeShareNamespace.NSShareResultCallbackAndroid
struct NSShareResultCallbackAndroid_t70A5A1AEE38FC283E6B2D2F3D9FB87A08B60CFB0  : public AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D
{
	// NativeShareNamespace.NSCallbackHelper NativeShareNamespace.NSShareResultCallbackAndroid::callbackHelper
	NSCallbackHelper_t0FE98DE6D6CF554080E07CC69C0C7AD59C49824C* ___callbackHelper_4;
};

// TriLibCore.Obj.ObjRootModel
struct ObjRootModel_t77027B651C5E22D870234B61AE6983C35444FFD1  : public ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5
{
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Obj.ObjRootModel::<AllModels>k__BackingField
	RuntimeObject* ___U3CAllModelsU3Ek__BackingField_14;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Obj.ObjRootModel::<AllGeometryGroups>k__BackingField
	RuntimeObject* ___U3CAllGeometryGroupsU3Ek__BackingField_15;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Obj.ObjRootModel::<AllAnimations>k__BackingField
	RuntimeObject* ___U3CAllAnimationsU3Ek__BackingField_16;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Obj.ObjRootModel::<AllMaterials>k__BackingField
	RuntimeObject* ___U3CAllMaterialsU3Ek__BackingField_17;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Obj.ObjRootModel::<AllTextures>k__BackingField
	RuntimeObject* ___U3CAllTexturesU3Ek__BackingField_18;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Obj.ObjRootModel::<AllCameras>k__BackingField
	RuntimeObject* ___U3CAllCamerasU3Ek__BackingField_19;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Obj.ObjRootModel::<AllLights>k__BackingField
	RuntimeObject* ___U3CAllLightsU3Ek__BackingField_20;
};

// TriLibCore.Obj.ObjStreamReader
struct ObjStreamReader_tB33FBA136D003FD1BB87E158012A6EAA106DEA8E  : public StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B
{
	// Unity.Collections.NativeArray`1<System.Char> TriLibCore.Obj.ObjStreamReader::_charStream
	NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A ____charStream_29;
	// System.String TriLibCore.Obj.ObjStreamReader::_charString
	String_t* ____charString_30;
	// System.Int32 TriLibCore.Obj.ObjStreamReader::_charPosition
	int32_t ____charPosition_31;
};

// TriLibCore.Ply.PlyRootModel
struct PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237  : public PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2
{
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Ply.PlyRootModel::<AllModels>k__BackingField
	RuntimeObject* ___U3CAllModelsU3Ek__BackingField_14;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Ply.PlyRootModel::<AllGeometryGroups>k__BackingField
	RuntimeObject* ___U3CAllGeometryGroupsU3Ek__BackingField_15;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Ply.PlyRootModel::<AllAnimations>k__BackingField
	RuntimeObject* ___U3CAllAnimationsU3Ek__BackingField_16;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Ply.PlyRootModel::<AllMaterials>k__BackingField
	RuntimeObject* ___U3CAllMaterialsU3Ek__BackingField_17;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Ply.PlyRootModel::<AllTextures>k__BackingField
	RuntimeObject* ___U3CAllTexturesU3Ek__BackingField_18;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Ply.PlyRootModel::<AllCameras>k__BackingField
	RuntimeObject* ___U3CAllCamerasU3Ek__BackingField_19;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Ply.PlyRootModel::<AllLights>k__BackingField
	RuntimeObject* ___U3CAllLightsU3Ek__BackingField_20;
};

// TriLibCore.Ply.PlyStreamReader
struct PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2  : public StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B
{
	// Unity.Collections.NativeArray`1<System.Char> TriLibCore.Ply.PlyStreamReader::_charStream
	NativeArray_1_t32EE7AF88C534DBAA1A88B1DD5D55F78660D508A ____charStream_24;
	// System.String TriLibCore.Ply.PlyStreamReader::_charString
	String_t* ____charString_25;
	// System.Int32 TriLibCore.Ply.PlyStreamReader::_charPosition
	int32_t ____charPosition_26;
	// System.Int64 TriLibCore.Ply.PlyStreamReader::<Position>k__BackingField
	int64_t ___U3CPositionU3Ek__BackingField_27;
};

// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// Firebase.Crashlytics.StackFrames
struct StackFrames_t8F4306185C13967101E94E78F0D2312DDC4401AC  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.Crashlytics.StackFrames::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.Crashlytics.StackFrames::swigCMemOwn
	bool ___swigCMemOwn_1;
};

// TriLibCore.Stl.StlRootModel
struct StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142  : public StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA
{
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Stl.StlRootModel::<AllModels>k__BackingField
	RuntimeObject* ___U3CAllModelsU3Ek__BackingField_14;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Stl.StlRootModel::<AllGeometryGroups>k__BackingField
	RuntimeObject* ___U3CAllGeometryGroupsU3Ek__BackingField_15;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Stl.StlRootModel::<AllAnimations>k__BackingField
	RuntimeObject* ___U3CAllAnimationsU3Ek__BackingField_16;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Stl.StlRootModel::<AllMaterials>k__BackingField
	RuntimeObject* ___U3CAllMaterialsU3Ek__BackingField_17;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Stl.StlRootModel::<AllTextures>k__BackingField
	RuntimeObject* ___U3CAllTexturesU3Ek__BackingField_18;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Stl.StlRootModel::<AllCameras>k__BackingField
	RuntimeObject* ___U3CAllCamerasU3Ek__BackingField_19;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Stl.StlRootModel::<AllLights>k__BackingField
	RuntimeObject* ___U3CAllLightsU3Ek__BackingField_20;
};

// TriLibCore.Stl.StlStreamReader
struct StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5  : public StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B
{
	// System.Int32 TriLibCore.Stl.StlStreamReader::_endOfLinePointer
	int32_t ____endOfLinePointer_22;
	// System.Char[] TriLibCore.Stl.StlStreamReader::_charStream
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____charStream_24;
	// System.String TriLibCore.Stl.StlStreamReader::_charString
	String_t* ____charString_25;
	// System.Int32 TriLibCore.Stl.StlStreamReader::_charPosition
	int32_t ____charPosition_26;
	// System.Int32 TriLibCore.Stl.StlStreamReader::<Line>k__BackingField
	int32_t ___U3CLineU3Ek__BackingField_27;
};

// System.Text.Encodings.Web.TextEncoder
struct TextEncoder_t35B2FDACE089FFCF6F1460C93866D1271356E691  : public RuntimeObject
{
	// System.Byte[][] System.Text.Encodings.Web.TextEncoder::_asciiEscape
	ByteU5BU5DU5BU5D_t19A0C6D66F22DF673E9CDB37DEF566FE0EC947FA* ____asciiEscape_0;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encodings.Web.TextEncoder::_isAsciiCacheInitialized
	bool ____isAsciiCacheInitialized_1;
	// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData System.Text.Encodings.Web.TextEncoder::_asciiNeedsEscaping
	AsciiNeedsEscapingData_t00F9B997CA974017BCD982A68921BC34F4D1C2FC ____asciiNeedsEscaping_2;
};

// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC  : public RuntimeObject
{
	// System.IntPtr UnityEngine.TextGenerator::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.String UnityEngine.TextGenerator::m_LastString
	String_t* ___m_LastString_1;
	// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::m_LastSettings
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3 ___m_LastSettings_2;
	// System.Boolean UnityEngine.TextGenerator::m_HasGenerated
	bool ___m_HasGenerated_3;
	// UnityEngine.TextGenerationError UnityEngine.TextGenerator::m_LastValid
	int32_t ___m_LastValid_4;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::m_Verts
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	// System.Collections.Generic.List`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::m_Characters
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	// System.Collections.Generic.List`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::m_Lines
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	// System.Boolean UnityEngine.TextGenerator::m_CachedVerts
	bool ___m_CachedVerts_8;
	// System.Boolean UnityEngine.TextGenerator::m_CachedCharacters
	bool ___m_CachedCharacters_9;
	// System.Boolean UnityEngine.TextGenerator::m_CachedLines
	bool ___m_CachedLines_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	char* ___m_LastString_1;
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_pinvoke ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};
// Native definition for COM marshalling of UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppChar* ___m_LastString_1;
	TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3_marshaled_com ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t09F8990ACE8783E311B473B0090859BA9C00FC2A* ___m_Verts_5;
	List_1_t67A1600A303BB89506DFD21B59687088B7E0675B* ___m_Characters_6;
	List_1_t9209B29AC606399207E97BDCD817DEA5B6C63CA5* ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};

// TriLibCore.ThreeMf.ThreeMfRootModel
struct ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396  : public ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2
{
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.ThreeMf.ThreeMfRootModel::<AllModels>k__BackingField
	RuntimeObject* ___U3CAllModelsU3Ek__BackingField_14;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.ThreeMf.ThreeMfRootModel::<AllGeometryGroups>k__BackingField
	RuntimeObject* ___U3CAllGeometryGroupsU3Ek__BackingField_15;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.ThreeMf.ThreeMfRootModel::<AllAnimations>k__BackingField
	RuntimeObject* ___U3CAllAnimationsU3Ek__BackingField_16;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.ThreeMf.ThreeMfRootModel::<AllMaterials>k__BackingField
	RuntimeObject* ___U3CAllMaterialsU3Ek__BackingField_17;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.ThreeMf.ThreeMfRootModel::<AllTextures>k__BackingField
	RuntimeObject* ___U3CAllTexturesU3Ek__BackingField_18;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.ThreeMf.ThreeMfRootModel::<AllCameras>k__BackingField
	RuntimeObject* ___U3CAllCamerasU3Ek__BackingField_19;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.ThreeMf.ThreeMfRootModel::<AllLights>k__BackingField
	RuntimeObject* ___U3CAllLightsU3Ek__BackingField_20;
	// IxMilia.ThreeMf.ThreeMfFile TriLibCore.ThreeMf.ThreeMfRootModel::File
	ThreeMfFile_tD22A5D01D291BAE0C8B10321E0ECC7D350476781* ___File_21;
};

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6* ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804* ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com* ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13;
};

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C  : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C
{
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::<webRequest>k__BackingField
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_pinvoke : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_com : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com* ___U3CwebRequestU3Ek__BackingField_2;
};

// UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A  : public UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6
{
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.UploadHandlerRaw::m_Payload
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_Payload_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A_marshaled_pinvoke : public UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_Payload_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A_marshaled_com : public UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com
{
	NativeArray_1_t81F55263465517B73C455D3400CF67B4BADD85CF ___m_Payload_1;
};

// UnityEngine.UIElements.UIR.Utility
struct Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD  : public RuntimeObject
{
};

// UnityEngine.Experimental.Video.VideoClipPlayable
struct VideoClipPlayable_t2AAA6B40D35759730C170CA3124BC2499B50A0F1 
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::m_Handle
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle_0;
};

// UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0 
{
	// UnityEngine.ParticleSystem/Particle UnityEngine.ParticleSystem/EmitParams::m_Particle
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_PositionSet
	bool ___m_PositionSet_1;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_VelocitySet
	bool ___m_VelocitySet_2;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AxisOfRotationSet
	bool ___m_AxisOfRotationSet_3;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RotationSet
	bool ___m_RotationSet_4;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AngularVelocitySet
	bool ___m_AngularVelocitySet_5;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartSizeSet
	bool ___m_StartSizeSet_6;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartColorSet
	bool ___m_StartColorSet_7;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RandomSeedSet
	bool ___m_RandomSeedSet_8;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartLifetimeSet
	bool ___m_StartLifetimeSet_9;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_MeshIndexSet
	bool ___m_MeshIndexSet_10;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_ApplyShapeToPosition
	bool ___m_ApplyShapeToPosition_11;
};
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_pinvoke
{
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0_marshaled_com
{
	Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};

// UnityEngine.XR.XRDisplaySubsystem/XRRenderPass
struct XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8 
{
	// System.IntPtr UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::displaySubsystemInstance
	intptr_t ___displaySubsystemInstance_0;
	// System.Int32 UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::renderPassIndex
	int32_t ___renderPassIndex_1;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::renderTarget
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___renderTarget_2;
	// UnityEngine.RenderTextureDescriptor UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::renderTargetDesc
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___renderTargetDesc_3;
	// System.Boolean UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::hasMotionVectorPass
	bool ___hasMotionVectorPass_4;
	// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::motionVectorRenderTarget
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___motionVectorRenderTarget_5;
	// UnityEngine.RenderTextureDescriptor UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::motionVectorRenderTargetDesc
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___motionVectorRenderTargetDesc_6;
	// System.Boolean UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::shouldFillOutDepth
	bool ___shouldFillOutDepth_7;
	// System.Int32 UnityEngine.XR.XRDisplaySubsystem/XRRenderPass::cullingPassIndex
	int32_t ___cullingPassIndex_8;
};
// Native definition for P/Invoke marshalling of UnityEngine.XR.XRDisplaySubsystem/XRRenderPass
struct XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8_marshaled_pinvoke
{
	intptr_t ___displaySubsystemInstance_0;
	int32_t ___renderPassIndex_1;
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___renderTarget_2;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___renderTargetDesc_3;
	int32_t ___hasMotionVectorPass_4;
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___motionVectorRenderTarget_5;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___motionVectorRenderTargetDesc_6;
	int32_t ___shouldFillOutDepth_7;
	int32_t ___cullingPassIndex_8;
};
// Native definition for COM marshalling of UnityEngine.XR.XRDisplaySubsystem/XRRenderPass
struct XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8_marshaled_com
{
	intptr_t ___displaySubsystemInstance_0;
	int32_t ___renderPassIndex_1;
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___renderTarget_2;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___renderTargetDesc_3;
	int32_t ___hasMotionVectorPass_4;
	RenderTargetIdentifier_tA528663AC6EB3911D8E91AA40F7070FA5455442B ___motionVectorRenderTarget_5;
	RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 ___motionVectorRenderTargetDesc_6;
	int32_t ___shouldFillOutDepth_7;
	int32_t ___cullingPassIndex_8;
};

// UnityEngine.AnimatorOverrideController
struct AnimatorOverrideController_tF78BD58B30BB0D767E7A96F8428EA66F2DFD5493  : public RuntimeAnimatorController_t6F7C753402B42EC23C163099CF935C5E0D7A7254
{
	// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback UnityEngine.AnimatorOverrideController::OnOverrideControllerDirty
	OnOverrideControllerDirtyCallback_tDC67F7661A27502AD804BDE0B696955AFD4A44D5* ___OnOverrideControllerDirty_4;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
	// System.Boolean UnityEngine.CanvasRenderer::<isMask>k__BackingField
	bool ___U3CisMaskU3Ek__BackingField_4;
};

// System.Text.Encodings.Web.HtmlEncoder
struct HtmlEncoder_tE84C4B976DB5BD6F0123CBBCD60556491170E353  : public TextEncoder_t35B2FDACE089FFCF6F1460C93866D1271356E691
{
};

// System.Text.Encodings.Web.JavaScriptEncoder
struct JavaScriptEncoder_tB923B5C026111225AE05B65AB4B46D258AD72AA2  : public TextEncoder_t35B2FDACE089FFCF6F1460C93866D1271356E691
{
};

// UnityEngine.XR.OpenXR.Features.OpenXRFeature
struct OpenXRFeature_tC2F8F480D62C277B2ECDD605F64E45053CD85143  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.Boolean UnityEngine.XR.OpenXR.Features.OpenXRFeature::m_enabled
	bool ___m_enabled_4;
	// System.Boolean UnityEngine.XR.OpenXR.Features.OpenXRFeature::<failedInitialization>k__BackingField
	bool ___U3CfailedInitializationU3Ek__BackingField_5;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::nameUi
	String_t* ___nameUi_7;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::version
	String_t* ___version_8;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::featureIdInternal
	String_t* ___featureIdInternal_9;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::openxrExtensionStrings
	String_t* ___openxrExtensionStrings_10;
	// System.String UnityEngine.XR.OpenXR.Features.OpenXRFeature::company
	String_t* ___company_11;
	// System.Int32 UnityEngine.XR.OpenXR.Features.OpenXRFeature::priority
	int32_t ___priority_12;
	// System.Boolean UnityEngine.XR.OpenXR.Features.OpenXRFeature::required
	bool ___required_13;
	// System.Boolean UnityEngine.XR.OpenXR.Features.OpenXRFeature::internalFieldsUpdated
	bool ___internalFieldsUpdated_14;
};

// UnityEngine.XR.XRDisplaySubsystem
struct XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1  : public IntegratedSubsystem_1_t8312865F01EEA1EDE4B24A973E47ADD526616848
{
	// System.Action`1<System.Boolean> UnityEngine.XR.XRDisplaySubsystem::displayFocusChanged
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___displayFocusChanged_2;
};

// UnityEngine.XR.Management.XRGeneralSettings
struct XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// UnityEngine.XR.Management.XRManagerSettings UnityEngine.XR.Management.XRGeneralSettings::m_LoaderManagerInstance
	XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52* ___m_LoaderManagerInstance_6;
	// System.Boolean UnityEngine.XR.Management.XRGeneralSettings::m_InitManagerOnStart
	bool ___m_InitManagerOnStart_7;
	// UnityEngine.XR.Management.XRManagerSettings UnityEngine.XR.Management.XRGeneralSettings::m_XRManager
	XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52* ___m_XRManager_8;
	// System.Boolean UnityEngine.XR.Management.XRGeneralSettings::m_ProviderIntialized
	bool ___m_ProviderIntialized_9;
	// System.Boolean UnityEngine.XR.Management.XRGeneralSettings::m_ProviderStarted
	bool ___m_ProviderStarted_10;
};

// UnityEngine.XR.XRInputSubsystem
struct XRInputSubsystem_tFECE6683FCAEBF05BAD05E5D612690095D8BAD34  : public IntegratedSubsystem_1_tF93BC76362E85BDD215312162457BE510FC76D3B
{
	// System.Action`1<UnityEngine.XR.XRInputSubsystem> UnityEngine.XR.XRInputSubsystem::trackingOriginUpdated
	Action_1_tC867D66471C553CFFF8707FF2C59FB7AAB03086A* ___trackingOriginUpdated_2;
	// System.Action`1<UnityEngine.XR.XRInputSubsystem> UnityEngine.XR.XRInputSubsystem::boundaryChanged
	Action_1_tC867D66471C553CFFF8707FF2C59FB7AAB03086A* ___boundaryChanged_3;
	// System.Collections.Generic.List`1<System.UInt64> UnityEngine.XR.XRInputSubsystem::m_DeviceIdsCache
	List_1_tB88E7361EE76DFB3EBB7FCD60CC59ACC3E48C284* ___m_DeviceIdsCache_4;
};

// UnityEngine.XR.Management.XRLoader
struct XRLoader_t80B1B1934C40561C5352ABC95D567DC2A7C9C976  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
};

// UnityEngine.XR.Management.XRManagerSettings
struct XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// System.Boolean UnityEngine.XR.Management.XRManagerSettings::m_InitializationComplete
	bool ___m_InitializationComplete_4;
	// System.Boolean UnityEngine.XR.Management.XRManagerSettings::m_RequiresSettingsUpdate
	bool ___m_RequiresSettingsUpdate_5;
	// System.Boolean UnityEngine.XR.Management.XRManagerSettings::m_AutomaticLoading
	bool ___m_AutomaticLoading_6;
	// System.Boolean UnityEngine.XR.Management.XRManagerSettings::m_AutomaticRunning
	bool ___m_AutomaticRunning_7;
	// System.Collections.Generic.List`1<UnityEngine.XR.Management.XRLoader> UnityEngine.XR.Management.XRManagerSettings::m_Loaders
	List_1_tA1A04BD6B1EE83992AE369D5DB31A028E9B57822* ___m_Loaders_8;
	// System.Collections.Generic.HashSet`1<UnityEngine.XR.Management.XRLoader> UnityEngine.XR.Management.XRManagerSettings::m_RegisteredLoaders
	HashSet_1_t174593AE6599738C19A33586587D63534CED9F0F* ___m_RegisteredLoaders_9;
	// UnityEngine.XR.Management.XRLoader UnityEngine.XR.Management.XRManagerSettings::<activeLoader>k__BackingField
	XRLoader_t80B1B1934C40561C5352ABC95D567DC2A7C9C976* ___U3CactiveLoaderU3Ek__BackingField_10;
};

// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.ConformanceAutomationFeature
struct ConformanceAutomationFeature_t0C2E5D391DBCC81F1B86F2D7E34719B7D292BFF7  : public OpenXRFeature_tC2F8F480D62C277B2ECDD605F64E45053CD85143
{
};

// System.Text.Encodings.Web.DefaultHtmlEncoder
struct DefaultHtmlEncoder_t65341C0036795EED811809CFA8956E9B170C0CAE  : public HtmlEncoder_tE84C4B976DB5BD6F0123CBBCD60556491170E353
{
	// System.Text.Internal.AllowedCharactersBitmap System.Text.Encodings.Web.DefaultHtmlEncoder::_allowedCharacters
	AllowedCharactersBitmap_tA1BC142B179A112F4EAA6A6BF578B0BFD58E8BED ____allowedCharacters_4;
};

// System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin
struct DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C  : public JavaScriptEncoder_tB923B5C026111225AE05B65AB4B46D258AD72AA2
{
};

// UnityEngine.XR.OpenXR.Features.Mock.MockRuntime
struct MockRuntime_tC5F1019CA2D3E89010E3A3BFC89834251257B2AF  : public OpenXRFeature_tC2F8F480D62C277B2ECDD605F64E45053CD85143
{
	// System.Boolean UnityEngine.XR.OpenXR.Features.Mock.MockRuntime::ignoreValidationErrors
	bool ___ignoreValidationErrors_20;
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.XR.OpenXR.Features.OculusQuestSupport.OculusQuestFeature
struct OculusQuestFeature_tF5FA7128BB36F546D5CF7094A93B812DAC667810  : public OpenXRFeature_tC2F8F480D62C277B2ECDD605F64E45053CD85143
{
	// System.Boolean UnityEngine.XR.OpenXR.Features.OculusQuestSupport.OculusQuestFeature::targetQuest
	bool ___targetQuest_17;
	// System.Boolean UnityEngine.XR.OpenXR.Features.OculusQuestSupport.OculusQuestFeature::targetQuest2
	bool ___targetQuest2_18;
};

// UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature
struct RuntimeDebuggerOpenXRFeature_t1E3C60D2636DEC25D95E63BB3300DBFC5E9057BA  : public OpenXRFeature_tC2F8F480D62C277B2ECDD605F64E45053CD85143
{
	// System.UInt32 UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature::cacheSize
	uint32_t ___cacheSize_18;
	// System.UInt32 UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature::perThreadCacheSize
	uint32_t ___perThreadCacheSize_19;
	// System.UInt32 UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature::lutOffset
	uint32_t ___lutOffset_20;
};

// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t48EA4A8117B822BC59590150DED9AD46C62F65D3  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::prepareCompleted
	EventHandler_tE1FA51515FC895E3A02E317912FBDCDCF390A411* ___prepareCompleted_4;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::loopPointReached
	EventHandler_tE1FA51515FC895E3A02E317912FBDCDCF390A411* ___loopPointReached_5;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::started
	EventHandler_tE1FA51515FC895E3A02E317912FBDCDCF390A411* ___started_6;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::frameDropped
	EventHandler_tE1FA51515FC895E3A02E317912FBDCDCF390A411* ___frameDropped_7;
	// UnityEngine.Video.VideoPlayer/ErrorEventHandler UnityEngine.Video.VideoPlayer::errorReceived
	ErrorEventHandler_t6DD292ED79129AF67D909EDA603DA9DC5C762462* ___errorReceived_8;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::seekCompleted
	EventHandler_tE1FA51515FC895E3A02E317912FBDCDCF390A411* ___seekCompleted_9;
	// UnityEngine.Video.VideoPlayer/TimeEventHandler UnityEngine.Video.VideoPlayer::clockResyncOccurred
	TimeEventHandler_t8F77A92148F1A0F37AE8DE9CA7E25D90D001DC1E* ___clockResyncOccurred_10;
	// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler UnityEngine.Video.VideoPlayer::frameReady
	FrameReadyEventHandler_tDEDADF060B051C2953122274EBF3896AF48735E8* ___frameReady_11;
};

// UnityEngine.XR.Management.XRLoaderHelper
struct XRLoaderHelper_tE96E7AE003148D5319D20BAD7E02654367E41DCC  : public XRLoader_t80B1B1934C40561C5352ABC95D567DC2A7C9C976
{
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.ISubsystem> UnityEngine.XR.Management.XRLoaderHelper::m_SubsystemInstanceMap
	Dictionary_2_tCDC65F572855EBDD1C12CEE33EBEBE0131F60C9C* ___m_SubsystemInstanceMap_4;
};

// Lean.Touch.LeanPulseScale
struct LeanPulseScale_tD2A516679E7F239C031C666338D2753CA2ECCA6B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Vector3 Lean.Touch.LeanPulseScale::baseScale
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___baseScale_4;
	// System.Single Lean.Touch.LeanPulseScale::size
	float ___size_5;
	// System.Single Lean.Touch.LeanPulseScale::pulseInterval
	float ___pulseInterval_6;
	// System.Single Lean.Touch.LeanPulseScale::pulseSize
	float ___pulseSize_7;
	// System.Single Lean.Touch.LeanPulseScale::damping
	float ___damping_8;
	// System.Single Lean.Touch.LeanPulseScale::counter
	float ___counter_9;
};

// NativeGalleryNamespace.NGCallbackHelper
struct NGCallbackHelper_t66F469013909FD93F5CEDB7C9C6689E79EB4605E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Action NativeGalleryNamespace.NGCallbackHelper::mainThreadAction
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___mainThreadAction_4;
};

// NativeShareNamespace.NSCallbackHelper
struct NSCallbackHelper_t0FE98DE6D6CF554080E07CC69C0C7AD59C49824C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// NativeShare/ShareResultCallback NativeShareNamespace.NSCallbackHelper::callback
	ShareResultCallback_t9BEF49D9F4FF6C26758394EA70E84DD6339733C5* ___callback_4;
	// NativeShare/ShareResult NativeShareNamespace.NSCallbackHelper::result
	int32_t ___result_5;
	// System.String NativeShareNamespace.NSCallbackHelper::shareTarget
	String_t* ___shareTarget_6;
	// System.Boolean NativeShareNamespace.NSCallbackHelper::resultReceived
	bool ___resultReceived_7;
};

// OpenLinkBehaviour
struct OpenLinkBehaviour_t1F9BDD58F83CEF5D9BFD78CA4EB4D002A0F6007E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String OpenLinkBehaviour::Url
	String_t* ___Url_4;
};

// UnityEngine.SpatialTracking.TrackedPoseDriver
struct TrackedPoseDriver_t5A2DEE7957ED76932DB7460383BF09AB1CB4CCCD  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.SpatialTracking.TrackedPoseDriver/DeviceType UnityEngine.SpatialTracking.TrackedPoseDriver::m_Device
	int32_t ___m_Device_4;
	// UnityEngine.SpatialTracking.TrackedPoseDriver/TrackedPose UnityEngine.SpatialTracking.TrackedPoseDriver::m_PoseSource
	int32_t ___m_PoseSource_5;
	// UnityEngine.Experimental.XR.Interaction.BasePoseProvider UnityEngine.SpatialTracking.TrackedPoseDriver::m_PoseProviderComponent
	BasePoseProvider_t55E2883DF2C8052200284D64B68471636876FA1D* ___m_PoseProviderComponent_6;
	// UnityEngine.SpatialTracking.TrackedPoseDriver/TrackingType UnityEngine.SpatialTracking.TrackedPoseDriver::m_TrackingType
	int32_t ___m_TrackingType_7;
	// UnityEngine.SpatialTracking.TrackedPoseDriver/UpdateType UnityEngine.SpatialTracking.TrackedPoseDriver::m_UpdateType
	int32_t ___m_UpdateType_8;
	// System.Boolean UnityEngine.SpatialTracking.TrackedPoseDriver::m_UseRelativeTransform
	bool ___m_UseRelativeTransform_9;
	// UnityEngine.Pose UnityEngine.SpatialTracking.TrackedPoseDriver::m_OriginPose
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 ___m_OriginPose_10;
};

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript
struct UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::_onJob
	bool ____onJob_4;
};

// WebViewObject
struct WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Action`1<System.String> WebViewObject::onJS
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onJS_4;
	// System.Action`1<System.String> WebViewObject::onError
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onError_5;
	// System.Action`1<System.String> WebViewObject::onHttpError
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onHttpError_6;
	// System.Action`1<System.String> WebViewObject::onStarted
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onStarted_7;
	// System.Action`1<System.String> WebViewObject::onLoaded
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onLoaded_8;
	// System.Action`1<System.String> WebViewObject::onHooked
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___onHooked_9;
	// System.Boolean WebViewObject::visibility
	bool ___visibility_10;
	// System.Boolean WebViewObject::alertDialogEnabled
	bool ___alertDialogEnabled_11;
	// System.Boolean WebViewObject::scrollBounceEnabled
	bool ___scrollBounceEnabled_12;
	// System.Int32 WebViewObject::mMarginLeft
	int32_t ___mMarginLeft_13;
	// System.Int32 WebViewObject::mMarginTop
	int32_t ___mMarginTop_14;
	// System.Int32 WebViewObject::mMarginRight
	int32_t ___mMarginRight_15;
	// System.Int32 WebViewObject::mMarginBottom
	int32_t ___mMarginBottom_16;
	// System.Boolean WebViewObject::mMarginRelative
	bool ___mMarginRelative_17;
	// System.Single WebViewObject::mMarginLeftComputed
	float ___mMarginLeftComputed_18;
	// System.Single WebViewObject::mMarginTopComputed
	float ___mMarginTopComputed_19;
	// System.Single WebViewObject::mMarginRightComputed
	float ___mMarginRightComputed_20;
	// System.Single WebViewObject::mMarginBottomComputed
	float ___mMarginBottomComputed_21;
	// System.Boolean WebViewObject::mMarginRelativeComputed
	bool ___mMarginRelativeComputed_22;
	// UnityEngine.AndroidJavaObject WebViewObject::webView
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* ___webView_23;
	// System.Boolean WebViewObject::mVisibility
	bool ___mVisibility_24;
	// System.Boolean WebViewObject::mIsKeyboardVisible
	bool ___mIsKeyboardVisible_25;
	// System.Int32 WebViewObject::mWindowVisibleDisplayFrameHeight
	int32_t ___mWindowVisibleDisplayFrameHeight_26;
	// System.Single WebViewObject::mResumedTimestamp
	float ___mResumedTimestamp_27;
};

// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir
struct SynchronizationContextBehavoir_t77EF1109836806C11E4F2C8ECF2867D9531FEBBA  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir::callbackQueue
	Queue_1_tBB890027A96D0D3343916455489502BC8FBEB73F* ___callbackQueue_4;
};

// System.Collections.Generic.Dictionary`2<System.String,System.String>

// System.Collections.Generic.Dictionary`2<System.String,System.String>

// Byn.Awrtc.Base.AWebRtcCall
struct AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD_StaticFields
{
	// System.String Byn.Awrtc.Base.AWebRtcCall::LOGTAG
	String_t* ___LOGTAG_0;
	// System.Byte Byn.Awrtc.Base.AWebRtcCall::MESSAGE_TYPE_DATA
	uint8_t ___MESSAGE_TYPE_DATA_6;
	// System.Byte Byn.Awrtc.Base.AWebRtcCall::MESSAGE_TYPE_STRING
	uint8_t ___MESSAGE_TYPE_STRING_7;
};

// Byn.Awrtc.Base.AWebRtcCall

// UnityEngine.Analytics.AnalyticsSessionInfo
struct AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76_StaticFields
{
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t1180FB66E702B635CAD9316DC661D931277B2A0C* ___sessionStateChanged_0;
	// UnityEngine.Analytics.AnalyticsSessionInfo/IdentityTokenChanged UnityEngine.Analytics.AnalyticsSessionInfo::identityTokenChanged
	IdentityTokenChanged_tE8CB0DAB5F6E640A847803F582E6CB6237742395* ___identityTokenChanged_1;
};

// UnityEngine.Analytics.AnalyticsSessionInfo

// ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider

// ImaginationOverflow.UniversalDeepLinking.Providers.AndroidLinkProvider

// Firebase.Platform.Default.AppConfigExtensions
struct AppConfigExtensions_t6B8627CD4EFF8F05D2F749CC406E5E12F04CEE48_StaticFields
{
	// System.Uri Firebase.Platform.Default.AppConfigExtensions::DefaultUpdateUrl
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___DefaultUpdateUrl_0;
	// System.String Firebase.Platform.Default.AppConfigExtensions::Default
	String_t* ___Default_1;
	// System.Object Firebase.Platform.Default.AppConfigExtensions::Sync
	RuntimeObject* ___Sync_2;
	// Firebase.Platform.Default.AppConfigExtensions Firebase.Platform.Default.AppConfigExtensions::_instance
	AppConfigExtensions_t6B8627CD4EFF8F05D2F749CC406E5E12F04CEE48* ____instance_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>> Firebase.Platform.Default.AppConfigExtensions::SStringState
	Dictionary_2_t73D7B210F00FD83B4F194279B052C32E9FCB1D04* ___SStringState_4;
};

// Firebase.Platform.Default.AppConfigExtensions

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration

// System.Attribute

// System.Attribute

// UnityEngine.Experimental.Audio.AudioSampleProvider

// UnityEngine.Experimental.Audio.AudioSampleProvider

// UnityEngine.AudioSettings
struct AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields
{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_tE071B0CBA3B3A77D3E41F5FCB65B4017885B3177* ___OnAudioConfigurationChanged_0;
	// System.Action UnityEngine.AudioSettings::OnAudioSystemShuttingDown
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnAudioSystemShuttingDown_1;
	// System.Action UnityEngine.AudioSettings::OnAudioSystemStartedUp
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnAudioSystemStartedUp_2;
};

// UnityEngine.AudioSettings

// Byn.Awrtc.BufferedFrame

// Byn.Awrtc.BufferedFrame

// ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage
struct ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields
{
	// System.String[] ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::SaveFolders
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___SaveFolders_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::SaveFile
	String_t* ___SaveFile_1;
	// System.String ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage::_fileLocation
	String_t* ____fileLocation_2;
};

// ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE
struct CrashlyticsInternalPINVOKE_t691F261E9A85343DA87F1275433EFC3767B4981B_StaticFields
{
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper Firebase.Crashlytics.CrashlyticsInternalPINVOKE::swigExceptionHelper
	SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970* ___swigExceptionHelper_0;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGStringHelper Firebase.Crashlytics.CrashlyticsInternalPINVOKE::swigStringHelper
	SWIGStringHelper_tA8CF9EF78BD75BC8C5CB597CC99E74764BFE361B* ___swigStringHelper_1;
};

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE

// UnityEngine.CustomYieldInstruction

// UnityEngine.CustomYieldInstruction

// Firebase.Platform.DebugLogger
struct DebugLogger_t06BB6B0EBB4D36FCB961CCF80EE1FE4F0A88B4C1_StaticFields
{
	// Firebase.Platform.DebugLogger Firebase.Platform.DebugLogger::_instance
	DebugLogger_t06BB6B0EBB4D36FCB961CCF80EE1FE4F0A88B4C1* ____instance_0;
};

// Firebase.Platform.DebugLogger

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager
struct DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_StaticFields
{
	// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::<Instance>k__BackingField
	DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* ___U3CInstanceU3Ek__BackingField_0;
};

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager

// Firebase.Dispatcher

// Firebase.Dispatcher

// TriLibCore.Gltf.Draco.DracoMeshLoader

// TriLibCore.Gltf.Draco.DracoMeshLoader

// ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider

// ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider

// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider
struct EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_StaticFields
{
	// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::_instance
	EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* ____instance_0;
};

// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider

// System.EventArgs
struct EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377_StaticFields
{
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377* ___Empty_0;
};

// System.EventArgs

// Firebase.ExceptionAggregator

// Firebase.ExceptionAggregator
struct ExceptionAggregator_t79259EF5E7CCF32340080CFAC267848A89DA90E3_ThreadStaticFields
{
	// System.Collections.Generic.List`1<System.Exception> Firebase.ExceptionAggregator::threadLocalExceptions
	List_1_tA61E6ACDC88C89A9523D28C560996ABFF548352B* ___threadLocalExceptions_0;
};

// Firebase.Crashlytics.ExceptionHandler

// Firebase.Crashlytics.ExceptionHandler

// Firebase.Platform.FirebaseAppUtilsStub
struct FirebaseAppUtilsStub_tA2C798DD03529908BA1CC968E5CD737BA58F5A02_StaticFields
{
	// Firebase.Platform.FirebaseAppUtilsStub Firebase.Platform.FirebaseAppUtilsStub::_instance
	FirebaseAppUtilsStub_tA2C798DD03529908BA1CC968E5CD737BA58F5A02* ____instance_0;
};

// Firebase.Platform.FirebaseAppUtilsStub

// Firebase.Platform.FirebaseEditorDispatcher
struct FirebaseEditorDispatcher_tC36AA65B08B38AB2721AADB81D51BA16CE86239C_StaticFields
{
	// System.Action Firebase.Platform.FirebaseEditorDispatcher::<>f__mg$cache0
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___U3CU3Ef__mgU24cache0_0;
	// System.Action Firebase.Platform.FirebaseEditorDispatcher::<>f__mg$cache1
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___U3CU3Ef__mgU24cache1_1;
	// System.Action Firebase.Platform.FirebaseEditorDispatcher::<>f__mg$cache2
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___U3CU3Ef__mgU24cache2_2;
};

// Firebase.Platform.FirebaseEditorDispatcher

// Firebase.Platform.FirebaseHandler
struct FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586_StaticFields
{
	// Firebase.Platform.FirebaseMonoBehaviour Firebase.Platform.FirebaseHandler::firebaseMonoBehaviour
	FirebaseMonoBehaviour_t0DC02A14DFF90538B38698592F07B4373DB03C72* ___firebaseMonoBehaviour_0;
	// Firebase.Platform.IFirebaseAppUtils Firebase.Platform.FirebaseHandler::<AppUtils>k__BackingField
	RuntimeObject* ___U3CAppUtilsU3Ek__BackingField_1;
	// System.Int32 Firebase.Platform.FirebaseHandler::tickCount
	int32_t ___tickCount_2;
	// Firebase.Dispatcher Firebase.Platform.FirebaseHandler::<ThreadDispatcher>k__BackingField
	Dispatcher_tBD1370511B5D6C10B211FBD3AF8E4F9D6B3CD8C6* ___U3CThreadDispatcherU3Ek__BackingField_3;
	// Firebase.Platform.FirebaseHandler Firebase.Platform.FirebaseHandler::firebaseHandler
	FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586* ___firebaseHandler_5;
	// System.Func`1<System.Boolean> Firebase.Platform.FirebaseHandler::<>f__am$cache0
	Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___U3CU3Ef__amU24cache0_9;
};

// Firebase.Platform.FirebaseHandler

// Firebase.Platform.FirebaseLogger
struct FirebaseLogger_t17574FB770AB2CB02AF3459B2EF9D27FC9C24F42_StaticFields
{
	// Firebase.Platform.MainThreadProperty`1<System.Boolean> Firebase.Platform.FirebaseLogger::incompatibleStackUnwindingEnabled
	MainThreadProperty_1_t0B3DED62013B23F9A8CB8C13A6EC7B04D8F02E97* ___incompatibleStackUnwindingEnabled_0;
	// System.Func`1<System.Boolean> Firebase.Platform.FirebaseLogger::<>f__mg$cache0
	Func_1_t2BE7F58348C9CC544A8973B3A9E55541DE43C457* ___U3CU3Ef__mgU24cache0_1;
};

// Firebase.Platform.FirebaseLogger

// Firebase.Messaging.FirebaseMessaging
struct FirebaseMessaging_tAF3E6E43968D1C156E887454B8F9BDB7723036D3_StaticFields
{
	// System.EventHandler`1<Firebase.Messaging.MessageReceivedEventArgs> Firebase.Messaging.FirebaseMessaging::MessageReceivedInternal
	EventHandler_1_tF3AA5EB88F494B5147D2E245A3AF5AB3CDF8D72B* ___MessageReceivedInternal_0;
	// System.EventHandler`1<Firebase.Messaging.TokenReceivedEventArgs> Firebase.Messaging.FirebaseMessaging::TokenReceivedInternal
	EventHandler_1_t628C7697D75CC4686AD6125D745540AC427C28A0* ___TokenReceivedInternal_1;
	// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging::listener
	Listener_tC16E83118F05783B5CA41A3F81ABF1A2C83D7BF7* ___listener_2;
};

// Firebase.Messaging.FirebaseMessaging

// Firebase.Messaging.FirebaseMessagingPINVOKE
struct FirebaseMessagingPINVOKE_t8D162519505D2E2C12BFCD6037CFE376283ABF4F_StaticFields
{
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper Firebase.Messaging.FirebaseMessagingPINVOKE::swigExceptionHelper
	SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C* ___swigExceptionHelper_0;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper Firebase.Messaging.FirebaseMessagingPINVOKE::swigStringHelper
	SWIGStringHelper_t953C1EA2870BB0BF9A25500F62877BBBA6D44AE9* ___swigStringHelper_1;
};

// Firebase.Messaging.FirebaseMessagingPINVOKE

// UnityEngine.TextCore.LowLevel.FontEngine
struct FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields
{
	// UnityEngine.TextCore.Glyph[] UnityEngine.TextCore.LowLevel.FontEngine::s_Glyphs
	GlyphU5BU5D_t345CEC8703A6C650639C40DB7D35269A2D467FC5* ___s_Glyphs_0;
	// System.UInt32[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphIndexes_MarshallingArray_A
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___s_GlyphIndexes_MarshallingArray_A_1;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_IN
	GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E* ___s_GlyphMarshallingStruct_IN_2;
	// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct[] UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphMarshallingStruct_OUT
	GlyphMarshallingStructU5BU5D_t9424A4B1FAAD615472A9346208026B1B9E22069E* ___s_GlyphMarshallingStruct_OUT_3;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_FreeGlyphRects
	GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70* ___s_FreeGlyphRects_4;
	// UnityEngine.TextCore.GlyphRect[] UnityEngine.TextCore.LowLevel.FontEngine::s_UsedGlyphRects
	GlyphRectU5BU5D_t494B690215E3F3F42B6F216930A461256CE2CC70* ___s_UsedGlyphRects_5;
	// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord[] UnityEngine.TextCore.LowLevel.FontEngine::s_PairAdjustmentRecords_MarshallingArray
	GlyphPairAdjustmentRecordU5BU5D_tD5DD2A739A4CA745E7F28ECCB2CD0BD0A65A38F7* ___s_PairAdjustmentRecords_MarshallingArray_6;
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.TextCore.Glyph> UnityEngine.TextCore.LowLevel.FontEngine::s_GlyphLookupDictionary
	Dictionary_2_tC61348D10610A6B3D7B65102D82AC3467D59EAA7* ___s_GlyphLookupDictionary_7;
};

// UnityEngine.TextCore.LowLevel.FontEngine

// TriLibCore.Geometries.Geometry

// TriLibCore.Geometries.Geometry

// UnityEngine.Gyroscope

// UnityEngine.Gyroscope

// Firebase.Crashlytics.Impl
struct Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF_StaticFields
{
	// System.String Firebase.Crashlytics.Impl::LogString
	String_t* ___LogString_0;
	// System.String Firebase.Crashlytics.Impl::SetKeyValueString
	String_t* ___SetKeyValueString_1;
	// System.String Firebase.Crashlytics.Impl::SetUserIdentifierString
	String_t* ___SetUserIdentifierString_2;
	// System.String Firebase.Crashlytics.Impl::LogExceptionString
	String_t* ___LogExceptionString_3;
	// System.String Firebase.Crashlytics.Impl::IsCrashlyticsCollectionEnabledString
	String_t* ___IsCrashlyticsCollectionEnabledString_4;
	// System.String Firebase.Crashlytics.Impl::SetCrashlyticsCollectionEnabledString
	String_t* ___SetCrashlyticsCollectionEnabledString_5;
};

// Firebase.Crashlytics.Impl

// UnityEngine.Input
struct Input_t47D83E2A50E6AF7F8A47AA06FBEF9EBE6BBC22BB_StaticFields
{
	// UnityEngine.Compass UnityEngine.Input::compassInstance
	Compass_t161CD14062E71ED7324FE12ED003EE0877EE546F* ___compassInstance_0;
	// UnityEngine.Gyroscope UnityEngine.Input::s_MainGyro
	Gyroscope_tA4CEC0F47FFB4CEB90410CC6B860D052BB35BE9E* ___s_MainGyro_1;
};

// UnityEngine.Input

// UnityEngine.XR.InputDevices
struct InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice> UnityEngine.XR.InputDevices::s_InputDeviceList
	List_1_tB66D13E0427CA48C600AEA4DBA5B173C3734C6CE* ___s_InputDeviceList_0;
	// System.Action`1<UnityEngine.XR.InputDevice> UnityEngine.XR.InputDevices::deviceConnected
	Action_1_tFAB0B519749BBE2B7AAD946105FAE8116636A8BC* ___deviceConnected_1;
	// System.Action`1<UnityEngine.XR.InputDevice> UnityEngine.XR.InputDevices::deviceDisconnected
	Action_1_tFAB0B519749BBE2B7AAD946105FAE8116636A8BC* ___deviceDisconnected_2;
	// System.Action`1<UnityEngine.XR.InputDevice> UnityEngine.XR.InputDevices::deviceConfigChanged
	Action_1_tFAB0B519749BBE2B7AAD946105FAE8116636A8BC* ___deviceConfigChanged_3;
};

// UnityEngine.XR.InputDevices

// UnityEngine.XR.InputTracking
struct InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1_StaticFields
{
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::trackingAcquired
	Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8* ___trackingAcquired_0;
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::trackingLost
	Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8* ___trackingLost_1;
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::nodeAdded
	Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8* ___nodeAdded_2;
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::nodeRemoved
	Action_1_t290119641EBA3C1EAC8AF78274C63CE01C3046D8* ___nodeRemoved_3;
};

// UnityEngine.XR.InputTracking

// ImaginationOverflow.UniversalDeepLinking.LinkActivation

// ImaginationOverflow.UniversalDeepLinking.LinkActivation

// ImaginationOverflow.UniversalDeepLinking.LinkInformation

// ImaginationOverflow.UniversalDeepLinking.LinkInformation

// System.MarshalByRefObject

// System.MarshalByRefObject

// Firebase.Crashlytics.Metadata

// Firebase.Crashlytics.Metadata

// Firebase.Crashlytics.MetadataBuilder
struct MetadataBuilder_t7BB701F903E4674E17AF9A4C8EE07943B6616FE9_StaticFields
{
	// System.String Firebase.Crashlytics.MetadataBuilder::METADATA_KEY
	String_t* ___METADATA_KEY_0;
};

// Firebase.Crashlytics.MetadataBuilder

// UnityEngine.Networking.MultipartFormDataSection

// UnityEngine.Networking.MultipartFormDataSection

// UnityEngine.Networking.MultipartFormFileSection

// UnityEngine.Networking.MultipartFormFileSection

// NativeGallery
struct NativeGallery_t5E55EACA16F196F4214E0A3AB3A39B5CF555EB0B_StaticFields
{
	// UnityEngine.AndroidJavaClass NativeGallery::m_ajc
	AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03* ___m_ajc_0;
	// UnityEngine.AndroidJavaObject NativeGallery::m_context
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* ___m_context_1;
	// System.String NativeGallery::m_temporaryImagePath
	String_t* ___m_temporaryImagePath_2;
	// System.String NativeGallery::m_selectedMediaPath
	String_t* ___m_selectedMediaPath_3;
};

// NativeGallery

// UnityEngineInternal.Input.NativeInputSystem
struct NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE_StaticFields
{
	// UnityEngineInternal.Input.NativeUpdateCallback UnityEngineInternal.Input.NativeInputSystem::onUpdate
	NativeUpdateCallback_tC5CA5A9117B79251968A4DA3758552EFE1D37495* ___onUpdate_0;
	// System.Action`1<UnityEngineInternal.Input.NativeInputUpdateType> UnityEngineInternal.Input.NativeInputSystem::onBeforeUpdate
	Action_1_t7797D4D8783204B10C3D28B96B049C48276C3B1B* ___onBeforeUpdate_1;
	// System.Func`2<UnityEngineInternal.Input.NativeInputUpdateType,System.Boolean> UnityEngineInternal.Input.NativeInputSystem::onShouldRunUpdate
	Func_2_t880CA675AE5D39E081BEEF14DC092D82674DE4F2* ___onShouldRunUpdate_2;
	// System.Action`2<System.Int32,System.String> UnityEngineInternal.Input.NativeInputSystem::s_OnDeviceDiscoveredCallback
	Action_2_t6AAF2E215E74E16A4EEF0A0749A4A325D99F5BA6* ___s_OnDeviceDiscoveredCallback_3;
};

// UnityEngineInternal.Input.NativeInputSystem

// NativeShare
struct NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B_StaticFields
{
	// UnityEngine.AndroidJavaClass NativeShare::m_ajc
	AndroidJavaClass_tE6296B30CC4BF84434A9B765267F3FD0DD8DDB03* ___m_ajc_0;
	// UnityEngine.AndroidJavaObject NativeShare::m_context
	AndroidJavaObject_t8FFB930F335C1178405B82AC2BF512BB1EEF9EB0* ___m_context_1;
};

// NativeShare

// Byn.Awrtc.NetworkConfig

// Byn.Awrtc.NetworkConfig

// TriLibCore.Obj.ObjMaterial

// TriLibCore.Obj.ObjMaterial

// UnityEngine.Physics
struct Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields
{
	// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>> UnityEngine.Physics::ContactModifyEvent
	Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2* ___ContactModifyEvent_0;
	// System.Action`2<UnityEngine.PhysicsScene,Unity.Collections.NativeArray`1<UnityEngine.ModifiableContactPair>> UnityEngine.Physics::ContactModifyEventCCD
	Action_2_t70E17A6F8F03189031C560482454FE2D87F496F2* ___ContactModifyEventCCD_1;
};

// UnityEngine.Physics

// UnityEngine.Physics2D
struct Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D> UnityEngine.Physics2D::m_LastDisabledRigidbody2D
	List_1_tCD5F926D25FC8BFAF39E4BE6F879C1FA11501C76* ___m_LastDisabledRigidbody2D_0;
};

// UnityEngine.Physics2D

// Firebase.Platform.PlatformInformation
struct PlatformInformation_t6F0BE1D51BAD01D2A08CEC7ED766A21511F40E94_StaticFields
{
	// System.String Firebase.Platform.PlatformInformation::runtimeVersion
	String_t* ___runtimeVersion_0;
	// System.Single Firebase.Platform.PlatformInformation::<RealtimeSinceStartupSafe>k__BackingField
	float ___U3CRealtimeSinceStartupSafeU3Ek__BackingField_1;
	// System.Func`1<System.String> Firebase.Platform.PlatformInformation::<>f__am$cache0
	Func_1_t367387BB2C476D3F32DB12161B5FDC128DC3231C* ___U3CU3Ef__amU24cache0_2;
	// System.Func`1<System.String> Firebase.Platform.PlatformInformation::<>f__am$cache1
	Func_1_t367387BB2C476D3F32DB12161B5FDC128DC3231C* ___U3CU3Ef__amU24cache1_3;
};

// Firebase.Platform.PlatformInformation

// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration

// ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration

// TriLibCore.Ply.PlyElement

// TriLibCore.Ply.PlyElement

// TriLibCore.Ply.PlyMaterial

// TriLibCore.Ply.PlyMaterial

// TriLibCore.Ply.PlyProcessor

// TriLibCore.Ply.PlyProcessor

// TriLibCore.Ply.PlyProperty

// TriLibCore.Ply.PlyProperty

// UnityEngine.SpatialTracking.PoseDataSource
struct PoseDataSource_tF08072CF4DBA9641E6AE6D1DBA1D421DA9C46858_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.XR.XRNodeState> UnityEngine.SpatialTracking.PoseDataSource::nodeStates
	List_1_t8AE5A65CCFBC753343962D38B82C801F7BDE3C87* ___nodeStates_0;
};

// UnityEngine.SpatialTracking.PoseDataSource

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449_StaticFields
{
	// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64> TriLibCore.ReaderBase::ProfileStepCallback
	Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2* ___ProfileStepCallback_0;
};

// TriLibCore.ReaderBase

// UnityEngine.RectTransformUtility
struct RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_StaticFields
{
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Corners_0;
};

// UnityEngine.RectTransformUtility

// UnityEngine.RemoteSettings
struct RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250_StaticFields
{
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_tB0D5A5BA322FE093894992C29DCF51E7E12579C4* ___Updated_0;
	// System.Action UnityEngine.RemoteSettings::BeforeFetchFromServer
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___BeforeFetchFromServer_1;
	// System.Action`3<System.Boolean,System.Boolean,System.Int32> UnityEngine.RemoteSettings::Completed
	Action_3_t4730167C8E7EB19F1E0034580790A915D549F6CB* ___Completed_2;
};

// UnityEngine.RemoteSettings

// HDRLoader.Rgbe

// HDRLoader.Rgbe

// Byn.Awrtc.SLog
struct SLog_tB306D01548C5F9057FEBA18F1046B6FBCCBB1721_StaticFields
{
	// System.String Byn.Awrtc.SLog::TAG_WARNING
	String_t* ___TAG_WARNING_0;
	// System.String Byn.Awrtc.SLog::TAG_ERROR
	String_t* ___TAG_ERROR_1;
	// System.String Byn.Awrtc.SLog::TAG_EXCEPTION
	String_t* ___TAG_EXCEPTION_2;
	// System.String Byn.Awrtc.SLog::TAG_INFO
	String_t* ___TAG_INFO_3;
	// System.String Byn.Awrtc.SLog::TAG_DEBUG
	String_t* ___TAG_DEBUG_4;
	// System.String Byn.Awrtc.SLog::TAG_VERBOSE
	String_t* ___TAG_VERBOSE_5;
	// System.Action`2<System.Object,System.String[]> Byn.Awrtc.SLog::sLogger
	Action_2_tAD573E58BF9243F5E6BB71D7ACD67F0388BDEEC7* ___sLogger_6;
};

// Byn.Awrtc.SLog

// Firebase.Platform.Services
struct Services_t4EDE95322734B379A0DA3AAEF079E28E9132D69E_StaticFields
{
	// Firebase.Platform.IAppConfigExtensions Firebase.Platform.Services::<AppConfig>k__BackingField
	RuntimeObject* ___U3CAppConfigU3Ek__BackingField_0;
	// Firebase.Platform.IClockService Firebase.Platform.Services::<Clock>k__BackingField
	RuntimeObject* ___U3CClockU3Ek__BackingField_1;
	// Firebase.Platform.ILoggingService Firebase.Platform.Services::<Logging>k__BackingField
	RuntimeObject* ___U3CLoggingU3Ek__BackingField_2;
};

// Firebase.Platform.Services

// Firebase.Crashlytics.StackTraceParser
struct StackTraceParser_tCD308CD049C1C2B3A198DBBDB3357B628F793B7D_StaticFields
{
	// System.String Firebase.Crashlytics.StackTraceParser::FrameArgsRegex
	String_t* ___FrameArgsRegex_0;
	// System.String Firebase.Crashlytics.StackTraceParser::FrameRegexWithoutFileInfo
	String_t* ___FrameRegexWithoutFileInfo_1;
	// System.String Firebase.Crashlytics.StackTraceParser::FrameRegexWithFileInfo
	String_t* ___FrameRegexWithFileInfo_2;
	// System.String Firebase.Crashlytics.StackTraceParser::MonoFilenameUnknownString
	String_t* ___MonoFilenameUnknownString_3;
	// System.String[] Firebase.Crashlytics.StackTraceParser::StringDelimiters
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___StringDelimiters_4;
};

// Firebase.Crashlytics.StackTraceParser

// UnityEngine.SubsystemDescriptor

// UnityEngine.SubsystemDescriptor

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore
struct SubsystemDescriptorStore_tEF3761B84B8C25EA4B93F94A487551820B268250_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.IntegratedSubsystemDescriptor> UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore::s_IntegratedDescriptors
	List_1_tACFC79734710927A89702FFC38900223BB85B5A6* ___s_IntegratedDescriptors_0;
	// System.Collections.Generic.List`1<UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider> UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore::s_StandaloneDescriptors
	List_1_t2D19D6F759F401FE6C5460698E5B8249E470E044* ___s_StandaloneDescriptors_1;
	// System.Collections.Generic.List`1<UnityEngine.SubsystemDescriptor> UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore::s_DeprecatedDescriptors
	List_1_t15AD773D34D3739AFB67421B6DFFACEA7638F64E* ___s_DeprecatedDescriptors_2;
};

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorStore

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider

// UnityEngine.SubsystemsImplementation.SubsystemDescriptorWithProvider

// UnityEngine.SubsystemManager
struct SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields
{
	// System.Action UnityEngine.SubsystemManager::reloadSubsytemsStarted
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___reloadSubsytemsStarted_0;
	// System.Action UnityEngine.SubsystemManager::reloadSubsytemsCompleted
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___reloadSubsytemsCompleted_1;
	// System.Action UnityEngine.SubsystemManager::beforeReloadSubsystems
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___beforeReloadSubsystems_2;
	// System.Action UnityEngine.SubsystemManager::afterReloadSubsystems
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___afterReloadSubsystems_3;
	// System.Collections.Generic.List`1<UnityEngine.IntegratedSubsystem> UnityEngine.SubsystemManager::s_IntegratedSubsystems
	List_1_t78E7232867D713AA9907E71F6C5B19B226F0B180* ___s_IntegratedSubsystems_4;
	// System.Collections.Generic.List`1<UnityEngine.SubsystemsImplementation.SubsystemWithProvider> UnityEngine.SubsystemManager::s_StandaloneSubsystems
	List_1_tD834E8FB7FDC0D4243FBCF922D7FE4E3C707AAC3* ___s_StandaloneSubsystems_5;
	// System.Collections.Generic.List`1<UnityEngine.Subsystem> UnityEngine.SubsystemManager::s_DeprecatedSubsystems
	List_1_t9E8CCD70A25458CE30A64503B35F06ECA62E3052* ___s_DeprecatedSubsystems_6;
};

// UnityEngine.SubsystemManager

// UnityEngine.SubsystemsImplementation.SubsystemProvider

// UnityEngine.SubsystemsImplementation.SubsystemProvider

// UnityEngine.SubsystemsImplementation.SubsystemWithProvider

// UnityEngine.SubsystemsImplementation.SubsystemWithProvider

// System.Threading.SynchronizationContext
struct SynchronizationContext_tCDB842BBE53B050802CBBB59C6E6DC45B5B06DC0_StaticFields
{
	// System.Type System.Threading.SynchronizationContext::s_cachedPreparedType1
	Type_t* ___s_cachedPreparedType1_1;
	// System.Type System.Threading.SynchronizationContext::s_cachedPreparedType2
	Type_t* ___s_cachedPreparedType2_2;
	// System.Type System.Threading.SynchronizationContext::s_cachedPreparedType3
	Type_t* ___s_cachedPreparedType3_3;
	// System.Type System.Threading.SynchronizationContext::s_cachedPreparedType4
	Type_t* ___s_cachedPreparedType4_4;
	// System.Type System.Threading.SynchronizationContext::s_cachedPreparedType5
	Type_t* ___s_cachedPreparedType5_5;
};

// System.Threading.SynchronizationContext

// Firebase.Platform.Default.SystemClock
struct SystemClock_t56839C5A7890CD3F89A336EE0173FAA84AB3FA13_StaticFields
{
	// Firebase.Platform.IClockService Firebase.Platform.Default.SystemClock::Instance
	RuntimeObject* ___Instance_0;
};

// Firebase.Platform.Default.SystemClock

// TriLibCore.ThreeMf.ThreeMfMaterial

// TriLibCore.ThreeMf.ThreeMfMaterial

// TriLibCore.ThreeMf.ThreeMfProcessor

// TriLibCore.ThreeMf.ThreeMfProcessor

// UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription
struct TrackedPoseDriverDataDescription_tDD80D37EB4F938007BCE0FAB9705032594E4D9FF_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData> UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription::DeviceData
	List_1_tA8C10B8A593896280D68A4B006217C43F236DE0D* ___DeviceData_0;
};

// UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative
struct UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields
{
	// System.Action UnityEngine.UIElements.UIElementsRuntimeUtilityNative::RepaintOverlayPanelsCallback
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___RepaintOverlayPanelsCallback_0;
	// System.Action UnityEngine.UIElements.UIElementsRuntimeUtilityNative::UpdateRuntimePanelsCallback
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___UpdateRuntimePanelsCallback_1;
};

// UnityEngine.UIElements.UIElementsRuntimeUtilityNative

// System.Text.Unicode.UnicodeHelpers
struct UnicodeHelpers_t5A5A5BBFB4BD837753ED0CAC899C31F43C701F71_StaticFields
{
	// System.UInt32[] System.Text.Unicode.UnicodeHelpers::_definedCharacterBitmapBigEndian
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ____definedCharacterBitmapBigEndian_0;
};

// System.Text.Unicode.UnicodeHelpers

// System.Text.Unicode.UnicodeRange

// System.Text.Unicode.UnicodeRange

// System.Text.Unicode.UnicodeRanges
struct UnicodeRanges_tE4E52421F705A0AC65C0E17C05CA14DA03E037C7_StaticFields
{
	// System.Text.Unicode.UnicodeRange System.Text.Unicode.UnicodeRanges::_u0000
	UnicodeRange_t39A4F977F558B4F00AFE470AD6573FA988BDA836* ____u0000_0;
};

// System.Text.Unicode.UnicodeRanges

// Firebase.Unity.UnityLoggingService
struct UnityLoggingService_t6BE39D9F1F1EFE495EB479964B33753AA2C87CBB_StaticFields
{
	// Firebase.Unity.UnityLoggingService Firebase.Unity.UnityLoggingService::_instance
	UnityLoggingService_t6BE39D9F1F1EFE495EB479964B33753AA2C87CBB* ____instance_0;
};

// Firebase.Unity.UnityLoggingService

// System.ValueType

// System.ValueType

// UnityEngine.WWWForm
struct WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields
{
	// System.Byte[] UnityEngine.WWWForm::dDash
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dDash_6;
	// System.Byte[] UnityEngine.WWWForm::crlf
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___crlf_7;
	// System.Byte[] UnityEngine.WWWForm::contentTypeHeader
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___contentTypeHeader_8;
	// System.Byte[] UnityEngine.WWWForm::dispositionHeader
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dispositionHeader_9;
	// System.Byte[] UnityEngine.WWWForm::endQuote
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___endQuote_10;
	// System.Byte[] UnityEngine.WWWForm::fileNameField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___fileNameField_11;
	// System.Byte[] UnityEngine.WWWForm::ampersand
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___ampersand_12;
	// System.Byte[] UnityEngine.WWWForm::equal
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___equal_13;
};

// UnityEngine.WWWForm

// UnityEngine.WWWTranscoder
struct WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields
{
	// System.Byte[] UnityEngine.WWWTranscoder::ucHexChars
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___ucHexChars_0;
	// System.Byte[] UnityEngine.WWWTranscoder::lcHexChars
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___lcHexChars_1;
	// System.Byte UnityEngine.WWWTranscoder::urlEscapeChar
	uint8_t ___urlEscapeChar_2;
	// System.Byte[] UnityEngine.WWWTranscoder::urlSpace
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___urlSpace_3;
	// System.Byte[] UnityEngine.WWWTranscoder::dataSpace
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dataSpace_4;
	// System.Byte[] UnityEngine.WWWTranscoder::urlForbidden
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___urlForbidden_5;
	// System.Byte UnityEngine.WWWTranscoder::qpEscapeChar
	uint8_t ___qpEscapeChar_6;
	// System.Byte[] UnityEngine.WWWTranscoder::qpSpace
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___qpSpace_7;
	// System.Byte[] UnityEngine.WWWTranscoder::qpForbidden
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___qpForbidden_8;
};

// UnityEngine.WWWTranscoder

// UnityEngineInternal.WebRequestUtils
struct WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443_StaticFields
{
	// System.Text.RegularExpressions.Regex UnityEngineInternal.WebRequestUtils::domainRegex
	Regex_tE773142C2BE45C5D362B0F815AFF831707A51772* ___domainRegex_0;
};

// UnityEngineInternal.WebRequestUtils

// UnityEngine.XR.XRDevice
struct XRDevice_tD076A68EFE413B3EEEEA362BE0364A488B58F194_StaticFields
{
	// System.Action`1<System.String> UnityEngine.XR.XRDevice::deviceLoaded
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___deviceLoaded_0;
};

// UnityEngine.XR.XRDevice

// UnityEngine.XR.Management.XRManagementAnalytics
struct XRManagementAnalytics_t9A6F8603FBB618B8FEC7A6BF3FD9615C61C1063F_StaticFields
{
	// System.Boolean UnityEngine.XR.Management.XRManagementAnalytics::s_Initialized
	bool ___s_Initialized_4;
};

// UnityEngine.XR.Management.XRManagementAnalytics

// UnityEngine.YieldInstruction

// UnityEngine.YieldInstruction

// Firebase.Crashlytics.AndroidImpl/<Log>c__AnonStorey0

// Firebase.Crashlytics.AndroidImpl/<Log>c__AnonStorey0

// Firebase.Crashlytics.AndroidImpl/<LogException>c__AnonStorey3

// Firebase.Crashlytics.AndroidImpl/<LogException>c__AnonStorey3

// Firebase.Crashlytics.AndroidImpl/<SetCrashlyticsCollectionEnabled>c__AnonStorey4

// Firebase.Crashlytics.AndroidImpl/<SetCrashlyticsCollectionEnabled>c__AnonStorey4

// Firebase.Crashlytics.AndroidImpl/<SetCustomKey>c__AnonStorey1

// Firebase.Crashlytics.AndroidImpl/<SetCustomKey>c__AnonStorey1

// Firebase.Crashlytics.AndroidImpl/<SetUserId>c__AnonStorey2

// Firebase.Crashlytics.AndroidImpl/<SetUserId>c__AnonStorey2

// UnityEngine.Animation/Enumerator

// UnityEngine.Animation/Enumerator

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c
struct U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields
{
	// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::<>9
	U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44* ___U3CU3E9_0;
	// System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::<>9__21_0
	Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* ___U3CU3E9__21_0_1;
	// System.Func`2<ImaginationOverflow.UniversalDeepLinking.PlatformLinkingConfiguration,System.Collections.Generic.List`1<ImaginationOverflow.UniversalDeepLinking.LinkInformation>> ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c::<>9__22_0
	Func_2_tA0A0103EAE30ADE84BA15C13251359AA5DF2E75E* ___U3CU3E9__22_0_2;
};

// ImaginationOverflow.UniversalDeepLinking.AppLinkingConfiguration/<>c

// UnityEngine.AudioSettings/Mobile
struct Mobile_t304A73480DF447472BDB16BA19A9E4FE2C8CB2DD_StaticFields
{
	// System.Boolean UnityEngine.AudioSettings/Mobile::<muteState>k__BackingField
	bool ___U3CmuteStateU3Ek__BackingField_0;
	// System.Boolean UnityEngine.AudioSettings/Mobile::_stopAudioOutputOnMute
	bool ____stopAudioOutputOnMute_1;
	// System.Action`1<System.Boolean> UnityEngine.AudioSettings/Mobile::OnMuteStateChanged
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___OnMuteStateChanged_2;
};

// UnityEngine.AudioSettings/Mobile

// Firebase.Crashlytics.Crashlytics/PlatformAccessor
struct PlatformAccessor_tB02E4C5B35E2A951376B828D3F51E6623A65336D_StaticFields
{
	// Firebase.Crashlytics.ExceptionHandler Firebase.Crashlytics.Crashlytics/PlatformAccessor::_exceptionHandler
	ExceptionHandler_t2DDC3721AFC96975EA180F4E7A4902FFB2CE138B* ____exceptionHandler_0;
	// Firebase.Crashlytics.Impl Firebase.Crashlytics.Crashlytics/PlatformAccessor::_impl
	Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF* ____impl_1;
	// Firebase.FirebaseApp Firebase.Crashlytics.Crashlytics/PlatformAccessor::_app
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ____app_2;
};

// Firebase.Crashlytics.Crashlytics/PlatformAccessor

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields
{
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::applicationDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___applicationDelegate_0;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::arithmeticDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___arithmeticDelegate_1;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::divideByZeroDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___divideByZeroDelegate_2;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::indexOutOfRangeDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___indexOutOfRangeDelegate_3;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::invalidCastDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___invalidCastDelegate_4;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::invalidOperationDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___invalidOperationDelegate_5;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::ioDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___ioDelegate_6;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::nullReferenceDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___nullReferenceDelegate_7;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::outOfMemoryDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___outOfMemoryDelegate_8;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::overflowDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___overflowDelegate_9;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::systemDelegate
	ExceptionDelegate_tA0B6FF90773F94E8F4ADB26E59C022EA9F4793BB* ___systemDelegate_10;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::argumentDelegate
	ExceptionArgumentDelegate_tE99B0B8FC5C781502FEC342E761FC29F4DB986B8* ___argumentDelegate_11;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::argumentNullDelegate
	ExceptionArgumentDelegate_tE99B0B8FC5C781502FEC342E761FC29F4DB986B8* ___argumentNullDelegate_12;
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper::argumentOutOfRangeDelegate
	ExceptionArgumentDelegate_tE99B0B8FC5C781502FEC342E761FC29F4DB986B8* ___argumentOutOfRangeDelegate_13;
};

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGExceptionHelper

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGPendingException
struct SWIGPendingException_t9C3365ED3C047C6935279BA7C05F89F3BDE54E35_StaticFields
{
	// System.Int32 Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGPendingException::numExceptionsPending
	int32_t ___numExceptionsPending_1;
};

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGPendingException
struct SWIGPendingException_t9C3365ED3C047C6935279BA7C05F89F3BDE54E35_ThreadStaticFields
{
	// System.Exception Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGPendingException::pendingException
	Exception_t* ___pendingException_0;
};

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGStringHelper
struct SWIGStringHelper_tA8CF9EF78BD75BC8C5CB597CC99E74764BFE361B_StaticFields
{
	// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGStringHelper::stringDelegate
	SWIGStringDelegate_t37770D7DA2368AE963BFF83DE2B3FC28CC710959* ___stringDelegate_0;
};

// Firebase.Crashlytics.CrashlyticsInternalPINVOKE/SWIGStringHelper

// Firebase.Platform.FirebaseHandler/<CreatePartialOnMainThread>c__AnonStorey0

// Firebase.Platform.FirebaseHandler/<CreatePartialOnMainThread>c__AnonStorey0

// Firebase.Messaging.FirebaseMessaging/Listener
struct Listener_tC16E83118F05783B5CA41A3F81ABF1A2C83D7BF7_StaticFields
{
	// Firebase.Messaging.FirebaseMessaging/Listener Firebase.Messaging.FirebaseMessaging/Listener::listener
	Listener_tC16E83118F05783B5CA41A3F81ABF1A2C83D7BF7* ___listener_3;
};

// Firebase.Messaging.FirebaseMessaging/Listener

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields
{
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::applicationDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___applicationDelegate_0;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::arithmeticDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___arithmeticDelegate_1;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::divideByZeroDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___divideByZeroDelegate_2;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::indexOutOfRangeDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___indexOutOfRangeDelegate_3;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::invalidCastDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___invalidCastDelegate_4;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::invalidOperationDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___invalidOperationDelegate_5;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::ioDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___ioDelegate_6;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::nullReferenceDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___nullReferenceDelegate_7;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::outOfMemoryDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___outOfMemoryDelegate_8;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::overflowDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___overflowDelegate_9;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::systemDelegate
	ExceptionDelegate_t9CD06E85552169C8AB7F8AD922D5B550352373D5* ___systemDelegate_10;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::argumentDelegate
	ExceptionArgumentDelegate_t1B9CEFDA449A0679DCC7B832B88DB9E6864A4110* ___argumentDelegate_11;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::argumentNullDelegate
	ExceptionArgumentDelegate_t1B9CEFDA449A0679DCC7B832B88DB9E6864A4110* ___argumentNullDelegate_12;
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper::argumentOutOfRangeDelegate
	ExceptionArgumentDelegate_t1B9CEFDA449A0679DCC7B832B88DB9E6864A4110* ___argumentOutOfRangeDelegate_13;
};

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGExceptionHelper

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException
struct SWIGPendingException_t0409902F046D41D7A8D2333129C7E51E76EC1909_StaticFields
{
	// System.Int32 Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::numExceptionsPending
	int32_t ___numExceptionsPending_1;
};

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException
struct SWIGPendingException_t0409902F046D41D7A8D2333129C7E51E76EC1909_ThreadStaticFields
{
	// System.Exception Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGPendingException::pendingException
	Exception_t* ___pendingException_0;
};

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t953C1EA2870BB0BF9A25500F62877BBBA6D44AE9_StaticFields
{
	// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper/SWIGStringDelegate Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper::stringDelegate
	SWIGStringDelegate_t3DABDDBE55902B8931CDE0C135374D8CBDD52CE6* ___stringDelegate_0;
};

// Firebase.Messaging.FirebaseMessagingPINVOKE/SWIGStringHelper

// Unity.VectorGraphics.InternalBridge/<>c
struct U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3_StaticFields
{
	// Unity.VectorGraphics.InternalBridge/<>c Unity.VectorGraphics.InternalBridge/<>c::<>9
	U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3* ___U3CU3E9_0;
	// System.Func`2<Unity.VectorGraphics.InternalBridge/VectorImageVertexBridge,UnityEngine.UIElements.VectorImageVertex> Unity.VectorGraphics.InternalBridge/<>c::<>9__5_0
	Func_2_t6C07AB9155802018C389E7DDD4F2D803421EC46F* ___U3CU3E9__5_0_1;
	// System.Func`2<Unity.VectorGraphics.InternalBridge/GradientSettingsBridge,UnityEngine.UIElements.GradientSettings> Unity.VectorGraphics.InternalBridge/<>c::<>9__5_1
	Func_2_t9E8CE435999D136D29DF6074600D6F34D77E48FE* ___U3CU3E9__5_1_2;
	// System.Func`2<UnityEngine.UIElements.VectorImageVertex,UnityEngine.Vector2> Unity.VectorGraphics.InternalBridge/<>c::<>9__6_0
	Func_2_t3D0310BCABDAAEF12A5955F712B55633C6CB8B9C* ___U3CU3E9__6_0_3;
	// System.Func`2<UnityEngine.UIElements.VectorImageVertex,UnityEngine.Vector2> Unity.VectorGraphics.InternalBridge/<>c::<>9__6_1
	Func_2_t3D0310BCABDAAEF12A5955F712B55633C6CB8B9C* ___U3CU3E9__6_1_4;
	// System.Func`2<UnityEngine.UIElements.VectorImageVertex,UnityEngine.Color> Unity.VectorGraphics.InternalBridge/<>c::<>9__6_2
	Func_2_t0471F7DAEBC22E8FBF8EB17B66AA9ED6750D2237* ___U3CU3E9__6_2_5;
	// System.Func`2<UnityEngine.UIElements.VectorImageVertex,UnityEngine.Vector2> Unity.VectorGraphics.InternalBridge/<>c::<>9__6_3
	Func_2_t3D0310BCABDAAEF12A5955F712B55633C6CB8B9C* ___U3CU3E9__6_3_6;
	// System.Func`2<UnityEngine.UIElements.GradientSettings,Unity.VectorGraphics.InternalBridge/GradientSettingsBridge> Unity.VectorGraphics.InternalBridge/<>c::<>9__6_4
	Func_2_t366DDDB5C5FB5478228D0B49FFF0721AEFC1D33F* ___U3CU3E9__6_4_7;
};

// Unity.VectorGraphics.InternalBridge/<>c

// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass4_0

// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass4_0

// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass5_0

// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid/<>c__DisplayClass5_0

// HDRLoader.Rgbe/Header

// HDRLoader.Rgbe/Header

// Firebase.Crashlytics.StackFrames/StackFramesEnumerator

// Firebase.Crashlytics.StackFrames/StackFramesEnumerator

// TriLibCore.ThreeMf.ThreeMfProcessor/<>c
struct U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields
{
	// TriLibCore.ThreeMf.ThreeMfProcessor/<>c TriLibCore.ThreeMf.ThreeMfProcessor/<>c::<>9
	U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138* ___U3CU3E9_0;
	// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,System.Boolean> TriLibCore.ThreeMf.ThreeMfProcessor/<>c::<>9__12_0
	Func_2_t407C4C2C9129DC8C4ED64BFC1245ADF5BC54144C* ___U3CU3E9__12_0_1;
	// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,IxMilia.ThreeMf.ThreeMfBaseMaterials> TriLibCore.ThreeMf.ThreeMfProcessor/<>c::<>9__12_1
	Func_2_t4FAF9B6978DD4BEBDB6080C8A5C65A89A7CA9EC9* ___U3CU3E9__12_1_2;
	// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,System.Boolean> TriLibCore.ThreeMf.ThreeMfProcessor/<>c::<>9__12_2
	Func_2_t407C4C2C9129DC8C4ED64BFC1245ADF5BC54144C* ___U3CU3E9__12_2_3;
	// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,IxMilia.ThreeMf.ThreeMfColorGroup> TriLibCore.ThreeMf.ThreeMfProcessor/<>c::<>9__12_3
	Func_2_t3888261809E6D9256EF59C505C2E2E5974D12D88* ___U3CU3E9__12_3_4;
	// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,System.Boolean> TriLibCore.ThreeMf.ThreeMfProcessor/<>c::<>9__12_4
	Func_2_t407C4C2C9129DC8C4ED64BFC1245ADF5BC54144C* ___U3CU3E9__12_4_5;
	// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,IxMilia.ThreeMf.ThreeMfTexture2D> TriLibCore.ThreeMf.ThreeMfProcessor/<>c::<>9__12_5
	Func_2_tDC2B856640B426865EF8F8A06D21921AC4CF86AE* ___U3CU3E9__12_5_6;
	// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,System.Boolean> TriLibCore.ThreeMf.ThreeMfProcessor/<>c::<>9__12_6
	Func_2_t407C4C2C9129DC8C4ED64BFC1245ADF5BC54144C* ___U3CU3E9__12_6_7;
	// System.Func`2<IxMilia.ThreeMf.ThreeMfResource,IxMilia.ThreeMf.ThreeMfTexture2DGroup> TriLibCore.ThreeMf.ThreeMfProcessor/<>c::<>9__12_7
	Func_2_tDAD1F04679E83654859656F7E338DB1304EA2FE7* ___U3CU3E9__12_7_8;
};

// TriLibCore.ThreeMf.ThreeMfProcessor/<>c

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey3

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey3

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey4

// Firebase.Unity.UnitySynchronizationContext/<Send>c__AnonStorey4

// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1

// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1

// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2

// Firebase.Unity.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2

// Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0

// Firebase.Unity.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4

// Firebase.Messaging.FirebaseMessaging/Listener/<TokenReceivedDelegateMethod>c__AnonStorey1

// Firebase.Messaging.FirebaseMessaging/Listener/<TokenReceivedDelegateMethod>c__AnonStorey1

// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0

// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.Management.XRLoader>

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.Management.XRLoader>

// UnityEngine.XR.InputFeatureUsage`1<System.Boolean>

// UnityEngine.XR.InputFeatureUsage`1<System.Boolean>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.Eyes>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.Eyes>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.Hand>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.Hand>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.InputTrackingState>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.InputTrackingState>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion>

// UnityEngine.XR.InputFeatureUsage`1<System.Single>

// UnityEngine.XR.InputFeatureUsage`1<System.Single>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3>

// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3>

// Unity.Collections.NativeArray`1<System.Byte>

// Unity.Collections.NativeArray`1<System.Byte>

// Unity.Collections.NativeArray`1<System.Char>

// Unity.Collections.NativeArray`1<System.Char>

// System.Nullable`1<System.Double>

// System.Nullable`1<System.Double>

// System.Text.Internal.AllowedCharactersBitmap

// System.Text.Internal.AllowedCharactersBitmap

// Firebase.Crashlytics.AndroidImpl

// Firebase.Crashlytics.AndroidImpl

// UnityEngine.AnimatorClipInfo

// UnityEngine.AnimatorClipInfo

// UnityEngine.AnimatorStateInfo

// UnityEngine.AnimatorStateInfo

// UnityEngine.AnimatorTransitionInfo

// UnityEngine.AnimatorTransitionInfo

// UnityEngine.AssetFileNameExtensionAttribute

// UnityEngine.AssetFileNameExtensionAttribute

// UnityEngine.XR.Bone

// UnityEngine.XR.Bone

// UnityEngine.Color

// UnityEngine.Color

// UnityEngine.Color32

// UnityEngine.Color32

// Byn.Awrtc.ConnectionId
struct ConnectionId_tEC0FFB08249CB4253D31AF1E9166778F5D22E2F7_StaticFields
{
	// Byn.Awrtc.ConnectionId Byn.Awrtc.ConnectionId::INVALID
	ConnectionId_tEC0FFB08249CB4253D31AF1E9166778F5D22E2F7 ___INVALID_0;
};

// Byn.Awrtc.ConnectionId

// System.Runtime.Serialization.DataContractAttribute

// System.Runtime.Serialization.DataContractAttribute

// System.Runtime.Serialization.DataMemberAttribute

// System.Runtime.Serialization.DataMemberAttribute

// System.DateTime
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_StaticFields
{
	// System.Int32[] System.DateTime::s_daysToMonth365
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth365_30;
	// System.Int32[] System.DateTime::s_daysToMonth366
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth366_31;
	// System.DateTime System.DateTime::MinValue
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MinValue_32;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MaxValue_33;
	// System.DateTime System.DateTime::UnixEpoch
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___UnixEpoch_34;
};

// System.DateTime

// UnityEngine.UIElements.UIR.DrawBufferRange

// UnityEngine.UIElements.UIR.DrawBufferRange

// System.Runtime.Serialization.EnumMemberAttribute

// System.Runtime.Serialization.EnumMemberAttribute

// UnityEngine.XR.Eyes

// UnityEngine.XR.Eyes

// UnityEngine.TextCore.FaceInfo

// UnityEngine.TextCore.FaceInfo

// UnityEngine.TextCore.LowLevel.FontReference

// UnityEngine.TextCore.LowLevel.FontReference

// UnityEngine.TextCore.GlyphMetrics

// UnityEngine.TextCore.GlyphMetrics

// UnityEngine.TextCore.GlyphRect
struct GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D_StaticFields
{
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.GlyphRect::s_ZeroGlyphRect
	GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D ___s_ZeroGlyphRect_4;
};

// UnityEngine.TextCore.GlyphRect

// UnityEngine.TextCore.LowLevel.GlyphValueRecord

// UnityEngine.TextCore.LowLevel.GlyphValueRecord

// System.Guid
struct Guid_t_StaticFields
{
	// System.Guid System.Guid::Empty
	Guid_t ___Empty_0;
};

// System.Guid

// UnityEngine.XR.Hand

// UnityEngine.XR.Hand

// UnityEngine.HumanDescription

// UnityEngine.HumanDescription

// UnityEngine.Bindings.IgnoreAttribute

// UnityEngine.Bindings.IgnoreAttribute

// UnityEngine.XR.InputDevice
struct InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystem> UnityEngine.XR.InputDevice::s_InputSubsystemCache
	List_1_t90832B88D7207769654164CC28440CF594CC397D* ___s_InputSubsystemCache_0;
};

// UnityEngine.XR.InputDevice

// UnityEngine.XR.InputFeatureUsage

// UnityEngine.XR.InputFeatureUsage

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// UnityEngine.LayerMask

// UnityEngine.LayerMask

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// UnityEngine.Matrix4x4

// UnityEngine.XR.MeshId
struct MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC_StaticFields
{
	// UnityEngine.XR.MeshId UnityEngine.XR.MeshId::s_InvalidId
	MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC ___s_InvalidId_0;
};

// UnityEngine.XR.MeshId

// Firebase.Messaging.MessageReceivedEventArgs

// Firebase.Messaging.MessageReceivedEventArgs

// UnityEngine.NativeClassAttribute

// UnityEngine.NativeClassAttribute

// UnityEngine.Bindings.NativeConditionalAttribute

// UnityEngine.Bindings.NativeConditionalAttribute

// UnityEngine.Bindings.NativeHeaderAttribute

// UnityEngine.Bindings.NativeHeaderAttribute

// UnityEngineInternal.Input.NativeInputEvent

// UnityEngineInternal.Input.NativeInputEvent

// UnityEngineInternal.Input.NativeInputEventBuffer

// UnityEngineInternal.Input.NativeInputEventBuffer

// UnityEngine.Bindings.NativeMethodAttribute

// UnityEngine.Bindings.NativeMethodAttribute

// UnityEngine.Bindings.NativeNameAttribute

// UnityEngine.Bindings.NativeNameAttribute

// UnityEngine.Bindings.NativeThrowsAttribute

// UnityEngine.Bindings.NativeThrowsAttribute

// UnityEngine.Bindings.NativeTypeAttribute

// UnityEngine.Bindings.NativeTypeAttribute

// UnityEngine.Bindings.NativeWritableSelfAttribute

// UnityEngine.Bindings.NativeWritableSelfAttribute

// UnityEngine.Bindings.NotNullAttribute

// UnityEngine.Bindings.NotNullAttribute

// System.Runtime.CompilerServices.NullableAttribute

// System.Runtime.CompilerServices.NullableAttribute

// System.Runtime.CompilerServices.NullableContextAttribute

// System.Runtime.CompilerServices.NullableContextAttribute

// System.Runtime.CompilerServices.NullablePublicOnlyAttribute

// System.Runtime.CompilerServices.NullablePublicOnlyAttribute

// TriLibCore.Obj.ObjGeometry

// TriLibCore.Obj.ObjGeometry

// TriLibCore.Obj.Reader.ObjReader
struct ObjReader_t526AA9305FE5DDF1EF48E7B3D4DBC0FFF1056BEB_StaticFields
{
	// System.Boolean TriLibCore.Obj.Reader.ObjReader::ParseNumbersAsDouble
	bool ___ParseNumbersAsDouble_10;
};

// TriLibCore.Obj.Reader.ObjReader

// UnityEngine.PhysicsScene

// UnityEngine.PhysicsScene

// UnityEngine.PhysicsScene2D

// UnityEngine.PhysicsScene2D

// TriLibCore.Ply.PlyListProperty

// TriLibCore.Ply.PlyListProperty

// TriLibCore.Ply.PlyValue
struct PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41_StaticFields
{
	// TriLibCore.Ply.PlyValue TriLibCore.Ply.PlyValue::Unknown
	PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41 ___Unknown_1;
};

// TriLibCore.Ply.PlyValue

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Quaternion

// UnityEngine.RectInt

// UnityEngine.RectInt

// UnityEngine.RenderTextureDescriptor

// UnityEngine.RenderTextureDescriptor

// UnityEngine.Scripting.RequiredByNativeCodeAttribute

// UnityEngine.Scripting.RequiredByNativeCodeAttribute

// UnityEngine.Bindings.StaticAccessorAttribute

// UnityEngine.Bindings.StaticAccessorAttribute

// TriLibCore.Stl.Reader.StlReader
struct StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields
{
	// System.Boolean TriLibCore.Stl.Reader.StlReader::FixInfacingNormals
	bool ___FixInfacingNormals_10;
	// System.Boolean TriLibCore.Stl.Reader.StlReader::LoadWithYUp
	bool ___LoadWithYUp_11;
	// System.Boolean TriLibCore.Stl.Reader.StlReader::StoreTriangleIndexInTexCoord0
	bool ___StoreTriangleIndexInTexCoord0_12;
	// System.Boolean TriLibCore.Stl.Reader.StlReader::ImportNormals
	bool ___ImportNormals_13;
};

// TriLibCore.Stl.Reader.StlReader

// System.IO.TextReader
struct TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7_StaticFields
{
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_tB8D43017CB6BE1633E5A86D64E7757366507C1F7* ___Null_1;
};

// System.IO.TextReader

// TriLibCore.ThreeMf.ThreeMfGeometry

// TriLibCore.ThreeMf.ThreeMfGeometry

// Firebase.Messaging.TokenReceivedEventArgs

// Firebase.Messaging.TokenReceivedEventArgs

// UnityEngine.UILineInfo

// UnityEngine.UILineInfo

// System.UIntPtr
struct UIntPtr_t_StaticFields
{
	// System.UIntPtr System.UIntPtr::Zero
	uintptr_t ___Zero_0;
};

// System.UIntPtr

// Firebase.Platform.Default.UnityConfigExtensions
struct UnityConfigExtensions_tA9B2A38D4A5718276B9A2E776C138969F0575152_StaticFields
{
	// Firebase.Platform.Default.UnityConfigExtensions Firebase.Platform.Default.UnityConfigExtensions::_instance
	UnityConfigExtensions_tA9B2A38D4A5718276B9A2E776C138969F0575152* ____instance_5;
	// System.Func`1<System.String> Firebase.Platform.Default.UnityConfigExtensions::<>f__am$cache0
	Func_1_t367387BB2C476D3F32DB12161B5FDC128DC3231C* ___U3CU3Ef__amU24cache0_6;
	// System.Func`1<System.String> Firebase.Platform.Default.UnityConfigExtensions::<>f__am$cache1
	Func_1_t367387BB2C476D3F32DB12161B5FDC128DC3231C* ___U3CU3Ef__amU24cache1_7;
};

// Firebase.Platform.Default.UnityConfigExtensions

// Firebase.Unity.UnitySynchronizationContext
struct UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690_StaticFields
{
	// Firebase.Unity.UnitySynchronizationContext Firebase.Unity.UnitySynchronizationContext::_instance
	UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690* ____instance_6;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.ManualResetEvent> Firebase.Unity.UnitySynchronizationContext::signalDictionary
	Dictionary_2_t685E1072D6BF5D03945108D75216A223A8EA7FBB* ___signalDictionary_11;
};

// Firebase.Unity.UnitySynchronizationContext

// UnityEngine.Scripting.UsedByNativeCodeAttribute

// UnityEngine.Scripting.UsedByNativeCodeAttribute

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// UnityEngine.Vector4

// UnityEngine.WWW

// UnityEngine.WWW

// UnityEngine.WebCamDevice

// UnityEngine.WebCamDevice

// UnityEngine.XR.Management.XRConfigurationDataAttribute

// UnityEngine.XR.Management.XRConfigurationDataAttribute

// UnityEngine.Yoga.YogaSize

// UnityEngine.Yoga.YogaSize

// UnityEngine.Yoga.YogaValue

// UnityEngine.Yoga.YogaValue

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=8192

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=8192

// <PrivateImplementationDetails>{f1ff318e-91e6-436a-9a52-ac02be4f7d0e}/$ArrayType=20

// <PrivateImplementationDetails>{f1ff318e-91e6-436a-9a52-ac02be4f7d0e}/$ArrayType=20

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoAttribute

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoMesh

// Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs

// Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs

// NativeGallery/ImageProperties

// NativeGallery/ImageProperties

// NativeGallery/VideoProperties

// NativeGallery/VideoProperties

// UnityEngine.ParticleSystem/MainModule

// UnityEngine.ParticleSystem/MainModule

// UnityEngine.ParticleSystem/MinMaxCurve

// UnityEngine.ParticleSystem/MinMaxCurve

// UnityEngine.SendMouseEvents/HitInfo

// UnityEngine.SendMouseEvents/HitInfo

// UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData

// UnityEngine.SpatialTracking.TrackedPoseDriverDataDescription/PoseData

// UnityEngine.XR.Management.XRManagementAnalytics/BuildEvent

// UnityEngine.XR.Management.XRManagementAnalytics/BuildEvent

// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer

// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData/<Data>e__FixedBuffer

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t6EB70CB763ED0F38063A7F9D39E7E5A0EDFBBB26_StaticFields
{
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=128 <PrivateImplementationDetails>::20731A0997122AAAE93B2C58C609D52BF080F0A1
	__StaticArrayInitTypeSizeU3D128_tE7EEBFDBD77CAEE63DB6AFD4F5162D364EF66602 ___20731A0997122AAAE93B2C58C609D52BF080F0A1_0;
};

// <PrivateImplementationDetails>

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t03A72430945148CB9D382B7B249DAE2DDF6F2843_StaticFields
{
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=8192 <PrivateImplementationDetails>::8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6
	__StaticArrayInitTypeSizeU3D8192_t6B42A11F775F676F25B0A63261E746DABAF39CB5 ___8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=256 <PrivateImplementationDetails>::EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E
	__StaticArrayInitTypeSizeU3D256_t801718944F76C36638EA43F29012837CBA710948 ___EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1;
};

// <PrivateImplementationDetails>

// <PrivateImplementationDetails>{f1ff318e-91e6-436a-9a52-ac02be4f7d0e}
struct U3CPrivateImplementationDetailsU3EU7Bf1ff318eU2D91e6U2D436aU2D9a52U2Dac02be4f7d0eU7D_t54B8FF32EAD70318A421BB5FB5ECE4E1E50CE79C_StaticFields
{
	// <PrivateImplementationDetails>{f1ff318e-91e6-436a-9a52-ac02be4f7d0e}/$ArrayType=20 <PrivateImplementationDetails>{f1ff318e-91e6-436a-9a52-ac02be4f7d0e}::$field-0
	U24ArrayTypeU3D20_tE46FE4B62B2712F036B53CB84CBD53BFB6EEC1A8 ___U24fieldU2D0_0;
};

// <PrivateImplementationDetails>{f1ff318e-91e6-436a-9a52-ac02be4f7d0e}

// UnityEngine.AndroidJavaProxy
struct AndroidJavaProxy_tE5521F9761F7B95444B9C39FB15FDFC23F80A78D_StaticFields
{
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaProxy::s_JavaLangSystemClass
	GlobalJavaObjectRef_t20D8E5AAFC2EB2518FCABBF40465855E797FF0D8* ___s_JavaLangSystemClass_2;
	// System.IntPtr UnityEngine.AndroidJavaProxy::s_HashCodeMethodID
	intptr_t ___s_HashCodeMethodID_3;
};

// UnityEngine.AndroidJavaProxy

// UnityEngine.Animations.AnimationHumanStream

// UnityEngine.Animations.AnimationHumanStream

// UnityEngine.Animations.AnimationStream

// UnityEngine.Animations.AnimationStream

// UnityEngine.AsyncOperation

// UnityEngine.AsyncOperation

// UnityEngine.Networking.CertificateHandler

// UnityEngine.Networking.CertificateHandler

// UnityEngine.Collision

// UnityEngine.Collision

// UnityEngine.Collision2D

// UnityEngine.Collision2D

// UnityEngine.XR.CommonUsages
struct CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields
{
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::isTracked
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___isTracked_0;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::primaryButton
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___primaryButton_1;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::primaryTouch
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___primaryTouch_2;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::secondaryButton
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___secondaryButton_3;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::secondaryTouch
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___secondaryTouch_4;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::gripButton
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___gripButton_5;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::triggerButton
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___triggerButton_6;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::menuButton
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___menuButton_7;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::primary2DAxisClick
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___primary2DAxisClick_8;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::primary2DAxisTouch
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___primary2DAxisTouch_9;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::secondary2DAxisClick
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___secondary2DAxisClick_10;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::secondary2DAxisTouch
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___secondary2DAxisTouch_11;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::userPresence
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___userPresence_12;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.InputTrackingState> UnityEngine.XR.CommonUsages::trackingState
	InputFeatureUsage_1_t4EF7DDCAC35EE23BA72694AC2AB76CF4A879FFD9 ___trackingState_13;
	// UnityEngine.XR.InputFeatureUsage`1<System.Single> UnityEngine.XR.CommonUsages::batteryLevel
	InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 ___batteryLevel_14;
	// UnityEngine.XR.InputFeatureUsage`1<System.Single> UnityEngine.XR.CommonUsages::trigger
	InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 ___trigger_15;
	// UnityEngine.XR.InputFeatureUsage`1<System.Single> UnityEngine.XR.CommonUsages::grip
	InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 ___grip_16;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2> UnityEngine.XR.CommonUsages::primary2DAxis
	InputFeatureUsage_1_tEB160A05BCDCCA4F96072CBA0866498D06B9A27C ___primary2DAxis_17;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2> UnityEngine.XR.CommonUsages::secondary2DAxis
	InputFeatureUsage_1_tEB160A05BCDCCA4F96072CBA0866498D06B9A27C ___secondary2DAxis_18;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::devicePosition
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___devicePosition_19;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::leftEyePosition
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___leftEyePosition_20;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::rightEyePosition
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___rightEyePosition_21;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::centerEyePosition
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___centerEyePosition_22;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::colorCameraPosition
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___colorCameraPosition_23;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::deviceVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___deviceVelocity_24;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::deviceAngularVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___deviceAngularVelocity_25;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::leftEyeVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___leftEyeVelocity_26;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::leftEyeAngularVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___leftEyeAngularVelocity_27;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::rightEyeVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___rightEyeVelocity_28;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::rightEyeAngularVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___rightEyeAngularVelocity_29;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::centerEyeVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___centerEyeVelocity_30;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::centerEyeAngularVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___centerEyeAngularVelocity_31;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::colorCameraVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___colorCameraVelocity_32;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::colorCameraAngularVelocity
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___colorCameraAngularVelocity_33;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::deviceAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___deviceAcceleration_34;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::deviceAngularAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___deviceAngularAcceleration_35;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::leftEyeAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___leftEyeAcceleration_36;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::leftEyeAngularAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___leftEyeAngularAcceleration_37;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::rightEyeAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___rightEyeAcceleration_38;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::rightEyeAngularAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___rightEyeAngularAcceleration_39;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::centerEyeAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___centerEyeAcceleration_40;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::centerEyeAngularAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___centerEyeAngularAcceleration_41;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::colorCameraAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___colorCameraAcceleration_42;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector3> UnityEngine.XR.CommonUsages::colorCameraAngularAcceleration
	InputFeatureUsage_1_t2E901FA41650EB29399194768CAA93D477CEBC58 ___colorCameraAngularAcceleration_43;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion> UnityEngine.XR.CommonUsages::deviceRotation
	InputFeatureUsage_1_t8489CEC68B1EC178F2634079A9D7CD9E90D3CF5D ___deviceRotation_44;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion> UnityEngine.XR.CommonUsages::leftEyeRotation
	InputFeatureUsage_1_t8489CEC68B1EC178F2634079A9D7CD9E90D3CF5D ___leftEyeRotation_45;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion> UnityEngine.XR.CommonUsages::rightEyeRotation
	InputFeatureUsage_1_t8489CEC68B1EC178F2634079A9D7CD9E90D3CF5D ___rightEyeRotation_46;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion> UnityEngine.XR.CommonUsages::centerEyeRotation
	InputFeatureUsage_1_t8489CEC68B1EC178F2634079A9D7CD9E90D3CF5D ___centerEyeRotation_47;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Quaternion> UnityEngine.XR.CommonUsages::colorCameraRotation
	InputFeatureUsage_1_t8489CEC68B1EC178F2634079A9D7CD9E90D3CF5D ___colorCameraRotation_48;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.Hand> UnityEngine.XR.CommonUsages::handData
	InputFeatureUsage_1_t64C1AA42D6E8BD57C54C7E891BD79A70A0F3A170 ___handData_49;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.XR.Eyes> UnityEngine.XR.CommonUsages::eyesData
	InputFeatureUsage_1_tD3FEDCAE0D7F51C7B42182162ACE726E68166B38 ___eyesData_50;
	// UnityEngine.XR.InputFeatureUsage`1<UnityEngine.Vector2> UnityEngine.XR.CommonUsages::dPad
	InputFeatureUsage_1_tEB160A05BCDCCA4F96072CBA0866498D06B9A27C ___dPad_51;
	// UnityEngine.XR.InputFeatureUsage`1<System.Single> UnityEngine.XR.CommonUsages::indexFinger
	InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 ___indexFinger_52;
	// UnityEngine.XR.InputFeatureUsage`1<System.Single> UnityEngine.XR.CommonUsages::middleFinger
	InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 ___middleFinger_53;
	// UnityEngine.XR.InputFeatureUsage`1<System.Single> UnityEngine.XR.CommonUsages::ringFinger
	InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 ___ringFinger_54;
	// UnityEngine.XR.InputFeatureUsage`1<System.Single> UnityEngine.XR.CommonUsages::pinkyFinger
	InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 ___pinkyFinger_55;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> UnityEngine.XR.CommonUsages::thumbrest
	InputFeatureUsage_1_tE336B2F0B9AC721519BFA17A08D6353FD5221637 ___thumbrest_56;
	// UnityEngine.XR.InputFeatureUsage`1<System.Single> UnityEngine.XR.CommonUsages::indexTouch
	InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 ___indexTouch_57;
	// UnityEngine.XR.InputFeatureUsage`1<System.Single> UnityEngine.XR.CommonUsages::thumbTouch
	InputFeatureUsage_1_t311D0F42F1A7BF37D3CEAC15A53A1F24165F1848 ___thumbTouch_58;
};

// UnityEngine.XR.CommonUsages

// UnityEngine.ContactFilter2D

// UnityEngine.ContactFilter2D

// UnityEngine.ContactPoint

// UnityEngine.ContactPoint

// UnityEngine.ContactPoint2D

// UnityEngine.ContactPoint2D

// UnityEngine.ControllerColliderHit

// UnityEngine.ControllerColliderHit

// UnityEngine.Networking.DownloadHandler

// UnityEngine.Networking.DownloadHandler

// System.Exception
struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};

// System.Exception

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange

// UnityEngine.UIElements.UIR.GfxUpdateBufferRange

// UnityEngine.TextCore.Glyph

// UnityEngine.TextCore.Glyph

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord

// UnityEngine.TextCore.LowLevel.GlyphAdjustmentRecord

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct

// UnityEngine.TextCore.LowLevel.GlyphMarshallingStruct

// System.Runtime.InteropServices.HandleRef

// System.Runtime.InteropServices.HandleRef

// UnityEngine.HumanLimit

// UnityEngine.HumanLimit

// UnityEngine.IntegratedSubsystem

// UnityEngine.IntegratedSubsystem

// UnityEngine.IntegratedSubsystemDescriptor

// UnityEngine.IntegratedSubsystemDescriptor

// UnityEngine.XR.MeshGenerationResult

// UnityEngine.XR.MeshGenerationResult

// UnityEngine.XR.MeshInfo

// UnityEngine.XR.MeshInfo

// UnityEngine.XR.MeshTransform

// UnityEngine.XR.MeshTransform

// UnityEngine.ModifiableContactPair

// UnityEngine.ModifiableContactPair

// UnityEngine.Bindings.NativePropertyAttribute

// UnityEngine.Bindings.NativePropertyAttribute

// Byn.Awrtc.NetworkEvent

// Byn.Awrtc.NetworkEvent

// TriLibCore.Obj.ObjModel

// TriLibCore.Obj.ObjModel

// TriLibCore.Obj.ObjProcessor
struct ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115_StaticFields
{
	// UnityEngine.Matrix4x4 TriLibCore.Obj.ObjProcessor::_conversionMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ____conversionMatrix_20;
};

// TriLibCore.Obj.ObjProcessor

// TriLibCore.Obj.ObjTexture

// TriLibCore.Obj.ObjTexture

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// UnityEngine.Playables.PlayableHandle
struct PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4_StaticFields
{
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::m_Null
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Null_2;
};

// UnityEngine.Playables.PlayableHandle

// UnityEngine.Playables.PlayableOutputHandle
struct PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883_StaticFields
{
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::m_Null
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Null_2;
};

// UnityEngine.Playables.PlayableOutputHandle

// TriLibCore.Ply.PlyModel

// TriLibCore.Ply.PlyModel

// TriLibCore.Ply.PlyTexture

// TriLibCore.Ply.PlyTexture

// UnityEngine.Pose
struct Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971_StaticFields
{
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 ___k_Identity_2;
};

// UnityEngine.Pose

// Unity.Profiling.ProfilerMarker

// Unity.Profiling.ProfilerMarker

// UnityEngine.RaycastHit

// UnityEngine.RaycastHit

// UnityEngine.RaycastHit2D

// UnityEngine.RaycastHit2D

// UnityEngine.RemoteConfigSettings

// UnityEngine.RemoteConfigSettings

// UnityEngine.Rendering.RenderTargetIdentifier

// UnityEngine.Rendering.RenderTargetIdentifier

// UnityEngine.SendMouseEvents
struct SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields
{
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseUsed
	bool ___s_MouseUsed_0;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_LastHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_LastHit_1;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_MouseDownHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_MouseDownHit_2;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_CurrentHit
	HitInfoU5BU5D_tDAE7DF0D2B0BE3EB2FD25FB4418704E27A2BF1D5* ___m_CurrentHit_3;
	// UnityEngine.Camera[] UnityEngine.SendMouseEvents::m_Cameras
	CameraU5BU5D_t1506EBA524A07AD1066D6DD4D7DFC6721F1AC26B* ___m_Cameras_4;
	// System.Func`1<System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Vector2>> UnityEngine.SendMouseEvents::s_GetMouseState
	Func_1_tF5F7F5DCF1679E08B2536581A6E1EEF5529155C9* ___s_GetMouseState_5;
	// UnityEngine.Vector2 UnityEngine.SendMouseEvents::s_MousePosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___s_MousePosition_6;
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseButtonPressedThisFrame
	bool ___s_MouseButtonPressedThisFrame_7;
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseButtonIsPressed
	bool ___s_MouseButtonIsPressed_8;
};

// UnityEngine.SendMouseEvents

// UnityEngine.SkeletonBone

// UnityEngine.SkeletonBone

// TriLibCore.Stl.StlModel

// TriLibCore.Stl.StlModel

// TriLibCore.Stl.StlProcessor

// TriLibCore.Stl.StlProcessor

// System.IO.StreamReader
struct StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B_StaticFields
{
	// System.IO.StreamReader System.IO.StreamReader::Null
	StreamReader_t81027449065C1B0C339DB46241D8001A6F61130B* ___Null_2;
};

// System.IO.StreamReader

// System.Text.Encodings.Web.TextEncoderSettings

// System.Text.Encodings.Web.TextEncoderSettings

// UnityEngine.TextGenerationSettings

// UnityEngine.TextGenerationSettings

// UnityEngine.UIElements.TextNativeSettings

// UnityEngine.UIElements.TextNativeSettings

// UnityEngine.UIElements.TextVertex

// UnityEngine.UIElements.TextVertex

// TriLibCore.ThreeMf.ThreeMfModel

// TriLibCore.ThreeMf.ThreeMfModel

// TriLibCore.ThreeMf.Reader.ThreeMfReader
struct ThreeMfReader_t8F4D229EE9D88E8F68C2AC4FA06C66477A7545C1_StaticFields
{
	// System.Nullable`1<System.Double> TriLibCore.ThreeMf.Reader.ThreeMfReader::ThreeMfConversionPrecision
	Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 ___ThreeMfConversionPrecision_11;
};

// TriLibCore.ThreeMf.Reader.ThreeMfReader

// TriLibCore.ThreeMf.ThreeMfTexture

// TriLibCore.ThreeMf.ThreeMfTexture

// UnityEngine.Touch

// UnityEngine.Touch

// UnityEngine.UICharInfo

// UnityEngine.UICharInfo

// UnityEngine.UIVertex
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields
{
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 ___simpleVert_10;
};

// UnityEngine.UIVertex

// UnityEngine.Networking.UploadHandler

// UnityEngine.Networking.UploadHandler

// UnityEngine.XR.XRNodeState

// UnityEngine.XR.XRNodeState

// UnityEngine.Yoga.YogaConfig
struct YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345_StaticFields
{
	// UnityEngine.Yoga.YogaConfig UnityEngine.Yoga.YogaConfig::Default
	YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345* ___Default_0;
};

// UnityEngine.Yoga.YogaConfig

// UnityEngine.Yoga.YogaNode

// UnityEngine.Yoga.YogaNode

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData

// TriLibCore.Gltf.Draco.DracoMeshLoader/DracoData

// Unity.VectorGraphics.InternalBridge/GradientSettingsBridge

// Unity.VectorGraphics.InternalBridge/GradientSettingsBridge

// Unity.VectorGraphics.InternalBridge/VectorImageVertexBridge

// Unity.VectorGraphics.InternalBridge/VectorImageVertexBridge

// UnityEngine.LocationService/HeadingInfo

// UnityEngine.LocationService/HeadingInfo

// UnityEngine.ParticleSystem/MinMaxGradient

// UnityEngine.ParticleSystem/MinMaxGradient

// UnityEngine.ParticleSystem/Particle

// UnityEngine.ParticleSystem/Particle

// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData

// System.Text.Encodings.Web.TextEncoder/AsciiNeedsEscapingData

// UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc

// UnityEngine.XR.XRDisplaySubsystem/XRMirrorViewBlitDesc

// UnityEngine.XR.Management.XRManagerSettings/<InitializeLoader>d__24

// UnityEngine.XR.Management.XRManagerSettings/<InitializeLoader>d__24

// UnityEngine.XR.XRMeshSubsystem/MeshTransformList

// UnityEngine.XR.XRMeshSubsystem/MeshTransformList

// Firebase.Messaging.FirebaseMessaging/Listener/<MessageReceivedDelegateMethod>c__AnonStorey0

// Firebase.Messaging.FirebaseMessaging/Listener/<MessageReceivedDelegateMethod>c__AnonStorey0

// UnityEngine.IntegratedSubsystem`1<UnityEngine.XR.XRDisplaySubsystemDescriptor>

// UnityEngine.IntegratedSubsystem`1<UnityEngine.XR.XRDisplaySubsystemDescriptor>

// UnityEngine.IntegratedSubsystem`1<UnityEngine.XR.XRInputSubsystemDescriptor>

// UnityEngine.IntegratedSubsystem`1<UnityEngine.XR.XRInputSubsystemDescriptor>

// Firebase.Messaging.AndroidNotificationParams

// Firebase.Messaging.AndroidNotificationParams

// UnityEngine.Animations.AnimationClipPlayable

// UnityEngine.Animations.AnimationClipPlayable

// UnityEngine.AnimationEvent

// UnityEngine.AnimationEvent

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D_StaticFields
{
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationLayerMixerPlayable

// UnityEngine.Animations.AnimationMixerPlayable
struct AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0_StaticFields
{
	// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Animations.AnimationMixerPlayable::m_NullPlayable
	AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationMixerPlayable

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable
struct AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18_StaticFields
{
	// UnityEngine.Animations.AnimationMotionXToDeltaPlayable UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_NullPlayable
	AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable

// UnityEngine.Animations.AnimationOffsetPlayable
struct AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4_StaticFields
{
	// UnityEngine.Animations.AnimationOffsetPlayable UnityEngine.Animations.AnimationOffsetPlayable::m_NullPlayable
	AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationOffsetPlayable

// UnityEngine.Animations.AnimationPlayableOutput

// UnityEngine.Animations.AnimationPlayableOutput

// UnityEngine.Animations.AnimationPosePlayable
struct AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C_StaticFields
{
	// UnityEngine.Animations.AnimationPosePlayable UnityEngine.Animations.AnimationPosePlayable::m_NullPlayable
	AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationPosePlayable

// UnityEngine.Animations.AnimationRemoveScalePlayable
struct AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD_StaticFields
{
	// UnityEngine.Animations.AnimationRemoveScalePlayable UnityEngine.Animations.AnimationRemoveScalePlayable::m_NullPlayable
	AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationRemoveScalePlayable

// UnityEngine.Animations.AnimationScriptPlayable
struct AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127_StaticFields
{
	// UnityEngine.Animations.AnimationScriptPlayable UnityEngine.Animations.AnimationScriptPlayable::m_NullPlayable
	AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127 ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimationScriptPlayable

// UnityEngine.Animations.AnimatorControllerPlayable
struct AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A_StaticFields
{
	// UnityEngine.Animations.AnimatorControllerPlayable UnityEngine.Animations.AnimatorControllerPlayable::m_NullPlayable
	AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A ___m_NullPlayable_1;
};

// UnityEngine.Animations.AnimatorControllerPlayable

// UnityEngine.AudioClip

// UnityEngine.AudioClip

// UnityEngine.Audio.AudioClipPlayable

// UnityEngine.Audio.AudioClipPlayable

// UnityEngine.Audio.AudioMixerPlayable

// UnityEngine.Audio.AudioMixerPlayable

// UnityEngine.Audio.AudioPlayableOutput

// UnityEngine.Audio.AudioPlayableOutput

// UnityEngine.Component

// UnityEngine.Component

// UnityEngine.Networking.DownloadHandlerAudioClip

// UnityEngine.Networking.DownloadHandlerAudioClip

// UnityEngine.Networking.DownloadHandlerBuffer

// UnityEngine.Networking.DownloadHandlerBuffer

// UnityEngine.Networking.DownloadHandlerTexture

// UnityEngine.Networking.DownloadHandlerTexture

// Firebase.Crashlytics.FirebaseCrashlyticsFrame

// Firebase.Crashlytics.FirebaseCrashlyticsFrame

// Firebase.Crashlytics.FirebaseCrashlyticsInternal

// Firebase.Crashlytics.FirebaseCrashlyticsInternal

// Firebase.Messaging.FirebaseMessage
struct FirebaseMessage_t09E58AB584806FC5C23F419AD7CDCE53969FE20F_StaticFields
{
	// System.DateTime Firebase.Messaging.FirebaseMessage::UnixEpochUtc
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___UnixEpochUtc_2;
};

// Firebase.Messaging.FirebaseMessage

// Firebase.Messaging.FirebaseNotification

// Firebase.Messaging.FirebaseNotification

// UnityEngine.Font
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6_StaticFields
{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_tD91E4D0ED3C2E385D3BDD4B3EA48B5F99D39F1DC* ___textureRebuilt_4;
};

// UnityEngine.Font

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord

// UnityEngine.TextCore.LowLevel.GlyphPairAdjustmentRecord

// UnityEngine.HumanBone

// UnityEngine.HumanBone

// Firebase.Crashlytics.LoggedException

// Firebase.Crashlytics.LoggedException

// Firebase.Messaging.MessagingOptions

// Firebase.Messaging.MessagingOptions

// UnityEngine.Motion

// UnityEngine.Motion

// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid

// NativeGalleryNamespace.NGMediaReceiveCallbackAndroid

// NativeGalleryNamespace.NGPermissionCallbackAndroid

// NativeGalleryNamespace.NGPermissionCallbackAndroid

// NativeShareNamespace.NSShareResultCallbackAndroid

// NativeShareNamespace.NSShareResultCallbackAndroid

// TriLibCore.Obj.ObjRootModel

// TriLibCore.Obj.ObjRootModel

// TriLibCore.Obj.ObjStreamReader

// TriLibCore.Obj.ObjStreamReader

// TriLibCore.Ply.PlyRootModel

// TriLibCore.Ply.PlyRootModel

// TriLibCore.Ply.PlyStreamReader

// TriLibCore.Ply.PlyStreamReader

// UnityEngine.RuntimeAnimatorController

// UnityEngine.RuntimeAnimatorController

// UnityEngine.ScriptableObject

// UnityEngine.ScriptableObject

// Firebase.Crashlytics.StackFrames

// Firebase.Crashlytics.StackFrames

// TriLibCore.Stl.StlRootModel

// TriLibCore.Stl.StlRootModel

// TriLibCore.Stl.StlStreamReader

// TriLibCore.Stl.StlStreamReader

// System.Text.Encodings.Web.TextEncoder
struct TextEncoder_t35B2FDACE089FFCF6F1460C93866D1271356E691_StaticFields
{
	// System.Byte[] System.Text.Encodings.Web.TextEncoder::s_noEscape
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_noEscape_3;
};

// System.Text.Encodings.Web.TextEncoder

// UnityEngine.TextGenerator

// UnityEngine.TextGenerator

// TriLibCore.ThreeMf.ThreeMfRootModel

// TriLibCore.ThreeMf.ThreeMfRootModel

// UnityEngine.Networking.UnityWebRequest

// UnityEngine.Networking.UnityWebRequest

// UnityEngine.Networking.UnityWebRequestAsyncOperation

// UnityEngine.Networking.UnityWebRequestAsyncOperation

// UnityEngine.Networking.UploadHandlerRaw

// UnityEngine.Networking.UploadHandlerRaw

// UnityEngine.UIElements.UIR.Utility
struct Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields
{
	// System.Action`1<System.Boolean> UnityEngine.UIElements.UIR.Utility::GraphicsResourcesRecreate
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___GraphicsResourcesRecreate_0;
	// System.Action UnityEngine.UIElements.UIR.Utility::EngineUpdate
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___EngineUpdate_1;
	// System.Action UnityEngine.UIElements.UIR.Utility::FlushPendingResources
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___FlushPendingResources_2;
	// System.Action`1<UnityEngine.Camera> UnityEngine.UIElements.UIR.Utility::RegisterIntermediateRenderers
	Action_1_t268986DA4CF361AC17B40338506A83AFB35832EA* ___RegisterIntermediateRenderers_3;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeAdd
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeAdd_4;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeExecute
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeExecute_5;
	// System.Action`1<System.IntPtr> UnityEngine.UIElements.UIR.Utility::RenderNodeCleanup
	Action_1_t2DF1ED40E3084E997390FF52F462390882271FE2* ___RenderNodeCleanup_6;
	// Unity.Profiling.ProfilerMarker UnityEngine.UIElements.UIR.Utility::s_MarkerRaiseEngineUpdate
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD ___s_MarkerRaiseEngineUpdate_7;
};

// UnityEngine.UIElements.UIR.Utility

// UnityEngine.Experimental.Video.VideoClipPlayable

// UnityEngine.Experimental.Video.VideoClipPlayable

// UnityEngine.ParticleSystem/EmitParams

// UnityEngine.ParticleSystem/EmitParams

// UnityEngine.XR.XRDisplaySubsystem/XRRenderPass

// UnityEngine.XR.XRDisplaySubsystem/XRRenderPass

// UnityEngine.AnimatorOverrideController

// UnityEngine.AnimatorOverrideController

// UnityEngine.Behaviour

// UnityEngine.Behaviour

// UnityEngine.CanvasRenderer

// UnityEngine.CanvasRenderer

// System.Text.Encodings.Web.HtmlEncoder

// System.Text.Encodings.Web.HtmlEncoder

// System.Text.Encodings.Web.JavaScriptEncoder

// System.Text.Encodings.Web.JavaScriptEncoder

// UnityEngine.XR.OpenXR.Features.OpenXRFeature
struct OpenXRFeature_tC2F8F480D62C277B2ECDD605F64E45053CD85143_StaticFields
{
	// System.Boolean UnityEngine.XR.OpenXR.Features.OpenXRFeature::<requiredFeatureFailed>k__BackingField
	bool ___U3CrequiredFeatureFailedU3Ek__BackingField_6;
};

// UnityEngine.XR.OpenXR.Features.OpenXRFeature

// UnityEngine.XR.XRDisplaySubsystem

// UnityEngine.XR.XRDisplaySubsystem

// UnityEngine.XR.Management.XRGeneralSettings
struct XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE_StaticFields
{
	// System.String UnityEngine.XR.Management.XRGeneralSettings::k_SettingsKey
	String_t* ___k_SettingsKey_4;
	// UnityEngine.XR.Management.XRGeneralSettings UnityEngine.XR.Management.XRGeneralSettings::s_RuntimeSettingsInstance
	XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE* ___s_RuntimeSettingsInstance_5;
};

// UnityEngine.XR.Management.XRGeneralSettings

// UnityEngine.XR.XRInputSubsystem

// UnityEngine.XR.XRInputSubsystem

// UnityEngine.XR.Management.XRLoader

// UnityEngine.XR.Management.XRLoader

// UnityEngine.XR.Management.XRManagerSettings

// UnityEngine.XR.Management.XRManagerSettings

// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields
{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::preWillRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___preWillRenderCanvases_4;
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___willRenderCanvases_5;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externBeginRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternBeginRenderOverlaysU3Ek__BackingField_6;
	// System.Action`2<System.Int32,System.Int32> UnityEngine.Canvas::<externRenderOverlaysBefore>k__BackingField
	Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8* ___U3CexternRenderOverlaysBeforeU3Ek__BackingField_7;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externEndRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternEndRenderOverlaysU3Ek__BackingField_8;
};

// UnityEngine.Canvas

// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.ConformanceAutomationFeature
struct ConformanceAutomationFeature_t0C2E5D391DBCC81F1B86F2D7E34719B7D292BFF7_StaticFields
{
	// System.UInt64 UnityEngine.XR.OpenXR.Features.ConformanceAutomation.ConformanceAutomationFeature::xrInstance
	uint64_t ___xrInstance_17;
	// System.UInt64 UnityEngine.XR.OpenXR.Features.ConformanceAutomation.ConformanceAutomationFeature::xrSession
	uint64_t ___xrSession_18;
};

// UnityEngine.XR.OpenXR.Features.ConformanceAutomation.ConformanceAutomationFeature

// System.Text.Encodings.Web.DefaultHtmlEncoder
struct DefaultHtmlEncoder_t65341C0036795EED811809CFA8956E9B170C0CAE_StaticFields
{
	// System.Text.Encodings.Web.DefaultHtmlEncoder System.Text.Encodings.Web.DefaultHtmlEncoder::Singleton
	DefaultHtmlEncoder_t65341C0036795EED811809CFA8956E9B170C0CAE* ___Singleton_5;
	// System.Char[] System.Text.Encodings.Web.DefaultHtmlEncoder::s_quote
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_quote_6;
	// System.Char[] System.Text.Encodings.Web.DefaultHtmlEncoder::s_ampersand
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_ampersand_7;
	// System.Char[] System.Text.Encodings.Web.DefaultHtmlEncoder::s_lessthan
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_lessthan_8;
	// System.Char[] System.Text.Encodings.Web.DefaultHtmlEncoder::s_greaterthan
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_greaterthan_9;
};

// System.Text.Encodings.Web.DefaultHtmlEncoder

// System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin
struct DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C_StaticFields
{
	// System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_singleton
	DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C* ___s_singleton_4;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_b
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_b_5;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_t
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_t_6;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_n
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_n_7;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_f
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_f_8;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_r
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_r_9;
	// System.Char[] System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin::s_back
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___s_back_10;
};

// System.Text.Encodings.Web.DefaultJavaScriptEncoderBasicLatin

// UnityEngine.XR.OpenXR.Features.Mock.MockRuntime
struct MockRuntime_tC5F1019CA2D3E89010E3A3BFC89834251257B2AF_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.OpenXR.Features.Mock.MockRuntime/AfterFunctionDelegate> UnityEngine.XR.OpenXR.Features.Mock.MockRuntime::s_AfterFunctionCallbacks
	Dictionary_2_t07B33916E5B9BFBEDDE5C206E597C4A2071ED5E6* ___s_AfterFunctionCallbacks_16;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.OpenXR.Features.Mock.MockRuntime/BeforeFunctionDelegate> UnityEngine.XR.OpenXR.Features.Mock.MockRuntime::s_BeforeFunctionCallbacks
	Dictionary_2_tE800B4C2237603A792FFCCFB9BFC21EA79CB7F3F* ___s_BeforeFunctionCallbacks_17;
	// UnityEngine.XR.OpenXR.Features.Mock.MockRuntime/ScriptEventDelegate UnityEngine.XR.OpenXR.Features.Mock.MockRuntime::onScriptEvent
	ScriptEventDelegate_t0DE50F0234F04B68173703087BFCB08BB4922E20* ___onScriptEvent_18;
};

// UnityEngine.XR.OpenXR.Features.Mock.MockRuntime

// UnityEngine.MonoBehaviour

// UnityEngine.MonoBehaviour

// UnityEngine.XR.OpenXR.Features.OculusQuestSupport.OculusQuestFeature

// UnityEngine.XR.OpenXR.Features.OculusQuestSupport.OculusQuestFeature

// UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature
struct RuntimeDebuggerOpenXRFeature_t1E3C60D2636DEC25D95E63BB3300DBFC5E9057BA_StaticFields
{
	// System.Guid UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature::kEditorToPlayerRequestDebuggerOutput
	Guid_t ___kEditorToPlayerRequestDebuggerOutput_16;
	// System.Guid UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature::kPlayerToEditorSendDebuggerOutput
	Guid_t ___kPlayerToEditorSendDebuggerOutput_17;
};

// UnityEngine.XR.OpenXR.Features.RuntimeDebugger.RuntimeDebuggerOpenXRFeature

// UnityEngine.Video.VideoPlayer

// UnityEngine.Video.VideoPlayer

// UnityEngine.XR.Management.XRLoaderHelper

// UnityEngine.XR.Management.XRLoaderHelper

// Lean.Touch.LeanPulseScale

// Lean.Touch.LeanPulseScale

// NativeGalleryNamespace.NGCallbackHelper

// NativeGalleryNamespace.NGCallbackHelper

// NativeShareNamespace.NSCallbackHelper

// NativeShareNamespace.NSCallbackHelper

// OpenLinkBehaviour

// OpenLinkBehaviour

// UnityEngine.SpatialTracking.TrackedPoseDriver

// UnityEngine.SpatialTracking.TrackedPoseDriver

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript

// WebViewObject

// WebViewObject

// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir

// Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir
#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12000[4] = 
{
	static_cast<int32_t>(offsetof(NetworkConfig_tDE09348A384F9FCF79DC5CE5BB4DFEECF9AB0766, ___mIceServers_0)),static_cast<int32_t>(offsetof(NetworkConfig_tDE09348A384F9FCF79DC5CE5BB4DFEECF9AB0766, ___mSignalingUrl_1)),static_cast<int32_t>(offsetof(NetworkConfig_tDE09348A384F9FCF79DC5CE5BB4DFEECF9AB0766, ___mAllowRenegotiation_2)),static_cast<int32_t>(offsetof(NetworkConfig_tDE09348A384F9FCF79DC5CE5BB4DFEECF9AB0766, ___mIsConference_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12001[16] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12002[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12003[3] = 
{
	static_cast<int32_t>(offsetof(NetworkEvent_t6D46042D56AE8CB787CC307645DFBF5C5F48A90E, ___type_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(NetworkEvent_t6D46042D56AE8CB787CC307645DFBF5C5F48A90E, ___connectionId_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(NetworkEvent_t6D46042D56AE8CB787CC307645DFBF5C5F48A90E, ___data_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12004[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12008[6] = 
{
	static_cast<int32_t>(offsetof(BufferedFrame_tC838AB09A0FAE3B7465B85D8B99E956D1F44F4D2, ___mBuffer_0)),static_cast<int32_t>(offsetof(BufferedFrame_tC838AB09A0FAE3B7465B85D8B99E956D1F44F4D2, ___mWidth_1)),static_cast<int32_t>(offsetof(BufferedFrame_tC838AB09A0FAE3B7465B85D8B99E956D1F44F4D2, ___mHeight_2)),static_cast<int32_t>(offsetof(BufferedFrame_tC838AB09A0FAE3B7465B85D8B99E956D1F44F4D2, ___mRotation_3)),static_cast<int32_t>(offsetof(BufferedFrame_tC838AB09A0FAE3B7465B85D8B99E956D1F44F4D2, ___mTopRowFirst_4)),static_cast<int32_t>(offsetof(BufferedFrame_tC838AB09A0FAE3B7465B85D8B99E956D1F44F4D2, ___mFormat_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12009[7] = 
{
	static_cast<int32_t>(offsetof(SLog_tB306D01548C5F9057FEBA18F1046B6FBCCBB1721_StaticFields, ___TAG_WARNING_0)),static_cast<int32_t>(offsetof(SLog_tB306D01548C5F9057FEBA18F1046B6FBCCBB1721_StaticFields, ___TAG_ERROR_1)),static_cast<int32_t>(offsetof(SLog_tB306D01548C5F9057FEBA18F1046B6FBCCBB1721_StaticFields, ___TAG_EXCEPTION_2)),static_cast<int32_t>(offsetof(SLog_tB306D01548C5F9057FEBA18F1046B6FBCCBB1721_StaticFields, ___TAG_INFO_3)),static_cast<int32_t>(offsetof(SLog_tB306D01548C5F9057FEBA18F1046B6FBCCBB1721_StaticFields, ___TAG_DEBUG_4)),static_cast<int32_t>(offsetof(SLog_tB306D01548C5F9057FEBA18F1046B6FBCCBB1721_StaticFields, ___TAG_VERBOSE_5)),static_cast<int32_t>(offsetof(SLog_tB306D01548C5F9057FEBA18F1046B6FBCCBB1721_StaticFields, ___sLogger_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12010[10] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12011[16] = 
{
	static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD_StaticFields, ___LOGTAG_0)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mNetworkConfig_1)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mMediaConfig_2)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___CallEvent_3)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mNetwork_4)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mConferenceMode_5)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD_StaticFields, ___MESSAGE_TYPE_DATA_6)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD_StaticFields, ___MESSAGE_TYPE_STRING_7)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mState_8)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mIsDisposed_9)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mLocalFrameEvents_10)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mServerInactive_11)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mConnectionIds_12)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mPendingListenCall_13)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mPendingCallCall_14)),static_cast<int32_t>(offsetof(AWebRtcCall_t9C4022707E5CB585F465E9EAD9211F3D21D6B6DD, ___mPendingAddress_15)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12013[1] = 
{
	static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6EB70CB763ED0F38063A7F9D39E7E5A0EDFBBB26_StaticFields, ___20731A0997122AAAE93B2C58C609D52BF080F0A1_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12015[1] = 
{
	static_cast<int32_t>(offsetof(WebRequestUtils_t23F1FB533DBFDA3BE5624D901D535B4C6EFAD443_StaticFields, ___domainRegex_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12016[14] = 
{
	static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045, ___formData_0)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045, ___fieldNames_1)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045, ___fileNames_2)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045, ___types_3)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045, ___boundary_4)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045, ___containsFiles_5)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields, ___dDash_6)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields, ___crlf_7)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields, ___contentTypeHeader_8)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields, ___dispositionHeader_9)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields, ___endQuote_10)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields, ___fileNameField_11)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields, ___ampersand_12)),static_cast<int32_t>(offsetof(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields, ___equal_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12017[9] = 
{
	static_cast<int32_t>(offsetof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields, ___ucHexChars_0)),static_cast<int32_t>(offsetof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields, ___lcHexChars_1)),static_cast<int32_t>(offsetof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields, ___urlEscapeChar_2)),static_cast<int32_t>(offsetof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields, ___urlSpace_3)),static_cast<int32_t>(offsetof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields, ___dataSpace_4)),static_cast<int32_t>(offsetof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields, ___urlForbidden_5)),static_cast<int32_t>(offsetof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields, ___qpEscapeChar_6)),static_cast<int32_t>(offsetof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields, ___qpSpace_7)),static_cast<int32_t>(offsetof(WWWTranscoder_t551AAF7200BB7381823C52321E9A60A9EE63641B_StaticFields, ___qpForbidden_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12018[1] = 
{
	static_cast<int32_t>(offsetof(UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C, ___U3CwebRequestU3Ek__BackingField_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12019[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12020[30] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12021[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12022[14] = 
{
	static_cast<int32_t>(offsetof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F, ___m_Ptr_0)),static_cast<int32_t>(offsetof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F, ___m_DownloadHandler_1)),static_cast<int32_t>(offsetof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F, ___m_UploadHandler_2)),static_cast<int32_t>(offsetof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F, ___m_CertificateHandler_3)),static_cast<int32_t>(offsetof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F, ___m_Uri_4)),0,0,0,0,0,0,static_cast<int32_t>(offsetof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F, ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_11)),static_cast<int32_t>(offsetof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_12)),static_cast<int32_t>(offsetof(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12024[3] = 
{
	static_cast<int32_t>(offsetof(MultipartFormDataSection_t655FED91F26A4D9444E0707D13C82C6A732288BC, ___name_0)),static_cast<int32_t>(offsetof(MultipartFormDataSection_t655FED91F26A4D9444E0707D13C82C6A732288BC, ___data_1)),static_cast<int32_t>(offsetof(MultipartFormDataSection_t655FED91F26A4D9444E0707D13C82C6A732288BC, ___content_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12025[4] = 
{
	static_cast<int32_t>(offsetof(MultipartFormFileSection_t87881191832E7F9AC25612810A3BE55555D398DB, ___name_0)),static_cast<int32_t>(offsetof(MultipartFormFileSection_t87881191832E7F9AC25612810A3BE55555D398DB, ___data_1)),static_cast<int32_t>(offsetof(MultipartFormFileSection_t87881191832E7F9AC25612810A3BE55555D398DB, ___file_2)),static_cast<int32_t>(offsetof(MultipartFormFileSection_t87881191832E7F9AC25612810A3BE55555D398DB, ___content_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12026[1] = 
{
	static_cast<int32_t>(offsetof(DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB, ___m_Ptr_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12027[1] = 
{
	static_cast<int32_t>(offsetof(DownloadHandlerBuffer_t34C626F6513FA9A44FDDDEE85455CF2CD9DA5974, ___m_NativeData_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12028[1] = 
{
	static_cast<int32_t>(offsetof(UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6, ___m_Ptr_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12029[1] = 
{
	static_cast<int32_t>(offsetof(UploadHandlerRaw_t0A24CF320CDF16F1BC6C5C086DE71A1908CBB91A, ___m_Payload_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12030[1] = 
{
	static_cast<int32_t>(offsetof(CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804, ___m_Ptr_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12033[1] = 
{
	static_cast<int32_t>(offsetof(AnimatorOverrideController_tF78BD58B30BB0D767E7A96F8428EA66F2DFD5493, ___OnOverrideControllerDirty_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12034[5] = 
{
	static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___name_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___parentName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___position_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___rotation_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(SkeletonBone_tA999028EED923DB65E286BB99F81541872F5B126, ___scale_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12035[5] = 
{
	static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_Min_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_Max_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_Center_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_AxisLength_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanLimit_tE825F951DEE60E2641DD91F3C92C6B56A139A36E, ___m_UseDefaultValues_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12036[3] = 
{
	static_cast<int32_t>(offsetof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8, ___m_BoneName_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8, ___m_HumanName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanBone_t9A978BB2457E935D0B6FA64ADDE60562ADAE5FD8, ___limit_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12037[14] = 
{
	static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___human_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___skeleton_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_ArmTwist_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_ForeArmTwist_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_UpperLegTwist_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_LegTwist_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_ArmStretch_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_LegStretch_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_FeetSpacing_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_GlobalScale_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_RootMotionBoneName_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_HasTranslationDoF_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_HasExtraRoot_12)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HumanDescription_tAEFC8398C9AA70799C171BC0AEE07C0031B0CD44, ___m_SkeletonHasParents_13)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12039[57] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12042[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12043[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12044[2] = 
{
	static_cast<int32_t>(offsetof(Enumerator_t81434F7D5603121F3D7BD6DB916FE1C755307530, ___m_Outer_0)),static_cast<int32_t>(offsetof(Enumerator_t81434F7D5603121F3D7BD6DB916FE1C755307530, ___m_CurrentIndex_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12047[11] = 
{
	static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_Time_0)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_FunctionName_1)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_StringParameter_2)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_ObjectReferenceParameter_3)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_FloatParameter_4)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_IntParameter_5)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_MessageOptions_6)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_Source_7)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_StateSender_8)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_AnimatorStateInfo_9)),static_cast<int32_t>(offsetof(AnimationEvent_t77294DB2372A5C387B53EEA3EFDC550C75EC3174, ___m_AnimatorClipInfo_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12048[1] = 
{
	static_cast<int32_t>(offsetof(Motion_tBCD49FBF5608AD21FC03B63C8182FABCEF2707AC, ___U3CisAnimatorMotionU3Ek__BackingField_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12050[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12051[2] = 
{
	static_cast<int32_t>(offsetof(AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03, ___m_ClipInstanceID_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorClipInfo_t0C913173594C893E36282602F54ABD06AC1CFA03, ___m_Weight_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12052[9] = 
{
	static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Name_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Path_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_FullPath_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_NormalizedTime_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Length_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Speed_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_SpeedMultiplier_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Tag_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorStateInfo_t1F47379289C7CE7FD588FBC3BBD79A777243B6B2, ___m_Loop_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12053[8] = 
{
	static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_FullPath_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_UserName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_Name_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_HasFixedDuration_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_Duration_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_NormalizedTime_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_AnyState_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t44894D5EDEACB368CDBA07458F6EE5F01A1F34AD, ___m_TransitionType_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12059[2] = 
{
	static_cast<int32_t>(offsetof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationRemoveScalePlayable_t915611F6D3CC150DDCAF56412AC3E5ACB518A9DD_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12060[2] = 
{
	static_cast<int32_t>(offsetof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimatorControllerPlayable_tADDCB301674D6243EFE1BD032E7D118FD091210A_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12061[2] = 
{
	static_cast<int32_t>(offsetof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationOffsetPlayable_t39A1B1103995D63650F606BA2EA4ABDF9484AFB4_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12063[2] = 
{
	static_cast<int32_t>(offsetof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationScriptPlayable_t1326433F6848D93D7D90BC54B4AB6649A5D59127_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12064[2] = 
{
	static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_tAD8D28A1E2FB76567E9748CDD11699AEF0B4317D_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12065[1] = 
{
	static_cast<int32_t>(offsetof(AnimationClipPlayable_t54CEA0DD315B1674C2BD49E681005C4271D73969, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12066[1] = 
{
	static_cast<int32_t>(offsetof(AnimationHumanStream_t31E8EAD3F7C2C29CAE7B4EFB87AA84ECC6DCC6EC, ___stream_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12067[2] = 
{
	static_cast<int32_t>(offsetof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationPosePlayable_tBB5B82AC675A509F3808C8F825EA24943714CD5C_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12068[2] = 
{
	static_cast<int32_t>(offsetof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationMotionXToDeltaPlayable_t3946605ADB0B4C054A27B3D65A59F8EB75B6BE18_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12069[2] = 
{
	static_cast<int32_t>(offsetof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationMixerPlayable_t2984697B87B8719A34519FCF2130545D6D7AB6C0_StaticFields, ___m_NullPlayable_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12070[7] = 
{
	static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___m_AnimatorBindingsVersion_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___constant_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___input_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___output_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___workspace_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___inputStreamAccessor_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(AnimationStream_tA73510DCEE63720142DF4F8E15C337A48E47B94A, ___animationHandleBinder_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12071[1] = 
{
	static_cast<int32_t>(offsetof(AnimationPlayableOutput_t753AC95DC826789BC537D18449E93114777DDF4E, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12075[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12076[7] = 
{
	static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____ygNode_0)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____config_1)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____parent_2)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____children_3)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____measureFunction_4)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____baselineFunction_5)),static_cast<int32_t>(offsetof(YogaNode_t4B5B593220CCB315B5A60CB48BA4795636F04DDA, ____data_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12077[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12078[9] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12079[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12082[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12084[3] = 
{
	static_cast<int32_t>(offsetof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345_StaticFields, ___Default_0)),static_cast<int32_t>(offsetof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345, ____ygConfig_1)),static_cast<int32_t>(offsetof(YogaConfig_tE8B56F99460C291C1F7F46DBD8BAC9F0B653A345, ____logger_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12085[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12086[2] = 
{
	static_cast<int32_t>(offsetof(YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C, ___value_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(YogaValue_t9066126971BFC18D9B4A8AB11435557F19598F8C, ___unit_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12087[10] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12088[2] = 
{
	static_cast<int32_t>(offsetof(YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA, ___width_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(YogaSize_tA276812CB1E90E7AA2028A9474EA6EA46B3B38EA, ___height_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12090[7] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12091[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12092[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12093[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12095[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12097[2] = 
{
	static_cast<int32_t>(offsetof(UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields, ___RepaintOverlayPanelsCallback_0)),static_cast<int32_t>(offsetof(UIElementsRuntimeUtilityNative_t9DE2C23158D553BB693212D0D8AEAE8594E75938_StaticFields, ___UpdateRuntimePanelsCallback_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12098[10] = 
{
	static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___text_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___font_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___size_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___scaling_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___style_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___color_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___anchor_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___wordWrap_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___wordWrapWidth_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextNativeSettings_tE9D302AD381537B4FD42C3D02583F719CFF40062, ___richText_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12099[3] = 
{
	static_cast<int32_t>(offsetof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3, ___position_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3, ___color_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextVertex_tF56662BA585F7DD34D71971F1AA1D2E767946CF3, ___uv0_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12101[3] = 
{
	static_cast<int32_t>(offsetof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97, ___offsetFromWriteStart_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97, ___size_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GfxUpdateBufferRange_tC47258BCB472B0727B4FCE21A2A53506644C1A97, ___source_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12102[4] = 
{
	static_cast<int32_t>(offsetof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4, ___firstIndex_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4, ___indexCount_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4, ___minIndexVal_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DrawBufferRange_t684F255F5C954760B12F6689F84E78811040C7A4, ___vertsReferenced_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12103[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12104[3] = 
{
	0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12105[8] = 
{
	static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___GraphicsResourcesRecreate_0)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___EngineUpdate_1)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___FlushPendingResources_2)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___RegisterIntermediateRenderers_3)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___RenderNodeAdd_4)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___RenderNodeExecute_5)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___RenderNodeCleanup_6)),static_cast<int32_t>(offsetof(Utility_t8BCC393462C6270211734BE47CF5350F05EC97AD_StaticFields, ___s_MarkerRaiseEngineUpdate_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12109[10] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12110[12] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12111[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12112[13] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12113[9] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12114[2] = 
{
	static_cast<int32_t>(offsetof(InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599, ___m_Name_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(InputFeatureUsage_t1E251DC4F8CD697080F0F5D98388955AF8B87599, ___m_InternalType_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12115[1] = 
{
	0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12116[59] = 
{
	static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___isTracked_0)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___primaryButton_1)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___primaryTouch_2)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___secondaryButton_3)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___secondaryTouch_4)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___gripButton_5)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___triggerButton_6)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___menuButton_7)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___primary2DAxisClick_8)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___primary2DAxisTouch_9)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___secondary2DAxisClick_10)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___secondary2DAxisTouch_11)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___userPresence_12)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___trackingState_13)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___batteryLevel_14)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___trigger_15)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___grip_16)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___primary2DAxis_17)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___secondary2DAxis_18)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___devicePosition_19)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___leftEyePosition_20)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___rightEyePosition_21)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___centerEyePosition_22)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___colorCameraPosition_23)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___deviceVelocity_24)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___deviceAngularVelocity_25)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___leftEyeVelocity_26)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___leftEyeAngularVelocity_27)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___rightEyeVelocity_28)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___rightEyeAngularVelocity_29)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___centerEyeVelocity_30)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___centerEyeAngularVelocity_31)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___colorCameraVelocity_32)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___colorCameraAngularVelocity_33)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___deviceAcceleration_34)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___deviceAngularAcceleration_35)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___leftEyeAcceleration_36)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___leftEyeAngularAcceleration_37)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___rightEyeAcceleration_38)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___rightEyeAngularAcceleration_39)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___centerEyeAcceleration_40)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___centerEyeAngularAcceleration_41)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___colorCameraAcceleration_42)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___colorCameraAngularAcceleration_43)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___deviceRotation_44)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___leftEyeRotation_45)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___rightEyeRotation_46)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___centerEyeRotation_47)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___colorCameraRotation_48)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___handData_49)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___eyesData_50)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___dPad_51)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___indexFinger_52)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___middleFinger_53)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___ringFinger_54)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___pinkyFinger_55)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___thumbrest_56)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___indexTouch_57)),static_cast<int32_t>(offsetof(CommonUsages_t9208F514F1E77BE70AC53EFEC94D57EDDAF3B8E1_StaticFields, ___thumbTouch_58)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12117[3] = 
{
	static_cast<int32_t>(offsetof(InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD_StaticFields, ___s_InputSubsystemCache_0)),static_cast<int32_t>(offsetof(InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD, ___m_DeviceId_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(InputDevice_t882EE3EE8A71D8F5F38BA3F9356A49F24510E8BD, ___m_Initialized_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12118[2] = 
{
	static_cast<int32_t>(offsetof(Hand_t67B90BC0D36CBC92DF7E38BD15463B925CB5912C, ___m_DeviceId_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Hand_t67B90BC0D36CBC92DF7E38BD15463B925CB5912C, ___m_FeatureIndex_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12119[2] = 
{
	static_cast<int32_t>(offsetof(Eyes_t9FD3821575977E294F11E0EB92D2A4CA509ED8C7, ___m_DeviceId_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Eyes_t9FD3821575977E294F11E0EB92D2A4CA509ED8C7, ___m_FeatureIndex_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12120[2] = 
{
	static_cast<int32_t>(offsetof(Bone_t2558B1DD1E4F405EA4D76A3B8D5149CA16011975, ___m_DeviceId_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Bone_t2558B1DD1E4F405EA4D76A3B8D5149CA16011975, ___m_FeatureIndex_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12121[4] = 
{
	static_cast<int32_t>(offsetof(InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_StaticFields, ___s_InputDeviceList_0)),static_cast<int32_t>(offsetof(InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_StaticFields, ___deviceConnected_1)),static_cast<int32_t>(offsetof(InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_StaticFields, ___deviceDisconnected_2)),static_cast<int32_t>(offsetof(InputDevices_t02B79FC19CEA9AC29A9945F5CDA6D790730FBF34_StaticFields, ___deviceConfigChanged_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12122[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12123[4] = 
{
	static_cast<int32_t>(offsetof(InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1_StaticFields, ___trackingAcquired_0)),static_cast<int32_t>(offsetof(InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1_StaticFields, ___trackingLost_1)),static_cast<int32_t>(offsetof(InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1_StaticFields, ___nodeAdded_2)),static_cast<int32_t>(offsetof(InputTracking_tA4F34D4D5EC8E560B56ED295177C040D9C9815F1_StaticFields, ___nodeRemoved_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12124[8] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12125[10] = 
{
	static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_Type_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_AvailableFields_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_Position_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_Rotation_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_Velocity_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_AngularVelocity_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_Acceleration_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_AngularAcceleration_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_Tracked_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRNodeState_t683158812A1D80A6BC73DB97405BB0B795A9111A, ___m_UniqueID_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12127[3] = 
{
	static_cast<int32_t>(offsetof(XRInputSubsystem_tFECE6683FCAEBF05BAD05E5D612690095D8BAD34, ___trackingOriginUpdated_2)),static_cast<int32_t>(offsetof(XRInputSubsystem_tFECE6683FCAEBF05BAD05E5D612690095D8BAD34, ___boundaryChanged_3)),static_cast<int32_t>(offsetof(XRInputSubsystem_tFECE6683FCAEBF05BAD05E5D612690095D8BAD34, ___m_DeviceIdsCache_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12129[3] = 
{
	static_cast<int32_t>(offsetof(MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC_StaticFields, ___s_InvalidId_0)),static_cast<int32_t>(offsetof(MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC, ___m_SubId1_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshId_t2CF122567F06D0AA4F80DDA5CB51E8CD3B7EA2AC, ___m_SubId2_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12130[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12132[9] = 
{
	static_cast<int32_t>(offsetof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C, ___U3CMeshIdU3Ek__BackingField_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C, ___U3CMeshU3Ek__BackingField_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C, ___U3CMeshColliderU3Ek__BackingField_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C, ___U3CStatusU3Ek__BackingField_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C, ___U3CAttributesU3Ek__BackingField_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C, ___U3CTimestampU3Ek__BackingField_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C, ___U3CPositionU3Ek__BackingField_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C, ___U3CRotationU3Ek__BackingField_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshGenerationResult_tD5A6D639B2CF1A3F855AFB41861DEC48DC0D3A9C, ___U3CScaleU3Ek__BackingField_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12133[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12134[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12135[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12136[3] = 
{
	static_cast<int32_t>(offsetof(MeshInfo_t0BB7A8C2C586FD5EAD40E9E4E1749150915FDFFA, ___U3CMeshIdU3Ek__BackingField_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshInfo_t0BB7A8C2C586FD5EAD40E9E4E1749150915FDFFA, ___U3CChangeStateU3Ek__BackingField_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshInfo_t0BB7A8C2C586FD5EAD40E9E4E1749150915FDFFA, ___U3CPriorityHintU3Ek__BackingField_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12137[5] = 
{
	static_cast<int32_t>(offsetof(MeshTransform_t669BDD0278EB7AC2976F16FD341CC1C4747FAA6A, ___U3CMeshIdU3Ek__BackingField_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshTransform_t669BDD0278EB7AC2976F16FD341CC1C4747FAA6A, ___U3CTimestampU3Ek__BackingField_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshTransform_t669BDD0278EB7AC2976F16FD341CC1C4747FAA6A, ___U3CPositionU3Ek__BackingField_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshTransform_t669BDD0278EB7AC2976F16FD341CC1C4747FAA6A, ___U3CRotationU3Ek__BackingField_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MeshTransform_t669BDD0278EB7AC2976F16FD341CC1C4747FAA6A, ___U3CScaleU3Ek__BackingField_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12138[1] = 
{
	static_cast<int32_t>(offsetof(MeshTransformList_t761D725D4B30CFD7DDF57B3725004994FB3B561F, ___m_Self_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12141[9] = 
{
	static_cast<int32_t>(offsetof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8, ___displaySubsystemInstance_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8, ___renderPassIndex_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8, ___renderTarget_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8, ___renderTargetDesc_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8, ___hasMotionVectorPass_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8, ___motionVectorRenderTarget_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8, ___motionVectorRenderTargetDesc_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8, ___shouldFillOutDepth_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRRenderPass_t9E8711E8C69E3917AA39A0CA8304B604ED2838E8, ___cullingPassIndex_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12142[4] = 
{
	static_cast<int32_t>(offsetof(XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0, ___displaySubsystemInstance_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0, ___nativeBlitAvailable_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0, ___nativeBlitInvalidStates_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(XRMirrorViewBlitDesc_tC913B2856BA2160BC3AD99F0C67293850E2162E0, ___blitParamsCount_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12143[1] = 
{
	static_cast<int32_t>(offsetof(XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1, ___displayFocusChanged_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12145[3] = 
{
	static_cast<int32_t>(offsetof(PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8, ___Properties_0)),static_cast<int32_t>(offsetof(PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8, ___Count_1)),static_cast<int32_t>(offsetof(PlyElement_t37A3C3E1660A91570103A028EFBD20F78DDDD3C8, ___Data_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12147[2] = 
{
	static_cast<int32_t>(offsetof(PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E, ___CounterType_2)),static_cast<int32_t>(offsetof(PlyListProperty_t093FB53C46C251396FC338725D760422D77FB96E, ___ItemType_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12148[6] = 
{
	static_cast<int32_t>(offsetof(PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6, ____properties_0)),static_cast<int32_t>(offsetof(PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6, ___U3CNameU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6, ___U3CUsedU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6, ___U3CIndexU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6, ___U3CProcessingU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(PlyMaterial_t50F9E5112D02734C27A4CECB738070432DD1BEF6, ___U3CProcessedU3Ek__BackingField_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12149[14] = 
{
	static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CUsedU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CLocalPositionU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CLocalRotationU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CLocalScaleU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CVisibilityU3Ek__BackingField_5)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CParentU3Ek__BackingField_6)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CChildrenU3Ek__BackingField_7)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CIsBoneU3Ek__BackingField_8)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CGeometryGroupU3Ek__BackingField_9)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CBonesU3Ek__BackingField_10)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CBindPosesU3Ek__BackingField_11)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CMaterialIndicesU3Ek__BackingField_12)),static_cast<int32_t>(offsetof(PlyModel_tF7A0A51D92938029A68D524B3EBF3878F1A6F1E2, ___U3CUserPropertiesU3Ek__BackingField_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12150[66] = 
{
	0,0,static_cast<int32_t>(offsetof(PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F, ____reader_2)),static_cast<int32_t>(offsetof(PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F, ____allVertices_3)),static_cast<int32_t>(offsetof(PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F, ____allNormals_4)),static_cast<int32_t>(offsetof(PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F, ____allColors_5)),static_cast<int32_t>(offsetof(PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F, ____allUVs_6)),static_cast<int32_t>(offsetof(PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F, ____floatCount_7)),static_cast<int32_t>(offsetof(PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F, ____vertexAttributes_8)),static_cast<int32_t>(offsetof(PlyProcessor_t7698B4DBE35BC5D1E9359B4470DA23C60A78159F, ____geometryGroup_9)),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12151[2] = 
{
	static_cast<int32_t>(offsetof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41, ____intValue_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(PlyValue_tBBD98403161A8F3744AEF3B88BD7E5551E647D41_StaticFields, ___Unknown_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12152[2] = 
{
	static_cast<int32_t>(offsetof(PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362, ___Type_0)),static_cast<int32_t>(offsetof(PlyProperty_t780C4C52A4C404E2D8C5EC0D9746BB8B0766E362, ___Offset_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12153[11] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12154[7] = 
{
	static_cast<int32_t>(offsetof(PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237, ___U3CAllModelsU3Ek__BackingField_14)),static_cast<int32_t>(offsetof(PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237, ___U3CAllGeometryGroupsU3Ek__BackingField_15)),static_cast<int32_t>(offsetof(PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237, ___U3CAllAnimationsU3Ek__BackingField_16)),static_cast<int32_t>(offsetof(PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237, ___U3CAllMaterialsU3Ek__BackingField_17)),static_cast<int32_t>(offsetof(PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237, ___U3CAllTexturesU3Ek__BackingField_18)),static_cast<int32_t>(offsetof(PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237, ___U3CAllCamerasU3Ek__BackingField_19)),static_cast<int32_t>(offsetof(PlyRootModel_tB6BD5531F4C5D89335B35E22B166B5B102744237, ___U3CAllLightsU3Ek__BackingField_20)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12155[10] = 
{
	0,0,0,0,0,0,static_cast<int32_t>(offsetof(PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2, ____charStream_24)),static_cast<int32_t>(offsetof(PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2, ____charString_25)),static_cast<int32_t>(offsetof(PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2, ____charPosition_26)),static_cast<int32_t>(offsetof(PlyStreamReader_t253A0F366D274097EFB5D6B622476A39FC0DF6D2, ___U3CPositionU3Ek__BackingField_27)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12156[13] = 
{
	static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CUsedU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CDataU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CDataStreamU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CFilenameU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CWrapModeUU3Ek__BackingField_5)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CWrapModeVU3Ek__BackingField_6)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CTilingU3Ek__BackingField_7)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3COffsetU3Ek__BackingField_8)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CTextureIdU3Ek__BackingField_9)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CResolvedFilenameU3Ek__BackingField_10)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CHasAlphaU3Ek__BackingField_11)),static_cast<int32_t>(offsetof(PlyTexture_tBDFDD402A28C5B1209AD5A5AC2FC274E3F93CE9C, ___U3CTextureFormatU3Ek__BackingField_12)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12158[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12164[14] = 
{
	static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___applicationDelegate_0)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___arithmeticDelegate_1)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___divideByZeroDelegate_2)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___indexOutOfRangeDelegate_3)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___invalidCastDelegate_4)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___invalidOperationDelegate_5)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___ioDelegate_6)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___nullReferenceDelegate_7)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___outOfMemoryDelegate_8)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___overflowDelegate_9)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___systemDelegate_10)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___argumentDelegate_11)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___argumentNullDelegate_12)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t991C08ABAEC4AB8BB0C48ACEC6436F8B9C1F8970_StaticFields, ___argumentOutOfRangeDelegate_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12165[2] = 
{
	static_cast<int32_t>(offsetof(SWIGPendingException_t9C3365ED3C047C6935279BA7C05F89F3BDE54E35_ThreadStaticFields, ___pendingException_0)) | THREAD_LOCAL_STATIC_MASK,static_cast<int32_t>(offsetof(SWIGPendingException_t9C3365ED3C047C6935279BA7C05F89F3BDE54E35_StaticFields, ___numExceptionsPending_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12167[1] = 
{
	static_cast<int32_t>(offsetof(SWIGStringHelper_tA8CF9EF78BD75BC8C5CB597CC99E74764BFE361B_StaticFields, ___stringDelegate_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12168[2] = 
{
	static_cast<int32_t>(offsetof(CrashlyticsInternalPINVOKE_t691F261E9A85343DA87F1275433EFC3767B4981B_StaticFields, ___swigExceptionHelper_0)),static_cast<int32_t>(offsetof(CrashlyticsInternalPINVOKE_t691F261E9A85343DA87F1275433EFC3767B4981B_StaticFields, ___swigStringHelper_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12169[2] = 
{
	static_cast<int32_t>(offsetof(FirebaseCrashlyticsInternal_t674756D0479C8ED14FE9E114B533CD274F9D6F40, ___swigCPtr_0)),static_cast<int32_t>(offsetof(FirebaseCrashlyticsInternal_t674756D0479C8ED14FE9E114B533CD274F9D6F40, ___swigCMemOwn_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12170[2] = 
{
	static_cast<int32_t>(offsetof(FirebaseCrashlyticsFrame_tF9A1EF7CF973F7510734D32E076E07391C77B7D6, ___swigCPtr_0)),static_cast<int32_t>(offsetof(FirebaseCrashlyticsFrame_tF9A1EF7CF973F7510734D32E076E07391C77B7D6, ___swigCMemOwn_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12171[4] = 
{
	static_cast<int32_t>(offsetof(StackFramesEnumerator_tDC4E1B8FC390A2688F9CAF90ACF938CBD75605A7, ___collectionRef_0)),static_cast<int32_t>(offsetof(StackFramesEnumerator_tDC4E1B8FC390A2688F9CAF90ACF938CBD75605A7, ___currentIndex_1)),static_cast<int32_t>(offsetof(StackFramesEnumerator_tDC4E1B8FC390A2688F9CAF90ACF938CBD75605A7, ___currentObject_2)),static_cast<int32_t>(offsetof(StackFramesEnumerator_tDC4E1B8FC390A2688F9CAF90ACF938CBD75605A7, ___currentSize_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12172[2] = 
{
	static_cast<int32_t>(offsetof(StackFrames_t8F4306185C13967101E94E78F0D2312DDC4401AC, ___swigCPtr_0)),static_cast<int32_t>(offsetof(StackFrames_t8F4306185C13967101E94E78F0D2312DDC4401AC, ___swigCMemOwn_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12173[5] = 
{
	static_cast<int32_t>(offsetof(StackTraceParser_tCD308CD049C1C2B3A198DBBDB3357B628F793B7D_StaticFields, ___FrameArgsRegex_0)),static_cast<int32_t>(offsetof(StackTraceParser_tCD308CD049C1C2B3A198DBBDB3357B628F793B7D_StaticFields, ___FrameRegexWithoutFileInfo_1)),static_cast<int32_t>(offsetof(StackTraceParser_tCD308CD049C1C2B3A198DBBDB3357B628F793B7D_StaticFields, ___FrameRegexWithFileInfo_2)),static_cast<int32_t>(offsetof(StackTraceParser_tCD308CD049C1C2B3A198DBBDB3357B628F793B7D_StaticFields, ___MonoFilenameUnknownString_3)),static_cast<int32_t>(offsetof(StackTraceParser_tCD308CD049C1C2B3A198DBBDB3357B628F793B7D_StaticFields, ___StringDelimiters_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12174[3] = 
{
	static_cast<int32_t>(offsetof(LoggedException_t43B89090462BFFD9B76040EF52EE2EFD63359887, ___U3CNameU3Ek__BackingField_18)),static_cast<int32_t>(offsetof(LoggedException_t43B89090462BFFD9B76040EF52EE2EFD63359887, ___U3CCustomStackTraceU3Ek__BackingField_19)),static_cast<int32_t>(offsetof(LoggedException_t43B89090462BFFD9B76040EF52EE2EFD63359887, ___U3CParsedStackTraceU3Ek__BackingField_20)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12175[1] = 
{
	static_cast<int32_t>(offsetof(MetadataBuilder_t7BB701F903E4674E17AF9A4C8EE07943B6616FE9_StaticFields, ___METADATA_KEY_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12176[20] = 
{
	static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___uv_0)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___idb_1)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___pt_2)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___pc_3)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___pf_4)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___sms_5)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gms_6)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gdid_7)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gdvid_8)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gdn_9)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gdv_10)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gdver_11)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gdt_12)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gsl_13)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___grtc_14)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gcts_15)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___gmts_16)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___ss_17)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___sdpi_18)),static_cast<int32_t>(offsetof(Metadata_tC80E86736BA3888D72B8344C8F8DB0E7AA5AB094, ___srr_19)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12177[6] = 
{
	static_cast<int32_t>(offsetof(Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF_StaticFields, ___LogString_0)),static_cast<int32_t>(offsetof(Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF_StaticFields, ___SetKeyValueString_1)),static_cast<int32_t>(offsetof(Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF_StaticFields, ___SetUserIdentifierString_2)),static_cast<int32_t>(offsetof(Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF_StaticFields, ___LogExceptionString_3)),static_cast<int32_t>(offsetof(Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF_StaticFields, ___IsCrashlyticsCollectionEnabledString_4)),static_cast<int32_t>(offsetof(Impl_t9BC9F6C5466C4F180F0FE9B6736ED9B2354D87DF_StaticFields, ___SetCrashlyticsCollectionEnabledString_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12178[1] = 
{
	static_cast<int32_t>(offsetof(ExceptionHandler_t2DDC3721AFC96975EA180F4E7A4902FFB2CE138B, ___isRegistered_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12179[3] = 
{
	static_cast<int32_t>(offsetof(PlatformAccessor_tB02E4C5B35E2A951376B828D3F51E6623A65336D_StaticFields, ____exceptionHandler_0)),static_cast<int32_t>(offsetof(PlatformAccessor_tB02E4C5B35E2A951376B828D3F51E6623A65336D_StaticFields, ____impl_1)),static_cast<int32_t>(offsetof(PlatformAccessor_tB02E4C5B35E2A951376B828D3F51E6623A65336D_StaticFields, ____app_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12182[2] = 
{
	static_cast<int32_t>(offsetof(U3CLogU3Ec__AnonStorey0_tDB91FA9FBE3AC12BEF4C7C6281D3DA6E16AE8AB2, ___message_0)),static_cast<int32_t>(offsetof(U3CLogU3Ec__AnonStorey0_tDB91FA9FBE3AC12BEF4C7C6281D3DA6E16AE8AB2, ___U24this_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12183[3] = 
{
	static_cast<int32_t>(offsetof(U3CSetCustomKeyU3Ec__AnonStorey1_t812094D30223D5102E7F6EFC1CE9A4E5CA7FE73F, ___key_0)),static_cast<int32_t>(offsetof(U3CSetCustomKeyU3Ec__AnonStorey1_t812094D30223D5102E7F6EFC1CE9A4E5CA7FE73F, ___value_1)),static_cast<int32_t>(offsetof(U3CSetCustomKeyU3Ec__AnonStorey1_t812094D30223D5102E7F6EFC1CE9A4E5CA7FE73F, ___U24this_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12184[2] = 
{
	static_cast<int32_t>(offsetof(U3CSetUserIdU3Ec__AnonStorey2_tA0BB257062BA7E82DFE15BDD46B0E34921249A83, ___identifier_0)),static_cast<int32_t>(offsetof(U3CSetUserIdU3Ec__AnonStorey2_tA0BB257062BA7E82DFE15BDD46B0E34921249A83, ___U24this_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12185[3] = 
{
	static_cast<int32_t>(offsetof(U3CLogExceptionU3Ec__AnonStorey3_t848B0B308CEC5E67144B287AB58F9E773C4393FA, ___loggedException_0)),static_cast<int32_t>(offsetof(U3CLogExceptionU3Ec__AnonStorey3_t848B0B308CEC5E67144B287AB58F9E773C4393FA, ___frames_1)),static_cast<int32_t>(offsetof(U3CLogExceptionU3Ec__AnonStorey3_t848B0B308CEC5E67144B287AB58F9E773C4393FA, ___U24this_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12186[2] = 
{
	static_cast<int32_t>(offsetof(U3CSetCrashlyticsCollectionEnabledU3Ec__AnonStorey4_tEC1D936F42B52A3B8DB09F7440CBC7DC100EE46F, ___enabled_0)),static_cast<int32_t>(offsetof(U3CSetCrashlyticsCollectionEnabledU3Ec__AnonStorey4_tEC1D936F42B52A3B8DB09F7440CBC7DC100EE46F, ___U24this_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12187[2] = 
{
	static_cast<int32_t>(offsetof(AndroidImpl_t09BB72854905028A1DF3FBA8772392723D2CCD76, ___crashlyticsInternal_6)),static_cast<int32_t>(offsetof(AndroidImpl_t09BB72854905028A1DF3FBA8772392723D2CCD76, ___firebaseApp_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12189[7] = 
{
	static_cast<int32_t>(offsetof(ObjRootModel_t77027B651C5E22D870234B61AE6983C35444FFD1, ___U3CAllModelsU3Ek__BackingField_14)),static_cast<int32_t>(offsetof(ObjRootModel_t77027B651C5E22D870234B61AE6983C35444FFD1, ___U3CAllGeometryGroupsU3Ek__BackingField_15)),static_cast<int32_t>(offsetof(ObjRootModel_t77027B651C5E22D870234B61AE6983C35444FFD1, ___U3CAllAnimationsU3Ek__BackingField_16)),static_cast<int32_t>(offsetof(ObjRootModel_t77027B651C5E22D870234B61AE6983C35444FFD1, ___U3CAllMaterialsU3Ek__BackingField_17)),static_cast<int32_t>(offsetof(ObjRootModel_t77027B651C5E22D870234B61AE6983C35444FFD1, ___U3CAllTexturesU3Ek__BackingField_18)),static_cast<int32_t>(offsetof(ObjRootModel_t77027B651C5E22D870234B61AE6983C35444FFD1, ___U3CAllCamerasU3Ek__BackingField_19)),static_cast<int32_t>(offsetof(ObjRootModel_t77027B651C5E22D870234B61AE6983C35444FFD1, ___U3CAllLightsU3Ek__BackingField_20)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12190[1] = 
{
	static_cast<int32_t>(offsetof(ObjGeometry_t251EC78EED9A9E6E40B71FDD1D7C79DDDAE04BEB, ___MaterialName_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12191[6] = 
{
	static_cast<int32_t>(offsetof(ObjMaterial_t333DED08F3CA57CC38255E5187814EB8821FEB9A, ____properties_0)),static_cast<int32_t>(offsetof(ObjMaterial_t333DED08F3CA57CC38255E5187814EB8821FEB9A, ___U3CNameU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(ObjMaterial_t333DED08F3CA57CC38255E5187814EB8821FEB9A, ___U3CUsedU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(ObjMaterial_t333DED08F3CA57CC38255E5187814EB8821FEB9A, ___U3CIndexU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(ObjMaterial_t333DED08F3CA57CC38255E5187814EB8821FEB9A, ___U3CProcessingU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(ObjMaterial_t333DED08F3CA57CC38255E5187814EB8821FEB9A, ___U3CProcessedU3Ek__BackingField_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12192[14] = 
{
	static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CUsedU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CLocalPositionU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CLocalRotationU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CLocalScaleU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CVisibilityU3Ek__BackingField_5)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CParentU3Ek__BackingField_6)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CChildrenU3Ek__BackingField_7)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CIsBoneU3Ek__BackingField_8)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CGeometryGroupU3Ek__BackingField_9)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CBonesU3Ek__BackingField_10)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CBindPosesU3Ek__BackingField_11)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CMaterialIndicesU3Ek__BackingField_12)),static_cast<int32_t>(offsetof(ObjModel_t261A8682A53FD61A1397B579A92B856F4A1AE0D5, ___U3CUserPropertiesU3Ek__BackingField_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12193[64] = 
{
	0,0,0,0,0,static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____processedMaterials_5)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____materialLibraries_6)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____geometryGroups_7)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____models_8)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____textures_9)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____geometryAndMaterialName_10)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____geometryGroupName_11)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____materialName_12)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____objectName_13)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____activeGeometryGroup_14)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____activeGeometry_15)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____activeMaterial_16)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____activeModel_17)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____unnamedTextureCount_18)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____reader_19)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115_StaticFields, ____conversionMatrix_20)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____allVertices_21)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____allNormals_22)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____allUVs_23)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____allColors_24)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____vertexAttributes_25)),static_cast<int32_t>(offsetof(ObjProcessor_t6D411F9091F9198C857848F98F9FE9638F125115, ____floatAttributesCount_26)),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12194[14] = 
{
	0,0,0,0,0,0,0,0,0,0,0,static_cast<int32_t>(offsetof(ObjStreamReader_tB33FBA136D003FD1BB87E158012A6EAA106DEA8E, ____charStream_29)),static_cast<int32_t>(offsetof(ObjStreamReader_tB33FBA136D003FD1BB87E158012A6EAA106DEA8E, ____charString_30)),static_cast<int32_t>(offsetof(ObjStreamReader_tB33FBA136D003FD1BB87E158012A6EAA106DEA8E, ____charPosition_31)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12195[13] = 
{
	static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CUsedU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CDataU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CDataStreamU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CFilenameU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CWrapModeUU3Ek__BackingField_5)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CWrapModeVU3Ek__BackingField_6)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CTilingU3Ek__BackingField_7)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3COffsetU3Ek__BackingField_8)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CTextureIdU3Ek__BackingField_9)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CResolvedFilenameU3Ek__BackingField_10)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CHasAlphaU3Ek__BackingField_11)),static_cast<int32_t>(offsetof(ObjTexture_t2827EFF1BB3192E2D9F11FF19E00035417343D43, ___U3CTextureFormatU3Ek__BackingField_12)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12196[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12197[1] = 
{
	static_cast<int32_t>(offsetof(ObjReader_t526AA9305FE5DDF1EF48E7B3D4DBC0FFF1056BEB_StaticFields, ___ParseNumbersAsDouble_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12199[3] = 
{
	static_cast<int32_t>(offsetof(FirebaseEditorDispatcher_tC36AA65B08B38AB2721AADB81D51BA16CE86239C_StaticFields, ___U3CU3Ef__mgU24cache0_0)),static_cast<int32_t>(offsetof(FirebaseEditorDispatcher_tC36AA65B08B38AB2721AADB81D51BA16CE86239C_StaticFields, ___U3CU3Ef__mgU24cache1_1)),static_cast<int32_t>(offsetof(FirebaseEditorDispatcher_tC36AA65B08B38AB2721AADB81D51BA16CE86239C_StaticFields, ___U3CU3Ef__mgU24cache2_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12200[1] = 
{
	static_cast<int32_t>(offsetof(ApplicationFocusChangedEventArgs_t690673A946CDA5EEC650A2D8E6A2848783AC9BAD, ___U3CHasFocusU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12201[1] = 
{
	static_cast<int32_t>(offsetof(U3CCreatePartialOnMainThreadU3Ec__AnonStorey0_t910286566361BA0521D1C41AAA1B4F473AD5B2FE, ___appUtils_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12202[10] = 
{
	static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586_StaticFields, ___firebaseMonoBehaviour_0)),static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586_StaticFields, ___U3CAppUtilsU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586_StaticFields, ___tickCount_2)),static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586_StaticFields, ___U3CThreadDispatcherU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586, ___U3CIsPlayModeU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586_StaticFields, ___firebaseHandler_5)),static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586, ___Updated_6)),static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586, ___UpdatedEventWrapper_7)),static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586, ___ApplicationFocusChanged_8)),static_cast<int32_t>(offsetof(FirebaseHandler_t11BC96204B8CDE75558E6BBDB2ED05FA2979B586_StaticFields, ___U3CU3Ef__amU24cache0_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12203[2] = 
{
	static_cast<int32_t>(offsetof(FirebaseLogger_t17574FB770AB2CB02AF3459B2EF9D27FC9C24F42_StaticFields, ___incompatibleStackUnwindingEnabled_0)),static_cast<int32_t>(offsetof(FirebaseLogger_t17574FB770AB2CB02AF3459B2EF9D27FC9C24F42_StaticFields, ___U3CU3Ef__mgU24cache0_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12205[4] = 
{
	static_cast<int32_t>(offsetof(PlatformInformation_t6F0BE1D51BAD01D2A08CEC7ED766A21511F40E94_StaticFields, ___runtimeVersion_0)),static_cast<int32_t>(offsetof(PlatformInformation_t6F0BE1D51BAD01D2A08CEC7ED766A21511F40E94_StaticFields, ___U3CRealtimeSinceStartupSafeU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(PlatformInformation_t6F0BE1D51BAD01D2A08CEC7ED766A21511F40E94_StaticFields, ___U3CU3Ef__amU24cache0_2)),static_cast<int32_t>(offsetof(PlatformInformation_t6F0BE1D51BAD01D2A08CEC7ED766A21511F40E94_StaticFields, ___U3CU3Ef__amU24cache1_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12206[3] = 
{
	static_cast<int32_t>(offsetof(UnityConfigExtensions_tA9B2A38D4A5718276B9A2E776C138969F0575152_StaticFields, ____instance_5)),static_cast<int32_t>(offsetof(UnityConfigExtensions_tA9B2A38D4A5718276B9A2E776C138969F0575152_StaticFields, ___U3CU3Ef__amU24cache0_6)),static_cast<int32_t>(offsetof(UnityConfigExtensions_tA9B2A38D4A5718276B9A2E776C138969F0575152_StaticFields, ___U3CU3Ef__amU24cache1_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12207[1] = 
{
	static_cast<int32_t>(offsetof(UnityLoggingService_t6BE39D9F1F1EFE495EB479964B33753AA2C87CBB_StaticFields, ____instance_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12209[6] = 
{
	static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t222A0004720CA576CAC089134B24CBDA80693E68, ___U3CentryU3E__0_0)),static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t222A0004720CA576CAC089134B24CBDA80693E68, ___U24locvar0_1)),static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t222A0004720CA576CAC089134B24CBDA80693E68, ___U24this_2)),static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t222A0004720CA576CAC089134B24CBDA80693E68, ___U24current_3)),static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t222A0004720CA576CAC089134B24CBDA80693E68, ___U24disposing_4)),static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t222A0004720CA576CAC089134B24CBDA80693E68, ___U24PC_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12210[1] = 
{
	static_cast<int32_t>(offsetof(SynchronizationContextBehavoir_t77EF1109836806C11E4F2C8ECF2867D9531FEBBA, ___callbackQueue_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12211[5] = 
{
	static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_tC58703BC1C0EB16184A306B8AACAE68243FF19CC, ___coroutine_0)),static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_tC58703BC1C0EB16184A306B8AACAE68243FF19CC, ___newSignal_1)),static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_tC58703BC1C0EB16184A306B8AACAE68243FF19CC, ___U24current_2)),static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_tC58703BC1C0EB16184A306B8AACAE68243FF19CC, ___U24disposing_3)),static_cast<int32_t>(offsetof(U3CSignaledCoroutineU3Ec__Iterator0_tC58703BC1C0EB16184A306B8AACAE68243FF19CC, ___U24PC_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12212[2] = 
{
	static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__AnonStorey1_tB73A38DA19EEBC025199918DCC1326071057950C, ___coroutine_0)),static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__AnonStorey1_tB73A38DA19EEBC025199918DCC1326071057950C, ___U24this_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12213[2] = 
{
	static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__AnonStorey2_tF3318E0B5D0C9626BEE7239B69C936A6ADEB446E, ___newSignal_0)),static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__AnonStorey2_tF3318E0B5D0C9626BEE7239B69C936A6ADEB446E, ___U3CU3Ef__refU241_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12214[1] = 
{
	static_cast<int32_t>(offsetof(U3CSendU3Ec__AnonStorey3_t0877E086BABDA1BDD2F95E0E90A5C52922B00ABB, ___d_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12215[2] = 
{
	static_cast<int32_t>(offsetof(U3CSendU3Ec__AnonStorey4_tBD2906EC335BDD635256AEFAF7A0DD5534EB873D, ___newSignal_0)),static_cast<int32_t>(offsetof(U3CSendU3Ec__AnonStorey4_tBD2906EC335BDD635256AEFAF7A0DD5534EB873D, ___U3CU3Ef__refU243_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12216[6] = 
{
	static_cast<int32_t>(offsetof(UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690_StaticFields, ____instance_6)),static_cast<int32_t>(offsetof(UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690, ___queue_7)),static_cast<int32_t>(offsetof(UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690, ___behavior_8)),static_cast<int32_t>(offsetof(UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690, ___mainThreadId_9)),0,static_cast<int32_t>(offsetof(UnitySynchronizationContext_t39352C0C5052D313916EAAF93A309E188B745690_StaticFields, ___signalDictionary_11)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12217[7] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12218[5] = 
{
	static_cast<int32_t>(offsetof(AppConfigExtensions_t6B8627CD4EFF8F05D2F749CC406E5E12F04CEE48_StaticFields, ___DefaultUpdateUrl_0)),static_cast<int32_t>(offsetof(AppConfigExtensions_t6B8627CD4EFF8F05D2F749CC406E5E12F04CEE48_StaticFields, ___Default_1)),static_cast<int32_t>(offsetof(AppConfigExtensions_t6B8627CD4EFF8F05D2F749CC406E5E12F04CEE48_StaticFields, ___Sync_2)),static_cast<int32_t>(offsetof(AppConfigExtensions_t6B8627CD4EFF8F05D2F749CC406E5E12F04CEE48_StaticFields, ____instance_3)),static_cast<int32_t>(offsetof(AppConfigExtensions_t6B8627CD4EFF8F05D2F749CC406E5E12F04CEE48_StaticFields, ___SStringState_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12219[1] = 
{
	static_cast<int32_t>(offsetof(SystemClock_t56839C5A7890CD3F89A336EE0173FAA84AB3FA13_StaticFields, ___Instance_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12220[1] = 
{
	static_cast<int32_t>(offsetof(ExceptionAggregator_t79259EF5E7CCF32340080CFAC267848A89DA90E3_ThreadStaticFields, ___threadLocalExceptions_0)) | THREAD_LOCAL_STATIC_MASK,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12224[1] = 
{
	static_cast<int32_t>(offsetof(DebugLogger_t06BB6B0EBB4D36FCB961CCF80EE1FE4F0A88B4C1_StaticFields, ____instance_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12225[3] = 
{
	static_cast<int32_t>(offsetof(Services_t4EDE95322734B379A0DA3AAEF079E28E9132D69E_StaticFields, ___U3CAppConfigU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(Services_t4EDE95322734B379A0DA3AAEF079E28E9132D69E_StaticFields, ___U3CClockU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(Services_t4EDE95322734B379A0DA3AAEF079E28E9132D69E_StaticFields, ___U3CLoggingU3Ek__BackingField_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12226[2] = 
{
	0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12227[3] = 
{
	0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12228[2] = 
{
	0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12229[2] = 
{
	static_cast<int32_t>(offsetof(Dispatcher_tBD1370511B5D6C10B211FBD3AF8E4F9D6B3CD8C6, ___ownerThreadId_0)),static_cast<int32_t>(offsetof(Dispatcher_tBD1370511B5D6C10B211FBD3AF8E4F9D6B3CD8C6, ___queue_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12230[7] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12232[1] = 
{
	static_cast<int32_t>(offsetof(FirebaseAppUtilsStub_tA2C798DD03529908BA1CC968E5CD737BA58F5A02_StaticFields, ____instance_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12234[3] = 
{
	0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12236[1] = 
{
	static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7Bf1ff318eU2D91e6U2D436aU2D9a52U2Dac02be4f7d0eU7D_t54B8FF32EAD70318A421BB5FB5ECE4E1E50CE79C_StaticFields, ___U24fieldU2D0_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12238[21] = 
{
	static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_FaceIndex_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_FamilyName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_StyleName_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_PointSize_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_Scale_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_UnitsPerEM_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_LineHeight_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_AscentLine_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_CapLine_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_MeanLine_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_Baseline_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_DescentLine_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_SuperscriptOffset_12)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_SuperscriptSize_13)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_SubscriptOffset_14)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_SubscriptSize_15)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_UnderlineOffset_16)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_UnderlineThickness_17)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_StrikethroughOffset_18)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_StrikethroughThickness_19)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FaceInfo_t12F0319E555A62CBA1D9E51A16C7963393932756, ___m_TabWidth_20)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12239[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12240[5] = 
{
	static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D, ___m_X_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D, ___m_Y_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D, ___m_Width_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D, ___m_Height_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphRect_tB6D225B9318A527A1CBC1B4078EB923398EB808D_StaticFields, ___s_ZeroGlyphRect_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12241[5] = 
{
	static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_Width_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_Height_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_HorizontalBearingX_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_HorizontalBearingY_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMetrics_t6C1C65A891A6279A0EE807C436436B1E44F7AF1A, ___m_HorizontalAdvance_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12242[6] = 
{
	static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_Index_0)),static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_Metrics_1)),static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_GlyphRect_2)),static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_Scale_3)),static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_AtlasIndex_4)),static_cast<int32_t>(offsetof(Glyph_t700CF8EBE04ED4AEAB520885AAA1B309E02A103F, ___m_ClassDefinitionType_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12243[6] = 
{
	static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___index_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___metrics_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___glyphRect_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___scale_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___atlasIndex_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphMarshallingStruct_tB45F92185E1A4A7880004B36591D7C73E4A2B87C, ___classDefinitionType_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12244[12] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12245[16] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12246[13] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12247[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12248[4] = 
{
	static_cast<int32_t>(offsetof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172, ___familyName_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172, ___styleName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172, ___faceIndex_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(FontReference_t550791D5AA7787156C3A229C65D7E1AC4BF76172, ___filePath_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12249[8] = 
{
	static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_Glyphs_0)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_GlyphIndexes_MarshallingArray_A_1)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_GlyphMarshallingStruct_IN_2)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_GlyphMarshallingStruct_OUT_3)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_FreeGlyphRects_4)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_UsedGlyphRects_5)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_PairAdjustmentRecords_MarshallingArray_6)),static_cast<int32_t>(offsetof(FontEngine_t4B8F87CAA77860B55B0C7FDF85FBBE178E2D5B7A_StaticFields, ___s_GlyphLookupDictionary_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12251[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12252[4] = 
{
	static_cast<int32_t>(offsetof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E, ___m_XPlacement_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E, ___m_YPlacement_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E, ___m_XAdvance_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphValueRecord_t780927A39D46924E0D546A2AE5DDF1BB2B5A9C8E, ___m_YAdvance_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12253[2] = 
{
	static_cast<int32_t>(offsetof(GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7, ___m_GlyphIndex_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphAdjustmentRecord_tC7A1B2E0AC7C4ED9CDB8E95E48790A46B6F315F7, ___m_GlyphValueRecord_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12254[3] = 
{
	static_cast<int32_t>(offsetof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E, ___m_FirstAdjustmentRecord_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E, ___m_SecondAdjustmentRecord_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GlyphPairAdjustmentRecord_t6E4295094D349DBF22BC59116FBC8F22EA55420E, ___m_FeatureLookupFlags_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12258[1] = 
{
	static_cast<int32_t>(offsetof(NullableAttribute_tCBFFAB20AD9E68064976C7DC9D9AD53E3130BA14, ___NullableFlags_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12259[1] = 
{
	static_cast<int32_t>(offsetof(NullableContextAttribute_t0F2E3CF46D8AB038E589C88FA392D5D8B69150FE, ___Flag_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12260[1] = 
{
	static_cast<int32_t>(offsetof(NullablePublicOnlyAttribute_t76ED56E08ED096A598020BA0FF5FD30635089077, ___IncludesInternals_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12264[1] = 
{
	static_cast<int32_t>(offsetof(UnicodeHelpers_t5A5A5BBFB4BD837753ED0CAC899C31F43C701F71_StaticFields, ____definedCharacterBitmapBigEndian_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12265[2] = 
{
	static_cast<int32_t>(offsetof(UnicodeRange_t39A4F977F558B4F00AFE470AD6573FA988BDA836, ___U3CFirstCodePointU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(UnicodeRange_t39A4F977F558B4F00AFE470AD6573FA988BDA836, ___U3CLengthU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12266[1] = 
{
	static_cast<int32_t>(offsetof(UnicodeRanges_tE4E52421F705A0AC65C0E17C05CA14DA03E037C7_StaticFields, ____u0000_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12267[1] = 
{
	static_cast<int32_t>(offsetof(AllowedCharactersBitmap_tA1BC142B179A112F4EAA6A6BF578B0BFD58E8BED, ____allowedCharacters_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12268[7] = 
{
	static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C_StaticFields, ___s_singleton_4)),static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C_StaticFields, ___s_b_5)),static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C_StaticFields, ___s_t_6)),static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C_StaticFields, ___s_n_7)),static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C_StaticFields, ___s_f_8)),static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C_StaticFields, ___s_r_9)),static_cast<int32_t>(offsetof(DefaultJavaScriptEncoderBasicLatin_t9C31BFC6663FA80C9B05E2964FAEEEDA77636E7C_StaticFields, ___s_back_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12270[6] = 
{
	static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t65341C0036795EED811809CFA8956E9B170C0CAE, ____allowedCharacters_4)),static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t65341C0036795EED811809CFA8956E9B170C0CAE_StaticFields, ___Singleton_5)),static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t65341C0036795EED811809CFA8956E9B170C0CAE_StaticFields, ___s_quote_6)),static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t65341C0036795EED811809CFA8956E9B170C0CAE_StaticFields, ___s_ampersand_7)),static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t65341C0036795EED811809CFA8956E9B170C0CAE_StaticFields, ___s_lessthan_8)),static_cast<int32_t>(offsetof(DefaultHtmlEncoder_t65341C0036795EED811809CFA8956E9B170C0CAE_StaticFields, ___s_greaterthan_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12273[1] = 
{
	static_cast<int32_t>(offsetof(U3CDataU3Ee__FixedBuffer_t20CBAE413C47188F7794C5CEEB13B732F34A5D37, ___FixedElementField_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12274[1] = 
{
	static_cast<int32_t>(offsetof(AsciiNeedsEscapingData_t00F9B997CA974017BCD982A68921BC34F4D1C2FC, ___Data_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12275[4] = 
{
	static_cast<int32_t>(offsetof(TextEncoder_t35B2FDACE089FFCF6F1460C93866D1271356E691, ____asciiEscape_0)),static_cast<int32_t>(offsetof(TextEncoder_t35B2FDACE089FFCF6F1460C93866D1271356E691, ____isAsciiCacheInitialized_1)),static_cast<int32_t>(offsetof(TextEncoder_t35B2FDACE089FFCF6F1460C93866D1271356E691, ____asciiNeedsEscaping_2)),static_cast<int32_t>(offsetof(TextEncoder_t35B2FDACE089FFCF6F1460C93866D1271356E691_StaticFields, ___s_noEscape_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12276[1] = 
{
	static_cast<int32_t>(offsetof(TextEncoderSettings_t893E26A25D5CD77AC6CD12AAF45CC8C14AB1296B, ____allowedCharactersBitmap_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12279[2] = 
{
	static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t03A72430945148CB9D382B7B249DAE2DDF6F2843_StaticFields, ___8B30AFDCF07C4ABDFE0FAF65F79FC40A2E9AC497C42B1BA5C996BDFB3F6EC2F6_0)),static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t03A72430945148CB9D382B7B249DAE2DDF6F2843_StaticFields, ___EFE627BE173681E4F55F4133AB4C1782E26D1080CB80CDB6BFAAC81416A2714E_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12283[1] = 
{
	static_cast<int32_t>(offsetof(U3CMessageReceivedDelegateMethodU3Ec__AnonStorey0_t522D1A0BD277B4D9560EE11B8C4B6147D72D9C6C, ___message_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12284[1] = 
{
	static_cast<int32_t>(offsetof(U3CTokenReceivedDelegateMethodU3Ec__AnonStorey1_t7B9B1F8EA01DA9EDF63D27A60D3D214AC248BD48, ___token_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12285[4] = 
{
	static_cast<int32_t>(offsetof(Listener_tC16E83118F05783B5CA41A3F81ABF1A2C83D7BF7, ___messageReceivedDelegate_0)),static_cast<int32_t>(offsetof(Listener_tC16E83118F05783B5CA41A3F81ABF1A2C83D7BF7, ___tokenReceivedDelegate_1)),static_cast<int32_t>(offsetof(Listener_tC16E83118F05783B5CA41A3F81ABF1A2C83D7BF7, ___app_2)),static_cast<int32_t>(offsetof(Listener_tC16E83118F05783B5CA41A3F81ABF1A2C83D7BF7_StaticFields, ___listener_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12286[3] = 
{
	static_cast<int32_t>(offsetof(FirebaseMessaging_tAF3E6E43968D1C156E887454B8F9BDB7723036D3_StaticFields, ___MessageReceivedInternal_0)),static_cast<int32_t>(offsetof(FirebaseMessaging_tAF3E6E43968D1C156E887454B8F9BDB7723036D3_StaticFields, ___TokenReceivedInternal_1)),static_cast<int32_t>(offsetof(FirebaseMessaging_tAF3E6E43968D1C156E887454B8F9BDB7723036D3_StaticFields, ___listener_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12289[14] = 
{
	static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___applicationDelegate_0)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___arithmeticDelegate_1)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___divideByZeroDelegate_2)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___indexOutOfRangeDelegate_3)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___invalidCastDelegate_4)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___invalidOperationDelegate_5)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___ioDelegate_6)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___nullReferenceDelegate_7)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___outOfMemoryDelegate_8)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___overflowDelegate_9)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___systemDelegate_10)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___argumentDelegate_11)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___argumentNullDelegate_12)),static_cast<int32_t>(offsetof(SWIGExceptionHelper_t1232776ACBAF1CAC4A098D8C4420E1570424930C_StaticFields, ___argumentOutOfRangeDelegate_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12290[2] = 
{
	static_cast<int32_t>(offsetof(SWIGPendingException_t0409902F046D41D7A8D2333129C7E51E76EC1909_ThreadStaticFields, ___pendingException_0)) | THREAD_LOCAL_STATIC_MASK,static_cast<int32_t>(offsetof(SWIGPendingException_t0409902F046D41D7A8D2333129C7E51E76EC1909_StaticFields, ___numExceptionsPending_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12292[1] = 
{
	static_cast<int32_t>(offsetof(SWIGStringHelper_t953C1EA2870BB0BF9A25500F62877BBBA6D44AE9_StaticFields, ___stringDelegate_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12293[2] = 
{
	static_cast<int32_t>(offsetof(FirebaseMessagingPINVOKE_t8D162519505D2E2C12BFCD6037CFE376283ABF4F_StaticFields, ___swigExceptionHelper_0)),static_cast<int32_t>(offsetof(FirebaseMessagingPINVOKE_t8D162519505D2E2C12BFCD6037CFE376283ABF4F_StaticFields, ___swigStringHelper_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12294[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12295[3] = 
{
	static_cast<int32_t>(offsetof(FirebaseMessage_t09E58AB584806FC5C23F419AD7CDCE53969FE20F, ___swigCPtr_0)),static_cast<int32_t>(offsetof(FirebaseMessage_t09E58AB584806FC5C23F419AD7CDCE53969FE20F, ___swigCMemOwn_1)),static_cast<int32_t>(offsetof(FirebaseMessage_t09E58AB584806FC5C23F419AD7CDCE53969FE20F_StaticFields, ___UnixEpochUtc_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12296[2] = 
{
	static_cast<int32_t>(offsetof(FirebaseNotification_t17D8D8BE158C522C9DBA575BB7295EBE3413EB0A, ___swigCPtr_0)),static_cast<int32_t>(offsetof(FirebaseNotification_t17D8D8BE158C522C9DBA575BB7295EBE3413EB0A, ___swigCMemOwn_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12297[2] = 
{
	static_cast<int32_t>(offsetof(AndroidNotificationParams_t5D31ABDF15208A4697A746B167C7C87A20CD5B1A, ___swigCPtr_0)),static_cast<int32_t>(offsetof(AndroidNotificationParams_t5D31ABDF15208A4697A746B167C7C87A20CD5B1A, ___swigCMemOwn_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12298[2] = 
{
	static_cast<int32_t>(offsetof(MessagingOptions_t8815B50D7DA2B2B7E1A353CD49EAE4E81997E65B, ___swigCPtr_0)),static_cast<int32_t>(offsetof(MessagingOptions_t8815B50D7DA2B2B7E1A353CD49EAE4E81997E65B, ___swigCMemOwn_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12299[1] = 
{
	static_cast<int32_t>(offsetof(MessageReceivedEventArgs_tAFDBCC69B3DEC0F5EB76ACAA8D97A404E61F73E5, ___U3CMessageU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12300[1] = 
{
	static_cast<int32_t>(offsetof(TokenReceivedEventArgs_t91FA07EA184701BF44700D7581779A5C84C7B98C, ___U3CTokenU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12302[2] = 
{
	static_cast<int32_t>(offsetof(ThreeMfGeometry_t0B0D7084532A5FB23D4AE3C1263F1ACCA896B9A5, ___ThreeMfTexture2D_8)),static_cast<int32_t>(offsetof(ThreeMfGeometry_t0B0D7084532A5FB23D4AE3C1263F1ACCA896B9A5, ___ThreeMfBase_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12303[6] = 
{
	static_cast<int32_t>(offsetof(ThreeMfMaterial_tFBC1F52BB8A3BE4740BAF19E80D1C1C52F312424, ____properties_0)),static_cast<int32_t>(offsetof(ThreeMfMaterial_tFBC1F52BB8A3BE4740BAF19E80D1C1C52F312424, ___U3CNameU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(ThreeMfMaterial_tFBC1F52BB8A3BE4740BAF19E80D1C1C52F312424, ___U3CUsedU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(ThreeMfMaterial_tFBC1F52BB8A3BE4740BAF19E80D1C1C52F312424, ___U3CIndexU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(ThreeMfMaterial_tFBC1F52BB8A3BE4740BAF19E80D1C1C52F312424, ___U3CProcessingU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(ThreeMfMaterial_tFBC1F52BB8A3BE4740BAF19E80D1C1C52F312424, ___U3CProcessedU3Ek__BackingField_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12304[14] = 
{
	static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CUsedU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CLocalPositionU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CLocalRotationU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CLocalScaleU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CVisibilityU3Ek__BackingField_5)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CParentU3Ek__BackingField_6)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CChildrenU3Ek__BackingField_7)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CBonesU3Ek__BackingField_8)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CIsBoneU3Ek__BackingField_9)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CGeometryGroupU3Ek__BackingField_10)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CBindPosesU3Ek__BackingField_11)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CMaterialIndicesU3Ek__BackingField_12)),static_cast<int32_t>(offsetof(ThreeMfModel_tF653F9D3172EEAD38A40A337F46122BD8E4B9EA2, ___U3CUserPropertiesU3Ek__BackingField_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12305[9] = 
{
	static_cast<int32_t>(offsetof(U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields, ___U3CU3E9_0)),static_cast<int32_t>(offsetof(U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields, ___U3CU3E9__12_0_1)),static_cast<int32_t>(offsetof(U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields, ___U3CU3E9__12_1_2)),static_cast<int32_t>(offsetof(U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields, ___U3CU3E9__12_2_3)),static_cast<int32_t>(offsetof(U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields, ___U3CU3E9__12_3_4)),static_cast<int32_t>(offsetof(U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields, ___U3CU3E9__12_4_5)),static_cast<int32_t>(offsetof(U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields, ___U3CU3E9__12_5_6)),static_cast<int32_t>(offsetof(U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields, ___U3CU3E9__12_6_7)),static_cast<int32_t>(offsetof(U3CU3Ec_t93727D07E308392C9B39D16C76FA85A588DD2138_StaticFields, ___U3CU3E9__12_7_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12306[11] = 
{
	0,0,0,0,static_cast<int32_t>(offsetof(ThreeMfProcessor_t15100D0D50076EC723D3D17136EE2280EB2A4E8C, ____reader_4)),static_cast<int32_t>(offsetof(ThreeMfProcessor_t15100D0D50076EC723D3D17136EE2280EB2A4E8C, ____rootModel_5)),static_cast<int32_t>(offsetof(ThreeMfProcessor_t15100D0D50076EC723D3D17136EE2280EB2A4E8C, ____allTextures_6)),static_cast<int32_t>(offsetof(ThreeMfProcessor_t15100D0D50076EC723D3D17136EE2280EB2A4E8C, ____allMaterials_7)),static_cast<int32_t>(offsetof(ThreeMfProcessor_t15100D0D50076EC723D3D17136EE2280EB2A4E8C, ____allTextureMaterials_8)),static_cast<int32_t>(offsetof(ThreeMfProcessor_t15100D0D50076EC723D3D17136EE2280EB2A4E8C, ____allConvertedColorGroups_9)),static_cast<int32_t>(offsetof(ThreeMfProcessor_t15100D0D50076EC723D3D17136EE2280EB2A4E8C, ____allConvertedCoordinates_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12307[8] = 
{
	static_cast<int32_t>(offsetof(ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396, ___U3CAllModelsU3Ek__BackingField_14)),static_cast<int32_t>(offsetof(ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396, ___U3CAllGeometryGroupsU3Ek__BackingField_15)),static_cast<int32_t>(offsetof(ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396, ___U3CAllAnimationsU3Ek__BackingField_16)),static_cast<int32_t>(offsetof(ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396, ___U3CAllMaterialsU3Ek__BackingField_17)),static_cast<int32_t>(offsetof(ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396, ___U3CAllTexturesU3Ek__BackingField_18)),static_cast<int32_t>(offsetof(ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396, ___U3CAllCamerasU3Ek__BackingField_19)),static_cast<int32_t>(offsetof(ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396, ___U3CAllLightsU3Ek__BackingField_20)),static_cast<int32_t>(offsetof(ThreeMfRootModel_tA1A2F771FE4577430FBD17B425744DD79BFFB396, ___File_21)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12308[14] = 
{
	static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___Coordinates_0)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CNameU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CUsedU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CDataU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CDataStreamU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CFilenameU3Ek__BackingField_5)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CWrapModeUU3Ek__BackingField_6)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CWrapModeVU3Ek__BackingField_7)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CTilingU3Ek__BackingField_8)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3COffsetU3Ek__BackingField_9)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CTextureIdU3Ek__BackingField_10)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CResolvedFilenameU3Ek__BackingField_11)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CHasAlphaU3Ek__BackingField_12)),static_cast<int32_t>(offsetof(ThreeMfTexture_t35269C4A3BD353755DDEDC7A3C9C3AA5039FFF22, ___U3CTextureFormatU3Ek__BackingField_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12309[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12310[2] = 
{
	static_cast<int32_t>(offsetof(ThreeMfReader_t8F4D229EE9D88E8F68C2AC4FA06C66477A7545C1, ___U3CModelCountU3Ek__BackingField_10)),static_cast<int32_t>(offsetof(ThreeMfReader_t8F4D229EE9D88E8F68C2AC4FA06C66477A7545C1_StaticFields, ___ThreeMfConversionPrecision_11)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12312[1] = 
{
	static_cast<int32_t>(offsetof(RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_StaticFields, ___s_Corners_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12313[1] = 
{
	static_cast<int32_t>(offsetof(CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860, ___U3CisMaskU3Ek__BackingField_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12314[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12315[7] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12317[5] = 
{
	static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___preWillRenderCanvases_4)),static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___willRenderCanvases_5)),static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___U3CexternBeginRenderOverlaysU3Ek__BackingField_6)),static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___U3CexternRenderOverlaysBeforeU3Ek__BackingField_7)),static_cast<int32_t>(offsetof(Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields, ___U3CexternEndRenderOverlaysU3Ek__BackingField_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12318[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12323[1] = 
{
	static_cast<int32_t>(offsetof(PhysicsScene2D_t550D023B9E77BE6844564BB4F9FA291EEA10FDC9, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12324[1] = 
{
	static_cast<int32_t>(offsetof(Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_StaticFields, ___m_LastDisabledRigidbody2D_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12325[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12326[11] = 
{
	static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___useTriggers_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___useLayerMask_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___useDepth_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___useOutsideDepth_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___useNormalAngle_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___useOutsideNormalAngle_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___layerMask_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___minDepth_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___maxDepth_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___minNormalAngle_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactFilter2D_t54A8515C326BF7DA16E5DE97EA7D3CD9B2F77F14, ___maxNormalAngle_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12327[9] = 
{
	static_cast<int32_t>(offsetof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B, ___m_Collider_0)),static_cast<int32_t>(offsetof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B, ___m_OtherCollider_1)),static_cast<int32_t>(offsetof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B, ___m_Rigidbody_2)),static_cast<int32_t>(offsetof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B, ___m_OtherRigidbody_3)),static_cast<int32_t>(offsetof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B, ___m_RelativeVelocity_4)),static_cast<int32_t>(offsetof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B, ___m_Enabled_5)),static_cast<int32_t>(offsetof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B, ___m_ContactCount_6)),static_cast<int32_t>(offsetof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B, ___m_ReusedContacts_7)),static_cast<int32_t>(offsetof(Collision2D_t81E83212C969FDDE2AB84EBCA31502818EEAB85B, ___m_LegacyContacts_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12328[11] = 
{
	static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_Point_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_Normal_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_RelativeVelocity_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_Separation_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_NormalImpulse_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_TangentImpulse_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_Collider_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_OtherCollider_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_Rigidbody_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_OtherRigidbody_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint2D_t16A7EE2DDFB4FA3A09C6554E11F30CEDAEBFA801, ___m_Enabled_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12329[6] = 
{
	static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Centroid_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Point_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Normal_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Distance_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Fraction_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA, ___m_Collider_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12334[2] = 
{
	static_cast<int32_t>(offsetof(Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields, ___ContactModifyEvent_0)),static_cast<int32_t>(offsetof(Physics_t1244C2983AEAFA149425AFFC3DF53BC91C18ED56_StaticFields, ___ContactModifyEventCCD_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12335[10] = 
{
	static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___actor_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___otherActor_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___shape_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___otherShape_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___rotation_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___position_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___otherRotation_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___otherPosition_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___numContacts_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ModifiableContactPair_t8D3CA3E20AF1718A5421A6098D633DDA67399960, ___contacts_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12336[6] = 
{
	static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_Point_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_Normal_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_FaceID_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_Distance_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_UV_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5, ___m_Collider_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12340[7] = 
{
	static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_Impulse_0)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_RelativeVelocity_1)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_Body_2)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_Collider_3)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_ContactCount_4)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_ReusedContacts_5)),static_cast<int32_t>(offsetof(Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0, ___m_LegacyContacts_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12345[5] = 
{
	static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_Point_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_Normal_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_ThisColliderInstanceID_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_OtherColliderInstanceID_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9, ___m_Separation_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12346[1] = 
{
	static_cast<int32_t>(offsetof(PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12347[7] = 
{
	static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Controller_0)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Collider_1)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Point_2)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Normal_3)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_MoveDirection_4)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_MoveLength_5)),static_cast<int32_t>(offsetof(ControllerColliderHit_tD0B734CBE0E2B089339B14600EB5A80295F6DE92, ___m_Push_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12348[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12353[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12354[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12355[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12356[14] = 
{
	static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_FingerId_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Position_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_RawPosition_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_PositionDelta_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_TimeDelta_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_TapCount_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Phase_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Type_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Pressure_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_maximumPossiblePressure_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_Radius_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_RadiusVariance_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_AltitudeAngle_12)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Touch_t03E51455ED508492B3F278903A0114FA0E87B417, ___m_AzimuthAngle_13)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12357[8] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12358[1] = 
{
	static_cast<int32_t>(offsetof(Gyroscope_tA4CEC0F47FFB4CEB90410CC6B860D052BB35BE9E, ___m_GyroIndex_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12359[5] = 
{
	static_cast<int32_t>(offsetof(HeadingInfo_tDE66705847BB57D5815F1D570980E7C306B513BC, ___magneticHeading_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HeadingInfo_tDE66705847BB57D5815F1D570980E7C306B513BC, ___trueHeading_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HeadingInfo_tDE66705847BB57D5815F1D570980E7C306B513BC, ___headingAccuracy_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HeadingInfo_tDE66705847BB57D5815F1D570980E7C306B513BC, ___raw_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HeadingInfo_tDE66705847BB57D5815F1D570980E7C306B513BC, ___timestamp_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12363[2] = 
{
	static_cast<int32_t>(offsetof(Input_t47D83E2A50E6AF7F8A47AA06FBEF9EBE6BBC22BB_StaticFields, ___compassInstance_0)),static_cast<int32_t>(offsetof(Input_t47D83E2A50E6AF7F8A47AA06FBEF9EBE6BBC22BB_StaticFields, ___s_MainGyro_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12364[2] = 
{
	static_cast<int32_t>(offsetof(HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314, ___target_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(HitInfo_t34AF939575E1C059D581AB7ED8F039BCFFC70314, ___camera_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12365[9] = 
{
	static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_MouseUsed_0)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___m_LastHit_1)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___m_MouseDownHit_2)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___m_CurrentHit_3)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___m_Cameras_4)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_GetMouseState_5)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_MousePosition_6)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_MouseButtonPressedThisFrame_7)),static_cast<int32_t>(offsetof(SendMouseEvents_t30F6848ABBD277C51340A02CF6664B8D6183EC39_StaticFields, ___s_MouseButtonIsPressed_8)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12367[1] = 
{
	static_cast<int32_t>(offsetof(IntegratedSubsystemDescriptor_t9232963B842E01748A8E032928DC8E35DF00C10D, ___m_Ptr_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12370[1] = 
{
	static_cast<int32_t>(offsetof(SubsystemDescriptor_tF417D2751C69A8B0DD86162EBCE55F84D3493A71, ___U3CidU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12372[2] = 
{
	static_cast<int32_t>(offsetof(IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3, ___m_Ptr_0)),static_cast<int32_t>(offsetof(IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3, ___m_SubsystemDescriptor_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12376[7] = 
{
	static_cast<int32_t>(offsetof(SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields, ___reloadSubsytemsStarted_0)),static_cast<int32_t>(offsetof(SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields, ___reloadSubsytemsCompleted_1)),static_cast<int32_t>(offsetof(SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields, ___beforeReloadSubsystems_2)),static_cast<int32_t>(offsetof(SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields, ___afterReloadSubsystems_3)),static_cast<int32_t>(offsetof(SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields, ___s_IntegratedSubsystems_4)),static_cast<int32_t>(offsetof(SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields, ___s_StandaloneSubsystems_5)),static_cast<int32_t>(offsetof(SubsystemManager_t9A7261E4D0B53B996F04B8707D8E1C33AB65E824_StaticFields, ___s_DeprecatedSubsystems_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12380[3] = 
{
	static_cast<int32_t>(offsetof(SubsystemDescriptorStore_tEF3761B84B8C25EA4B93F94A487551820B268250_StaticFields, ___s_IntegratedDescriptors_0)),static_cast<int32_t>(offsetof(SubsystemDescriptorStore_tEF3761B84B8C25EA4B93F94A487551820B268250_StaticFields, ___s_StandaloneDescriptors_1)),static_cast<int32_t>(offsetof(SubsystemDescriptorStore_tEF3761B84B8C25EA4B93F94A487551820B268250_StaticFields, ___s_DeprecatedDescriptors_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12381[3] = 
{
	static_cast<int32_t>(offsetof(SubsystemDescriptorWithProvider_t2A61A2C951A4A179E898CF207726BF6B5AF474D5, ___U3CidU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(SubsystemDescriptorWithProvider_t2A61A2C951A4A179E898CF207726BF6B5AF474D5, ___U3CproviderTypeU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(SubsystemDescriptorWithProvider_t2A61A2C951A4A179E898CF207726BF6B5AF474D5, ___U3CsubsystemTypeOverrideU3Ek__BackingField_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12383[2] = 
{
	static_cast<int32_t>(offsetof(SubsystemWithProvider_tC72E35EE2D413A4B0635B058154BABF265F31242, ___U3CrunningU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(SubsystemWithProvider_tC72E35EE2D413A4B0635B058154BABF265F31242, ___U3CproviderBaseU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12384[2] = 
{
	0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12385[1] = 
{
	static_cast<int32_t>(offsetof(SubsystemProvider_tE1865B8FE698C81A59AED35E0E536BD53F402455, ___m_Running_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12388[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12389[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12390[18] = 
{
	static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___font_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___color_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___fontSize_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___lineSpacing_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___richText_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___scaleFactor_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___fontStyle_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___textAnchor_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___alignByGeometry_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___resizeTextForBestFit_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___resizeTextMinSize_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___resizeTextMaxSize_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___updateBounds_12)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___verticalOverflow_13)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___horizontalOverflow_14)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___generationExtents_15)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___pivot_16)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(TextGenerationSettings_tBB6E86AC0B348D19158D6721BE790865B04993F3, ___generateOutOfBounds_17)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12391[11] = 
{
	static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_Ptr_0)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_LastString_1)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_LastSettings_2)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_HasGenerated_3)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_LastValid_4)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_Verts_5)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_Characters_6)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_Lines_7)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_CachedVerts_8)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_CachedCharacters_9)),static_cast<int32_t>(offsetof(TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC, ___m_CachedLines_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12392[10] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12393[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12394[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12395[2] = 
{
	static_cast<int32_t>(offsetof(UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD, ___cursorPos_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD, ___charWidth_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12396[4] = 
{
	static_cast<int32_t>(offsetof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC, ___startCharIdx_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC, ___height_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC, ___topY_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC, ___leading_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12397[11] = 
{
	static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___position_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___normal_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___tangent_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___color_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___uv0_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___uv1_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___uv2_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207, ___uv3_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields, ___s_DefaultColor_8)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields, ___s_DefaultTangent_9)),static_cast<int32_t>(offsetof(UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields, ___simpleVert_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12399[2] = 
{
	static_cast<int32_t>(offsetof(Font_tC95270EA3198038970422D78B74A7F2E218A96B6_StaticFields, ___textureRebuilt_4)),static_cast<int32_t>(offsetof(Font_tC95270EA3198038970422D78B74A7F2E218A96B6, ___m_FontTextureRebuildCallback_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12401[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12403[3] = 
{
	static_cast<int32_t>(offsetof(Mobile_t304A73480DF447472BDB16BA19A9E4FE2C8CB2DD_StaticFields, ___U3CmuteStateU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(Mobile_t304A73480DF447472BDB16BA19A9E4FE2C8CB2DD_StaticFields, ____stopAudioOutputOnMute_1)),static_cast<int32_t>(offsetof(Mobile_t304A73480DF447472BDB16BA19A9E4FE2C8CB2DD_StaticFields, ___OnMuteStateChanged_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12404[3] = 
{
	static_cast<int32_t>(offsetof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields, ___OnAudioConfigurationChanged_0)),static_cast<int32_t>(offsetof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields, ___OnAudioSystemShuttingDown_1)),static_cast<int32_t>(offsetof(AudioSettings_t66C4BCA1E463B061E2EC9063FB882ACED20D47BD_StaticFields, ___OnAudioSystemStartedUp_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12407[2] = 
{
	static_cast<int32_t>(offsetof(AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20, ___m_PCMReaderCallback_4)),static_cast<int32_t>(offsetof(AudioClip_t5D272C4EB4F2D3ED49F1C346DEA373CF6D585F20, ___m_PCMSetPositionCallback_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12411[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12412[5] = 
{
	static_cast<int32_t>(offsetof(WebCamDevice_t3D72A89B7F52FD27192023A67254D19E89D9738C, ___m_Name_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(WebCamDevice_t3D72A89B7F52FD27192023A67254D19E89D9738C, ___m_DepthCameraName_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(WebCamDevice_t3D72A89B7F52FD27192023A67254D19E89D9738C, ___m_Flags_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(WebCamDevice_t3D72A89B7F52FD27192023A67254D19E89D9738C, ___m_Kind_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(WebCamDevice_t3D72A89B7F52FD27192023A67254D19E89D9738C, ___m_Resolutions_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12414[1] = 
{
	static_cast<int32_t>(offsetof(AudioPlayableOutput_tC3DFF8095F429D90129A367EAB98A24F6D6ADF20, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12416[1] = 
{
	static_cast<int32_t>(offsetof(AudioClipPlayable_tD4B758E68CAE03CB0CD31F90C8A3E603B97143A0, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12417[1] = 
{
	static_cast<int32_t>(offsetof(AudioMixerPlayable_t6AADDF0C53DF1B4C17969EC24B3B4E4975F3A56C, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12419[2] = 
{
	static_cast<int32_t>(offsetof(AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2, ___sampleFramesAvailable_0)),static_cast<int32_t>(offsetof(AudioSampleProvider_t602353124A2F6F2AEC38E56C3C21932344F712E2, ___sampleFramesOverflow_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12422[14] = 
{
	static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CUsedU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CLocalPositionU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CLocalRotationU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CLocalScaleU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CVisibilityU3Ek__BackingField_5)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CParentU3Ek__BackingField_6)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CChildrenU3Ek__BackingField_7)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CIsBoneU3Ek__BackingField_8)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CGeometryGroupU3Ek__BackingField_9)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CBonesU3Ek__BackingField_10)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CBindPosesU3Ek__BackingField_11)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CMaterialIndicesU3Ek__BackingField_12)),static_cast<int32_t>(offsetof(StlModel_t8BF6A6BC59B892CCDD30EBFD91DA699224ADD9EA, ___U3CUserPropertiesU3Ek__BackingField_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12423[25] = 
{
	0,0,0,static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____geometryGroups_3)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____models_4)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____reader_5)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____groupName_6)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____lastGeometryGroupName_7)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____lastLoopNumber_8)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____activeGeometryGroup_9)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____activeGeometry_10)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____loopNumber_11)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____facetNormal_12)),0,0,0,0,0,0,0,0,static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____partColor_21)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____facetColor_22)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____floatCount_23)),static_cast<int32_t>(offsetof(StlProcessor_tFF16A2DB05BAC4F11FAB8C8867950687C61995B0, ____vertexAttributes_24)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12424[7] = 
{
	static_cast<int32_t>(offsetof(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142, ___U3CAllModelsU3Ek__BackingField_14)),static_cast<int32_t>(offsetof(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142, ___U3CAllGeometryGroupsU3Ek__BackingField_15)),static_cast<int32_t>(offsetof(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142, ___U3CAllAnimationsU3Ek__BackingField_16)),static_cast<int32_t>(offsetof(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142, ___U3CAllMaterialsU3Ek__BackingField_17)),static_cast<int32_t>(offsetof(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142, ___U3CAllTexturesU3Ek__BackingField_18)),static_cast<int32_t>(offsetof(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142, ___U3CAllCamerasU3Ek__BackingField_19)),static_cast<int32_t>(offsetof(StlRootModel_tDC41670D4D8AA87163E57BC13EED265B48D3A142, ___U3CAllLightsU3Ek__BackingField_20)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12425[10] = 
{
	0,0,0,0,static_cast<int32_t>(offsetof(StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5, ____endOfLinePointer_22)),0,static_cast<int32_t>(offsetof(StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5, ____charStream_24)),static_cast<int32_t>(offsetof(StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5, ____charString_25)),static_cast<int32_t>(offsetof(StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5, ____charPosition_26)),static_cast<int32_t>(offsetof(StlStreamReader_tFFED5E3DDA30F881B1305039B3F58CC186E155C5, ___U3CLineU3Ek__BackingField_27)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12426[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12427[4] = 
{
	static_cast<int32_t>(offsetof(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields, ___FixInfacingNormals_10)),static_cast<int32_t>(offsetof(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields, ___LoadWithYUp_11)),static_cast<int32_t>(offsetof(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields, ___StoreTriangleIndexInTexCoord0_12)),static_cast<int32_t>(offsetof(StlReader_t142A4B8D26855A97FC4FA57D581D5092A1A3FE7A_StaticFields, ___ImportNormals_13)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12429[2] = 
{
	static_cast<int32_t>(offsetof(AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056, ___U3CpreferredExtensionU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(AssetFileNameExtensionAttribute_tEA86B663DC42BB5C4F9A2A081CD7D28845D9D056, ___U3CotherExtensionsU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12433[2] = 
{
	static_cast<int32_t>(offsetof(NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7, ___U3CQualifiedNativeNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(NativeClassAttribute_t774C48B9F745C9B0FD2FA82F9B42D4A18E162FA7, ___U3CDeclarationU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12435[1] = 
{
	static_cast<int32_t>(offsetof(UsedByNativeCodeAttribute_t3FE9A7CDCC6A3A4122D8BF44F1D0A37BB38894C1, ___U3CNameU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12436[3] = 
{
	static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA, ___U3COptionalU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t86B11F2BA12BB463CE3258E64E16B43484014FCA, ___U3CGenerateProxyU3Ek__BackingField_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12438[2] = 
{
	static_cast<int32_t>(offsetof(NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA, ___U3CConditionU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(NativeConditionalAttribute_tB722B3ED350E82853F8CEFF672A6CDC4B6B362CA, ___U3CEnabledU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12439[1] = 
{
	static_cast<int32_t>(offsetof(NativeHeaderAttribute_t35DDAA41C31EEE4C94D2586F33D3EB26C0EA6F51, ___U3CHeaderU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12440[1] = 
{
	static_cast<int32_t>(offsetof(NativeNameAttribute_t222751782B5418807DFE2A88CA0B24CA691B8621, ___U3CNameU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12441[1] = 
{
	static_cast<int32_t>(offsetof(NativeWritableSelfAttribute_t2ABC353836DDC2F15B1FBED9C0CF2E5ED0D1686C, ___U3CWritableSelfU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12442[5] = 
{
	static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CIsThreadSafeU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CIsFreeFunctionU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CThrowsExceptionU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(NativeMethodAttribute_tDE40C2DA59999D4870D672D8EDACC3504D2FA270, ___U3CHasExplicitThisU3Ek__BackingField_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12443[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12444[1] = 
{
	static_cast<int32_t>(offsetof(NativePropertyAttribute_tAF7FB03BF7FFE9E8AB0E75B0F842FC0AA22AE607, ___U3CTargetTypeU3Ek__BackingField_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12445[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12447[3] = 
{
	static_cast<int32_t>(offsetof(NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1, ___U3CHeaderU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1, ___U3CIntermediateScriptingStructNameU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(NativeTypeAttribute_tB60F1675F1F20B6CB1B871FDDD067D672F75B8D1, ___U3CCodegenOptionsU3Ek__BackingField_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12448[1] = 
{
	static_cast<int32_t>(offsetof(NotNullAttribute_t2E29B7802E8ED55CEA04EC4A6C254C6B60272DF7, ___U3CExceptionU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12452[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12453[2] = 
{
	static_cast<int32_t>(offsetof(StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A, ___U3CNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(StaticAccessorAttribute_tDE194716AED7A414D473DC570B2E0035A5CE130A, ___U3CTypeU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12454[1] = 
{
	static_cast<int32_t>(offsetof(NativeThrowsAttribute_t211CE8D047A8D45676C9ED399D5AA3B4A2C3E625, ___U3CThrowsExceptionU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12455[1] = 
{
	static_cast<int32_t>(offsetof(IgnoreAttribute_tAB3F6C4808BA16CD585D60A6353B3E0599DFCE4D, ___U3CDoesNotContributeToSizeU3Ek__BackingField_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12458[4] = 
{
	static_cast<int32_t>(offsetof(ImageProperties_t7C971D51A4992A01CD205754169B919B56696F59, ___width_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ImageProperties_t7C971D51A4992A01CD205754169B919B56696F59, ___height_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ImageProperties_t7C971D51A4992A01CD205754169B919B56696F59, ___mimeType_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(ImageProperties_t7C971D51A4992A01CD205754169B919B56696F59, ___orientation_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12459[4] = 
{
	static_cast<int32_t>(offsetof(VideoProperties_t11DD7C4EACACE50EA3D710F3E2595A68B17103A5, ___width_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(VideoProperties_t11DD7C4EACACE50EA3D710F3E2595A68B17103A5, ___height_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(VideoProperties_t11DD7C4EACACE50EA3D710F3E2595A68B17103A5, ___duration_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(VideoProperties_t11DD7C4EACACE50EA3D710F3E2595A68B17103A5, ___rotation_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12460[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12461[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12462[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12463[10] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12467[5] = 
{
	static_cast<int32_t>(offsetof(NativeGallery_t5E55EACA16F196F4214E0A3AB3A39B5CF555EB0B_StaticFields, ___m_ajc_0)),static_cast<int32_t>(offsetof(NativeGallery_t5E55EACA16F196F4214E0A3AB3A39B5CF555EB0B_StaticFields, ___m_context_1)),static_cast<int32_t>(offsetof(NativeGallery_t5E55EACA16F196F4214E0A3AB3A39B5CF555EB0B_StaticFields, ___m_temporaryImagePath_2)),static_cast<int32_t>(offsetof(NativeGallery_t5E55EACA16F196F4214E0A3AB3A39B5CF555EB0B_StaticFields, ___m_selectedMediaPath_3)),0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12468[1] = 
{
	static_cast<int32_t>(offsetof(NGCallbackHelper_t66F469013909FD93F5CEDB7C9C6689E79EB4605E, ___mainThreadAction_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12469[2] = 
{
	static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tD80C32708DF9E47C151599335772558DC8B3D9E8, ___U3CU3E4__this_0)),static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tD80C32708DF9E47C151599335772558DC8B3D9E8, ___path_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12470[2] = 
{
	static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tAE5229FA2486EA375EA2E7C0D014785D19289D9B, ___U3CU3E4__this_0)),static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tAE5229FA2486EA375EA2E7C0D014785D19289D9B, ___result_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12471[3] = 
{
	static_cast<int32_t>(offsetof(NGMediaReceiveCallbackAndroid_tB8DDBFAD614592BF98999BEAC00A75E0D420CC52, ___callback_4)),static_cast<int32_t>(offsetof(NGMediaReceiveCallbackAndroid_tB8DDBFAD614592BF98999BEAC00A75E0D420CC52, ___callbackMultiple_5)),static_cast<int32_t>(offsetof(NGMediaReceiveCallbackAndroid_tB8DDBFAD614592BF98999BEAC00A75E0D420CC52, ___callbackHelper_6)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12472[2] = 
{
	static_cast<int32_t>(offsetof(NGPermissionCallbackAndroid_t6824CF0103F77A92558E94C46463D4DECB491EEB, ___threadLock_4)),static_cast<int32_t>(offsetof(NGPermissionCallbackAndroid_t6824CF0103F77A92558E94C46463D4DECB491EEB, ___U3CResultU3Ek__BackingField_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12475[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12476[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12477[7] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12478[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12479[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12480[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12481[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12486[8] = 
{
	static_cast<int32_t>(offsetof(VideoPlayer_t48EA4A8117B822BC59590150DED9AD46C62F65D3, ___prepareCompleted_4)),static_cast<int32_t>(offsetof(VideoPlayer_t48EA4A8117B822BC59590150DED9AD46C62F65D3, ___loopPointReached_5)),static_cast<int32_t>(offsetof(VideoPlayer_t48EA4A8117B822BC59590150DED9AD46C62F65D3, ___started_6)),static_cast<int32_t>(offsetof(VideoPlayer_t48EA4A8117B822BC59590150DED9AD46C62F65D3, ___frameDropped_7)),static_cast<int32_t>(offsetof(VideoPlayer_t48EA4A8117B822BC59590150DED9AD46C62F65D3, ___errorReceived_8)),static_cast<int32_t>(offsetof(VideoPlayer_t48EA4A8117B822BC59590150DED9AD46C62F65D3, ___seekCompleted_9)),static_cast<int32_t>(offsetof(VideoPlayer_t48EA4A8117B822BC59590150DED9AD46C62F65D3, ___clockResyncOccurred_10)),static_cast<int32_t>(offsetof(VideoPlayer_t48EA4A8117B822BC59590150DED9AD46C62F65D3, ___frameReady_11)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12487[1] = 
{
	static_cast<int32_t>(offsetof(VideoClipPlayable_t2AAA6B40D35759730C170CA3124BC2499B50A0F1, ___m_Handle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12489[2] = 
{
	static_cast<int32_t>(offsetof(XRConfigurationDataAttribute_tFCF093318B5224BCC0E168E99C562AB50DDFC0B2, ___U3CdisplayNameU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(XRConfigurationDataAttribute_tFCF093318B5224BCC0E168E99C562AB50DDFC0B2, ___U3CbuildSettingsKeyU3Ek__BackingField_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12490[7] = 
{
	static_cast<int32_t>(offsetof(XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE_StaticFields, ___k_SettingsKey_4)),static_cast<int32_t>(offsetof(XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE_StaticFields, ___s_RuntimeSettingsInstance_5)),static_cast<int32_t>(offsetof(XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE, ___m_LoaderManagerInstance_6)),static_cast<int32_t>(offsetof(XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE, ___m_InitManagerOnStart_7)),static_cast<int32_t>(offsetof(XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE, ___m_XRManager_8)),static_cast<int32_t>(offsetof(XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE, ___m_ProviderIntialized_9)),static_cast<int32_t>(offsetof(XRGeneralSettings_t8F8D096944606B5AD845D010706BF7094ADEC8CE, ___m_ProviderStarted_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12492[1] = 
{
	static_cast<int32_t>(offsetof(XRLoaderHelper_tE96E7AE003148D5319D20BAD7E02654367E41DCC, ___m_SubsystemInstanceMap_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12493[4] = 
{
	static_cast<int32_t>(offsetof(BuildEvent_tE21D64E5354C87B5E40D2D2648C677B96CCA811E, ___buildGuid_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(BuildEvent_tE21D64E5354C87B5E40D2D2648C677B96CCA811E, ___buildTarget_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(BuildEvent_tE21D64E5354C87B5E40D2D2648C677B96CCA811E, ___buildTargetGroup_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(BuildEvent_tE21D64E5354C87B5E40D2D2648C677B96CCA811E, ___assigned_loaders_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12494[5] = 
{
	0,0,0,0,static_cast<int32_t>(offsetof(XRManagementAnalytics_t9A6F8603FBB618B8FEC7A6BF3FD9615C61C1063F_StaticFields, ___s_Initialized_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12495[4] = 
{
	static_cast<int32_t>(offsetof(U3CInitializeLoaderU3Ed__24_t02DE083CB86C55D65BAF82701483413204DE18C7, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CInitializeLoaderU3Ed__24_t02DE083CB86C55D65BAF82701483413204DE18C7, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CInitializeLoaderU3Ed__24_t02DE083CB86C55D65BAF82701483413204DE18C7, ___U3CU3E4__this_2)),static_cast<int32_t>(offsetof(U3CInitializeLoaderU3Ed__24_t02DE083CB86C55D65BAF82701483413204DE18C7, ___U3CU3E7__wrap1_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12496[7] = 
{
	static_cast<int32_t>(offsetof(XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52, ___m_InitializationComplete_4)),static_cast<int32_t>(offsetof(XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52, ___m_RequiresSettingsUpdate_5)),static_cast<int32_t>(offsetof(XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52, ___m_AutomaticLoading_6)),static_cast<int32_t>(offsetof(XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52, ___m_AutomaticRunning_7)),static_cast<int32_t>(offsetof(XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52, ___m_Loaders_8)),static_cast<int32_t>(offsetof(XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52, ___m_RegisteredLoaders_9)),static_cast<int32_t>(offsetof(XRManagerSettings_t7923B66EB3FEE58C7B9F85FF61749B774D3B9E52, ___U3CactiveLoaderU3Ek__BackingField_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12498[1] = 
{
	static_cast<int32_t>(offsetof(MainModule_tC7ECD8330C14B0808478A748048988A6085CE2A9, ___m_ParticleSystem_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12499[6] = 
{
	static_cast<int32_t>(offsetof(MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23, ___m_Mode_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23, ___m_CurveMultiplier_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23, ___m_CurveMin_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23, ___m_CurveMax_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23, ___m_ConstantMin_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MinMaxCurve_t812D571297EED6498776CC58949A42B172E60E23, ___m_ConstantMax_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12500[5] = 
{
	static_cast<int32_t>(offsetof(MinMaxGradient_tFF31B8EC2855D0074AB86E8B37BEA6609070AC69, ___m_Mode_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MinMaxGradient_tFF31B8EC2855D0074AB86E8B37BEA6609070AC69, ___m_GradientMin_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MinMaxGradient_tFF31B8EC2855D0074AB86E8B37BEA6609070AC69, ___m_GradientMax_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MinMaxGradient_tFF31B8EC2855D0074AB86E8B37BEA6609070AC69, ___m_ColorMin_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(MinMaxGradient_tFF31B8EC2855D0074AB86E8B37BEA6609070AC69, ___m_ColorMax_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12501[17] = 
{
	static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Position_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Velocity_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_AnimatedVelocity_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_InitialVelocity_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_AxisOfRotation_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Rotation_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_AngularVelocity_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_StartSize_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_StartColor_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_RandomSeed_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_ParentRandomSeed_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Lifetime_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_StartLifetime_12)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_MeshIndex_13)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_EmitAccumulator0_14)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_EmitAccumulator1_15)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(Particle_tF16C89682A98AB276CCBE4DA0A6E82F98500F79D, ___m_Flags_16)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12502[12] = 
{
	static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_Particle_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_PositionSet_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_VelocitySet_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_AxisOfRotationSet_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_RotationSet_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_AngularVelocitySet_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_StartSizeSet_6)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_StartColorSet_7)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_RandomSeedSet_8)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_StartLifetimeSet_9)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_MeshIndexSet_10)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(EmitParams_tE76279CE754C7B0A4ECDA7E294587AACB039FBA0, ___m_ApplyShapeToPosition_11)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12505[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12506[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12508[1] = 
{
	static_cast<int32_t>(offsetof(OpenLinkBehaviour_t1F9BDD58F83CEF5D9BFD78CA4EB4D002A0F6007E, ___Url_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12509[24] = 
{
	static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___onJS_4)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___onError_5)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___onHttpError_6)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___onStarted_7)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___onLoaded_8)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___onHooked_9)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___visibility_10)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___alertDialogEnabled_11)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___scrollBounceEnabled_12)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginLeft_13)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginTop_14)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginRight_15)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginBottom_16)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginRelative_17)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginLeftComputed_18)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginTopComputed_19)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginRightComputed_20)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginBottomComputed_21)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mMarginRelativeComputed_22)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___webView_23)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mVisibility_24)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mIsKeyboardVisible_25)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mWindowVisibleDisplayFrameHeight_26)),static_cast<int32_t>(offsetof(WebViewObject_t60574107C4C5AD427329795E7DE921E79A446B56, ___mResumedTimestamp_27)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12510[6] = 
{
	static_cast<int32_t>(offsetof(LeanPulseScale_tD2A516679E7F239C031C666338D2753CA2ECCA6B, ___baseScale_4)),static_cast<int32_t>(offsetof(LeanPulseScale_tD2A516679E7F239C031C666338D2753CA2ECCA6B, ___size_5)),static_cast<int32_t>(offsetof(LeanPulseScale_tD2A516679E7F239C031C666338D2753CA2ECCA6B, ___pulseInterval_6)),static_cast<int32_t>(offsetof(LeanPulseScale_tD2A516679E7F239C031C666338D2753CA2ECCA6B, ___pulseSize_7)),static_cast<int32_t>(offsetof(LeanPulseScale_tD2A516679E7F239C031C666338D2753CA2ECCA6B, ___damping_8)),static_cast<int32_t>(offsetof(LeanPulseScale_tD2A516679E7F239C031C666338D2753CA2ECCA6B, ___counter_9)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12514[3] = 
{
	static_cast<int32_t>(offsetof(RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250_StaticFields, ___Updated_0)),static_cast<int32_t>(offsetof(RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250_StaticFields, ___BeforeFetchFromServer_1)),static_cast<int32_t>(offsetof(RemoteSettings_t9DFFC747AB3E7A39DF4527F245B529A407427250_StaticFields, ___Completed_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12515[2] = 
{
	static_cast<int32_t>(offsetof(RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52, ___m_Ptr_0)),static_cast<int32_t>(offsetof(RemoteConfigSettings_tC979947EE51355162B3241B9F80D95A8FD25FE52, ___Updated_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12516[12] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12519[9] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12520[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12523[2] = 
{
	static_cast<int32_t>(offsetof(AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76_StaticFields, ___sessionStateChanged_0)),static_cast<int32_t>(offsetof(AnalyticsSessionInfo_tDE8F7A9E13EF9723E2D975F76E916753DA61AD76_StaticFields, ___identityTokenChanged_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12526[1] = 
{
	static_cast<int32_t>(offsetof(WWW_tEADA9A43B98FC277E498F8E3206A3B8C4E5AF3FB, ____uwr_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12530[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12531[4] = 
{
	static_cast<int32_t>(offsetof(NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD, ___eventBuffer_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD, ___eventCount_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD, ___sizeInBytes_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(NativeInputEventBuffer_t4EE5873AD7998E0E83C9F8585C338AB14C9101FD, ___capacityInBytes_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12532[6] = 
{
	0,static_cast<int32_t>(offsetof(NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8, ___type_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8, ___sizeInBytes_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8, ___deviceId_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8, ___time_4)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(NativeInputEvent_tDE7DE9A48ACA442A8D37E2920836D00C26408CB8, ___eventId_5)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12533[6] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12534[4] = 
{
	static_cast<int32_t>(offsetof(NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE_StaticFields, ___onUpdate_0)),static_cast<int32_t>(offsetof(NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE_StaticFields, ___onBeforeUpdate_1)),static_cast<int32_t>(offsetof(NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE_StaticFields, ___onShouldRunUpdate_2)),static_cast<int32_t>(offsetof(NativeInputSystem_tCFE5554EBC0D3EE1DAD80FC55CE0DE38A3DDC5EE_StaticFields, ___s_OnDeviceDiscoveredCallback_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12537[4] = 
{
	static_cast<int32_t>(offsetof(DownloadHandlerTexture_t45E2D719000AA1594E648810F0B57A77FA7C568C, ___m_NativeData_1)),static_cast<int32_t>(offsetof(DownloadHandlerTexture_t45E2D719000AA1594E648810F0B57A77FA7C568C, ___mTexture_2)),static_cast<int32_t>(offsetof(DownloadHandlerTexture_t45E2D719000AA1594E648810F0B57A77FA7C568C, ___mHasTexture_3)),static_cast<int32_t>(offsetof(DownloadHandlerTexture_t45E2D719000AA1594E648810F0B57A77FA7C568C, ___mNonReadable_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12539[8] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12540[3] = 
{
	static_cast<int32_t>(offsetof(PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D, ____domainProtocols_0)),static_cast<int32_t>(offsetof(PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D, ____deepLinkingProtocols_1)),static_cast<int32_t>(offsetof(PlatformLinkingConfiguration_tD22B9A509398D280266833C7AD96A081E533BC4D, ____initialized_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12541[3] = 
{
	static_cast<int32_t>(offsetof(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields, ___U3CU3E9_0)),static_cast<int32_t>(offsetof(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields, ___U3CU3E9__21_0_1)),static_cast<int32_t>(offsetof(U3CU3Ec_tF01063F3B9FE6DB54F7DC874033C59C62FF0CE44_StaticFields, ___U3CU3E9__22_0_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12542[4] = 
{
	static_cast<int32_t>(offsetof(AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D, ____steamId_0)),static_cast<int32_t>(offsetof(AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D, ____displayName_1)),static_cast<int32_t>(offsetof(AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D, ____globalConfiguration_2)),static_cast<int32_t>(offsetof(AppLinkingConfiguration_tE8D481996F2261D504C86A1BFB61FF316083FF5D, ____customDeepLinkingProtocols_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12546[3] = 
{
	static_cast<int32_t>(offsetof(LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9, ___U3CUriU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9, ___U3CRawQueryStringU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9, ___U3CQueryStringU3Ek__BackingField_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12547[3] = 
{
	static_cast<int32_t>(offsetof(LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A, ____scheme_0)),static_cast<int32_t>(offsetof(LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A, ____host_1)),static_cast<int32_t>(offsetof(LinkInformation_t3D4EBD09BD07F41077EA1D855B8D87DB47B4597A, ____path_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12549[3] = 
{
	static_cast<int32_t>(offsetof(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields, ___SaveFolders_0)),static_cast<int32_t>(offsetof(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields, ___SaveFile_1)),static_cast<int32_t>(offsetof(ConfigurationStorage_t786593904ED7945153003EDBC74DA7A38E1B1738_StaticFields, ____fileLocation_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12551[2] = 
{
	static_cast<int32_t>(offsetof(UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061, ___U3CUrlU3Ek__BackingField_14)),static_cast<int32_t>(offsetof(UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061, ___U3CQueryU3Ek__BackingField_15)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12552[6] = 
{
	static_cast<int32_t>(offsetof(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_StaticFields, ___U3CInstanceU3Ek__BackingField_0)),static_cast<int32_t>(offsetof(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F, ___U3CIsSteamBuildU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F, ____activated_2)),static_cast<int32_t>(offsetof(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F, ____currProvider_3)),static_cast<int32_t>(offsetof(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F, ____go_4)),static_cast<int32_t>(offsetof(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F, ____storedActivation_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12553[3] = 
{
	static_cast<int32_t>(offsetof(U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D, ___U3CU3E1__state_0)),static_cast<int32_t>(offsetof(U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D, ___U3CU3E2__current_1)),static_cast<int32_t>(offsetof(U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D, ___U3CU3E4__this_2)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12554[1] = 
{
	static_cast<int32_t>(offsetof(UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6, ____onJob_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12555[1] = 
{
	static_cast<int32_t>(offsetof(DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923, ___LinkReceived_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12556[2] = 
{
	static_cast<int32_t>(offsetof(EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_StaticFields, ____instance_0)),static_cast<int32_t>(offsetof(EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D, ___LinkReceived_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12561[1] = 
{
	static_cast<int32_t>(offsetof(DownloadHandlerAudioClip_t11D829901BD9F3137CBB5D7BEA99FEAD976E56AC, ___m_NativeData_1)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12563[1] = 
{
	static_cast<int32_t>(offsetof(XRDevice_tD076A68EFE413B3EEEEA362BE0364A488B58F194_StaticFields, ___deviceLoaded_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12578[6] = 
{
	static_cast<int32_t>(offsetof(Header_tA6CD0237F1CA9F61AFCA3C7FA51ECEBC4353E808, ____valid_0)),static_cast<int32_t>(offsetof(Header_tA6CD0237F1CA9F61AFCA3C7FA51ECEBC4353E808, ___U3CExposureU3Ek__BackingField_1)),static_cast<int32_t>(offsetof(Header_tA6CD0237F1CA9F61AFCA3C7FA51ECEBC4353E808, ___U3CGammaU3Ek__BackingField_2)),static_cast<int32_t>(offsetof(Header_tA6CD0237F1CA9F61AFCA3C7FA51ECEBC4353E808, ___U3CHeightU3Ek__BackingField_3)),static_cast<int32_t>(offsetof(Header_tA6CD0237F1CA9F61AFCA3C7FA51ECEBC4353E808, ___U3CProgramTypeU3Ek__BackingField_4)),static_cast<int32_t>(offsetof(Header_tA6CD0237F1CA9F61AFCA3C7FA51ECEBC4353E808, ___U3CWidthU3Ek__BackingField_5)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12579[6] = 
{
	0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12583[2] = 
{
	static_cast<int32_t>(offsetof(PoseData_t67ECD1DA6257206C1935CCD030E1E1EF57E1443A, ___PoseNames_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(PoseData_t67ECD1DA6257206C1935CCD030E1E1EF57E1443A, ___Poses_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12584[1] = 
{
	static_cast<int32_t>(offsetof(TrackedPoseDriverDataDescription_tDD80D37EB4F938007BCE0FAB9705032594E4D9FF_StaticFields, ___DeviceData_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12585[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12586[1] = 
{
	static_cast<int32_t>(offsetof(PoseDataSource_tF08072CF4DBA9641E6AE6D1DBA1D421DA9C46858_StaticFields, ___nodeStates_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12587[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12588[12] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12589[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12590[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12591[7] = 
{
	static_cast<int32_t>(offsetof(TrackedPoseDriver_t5A2DEE7957ED76932DB7460383BF09AB1CB4CCCD, ___m_Device_4)),static_cast<int32_t>(offsetof(TrackedPoseDriver_t5A2DEE7957ED76932DB7460383BF09AB1CB4CCCD, ___m_PoseSource_5)),static_cast<int32_t>(offsetof(TrackedPoseDriver_t5A2DEE7957ED76932DB7460383BF09AB1CB4CCCD, ___m_PoseProviderComponent_6)),static_cast<int32_t>(offsetof(TrackedPoseDriver_t5A2DEE7957ED76932DB7460383BF09AB1CB4CCCD, ___m_TrackingType_7)),static_cast<int32_t>(offsetof(TrackedPoseDriver_t5A2DEE7957ED76932DB7460383BF09AB1CB4CCCD, ___m_UpdateType_8)),static_cast<int32_t>(offsetof(TrackedPoseDriver_t5A2DEE7957ED76932DB7460383BF09AB1CB4CCCD, ___m_UseRelativeTransform_9)),static_cast<int32_t>(offsetof(TrackedPoseDriver_t5A2DEE7957ED76932DB7460383BF09AB1CB4CCCD, ___m_OriginPose_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12594[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12596[11] = 
{
	static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B_StaticFields, ___m_ajc_0)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B_StaticFields, ___m_context_1)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B, ___subject_2)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B, ___text_3)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B, ___title_4)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B, ___url_5)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B, ___targetPackages_6)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B, ___targetClasses_7)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B, ___files_8)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B, ___mimes_9)),static_cast<int32_t>(offsetof(NativeShare_t89B79EA6B0A34E197A0FF3815CEE1B970D30960B, ___callback_10)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12597[4] = 
{
	static_cast<int32_t>(offsetof(NSCallbackHelper_t0FE98DE6D6CF554080E07CC69C0C7AD59C49824C, ___callback_4)),static_cast<int32_t>(offsetof(NSCallbackHelper_t0FE98DE6D6CF554080E07CC69C0C7AD59C49824C, ___result_5)),static_cast<int32_t>(offsetof(NSCallbackHelper_t0FE98DE6D6CF554080E07CC69C0C7AD59C49824C, ___shareTarget_6)),static_cast<int32_t>(offsetof(NSCallbackHelper_t0FE98DE6D6CF554080E07CC69C0C7AD59C49824C, ___resultReceived_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12598[1] = 
{
	static_cast<int32_t>(offsetof(NSShareResultCallbackAndroid_t70A5A1AEE38FC283E6B2D2F3D9FB87A08B60CFB0, ___callbackHelper_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12600[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12604[6] = 
{
	static_cast<int32_t>(offsetof(MockRuntime_tC5F1019CA2D3E89010E3A3BFC89834251257B2AF_StaticFields, ___s_AfterFunctionCallbacks_16)),static_cast<int32_t>(offsetof(MockRuntime_tC5F1019CA2D3E89010E3A3BFC89834251257B2AF_StaticFields, ___s_BeforeFunctionCallbacks_17)),static_cast<int32_t>(offsetof(MockRuntime_tC5F1019CA2D3E89010E3A3BFC89834251257B2AF_StaticFields, ___onScriptEvent_18)),0,static_cast<int32_t>(offsetof(MockRuntime_tC5F1019CA2D3E89010E3A3BFC89834251257B2AF, ___ignoreValidationErrors_20)),0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12606[13] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12607[7] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12608[2] = 
{
	static_cast<int32_t>(offsetof(DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A, ___dataType_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DracoData_tBAB19BFE46131CC9F91F1E27A369090308D1C18A, ___data_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12609[4] = 
{
	static_cast<int32_t>(offsetof(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7, ___attributeType_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7, ___dataType_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7, ___numComponents_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DracoAttribute_t80919B94F3EA34766AFE6A4E9DFE31F5739CF5B7, ___uniqueId_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12610[3] = 
{
	static_cast<int32_t>(offsetof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED, ___numFaces_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED, ___numVertices_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(DracoMesh_t22B1E8AE9C29C7B45D5C21EEABA0BD02E642E2ED, ___numAttributes_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12611[1] = 
{
	0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12614[5] = 
{
	static_cast<int32_t>(offsetof(AndroidLinkProvider_t25725DADB9911DB50E702686EA6A3796B2F17EBC, ____unityPlayer_0)),static_cast<int32_t>(offsetof(AndroidLinkProvider_t25725DADB9911DB50E702686EA6A3796B2F17EBC, ____currentActivity_1)),static_cast<int32_t>(offsetof(AndroidLinkProvider_t25725DADB9911DB50E702686EA6A3796B2F17EBC, ____deepLinkIntent_2)),static_cast<int32_t>(offsetof(AndroidLinkProvider_t25725DADB9911DB50E702686EA6A3796B2F17EBC, ____deepLink_3)),static_cast<int32_t>(offsetof(AndroidLinkProvider_t25725DADB9911DB50E702686EA6A3796B2F17EBC, ____linkReceived_4)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12616[4] = 
{
	static_cast<int32_t>(offsetof(VectorImageVertexBridge_t2843746D575BD491159E12640C7D2BC8E4CF0285, ___position_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(VectorImageVertexBridge_t2843746D575BD491159E12640C7D2BC8E4CF0285, ___tint_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(VectorImageVertexBridge_t2843746D575BD491159E12640C7D2BC8E4CF0285, ___uv_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(VectorImageVertexBridge_t2843746D575BD491159E12640C7D2BC8E4CF0285, ___settingIndex_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12617[4] = 
{
	static_cast<int32_t>(offsetof(GradientSettingsBridge_t3724705E3C6E74096502E2041E29482B09F05544, ___gradientType_0)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GradientSettingsBridge_t3724705E3C6E74096502E2041E29482B09F05544, ___addressMode_1)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GradientSettingsBridge_t3724705E3C6E74096502E2041E29482B09F05544, ___radialFocus_2)) + static_cast<int32_t>(sizeof(RuntimeObject)),static_cast<int32_t>(offsetof(GradientSettingsBridge_t3724705E3C6E74096502E2041E29482B09F05544, ___location_3)) + static_cast<int32_t>(sizeof(RuntimeObject)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12618[3] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12619[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12620[8] = 
{
	static_cast<int32_t>(offsetof(U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3_StaticFields, ___U3CU3E9_0)),static_cast<int32_t>(offsetof(U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3_StaticFields, ___U3CU3E9__5_0_1)),static_cast<int32_t>(offsetof(U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3_StaticFields, ___U3CU3E9__5_1_2)),static_cast<int32_t>(offsetof(U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3_StaticFields, ___U3CU3E9__6_0_3)),static_cast<int32_t>(offsetof(U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3_StaticFields, ___U3CU3E9__6_1_4)),static_cast<int32_t>(offsetof(U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3_StaticFields, ___U3CU3E9__6_2_5)),static_cast<int32_t>(offsetof(U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3_StaticFields, ___U3CU3E9__6_3_6)),static_cast<int32_t>(offsetof(U3CU3Ec_t3D6A2F9331B03DAC48088B7A6E141BCC153BA9B3_StaticFields, ___U3CU3E9__6_4_7)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12627[4] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12630[5] = 
{
	static_cast<int32_t>(sizeof(RuntimeObject)),0,0,0,0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12638[4] = 
{
	0,static_cast<int32_t>(offsetof(ConformanceAutomationFeature_t0C2E5D391DBCC81F1B86F2D7E34719B7D292BFF7_StaticFields, ___xrInstance_17)),static_cast<int32_t>(offsetof(ConformanceAutomationFeature_t0C2E5D391DBCC81F1B86F2D7E34719B7D292BFF7_StaticFields, ___xrSession_18)),0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12640[1] = 
{
	static_cast<int32_t>(offsetof(DataContractAttribute_tD065D7D14CC8AA548815166AB8B8210D1B3C699F, ___isReference_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12641[4] = 
{
	static_cast<int32_t>(offsetof(DataMemberAttribute_t8AE446BE9032B9BC8E7B2EDC785F5C6FA0E5BB73, ___name_0)),static_cast<int32_t>(offsetof(DataMemberAttribute_t8AE446BE9032B9BC8E7B2EDC785F5C6FA0E5BB73, ___order_1)),static_cast<int32_t>(offsetof(DataMemberAttribute_t8AE446BE9032B9BC8E7B2EDC785F5C6FA0E5BB73, ___isRequired_2)),static_cast<int32_t>(offsetof(DataMemberAttribute_t8AE446BE9032B9BC8E7B2EDC785F5C6FA0E5BB73, ___emitDefaultValue_3)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12642[1] = 
{
	static_cast<int32_t>(offsetof(EnumMemberAttribute_t65B5E85E642C96791DD6AE5EAD0276350954126F, ___value_0)),};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12645[6] = 
{
	static_cast<int32_t>(offsetof(RuntimeDebuggerOpenXRFeature_t1E3C60D2636DEC25D95E63BB3300DBFC5E9057BA_StaticFields, ___kEditorToPlayerRequestDebuggerOutput_16)),static_cast<int32_t>(offsetof(RuntimeDebuggerOpenXRFeature_t1E3C60D2636DEC25D95E63BB3300DBFC5E9057BA_StaticFields, ___kPlayerToEditorSendDebuggerOutput_17)),static_cast<int32_t>(offsetof(RuntimeDebuggerOpenXRFeature_t1E3C60D2636DEC25D95E63BB3300DBFC5E9057BA, ___cacheSize_18)),static_cast<int32_t>(offsetof(RuntimeDebuggerOpenXRFeature_t1E3C60D2636DEC25D95E63BB3300DBFC5E9057BA, ___perThreadCacheSize_19)),static_cast<int32_t>(offsetof(RuntimeDebuggerOpenXRFeature_t1E3C60D2636DEC25D95E63BB3300DBFC5E9057BA, ___lutOffset_20)),0,};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable12652[3] = 
{
	0,static_cast<int32_t>(offsetof(OculusQuestFeature_tF5FA7128BB36F546D5CF7094A93B812DAC667810, ___targetQuest_17)),static_cast<int32_t>(offsetof(OculusQuestFeature_tF5FA7128BB36F546D5CF7094A93B812DAC667810, ___targetQuest2_18)),};
