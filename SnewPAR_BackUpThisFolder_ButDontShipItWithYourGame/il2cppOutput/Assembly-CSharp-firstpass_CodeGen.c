﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void OpenLinkBehaviour::Start()
extern void OpenLinkBehaviour_Start_mDD0095E1337E9DA6FDBC99C70359D9BDBD78D6DF (void);
// 0x00000002 System.Void OpenLinkBehaviour::Open()
extern void OpenLinkBehaviour_Open_m2D766C0D0E16F5B835DE0E6DA2A7759A9AF028DF (void);
// 0x00000003 System.Void OpenLinkBehaviour::.ctor()
extern void OpenLinkBehaviour__ctor_m271302A1F87B40B4C7B986328DD305BA68B71D0C (void);
// 0x00000004 System.Void WebViewObject::OnApplicationPause(System.Boolean)
extern void WebViewObject_OnApplicationPause_mEF7508EAD95F66951D19A6A649C12DEA206AA487 (void);
// 0x00000005 System.Void WebViewObject::Update()
extern void WebViewObject_Update_mF2E816DF16248D209B2B1DC9645518F2CD1C04F0 (void);
// 0x00000006 System.Void WebViewObject::SetKeyboardVisible(System.String)
extern void WebViewObject_SetKeyboardVisible_m383CC52DFD8817355D05C12A4030C3977D5D3ECD (void);
// 0x00000007 System.Int32 WebViewObject::AdjustBottomMargin(System.Int32)
extern void WebViewObject_AdjustBottomMargin_m1381ADCAAB043E7CC77BE491213233A9C93910BC (void);
// 0x00000008 System.Void WebViewObject::Awake()
extern void WebViewObject_Awake_mFC86DDAAF3DB5473B03712E197F0F37A030CE5D8 (void);
// 0x00000009 System.Boolean WebViewObject::get_IsKeyboardVisible()
extern void WebViewObject_get_IsKeyboardVisible_mDE8102B8B4BC1FAADEDB3B5EC19D60DB1690239E (void);
// 0x0000000A System.Boolean WebViewObject::IsWebViewAvailable()
extern void WebViewObject_IsWebViewAvailable_m2512189A0ABB754954BDBF4BAA0354B8F8488D4F (void);
// 0x0000000B System.Void WebViewObject::Init(System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Boolean,System.Boolean,System.String,System.Int32,System.Boolean,System.Int32,System.Boolean,System.Boolean)
extern void WebViewObject_Init_m6A3C5F7D69A7729A4643B9E702F965A92FB5EC5E (void);
// 0x0000000C System.Void WebViewObject::OnDestroy()
extern void WebViewObject_OnDestroy_mB9C124234CD0CCA08F271889192EC6A55B9A7D1F (void);
// 0x0000000D System.Void WebViewObject::Pause()
extern void WebViewObject_Pause_m871DBD0F191B6E56128FB8033D20A44045A41D61 (void);
// 0x0000000E System.Void WebViewObject::Resume()
extern void WebViewObject_Resume_mC92B6E403B1C59C024FDEF2542DD57FF6ACFEB4C (void);
// 0x0000000F System.Void WebViewObject::SetCenterPositionWithScale(UnityEngine.Vector2,UnityEngine.Vector2)
extern void WebViewObject_SetCenterPositionWithScale_m3AAEC2F29EFE6B7DB69FE3E414DE5EA00EDAC1B4 (void);
// 0x00000010 System.Void WebViewObject::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WebViewObject_SetMargins_m5CF9511289B90C24DE6CB001F516A85A10F649F1 (void);
// 0x00000011 System.Void WebViewObject::SetVisibility(System.Boolean)
extern void WebViewObject_SetVisibility_m98D4DDA62673C386C15F8CACCE382F7CB09FCE20 (void);
// 0x00000012 System.Boolean WebViewObject::GetVisibility()
extern void WebViewObject_GetVisibility_mE51418A320BAA7C6B9D1394C27BFFDEA197700DE (void);
// 0x00000013 System.Void WebViewObject::SetScrollbarsVisibility(System.Boolean)
extern void WebViewObject_SetScrollbarsVisibility_m33122C51AE0AE8F90B8917F03084F4CDD6137040 (void);
// 0x00000014 System.Void WebViewObject::SetAlertDialogEnabled(System.Boolean)
extern void WebViewObject_SetAlertDialogEnabled_m8A2CF01D1C03D2877012BAF2BDE44569AB5BE6EE (void);
// 0x00000015 System.Boolean WebViewObject::GetAlertDialogEnabled()
extern void WebViewObject_GetAlertDialogEnabled_m1EE61734B7255B7046C6D446BC7CC349D8EDDB45 (void);
// 0x00000016 System.Void WebViewObject::SetScrollBounceEnabled(System.Boolean)
extern void WebViewObject_SetScrollBounceEnabled_m98A87C8B2A38EFC14BCB07849ED68F970495CE32 (void);
// 0x00000017 System.Boolean WebViewObject::GetScrollBounceEnabled()
extern void WebViewObject_GetScrollBounceEnabled_m5CCDDD4783BDD303A87EDD2C74DE64B262A28D14 (void);
// 0x00000018 System.Void WebViewObject::SetCameraAccess(System.Boolean)
extern void WebViewObject_SetCameraAccess_m47EAE84CD2558E13DF6AC182F12B1F9AA52B3974 (void);
// 0x00000019 System.Void WebViewObject::SetMicrophoneAccess(System.Boolean)
extern void WebViewObject_SetMicrophoneAccess_m22A6EAA4BE29704E62B5DC41701AC53DD5954549 (void);
// 0x0000001A System.Boolean WebViewObject::SetURLPattern(System.String,System.String,System.String)
extern void WebViewObject_SetURLPattern_mF92AF5D104D34BF0D2CE6FF8485A01BDC0D90043 (void);
// 0x0000001B System.Void WebViewObject::LoadURL(System.String)
extern void WebViewObject_LoadURL_m6FFE5277B0AA90377C3C83E6D05CDCF024807F68 (void);
// 0x0000001C System.Void WebViewObject::LoadHTML(System.String,System.String)
extern void WebViewObject_LoadHTML_m8FDE50941C0B520DF5F1576AFB1C8F4C3DC082B5 (void);
// 0x0000001D System.Void WebViewObject::EvaluateJS(System.String)
extern void WebViewObject_EvaluateJS_m6FCEAB314654DDA32521D87BE5F7704DBEA01424 (void);
// 0x0000001E System.Int32 WebViewObject::Progress()
extern void WebViewObject_Progress_m3E8E335E7556FADA5ECF5280E829FC27942E918B (void);
// 0x0000001F System.Boolean WebViewObject::CanGoBack()
extern void WebViewObject_CanGoBack_mF273E006571B6704AD9AE526D3ED1BD202834982 (void);
// 0x00000020 System.Boolean WebViewObject::CanGoForward()
extern void WebViewObject_CanGoForward_m51CCE8A4DBBCFF9580379626DB1E7878F4BE8FBD (void);
// 0x00000021 System.Void WebViewObject::GoBack()
extern void WebViewObject_GoBack_m776A8838FE4A676C44EA00D64B702FF0E55BB673 (void);
// 0x00000022 System.Void WebViewObject::GoForward()
extern void WebViewObject_GoForward_m57C2691B12E772B3344612359CE70479D8B4D0BD (void);
// 0x00000023 System.Void WebViewObject::Reload()
extern void WebViewObject_Reload_mB1A128B539EC7C3D2DABD5AA9BC573774AA00002 (void);
// 0x00000024 System.Void WebViewObject::CallOnError(System.String)
extern void WebViewObject_CallOnError_m5A5E6E80BFC1CC2C64D71CDFEC5E528D0B7EC194 (void);
// 0x00000025 System.Void WebViewObject::CallOnHttpError(System.String)
extern void WebViewObject_CallOnHttpError_m346A86E9BE0FAAD12EC6984075E0492C3E08E9EE (void);
// 0x00000026 System.Void WebViewObject::CallOnStarted(System.String)
extern void WebViewObject_CallOnStarted_m4EC8FB335740B98B7405BC03ABCA20E3ADF7DCAE (void);
// 0x00000027 System.Void WebViewObject::CallOnLoaded(System.String)
extern void WebViewObject_CallOnLoaded_mD6023A4606B6FC4B4C2450851D4E3C1D635B1BB1 (void);
// 0x00000028 System.Void WebViewObject::CallFromJS(System.String)
extern void WebViewObject_CallFromJS_m048451C28598BF0A08B237F1D8323295CE3689E6 (void);
// 0x00000029 System.Void WebViewObject::CallOnHooked(System.String)
extern void WebViewObject_CallOnHooked_m607F859707387BCBBF092CA4D13334E4A04B1240 (void);
// 0x0000002A System.Void WebViewObject::AddCustomHeader(System.String,System.String)
extern void WebViewObject_AddCustomHeader_mE5491C87D0C435438A906E1CC9DA2DF2C341DA87 (void);
// 0x0000002B System.String WebViewObject::GetCustomHeaderValue(System.String)
extern void WebViewObject_GetCustomHeaderValue_m7A283511B98AA48B897283848653D44B7CCC592A (void);
// 0x0000002C System.Void WebViewObject::RemoveCustomHeader(System.String)
extern void WebViewObject_RemoveCustomHeader_m0586843BDDA67C9A904125A44832F4EB35139BF6 (void);
// 0x0000002D System.Void WebViewObject::ClearCustomHeader()
extern void WebViewObject_ClearCustomHeader_mD8BF938D7B246B45EB6598CB1CD9E67D060CC97D (void);
// 0x0000002E System.Void WebViewObject::ClearCookies()
extern void WebViewObject_ClearCookies_m30570F9E03339E796E35AE1EBA69E19912479113 (void);
// 0x0000002F System.Void WebViewObject::SaveCookies()
extern void WebViewObject_SaveCookies_mD50DC21F6A13CE1A99DF8720102F4DC1C1069FF7 (void);
// 0x00000030 System.String WebViewObject::GetCookies(System.String)
extern void WebViewObject_GetCookies_mC4C1C16E6CF16976032FBD084F9B936D968E5161 (void);
// 0x00000031 System.Void WebViewObject::SetBasicAuthInfo(System.String,System.String)
extern void WebViewObject_SetBasicAuthInfo_m5B3FC6CE99B5F0B91AAF6EBD6286A0EF3871D87E (void);
// 0x00000032 System.Void WebViewObject::ClearCache(System.Boolean)
extern void WebViewObject_ClearCache_m33CCF4FE775014847A78F8D3EDEE64C944A60583 (void);
// 0x00000033 System.Void WebViewObject::SetTextZoom(System.Int32)
extern void WebViewObject_SetTextZoom_m8FD76834FD8E118FF9034BDCBC003D091A1535E5 (void);
// 0x00000034 System.Void WebViewObject::.ctor()
extern void WebViewObject__ctor_mB6EABD80A28F1B3C876C5DA6D5679200F3343A2C (void);
// 0x00000035 System.Void Lean.Touch.LeanPulseScale::set_BaseScale(UnityEngine.Vector3)
extern void LeanPulseScale_set_BaseScale_m4B768B064BAB62AD08F305A195642E05D6B22391 (void);
// 0x00000036 UnityEngine.Vector3 Lean.Touch.LeanPulseScale::get_BaseScale()
extern void LeanPulseScale_get_BaseScale_m27D6D46137B94F4081AA89ACD12DF253183D2FC1 (void);
// 0x00000037 System.Void Lean.Touch.LeanPulseScale::set_Size(System.Single)
extern void LeanPulseScale_set_Size_mAE3D13FA565AFC5666F25552620765675F2DCA34 (void);
// 0x00000038 System.Single Lean.Touch.LeanPulseScale::get_Size()
extern void LeanPulseScale_get_Size_m5ED588012827D464CD4EEDA85FD4F8C84E22C0D6 (void);
// 0x00000039 System.Void Lean.Touch.LeanPulseScale::set_PulseInterval(System.Single)
extern void LeanPulseScale_set_PulseInterval_m2C5C7519A1DC36DA76F26903B5DB0BCAA015D699 (void);
// 0x0000003A System.Single Lean.Touch.LeanPulseScale::get_PulseInterval()
extern void LeanPulseScale_get_PulseInterval_m492C9D07F01F1DCF480A182DCA5A6EDCBD45DC10 (void);
// 0x0000003B System.Void Lean.Touch.LeanPulseScale::set_PulseSize(System.Single)
extern void LeanPulseScale_set_PulseSize_m9DEECF0CBA5F1C2CE7D20ED04E4D471576F7B16C (void);
// 0x0000003C System.Single Lean.Touch.LeanPulseScale::get_PulseSize()
extern void LeanPulseScale_get_PulseSize_m139062A4C0B457F07C3E67D2F2E5C3ACFB7A3DCB (void);
// 0x0000003D System.Void Lean.Touch.LeanPulseScale::set_Damping(System.Single)
extern void LeanPulseScale_set_Damping_mE405B5B718CDD62AA7924D0A353D4CC7691818DE (void);
// 0x0000003E System.Single Lean.Touch.LeanPulseScale::get_Damping()
extern void LeanPulseScale_get_Damping_m6ECAFE5CFBB901B79AF6218B9A3E37E7830EBE93 (void);
// 0x0000003F System.Void Lean.Touch.LeanPulseScale::Update()
extern void LeanPulseScale_Update_m2BC8B83015F15A629FCD11BCD047F5FAAC1E4CB6 (void);
// 0x00000040 System.Void Lean.Touch.LeanPulseScale::.ctor()
extern void LeanPulseScale__ctor_m8F1705BC190A5B9117B95C4C2D648C3D0DF48DA1 (void);
// 0x00000041 System.Void Lean.Touch.LeanTouchEvents::OnEnable()
extern void LeanTouchEvents_OnEnable_mC414AB54B7A99A7E68309090C6F029D1CFAF169F (void);
// 0x00000042 System.Void Lean.Touch.LeanTouchEvents::OnDisable()
extern void LeanTouchEvents_OnDisable_mE1F3C9C7F96517CF14F32FEDD36532F36FBEF20F (void);
// 0x00000043 System.Void Lean.Touch.LeanTouchEvents::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerDown_m4AA499342144F25ED1EDD1D59B1477EC83C483A4 (void);
// 0x00000044 System.Void Lean.Touch.LeanTouchEvents::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerUpdate_m0B166607F73A7FD291985551756F64D99351EB56 (void);
// 0x00000045 System.Void Lean.Touch.LeanTouchEvents::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerUp_m7593200A9A4D7E68B7408E1711F3F3C4761C01ED (void);
// 0x00000046 System.Void Lean.Touch.LeanTouchEvents::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerTap_m69D81AABDEE2F3CB8F3169726E3139EF8DD409E3 (void);
// 0x00000047 System.Void Lean.Touch.LeanTouchEvents::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerSwipe_mEE3F918DA479A728FC3A767BAEF6CD38BF9D7E8B (void);
// 0x00000048 System.Void Lean.Touch.LeanTouchEvents::HandleGesture(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanTouchEvents_HandleGesture_mA28336D8894E319D4A7F338244E53486E2BDFCE0 (void);
// 0x00000049 System.Void Lean.Touch.LeanTouchEvents::.ctor()
extern void LeanTouchEvents__ctor_m7E262CCD26482369D4776CA6D1240B8E2305EE33 (void);
static Il2CppMethodPointer s_methodPointers[73] = 
{
	OpenLinkBehaviour_Start_mDD0095E1337E9DA6FDBC99C70359D9BDBD78D6DF,
	OpenLinkBehaviour_Open_m2D766C0D0E16F5B835DE0E6DA2A7759A9AF028DF,
	OpenLinkBehaviour__ctor_m271302A1F87B40B4C7B986328DD305BA68B71D0C,
	WebViewObject_OnApplicationPause_mEF7508EAD95F66951D19A6A649C12DEA206AA487,
	WebViewObject_Update_mF2E816DF16248D209B2B1DC9645518F2CD1C04F0,
	WebViewObject_SetKeyboardVisible_m383CC52DFD8817355D05C12A4030C3977D5D3ECD,
	WebViewObject_AdjustBottomMargin_m1381ADCAAB043E7CC77BE491213233A9C93910BC,
	WebViewObject_Awake_mFC86DDAAF3DB5473B03712E197F0F37A030CE5D8,
	WebViewObject_get_IsKeyboardVisible_mDE8102B8B4BC1FAADEDB3B5EC19D60DB1690239E,
	WebViewObject_IsWebViewAvailable_m2512189A0ABB754954BDBF4BAA0354B8F8488D4F,
	WebViewObject_Init_m6A3C5F7D69A7729A4643B9E702F965A92FB5EC5E,
	WebViewObject_OnDestroy_mB9C124234CD0CCA08F271889192EC6A55B9A7D1F,
	WebViewObject_Pause_m871DBD0F191B6E56128FB8033D20A44045A41D61,
	WebViewObject_Resume_mC92B6E403B1C59C024FDEF2542DD57FF6ACFEB4C,
	WebViewObject_SetCenterPositionWithScale_m3AAEC2F29EFE6B7DB69FE3E414DE5EA00EDAC1B4,
	WebViewObject_SetMargins_m5CF9511289B90C24DE6CB001F516A85A10F649F1,
	WebViewObject_SetVisibility_m98D4DDA62673C386C15F8CACCE382F7CB09FCE20,
	WebViewObject_GetVisibility_mE51418A320BAA7C6B9D1394C27BFFDEA197700DE,
	WebViewObject_SetScrollbarsVisibility_m33122C51AE0AE8F90B8917F03084F4CDD6137040,
	WebViewObject_SetAlertDialogEnabled_m8A2CF01D1C03D2877012BAF2BDE44569AB5BE6EE,
	WebViewObject_GetAlertDialogEnabled_m1EE61734B7255B7046C6D446BC7CC349D8EDDB45,
	WebViewObject_SetScrollBounceEnabled_m98A87C8B2A38EFC14BCB07849ED68F970495CE32,
	WebViewObject_GetScrollBounceEnabled_m5CCDDD4783BDD303A87EDD2C74DE64B262A28D14,
	WebViewObject_SetCameraAccess_m47EAE84CD2558E13DF6AC182F12B1F9AA52B3974,
	WebViewObject_SetMicrophoneAccess_m22A6EAA4BE29704E62B5DC41701AC53DD5954549,
	WebViewObject_SetURLPattern_mF92AF5D104D34BF0D2CE6FF8485A01BDC0D90043,
	WebViewObject_LoadURL_m6FFE5277B0AA90377C3C83E6D05CDCF024807F68,
	WebViewObject_LoadHTML_m8FDE50941C0B520DF5F1576AFB1C8F4C3DC082B5,
	WebViewObject_EvaluateJS_m6FCEAB314654DDA32521D87BE5F7704DBEA01424,
	WebViewObject_Progress_m3E8E335E7556FADA5ECF5280E829FC27942E918B,
	WebViewObject_CanGoBack_mF273E006571B6704AD9AE526D3ED1BD202834982,
	WebViewObject_CanGoForward_m51CCE8A4DBBCFF9580379626DB1E7878F4BE8FBD,
	WebViewObject_GoBack_m776A8838FE4A676C44EA00D64B702FF0E55BB673,
	WebViewObject_GoForward_m57C2691B12E772B3344612359CE70479D8B4D0BD,
	WebViewObject_Reload_mB1A128B539EC7C3D2DABD5AA9BC573774AA00002,
	WebViewObject_CallOnError_m5A5E6E80BFC1CC2C64D71CDFEC5E528D0B7EC194,
	WebViewObject_CallOnHttpError_m346A86E9BE0FAAD12EC6984075E0492C3E08E9EE,
	WebViewObject_CallOnStarted_m4EC8FB335740B98B7405BC03ABCA20E3ADF7DCAE,
	WebViewObject_CallOnLoaded_mD6023A4606B6FC4B4C2450851D4E3C1D635B1BB1,
	WebViewObject_CallFromJS_m048451C28598BF0A08B237F1D8323295CE3689E6,
	WebViewObject_CallOnHooked_m607F859707387BCBBF092CA4D13334E4A04B1240,
	WebViewObject_AddCustomHeader_mE5491C87D0C435438A906E1CC9DA2DF2C341DA87,
	WebViewObject_GetCustomHeaderValue_m7A283511B98AA48B897283848653D44B7CCC592A,
	WebViewObject_RemoveCustomHeader_m0586843BDDA67C9A904125A44832F4EB35139BF6,
	WebViewObject_ClearCustomHeader_mD8BF938D7B246B45EB6598CB1CD9E67D060CC97D,
	WebViewObject_ClearCookies_m30570F9E03339E796E35AE1EBA69E19912479113,
	WebViewObject_SaveCookies_mD50DC21F6A13CE1A99DF8720102F4DC1C1069FF7,
	WebViewObject_GetCookies_mC4C1C16E6CF16976032FBD084F9B936D968E5161,
	WebViewObject_SetBasicAuthInfo_m5B3FC6CE99B5F0B91AAF6EBD6286A0EF3871D87E,
	WebViewObject_ClearCache_m33CCF4FE775014847A78F8D3EDEE64C944A60583,
	WebViewObject_SetTextZoom_m8FD76834FD8E118FF9034BDCBC003D091A1535E5,
	WebViewObject__ctor_mB6EABD80A28F1B3C876C5DA6D5679200F3343A2C,
	LeanPulseScale_set_BaseScale_m4B768B064BAB62AD08F305A195642E05D6B22391,
	LeanPulseScale_get_BaseScale_m27D6D46137B94F4081AA89ACD12DF253183D2FC1,
	LeanPulseScale_set_Size_mAE3D13FA565AFC5666F25552620765675F2DCA34,
	LeanPulseScale_get_Size_m5ED588012827D464CD4EEDA85FD4F8C84E22C0D6,
	LeanPulseScale_set_PulseInterval_m2C5C7519A1DC36DA76F26903B5DB0BCAA015D699,
	LeanPulseScale_get_PulseInterval_m492C9D07F01F1DCF480A182DCA5A6EDCBD45DC10,
	LeanPulseScale_set_PulseSize_m9DEECF0CBA5F1C2CE7D20ED04E4D471576F7B16C,
	LeanPulseScale_get_PulseSize_m139062A4C0B457F07C3E67D2F2E5C3ACFB7A3DCB,
	LeanPulseScale_set_Damping_mE405B5B718CDD62AA7924D0A353D4CC7691818DE,
	LeanPulseScale_get_Damping_m6ECAFE5CFBB901B79AF6218B9A3E37E7830EBE93,
	LeanPulseScale_Update_m2BC8B83015F15A629FCD11BCD047F5FAAC1E4CB6,
	LeanPulseScale__ctor_m8F1705BC190A5B9117B95C4C2D648C3D0DF48DA1,
	LeanTouchEvents_OnEnable_mC414AB54B7A99A7E68309090C6F029D1CFAF169F,
	LeanTouchEvents_OnDisable_mE1F3C9C7F96517CF14F32FEDD36532F36FBEF20F,
	LeanTouchEvents_HandleFingerDown_m4AA499342144F25ED1EDD1D59B1477EC83C483A4,
	LeanTouchEvents_HandleFingerUpdate_m0B166607F73A7FD291985551756F64D99351EB56,
	LeanTouchEvents_HandleFingerUp_m7593200A9A4D7E68B7408E1711F3F3C4761C01ED,
	LeanTouchEvents_HandleFingerTap_m69D81AABDEE2F3CB8F3169726E3139EF8DD409E3,
	LeanTouchEvents_HandleFingerSwipe_mEE3F918DA479A728FC3A767BAEF6CD38BF9D7E8B,
	LeanTouchEvents_HandleGesture_mA28336D8894E319D4A7F338244E53486E2BDFCE0,
	LeanTouchEvents__ctor_m7E262CCD26482369D4776CA6D1240B8E2305EE33,
};
static const int32_t s_InvokerIndices[73] = 
{
	9442,
	9442,
	9442,
	7469,
	9442,
	7597,
	6319,
	9442,
	9161,
	14423,
	14,
	9442,
	9442,
	9442,
	4476,
	759,
	7469,
	9161,
	7469,
	7469,
	9161,
	7469,
	9161,
	7469,
	7469,
	1874,
	7597,
	4368,
	7597,
	9247,
	9161,
	9161,
	9442,
	9442,
	9442,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	4368,
	6745,
	7597,
	9442,
	9442,
	9442,
	6745,
	4368,
	7469,
	7557,
	9442,
	7726,
	9432,
	7646,
	9343,
	7646,
	9343,
	7646,
	9343,
	7646,
	9343,
	9442,
	9442,
	9442,
	9442,
	7597,
	7597,
	7597,
	7597,
	7597,
	7597,
	9442,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	73,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
