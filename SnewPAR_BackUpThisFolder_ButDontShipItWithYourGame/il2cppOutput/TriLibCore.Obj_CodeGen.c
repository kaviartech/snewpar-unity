﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Obj.ObjRootModel::get_AllModels()
extern void ObjRootModel_get_AllModels_m1557B04C9702279905CE34CBA291DB1C9E1A3644 (void);
// 0x00000002 System.Void TriLibCore.Obj.ObjRootModel::set_AllModels(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
extern void ObjRootModel_set_AllModels_m5CCBD528B562AD652AFAB18A52CBF6D4A444B4C6 (void);
// 0x00000003 System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Obj.ObjRootModel::get_AllGeometryGroups()
extern void ObjRootModel_get_AllGeometryGroups_m1105520338237E4435B908ADD8AA3766ECE55924 (void);
// 0x00000004 System.Void TriLibCore.Obj.ObjRootModel::set_AllGeometryGroups(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>)
extern void ObjRootModel_set_AllGeometryGroups_mD8945D37B2FF1A1C0C756EBD4AACA63519FB0515 (void);
// 0x00000005 System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Obj.ObjRootModel::get_AllAnimations()
extern void ObjRootModel_get_AllAnimations_m5CB76D627BE2B0B9A0222F2CFBB21A7B14587F1D (void);
// 0x00000006 System.Void TriLibCore.Obj.ObjRootModel::set_AllAnimations(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation>)
extern void ObjRootModel_set_AllAnimations_mC613AD0B076712A10E5B794F9920B2E35828D968 (void);
// 0x00000007 System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Obj.ObjRootModel::get_AllMaterials()
extern void ObjRootModel_get_AllMaterials_mE13BFE29674C73F8C33DCAD8FBE10C9A8600AB72 (void);
// 0x00000008 System.Void TriLibCore.Obj.ObjRootModel::set_AllMaterials(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>)
extern void ObjRootModel_set_AllMaterials_m3D15333B6E928CAFA6CB24A05C4DC1858F168E65 (void);
// 0x00000009 System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Obj.ObjRootModel::get_AllTextures()
extern void ObjRootModel_get_AllTextures_mE21ED8F9FBFD7B8F6372F1ADE7983F825C350DAE (void);
// 0x0000000A System.Void TriLibCore.Obj.ObjRootModel::set_AllTextures(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>)
extern void ObjRootModel_set_AllTextures_m1BE6095AF1DD1FDE2999D49F78BEB94BAE064C12 (void);
// 0x0000000B System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Obj.ObjRootModel::get_AllCameras()
extern void ObjRootModel_get_AllCameras_m2A9AA0C09D58FBEEC49D93F106D4ADE503404AC5 (void);
// 0x0000000C System.Void TriLibCore.Obj.ObjRootModel::set_AllCameras(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera>)
extern void ObjRootModel_set_AllCameras_mDA5E7937D9EB8F5E8F678FD5F62F4334E9E48FBB (void);
// 0x0000000D System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Obj.ObjRootModel::get_AllLights()
extern void ObjRootModel_get_AllLights_m06F529219F192D506FEAC53D1A5C8D93B02D06EC (void);
// 0x0000000E System.Void TriLibCore.Obj.ObjRootModel::set_AllLights(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight>)
extern void ObjRootModel_set_AllLights_m2DCB53285DF60D20BAB93013D49104FB61484DF4 (void);
// 0x0000000F System.Void TriLibCore.Obj.ObjRootModel::.ctor()
extern void ObjRootModel__ctor_m25DD956602741340A765164805EF9C989F223ED1 (void);
// 0x00000010 System.Void TriLibCore.Obj.ObjGeometry::.ctor()
extern void ObjGeometry__ctor_m9B2EB8EF2F3A259A5062B04EA05794E6437D10A8 (void);
// 0x00000011 System.String TriLibCore.Obj.ObjMaterial::get_Name()
extern void ObjMaterial_get_Name_mF3BE829D1E68B20A1742A213B93B0655A184C4CC (void);
// 0x00000012 System.Void TriLibCore.Obj.ObjMaterial::set_Name(System.String)
extern void ObjMaterial_set_Name_m67758F02A6ED3829BA138D5986688883DD45405A (void);
// 0x00000013 System.Boolean TriLibCore.Obj.ObjMaterial::get_Used()
extern void ObjMaterial_get_Used_m59F3B08D9E01ED70B551A29AFDA42DCBB84420F0 (void);
// 0x00000014 System.Void TriLibCore.Obj.ObjMaterial::set_Used(System.Boolean)
extern void ObjMaterial_set_Used_m86B2D87CF79EEF7A65F49539FA1E201F342634B3 (void);
// 0x00000015 System.Boolean TriLibCore.Obj.ObjMaterial::get_UsesAlpha()
extern void ObjMaterial_get_UsesAlpha_mE4C8D6E458B450F6419BB662AD2E755ECA6FFB1A (void);
// 0x00000016 System.Boolean TriLibCore.Obj.ObjMaterial::ApplyOffsetAndScale(TriLibCore.TextureLoadingContext)
extern void ObjMaterial_ApplyOffsetAndScale_m8B47D6243D0E3869DFDB3AFD3A1EB2D210DE5DEF (void);
// 0x00000017 System.Void TriLibCore.Obj.ObjMaterial::AddProperty(System.String,System.Object,System.Boolean)
extern void ObjMaterial_AddProperty_m78788950C36C6D2BEA61A72CD850F4CFE1914B8F (void);
// 0x00000018 System.String TriLibCore.Obj.ObjMaterial::GetGenericPropertyName(TriLibCore.General.GenericMaterialProperty)
extern void ObjMaterial_GetGenericPropertyName_m1451CD5FE3EE3200B7AE0D2D0762F9F035FFE0A6 (void);
// 0x00000019 UnityEngine.Color TriLibCore.Obj.ObjMaterial::GetGenericColorValueMultiplied(TriLibCore.General.GenericMaterialProperty,TriLibCore.MaterialMapperContext)
extern void ObjMaterial_GetGenericColorValueMultiplied_mA702465153903FD9B5B3A50E2A090140EC6C5FCF (void);
// 0x0000001A System.Single TriLibCore.Obj.ObjMaterial::GetGenericFloatValueMultiplied(TriLibCore.General.GenericMaterialProperty,TriLibCore.MaterialMapperContext)
extern void ObjMaterial_GetGenericFloatValueMultiplied_m41BB77E5AAA20D1498D1F4B37AEEAE7E59E6A17F (void);
// 0x0000001B System.Single TriLibCore.Obj.ObjMaterial::GetGenericFloatValue(TriLibCore.General.GenericMaterialProperty)
extern void ObjMaterial_GetGenericFloatValue_mEEFBCD54C6BDF1FB049105BB9E62C54D6FE159BF (void);
// 0x0000001C System.Int32 TriLibCore.Obj.ObjMaterial::GetGenericIntValue(TriLibCore.General.GenericMaterialProperty)
extern void ObjMaterial_GetGenericIntValue_mE7F415D30FF462958B465A2C8E12C6C8EF6378D6 (void);
// 0x0000001D System.String TriLibCore.Obj.ObjMaterial::GetGenericStringValue(TriLibCore.General.GenericMaterialProperty)
extern void ObjMaterial_GetGenericStringValue_m5F9E576E152616DACF51AACE6E901A76971E6780 (void);
// 0x0000001E UnityEngine.Vector3 TriLibCore.Obj.ObjMaterial::GetGenericVector3Value(TriLibCore.General.GenericMaterialProperty)
extern void ObjMaterial_GetGenericVector3Value_m3EBD5D6D34FCCDEE3926188EDAE3854C7ABCC887 (void);
// 0x0000001F UnityEngine.Vector4 TriLibCore.Obj.ObjMaterial::GetGenericVector4Value(TriLibCore.General.GenericMaterialProperty)
extern void ObjMaterial_GetGenericVector4Value_m8BA18A98ED497BED40899B421E997CC9349431B5 (void);
// 0x00000020 UnityEngine.Color TriLibCore.Obj.ObjMaterial::GetGenericColorValue(TriLibCore.General.GenericMaterialProperty)
extern void ObjMaterial_GetGenericColorValue_mE88182F84854D6429EC598DC292D6067F802B0A3 (void);
// 0x00000021 TriLibCore.Interfaces.ITexture TriLibCore.Obj.ObjMaterial::GetGenericTextureValue(TriLibCore.General.GenericMaterialProperty)
extern void ObjMaterial_GetGenericTextureValue_mA247FE477E6460ACFF1C9E5CE8042D4477D80FE0 (void);
// 0x00000022 System.Single TriLibCore.Obj.ObjMaterial::GetFloatValue(System.String)
extern void ObjMaterial_GetFloatValue_m08F03CEDCFBFF2915CFDCF874FB69AF4F29A3DFE (void);
// 0x00000023 System.Int32 TriLibCore.Obj.ObjMaterial::GetIntValue(System.String)
extern void ObjMaterial_GetIntValue_m8BB38F5F713591AD5572E60BB3A2620752A6196D (void);
// 0x00000024 System.String TriLibCore.Obj.ObjMaterial::GetStringValue(System.String)
extern void ObjMaterial_GetStringValue_m186A7CC792B1953ACC93F428C43BC68C273D20DD (void);
// 0x00000025 UnityEngine.Vector3 TriLibCore.Obj.ObjMaterial::GetVector3Value(System.String)
extern void ObjMaterial_GetVector3Value_mAA7F106DA990F073725640890F26A854340AC38C (void);
// 0x00000026 UnityEngine.Vector4 TriLibCore.Obj.ObjMaterial::GetVector4Value(System.String)
extern void ObjMaterial_GetVector4Value_m55BBC5AEDF609A18DB476EDA9676FDA5C55020A5 (void);
// 0x00000027 UnityEngine.Color TriLibCore.Obj.ObjMaterial::GetColorValue(System.String)
extern void ObjMaterial_GetColorValue_mC3440DA2624514F044C31909B9B2B38633AB1290 (void);
// 0x00000028 TriLibCore.Interfaces.ITexture TriLibCore.Obj.ObjMaterial::GetTextureValue(System.String)
extern void ObjMaterial_GetTextureValue_mC1F722C0B59AE59081C4D7474C4174CB7532C255 (void);
// 0x00000029 System.Boolean TriLibCore.Obj.ObjMaterial::HasProperty(System.String)
extern void ObjMaterial_HasProperty_m3FE827837C6F09B505634ED4C79D3413E51921EA (void);
// 0x0000002A System.Boolean TriLibCore.Obj.ObjMaterial::PostProcessTexture(TriLibCore.TextureLoadingContext)
extern void ObjMaterial_PostProcessTexture_m35F0712F7DA575A2483B0F3B2155BF64C5629412 (void);
// 0x0000002B TriLibCore.General.MaterialShadingSetup TriLibCore.Obj.ObjMaterial::get_MaterialShadingSetup()
extern void ObjMaterial_get_MaterialShadingSetup_m96F5643BF0F7BD4A30003BEDC7CB082670E552D0 (void);
// 0x0000002C System.Int32 TriLibCore.Obj.ObjMaterial::get_Index()
extern void ObjMaterial_get_Index_m6B04DB65B55F2A00680C3411C82B02F59B12D4A6 (void);
// 0x0000002D System.Void TriLibCore.Obj.ObjMaterial::set_Index(System.Int32)
extern void ObjMaterial_set_Index_m6BA9209E1B1B198D6002DC4B38FA99383C61E217 (void);
// 0x0000002E System.Boolean TriLibCore.Obj.ObjMaterial::get_IsAutodeskInteractive()
extern void ObjMaterial_get_IsAutodeskInteractive_m7AFE0C7CE79E417E0E4DD8DD0EDD57144252242B (void);
// 0x0000002F System.Boolean TriLibCore.Obj.ObjMaterial::get_Processing()
extern void ObjMaterial_get_Processing_mD422B69A5E6FB75BF81623CC39E56109CB174291 (void);
// 0x00000030 System.Void TriLibCore.Obj.ObjMaterial::set_Processing(System.Boolean)
extern void ObjMaterial_set_Processing_m62EFAFBAE74DA86B7DD74382ED24CC328169C865 (void);
// 0x00000031 System.Boolean TriLibCore.Obj.ObjMaterial::get_Processed()
extern void ObjMaterial_get_Processed_m34F675C2D220C719C56885FAED89E5EB26143C94 (void);
// 0x00000032 System.Void TriLibCore.Obj.ObjMaterial::set_Processed(System.Boolean)
extern void ObjMaterial_set_Processed_m50933F26D622A21AB16D9BE28F6305E68CFC5EA9 (void);
// 0x00000033 System.Void TriLibCore.Obj.ObjMaterial::.ctor()
extern void ObjMaterial__ctor_mD7A1B85EE2BF4E483F380826BC4DFDCB93C5B5BF (void);
// 0x00000034 System.String TriLibCore.Obj.ObjModel::get_Name()
extern void ObjModel_get_Name_m2C8E73E063FFEC8C3C8E52E6B9D7F8344D1502B5 (void);
// 0x00000035 System.Void TriLibCore.Obj.ObjModel::set_Name(System.String)
extern void ObjModel_set_Name_m16F5A674D6657E7BA99CE421FA3843FA5BC2E2CF (void);
// 0x00000036 System.Boolean TriLibCore.Obj.ObjModel::get_Used()
extern void ObjModel_get_Used_mEA1A39F8F76139F56C6112C7E137C009E650B8F5 (void);
// 0x00000037 System.Void TriLibCore.Obj.ObjModel::set_Used(System.Boolean)
extern void ObjModel_set_Used_m59F9B2D9908A86B73AD9375AEA4701819D276009 (void);
// 0x00000038 UnityEngine.Vector3 TriLibCore.Obj.ObjModel::get_LocalPosition()
extern void ObjModel_get_LocalPosition_m019943AE98C9F63D000FBEBFF4ADC3664C4AD672 (void);
// 0x00000039 System.Void TriLibCore.Obj.ObjModel::set_LocalPosition(UnityEngine.Vector3)
extern void ObjModel_set_LocalPosition_mA40B5B435DF47CE280E890F9D725FC27B755B976 (void);
// 0x0000003A UnityEngine.Quaternion TriLibCore.Obj.ObjModel::get_LocalRotation()
extern void ObjModel_get_LocalRotation_m54F47E9CFAD8699190DCDCA1ACBBD1CA4D0367FA (void);
// 0x0000003B System.Void TriLibCore.Obj.ObjModel::set_LocalRotation(UnityEngine.Quaternion)
extern void ObjModel_set_LocalRotation_mB967A8D0DDEB6E6D6A107CD392E8C7C5EBF0DE84 (void);
// 0x0000003C UnityEngine.Vector3 TriLibCore.Obj.ObjModel::get_LocalScale()
extern void ObjModel_get_LocalScale_m617288C7F81768CF0560EBDB24482C7301B0F9B7 (void);
// 0x0000003D System.Void TriLibCore.Obj.ObjModel::set_LocalScale(UnityEngine.Vector3)
extern void ObjModel_set_LocalScale_m2BE5321944A5EFE896B8B77664313472F5A7A83D (void);
// 0x0000003E System.Boolean TriLibCore.Obj.ObjModel::get_Visibility()
extern void ObjModel_get_Visibility_m31DD540B169D23EF45CBD6F5AFA72F991FC8DEE0 (void);
// 0x0000003F System.Void TriLibCore.Obj.ObjModel::set_Visibility(System.Boolean)
extern void ObjModel_set_Visibility_mD3B2EA70D2AC5D6DCD50E341267F61D1BE80C3E9 (void);
// 0x00000040 TriLibCore.Interfaces.IModel TriLibCore.Obj.ObjModel::get_Parent()
extern void ObjModel_get_Parent_m173B8A10382E6CB5C03E6BC7AA24C65609C5263C (void);
// 0x00000041 System.Void TriLibCore.Obj.ObjModel::set_Parent(TriLibCore.Interfaces.IModel)
extern void ObjModel_set_Parent_m2FF3074C74C41BEB282D8BDCCC16134402F062F5 (void);
// 0x00000042 System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Obj.ObjModel::get_Children()
extern void ObjModel_get_Children_m51CDC47B9C398CABCA8C63316778281224BB8F5C (void);
// 0x00000043 System.Void TriLibCore.Obj.ObjModel::set_Children(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
extern void ObjModel_set_Children_m49B71BE2A532A1E33620312E928AB6000F7598C6 (void);
// 0x00000044 System.Boolean TriLibCore.Obj.ObjModel::get_IsBone()
extern void ObjModel_get_IsBone_m84C8F6146737ECA122E3E5F56E7D0914300FC947 (void);
// 0x00000045 System.Void TriLibCore.Obj.ObjModel::set_IsBone(System.Boolean)
extern void ObjModel_set_IsBone_m616213BCB93FE374CDEB21DAEA01391E4F1F405E (void);
// 0x00000046 TriLibCore.Interfaces.IGeometryGroup TriLibCore.Obj.ObjModel::get_GeometryGroup()
extern void ObjModel_get_GeometryGroup_m79E91A44068854E22B18748ECE927C0236A22592 (void);
// 0x00000047 System.Void TriLibCore.Obj.ObjModel::set_GeometryGroup(TriLibCore.Interfaces.IGeometryGroup)
extern void ObjModel_set_GeometryGroup_m8CBDE00B3C5C23B045DF004687AF41709B149948 (void);
// 0x00000048 System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Obj.ObjModel::get_Bones()
extern void ObjModel_get_Bones_m3B23943A5B1E7B619DF3D630EA3A33F28DC64024 (void);
// 0x00000049 System.Void TriLibCore.Obj.ObjModel::set_Bones(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
extern void ObjModel_set_Bones_mE16ECF86C4662EFACE0B67B63B315EEB8B36AE4B (void);
// 0x0000004A System.String TriLibCore.Obj.ObjModel::ToString()
extern void ObjModel_ToString_mF1B1D0D6EC0911606CF1BBB3A0ABFA531119311A (void);
// 0x0000004B UnityEngine.Matrix4x4[] TriLibCore.Obj.ObjModel::get_BindPoses()
extern void ObjModel_get_BindPoses_mEFF5144DFAEB4489F27682C328A9D853A43E95A6 (void);
// 0x0000004C System.Void TriLibCore.Obj.ObjModel::set_BindPoses(UnityEngine.Matrix4x4[])
extern void ObjModel_set_BindPoses_m2AE0752DB8E469FCDD91D4E9F4F7273E154536E6 (void);
// 0x0000004D System.Collections.Generic.IList`1<System.Int32> TriLibCore.Obj.ObjModel::get_MaterialIndices()
extern void ObjModel_get_MaterialIndices_m46511D09445C94A8E0E45F4FF42353DF2656A129 (void);
// 0x0000004E System.Void TriLibCore.Obj.ObjModel::set_MaterialIndices(System.Collections.Generic.IList`1<System.Int32>)
extern void ObjModel_set_MaterialIndices_m62D473B0D762A3BADFE5EC5D957DF518933229D5 (void);
// 0x0000004F System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Obj.ObjModel::get_UserProperties()
extern void ObjModel_get_UserProperties_m427BDF56DEC491604945BC9EE3F01F085BEA6145 (void);
// 0x00000050 System.Void TriLibCore.Obj.ObjModel::set_UserProperties(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void ObjModel_set_UserProperties_m47B929D487E0B3E13D9F5182046A61C977E3CA8D (void);
// 0x00000051 System.Void TriLibCore.Obj.ObjModel::.ctor()
extern void ObjModel__ctor_mBDB61F8E37D5BD82DA87E06DD7AD52AD5EE6FC5D (void);
// 0x00000052 System.Void TriLibCore.Obj.ObjProcessor::GetActiveGeometryGroup()
extern void ObjProcessor_GetActiveGeometryGroup_m2E54325F3BA2397999B6EC2B08898A42708DEFDE (void);
// 0x00000053 System.Void TriLibCore.Obj.ObjProcessor::GetActiveGeometry(System.Boolean)
extern void ObjProcessor_GetActiveGeometry_m392FF935FA8F46A993B1D7EC8A63ADB3381E296D (void);
// 0x00000054 System.Void TriLibCore.Obj.ObjProcessor::GetActiveMaterial()
extern void ObjProcessor_GetActiveMaterial_mA738B0664B156E8C01333E3BFF33FA256835A6FD (void);
// 0x00000055 System.Void TriLibCore.Obj.ObjProcessor::GetActiveModel(TriLibCore.Interfaces.IModel)
extern void ObjProcessor_GetActiveModel_m164605039945A3AC7E08DA6646172E56F5FF7E93 (void);
// 0x00000056 System.Int32 TriLibCore.Obj.ObjProcessor::GetElementIndex(System.Int32,System.Int32)
extern void ObjProcessor_GetElementIndex_mB0677115457EA468BBB09E166DE49EA378FD751E (void);
// 0x00000057 TriLibCore.Interfaces.IRootModel TriLibCore.Obj.ObjProcessor::Process(TriLibCore.Obj.Reader.ObjReader,System.IO.Stream)
extern void ObjProcessor_Process_m2ED19B140F9C602CC611918AD4DDE0480D94E8C5 (void);
// 0x00000058 TriLibCore.Geometries.InterpolatedVertex TriLibCore.Obj.ObjProcessor::BuildVertexData(TriLibCore.Interfaces.IGeometry,TriLibCore.Interfaces.IGeometryGroup,System.Collections.Generic.List`1<TriLibCore.Geometries.VertexDataIndices>,System.Int32)
extern void ObjProcessor_BuildVertexData_m87DDCCD3284C359CDF1CCD4730B6C2BB823485EB (void);
// 0x00000059 System.Void TriLibCore.Obj.ObjProcessor::AddVertex(TriLibCore.Geometries.Geometry,System.Collections.Generic.IList`1<TriLibCore.Geometries.VertexDataIndices>,System.Int32)
extern void ObjProcessor_AddVertex_mEEED2CF181FFE9E10B6EB08BDE613B966CB35FC4 (void);
// 0x0000005A System.Void TriLibCore.Obj.ObjProcessor::ProcessMaterialLibrary(System.String)
extern void ObjProcessor_ProcessMaterialLibrary_mF170F7025A2BDCF97AAAC0021EA4C4E8B69EC7A1 (void);
// 0x0000005B TriLibCore.Obj.ObjTexture TriLibCore.Obj.ObjProcessor::ProcessMap(TriLibCore.Obj.ObjStreamReader,System.Single&)
extern void ObjProcessor_ProcessMap_mEE7E8F1880D0CA92C7190C92D41A8282A2A6385E (void);
// 0x0000005C System.Void TriLibCore.Obj.ObjProcessor::.ctor()
extern void ObjProcessor__ctor_mDE4C9639F1838CE0766C3DC8F8B1DCECDBBB8759 (void);
// 0x0000005D System.Void TriLibCore.Obj.ObjProcessor::.cctor()
extern void ObjProcessor__cctor_m796D5FF50A0E05082A6538E9F01F485C78A84D4F (void);
// 0x0000005E System.Single TriLibCore.Obj.ObjStreamReader::ReadTokenAsFloat(System.Boolean,System.Boolean,System.Boolean)
extern void ObjStreamReader_ReadTokenAsFloat_m4DADB0413F81E01B6B226C23D6354551FBB1933F (void);
// 0x0000005F System.String TriLibCore.Obj.ObjStreamReader::ReadTokenAsString(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void ObjStreamReader_ReadTokenAsString_mB5EB72F2244914D6C73C8508E204C1081B159146 (void);
// 0x00000060 System.Boolean TriLibCore.Obj.ObjStreamReader::TokenStartsWith(System.String)
extern void ObjStreamReader_TokenStartsWith_mAE587D9AB2C842D4C4C0B647CF975D285D1191A4 (void);
// 0x00000061 System.Int32 TriLibCore.Obj.ObjStreamReader::GetCharAt(System.Int32)
extern void ObjStreamReader_GetCharAt_m6D77DC69528E4222E0472A9BFC17471C7D5038E8 (void);
// 0x00000062 System.Void TriLibCore.Obj.ObjStreamReader::CopyCharStreamToString(System.String,System.Int32)
extern void ObjStreamReader_CopyCharStreamToString_m1C26AC2DB01318CD21F1E8F6769F114C34E94283 (void);
// 0x00000063 System.String TriLibCore.Obj.ObjStreamReader::GetTokenAsString()
extern void ObjStreamReader_GetTokenAsString_m5CBEB0CD09F8C6438AE79E4E70A8D6B309C86B41 (void);
// 0x00000064 System.Boolean TriLibCore.Obj.ObjStreamReader::GetTokenAsFloat(System.Single&)
extern void ObjStreamReader_GetTokenAsFloat_m93AC0588EF7DC6B8A00D258C22C882BCC451ECB7 (void);
// 0x00000065 System.Boolean TriLibCore.Obj.ObjStreamReader::GetTokenAsInt(System.Int32&)
extern void ObjStreamReader_GetTokenAsInt_m03D488F4FCA26840E78432A260953E84E81A163B (void);
// 0x00000066 System.Void TriLibCore.Obj.ObjStreamReader::UpdateStringFromCharString()
extern void ObjStreamReader_UpdateStringFromCharString_mBA24B430F0BD06295EDBAFE5512FB5FCE66DA419 (void);
// 0x00000067 System.Int64 TriLibCore.Obj.ObjStreamReader::ReadToken(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void ObjStreamReader_ReadToken_m1CCF5745DEB3B4C2BA42FAFC437EBB05A29EE148 (void);
// 0x00000068 System.Void TriLibCore.Obj.ObjStreamReader::.ctor(System.IO.Stream)
extern void ObjStreamReader__ctor_m2D1227C325CCDA6E71B366230080091D803533FA (void);
// 0x00000069 System.Void TriLibCore.Obj.ObjStreamReader::Dispose(System.Boolean)
extern void ObjStreamReader_Dispose_m76FE0A6CFAE172C1BDE6C9DD0C979F54189D8973 (void);
// 0x0000006A System.String TriLibCore.Obj.ObjTexture::get_Name()
extern void ObjTexture_get_Name_mEB8DE4B35411D93598982A9CA91506FDCC00213F (void);
// 0x0000006B System.Void TriLibCore.Obj.ObjTexture::set_Name(System.String)
extern void ObjTexture_set_Name_m16296B436CCC7FB00A0F1BE0EDEE008ECABF7270 (void);
// 0x0000006C System.Boolean TriLibCore.Obj.ObjTexture::get_Used()
extern void ObjTexture_get_Used_mE09C860397381ADE41580776D940F4FC71E3AB0D (void);
// 0x0000006D System.Void TriLibCore.Obj.ObjTexture::set_Used(System.Boolean)
extern void ObjTexture_set_Used_m2000A3EDFDCEC9B251757F4239E07BB9ADE1AAEF (void);
// 0x0000006E TriLibCore.Interfaces.ITexture TriLibCore.Obj.ObjTexture::GetSubTexture(System.Int32)
extern void ObjTexture_GetSubTexture_mE50635021956AC8A76EAAC7EEEACD3C0B9A750A6 (void);
// 0x0000006F System.Int32 TriLibCore.Obj.ObjTexture::GetSubTextureCount()
extern void ObjTexture_GetSubTextureCount_mEE530E3B6BB0A279B5472D9C76647366CDCBD6EA (void);
// 0x00000070 System.Single TriLibCore.Obj.ObjTexture::GetWeight(System.Int32)
extern void ObjTexture_GetWeight_m7BAA2A36FE3810AADB4759D651F1C60D620805DE (void);
// 0x00000071 System.Void TriLibCore.Obj.ObjTexture::AddTexture(TriLibCore.Interfaces.ITexture)
extern void ObjTexture_AddTexture_mFF7941A56004F6C64308C35FD379E43D3C1912B8 (void);
// 0x00000072 System.Byte[] TriLibCore.Obj.ObjTexture::get_Data()
extern void ObjTexture_get_Data_m8D98BB87F54CF6AB51D85826F827D0DCEA5984AE (void);
// 0x00000073 System.Void TriLibCore.Obj.ObjTexture::set_Data(System.Byte[])
extern void ObjTexture_set_Data_m97B9C8C2EEC7EB4855907A5FF652CCFD837592B6 (void);
// 0x00000074 System.IO.Stream TriLibCore.Obj.ObjTexture::get_DataStream()
extern void ObjTexture_get_DataStream_m23CEB1E680F3038CB87DAEA43D537971F49F5CB3 (void);
// 0x00000075 System.Void TriLibCore.Obj.ObjTexture::set_DataStream(System.IO.Stream)
extern void ObjTexture_set_DataStream_m0015701B3141ECAA2EC4B3692DA23E5BB76D5EEA (void);
// 0x00000076 System.String TriLibCore.Obj.ObjTexture::get_Filename()
extern void ObjTexture_get_Filename_mBD3FFDED225352E45B003157DF4443917A795B67 (void);
// 0x00000077 System.Void TriLibCore.Obj.ObjTexture::set_Filename(System.String)
extern void ObjTexture_set_Filename_mEB6422B88C72693BA4FA29510DA0663002D1BD36 (void);
// 0x00000078 UnityEngine.TextureWrapMode TriLibCore.Obj.ObjTexture::get_WrapModeU()
extern void ObjTexture_get_WrapModeU_mA07D9142D9C5E6925837B11D9CB36BC68822B45A (void);
// 0x00000079 System.Void TriLibCore.Obj.ObjTexture::set_WrapModeU(UnityEngine.TextureWrapMode)
extern void ObjTexture_set_WrapModeU_m584EDAA15965192877B7D4EDF529F43FDD7B098F (void);
// 0x0000007A UnityEngine.TextureWrapMode TriLibCore.Obj.ObjTexture::get_WrapModeV()
extern void ObjTexture_get_WrapModeV_m83D0BE62C7D34CF7892BD320314811D542A94B77 (void);
// 0x0000007B System.Void TriLibCore.Obj.ObjTexture::set_WrapModeV(UnityEngine.TextureWrapMode)
extern void ObjTexture_set_WrapModeV_mB95606BB378E9A1FD366BCF50A0524C0443B867F (void);
// 0x0000007C UnityEngine.Vector2 TriLibCore.Obj.ObjTexture::get_Tiling()
extern void ObjTexture_get_Tiling_m88619E4C7C513ACE793E112E3D14D2523FC9D823 (void);
// 0x0000007D System.Void TriLibCore.Obj.ObjTexture::set_Tiling(UnityEngine.Vector2)
extern void ObjTexture_set_Tiling_m6FE72BDA086B60006118A38302EF6EAA1BC0736F (void);
// 0x0000007E UnityEngine.Vector2 TriLibCore.Obj.ObjTexture::get_Offset()
extern void ObjTexture_get_Offset_m6DA6DF4522C52BC31DEBF929EB24DD3E113F6C8B (void);
// 0x0000007F System.Void TriLibCore.Obj.ObjTexture::set_Offset(UnityEngine.Vector2)
extern void ObjTexture_set_Offset_mBCBBF8403BC14A439FD9819301EE4DB55E37EF31 (void);
// 0x00000080 System.Int32 TriLibCore.Obj.ObjTexture::get_TextureId()
extern void ObjTexture_get_TextureId_m3570D9DD918AEF1C928D3B97456371D150D21512 (void);
// 0x00000081 System.Void TriLibCore.Obj.ObjTexture::set_TextureId(System.Int32)
extern void ObjTexture_set_TextureId_m6D8639A20E4F218EBB26AF130546306EBB736E6B (void);
// 0x00000082 System.String TriLibCore.Obj.ObjTexture::get_ResolvedFilename()
extern void ObjTexture_get_ResolvedFilename_m397012402BFE6A2B6A4385DA54D046975D033A4B (void);
// 0x00000083 System.Void TriLibCore.Obj.ObjTexture::set_ResolvedFilename(System.String)
extern void ObjTexture_set_ResolvedFilename_m99AA3A6F9AD5A2979A3687716B7AD2226D9F7220 (void);
// 0x00000084 System.Boolean TriLibCore.Obj.ObjTexture::get_IsValid()
extern void ObjTexture_get_IsValid_mBD972CCA041EF7B0D76B53F6ECCCF42771B5F985 (void);
// 0x00000085 System.Boolean TriLibCore.Obj.ObjTexture::get_HasAlpha()
extern void ObjTexture_get_HasAlpha_m64A81E8409B360A15ECC3AE888AC8BA8E93BB13D (void);
// 0x00000086 System.Void TriLibCore.Obj.ObjTexture::set_HasAlpha(System.Boolean)
extern void ObjTexture_set_HasAlpha_m1F66F60E6E4DB3B2EDBA0CFC223AF15745A82BAA (void);
// 0x00000087 TriLibCore.General.TextureFormat TriLibCore.Obj.ObjTexture::get_TextureFormat()
extern void ObjTexture_get_TextureFormat_m2DD8B398D39F41F0DD40357A1A75CAC5E5A7759F (void);
// 0x00000088 System.Void TriLibCore.Obj.ObjTexture::set_TextureFormat(TriLibCore.General.TextureFormat)
extern void ObjTexture_set_TextureFormat_m532BF2DC9EEF78C2DF189266CFCB3A64F65D5675 (void);
// 0x00000089 System.Boolean TriLibCore.Obj.ObjTexture::Equals(TriLibCore.Interfaces.ITexture)
extern void ObjTexture_Equals_mBAC611371DE17333A2C686E2CD0E646BFB7D56E2 (void);
// 0x0000008A System.Boolean TriLibCore.Obj.ObjTexture::Equals(System.Object)
extern void ObjTexture_Equals_m90BDF0DF24E9616BD47124C38FA3BB0FD07F0753 (void);
// 0x0000008B System.Int32 TriLibCore.Obj.ObjTexture::GetHashCode()
extern void ObjTexture_GetHashCode_m28ED4AB25AC0BA5EFDCED02395DF54E45E166BB3 (void);
// 0x0000008C System.Void TriLibCore.Obj.ObjTexture::.ctor()
extern void ObjTexture__ctor_mE85DA8AF5F68128CA229CE5F445E66B3D11B6B0C (void);
// 0x0000008D System.String[] TriLibCore.Obj.Reader.ObjReader::GetExtensions()
extern void ObjReader_GetExtensions_m6969794E7DD0E5B74986C565A8C0F7DD1B83E17B (void);
// 0x0000008E System.String TriLibCore.Obj.Reader.ObjReader::get_Name()
extern void ObjReader_get_Name_m09F1C43CDD9D7C282640DE9BCFCBA1E2A5C5EFA5 (void);
// 0x0000008F System.Type TriLibCore.Obj.Reader.ObjReader::get_LoadingStepEnumType()
extern void ObjReader_get_LoadingStepEnumType_mC332CD5DC53EE03BBFFB461F1A1329DDF911770A (void);
// 0x00000090 TriLibCore.Interfaces.IRootModel TriLibCore.Obj.Reader.ObjReader::ReadStream(System.IO.Stream,TriLibCore.AssetLoaderContext,System.String,System.Action`2<TriLibCore.AssetLoaderContext,System.Single>)
extern void ObjReader_ReadStream_m528CFDFC027D69D5582FEE6386305269DD690E60 (void);
// 0x00000091 TriLibCore.Interfaces.IRootModel TriLibCore.Obj.Reader.ObjReader::CreateRootModel()
extern void ObjReader_CreateRootModel_m74202A90181419D918275B7207BCD58940A236E3 (void);
// 0x00000092 System.Void TriLibCore.Obj.Reader.ObjReader::.ctor()
extern void ObjReader__ctor_mA11F4582A777AF963F4449EAC9FA6872AD461620 (void);
static Il2CppMethodPointer s_methodPointers[146] = 
{
	ObjRootModel_get_AllModels_m1557B04C9702279905CE34CBA291DB1C9E1A3644,
	ObjRootModel_set_AllModels_m5CCBD528B562AD652AFAB18A52CBF6D4A444B4C6,
	ObjRootModel_get_AllGeometryGroups_m1105520338237E4435B908ADD8AA3766ECE55924,
	ObjRootModel_set_AllGeometryGroups_mD8945D37B2FF1A1C0C756EBD4AACA63519FB0515,
	ObjRootModel_get_AllAnimations_m5CB76D627BE2B0B9A0222F2CFBB21A7B14587F1D,
	ObjRootModel_set_AllAnimations_mC613AD0B076712A10E5B794F9920B2E35828D968,
	ObjRootModel_get_AllMaterials_mE13BFE29674C73F8C33DCAD8FBE10C9A8600AB72,
	ObjRootModel_set_AllMaterials_m3D15333B6E928CAFA6CB24A05C4DC1858F168E65,
	ObjRootModel_get_AllTextures_mE21ED8F9FBFD7B8F6372F1ADE7983F825C350DAE,
	ObjRootModel_set_AllTextures_m1BE6095AF1DD1FDE2999D49F78BEB94BAE064C12,
	ObjRootModel_get_AllCameras_m2A9AA0C09D58FBEEC49D93F106D4ADE503404AC5,
	ObjRootModel_set_AllCameras_mDA5E7937D9EB8F5E8F678FD5F62F4334E9E48FBB,
	ObjRootModel_get_AllLights_m06F529219F192D506FEAC53D1A5C8D93B02D06EC,
	ObjRootModel_set_AllLights_m2DCB53285DF60D20BAB93013D49104FB61484DF4,
	ObjRootModel__ctor_m25DD956602741340A765164805EF9C989F223ED1,
	ObjGeometry__ctor_m9B2EB8EF2F3A259A5062B04EA05794E6437D10A8,
	ObjMaterial_get_Name_mF3BE829D1E68B20A1742A213B93B0655A184C4CC,
	ObjMaterial_set_Name_m67758F02A6ED3829BA138D5986688883DD45405A,
	ObjMaterial_get_Used_m59F3B08D9E01ED70B551A29AFDA42DCBB84420F0,
	ObjMaterial_set_Used_m86B2D87CF79EEF7A65F49539FA1E201F342634B3,
	ObjMaterial_get_UsesAlpha_mE4C8D6E458B450F6419BB662AD2E755ECA6FFB1A,
	ObjMaterial_ApplyOffsetAndScale_m8B47D6243D0E3869DFDB3AFD3A1EB2D210DE5DEF,
	ObjMaterial_AddProperty_m78788950C36C6D2BEA61A72CD850F4CFE1914B8F,
	ObjMaterial_GetGenericPropertyName_m1451CD5FE3EE3200B7AE0D2D0762F9F035FFE0A6,
	ObjMaterial_GetGenericColorValueMultiplied_mA702465153903FD9B5B3A50E2A090140EC6C5FCF,
	ObjMaterial_GetGenericFloatValueMultiplied_m41BB77E5AAA20D1498D1F4B37AEEAE7E59E6A17F,
	ObjMaterial_GetGenericFloatValue_mEEFBCD54C6BDF1FB049105BB9E62C54D6FE159BF,
	ObjMaterial_GetGenericIntValue_mE7F415D30FF462958B465A2C8E12C6C8EF6378D6,
	ObjMaterial_GetGenericStringValue_m5F9E576E152616DACF51AACE6E901A76971E6780,
	ObjMaterial_GetGenericVector3Value_m3EBD5D6D34FCCDEE3926188EDAE3854C7ABCC887,
	ObjMaterial_GetGenericVector4Value_m8BA18A98ED497BED40899B421E997CC9349431B5,
	ObjMaterial_GetGenericColorValue_mE88182F84854D6429EC598DC292D6067F802B0A3,
	ObjMaterial_GetGenericTextureValue_mA247FE477E6460ACFF1C9E5CE8042D4477D80FE0,
	ObjMaterial_GetFloatValue_m08F03CEDCFBFF2915CFDCF874FB69AF4F29A3DFE,
	ObjMaterial_GetIntValue_m8BB38F5F713591AD5572E60BB3A2620752A6196D,
	ObjMaterial_GetStringValue_m186A7CC792B1953ACC93F428C43BC68C273D20DD,
	ObjMaterial_GetVector3Value_mAA7F106DA990F073725640890F26A854340AC38C,
	ObjMaterial_GetVector4Value_m55BBC5AEDF609A18DB476EDA9676FDA5C55020A5,
	ObjMaterial_GetColorValue_mC3440DA2624514F044C31909B9B2B38633AB1290,
	ObjMaterial_GetTextureValue_mC1F722C0B59AE59081C4D7474C4174CB7532C255,
	ObjMaterial_HasProperty_m3FE827837C6F09B505634ED4C79D3413E51921EA,
	ObjMaterial_PostProcessTexture_m35F0712F7DA575A2483B0F3B2155BF64C5629412,
	ObjMaterial_get_MaterialShadingSetup_m96F5643BF0F7BD4A30003BEDC7CB082670E552D0,
	ObjMaterial_get_Index_m6B04DB65B55F2A00680C3411C82B02F59B12D4A6,
	ObjMaterial_set_Index_m6BA9209E1B1B198D6002DC4B38FA99383C61E217,
	ObjMaterial_get_IsAutodeskInteractive_m7AFE0C7CE79E417E0E4DD8DD0EDD57144252242B,
	ObjMaterial_get_Processing_mD422B69A5E6FB75BF81623CC39E56109CB174291,
	ObjMaterial_set_Processing_m62EFAFBAE74DA86B7DD74382ED24CC328169C865,
	ObjMaterial_get_Processed_m34F675C2D220C719C56885FAED89E5EB26143C94,
	ObjMaterial_set_Processed_m50933F26D622A21AB16D9BE28F6305E68CFC5EA9,
	ObjMaterial__ctor_mD7A1B85EE2BF4E483F380826BC4DFDCB93C5B5BF,
	ObjModel_get_Name_m2C8E73E063FFEC8C3C8E52E6B9D7F8344D1502B5,
	ObjModel_set_Name_m16F5A674D6657E7BA99CE421FA3843FA5BC2E2CF,
	ObjModel_get_Used_mEA1A39F8F76139F56C6112C7E137C009E650B8F5,
	ObjModel_set_Used_m59F9B2D9908A86B73AD9375AEA4701819D276009,
	ObjModel_get_LocalPosition_m019943AE98C9F63D000FBEBFF4ADC3664C4AD672,
	ObjModel_set_LocalPosition_mA40B5B435DF47CE280E890F9D725FC27B755B976,
	ObjModel_get_LocalRotation_m54F47E9CFAD8699190DCDCA1ACBBD1CA4D0367FA,
	ObjModel_set_LocalRotation_mB967A8D0DDEB6E6D6A107CD392E8C7C5EBF0DE84,
	ObjModel_get_LocalScale_m617288C7F81768CF0560EBDB24482C7301B0F9B7,
	ObjModel_set_LocalScale_m2BE5321944A5EFE896B8B77664313472F5A7A83D,
	ObjModel_get_Visibility_m31DD540B169D23EF45CBD6F5AFA72F991FC8DEE0,
	ObjModel_set_Visibility_mD3B2EA70D2AC5D6DCD50E341267F61D1BE80C3E9,
	ObjModel_get_Parent_m173B8A10382E6CB5C03E6BC7AA24C65609C5263C,
	ObjModel_set_Parent_m2FF3074C74C41BEB282D8BDCCC16134402F062F5,
	ObjModel_get_Children_m51CDC47B9C398CABCA8C63316778281224BB8F5C,
	ObjModel_set_Children_m49B71BE2A532A1E33620312E928AB6000F7598C6,
	ObjModel_get_IsBone_m84C8F6146737ECA122E3E5F56E7D0914300FC947,
	ObjModel_set_IsBone_m616213BCB93FE374CDEB21DAEA01391E4F1F405E,
	ObjModel_get_GeometryGroup_m79E91A44068854E22B18748ECE927C0236A22592,
	ObjModel_set_GeometryGroup_m8CBDE00B3C5C23B045DF004687AF41709B149948,
	ObjModel_get_Bones_m3B23943A5B1E7B619DF3D630EA3A33F28DC64024,
	ObjModel_set_Bones_mE16ECF86C4662EFACE0B67B63B315EEB8B36AE4B,
	ObjModel_ToString_mF1B1D0D6EC0911606CF1BBB3A0ABFA531119311A,
	ObjModel_get_BindPoses_mEFF5144DFAEB4489F27682C328A9D853A43E95A6,
	ObjModel_set_BindPoses_m2AE0752DB8E469FCDD91D4E9F4F7273E154536E6,
	ObjModel_get_MaterialIndices_m46511D09445C94A8E0E45F4FF42353DF2656A129,
	ObjModel_set_MaterialIndices_m62D473B0D762A3BADFE5EC5D957DF518933229D5,
	ObjModel_get_UserProperties_m427BDF56DEC491604945BC9EE3F01F085BEA6145,
	ObjModel_set_UserProperties_m47B929D487E0B3E13D9F5182046A61C977E3CA8D,
	ObjModel__ctor_mBDB61F8E37D5BD82DA87E06DD7AD52AD5EE6FC5D,
	ObjProcessor_GetActiveGeometryGroup_m2E54325F3BA2397999B6EC2B08898A42708DEFDE,
	ObjProcessor_GetActiveGeometry_m392FF935FA8F46A993B1D7EC8A63ADB3381E296D,
	ObjProcessor_GetActiveMaterial_mA738B0664B156E8C01333E3BFF33FA256835A6FD,
	ObjProcessor_GetActiveModel_m164605039945A3AC7E08DA6646172E56F5FF7E93,
	ObjProcessor_GetElementIndex_mB0677115457EA468BBB09E166DE49EA378FD751E,
	ObjProcessor_Process_m2ED19B140F9C602CC611918AD4DDE0480D94E8C5,
	ObjProcessor_BuildVertexData_m87DDCCD3284C359CDF1CCD4730B6C2BB823485EB,
	ObjProcessor_AddVertex_mEEED2CF181FFE9E10B6EB08BDE613B966CB35FC4,
	ObjProcessor_ProcessMaterialLibrary_mF170F7025A2BDCF97AAAC0021EA4C4E8B69EC7A1,
	ObjProcessor_ProcessMap_mEE7E8F1880D0CA92C7190C92D41A8282A2A6385E,
	ObjProcessor__ctor_mDE4C9639F1838CE0766C3DC8F8B1DCECDBBB8759,
	ObjProcessor__cctor_m796D5FF50A0E05082A6538E9F01F485C78A84D4F,
	ObjStreamReader_ReadTokenAsFloat_m4DADB0413F81E01B6B226C23D6354551FBB1933F,
	ObjStreamReader_ReadTokenAsString_mB5EB72F2244914D6C73C8508E204C1081B159146,
	ObjStreamReader_TokenStartsWith_mAE587D9AB2C842D4C4C0B647CF975D285D1191A4,
	ObjStreamReader_GetCharAt_m6D77DC69528E4222E0472A9BFC17471C7D5038E8,
	ObjStreamReader_CopyCharStreamToString_m1C26AC2DB01318CD21F1E8F6769F114C34E94283,
	ObjStreamReader_GetTokenAsString_m5CBEB0CD09F8C6438AE79E4E70A8D6B309C86B41,
	ObjStreamReader_GetTokenAsFloat_m93AC0588EF7DC6B8A00D258C22C882BCC451ECB7,
	ObjStreamReader_GetTokenAsInt_m03D488F4FCA26840E78432A260953E84E81A163B,
	ObjStreamReader_UpdateStringFromCharString_mBA24B430F0BD06295EDBAFE5512FB5FCE66DA419,
	ObjStreamReader_ReadToken_m1CCF5745DEB3B4C2BA42FAFC437EBB05A29EE148,
	ObjStreamReader__ctor_m2D1227C325CCDA6E71B366230080091D803533FA,
	ObjStreamReader_Dispose_m76FE0A6CFAE172C1BDE6C9DD0C979F54189D8973,
	ObjTexture_get_Name_mEB8DE4B35411D93598982A9CA91506FDCC00213F,
	ObjTexture_set_Name_m16296B436CCC7FB00A0F1BE0EDEE008ECABF7270,
	ObjTexture_get_Used_mE09C860397381ADE41580776D940F4FC71E3AB0D,
	ObjTexture_set_Used_m2000A3EDFDCEC9B251757F4239E07BB9ADE1AAEF,
	ObjTexture_GetSubTexture_mE50635021956AC8A76EAAC7EEEACD3C0B9A750A6,
	ObjTexture_GetSubTextureCount_mEE530E3B6BB0A279B5472D9C76647366CDCBD6EA,
	ObjTexture_GetWeight_m7BAA2A36FE3810AADB4759D651F1C60D620805DE,
	ObjTexture_AddTexture_mFF7941A56004F6C64308C35FD379E43D3C1912B8,
	ObjTexture_get_Data_m8D98BB87F54CF6AB51D85826F827D0DCEA5984AE,
	ObjTexture_set_Data_m97B9C8C2EEC7EB4855907A5FF652CCFD837592B6,
	ObjTexture_get_DataStream_m23CEB1E680F3038CB87DAEA43D537971F49F5CB3,
	ObjTexture_set_DataStream_m0015701B3141ECAA2EC4B3692DA23E5BB76D5EEA,
	ObjTexture_get_Filename_mBD3FFDED225352E45B003157DF4443917A795B67,
	ObjTexture_set_Filename_mEB6422B88C72693BA4FA29510DA0663002D1BD36,
	ObjTexture_get_WrapModeU_mA07D9142D9C5E6925837B11D9CB36BC68822B45A,
	ObjTexture_set_WrapModeU_m584EDAA15965192877B7D4EDF529F43FDD7B098F,
	ObjTexture_get_WrapModeV_m83D0BE62C7D34CF7892BD320314811D542A94B77,
	ObjTexture_set_WrapModeV_mB95606BB378E9A1FD366BCF50A0524C0443B867F,
	ObjTexture_get_Tiling_m88619E4C7C513ACE793E112E3D14D2523FC9D823,
	ObjTexture_set_Tiling_m6FE72BDA086B60006118A38302EF6EAA1BC0736F,
	ObjTexture_get_Offset_m6DA6DF4522C52BC31DEBF929EB24DD3E113F6C8B,
	ObjTexture_set_Offset_mBCBBF8403BC14A439FD9819301EE4DB55E37EF31,
	ObjTexture_get_TextureId_m3570D9DD918AEF1C928D3B97456371D150D21512,
	ObjTexture_set_TextureId_m6D8639A20E4F218EBB26AF130546306EBB736E6B,
	ObjTexture_get_ResolvedFilename_m397012402BFE6A2B6A4385DA54D046975D033A4B,
	ObjTexture_set_ResolvedFilename_m99AA3A6F9AD5A2979A3687716B7AD2226D9F7220,
	ObjTexture_get_IsValid_mBD972CCA041EF7B0D76B53F6ECCCF42771B5F985,
	ObjTexture_get_HasAlpha_m64A81E8409B360A15ECC3AE888AC8BA8E93BB13D,
	ObjTexture_set_HasAlpha_m1F66F60E6E4DB3B2EDBA0CFC223AF15745A82BAA,
	ObjTexture_get_TextureFormat_m2DD8B398D39F41F0DD40357A1A75CAC5E5A7759F,
	ObjTexture_set_TextureFormat_m532BF2DC9EEF78C2DF189266CFCB3A64F65D5675,
	ObjTexture_Equals_mBAC611371DE17333A2C686E2CD0E646BFB7D56E2,
	ObjTexture_Equals_m90BDF0DF24E9616BD47124C38FA3BB0FD07F0753,
	ObjTexture_GetHashCode_m28ED4AB25AC0BA5EFDCED02395DF54E45E166BB3,
	ObjTexture__ctor_mE85DA8AF5F68128CA229CE5F445E66B3D11B6B0C,
	ObjReader_GetExtensions_m6969794E7DD0E5B74986C565A8C0F7DD1B83E17B,
	ObjReader_get_Name_m09F1C43CDD9D7C282640DE9BCFCBA1E2A5C5EFA5,
	ObjReader_get_LoadingStepEnumType_mC332CD5DC53EE03BBFFB461F1A1329DDF911770A,
	ObjReader_ReadStream_m528CFDFC027D69D5582FEE6386305269DD690E60,
	ObjReader_CreateRootModel_m74202A90181419D918275B7207BCD58940A236E3,
	ObjReader__ctor_mA11F4582A777AF963F4449EAC9FA6872AD461620,
};
static const int32_t s_InvokerIndices[146] = 
{
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9442,
	9442,
	9289,
	7597,
	9161,
	7469,
	9161,
	5478,
	2414,
	6739,
	2967,
	3506,
	6868,
	6319,
	6739,
	7015,
	7024,
	5830,
	6739,
	6873,
	6353,
	6745,
	7016,
	7025,
	5831,
	6745,
	5478,
	5478,
	9247,
	9247,
	7557,
	9161,
	9161,
	7469,
	9161,
	7469,
	9442,
	9289,
	7597,
	9161,
	7469,
	9432,
	7726,
	9311,
	7619,
	9432,
	7726,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9161,
	7469,
	9289,
	7597,
	9289,
	7597,
	9289,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9442,
	9442,
	7469,
	9442,
	7597,
	12714,
	3462,
	1461,
	2419,
	7597,
	3454,
	9442,
	14502,
	2136,
	665,
	5478,
	6319,
	4359,
	9289,
	5304,
	5304,
	9442,
	653,
	7597,
	7469,
	9289,
	7597,
	9161,
	7469,
	6739,
	9247,
	6868,
	7597,
	9289,
	7597,
	9289,
	7597,
	9289,
	7597,
	9247,
	7557,
	9247,
	7557,
	9430,
	7724,
	9430,
	7724,
	9247,
	7557,
	9289,
	7597,
	9161,
	9161,
	7469,
	9247,
	7557,
	5478,
	5478,
	9247,
	9442,
	14458,
	9289,
	9289,
	1462,
	9289,
	9442,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_TriLibCore_Obj_CodeGenModule;
const Il2CppCodeGenModule g_TriLibCore_Obj_CodeGenModule = 
{
	"TriLibCore.Obj.dll",
	146,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
