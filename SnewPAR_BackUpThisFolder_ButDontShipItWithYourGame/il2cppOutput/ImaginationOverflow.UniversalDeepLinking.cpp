﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_tB45A861D090B15129521119AE48ED3813820A974;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t238D0D2427C6B841A01F522A41540165A2C4AE76;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t1AF33AD0B7330843448956EC4277517081658AE7;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// System.AsyncCallback
struct AsyncCallback_t7FEF460CBDCFB9C5FA2EF776984778B9A4145F4C;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager
struct DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider
struct DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923;
// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider
struct EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D;
// System.Exception
struct Exception_t;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.IAsyncResult
struct IAsyncResult_t7B9B5A0ECB35DCEC31B8A8122C37D687369253B5;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// ImaginationOverflow.UniversalDeepLinking.ILinkProvider
struct ILinkProvider_tF3B30886969F4D27850BC6D352C4E6AC1BB3CE7C;
// ImaginationOverflow.UniversalDeepLinking.LinkActivation
struct LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9;
// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler
struct LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B;
// ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory
struct LinkProviderFactory_t4245E405A0E51908A712F87DD09C2DA856CF3C9E;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.String
struct String_t;
// System.Type
struct Type_t;
// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript
struct UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3;
// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser
struct UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061;
// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4
struct U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t73FD060C436E3C4264A734C8F8DCC01DFF6046B8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ILinkProvider_tF3B30886969F4D27850BC6D352C4E6AC1BB3CE7C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkProviderFactory_t4245E405A0E51908A712F87DD09C2DA856CF3C9E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1802072B6C85CB9E034162DE2A5ECF0CFC99AB85;
IL2CPP_EXTERN_C String_t* _stringLiteral1FA13CA565DC9B5105D70A528D26A6FC7A57049E;
IL2CPP_EXTERN_C String_t* _stringLiteral4697D4C3588C40B4B90B49284BEEC64F643F83E0;
IL2CPP_EXTERN_C String_t* _stringLiteral51D9E2BE3865F31E8579BB2E215D2CBA557E7777;
IL2CPP_EXTERN_C String_t* _stringLiteral5A1FA7C21B5462E60F25F1F1448FB5B3CB05FCFD;
IL2CPP_EXTERN_C String_t* _stringLiteral738F291E53E97C08DAE378C71EF70A60E31AE900;
IL2CPP_EXTERN_C String_t* _stringLiteral86F3118A685A936A755CDCDC1C8266CA46F98E01;
IL2CPP_EXTERN_C String_t* _stringLiteral8D004CCFB2C7F7062B882865483FF7F4DC36E04E;
IL2CPP_EXTERN_C String_t* _stringLiteralED267C8DCB7043D2BDE06822955C482B2637216C;
IL2CPP_EXTERN_C const RuntimeMethod* DeepLinkManager_RegisterIfNecessary_m81D2D904FDEC2F588568492940D8B2B4B2E1588B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeepLinkManager__currProvider_LinkReceived_m2C6757D54FEDF48D852F31672EB689D00C9B6638_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mC78C20D5901C87AAC38F37C906FAB6946BDE5F13_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Clear_m96A01966628B0B9F973D4E29050CC769B116A586_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m17345EA05D3F26087F953F6793B2401AA6EE7B0F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m768E076F1E804CE4959F4E71D3E6A9ADE2F55052_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mB13DFB3E7499031847CF544977D4EFB1AC0157AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Keys_m0014C8E91B9B4377ACFBD26A9175A7E5C016D9E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m6C974325ADBDD3ECEBAA290D7940AC282C56386A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mE8FB9EBD177219F5AC0BF48642FB47D3E186C283_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m4620EF2C1DF7D94D5A511226C42A3A42040B1C9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_m8274147DE3B239BC1ADFA2A6DA18DCF47B744F6D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_mD7EA12266020CF9833672DD2B1D62687BEBB96D8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyCollection_GetEnumerator_m6B09BC0C54723DE1DB3E62395E41B76F419BAC22_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_IEnumerator_Reset_mFCAC32BCF1F334879A7CAECAF6D4E7848FF1AEA8_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t7494AA68B656E73A0CE87A3B5FD014BC13CBF974 
{
};

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t1AF33AD0B7330843448956EC4277517081658AE7* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t238D0D2427C6B841A01F522A41540165A2C4AE76* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection::_dictionary
	Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ____dictionary_0;
};

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager
struct DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F  : public RuntimeObject
{
	// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::<IsSteamBuild>k__BackingField
	bool ___U3CIsSteamBuildU3Ek__BackingField_1;
	// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_activated
	LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* ____activated_2;
	// ImaginationOverflow.UniversalDeepLinking.ILinkProvider ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_currProvider
	RuntimeObject* ____currProvider_3;
	// UnityEngine.GameObject ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_go
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ____go_4;
	// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_storedActivation
	String_t* ____storedActivation_5;
};

// ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider
struct DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923  : public RuntimeObject
{
	// System.Action`1<System.String> ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider::LinkReceived
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___LinkReceived_0;
};

// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider
struct EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D  : public RuntimeObject
{
	// System.Action`1<System.String> ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::LinkReceived
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___LinkReceived_1;
};

// ImaginationOverflow.UniversalDeepLinking.LinkActivation
struct LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9  : public RuntimeObject
{
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::<Uri>k__BackingField
	String_t* ___U3CUriU3Ek__BackingField_0;
	// System.String ImaginationOverflow.UniversalDeepLinking.LinkActivation::<RawQueryString>k__BackingField
	String_t* ___U3CRawQueryStringU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.LinkActivation::<QueryString>k__BackingField
	Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___U3CQueryStringU3Ek__BackingField_2;
};

// ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory
struct LinkProviderFactory_t4245E405A0E51908A712F87DD09C2DA856CF3C9E  : public RuntimeObject
{
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4
struct U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D  : public RuntimeObject
{
	// System.Int32 ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::<>4__this
	UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* ___U3CU3E4__this_2;
};

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t65CC956745B1180C04CE6C6910FB27C5F32BB9FF 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::_dictionary
	Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* ____dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::_version
	int32_t ____version_2;
	// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::_currentKey
	RuntimeObject* ____currentKey_3;
};

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>
struct Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::_dictionary
	Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ____dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::_version
	int32_t ____version_2;
	// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator::_currentKey
	String_t* ____currentKey_3;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	float ___m_Seconds_0;
};

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser
struct UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061  : public Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83
{
	// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_14;
	// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::<Query>k__BackingField
	String_t* ___U3CQueryU3Ek__BackingField_15;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87  : public MulticastDelegate_t
{
};

// System.Action`1<System.String>
struct Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler
struct LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B  : public MulticastDelegate_t
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript
struct UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::_onJob
	bool ____onJob_4;
};

// <Module>

// <Module>

// System.Collections.Generic.Dictionary`2<System.String,System.String>

// System.Collections.Generic.Dictionary`2<System.String,System.String>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager
struct DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_StaticFields
{
	// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::<Instance>k__BackingField
	DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* ___U3CInstanceU3Ek__BackingField_0;
};

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager

// ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider

// ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider

// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider
struct EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_StaticFields
{
	// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::_instance
	EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* ____instance_0;
};

// ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider

// ImaginationOverflow.UniversalDeepLinking.LinkActivation

// ImaginationOverflow.UniversalDeepLinking.LinkActivation

// ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory

// ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory

// System.Reflection.MemberInfo

// System.Reflection.MemberInfo

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.ValueType

// System.ValueType

// UnityEngine.YieldInstruction

// UnityEngine.YieldInstruction

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// System.Int32

// System.Int32

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// System.Single

// System.Single

// System.Void

// System.Void

// UnityEngine.WaitForSeconds

// UnityEngine.WaitForSeconds

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser

// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser

// UnityEngine.Coroutine

// UnityEngine.Coroutine

// System.Delegate

// System.Delegate

// System.Exception
struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};

// System.Exception

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// System.RuntimeTypeHandle

// System.RuntimeTypeHandle

// UnityEngine.Component

// UnityEngine.Component

// UnityEngine.GameObject

// UnityEngine.GameObject

// System.MulticastDelegate

// System.MulticastDelegate

// System.SystemException

// System.SystemException

// System.Type
struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Type

// System.Action`1<System.Object>

// System.Action`1<System.Object>

// System.Action`1<System.String>

// System.Action`1<System.String>

// UnityEngine.Behaviour

// UnityEngine.Behaviour

// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler

// ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler

// System.NotSupportedException

// System.NotSupportedException

// UnityEngine.MonoBehaviour

// UnityEngine.MonoBehaviour

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript

// ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB  : public RuntimeArray
{
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Clear_mCFB5EA7351D5860D2B91592B91A84CA265A41433_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_key, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Keys()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyCollection_tB45A861D090B15129521119AE48ED3813820A974* Dictionary_2_get_Keys_m72D290F90654BFD683FA7AA7C63D9F4F692218B6_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t65CC956745B1180C04CE6C6910FB27C5F32BB9FF KeyCollection_GetEnumerator_m7E77FBA7DE2D3876EC02F396712C4AA5B1D535A5_gshared (KeyCollection_tB45A861D090B15129521119AE48ED3813820A974* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mEB2A61F3F3F420C1B1DFD5C74EEFD4BD7761BCF7_gshared (Enumerator_t65CC956745B1180C04CE6C6910FB27C5F32BB9FF* __this, const RuntimeMethod* method) ;
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_mD6472FA27D28B9AE64A0FEF796C72ABBC2420EBF_gshared_inline (Enumerator_t65CC956745B1180C04CE6C6910FB27C5F32BB9FF* __this, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_key, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mB5E00F11BCA3AC963F9BA72659FA76B107863F3C_gshared (Enumerator_t65CC956745B1180C04CE6C6910FB27C5F32BB9FF* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) ;

// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__ctor_m21DCCB067662DD32B9ACA2E004B7623C764D5F1B (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::set_Instance(ImaginationOverflow.UniversalDeepLinking.DeepLinkManager)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeepLinkManager_set_Instance_mDDB14018A99B01EC19C6242FC446C42C0151D6F4_inline (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* ___0_value, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::add__activated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_add__activated_m174E1DE6A99202FA897A8C8008B413A0B4F85512 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* ___0_value, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::RegisterIfNecessary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_RegisterIfNecessary_m81D2D904FDEC2F588568492940D8B2B4B2E1588B (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::remove__activated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_remove__activated_mAD6E506F02FE83D906F088412130D87906D88E42 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* ___0_value, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00 (Delegate_t* ___0_a, Delegate_t* ___1_b, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3 (Delegate_t* ___0_source, Delegate_t* ___1_value, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_currProvider_LinkReceived(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__currProvider_LinkReceived_m2C6757D54FEDF48D852F31672EB689D00C9B6638 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_s, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478 (String_t* ___0_value, const RuntimeMethod* method) ;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138 (const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider__ctor_mFCB437434492667E1AA4DBD31326D8409F2FE7F2 (EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* __this, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkProviderFactory__ctor_m24F28D8A98298F29F9203C929D619259BC19DAEA (LinkProviderFactory_t4245E405A0E51908A712F87DD09C2DA856CF3C9E* __this, const RuntimeMethod* method) ;
// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::get_IsSteamBuild()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DeepLinkManager_get_IsSteamBuild_mF4EEEF3AE88E4D5C4E8245717BFD45D6DA89BC17_inline (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) ;
// ImaginationOverflow.UniversalDeepLinking.ILinkProvider ImaginationOverflow.UniversalDeepLinking.LinkProviderFactory::GetProvider(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LinkProviderFactory_GetProvider_m9BC8331A4AE58F5C2B86B075B629E958C09BEA37 (LinkProviderFactory_t4245E405A0E51908A712F87DD09C2DA856CF3C9E* __this, bool ___0_isSteamBuild, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::CreatePauseGameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_CreatePauseGameObject_m60DBCFDD2031FCB4AE0BF6D5ED3C5E048B9232EB (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) ;
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F (Exception_t* __this, String_t* ___0_message, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m9DC2953C55C4D7D4B7BEFE03D84DA1F9362D652C (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared)(__this, ___0_object, ___1_method, method);
}
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9E3155FB84015C823606188F53B47CB44C444991 (String_t* ___0_str0, String_t* ___1_str1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2 (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_x, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___1_y, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript>()
inline UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_mD7EA12266020CF9833672DD2B1D62687BEBB96D8 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_obj, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_m7D0340DE160786E6EFA8DABD39EC3B694DA30AAD (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::AddComponent<ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript>()
inline UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_m8274147DE3B239BC1ADFA2A6DA18DCF47B744F6D (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared)(__this, method);
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::OnActivated(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeepLinkManager_OnActivated_mEE0A61F2FA50BD5AC5C41177C0BDC99E9B4CB2E9 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_s, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::StoreActivation(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeepLinkManager_StoreActivation_mCE4E27C176157602A9EB18C119B85AD76B36674D_inline (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_s, const RuntimeMethod* method) ;
// ImaginationOverflow.UniversalDeepLinking.LinkActivation ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::CreateLinkActivation(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* DeepLinkManager_CreateLinkActivation_m79A86840643C2DD9B3EDAF5C8DC4DBE550EACDCF (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_s, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler::Invoke(ImaginationOverflow.UniversalDeepLinking.LinkActivation)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_inline (LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogException_mAB3F4DC7297ED8FBB49DAA718B70E59A6B0171B0 (Exception_t* ___0_exception, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Debug::get_isDebugBuild()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Debug_get_isDebugBuild_m9277C4A9591F7E1D8B76340B4CAE5EA33D63AF01 (const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
inline void Dictionary_2__ctor_m768E076F1E804CE4959F4E71D3E6A9ADE2F55052 (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlEncodingParser__ctor_m9357EADB01DF548B86A75B491A2A8A669867483D (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_queryStringOrUrl, const RuntimeMethod* method) ;
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::get_Query()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Query_m68878E46DA1EB0F469FDC62B500C88105F6FBE0D_inline (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.LinkActivation::.ctor(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkActivation__ctor_m758C8A28F4C84308C360404913D6FE6A6D849C89 (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* __this, String_t* ___0_uri, String_t* ___1_rawQueryString, Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___2_queryStringParams, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::set_Url(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Url_m0C1F31295AC715B962EAEBD390D7A805076743AF_inline (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* UrlEncodingParser_Parse_m7C2982DBB8359056C28B8DE6DF593C9EC6DD3952 (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_query, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(TKey,TValue)
inline void Dictionary_2_Add_mC78C20D5901C87AAC38F37C906FAB6946BDE5F13 (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* __this, String_t* ___0_key, String_t* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83*, String_t*, String_t*, const RuntimeMethod*))Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Boolean System.Uri::IsWellFormedUriString(System.String,System.UriKind)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Uri_IsWellFormedUriString_m35D88312036B391850B73BD0F09779D9D47ECCE8 (String_t* ___0_uriString, int32_t ___1_uriKind, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Clear()
inline void Dictionary_2_Clear_m96A01966628B0B9F973D4E29050CC769B116A586 (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83*, const RuntimeMethod*))Dictionary_2_Clear_mCFB5EA7351D5860D2B91592B91A84CA265A41433_gshared)(__this, method);
}
// System.Int32 System.String::IndexOf(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_IndexOf_mE21E78F35EF4A7768E385A72814C88D22B689966 (String_t* __this, Il2CppChar ___0_value, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.String System.String::Substring(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_m6BA4A3FA3800FE92662D0847CC8E1EEF940DF472 (String_t* __this, int32_t ___0_startIndex, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::set_Query(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Query_m39ED995D87E2479867B08466DD154D8A3D727699_inline (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* String_Split_m101D35FEC86371D2BB4E3480F6F896880093B2E9 (String_t* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___0_separator, const RuntimeMethod* method) ;
// System.String System.String::Substring(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE (String_t* __this, int32_t ___0_startIndex, int32_t ___1_length, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m81A40DADB89BCDB78DA8D63D6B723B0023A09A1A (RuntimeObject* ___0_arg0, RuntimeObject* ___1_arg1, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(TKey)
inline bool Dictionary_2_ContainsKey_m17345EA05D3F26087F953F6793B2401AA6EE7B0F (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* __this, String_t* ___0_key, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83*, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m703047C213F7AB55C9DC346596287773A1F670CD_gshared)(__this, ___0_key, method);
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Keys()
inline KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342* Dictionary_2_get_Keys_m0014C8E91B9B4377ACFBD26A9175A7E5C016D9E9 (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* __this, const RuntimeMethod* method)
{
	return ((  KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342* (*) (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83*, const RuntimeMethod*))Dictionary_2_get_Keys_m72D290F90654BFD683FA7AA7C63D9F4F692218B6_gshared)(__this, method);
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>::GetEnumerator()
inline Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867 KeyCollection_GetEnumerator_m6B09BC0C54723DE1DB3E62395E41B76F419BAC22 (KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867 (*) (KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342*, const RuntimeMethod*))KeyCollection_GetEnumerator_m7E77FBA7DE2D3876EC02F396712C4AA5B1D535A5_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>::Dispose()
inline void Enumerator_Dispose_m6C974325ADBDD3ECEBAA290D7940AC282C56386A (Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867*, const RuntimeMethod*))Enumerator_Dispose_mEB2A61F3F3F420C1B1DFD5C74EEFD4BD7761BCF7_gshared)(__this, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>::get_Current()
inline String_t* Enumerator_get_Current_m4620EF2C1DF7D94D5A511226C42A3A42040B1C9E_inline (Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867* __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867*, const RuntimeMethod*))Enumerator_get_Current_mD6472FA27D28B9AE64A0FEF796C72ABBC2420EBF_gshared_inline)(__this, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(TKey)
inline String_t* Dictionary_2_get_Item_mB13DFB3E7499031847CF544977D4EFB1AC0157AB (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* __this, String_t* ___0_key, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___0_key, method);
}
// System.String System.Uri::EscapeUriString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Uri_EscapeUriString_m1F1E67F8150470F51174B1C3B93E6F5B9C8C1A38 (String_t* ___0_stringToEscape, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___0_values, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,System.String>::MoveNext()
inline bool Enumerator_MoveNext_mE8FB9EBD177219F5AC0BF48642FB47D3E186C283 (Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867*, const RuntimeMethod*))Enumerator_MoveNext_mB5E00F11BCA3AC963F9BA72659FA76B107863F3C_gshared)(__this, method);
}
// System.String System.String::Trim(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Trim_m81BD35659E6F89DDD56816975E6E05390D023FE5 (String_t* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___0_trimChars, const RuntimeMethod* method) ;
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::get_Url()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Url_mE0ACF7A80D9BB66529F8FDEC1538DEAAFF25C66A_inline (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, const RuntimeMethod* method) ;
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3 (String_t* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B (String_t* ___0_str0, String_t* ___1_str1, String_t* ___2_str2, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m4B70C3AEF886C176543D1295507B6455C9DCAEA7 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_target, const RuntimeMethod* method) ;
// System.Collections.IEnumerator ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::CallDeepLinkManagerAfterDelay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UniversalDeeplinkingRuntimeScript_CallDeepLinkManagerAfterDelay_m8FF09F77A4C5D6C163874430FE8C358C6B790875 (UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* __this, const RuntimeMethod* method) ;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, RuntimeObject* ___0_routine, const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCallDeepLinkManagerAfterDelayU3Ed__4__ctor_m89A227A443A7DBF09A566977E18E9099C961FB25 (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D* __this, int32_t ___0_U3CU3E1__state, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* __this, float ___0_seconds, const RuntimeMethod* method) ;
// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* DeepLinkManager_get_Instance_mEA1C431B7FB9CF97E4776FE073A655E7F3804F4C_inline (const RuntimeMethod* method) ;
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::GameCameFromPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_GameCameFromPause_m3ED4809D0F55061092E005BF387877AB0B29F7C7 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m94F967AB31244EACE68C3BE1DD85B69ED3334C0E (RuntimeObject* ___0_message, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___1_context, const RuntimeMethod* method) ;
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.String>::Invoke(T)
inline void Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* __this, String_t* ___0_obj, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*, String_t*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___0_obj, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// ImaginationOverflow.UniversalDeepLinking.DeepLinkManager ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* DeepLinkManager_get_Instance_mEA1C431B7FB9CF97E4776FE073A655E7F3804F4C (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* L_0 = ((DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var))->___U3CInstanceU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::set_Instance(ImaginationOverflow.UniversalDeepLinking.DeepLinkManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_set_Instance_mDDB14018A99B01EC19C6242FC446C42C0151D6F4 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* L_0 = ___0_value;
		il2cpp_codegen_runtime_class_init_inline(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		((DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var))->___U3CInstanceU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var))->___U3CInstanceU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::get_IsSteamBuild()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeepLinkManager_get_IsSteamBuild_mF4EEEF3AE88E4D5C4E8245717BFD45D6DA89BC17 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsSteamBuildU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::set_IsSteamBuild(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_set_IsSteamBuild_mB8588CBCF0931009805FC92973DD2469D0386A70 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CIsSteamBuildU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__cctor_mDBD828E64BD7E6430E868384C06151666C1D1592 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* L_0 = (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F*)il2cpp_codegen_object_new(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		DeepLinkManager__ctor_m21DCCB067662DD32B9ACA2E004B7623C764D5F1B(L_0, NULL);
		DeepLinkManager_set_Instance_mDDB14018A99B01EC19C6242FC446C42C0151D6F4_inline(L_0, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__ctor_m21DCCB067662DD32B9ACA2E004B7623C764D5F1B (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::add_LinkActivated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_add_LinkActivated_mD63C3864D3F9994A858B5B642C602AB1C064D394 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* ___0_value, const RuntimeMethod* method) 
{
	{
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_0 = ___0_value;
		DeepLinkManager_add__activated_m174E1DE6A99202FA897A8C8008B413A0B4F85512(__this, L_0, NULL);
		DeepLinkManager_RegisterIfNecessary_m81D2D904FDEC2F588568492940D8B2B4B2E1588B(__this, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::remove_LinkActivated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_remove_LinkActivated_m8AF1335C56E03FAABA573BC416BFFB4D5FDD3A68 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* ___0_value, const RuntimeMethod* method) 
{
	{
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_0 = ___0_value;
		DeepLinkManager_remove__activated_mAD6E506F02FE83D906F088412130D87906D88E42(__this, L_0, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::add__activated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_add__activated_m174E1DE6A99202FA897A8C8008B413A0B4F85512 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* V_0 = NULL;
	LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* V_1 = NULL;
	LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* V_2 = NULL;
	{
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_0 = __this->____activated_2;
		V_0 = L_0;
	}

IL_0007:
	{
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_1 = V_0;
		V_1 = L_1;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_2 = V_1;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B*)CastclassSealed((RuntimeObject*)L_4, LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B_il2cpp_TypeInfo_var));
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B** L_5 = (&__this->____activated_2);
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_6 = V_2;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_7 = V_1;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_8;
		L_8 = InterlockedCompareExchangeImpl<LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B*>(L_5, L_6, L_7);
		V_0 = L_8;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_9 = V_0;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_10 = V_1;
		if ((!(((RuntimeObject*)(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B*)L_9) == ((RuntimeObject*)(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::remove__activated(ImaginationOverflow.UniversalDeepLinking.LinkActivationHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_remove__activated_mAD6E506F02FE83D906F088412130D87906D88E42 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* V_0 = NULL;
	LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* V_1 = NULL;
	LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* V_2 = NULL;
	{
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_0 = __this->____activated_2;
		V_0 = L_0;
	}

IL_0007:
	{
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_1 = V_0;
		V_1 = L_1;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_2 = V_1;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B*)CastclassSealed((RuntimeObject*)L_4, LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B_il2cpp_TypeInfo_var));
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B** L_5 = (&__this->____activated_2);
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_6 = V_2;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_7 = V_1;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_8;
		L_8 = InterlockedCompareExchangeImpl<LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B*>(L_5, L_6, L_7);
		V_0 = L_8;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_9 = V_0;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_10 = V_1;
		if ((!(((RuntimeObject*)(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B*)L_9) == ((RuntimeObject*)(LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::ManuallyTriggerDeepLink(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_ManuallyTriggerDeepLink_mF56384D55946AE328B77ABAFA386FFAEE17D3EF2 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_args, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_args;
		DeepLinkManager__currProvider_LinkReceived_m2C6757D54FEDF48D852F31672EB689D00C9B6638(__this, L_0, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::RegisterIfNecessary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_RegisterIfNecessary_m81D2D904FDEC2F588568492940D8B2B4B2E1588B (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager__currProvider_LinkReceived_m2C6757D54FEDF48D852F31672EB689D00C9B6638_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ILinkProvider_tF3B30886969F4D27850BC6D352C4E6AC1BB3CE7C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkProviderFactory_t4245E405A0E51908A712F87DD09C2DA856CF3C9E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
		RuntimeObject* L_0 = __this->____currProvider_3;
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_1 = __this->____storedActivation_5;
		bool L_2;
		L_2 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_1, NULL);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_3 = __this->____storedActivation_5;
		V_0 = L_3;
		__this->____storedActivation_5 = (String_t*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____storedActivation_5), (void*)(String_t*)NULL);
		String_t* L_4 = V_0;
		DeepLinkManager__currProvider_LinkReceived_m2C6757D54FEDF48D852F31672EB689D00C9B6638(__this, L_4, NULL);
	}

IL_002a:
	{
		return;
	}

IL_002b:
	{
		int32_t L_5;
		L_5 = Application_get_platform_m59EF7D6155D18891B24767F83F388160B1FF2138(NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)7)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_8 = V_1;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_004a;
		}
	}

IL_003d:
	{
		EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* L_9 = (EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D*)il2cpp_codegen_object_new(EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_il2cpp_TypeInfo_var);
		NullCheck(L_9);
		EditorLinkProvider__ctor_mFCB437434492667E1AA4DBD31326D8409F2FE7F2(L_9, NULL);
		__this->____currProvider_3 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____currProvider_3), (void*)L_9);
		goto IL_0060;
	}

IL_004a:
	{
		LinkProviderFactory_t4245E405A0E51908A712F87DD09C2DA856CF3C9E* L_10 = (LinkProviderFactory_t4245E405A0E51908A712F87DD09C2DA856CF3C9E*)il2cpp_codegen_object_new(LinkProviderFactory_t4245E405A0E51908A712F87DD09C2DA856CF3C9E_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		LinkProviderFactory__ctor_m24F28D8A98298F29F9203C929D619259BC19DAEA(L_10, NULL);
		bool L_11;
		L_11 = DeepLinkManager_get_IsSteamBuild_mF4EEEF3AE88E4D5C4E8245717BFD45D6DA89BC17_inline(__this, NULL);
		NullCheck(L_10);
		RuntimeObject* L_12;
		L_12 = LinkProviderFactory_GetProvider_m9BC8331A4AE58F5C2B86B075B629E958C09BEA37(L_10, L_11, NULL);
		__this->____currProvider_3 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____currProvider_3), (void*)L_12);
	}

IL_0060:
	{
		DeepLinkManager_CreatePauseGameObject_m60DBCFDD2031FCB4AE0BF6D5ED3C5E048B9232EB(__this, NULL);
	}
	try
	{// begin try (depth: 1)
		{
			RuntimeObject* L_13 = __this->____currProvider_3;
			NullCheck(L_13);
			bool L_14;
			L_14 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean ImaginationOverflow.UniversalDeepLinking.ILinkProvider::Initialize() */, ILinkProvider_tF3B30886969F4D27850BC6D352C4E6AC1BB3CE7C_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_007e_1;
			}
		}
		{
			Exception_t* L_15 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
			NullCheck(L_15);
			Exception__ctor_m9B2BD92CD68916245A75109105D9071C9D430E7F(L_15, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral1802072B6C85CB9E034162DE2A5ECF0CFC99AB85)), NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_15, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeepLinkManager_RegisterIfNecessary_m81D2D904FDEC2F588568492940D8B2B4B2E1588B_RuntimeMethod_var)));
		}

IL_007e_1:
		{
			RuntimeObject* L_16 = __this->____currProvider_3;
			Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_17 = (Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)il2cpp_codegen_object_new(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
			NullCheck(L_17);
			Action_1__ctor_m9DC2953C55C4D7D4B7BEFE03D84DA1F9362D652C(L_17, __this, (intptr_t)((void*)DeepLinkManager__currProvider_LinkReceived_m2C6757D54FEDF48D852F31672EB689D00C9B6638_RuntimeMethod_var), NULL);
			NullCheck(L_16);
			InterfaceActionInvoker1< Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* >::Invoke(1 /* System.Void ImaginationOverflow.UniversalDeepLinking.ILinkProvider::add_LinkReceived(System.Action`1<System.String>) */, ILinkProvider_tF3B30886969F4D27850BC6D352C4E6AC1BB3CE7C_il2cpp_TypeInfo_var, L_16, L_17);
			goto IL_00c4;
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0097;
		}
		throw e;
	}

CATCH_0097:
	{// begin catch(System.Exception)
		RuntimeObject* L_18 = __this->____currProvider_3;
		NullCheck(L_18);
		Type_t* L_19;
		L_19 = Object_GetType_mE10A8FC1E57F3DF29972CCBC026C2DC3942263B3(L_18, NULL);
		NullCheck(L_19);
		String_t* L_20;
		L_20 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		String_t* L_21;
		L_21 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralED267C8DCB7043D2BDE06822955C482B2637216C)), L_20, NULL);
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var)));
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(L_21, NULL);
		__this->____currProvider_3 = (RuntimeObject*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____currProvider_3), (void*)(RuntimeObject*)NULL);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*)), NULL);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_00c4;
	}// end catch (depth: 1)

IL_00c4:
	{
		DeepLinkManager_CreatePauseGameObject_m60DBCFDD2031FCB4AE0BF6D5ED3C5E048B9232EB(__this, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::CreatePauseGameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_CreatePauseGameObject_m60DBCFDD2031FCB4AE0BF6D5ED3C5E048B9232EB (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_m8274147DE3B239BC1ADFA2A6DA18DCF47B744F6D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_mD7EA12266020CF9833672DD2B1D62687BEBB96D8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral51D9E2BE3865F31E8579BB2E215D2CBA557E7777);
		s_Il2CppMethodInitialized = true;
	}
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->____go_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->____go_4;
		NullCheck(L_2);
		UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* L_3;
		L_3 = GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_mD7EA12266020CF9833672DD2B1D62687BEBB96D8(L_2, GameObject_GetComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_mD7EA12266020CF9833672DD2B1D62687BEBB96D8_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->____go_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB(L_5, NULL);
		__this->____go_4 = (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____go_4), (void*)(GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)NULL);
	}

IL_0034:
	{
	}
	try
	{// begin try (depth: 1)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)il2cpp_codegen_object_new(GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		GameObject__ctor_m7D0340DE160786E6EFA8DABD39EC3B694DA30AAD(L_6, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = L_6;
		NullCheck(L_7);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_7, _stringLiteral51D9E2BE3865F31E8579BB2E215D2CBA557E7777, NULL);
		__this->____go_4 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____go_4), (void*)L_7);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8 = __this->____go_4;
		NullCheck(L_8);
		UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* L_9;
		L_9 = GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_m8274147DE3B239BC1ADFA2A6DA18DCF47B744F6D(L_8, GameObject_AddComponent_TisUniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6_m8274147DE3B239BC1ADFA2A6DA18DCF47B744F6D_RuntimeMethod_var);
		goto IL_0060;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0059;
		}
		throw e;
	}

CATCH_0059:
	{// begin catch(System.Exception)
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var)));
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*)), NULL);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_0060;
	}// end catch (depth: 1)

IL_0060:
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::_currProvider_LinkReceived(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager__currProvider_LinkReceived_m2C6757D54FEDF48D852F31672EB689D00C9B6638 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_s, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_s;
		bool L_1;
		L_1 = DeepLinkManager_OnActivated_mEE0A61F2FA50BD5AC5C41177C0BDC99E9B4CB2E9(__this, L_0, NULL);
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		String_t* L_2 = ___0_s;
		DeepLinkManager_StoreActivation_mCE4E27C176157602A9EB18C119B85AD76B36674D_inline(__this, L_2, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::StoreActivation(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_StoreActivation_mCE4E27C176157602A9EB18C119B85AD76B36674D (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_s, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_s;
		__this->____storedActivation_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____storedActivation_5), (void*)L_0);
		return;
	}
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::OnActivated(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeepLinkManager_OnActivated_mEE0A61F2FA50BD5AC5C41177C0BDC99E9B4CB2E9 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_s, const RuntimeMethod* method) 
{
	LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* V_0 = NULL;
	LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* V_1 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
		String_t* L_0 = ___0_s;
		LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* L_1;
		L_1 = DeepLinkManager_CreateLinkActivation_m79A86840643C2DD9B3EDAF5C8DC4DBE550EACDCF(__this, L_0, NULL);
		V_0 = L_1;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_2 = __this->____activated_2;
		V_1 = L_2;
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_3 = V_1;
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	try
	{// begin try (depth: 1)
		LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* L_4 = V_1;
		LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* L_5 = V_0;
		NullCheck(L_4);
		LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_inline(L_4, L_5, NULL);
		goto IL_0035;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_001b;
		}
		throw e;
	}

CATCH_001b:
	{// begin catch(System.Exception)
		{
			il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var)));
			Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral5A1FA7C21B5462E60F25F1F1448FB5B3CB05FCFD)), NULL);
			Debug_LogException_mAB3F4DC7297ED8FBB49DAA718B70E59A6B0171B0(((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*)), NULL);
			bool L_6;
			L_6 = Debug_get_isDebugBuild_m9277C4A9591F7E1D8B76340B4CAE5EA33D63AF01(NULL);
			if (!L_6)
			{
				goto IL_0033;
			}
		}
		{
			IL2CPP_RETHROW_MANAGED_EXCEPTION(IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
		}

IL_0033:
		{
			IL2CPP_POP_ACTIVE_EXCEPTION();
			goto IL_0035;
		}
	}// end catch (depth: 1)

IL_0035:
	{
		return (bool)1;
	}

IL_0037:
	{
		return (bool)0;
	}
}
// ImaginationOverflow.UniversalDeepLinking.LinkActivation ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::CreateLinkActivation(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* DeepLinkManager_CreateLinkActivation_m79A86840643C2DD9B3EDAF5C8DC4DBE550EACDCF (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_s, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m768E076F1E804CE4959F4E71D3E6A9ADE2F55052_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* V_1 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->___Empty_6;
		V_0 = L_0;
		Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* L_1 = (Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83*)il2cpp_codegen_object_new(Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		Dictionary_2__ctor_m768E076F1E804CE4959F4E71D3E6A9ADE2F55052(L_1, Dictionary_2__ctor_m768E076F1E804CE4959F4E71D3E6A9ADE2F55052_RuntimeMethod_var);
		V_1 = L_1;
	}
	try
	{// begin try (depth: 1)
		String_t* L_2 = ___0_s;
		UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* L_3 = (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061*)il2cpp_codegen_object_new(UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		UrlEncodingParser__ctor_m9357EADB01DF548B86A75B491A2A8A669867483D(L_3, L_2, NULL);
		UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* L_4 = L_3;
		V_1 = L_4;
		NullCheck(L_4);
		String_t* L_5;
		L_5 = UrlEncodingParser_get_Query_m68878E46DA1EB0F469FDC62B500C88105F6FBE0D_inline(L_4, NULL);
		V_0 = L_5;
		goto IL_001f;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_001c;
		}
		throw e;
	}

CATCH_001c:
	{// begin catch(System.Exception)
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_001f;
	}// end catch (depth: 1)

IL_001f:
	{
		String_t* L_6 = ___0_s;
		String_t* L_7 = V_0;
		Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* L_8 = V_1;
		LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* L_9 = (LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9*)il2cpp_codegen_object_new(LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9_il2cpp_TypeInfo_var);
		NullCheck(L_9);
		LinkActivation__ctor_m758C8A28F4C84308C360404913D6FE6A6D849C89(L_9, L_6, L_7, L_8, NULL);
		return L_9;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager::GameCameFromPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeepLinkManager_GameCameFromPause_m3ED4809D0F55061092E005BF387877AB0B29F7C7 (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ILinkProvider_tF3B30886969F4D27850BC6D352C4E6AC1BB3CE7C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->____currProvider_3;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = __this->____currProvider_3;
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(3 /* System.Void ImaginationOverflow.UniversalDeepLinking.ILinkProvider::PollInfoAfterPause() */, ILinkProvider_tF3B30886969F4D27850BC6D352C4E6AC1BB3CE7C_il2cpp_TypeInfo_var, L_1);
	}

IL_0013:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::get_Url()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Url_mE0ACF7A80D9BB66529F8FDEC1538DEAAFF25C66A (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CUrlU3Ek__BackingField_14;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::set_Url(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Url_m0C1F31295AC715B962EAEBD390D7A805076743AF (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CUrlU3Ek__BackingField_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CUrlU3Ek__BackingField_14), (void*)L_0);
		return;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::get_Query()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Query_m68878E46DA1EB0F469FDC62B500C88105F6FBE0D (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CQueryU3Ek__BackingField_15;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::set_Query(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Query_m39ED995D87E2479867B08466DD154D8A3D727699 (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CQueryU3Ek__BackingField_15 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CQueryU3Ek__BackingField_15), (void*)L_0);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlEncodingParser__ctor_m9357EADB01DF548B86A75B491A2A8A669867483D (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_queryStringOrUrl, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m768E076F1E804CE4959F4E71D3E6A9ADE2F55052_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2__ctor_m768E076F1E804CE4959F4E71D3E6A9ADE2F55052(__this, Dictionary_2__ctor_m768E076F1E804CE4959F4E71D3E6A9ADE2F55052_RuntimeMethod_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->___Empty_6;
		UrlEncodingParser_set_Url_m0C1F31295AC715B962EAEBD390D7A805076743AF_inline(__this, L_0, NULL);
		String_t* L_1 = ___0_queryStringOrUrl;
		bool L_2;
		L_2 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_1, NULL);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___0_queryStringOrUrl;
		Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* L_4;
		L_4 = UrlEncodingParser_Parse_m7C2982DBB8359056C28B8DE6DF593C9EC6DD3952(__this, L_3, NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::SetValues(System.String,System.Collections.Generic.IEnumerable`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlEncodingParser_SetValues_m8295243CB9BE81D5D12B6029AA3343163D64A4A7 (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_key, RuntimeObject* ___1_values, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mC78C20D5901C87AAC38F37C906FAB6946BDE5F13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t73FD060C436E3C4264A734C8F8DCC01DFF6046B8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		RuntimeObject* L_0 = ___1_values;
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0022:
			{// begin finally (depth: 1)
				{
					RuntimeObject* L_2 = V_0;
					if (!L_2)
					{
						goto IL_002b;
					}
				}
				{
					RuntimeObject* L_3 = V_0;
					NullCheck(L_3);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_3);
				}

IL_002b:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0018_1;
			}

IL_0009_1:
			{
				RuntimeObject* L_4 = V_0;
				NullCheck(L_4);
				String_t* L_5;
				L_5 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t73FD060C436E3C4264A734C8F8DCC01DFF6046B8_il2cpp_TypeInfo_var, L_4);
				V_1 = L_5;
				String_t* L_6 = ___0_key;
				String_t* L_7 = V_1;
				Dictionary_2_Add_mC78C20D5901C87AAC38F37C906FAB6946BDE5F13(__this, L_6, L_7, Dictionary_2_Add_mC78C20D5901C87AAC38F37C906FAB6946BDE5F13_RuntimeMethod_var);
			}

IL_0018_1:
			{
				RuntimeObject* L_8 = V_0;
				NullCheck(L_8);
				bool L_9;
				L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_8);
				if (L_9)
				{
					goto IL_0009_1;
				}
			}
			{
				goto IL_002c;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_002c:
	{
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* UrlEncodingParser_Parse_m7C2982DBB8359056C28B8DE6DF593C9EC6DD3952 (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_query, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mC78C20D5901C87AAC38F37C906FAB6946BDE5F13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Clear_m96A01966628B0B9F973D4E29050CC769B116A586_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m17345EA05D3F26087F953F6793B2401AA6EE7B0F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___0_query;
		il2cpp_codegen_runtime_class_init_inline(Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Uri_IsWellFormedUriString_m35D88312036B391850B73BD0F09779D9D47ECCE8(L_0, 1, NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		String_t* L_2 = ___0_query;
		UrlEncodingParser_set_Url_m0C1F31295AC715B962EAEBD390D7A805076743AF_inline(__this, L_2, NULL);
	}

IL_0010:
	{
		String_t* L_3 = ___0_query;
		bool L_4;
		L_4 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_3, NULL);
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_Clear_m96A01966628B0B9F973D4E29050CC769B116A586(__this, Dictionary_2_Clear_m96A01966628B0B9F973D4E29050CC769B116A586_RuntimeMethod_var);
		goto IL_00c9;
	}

IL_0023:
	{
		String_t* L_5 = ___0_query;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = String_IndexOf_mE21E78F35EF4A7768E385A72814C88D22B689966(L_5, ((int32_t)63), NULL);
		V_0 = L_6;
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) <= ((int32_t)(-1))))
		{
			goto IL_0046;
		}
	}
	{
		String_t* L_8 = ___0_query;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_8, NULL);
		int32_t L_10 = V_0;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)il2cpp_codegen_add(L_10, 1)))))
		{
			goto IL_0046;
		}
	}
	{
		String_t* L_11 = ___0_query;
		int32_t L_12 = V_0;
		NullCheck(L_11);
		String_t* L_13;
		L_13 = String_Substring_m6BA4A3FA3800FE92662D0847CC8E1EEF940DF472(L_11, ((int32_t)il2cpp_codegen_add(L_12, 1)), NULL);
		___0_query = L_13;
	}

IL_0046:
	{
		String_t* L_14 = ___0_query;
		UrlEncodingParser_set_Query_m39ED995D87E2479867B08466DD154D8A3D727699_inline(__this, L_14, NULL);
		String_t* L_15 = ___0_query;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_16 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_17 = L_16;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)38));
		NullCheck(L_15);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18;
		L_18 = String_Split_m101D35FEC86371D2BB4E3480F6F896880093B2E9(L_15, L_17, NULL);
		V_1 = L_18;
		V_2 = 0;
		goto IL_00c3;
	}

IL_0063:
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = V_1;
		int32_t L_20 = V_2;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_3 = L_22;
		String_t* L_23 = V_3;
		NullCheck(L_23);
		int32_t L_24;
		L_24 = String_IndexOf_mE21E78F35EF4A7768E385A72814C88D22B689966(L_23, ((int32_t)61), NULL);
		V_4 = L_24;
		int32_t L_25 = V_4;
		if ((((int32_t)L_25) <= ((int32_t)0)))
		{
			goto IL_00bf;
		}
	}
	{
		String_t* L_26 = V_3;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		String_t* L_28;
		L_28 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_26, 0, L_27, NULL);
		V_5 = L_28;
		String_t* L_29 = V_3;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		String_t* L_31;
		L_31 = String_Substring_m6BA4A3FA3800FE92662D0847CC8E1EEF940DF472(L_29, ((int32_t)il2cpp_codegen_add(L_30, 1)), NULL);
		V_6 = L_31;
		String_t* L_32 = V_5;
		V_7 = L_32;
		V_8 = 2;
		goto IL_00ab;
	}

IL_0096:
	{
		String_t* L_33 = V_7;
		int32_t L_34 = V_8;
		int32_t L_35 = L_34;
		V_8 = ((int32_t)il2cpp_codegen_add(L_35, 1));
		int32_t L_36 = L_35;
		RuntimeObject* L_37 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_36);
		String_t* L_38;
		L_38 = String_Concat_m81A40DADB89BCDB78DA8D63D6B723B0023A09A1A(L_33, L_37, NULL);
		V_5 = L_38;
	}

IL_00ab:
	{
		String_t* L_39 = V_5;
		bool L_40;
		L_40 = Dictionary_2_ContainsKey_m17345EA05D3F26087F953F6793B2401AA6EE7B0F(__this, L_39, Dictionary_2_ContainsKey_m17345EA05D3F26087F953F6793B2401AA6EE7B0F_RuntimeMethod_var);
		if (L_40)
		{
			goto IL_0096;
		}
	}
	{
		String_t* L_41 = V_5;
		String_t* L_42 = V_6;
		Dictionary_2_Add_mC78C20D5901C87AAC38F37C906FAB6946BDE5F13(__this, L_41, L_42, Dictionary_2_Add_mC78C20D5901C87AAC38F37C906FAB6946BDE5F13_RuntimeMethod_var);
	}

IL_00bf:
	{
		int32_t L_43 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_43, 1));
	}

IL_00c3:
	{
		int32_t L_44 = V_2;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_45 = V_1;
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)((int32_t)(((RuntimeArray*)L_45)->max_length)))))
		{
			goto IL_0063;
		}
	}

IL_00c9:
	{
		return __this;
	}
}
// System.String ImaginationOverflow.UniversalDeepLinking.DeepLinkManager/UrlEncodingParser::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_ToString_mA470AB2A468B3FA3661AF9F1EB04DDBC6439E75D (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mB13DFB3E7499031847CF544977D4EFB1AC0157AB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Keys_m0014C8E91B9B4377ACFBD26A9175A7E5C016D9E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m6C974325ADBDD3ECEBAA290D7940AC282C56386A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mE8FB9EBD177219F5AC0BF48642FB47D3E186C283_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m4620EF2C1DF7D94D5A511226C42A3A42040B1C9E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyCollection_GetEnumerator_m6B09BC0C54723DE1DB3E62395E41B76F419BAC22_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1FA13CA565DC9B5105D70A528D26A6FC7A57049E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral738F291E53E97C08DAE378C71EF70A60E31AE900);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8D004CCFB2C7F7062B882865483FF7F4DC36E04E);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867 V_1;
	memset((&V_1), 0, sizeof(V_1));
	String_t* V_2 = NULL;
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->___Empty_6;
		V_0 = L_0;
		KeyCollection_t2EDD317F5771E575ACB63527B5AFB71291040342* L_1;
		L_1 = Dictionary_2_get_Keys_m0014C8E91B9B4377ACFBD26A9175A7E5C016D9E9(__this, Dictionary_2_get_Keys_m0014C8E91B9B4377ACFBD26A9175A7E5C016D9E9_RuntimeMethod_var);
		NullCheck(L_1);
		Enumerator_t84BD4D6D35ABE5554A430614BF2F7588BC152867 L_2;
		L_2 = KeyCollection_GetEnumerator_m6B09BC0C54723DE1DB3E62395E41B76F419BAC22(L_1, KeyCollection_GetEnumerator_m6B09BC0C54723DE1DB3E62395E41B76F419BAC22_RuntimeMethod_var);
		V_1 = L_2;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_005a:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m6C974325ADBDD3ECEBAA290D7940AC282C56386A((&V_1), Enumerator_Dispose_m6C974325ADBDD3ECEBAA290D7940AC282C56386A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_004f_1;
			}

IL_0014_1:
			{
				String_t* L_3;
				L_3 = Enumerator_get_Current_m4620EF2C1DF7D94D5A511226C42A3A42040B1C9E_inline((&V_1), Enumerator_get_Current_m4620EF2C1DF7D94D5A511226C42A3A42040B1C9E_RuntimeMethod_var);
				V_2 = L_3;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = L_4;
				String_t* L_6 = V_0;
				NullCheck(L_5);
				ArrayElementTypeCheck (L_5, L_6);
				(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_6);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_7 = L_5;
				String_t* L_8 = V_2;
				NullCheck(L_7);
				ArrayElementTypeCheck (L_7, L_8);
				(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_9 = L_7;
				NullCheck(L_9);
				ArrayElementTypeCheck (L_9, _stringLiteral1FA13CA565DC9B5105D70A528D26A6FC7A57049E);
				(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1FA13CA565DC9B5105D70A528D26A6FC7A57049E);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_10 = L_9;
				String_t* L_11 = V_2;
				String_t* L_12;
				L_12 = Dictionary_2_get_Item_mB13DFB3E7499031847CF544977D4EFB1AC0157AB(__this, L_11, Dictionary_2_get_Item_mB13DFB3E7499031847CF544977D4EFB1AC0157AB_RuntimeMethod_var);
				il2cpp_codegen_runtime_class_init_inline(Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E_il2cpp_TypeInfo_var);
				String_t* L_13;
				L_13 = Uri_EscapeUriString_m1F1E67F8150470F51174B1C3B93E6F5B9C8C1A38(L_12, NULL);
				NullCheck(L_10);
				ArrayElementTypeCheck (L_10, L_13);
				(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_13);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = L_10;
				NullCheck(L_14);
				ArrayElementTypeCheck (L_14, _stringLiteral8D004CCFB2C7F7062B882865483FF7F4DC36E04E);
				(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral8D004CCFB2C7F7062B882865483FF7F4DC36E04E);
				String_t* L_15;
				L_15 = String_Concat_m647EBF831F54B6DF7D5AFA5FD012CF4EE7571B6A(L_14, NULL);
				V_0 = L_15;
			}

IL_004f_1:
			{
				bool L_16;
				L_16 = Enumerator_MoveNext_mE8FB9EBD177219F5AC0BF48642FB47D3E186C283((&V_1), Enumerator_MoveNext_mE8FB9EBD177219F5AC0BF48642FB47D3E186C283_RuntimeMethod_var);
				if (L_16)
				{
					goto IL_0014_1;
				}
			}
			{
				goto IL_0068;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0068:
	{
		String_t* L_17 = V_0;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_18 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_19 = L_18;
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)38));
		NullCheck(L_17);
		String_t* L_20;
		L_20 = String_Trim_m81BD35659E6F89DDD56816975E6E05390D023FE5(L_17, L_19, NULL);
		V_0 = L_20;
		String_t* L_21;
		L_21 = UrlEncodingParser_get_Url_mE0ACF7A80D9BB66529F8FDEC1538DEAAFF25C66A_inline(__this, NULL);
		bool L_22;
		L_22 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_21, NULL);
		if (L_22)
		{
			goto IL_00cf;
		}
	}
	{
		String_t* L_23;
		L_23 = UrlEncodingParser_get_Url_mE0ACF7A80D9BB66529F8FDEC1538DEAAFF25C66A_inline(__this, NULL);
		NullCheck(L_23);
		bool L_24;
		L_24 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_23, _stringLiteral738F291E53E97C08DAE378C71EF70A60E31AE900, NULL);
		if (!L_24)
		{
			goto IL_00bd;
		}
	}
	{
		String_t* L_25;
		L_25 = UrlEncodingParser_get_Url_mE0ACF7A80D9BB66529F8FDEC1538DEAAFF25C66A_inline(__this, NULL);
		String_t* L_26;
		L_26 = UrlEncodingParser_get_Url_mE0ACF7A80D9BB66529F8FDEC1538DEAAFF25C66A_inline(__this, NULL);
		NullCheck(L_26);
		int32_t L_27;
		L_27 = String_IndexOf_mE21E78F35EF4A7768E385A72814C88D22B689966(L_26, ((int32_t)63), NULL);
		NullCheck(L_25);
		String_t* L_28;
		L_28 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_25, 0, ((int32_t)il2cpp_codegen_add(L_27, 1)), NULL);
		String_t* L_29 = V_0;
		String_t* L_30;
		L_30 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(L_28, L_29, NULL);
		V_0 = L_30;
		goto IL_00cf;
	}

IL_00bd:
	{
		String_t* L_31;
		L_31 = UrlEncodingParser_get_Url_mE0ACF7A80D9BB66529F8FDEC1538DEAAFF25C66A_inline(__this, NULL);
		String_t* L_32 = V_0;
		String_t* L_33;
		L_33 = String_Concat_m8855A6DE10F84DA7F4EC113CADDB59873A25573B(L_31, _stringLiteral738F291E53E97C08DAE378C71EF70A60E31AE900, L_32, NULL);
		V_0 = L_33;
	}

IL_00cf:
	{
		String_t* L_34 = V_0;
		return L_34;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalDeeplinkingRuntimeScript_Start_m6556A267A12B3918FE6D13CB69759FE194604435 (UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4B70C3AEF886C176543D1295507B6455C9DCAEA7(L_0, NULL);
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::OnApplicationPause(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalDeeplinkingRuntimeScript_OnApplicationPause_m208BDA66613ADA18D304D052695602106374B3A0 (UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* __this, bool ___0_pauseStatus, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_pauseStatus;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		RuntimeObject* L_1;
		L_1 = UniversalDeeplinkingRuntimeScript_CallDeepLinkManagerAfterDelay_m8FF09F77A4C5D6C163874430FE8C358C6B790875(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_2;
		L_2 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_1, NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::OnApplicationFocus(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalDeeplinkingRuntimeScript_OnApplicationFocus_m0CFE61E73EDC160BF0847F6E711953BBA6931C08 (UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* __this, bool ___0_focus, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_focus;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		RuntimeObject* L_1;
		L_1 = UniversalDeeplinkingRuntimeScript_CallDeepLinkManagerAfterDelay_m8FF09F77A4C5D6C163874430FE8C358C6B790875(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_2;
		L_2 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_1, NULL);
	}

IL_0010:
	{
		return;
	}
}
// System.Collections.IEnumerator ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::CallDeepLinkManagerAfterDelay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UniversalDeeplinkingRuntimeScript_CallDeepLinkManagerAfterDelay_m8FF09F77A4C5D6C163874430FE8C358C6B790875 (UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D* L_0 = (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D*)il2cpp_codegen_object_new(U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CCallDeepLinkManagerAfterDelayU3Ed__4__ctor_m89A227A443A7DBF09A566977E18E9099C961FB25(L_0, 0, NULL);
		U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniversalDeeplinkingRuntimeScript__ctor_m570FD7F1929D4832FEE4AC36D62286C947347A4F (UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCallDeepLinkManagerAfterDelayU3Ed__4__ctor_m89A227A443A7DBF09A566977E18E9099C961FB25 (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D* __this, int32_t ___0_U3CU3E1__state, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___0_U3CU3E1__state;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_IDisposable_Dispose_m33EE387E1606D5D48133EDB54EB67AE65CEB44E4 (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCallDeepLinkManagerAfterDelayU3Ed__4_MoveNext_mD6F2D111D998B6232DC3C3F17653F25FB6DAD884 (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t* V_1 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_004b;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* L_3 = __this->___U3CU3E4__this_2;
		NullCheck(L_3);
		bool L_4 = L_3->____onJob_4;
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		return (bool)0;
	}

IL_0026:
	{
		UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* L_5 = __this->___U3CU3E4__this_2;
		NullCheck(L_5);
		L_5->____onJob_4 = (bool)1;
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_6 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_6, (0.200000003f), NULL);
		__this->___U3CU3E2__current_1 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_6);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_004b:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_007c:
			{// begin finally (depth: 1)
				UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* L_7 = __this->___U3CU3E4__this_2;
				NullCheck(L_7);
				L_7->____onJob_4 = (bool)0;
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			try
			{// begin try (depth: 2)
				il2cpp_codegen_runtime_class_init_inline(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
				DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* L_8;
				L_8 = DeepLinkManager_get_Instance_mEA1C431B7FB9CF97E4776FE073A655E7F3804F4C_inline(NULL);
				NullCheck(L_8);
				DeepLinkManager_GameCameFromPause_m3ED4809D0F55061092E005BF387877AB0B29F7C7(L_8, NULL);
				goto IL_0089;
			}// end try (depth: 2)
			catch(Il2CppExceptionWrapper& e)
			{
				if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
				{
					IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
					goto CATCH_005e_1;
				}
				throw e;
			}

CATCH_005e_1:
			{// begin catch(System.Exception)
				V_1 = ((Exception_t*)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t*));
				Exception_t* L_9 = V_1;
				String_t* L_10;
				L_10 = String_Concat_m81A40DADB89BCDB78DA8D63D6B723B0023A09A1A(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral4697D4C3588C40B4B90B49284BEEC64F643F83E0)), L_9, NULL);
				UniversalDeeplinkingRuntimeScript_t313AC3358BF599D62A660601D70E281FDD88D9C6* L_11 = __this->___U3CU3E4__this_2;
				NullCheck(L_11);
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12;
				L_12 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_11, NULL);
				il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var)));
				Debug_LogError_m94F967AB31244EACE68C3BE1DD85B69ED3334C0E(L_10, L_12, NULL);
				IL2CPP_POP_ACTIVE_EXCEPTION();
				goto IL_0089;
			}// end catch (depth: 2)
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0089:
	{
		return (bool)0;
	}
}
// System.Object ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE46E5437BB886F958E8AB8F42AFE91DDB4069ADC (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_IEnumerator_Reset_mFCAC32BCF1F334879A7CAECAF6D4E7848FF1AEA8 (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_IEnumerator_Reset_mFCAC32BCF1F334879A7CAECAF6D4E7848FF1AEA8_RuntimeMethod_var)));
	}
}
// System.Object ImaginationOverflow.UniversalDeepLinking.UniversalDeeplinkingRuntimeScript/<CallDeepLinkManagerAfterDelay>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCallDeepLinkManagerAfterDelayU3Ed__4_System_Collections_IEnumerator_get_Current_m447D65981EB2BB4BDA0F2A89A4E520AD5A32E99C (U3CCallDeepLinkManagerAfterDelayU3Ed__4_t43D58CDE02E7BFEAF5395304AD1B359E484FB05D* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DummyLinkProvider_Initialize_m833CAC38E08BBA2F408FA82755B46819479720FC (DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86F3118A685A936A755CDCDC1C8266CA46F98E01);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(_stringLiteral86F3118A685A936A755CDCDC1C8266CA46F98E01, NULL);
		return (bool)0;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider::add_LinkReceived(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DummyLinkProvider_add_LinkReceived_m11BFA002A57713693925AC78BBD550BD9FD45995 (DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___LinkReceived_0;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___LinkReceived_0);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider::remove_LinkReceived(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DummyLinkProvider_remove_LinkReceived_mA5AFDF268BEFDA3BB513ADDB1E23C2DB2850550A (DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___LinkReceived_0;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___LinkReceived_0);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider::PollInfoAfterPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DummyLinkProvider_PollInfoAfterPause_m8E661A2643CB69FE8753939BB5E76F8729950C42 (DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider::OnLinkReceived(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DummyLinkProvider_OnLinkReceived_m031058ADF466A1819DA490F266DE7F4F57F4FF3E (DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923* __this, String_t* ___0_obj, const RuntimeMethod* method) 
{
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___LinkReceived_0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = __this->___LinkReceived_0;
		String_t* L_2 = ___0_obj;
		NullCheck(L_1);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_1, L_2, NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.DummyLinkProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DummyLinkProvider__ctor_mB55B1A7928A1F581BA1CA886FEEDDE9846E413C6 (DummyLinkProvider_t9B2E246C3A9C16F682939AC5858942B3AE0FB923* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider__ctor_mFCB437434492667E1AA4DBD31326D8409F2FE7F2 (EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		((EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_StaticFields*)il2cpp_codegen_static_fields_for(EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_il2cpp_TypeInfo_var))->____instance_0 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&((EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_StaticFields*)il2cpp_codegen_static_fields_for(EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_il2cpp_TypeInfo_var))->____instance_0), (void*)__this);
		return;
	}
}
// System.Boolean ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EditorLinkProvider_Initialize_mB68C2F44C7EC9E0666B8C1FA503814371EEC758B (EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* __this, const RuntimeMethod* method) 
{
	{
		return (bool)1;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::add_LinkReceived(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider_add_LinkReceived_m8977312F6A7526252247D2A1508FE7E56B4596C9 (EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___LinkReceived_1;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___LinkReceived_1);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::remove_LinkReceived(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider_remove_LinkReceived_m2F2E1530A3AF22F2CC98BD46A7ECFAC05E53919E (EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* __this, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_1 = NULL;
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_2 = NULL;
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_0 = __this->___LinkReceived_1;
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_1 = V_0;
		V_1 = L_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)Castclass((RuntimeObject*)L_4, Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A_il2cpp_TypeInfo_var));
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A** L_5 = (&__this->___LinkReceived_1);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_6 = V_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_7 = V_1;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*>(L_5, L_6, L_7);
		V_0 = L_8;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_9 = V_0;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_9) == ((RuntimeObject*)(Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::PollInfoAfterPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider_PollInfoAfterPause_m84309BFBA25D682F8CEFAE142982983CE001F928 (EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Void ImaginationOverflow.UniversalDeepLinking.Providers.EditorLinkProvider::SimulateLink(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EditorLinkProvider_SimulateLink_m1E51A4C24DC65BA6FA078F696521C38A7F1F7148 (String_t* ___0_link, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* V_0 = NULL;
	{
		EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* L_0 = ((EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_StaticFields*)il2cpp_codegen_static_fields_for(EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_il2cpp_TypeInfo_var))->____instance_0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D* L_1 = ((EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_StaticFields*)il2cpp_codegen_static_fields_for(EditorLinkProvider_t71DDD853A5072A264018203E2C9F36DE24C1499D_il2cpp_TypeInfo_var))->____instance_0;
		NullCheck(L_1);
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_2 = L_1->___LinkReceived_1;
		V_0 = L_2;
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_3 = V_0;
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		Action_1_t3CB5D1A819C3ED3F99E9E39F890F18633253949A* L_4 = V_0;
		String_t* L_5 = ___0_link;
		NullCheck(L_4);
		Action_1_Invoke_m690438AAE38F9762172E3AE0A33D0B42ACD35790_inline(L_4, L_5, NULL);
	}

IL_001d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeepLinkManager_set_Instance_mDDB14018A99B01EC19C6242FC446C42C0151D6F4_inline (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* L_0 = ___0_value;
		il2cpp_codegen_runtime_class_init_inline(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		((DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var))->___U3CInstanceU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var))->___U3CInstanceU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DeepLinkManager_get_IsSteamBuild_mF4EEEF3AE88E4D5C4E8245717BFD45D6DA89BC17_inline (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsSteamBuildU3Ek__BackingField_1;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeepLinkManager_StoreActivation_mCE4E27C176157602A9EB18C119B85AD76B36674D_inline (DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* __this, String_t* ___0_s, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_s;
		__this->____storedActivation_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____storedActivation_5), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LinkActivationHandler_Invoke_m7F3CE9562E632331EBD841CBB26797A6F6D111F7_inline (LinkActivationHandler_t42071E9E183FA7EF37DFFA5D26440A10AC5F0F4B* __this, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9* ___0_s, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, LinkActivation_tC328A46436E68AF8A4F4A11B817E17EB437588B9*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_s, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Query_m68878E46DA1EB0F469FDC62B500C88105F6FBE0D_inline (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CQueryU3Ek__BackingField_15;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Url_m0C1F31295AC715B962EAEBD390D7A805076743AF_inline (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CUrlU3Ek__BackingField_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CUrlU3Ek__BackingField_14), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UrlEncodingParser_set_Query_m39ED995D87E2479867B08466DD154D8A3D727699_inline (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CQueryU3Ek__BackingField_15 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CQueryU3Ek__BackingField_15), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* UrlEncodingParser_get_Url_mE0ACF7A80D9BB66529F8FDEC1538DEAAFF25C66A_inline (UrlEncodingParser_t4C4E7B55FFD2D0D7AF233EBD34A298A6EDF37061* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CUrlU3Ek__BackingField_14;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* DeepLinkManager_get_Instance_mEA1C431B7FB9CF97E4776FE073A655E7F3804F4C_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var);
		DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F* L_0 = ((DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_StaticFields*)il2cpp_codegen_static_fields_for(DeepLinkManager_tF75B7785A62A7F04E9C4E27719FDFCC89819F10F_il2cpp_TypeInfo_var))->___U3CInstanceU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_mD6472FA27D28B9AE64A0FEF796C72ABBC2420EBF_gshared_inline (Enumerator_t65CC956745B1180C04CE6C6910FB27C5F32BB9FF* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->____currentKey_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_obj, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
