﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<TriLibCore.AssetLoaderContext>
struct Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226;
// System.Action`1<TriLibCore.IContextualizedError>
struct Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30;
// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
struct Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD;
// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64>
struct Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.General.CompoundMaterialKey,TriLibCore.TextureLoadingContext>
struct ConcurrentDictionary_2_t94764B51655C4F04FDAE59E1A6327AFEE05EA292;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,System.Collections.Generic.List`1<TriLibCore.MaterialRendererContext>>
struct ConcurrentDictionary_2_tA783589C825EB0CEA850D32094AAEFFBB3FD5D82;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material>
struct ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2;
// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.ITexture,TriLibCore.TextureLoadingContext>
struct ConcurrentDictionary_2_tBB5915FD91B3F65141A8C3EC64A14F14EEA3905B;
// TriLibCore.General.ConcurrentDictionary`2<System.String,System.String>
struct ConcurrentDictionary_2_tCF44E0035FB42A2A1DF508A4CE0B233163C23F1E;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,TriLibCore.Interfaces.IModel>
struct Dictionary_2_tE704ACFE7C32537A046D8577F8299D1B52ED0C00;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String>
struct Dictionary_2_t15A9DEF843D5DA84170CD8536BA0EBB039EB4ADF;
// System.Collections.Generic.Dictionary`2<TriLibCore.Interfaces.IModel,UnityEngine.GameObject>
struct Dictionary_2_tADE1FC3F6C786CACD6652C2C7275C3A0FD274A9C;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]>
struct Dictionary_2_t23C2BC333CAB1901F8EC82B59264ED8D028DD1AB;
// System.Collections.Generic.Dictionary`2<System.Int32,TriLibCore.Gltf.GltfModel>
struct Dictionary_2_tF02E292412E61E0CEAD729AAEA30E75F26A038B3;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t17BB14695909F39BA5B9F9F51F527D09419C12FC;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>
struct Dictionary_2_t9443D9DE9301D7A3459EAF8D0F2C28822AB6F27E;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710;
// System.Func`2<System.Byte[],TriLibCore.Gltf.GltfTempGeometryGroup>
struct Func_2_t3BD3A03C7D27C20A8AEFB415001126EF9524C8A0;
// System.Collections.Generic.HashSet`1<UnityEngine.Texture>
struct HashSet_1_t70836788BCAF42568800A162B9F23937F5309AE8;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_tDBFC8496F14612776AF930DBF84AFE7D06D1F0E9;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.IList`1<TriLibCore.Gltf.GltfTexture>
struct IList_1_t8FA072AE304C235DECF9BBA777C475459F219E80;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation>
struct IList_1_t7A16CD7EF0938B36E4D20182185F284ECA5F93A2;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimationCurve>
struct IList_1_t8363EB7DF6F32C589EF4B468553C90C405B1C47F;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimationCurveBinding>
struct IList_1_t384D4210F120FEF114CA9675F2A1F4263C660C29;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera>
struct IList_1_t13EA3E1B6894AF8023B793D65EA2E1ED596B6E82;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>
struct IList_1_t54EA2EAA8FF287B3E144BC90047C3E635336CB4C;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight>
struct IList_1_t95B0FF72887258CDC012A1B81E66B66AF3BBE38E;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>
struct IList_1_t0662D113B996C51F1676FFC848F7B3448D818DB7;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>
struct IList_1_tBAC2F9CBFB365F17F69446225AF2802DEF7B2956;
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>
struct IList_1_t2988C79E2C0A953B91ACE72118B299F94ECFEB62;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_tFB8BE2ED9A601C1259EAB8D73D1B3E96EA321FA1;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t4EEE459A249DDE104FA2E88234C593389EE5D291;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t67E8423B5AEB30C254013AD88AB68D2A36F1F436;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36;
// System.Collections.Generic.List`1<TriLibCore.Gltf.GltfTexture>
struct List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33;
// System.Collections.Generic.List`1<TriLibCore.Interfaces.IAnimationCurve>
struct List_1_t098836BD89E26112D46D0209C17F10F176C6D586;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3;
// System.Collections.Generic.List`1<System.Threading.Tasks.Task>
struct List_1_t84C257E858DDB8EA0B6269E08AAD9A2A2018A551;
// System.Collections.Generic.List`1<TriLibCore.Utils.JsonParser/JsonValue>
struct List_1_t9F3D78A42CC34B67EB8F82953E3089BA27242131;
// System.Collections.Generic.Queue`1<TriLibCore.Interfaces.IContextualizedAction>
struct Queue_1_t952DE88AF42216B755D09647735E4235DA7138D4;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t74AF7C1BAE06C66E984668F663D574ED6A596D28;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>[]
struct Dictionary_2U5BU5D_t1C8B29337AC2B2BAE4525B35ADAA299185E6FC49;
// System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Int32>[]
struct EntryU5BU5D_t197C691F43F1694B771BF83C278D12BBFEEB86FA;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// TriLibCore.Gltf.GltfTexture[]
struct GltfTextureU5BU5D_t0D170B280EEFD2D1B9C7B46910E12AA8F08E4CA7;
// TriLibCore.Interfaces.IAnimation[]
struct IAnimationU5BU5D_t70DF9CA11F100A23CC71AA7A9C3ECC44413BF683;
// TriLibCore.Interfaces.IAnimationCurve[]
struct IAnimationCurveU5BU5D_tE36C14C72E92E5F7553EC344F13270A6CF86FB37;
// TriLibCore.Interfaces.IGeometryGroup[]
struct IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711;
// TriLibCore.Interfaces.IMaterial[]
struct IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
// TriLibCore.Gltf.StreamChunk[]
struct StreamChunkU5BU5D_tB8CEFF6BD29A73CA0268FEF0416BDA1EF08AE299;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// UnityEngine.AnimationCurve
struct AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354;
// TriLibCore.AssetLoaderContext
struct AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C;
// TriLibCore.AssetLoaderOptions
struct AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6;
// System.IO.BinaryReader
struct BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// TriLibCore.Gltf.GltfAnimation
struct GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA;
// TriLibCore.Gltf.GltfAnimationCurve
struct GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65;
// TriLibCore.Gltf.GltfAnimationCurveBinding
struct GltfAnimationCurveBinding_t92A27388709EF32E3FCC6565B2E2804A5106BE6F;
// TriLibCore.Gltf.GltfBlendShapeKey
struct GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01;
// TriLibCore.Gltf.GltfModel
struct GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2;
// TriLibCore.Gltf.Reader.GltfReader
struct GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC;
// TriLibCore.Gltf.GltfRootModel
struct GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727;
// TriLibCore.Gltf.GltfTexture
struct GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8;
// TriLibCore.Gltf.GtlfProcessor
struct GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A;
// TriLibCore.Interfaces.IGeometryGroup
struct IGeometryGroup_tD96DA51C7C61A1D17AF86985D3E39BA7A14A6262;
// TriLibCore.Interfaces.IModel
struct IModel_tB1636FE3764D3ADC10719BBE5EFDD5542F86F6FB;
// TriLibCore.Interfaces.IRootModel
struct IRootModel_t83ED40397FD23448FC9A99336523CC7DE8A841BB;
// TriLibCore.Interfaces.ITexture
struct ITexture_t4CD71425D2DAB0C38B4E57E909DEAC9A9AC89FE8;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2;
// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE;
// System.String
struct String_t;
// System.Threading.Tasks.Task
struct Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572;
// System.Type
struct Type_t;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05;

IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_t197C7901E591B451319A83E16C668AD8CC21DFFA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_t9D9853EC356A71B3BC036D2810F70EC0DF1361DD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IList_1_t8FA072AE304C235DECF9BBA777C475459F219E80_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t098836BD89E26112D46D0209C17F10F176C6D586_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral119B330A1FA1C051D7BE15A6ABD55D08AFF99689;
IL2CPP_EXTERN_C String_t* _stringLiteral80A749BBF097574027749577C42FF229E10C6B66;
IL2CPP_EXTERN_C String_t* _stringLiteralA18955232B8E3141B1616DF7C0C29A59B8A13EF3;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mB381575D9E7BA6706F57C22D567610F51B5CD868_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m050637C82E9575B3CB1DC7720A6432B96DE8F2A1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF2089CE11FD3E3DBF9541C4C1122B489E72BD4AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* ProcessingSteps_t85CB655500227D688208A221C3E52FF54DC2B490_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_0_0_0_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t197C691F43F1694B771BF83C278D12BBFEEB86FA* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t67E8423B5AEB30C254013AD88AB68D2A36F1F436* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_t74AF7C1BAE06C66E984668F663D574ED6A596D28* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.List`1<TriLibCore.Gltf.GltfTexture>
struct List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	GltfTextureU5BU5D_t0D170B280EEFD2D1B9C7B46910E12AA8F08E4CA7* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<TriLibCore.Interfaces.IAnimationCurve>
struct List_1_t098836BD89E26112D46D0209C17F10F176C6D586  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	IAnimationCurveU5BU5D_tE36C14C72E92E5F7553EC344F13270A6CF86FB37* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_tFE65A6ACFEE669F7B22C131E38A0D585637FDB33  : public RuntimeObject
{
};

// TriLibCore.Gltf.GltfAnimation
struct GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA  : public RuntimeObject
{
	// System.String TriLibCore.Gltf.GltfAnimation::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Gltf.GltfAnimation::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimationCurveBinding> TriLibCore.Gltf.GltfAnimation::<AnimationCurveBindings>k__BackingField
	RuntimeObject* ___U3CAnimationCurveBindingsU3Ek__BackingField_2;
	// System.Single TriLibCore.Gltf.GltfAnimation::<FrameRate>k__BackingField
	float ___U3CFrameRateU3Ek__BackingField_3;
};

// TriLibCore.Gltf.GltfAnimationCurve
struct GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65  : public RuntimeObject
{
	// System.String TriLibCore.Gltf.GltfAnimationCurve::<Property>k__BackingField
	String_t* ___U3CPropertyU3Ek__BackingField_0;
	// System.Type TriLibCore.Gltf.GltfAnimationCurve::<AnimatedType>k__BackingField
	Type_t* ___U3CAnimatedTypeU3Ek__BackingField_1;
	// UnityEngine.AnimationCurve TriLibCore.Gltf.GltfAnimationCurve::<AnimationCurve>k__BackingField
	AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___U3CAnimationCurveU3Ek__BackingField_2;
	// TriLibCore.General.TangentMode TriLibCore.Gltf.GltfAnimationCurve::<TangentMode>k__BackingField
	int32_t ___U3CTangentModeU3Ek__BackingField_3;
};

// TriLibCore.Gltf.GltfAnimationCurveBinding
struct GltfAnimationCurveBinding_t92A27388709EF32E3FCC6565B2E2804A5106BE6F  : public RuntimeObject
{
	// TriLibCore.Interfaces.IModel TriLibCore.Gltf.GltfAnimationCurveBinding::<Model>k__BackingField
	RuntimeObject* ___U3CModelU3Ek__BackingField_0;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimationCurve> TriLibCore.Gltf.GltfAnimationCurveBinding::<AnimationCurves>k__BackingField
	RuntimeObject* ___U3CAnimationCurvesU3Ek__BackingField_1;
};

// TriLibCore.Gltf.GltfBlendShapeKey
struct GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01  : public RuntimeObject
{
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TriLibCore.Gltf.GltfBlendShapeKey::<IndexMap>k__BackingField
	Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* ___U3CIndexMapU3Ek__BackingField_0;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Gltf.GltfBlendShapeKey::<Vertices>k__BackingField
	RuntimeObject* ___U3CVerticesU3Ek__BackingField_1;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Gltf.GltfBlendShapeKey::<Normals>k__BackingField
	RuntimeObject* ___U3CNormalsU3Ek__BackingField_2;
	// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Gltf.GltfBlendShapeKey::<Tangents>k__BackingField
	RuntimeObject* ___U3CTangentsU3Ek__BackingField_3;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Gltf.GltfBlendShapeKey::<Indices>k__BackingField
	RuntimeObject* ___U3CIndicesU3Ek__BackingField_4;
	// System.Single TriLibCore.Gltf.GltfBlendShapeKey::<FrameWeight>k__BackingField
	float ___U3CFrameWeightU3Ek__BackingField_5;
	// System.Boolean TriLibCore.Gltf.GltfBlendShapeKey::<FullGeometryShape>k__BackingField
	bool ___U3CFullGeometryShapeU3Ek__BackingField_6;
	// System.String TriLibCore.Gltf.GltfBlendShapeKey::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_7;
	// System.Boolean TriLibCore.Gltf.GltfBlendShapeKey::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_8;
};

// System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE  : public RuntimeObject
{
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject* ____identity_0;
};
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449  : public RuntimeObject
{
	// System.String[] TriLibCore.ReaderBase::_loadingStepEnumNames
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____loadingStepEnumNames_1;
	// TriLibCore.AssetLoaderContext TriLibCore.ReaderBase::<AssetLoaderContext>k__BackingField
	AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___U3CAssetLoaderContextU3Ek__BackingField_2;
	// System.String TriLibCore.ReaderBase::_filename
	String_t* ____filename_3;
	// System.Action`2<TriLibCore.AssetLoaderContext,System.Single> TriLibCore.ReaderBase::_onProgress
	Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ____onProgress_4;
	// System.Int32 TriLibCore.ReaderBase::_nameCounter
	int32_t ____nameCounter_5;
	// System.Int32 TriLibCore.ReaderBase::_materialCounter
	int32_t ____materialCounter_6;
	// System.Int32 TriLibCore.ReaderBase::_textureCounter
	int32_t ____textureCounter_7;
	// System.Int32 TriLibCore.ReaderBase::_geometryGroupCounter
	int32_t ____geometryGroupCounter_8;
	// System.Int32 TriLibCore.ReaderBase::_animationCounter
	int32_t ____animationCounter_9;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED 
{
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::_source
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};
// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_pinvoke
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_com
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

// TriLibCore.Gltf.Reader.GltfReader
struct GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC  : public ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449
{
	// TriLibCore.Gltf.GtlfProcessor TriLibCore.Gltf.Reader.GltfReader::_gtlfProcessor
	GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A* ____gtlfProcessor_11;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE  : public MarshalByRefObject_t8C2F4C5854177FD60439EB1FCCFC1B3CFAFE8DCE
{
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t0821BF49EE38596C7734E86E1A6A39D769BE2C05* ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t0D5CB5685D9BFA5BF95CEC6E7395490F933E8DB2* ____asyncActiveSemaphore_4;
};

// TriLibCore.Gltf.TemporaryString
struct TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7 
{
	// System.Char[] TriLibCore.Gltf.TemporaryString::_chars
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ____chars_0;
	// System.String TriLibCore.Gltf.TemporaryString::_charString
	String_t* ____charString_1;
	// System.Int32 TriLibCore.Gltf.TemporaryString::_length
	int32_t ____length_2;
};
// Native definition for P/Invoke marshalling of TriLibCore.Gltf.TemporaryString
struct TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshaled_pinvoke
{
	uint8_t* ____chars_0;
	char* ____charString_1;
	int32_t ____length_2;
};
// Native definition for COM marshalling of TriLibCore.Gltf.TemporaryString
struct TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshaled_com
{
	uint8_t* ____chars_0;
	Il2CppChar* ____charString_1;
	int32_t ____length_2;
};

// System.UInt32
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// TriLibCore.Utils.JsonParser/JsonValue
struct JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 
{
	// System.IO.BinaryReader TriLibCore.Utils.JsonParser/JsonValue::<BinaryReader>k__BackingField
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___U3CBinaryReaderU3Ek__BackingField_0;
	// System.Int32 TriLibCore.Utils.JsonParser/JsonValue::<Position>k__BackingField
	int32_t ___U3CPositionU3Ek__BackingField_1;
	// System.Int32 TriLibCore.Utils.JsonParser/JsonValue::<ValueLength>k__BackingField
	int32_t ___U3CValueLengthU3Ek__BackingField_2;
	// TriLibCore.Utils.JsonParser/JsonValueType TriLibCore.Utils.JsonParser/JsonValue::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.Int64,System.Int32> TriLibCore.Utils.JsonParser/JsonValue::_hashes
	Dictionary_2_t9443D9DE9301D7A3459EAF8D0F2C28822AB6F27E* ____hashes_4;
	// System.Collections.Generic.List`1<TriLibCore.Utils.JsonParser/JsonValue> TriLibCore.Utils.JsonParser/JsonValue::_children
	List_1_t9F3D78A42CC34B67EB8F82953E3089BA27242131* ____children_5;
};
// Native definition for P/Invoke marshalling of TriLibCore.Utils.JsonParser/JsonValue
struct JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914_marshaled_pinvoke
{
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___U3CBinaryReaderU3Ek__BackingField_0;
	int32_t ___U3CPositionU3Ek__BackingField_1;
	int32_t ___U3CValueLengthU3Ek__BackingField_2;
	int32_t ___U3CTypeU3Ek__BackingField_3;
	Dictionary_2_t9443D9DE9301D7A3459EAF8D0F2C28822AB6F27E* ____hashes_4;
	List_1_t9F3D78A42CC34B67EB8F82953E3089BA27242131* ____children_5;
};
// Native definition for COM marshalling of TriLibCore.Utils.JsonParser/JsonValue
struct JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914_marshaled_com
{
	BinaryReader_t9A6D85F0FE9AE4EBB5E8D66997DFD1D84939E158* ___U3CBinaryReaderU3Ek__BackingField_0;
	int32_t ___U3CPositionU3Ek__BackingField_1;
	int32_t ___U3CValueLengthU3Ek__BackingField_2;
	int32_t ___U3CTypeU3Ek__BackingField_3;
	Dictionary_2_t9443D9DE9301D7A3459EAF8D0F2C28822AB6F27E* ____hashes_4;
	List_1_t9F3D78A42CC34B67EB8F82953E3089BA27242131* ____children_5;
};

// UnityEngine.AnimationCurve
struct AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354  : public RuntimeObject
{
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// TriLibCore.AssetLoaderContext
struct AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C  : public RuntimeObject
{
	// TriLibCore.AssetLoaderOptions TriLibCore.AssetLoaderContext::Options
	AssetLoaderOptions_t48AC975EC75B1EB3A0523309F6FD3713B4CA93D6* ___Options_0;
	// TriLibCore.ReaderBase TriLibCore.AssetLoaderContext::Reader
	ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* ___Reader_1;
	// System.String TriLibCore.AssetLoaderContext::Filename
	String_t* ___Filename_2;
	// System.String TriLibCore.AssetLoaderContext::FileExtension
	String_t* ___FileExtension_3;
	// System.IO.Stream TriLibCore.AssetLoaderContext::Stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Stream_4;
	// System.String TriLibCore.AssetLoaderContext::BasePath
	String_t* ___BasePath_5;
	// UnityEngine.GameObject TriLibCore.AssetLoaderContext::RootGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___RootGameObject_6;
	// TriLibCore.Interfaces.IRootModel TriLibCore.AssetLoaderContext::RootModel
	RuntimeObject* ___RootModel_7;
	// System.Collections.Generic.Dictionary`2<TriLibCore.Interfaces.IModel,UnityEngine.GameObject> TriLibCore.AssetLoaderContext::GameObjects
	Dictionary_2_tADE1FC3F6C786CACD6652C2C7275C3A0FD274A9C* ___GameObjects_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,TriLibCore.Interfaces.IModel> TriLibCore.AssetLoaderContext::Models
	Dictionary_2_tE704ACFE7C32537A046D8577F8299D1B52ED0C00* ___Models_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String> TriLibCore.AssetLoaderContext::GameObjectPaths
	Dictionary_2_t15A9DEF843D5DA84170CD8536BA0EBB039EB4ADF* ___GameObjectPaths_10;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,System.Collections.Generic.List`1<TriLibCore.MaterialRendererContext>> TriLibCore.AssetLoaderContext::MaterialRenderers
	ConcurrentDictionary_2_tA783589C825EB0CEA850D32094AAEFFBB3FD5D82* ___MaterialRenderers_11;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material> TriLibCore.AssetLoaderContext::LoadedMaterials
	ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2* ___LoadedMaterials_12;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.IMaterial,UnityEngine.Material> TriLibCore.AssetLoaderContext::GeneratedMaterials
	ConcurrentDictionary_2_t3C2E41F7177FE1C1CDEBF4CACA510701BAF4AEF2* ___GeneratedMaterials_13;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.Interfaces.ITexture,TriLibCore.TextureLoadingContext> TriLibCore.AssetLoaderContext::LoadedTextures
	ConcurrentDictionary_2_tBB5915FD91B3F65141A8C3EC64A14F14EEA3905B* ___LoadedTextures_14;
	// TriLibCore.General.ConcurrentDictionary`2<TriLibCore.General.CompoundMaterialKey,TriLibCore.TextureLoadingContext> TriLibCore.AssetLoaderContext::MaterialTextures
	ConcurrentDictionary_2_t94764B51655C4F04FDAE59E1A6327AFEE05EA292* ___MaterialTextures_15;
	// TriLibCore.General.ConcurrentDictionary`2<System.String,System.String> TriLibCore.AssetLoaderContext::LoadedExternalData
	ConcurrentDictionary_2_tCF44E0035FB42A2A1DF508A4CE0B233163C23F1E* ___LoadedExternalData_16;
	// System.Collections.Generic.HashSet`1<UnityEngine.Texture> TriLibCore.AssetLoaderContext::UsedTextures
	HashSet_1_t70836788BCAF42568800A162B9F23937F5309AE8* ___UsedTextures_17;
	// System.Collections.Generic.List`1<UnityEngine.Object> TriLibCore.AssetLoaderContext::Allocations
	List_1_t9A2E2984B23260AECDFA90CEB1F2887075FA4DF3* ___Allocations_18;
	// System.Boolean TriLibCore.AssetLoaderContext::Async
	bool ___Async_19;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnLoad_20;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnMaterialsLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnMaterialsLoad_21;
	// System.Action`2<TriLibCore.AssetLoaderContext,System.Single> TriLibCore.AssetLoaderContext::OnProgress
	Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___OnProgress_22;
	// System.Action`1<TriLibCore.IContextualizedError> TriLibCore.AssetLoaderContext::OnError
	Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30* ___OnError_23;
	// System.Action`1<TriLibCore.IContextualizedError> TriLibCore.AssetLoaderContext::HandleError
	Action_1_t3B6442C6168F6F5364512C62A54645843CF93C30* ___HandleError_24;
	// System.Object TriLibCore.AssetLoaderContext::CustomData
	RuntimeObject* ___CustomData_25;
	// System.Collections.Generic.List`1<System.Threading.Tasks.Task> TriLibCore.AssetLoaderContext::Tasks
	List_1_t84C257E858DDB8EA0B6269E08AAD9A2A2018A551* ___Tasks_26;
	// System.Threading.Tasks.Task TriLibCore.AssetLoaderContext::Task
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___Task_27;
	// System.Boolean TriLibCore.AssetLoaderContext::HaltTasks
	bool ___HaltTasks_28;
	// UnityEngine.GameObject TriLibCore.AssetLoaderContext::WrapperGameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___WrapperGameObject_29;
	// System.Threading.CancellationToken TriLibCore.AssetLoaderContext::CancellationToken
	CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED ___CancellationToken_30;
	// System.Single TriLibCore.AssetLoaderContext::LoadingProgress
	float ___LoadingProgress_31;
	// System.Threading.CancellationTokenSource TriLibCore.AssetLoaderContext::CancellationTokenSource
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ___CancellationTokenSource_32;
	// System.Action`1<TriLibCore.AssetLoaderContext> TriLibCore.AssetLoaderContext::OnPreLoad
	Action_1_tD39AE6C0C913B7D513756AFCD7E9F0539538D226* ___OnPreLoad_33;
	// System.Boolean TriLibCore.AssetLoaderContext::IsZipFile
	bool ___IsZipFile_34;
	// System.String TriLibCore.AssetLoaderContext::<PersistentDataPath>k__BackingField
	String_t* ___U3CPersistentDataPathU3Ek__BackingField_35;
	// System.String TriLibCore.AssetLoaderContext::ModificationDate
	String_t* ___ModificationDate_36;
	// System.Int32 TriLibCore.AssetLoaderContext::LoadingStep
	int32_t ___LoadingStep_37;
	// System.Int32 TriLibCore.AssetLoaderContext::PreviousLoadingStep
	int32_t ___PreviousLoadingStep_38;
	// System.Collections.Generic.Queue`1<TriLibCore.Interfaces.IContextualizedAction> TriLibCore.AssetLoaderContext::<CustomDispatcherQueue>k__BackingField
	Queue_1_t952DE88AF42216B755D09647735E4235DA7138D4* ___U3CCustomDispatcherQueueU3Ek__BackingField_39;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Byte[]> TriLibCore.AssetLoaderContext::_bufferPool
	Dictionary_2_t23C2BC333CAB1901F8EC82B59264ED8D028DD1AB* ____bufferPool_40;
	// System.Boolean TriLibCore.AssetLoaderContext::<Completed>k__BackingField
	bool ___U3CCompletedU3Ek__BackingField_41;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// TriLibCore.Gltf.GltfModel
struct GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2  : public RuntimeObject
{
	// System.String TriLibCore.Gltf.GltfModel::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Gltf.GltfModel::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// UnityEngine.Vector3 TriLibCore.Gltf.GltfModel::<LocalPosition>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalPositionU3Ek__BackingField_2;
	// UnityEngine.Quaternion TriLibCore.Gltf.GltfModel::<LocalRotation>k__BackingField
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___U3CLocalRotationU3Ek__BackingField_3;
	// UnityEngine.Vector3 TriLibCore.Gltf.GltfModel::<LocalScale>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CLocalScaleU3Ek__BackingField_4;
	// System.Boolean TriLibCore.Gltf.GltfModel::<Visibility>k__BackingField
	bool ___U3CVisibilityU3Ek__BackingField_5;
	// TriLibCore.Interfaces.IModel TriLibCore.Gltf.GltfModel::<Parent>k__BackingField
	RuntimeObject* ___U3CParentU3Ek__BackingField_6;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Gltf.GltfModel::<Children>k__BackingField
	RuntimeObject* ___U3CChildrenU3Ek__BackingField_7;
	// System.Boolean TriLibCore.Gltf.GltfModel::<IsBone>k__BackingField
	bool ___U3CIsBoneU3Ek__BackingField_8;
	// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Gltf.GltfModel::<GeometryGroup>k__BackingField
	RuntimeObject* ___U3CGeometryGroupU3Ek__BackingField_9;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Gltf.GltfModel::<Bones>k__BackingField
	RuntimeObject* ___U3CBonesU3Ek__BackingField_10;
	// UnityEngine.Matrix4x4[] TriLibCore.Gltf.GltfModel::<BindPoses>k__BackingField
	Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___U3CBindPosesU3Ek__BackingField_11;
	// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Gltf.GltfModel::<MaterialIndices>k__BackingField
	RuntimeObject* ___U3CMaterialIndicesU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Gltf.GltfModel::<UserProperties>k__BackingField
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___U3CUserPropertiesU3Ek__BackingField_13;
};

// TriLibCore.Gltf.GltfTexture
struct GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8  : public RuntimeObject
{
	// System.String TriLibCore.Gltf.GltfTexture::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean TriLibCore.Gltf.GltfTexture::<Used>k__BackingField
	bool ___U3CUsedU3Ek__BackingField_1;
	// System.Byte[] TriLibCore.Gltf.GltfTexture::<Data>k__BackingField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___U3CDataU3Ek__BackingField_2;
	// System.IO.Stream TriLibCore.Gltf.GltfTexture::<DataStream>k__BackingField
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___U3CDataStreamU3Ek__BackingField_3;
	// System.String TriLibCore.Gltf.GltfTexture::<Filename>k__BackingField
	String_t* ___U3CFilenameU3Ek__BackingField_4;
	// UnityEngine.TextureWrapMode TriLibCore.Gltf.GltfTexture::<WrapModeU>k__BackingField
	int32_t ___U3CWrapModeUU3Ek__BackingField_5;
	// UnityEngine.TextureWrapMode TriLibCore.Gltf.GltfTexture::<WrapModeV>k__BackingField
	int32_t ___U3CWrapModeVU3Ek__BackingField_6;
	// UnityEngine.Vector2 TriLibCore.Gltf.GltfTexture::<Tiling>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CTilingU3Ek__BackingField_7;
	// UnityEngine.Vector2 TriLibCore.Gltf.GltfTexture::<Offset>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3COffsetU3Ek__BackingField_8;
	// System.Int32 TriLibCore.Gltf.GltfTexture::<TextureId>k__BackingField
	int32_t ___U3CTextureIdU3Ek__BackingField_9;
	// System.String TriLibCore.Gltf.GltfTexture::<ResolvedFilename>k__BackingField
	String_t* ___U3CResolvedFilenameU3Ek__BackingField_10;
	// System.Int32 TriLibCore.Gltf.GltfTexture::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_11;
	// System.Boolean TriLibCore.Gltf.GltfTexture::<HasAlpha>k__BackingField
	bool ___U3CHasAlphaU3Ek__BackingField_12;
	// TriLibCore.General.TextureFormat TriLibCore.Gltf.GltfTexture::<TextureFormat>k__BackingField
	int32_t ___U3CTextureFormatU3Ek__BackingField_13;
	// System.Collections.Generic.IList`1<TriLibCore.Gltf.GltfTexture> TriLibCore.Gltf.GltfTexture::TextureVariations
	RuntimeObject* ___TextureVariations_14;
};

// TriLibCore.Gltf.GtlfProcessor
struct GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A  : public RuntimeObject
{
	// TriLibCore.Gltf.Reader.GltfReader TriLibCore.Gltf.GtlfProcessor::_reader
	GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC* ____reader_125;
	// TriLibCore.Gltf.StreamChunk[] TriLibCore.Gltf.GtlfProcessor::_buffersData
	StreamChunkU5BU5D_tB8CEFF6BD29A73CA0268FEF0416BDA1EF08AE299* ____buffersData_126;
	// TriLibCore.Interfaces.IAnimation[] TriLibCore.Gltf.GtlfProcessor::_animations
	IAnimationU5BU5D_t70DF9CA11F100A23CC71AA7A9C3ECC44413BF683* ____animations_127;
	// TriLibCore.Interfaces.IMaterial[] TriLibCore.Gltf.GtlfProcessor::_materials
	IMaterialU5BU5D_tDC50DDA10471488138C03AB89CC3A07F6AF8BAD2* ____materials_128;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Gltf.GtlfProcessor::_textures
	RuntimeObject* ____textures_129;
	// TriLibCore.Interfaces.IGeometryGroup[] TriLibCore.Gltf.GtlfProcessor::_geometryGroups
	IGeometryGroupU5BU5D_tA72A26B4C22A9EBB26F3F9CD8C05F44B292DA711* ____geometryGroups_130;
	// System.Collections.Generic.Dictionary`2<System.Int32,TriLibCore.Gltf.GltfModel> TriLibCore.Gltf.GtlfProcessor::_models
	Dictionary_2_tF02E292412E61E0CEAD729AAEA30E75F26A038B3* ____models_131;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Matrix4x4>[] TriLibCore.Gltf.GtlfProcessor::_skins
	Dictionary_2U5BU5D_t1C8B29337AC2B2BAE4525B35ADAA299185E6FC49* ____skins_132;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Gltf.GtlfProcessor::_cameras
	RuntimeObject* ____cameras_133;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Gltf.GtlfProcessor::_lights
	RuntimeObject* ____lights_134;
	// System.IO.Stream TriLibCore.Gltf.GtlfProcessor::_stream
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ____stream_135;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::buffers
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___buffers_136;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::textures
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___textures_137;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::images
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___images_138;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::samplers
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___samplers_139;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::materials
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___materials_140;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::accessors
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___accessors_141;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::bufferViews
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___bufferViews_142;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::scenes
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___scenes_143;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::nodes
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___nodes_144;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::skins
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___skins_145;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::animations
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___animations_146;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::meshes
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___meshes_147;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::cameras
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___cameras_148;
	// TriLibCore.Utils.JsonParser/JsonValue TriLibCore.Gltf.GtlfProcessor::lights
	JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___lights_149;
	// System.Boolean TriLibCore.Gltf.GtlfProcessor::_quantitized
	bool ____quantitized_150;
	// System.Boolean TriLibCore.Gltf.GtlfProcessor::_usesDraco
	bool ____usesDraco_151;
	// System.Boolean TriLibCore.Gltf.GtlfProcessor::_usesLights
	bool ____usesLights_152;
	// System.Byte[] TriLibCore.Gltf.GtlfProcessor::_tempBytes8
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____tempBytes8_191;
	// TriLibCore.Gltf.TemporaryString TriLibCore.Gltf.GtlfProcessor::_temporaryString
	TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7 ____temporaryString_192;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TriLibCore.Gltf.GtlfProcessor::_minIntValues
	Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* ____minIntValues_193;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TriLibCore.Gltf.GtlfProcessor::_maxIntValues
	Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* ____maxIntValues_194;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> TriLibCore.Gltf.GtlfProcessor::_minFloatValues
	Dictionary_2_t17BB14695909F39BA5B9F9F51F527D09419C12FC* ____minFloatValues_195;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> TriLibCore.Gltf.GtlfProcessor::_maxFloatValues
	Dictionary_2_t17BB14695909F39BA5B9F9F51F527D09419C12FC* ____maxFloatValues_196;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// TriLibCore.Gltf.GltfRootModel
struct GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727  : public GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2
{
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Gltf.GltfRootModel::<AllModels>k__BackingField
	RuntimeObject* ___U3CAllModelsU3Ek__BackingField_14;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Gltf.GltfRootModel::<AllGeometryGroups>k__BackingField
	RuntimeObject* ___U3CAllGeometryGroupsU3Ek__BackingField_15;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Gltf.GltfRootModel::<AllAnimations>k__BackingField
	RuntimeObject* ___U3CAllAnimationsU3Ek__BackingField_16;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Gltf.GltfRootModel::<AllMaterials>k__BackingField
	RuntimeObject* ___U3CAllMaterialsU3Ek__BackingField_17;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Gltf.GltfRootModel::<AllTextures>k__BackingField
	RuntimeObject* ___U3CAllTexturesU3Ek__BackingField_18;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Gltf.GltfRootModel::<AllCameras>k__BackingField
	RuntimeObject* ___U3CAllCamerasU3Ek__BackingField_19;
	// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Gltf.GltfRootModel::<AllLights>k__BackingField
	RuntimeObject* ___U3CAllLightsU3Ek__BackingField_20;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
struct Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD  : public MulticastDelegate_t
{
};

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>

// System.Collections.Generic.List`1<TriLibCore.Gltf.GltfTexture>
struct List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	GltfTextureU5BU5D_t0D170B280EEFD2D1B9C7B46910E12AA8F08E4CA7* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<TriLibCore.Gltf.GltfTexture>

// System.Collections.Generic.List`1<TriLibCore.Interfaces.IAnimationCurve>
struct List_1_t098836BD89E26112D46D0209C17F10F176C6D586_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	IAnimationCurveU5BU5D_tE36C14C72E92E5F7553EC344F13270A6CF86FB37* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<TriLibCore.Interfaces.IAnimationCurve>

// <PrivateImplementationDetails>

// <PrivateImplementationDetails>

// TriLibCore.Gltf.GltfAnimation

// TriLibCore.Gltf.GltfAnimation

// TriLibCore.Gltf.GltfAnimationCurve

// TriLibCore.Gltf.GltfAnimationCurve

// TriLibCore.Gltf.GltfAnimationCurveBinding

// TriLibCore.Gltf.GltfAnimationCurveBinding

// TriLibCore.Gltf.GltfBlendShapeKey

// TriLibCore.Gltf.GltfBlendShapeKey

// System.MarshalByRefObject

// System.MarshalByRefObject

// System.Reflection.MemberInfo

// System.Reflection.MemberInfo

// TriLibCore.ReaderBase
struct ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449_StaticFields
{
	// System.Action`4<System.String,System.String,System.TimeSpan,System.Int64> TriLibCore.ReaderBase::ProfileStepCallback
	Action_4_tA3594528C5AC13E7A27B50D19223DC951CD1E8B2* ___ProfileStepCallback_0;
};

// TriLibCore.ReaderBase

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// System.ValueType

// System.ValueType

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// System.Byte

// System.Byte

// System.Threading.CancellationToken
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_StaticFields
{
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_actionToActionObjShunt
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___s_actionToActionObjShunt_1;
};

// System.Threading.CancellationToken

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// System.Char

// TriLibCore.Gltf.Reader.GltfReader
struct GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC_StaticFields
{
	// System.Single TriLibCore.Gltf.Reader.GltfReader::SpotLightDistance
	float ___SpotLightDistance_10;
	// System.Func`2<System.Byte[],TriLibCore.Gltf.GltfTempGeometryGroup> TriLibCore.Gltf.Reader.GltfReader::DracoDecompressorCallback
	Func_2_t3BD3A03C7D27C20A8AEFB415001126EF9524C8A0* ___DracoDecompressorCallback_12;
};

// TriLibCore.Gltf.Reader.GltfReader

// System.Int32

// System.Int32

// System.Int64

// System.Int64

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// UnityEngine.Matrix4x4

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Quaternion

// System.Single

// System.Single

// System.IO.Stream
struct Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE_StaticFields
{
	// System.IO.Stream System.IO.Stream::Null
	Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___Null_1;
};

// System.IO.Stream

// TriLibCore.Gltf.TemporaryString

// TriLibCore.Gltf.TemporaryString

// System.UInt32

// System.UInt32

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3

// System.Void

// System.Void

// TriLibCore.Utils.JsonParser/JsonValue

// TriLibCore.Utils.JsonParser/JsonValue

// UnityEngine.AnimationCurve

// UnityEngine.AnimationCurve

// TriLibCore.AssetLoaderContext

// TriLibCore.AssetLoaderContext

// System.Delegate

// System.Delegate

// TriLibCore.Gltf.GltfModel

// TriLibCore.Gltf.GltfModel

// TriLibCore.Gltf.GltfTexture

// TriLibCore.Gltf.GltfTexture

// TriLibCore.Gltf.GtlfProcessor

// TriLibCore.Gltf.GtlfProcessor

// System.RuntimeTypeHandle

// System.RuntimeTypeHandle

// TriLibCore.Gltf.GltfRootModel

// TriLibCore.Gltf.GltfRootModel

// System.MulticastDelegate

// System.MulticastDelegate

// System.Type
struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// System.Type

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>

// System.Action`2<TriLibCore.AssetLoaderContext,System.Single>
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D  : public RuntimeArray
{
	ALIGN_FIELD (8) Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 m_Items[1];

	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB  : public RuntimeArray
{
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mF225F49F6BE54C39563CECD7C693F0AE4F0530E8_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, int32_t ___0_capacity, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m76CBBC3E2F0583F5AD30CE592CEA1225C06A0428_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___0_capacity, const RuntimeMethod* method) ;

// System.String TriLibCore.Gltf.GltfModel::get_Name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* GltfModel_get_Name_m44A2759A9531585217EB070E7BC5B2E12B03B375_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 TriLibCore.Gltf.GltfModel::get_LocalPosition()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GltfModel_get_LocalPosition_m0226FF0DA5FA36427DD272454DA10365DD64ED96_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) ;
// UnityEngine.Quaternion TriLibCore.Gltf.GltfModel::get_LocalRotation()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 GltfModel_get_LocalRotation_m9E05B0215974A1CA3C784E15769C54D9FEFC45D1_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 TriLibCore.Gltf.GltfModel::get_LocalScale()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GltfModel_get_LocalScale_m5AD4BBDA83740FA1C19D5B092401EAAABFDD663D_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_TRS_mCC04FD47347234B451ACC6CCD2CE6D02E1E0E1E3 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_pos, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_q, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_s, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IModel TriLibCore.Gltf.GltfModel::get_Parent()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* GltfModel_get_Parent_mF5F72F59E99F973AF93FFF62CFB03213E64120C9_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) ;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_op_Multiply_m75E91775655DCA8DFC8EDE0AB787285BB3935162 (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___0_lhs, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___1_rhs, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor(System.Int32)
inline void Dictionary_2__ctor_mB381575D9E7BA6706F57C22D567610F51B5CD868 (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* __this, int32_t ___0_capacity, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*, int32_t, const RuntimeMethod*))Dictionary_2__ctor_mF225F49F6BE54C39563CECD7C693F0AE4F0530E8_gshared)(__this, ___0_capacity, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel__ctor_m8B12ADEF72EE1FF063181890A6C04E2629E0B711 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<TriLibCore.Interfaces.IAnimationCurve>::.ctor(System.Int32)
inline void List_1__ctor_m050637C82E9575B3CB1DC7720A6432B96DE8F2A1 (List_1_t098836BD89E26112D46D0209C17F10F176C6D586* __this, int32_t ___0_capacity, const RuntimeMethod* method)
{
	((  void (*) (List_1_t098836BD89E26112D46D0209C17F10F176C6D586*, int32_t, const RuntimeMethod*))List_1__ctor_m76CBBC3E2F0583F5AD30CE592CEA1225C06A0428_gshared)(__this, ___0_capacity, method);
}
// System.String TriLibCore.Gltf.GltfTexture::get_Filename()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* GltfTexture_get_Filename_m79603B286978B911560B7E71FC6A7B6457DF6223_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478 (String_t* ___0_value, const RuntimeMethod* method) ;
// System.IO.Stream TriLibCore.Gltf.GltfTexture::get_DataStream()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* GltfTexture_get_DataStream_m78C51227548078591E510334983542E3FE55DED8_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline (const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<TriLibCore.Gltf.GltfTexture>::.ctor(System.Int32)
inline void List_1__ctor_mF2089CE11FD3E3DBF9541C4C1122B489E72BD4AB (List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33* __this, int32_t ___0_capacity, const RuntimeMethod* method)
{
	((  void (*) (List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33*, int32_t, const RuntimeMethod*))List_1__ctor_m76CBBC3E2F0583F5AD30CE592CEA1225C06A0428_gshared)(__this, ___0_capacity, method);
}
// System.Boolean TriLibCore.Utils.TextureComparators::TextureEquals(TriLibCore.Interfaces.ITexture,TriLibCore.Interfaces.ITexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TextureComparators_TextureEquals_m566CCA88570A7A060514DCAEF48AE3170D743679 (RuntimeObject* ___0_a, RuntimeObject* ___1_b, const RuntimeMethod* method) ;
// System.Boolean TriLibCore.Utils.TextureComparators::Equals(TriLibCore.Interfaces.ITexture,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TextureComparators_Equals_mA1D187553F7AC8EB27F3C8D0F2D1316C5E05E4AC (RuntimeObject* ___0_a, RuntimeObject* ___1_b, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Utils.TextureComparators::GetHashCode(TriLibCore.Interfaces.ITexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TextureComparators_GetHashCode_mF57C0A300F03E349E694DB594CA2FF73427BECA3 (RuntimeObject* ___0_a, const RuntimeMethod* method) ;
// UnityEngine.Vector2 TriLibCore.Gltf.GltfTexture::get_Tiling()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GltfTexture_get_Tiling_mFEAAB3E8149A9EFD97B1063F43646B07E58AF741_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m6F2E069A50E787D131261E5CB25FC9E03F95B5E1_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_lhs, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_rhs, const RuntimeMethod* method) ;
// UnityEngine.Vector2 TriLibCore.Gltf.GltfTexture::get_Offset()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GltfTexture_get_Offset_mFC4F6AA81B54647B0DB71DF46162B702BC511D98_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// UnityEngine.TextureWrapMode TriLibCore.Gltf.GltfTexture::get_WrapModeU()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t GltfTexture_get_WrapModeU_m7BF98972EE75BDD2C7B89A61354C8140938977BA_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// UnityEngine.TextureWrapMode TriLibCore.Gltf.GltfTexture::get_WrapModeV()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t GltfTexture_get_WrapModeV_mF4CBE4399CA34B7B6210DDAB1BC3BB97EA01FD83_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture__ctor_mC5C7D8F8CC9626BA0CF2987C054473396C752967 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// System.String TriLibCore.Gltf.GltfTexture::get_Name()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* GltfTexture_get_Name_m462A1B85755F017D16C4E1BDF14690B4F1D6CCC6_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_Name(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Name_m70BE7947472611A64DDDF01FC7E5A12587F4A60E_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_Filename(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Filename_mC031AA8746A2C2FBFC2691CD2993928B1FF0EEB8_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, String_t* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_DataStream(System.IO.Stream)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_DataStream_mCF4AA7BEC7E868BE8430F31B0D825DC9B44ECBC1_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_WrapModeU(UnityEngine.TextureWrapMode)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_WrapModeU_m81AD5018616FAF2FE7B7441DEEB0B9A4699649BD_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_WrapModeV(UnityEngine.TextureWrapMode)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_WrapModeV_m2453F4E391C58F881C3218172B058B3902D8F9C3_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Gltf.GltfTexture::get_TextureId()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t GltfTexture_get_TextureId_m1F53C4D9772FC0A4189FF212084C1AFB50E7DBC2_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_TextureId(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_TextureId_m41C5BC6BB42FFEEA88FFF8F36DABDF403F8495B7_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// TriLibCore.General.TextureFormat TriLibCore.Gltf.GltfTexture::get_TextureFormat()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t GltfTexture_get_TextureFormat_m3E443311277883CF97207657BE98095C605C90C0_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_TextureFormat(TriLibCore.General.TextureFormat)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_TextureFormat_mA70D678A60ED5664862252F855EA5F12E0358456_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_Index(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Index_mEBD01A47246DDFA65D63BFEA418E534CE168204B_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_Tiling(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Tiling_m024B66B808551A6DB90428782D94A629D7340989_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfTexture::set_Offset(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Offset_mC7C4FCA647FAA257CAC7CEE793884E534540DBEE_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) ;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57 (RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___0_handle, const RuntimeMethod* method) ;
// System.String System.String::CreateString(System.Char,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B (String_t* __this, Il2CppChar ___0_c, int32_t ___1_count, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.TemporaryString::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemporaryString__ctor_m53730179272D19FE464D2A9AB74E82337BF40C84 (TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7* __this, int32_t ___0_length, const RuntimeMethod* method) ;
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemSet(System.Void*,System.Byte,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_MemSet_m4CD74CD43260EB2962A46F57E0D93DD5C332FC2B (void* ___0_destination, uint8_t ___1_value, int64_t ___2_size, const RuntimeMethod* method) ;
// System.Int32 TriLibCore.Utils.JsonParser/JsonValue::CopyTo(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t JsonValue_CopyTo_m19C8979ABD0FF52FCE82585D7724A0AA5E6DFAED (JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___0_buffer, const RuntimeMethod* method) ;
// System.Int32 System.Runtime.CompilerServices.RuntimeHelpers::get_OffsetToStringData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RuntimeHelpers_get_OffsetToStringData_m90A5D27EF88BE9432BF7093B7D7E7A0ACB0A8FBD (const RuntimeMethod* method) ;
// System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemCpy(System.Void*,System.Void*,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177 (void* ___0_destination, void* ___1_source, int64_t ___2_size, const RuntimeMethod* method) ;
// System.String TriLibCore.Gltf.TemporaryString::GetString(TriLibCore.Utils.JsonParser/JsonValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TemporaryString_GetString_m13E0621057D7FE2FC58C8D3E4C8AE05DBAADF725 (TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7* __this, JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___0_value, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase__ctor_m5C4FE7A4BC205B65DAB56FF3CC5202D0B04937DA (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GtlfProcessor::.ctor(TriLibCore.Gltf.Reader.GltfReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GtlfProcessor__ctor_mFC99051F9A1BB0E858525318A04FA48214844070 (GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A* __this, GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC* ___0_reader, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.ReaderBase::ReadStream(System.IO.Stream,TriLibCore.AssetLoaderContext,System.String,System.Action`2<TriLibCore.AssetLoaderContext,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReaderBase_ReadStream_m725378DF096B29E0DB3BE3FB9E5F1E37747883F4 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, String_t* ___2_filename, Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___3_onProgress, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::SetupStream(System.IO.Stream&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_SetupStream_mCDC78453E3657CB3FBB713C40FB50B4941455942 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE** ___0_stream, const RuntimeMethod* method) ;
// TriLibCore.Interfaces.IRootModel TriLibCore.Gltf.GtlfProcessor::ParseModel(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GtlfProcessor_ParseModel_mB501D32A6D08024CF9CF3FC2C01D7A5053D36122 (GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, const RuntimeMethod* method) ;
// System.Void TriLibCore.ReaderBase::PostProcessModel(TriLibCore.Interfaces.IRootModel&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReaderBase_PostProcessModel_mD3BB953DE8BEB5FCA65077455BDA0750F740C667 (ReaderBase_tF4CA317DE26742ECCE011521FF46AC6E30980449* __this, RuntimeObject** ___0_model, const RuntimeMethod* method) ;
// System.Void TriLibCore.Gltf.GltfRootModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfRootModel__ctor_m96F355F5F103CF03D742288869F688E414E0C5CE (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, const RuntimeMethod* method) ;
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3 (String_t* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLibCore.Gltf.GltfModel::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GltfModel_get_Name_m44A2759A9531585217EB070E7BC5B2E12B03B375 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_Name_m9759886D1B9446C815E5E83EEA5E826711006AB6 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Gltf.GltfModel::get_Used()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfModel_get_Used_m5624C21E490B0BC1845A452E476B10AEFDE6EEFD (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CUsedU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_Used(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_Used_m558B426D62BC3117062AECB2FC27BBE23C5182F3 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CUsedU3Ek__BackingField_1 = L_0;
		return;
	}
}
// UnityEngine.Vector3 TriLibCore.Gltf.GltfModel::get_LocalPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GltfModel_get_LocalPosition_m0226FF0DA5FA36427DD272454DA10365DD64ED96 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___U3CLocalPositionU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_LocalPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_LocalPosition_m77A4A4F9C8AEED2810E24A0662814AF58EECF1F9 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->___U3CLocalPositionU3Ek__BackingField_2 = L_0;
		return;
	}
}
// UnityEngine.Quaternion TriLibCore.Gltf.GltfModel::get_LocalRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 GltfModel_get_LocalRotation_m9E05B0215974A1CA3C784E15769C54D9FEFC45D1 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = __this->___U3CLocalRotationU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_LocalRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_LocalRotation_m7563C98EA8BC6A2CC8CB3F5506E52C7D648A6CAD (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_value, const RuntimeMethod* method) 
{
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___0_value;
		__this->___U3CLocalRotationU3Ek__BackingField_3 = L_0;
		return;
	}
}
// UnityEngine.Vector3 TriLibCore.Gltf.GltfModel::get_LocalScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GltfModel_get_LocalScale_m5AD4BBDA83740FA1C19D5B092401EAAABFDD663D (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___U3CLocalScaleU3Ek__BackingField_4;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_LocalScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_LocalScale_m3B7956B3910519C3DC2E017D27D560AE29615BA0 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		__this->___U3CLocalScaleU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean TriLibCore.Gltf.GltfModel::get_Visibility()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfModel_get_Visibility_m08B91272D8113F4D5CD73467DAB16C515433A0F6 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CVisibilityU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_Visibility(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_Visibility_mB104FE40D13BF90FF6A1E2E91C79DE230972C684 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CVisibilityU3Ek__BackingField_5 = L_0;
		return;
	}
}
// TriLibCore.Interfaces.IModel TriLibCore.Gltf.GltfModel::get_Parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfModel_get_Parent_mF5F72F59E99F973AF93FFF62CFB03213E64120C9 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CParentU3Ek__BackingField_6;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_Parent(TriLibCore.Interfaces.IModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_Parent_m9085752628B0DBB1ACABCF53BBAF3A3678A3AD28 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CParentU3Ek__BackingField_6 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CParentU3Ek__BackingField_6), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Gltf.GltfModel::get_Children()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfModel_get_Children_m655F8116216113238EE8F72EEA782D61243B459B (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CChildrenU3Ek__BackingField_7;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_Children(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_Children_m1AECD8080C5A22BF51E87737F20DDD7085E11228 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CChildrenU3Ek__BackingField_7 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CChildrenU3Ek__BackingField_7), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Gltf.GltfModel::get_IsBone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfModel_get_IsBone_m920F0CAB5BC52D1CAEFE1DE0BB63CBB23831AF2C (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CIsBoneU3Ek__BackingField_8;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_IsBone(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_IsBone_mA0DB47DE0DE5F23DED67A9F40A0517FB808CC0EF (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CIsBoneU3Ek__BackingField_8 = L_0;
		return;
	}
}
// TriLibCore.Interfaces.IGeometryGroup TriLibCore.Gltf.GltfModel::get_GeometryGroup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfModel_get_GeometryGroup_m19B15675E2C846CCD99B0F28CC9FB19EC35EBE58 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CGeometryGroupU3Ek__BackingField_9;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_GeometryGroup(TriLibCore.Interfaces.IGeometryGroup)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_GeometryGroup_mF3EF4EC9BA894B672A08D886EE970BF8CF8B475D (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CGeometryGroupU3Ek__BackingField_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CGeometryGroupU3Ek__BackingField_9), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Gltf.GltfModel::get_Bones()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfModel_get_Bones_mC4CFEFACEF69E8B609D450BF7504C8AB59FDAF3F (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CBonesU3Ek__BackingField_10;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_Bones(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_Bones_mF41902CAF9FC7E4D8BFE9712C137EB9557314F40 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CBonesU3Ek__BackingField_10 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CBonesU3Ek__BackingField_10), (void*)L_0);
		return;
	}
}
// UnityEngine.Matrix4x4[] TriLibCore.Gltf.GltfModel::get_BindPoses()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* GltfModel_get_BindPoses_m5D71ACDFC704EDAA0AB5B1D65608F887AF657C88 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_0 = __this->___U3CBindPosesU3Ek__BackingField_11;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_BindPoses(UnityEngine.Matrix4x4[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_BindPoses_m0E108ECF228CCF68CC24212AD91800C292D92586 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___0_value, const RuntimeMethod* method) 
{
	{
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_0 = ___0_value;
		__this->___U3CBindPosesU3Ek__BackingField_11 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CBindPosesU3Ek__BackingField_11), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Gltf.GltfModel::get_MaterialIndices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfModel_get_MaterialIndices_m8A98A3AAE8799B126E1DD6C4B01970FEA974B73E (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CMaterialIndicesU3Ek__BackingField_12;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_MaterialIndices(System.Collections.Generic.IList`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_MaterialIndices_m440B5521AC5FCBFB425C73381157D0D058D1CC2C (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CMaterialIndicesU3Ek__BackingField_12 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CMaterialIndicesU3Ek__BackingField_12), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> TriLibCore.Gltf.GltfModel::get_UserProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* GltfModel_get_UserProperties_m3952CB551AA6EBFE1B3ED2E8EF211EBD78297679 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_0 = __this->___U3CUserPropertiesU3Ek__BackingField_13;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::set_UserProperties(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel_set_UserProperties_m6DC89D3F1CF57EAFEAE61348655C083E31EFD02C (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___0_value, const RuntimeMethod* method) 
{
	{
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_0 = ___0_value;
		__this->___U3CUserPropertiesU3Ek__BackingField_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CUserPropertiesU3Ek__BackingField_13), (void*)L_0);
		return;
	}
}
// System.String TriLibCore.Gltf.GltfModel::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GltfModel_ToString_mAB566101224F8FBEEA984A42DF19B3083233FD04 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0;
		L_0 = GltfModel_get_Name_m44A2759A9531585217EB070E7BC5B2E12B03B375_inline(__this, NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 TriLibCore.Gltf.GltfModel::GetLocalMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GltfModel_GetLocalMatrix_mABE9D3C3492A5046647BC2DFA23E21F83D97A2F5 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = GltfModel_get_LocalPosition_m0226FF0DA5FA36427DD272454DA10365DD64ED96_inline(__this, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1;
		L_1 = GltfModel_get_LocalRotation_m9E05B0215974A1CA3C784E15769C54D9FEFC45D1_inline(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = GltfModel_get_LocalScale_m5AD4BBDA83740FA1C19D5B092401EAAABFDD663D_inline(__this, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_3;
		L_3 = Matrix4x4_TRS_mCC04FD47347234B451ACC6CCD2CE6D02E1E0E1E3(L_0, L_1, L_2, NULL);
		return L_3;
	}
}
// UnityEngine.Matrix4x4 TriLibCore.Gltf.GltfModel::GetGlobalMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GltfModel_GetGlobalMatrix_mCCA7530F45F4912B2D9D55209EB7EC13FAE0A09E (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* V_1 = NULL;
	{
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0;
		L_0 = VirtualFuncInvoker0< Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 >::Invoke(32 /* UnityEngine.Matrix4x4 TriLibCore.Gltf.GltfModel::GetLocalMatrix() */, __this);
		V_0 = L_0;
		RuntimeObject* L_1;
		L_1 = GltfModel_get_Parent_mF5F72F59E99F973AF93FFF62CFB03213E64120C9_inline(__this, NULL);
		V_1 = ((GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2*)IsInstClass((RuntimeObject*)L_1, GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2_il2cpp_TypeInfo_var));
		goto IL_002e;
	}

IL_0015:
	{
		GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* L_2 = V_1;
		NullCheck(L_2);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_3;
		L_3 = VirtualFuncInvoker0< Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 >::Invoke(32 /* UnityEngine.Matrix4x4 TriLibCore.Gltf.GltfModel::GetLocalMatrix() */, L_2);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_4 = V_0;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_5;
		L_5 = Matrix4x4_op_Multiply_m75E91775655DCA8DFC8EDE0AB787285BB3935162(L_3, L_4, NULL);
		V_0 = L_5;
		GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* L_6 = V_1;
		NullCheck(L_6);
		RuntimeObject* L_7;
		L_7 = GltfModel_get_Parent_mF5F72F59E99F973AF93FFF62CFB03213E64120C9_inline(L_6, NULL);
		V_1 = ((GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2*)IsInstClass((RuntimeObject*)L_7, GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2_il2cpp_TypeInfo_var));
	}

IL_002e:
	{
		GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* L_8 = V_1;
		if (L_8)
		{
			goto IL_0015;
		}
	}
	{
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_9 = V_0;
		return L_9;
	}
}
// System.Void TriLibCore.Gltf.GltfModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfModel__ctor_m8B12ADEF72EE1FF063181890A6C04E2629E0B711 (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mB381575D9E7BA6706F57C22D567610F51B5CD868_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_0 = (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*)il2cpp_codegen_object_new(Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Dictionary_2__ctor_mB381575D9E7BA6706F57C22D567610F51B5CD868(L_0, 2, Dictionary_2__ctor_mB381575D9E7BA6706F57C22D567610F51B5CD868_RuntimeMethod_var);
		__this->___U3CUserPropertiesU3Ek__BackingField_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CUserPropertiesU3Ek__BackingField_13), (void*)L_0);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLibCore.Gltf.GltfAnimation::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GltfAnimation_get_Name_mB6CC64BC3B0BF5BE076474DC79049473DA8625C0 (GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimation::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimation_set_Name_mFA07DB861A0DC03E59C445BF5EA0D31AB732A62A (GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Gltf.GltfAnimation::get_Used()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfAnimation_get_Used_mD8A5AB7A594401DF23E8F45FC24E874F10534D7A (GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CUsedU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimation::set_Used(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimation_set_Used_m18DA58CB94101ADD348B6BADD684162939B9EB27 (GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CUsedU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimationCurveBinding> TriLibCore.Gltf.GltfAnimation::get_AnimationCurveBindings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfAnimation_get_AnimationCurveBindings_m55FBB5F3A009E7CA2C34563DEBB260C62768850E (GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAnimationCurveBindingsU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimation::set_AnimationCurveBindings(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimationCurveBinding>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimation_set_AnimationCurveBindings_m9846DB79E6AA91A7036271DE96516BC5BDC30E25 (GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAnimationCurveBindingsU3Ek__BackingField_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAnimationCurveBindingsU3Ek__BackingField_2), (void*)L_0);
		return;
	}
}
// System.Single TriLibCore.Gltf.GltfAnimation::get_FrameRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GltfAnimation_get_FrameRate_m2CFD4879A49A5FD166ADD947C8AF81D6AA79E832 (GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CFrameRateU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimation::set_FrameRate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimation_set_FrameRate_mF4739D3E40775D82AF85B456C0F8CB7F58B0F785 (GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		__this->___U3CFrameRateU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimation__ctor_mB878FBDBBAD4C68A48B6F9C7EC0A7D558498A677 (GltfAnimation_t5FCB3628EAE37CD7B330DFB088483BEAE15A8BDA* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TriLibCore.Gltf.GltfBlendShapeKey::get_IndexMap()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* GltfBlendShapeKey_get_IndexMap_m8EFA17939DB083500D8852743E9361D4EF0F9BAA (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* L_0 = __this->___U3CIndexMapU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::set_IndexMap(System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey_set_IndexMap_m0687AE18630E97499C33A34D30F9A466045D2B37 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* ___0_value, const RuntimeMethod* method) 
{
	{
		Dictionary_2_tABE19B9C5C52F1DE14F0D3287B2696E7D7419180* L_0 = ___0_value;
		__this->___U3CIndexMapU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CIndexMapU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Gltf.GltfBlendShapeKey::get_Vertices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfBlendShapeKey_get_Vertices_m05D2EE843DDAC9978763830B86EDC20506DB0DE5 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CVerticesU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::set_Vertices(System.Collections.Generic.IList`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey_set_Vertices_mADCE80ED4FE5796FB3AFBB8ACA5A1E4D947176B7 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CVerticesU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CVerticesU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Gltf.GltfBlendShapeKey::get_Normals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfBlendShapeKey_get_Normals_mF780E85045E7528C89D2A22094682F3982B677A7 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CNormalsU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::set_Normals(System.Collections.Generic.IList`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey_set_Normals_mDB66CD51CF8C1D4CCE450AA5AD6BD699AB994E75 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CNormalsU3Ek__BackingField_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNormalsU3Ek__BackingField_2), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.Vector3> TriLibCore.Gltf.GltfBlendShapeKey::get_Tangents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfBlendShapeKey_get_Tangents_m758343297885173B8ADCDED7D2E455ED2AAD9D1E (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CTangentsU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::set_Tangents(System.Collections.Generic.IList`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey_set_Tangents_m004EED846794E183C39C7AED66C1400FDF1E3A8E (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CTangentsU3Ek__BackingField_3 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CTangentsU3Ek__BackingField_3), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<System.Int32> TriLibCore.Gltf.GltfBlendShapeKey::get_Indices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfBlendShapeKey_get_Indices_mFA8D7B2B1396B7B4B75158F34D98400D3AFF65FF (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CIndicesU3Ek__BackingField_4;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::set_Indices(System.Collections.Generic.IList`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey_set_Indices_m6071A9E857B550DC0AE20A5C4C450562963104A3 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CIndicesU3Ek__BackingField_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CIndicesU3Ek__BackingField_4), (void*)L_0);
		return;
	}
}
// System.Single TriLibCore.Gltf.GltfBlendShapeKey::get_FrameWeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GltfBlendShapeKey_get_FrameWeight_mA4FE29F50025C6509192EA2AB3B3CACFE91068ED (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CFrameWeightU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::set_FrameWeight(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey_set_FrameWeight_m12FCED3ABB5FE040B8D6753CD9A38E2C31B90D9D (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		__this->___U3CFrameWeightU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Boolean TriLibCore.Gltf.GltfBlendShapeKey::get_FullGeometryShape()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfBlendShapeKey_get_FullGeometryShape_m02CC55662621FC5BC24FCE016E50E8F3BFBB1C47 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CFullGeometryShapeU3Ek__BackingField_6;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::set_FullGeometryShape(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey_set_FullGeometryShape_m436E0233BAAE2F224493C944801355645E759936 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CFullGeometryShapeU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String TriLibCore.Gltf.GltfBlendShapeKey::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GltfBlendShapeKey_get_Name_m7697ED11F04A16AB1CB1846B89A00B774319A5BB (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_7;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey_set_Name_mEB1DFCF256D8D4A59B648D5F2B29BA4F97E5312C (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_7 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_7), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Gltf.GltfBlendShapeKey::get_Used()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfBlendShapeKey_get_Used_m328882C196AE52146393C35997F1A9F10B3D2E6F (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CUsedU3Ek__BackingField_8;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::set_Used(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey_set_Used_m1B7E2FC52E12F365DF82019BA63E39E0D9174CE6 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CUsedU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.Void TriLibCore.Gltf.GltfBlendShapeKey::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfBlendShapeKey__ctor_m632BE487754CDF7DACBBC4603099A531A5DB1018 (GltfBlendShapeKey_t80BF2EA6D83A62C9C867162E87B03B9577AF5C01* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel> TriLibCore.Gltf.GltfRootModel::get_AllModels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfRootModel_get_AllModels_mF386B800B752FCBEBDBCA436F69680A1EB086A35 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllModelsU3Ek__BackingField_14;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfRootModel::set_AllModels(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IModel>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfRootModel_set_AllModels_mA59FAAA3786F339E9B7C466ED0EDD5E905877B16 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllModelsU3Ek__BackingField_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllModelsU3Ek__BackingField_14), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup> TriLibCore.Gltf.GltfRootModel::get_AllGeometryGroups()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfRootModel_get_AllGeometryGroups_mF47CAFDB29ABCD2A0079E26A824362F632285394 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllGeometryGroupsU3Ek__BackingField_15;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfRootModel::set_AllGeometryGroups(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IGeometryGroup>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfRootModel_set_AllGeometryGroups_mA2598A11ACE3D10F0166374C0AEC8F8CC65C9731 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllGeometryGroupsU3Ek__BackingField_15 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllGeometryGroupsU3Ek__BackingField_15), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation> TriLibCore.Gltf.GltfRootModel::get_AllAnimations()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfRootModel_get_AllAnimations_m405C1180695EB4242BA7DC861DE159CEA408AD85 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllAnimationsU3Ek__BackingField_16;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfRootModel::set_AllAnimations(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfRootModel_set_AllAnimations_mBC336819DA8732F08840DDB3C0F959C2F947AD14 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllAnimationsU3Ek__BackingField_16 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllAnimationsU3Ek__BackingField_16), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial> TriLibCore.Gltf.GltfRootModel::get_AllMaterials()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfRootModel_get_AllMaterials_mEF94EC232849AD8C999F267CB4DFE3C418CFECF5 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllMaterialsU3Ek__BackingField_17;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfRootModel::set_AllMaterials(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IMaterial>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfRootModel_set_AllMaterials_m037E9568BEB219D7FC38DC993904148EE7052EAE (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllMaterialsU3Ek__BackingField_17 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllMaterialsU3Ek__BackingField_17), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture> TriLibCore.Gltf.GltfRootModel::get_AllTextures()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfRootModel_get_AllTextures_mDE913F4B02B61421FDA2C0B139AC7D627301ECE9 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllTexturesU3Ek__BackingField_18;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfRootModel::set_AllTextures(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfRootModel_set_AllTextures_m5217932B980A9D4E2542435198C9AECCB47E9DDF (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllTexturesU3Ek__BackingField_18 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllTexturesU3Ek__BackingField_18), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera> TriLibCore.Gltf.GltfRootModel::get_AllCameras()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfRootModel_get_AllCameras_mC3885AE6569376A8E4EE7C82ACC630BAC8C5926E (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllCamerasU3Ek__BackingField_19;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfRootModel::set_AllCameras(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ICamera>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfRootModel_set_AllCameras_m909C7815EAAADA9B1B103EEC3F9A138458D4CC92 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllCamerasU3Ek__BackingField_19 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllCamerasU3Ek__BackingField_19), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight> TriLibCore.Gltf.GltfRootModel::get_AllLights()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfRootModel_get_AllLights_m4E16578EA9F6A45D63A9F80409E39ACA8FFD93DD (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAllLightsU3Ek__BackingField_20;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfRootModel::set_AllLights(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ILight>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfRootModel_set_AllLights_mA65DEA975E7046A658C821E8B7605D95D9342829 (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAllLightsU3Ek__BackingField_20 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAllLightsU3Ek__BackingField_20), (void*)L_0);
		return;
	}
}
// System.Void TriLibCore.Gltf.GltfRootModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfRootModel__ctor_m96F355F5F103CF03D742288869F688E414E0C5CE (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* __this, const RuntimeMethod* method) 
{
	{
		GltfModel__ctor_m8B12ADEF72EE1FF063181890A6C04E2629E0B711(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// TriLibCore.Interfaces.IModel TriLibCore.Gltf.GltfAnimationCurveBinding::get_Model()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfAnimationCurveBinding_get_Model_m524DB53301589C6675817D60DFBFD48C349C76B8 (GltfAnimationCurveBinding_t92A27388709EF32E3FCC6565B2E2804A5106BE6F* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CModelU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimationCurveBinding::set_Model(TriLibCore.Interfaces.IModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimationCurveBinding_set_Model_m2052FFDBC5A0204D0DA01DAF0F7AB3E2321051C9 (GltfAnimationCurveBinding_t92A27388709EF32E3FCC6565B2E2804A5106BE6F* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CModelU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CModelU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimationCurve> TriLibCore.Gltf.GltfAnimationCurveBinding::get_AnimationCurves()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfAnimationCurveBinding_get_AnimationCurves_m6592E3C44153438D191D81C269CBAC07213B1D4C (GltfAnimationCurveBinding_t92A27388709EF32E3FCC6565B2E2804A5106BE6F* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CAnimationCurvesU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimationCurveBinding::set_AnimationCurves(System.Collections.Generic.IList`1<TriLibCore.Interfaces.IAnimationCurve>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimationCurveBinding_set_AnimationCurves_m7EC7C7286B455E4A74CAC7FC94878BD023508B6D (GltfAnimationCurveBinding_t92A27388709EF32E3FCC6565B2E2804A5106BE6F* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_value;
		__this->___U3CAnimationCurvesU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAnimationCurvesU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimationCurveBinding::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimationCurveBinding__ctor_m5763257157DFC2C626FD107E22BEAC908291C0D1 (GltfAnimationCurveBinding_t92A27388709EF32E3FCC6565B2E2804A5106BE6F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m050637C82E9575B3CB1DC7720A6432B96DE8F2A1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t098836BD89E26112D46D0209C17F10F176C6D586_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t098836BD89E26112D46D0209C17F10F176C6D586* L_0 = (List_1_t098836BD89E26112D46D0209C17F10F176C6D586*)il2cpp_codegen_object_new(List_1_t098836BD89E26112D46D0209C17F10F176C6D586_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_m050637C82E9575B3CB1DC7720A6432B96DE8F2A1(L_0, 1, List_1__ctor_m050637C82E9575B3CB1DC7720A6432B96DE8F2A1_RuntimeMethod_var);
		__this->___U3CAnimationCurvesU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAnimationCurvesU3Ek__BackingField_1), (void*)L_0);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLibCore.Gltf.GltfTexture::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GltfTexture_get_Name_m462A1B85755F017D16C4E1BDF14690B4F1D6CCC6 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_Name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_Name_m70BE7947472611A64DDDF01FC7E5A12587F4A60E (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Boolean TriLibCore.Gltf.GltfTexture::get_Used()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfTexture_get_Used_mDDDC2B3F19BF809DE4FF8C2E82E306927E63EAD7 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CUsedU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_Used(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_Used_m97E767BD293C544BE61EA6DF552B5CE106414D2B (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CUsedU3Ek__BackingField_1 = L_0;
		return;
	}
}
// TriLibCore.Interfaces.ITexture TriLibCore.Gltf.GltfTexture::GetSubTexture(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfTexture_GetSubTexture_m3B0DF169EF4222E3FF5A906839AF91AD5B2B6440 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		return __this;
	}
}
// System.Int32 TriLibCore.Gltf.GltfTexture::GetSubTextureCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GltfTexture_GetSubTextureCount_m9E127A35A4942966BE640381C337B510B3F99234 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		return 0;
	}
}
// System.Single TriLibCore.Gltf.GltfTexture::GetWeight(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GltfTexture_GetWeight_m79D8B4182F48DD098B027665A07F70F0E7B170A5 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	{
		return (0.0f);
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::AddTexture(TriLibCore.Interfaces.ITexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_AddTexture_m17AB7C4241C1CFCD8C3F2A944C23292D8B47F7BA (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, RuntimeObject* ___0_texture, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Byte[] TriLibCore.Gltf.GltfTexture::get_Data()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* GltfTexture_get_Data_mA55436B8A10C5DEBF90123DE263F0B331F71CA61 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = __this->___U3CDataU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_Data(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_Data_mF789E54CE306CF963F969C98A56AE40287EB925F (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___0_value, const RuntimeMethod* method) 
{
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___0_value;
		__this->___U3CDataU3Ek__BackingField_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CDataU3Ek__BackingField_2), (void*)L_0);
		return;
	}
}
// System.IO.Stream TriLibCore.Gltf.GltfTexture::get_DataStream()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* GltfTexture_get_DataStream_m78C51227548078591E510334983542E3FE55DED8 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = __this->___U3CDataStreamU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_DataStream(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_DataStream_mCF4AA7BEC7E868BE8430F31B0D825DC9B44ECBC1 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_value, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_value;
		__this->___U3CDataStreamU3Ek__BackingField_3 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CDataStreamU3Ek__BackingField_3), (void*)L_0);
		return;
	}
}
// System.String TriLibCore.Gltf.GltfTexture::get_Filename()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GltfTexture_get_Filename_m79603B286978B911560B7E71FC6A7B6457DF6223 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CFilenameU3Ek__BackingField_4;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_Filename(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_Filename_mC031AA8746A2C2FBFC2691CD2993928B1FF0EEB8 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CFilenameU3Ek__BackingField_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CFilenameU3Ek__BackingField_4), (void*)L_0);
		return;
	}
}
// UnityEngine.TextureWrapMode TriLibCore.Gltf.GltfTexture::get_WrapModeU()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GltfTexture_get_WrapModeU_m7BF98972EE75BDD2C7B89A61354C8140938977BA (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CWrapModeUU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_WrapModeU(UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_WrapModeU_m81AD5018616FAF2FE7B7441DEEB0B9A4699649BD (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CWrapModeUU3Ek__BackingField_5 = L_0;
		return;
	}
}
// UnityEngine.TextureWrapMode TriLibCore.Gltf.GltfTexture::get_WrapModeV()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GltfTexture_get_WrapModeV_mF4CBE4399CA34B7B6210DDAB1BC3BB97EA01FD83 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CWrapModeVU3Ek__BackingField_6;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_WrapModeV(UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_WrapModeV_m2453F4E391C58F881C3218172B058B3902D8F9C3 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CWrapModeVU3Ek__BackingField_6 = L_0;
		return;
	}
}
// UnityEngine.Vector2 TriLibCore.Gltf.GltfTexture::get_Tiling()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GltfTexture_get_Tiling_mFEAAB3E8149A9EFD97B1063F43646B07E58AF741 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___U3CTilingU3Ek__BackingField_7;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_Tiling(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_Tiling_m024B66B808551A6DB90428782D94A629D7340989 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_value;
		__this->___U3CTilingU3Ek__BackingField_7 = L_0;
		return;
	}
}
// UnityEngine.Vector2 TriLibCore.Gltf.GltfTexture::get_Offset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GltfTexture_get_Offset_mFC4F6AA81B54647B0DB71DF46162B702BC511D98 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___U3COffsetU3Ek__BackingField_8;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_Offset(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_Offset_mC7C4FCA647FAA257CAC7CEE793884E534540DBEE (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_value;
		__this->___U3COffsetU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.Int32 TriLibCore.Gltf.GltfTexture::get_TextureId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GltfTexture_get_TextureId_m1F53C4D9772FC0A4189FF212084C1AFB50E7DBC2 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CTextureIdU3Ek__BackingField_9;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_TextureId(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_TextureId_m41C5BC6BB42FFEEA88FFF8F36DABDF403F8495B7 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CTextureIdU3Ek__BackingField_9 = L_0;
		return;
	}
}
// System.String TriLibCore.Gltf.GltfTexture::get_ResolvedFilename()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GltfTexture_get_ResolvedFilename_m438E10EB869EC7B6C304FEA1F0BB33BF10E10BDD (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CResolvedFilenameU3Ek__BackingField_10;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_ResolvedFilename(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_ResolvedFilename_m7EB7E162FCA74618F1E52784E68CB54C3F4F056E (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CResolvedFilenameU3Ek__BackingField_10 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CResolvedFilenameU3Ek__BackingField_10), (void*)L_0);
		return;
	}
}
// System.Int32 TriLibCore.Gltf.GltfTexture::get_Index()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GltfTexture_get_Index_m9F2A26ABA4F7A68FF509A8AE7FDE911FDFC4B274 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CIndexU3Ek__BackingField_11;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_Index(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_Index_mEBD01A47246DDFA65D63BFEA418E534CE168204B (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CIndexU3Ek__BackingField_11 = L_0;
		return;
	}
}
// System.Boolean TriLibCore.Gltf.GltfTexture::get_IsValid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfTexture_get_IsValid_mCCC0DD7CFA9C9B1C67A4AEF195AFE71EE6033046 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0;
		L_0 = GltfTexture_get_Filename_m79603B286978B911560B7E71FC6A7B6457DF6223_inline(__this, NULL);
		bool L_1;
		L_1 = String_IsNullOrEmpty_mEA9E3FB005AC28FE02E69FCF95A7B8456192B478(L_0, NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_2;
		L_2 = GltfTexture_get_DataStream_m78C51227548078591E510334983542E3FE55DED8_inline(__this, NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_3;
		L_3 = GltfTexture_get_DataStream_m78C51227548078591E510334983542E3FE55DED8_inline(__this, NULL);
		NullCheck(L_3);
		int64_t L_4;
		L_4 = VirtualFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Length() */, L_3);
		return (bool)((((int64_t)L_4) > ((int64_t)((int64_t)0)))? 1 : 0);
	}

IL_0025:
	{
		return (bool)0;
	}

IL_0027:
	{
		return (bool)1;
	}
}
// System.Boolean TriLibCore.Gltf.GltfTexture::get_HasAlpha()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfTexture_get_HasAlpha_mB661FD90349B5009F2FAFB7A35611108F46108F3 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___U3CHasAlphaU3Ek__BackingField_12;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_HasAlpha(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_HasAlpha_mEBA232933276122DFC562EF65742FCB95839D6F2 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___U3CHasAlphaU3Ek__BackingField_12 = L_0;
		return;
	}
}
// TriLibCore.General.TextureFormat TriLibCore.Gltf.GltfTexture::get_TextureFormat()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GltfTexture_get_TextureFormat_m3E443311277883CF97207657BE98095C605C90C0 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CTextureFormatU3Ek__BackingField_13;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::set_TextureFormat(TriLibCore.General.TextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture_set_TextureFormat_mA70D678A60ED5664862252F855EA5F12E0358456 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CTextureFormatU3Ek__BackingField_13 = L_0;
		return;
	}
}
// System.Void TriLibCore.Gltf.GltfTexture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfTexture__ctor_mC5C7D8F8CC9626BA0CF2987C054473396C752967 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_1_t197C7901E591B451319A83E16C668AD8CC21DFFA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF2089CE11FD3E3DBF9541C4C1122B489E72BD4AB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		L_0 = Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline(NULL);
		__this->___U3CTilingU3Ek__BackingField_7 = L_0;
		List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33* L_1 = (List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33*)il2cpp_codegen_object_new(List_1_t606D24ECFC7D63276BAC4BC062DC93AAECC0EB33_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_mF2089CE11FD3E3DBF9541C4C1122B489E72BD4AB(L_1, 1, List_1__ctor_mF2089CE11FD3E3DBF9541C4C1122B489E72BD4AB_RuntimeMethod_var);
		__this->___TextureVariations_14 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___TextureVariations_14), (void*)L_1);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		RuntimeObject* L_2 = __this->___TextureVariations_14;
		NullCheck(L_2);
		InterfaceActionInvoker1< GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<TriLibCore.Gltf.GltfTexture>::Add(T) */, ICollection_1_t197C7901E591B451319A83E16C668AD8CC21DFFA_il2cpp_TypeInfo_var, L_2, __this);
		return;
	}
}
// System.Boolean TriLibCore.Gltf.GltfTexture::Equals(TriLibCore.Interfaces.ITexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfTexture_Equals_m7B3AAED47F59B8721CD316B68B031EF00D8BE1FC (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, RuntimeObject* ___0_other, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_other;
		bool L_1;
		L_1 = TextureComparators_TextureEquals_m566CCA88570A7A060514DCAEF48AE3170D743679(__this, L_0, NULL);
		return L_1;
	}
}
// System.Boolean TriLibCore.Gltf.GltfTexture::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GltfTexture_Equals_m54D7DDA1CB968F46C66086735A1A717D225759CB (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = ___0_obj;
		bool L_1;
		L_1 = TextureComparators_Equals_mA1D187553F7AC8EB27F3C8D0F2D1316C5E05E4AC(__this, L_0, NULL);
		return L_1;
	}
}
// System.Int32 TriLibCore.Gltf.GltfTexture::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GltfTexture_GetHashCode_m72C0145EB1B73B1BDF2685DB2F39293DE13F3DC0 (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0;
		L_0 = TextureComparators_GetHashCode_mF57C0A300F03E349E694DB594CA2FF73427BECA3(__this, NULL);
		return L_0;
	}
}
// TriLibCore.Gltf.GltfTexture TriLibCore.Gltf.GltfTexture::GetTextureWithTilingAndOffset(System.Collections.Generic.IList`1<TriLibCore.Interfaces.ITexture>,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.TextureWrapMode,UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* GltfTexture_GetTextureWithTilingAndOffset_m0DAF60F47E4E43DCB7E77EC6C198F59EC5F6199A (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, RuntimeObject* ___0_textures, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_textureTiling, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___2_textureOffset, int32_t ___3_wrapU, int32_t ___4_wrapV, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_1_t197C7901E591B451319A83E16C668AD8CC21DFFA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_1_t9D9853EC356A71B3BC036D2810F70EC0DF1361DD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_1_t8FA072AE304C235DECF9BBA777C475459F219E80_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* V_0 = NULL;
	int32_t V_1 = 0;
	GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* V_2 = NULL;
	{
		V_1 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		RuntimeObject* L_0 = __this->___TextureVariations_14;
		int32_t L_1 = V_1;
		NullCheck(L_0);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_2;
		L_2 = InterfaceFuncInvoker1< GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8*, int32_t >::Invoke(0 /* T System.Collections.Generic.IList`1<TriLibCore.Gltf.GltfTexture>::get_Item(System.Int32) */, IList_1_t8FA072AE304C235DECF9BBA777C475459F219E80_il2cpp_TypeInfo_var, L_0, L_1);
		V_2 = L_2;
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_3 = V_2;
		NullCheck(L_3);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		L_4 = GltfTexture_get_Tiling_mFEAAB3E8149A9EFD97B1063F43646B07E58AF741_inline(L_3, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = ___1_textureTiling;
		bool L_6;
		L_6 = Vector2_op_Equality_m6F2E069A50E787D131261E5CB25FC9E03F95B5E1_inline(L_4, L_5, NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_7 = V_2;
		NullCheck(L_7);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		L_8 = GltfTexture_get_Offset_mFC4F6AA81B54647B0DB71DF46162B702BC511D98_inline(L_7, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = ___2_textureOffset;
		bool L_10;
		L_10 = Vector2_op_Equality_m6F2E069A50E787D131261E5CB25FC9E03F95B5E1_inline(L_8, L_9, NULL);
		if (!L_10)
		{
			goto IL_0043;
		}
	}
	{
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_11 = V_2;
		NullCheck(L_11);
		int32_t L_12;
		L_12 = GltfTexture_get_WrapModeU_m7BF98972EE75BDD2C7B89A61354C8140938977BA_inline(L_11, NULL);
		int32_t L_13 = ___3_wrapU;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0043;
		}
	}
	{
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15;
		L_15 = GltfTexture_get_WrapModeV_mF4CBE4399CA34B7B6210DDAB1BC3BB97EA01FD83_inline(L_14, NULL);
		int32_t L_16 = ___4_wrapV;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0043;
		}
	}
	{
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_17 = V_2;
		return L_17;
	}

IL_0043:
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_18, 1));
	}

IL_0047:
	{
		int32_t L_19 = V_1;
		RuntimeObject* L_20 = __this->___TextureVariations_14;
		NullCheck(L_20);
		int32_t L_21;
		L_21 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<TriLibCore.Gltf.GltfTexture>::get_Count() */, ICollection_1_t197C7901E591B451319A83E16C668AD8CC21DFFA_il2cpp_TypeInfo_var, L_20);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0004;
		}
	}
	{
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_22 = (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8*)il2cpp_codegen_object_new(GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8_il2cpp_TypeInfo_var);
		NullCheck(L_22);
		GltfTexture__ctor_mC5C7D8F8CC9626BA0CF2987C054473396C752967(L_22, NULL);
		V_0 = L_22;
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_23 = V_0;
		String_t* L_24;
		L_24 = GltfTexture_get_Name_m462A1B85755F017D16C4E1BDF14690B4F1D6CCC6_inline(__this, NULL);
		NullCheck(L_23);
		GltfTexture_set_Name_m70BE7947472611A64DDDF01FC7E5A12587F4A60E_inline(L_23, L_24, NULL);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_25 = V_0;
		String_t* L_26;
		L_26 = GltfTexture_get_Filename_m79603B286978B911560B7E71FC6A7B6457DF6223_inline(__this, NULL);
		NullCheck(L_25);
		GltfTexture_set_Filename_mC031AA8746A2C2FBFC2691CD2993928B1FF0EEB8_inline(L_25, L_26, NULL);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_27 = V_0;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_28;
		L_28 = GltfTexture_get_DataStream_m78C51227548078591E510334983542E3FE55DED8_inline(__this, NULL);
		NullCheck(L_27);
		GltfTexture_set_DataStream_mCF4AA7BEC7E868BE8430F31B0D825DC9B44ECBC1_inline(L_27, L_28, NULL);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_29 = V_0;
		int32_t L_30;
		L_30 = GltfTexture_get_WrapModeU_m7BF98972EE75BDD2C7B89A61354C8140938977BA_inline(__this, NULL);
		NullCheck(L_29);
		GltfTexture_set_WrapModeU_m81AD5018616FAF2FE7B7441DEEB0B9A4699649BD_inline(L_29, L_30, NULL);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_31 = V_0;
		int32_t L_32;
		L_32 = GltfTexture_get_WrapModeV_mF4CBE4399CA34B7B6210DDAB1BC3BB97EA01FD83_inline(__this, NULL);
		NullCheck(L_31);
		GltfTexture_set_WrapModeV_m2453F4E391C58F881C3218172B058B3902D8F9C3_inline(L_31, L_32, NULL);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_33 = V_0;
		int32_t L_34;
		L_34 = GltfTexture_get_TextureId_m1F53C4D9772FC0A4189FF212084C1AFB50E7DBC2_inline(__this, NULL);
		NullCheck(L_33);
		GltfTexture_set_TextureId_m41C5BC6BB42FFEEA88FFF8F36DABDF403F8495B7_inline(L_33, L_34, NULL);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_35 = V_0;
		int32_t L_36;
		L_36 = GltfTexture_get_TextureFormat_m3E443311277883CF97207657BE98095C605C90C0_inline(__this, NULL);
		NullCheck(L_35);
		GltfTexture_set_TextureFormat_mA70D678A60ED5664862252F855EA5F12E0358456_inline(L_35, L_36, NULL);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_37 = V_0;
		RuntimeObject* L_38 = ___0_textures;
		NullCheck(L_38);
		int32_t L_39;
		L_39 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<TriLibCore.Interfaces.ITexture>::get_Count() */, ICollection_1_t9D9853EC356A71B3BC036D2810F70EC0DF1361DD_il2cpp_TypeInfo_var, L_38);
		NullCheck(L_37);
		GltfTexture_set_Index_mEBD01A47246DDFA65D63BFEA418E534CE168204B_inline(L_37, L_39, NULL);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_40 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_41 = ___1_textureTiling;
		NullCheck(L_40);
		GltfTexture_set_Tiling_m024B66B808551A6DB90428782D94A629D7340989_inline(L_40, L_41, NULL);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_42 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_43 = ___2_textureOffset;
		NullCheck(L_42);
		GltfTexture_set_Offset_mC7C4FCA647FAA257CAC7CEE793884E534540DBEE_inline(L_42, L_43, NULL);
		RuntimeObject* L_44 = __this->___TextureVariations_14;
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_45 = V_0;
		NullCheck(L_44);
		InterfaceActionInvoker1< GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<TriLibCore.Gltf.GltfTexture>::Add(T) */, ICollection_1_t197C7901E591B451319A83E16C668AD8CC21DFFA_il2cpp_TypeInfo_var, L_44, L_45);
		RuntimeObject* L_46 = ___0_textures;
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_47 = V_0;
		NullCheck(L_46);
		InterfaceActionInvoker1< RuntimeObject* >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<TriLibCore.Interfaces.ITexture>::Add(T) */, ICollection_1_t9D9853EC356A71B3BC036D2810F70EC0DF1361DD_il2cpp_TypeInfo_var, L_46, L_47);
		GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* L_48 = V_0;
		return L_48;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String TriLibCore.Gltf.GltfAnimationCurve::get_Property()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GltfAnimationCurve_get_Property_m37E01634BB4018CE37D7B9C35C3D6FC426F791E3 (GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CPropertyU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimationCurve::set_Property(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimationCurve_set_Property_mE2E5BAC3623DA3172E1AD4B2A03F6EBFEDD3D785 (GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CPropertyU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CPropertyU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.Type TriLibCore.Gltf.GltfAnimationCurve::get_AnimatedType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* GltfAnimationCurve_get_AnimatedType_m50174912E652DF5FD4772AB820072C71050F2C78 (GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65* __this, const RuntimeMethod* method) 
{
	{
		Type_t* L_0 = __this->___U3CAnimatedTypeU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimationCurve::set_AnimatedType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimationCurve_set_AnimatedType_m139863FB7E86892032207EF58E23912234D97F63 (GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65* __this, Type_t* ___0_value, const RuntimeMethod* method) 
{
	{
		Type_t* L_0 = ___0_value;
		__this->___U3CAnimatedTypeU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAnimatedTypeU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
// UnityEngine.AnimationCurve TriLibCore.Gltf.GltfAnimationCurve::get_AnimationCurve()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* GltfAnimationCurve_get_AnimationCurve_m3E75BE3ADB56E4853431D52C4C97B627D24D73CD (GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65* __this, const RuntimeMethod* method) 
{
	{
		AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* L_0 = __this->___U3CAnimationCurveU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimationCurve::set_AnimationCurve(UnityEngine.AnimationCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimationCurve_set_AnimationCurve_m8C752BFC2E3E3ADB809B4A18BF5B51B62D644456 (GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65* __this, AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* ___0_value, const RuntimeMethod* method) 
{
	{
		AnimationCurve_tCBFFAAD05CEBB35EF8D8631BD99914BE1A6BB354* L_0 = ___0_value;
		__this->___U3CAnimationCurveU3Ek__BackingField_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAnimationCurveU3Ek__BackingField_2), (void*)L_0);
		return;
	}
}
// TriLibCore.General.TangentMode TriLibCore.Gltf.GltfAnimationCurve::get_TangentMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GltfAnimationCurve_get_TangentMode_m46F78E04FF60FFA2622C632FECB5C6E577D7BB9C (GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CTangentModeU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimationCurve::set_TangentMode(TriLibCore.General.TangentMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimationCurve_set_TangentMode_mAD5A4810802F4B5CCDA43B79169A3458070BFA9A (GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CTangentModeU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Void TriLibCore.Gltf.GltfAnimationCurve::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfAnimationCurve__ctor_m93014DCBBCDF4026D14B35E518121439978C20BB (GltfAnimationCurve_t3A66AE2837EE789BFF3DF6E20A7F764F30356D65* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_0 = { reinterpret_cast<intptr_t> (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_1;
		L_1 = Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57(L_0, NULL);
		__this->___U3CAnimatedTypeU3Ek__BackingField_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CAnimatedTypeU3Ek__BackingField_1), (void*)L_1);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: TriLibCore.Gltf.TemporaryString
IL2CPP_EXTERN_C void TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshal_pinvoke(const TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7& unmarshaled, TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshaled_pinvoke& marshaled)
{
	if (unmarshaled.____chars_0 != NULL)
	{
		il2cpp_array_size_t _unmarshaled_chars_Length = (unmarshaled.____chars_0)->max_length;
		marshaled.____chars_0 = il2cpp_codegen_marshal_allocate_array<uint8_t>(_unmarshaled_chars_Length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_unmarshaled_chars_Length); i++)
		{
			(marshaled.____chars_0)[i] = static_cast<uint8_t>((unmarshaled.____chars_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		marshaled.____chars_0 = NULL;
	}
	marshaled.____charString_1 = il2cpp_codegen_marshal_string(unmarshaled.____charString_1);
	marshaled.____length_2 = unmarshaled.____length_2;
}
IL2CPP_EXTERN_C void TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshal_pinvoke_back(const TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshaled_pinvoke& marshaled, TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	if (marshaled.____chars_0 != NULL)
	{
		if (unmarshaled.____chars_0 == NULL)
		{
			unmarshaled.____chars_0 = reinterpret_cast<CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*>((CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, 1));
			Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.____chars_0), (void*)reinterpret_cast<CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*>((CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, 1)));
		}
		il2cpp_array_size_t _arrayLength = (unmarshaled.____chars_0)->max_length;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_arrayLength); i++)
		{
			(unmarshaled.____chars_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), static_cast<Il2CppChar>((marshaled.____chars_0)[i]));
		}
	}
	unmarshaled.____charString_1 = il2cpp_codegen_marshal_string_result(marshaled.____charString_1);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.____charString_1), (void*)il2cpp_codegen_marshal_string_result(marshaled.____charString_1));
	int32_t unmarshaled_length_temp_2 = 0;
	unmarshaled_length_temp_2 = marshaled.____length_2;
	unmarshaled.____length_2 = unmarshaled_length_temp_2;
}
// Conversion method for clean up from marshalling of: TriLibCore.Gltf.TemporaryString
IL2CPP_EXTERN_C void TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshal_pinvoke_cleanup(TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshaled_pinvoke& marshaled)
{
	if (marshaled.____chars_0 != NULL)
	{
		il2cpp_codegen_marshal_free(marshaled.____chars_0);
		marshaled.____chars_0 = NULL;
	}
	il2cpp_codegen_marshal_free(marshaled.____charString_1);
	marshaled.____charString_1 = NULL;
}
// Conversion methods for marshalling of: TriLibCore.Gltf.TemporaryString
IL2CPP_EXTERN_C void TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshal_com(const TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7& unmarshaled, TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshaled_com& marshaled)
{
	if (unmarshaled.____chars_0 != NULL)
	{
		il2cpp_array_size_t _unmarshaled_chars_Length = (unmarshaled.____chars_0)->max_length;
		marshaled.____chars_0 = il2cpp_codegen_marshal_allocate_array<uint8_t>(_unmarshaled_chars_Length);
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_unmarshaled_chars_Length); i++)
		{
			(marshaled.____chars_0)[i] = static_cast<uint8_t>((unmarshaled.____chars_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		marshaled.____chars_0 = NULL;
	}
	marshaled.____charString_1 = il2cpp_codegen_marshal_bstring(unmarshaled.____charString_1);
	marshaled.____length_2 = unmarshaled.____length_2;
}
IL2CPP_EXTERN_C void TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshal_com_back(const TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshaled_com& marshaled, TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	if (marshaled.____chars_0 != NULL)
	{
		if (unmarshaled.____chars_0 == NULL)
		{
			unmarshaled.____chars_0 = reinterpret_cast<CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*>((CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, 1));
			Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.____chars_0), (void*)reinterpret_cast<CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*>((CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, 1)));
		}
		il2cpp_array_size_t _arrayLength = (unmarshaled.____chars_0)->max_length;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(_arrayLength); i++)
		{
			(unmarshaled.____chars_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(i), static_cast<Il2CppChar>((marshaled.____chars_0)[i]));
		}
	}
	unmarshaled.____charString_1 = il2cpp_codegen_marshal_bstring_result(marshaled.____charString_1);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.____charString_1), (void*)il2cpp_codegen_marshal_bstring_result(marshaled.____charString_1));
	int32_t unmarshaled_length_temp_2 = 0;
	unmarshaled_length_temp_2 = marshaled.____length_2;
	unmarshaled.____length_2 = unmarshaled_length_temp_2;
}
// Conversion method for clean up from marshalling of: TriLibCore.Gltf.TemporaryString
IL2CPP_EXTERN_C void TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshal_com_cleanup(TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7_marshaled_com& marshaled)
{
	if (marshaled.____chars_0 != NULL)
	{
		il2cpp_codegen_marshal_free(marshaled.____chars_0);
		marshaled.____chars_0 = NULL;
	}
	il2cpp_codegen_marshal_free_bstring(marshaled.____charString_1);
	marshaled.____charString_1 = NULL;
}
// System.Void TriLibCore.Gltf.TemporaryString::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemporaryString__ctor_m53730179272D19FE464D2A9AB74E82337BF40C84 (TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7* __this, int32_t ___0_length, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___0_length;
		String_t* L_1;
		L_1 = String_CreateString_mAA0705B41B390BDB42F67894B9B67C956814C71B(NULL, 0, L_0, NULL);
		__this->____charString_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____charString_1), (void*)L_1);
		int32_t L_2 = ___0_length;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_3 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)L_2);
		__this->____chars_0 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____chars_0), (void*)L_3);
		int32_t L_4 = ___0_length;
		__this->____length_2 = L_4;
		return;
	}
}
IL2CPP_EXTERN_C  void TemporaryString__ctor_m53730179272D19FE464D2A9AB74E82337BF40C84_AdjustorThunk (RuntimeObject* __this, int32_t ___0_length, const RuntimeMethod* method)
{
	TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7*>(__this + _offset);
	TemporaryString__ctor_m53730179272D19FE464D2A9AB74E82337BF40C84(_thisAdjusted, ___0_length, method);
}
// System.String TriLibCore.Gltf.TemporaryString::GetString(TriLibCore.Utils.JsonParser/JsonValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TemporaryString_GetString_m13E0621057D7FE2FC58C8D3E4C8AE05DBAADF725 (TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7* __this, JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___0_value, const RuntimeMethod* method) 
{
	Il2CppChar* V_0 = NULL;
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* V_1 = NULL;
	Il2CppChar* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = __this->____chars_0;
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_1 = L_0;
		V_1 = L_1;
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_2 = V_1;
		NullCheck(L_2);
		if (((int32_t)(((RuntimeArray*)L_2)->max_length)))
		{
			goto IL_0014;
		}
	}

IL_000f:
	{
		V_0 = (Il2CppChar*)((uintptr_t)0);
		goto IL_001d;
	}

IL_0014:
	{
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_3 = V_1;
		NullCheck(L_3);
		V_0 = (Il2CppChar*)((uintptr_t)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
	}

IL_001d:
	{
		Il2CppChar* L_4 = V_0;
		int32_t L_5 = __this->____length_2;
		UnsafeUtility_MemSet_m4CD74CD43260EB2962A46F57E0D93DD5C332FC2B((void*)L_4, (uint8_t)0, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_5, 2))), NULL);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_6 = __this->____chars_0;
		int32_t L_7;
		L_7 = JsonValue_CopyTo_m19C8979ABD0FF52FCE82585D7724A0AA5E6DFAED((&___0_value), L_6, NULL);
		String_t* L_8 = __this->____charString_1;
		V_3 = L_8;
		String_t* L_9 = V_3;
		V_2 = (Il2CppChar*)((uintptr_t)L_9);
		Il2CppChar* L_10 = V_2;
		if (!L_10)
		{
			goto IL_0050;
		}
	}
	{
		Il2CppChar* L_11 = V_2;
		int32_t L_12;
		L_12 = RuntimeHelpers_get_OffsetToStringData_m90A5D27EF88BE9432BF7093B7D7E7A0ACB0A8FBD(NULL);
		V_2 = ((Il2CppChar*)il2cpp_codegen_add((intptr_t)L_11, L_12));
	}

IL_0050:
	{
		Il2CppChar* L_13 = V_2;
		Il2CppChar* L_14 = V_0;
		int32_t L_15 = __this->____length_2;
		UnsafeUtility_MemCpy_m5CEA91ACDADC522E584AE3A2AB2B0B74393A9177((void*)L_13, (void*)L_14, ((int64_t)((int32_t)il2cpp_codegen_multiply(L_15, 2))), NULL);
		V_3 = (String_t*)NULL;
		V_1 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)NULL;
		String_t* L_16 = __this->____charString_1;
		return L_16;
	}
}
IL2CPP_EXTERN_C  String_t* TemporaryString_GetString_m13E0621057D7FE2FC58C8D3E4C8AE05DBAADF725_AdjustorThunk (RuntimeObject* __this, JsonValue_t20C2533D6F55A7EA63C03D8149A46C268561F914 ___0_value, const RuntimeMethod* method)
{
	TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<TemporaryString_tD8F2669292664D80619D72E9A309F3A04B2096B7*>(__this + _offset);
	String_t* _returnValue;
	_returnValue = TemporaryString_GetString_m13E0621057D7FE2FC58C8D3E4C8AE05DBAADF725(_thisAdjusted, ___0_value, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String[] TriLibCore.Gltf.Reader.GltfReader::GetExtensions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* GltfReader_GetExtensions_mDDD9AF7AE0AA8264465360CB3995F783D7F58DC4 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral119B330A1FA1C051D7BE15A6ABD55D08AFF99689);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA18955232B8E3141B1616DF7C0C29A59B8A13EF3);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)2);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral119B330A1FA1C051D7BE15A6ABD55D08AFF99689);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral119B330A1FA1C051D7BE15A6ABD55D08AFF99689);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteralA18955232B8E3141B1616DF7C0C29A59B8A13EF3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralA18955232B8E3141B1616DF7C0C29A59B8A13EF3);
		return L_2;
	}
}
// System.Void TriLibCore.Gltf.Reader.GltfReader::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfReader__ctor_m9087A48AE5DF67D502B9DFEF07367B8251835A3B (GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReaderBase__ctor_m5C4FE7A4BC205B65DAB56FF3CC5202D0B04937DA(__this, NULL);
		GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A* L_0 = (GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A*)il2cpp_codegen_object_new(GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		GtlfProcessor__ctor_mFC99051F9A1BB0E858525318A04FA48214844070(L_0, __this, NULL);
		__this->____gtlfProcessor_11 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____gtlfProcessor_11), (void*)L_0);
		return;
	}
}
// System.String TriLibCore.Gltf.Reader.GltfReader::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GltfReader_get_Name_mEC8E40740500B0A5A64B64152830739EF8230AC3 (GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral80A749BBF097574027749577C42FF229E10C6B66);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral80A749BBF097574027749577C42FF229E10C6B66;
	}
}
// System.Type TriLibCore.Gltf.Reader.GltfReader::get_LoadingStepEnumType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* GltfReader_get_LoadingStepEnumType_m58735F7B749119178BF34BF4607BACDF4505C57F (GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProcessingSteps_t85CB655500227D688208A221C3E52FF54DC2B490_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_0 = { reinterpret_cast<intptr_t> (ProcessingSteps_t85CB655500227D688208A221C3E52FF54DC2B490_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_1;
		L_1 = Type_GetTypeFromHandle_m6062B81682F79A4D6DF2640692EE6D9987858C57(L_0, NULL);
		return L_1;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Gltf.Reader.GltfReader::ReadStream(System.IO.Stream,TriLibCore.AssetLoaderContext,System.String,System.Action`2<TriLibCore.AssetLoaderContext,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfReader_ReadStream_m7201712D5C1157A13E15E2D6AB9E7DF8125B4597 (GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_stream, AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* ___1_assetLoaderContext, String_t* ___2_filename, Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* ___3_onProgress, const RuntimeMethod* method) 
{
	RuntimeObject* V_0 = NULL;
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_stream;
		AssetLoaderContext_t94854AA4BCC0F8C2A92047DD2CC6BF830001DD2C* L_1 = ___1_assetLoaderContext;
		String_t* L_2 = ___2_filename;
		Action_2_t5A98318AA2335D7048A849A249280B64AD125DCD* L_3 = ___3_onProgress;
		RuntimeObject* L_4;
		L_4 = ReaderBase_ReadStream_m725378DF096B29E0DB3BE3FB9E5F1E37747883F4(__this, L_0, L_1, L_2, L_3, NULL);
		ReaderBase_SetupStream_mCDC78453E3657CB3FBB713C40FB50B4941455942(__this, (&___0_stream), NULL);
		GtlfProcessor_t9E6F312E0E6B67204245CAC0775677302FCCEF8A* L_5 = __this->____gtlfProcessor_11;
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_6 = ___0_stream;
		NullCheck(L_5);
		RuntimeObject* L_7;
		L_7 = GtlfProcessor_ParseModel_mB501D32A6D08024CF9CF3FC2C01D7A5053D36122(L_5, L_6, NULL);
		V_0 = L_7;
		ReaderBase_PostProcessModel_mD3BB953DE8BEB5FCA65077455BDA0750F740C667(__this, (&V_0), NULL);
		RuntimeObject* L_8 = V_0;
		return L_8;
	}
}
// TriLibCore.Interfaces.IRootModel TriLibCore.Gltf.Reader.GltfReader::CreateRootModel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GltfReader_CreateRootModel_mF5BEA162A470C694D1B34559349C1749A0D6FE8A (GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727* L_0 = (GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727*)il2cpp_codegen_object_new(GltfRootModel_t7D9C44CC5925392042E483F2A5768105B5EE3727_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		GltfRootModel__ctor_m96F355F5F103CF03D742288869F688E414E0C5CE(L_0, NULL);
		return L_0;
	}
}
// System.Void TriLibCore.Gltf.Reader.GltfReader::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GltfReader__cctor_m898B5A0E3C7EB5C465F4972F3C5F632C5FFADEB5 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC_StaticFields*)il2cpp_codegen_static_fields_for(GltfReader_t78F6ACB57FB631BD39DD220810333DD45D639DEC_il2cpp_TypeInfo_var))->___SpotLightDistance_10 = (999999.0f);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t U3CPrivateImplementationDetailsU3E_ComputeStringHash_mF98E565B75872FC9E3F2A26C6075D3D0F99DF5F1 (String_t* ___0_s, const RuntimeMethod* method) 
{
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___0_s;
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		V_0 = ((int32_t)-2128831035);
		V_1 = 0;
		goto IL_0021;
	}

IL_000d:
	{
		String_t* L_1 = ___0_s;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Il2CppChar L_3;
		L_3 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_1, L_2, NULL);
		uint32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_multiply(((int32_t)((int32_t)L_3^(int32_t)L_4)), ((int32_t)16777619)));
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0021:
	{
		int32_t L_6 = V_1;
		String_t* L_7 = ___0_s;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_7, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_000d;
		}
	}

IL_002a:
	{
		uint32_t L_9 = V_0;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* GltfModel_get_Name_m44A2759A9531585217EB070E7BC5B2E12B03B375_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GltfModel_get_LocalPosition_m0226FF0DA5FA36427DD272454DA10365DD64ED96_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___U3CLocalPositionU3Ek__BackingField_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 GltfModel_get_LocalRotation_m9E05B0215974A1CA3C784E15769C54D9FEFC45D1_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = __this->___U3CLocalRotationU3Ek__BackingField_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GltfModel_get_LocalScale_m5AD4BBDA83740FA1C19D5B092401EAAABFDD663D_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->___U3CLocalScaleU3Ek__BackingField_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* GltfModel_get_Parent_mF5F72F59E99F973AF93FFF62CFB03213E64120C9_inline (GltfModel_t7E6F18A9288B4327BA2168BB381893C5E40FB6A2* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CParentU3Ek__BackingField_6;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* GltfTexture_get_Filename_m79603B286978B911560B7E71FC6A7B6457DF6223_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CFilenameU3Ek__BackingField_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* GltfTexture_get_DataStream_m78C51227548078591E510334983542E3FE55DED8_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = __this->___U3CDataStreamU3Ek__BackingField_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_one_m9097EB8DC23C26118A591AF16702796C3EF51DFB_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___oneVector_3;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GltfTexture_get_Tiling_mFEAAB3E8149A9EFD97B1063F43646B07E58AF741_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___U3CTilingU3Ek__BackingField_7;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m6F2E069A50E787D131261E5CB25FC9E03F95B5E1_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_lhs, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_rhs, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_lhs;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___1_rhs;
		float L_3 = L_2.___x_0;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___0_lhs;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___1_rhs;
		float L_7 = L_6.___y_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		float L_8 = V_0;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_1;
		V_2 = (bool)((((float)((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_8, L_9)), ((float)il2cpp_codegen_multiply(L_10, L_11))))) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_002e;
	}

IL_002e:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 GltfTexture_get_Offset_mFC4F6AA81B54647B0DB71DF46162B702BC511D98_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___U3COffsetU3Ek__BackingField_8;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t GltfTexture_get_WrapModeU_m7BF98972EE75BDD2C7B89A61354C8140938977BA_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CWrapModeUU3Ek__BackingField_5;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t GltfTexture_get_WrapModeV_mF4CBE4399CA34B7B6210DDAB1BC3BB97EA01FD83_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CWrapModeVU3Ek__BackingField_6;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* GltfTexture_get_Name_m462A1B85755F017D16C4E1BDF14690B4F1D6CCC6_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CNameU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Name_m70BE7947472611A64DDDF01FC7E5A12587F4A60E_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CNameU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Filename_mC031AA8746A2C2FBFC2691CD2993928B1FF0EEB8_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___0_value;
		__this->___U3CFilenameU3Ek__BackingField_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CFilenameU3Ek__BackingField_4), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_DataStream_mCF4AA7BEC7E868BE8430F31B0D825DC9B44ECBC1_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* ___0_value, const RuntimeMethod* method) 
{
	{
		Stream_tF844051B786E8F7F4244DBD218D74E8617B9A2DE* L_0 = ___0_value;
		__this->___U3CDataStreamU3Ek__BackingField_3 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CDataStreamU3Ek__BackingField_3), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_WrapModeU_m81AD5018616FAF2FE7B7441DEEB0B9A4699649BD_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CWrapModeUU3Ek__BackingField_5 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_WrapModeV_m2453F4E391C58F881C3218172B058B3902D8F9C3_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CWrapModeVU3Ek__BackingField_6 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t GltfTexture_get_TextureId_m1F53C4D9772FC0A4189FF212084C1AFB50E7DBC2_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CTextureIdU3Ek__BackingField_9;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_TextureId_m41C5BC6BB42FFEEA88FFF8F36DABDF403F8495B7_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CTextureIdU3Ek__BackingField_9 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t GltfTexture_get_TextureFormat_m3E443311277883CF97207657BE98095C605C90C0_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___U3CTextureFormatU3Ek__BackingField_13;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_TextureFormat_mA70D678A60ED5664862252F855EA5F12E0358456_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CTextureFormatU3Ek__BackingField_13 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Index_mEBD01A47246DDFA65D63BFEA418E534CE168204B_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___U3CIndexU3Ek__BackingField_11 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Tiling_m024B66B808551A6DB90428782D94A629D7340989_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_value;
		__this->___U3CTilingU3Ek__BackingField_7 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GltfTexture_set_Offset_mC7C4FCA647FAA257CAC7CEE793884E534540DBEE_inline (GltfTexture_t9C29E1BF684E384B94380A23E12183240B25EEA8* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) 
{
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_value;
		__this->___U3COffsetU3Ek__BackingField_8 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
